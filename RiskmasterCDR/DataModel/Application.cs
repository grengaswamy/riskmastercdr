/*Initial Design: Wizard Dev Team
 * Initial Implementation: BSB 04.22.2004
 * 
 * Application.cs contains the two root objects for dealing with the Riskmaster.DataModel which is the 
 * reincarnation of the RMObjLib Visual Basic COM module.
 * 
 * All objects publicly accessible in the DataModel must derive from the base DataModel classes.  This
 * ensures proper interfaces exist for the DataModel framework itself and that interfaces exposed for 
 * product automation are consistent.
 * 
 * All DataModel objects must be created through the ModelFactory class methods.  These
 * methods gaurantee proper population of the Context property(ies) on all DataModel objects.
 * 
 * In order to instantiate the ModelFactory (required before any other DataModel objects can be used.)
 * the client must "initialize" the factory by providing appropriate credentials and or global configuration information.
 * 
 * The Context class completes the equivalence with CRMApplication by storing global information
 * that must be shared by all DataModel objects in a single session.
 *   
 * For example these objects must be able to share a single Database connection in order 
 * to accomplish transactional updates.  
 * 
 */


using System;
using Riskmaster.Security;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Collections;
using System.Diagnostics;
using Riskmaster.Scripting;
using System.Collections.Specialized;

namespace Riskmaster.DataModel
{
	#region Enumerations
	public enum SupplementalFieldTypes : int
	{
		SuppTypeText = 0,
		SuppTypeNumber = 1,
		SuppTypeCurrency = 2,
		SuppTypeDate = 3,
		SuppTypeTime = 4,
		SuppTypeMemo = 5,
		SuppTypeCode = 6,
		SuppTypePrimaryKey = 7,
		SuppTypeEntity = 8,
		SuppTypeState = 9,
		SuppTypeClaimLookup = 10,
		SuppTypeFreeText = 11,
		SuppTypeEventLookup = 12,
		SuppTypeVehicleLookup = 13,
		SuppTypeMultiCode = 14, //        ' JP 4/30/2002
		SuppTypeMultiState = 15, //       ' JP 4/30/2002
		SuppTypeMultiEntity = 16,//      ' JP 4/30/2002
		SuppTypeSecondaryKey = 20,
        SuppTypeUserLookup = 22,//sharishkumar Jira 6415
        SuppTypeCheckBox = 31,//MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
		SuppTypeGrid = 17, // Shivendu for Supplemental Grid
        SuppTypeAttachedRecord =21,//skhare7 attached recordNumber for Diary search
		//Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
		//Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618
		//SuppTypePolicyLookup = 18,
		SuppTypePolicyTrackingLookup = 18,
		SuppTypePolicyManagementLookup = 19,
        SuppTypeHTML = 25,//asharma326 JIRA 6422
        SuppTypeHyperlink = 23 //WWIG GAP20A - agupta298 - MITS 36804 - JIRA-4691
		//End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
	}

	public enum PiType : int
	{
		PiEmployee = 1,
		PiMedicalStaff = 2,
		PiOther = 3,
		PiPatient = 4,
		PiPhysician = 5,
		PiWitness = 6, 
        PiDriver =7
	}

	enum DMTrace : int
	{
		None = 0,
		Connect = 1,
		Disconnect = 2,
		//Additional = 4,
		//Additional = 8,
		//Additional = 16,
		//Additional = 32,
		UnInitializerStack = 64,
		InitializerStack = 128

	} 
	#endregion

	/// <summary>
	/// Context class holds global context information for the current DataModel objects.
	/// During creation of DataModel objects in the Factory, the Context property of each
	/// is set to a single shared Context instance held in the factory.
	/// </summary>
	public sealed class Context : IDisposable
	{

		//So far this is the only application state we really have populated...
		public delegate void PostTransCommitHandler();
		public PreInitHandler PreInit;  //Only called from DataObject derived types.
		public string PreInitOwner;  //Will only toss the event for the InitScript of the proper Owner...
        private int m_iClientId = 0;
		private Riskmaster.Security.RiskmasterDatabase m_objRMDB = null;
		private Riskmaster.Security.UserLogin m_objUserLogin = null;
		private Riskmaster.Db.DbConnection m_objConn = null;
		private Riskmaster.Db.DbTransaction m_objTrans = null;
		private Riskmaster.Db.DbConnection m_objConnLookup = null;
		private Riskmaster.Settings.SysParms m_objSysParms = null;
		private WeakReference m_pFactory = null;
		private LocalCache m_LocalCache = null;
		private Riskmaster.Scripting.ScriptEngine m_objScriptEngine = null;
		private bool m_RegScriptFlagsLoaded = false;
		private Hashtable m_objScriptValidationErrors = null;
		private Hashtable m_objScriptWarningErrors = null;
		private const string CodeAccessPublicKey = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
		public event PostTransCommitHandler OnPostTransCommit;
		private NameValueCollection m_ContextSettings = null;
		private Hashtable m_RecordDefaults = null;
		private bool isDisposed = false;
		// Holds settings which determine 
		// Validation, Initialization, Calculate Data
		// scriptiong functions should be performed
		private bool m_runValidation = false;
		private bool m_runInitialization = false;
		private bool m_runCalculateData = false;
		private bool m_runBeforeSave = false;
		private bool m_runAfterSave = false;
        private bool m_runBeforeDelete = false;
		private bool m_WarningErrorsCleared = false;//Added by Shivendu for MITS 12258.

        //rupal:start, omig mits 33913
        private string m_sPointConnectionString = string.Empty;
        /// <summary>
        /// Returns Point Connection string only if it was set explicitely.
        /// </summary>
        public string PointConnectionString
        {
            get
            {
                return m_sPointConnectionString;
            }
            set
            {
                m_sPointConnectionString = value;
            }
        }
        //rupal:end, omig
        
		/// <summary>
		/// OVERLOADED: Class constructor
		/// </summary>
		/// <param name="strDbConnectionString">string containing the database connection string</param>
		/// <param name="objFactory">instance of DataModelFactory</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		internal Context(string strDbConnectionString, DataModelFactory objFactory, int p_iClientId)
		{
			InitializeContext(strDbConnectionString, objFactory, p_iClientId);
			m_objRMDB = new RiskmasterDatabase(p_iClientId);
			m_objRMDB.ConnectionString = strDbConnectionString;
			m_LocalCache = new LocalCache(strDbConnectionString,p_iClientId);
			m_objSysParms = new SysParms(strDbConnectionString,p_iClientId);
		} // constructor



		/// <summary>
		/// Provides handles to associated objects with DataModelFactory
		/// </summary>
		/// <param name="objUserLogin">instance of UserLogin class</param>
		/// <param name="objFactory">instance of current DataModelFactory class</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		internal Context(UserLogin objUserLogin, DataModelFactory objFactory, int p_iClientId)
		{
			m_objUserLogin = objUserLogin;
			m_objRMDB = objUserLogin.objRiskmasterDatabase;
			InitializeContext(objUserLogin.objRiskmasterDatabase.ConnectionString, objFactory, p_iClientId);
		}//constructor

		/// <summary>
		/// Initializes the context class with appropriate values to populate member variables etc.
		/// </summary>
		/// <param name="strDbConnectionString">string containing the database connection string</param>
		/// <param name="objFactory">instance of DataModelFactory</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		internal void InitializeContext(string strDbConnectionString, DataModelFactory objFactory, int p_iClientId)
		{
			m_pFactory = new WeakReference(objFactory);
			m_objSysParms = new SysParms(strDbConnectionString, p_iClientId);
			
			//Open the required database connections
			m_objConn = DbFactory.GetDbConnection(strDbConnectionString);
			m_objConn.Open();
			// Add special case logic for Sharing a Connection on specific vendors when possible.
			if (m_objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
			{
				m_objConnLookup = m_objConn;
			}//if
			else
			{
				m_objConnLookup = DbFactory.GetDbConnection(strDbConnectionString);
				m_objConnLookup.Open();
			}//else
			m_objScriptValidationErrors = new Hashtable();
			m_objScriptWarningErrors = new Hashtable();
            m_iClientId = p_iClientId;
		} // method: InitializeContext


		//			 ~Context()
		//			 {
		//				 Trace.WriteLine("***** FINALIZED DM CONTEXT*************");
		//			 }	 
		//  BSB 11.05.2005 Let the Property Lazy load this.  We don't want to cause a problem cycle during 
		//  the DataModelFactory Ctor before this context is applied onto the dmf for use by GetScriptEngine.
		//	m_objScriptEngine=GetScriptEngine();
		//run the registered methods of eventhandler
		public void PostTransThrow()
		{

			if (OnPostTransCommit != null)
			{
				PostTransCommitHandler objEventToInvoke = OnPostTransCommit;
				OnPostTransCommit = null;
				objEventToInvoke();
			}

		}
        public void DestroyPostTransForMigration()
        {
            OnPostTransCommit = null;
        }
		//BSB Used to provide a place for client code to pre-populate field values before
		// the InitScript is run against a DataObject.
		public void PreInitThrow(DataObject o, InitObjEventArgs e)
		{
			if (PreInit != null && PreInitOwner == o.GetType().Name)
			{

				PreInit(o, e);
				PreInit = null;
				PreInitOwner = null;
			}

		}

		#region Public Properties
		public DataModelFactory Factory
		{
			get
			{
				if (m_pFactory != null && m_pFactory.IsAlive)
					return (m_pFactory.Target as DataModelFactory);
				return null;
			}

		}

		public LocalCache LocalCache
		{
			get
			{
				if (m_LocalCache != null)
					return m_LocalCache;
				else
				{
					m_LocalCache = new LocalCache(this.RMDatabase.ConnectionString,m_iClientId);
					return m_LocalCache;
				}
			}
		}

		public SysParms InternalSettings
		{
			get
			{
				if (m_objSysParms != null)
					return m_objSysParms;
				else
				{
					m_objSysParms = new SysParms(this.RMDatabase.ConnectionString, this.ClientId);
					return m_objSysParms;
				}
			}
		}

        /// <summary>
        /// ClientId for Cloud Environment and 0 for Non Cloud environment
        /// </summary>
        public int ClientId 
        { 
            get 
            {
                return m_iClientId;
            } 
            set
            {
                m_iClientId = value;
            } 
        }
		#endregion


		/// <summary>
		/// Retrieves and increments the current unique id counter for "tableName" from the Riskmaster Glossary.</summary>
		/// <param name="tableName">The name of the table who's next unique row id should be retrieved and incremented.</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		public string GetRecordDefault(string className, int recordId)
		{
			string ret = "";
			if (recordId == 0 || className == "")
				return ret;

			//BSB Hack to handle Enhanced Policy.
			if (className == "Policy" && InternalSettings.SysSettings.UseEnhPolFlag != 0)
				className = "PolicyEnh";

			string sKey = String.Format("RD_{0}_{1}", className, recordId);

			if (m_RecordDefaults == null)
				m_RecordDefaults = new Hashtable(10);

			if (m_RecordDefaults.ContainsKey(sKey))
				return m_RecordDefaults[sKey] as string;

			try
			{
				DataObject obj = this.Factory.GetDataModelObject(className, false) as DataObject;
				if (obj == null)
					return ret;
				obj.MoveTo(recordId);
				ret = obj.Default;
				m_RecordDefaults.Add(sKey, ret);
			}
			catch (Exception) { }

			return ret;

		}

		/// <summary>
		/// Retrieves and increments the current unique id counter for "tableName" from the Riskmaster Glossary.</summary>
		/// <param name="tableName">The name of the table who's next unique row id should be retrieved and incremented.</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		public int GetNextUID(string tableName)
		{
            return Utilities.GetNextUID(this.DbConn.ConnectionString, tableName, ClientId);
		}//method: GetNextUID();



		#region Scripting Support

		public Hashtable ScriptWarningErrors
		{
			get
			{
				if (m_objScriptWarningErrors != null)
					return m_objScriptWarningErrors;
				else
				{
					m_objScriptWarningErrors = new Hashtable();
					return m_objScriptWarningErrors;
				}
			}
		}


		public void RunDataModelScript(ScriptTypes ScriptType, object objSender)
		{
#if NO_SCRIPT
			return;
#else

			string methodPrefix = "";
			string senderName = objSender.GetType().Name;
            //rsolanki2: short circuiting in case of sender is a supplemental object
            if (string.Compare(senderName,"supplementals",true)==0)
            {
                return;
            }

			LoadScriptFlags();
			//Start by Shivendu for MITS 12258. Clear the Warnings HashTable when first time this function is called.
			if (!m_WarningErrorsCleared)
			{
				m_objScriptWarningErrors.Clear();
				m_WarningErrorsCleared = true;
			}
			//End by Shivendu for MITS 12258
			switch (ScriptType)
			{
				case ScriptTypes.SCRIPTID_VALIDATE:								//Validate
					if (!m_runValidation)
						return;
					methodPrefix = "ValC";
					m_objScriptValidationErrors.Clear();
					break;
				case ScriptTypes.SCRIPTID_CALCULATE:								//Calculate
					if (!m_runCalculateData)
						return;
					methodPrefix = "CalcC";
					break;
				case ScriptTypes.SCRIPTID_INITIALIZE:								//Initialize
					if (!m_runInitialization)
						return;
					methodPrefix = "InitC";
					break;
				case ScriptTypes.SCRIPTID_BEFORESAVE:								//Before Save
					if (!m_runBeforeSave)
						return;
					methodPrefix = "PSC";
					break;
				case ScriptTypes.SCRIPTID_AFTERSAVE:								//After Save
					if (!m_runAfterSave)
						return;
					methodPrefix = "ASC";
					break;
                case ScriptTypes.SCRIPTID_BEFOREDELETE:								//Before Delete
                    if (!m_runBeforeDelete)
                        return;
                    methodPrefix = "PDC";
                    break;
			}

			try
			{
				this.ScriptEngine.RunScriptMethod(methodPrefix + senderName, objSender);
				// 03.06.2007 BSB MITS 8688 PersonInvolved Scripts not running.
				// Found that this is becuase the most derived class is used for senderName
				// in this case, the UI only lets us script to the PersonInvolved base class.
				// Just add a special case to run PersonInvolved scripting for all Pi types 
				// if available.
				if (objSender is PersonInvolved)
					this.ScriptEngine.RunScriptMethod(methodPrefix + "PersonInvolved", objSender);

			}
			catch (Exception ex)
			{
				string sErrorMessage = ex.Message;
				Exception oInnerEx = ex.InnerException;
				while (oInnerEx != null)
				{
					sErrorMessage += " InnerException: " + oInnerEx.Message;
					oInnerEx = oInnerEx.InnerException;
				}
				this.ScriptEngine.LogScriptError(methodPrefix + senderName, 0, ex.Source, sErrorMessage);
			}
#endif
		}

		public ScriptEngine ScriptEngine
		{
			[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CodeAccessPublicKey)]
			get
			{
				if (m_objScriptEngine != null)
					return m_objScriptEngine;
				else
				{
					m_objScriptEngine = GetScriptEngine();
					return m_objScriptEngine;
				}
			}
		}

		public Hashtable ScriptValidationErrors
		{
			get
			{
				if (m_objScriptValidationErrors != null)
					return m_objScriptValidationErrors;
				else
				{
					m_objScriptValidationErrors = new Hashtable();
					return m_objScriptValidationErrors;
				}
			}
		}


		//Should be called AFTER all other context initialization is done.
		//Prepares a runnable script engine and sets flags from the Registry.
		private ScriptEngine GetScriptEngine()
		{
#if NO_SCRIPT
			return null;
#else
			ScriptEngine objEngine = new ScriptEngine(this.RMDatabase.ConnectionString, this.ClientId);

			LoadScriptFlags();

			// Plug in Helper Object Instances
			objEngine.SetGlobal(ScriptEngine.SCR_VAL_OBJ, m_objScriptValidationErrors);
			objEngine.SetGlobal(ScriptEngine.SCR_WAR_OBJ, m_objScriptWarningErrors);
			objEngine.SetGlobal(ScriptEngine.SCR_UTIL_OBJ, this.InternalSettings); //"Riskmaster.Settings.SysParms","Riskmaster.Settings.dll","Riskmaster.Settings");
			objEngine.SetGlobal(ScriptEngine.SCR_USER_OBJ, this.RMUser);//"Riskmaster.Security.UserLogin","Riskmaster.Security.dll","Riskmaster.Security");
			objEngine.SetGlobal(ScriptEngine.SCR_CACHE_OBJ, this.LocalCache);//"Riskmaster.DataModel.Context.DataLocalCache","Riskmaster.DataModel.dll","Riskmaster.DataModel");
			objEngine.SetGlobal(ScriptEngine.SCR_STORE_OBJ, this.Factory.GetDataModelObject("DataStorage", true));//"Riskmaster.DataModel.DataStorage","Riskmaster.DataModel.dll","Riskmaster.DataModel");
			//Many of the legacy functions of ScriptEnvironment are now redundant.
			objEngine.SetGlobal(ScriptEngine.SCR_ENV_OBJ, this.Factory.GetDataModelObject("ScriptEnvironment", true)); //"Riskmaster.DataModel.ScriptEnvironment","Riskmaster.DataModel.dll","Riskmaster.DataModel");

			objEngine.StartEngine();
			return objEngine;
#endif
		}
		/// <summary>
		/// Reads the Configuration File to determine whether or not Scripting has been enabled
		/// </summary>
		internal void LoadScriptFlags()
		{
			if (m_RegScriptFlagsLoaded)
			{
				return;
			}//if

			try
			{
				Hashtable objScriptingEvents = RMConfigurationManager.GetDictionarySectionSettings("ScriptingEvents", this.RMDatabase.ConnectionString, this.ClientId);

				m_runValidation = System.Convert.ToBoolean(objScriptingEvents["ValidationEnabled"]);
				m_runInitialization = System.Convert.ToBoolean(objScriptingEvents["InitializationEnabled"]);
				m_runCalculateData = System.Convert.ToBoolean(objScriptingEvents["CalculationEnabled"]);
				m_runBeforeSave = System.Convert.ToBoolean(objScriptingEvents["BeforeSaveEnabled"]);
				m_runAfterSave = System.Convert.ToBoolean(objScriptingEvents["AfterSaveEnabled"]);
                m_runBeforeDelete = System.Convert.ToBoolean(objScriptingEvents["BeforeDeleteEnabled"]);
			}//try
			catch (System.Configuration.ConfigurationErrorsException)
			{
				//If the specified configuration section does not exist, default all values to true
				m_runValidation = true;
				m_runInitialization = true;
				m_runCalculateData = true;
				m_runBeforeSave = true;
				m_runAfterSave = true;
                m_runBeforeDelete = true;
			}//catch


			//Set the loaded flag to true
			m_RegScriptFlagsLoaded = true;
		}//LoadScriptFlags 
		#endregion


		/// <summary>
		/// TODO: Summary of Riskmaster.DataModel.Context.RMUser.</summary>
		/// <returns>Not specified.</returns>
		/// <value>TODO: Value of Riskmaster.DataModel.Context.RMUser.</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public UserLogin RMUser { get { return m_objUserLogin; } }

		/// <summary>
		/// TODO: Summary of Riskmaster.DataModel.Context.RMDatabase.</summary>
		/// <returns>Not specified.</returns>
		/// <value>TODO: Value of Riskmaster.DataModel.Context.RMDatabase.</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public RiskmasterDatabase RMDatabase { get { return m_objRMDB; } }

		public DbConnection DbConn { get { return m_objConn; } }
		public DbConnection DbConnLookup { get { return m_objConnLookup; } }
		internal void SQLBindToExistingTransContext(DbConnection objToShareFrom, DbConnection objToBeginSharing)
		{
			DbParameter p;
			DbCommand cmd;
			string sBindToken;

			try
			{
				if (objToShareFrom.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
				{


					//Invoke sp_getbindtoken on the existing transactional connection.
					cmd = objToShareFrom.CreateCommand();
					cmd.Transaction = m_objTrans;
					cmd.CommandType = System.Data.CommandType.StoredProcedure;

					if (objToShareFrom.ConnectionType == eConnectionType.Odbc)
					{
						// ODBC Needs a bit of prodding to start the transaction
						// Even though BeginTransaction has been called it doesn't 
						// "really" start until after the first query
						// has been run.
						objToShareFrom.ExecuteNonQuery("SELECT SHORT_CODE FROM CODES WHERE CODE_ID=241", m_objTrans);
						cmd.CommandText = "{? = CALL sp_getbindtoken (?)}";
					}
					else
						cmd.CommandText = "sp_getbindtoken";

					//Add return value slot
					p = cmd.CreateParameter();
					p.Size = 255;
					p.Direction = System.Data.ParameterDirection.ReturnValue;
					cmd.Parameters.Add(p);

					//Add output parameter to hold active binding token from the server.
					p = cmd.CreateParameter();
					p.ParameterName = "out_token";
					p.Direction = System.Data.ParameterDirection.Output;
					p.DbType = System.Data.DbType.AnsiString;
					p.Size = 255;
					cmd.Parameters.Add(p);

					//Get Results.
					cmd.ExecuteNonQuery();
					sBindToken = (cmd.Parameters["out_token"] as DbParameter).Value.ToString();

					//Invoke sp_bindsession on the new connection joining the transaction.
					cmd = objToBeginSharing.CreateCommand();
					cmd.CommandType = System.Data.CommandType.StoredProcedure;
					if (objToShareFrom.ConnectionType == eConnectionType.Odbc)
						cmd.CommandText = "{? = CALL sp_bindsession (?)}";
					else
						cmd.CommandText = "sp_bindsession";

					//Create slot for return value
					p = cmd.CreateParameter();
					p.Direction = System.Data.ParameterDirection.ReturnValue;
					p.Size = 4;
					cmd.Parameters.Add(p);

					//Create slot for required bind_token input. (just fetched by previous stored proc call)
					p = cmd.CreateParameter();
					p.ParameterName = "bind_token";
					p.Direction = System.Data.ParameterDirection.Input;
					if (objToShareFrom.ConnectionType == eConnectionType.Odbc)
						p.DbType = (System.Data.DbType)System.Data.Odbc.OdbcType.VarChar;
					else
						p.DbType = System.Data.DbType.AnsiString;
					p.Size = 8000;
					p.Value = sBindToken;
					cmd.Parameters.Add(p);

					//Get Results
					cmd.ExecuteNonQuery();
					//Trace.WriteLine( (cmd.Parameters[0] as DbParameter).Value);
				}
			}
            catch (Exception e) { Log.Write(String.Format(Globalization.GetString("Context.SQLBindToExistingTransContext.Exception", this.ClientId), e.Message), this.ClientId); }
		}

		public DbTransaction TransStart()
		{
			if (DbTrans != null)
				return DbTrans;

			this.DbTrans = this.DbConn.BeginTransaction();
			return this.DbTrans;
		}
		public void TransCommit()
		{
			if (DbTrans != null)
			{
				DbTrans.Commit();
				PostTransThrow(); //Invoke any waiting handlers.
			}
			DbTrans = null;
		}
		public void TransRollback()
		{
			if (DbTrans != null)
			{
				DbTrans.Rollback();
				OnPostTransCommit = null; //Just Clear any waiting handlers.
			}
			DbTrans = null;
		}
		public DbTransaction DbTrans
		{
			get { return m_objTrans; }
			set
			{
				m_objTrans = value;
				if (value == null)
					return;
				if (m_objConn.DatabaseType == Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR)
					this.SQLBindToExistingTransContext(m_objConn, m_objConnLookup);//Join the Lookup connection into the same context to dodge dead-lock issues.
			}
		}

		#region IDisposable Members
		/// <summary>
		/// Dispose method to destroy all open
		/// managed and unmanaged resources in the class
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Protected method following IDisposable pattern
		/// for destroying managed and unmanaged resources
		/// </summary>
		/// <param name="disposing">boolean indicating whether or not the Dispose method was explicitly
		/// called on the class/object</param>
		private void Dispose(bool disposing)
		{
			
			if (! isDisposed)    
			{
				if (disposing)
				{
					Trace.WriteLine("*************Disposing Context (and closing associated Db Connections)******************");
					//Cleanup managed objects
					if (m_objConn != null)
					{
						m_objConn.Close();
						m_objConn.Dispose();
					} // if

					if (m_objConnLookup != null)
					{
						m_objConnLookup.Close();
						m_objConn.Dispose();
					} // if

					if (m_LocalCache != null)
					{
						m_LocalCache.Dispose();
						m_LocalCache = null;
					} // if

					if (m_objScriptEngine != null)
					{
						m_objScriptEngine.StopEngine();
						m_objScriptEngine = null;
					} // if
				} // if

			} // if
			isDisposed = true;
		} // method: Dispose


		/// <summary>
		/// Class destructor in the event that the Dispose method
		/// is not called on the class
		/// </summary>
		~Context()
		{
			Dispose(false);
		}//destructor

		#endregion

		
	}
}
