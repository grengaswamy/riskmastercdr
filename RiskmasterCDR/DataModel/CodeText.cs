
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Code Text is NOT a full fledged object library object.
	/// The reason for this is that it has no single unique row_id type key.
	/// Save, Delete etc will be handled from CodeTextList.
	/// </summary>
	public class CodeTextItem  : DataRoot	
	{
		int m_CodeId = 0;
		int m_LanguageCode = 0;
		string m_ShortCode ="";
		string m_CodeDesc="";
#region Field junk
		public int CodeId
		{
			get
			{
				return m_CodeId;
			}
			set
			{
				if(m_CodeId!=value)
					base.m_DataChanged = true;
				m_CodeId=value;
			}
		}
		public int LanguageCode
		{
			get
			{
				return m_LanguageCode;
			}
			set
			{
				if(m_LanguageCode!=value)
					base.m_DataChanged = true;
				m_LanguageCode=value;
			}
		}
		public string ShortCode
		{
			get
			{
				return m_ShortCode;
			}
			set
			{
				if(m_ShortCode!=value)
					base.m_DataChanged = true;
				m_ShortCode=value;
			}
		}
		public string CodeDesc
		{
			get
			{
				return m_CodeDesc;
			}
			set
			{
				if(m_CodeDesc!=value)
					base.m_DataChanged = true;
				m_CodeDesc=value;
			}
		}
		#endregion

		internal CodeTextItem(bool isLocked,Context context):base(isLocked, context){}
		public void ClearFields()
		{
			 m_CodeId = 0;
			 m_LanguageCode = 0;
			 m_ShortCode ="";
			 m_CodeDesc="";
		}
		public override bool DataChanged{get{return base.m_DataChanged;}set{base.m_DataChanged = value;}}
	}
}