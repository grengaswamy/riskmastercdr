﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POINT_UNIT_DATA_STGNG", "ROW_ID")]
    class PointUnitDataStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"RowId", "ROW_ID"},
														{"UnitId", "UNIT_ID"},
                                                        {"UnitType", "UNIT_TYPE"},
                                                        {"UnitNumber", "UNIT_NUMBER"},
                                                        {"UnitRiskLoc", "UNIT_RISK_LOC"},
                                                        {"UnitRiskSubLoc", "UNIT_RISK_SUB_LOC"},
														{"SiteSeqNumber", "SITE_SEQ_NUMBER"},
                                                        {"Product", "PRODUCT"},
                                                        {"INSLine", "INS_LINE"},
                                                        {"StatUnitNumber", "STAT_UNIT_NUMBER"},
                                                        {"UnitPremium", "UNIT_PREM"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
		};

        public int PolicyUnitRowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }

        public int UnitId { get { return GetFieldInt("UNIT_ID"); } set { SetField("UNIT_ID", value); } }
        public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
        public string UnitNumber { get { return GetFieldString("UNIT_NUMBER"); } set { SetField("UNIT_NUMBER", value); } }
        public string UnitRiskLoc { get { return GetFieldString("UNIT_RISK_LOC"); } set { SetField("UNIT_RISK_LOC", value); } }
        public string UnitRiskSubLoc { get { return GetFieldString("UNIT_RISK_SUB_LOC"); } set { SetField("UNIT_RISK_SUB_LOC", value); } }
        public string SiteSeqNumber { get { return GetFieldString("SITE_SEQ_NUMBER"); } set { SetField("SITE_SEQ_NUMBER", value); } }
        public string Product { get { return GetFieldString("PRODUCT"); } set { SetField("PRODUCT", value); } }
        public string INSLine { get { return GetFieldString("INS_LINE"); } set { SetField("INS_LINE", value); } }
        public string StatUnitNumber { get { return GetFieldString("STAT_UNIT_NUMBER"); } set { SetField("STAT_UNIT_NUMBER", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public double UnitPremium { get { return GetFieldDouble("UNIT_PREM"); } set { SetField("UNIT_PREM", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Ends
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        internal PointUnitDataStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "POINT_UNIT_DATA_STGNG";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = string.Empty;
            this.m_sParentClassName = "PolicyStgng";
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();
        }
    }
}
