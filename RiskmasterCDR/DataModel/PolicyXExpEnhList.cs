
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXExpEnhList.
	/// </summary>
	public class PolicyXExpEnhList : DataCollection
	{
		internal PolicyXExpEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EXPOSURE_ID";
			this.SQLFromTable =	"POLICY_X_EXP_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXExpEnh";
		}
		public new PolicyXExpEnh this[int keyValue]{get{return base[keyValue] as PolicyXExpEnh;}}
		public new PolicyXExpEnh AddNew(){return base.AddNew() as PolicyXExpEnh;}
		public  PolicyXExpEnh Add(PolicyXExpEnh obj){return base.Add(obj) as PolicyXExpEnh;}
		public new PolicyXExpEnh Add(int keyValue){return base.Add(keyValue) as PolicyXExpEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}