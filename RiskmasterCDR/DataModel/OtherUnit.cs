﻿using System;


namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("OTHER_UNIT", "OTHER_UNIT_ID", "SiteNumber")]
    public class OtherUnit : DataObject	
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"OtherUnitId", "OTHER_UNIT_ID"},
                                        {"EntityId", "ENTITY_ID"},
                                        {"UnitType", "UNIT_TYPE"},                                        
                                        {"DeletedFlag", "DELETED_FLAG"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"}    
                                    };

        public int OtherUnitId { get { return GetFieldInt("OTHER_UNIT_ID"); } set { SetField("OTHER_UNIT_ID", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }

        #endregion

        #region Child Implementation

        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            base.OnChildPostSave(childName, childValue, isNew);
        }
        #endregion

        internal OtherUnit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "OTHER_UNIT";
            this.m_sKeyField = "OTHER_UNIT_ID";
            this.m_sFilterClause = string.Empty;
            //Add all object Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            this.m_sParentClassName = string.Empty;
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();
        }

        internal override string OnBuildDeleteSQL()
        {
            return "UPDATE OTHER_UNIT SET DELETED_FLAG = -1 WHERE SITE_ID=" + this.OtherUnitId;
        }

        internal override bool OnSuppDelete()
        {
            return false;
        }


        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            return;
        }

        internal override string OnApplyFilterClauseSQL(string CurrentSQL)
        {
            string sExtendedFilterClause = this.m_sFilterClause;
            string sSQL = CurrentSQL;
            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                sExtendedFilterClause += " AND ";
            }
            sExtendedFilterClause += "DELETED_FLAG=0";

            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                if (sSQL.IndexOf(" WHERE ") > 0)
                {
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                }
                else
                {
                    sSQL += " WHERE " + sExtendedFilterClause;
                }
            }
            return sSQL;
        }
      

        #region Supplemental Fields Exposed

        /// <summary>
        /// Supplementals are implemented in the base class but not exposed externally.
        ///This allows the derived class to control whether the object appears to have
        ///supplementals or not. Entity exposes Supplementals.
        /// </summary>
        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion
    }
}
