﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class LinkedEventInfoList : DataCollection
    {
        internal LinkedEventInfoList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "LNKEVT_ROW_ID";
            this.SQLFromTable = "EVENT_X_LINK_EVT_INFO_PSO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "LinkedEventInfo";
		}
        public new LinkedEventInfo this[int keyValue] { get { return base[keyValue] as LinkedEventInfo; } }
        public new LinkedEventInfo AddNew() { return base.AddNew() as LinkedEventInfo; }
        public LinkedEventInfo Add(LinkedEventInfo obj) { return base.Add(obj) as LinkedEventInfo; }
        public new LinkedEventInfo Add(int keyValue) { return base.Add(keyValue) as LinkedEventInfo; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
