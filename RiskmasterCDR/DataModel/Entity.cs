/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/10/2014 | 34276  | achouhan3   | changes req for Entity ID Number and ID Number Type
 **********************************************************************************************/
using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Data;
namespace Riskmaster.DataModel
{
    ///	<summary>
    ///	Summary	description	for	Entity.
    ///	</summary>
    //TODO:	Complete Summary
    [Riskmaster.DataModel.Summary("ENTITY", "ENTITY_ID")]
    public class Entity : DataObject
    {
        public override string Default
        {
            get
            {
                if (this.FirstName != "")
                    return this.GetLastFirstName();
                else
                    return this.LastName;
            }
        }

        //Book-Keeping for Locking Modes
        protected bool m_LockEntityTableChange = false;
        protected bool m_LockMoveNavigation = false;
        protected bool m_HideDeletedRecords = true;
        private string m_sFormName = string.Empty; //rjhamb 22497
        private bool m_AfterSave = true; // atavaragiri: mits 29058//
        //mits 35925 start
        private int iAddressSeqNum = 0;

        //mits 35925 end
        #region	Database Field List
        private string[,] sFields =	
{
{ "County",	"COUNTY"},
{ "BusinessTypeCode", "BUSINESS_TYPE_CODE"},
{ "NatureOfBusiness", "NATURE_OF_BUSINESS"}, 
{ "SicCode", "SIC_CODE"},
{ "SicCodeDesc", "SIC_CODE_DESC"}, 
{ "WcFillingNumber", "WC_FILING_NUMBER"}, 
{ "EntityId", "ENTITY_ID"},	
{ "EntityTableId", "ENTITY_TABLE_ID"}, 
{ "LastName", "LAST_NAME"},	
{ "LastNameSoundex", "LAST_NAME_SOUNDEX"},
{ "FirstName", "FIRST_NAME"}, 
{ "AlsoKnownAs", "ALSO_KNOWN_AS"},
{ "Abbreviation", "ABBREVIATION"}, 
{ "CostCenterCode",	"COST_CENTER_CODE"}, 
{ "Addr1", "ADDR1"}, 
{ "Addr2", "ADDR2"}, 
{ "Addr3", "ADDR3"}, 
{ "Addr4", "ADDR4"}, 
{ "City", "CITY"}, 
{ "CountryCode", "COUNTRY_CODE"}, 
{ "StateId", "STATE_ID"}, 
{ "ZipCode", "ZIP_CODE"}, 
{ "ParentEid", "PARENT_EID"},
{ "TaxId", "TAX_ID"},
{ "Contact", "CONTACT"}, 
{ "Comments", "COMMENTS"}, 
{ "EmailTypeCode", "EMAIL_TYPE_CODE"}, 
{ "EmailAddress", "EMAIL_ADDRESS"},	
{ "SexCode", "SEX_CODE"}, 
{ "BirthDate", "BIRTH_DATE"}, 
{ "Phone1",	"PHONE1"}, 
{ "Phone2",	"PHONE2"}, 
{ "FaxNumber", "FAX_NUMBER"}, 
{"DttmRcdAdded","DTTM_RCD_ADDED"},
{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
{"UpdatedByUser","UPDATED_BY_USER"},
{"AddedByUser","ADDED_BY_USER"},
{ "DeletedFlag", "DELETED_FLAG"},
	//{	"",	"SEC_DEPT_EID"},
{ "TriggerDateField", "TRIGGER_DATE_FIELD"}, 
{ "EffStartDate", "EFF_START_DATE"}, 
{ "EffEndDate",	"EFF_END_DATE"},
{ "Parent1099EID", "PARENT_1099_EID"},
{ "Entity1099Reportable", "REPORT_1099_FLAG"},
{ "MiddleName",	"MIDDLE_NAME"},	
{ "Title", "TITLE"},
{ "NaicsCode", "NAICS_CODE"},
{ "RMUserId", "RM_USER_ID"},
{ "FreezePayments",	"FREEZE_PAYMENTS"},
{"HTMLComments","HTMLCOMMENTS"},
// Start Naresh 9/20/2007 MI Forms Changes
// Start Gagan for MITS 11220
{"OrganizationType","ORGANIZATION_TYPE"},
// End Naresh 9/20/2007 MI Forms Changes
{"NPINumber","NPI_NUMBER"},
// End Gagan for MITS 11220
//Animesh Inserted RMSC Bill Review  //skhare7 RMSC Merge 28397
{"HBRAmt","HOSPITAL_AMT"},
{"HBRFeeLines","HOSPITAL_LINES"},
{"MBRAmt","MEDICAL_AMT"},
{"MBRFeeLines","MEDICAL_LINES"},
//Animesh Insertion Ends   //skhare7 RMSC Merge 28397 End
//sgoel6 Medicare 05/14/2009
{"MMSEATINEditFlag","MMSEA_TIN_EDT_FLAG"},
{"EmailPassword","FROI_ACORD_PASSWORD"}, //added by Navdeep :chubb 
{"AutoDiscFlg","DISCOUNT_FLAG"},
{"TimeZoneTracking","TIME_ZONE_TRACKING"},  // By Yatharth for Time Zone tracking
//{"DayLightSavings","DAY_LIGHT_SAVINGS"},    // By Yatharth for Day Light Savings
{"TimeZoneCode","TIME_ZONE_CODE"},           // By Yatharth for Time Zone Code
{"AutoDiscount","AUTO_DISCOUNT"},
//Start rsushilaggar 05/05/2010 MITs 20606
{"EntityApprovalStatusCode","ENTITY_APPROVAL_STATUS"},
{"RejectReasonText","ENTITY_REJECT_COMMENTS"},
{"RejectReasonText_HTMLComments","ENTITY_REJECT_HTMLCOMMENTS"},
//End rsushilaggar
{"OverrideOfacCheck","OVERRIDE_OFAC_CHECK"},  //Yatharth - Payee Check Utility
{"CurrencyType", "ORG_CURR_CODE"},
{"IDType", "ID_TYPE"}, //mbahl3 mits:29316
{"LegalName","LEGAL_NAME"},//prashbiharis MITS:30225
//mbahl3 mits 30224
{"EffectiveDate", "EFFECTIVE_DATE"},
{"ExpirationDate", "EXPIRATION_DATE"},
//mbahl3 mits 30224
//Ankit Start : Financial Enhancement - Prefix and Suffix Changes
{"Prefix", "PREFIX"},
{"SuffixCommon", "SUFFIX_COMMON"},
{"SuffixLegal", "SUFFIX_LEGAL"}, 
//Ankit End
{"ReferenceNumber", "REFERENCE_NUMBER"} //Praveen PI Changes
,{"ClientSequenceNumber", "CLIENT_SEQ_NUM"},
//mits 35925  (dbisht6 :removing all the refernce of ADDRESS_SEQ_NUM in code behind)
//{"AddressSequenceNumber", "ADDRESS_SEQ_NUM"},
//dbisht6 end
{"NameType","NAME_TYPE"},
//Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
{"AdjusterEId", "ASSIGNADJ_EID"},

//Ankit End
{"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"},
//added by swati- RMA - 5497
{"NcciCarrierCode","NCCI_CARRIER_CODE"},
//change end here by swati- RMA - 5497
{"DUNSNumber", "CLUE_DUNS_NUM"}, //neha goel MITS#36916 PMC gap 7 CLUE - RMA - 5499
{"AUSTetraNumber", "CLUE_AUS_TETRA_NUM"}, //neha goel MITS#36916 PMC gap 7 CLUE - RMA - 5499
{"ExperianBIN", "CLUE_EXPN_BIN_NUM"} //neha goel MITS#36916 PMC gap 7 CLUE - RMA - 5499
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
        //mbahl3 mits 30224
        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        //mbahl3 mits 30224
        //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_PREFIX")]
        public int Prefix { get { return GetFieldInt("PREFIX"); } set { SetField("PREFIX", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_SUFFIX")]
        public int SuffixCommon { get { return GetFieldInt("SUFFIX_COMMON"); } set { SetField("SUFFIX_COMMON", value); } }
        public string SuffixLegal { get { return GetFieldString("SUFFIX_LEGAL"); } set { SetField("SUFFIX_LEGAL", value); } }
        //Ankit End
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        [ExtendedTypeAttribute(RMExtType.Entity, "ADJUSTERS")]
        public int AdjusterEId { get { return GetFieldInt("ASSIGNADJ_EID"); } set { SetField("ASSIGNADJ_EID", value); } }
        //Ankit End
        //mbahl3 mits:29316
        public int IDType { get { return GetFieldInt("ID_TYPE"); } set { SetField("ID_TYPE", value); } }
        //mbahl3 mits:29316
        //prashbiharis MITS:30225 
        public string LegalName { get { return GetFieldString("LEGAL_NAME"); } set { SetField("LEGAL_NAME", value); } }
        //prashbiharis MITS:30225
        public string County { get { return GetFieldString("COUNTY"); } set { SetField("COUNTY", value); } }
        public string NatureOfBusiness { get { return GetFieldString("NATURE_OF_BUSINESS"); } set { SetField("NATURE_OF_BUSINESS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "BUSINESS_TYPE")]
        public int BusinessTypeCode { get { return GetFieldInt("BUSINESS_TYPE_CODE"); } set { SetField("BUSINESS_TYPE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SIC_CODE")]
        public int SicCode { get { return GetFieldInt("SIC_CODE"); } set { SetField("SIC_CODE", value); } }
        public string SicCodeDesc { get { return GetFieldString("SIC_CODE_DESC"); } set { SetField("SIC_CODE_DESC", value); } }
        public string WcFillingNumber { get { return GetFieldString("WC_FILING_NUMBER"); } set { SetField("WC_FILING_NUMBER", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int EntityTableId
        {
            get { return GetFieldInt("ENTITY_TABLE_ID"); }
            set
            {

                if (m_LockEntityTableChange && this.EntityTableId != value)
                    throw new DataModelException(Globalization.GetString("Entity.EntityTableId.get.Exception.EntityTableIsLocked", this.Context.ClientId));
                SetField("ENTITY_TABLE_ID", value);
            }
        }
        public string LastName
        {
            get { return GetFieldString("LAST_NAME"); }
            set
            {
                SetField("LAST_NAME", value);
                // BSB 02.07.2006 Hack - Soundex defaulting to "000" causes dirty flag to flip for new
                // records during population.  
                // Just skip this if the record is new and value is null or empty string.
                if (!this.IsNew || (value != null && value != ""))
                    SetField("LAST_NAME_SOUNDEX", Utilities.FindSoundexValue(value));
            }
        }
        public string LastNameSoundex { get { return GetFieldString("LAST_NAME_SOUNDEX"); } }
        public string FirstName { get { return GetFieldString("FIRST_NAME"); } set { SetField("FIRST_NAME", value); } }
        public string AlsoKnownAs { get { return GetFieldString("ALSO_KNOWN_AS"); } set { SetField("ALSO_KNOWN_AS", value); } }
        public string Abbreviation { get { return GetFieldString("ABBREVIATION"); } set { SetField("ABBREVIATION", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COST_CENTER")]
        public int CostCenterCode { get { return GetFieldInt("COST_CENTER_CODE"); } set { SetField("COST_CENTER_CODE", value); } }
        public string Addr1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Addr2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public int CountryCode { get { return GetFieldInt("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public int StateId { get { return GetFieldInt("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int ParentEid { get { return GetFieldInt("PARENT_EID"); } set { SetField("PARENT_EID", value); } }
        //public string TaxId{get{ return	GetFieldStringForTaxId("TAX_ID");}set{SetField("TAX_ID",value);}}
        //Deb MITS 25775 
        //mbahl3 mits:29316
        public string TaxId { get { return GetFieldStringForTaxId("TAX_ID", "ID_TYPE"); } set { SetFieldForTaxId("TAX_ID", value, "ID_TYPE"); } }
        //mbahl3 mits:29316
        //skhare7 Start : Worked on JIRA - 1394
        public string UnMaskedTaxId
        {
            get
            {
                return GetFieldString("TAX_ID");
            }
        }
        //skhare7 End
        public string Contact { get { return GetFieldString("CONTACT"); } set { SetField("CONTACT", value); } }
        public string Comments { get { return GetFieldString("COMMENTS"); } set { SetField("COMMENTS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "EMAIL_TYPE")]
        public int EmailTypeCode { get { return GetFieldInt("EMAIL_TYPE_CODE"); } set { SetField("EMAIL_TYPE_CODE", value); } }
        public string EmailAddress { get { return GetFieldString("EMAIL_ADDRESS"); } set { SetField("EMAIL_ADDRESS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SEX_CODE")]
        public int SexCode { get { return GetFieldInt("SEX_CODE"); } set { SetField("SEX_CODE", value); } }
        public string BirthDate { get { return GetFieldString("BIRTH_DATE"); } set { SetField("BIRTH_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string Phone1 { get { return GetFieldString("PHONE1"); } set { SetField("PHONE1", value); } }
        public string Phone2 { get { return GetFieldString("PHONE2"); } set { SetField("PHONE2", value); } }
        public string FaxNumber { get { return GetFieldString("FAX_NUMBER"); } set { SetField("FAX_NUMBER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string TriggerDateField { get { return GetFieldString("TRIGGER_DATE_FIELD"); } set { SetField("TRIGGER_DATE_FIELD", value); } }
        public string EffStartDate { get { return GetFieldString("EFF_START_DATE"); } set { SetField("EFF_START_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string EffEndDate { get { return GetFieldString("EFF_END_DATE"); } set { SetField("EFF_END_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int Parent1099EID { get { return GetFieldInt("PARENT_1099_EID"); } set { SetField("PARENT_1099_EID", value); } }
        public bool Entity1099Reportable { get { return GetFieldBool("REPORT_1099_FLAG"); } set { SetField("REPORT_1099_FLAG", value); } }
        public string MiddleName { get { return GetFieldString("MIDDLE_NAME"); } set { SetField("MIDDLE_NAME", value); } }
        public string Title { get { return GetFieldString("TITLE"); } set { SetField("TITLE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "NAICS_CODES")]
        public int NaicsCode { get { return GetFieldInt("NAICS_CODE"); } set { SetField("NAICS_CODE", value); } }
        //By Yatharth - to get/set Override Ofac Check
        public bool OverrideOfacCheck { get { return GetFieldBool("OVERRIDE_OFAC_CHECK"); } set { SetField("OVERRIDE_OFAC_CHECK", value); } }
        //By Yatharth- to get/set Time Zone Code
        [ExtendedTypeAttribute(RMExtType.Code, "TIME_ZONE_CODES")]
        public int TimeZoneCode { get { return GetFieldInt("TIME_ZONE_CODE"); } set { SetField("TIME_ZONE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "RM_SYS_USERS")]
        public int RMUserId { get { return GetFieldInt("RM_USER_ID"); } set { SetField("RM_USER_ID", value); } }
        public bool FreezePayments { get { return GetFieldBool("FREEZE_PAYMENTS"); } set { SetField("FREEZE_PAYMENTS", value); } }
        //By Yatharth- to get/set Time Zone tracking
        public bool TimeZoneTracking { get { return GetFieldBool("TIME_ZONE_TRACKING"); } set { SetField("TIME_ZONE_TRACKING", value); } }
        //By Yatharth- to get/set Day Light Savings
        //public bool DayLightSavings { get { return GetFieldBool("DAY_LIGHT_SAVINGS"); } set { SetField("DAY_LIGHT_SAVINGS", value); } }
        // Start Naresh 9/20/2007 MI Forms Changes
        [ExtendedTypeAttribute(RMExtType.Code, "ORGANIZATION_TYPE")]
        public int OrganizationType { get { return GetFieldInt("ORGANIZATION_TYPE"); } set { SetField("ORGANIZATION_TYPE", value); } }
        // End Naresh 9/20/2007 MI Forms Changes
        // Start Gagan for MITS 11220       
        public string NPINumber { get { return GetFieldString("NPI_NUMBER"); } set { SetField("NPI_NUMBER", value); } }
        // End Gagan for MITS 11220       
        //ADDED BY Navdeep  
        public string EmailPassword { get { return GetFieldString("FROI_ACORD_PASSWORD"); } set { SetField("FROI_ACORD_PASSWORD", value); } }
        //Animesh Inserted RMSC Bill Review  //skhare7 RMSC Merge 28397
        public int HBRFeeLines { get { return GetFieldInt("HOSPITAL_LINES"); } set { SetField("HOSPITAL_LINES", value); } }
        public double HBRAmt { get { return GetFieldDouble("HOSPITAL_AMT"); } set { SetField("HOSPITAL_AMT", value); } }
        public int MBRFeeLines { get { return GetFieldInt("MEDICAL_LINES"); } set { SetField("MEDICAL_LINES", value); } }
        public double MBRAmt { get { return GetFieldDouble("MEDICAL_AMT"); } set { SetField("MEDICAL_AMT", value); } }
        //Animesh Insertion Ends
        //sgoel6 Medicare 05/14/2009
        public bool MMSEATINEditFlag { get { return GetFieldBool("MMSEA_TIN_EDT_FLAG"); } set { SetField("MMSEA_TIN_EDT_FLAG", value); } }
        public bool AutoDiscFlg { get { return GetFieldBool("DISCOUNT_FLAG"); } set { SetField("DISCOUNT_FLAG", value); } }
        public double AutoDiscount { get { return GetFieldDouble("AUTO_DISCOUNT"); } set { SetField("AUTO_DISCOUNT", value); } }
        //public string HTMLComments{get{return GetFieldString("HTMLCOMMENTS");}set{SetField("HTMLCOMMENTS",value);}}
        public string HTMLComments { get { return GetFieldStringFallback("HTMLCOMMENTS", "COMMENTS"); } set { SetField("HTMLCOMMENTS", value); } }
        //Start rsushilaggar 05/05/2010 MITs 20606
        public string RejectReasonText { get { return GetFieldString("ENTITY_REJECT_COMMENTS"); } set { SetField("ENTITY_REJECT_COMMENTS", value); } }
        public string RejectReasonText_HTMLComments { get { return GetFieldStringFallback("ENTITY_REJECT_HTMLCOMMENTS", "ENTITY_REJECT_COMMENTS"); } set { SetField("ENTITY_REJECT_HTMLCOMMENTS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_APPRV_REJ")]
        public int EntityApprovalStatusCode { get { return GetFieldInt("ENTITY_APPROVAL_STATUS"); } set { SetField("ENTITY_APPROVAL_STATUS", value); } }
        //End rsushilaggar
        [ExtendedTypeAttribute(RMExtType.Code, "CURRENCY_TYPE")]
        public int CurrencyType { get { return GetFieldInt("ORG_CURR_CODE"); } set { SetField("ORG_CURR_CODE", value); } }
        //Added for Froi Migration- Mits 33585,33586,33587
        public string WCFilingNumber
        {
            get
            {
                return this.WcFillingNumber;
            }
        }
        public string ReferenceNumber { get { return GetFieldString("REFERENCE_NUMBER"); } set { SetField("REFERENCE_NUMBER", value); } } // Praveen PI Changes
        public string ClientSequenceNumber { get { return GetFieldString("CLIENT_SEQ_NUM"); } set { SetField("CLIENT_SEQ_NUM", value); } }
        //  mits 35925 (dbisht6:removing all the refernce of ADDRESS_SEQ_NUM in code behind)
        // public string AddressSequenceNumber { get { return GetFieldString("ADDRESS_SEQ_NUM"); } set { SetField("ADDRESS_SEQ_NUM", value); } }
        //dbisht6 end
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_NAME_TYPE")]
        public int NameType { get { return GetFieldInt("NAME_TYPE"); } set { SetField("NAME_TYPE", value); } }
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        int iClaimID = 0;
        public int ClaimId
        {
            get
            {
                return iClaimID;
            }
            set
            {
                iClaimID = value;
            }
        }
        //Ankit End
        //added by swati- RMA - 5497
        public string NcciCarrierCode { get { return GetFieldString("NCCI_CARRIER_CODE"); } set { SetField("NCCI_CARRIER_CODE", value); } }
        //change end here by swati- RMA - 5497
        //Neha Goel MITS# 36916 PMC Gap 7 CLUE --start- RMA - 5499
        public string DUNSNumber { get { return GetFieldString("CLUE_DUNS_NUM"); } set { SetField("CLUE_DUNS_NUM", value); } }
        public string AUSTetraNumber { get { return GetFieldString("CLUE_AUS_TETRA_NUM"); } set { SetField("CLUE_AUS_TETRA_NUM", value); } }
        public string ExperianBIN { get { return GetFieldString("CLUE_EXPN_BIN_NUM"); } set { SetField("CLUE_EXPN_BIN_NUM", value); } }
        //Neha Goel MITS# 36916 PMC Gap 7 CLUE --end- RMA - 5499
        #endregion
        #region	Child List
        //String array of the form {{ChildName1,ChildType1}{ChildName2,ChildType2}...}
        private string[,] sChildren = {{"EntityXExposureList","EntityXExposureList"},
                {"EntityXRoleList","EntityXRoleList"},      //avipinsrivas start : Worked for Jira-340
                {"EntityXOperatingAsList","EntityXOperatingAsList"},
                {"EntityXContactInfoList","EntityXContactInfoList"},
                {"EntityXSelfInsuredList","EntityXSelfInsuredList"},
                {"ClientLimitsList","ClientLimitsList"},
                {"JurisLicenseOrCodeList","JurisLicenseOrCodeList"},
                {"Instructions","EntInstructions"},
                {"CMMSEAXDataList","CMMSEAXDataList"},
                {"EntityXAddressesList","EntityXAddressesList"}, //Added Rakhi for R7:Add Employee Data Elements
                {"AddressXPhoneInfoList","AddressXPhoneInfoList"}, //Added Rakhi for R7:Add Employee Data Elements
                {"EntityXWithholding","EntityXWithholding"}, //smishra54: R8 Withholding Enhancement, MITS 26019
                {"EntityXEntityIDTypeList","EntityXEntityIDTypeList"} //MITS:34276-- Entity ID Type  
            };
        #endregion

        #region	Supplemental Fields	Exposed
        //Supplementals	are	implemented	in the base	class but not exposed externally.
        //This allows the derived class	to control whether the object appears to have
        //supplementals	or not.	
        //Entity exposes Supplementals.
        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion


        internal Entity(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so	that scripting can be called successfully.

            this.m_sTableName = "ENTITY";
            this.m_sKeyField = "ENTITY_ID";
            this.m_sFilterClause = "";

            //Add all obect	Fields into	the	Field Collection from our Field	List.
            base.InitFields(sFields);

            //Add all object Children into the Children	collection from	our	"sChildren"	list.
            base.InitChildren(sChildren);
            this.m_sParentClassName = "";
            base.Initialize();	//Moved	after most init	logic so that scripting	can	be called successfully.

        }


        protected override void Clear()
        {
            int prevEntityTableId = this.EntityTableId;
            base.Clear();
            if (this.m_LockEntityTableChange)
                this.m_Fields["ENTITY_TABLE_ID"] = prevEntityTableId;

            this.DataChanged = false;
        }

        protected override bool OnForceParentSave()
        {
            return false;
        }

        internal override string OnApplyFilterClauseSQL(string CurrentSQL)
        {
            string sSQL = CurrentSQL;
            string sExtendedFilterClause = m_sFilterClause;

            if (m_LockEntityTableChange)
            {
                if (sExtendedFilterClause != "")
                    sExtendedFilterClause += " AND ";
                sExtendedFilterClause += "ENTITY_TABLE_ID=" + this.EntityTableId;
            }

            if (this.HideDeletedRecords)
            {
                if (sExtendedFilterClause != "")
                    sExtendedFilterClause += " AND ";
                sExtendedFilterClause += "DELETED_FLAG=0";
            }

            if (sExtendedFilterClause != "")
                if (sSQL.IndexOf(" WHERE ") > 0)
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                else
                    sSQL += " WHERE " + sExtendedFilterClause;

            return sSQL;
        }

        internal override string OnBuildDeleteSQL()
        {
            return String.Format("UPDATE ENTITY	SET	DELETED_FLAG = -1, DTTM_RCD_LAST_UPD= '{0}', UPDATED_BY_USER='{1}' WHERE	ENTITY_ID={2}", Common.Conversion.ToDbDateTime(DateTime.Now), Context.RMUser.LoginName, EntityId);
            //return base.OnBuildDeleteSQL ();
        }

        // Cancel Deletion of Supplementals during "delete" of this object.
        internal override bool OnSuppDelete()
        {
            return false;
        }

        // Cancel Deletion of all Entity children.
        // (Obviously will these children would be needed if the entity is ever 
        // "undeleted" by changing the entity deleted flag.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            //avipinsrivas start : Worked for Jira-340
            //string sSQL = "";
            //switch (childName)
            //{
            //    case "EntityXRoleList":
            //        sSQL = String.Format("UPDATE ENTITY_X_ROLES SET DELETED_FLAG = -1 WHERE ENTITY_ID={0} ", (int)this.KeyFieldValue);
            //        Context.DbConn.ExecuteNonQuery(sSQL, Context.DbTrans);
            //        break;
            //}
            ////avipinsrivas End
            return;
        }
        // This	is where for collection	items, the foreign key values on new items 
        // can be set by the parent	at the time	of collection membership.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "EntityXExposureList":
                    itemValue.Parent = this;
                    (itemValue as EntityXExposure).EntityId = this.EntityId;
                    break;
                //avipinsrivas start : Worked for Jira-340
                case "EntityXRoleList":
                    (itemValue as EntityXRole).EntityId = this.EntityId;
                    break;
                //avipinsrivas End
                case "EntityXOperatingAsList":
                    itemValue.Parent = this;
                    (itemValue as EntityXOperatingAs).EntityId = this.EntityId;
                    break;
                case "EntityXContactInfoList":
                    itemValue.Parent = this;
                    (itemValue as EntityXContactInfo).EntityId = this.EntityId;
                    break;
                case "EntityXSelfInsuredList":
                    itemValue.Parent = this;
                    (itemValue as EntityXSelfInsured).EntityId = this.EntityId;
                    break;
                case "ClientLimitsList":
                    itemValue.Parent = this;
                    (itemValue as ClientLimits).ClientEid = this.EntityId;
                    break;
                case "JurisLicenseOrCodeList":
                    itemValue.Parent = this;
                    (itemValue as JurisLicenseOrCode).EntityId = this.EntityId;
                    break;
                case "CMMSEAXDataList":
                    itemValue.Parent = this;
                    (itemValue as CMMSEAXData).EntityEID = this.EntityId;
                    break;
                case "EntityXAddressesList": //Added Rakhi for R7:Add Employee Data Elements
                    itemValue.Parent = this;
                    (itemValue as EntityXAddresses).EntityId = this.EntityId;
                    break;
                case "AddressXPhoneInfoList": //Added Rakhi for R7:Add Employee Data Elements
                    itemValue.Parent = this;
                    (itemValue as AddressXPhoneInfo).EntityId = this.EntityId;
                    break;
                case "EntityXEntityIDTypeList":  //MITS:34276-- Added for Entity ID Type 
                    itemValue.Parent = this;
                    (itemValue as EntityXEntityIDType).EntityId = this.EntityId;
                    break;
            }
        }
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            //Sets all parent foreign keys in the case of a	new	Entity Record Save.
            switch (childName)
            {
                case "EntityXExposureList":
                    foreach (EntityXExposure itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                //avipinsrivas start : Worked for Jira-340
                case "EntityXRoleList":
                    if (isNew)
                        foreach (EntityXRole item in (childValue as EntityXRoleList))
                            item.EntityId = this.EntityId;
                    (childValue as IPersistence).Save();
                    break;
                //avipinsrivas End
                case "EntityXOperatingAsList":
                    foreach (EntityXOperatingAs itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "EntityXContactInfoList":
                    foreach (EntityXContactInfo itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "EntityXSelfInsuredList":
                    foreach (EntityXSelfInsured itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "ClientLimitsList":
                    foreach (ClientLimits itemValue in (childValue as DataCollection))
                        itemValue.ClientEid = this.EntityId;
                    break;
                case "JurisLicenseOrCodeList":
                    foreach (JurisLicenseOrCode itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "Instructions":
                    (childValue as EntInstructions).EntityId = this.EntityId;
                    break;
                case "CMMSEAXDataList":
                    foreach (CMMSEAXData itemValue in (childValue as DataCollection))
                        itemValue.EntityEID = this.EntityId;
                    break;
                case "EntityXAddressesList"://Added Rakhi for R7:Add Employee Data Elements
                    foreach (EntityXAddresses itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "AddressXPhoneInfoList"://Added Rakhi for R7:Add Employee Data Elements
                    foreach (AddressXPhoneInfo itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "EntityXWithholding": //smishra54: R8 Withholding Enhancement, MITS 26019
                    (childValue as EntityXWithholding).EntityId = this.EntityId;
                    break;
                case "EntityXEntityIDTypeList":  //MITS:34276-- Added for Entity ID Type 
                    foreach (EntityXEntityIDType itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);

            //Hack to do these activities only once	during the save.
            // Simply kicks	out	on all but the last	child object call.
            //Note that	if more	children are added,	this childName may need	to be changed.
            if (childName != "ClientLimitsList")
                return;

            //Proceed with 1 time per save activities...


            if (this.EntityTableId >= 1005 && this.EntityTableId <= 1012) //Is	ORGH Entity...
            {
                //Update Glossary for proper Legacy	Caching	Behavior
                Context.DbConn.ExecuteNonQuery(String.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE={0} WHERE SYSTEM_TABLE_NAME = 'ENTITY'", base.m_sDateTimeStamp), Context.DbTrans);

                //Update ORGH Table
                string sField = Common.Conversion.EntityTableIdToOrgTableName(this.EntityTableId);
                string sTable = sField;
                string SQL = "";
                if (sTable == "CLIENT")
                    sTable = "CLIENTENT";

                SQL = String.Format(@"DELETE FROM ORG_HIERARCHY	WHERE {0}_EID={1}", sField, this.EntityId);
                Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);

                SQL = String.Format(@"INSERT INTO ORG_HIERARCHY
															(DEPARTMENT_EID, FACILITY_EID,LOCATION_EID,	DIVISION_EID, REGION_EID,OPERATION_EID,COMPANY_EID,CLIENT_EID)
															SELECT DISTINCT	
															DEPARTMENT.ENTITY_ID DEPARTMENT_EID,
															FACILITY.ENTITY_ID FACILITY_EID,
															LOCATION.ENTITY_ID LOCATION_EID,
															DIVISION.ENTITY_ID DIVISION_EID,
															REGION.ENTITY_ID REGION_EID,
															OPERATION.ENTITY_ID	OPERATION_EID,
															COMPANY.ENTITY_ID COMPANY_EID, 
															CLIENTENT.ENTITY_ID	CLIENT_EID 
															FROM 
															ENTITY CLIENTENT,
															ENTITY COMPANY,
															ENTITY OPERATION,
															ENTITY REGION,
															ENTITY DIVISION,
															ENTITY LOCATION,
															ENTITY FACILITY,
															ENTITY DEPARTMENT
															WHERE
															{0}.ENTITY_ID =	{1}	AND
															(DEPARTMENT.ENTITY_TABLE_ID=1012) AND
															(DEPARTMENT.PARENT_EID = FACILITY.ENTITY_ID) AND
															(FACILITY.PARENT_EID = LOCATION.ENTITY_ID) AND
															(LOCATION.PARENT_EID = DIVISION.ENTITY_ID) AND
															(DIVISION.PARENT_EID =REGION.ENTITY_ID)	AND
															(REGION.PARENT_EID = OPERATION.ENTITY_ID) AND
															(OPERATION.PARENT_EID =	COMPANY.ENTITY_ID) AND
															(COMPANY.PARENT_EID	= CLIENTENT.ENTITY_ID)", sTable, this.EntityId);
                Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
            }
        }

        public EntityXExposureList EntityXExposureList
        {
            get
            {
                EntityXExposureList objList = base.m_Children["EntityXExposureList"] as EntityXExposureList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //avipinsrivas start : Worked for Jira-340
        public EntityXRoleList EntityXRoleList
        {
            get
            {
                EntityXRoleList objList = base.m_Children["EntityXRoleList"] as EntityXRoleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    //Payal RMA:10713 Starts
                    //  objList.SQLFilter = String.Format("{0}={1} AND {2} <> {3}", "ENTITY_ID", this.EntityId, "DELETED_FLAG", -1);
                    objList.SQLFilter = String.Format("{0}={1}", "ENTITY_ID", this.EntityId);
                //Payal RMA:10713 Ends
                return objList;
            }
        }
        //avipinsrivas End
        public EntityXOperatingAsList EntityXOperatingAsList
        {
            get
            {
                EntityXOperatingAsList objList = base.m_Children["EntityXOperatingAsList"] as EntityXOperatingAsList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public EntityXContactInfoList EntityXContactInfoList
        {
            get
            {
                EntityXContactInfoList objList = base.m_Children["EntityXContactInfoList"] as EntityXContactInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public EntityXSelfInsuredList EntityXSelfInsuredList
        {
            get
            {
                EntityXSelfInsuredList objList = base.m_Children["EntityXSelfInsuredList"] as EntityXSelfInsuredList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //Added for Froi Migration- Mits 33585,33586,33587
        public EntityXSelfInsuredList objSelfInsured
        {
            get
            {
                EntityXSelfInsuredList objList = base.m_Children["EntityXSelfInsuredList"] as EntityXSelfInsuredList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }

        public ClientLimitsList ClientLimitsList
        {
            get
            {
                ClientLimitsList objList = base.m_Children["ClientLimitsList"] as ClientLimitsList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = "CLIENT_EID" + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public CMMSEAXDataList CMMSEAXDataList
        {
            get
            {
                CMMSEAXDataList objList = base.m_Children["CMMSEAXDataList"] as CMMSEAXDataList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public JurisLicenseOrCodeList JurisLicenseOrCodeList
        {
            get
            {
                JurisLicenseOrCodeList objList = base.m_Children["JurisLicenseOrCodeList"] as JurisLicenseOrCodeList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }

        public EntInstructions Instructions
        {
            get
            {
                EntInstructions objItem = base.m_Children["Instructions"] as EntInstructions;

                //Raman 10/21/2009 MITS 17303: Moving to a child record should only happen for an existing record and NOT a new one

                //if((objItem as IDataModel).IsStale)
                //{
                //    objItem = this.CreatableMoveChildTo("Instructions",(objItem as DataObject),this.EntityId) as EntInstructions;
                //    objItem.EntityId = EntityId;
                //}
                if ((objItem as IDataModel).IsStale)
                {
                    if (!base.IsNew)
                    {
                        //Added for Mits 18601:Merged the code from R5 branch
                        try { objItem.MoveTo(this.EntityId); }
                        catch (RecordNotFoundException)
                        {// 12.27.2005 BSB Allow for creation of new record on the fly.
                            CallOnChildInit("Instructions", "EntInstructions");
                            objItem = base.m_Children["Instructions"] as EntInstructions;
                        }
                    }
                    else
                    {
                        objItem.Refresh();
                        objItem.EntityId = this.EntityId;
                    }
                }
                return objItem;
            }
        }
        //Added Rakhi for R7:Add Employee Data Elements
        public EntityXAddressesList EntityXAddressesList
        {
            get
            {
                EntityXAddressesList objList = base.m_Children["EntityXAddressesList"] as EntityXAddressesList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public AddressXPhoneInfoList AddressXPhoneInfoList
        {
            get
            {
                AddressXPhoneInfoList objList = base.m_Children["AddressXPhoneInfoList"] as AddressXPhoneInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //Added Rakhi for R7:Add Employee Data Elements

        //MITS:34276-- Entity ID Type start
        public EntityXEntityIDTypeList EntityXEntityIDTypeList
        {
            get
            {
                EntityXEntityIDTypeList objList = base.m_Children["EntityXEntityIDTypeList"] as EntityXEntityIDTypeList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //MITS:34276-- Entity ID Type end

        //smishra54: R8 Withholding Enhancement, MITS 26019
        public EntityXWithholding EntityXWithholding
        {
            get
            {
                EntityXWithholding objItem = base.m_Children["EntityXWithholding"] as EntityXWithholding;
                if ((objItem as IDataModel).IsStale)
                {
                    if (!base.IsNew)
                    {
                        (objItem as INavigation).Filter = this.KeyFieldName + "=" + this.EntityId;
                        objItem.MoveFirst();
                    }
                    else
                    {
                        objItem.Refresh();
                    }
                }
                return objItem;
            }
        }
        //smishra54: End

        public bool HideDeletedRecords { get { return m_HideDeletedRecords; } set { m_HideDeletedRecords = value; } }
        public bool LockEntityTableChange { set { m_LockEntityTableChange = value; } }
        internal bool LockMoveNavigation { set { m_LockMoveNavigation = value; } }

        public string GetLastFirstName() //Bkuzhanthaim : JIRA : RMA-1117 : Error is seen on status detail page
        {
            if (this.LastName == "")
                return this.FirstName;
            else if (this.FirstName == "")
                return this.LastName;
            else
                return this.LastName + ", " + this.FirstName;
        }



        /// <summary>
        /// Gets the last name of the first.Added for Froi Migration- Mits 33585,33586,33587

        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        public string GetLastFirstName(int entityId = 0)
        {
            string sFirst = "";
            string sLast = "";
            string sRet = "";
            //MITS 11338 : UMESH
            if (entityId == 0)
            {
                if (this.LastName == "")
                    return this.FirstName;
                else if (this.FirstName == "")
                    return this.LastName;
                else
                    return this.LastName + ", " + this.FirstName;
            }
            else
            {
                DbReader objReader = Context.DbConn.ExecuteReader(String.Format("SELECT	LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID={0} AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)", entityId));
                if (objReader.Read())
                {
                    sLast = objReader.GetString("LAST_NAME");
                    sFirst = objReader.GetString("FIRST_NAME");
                    if (sFirst != "" && sLast != "")
                        sRet = sLast + ", " + sFirst;
                    else
                        sRet = sLast + sFirst;
                }
                objReader.Close();
                return sRet;
            }
        }

        //TODO Remove this after debugging is done.	 Could be a	security issue.
        public override string ToString()
        {
            return (this as DataObject).Dump();
        }
        public string FormName
        {
            get
            {
                return m_sFormName;
            }
            set
            {
                m_sFormName = value;
            }
        }
        // mits35925 start

        public int AddressSequenceNumber
        {

            get
            {
                return iAddressSeqNum;
            }

            set
            {
                iAddressSeqNum = value;
            }

        }
        //mits35925 end
        public override void Save()
        {

            string sAddr1 = this.Addr1;
            string sAddr2 = this.Addr2;
            string sAddr3 = this.Addr3;
            string sAddr4 = this.Addr4;
            string sCity = this.City;
            int iCountryCode = this.CountryCode;
            int iStateId = this.StateId;
            string sEmailAddress = this.EmailAddress;
            string sFaxNumber = this.FaxNumber;
            string sCounty = this.County;
            string sZipCode = this.ZipCode;
            int iAddressSequenceNumber = this.AddressSequenceNumber;//AA//RMA-8753 nshah28(Added by ashish)
            string sOfficePhone = this.Phone1;
            string sHomePhone = this.Phone2;
            bool bOfficePhoneCodeExists = false;
            bool bHomePhoneCodeExists = false;
            int iOfficeCode = 0;
            int iHomeCode = 0;
            string sEffectiveDate = this.EffectiveDate;  //mbahl3 mits 30221
           string  sExpirationDate = this.ExpirationDate; //mbahl3 mits 30221
            //RMA-8753 nshah28(Added by ashish) Start
            string sStateDesc = string.Empty;
            string sStateCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sSearchString = string.Empty;
            //RMA-8753 nshah28(Added by ashish) END
           //  mits 35925 start
            //int iAddressSeqNum = this.AddressSequenceNumber;
            string  sClientSeqNum = this.ClientSequenceNumber;
            //mits 35925 end
            LocalCache objCache = null;

            try
            {
                if (FormName == string.Empty)
                {
                    objCache = new LocalCache(this.Context.DbConn.ConnectionString, this.Context.ClientId);
                    iOfficeCode = objCache.GetCodeId("O", "PHONES_CODES");
                    iHomeCode = objCache.GetCodeId("H", "PHONES_CODES");

                    #region Updating Entity_X_Addresses Table
                    if (
                                 sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty || sCity != string.Empty ||
                                 iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                 sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty || sEffectiveDate != string.Empty || sExpirationDate != string.Empty //mbahl3 mits 30221
                             )
                    {
                        if (this.EntityXAddressesList.Count > 0)
                        {
                            foreach (EntityXAddresses objEntityXAddressesInfo in this.EntityXAddressesList)
                            {
                                //  mits 35925 start
                                //Ashish Ahuja Address Master
                                if ((objEntityXAddressesInfo.PrimaryAddress == -1) )
                                {
                                    objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                                    objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                                    sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + iAddressSequenceNumber).ToLower().Replace(" ", ""); //Remove "County"
                                    objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                    if (this.Context.DbTrans == null)
                                    {
                                        objEntityXAddressesInfo.Address.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Context.LocalCache.DbConnectionString, this.Context.ClientId);
                                    }
                                    else
                                    {
                                        objEntityXAddressesInfo.Address.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Context.LocalCache.DbConnectionString, this.Context.ClientId,this.Context.DbTrans);
                                    }
                                    objEntityXAddressesInfo.AddressId = objEntityXAddressesInfo.Address.AddressId;
                                    objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                    objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                    objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                    objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                    objEntityXAddressesInfo.Address.City = sCity;
                                    objEntityXAddressesInfo.Address.Country = iCountryCode;
                                    objEntityXAddressesInfo.Address.State = iStateId;
                                    objEntityXAddressesInfo.Address.County = sCounty;
                                    objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                    objEntityXAddressesInfo.Email = sEmailAddress;
                                    objEntityXAddressesInfo.Fax = sFaxNumber;
                                    
                                   objEntityXAddressesInfo.EffectiveDate = sEffectiveDate; //mbahl3 mits 30221
                                   objEntityXAddressesInfo.ExpirationDate = sExpirationDate;//mbahl3 mits 30221
                                    objEntityXAddressesInfo.EntityId = this.EntityId;
                                    //objEntityXAddressesInfo.EntityId = 0;
                                    
                                    objEntityXAddressesInfo.PrimaryAddress = -1;
                                   //objEntityXAddressesInfo.AddressSeqNum = iAddressSeqNum;
                                   //objEntityXAddressesInfo.AddressId = 0;

                                   break;
                                    //mits 35925 end
                                }

                            }
                        }
                        else
                        {

                            {
                                objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                                objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                                sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + iAddressSequenceNumber).ToLower().Replace(" ", ""); //Remove "County"
                                EntityXAddresses objEntityXAddressesInfo = this.EntityXAddressesList.AddNew();
                                if (this.Context.DbTrans == null)
                                {
                                    objEntityXAddressesInfo.Address.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Context.LocalCache.DbConnectionString, this.Context.ClientId);
                                }
                                else
                                {
                                    objEntityXAddressesInfo.Address.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Context.LocalCache.DbConnectionString, this.Context.ClientId,this.Context.DbTrans);
                                }
                                objEntityXAddressesInfo.AddressId = objEntityXAddressesInfo.Address.AddressId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                               
                                objEntityXAddressesInfo.EntityId = this.EntityId;
                                objEntityXAddressesInfo.PrimaryAddress = -1;
                                objEntityXAddressesInfo.AddressId = -1;
                                objEntityXAddressesInfo.EffectiveDate = sEffectiveDate;//mbahl3 mits 30221
                                objEntityXAddressesInfo.ExpirationDate = sExpirationDate; //mbahl3 mits 30221
                                // mits 35925 start
                                //Ashish Ahuja - Address Master Enhancement
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                //objEntityXAddressesInfo.AddressSeqNum = iAddressSeqNum;
                                //mits 35925 end
                            }
                        }
                    }
                    else
                    {
                        if (this.EntityXAddressesList.Count > 0)
                        {
                            foreach (EntityXAddresses objEntityXAddressesInfo in this.EntityXAddressesList)
                            {
                                if (objEntityXAddressesInfo.PrimaryAddress == -1)
                                {
                                    this.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                    break;
                                }

                            }
                        }
                    }
                    #endregion
                    //RMA-8753 nshah28(Added by ashish) End
                    #region Updating Phone Numbers
                    if (this.AddressXPhoneInfoList.Count > 0)
                    {
                        foreach (AddressXPhoneInfo objAddressXPhoneInfo in this.AddressXPhoneInfoList)
                        {
                            if (objAddressXPhoneInfo.PhoneCode == iOfficeCode)
                            {
                                if (sOfficePhone.Trim() != string.Empty)
                                    objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                                else
                                    this.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                bOfficePhoneCodeExists = true;
                            }
                            if (objAddressXPhoneInfo.PhoneCode == iHomeCode)
                            {
                                if (sHomePhone.Trim() != string.Empty)
                                    objAddressXPhoneInfo.PhoneNo = sHomePhone;
                                else
                                    this.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                bHomePhoneCodeExists = true;
                            }

                        }
                        if (!bOfficePhoneCodeExists && sOfficePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iOfficeCode;
                            objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                        }
                        if (!bHomePhoneCodeExists && sHomePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iHomeCode;
                            objAddressXPhoneInfo.PhoneNo = sHomePhone;
                        }
                    }
                    else
                    {
                        if (sOfficePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iOfficeCode;
                            objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                        }
                        if (sHomePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iHomeCode;
                            objAddressXPhoneInfo.PhoneNo = sHomePhone;
                        }
                    }
                    #endregion
                }

                // atavaragiri: mits 29058 :checks whether the LSS interface is enabled //

                if (this.Context.InternalSettings.SysSettings.RMXLSSEnable == true)
                {
                    this.Context.OnPostTransCommit += new Riskmaster.DataModel.Context.PostTransCommitHandler(LSSAfterSave);
                    m_AfterSave = false;
                }
                // end :MITS 29058//
                
                base.Save();

                if (!this.IsMigrationImport)
                {
                    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                    if (this.ClaimId != 0)
                    {
                        int iAssignmentLvlCodeID = Context.LocalCache.GetCodeId("UN", "ASSIGNMENT_LEVEL");
                        string sAttachTable = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.OTHERUNIT.ToString();
                        int iAttachRecordID = this.EntityId;

                        CommonFunctions.UpdateAdjAssignment(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.ClaimId, iAssignmentLvlCodeID, sAttachTable, iAttachRecordID, this.Context.ClientId);

                        if (this.Context.InternalSettings.SysSettings.AdjAssignmentAutoDiary.Equals(-1))
                        {
                            string sEntryName = "Diary for Claim ";
                            string sClaimNumber = string.Empty;
                            int iAdjUserID = CommonFunctions.GetAdjusterUserID(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.Context.ClientId);
                            string sAssignedUser = string.Empty;

                        if (!int.Equals(iAdjUserID, 0))
                        {
                            //Getting Login Name for Adjuster******************************************
                            sAssignedUser = Context.LocalCache.GetSystemLoginName(iAdjUserID, Context.RMUser.DatabaseId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId),Context.ClientId);
                            //End for Login Name*******************************************************

                                if (!string.IsNullOrEmpty(sAssignedUser))
                                {
                                    Claim objClaim = this.Parent as Claim;
                                    if (objClaim == null && this.ClaimId != 0)
                                    {
                                        objClaim = (Claim)Context.Factory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(this.ClaimId);
                                    }
                                    if ((objClaim != null) && (this.ClaimId != 0))
                                    {
                                        sClaimNumber = objClaim.ClaimNumber;
                                    }

                                    sEntryName = string.Concat(sEntryName, sClaimNumber, ", New Other Unit Assignment, ", Context.LocalCache.GetEntityLastFirstName(this.EntityId));

                                    //Creation of Diary for Assigned Adjuster**********************************
                                    CommonFunctions.CreateWPADiary(Context.LocalCache.DbConnectionString, sEntryName, null, 2, -1, sAssignedUser, "SYSTEM", string.Empty, -1, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.ENTITYMAINT.ToString(), iAttachRecordID, null, Conversion.GetTime(DateTime.Now.ToString()), Conversion.ToDbDate(System.DateTime.Today), "0|0|0|0|0|0|0|0|0", this.Context.ClientId);
                                    //End Diary Creation*******************************************************
                                }
                            }
                        }
                    }
                    //Ankit End
                }
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }
        }

        public override void Delete()
        {
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            if (this.ClaimId != 0)
            {
                string sSql = string.Empty;
                sSql = String.Format("DELETE FROM CLAIM_ADJ_ASSIGNMENT WHERE ATTACH_RECORDID={0} AND ATTACH_TABLE='{1}'", this.EntityId, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.OTHERUNIT.ToString());
                Context.DbConnLookup.ExecuteNonQuery(sSql);
            }
            //Ankit End

            base.Delete();
        }
        //atavaragiri MITS 29058 //
        #region LSSAfterSave

        private void LSSAfterSave()
        {


            String sDTTM = String.Empty;
            String sEntityType = String.Empty;
            String sTableName = String.Empty;
            String sUser = String.Empty;
            String sLSSTable = String.Empty;
			//Rijul RMA 7914 start
            int iReturnVal = this.EntityTableId;
            int intEntityID = 0;
			//Rijul RMA 7914 end
            sDTTM = System.DateTime.Now.ToString("yyyyMMddHHmmss");

            ////Manoj - this will pick user who is being used for RMX LSS interface. Update done by this user won't trigger export process otherwise it will be a circular process between RMX and LSS
            sUser = this.Context.DbConnLookup.ExecuteString("SELECT RMX_LSS_USER FROM SYS_PARMS");


            if (sUser != this.Context.RMUser.LoginName)
            {
			//Rijul RMA 7914 start
                if (this.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    using (EntityXRoleList objEXRL = this.EntityXRoleList)
                    {
                        foreach (EntityXRole oEXR in objEXRL)
                        {
                            iReturnVal = oEXR.EntityTableId;
                            sEntityType = "";
                            sTableName = this.Context.LocalCache.GetTableName(iReturnVal);
                            switch (sTableName.ToUpper())
                            {
                                case "ADJUSTERS":
                                    sEntityType = "ADJUSTER";
                                    sLSSTable = "RM_LSS_ADMIN_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
                                    intEntityID = this.EntityId;
                                    break;
                                case "ATTORNEY_FIRMS":
                                    sEntityType = "ATTORNEY_FIRMS";
                                    sLSSTable = "RM_LSS_PAYEE_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
                                    //Rijul RMA 7914 start
                                    intEntityID = this.EntityId;
                                    break;
                                    //Rijul RMA 10998 start
                                case "ATTORNEYS":
                                    sEntityType = "ATTORNEY_FIRMS";
                                    sLSSTable = "RM_LSS_PAYEE_EXP";
                                    intEntityID = this.ParentEid;
                                    break;
                                    //Rijul RMA 10998 
                            }
                            //rkatyal4 : JIRA 15308 and 15309 start
                            if (!String.IsNullOrEmpty(sEntityType) && (intEntityID != 0))
                                this.Context.DbConnLookup.ExecuteString("INSERT INTO " + sLSSTable + " (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) VALUES							(" + intEntityID + ", '" + sDTTM + "', '" + sEntityType + "', 0)");
                            else if (intEntityID == 0)
                                return;
                            //rkatyal4 : JIRA 15308 and 15309 end
                            //Rijul RMA 7914 end
                        }
                    }
                }
                //Rijul RMA 7914 end
                else
                {
                    sTableName = this.Context.LocalCache.GetTableName(this.EntityTableId);

                    switch (sTableName.ToUpper())
                    {
                        case "ADJUSTERS":
                            sEntityType = "ADJUSTER";
                            sLSSTable = "RM_LSS_ADMIN_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
                            break;
                        case "ATTORNEY_FIRMS":
                            sEntityType = "ATTORNEY_FIRMS";
                            sLSSTable = "RM_LSS_PAYEE_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
                            break;
                    }

                    if (!String.IsNullOrEmpty(sEntityType))
                        this.Context.DbConnLookup.ExecuteString("INSERT INTO " + sLSSTable + " (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) VALUES							(" + this.EntityId + ", '" + sDTTM + "', '" + sEntityType + "', 0)");
                    //Rijul RMA 7914 end
                }
            }


        }
        //atavaragiri MITS 29058 //
        #endregion LSSAfterSave
        private EntityXExposure m_objEntityExposure = null;

        public EntityXExposure EntityExposure
        {
            get
            {
                if (object.ReferenceEquals(m_objEntityExposure, null))
                {
                    m_objEntityExposure = this.Context.Factory.GetDataModelObject("EntityXExposure", false) as EntityXExposure;
                    m_objEntityExposure.Parent = this;
                    m_objEntityExposure.EntityId = this.EntityId;
                    if (this.EntityId != default(int))
                    {
                        INavigation pINav = m_objEntityExposure;
                        pINav.Filter = string.Format("{0} = {1}", this.KeyFieldName, this.EntityId);
                        pINav.MoveFirst();
                    }
                }
                return m_objEntityExposure;
            }
        }
        //avipinsrivas start : Worked for Jira-340
        //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)        
        /// <summary>
        /// This Function will be used by EntityMaintForm.cs and PeopleForm.cs
        /// will get Space Seperated EntityTableIds for Given GlossaryTypeCode
        /// Unwanted TableIds will be removed from given sEntityTableIds
        /// </summary>
        /// <param name="sEntityTableIds">String of Space Separated Entity Table Ids</param>
        /// <param name="iGlossaryTypeCode">4 or 7</param>
        /// <param name="isBadGlossaryType">true if not needed; false if needed in return string</param>
        /// <returns>will Return Space Seperated EntityTableIds on behalf of given GlossaryTypeCode</returns>
        public string GetSpaceSepTableId(string sEntityTableIds, int iGlossaryTypeCode, bool isBadGlossaryType)
        {
            string sReturn = string.Empty;
            if (string.IsNullOrEmpty(sEntityTableIds) || iGlossaryTypeCode <= 0)
                return sReturn;

            string[] arrTableIds = sEntityTableIds.Split(' ');
            bool bSuccess;
            bool bAppendInReturnString = false;
            int iTableID = 0;

            foreach (string sTableId in arrTableIds)
            {
                if(!string.IsNullOrEmpty(sTableId))
                    iTableID = Conversion.CastToType<int>(sTableId, out bSuccess);

                if (iTableID > 0)
                {
                    if (int.Equals(iGlossaryTypeCode, 7))
                    {
                        bool bHiddenRoles;
                        switch (Context.LocalCache.GetTableName(iTableID).ToUpper())
                        {
                            case "EMPLOYEES":
                            case "PHYSICIANS":
                            case "MEDICAL_STAFF":
                            case "PATIENTS":
                            case "WITNESS":
                                bHiddenRoles = true;
                                break;
                            default:
                                bHiddenRoles = false;
                                break;
                        }
                        if (isBadGlossaryType)
                            bAppendInReturnString = (bHiddenRoles || !int.Equals(Context.LocalCache.GetGlossaryType(iTableID), iGlossaryTypeCode));
                        else
                            bAppendInReturnString = (!bHiddenRoles || int.Equals(Context.LocalCache.GetGlossaryType(iTableID), iGlossaryTypeCode));
                    }
                    else if (int.Equals(iGlossaryTypeCode, 4))
                    {
                        if (isBadGlossaryType)
                            bAppendInReturnString = !int.Equals(Context.LocalCache.GetGlossaryType(iTableID), iGlossaryTypeCode);
                        else
                            bAppendInReturnString = int.Equals(Context.LocalCache.GetGlossaryType(iTableID), iGlossaryTypeCode);
                    }

                    if (bAppendInReturnString)
                    {
                        if (string.IsNullOrEmpty(sReturn.Trim()))
                            sReturn = sTableId;
                        else
                            sReturn = string.Concat(sReturn, " ", sTableId);
                    }
                }
            }
            return sReturn;
        }
        //avipinsrivas End
        /// <summary>
        /// will get Space Seperated EntityTableIds from Given XML
        /// </summary>
        /// <param name="objXML">Object of base.SysEx</param>
        /// <param name="objEntity">Object of Entity DataModel</param>
        /// <param name="iGlossaryTypeCode">4 for EntityMaint and "7" for People</param>
        /// <returns>will Return Space Seperated EntityTableIds</returns>
        public string RenderEntityRoleList(XmlDocument objXML, Entity objEntity, int iGlossaryTypeCode)     //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
        {
            XmlElement objRoleListElement = null;
            string sReturnedCodeIds = string.Empty;
            string sDataToDisplay = string.Empty;
            bool bNeedToRender = true;        //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
            try
            {
                objRoleListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/EntityXRoleList");
                if (objRoleListElement != null)
                    objRoleListElement.ParentNode.RemoveChild(objRoleListElement);

                CommonFunctions.CreateElement(objXML.DocumentElement, "EntityXRoleList", ref objRoleListElement, this.Context.ClientId);

                foreach (EntityXRole objEntityXRole in objEntity.EntityXRoleList)
                {
                    if (string.IsNullOrEmpty(sReturnedCodeIds) && objEntityXRole.EntityTableId != 0)
                    {
                        sReturnedCodeIds = objEntityXRole.EntityTableId.ToString();
                    }
                    else if (objEntityXRole.EntityTableId != 0)
                    {
                        sReturnedCodeIds = string.Concat(sReturnedCodeIds + " " + objEntityXRole.EntityTableId.ToString());
                    }
                    //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
                    if (int.Equals(iGlossaryTypeCode, 7))
                    {
                        switch (Context.LocalCache.GetTableName(objEntityXRole.EntityTableId).ToUpper())
                        {
                            case "EMPLOYEES":
                            case "PHYSICIANS":
                            case "MEDICAL_STAFF":
                                //case "PATIENTS":      //avipinsrivas Start : Worked for JIRA - 9396 (Issue - 4634 / Epic - 340)
                                //case "WITNESS":       //avipinsrivas Start : Worked for JIRA - 9378 (Issue - 4634 / Epic - 340)
                                bNeedToRender = false;
                                break;
                            default:
                                bNeedToRender = true;
                                break;
                        }
                    }
                    //avipinsrivas End
                    if (int.Equals(objEntity.Context.LocalCache.GetGlossaryType(objEntityXRole.EntityTableId), iGlossaryTypeCode) && bNeedToRender)      //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
                        CommonFunctions.CreateAndSetElement(objRoleListElement, "Item", objEntity.Context.LocalCache.GetUserTableName(objEntityXRole.EntityTableId), objEntityXRole.EntityTableId, this.Context.ClientId);
                }
                objRoleListElement.SetAttribute("codeid", sReturnedCodeIds);
            }
            finally
            {
                objRoleListElement = null;
            }
            return sReturnedCodeIds;
        }
        /// <summary>
        /// To get Entity Table ID column on behalf of Use Entity Role Setting
        /// </summary>
        /// <param name="iErRowID">ER_ROW_ID of Entity_X_Role Table</param>
        /// <returns>will return Entity_Table_ID Value based on Use Entity Role Setting</returns>
        public int GetEntityTableID(int iErRowID)
        {
            int iReturnVal = this.EntityTableId;
            if (this.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                using (EntityXRoleList objEXRL = this.EntityXRoleList)
                {
                    foreach (EntityXRole oEXR in objEXRL)
                    {
                        if (int.Equals(oEXR.ERRowId, iErRowID))
                        {
                            iReturnVal = oEXR.EntityTableId;
                            break;
                        }
                    }
                }
            }
            return iReturnVal;
        }
        /// <summary>
        /// will check if Entity Role exist int EntityXRole Table of selected Entity
        /// </summary>
        /// <param name="objEXRL">Object of EntityXRoleList</param>
        /// <param name="intTableId">Selected EntityTableID</param>
        /// <returns>will return true if Entity Role exist int EntityXRole Table of selected Entity</returns>
        public int IsEntityRoleExists(EntityXRoleList objEXRL, int intTableId)
        {
            int intErRowID = -1;
            foreach (EntityXRole oEXR in objEXRL)
            {
                if (int.Equals(oEXR.EntityTableId, intTableId))
                {
                    intErRowID = oEXR.ERRowId;
                    break;
                }
            }
            return intErRowID;
        }
        /// <summary>
        /// Will update Entity_X_Roles table's records.
        /// </summary>
        /// <param name="childValue">IDataModel Object</param>
        /// <param name="intTableID">Selected Entity_Table_ID Column value</param>
        /// <param name="objReturnVal">Will return EntityID and EntityRoleRowID</param>
        /// <returns></returns>
        public void UpdateEntityRoles(IDataModel childValue, int intTableID, Dictionary<string, int> objReturnVal = null)
        {
            int intErRowID = 0;
            Entity objEntity = ((childValue as Entity));
            if (Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (!string.IsNullOrEmpty(objEntity.LastName))
                {
                    int iGlossaryTypeCode = objEntity.Context.LocalCache.GetGlossaryType(objEntity.EntityTableId);
                    if (objEntity.EntityTableId < 0 || int.Equals(iGlossaryTypeCode, 4) || int.Equals(iGlossaryTypeCode, 7))
                        objEntity.EntityTableId = 0;
                    //if (int.Equals(objEntity.EntityApprovalStatusCode, 0))
                    //    objEntity.EntityApprovalStatusCode = objEntity.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ"); 
                    intErRowID = objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, intTableID);
                    if (int.Equals(intErRowID, -1) || (int.Equals(objEntity.EntityId, 0) && !string.IsNullOrEmpty(objEntity.LastName)))
                    {
                        //avipinsrivas Start : Worked for 13968 (Issue of 13196 - Epic 7767)
                        if (objEntity.NameType <= 0)
                            objEntity.NameType = objEntity.Context.LocalCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
                        //avipinsrivas end
                        UpdateEntityRoles(childValue, intTableID.ToString(), true, out intErRowID);
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(objEntity.LastName))
                {
                    if (objEntity != null && int.Equals(objEntity.EntityTableId, 0) && intTableID > 0)
                        objEntity.EntityTableId = intTableID;
                    (childValue as IPersistence).Save();
                }
            }
            if (objReturnVal != null)
            {
                objReturnVal.Add(Globalization.ConstReturnValues.EntityID.ToString(), objEntity.EntityId);
                objReturnVal.Add(Globalization.ConstReturnValues.EntityRoleRowID.ToString(), intErRowID);
            }
        }
        /// <summary>
        /// Will update Entity_X_Roles table's records.
        /// </summary>
        /// <param name="childValue">IDataModel Object</param>
        /// <param name="strEntityTableIdList">List of Entity_Table_ID Columns</param>
        /// <param name="blnIsNew">Determines -- Existing Entity or New Entity</param>
        /// <param name="intErRowId">Last Updated ER_Row_ID of ENTITY_X_ROLES Table</param>
        /// <returns></returns>
        public void UpdateEntityRoles(IDataModel childValue, string strEntityTableIdList, bool blnIsNew, out int intErRowId)
        {
            string[] array = null;
            bool bSusscess;
            intErRowId = 0;
            Entity oEntity = (childValue as Entity);

            if(!string.IsNullOrEmpty(strEntityTableIdList))
                array = strEntityTableIdList.Split(" ".ToCharArray()[0]);

            if (Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (array != null)
                {
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        bool bAdd = true;
                        foreach (EntityXRole OEntityXRole in oEntity.EntityXRoleList)
                        {
                            if (Conversion.CastToType<int>(array[count], out bSusscess) == OEntityXRole.EntityTableId)
                            {
                                bAdd = false;
                                break;
                            }
                        }
                        if (bAdd)
                        {
                            EntityXRole objEntityXRole = oEntity.EntityXRoleList.AddNew();
                            objEntityXRole.EntityTableId = Conversion.CastToType<int>(array[count], out bSusscess);
                        }
                    }
                }
                foreach (EntityXRole OEntityXRole in oEntity.EntityXRoleList)
                {
                    bool bDelete = true;
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        if (Conversion.CastToType<int>(array[count], out bSusscess) == OEntityXRole.EntityTableId)
                        {
                            bDelete = false;
                            break;
                        }
                    }
                    if (bDelete)
                    {
                        if (!blnIsNew)
                            oEntity.EntityXRoleList.Remove(OEntityXRole.ERRowId);
                    }
                    else
                    {
                        if (blnIsNew)
                        {
                            (this.Children["EntityXRoleList"] as IDataModel).IsStale = false;
                            (childValue as IPersistence).Save();
                        }
                        intErRowId = (OEntityXRole.ERRowId < 0 ? 0 : OEntityXRole.ERRowId);
                    }
                }
            }
        }
        //avipinsrivas End
    }
}
