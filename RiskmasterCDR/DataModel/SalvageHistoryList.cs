﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public class SalvageHistoryList : DataCollection
    {

        internal SalvageHistoryList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "SALVAGE_HIST_ROW_ID";
            this.SQLFromTable = "SALVAGEHISTORY";
				
            this.TypeName = "SalvageHistory";
		}
        public new SalvageHistory this[int keyValue] { get { return base[keyValue] as SalvageHistory; } }
        public new SalvageHistory AddNew() { return base.AddNew() as SalvageHistory; }
        public SalvageHistory Add(SalvageHistory obj) { return base.Add(obj) as SalvageHistory; }
        public new SalvageHistory Add(int keyValue) { return base.Add(keyValue) as SalvageHistory; }

		
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}

        public SalvageHistory LastStatusChange()
        {
            string SQL = "SELECT MAX(SALVAGE_HIST_ROW_ID) FROM SALVAGEHISTORY ";
            if (this.SQLFilter == "")
                this.SQLFilter = " 1=0 ";
            SQL += "WHERE " + this.SQLFilter;
            int lastChangeId = (int)Context.DbConnLookup.ExecuteInt(SQL);
            if (lastChangeId == 0 && this.Count != 0)
                foreach (object iKey in this.m_keySet.Keys)
                    lastChangeId = Math.Min((int)iKey, lastChangeId);
            return this[lastChangeId];
        }
    }
}
