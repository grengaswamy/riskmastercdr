
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for PolicyXBillEnhList.
	/// </summary>
	public class PolicyXBillEnhList : DataCollection
	{
		internal PolicyXBillEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POLICY_BILLING_ID";
			this.SQLFromTable =	"POLICY_X_BILL_ENH";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXBillEnh";
		}
		public new PolicyXBillEnh this[int keyValue]{get{return base[keyValue] as PolicyXBillEnh;}}
		public new PolicyXBillEnh AddNew(){return base.AddNew() as PolicyXBillEnh;}
		public  PolicyXBillEnh Add(PolicyXBillEnh obj){return base.Add(obj) as PolicyXBillEnh;}
		public new PolicyXBillEnh Add(int keyValue){return base.Add(keyValue) as PolicyXBillEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}