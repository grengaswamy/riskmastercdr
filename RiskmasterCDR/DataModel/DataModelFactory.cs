﻿using System;
using Riskmaster.Security;
using Riskmaster.Common;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Specialized;
using System.IO;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Factory class centrally controls creation of DataModel objects.
    /// Takes responsibility for ensuring that all common initialization information
    /// has been accepted and populates the Context property of each
    /// object during creation.
    /// </summary>
    public sealed class DataModelFactory : IDisposable
    {
        //BSB 12.4.2006 Debugging for Code that never uninitializes DM
        #region Debugging Logic
        static private NameValueCollection m_ContextSettings = null;
        static private TextWriter m_stm = null;
        static private string m_sTracePath;
        static private int m_TraceDetail;
        static private int g_TotalInstanceCount = 0;
        static private int g_ClosedInstanceCount = 0;
        private bool m_disposed;
        private const string CodeAccessPublicKey = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
        private Context m_context = null;
        private int m_InstanceId = 0;

        #region Debug methods
        static internal void Dbg(string s, DMTrace traceDetailLevel)
        {
            if (IsDbgLevel((int)traceDetailLevel))
            {
                m_stm.WriteLine(s);
                m_stm.Flush();
                System.Diagnostics.Trace.WriteLine(s);
            }
        }
        static internal bool IsDbgLevel(int traceDetailLevel)
        {
            return ((traceDetailLevel & m_TraceDetail) != 0);
        }


        private string DumpCallingStack(DMTrace TraceDetail)
        {
            string sTmpCallerInfo = "n/a";
            if (DataModelFactory.IsDbgLevel((int)TraceDetail))
            {
                StackTrace objTrace = new StackTrace(true);
                StackFrame objFrame = objTrace.GetFrame(1);
                sTmpCallerInfo = objFrame.GetMethod().ReflectedType.Name + objTrace.ToString();
            }
            return sTmpCallerInfo;
        } 
        #endregion

        #endregion

        #region Public properties
        /// <summary>
        /// Gets the context object associated with the DataModelFactory
        /// </summary>
        public Context Context
        {
            [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CodeAccessPublicKey)]
            get { return m_context; }
        }
        #endregion

        

        #region Class constructors
         //mbahl3
        /// <summary>
        /// Constructor for scripting class
        /// </summary>
        public DataModelFactory(string dataSourceName, string username, string password)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                int p_iClientId = 0;
                m_InstanceId = DataModelFactory.g_TotalInstanceCount++;
                if (!Initialize(dataSourceName, username, password, p_iClientId))
                    throw (new Riskmaster.ExceptionTypes.InitializationException(Globalization.GetString("DataModelFactory.Ctor.InitializationFailed", p_iClientId)));//dvatsa-cloud
            }
            else
                throw new Exception("DataModelFactory" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        
        }
        
        /// <summary>
        /// Creates an instance of the DataModelFactory object
        /// </summary>
        /// <param name="dataSourceName">string containing the name of the selected User Data Source</param>
        /// <param name="username">string containing the user name</param>
        /// <param name="password">string containing the user password</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        /// <example>DataModelFactory dmf = new DataModelFactory("Training", "rmxuser", "rmxuserpwd",10);</example>
        public DataModelFactory(string dataSourceName, string username, string password, int p_iClientId)
        {
            
            m_InstanceId = DataModelFactory.g_TotalInstanceCount++;
            if (!Initialize(dataSourceName, username, password, p_iClientId))
                throw (new Riskmaster.ExceptionTypes.InitializationException(Globalization.GetString("DataModelFactory.Ctor.InitializationFailed",p_iClientId)));//dvatsa-cloud
        }//constructor


        //mbahl3
        /// <summary>
        /// Constructor for scripting class
        /// </summary>
        public DataModelFactory(UserLogin objUserLogin)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                int p_iClientId = 0;
                m_InstanceId = DataModelFactory.g_TotalInstanceCount++;
                if (!Initialize(objUserLogin, p_iClientId))
                    throw (new Riskmaster.ExceptionTypes.InitializationException(Globalization.GetString("DataModelFactory.Ctor.InitializationFailed", p_iClientId)));//dvatsa-cloud
            }
            else
                throw new Exception("DataModelFactory" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only

        }
        //mbahl3 constructor for custom scripts

        /// <summary>
        /// Summary of Riskmaster.DataModel.DataModelFactory.DataModelFactory.  This ctor 
        /// takes a pre-populated UserLogin object.  This accomplishes two things:
        /// 1.) Avoids moving pwds around the system in plain-text.
        /// 2.) Performance improvement becuase duplication of Security Database 
        /// authentication can be eliminated.</summary>
        /// <param name="objUserLogin">Existing UserLogin object representing a logged in user.</param  
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        /// <example>
        /// UserLogin objUserLogin = new UserLogin(username, databaseName);
        /// DataModelFactory dmf = new DataModelFactory(objuserLogin,10);
        /// </example>
        public DataModelFactory(UserLogin objUserLogin, int p_iClientId)
        {
            m_InstanceId = DataModelFactory.g_TotalInstanceCount++;
            if (!Initialize(objUserLogin, p_iClientId))
                throw (new Riskmaster.ExceptionTypes.InitializationException(Globalization.GetString("DataModelFactory.Ctor.InitializationFailed",p_iClientId)));//dvatsa-cloud
        }

        /// <summary>
        /// Internal default constructor
        /// </summary>
        /// <param name="strDbConnString">string containing the database connection string</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        internal DataModelFactory(string strDbConnString, int p_iClientId)
        {
            //Initialize instance values
            m_InstanceId = DataModelFactory.g_TotalInstanceCount++;
            m_context = new Context(strDbConnString, this, p_iClientId);
        } // method: DataModelFactory 

      

        static DataModelFactory()
        {

            if (m_TraceDetail > 0)
                m_stm = System.IO.TextWriter.Synchronized(new System.IO.StreamWriter(m_sTracePath, false));
        }

        
        #endregion

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for CreateDataModelFactory
        /// </summary>     
        public static DataModelFactory CreateDataModelFactory(string strDbConnString)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return CreateDataModelFactory(strDbConnString, 0);
            }
            else
                throw new Exception("CreateDataModelFactory" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
           // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Uses Abstract Factory pattern/Factory Method pattern
        /// to return an instance of the DataModelFactory class
        /// by instantiating through default internal constructor
        /// rather than overloaded constructor
        /// </summary>
        /// <param name="strDbConnString">string containing the database connection string</param>
        /// <returns>instance of the DataModelFactory class</returns>
        /// <remarks>Used by tools and utilities which do not need
        /// to perform authentication prior to instantiating DataModelFactory</remarks>
        public static DataModelFactory CreateDataModelFactory(string strDbConnString,int p_iClientId)
        {
            //Initialize DataModelFactory with just a database connection string
            DataModelFactory dmf = new DataModelFactory(strDbConnString, p_iClientId);

            return dmf;
        } // method: CreateDataModelFatory



        /// <summary>
        /// TODO: Summary of Riskmaster.DataModel.DataModelFactory.Initialize.</summary>
        /// <param name="databaseName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        /// <returns>Not specified.</returns>
        /// <remarks>none</remarks>
        private bool Initialize(string databaseName, string username, string password, int p_iClientid)
        {
            UserLogin objUserLogin = null;
            DataModelFactory.Dbg("Instance:" + this.m_InstanceId + " Action:OPENING " + "Inclusive Total Open COUNT:" + (DataModelFactory.g_TotalInstanceCount - DataModelFactory.g_ClosedInstanceCount) + " CALLER:" + DumpCallingStack(DMTrace.InitializerStack), DMTrace.Connect);

            if (!IsInitialized)
            {
                objUserLogin = new UserLogin(username, databaseName, p_iClientid);

                m_context = new Context(objUserLogin, this, p_iClientid);

                return true;

            }
            return false;
        }//method: Initialize

        #region Custom instantiation logic (takes: userLogin object)
        /// <summary>
        /// TODO: Summary of Riskmaster.DataModel.DataModelFactory.Initialize.</summary>
        /// <param name="objUserLogin">Existing UserLogin object representing a logged in user.</param>
        /// <returns>boolean for success\failure</returns>
        /// <remarks>used by the Ctor with matching signature to avoid duplicating the Authentication logic.</remarks>
        private bool Initialize(UserLogin objUserLogin, int p_iClientId)
        {
            RiskmasterDatabase objRMDB = null;
            DataModelFactory.Dbg("Instance:" + this.m_InstanceId + " Action:OPENING " + "Inclusive Total Open COUNT:" + (DataModelFactory.g_TotalInstanceCount - DataModelFactory.g_ClosedInstanceCount) + " CALLER:" + DumpCallingStack(DMTrace.InitializerStack), DMTrace.Connect);

            if (!IsInitialized)
            {
                if (objUserLogin != null) // User Authenticated Successfully 
                // somewhere previously in the system...
                {
                    //					objRMDB = new RiskmasterDatabase(objUserLogin.DatabaseId);
                    objRMDB = objUserLogin.objRiskmasterDatabase;
                    m_context = new Context(objUserLogin, this, p_iClientId);
                    return true;
                }
            }
            return false;
        }

        #endregion
        /// <summary>
        /// TODO: Summary of Riskmaster.DataModel.DataModelFactory.UnInitialize.</summary>
        /// <returns>Not specified.</returns>
        /// <remarks>none</remarks>
        /// <example>No example available.</example>
        public bool UnInitialize()
        {
            DataModelFactory.g_ClosedInstanceCount++;
            DataModelFactory.Dbg("Instance:" + this.m_InstanceId + " Action:CLOSING " + "Remaining Total Open COUNT:" + (DataModelFactory.g_TotalInstanceCount - DataModelFactory.g_ClosedInstanceCount) + " CALLER:" + DumpCallingStack(DMTrace.UnInitializerStack), DMTrace.Disconnect);
            if (IsInitialized)
            {
                m_context.Dispose();
                m_context = null;
            } return true;
        }




        /// <summary>
        /// TODO: Summary of Riskmaster.DataModel.DataModelFactory.GetDataModelObject.</summary>
        /// <returns>Requested DataModelObject.</returns>
        /// <remarks>DataModelObject is the base class for all data classes in the object Model.</remarks>
        /// <example>No example available.</example>
        public DataRoot GetDataModelObject(string objectType, bool isLocked)
        {
            return this.GetDataModelObject(objectType, isLocked, null);
        }



        //BSB 10.10.2006 Allow for a PreInitHandler to fill in some object properties just before the Init Script
        // (if any) is fired.
        public DataRoot GetDataModelObject(string objectType, bool isLocked, PreInitHandler preInit)
        {
            try
            {
                if (preInit != null)
                {
                    m_context.PreInitOwner = objectType;
                    m_context.PreInit += preInit;
                }
                object[] constructorParams = { isLocked, m_context };
                BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
                Type t = Type.GetType("Riskmaster.DataModel." + objectType, true);
                //System.Runtime.Remoting.ObjectHandle hObj =  System.Activator.CreateInstance(t,bindingFlags,null,constructorParams,null,null);
                return (DataRoot)System.Activator.CreateInstance(t, bindingFlags, null, constructorParams, null, null);
            }
            catch (Exception e)
            { throw new Riskmaster.ExceptionTypes.RMAppException(Globalization.GetString("DataModelFactory.GetDataModel.Exception", this.Context.ClientId), e); }

        }


        /// <summary>
        /// TODO: Summary of Riskmaster.DataModel.DataModelFactory.IsInitialized.</summary>
        /// <returns>Not specified.</returns>
        /// <value>TODO: Value of Riskmaster.DataModel.DataModelFactory.IsInitialized.</value>
        /// <remarks>none</remarks>
        /// <example>No example available.</example>
        public bool IsInitialized { get { return m_context != null; } }
        #region IDisposable Members
        public void Dispose() { Dispose(true); GC.SuppressFinalize(this); }
        public void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                UnInitialize();
                m_disposed = true;
            }
        }

        ~DataModelFactory()
        {
            Dispose(false);
        }
        #endregion
    }//class: DataModelFactory
}
