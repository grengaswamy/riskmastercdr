﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for PolicyXUnitList.
	/// </summary>
	public class PolicyXUnitList : DataCollection
	{
		internal PolicyXUnitList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "POLICY_UNIT_ROW_ID";
            this.SQLFromTable = "POLICY_X_UNIT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXUnit";
		}
        public new PolicyXUnit this[int keyValue] { get { return base[keyValue] as PolicyXUnit; } }
        public new PolicyXUnit AddNew() { return base.AddNew() as PolicyXUnit; }
        public PolicyXUnit Add(UnitXClaim obj) { return base.Add(obj) as PolicyXUnit; }
        public new PolicyXUnit Add(int keyValue) { return base.Add(keyValue) as PolicyXUnit; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}

