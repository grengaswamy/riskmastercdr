using System;
using System.Text;
namespace Riskmaster.DataModel
{
    /// <summary>
    /// Author - Ayush Arya
    /// Date - 12//18/2009
    /// MITS# - 18230
    /// Description - Property Claim Schedule Info tab.
    /// </summary>
	public class PolicyXPschedEnhList : DataCollection
	{
        internal PolicyXPschedEnhList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PSCHED_ROW_ID";
            this.SQLFromTable = "POLICY_X_PSCHED_ENH";
            //Must be set by parent at runtime for current record.
			//			this.SQLFilter =				"???" 
            this.TypeName = "PolicyXPschedEnh";
		}
        public new PolicyXPschedEnh this[int keyValue] { get { return base[keyValue] as PolicyXPschedEnh; } }
        public new PolicyXPschedEnh AddNew() { return base.AddNew() as PolicyXPschedEnh; }
        public PolicyXPschedEnh Add(PolicyXPschedEnh obj) { return base.Add(obj) as PolicyXPschedEnh; }
        public new PolicyXPschedEnh Add(int keyValue) { return base.Add(keyValue) as PolicyXPschedEnh; }

		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
            StringBuilder s = new StringBuilder();
            foreach (DataObject obj in this)
            {
                s.Append(obj.Dump());
            }
            return s.ToString();
		}
	}
}