﻿using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ENTITY_STGNG", "ENTITY_ID")]
    public class EntityStgng:DataObject
    {
        public override string Default
        {
            get
            {
                if (this.FirstName != "")
                    return this.GetLastFirstName();
                else
                    return this.LastName;
            }
        }

        //Book-Keeping for Locking Modes
        protected bool m_LockEntityTableChange = false;
        protected bool m_LockMoveNavigation = false;
        protected bool m_HideDeletedRecords = true;
        private string m_sFormName = string.Empty; //rjhamb 22497
        private bool m_AfterSave = true; // atavaragiri: mits 29058//

        		#region	Database Field List
        private string[,] sFields =	{

                                                        { "BusinessTypeCode", "BUSINESS_TYPE_CODE"},
                                                        { "County",	"COUNTY"},
                                                        { "NatureOfBusiness", "NATURE_OF_BUSINESS"}, 
                                                        { "SicCode", "SIC_CODE"},
                                                        { "SicCodeDesc", "SIC_CODE_DESC"}, 
                                                        { "WcFillingNumber", "WC_FILING_NUMBER"}, 
                                                        { "EntityId", "ENTITY_ID"},	
                                                        { "EntityTableId", "ENTITY_TABLE_ID"}, 
                                                        //Anu Tennyson Pre and POST WWIG Starts
                                                        { "EntityTableName", "ENTITY_TABLE_NAME"}, 
                                                        //Anu Tennyson End
                                                        { "LastName", "LAST_NAME"},	                                                        
                                                        { "FirstName", "FIRST_NAME"}, 
                                                        { "AlsoKnownAs", "ALSO_KNOWN_AS"},
                                                        { "Abbreviation", "ABBREVIATION"}, 
                                                        { "CostCenterCode",	"COST_CENTER_CODE"}, 
                                                        { "Addr1", "ADDR1"}, 
                                                        { "Addr2", "ADDR2"}, 
                                                        { "Addr3", "ADDR3"}, 
                                                        { "Addr4", "ADDR4"}, 
                                                        { "City", "CITY"}, 
                                                        { "CountryCode", "COUNTRY_CODE"}, 
                                                        { "StateId", "STATE_ID"}, 
                                                        { "ZipCode", "ZIP_CODE"}, 
                                                        { "ParentEid", "PARENT_EID"},
                                                        { "TaxId", "TAX_ID"},
                                                        { "Contact", "CONTACT"}, 
                                                        { "Comments", "COMMENTS"}, 
                                                        { "EmailTypeCode", "EMAIL_TYPE_CODE"}, 
                                                        { "EmailAddress", "EMAIL_ADDRESS"},	
                                                        { "SexCode", "SEX_CODE"}, 
                                                        { "BirthDate", "BIRTH_DATE"}, 
                                                        { "Phone1",	"PHONE1"}, 
                                                        { "Phone2",	"PHONE2"}, 
                                                        { "FaxNumber", "FAX_NUMBER"}, 
                                                        {"DttmRcdAdded","DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        {"UpdatedByUser","UPDATED_BY_USER"},
                                                        {"AddedByUser","ADDED_BY_USER"},
                                                        {"SecDeptEid","SEC_DEPT_EID"},
                                                        { "EffStartDate", "EFF_START_DATE"}, 
                                                        { "EffEndDate",	"EFF_END_DATE"},
                                                        { "Parent1099EID", "PARENT_1099_EID"},
                                                        { "Entity1099Reportable", "REPORT_1099_FLAG"},
                                                        { "MiddleName",	"MIDDLE_NAME"},	
                                                        { "Title", "TITLE"},
                                                        { "NaicsCode", "NAICS_CODE"},
                                                        { "FreezePayments",	"FREEZE_PAYMENTS"},
                                                        {"OrganizationType","ORGANIZATION_TYPE"},
                                                        {"NPINumber","NPI_NUMBER"},
                                                        {"TimeZoneCode","TIME_ZONE_CODE"},    
                                                        {"Prefix", "PREFIX"},
                                                        {"SuffixCommon", "SUFFIX_COMMON"},
                                                        {"SuffixLegal", "SUFFIX_LEGAL"}, 
                                                        {"LegalName","LEGAL_NAME"},
                                                        {"ReferenceNumber", "REFERENCE_NUMBER"}
                                    };

        [ExtendedTypeAttribute(RMExtType.Code, "BUSINESS_TYPE")]
        public string BusinessTypeCode { get { return GetFieldString("BUSINESS_TYPE_CODE"); } set { SetField("BUSINESS_TYPE_CODE", value); } }
        public string County { get { return GetFieldString("COUNTY"); } set { SetField("COUNTY", value); } }
        public string NatureOfBusiness { get { return GetFieldString("NATURE_OF_BUSINESS"); } set { SetField("NATURE_OF_BUSINESS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SIC_CODE")]
        public string SicCode { get { return GetFieldString("SIC_CODE"); } set { SetField("SIC_CODE", value); } }
        public string SicCodeDesc { get { return GetFieldString("SIC_CODE_DESC"); } set { SetField("SIC_CODE_DESC", value); } }
        public string WcFillingNumber { get { return GetFieldString("WC_FILING_NUMBER"); } set { SetField("WC_FILING_NUMBER", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int EntityTableId
        {
            get { return GetFieldInt("ENTITY_TABLE_ID"); }
            set
            {

                if (m_LockEntityTableChange && this.EntityTableId != value)
                    throw new DataModelException(Globalization.GetString("Entity.EntityTableId.get.Exception.EntityTableIsLocked", this.Context.ClientId));
                SetField("ENTITY_TABLE_ID", value);
            }
        }
        //Anu Tennyson POST and PRE WWIG Starts
        public string EntityTableName
        {
            get { return GetFieldString("ENTITY_TABLE_NAME"); }
            set
            {

                if (m_LockEntityTableChange && this.EntityTableName != value)
                    throw new DataModelException(Globalization.GetString("Entity.EntityTableId.get.Exception.EntityTableIsLocked", this.Context.ClientId));
                SetField("ENTITY_TABLE_NAME", value);
            }
        }
        //Anu Tennyson Ends
        public string LastName
        {
            get { return GetFieldString("LAST_NAME"); }
            set
            {
                SetField("LAST_NAME", value);               
            }
        }        
        public string FirstName { get { return GetFieldString("FIRST_NAME"); } set { SetField("FIRST_NAME", value); } }
        public string AlsoKnownAs { get { return GetFieldString("ALSO_KNOWN_AS"); } set { SetField("ALSO_KNOWN_AS", value); } }
        public string Abbreviation { get { return GetFieldString("ABBREVIATION"); } set { SetField("ABBREVIATION", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COST_CENTER")]
        public string CostCenterCode { get { return GetFieldString("COST_CENTER_CODE"); } set { SetField("COST_CENTER_CODE", value); } }
        public string Addr1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Addr2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public string CountryCode { get { return GetFieldString("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public string StateId { get { return GetFieldString("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int ParentEid { get { return GetFieldInt("PARENT_EID"); } set { SetField("PARENT_EID", value); } }
        //public string TaxId{get{ return	GetFieldStringForTaxId("TAX_ID");}set{SetField("TAX_ID",value);}}
        //Deb MITS 25775 
        //mbahl3 mits:29316
        public string TaxId { get { return GetFieldStringForTaxId("TAX_ID", "ID_TYPE"); } set { SetFieldForTaxId("TAX_ID", value, "ID_TYPE"); } }
        //mbahl3 mits:29316
        public string Contact { get { return GetFieldString("CONTACT"); } set { SetField("CONTACT", value); } }
        public string Comments { get { return GetFieldString("COMMENTS"); } set { SetField("COMMENTS", value); } }
        public string EmailTypeCode { get { return GetFieldString("EMAIL_TYPE_CODE"); } set { SetField("EMAIL_TYPE_CODE", value); } }
        public string EmailAddress { get { return GetFieldString("EMAIL_ADDRESS"); } set { SetField("EMAIL_ADDRESS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SEX_CODE")]
        public string SexCode { get { return GetFieldString("SEX_CODE"); } set { SetField("SEX_CODE", value); } }
        public string BirthDate { get { return GetFieldString("BIRTH_DATE"); } set { SetField("BIRTH_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string Phone1 { get { return GetFieldString("PHONE1"); } set { SetField("PHONE1", value); } }
        public string Phone2 { get { return GetFieldString("PHONE2"); } set { SetField("PHONE2", value); } }
        public string FaxNumber { get { return GetFieldString("FAX_NUMBER"); } set { SetField("FAX_NUMBER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public int SecDeptEid { get { return GetFieldInt("SEC_DEPT_EID"); } set { SetField("SEC_DEPT_EID", value); } }
        public string EffStartDate { get { return GetFieldString("EFF_START_DATE"); } set { SetField("EFF_START_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string EffEndDate { get { return GetFieldString("EFF_END_DATE"); } set { SetField("EFF_END_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int Parent1099EID { get { return GetFieldInt("PARENT_1099_EID"); } set { SetField("PARENT_1099_EID", value); } }
        public bool Entity1099Reportable { get { return GetFieldBool("REPORT_1099_FLAG"); } set { SetField("REPORT_1099_FLAG", value); } }
        public string MiddleName { get { return GetFieldString("MIDDLE_NAME"); } set { SetField("MIDDLE_NAME", value); } }
        public string Title { get { return GetFieldString("TITLE"); } set { SetField("TITLE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "NAICS_CODES")]
        public string NaicsCode { get { return GetFieldString("NAICS_CODE"); } set { SetField("NAICS_CODE", value); } }
        public bool FreezePayments { get { return GetFieldBool("FREEZE_PAYMENTS"); } set { SetField("FREEZE_PAYMENTS", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ORGANIZATION_TYPE")]
        public string OrganizationType { get { return GetFieldString("ORGANIZATION_TYPE"); } set { SetField("ORGANIZATION_TYPE", value); } }
        public string NPINumber { get { return GetFieldString("NPI_NUMBER"); } set { SetField("NPI_NUMBER", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "TIME_ZONE_CODES")]
        public string TimeZoneCode { get { return GetFieldString("TIME_ZONE_CODE"); } set { SetField("TIME_ZONE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_PREFIX")]
        public int Prefix { get { return GetFieldInt("PREFIX"); } set { SetField("PREFIX", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_SUFFIX")]
        public string SuffixCommon { get { return GetFieldString("SUFFIX_COMMON"); } set { SetField("SUFFIX_COMMON", value); } }
        public string SuffixLegal { get { return GetFieldString("SUFFIX_LEGAL"); } set { SetField("SUFFIX_LEGAL", value); } }
        public string LegalName { get { return GetFieldString("LEGAL_NAME"); } set { SetField("LEGAL_NAME", value); } }
        public string ReferenceNumber { get { return GetFieldString("REFERENCE_NUMBER"); } set { SetField("REFERENCE_NUMBER", value); } } 
        int iClaimID = 0;
        public int ClaimId
        {
            get
            {
                return iClaimID;
            }
            set
            {
                iClaimID = value;
            }
        }
                #endregion

        #region	Child List
		//String array of the form {{ChildName1,ChildType1}{ChildName2,ChildType2}...}
		private	string[,] sChildren	= {{"EntityXExposureList","EntityXExposureList"},
                {"EntityXOperatingAsList","EntityXOperatingAsList"},
                {"EntityXContactInfoList","EntityXContactInfoList"},
                {"EntityXSelfInsuredList","EntityXSelfInsuredList"},
                {"ClientLimitsList","ClientLimitsList"},
                {"JurisLicenseOrCodeList","JurisLicenseOrCodeList"},
                {"Instructions","EntInstructions"},
                {"CMMSEAXDataList","CMMSEAXDataList"},
                {"EntityXAddressesList","EntityXAddressesList"}, //Added Rakhi for R7:Add Employee Data Elements
                {"AddressXPhoneInfoList","AddressXPhoneInfoList"}, //Added Rakhi for R7:Add Employee Data Elements
                {"EntityXWithholding","EntityXWithholding"} //smishra54: R8 Withholding Enhancement, MITS 26019
            }; 
		#endregion
		
        //#region	Supplemental Fields	Exposed
        ////Supplementals	are	implemented	in the base	class but not exposed externally.
        ////This allows the derived class	to control whether the object appears to have
        ////supplementals	or not.	
        ////Entity exposes Supplementals.
        //public new Supplementals Supplementals
        //{	
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        //#endregion


		internal EntityStgng(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new	private	void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so	that scripting can be called successfully.

            this.m_sTableName = "ENTITY_STGNG";
			this.m_sKeyField = "ENTITY_ID";
			this.m_sFilterClause = "";

			//Add all obect	Fields into	the	Field Collection from our Field	List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children	collection from	our	"sChildren"	list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();	//Moved	after most init	logic so that scripting	can	be called successfully.

		}


		protected override void	Clear()
		{
			int	prevEntityTableId =	this.EntityTableId;
			base.Clear();
			if(this.m_LockEntityTableChange)
				this.m_Fields["ENTITY_TABLE_ID"] = prevEntityTableId;
			
			this.DataChanged = false;
		}

		protected override bool	OnForceParentSave()
		{
			return false;
		}

		internal override string OnApplyFilterClauseSQL(string CurrentSQL)
		{
			string sSQL	= CurrentSQL;
			string sExtendedFilterClause = m_sFilterClause;
			
			if(m_LockEntityTableChange)
			{
				if(sExtendedFilterClause !="")
					sExtendedFilterClause += " AND ";
				sExtendedFilterClause += "ENTITY_TABLE_ID="	+ this.EntityTableId;
			}
			

			if(sExtendedFilterClause !=	"")	
				if(sSQL.IndexOf(" WHERE ") > 0)
					sSQL +=	" AND (" + sExtendedFilterClause + ")";
				else
					sSQL +=" WHERE " + sExtendedFilterClause;
			
			return sSQL;
		}

		// Cancel Deletion of Supplementals during "delete" of this object.
		internal override bool OnSuppDelete()
		{
			return false;
		}

		// Cancel Deletion of all Entity children.
		// (Obviously will these children would be needed if the entity is ever 
		// "undeleted" by changing the entity deleted flag.
		internal override void OnChildDelete(string childName, IDataModel childValue)
		{
			return;
		}
		// This	is where for collection	items, the foreign key values on new items 
		// can be set by the parent	at the time	of collection membership.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			switch(childName)
			{
				case "EntityXExposureList":
					itemValue.Parent = this;
					(itemValue as EntityXExposure).EntityId= this.EntityId;
					break;
				case "EntityXOperatingAsList":
					itemValue.Parent = this;
					(itemValue as EntityXOperatingAs).EntityId=	this.EntityId;
					break;
				case "EntityXContactInfoList":
					itemValue.Parent = this;
					(itemValue as EntityXContactInfo).EntityId=	this.EntityId;
					break;
				case "EntityXSelfInsuredList":
					itemValue.Parent = this;
					(itemValue as EntityXSelfInsured).EntityId=	this.EntityId;
					break;
				case "ClientLimitsList":
					itemValue.Parent = this;
					(itemValue as ClientLimits).ClientEid= this.EntityId;
					break;
				case "JurisLicenseOrCodeList":
					itemValue.Parent = this;
					(itemValue as JurisLicenseOrCode).EntityId= this.EntityId;
					break;
                case "CMMSEAXDataList":
                    itemValue.Parent = this;
                    (itemValue as CMMSEAXData).EntityEID = this.EntityId;
                    break;
                case "EntityXAddressesList": //Added Rakhi for R7:Add Employee Data Elements
                    itemValue.Parent = this;
                    (itemValue as EntityXAddresses).EntityId = this.EntityId;
                    break;
                case "AddressXPhoneInfoList": //Added Rakhi for R7:Add Employee Data Elements
                    itemValue.Parent = this;
                    (itemValue as AddressXPhoneInfo).EntityId = this.EntityId;
                    break;
			}
		}
		override internal void OnChildPostSave(string childName, IDataModel	childValue,	bool isNew)
		{
			//Sets all parent foreign keys in the case of a	new	Entity Record Save.
			switch(childName)
			{
				case "EntityXExposureList":
					foreach(EntityXExposure	itemValue in (childValue as	DataCollection))
						itemValue.EntityId=	this.EntityId;
					break;
				case "EntityXOperatingAsList":
					foreach(EntityXOperatingAs itemValue in	(childValue	as DataCollection))
						itemValue.EntityId=	this.EntityId;
					break;
				case "EntityXContactInfoList":
					foreach(EntityXContactInfo itemValue in	(childValue	as DataCollection))
						itemValue.EntityId=	this.EntityId;
					break;
				case "EntityXSelfInsuredList":
					foreach(EntityXSelfInsured itemValue in	(childValue	as DataCollection))
						itemValue.EntityId=	this.EntityId;
					break;
				case "ClientLimitsList":
					foreach(ClientLimits itemValue in (childValue as DataCollection))
						itemValue.ClientEid= this.EntityId;
					break;
				case "JurisLicenseOrCodeList":
					foreach(JurisLicenseOrCode itemValue in (childValue as DataCollection))
						itemValue.EntityId=	this.EntityId;
					break;
				case "Instructions":
					(childValue as	EntInstructions).EntityId =	this.EntityId;
					break;
                case "CMMSEAXDataList":
                    foreach (CMMSEAXData itemValue in (childValue as DataCollection))
						itemValue.EntityEID = this.EntityId;
					break;
                case "EntityXAddressesList"://Added Rakhi for R7:Add Employee Data Elements
                    foreach (EntityXAddresses itemValue in (childValue as DataCollection))
                        itemValue.EntityId= this.EntityId;
                    break;
                case "AddressXPhoneInfoList"://Added Rakhi for R7:Add Employee Data Elements
                    foreach (AddressXPhoneInfo itemValue in (childValue as DataCollection))
                        itemValue.EntityId = this.EntityId;
                    break;
                case "EntityXWithholding": //smishra54: R8 Withholding Enhancement, MITS 26019
                    (childValue as EntityXWithholding).EntityId = this.EntityId;
                    break;
			}
			base.OnChildPostSave(childName,childValue, isNew);
			
			//Hack to do these activities only once	during the save.
			// Simply kicks	out	on all but the last	child object call.
			//Note that	if more	children are added,	this childName may need	to be changed.
			if(childName !=	"ClientLimitsList")
				return;

			//Proceed with 1 time per save activities...
			
			
			if(this.EntityTableId >=1005 &&	this.EntityTableId <=1012) //Is	ORGH Entity...
			{
				//Update Glossary for proper Legacy	Caching	Behavior
                Context.DbConn.ExecuteNonQuery(String.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE={0} WHERE SYSTEM_TABLE_NAME = 'ENTITY_STGNG'", base.m_sDateTimeStamp), Context.DbTrans);
			
				//Update ORGH Table
				string sField =	Common.Conversion.EntityTableIdToOrgTableName(this.EntityTableId);
				string sTable =	sField;
				string SQL = "";
				if(sTable == "CLIENT")
					sTable = "CLIENTENT";
				
				SQL	= String.Format(@"DELETE FROM ORG_HIERARCHY	WHERE {0}_EID={1}",sField,this.EntityId);
				Context.DbConn.ExecuteNonQuery(SQL,	Context.DbTrans);

				SQL=  String.Format(@"INSERT INTO ORG_HIERARCHY
															(DEPARTMENT_EID, FACILITY_EID,LOCATION_EID,	DIVISION_EID, REGION_EID,OPERATION_EID,COMPANY_EID,CLIENT_EID)
															SELECT DISTINCT	
															DEPARTMENT.ENTITY_ID DEPARTMENT_EID,
															FACILITY.ENTITY_ID FACILITY_EID,
															LOCATION.ENTITY_ID LOCATION_EID,
															DIVISION.ENTITY_ID DIVISION_EID,
															REGION.ENTITY_ID REGION_EID,
															OPERATION.ENTITY_ID	OPERATION_EID,
															COMPANY.ENTITY_ID COMPANY_EID, 
															CLIENTENT.ENTITY_ID	CLIENT_EID 
															FROM 
															ENTITY CLIENTENT,
															ENTITY COMPANY,
															ENTITY OPERATION,
															ENTITY REGION,
															ENTITY DIVISION,
															ENTITY LOCATION,
															ENTITY FACILITY,
															ENTITY DEPARTMENT
															WHERE
															{0}.ENTITY_ID =	{1}	AND
															(DEPARTMENT.ENTITY_TABLE_ID=1012) AND
															(DEPARTMENT.PARENT_EID = FACILITY.ENTITY_ID) AND
															(FACILITY.PARENT_EID = LOCATION.ENTITY_ID) AND
															(LOCATION.PARENT_EID = DIVISION.ENTITY_ID) AND
															(DIVISION.PARENT_EID =REGION.ENTITY_ID)	AND
															(REGION.PARENT_EID = OPERATION.ENTITY_ID) AND
															(OPERATION.PARENT_EID =	COMPANY.ENTITY_ID) AND
															(COMPANY.PARENT_EID	= CLIENTENT.ENTITY_ID)",sTable,this.EntityId);
				Context.DbConn.ExecuteNonQuery(SQL,Context.DbTrans);
			}
		}

		public EntityXExposureList EntityXExposureList
		{
			get
			{
				EntityXExposureList	objList	= base.m_Children["EntityXExposureList"] as	EntityXExposureList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}		
		public EntityXOperatingAsList EntityXOperatingAsList
		{
			get
			{
				EntityXOperatingAsList objList = base.m_Children["EntityXOperatingAsList"] as EntityXOperatingAsList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}		
		public EntityXContactInfoList EntityXContactInfoList
		{
			get
			{
				EntityXContactInfoList objList = base.m_Children["EntityXContactInfoList"] as EntityXContactInfoList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}		
		public EntityXSelfInsuredList EntityXSelfInsuredList
		{
			get
			{
				EntityXSelfInsuredList objList = base.m_Children["EntityXSelfInsuredList"] as EntityXSelfInsuredList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}	
		public ClientLimitsList	ClientLimitsList
		{
			get
			{
				ClientLimitsList objList = base.m_Children["ClientLimitsList"] as ClientLimitsList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	"CLIENT_EID"+ "=" +	this.KeyFieldValue;
				return objList;
			}
		}	
        public CMMSEAXDataList CMMSEAXDataList
        {
            get
            {
                CMMSEAXDataList objList = base.m_Children["CMMSEAXDataList"] as CMMSEAXDataList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }	
		public JurisLicenseOrCodeList	JurisLicenseOrCodeList
		{
			get
			{
				JurisLicenseOrCodeList objList = base.m_Children["JurisLicenseOrCodeList"] as JurisLicenseOrCodeList;
				if (!base.IsNew	&& (objList	as IDataModel).IsStale)
					objList.SQLFilter =	this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}	

		public EntInstructions Instructions
		{
			get
			{
                EntInstructions objItem = base.m_Children["Instructions"] as EntInstructions;

                //Raman 10/21/2009 MITS 17303: Moving to a child record should only happen for an existing record and NOT a new one
                
                //if((objItem as IDataModel).IsStale)
                //{
                //    objItem = this.CreatableMoveChildTo("Instructions",(objItem as DataObject),this.EntityId) as EntInstructions;
                //    objItem.EntityId = EntityId;
                //}
                if ((objItem as IDataModel).IsStale)
                {
                    if (!base.IsNew)
                    {
                        //Added for Mits 18601:Merged the code from R5 branch
                        try { objItem.MoveTo(this.EntityId); }
                        catch (RecordNotFoundException)
                        {// 12.27.2005 BSB Allow for creation of new record on the fly.
                            CallOnChildInit("Instructions", "EntInstructions");
                            objItem = base.m_Children["Instructions"] as EntInstructions;
                        }
                    }
                    else
                    {
                        objItem.Refresh();
                        objItem.EntityId = this.EntityId;
                    }
                }
				return objItem;
			}
		}			
        //Added Rakhi for R7:Add Employee Data Elements
        public EntityXAddressesList EntityXAddressesList
        {
            get
            {
                EntityXAddressesList objList = base.m_Children["EntityXAddressesList"] as EntityXAddressesList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public AddressXPhoneInfoList AddressXPhoneInfoList
        {
            get
            {
                AddressXPhoneInfoList objList = base.m_Children["AddressXPhoneInfoList"] as AddressXPhoneInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //Added Rakhi for R7:Add Employee Data Elements

        //smishra54: R8 Withholding Enhancement, MITS 26019
        public EntityXWithholding EntityXWithholding
        { 
            get
            {
                EntityXWithholding objItem = base.m_Children["EntityXWithholding"] as EntityXWithholding;
                if ((objItem as IDataModel).IsStale)
                {
                    if (!base.IsNew)
                    {
                        (objItem as INavigation).Filter = this.KeyFieldName + "=" + this.EntityId;
                        objItem.MoveFirst();
                    }
                    else
                    {
                        objItem.Refresh();
                    }
                }
                return objItem;
            }
        }
        //smishra54: End

		public bool	HideDeletedRecords{get{return m_HideDeletedRecords;}set{m_HideDeletedRecords = value;}}
		public bool	LockEntityTableChange{set{m_LockEntityTableChange =	value;}}
		internal bool LockMoveNavigation{set{m_LockMoveNavigation =	value;}}

		public string GetLastFirstName()
		{
			if(this.LastName =="")
				return this.FirstName;
			else if( this.FirstName	=="")
				return this.LastName;
			else
				return this.LastName + ", "	+ this.FirstName;
		}

		public string GetLastFirstName(int entityId)
		{
			string sFirst="";
			string sLast="";
			string sRet	 = "";
            //MITS 11338 : UMESH
            DbReader objReader = Context.DbConn.ExecuteReader(String.Format("SELECT	LAST_NAME,FIRST_NAME FROM ENTITY_STGNG WHERE ENTITY_ID={0} AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)", entityId));
			if(objReader.Read())
			{
				sLast =	objReader.GetString("LAST_NAME");
				sFirst = objReader.GetString("FIRST_NAME");
				if(sFirst !="" && sLast	!= "")
					sRet =	sLast +	", " + sFirst;
				else
					sRet =	sLast +	sFirst;
			}
			objReader.Close();
			return sRet;
		}

		//TODO Remove this after debugging is done.	 Could be a	security issue.
		public override	string ToString()
		{
			return (this as	DataObject).Dump();
		}
        public string FormName
        {
            get
            {
                return m_sFormName;
            }
            set
            {
                m_sFormName = value;
            }
        }
        public override void Save()
        {

            string sAddr1 = this.Addr1;
            string sAddr2 = this.Addr2;
            string sAddr3 = this.Addr3;
            string sAddr4 = this.Addr4;
            string sCity = this.City;
            string sCountryCode = this.CountryCode;
            string sStateId = this.StateId;
            string sEmailAddress = this.EmailAddress;
            string sFaxNumber = this.FaxNumber;
            string sCounty = this.County;
            string sZipCode = this.ZipCode;
            string sOfficePhone = this.Phone1;
            string sHomePhone = this.Phone2;
            bool bOfficePhoneCodeExists = false;
            bool bHomePhoneCodeExists = false;
            int iOfficeCode=0;
            int iHomeCode=0;
           // string sEffectiveDate = this.EffectiveDate;  //mbahl3 mits 30221
           //string  sExpirationDate = this.ExpirationDate; //mbahl3 mits 30221
            LocalCache objCache=null;
			
            try
            {
                if (FormName == string.Empty)
                {
                    objCache = new LocalCache(this.Context.DbConn.ConnectionString,this.Context.ClientId);
                    iOfficeCode = objCache.GetCodeId("O", "PHONES_CODES");
                    iHomeCode = objCache.GetCodeId("H", "PHONES_CODES");

                    #region Updating Entity_X_Addresses Table
                    if (
                                 sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty || sCity != string.Empty ||
                                 sCountryCode != string.Empty || sStateId != string.Empty  || sEmailAddress != string.Empty ||
                                 sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty 
                             )
                    {
                        if (this.EntityXAddressesList.Count > 0)
                        {
                            foreach (EntityXAddresses objEntityXAddressesInfo in this.EntityXAddressesList)
                            {
                                if (objEntityXAddressesInfo.PrimaryAddress == -1)
                                {
                                    objEntityXAddressesInfo.Addr1 = sAddr1;
                                    objEntityXAddressesInfo.Addr2 = sAddr2;
                                    objEntityXAddressesInfo.Addr3 = sAddr3;
                                    objEntityXAddressesInfo.Addr4 = sAddr4;
                                    objEntityXAddressesInfo.City = sCity;
                                    objEntityXAddressesInfo.Country = Context.LocalCache.GetCodeId(sCountryCode, "COUNTRY");
                                    objEntityXAddressesInfo.State = Context.LocalCache.GetStateRowID(sStateId);
                                    objEntityXAddressesInfo.Email = sEmailAddress;
                                    objEntityXAddressesInfo.Fax = sFaxNumber;
                                    objEntityXAddressesInfo.County = sCounty;
                                    objEntityXAddressesInfo.ZipCode = sZipCode;
                                   //objEntityXAddressesInfo.EffectiveDate = sEffectiveDate; //mbahl3 mits 30221
                                   //objEntityXAddressesInfo.ExpirationDate = sExpirationDate;//mbahl3 mits 30221

                                    break;
                                }

                            }
                        }
                        else
                        {

                            {
                                EntityXAddresses objEntityXAddressesInfo = this.EntityXAddressesList.AddNew();
                                objEntityXAddressesInfo.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Addr4 = sAddr4;
                                objEntityXAddressesInfo.City = sCity;
                                objEntityXAddressesInfo.Country = Context.LocalCache.GetCodeId(sCountryCode, "COUNTRY");
                                objEntityXAddressesInfo.State = Context.LocalCache.GetStateRowID(sStateId);
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                objEntityXAddressesInfo.County = sCounty;
                                objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.EntityId = this.EntityId;
                                objEntityXAddressesInfo.PrimaryAddress = -1;
                                objEntityXAddressesInfo.AddressId = -1;
                                //objEntityXAddressesInfo.EffectiveDate = sEffectiveDate;//mbahl3 mits 30221
                                //objEntityXAddressesInfo.ExpirationDate = sExpirationDate; //mbahl3 mits 30221
                            }
                        }
                    }
                    else
                    {
                        if (this.EntityXAddressesList.Count > 0)
                        {
                            foreach (EntityXAddresses objEntityXAddressesInfo in this.EntityXAddressesList)
                            {
                                if (objEntityXAddressesInfo.PrimaryAddress == -1)
                                {
                                    this.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                    break;
                                }

                            }
                        }
                    }
                    #endregion
                    #region Updating Phone Numbers
                    if (this.AddressXPhoneInfoList.Count > 0)
                    {
                        foreach (AddressXPhoneInfo objAddressXPhoneInfo in this.AddressXPhoneInfoList)
                        {
                            if (objAddressXPhoneInfo.PhoneCode == iOfficeCode)
                            {
                                if (sOfficePhone.Trim() != string.Empty)
                                    objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                                else
                                    this.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                bOfficePhoneCodeExists = true;
                            }
                            if (objAddressXPhoneInfo.PhoneCode == iHomeCode)
                            {
                                if (sHomePhone.Trim() != string.Empty)
                                    objAddressXPhoneInfo.PhoneNo = sHomePhone;
                                else
                                    this.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                bHomePhoneCodeExists = true;
                            }

                        }
                        if (!bOfficePhoneCodeExists && sOfficePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iOfficeCode;
                            objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                        }
                        if (!bHomePhoneCodeExists && sHomePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iHomeCode;
                            objAddressXPhoneInfo.PhoneNo = sHomePhone;
                        }
                    }
                    else
                    {
                        if (sOfficePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iOfficeCode;
                            objAddressXPhoneInfo.PhoneNo = sOfficePhone;
                        }
                        if (sHomePhone.Trim() != string.Empty)
                        {
                            AddressXPhoneInfo objAddressXPhoneInfo = this.AddressXPhoneInfoList.AddNew();
                            objAddressXPhoneInfo.PhoneId = -1;
                            objAddressXPhoneInfo.PhoneCode = iHomeCode;
                            objAddressXPhoneInfo.PhoneNo = sHomePhone;
                        }
                    }
                    #endregion
                }

				// atavaragiri: mits 29058 :checks whether the LSS interface is enabled //
				
				if (this.Context.InternalSettings.SysSettings.RMXLSSEnable == true)
				{
					this.Context.OnPostTransCommit += new Riskmaster.DataModel.Context.PostTransCommitHandler(LSSAfterSave);
					m_AfterSave = false;
				}
          			
				// end :MITS 29058//

                base.Save();
               
            }
            finally
            {
                if(objCache!=null)
                    objCache.Dispose();
            }
        }

        public override void Delete()
        {
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            if (this.ClaimId != 0)
            {
                string sSql = string.Empty;
                sSql = String.Format("DELETE FROM CLAIM_ADJ_ASSIGNMENT WHERE ATTACH_RECORDID={0} AND ATTACH_TABLE='{1}'", this.EntityId, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.OTHERUNIT.ToString());
                Context.DbConnLookup.ExecuteNonQuery(sSql);
            }
            //Ankit End

            base.Delete();
        }
		//atavaragiri MITS 29058 //
		#region LSSAfterSave  	
	
		private void LSSAfterSave()
		{

			
		    String sDTTM = String.Empty;
		    String sEntityType = String.Empty;
		    String sTableName = String.Empty;
		    String sUser = String.Empty;
		    String sLSSTable = String.Empty;
			

		    sDTTM = System.DateTime.Now.ToString("yyyyMMddHHmmss");

		    ////Manoj - this will pick user who is being used for RMX LSS interface. Update done by this user won't trigger export process otherwise it will be a circular process between RMX and LSS
		    sUser = this.Context.DbConnLookup.ExecuteString("SELECT RMX_LSS_USER FROM SYS_PARMS");

					
		        if (sUser != this.Context.RMUser.LoginName)
		        {
		            sEntityType = "";
		            sTableName = this.Context.LocalCache.GetTableName(this.EntityTableId);

		            switch (sTableName.ToUpper())
		            {
		                case "ADJUSTERS":
		                    sEntityType = "ADJUSTER";
		                    sLSSTable = "RM_LSS_ADMIN_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
		                    break;
						case "ATTORNEY_FIRMS":
							sEntityType = "ATTORNEY_FIRMS";
		                    sLSSTable = "RM_LSS_PAYEE_EXP"; //Debabrata 02/10/2010 MITS# 19730  Commented this Line for Adjusters pushed as PAYEE
		                    break;
		            }

		           if(!String.IsNullOrEmpty(sEntityType))
		                this.Context.DbConnLookup.ExecuteString("INSERT INTO " + sLSSTable + " (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) VALUES							(" + this.EntityId + ", '" + sDTTM + "', '" + sEntityType + "', 0)");
					
		        }
							 

		}
		//atavaragiri MITS 29058 //
		#endregion LSSAfterSave  	
    }
}
