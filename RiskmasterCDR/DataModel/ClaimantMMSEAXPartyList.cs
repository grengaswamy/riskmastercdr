﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class ClaimantMMSEAXPartyList : DataCollection
    {

        internal ClaimantMMSEAXPartyList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "MMSEA_CLMT_ROW_ID";
            this.SQLFromTable = "CLAIMANT_X_MMS_CLT";
            this.TypeName = "ClaimantMMSEAXParty";
		}

        public new ClaimantMMSEAXParty this[int keyValue] { get { return base[keyValue] as ClaimantMMSEAXParty; } }
        public new ClaimantMMSEAXParty AddNew() { return base.AddNew() as ClaimantMMSEAXParty; }
        public ClaimantMMSEAXParty Add(ClaimantMMSEAXParty obj) { return base.Add(obj) as ClaimantMMSEAXParty; }
        public new ClaimantMMSEAXParty Add(int keyValue) { return base.Add(keyValue) as ClaimantMMSEAXParty; }
		




    }
}
