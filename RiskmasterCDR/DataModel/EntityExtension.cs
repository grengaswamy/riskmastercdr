﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Claim Extension class extends claim class to make claim compatible with PDS forms.
    /// </summary>
    public class EntityExtension : Entity
    {
        internal EntityExtension(bool isLocked, Context context)
            : base(isLocked, context)
        {

        }

        /// <summary>
        /// Gets the pi x work loss.
        /// </summary>
        /// <value>
        /// The pi x work loss.
        /// </value>
        public EntityXExposure EntityExposure
        {
            get
            {
                EntityXExposure objEntityXExposure = EntityXExposureList.Cast<EntityXExposure>().FirstOrDefault();
                int ExposureID = 0;
                if (this.EntityId != 0)
                {
                    string sSQL = string.Format("SELECT EXPOSURE_ROW_ID FROM ENTITY_EXPOSURE WHERE ENTITY_ID = {0}", this.EntityId);
                    ExposureID = Context.DbConnLookup.ExecuteInt(sSQL);

                    if (object.ReferenceEquals(objEntityXExposure, default(EntityXExposure)))
                    {
                        objEntityXExposure = this.Context.Factory.GetDataModelObject("EntityXExposure", false) as EntityXExposure;
                        objEntityXExposure.Parent = this;
                        objEntityXExposure.ExposureRowId = this.EntityExposure.ExposureRowId;

                        if (this.EntityExposure.ExposureRowId!=0)
                        {
                            INavigation pINav = objEntityXExposure;
                        
                        }
                        objEntityXExposure.MoveTo(ExposureID);
                    }
                }
                return objEntityXExposure;
            }
        }

        private PiXRestrict objRestrict = null;

        /// <summary>
        /// Gets the pi x restrict.
        /// </summary>
        /// <value>
        /// The pi x restrict.
        /// </value>
        public PiXRestrict PiXRestrict
        {
            get
            {
                if (objRestrict == null)
                {
                    objRestrict = this.Context.Factory.GetDataModelObject("PiXRestrict", false) as PiXRestrict;
                    objRestrict.Parent = this;
                    objRestrict.PiRowId = this.PiXRestrict.PiRowId;
                    if (this.PiXRestrict.PiRowId != 0)
                    {
                        INavigation pINav = objRestrict;
                        pINav.Filter = string.Format("{0} = {1}", this.KeyFieldName, this.PiXRestrict.PiRowId);
                        pINav.MoveFirst();
                    }
                }
                return objRestrict;
            }
        }

    }
}
