using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClaimantList.
	/// </summary>
	public class ClaimantList : DataCollection
	{
		internal ClaimantList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CLAIMANT_ROW_ID";
			this.SQLFromTable =	"CLAIMANT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Claimant";
		}
		public new Claimant this[int keyValue]{
            //Deb MITS 31370
            get
            {
                Claimant objclaimant = base[keyValue] as Claimant;
                if (objclaimant == null)
                {
                    foreach (Claimant obj in this)
                    {
                        if (obj.ClaimantEid == keyValue)
                        {
                            objclaimant = base[obj.ClaimantRowId] as Claimant;
                        }
                    }
                    return objclaimant;
                }
                else
                {
                    return objclaimant;
                }
            }
            //Deb MITS 31370
        }
		public new Claimant AddNew(){return base.AddNew() as Claimant;}
		public  Claimant Add(Claimant obj){return base.Add(obj) as Claimant;}
		public new Claimant Add(int keyValue){return base.Add(keyValue) as Claimant;}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}