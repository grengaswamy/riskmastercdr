﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("DED_BILL_INVOICE_DETAILS", "DED_BILL_INVOICE_ID")]
    class DedBillInvoiceDetails : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"DedBillInvoiceId", "DED_BILL_INVOICE_ID"},
                                        {"TransId", "TRANS_ID"},
                                        {"PolcvgRowId", "POLCVG_ROW_ID"},
                                        {"AdminActivitydate", "ADMIN_ACTIVITY_DATE"},
                                        {"AdminCompanyName", "ADMIN_COMPANY_NAME"},
                                        {"MasterCompanyName", "MASTER_COMPANY_NAME"},
                                        {"PolicyCompanyName", "POLICY_COMPANY_NAME"},
                                        {"PolicyModule", "POLICY_MODULE"},
                                        {"PolicyNumber", "POLICY_NUMBER"},
                                        {"PolicySymbol", "POLICY_SYMBOL"},
                                        {"TransEffDate", "TRANS_EFF_DATE"},
                                        {"ClaimCurrAdjName", "CLAIM_CURR_ADJ_NAME"},
                                        {"ClaimNumber", "CLAIM_NUMBER"},
                                        {"ClaimantName", "CLAIMANT_NAME"},
                                        {"EventLossDate", "EVENT_LOSS_DATE"},
                                        {"OperatorName", "OPERATOR_NAME"},
                                        {"PolicyDeductibleAmt", "POLICY_DEDUCTIBLE_AMT"},
                                        {"TotalPaidAmt", "TOTAL_PAID_AMT"},
                                        {"DeductibleDueAmt", "DEDUCTIBLE_DUE_AMT"},
                                        {"PaidDate", "PAID_DATE"},
                                        {"PayeeName", "PAYEE_NAME"},
                                        {"PaymentAmount", "PAYMENT_AMOUNT"},
                                        {"PaymentTypeCode", "PAYMENT_TYPE_CODE"},
                                        {"ReferenceCode", "REFRENCE_CODE"},
                                        {"VoidFlag", "VOID_FLAG"},
                                        {"StatusFlag", "STATUS_FLAG"},
                                        {"StatusDate", "STATUS_DATE"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastupd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"}
                                    };

        public int DedBillInvoiceId { get { return GetFieldInt("DED_BILL_INVOICE_ID"); } set { SetField("DED_BILL_INVOICE_ID", value); } }
        public int TransId { get { return GetFieldInt("TRANS_ID"); } set { SetField("TRANS_ID", value); } }
        public int PolcvgRowId { get { return GetFieldInt("POLCVG_ROW_ID"); } set { SetField("POLCVG_ROW_ID", value); } }
        public string AdminActivitydate { get { return GetFieldString("ADMIN_ACTIVITY_DATE"); } set { SetField("ADMIN_ACTIVITY_DATE", value); } }
        public string AdminCompanyName { get { return GetFieldString("ADMIN_COMPANY_NAME"); } set { SetField("ADMIN_COMPANY_NAME", value); } }
        public string MasterCompanyName { get { return GetFieldString("MASTER_COMPANY_NAME"); } set { SetField("MASTER_COMPANY_NAME", value); } }
        public string PolicyCompanyName { get { return GetFieldString("POLICY_COMPANY_NAME"); } set { SetField("POLICY_COMPANY_NAME", value); } }
        public string PolicyModule { get { return GetFieldString("POLICY_MODULE"); } set { SetField("POLICY_MODULE", value); } }
        public string PolicyNumber { get { return GetFieldString("POLICY_NUMBER"); } set { SetField("POLICY_NUMBER", value); } }
        public string PolicySymbol { get { return GetFieldString("POLICY_SYMBOL"); } set { SetField("POLICY_SYMBOL", value); } }
        public string TransEffDate { get { return GetFieldString("TRANS_EFF_DATE"); } set { SetField("TRANS_EFF_DATE", value); } }
        public string ClaimCurrAdjName { get { return GetFieldString("CLAIM_CURR_ADJ_NAME"); } set { SetField("CLAIM_CURR_ADJ_NAME", value); } }
        public string ClaimNumber { get { return GetFieldString("CLAIM_NUMBER"); } set { SetField("CLAIM_NUMBER", value); } }
        public string ClaimantName { get { return GetFieldString("CLAIMANT_NAME"); } set { SetField("CLAIMANT_NAME", value); } }
        public string EventLossDate { get { return GetFieldString("EVENT_LOSS_DATE"); } set { SetField("EVENT_LOSS_DATE", value); } }
        public string OperatorName { get { return GetFieldString("OPERATOR_NAME"); } set { SetField("OPERATOR_NAME", value); } }
        public double PolicyDeductibleAmt { get { return GetFieldDouble("POLICY_DEDUCTIBLE_AMT"); } set { SetField("POLICY_DEDUCTIBLE_AMT", value); } }
        public double TotalPaidAmt { get { return GetFieldDouble("TOTAL_PAID_AMT"); } set { SetField("TOTAL_PAID_AMT", value); } }
        public double DeductibleDueAmt { get { return GetFieldDouble("DEDUCTIBLE_DUE_AMT"); } set { SetField("DEDUCTIBLE_DUE_AMT", value); } }
        public string PaidDate { get { return GetFieldString("PAID_DATE"); } set { SetField("PAID_DATE", value); } }
        public string PayeeName { get { return GetFieldString("PAYEE_NAME"); } set { SetField("PAYEE_NAME", value); } }
        public double PaymentAmount { get { return GetFieldDouble("PAYMENT_AMOUNT"); } set { SetField("PAYMENT_AMOUNT", value); } }
        public string PaymentTypeCode { get { return GetFieldString("PAYMENT_TYPE_CODE"); } set { SetField("PAYMENT_TYPE_CODE", value); } }
        public int ReferenceCode { get { return GetFieldInt("REFRENCE_CODE"); } set { SetField("REFRENCE_CODE", value); } }
        public int VoidFlag { get { return GetFieldInt("VOID_FLAG"); } set { SetField("VOID_FLAG", value); } }
        public int StatusFlag { get { return GetFieldInt("STATUS_FLAG"); } set { SetField("STATUS_FLAG", value); } }
        public string StatusDate { get { return GetFieldString("STATUS_DATE"); } set { SetField("STATUS_DATE", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastupd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            base.Save();
        }

        internal DedBillInvoiceDetails(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "DED_BILL_INVOICE_DETAILS";
            this.m_sKeyField = "DED_BILL_INVOICE_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            base.Initialize();
        }
    }
}
