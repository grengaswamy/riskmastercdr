﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class ClaimXPolicyList : DataCollection
    {
        internal ClaimXPolicyList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"ROW_ID";
            this.SQLFromTable = "CLAIM_X_POLICY";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "ClaimXPolicy";
		}
        public new ClaimXPolicy this[int keyValue] { get { return base[keyValue] as ClaimXPolicy; } }
        public new ClaimXPolicy AddNew() { return base.AddNew() as ClaimXPolicy; }
        public ClaimXPolicy Add(UnitXClaim obj) { return base.Add(obj) as ClaimXPolicy; }
        public new ClaimXPolicy Add(int keyValue) { return base.Add(keyValue) as ClaimXPolicy; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
