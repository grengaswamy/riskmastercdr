using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forMedicalStaffPrivilegeList.
	/// </summary>
	public class MedicalStaffPrivilegeList : DataCollection
	{
		internal MedicalStaffPrivilegeList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"STAFF_PRIV_ID";
			this.SQLFromTable =	"STAFF_PRIVS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "MedicalStaffPrivilege";
		}
		public new MedicalStaffPrivilege this[int keyValue]{get{return base[keyValue] as MedicalStaffPrivilege;}}
		public new MedicalStaffPrivilege AddNew(){return base.AddNew() as MedicalStaffPrivilege;}
		public  MedicalStaffPrivilege Add(MedicalStaffPrivilege obj){return base.Add(obj) as MedicalStaffPrivilege;}
		public new MedicalStaffPrivilege Add(int keyValue){return base.Add(keyValue) as MedicalStaffPrivilege;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}