using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXContactInfoList.
	/// </summary>
	public class EntityXContactInfoList : DataCollection
	{
		internal EntityXContactInfoList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CONTACT_ID";
			this.SQLFromTable =	"ENT_X_CONTACTINFO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EntityXContactInfo";
		}
		public new EntityXContactInfo this[int keyValue]{get{return base[keyValue] as EntityXContactInfo;}}
		public new EntityXContactInfo AddNew(){return base.AddNew() as EntityXContactInfo;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}