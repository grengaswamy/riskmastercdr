using System;
using System.Diagnostics;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXTransEnhList.
	/// </summary>
	public class PolicyXTransEnhList : DataCollection
	{
		internal PolicyXTransEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"TRANSACTION_ID";
			this.SQLFromTable =	"POLICY_X_TRANS_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXTransEnh";
		}
		//public new PolicyXTransEnh this[int keyValue]{get{return base[keyValue] as PolicyXTransEnh;}}

        public override DataObject this[int keyValue]
        {
            get
            {
                DataObject objPolicyXTransEnh = null;
                StackTrace stackTrace = new StackTrace();
                string sStackTrace = Convert.ToString(stackTrace);
                if (!base.m_keySet.ContainsKey(keyValue) && sStackTrace.Contains("Riskmaster.ActiveScript.ScriptHost.ExecuteMethod") && sStackTrace.Contains("Riskmaster.Application.FormProcessor.FormProcessor.ProcessForm"))
                {
                    int[] arrKeys = new int[base.m_keySet.Count];
                    base.m_keySet.Keys.CopyTo(arrKeys, 0);
                    objPolicyXTransEnh = base[arrKeys[keyValue - 1]] as DataObject;
                }
                else
                {
                    objPolicyXTransEnh = base[keyValue] as DataObject;
                }
                return objPolicyXTransEnh as PolicyXTransEnh;
            }
        }
		public new PolicyXTransEnh AddNew(){return base.AddNew() as PolicyXTransEnh;}
		public  PolicyXTransEnh Add(PolicyXTransEnh obj){return base.Add(obj) as PolicyXTransEnh;}
		public new PolicyXTransEnh Add(int keyValue){return base.Add(keyValue) as PolicyXTransEnh;}

		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}