using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for PolicyXUarList.
    /// Author: Sumit Kumar
    /// Date: 03/16/2010
    /// MITS: 18229 
    /// </summary>
    [Riskmaster.DataModel.Summary("POLICY_X_UAR", "UAR_ROW_ID", "UarRowId")]
    public class PolicyXUar : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
									   {"UarRowId","UAR_ROW_ID"},
									   {"PolicyId","POLICY_ID"},
                                       {"ExposureId","EXPOSURE_ID"},
									   {"UarId","UAR_ID"},
                                       {"PolicyStatusCode","POLICY_STATUS_CODE"},
                                       {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                       {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                       {"AddedByUser", "ADDED_BY_USER"},
                                       {"UpdatedByUser", "UPDATED_BY_USER"},
		};

        public int UarRowId { get { return GetFieldInt("UAR_ROW_ID"); } set { SetField("UAR_ROW_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public int ExposureId { get { return GetFieldInt("EXPOSURE_ID"); } set { SetField("EXPOSURE_ID", value); } }
        public int UarId { get { return GetFieldInt("UAR_ID"); } set { SetField("UAR_ID", value); } }
        public int PolicyStatusCode { get { return GetFieldInt("POLICY_STATUS_CODE"); } set { SetField("POLICY_STATUS_CODE", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        //Sumit - Start(04/29/2010) - MITS# 20483
        #region Child Implementation
        private string[,] sChildren = {{"PolicyXPschedEnhList","PolicyXPschedEnhList"}
									  };

        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            base.OnChildInit(childName, childType);
        }

        //Sumit - Start(04/29/2010) - MITS# 20483
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "PolicyXPschedEnhList":
                    (itemValue as PolicyXPschedEnh).PolicyId = this.PolicyId;
                    (itemValue as PolicyXPschedEnh).UarId = this.UarRowId;
                    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }
        //Sumit - End

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            //BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
        }

        //Sumit - Start(04/29/2010) - MITS# 20483
        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "PolicyXPschedEnhList":
                    foreach (PolicyXPschedEnh item in (childValue as PolicyXPschedEnhList))
                    {
                        if (item.IsNew)
                        {
                            item.PolicyId = this.PolicyId;
                            item.UarId = this.UarRowId;
                        }
                    }
                    (childValue as IPersistence).Save();
                    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }
        //Sumit - End

        //Sumit - Start(03/16/2010) - MITS# 18229
        public PolicyXPschedEnhList PolicyXPschedEnhList
        {
            get
            {
                PolicyXPschedEnhList objList = base.m_Children["PolicyXPschedEnhList"] as PolicyXPschedEnhList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                {
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                }
                return objList;
            }
        }
        //Sumit - End
        #endregion
        internal PolicyXUar(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "POLICY_X_UAR";
            this.m_sKeyField = "UAR_ROW_ID";
            this.m_sFilterClause = string.Empty;
            this.m_sParentClassName = "PolicyXExpEnh";
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Sumit - Start(04/29/2010) - MITS# 20483
            base.InitChildren(sChildren);
            //Sumit - End
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();  
        }

        //TODO Remove this after debugging is done.  Could be a security issue.
        new string ToString()
        {
            return (this as DataObject).Dump();
        }
    }
}
