
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forFundsDepositList.
	/// </summary>
	public class FundsDepositList : DataCollection
	{
		internal FundsDepositList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"DEPOSIT_ID";
			this.SQLFromTable =	"FUNDS_DEPOSIT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "FundsDeposit";
		}
		public new FundsDeposit this[int keyValue]{get{return base[keyValue] as FundsDeposit;}}
		public new FundsDeposit AddNew(){return base.AddNew() as FundsDeposit;}
		public  FundsDeposit Add(FundsDeposit obj){return base.Add(obj) as FundsDeposit;}
		public new FundsDeposit Add(int keyValue){return base.Add(keyValue) as FundsDeposit;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}