
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClaimXLitigationList.
	/// </summary>
	public class ClaimXLitigationList : DataCollection
	{
		internal ClaimXLitigationList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"LITIGATION_ROW_ID";
			this.SQLFromTable =	"CLAIM_X_LITIGATION";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ClaimXLitigation";
		}
		public new ClaimXLitigation this[int keyValue]{get{return base[keyValue] as ClaimXLitigation;}}
		public new ClaimXLitigation AddNew(){return base.AddNew() as ClaimXLitigation;}
		public  ClaimXLitigation Add(ClaimXLitigation obj){return base.Add(obj) as ClaimXLitigation;}
		public new ClaimXLitigation Add(int keyValue){return base.Add(keyValue) as ClaimXLitigation;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}