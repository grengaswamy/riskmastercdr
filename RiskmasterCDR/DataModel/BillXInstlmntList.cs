
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXDsbrsmntList.
	/// </summary>
	public class BillXInstlmntList : DataCollection
	{
		internal BillXInstlmntList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"INSTALLMENT_ROWID";
			this.SQLFromTable =	"BILL_X_INSTLMNT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXInstlmnt";
		}
		public new BillXInstlmnt this[int keyValue]{get{return base[keyValue] as BillXInstlmnt;}}
		public new BillXInstlmnt AddNew(){return base.AddNew() as BillXInstlmnt;}
		public  BillXInstlmnt Add(BillXInstlmnt obj){return base.Add(obj,obj.KeyFieldValue > 0) as BillXInstlmnt;}
		public new BillXInstlmnt Add(int keyValue){return base.Add(keyValue) as BillXInstlmnt;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}