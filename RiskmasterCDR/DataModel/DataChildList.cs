using System;
using System.Collections;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// DataChildList contains a list of names and values for all child objects or "child object lists".
	/// Used by the base DataObject to handle children objects.
	/// Notes: 1.)  Typically individual child objects will be wrapped in DataLazyLoader wrapper objects.
	///				2.) "Child object lists" (example: PersonsInvolved) will be fully loaded but their member objects will each
	///				be wrapped in DataLazyLoader wrapper objects.
	/// </summary>
	internal class DataChildList : IEnumerable 
	{
		protected System.Collections.Hashtable m_Children=null;

		public DataChildList()
		{
			m_Children = new System.Collections.Hashtable();
		}
		
		public object this[string PropertyName] //Could contain a single object or another LIST of objects.
		{
			get{	return m_Children[PropertyName]; }
			set
			{
				if(!m_Children.ContainsKey(PropertyName))
					m_Children.Add(PropertyName,value);
				else
					m_Children[PropertyName] = value;
			}
		}

		//Marks all Children objects as "stale".
		public void Clear(bool bNewParent)
		{ 
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = this.GetKeyArray();
			DataCollection objCol = null;
			IDataModel pIDataModel =null;
			DataSimpleList objSimpleList = null;
			for(int i=0; i< arr.Length;i++)
			{
				pIDataModel  = (m_Children[arr[i]] as IDataModel);
				objCol = pIDataModel as DataCollection;
				objSimpleList = pIDataModel as DataSimpleList;
				if(objCol!=null)
				{
					objCol.ClearContent();
					if(!bNewParent)
						pIDataModel.IsStale = true;
				}
				else if(objSimpleList!=null)
				{
					objSimpleList.ClearAll();
					pIDataModel.DataChanged = false;
					if(!bNewParent)
						pIDataModel.IsStale = true;
				}
				else
				{
					pIDataModel.IsStale = true;
					pIDataModel.DataChanged =false;
				}
			}
		}
		
		public void Empty()
		{
			m_Children = new System.Collections.Hashtable();
		}		
		public int Count()
		{
			return m_Children.Count;
		}
		
		internal ICollection Keys{get{return m_Children.Keys;}}
		
		internal object[] GetKeyArray()
		{
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = new object[m_Children.Keys.Count];
			m_Children.Keys.CopyTo(arr,0);
			return arr;
		}
		public System.Collections.IEnumerator GetEnumerator()
		{return m_Children.Keys.GetEnumerator();}
		
		public bool ContainsChild(string propertyName, bool ignoreCase)
		{
			//Check Object Field Properties
			if(ignoreCase)
			{
				foreach(string s in m_Children.Keys)
					if(0==String.Compare(s, propertyName, ignoreCase))
						return true;
			}else
				return m_Children.ContainsKey(propertyName);

			return false;
		}


		/// <summary>
		/// ToString is overridden to provide a comma separated list of field names suitable for use in SQL queries.
		/// </summary>
		/// <returns></returns>
		override public string ToString()
		{
			string ret="";
			foreach (string s in m_Children.Keys)
				ret += s.ToUpper() + ",";
			
			return ret.TrimEnd(',');
		}	
	}

}
