
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXReceiptList.
	/// </summary>
	public class BillXReceiptList : DataCollection
	{
		internal BillXReceiptList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"";
			this.SQLFromTable =	"BILL_X_RECEIPT";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXReceipt";
		}
		public new BillXReceipt this[int keyValue]{get{return base[keyValue] as BillXReceipt;}}
		public new BillXReceipt AddNew(){return base.AddNew() as BillXReceipt;}
		public  BillXReceipt Add(BillXReceipt obj){return base.Add(obj) as BillXReceipt;}
		public new BillXReceipt Add(int keyValue){return base.Add(keyValue) as BillXReceipt;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}