using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.DataModel
{
	/// <summary>
    /// Author - Ayush Arya
    /// Date - 12//18/2009
    /// MITS# - 18230
	/// Description - Property Claim Schedule Info tab.
	/// </summary>

    [Riskmaster.DataModel.Summary("POLICY_X_PSCHED_ENH", "PSCHED_ROW_ID")]
	public class PolicyXPschedEnh : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
                                        {"PschedRowID","PSCHED_ROW_ID"},
                                        //Start:Nitin Goel - 02/17/2010 MITS#18230
                                        //{"PropertyId","PROPERTY_ID"},
                                         {"UarId","UAR_ROW_ID"},
                                        //End:Nitin Goel - 02/17/2010 MITS#18230
                                        {"PolicyId","POLICY_ID"},
                                         //Start:Nitin Goel - 02/17/2010 MITS#18230
                                        //{"ScheduleName", "NAME"},
                                        {"Name", "NAME"},
                                         //End:Nitin Goel - 02/17/2010 MITS#18230
                                        {"Amount", "AMOUNT"},
		                            };

        public int PschedRowID { get { return GetFieldInt("PSCHED_ROW_ID"); } set { SetField("PSCHED_ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.ChildLink, "Property")]
        //Start:Nitin Goel - 02/17/2010 MITS#18230
        //public int PropertyID { get { return GetFieldInt("PROPERTY_ID"); } set { SetField("PROPERTY_ID", value); } }
        public int UarId { get { return GetFieldInt("UAR_ROW_ID"); } set { SetField("UAR_ROW_ID", value); } }
        //End:Nitin Goel - 02/17/2010 MITS#18230
        [ExtendedTypeAttribute(RMExtType.ChildLink, "policy_enh")]
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        //Start:Nitin Goel - 02/17/2010 MITS#18230
        //public string ScheduleName { get { return GetFieldString("NAME"); } set { SetField("NAME", value); } }
        public string Name { get { return GetFieldString("NAME"); } set { SetField("NAME", value); } }
        //End:Nitin Goel - 02/17/2010 MITS#18230
        public double  Amount { get { return GetFieldDouble("AMOUNT"); } set { SetField("AMOUNT", value); } }
        
        #endregion

        internal PolicyXPschedEnh(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}


		new private void Initialize()
		{
            this.m_sTableName = "POLICY_X_PSCHED_ENH";
            this.m_sKeyField = "PSCHED_ROW_ID";
            this.m_sFilterClause = string.Empty;
			//Add all object Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
            this.m_sParentClassName = "PolicyXUar";
            //Moved after most init logic so that scripting can be called successfully.
			base.Initialize();  
		}
		
		protected override void OnBuildNewUniqueId()
		{
            base.OnBuildNewUniqueId();
		}
	}
}
