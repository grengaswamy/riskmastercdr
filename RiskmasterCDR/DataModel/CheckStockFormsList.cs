using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for CheckStockFormsList.
	/// </summary>
	public class CheckStockFormsList : DataCollection
	{
		internal CheckStockFormsList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"FORM_ROW_ID";
			this.SQLFromTable =	"CHECK_STOCK_FORMS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CheckStockForms";
		}
		public new CheckStockForms this[int keyValue]{get{return base[keyValue] as CheckStockForms;}}
		public new CheckStockForms AddNew(){return base.AddNew() as CheckStockForms;}
		public  CheckStockForms Add(CheckStockForms obj){return base.Add(obj) as CheckStockForms;}
		public new CheckStockForms Add(int keyValue){return base.Add(keyValue) as CheckStockForms;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}