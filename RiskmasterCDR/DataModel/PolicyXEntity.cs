﻿using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Add by skhare7 for policyInterface
    
    /// </summary>

    [Riskmaster.DataModel.Summary("POLICY_X_ENTITY", "POLICYENTITY_ROWID", "PolicyEid")]
    public class PolicyXEntity : DataObject
    {
        #region Database Field List
        private string[,] sFields = {   
                                        {"EntityId","ENTITY_ID"},
                                       {"PolicyId","POLICY_ID"},
                                        {"PolicyEid","POLICYENTITY_ROWId"},
                                        {"TypeCode","TYPE_CODE"},
                                          {"PolicyUnitRowid","POLICY_UNIT_ROW_ID"},
                                          {"ExternalRole","EXTERNAL_ROLE"},
                                          // psharma Jira 12759
										  {"AddAsClaimant","ADD_AS_CLAIMANT"},    
                                          // mits 35925 start
                                          {"AddressId","ADDRESS_ID"}
                                          //mits 35925 end
                                    };
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }

        public int PolicyEid { get { return GetFieldInt("POLICYENTITY_ROWId"); } set { SetField("POLICYENTITY_ROWId", value); } }
        public int TypeCode { get { return GetFieldInt("TYPE_CODE"); } set { SetField("TYPE_CODE", value); } }
        public int PolicyUnitRowid { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        public string ExternalRole { get { return GetFieldString("EXTERNAL_ROLE"); } set { SetField("EXTERNAL_ROLE", value); } }
       // mits 35925 start
         public int AddressId { get { return GetFieldInt("ADDRESS_ID"); } set { SetField("ADDRESS_ID",value); } }
        //mits 35925 end
		public bool AddAsClaimant { get { return GetFieldBool("ADD_AS_CLAIMANT"); } set { SetField("ADD_AS_CLAIMANT", value); } }
        #endregion


        internal PolicyXEntity(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "POLICY_X_ENTITY";
            this.m_sKeyField = "POLICYENTITY_ROWID";
            this.m_sParentClassName = "Policy";
            this.m_sFilterClause = string.Empty;
                     base.InitFields(sFields);
           
            base.Initialize();
        }
    }
}
