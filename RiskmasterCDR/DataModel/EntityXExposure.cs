using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXExposure.
	/// </summary>
	//TODO: Complete Summary
	[Riskmaster.DataModel.Summary("ENTITY_EXPOSURE","EXPOSURE_ROW_ID")]
	public class EntityXExposure : DataObject
	{
		public override string Default
		{
			get
			{
				return String.Format("{0} - {1}", Common.Conversion.ToDate(this.StartDate).ToShortDateString(),Common.Conversion.ToDate(this.EndDate).ToShortDateString());
			}
		}

		#region Database Field List
		private string[,] sFields = {
										{"ExposureRowId","EXPOSURE_ROW_ID"},
									   {"EntityId","ENTITY_ID"},
									   {"StartDate","START_DATE"},
									   {"EndDate","END_DATE"},
									   {"NoOfEmployees","NO_OF_EMPLOYEES"},
									   {"NoOfWorkHours","NO_OF_WORK_HOURS"},
									   {"PayrollAmount","PAYROLL_AMOUNT"},
									   {"AssetValue","ASSET_VALUE"},
									   {"SquareFootage","SQUARE_FOOTAGE"},
										{"TotalRevenue","TOTAL_REVENUE"},
										{"OtherBase","OTHER_BASE"},
										{"RiskMgmtOverhead","RISK_MGMT_OVERHEAD"},
										{"UserGeneratedFlag","USER_GENERATD_FLAG"},
										{"VehicleCount","VEHICLE_COUNT"},
	};
		public int  ExposureRowId{	get{  return GetFieldInt("EXPOSURE_ROW_ID");}	set{ SetField("EXPOSURE_ROW_ID",value);}}
		public int  EntityId{get{  return   GetFieldInt("ENTITY_ID");}set{ SetField("ENTITY_ID",value);}}
		public string StartDate{get{ return GetFieldString("START_DATE");}set{SetField("START_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string EndDate{get{ return GetFieldString("END_DATE");}set{SetField("END_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public int NoOfEmployees{get{ return GetFieldInt("NO_OF_EMPLOYEES");}set{SetField("NO_OF_EMPLOYEES",value);}}
		public int NoOfWorkHours{get{ return GetFieldInt("NO_OF_WORK_HOURS");}set{SetField("NO_OF_WORK_HOURS",value);}}
		public double PayrollAmount{get{ return GetFieldDouble("PAYROLL_AMOUNT");}set{SetField("PAYROLL_AMOUNT",value);}}
		public double AssetValue{get{ return GetFieldDouble("ASSET_VALUE");}set{SetField("ASSET_VALUE",value);}}
		public int SquareFootage{get{ return GetFieldInt("SQUARE_FOOTAGE");}set{SetField("SQUARE_FOOTAGE",value);}}
		public double TotalRevenue{get{ return GetFieldDouble("TOTAL_REVENUE");}set{SetField("TOTAL_REVENUE",value);}}
		public double OtherBase{get{ return GetFieldDouble("OTHER_BASE");}set{SetField("OTHER_BASE",value);}}
		public double RiskMgmtOverhead{get{ return GetFieldDouble("RISK_MGMT_OVERHEAD");}set{SetField("RISK_MGMT_OVERHEAD",value);}}
		public bool UserGeneratedFlag{get{ return GetFieldBool("USER_GENERATD_FLAG");}set{SetField("USER_GENERATD_FLAG",value);}}
		public int VehicleCount{get{ return GetFieldInt("VEHICLE_COUNT");}set{SetField("VEHICLE_COUNT",value);}}
		#endregion

		#region Supplemental Fields Exposed
		internal override string OnBuildSuppTableName()
		{
			return "ENT_EXPOSURE_SUPP";
		}

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion

		internal EntityXExposure(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENTITY_EXPOSURE";
			this.m_sKeyField = "EXPOSURE_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
