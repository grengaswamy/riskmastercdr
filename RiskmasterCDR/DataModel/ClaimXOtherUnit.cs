﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_OTHERUNIT", "ROW_ID", "CLAIM_ID")]

    public class ClaimXOtherUnit:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"RowId", "ROW_ID"},                                                        
														{"ClaimId", "CLAIM_ID"},
                                                        {"OtherUnitId","OTHER_UNIT_ID"}, 
                                                        //{"UnitType", "UNIT_TYPE"},                                        														                                                  
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                                        {"Insured", "ISINSURED"}
		};


        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.ChildLink, "OtherUnit")]
        public int OtherUnitId { get { return GetFieldInt("OTHER_UNIT_ID"); } set { SetField("OTHER_UNIT_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        //public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
        public bool Insured { get { return GetFieldBool("ISINSURED"); } set { SetField("ISINSURED", value); } }
        #endregion

        #region Child Implementation

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            // If Entity is involved we save this first (in case we need the EntityId.)
            ;
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }



        #endregion

        internal ClaimXOtherUnit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        public override IDataModel Parent
        {
            get
            {                
                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

        new private void Initialize()
        {

            this.m_sTableName = "CLAIM_X_OTHERUNIT";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.            
            
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
    }
}
