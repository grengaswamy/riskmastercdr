
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPhysicianPrevHospitalList.
	/// </summary>
	public class PhysicianPrevHospitalList : DataCollection
	{
		internal PhysicianPrevHospitalList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PREV_HOSP_ID";
			this.SQLFromTable =	"PHYS_PREV_HOSP";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PhysicianPrevHospital";
		}
		public new PhysicianPrevHospital this[int keyValue]{get{return base[keyValue] as PhysicianPrevHospital;}}
		public new PhysicianPrevHospital AddNew(){return base.AddNew() as PhysicianPrevHospital;}
		public  PhysicianPrevHospital Add(PhysicianPrevHospital obj){return base.Add(obj) as PhysicianPrevHospital;}
		public new PhysicianPrevHospital Add(int keyValue){return base.Add(keyValue) as PhysicianPrevHospital;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}