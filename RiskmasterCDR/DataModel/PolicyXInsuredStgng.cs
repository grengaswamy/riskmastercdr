﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_INSURED_STGNG", "INSRD_ROW_ID", "InsuredEid")]
    public class PolicyXInsuredStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {   
                                        {"PolicyId","POLICY_ID"},
                                        {"InsuredEid","INSURED_EID"}                                       
                                    };
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.OrgH, "ANY")]
        public int InsuredEid { get { return GetFieldInt("INSURED_EID"); } set { SetField("INSURED_EID", value); } }        
        #endregion

        internal PolicyXInsuredStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "POLICY_X_INSURED_STGNG";
            this.m_sKeyField = "INSRD_ROW_ID";
            this.m_sFilterClause = string.Empty;
            this.m_sParentClassName = "PolicyEnh";
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();
        }
    }
}
