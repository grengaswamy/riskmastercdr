﻿using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary of Demand Class.
    /// </summary>
    //TODO: Complete Summary
    [Riskmaster.DataModel.Summary("DEMAND_OFFER", "DEMAND_OFFER_ROW_ID")]
    public class DemandOffer : DataObject
    {
        public static string ParentKeyFieldName = "PARENT_ID";
        public static string ParentNameFieldName = "PARENT_NAME";
        #region Database Field List
        private string[,] sFields = {
														{"ActivityCode", "ACTIVITY_CODE"},
														{"DemandOfferRowId", "DEMAND_OFFER_ROW_ID"},
														{"ParentId", "PARENT_ID"},
														{"ParentName", "PARENT_NAME"},
														{"DemandOfferAmount", "DEMAND_OFFER_AMOUNT"},
														{"DemandOfferDate", "DEMAND_OFFER_DATE"},
														{"ResultCode", "RESULT_CODE"},
														{"DecisionCode", "DECISION_CODE"},
														{"Comments", "COMMENTS"},
														{"HTMLComments","HTMLCOMMENTS"},
														{"AddedByUser","ADDED_BY_USER"},
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"}
		};

        [ExtendedTypeAttribute(RMExtType.Code, "DEM_OFFER_ACTV")]
        public int ActivityCode { get { return GetFieldInt("ACTIVITY_CODE"); } set { SetField("ACTIVITY_CODE", value); } }
        public int DemandOfferRowId { get { return GetFieldInt("DEMAND_OFFER_ROW_ID"); } set { SetField("DEMAND_OFFER_ROW_ID", value); } }
        public int ParentId { get { return GetFieldInt("PARENT_ID"); } set { SetField("PARENT_ID", value); } }
        public string ParentName { get { return GetFieldString("PARENT_NAME"); } set { SetField("PARENT_NAME", value); } }
        public double DemandOfferAmount { get { return GetFieldDouble("DEMAND_OFFER_AMOUNT"); } set { SetField("DEMAND_OFFER_AMOUNT", value); } }
        public string DemandOfferDate { get { return GetFieldString("DEMAND_OFFER_DATE"); } set { SetField("DEMAND_OFFER_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DEM_OFFER_RSLT")]
        public int ResultCode { get { return GetFieldInt("RESULT_CODE"); } set { SetField("RESULT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DEM_OFFER_DECS")]
        public int DecisionCode { get { return GetFieldInt("DECISION_CODE"); } set { SetField("DECISION_CODE", value); } }

        public string Comments { get { return GetFieldString("COMMENTS"); } set { SetField("COMMENTS", value); } }
        public string HTMLComments { get { return GetFieldStringFallback("HTMLCOMMENTS", "COMMENTS"); } set { SetField("HTMLCOMMENTS", value); } }

        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        #endregion

        #region Child Implementation
        

        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }
        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            ;
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }


        //Child Property Accessors
        

        #endregion
        internal DemandOffer(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "DEMAND_OFFER";
            this.m_sKeyField = "DEMAND_OFFER_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //base.InitChildren(sChildren);

            this.m_sParentClassName = this.ParentName;

            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion
    }
}
