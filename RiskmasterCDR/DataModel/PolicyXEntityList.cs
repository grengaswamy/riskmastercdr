﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class PolicyXEntityList : DataCollection
    {


        internal PolicyXEntityList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "POLICYENTITY_ROWID";
            this.SQLFromTable = "POLICY_X_ENTITY";
            this.TypeName = "PolicyXEntity";
        }
        public new PolicyXEntity this[int keyValue] { get { return base[keyValue] as PolicyXEntity; } }
        public new PolicyXEntity AddNew() { return base.AddNew() as PolicyXEntity; }
        public PolicyXEntity Add(PolicyXEntity obj) { return base.Add(obj) as PolicyXEntity; }
        public new PolicyXEntity Add(int keyValue) { return base.Add(keyValue) as PolicyXEntity; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
