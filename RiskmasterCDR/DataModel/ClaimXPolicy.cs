﻿using System;

namespace Riskmaster.DataModel
{
     [Riskmaster.DataModel.Summary("CLAIM_X_POLICY", "ROW_ID")]
    public class ClaimXPolicy : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"RowId", "ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"PolicyId", "POLICY_ID"},
		};

        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        #endregion

        internal ClaimXPolicy(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}

        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIM_X_POLICY";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //base.InitChildren(sChildren);
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
    }
}
