using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forReserveCurrentList.
	/// </summary>
	public class ReserveCurrentList : DataCollection
	{
		internal ReserveCurrentList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"RC_ROW_ID";
			this.SQLFromTable =	"RESERVE_CURRENT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ReserveCurrent";
		}
		public new ReserveCurrent this[int keyValue]{get{return base[keyValue] as ReserveCurrent;}}
		public new ReserveCurrent AddNew(){return base.AddNew() as ReserveCurrent;}
		public  ReserveCurrent Add(ReserveCurrent obj){return base.Add(obj) as ReserveCurrent;}
		public new ReserveCurrent Add(int keyValue){return base.Add(keyValue) as ReserveCurrent;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}