﻿using System;

namespace Riskmaster.DataModel
{
   public class CMMSEAXDataList:DataCollection
    {
        internal CMMSEAXDataList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ENT_MMSEA_ROW_ID";
            this.SQLFromTable = "ENTITY_X_MMSEA";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "CMMSEAXData";
		}
        public new CMMSEAXData this[int keyValue] { get { return base[keyValue] as CMMSEAXData; } }
        public new CMMSEAXData AddNew() { return base.AddNew() as CMMSEAXData; }
        public CMMSEAXData Add(CmXTreatmentPln obj) { return base.Add(obj) as CMMSEAXData; }
        public new CMMSEAXData Add(int keyValue) { return base.Add(keyValue) as CMMSEAXData; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
