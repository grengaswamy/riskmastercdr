using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXDcntEnhList.
	/// </summary>
	public class PolicyXDcntEnhList : DataCollection
	{
		internal PolicyXDcntEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"DISCOUNT_ID";
			this.SQLFromTable =	"POLICY_X_DCNT_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXDcntEnh";
		}
		public new PolicyXDcntEnh this[int keyValue]{get{return base[keyValue] as PolicyXDcntEnh;}}
		public new PolicyXDcntEnh AddNew(){return base.AddNew() as PolicyXDcntEnh;}
		public  PolicyXDcntEnh Add(PolicyXDcntEnh obj){return base.Add(obj) as PolicyXDcntEnh;}
		public new PolicyXDcntEnh Add(int keyValue){return base.Add(keyValue) as PolicyXDcntEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}