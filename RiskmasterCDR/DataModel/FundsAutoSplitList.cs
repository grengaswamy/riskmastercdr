using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forFundsAutoSplitList.
	/// </summary>
	public class FundsAutoSplitList : DataCollection
	{
		internal FundsAutoSplitList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"AUTO_SPLIT_ID";
			this.SQLFromTable =	"FUNDS_AUTO_SPLIT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "FundsAutoSplit";
		}
		public new FundsAutoSplit this[int keyValue]{get{return base[keyValue] as FundsAutoSplit;}}
		public new FundsAutoSplit AddNew(){return base.AddNew() as FundsAutoSplit;}
		public  FundsAutoSplit Add(FundsAutoSplit obj){return base.Add(obj) as FundsAutoSplit;}
		public new FundsAutoSplit Add(int keyValue){return base.Add(keyValue) as FundsAutoSplit;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}