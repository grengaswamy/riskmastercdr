﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("PROPERTY_UNIT_STGNG", "PROPERTY_ID", "PIN")]
    public class PropertyUnitStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                                        {"PropertyId", "PROPERTY_ID"},
                                                        {"Pin", "PIN"},
                                                        {"Description", "DESCRIPTION"},
                                                        {"Addr1", "ADDR1"},
                                                        {"Addr2", "ADDR2"},
                                                        {"City", "CITY"},
                                                        {"StateId", "STATE_ID"},
                                                        {"ZipCode", "ZIP_CODE"},
                                                        {"YearOfConstruction", "YEAR_OF_CONS"},
                                                        {"AppraisedValue", "APPRAISED_VALUE"},
                                                        {"ReplacementValue", "REPLACEMENT_VALUE"},
                                                        {"AppraisedDate", "APPRAISED_DATE"},
                                                        {"LandValue", "LAND_VALUE"},
                                                        { "CountryCode", "COUNTRY_CODE"}, 
                                                        { "Status", "STATUS"},
                                                        { "Ratebook", "RATEBOOK"},
                                                        { "Premium", "PREM"},
                                                        { "Territory", "TERRITORY"},
                                                        { "ProtectionCls", "PROTECTION_CLS"},
                                                        { "Deductible", "DEDUCTIBLE"},
                                                        { "NumOfFamilies", "FAMILIES_NUM"},
                                                        { "ConstrtnType", "CONSTR_TYPE"},
                                                        { "InsideCity", "INSIDE_CITY"},
                                                        { "Occupance", "OCCUPANCE"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
                                    };
        public int PropertyId { get { return GetFieldInt("PROPERTY_ID"); } set { SetField("PROPERTY_ID", value); } }
        public string Pin { get { return GetFieldString("PIN"); } set { SetField("PIN", value); } }
        public string Description { get { return GetFieldString("DESCRIPTION"); } set { SetField("DESCRIPTION", value); } }
        public string Addr1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Addr2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public string StateId { get { return GetFieldString("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        public int YearOfConstruction { get { return GetFieldInt("YEAR_OF_CONS"); } set { SetField("YEAR_OF_CONS", value); } }
        public double AppraisedValue { get { return GetFieldDouble("APPRAISED_VALUE"); } set { SetField("APPRAISED_VALUE", value); } }
        public double ReplacementValue { get { return GetFieldDouble("REPLACEMENT_VALUE"); } set { SetField("REPLACEMENT_VALUE", value); } }
        public string AppraisedDate { get { return GetFieldString("APPRAISED_DATE"); } set { SetField("APPRAISED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public double LandValue { get { return GetFieldDouble("LAND_VALUE"); } set { SetField("LAND_VALUE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public int CountryCode { get { return GetFieldInt("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public string Status { get { return GetFieldString("STATUS"); } set { SetField("STATUS", value); } }
        public string Ratebook { get { return GetFieldString("RATEBOOK"); } set { SetField("RATEBOOK", value); } }
        public double Premium { get { return GetFieldDouble("PREM"); } set { SetField("PREM", value); } }
        public string Territory { get { return GetFieldString("TERRITORY"); } set { SetField("TERRITORY", value); } }
        public string ProtectionCls { get { return GetFieldString("PROTECTION_CLS"); } set { SetField("PROTECTION_CLS", value); } }
        public double Deductible { get { return GetFieldDouble("DEDUCTIBLE"); } set { SetField("DEDUCTIBLE", value); } }
        public string NumOfFamilies { get { return GetFieldString("FAMILIES_NUM"); } set { SetField("FAMILIES_NUM", value); } }
        public string ConstrtnType { get { return GetFieldString("CONSTR_TYPE"); } set { SetField("CONSTR_TYPE", value); } }
        public string InsideCity { get { return GetFieldString("INSIDE_CITY"); } set { SetField("INSIDE_CITY", value); } }
        public string Occupance { get { return GetFieldString("OCCUPANCE"); } set { SetField("OCCUPANCE", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Ends
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        #region Child Implementation

		//Handle adding the our key to any additions to the record collection properties.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			base.OnChildItemAdded (childName, itemValue);
		}					

		//Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			base.OnChildPostSave (childName, childValue, isNew);
		}		
		#endregion
		
        internal PropertyUnitStgng(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}

		new private void Initialize()
		{
			this.m_sTableName = "PROPERTY_UNIT_STGNG";
			this.m_sKeyField = "PROPERTY_ID";
			this.m_sFilterClause = string.Empty;
			//Add all object Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
            this.m_sParentClassName = string.Empty;
            //Moved after most init logic so that scripting can be called successfully.
			base.Initialize();  
		}
		
        internal override string OnBuildDeleteSQL()
		{
			return "UPDATE PROPERTY_UNIT_STGNG SET DELETED_FLAG = -1 WHERE PROPERTY_ID=" + this.PropertyId;
		}

		// Cancel Deletion of Supplementals during "delete" of this object.
		internal override bool OnSuppDelete()
		{
			return false;
		}

		// Cancel Deletion of all PropertyUnit children.
		// (Obviously will these children would be needed if the entity is ever 
		// "undeleted" by changing the entity deleted flag.
		internal override void OnChildDelete(string childName, IDataModel childValue)
		{
			return;
		}

		internal override string OnApplyFilterClauseSQL(string CurrentSQL)
		{
			string sExtendedFilterClause = this.m_sFilterClause;
			string sSQL = CurrentSQL;
            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                sExtendedFilterClause += " AND ";
            }
			sExtendedFilterClause += "DELETED_FLAG=0";

            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                if (sSQL.IndexOf(" WHERE ") > 0)
                {
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                }
                else
                {
                    sSQL += " WHERE " + sExtendedFilterClause;
                }
            }
			return sSQL;
		}

		protected override void OnBuildNewUniqueId()
		{
			base.OnBuildNewUniqueId();
            if (this.Pin == "" && this.Context.InternalSettings.SysSettings.AutoNumPin)
            {
                this.Pin = GetPin();
            }
		}

		private string GetPin()
		{
			int iPin= this.Context.GetNextUID("PIN_NUMBERS");
			return iPin.ToString("0000000");
		}

        //#region Supplemental Fields Exposed
		
        ///// <summary>
        ///// Supplementals are implemented in the base class but not exposed externally.
        /////This allows the derived class to control whether the object appears to have
        /////supplementals or not. Entity exposes Supplementals.
        ///// </summary>
        //public new Supplementals Supplementals
        //{	
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        //#endregion
    }
}
