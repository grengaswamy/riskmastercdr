﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/19/2014 | 34276  | anavinkumars   | Add new EntityXEntityIDType class to fetch data from db 
 **********************************************************************************************/

using System;
//using Riskmaster.Db;
//using Riskmaster.Common;
//using Riskmaster.ExceptionTypes;


namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for EntityXEntityIDType.cs.
    /// </summary>
    //TODO: Complete Summary
    [Riskmaster.DataModel.Summary("ENT_ID_TYPE", "ID_NUM_ROW_ID", "IdNumRowId")]
    public class EntityXEntityIDType : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                         {"IdNumRowId","ID_NUM_ROW_ID"},
                                         {"EntityId","ENTITY_ID"},
                                         {"IdType","ID_TYPE"},
                                         {"IdNum","ID_NUM"},
                                         {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                         {"DttmRcdAdded","DTTM_RCD_ADDED"},
                                         {"UpdatedByUser","UPDATED_BY_USER"},
                                         {"AddedByUser","ADDED_BY_USER"},
                                         {"EffStartDate","EFF_START_DATE"},
                                         {"EffEndDate","EFF_END_DATE"},
                                         {"DeletedFlag","DELETED_FLAG"}
    };

        public int IdNumRowId { get { return GetFieldInt("ID_NUM_ROW_ID"); } set { SetField("ID_NUM_ROW_ID", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }

        [ExtendedTypeAttribute(RMExtType.Code, "ENTITY_ID_TYPE")]
        public int IdType { get { return GetFieldInt("ID_TYPE"); } set { SetField("ID_TYPE", value); } }
        public string IdNum { get { return GetFieldString("ID_NUM"); } set { SetField("ID_NUM", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string EffStartDate { get { return GetFieldString("EFF_START_DATE"); } set { SetField("EFF_START_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string EffEndDate { get { return GetFieldString("EFF_END_DATE"); } set { SetField("EFF_END_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }

        #endregion


        internal EntityXEntityIDType(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "ENT_ID_TYPE";
            this.m_sKeyField = "ID_NUM_ROW_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            this.m_sParentClassName = "ENTITY";
            base.Initialize();

        }

        new string ToString()
        {
            return (this as DataObject).Dump();
        }
    }
}
