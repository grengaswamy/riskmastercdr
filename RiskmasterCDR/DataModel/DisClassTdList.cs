
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forDisClassTdList.
	/// </summary>
	public class DisClassTdList : DataCollection
	{
		internal DisClassTdList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"TD_ROW_ID";
			this.SQLFromTable =	"DIS_CLASS_TD";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "DisClassTd";
		}
		public new DisClassTd this[int keyValue]{get{return base[keyValue] as DisClassTd;}}
		public new DisClassTd AddNew(){return base.AddNew() as DisClassTd;}
		public  DisClassTd Add(DisClassTd obj){return base.Add(obj) as DisClassTd;}
		public new DisClassTd Add(int keyValue){return base.Add(keyValue) as DisClassTd;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}