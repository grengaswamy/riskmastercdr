/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/19/2014 | 34276  | anavinkumars   | Added Contact_Type for any contact type
 **********************************************************************************************/
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXContactInfo.
	/// </summary>
	[Riskmaster.DataModel.Summary("ENT_X_CONTACTINFO","CONTACT_ID", "ContactName")]
	public class EntityXContactInfo : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"ContactId","CONTACT_ID"},
									   {"EntityId","ENTITY_ID"},
									   {"ContactName","CONTACT_NAME"},
									   {"Title","TITLE"},
									   {"Initials","INITIALS"},
									   {"Addr1","ADDR1"},
									   {"Addr2","ADDR2"},
                                       {"Addr3","ADDR3"},
									   {"Addr4","ADDR4"},
									   {"City","CITY"},
									   {"State","STATE_ID"},
										{"ZipCode","ZIP_CODE"},
										{"Phone","PHONE1"},
										{"Fax","FAX_NUMBER"},
										{"Email","EMAIL_ADDRESS"},
                                        {"EmailAck","EMAIL_ACK"}, //added by navdeep
                                        {"EmailFroi","EMAIL_FROI"},
                                        {"EmailAcord","EMAIL_ACORD"},
                                        //{"LineofBusCode","LINE_OF_BUS_CODE"},
                                        {"ContactType","CONTACT_TYPE_CODE"} //MITS:34276 Contact Type
		};

		public int  ContactId{get{  return GetFieldInt("CONTACT_ID");}set{ SetField("CONTACT_ID",value);}}
		public int EntityId{get{ return GetFieldInt("ENTITY_ID");}set{SetField("ENTITY_ID",value);}}
		public string ContactName{get{ return GetFieldString("CONTACT_NAME");}set{SetField("CONTACT_NAME",value);}}
		public string Title{get{ return GetFieldString("TITLE");}set{SetField("TITLE",value);}}
		public string Initials{get{ return GetFieldString("INITIALS");}set{SetField("INITIALS",value);}}
		public string Addr1{get{ return GetFieldString("ADDR1");}set{SetField("ADDR1",value);}}
		public string Addr2{get{ return GetFieldString("ADDR2");}set{SetField("ADDR2",value);}}
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
		public string City{get{ return GetFieldString("CITY");}set{SetField("CITY",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STATES")]
        public int State{get{ return GetFieldInt("STATE_ID");}set{SetField("STATE_ID",value);}}
		public string ZipCode{get{ return GetFieldString("ZIP_CODE");}set{SetField("ZIP_CODE",value);}}
		public string Phone{get{ return GetFieldString("PHONE1");}set{SetField("PHONE1",value);}}
		public string Fax{get{ return GetFieldString("FAX_NUMBER");}set{SetField("FAX_NUMBER",value);}}
		public string Email{get{ return GetFieldString("EMAIL_ADDRESS");}set{SetField("EMAIL_ADDRESS",value);}}
        //added by navdeep for Chubb
        public bool EmailAck { get { return GetFieldBool("EMAIL_ACK"); } set { SetField("EMAIL_ACK", value); } }
        public bool EmailFroi { get { return GetFieldBool("EMAIL_FROI"); } set { SetField("EMAIL_FROI", value); } }
        public bool EmailAcord { get { return GetFieldBool("EMAIL_ACORD"); } set { SetField("EMAIL_ACORD", value); } }
        //[ExtendedTypeAttribute(RMExtType.Code, "LINE_OF_BUSINESS")]
        //public string LineofBusCode { get { return GetFieldString("LINE_OF_BUS_CODE"); } set { SetField("LINE_OF_BUS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ENT_CONTACT_TYPE")]
        public int ContactType { get { return GetFieldInt("CONTACT_TYPE_CODE"); } set { SetField("CONTACT_TYPE_CODE", value); } }//MITS:34276 Contact Type

		#endregion
        //Geeta Mits 18718 : Child Implemented for Acord Comma Seperated List Issue
        #region Child Implementation

        private string[,] sChildren = {{"LineofBusList","DataSimpleList"}									
									  };

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            Entity objEnt = null;
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {                
                case "DataSimpleList": //There's currently only one so just assume it's LineofBusList.                    
                    DataSimpleList objList = base.m_Children[childName] as DataSimpleList;
                    objList.Initialize("LINE_OF_BUS_CODE", "CONTACT_LOB", "CONTACT_ID");
                    break;
            }
        }        
                
        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "LineofBusList":
                    (childValue as DataSimpleList).SetKeyValue(this.ContactId);                    
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;                
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            string sSQL = "";
            switch (childName)
            {
                case "LineofBusList":
                    sSQL = String.Format("DELETE FROM CONTACT_LOB WHERE CONTACT_ID={0}", (int)this.ContactId);
                    Context.DbConn.ExecuteNonQuery(sSQL, Context.DbTrans);	
                break;
            }            
        }

        [ExtendedTypeAttribute(RMExtType.CodeList, "LINE_OF_BUSINESS")]
        public DataSimpleList LineofBusList
        {
            get
            {
                DataSimpleList objList = base.m_Children["LineofBusList"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                {
                    objList.LoadSimpleList(String.Format("SELECT LINE_OF_BUS_CODE FROM CONTACT_LOB WHERE CONTACT_ID={0}", (int)this.KeyFieldValue));					                
                }
                return objList;
            }
        }    
          
        #endregion


		internal EntityXContactInfo(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENT_X_CONTACTINFO";
			this.m_sKeyField = "CONTACT_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
