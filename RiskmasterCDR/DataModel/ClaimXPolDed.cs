﻿using System;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Linq;
using System.Diagnostics;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_POL_DED", "CLM_X_POL_DED_ID")]
    public class ClaimXPolDed : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"ClmXPolDedId", "CLM_X_POL_DED_ID"},
                                        {"PolicyId", "POLICY_ID"},
                                        {"ClaimId", "CLAIM_ID"},
                                        {"UnitRowId", "POLICY_UNIT_ROW_ID"},
                                        {"PolcvgRowId","POLCVG_ROW_ID"},
                                        {"DedTypeCode", "DED_TYPE_CODE"},
                                        {"ExcludeExpenseFlag", "EXCLUDE_EXPENSE_FLAG"},
                                        {"SirDedAmt", "SIR_DED_AMT"},
                                        {"DttmRcdAdded","DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpdated","DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser","ADDED_BY_USER"},
                                        {"UpdatedByUser","UPDATED_BY_USER"},
                                        {"DiminishingTypeCode","DIMNSHNG_TYPE_CODE"},
                                        {"ClaimantEid","CLAIMANT_EID"},
                                        {"DimPercentNum","DIM_PERCENT_NUM"},
                                        {"DimEvaluationDate","COV_EVALUATION_DATE"},
                                        {"CustomerNum","CUSTOMER_NUM"},
                                        {"PolicyLobCode","POLICY_LOB_CODE"},
                                        {"PolicyXInslineGroupId","POLICY_X_INSLINE_GROUP_ID"},
                                        {"EventId","EVENT_ID"},
                                        {"UnitStateRowId","UNIT_STATE_ROW_ID"}
                                    };

        public int ClmXPolDedId { get { return GetFieldInt("CLM_X_POL_DED_ID"); } set { SetField("CLM_X_POL_DED_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        public int UnitRowId { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        public int PolcvgRowId { get { return GetFieldInt("POLCVG_ROW_ID"); } set { SetField("POLCVG_ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DEDUCTIBLE_TYPE")]
        public int DedTypeCode { get { return GetFieldInt("DED_TYPE_CODE"); } set { SetField("DED_TYPE_CODE", value); } }
        public bool ExcludeExpenseFlag { get { return GetFieldBool("EXCLUDE_EXPENSE_FLAG"); } set { SetField("EXCLUDE_EXPENSE_FLAG", value); } }
        public double SirDedAmt { get { return GetFieldDouble("SIR_DED_AMT"); } set { SetField("SIR_DED_AMT", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpdated { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DIMINISHING_TYPE")]
        public int DiminishingTypeCode { get { return GetFieldInt("DIMNSHNG_TYPE_CODE"); } set { SetField("DIMNSHNG_TYPE_CODE", value); } }
        public int ClaimantEid { get { return GetFieldInt("CLAIMANT_EID"); } set { SetField("CLAIMANT_EID", value); } }
        public double DimPercentNum { get { return GetFieldDouble("DIM_PERCENT_NUM"); } set { SetField("DIM_PERCENT_NUM", value); } }
        public string DimEvaluationDate { get { return GetFieldString("COV_EVALUATION_DATE"); } set { SetField("COV_EVALUATION_DATE", value); } }
        public string CustomerNum { get { return GetFieldString("CUSTOMER_NUM"); } set { SetField("CUSTOMER_NUM", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "POLICY_CLAIM_LOB")]
        public int PolicyLobCode { get { return GetFieldInt("POLICY_LOB_CODE"); } set { SetField("POLICY_LOB_CODE", value); } }
        public int PolicyXInslineGroupId { get { return GetFieldInt("POLICY_X_INSLINE_GROUP_ID"); } set { SetField("POLICY_X_INSLINE_GROUP_ID", value); } }
        public int EventId { get { return GetFieldInt("EVENT_ID"); } set { SetField("EVENT_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public int UnitStateRowId { get { return GetFieldInt("UNIT_STATE_ROW_ID"); } set { SetField("UNIT_STATE_ROW_ID", value); } }
        public bool IsExcludeExpenseFlagModified { get; set; }
        #endregion
        

        #region Child Implementation
        private string[,] sChildren = {
                                          {"ClaimXPolDedHistList", "ClaimXPolDedHistList"}
                                      };

        internal override void OnChildInit(string childName, string childType)
        {
            base.OnChildInit(childName, childType);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "ClaimXPolDedHistList":
                    (itemValue as ClaimXPolDedHist).ClmXPolDedId = this.ClmXPolDedId;
                    break;
            }
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }

        private void UpdateHistory()
        {
            ClaimXPolDedHist objHist = null;
            objHist = this.ClaimXPolDedHistList.LastStatusChange();

            if (objHist == null || objHist.DedTypeCode != this.DedTypeCode || objHist.ExcludeExpenseFlag != this.ExcludeExpenseFlag || objHist.SirDedAmt != this.SirDedAmt
                || objHist.DiminishingTypeCode != this.DiminishingTypeCode || objHist.DimPercentNum != this.DimPercentNum || objHist.ClaimantEid != this.ClaimantEid)
            {
                if (objHist == null || !objHist.IsNew)
                {
                    objHist = this.ClaimXPolDedHistList.AddNew();
                }
                objHist.ClmXPolDedId = this.ClmXPolDedId;
                objHist.DateDedctblChgd = Conversion.ToDbDateTime(DateTime.Now);
                objHist.DedctblChgdBy = this.UpdatedByUser;
                objHist.DedTypeCode = this.DedTypeCode;
                objHist.ExcludeExpenseFlag = this.ExcludeExpenseFlag;
                objHist.SirDedAmt = this.SirDedAmt;
                objHist.DiminishingTypeCode = this.DiminishingTypeCode;
                objHist.ClaimantEid = this.ClaimantEid;
                objHist.DimPercentNum = this.DimPercentNum;
            }
        }

        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "ClaimXPolDedHistList":
                    if (isNew)
                    {
                        //BSB 09.21.2004 Fix for query looking for "last change id" run against the list during save.
                        //Most child list items do not need their filter set before calling save.
                        (childValue as ClaimXPolDedHistList).SQLFilter = String.Format("{0}={1}", "CLM_X_POL_DED_ID", this.ClmXPolDedId);
                        foreach (ClaimXPolDedHist item in (childValue as ClaimXPolDedHistList))
                            item.ClmXPolDedId = this.ClmXPolDedId;
                        this.UpdateHistory();
                    }
                    (childValue as IPersistence).Save();
                    break;
            }
        }

        public ClaimXPolDedHistList ClaimXPolDedHistList
        {
            get
            {
                ClaimXPolDedHistList objList = base.m_Children["ClaimXPolDedHistList"] as ClaimXPolDedHistList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = String.Format("{0}={1}", "CLM_X_POL_DED_ID", this.ClmXPolDedId);
                return objList;
            }
        }
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            this.UpdateHistory();

            if (!CheckCallingStack("Riskmaster.BusinessAdaptor.ClaimGCForm") && !CheckCallingStack("Riskmaster.BusinessAdaptor.ClaimWCForm"))
            {
                if(this.FiringScriptFlag == 0)
                    this.FiringScriptFlag = 2;
            }
            base.Save();
        }

        internal ClaimXPolDed(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "CLAIM_X_POL_DED";
            this.m_sKeyField = "CLM_X_POL_DED_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);

            //Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);
            base.Initialize();

        }
        /// <summary>
        /// Added by Nitin goel, to update the exclude expense payment flag of other coverages sharing the same group.
        /// </summary>
        protected override void OnPostCommitSave()
        {
            base.OnPostCommitSave();
            //if (this.IsExcludeExpenseFlagModified) //Temporary commented.
            //{
            //this.Context.DbConnLookup.ExecuteNonQuery("UPDATE CLAIM_X_POL_DED SET EXCLUDE_EXPENSE_FLAG='"+ this.ExcludeExpenseFlag+"', DTTM_RCD_LAST_UPD='"+ this.DttmRcdLastUpdated+"',UPDATED_BY_USER= '"+this.UpdatedByUser +"' WHERE POLICY_X_INSLINE_GROUP_ID="+this.PolicyXInslineGroupId);
            //}

        }
        
        /// <summary>
        /// 
        /// </summary>
        public override IDataModel Parent
        {
            get
            {
                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("PolicyXCvgType", true);
                    m_isParentStale = false;
                    if (this.PolcvgRowId != 0)
                        (base.Parent as DataObject).MoveTo(this.PolcvgRowId);
                }
                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

        /// <summary>
        /// calculates remaining deductible amount. Use this method when Coverage Group Id is present (p_iCovGroupCode>0).
        /// </summary>
        /// <param name="p_dSirDedAmt">Deductible Per Event</param>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iCovGroupCode">Coverage Group Id</param>
        /// <param name="p_bSkipAggregateCalc">send true to skip Aggregate Calculation</param>
        /// <returns>Remaining Amount</returns>
        public double CalculateRemainingAmount(double p_dSirDedAmt, int p_iClaimId, int p_iCovGroupCode, bool p_bSkipAggregateCalc)
        {
            int iTPDedCode = 0;
            int iSirDedCode = 0;
            int iDedRecoveryReserveTypeCode = 0;
            double dDeductibleApplied = 0d;
            double dRemainingAmt = 0d;
            double dAggregateBalance = 0d;
            StringBuilder sbSQL = null;
            Claim objClaim = null;

            iTPDedCode = this.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
            iSirDedCode = this.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");

            objClaim = this.Context.Factory.GetDataModelObject("Claim", false) as Claim;
            objClaim.MoveTo(p_iClaimId);
            iDedRecoveryReserveTypeCode = this.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveType;

            sbSQL = new StringBuilder();
            //Start:added BY nitin goel, MITS 36319,05/09/2014
            //sbSQL.Append(" SELECT SUM(RESERVE_AMOUNT) - SUM(BALANCE_AMOUNT) AS DED_APPLIED_AMT ");
            sbSQL.Append(" SELECT SUM(COLLECTION_TOTAL) AS DED_APPLIED_AMT ");
            //end:added by nitin goel
            sbSQL.Append(" FROM RESERVE_CURRENT RC ");
            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID=RC.POLCVG_LOSS_ROW_ID ");
            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.CLAIM_ID = RC.CLAIM_ID AND CXD.POLCVG_ROW_ID =CXL.POLCVG_ROW_ID ");
            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
            sbSQL.Append(string.Format(" WHERE CXD.EVENT_ID={0} AND RC.RESERVE_TYPE_CODE={1} ", objClaim.EventId, iDedRecoveryReserveTypeCode));
            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(p_iCovGroupCode);
            sbSQL.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDedCode);
            sbSQL.Append(" OR CXD.DED_TYPE_CODE=").Append(iSirDedCode).Append(")");
            dDeductibleApplied = this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
            dRemainingAmt = p_dSirDedAmt - dDeductibleApplied;

            if (p_iCovGroupCode > 0 && !p_bSkipAggregateCalc)
            {
                dAggregateBalance = GetAgregateBalance(p_iCovGroupCode, objClaim.LineOfBusCode, out p_bSkipAggregateCalc);
                if (dRemainingAmt > dAggregateBalance && !p_bSkipAggregateCalc)
                {
                    dRemainingAmt = dAggregateBalance;
                }
            }

            if (dRemainingAmt < 0)
            {
                dRemainingAmt = 0d;
            }
            if (dRemainingAmt > p_dSirDedAmt)
            {
                dRemainingAmt = p_dSirDedAmt;
            }

            return dRemainingAmt;
        }


        /// <summary>
        /// calculates remaining deductible amount. Use this method when Coverage Group Id is not defined.
        /// </summary>
        /// <param name="p_dSirDedAmt">Reduced Deductible Amount</param>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iPolCvgRowId">PolCvg_Row_Id</param>
        /// <param name="p_iDedTypeCode">Deductible Type Code</param>
        /// <returns>Remaining Amount</returns>
        public double CalculateRemainingAmount(double p_dSirDedAmt, int p_iClaimId, int p_iPolCvgRowId, int p_iDedTypeCode)
        {
            int iFPDedCode = 0;
            int iTPDedCode = 0;
            int iSirDedCode = 0;
            int iDedRecoveryReserveTypeCode = 0;
            int iRecoveryMaster = 0;
            int iRcRowId = 0;
            int iLossType = 0;

            Claim objClaim = null;

            string sSqlPayment = string.Empty;
            string sSqlCollection = string.Empty;
            StringBuilder sbSQL = null;
            double dSumOfPayments = 0d;
            double dCollection = 0d;
            double dRemainingAmount = 0d;
            double dActualPayment = 0d;

            objClaim = this.Context.Factory.GetDataModelObject("Claim", false) as Claim;
            objClaim.MoveTo(p_iClaimId);

            iDedRecoveryReserveTypeCode = this.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveType;
            //start - Commented by Nikhil on 07/17/14 - Loss type setting has been remmoved
        //    iLossType = this.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveLossType;
            //end - Commented by Nikhil on 07/17/14 - Loss type setting has been remmoved
            //iRcRowId = GetRecoveryReserveId(p_iPolCvgRowId, iDedRecoveryReserveTypeCode, p_iClaimId, iLossType);
            iFPDedCode = this.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
            iTPDedCode = this.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
            iSirDedCode = this.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");
            sbSQL = new StringBuilder();
            //Start: added  by Nitin goel, Code is commented as a part to keep same POLCOVSEQ
            //sbSQL.Append(" SELECT SUM(F.AMOUNT) FROM FUNDS F ");
            //sbSQL.Append(" INNER JOIN FUNDS_X_DED_COL_MAP FXC ON F.TRANS_ID = FXC.DED_COL_TRANS_ID ");
            //sbSQL.Append(" WHERE (F.PAYMENT_FLAG = 0 OR F.PAYMENT_FLAG IS NULL) ");
            //sbSQL.Append(" AND (F.VOID_FLAG = 0 OR F.VOID_FLAG IS NULL) ");
            //sbSQL.Append(" AND F.COLLECTION_FLAG = -1 ");
            //sbSQL.Append(string.Format("AND FXC.CLAIM_ID={0} AND FXC.POLCVG_ROW_ID={1}", p_iClaimId, p_iPolCvgRowId));

            //dSumOfPayments = this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
            //dRemainingAmount = p_dSirDedAmt - dSumOfPayments;

            //if (p_iDedTypeCode == iTPDedCode || p_iDedTypeCode == iSirDedCode)
            //{
            //    if (iRcRowId == 0)
            //    {
            //        dRemainingAmount = p_dSirDedAmt;
            //    }
            //    else
            //    {
            //end: added by nitin goelCode is commented as a part to keep same POLCOVSEQ
                    sbSQL.Length = 0;
                    //Start:added BY nitin goel, MITS 36319,05/09/2014
                    //sbSQL.Append("SELECT BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE RC_ROW_ID=").Append(iRcRowId);
                    //double dRemainingAmtOfDed = this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    
                    //start:Commented by Nitin goel, to removed issue of POLCOVSEQ in POINT
                    //sbSQL.Append("SELECT COLLECTION_TOTAL FROM RESERVE_CURRENT WHERE RC_ROW_ID=").Append(iRcRowId);
                    sbSQL.Append(string.Format("SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT WHERE CLAIM_ID= {0} AND POLCVG_ROW_ID= {1} AND RESERVE_TYPE_CODE={2}", p_iClaimId,p_iPolCvgRowId,iDedRecoveryReserveTypeCode));
                    //end:added by nitin goel
                    
                    double dRemainingAmtOfDed = (p_dSirDedAmt) -(this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString()));
                    //end:added BY nitin goel, MITS 36319,05/09/2014
                    dRemainingAmount = dRemainingAmtOfDed;
               // }
                


                //sbSQL.Remove(0, sbSQL.Length);
                //sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                //sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                //sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                //sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                //sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                //sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                //sbSQL.Append(" INNER JOIN CODES C ON RC.RESERVE_TYPE_CODE = C.CODE_ID ");
                //sbSQL.Append(" WHERE (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG<>0) ");
                //sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(p_iClaimId);
                //sbSQL.Append(" AND CXD.POLCVG_ROW_ID = ").Append(p_iPolCvgRowId);
                //sbSQL.Append(" AND F.STATUS_CODE=" + this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));

                //dSumOfPayments = this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());

                //dActualPayment = dSumOfPayments - p_dSirDedAmt;

                
                
                //iRecoveryMaster = this.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE");

                //sSqlCollection = "(F.COLLECTION_FLAG IS NOT NULL AND F.COLLECTION_FLAG <>0 "
                //    + " AND F.STATUS_CODE=" + this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")
                //    + " AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryReserveTypeCode
                //    + " AND C.RELATED_CODE_ID =" + iRecoveryMaster + ")";
                //sbSQL.Remove(0, sbSQL.Length);
                //sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                //sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                //sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                //sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                //sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                //sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                //sbSQL.Append(" INNER JOIN CODES C ON RC.RESERVE_TYPE_CODE = C.CODE_ID ");
                //sbSQL.Append(" WHERE (" + sSqlCollection + ") ");
                //sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(p_iClaimId);
                //sbSQL.Append(" AND CXD.POLCVG_ROW_ID = ").Append(p_iPolCvgRowId);

                ////Collection will be negative. Multiply by -1 to make it positive
                //dCollection = -1 * this.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());

                //if (dCollection > dSumOfPayments)
                //{
                //    dRemainingAmount = p_dSirDedAmt;
                //}
                //else if (dCollection > dActualPayment)
                //{
                //    dRemainingAmount = dCollection - dActualPayment;
                //}

                
            //}

            if (dRemainingAmount < 0)
            {
                dRemainingAmount = 0d;
            }
            if (dRemainingAmount > p_dSirDedAmt)
            {
                dRemainingAmount = p_dSirDedAmt;
            }
            
            return dRemainingAmount;
        }
        //NI- PCR changes by Nikhil,added overload method CalculateRemainingAmount to handle condition when Coverage Group Id is present (p_iCovGroupCode>0) and Enable Deductible(Per Event) is not checked  - Start
        /// <summary>
        /// calculates remaining deductible amount. Use this method when Coverage Group Id is present (p_iCovGroupCode>0) and Enable Deductible(Per Event) is not checked .  Nikhil
        /// </summary>
        /// <param name="p_dSirDedAmt">Reduced Deductible Amount</param>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iPolCvgRowId">PolCvg_Row_Id</param>
        /// <param name="p_iDedTypeCode">Deductible Type Code</param>
        /// <param name="p_iCovGroupCode">Coverage Group Id</param>
        /// <param name="p_bSkipAggregateCalc">send true to skip Aggregate Calculation</param>
        /// <returns>Remaining Amount</returns>
        public double CalculateRemainingAmount(double p_dSirDedAmt, int p_iClaimId, int p_iPolCvgRowId, int p_iDedTypeCode, int p_iCovGroupCode, bool p_bSkipAggregateCalc)
        {

            double dRemainingAmount = 0d;

            dRemainingAmount = CalculateRemainingAmount(p_dSirDedAmt, p_iClaimId, p_iPolCvgRowId, p_iDedTypeCode);

            double dAggregateBalance = 0d;
            
            Claim objClaim = null;

            objClaim = this.Context.Factory.GetDataModelObject("Claim", false) as Claim;
            objClaim.MoveTo(p_iClaimId);
               

            if (p_iCovGroupCode > 0 && !p_bSkipAggregateCalc)
            {
                dAggregateBalance = GetAgregateBalance(p_iCovGroupCode, objClaim.LineOfBusCode, out p_bSkipAggregateCalc);
                if (dRemainingAmount > dAggregateBalance && !p_bSkipAggregateCalc)
                {
                    dRemainingAmount = dAggregateBalance;
                }
            }

            if (dRemainingAmount < 0)
            {
                dRemainingAmount = 0d;
            }
            if (dRemainingAmount > p_dSirDedAmt)
            {
                dRemainingAmount = p_dSirDedAmt;
            }

            return dRemainingAmount;
        }
        //NI- PCR changes by Nikhil,added overload method CalculateRemainingAmount to handle condition when Coverage Group Id is present (p_iCovGroupCode>0) and Enable Deductible(Per Event) is not checked  - End

        /// <summary>
        /// Calucates Reduced Deductible Amount
        /// </summary>
        /// <param name="iClaimId">ClaimId</param>
        /// <param name="iDiminishingTypeCode"></param>
        /// <param name="iDedTypeCode"></param>
        /// <param name="dSirDedAmount"></param>
        /// <param name="dDimPercent"></param>
        /// <param name="iClaimXPolDedId"></param>
        /// <returns></returns>
        public double CalculateReducedDeductibleAmount(int iClaimId, int iDiminishingTypeCode, int iDedTypeCode, double dSirDedAmount, double dDimPercent, int iClaimXPolDedId)
        {
            double dReducedAmt = 0d;
            
            string sSql = string.Empty;

            if (iDiminishingTypeCode == 0)
            {
                dReducedAmt = dSirDedAmount;
            }
            else if (iDedTypeCode != this.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
            {
                throw new Riskmaster.ExceptionTypes.RMAppException(Globalization.GetString("DiminishingDeductible.NonFirstParty.Error", this.Context.ClientId));
            }
            
            else
            {
                dReducedAmt = CalculateReducedDeductibleAmount(dSirDedAmount, dDimPercent);
            }

            return dReducedAmt;
        }

        /// <summary>
        /// Calucates Reduced Deductible Amount
        /// </summary>
        /// <param name="dSirDedAmt"></param>
        /// <param name="dDimPercent"></param>
        /// <returns></returns>
        public double CalculateReducedDeductibleAmount(double dSirDedAmt, double dDimPercent)
        {
            double dReducedAmt = 0d;
            dReducedAmt = dSirDedAmt * (100d - dDimPercent) / 100d;

            if (dReducedAmt < 0d)
            {
                dReducedAmt = 0d;
            }

            return dReducedAmt;
        }

        /// <summary>
        /// Gets Remaining Aggregate Balance
        /// </summary>
        /// <param name="p_iCovGroupId"></param>
        /// <param name="p_iLobCode"></param>
        /// <returns></returns>
        public double GetAgregateBalance(int p_iCovGroupId, int p_iLobCode, out bool p_bSkipAggCalc)
        {
            double dAggBalance = 0d;
            //double dDedAmt = 0d;
            //double dRemainingAmt = 0d;
            double dAggLimit = 0d;
            double dAppliedAmt = 0d;
            int iDedReserveTypeCode = 0;
            StringBuilder sbSql = null;
            bool bSuccess = false;
            int iTPDedCode = 0;
            int iSirDedCode = 0;

            dAggLimit = this.Context.DbConnLookup.ExecuteDouble("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + p_iCovGroupId);
            iTPDedCode = this.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
            iSirDedCode = this.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");
            //If AggLimit is 0 then Aggregate Calculation will not be applied.
            if (dAggLimit == 0d)
            {
                p_bSkipAggCalc = true;
                return dAggLimit;
            }
            else
            {
                p_bSkipAggCalc = false;
            }

            iDedReserveTypeCode = this.Context.InternalSettings.ColLobSettings[p_iLobCode].DedRecReserveType;
            sbSql = new StringBuilder();
            //Start:Added by Nitin goel, MITS 36319,05/09/2014
            //sbSql.Append(" SELECT SUM(RC.RESERVE_AMOUNT) AS DED_AMT, SUM(BALANCE_AMOUNT) AS DED_REMAINING_AMT ");
            sbSql.Append(" SELECT SUM(COLLECTION_TOTAL) AS DED_APPLIED_AMT ");
            //end:added by Nitin goel
            sbSql.Append(" FROM RESERVE_CURRENT RC ");
            sbSql.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID=RC.POLCVG_LOSS_ROW_ID ");
            sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON RC.POLCVG_ROW_ID = CXD.POLCVG_ROW_ID AND RC.CLAIM_ID = CXD.CLAIM_ID ");
            sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXG ON PXG.POLICY_X_INSLINE_GROUP_ID = CXD.POLICY_X_INSLINE_GROUP_ID ");
            sbSql.Append(" WHERE RC.RESERVE_TYPE_CODE = ").Append(iDedReserveTypeCode);
            sbSql.Append(" AND PXG.COV_GROUP_CODE = ").Append(p_iCovGroupId);
            sbSql.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDedCode);
            sbSql.Append(" OR CXD.DED_TYPE_CODE=").Append(iSirDedCode).Append(")");

           // using (DbReader oDbReader = this.Context.DbConnLookup.ExecuteReader(sbSql.ToString()))
            using (DbReader oDbReader = DbFactory.ExecuteReader(this.Context.DbConnLookup.ConnectionString, sbSql.ToString()))
            {
                if (oDbReader.Read())
                {
                    //if (oDbReader["DED_AMT"] != DBNull.Value && oDbReader["DED_AMT"] != null)
                    //{
                    //    dDedAmt = Conversion.CastToType<double>(oDbReader["DED_AMT"].ToString(), out bSuccess);
                    //}
                    //if (oDbReader["DED_REMAINING_AMT"] != DBNull.Value && oDbReader["DED_REMAINING_AMT"] != null)
                    //{
                    //    dRemainingAmt = Conversion.CastToType<double>(oDbReader["DED_REMAINING_AMT"].ToString(), out bSuccess);
                    //}
                    if (oDbReader["DED_APPLIED_AMT"] != DBNull.Value && oDbReader["DED_APPLIED_AMT"] != null) 
                    {                        
                        dAppliedAmt = Conversion.CastToType<double>(oDbReader["DED_APPLIED_AMT"].ToString(), out bSuccess);
                    }
                }
            }

           // dAggBalance = dAggLimit - (dDedAmt - dRemainingAmt);
            dAggBalance = dAggLimit - (dAppliedAmt);
            return dAggBalance;
        }

        private int GetRecoveryReserveId(int iPolCvgRowId, int iRecoveryReserveType, int iClaimId, int iLossTypeId)
        {
            int iCvgLossCode = 0;
            string sSql = string.Empty;

            sSql = "SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE " +
                string.Format("POLCVG_ROW_ID={0} AND LOSS_CODE={1}", iPolCvgRowId, iLossTypeId);
            iCvgLossCode = this.Context.DbConnLookup.ExecuteInt(sSql.ToString());

            sSql = "SELECT RC_ROW_ID FROM RESERVE_CURRENT " +
            string.Format("WHERE POLCVG_LOSS_ROW_ID={0} AND RESERVE_TYPE_CODE={1} AND CLAIM_ID={2}", iCvgLossCode, iRecoveryReserveType, iClaimId);


            return this.Context.DbConnLookup.ExecuteInt(sSql.ToString());
        }
        public static bool CheckCallingStack(string Key)
        {
            try
            {
                StackTrace stackTrace = new StackTrace();
                StackFrame[] frames = stackTrace.GetFrames();

                foreach (StackFrame frame in frames)
                {
                    if (frame.GetMethod().DeclaringType.FullName.Contains(Key))
                        return true;
                }
                //var script = frames.FirstOrDefault(s => s.GetMethod().DeclaringType.Name.ToString().Contains(Key));
                //return script.GetMethod().DeclaringType.Name.Contains(Key);
                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
