using System;
using System.Collections.Generic;
using Riskmaster.Common;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EventXDatedText.
	/// </summary>
	[Riskmaster.DataModel.Summary("EVENT_X_INTERVENT","INTERVENT_ROW_ID")]
	public class EventXIntervention : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"EvIntRowId","INTERVENT_ROW_ID"},
										{"EventId","EVENT_ID"},
										{"EmpEid","EMPLOYEE_EID"},
										{"SupEid","SUPERVISOR_EID"},
										{"DeptEid","DEPT_ASSIGNED_EID"},
										{"Status","STATUS_CODE"},
										{"EvalEid","EVALUATOR_EID"},
										{"DtRec","DATE_RECEIVED"},
										{"RefCode","REFERRAL_CODE"},
										{"CompCode","COMPLAINT_CODE"},
										{"DateComplete","DATE_COMPLETED"},
										{"AntiCost","ANTICIPATED_COST"},
										{"DateSent","DATE_SENT"},
										{"DateResp","DATE_RESPONSE"},
										{"NonComp","NONCOMPLIANCE_CODE"},
										{"NoHours","NO_HOURS"},
										{"Rate","RATE"},
										{"TotalFlatAmt","TOTAL_FLAT_AMT"},
										{"InvDesc","INTERVENT_DESC"},
										{"DttmRcdAdded","DTTM_RCD_ADDED"},
										{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
										{"UpdatedByUser","UPDATED_BY_USER"},
										{"AddedByUser","ADDED_BY_USER"}
									 };
		public int  EvIntRowId{	get{  return GetFieldInt("INTERVENT_ROW_ID");}	set{ SetField("INTERVENT_ROW_ID",value);}}
		public int  EventId{get{  return   GetFieldInt("EVENT_ID");}set{ SetField("EVENT_ID",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"EMPLOYEE")]
		public int  EmpEid{get{  return   GetFieldInt("EMPLOYEE_EID");}set{ SetField("EMPLOYEE_EID",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"ANY")]
        public int SupEid { get { return GetFieldInt("SUPERVISOR_EID"); } set { SetFieldAndNavTo("SUPERVISOR_EID", value, "SupervisorEntity"); } }
		[ExtendedTypeAttribute(RMExtType.OrgH,"DEPARTMENT")]
		public int  DeptEid{get{  return   GetFieldInt("DEPT_ASSIGNED_EID");}set{ SetField("DEPT_ASSIGNED_EID",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"INTERVENT_STATUS")]
		public int  Status{get{  return   GetFieldInt("STATUS_CODE");}set{ SetField("STATUS_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"EVALUATOR")]
		public int  EvalEid{get{  return   GetFieldInt("EVALUATOR_EID");}set{ SetField("EVALUATOR_EID",value);}}
		public string  DtRec{get{  return   GetFieldString("DATE_RECEIVED");}set{SetField("DATE_RECEIVED",Riskmaster.Common.Conversion.GetDate(value));}}
		[ExtendedTypeAttribute(RMExtType.Code,"REFERRAL_SOURCE")]
		public int  RefCode{get{  return   GetFieldInt("REFERRAL_CODE");}set{ SetField("REFERRAL_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"COMPLAINT_CODE")]
		public int  CompCode{get{  return   GetFieldInt("COMPLAINT_CODE");}set{ SetField("COMPLAINT_CODE",value);}}
		public string  DateComplete{get{  return   GetFieldString("DATE_COMPLETED");}set{ SetField("DATE_COMPLETED",Riskmaster.Common.Conversion.GetDate(value));}}
		public Double  AntiCost{get{  return   GetFieldDouble("ANTICIPATED_COST");}set{ SetField("ANTICIPATED_COST",value);}}
		public string  DateSent{get{  return   GetFieldString("DATE_SENT");}set{ SetField("DATE_SENT",Riskmaster.Common.Conversion.GetDate(value));}}
		public string  DateResp{get{  return   GetFieldString("DATE_RESPONSE");}set{ SetField("DATE_RESPONSE",Riskmaster.Common.Conversion.GetDate(value));}}
		[ExtendedTypeAttribute(RMExtType.Code,"NONCOMPLIANCE_CODE")]
		public int  NonComp{get{  return   GetFieldInt("NONCOMPLIANCE_CODE");}set{ SetField("NONCOMPLIANCE_CODE",value);}}
		public double  NoHours{get{  return   GetFieldDouble("NO_HOURS");}set{ SetField("NO_HOURS",value);}}
		public double  Rate{get{  return   GetFieldDouble("RATE");}set{ SetField("RATE",value);}}
		public double  TotalFlatAmt{get{  return   GetFieldDouble("TOTAL_FLAT_AMT");}set{ SetField("TOTAL_FLAT_AMT",value);}}
		public string  InvDesc{get{  return   GetFieldString("INTERVENT_DESC");}set{ SetField("INTERVENT_DESC",value);}}
		public string DttmRcdAdded{get{ return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{ return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{ return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string AddedByUser{get{ return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		#endregion
		internal EventXIntervention(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		private string[,] sChildren = {{"EventXIntCodes","DataSimpleList"},
                                      {"SupervisorEntity","Entity"}};
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "EVENT_X_INTERVENT";
			this.m_sKeyField = "INTERVENT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			base.InitChildren(sChildren);
			this.m_sParentClassName = "Event";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
        //dbisht6 Entity Role
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);

            this.SetFieldAndNavTo("SUPERVISOR_EID", this.SupEid, "SupervisorEntity", true);
        
        }

        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            int intTableID;
            Dictionary<string, int> objReturnVal = new Dictionary<string, int>();
            
            //dbisht6 Entity Role
            if (childName == "SupervisorEntity")
            {
                if (!int.Equals(SupEid, 0))
                {
                    Entity objEntity = ((childValue as Entity));
                    intTableID = Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.EMPLOYEES.ToString());
                    objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                    if (objReturnVal != null)
                    {
                        this.m_Fields["SUPERVISOR_EID"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
                        this.SupEid = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
                    }
                }
                else
                    this.SupEid = 0;
            }
            objReturnVal = null;
            //dbisht6 End
        }

        public Entity SupervisorEntity
        {
            get
            {
                Entity objItem = base.m_Children["SupervisorEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("SupervisorEntity", (objItem as DataObject), this.SupEid) as Entity;
                return objItem;
            }
        }
        

        //dbisht6 end
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			//Some of these children are 1 to {0|1} so we'll check and see if the 
			// child actually was touched before forcing it to save.
			bool bIsNewAndModified = isNew && childValue.DataChanged;
			switch(childName)
			{
				case "EventXIntCodes": 
					if(isNew)
						(childValue as DataSimpleList).SetKeyValue(this.EventId);
					(childValue as DataSimpleList).SaveSimpleList();
					break;
			}
		}
		[ExtendedTypeAttribute(RMExtType.CodeList,"INTERVENTION_CODE")]
		public DataSimpleList EventXIntCodes
		{
			get
			{
				DataSimpleList objList = base.m_Children["EventXIntCodes"] as DataSimpleList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.LoadSimpleList((int)this.KeyFieldValue);
				return objList;
			}
		}
		override internal  void OnChildInit(string childName, string childType)
		{
            Entity objEnt=null;
            //Do default per-child processing.
			base.OnChildInit(childName, childType);
			
			//Do any custom per-child processing.
			object obj = base.m_Children[childName];
			switch(childType)
			{
				case "DataSimpleList":
					DataSimpleList objList =  obj as DataSimpleList;
				switch(childName)
				{
					case "EventXIntCodes":
						objList.Initialize("INTERVENTION_CODE", "EVENT_X_INT_CODES","INTERVENT_ROW_ID");
						break;
                    
                    
                }
					break;
                //dbisht6 Entity Role
                case "Entity":

                    switch (childName)
                    {
                        case "SupervisorEntity":

                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                    }
                    break;
                //dbisht6 end
				default: 
					//No default process.
					break;
			}

		}


		#region Supplemental Fields Exposed
		internal override string OnBuildSuppTableName()
		{
			return "EVENT_X_INT_SUPP";
		}

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
