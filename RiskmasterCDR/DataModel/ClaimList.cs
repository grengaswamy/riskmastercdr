using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClaimList.
	/// </summary>
	public class ClaimList : DataCollection
	{
		internal ClaimList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CLAIM_ID";
			this.SQLFromTable =	"CLAIM";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Claim";
		}
		public new Claim this[int keyValue]{get{return base[keyValue] as Claim;}}
		public new Claim AddNew(){return base.AddNew() as Claim;}
		public  Claim Add(Claim obj){return base.Add(obj) as Claim;}
		public new Claim Add(int keyValue){return base.Add(keyValue) as Claim;}
		
		//BSB 05.30.2006 Replace\Set Claim object
		internal void SetClaimObject(int keyValue, Claim objClaim){base.m_keySet[keyValue]=objClaim;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}