﻿using System;

namespace Riskmaster.DataModel
{
    public class PolicyXUnitListStgng:DataCollection 
    {
        internal PolicyXUnitListStgng(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "POLICY_UNIT_ROW_ID";
            this.SQLFromTable = "POLICY_X_UNIT_STGNG";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "PolicyXUnitStgng";
		}
        public new PolicyXUnitStgng this[int keyValue] { get { return base[keyValue] as PolicyXUnitStgng; } }
        public new PolicyXUnitStgng AddNew() { return base.AddNew() as PolicyXUnitStgng; }
        public PolicyXUnitStgng Add(UnitXClaim obj) { return base.Add(obj) as PolicyXUnitStgng; }
        public new PolicyXUnitStgng Add(int keyValue) { return base.Add(keyValue) as PolicyXUnitStgng; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
