﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_INSURER_STGNG", "ROW_ID")]
    public class PolicyXInsurerStgng:DataObject
    {
        public override string Default
        {
            get
            {
                if (this.InsurerCode > 0)
                    return Context.LocalCache.GetEntityLastFirstName(this.InsurerCode);
                else
                    return "";
            }
        }

        #region Database Field List
        private string[,] sFields = {
                                                        {"RowID", "ROW_ID"},
														{"PolicyId", "POLICY_ID"},
                                                        {"InsurerCode", "INSURER_CODE"},
														{"ResPercentage", "RES_PERCENTAGE"},
                                                        {"PreAmount", "PREMIUM_AMOUNT"}, //added by Navdeep
                                                        {"OccuranceLimit", "OCCURENCE_LIMIT"}
                                    };

        public int RowID { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "INSURERS")]
        public int InsurerCode { get { return GetFieldInt("INSURER_CODE"); } set { SetFieldAndNavTo("INSURER_CODE", value, "InsurerEntity"); } }
        public double ResPercentage { get { return GetFieldDouble("RES_PERCENTAGE"); } set { SetField("RES_PERCENTAGE", value); } }
        public double PreAmount { get { return GetFieldDouble("PREMIUM_AMOUNT"); } set { SetField("PREMIUM_AMOUNT", value); } }
        public double OccuranceLimit { get { return GetFieldDouble("OCCURENCE_LIMIT"); } set { SetField("OCCURENCE_LIMIT", value); } }
        #endregion

        #region Child Implementation
        private string[,] sChildren = { { "InsurerEntityStgng", "EntityStgng" } };//,
                                      //{"PolicyXReInsurerListStgng", "PolicyXReInsurerListStgng"}};

		// Lock Entity Tables
		// Also Initialize SimpleList with desired table, field details.
//		override internal  void OnChildInit(string childName, string childType)
//		{
//			Entity objEnt =  null;
//			//Do default per-child processing.
//			base.OnChildInit(childName, childType);
//			
//			//Do any custom per-child processing.
//			object obj = base.m_Children[childName];
//			switch(childType)
//			{
//				case "Entity": 
//				switch(childName)
//				{
//					case "InsurerEntity":
//						objEnt = (obj as Entity);
//						objEnt.EntityTableId= Context.LocalCache.GetTableId("INSURERS");
//						objEnt.LockEntityTableChange = true;
//						objEnt.DataChanged=false;
//						//no real way to lock entity class navigation from internal access.
//						break;
//				}
//					break;
//			}
//		}
						
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);
			//BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
			this.SetFieldAndNavTo("INSURER_CODE",this.InsurerCode,"InsurerEntityStgng",true);
		}

		//Handle any sub-object(s) for which we need the key in our own table.
		internal override void OnChildPreSave(string childName, IDataModel childValue)
		{
			// If Entity is involved we save this first (in case we need the EntityId.)
            if (childName == "InsurerEntityStgng")
			{
				(childValue as IPersistence).Save();
				this.m_Fields["INSURER_CODE"]= (childValue as EntityStgng).EntityId;
			}
		}
        
        //NAVDEEP
        //Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			switch(childName)
			{
                //case "PolicyXReInsured":
                //    if(isNew)
                //        (childValue as DataSimpleList).SetKeyValue(this.InRowID);
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;

                //case "PolicyXReInsurerList":
                //    if (isNew)
                //        foreach (PolicyXReInsurer item in (childValue as PolicyXReInsurerList))
                //            item.InRowID = this.RowID;
                //    (childValue as IPersistence).Save();
                //    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                //case "PolicyXReInsurerList":
                //    (itemValue as PolicyXReInsurer).InRowID = this.RowID;
                //    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }					

		//Child Property Accessors
		public EntityStgng InsurerEntityStgng
		{
			get
			{
                EntityStgng objItem = base.m_Children["InsurerEntityStgng"] as EntityStgng;
				if((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("InsurerEntityStgng", (objItem as DataObject), this.InsurerCode) as EntityStgng;
				return objItem;
			}
		}		
		#endregion

        protected override void OnPreCommitSave()
        {           
            base.OnPreCommitSave();
        }

        // Added by NAVDEEP - Multiple RE Insurers
        //public PolicyXReInsurerList PolicyXReInsurerList
        //{
        //    get
        //    {
        //        PolicyXReInsurerList objList = base.m_Children["PolicyXReInsurerList"] as PolicyXReInsurerList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //           objList.SQLFilter = "POL_X_INS_ROW_ID =" + this.KeyFieldValue;
        //        return objList;
        //    }
        //}

		internal PolicyXInsurerStgng(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "POLICY_X_INSURER_STGNG";
			this.m_sKeyField = "ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "PolicyStgng";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
		}
        //Nadim-Supplementals where not working on policyInsurer screen-start
        //#region Supplemental Fields Exposed
        ////Supplementals are implemented in the base class but not exposed externally.
        ////This allows the derived class to control whether the object appears to have
        ////supplementals or not. 
        ////Entity exposes Supplementals.
        //public new Supplementals Supplementals
        //{
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}

        
        //#endregion
        //Nadim-overload written because supplemental table name was different from standard(tablename_supp)
        internal override string OnBuildSuppTableName()
        {
            return "POLICY_X_INSR_SUPP";
        }
    }
}
