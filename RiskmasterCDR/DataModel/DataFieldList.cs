using System;
using System.Collections;
using System.Text;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// DataFieldList contains the list of fields and their values for a single DataObject.
	/// </summary>
	internal class DataFieldList : IEnumerable 
	{
		protected System.Collections.Hashtable m_Fields=null;

		public DataFieldList()
		{
			m_Fields = new System.Collections.Hashtable(StringComparer.OrdinalIgnoreCase);
		}
		public object this[string PropertyName]
		{
			get{	return m_Fields[PropertyName]; }
			set
			{
				if(!m_Fields.ContainsKey(PropertyName))
					m_Fields.Add(PropertyName,value);
				else
					m_Fields[PropertyName] = value;
			}
		}

		//Reset all field values to defaults.
		public void Clear()
		{ 
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = new object[m_Fields.Keys.Count];
			m_Fields.Keys.CopyTo(arr,0);
			
			for(int i=0; i< arr.Length;i++)
				m_Fields[arr[i]] = "";
		}
		
		public void Empty()
		{
            m_Fields = new System.Collections.Hashtable(StringComparer.OrdinalIgnoreCase);
		}		
		public int Count()
		{
			return m_Fields.Count;
		}
		
		internal ICollection Keys{get{return m_Fields.Keys;}}
		internal object[] GetKeyArray()
		{
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = new object[m_Fields.Keys.Count];
			m_Fields.Keys.CopyTo(arr,0);
			return arr;
		}
		public System.Collections.IEnumerator GetEnumerator()
		{return m_Fields.Keys.GetEnumerator();}
		
		public bool ContainsField(string fieldName)
		{
			return m_Fields.ContainsKey(fieldName);
		}
		/// <summary>
		/// ToString is overridden to provide a comma separated list of field names suitable for use in SQL queries.
		/// </summary>
		/// <returns></returns>
		override public string ToString()
		{
            //rsolanki2: replacing with stringbuilder
            StringBuilder sb = new StringBuilder();
            //string ret="";
            foreach (string s in m_Fields.Keys)
            {
                //ret += s.ToUpper() + ",";
                //ret += s.ToUpper() + ",";
                sb.Append(s.ToUpper()).Append(",");
            }

            //return ret.TrimEnd(',');
            return sb.ToString().TrimEnd(',');

		}	
	}

}
