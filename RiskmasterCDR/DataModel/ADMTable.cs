using System;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Scripting;
using System.Xml;
using System.Collections.Generic;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for Supplementals.
	/// </summary>
	public class ADMTable :DataRoot , IDataModel, IPersistence, INavigation, IEnumerable
	{
		private string m_ViewXml="";
		private bool m_isNew = true;
		private string m_TableName = "";
		private string m_TableUserName = "";
		private int m_TableId = 0;
		private int m_LangId = 1033;  //TODO Could switch this value during init based on current culture?
		private bool m_DefinitionLoaded = false;
		private string m_sFilterClause="";
		private DataChildList  m_SuppFields = new DataChildList();
        //Start by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
        private bool m_PowerViewUpgrade = false;
        //End by Shivendu
        //MITS 11296 -Abhishek Added for checking number of records returned
        private int m_RecordCount = -1;
        /// <summary>
        /// Raise an event to call custom script Init function
        /// MITS 12028
        /// </summary>
        override public void InitializeScriptData()
        {
            this.InitData(this, new InitDataEventArgs());
        }

		//DEFAULT EVENT HANDLERS  (customize above to spawn scripts if desired....)
		private void InitObjDefaultHandler(object sender, InitObjEventArgs e)
		{
		}
        private void InitDataDefaultHandler(object sender, InitDataEventArgs e)
        {
            //MITS 12028 Call the script only if the action is from AdminTracking screen
            if (this.FiringScriptFlag != 0)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_INITIALIZE, sender);
            }
        }
		private bool CalculateDataDefaultHandler(object sender, CalculateDataEventArgs e)
		{
            //MITS 12028 Call the script only if the action is from AdminTracking screen
            if (this.FiringScriptFlag == 2)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_CALCULATE, sender);
                this.FiringScriptFlag++;
            }
            return (Context.ScriptValidationErrors.Count == 0);
		}
		private bool ValidateDefaultHandler(object sender, ValidateEventArgs e)
		{
            //MITS 12028 Call the script only if the action is from AdminTracking screen
            if (this.FiringScriptFlag == 3)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_VALIDATE, sender);
                this.FiringScriptFlag++;
            }
			return (Context.ScriptValidationErrors.Count == 0);
		}
		private bool BeforeSaveDefaultHandler(object sender, BeforeSaveEventArgs e)
		{
            //MITS 12028 Call the script only if the action is from AdminTracking screen
            if (this.FiringScriptFlag == 4)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_BEFORESAVE, sender);
                this.FiringScriptFlag++;
            }
            return (Context.ScriptValidationErrors.Count == 0);
		}
		private void AfterSaveDefaultHandler(object sender, AfterSaveEventArgs e)
		{
            //MITS 12028 Call the script only if the action is from AdminTracking screen
            if (this.FiringScriptFlag == 5)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_AFTERSAVE, sender);
            }
		}
		internal  ADMTable(bool isLocked, Context context) : base(isLocked, context){Initialize();}
		private void Initialize()
		{
			//Add the default Event Handlers
			InitObj+=  new InitObjEventHandler(InitObjDefaultHandler);
            InitData += new InitDataEventHandler(InitDataDefaultHandler);
			CalculateData+= new CalculateDataEventHandler(CalculateDataDefaultHandler);
			ValidateData+= new ValidateEventHandler(ValidateDefaultHandler);
			BeforeSave+=new BeforeSaveEventHandler( BeforeSaveDefaultHandler);
			AfterSave+=new AfterSaveEventHandler(AfterSaveDefaultHandler);
		}

		public  void Clear()
		{
			this.Clear(true);
		}
		internal void Clear(bool clearKey)
		{
			foreach(SupplementalField objSupp in this)
				if(clearKey || !objSupp.IsKeyValue)
				{
					objSupp.Value = null;
                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // Clear the values if the Supplemental is of type user lookup as well
					if(	objSupp.IsMultiValue || objSupp.IsUserLookup)
						objSupp.Values.ClearAll();
				}
		}

        //MITS 11296 -Abhishek Added for checking number of records returned
        /// <summary>
        /// Number of records returned for searched record id
        /// </summary>
        public int RecordCount
        {
            get { return m_RecordCount; }
        }

		public int KeyValue
		{
			get
			{
				try
				{
					foreach(SupplementalField objSupp in this)
						if(objSupp.FieldType ==SupplementalFieldTypes.SuppTypePrimaryKey)
                            return Conversion.ConvertObjToInt(objSupp.Value, this.Context.ClientId);
					
				}
				catch(Exception e){throw new DataModelException(Globalization.GetString("ADMTable.KeyValue.Get.Exception",this.Context.ClientId),e);}
                throw new DataModelException(Globalization.GetString("ADMTable.KeyValue.Get.Exception.NoPrimaryKeyFieldFound", this.Context.ClientId));
			}
			set
			{
				try
				{
					foreach(string key in m_SuppFields)
						if((m_SuppFields[key] as SupplementalField).FieldType ==SupplementalFieldTypes.SuppTypePrimaryKey)
							(m_SuppFields[key] as SupplementalField).Value = value;
					return;
				}
				catch(Exception e){throw new DataModelException(Globalization.GetString("ADMTable.KeyValue.Set.Exception",this.Context.ClientId),e);}
                
				//				throw new DataModelException(Globalization.GetString("ADMTable.KeyValue.Set.Exception.NoPrimaryKeyFieldFound"));
			}

		}
		public string KeyName
		{
			get
			{
				foreach(SupplementalField objSupp in this)
					if(objSupp.IsKeyValue)
						return objSupp.FieldName;
                throw new DataModelException(Globalization.GetString("ADMTable.KeyName.get.Exception.NoKeyFieldFound", this.Context.ClientId));
			}
		}
		public string TableName
		{
			get{return m_TableName;}
			set
			{
				if (value.ToUpper() != m_TableName)
				{
					m_TableName = value.ToUpper();
					LoadTableDefinition();
				}
				if(!DefinitionLoaded)
					LoadTableDefinition();
			}
		}
        //Start by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool

        public bool PowerViewUpgrade
        {
            get { return m_PowerViewUpgrade; }

            set
            {
                m_PowerViewUpgrade = value;
            }
        }
        //End by Shivendu
		public int TableId
		{
			get{return m_TableId;}
			set
			{
				if (value != m_TableId)
				{
					m_TableId = value;
					//					LoadTableDefinition(); // Only reload based on Table Name.
				}
			}
		}		

		public string TableUserName{get{return m_TableUserName;}}
		internal void SetTableUserName(string Value)
		{
			if (Value.ToUpper() != m_TableUserName.ToUpper())
			{
				m_TableUserName = Value;
				this.DataChanged = true;
			}
		}

		public bool DefinitionLoaded{get{return m_DefinitionLoaded;}}
		private void LoadTableDefinition()
		{
			m_isNew = true;
			string SQL = "";
			SupplementalField objSupp = null;
			DbReader objReader =null;
			try
			{
				m_SuppFields = new DataChildList();
				SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 ORDER BY SEQ_NUM",this.TableName);
				objReader = Context.DbConnLookup.ExecuteReader(SQL);
				while(objReader.Read())
				{
					objSupp = Context.Factory.GetDataModelObject("SupplementalField",true) as SupplementalField;
					//Load Field Meta-Info
					objSupp.DesignMode = true;
					//					objSupp.AssocField = objReader.GetString("ASSOC_FIELD").ToUpper();
					objSupp.Caption= objReader.GetString("USER_PROMPT");
					objSupp.CodeFileId= objReader.GetInt("CODE_FILE_ID");
					objSupp.SetFieldId(objReader.GetInt("FIELD_ID"));
					objSupp.FieldName= objReader.GetString("SYS_FIELD_NAME").ToUpper();
					objSupp.FieldSize = objReader.GetInt("FIELD_SIZE");
					objSupp.FieldType= (SupplementalFieldTypes)objReader.GetInt("FIELD_TYPE");
					objSupp.Format= objReader.GetString("PATTERN");
					objSupp.Lookup= ((int)objReader.GetInt16("LOOKUP_FLAG") !=0);
					objSupp.Required= ((int)objReader.GetInt16("REQUIRED_FLAG") !=0);
					//					objSupp.SetGrpAssocFlag(((int)objReader.GetInt16("GRP_ASSOC_FLAG") != 0));
					objSupp.NetVisibleFlag= ((int)objReader.GetInt16("NETVISIBLE_FLAG") != 0);
					objSupp.SeqNum = (int)objReader.GetInt("SEQ_NUM");
                    objSupp.WebLinkId = objReader.GetInt("WEB_LINK_ID"); //WWIG GAP20A - agupta298 - MITS 36804 - JIRA-4691
                    // npadhy Jira 6415
                    // Handle the controls in Admin Tracking as well
                    objSupp.Multiselect = objReader.GetInt16("MULTISELECT");

                    if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeUserLookup)
                    {
                        // npadhy Jira 6415 Retrieve the UserGroup for the selected Field id
                        SQL = String.Format("SELECT USER_GROUP_IND FROM SUPP_USERGROUP_DICT WHERE FIELD_ID = {0}", objSupp.FieldId);
                        objSupp.UserGroups = Conversion.ConvertObjToInt(Context.DbConn.ExecuteScalar(SQL), this.Context.ClientId);
                    }

                    //npadhy Jira 6415 ends

					objSupp.DesignMode = false;
					objSupp.DataChanged = false;
					if(	!objSupp.IsKeyValue)
						objSupp.Visible = true;
					
					//Toss into the Collection
					this.Add(objSupp);
				}											//End while more fields.
				objReader.Close();

				//Handle loading Table Meta-Info.
				SQL = String.Format(@" SELECT GLOSSARY.TABLE_ID, GLOSSARY_TEXT.TABLE_NAME 
																FROM GLOSSARY, GLOSSARY_TEXT 
																WHERE GLOSSARY.SYSTEM_TABLE_NAME='{0}' AND 
																GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID AND
																GLOSSARY_TEXT.LANGUAGE_CODE = {1}",this.TableName, this.m_LangId);
																
				objReader = Context.DbConnLookup.ExecuteReader(SQL);
				if(objReader.Read())
				{
					m_TableUserName = objReader.GetString("TABLE_NAME");
					m_TableId = objReader.GetInt("TABLE_ID");
				}
				objReader.Close();
				m_ViewXml = "";
				m_DefinitionLoaded = true;
                //If added by Shivendu to avoid scripting processing 
                //when LoadTableDefinition is called from PowerviewUpgrade tool
                if (!this.m_PowerViewUpgrade)
                {
                    InitObj(this, new InitObjEventArgs());
                }
			}
            catch (Exception e) { throw new DataModelException(Globalization.GetString("ADM.LoadTableDefinition.Exception", this.Context.ClientId), e); }
			finally
			{
				if (objReader!=null)
					objReader.Close();
				objReader = null;
			}

		}
		public void Load()
		{
			string SQL = "";
			string sSelect = "";
			string sWhere = "";
			int recordId=0;
			this.m_isNew =true;
			DbReader objReader = null;

			try
			{
				//Clear All
				this.Clear(false);
				
				//Create Data Query
				foreach(SupplementalField objSupp in this)
				{
					if(	objSupp.IsKeyValue)
					{
						if(sWhere != "")
							sWhere += " AND ";
						sWhere = String.Format("{0} {1} = {2}",sWhere,objSupp.FieldName, objSupp.Value);
					}
					else
					{
						if(sSelect !="")
							sSelect += ",";
						sSelect +=objSupp.FieldName;
					}
				}
				
				//Check for any Supp fields defined.
				if (sSelect == "")
					return;

				SQL = String.Format("SELECT * FROM {0} WHERE {1}",this.TableName,sWhere);
				objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString,SQL);
				//objReader = Context.DbConn.ExecuteReader(SQL);
                m_RecordCount = 0;
                while(objReader.Read())
				{
					this.m_isNew =false;
                    //MITS 11296 -Abhishek Added for checking number of records returned
                    m_RecordCount = m_RecordCount + 1;

                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // Do not load the values in this loop, if the Supp type is User lookup. We will handle User lookup differently
					foreach(SupplementalField objSupp in this)
						if(! (objSupp.IsKeyValue || objSupp.IsMultiValue || objSupp.IsUserLookup))
						{
                            try
                            {
                                objSupp.Value = objReader.GetValue(objSupp.FieldName);
                                objSupp.Visible = true;
                            }
                            catch{};
						}
				}
				objReader.Close();

				//Handle Multi-Fields
				recordId = this.KeyValue;
                foreach (SupplementalField objSupp in this)
                {
                    if (objSupp.IsMultiValue || objSupp.IsUserLookup)
                        if (recordId != 0)
                            objSupp.Values.LoadSimpleList(String.Format("SELECT CODE_ID FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}", objSupp.FieldId, recordId));
                }

                //-----------------------
                //Start by Shivendu for supplemental grid
                recordId = this.KeyValue;
                foreach (SupplementalField objSupp in this)
                {
                    
                        if (objSupp.FieldType == (SupplementalFieldTypes)17)
                        {

                            SQL = "SELECT * FROM GRID_SYS_PARMS";
                            //zmohammad MITS 33695 Start : Fix for special characters
                            using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                            {
                                while (objRdr.Read())
                                {
                                    objSupp.minRows = objRdr["MIN_ROWS"].ToString();
                                    objSupp.maxRows = objRdr["MAX_ROWS"].ToString();

                                }
                            }
                            if (recordId != 0)
                            {

                                int colCount = 0;
                                //Raman: Changing Location of columns.xml from bin to Appfiles/Supp - 02/04/2009
                                string path = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                                //string path = AppDomain.CurrentDomain.BaseDirectory + "bin/columns.xml";

                                XmlElement objXMLElem = null;
                                XmlElement objXMLTitleElement = null;
                                XmlElement objXMLCellElement = null;
                                XmlElement objXMLRowElement = null;
                                XmlNode objXMLFieldsNode = null;
                                XmlNode objXMLRowsNode = null;
                                XmlNode objXMLRowNode = null;
                                bool bRow = false;
                                bool flag = true;
                                objSupp.gridXML = new XmlDocument();
                                objSupp.gridXML.Load(path);
                                SQL = string.Concat("SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '"
                                    , objSupp.FieldId
                                    , "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC");
                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    while (objRdr.Read())
                                    {
                                        colCount++;
                                        objXMLElem = objSupp.gridXML.CreateElement("field");
                                        objXMLTitleElement = objSupp.gridXML.CreateElement("title");
                                        objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();

                                        objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");

                                        objXMLElem.AppendChild(objXMLTitleElement);
                                        objXMLFieldsNode = objSupp.gridXML.SelectSingleNode("/grid/table/fields");
                                        objXMLFieldsNode.AppendChild(objXMLElem);
                                    }
                                }
                                SQL = "SELECT * FROM SUPP_GRID_VALUE  WHERE FIELD_ID = '"
                                    + objSupp.FieldId
                                    + "' AND RECORD_ID = '"
                                    + recordId
                                    + "' ORDER BY ROW_NUM,COL_NUM";


                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    objXMLRowNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows/row");
                                    objSupp.gridXML.SelectSingleNode("/grid/table/rows").RemoveChild(objXMLRowNode);
                                    objSupp.Value = "";
                                    long lRowNoPrev = 0;
                                    long lRow = 0;
                                    long lCol = 0;
                                    long lColNo = 0;
                                    objXMLRowElement = objSupp.gridXML.CreateElement("row");
                                    while (flag)
                                    {
                                        if (objRdr.Read())
                                        {
                                            lCol = Convert.ToInt64(objRdr["COL_NUM"].ToString());
                                            lRow = Convert.ToInt64(objRdr["ROW_NUM"].ToString());
                                            if (lRowNoPrev != lRow)
                                            {
                                                for (int i = Convert.ToInt32(lColNo); i < colCount; i++)
                                                {
                                                    objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                                    objXMLCellElement.InnerText = "";
                                                    objSupp.Value = string.Concat(objSupp.Value, "KNALB", "~");
                                                    objXMLRowElement.AppendChild(objXMLCellElement);
                                                    objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                                    objXMLRowsNode.AppendChild(objXMLRowElement);
                                                    bRow = true;
                                                }
                                                lColNo = 0;
                                                objXMLRowElement = objSupp.gridXML.CreateElement("row");
                                            }
                                            objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                            objXMLCellElement.InnerText = objRdr["CELL_VALUE"].ToString();
                                            if (objRdr["CELL_VALUE"].ToString() == "")
                                                objSupp.Value = objSupp.Value + "KNALB" + "~";
                                            else
                                                objSupp.Value = objSupp.Value + objRdr["CELL_VALUE"].ToString() + "~";

                                            objXMLRowElement.AppendChild(objXMLCellElement);
                                            objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                            objXMLRowsNode.AppendChild(objXMLRowElement);
                                            bRow = true;
                                        }
                                        else
                                        {
                                            for (int i = Convert.ToInt32(lColNo); i < colCount; i++)
                                            {

                                                objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                                objXMLCellElement.InnerText = "";
                                                objSupp.Value = string.Concat(objSupp.Value, "KNALB", "~");
                                                objXMLRowElement.AppendChild(objXMLCellElement);
                                                objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                                objXMLRowsNode.AppendChild(objXMLRowElement);
                                                bRow = true;

                                            }
                                            break;
                                        }
                                        lColNo = lColNo + 1;
                                        lRowNoPrev = lRow;
                                    }
                                }

                                if (!bRow)
                                {
                                    objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                    objXMLRowsNode.AppendChild(objXMLRowElement);

                                }
                                if (objSupp.Value != null)
                                    if (objSupp.Value.ToString() != "")
                                        objSupp.Value = objSupp.Value.ToString().Substring(0, objSupp.Value.ToString().Length - 1);

                            }
                            else
                            {
                                //Raman: Changing Location of columns.xml from bin to Appfiles/Supp - 02/04/2009
                                string path = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                                //string path = AppDomain.CurrentDomain.BaseDirectory + "bin/columns.xml";
                                XmlElement objXMLElem = null;
                                XmlNode objXMLFieldsNode = null;
                                XmlElement objXMLTitleElement = null;
                                objSupp.gridXML = new XmlDocument();
                                objSupp.gridXML.Load(path);
                                SQL = string.Concat("SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '"
                                    , objSupp.FieldId
                                    , "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC");
                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    while (objRdr.Read())
                                    {
                                        objXMLElem = objSupp.gridXML.CreateElement("field");
                                        objXMLTitleElement = objSupp.gridXML.CreateElement("title");
                                        objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();
                                        objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");
                                        objXMLElem.AppendChild(objXMLTitleElement);
                                        objXMLFieldsNode = objSupp.gridXML.SelectSingleNode("/grid/table/fields");
                                        objXMLFieldsNode.AppendChild(objXMLElem);
                                    }
                                }
                            }

                        }

                 //asharma326 JIRA# 6422 Starts 
                        else if (objSupp.FieldType == (SupplementalFieldTypes)25)
                        {
                            SQL = string.Concat("SELECT HTML_TEXT FROM SUPP_TEXT_VALUES WHERE FIELD_ID = '"
                                   , objSupp.FieldId
                                   , "' AND RECORD_ID = '" + recordId + "'");
                            using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                            {
                                if (objRdr.Read())
                                {
                                    // read table column values
                                    //objSupp.Value = objRdr["PLAIN_TEXT"].ToString() + SupplementalField.HTMLDELIMITER + objRdr["HTML_TEXT"].ToString();
                                    //objSupp.HTMLContents = new string[2];
                                    //objSupp.HTMLContents[0] = Convert.ToString(objRdr["PLAIN_TEXT"]);
                                    objSupp.HTMLContents = Convert.ToString(objRdr["HTML_TEXT"]);
                                }
                            }
                        }
                    //asharma326 JIRA# 6422 Ends           
                }
                //zmohammad MITS 33695 End
                //End by Shivendu for supplemental grid




                //-----------------------


				this.DataChanged = false;
				(this as IDataModel).IsStale = false;

			}
			catch(Exception e)
            { throw new DataModelException(Globalization.GetString("ADMTable.Load.Exception", this.Context.ClientId), e); }
			finally
			{
				if (objReader != null)
					objReader.Close();
				objReader = null;
			}
		}
		//		public bool UsesAssociation{get{return m_UsesAssociation;}}

		public  void Delete()
		{
			string SQL ="";
			string sWhere="";
			SupplementalField objSupp =null;
			bool isOurDbTrans = false;

			//Check for supp table definition
			if(!this.DefinitionLoaded)
				return;
			
			// Check for Stale Data  
			// Means data is out of sync with parent object and the new data was never touched to be loaded.
			if((this as IDataModel).IsStale)
				return;

			foreach(string key in m_SuppFields)
			{
				objSupp = (m_SuppFields[key] as SupplementalField);
				if(	objSupp.IsKeyValue)
				{
					if(sWhere != "")
						sWhere += " AND ";
					sWhere += objSupp.FieldName + "=" + objSupp.Value;
				}
			}

			//Check for any Supp Fields defined
			if(sWhere=="")
				return;
			
			try
			{
			
				//If we're not already in a transaction, start one.
				if(this.Context.DbTrans == null)
				{
					this.Context.DbTrans = Context.DbConn.BeginTransaction();
					isOurDbTrans= true;
				}

				SQL = String.Format("DELETE FROM {0} WHERE {1}",this.TableName,sWhere);

				DbCommand cmd = this.Context.DbConn.CreateCommand();
				cmd.Transaction = Context.DbTrans;
				cmd.CommandText = SQL;
				cmd.ExecuteNonQuery();

				//Handle Multi-Value Fields
				object[] arr = this.m_SuppFields.GetKeyArray();
				int oldKey = this.KeyValue;
				for(int i=0; i< arr.Length;i++)
				{
					objSupp = (m_SuppFields[(string)arr[i]] as SupplementalField);
					{
						objSupp.Value = null;
                        // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                        // We need to delete the values from User Lookup as well
						if(	objSupp.IsMultiValue || objSupp.IsUserLookup)
						{
							objSupp.Values.ClearAll();
							SQL = String.Format("DELETE FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID = {1}",objSupp.FieldId,oldKey);
							cmd.CommandText = SQL;
							cmd.ExecuteNonQuery();//Shares cmd and Transaction as necessary.
						}
					}
				}

				//Book-Keeping
				if(isOurDbTrans)
					Context.DbTrans.Commit();
			}
			catch(Exception e)
			{
				this.Context.DbTrans.Rollback();
                throw new DataModelException(Globalization.GetString("ADMTable.Delete.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if(isOurDbTrans)
					this.Context.DbTrans = null;

			}
		}

		public int Count(){return m_SuppFields.Count();}
		public SupplementalField this[string sFieldName]{get{return (m_SuppFields[sFieldName]as SupplementalField);}}

		// Implemented for classes like Claimant where we have 
		// Claim_ROW_ID field which is not listed in supplemental table definition.
		internal void Add(SupplementalField objSupp){m_SuppFields[objSupp.FieldName] = objSupp;}
		public bool IsAnyRequired
		{
			get
			{
				foreach(SupplementalField objSupp in this)
					if(objSupp.Required &&  objSupp.Visible)
						return true;
				return false;
			}
		}

		IEnumerator IEnumerable.GetEnumerator(){return new SupplementalEnumerator(this,this.Context.ClientId);}

		#region IPersistence Members
		public event InitObjEventHandler InitObj;

        public event InitDataEventHandler InitData;
		public event Riskmaster.DataModel.CalculateDataEventHandler CalculateData;

		public event Riskmaster.DataModel.ValidateEventHandler ValidateData;

		public event Riskmaster.DataModel.BeforeSaveEventHandler BeforeSave;

		public event Riskmaster.DataModel.AfterSaveEventHandler AfterSave;

		public bool IsNew
		{
			get{return (m_isNew || this.KeyValue ==0);}
		}

		override public  bool DataChanged
		{
			get
			{
				foreach(string key in m_SuppFields)
					if((m_SuppFields[key] as SupplementalField).DataChanged)
						return true;
				return false;
			}
			set
			{
				if(this.IsDataChangedFlagLocked())
					return;
				foreach(string key in m_SuppFields)
					(m_SuppFields[key] as SupplementalField).DataChanged = value;
			}
		}

		override public bool AnyDataChanged
		{
			get
			{
				return this.DataChanged;
			}
		}


		public void Save()
		{
			string SQL = "";
			string sWhere = "";
			SupplementalField objSupp = null;
			bool isOurDbTrans = false;
			DbWriter objWriter = null;
			int recordId= 0;
            //added by Abhishek for Supplemental grid
            DbReader objReader = null;
            Dictionary<string, object> dictParams = null;
            DbCommand objCommand = null;
			try
			{
				
				// Check for Stale Data  
				// Means data is out of sync with parent object and the new data was never touched to be loaded.
				if((this as IDataModel).IsStale)
					return;

				//Check for data changed.
				if(!this.DataChanged)
					return;


                //BSB 12.14.2006 Clear ScriptWarnings Explicitly (Don't rely on GC)
                if (this.Context.DbTrans == null)
                {
                    this.Context.ScriptWarningErrors.Clear();
                    this.Context.ScriptValidationErrors.Clear();
                }
                //Trigger Calculate, Validate and BeforeSave script events.
				//TODO Error Trapped & stronger typed Calculate Data Script Call
				this.CalculateData(this,new CalculateDataEventArgs());
				//TODO Fully Error trapped Validate Data Script Call
				if(!this.ValidateData(this,new ValidateEventArgs())) throw new RMAppException("DataObject.Save.ValidationFailure");


				// Where Clause Creation
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
					if(objSupp.IsKeyValue || objSupp.DataChanged)
						if(objSupp.IsKeyValue)
						{
							if(sWhere !="")
								sWhere += " AND ";
							if((int)objSupp.Value ==0)
								objSupp.Value = Context.GetNextUID(this.TableName);
							else
								sWhere += objSupp.FieldName + "=" + objSupp.Value;
						}
				}
				
				
				//TODO Error Trapped & stronger typed BeforeSave Data Script Call
				this.BeforeSave(this,new BeforeSaveEventArgs());

				//Ensure we're in a  transaction.
				if (Context.DbTrans ==null)
				{
					Context.DbTrans = Context.DbConn.BeginTransaction();
					isOurDbTrans = true;
				}

				
				//Begin Preparing Writer
				objWriter = DbFactory.GetDbWriter(Context.DbConn);

				objWriter.Tables.Add(this.TableName);
				objWriter.Where.Add(sWhere);

				//Save single value fields.
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
                    // if((objSupp.IsKeyValue || objSupp.DataChanged) && ! objSupp.IsMultiValue)
                    //Modified by Abhishek for Supplemental Grid
                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // While saving the Supplemental fields, we need to handle User Lookup separately
                    if (!objSupp.IsMultiValue && !objSupp.IsUserLookup && !(objSupp.FieldType == SupplementalFieldTypes.SuppTypeGrid))
						objWriter.Fields.Add(objSupp.FieldName,objSupp.Value);
				}
				objWriter.Execute(Context.DbTrans);

				//Save Multi-value fields.
				recordId = this.KeyValue;
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);

                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // While saving the User Lookup Supplementals we need to delete all the values for that record and then Insert all the values in it
					if((objSupp.IsMultiValue || objSupp.IsUserLookup) && objSupp.DataChanged && recordId != 0)
					{
						SQL = String.Format("DELETE FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}",objSupp.FieldId,recordId);
						Context.DbConn.ExecuteNonQuery(SQL,Context.DbTrans);

						SQL = String.Format(@"INSERT INTO SUPP_MULTI_VALUE (FIELD_ID,RECORD_ID,CODE_ID) 
							VALUES({0},{1},{2})",objSupp.FieldId,recordId,"{SIMPLELISTVALUE}");
						objSupp.Values.SaveSimpleList(SQL);
					}
                    //Start by Abhishek to save Supplemental grid values
                    else if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeGrid && objSupp.DataChanged && recordId != 0)
                    {
                        SQL = String.Format("DELETE FROM SUPP_GRID_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}", objSupp.FieldId, recordId);
                        Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                        if (objSupp.Value.ToString() != "")
                        {
                            int rowID = 0;
                            int colCount = 0;
                            long nextRowId = 0;
                            int counter = -1;
                            bool nextRow = true;
                            string[] gridValue = null;
                            ArrayList colNumber = new ArrayList();
                            gridValue = objSupp.Value.ToString().Split('~');
                            SQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + objSupp.FieldId + "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC";
                            objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL);
                            while (objReader.Read())
                            {

                                colNumber.Add(objReader["COL_NO"].ToString());
                                colCount++;


                            }


                            while (nextRow)
                            {
                                //zmohammad MITs 33695 start : Fix for special characters.
                                foreach (object item in colNumber)
                                {
                                    counter = counter + 1;
                                    if (counter == gridValue.Length)
                                    {
                                        nextRow = false;
                                        break;
                                    }
                                    if (gridValue[counter] == "KNALB")
                                        gridValue[counter] = "";
                                    objWriter = DbFactory.GetDbWriter(Context.DbConn);
                                    objWriter.Tables.Add("SUPP_GRID_VALUE");

                                    nextRowId = Utilities.GetNextUID(Context.DbConn.ConnectionString, "SUPP_GRID_VALUE", this.Context.ClientId);
                                    objWriter.Fields.Add((string)"GRID_VALUE_ROW_ID", nextRowId);
                                    objWriter.Fields.Add((string)"FIELD_ID", objSupp.FieldId);
                                    objWriter.Fields.Add((string)"RECORD_ID", recordId);
                                    objWriter.Fields.Add((string)"ROW_NUM", rowID);
                                    objWriter.Fields.Add((string)"COL_NUM", item.ToString());
                                    objWriter.Fields.Add((string)"CELL_VALUE", gridValue[counter]);

                                    objWriter.Execute(Context.DbTrans);

                                }
                                rowID++;
                                //zmohammad MITs 33695 end
                            }


                        }

                    }
                    //End by Abhishek to save Supplemental grid values
                    //asharma326 JIRA# 6422 Starts  
                    else if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeHTML && objSupp.DataChanged && recordId != 0)
                    {
                        SQL = String.Format("DELETE FROM SUPP_TEXT_VALUES WHERE FIELD_ID={0} AND RECORD_ID={1}"
                               , objSupp.FieldId
                               , recordId);
                        Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                        if ((!String.IsNullOrEmpty(objSupp.HTMLContents)))
                        {
                            dictParams = new Dictionary<string, object>();
                            objCommand = Context.DbConn.CreateCommand();
                            SQL = String.Format(@"INSERT INTO SUPP_TEXT_VALUES (FIELD_ID,RECORD_ID,HTML_TEXT) VALUES({0},{1},{2})", "~FieldId~", "~recordId~", "~HTMLContents~");
                            //, objSupp.FieldId
                            //, recordId
                            //, objSupp.HTMLContents);
                            dictParams.Add("FieldId", objSupp.FieldId);
                            dictParams.Add("recordId", recordId);
                            dictParams.Add("HTMLContents", objSupp.HTMLContents);
                            objCommand.CommandText = SQL;
                            objCommand.Transaction = Context.DbTrans;
                            DbFactory.ExecuteNonQuery(objCommand, dictParams);

                            //SQL = String.Format(@"INSERT INTO SUPP_TEXT_VALUES (FIELD_ID,RECORD_ID,HTML_TEXT) VALUES({0},{1},'{2}')"
                            //    , objSupp.FieldId
                            //    , recordId
                            //    , objSupp.HTMLContents);
                            //Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                        }
                    }
                    //asharma326 JIRA# 6422 Ends
				}

				//Book-Keeping
				if(isOurDbTrans)
					Context.DbTrans.Commit();

				this.m_DataChanged = false;

				//TODO Error Trapped & stronger typed BeforeSave Data Script Call
				this.AfterSave(this,new AfterSaveEventArgs());

			}
			catch(Exception e)
			{
                if (this.Context.DbTrans != null)//null check added by Shivendu for MITS 19002
                {
                    this.Context.DbTrans.Rollback();
                }
                throw new DataModelException(Globalization.GetString("ADMTable.Save.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if(isOurDbTrans)
					this.Context.DbTrans = null;
			}
		}
		public bool Validate(){return this.ValidateData(this,new ValidateEventArgs());}

		public void Refresh()
		{
			Load();
		}

		#endregion
		#region INavigation Members
		public virtual void MoveFirst(){MoveAndLoad(MOVE_FIRST);} 
		public virtual void MoveLast(){MoveAndLoad(MOVE_LAST);} 
		public virtual void MoveNext(){MoveAndLoad(MOVE_NEXT);} 
		public virtual void MovePrevious(){MoveAndLoad(MOVE_PREVIOUS);}
		public virtual void MoveTo( params int[] keyValue)
		{ 
			Clear();
			this.KeyValue = keyValue[0];
			Load();
		} 
		protected void MoveAndLoad(int moveDirection)
		{
			DbReader rdr= null;
			string sSQL="";
			int lNewID=0;
			int keyValue = this.KeyValue;
			string keyName = this.KeyName;

			try
			{
				// TODO: SetAllChanged False
				switch (moveDirection)
				{
					case MOVE_FIRST:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0",keyName,this.m_TableName, keyName);
						break;
					case MOVE_LAST:
						sSQL = String.Format("SELECT MAX({0}) FROM {1}",KeyName,this.m_TableName);
						break;
					case MOVE_NEXT:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",keyName,this.m_TableName, keyName,keyValue);
						break;
					case MOVE_PREVIOUS:
						if(keyValue==0)
							sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",keyName,this.m_TableName, keyName,keyValue);
						else
							sSQL = String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}",keyName,this.m_TableName, keyName,keyValue);
						break;
				}

				//Apply Filter Clause
				if (m_sFilterClause != "") 
					if(sSQL.IndexOf(" WHERE ") > 0)
						sSQL += " AND (" + m_sFilterClause + ")";
					else
						sSQL +=" WHERE " + m_sFilterClause;

				///Fetch the RowId
				rdr = Context.DbConnLookup.ExecuteReader(sSQL);
				if (rdr.Read())
					lNewID = rdr.GetInt(0);
				rdr.Close();
				rdr = null;
				
				//Only if New Unique ID is not 0 and New Unique ID different than old one
				if (lNewID != 0 && lNewID != keyValue)
				{
					this.Clear();
					this.KeyValue= lNewID;
					Load();
				}
			}
			catch(Exception e)
			{
                throw new RMAppException(Globalization.GetString("ADMTable.MoveAndLoad.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if(rdr !=null)
					rdr.Close();
			}
		}
		string INavigation.Filter
		{
			get{return this.m_sFilterClause;}
			set
			{
				if(!base.Locked)
					m_sFilterClause = value;
			}
		}
		#endregion
		//TODO Remove after debuggging.
		public string Dump()
		{
			string s = "";
			string sIndent ="";
			for(int i=1;i< DataObject.m_NestedDumpLevel;i++)
				sIndent+="\t";

			foreach(SupplementalField objSupp in this)
			{
                // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                // Check the values for User lookup
				if(objSupp.IsMultiValue || objSupp.IsUserLookup)
				{
					s += sIndent + objSupp.Caption + "(" + objSupp.FieldName + ")=\n";
					foreach(DictionaryEntry o in objSupp.Values)
					{
						if(o.Value==null)
							s += "null\n";
						else
							s += o.Value.ToString() + "\n";
					}
				}
				else
				{	
					if(objSupp.Value==null)
						s+= sIndent + objSupp.Caption + "(" + objSupp.FieldName + ")="  + "null\n";
					else
						s+= sIndent +  objSupp.Caption + "(" + objSupp.FieldName + ")="  + (string)(objSupp.Value).ToString() + "\n";
				}
			}
			return s;
		}
		public XmlDocument ViewXml
		{
			get
			{
				try
				{

					XmlDocument objXML = new XmlDocument();
					XmlElement objXMLRoot;
					XmlElement objXMLElem;
					string s="";

					//We can cache this information...
					if(m_ViewXml!="")
					{
						objXML.LoadXml(m_ViewXml);
						return objXML;
					}

					// Start by creating XML definition
					//	<group name="generalinfo" title="General Information">

					objXMLRoot = objXML.CreateElement("group");
					objXMLElem = objXMLRoot;
					objXMLElem.SetAttribute("name", "admgroup");
					objXMLElem.SetAttribute("title", this.TableUserName);
					//TR#1671-Pankaj:Default first tab as selected.
					objXMLElem.SetAttribute("selected", "1");
					objXML.AppendChild(objXMLRoot);
					
					// Loop through all fields and create XML definition
					int i=0;
					int max = 1000;
					foreach(SupplementalField objField in this)
					{
						if(i ==max)
							break;
						i++;
						s += "<displaycolumn>" + objField.ViewXML.OuterXml.Replace("*/Supplementals","ADMTable") + "</displaycolumn>";
					}
					//Hack a consistent hidden RecordId Field into the view.
//					s += @"<displaycolumn><control id=""recordid"" type=""id"" ref=""/Instance/ADMTable/@RECORD_ID"" /></displaycolumn>";
					

					objXMLRoot.InnerXml = s;
					
					m_ViewXml = objXML.OuterXml;
					return objXML;
				}
				catch{}
				return new XmlDocument();
			}
		}

		//TODO: BSB Add ADMTable to the static schema file.
		const string SCHEMA_FILE= "schema-1.xsd";
		public bool PopulateObject(XmlDocument propertyStore)
		{
			return PopulateObject(propertyStore,this.GetType().Name,false);
		}
		public bool PopulateObject(XmlDocument propertyStore,bool bKeepCurrentData)
		{
			return PopulateObject(propertyStore,this.GetType().Name,bKeepCurrentData);
		}
		internal bool PopulateObject(XmlDocument propertyStore, string rootTagName)
		{
			return PopulateObject(propertyStore,rootTagName,false);
				
		}
		internal bool PopulateObject(XmlDocument propertyStore, string rootTagName,bool bKeepCurrentData)
		{
            bool bNodeRemoved = false;
			bool bAddingNew = false;
            string sProp = string.Empty;
            XmlElement elt = null;
            XmlReader xmlValidator = null;

			//1.) Determine if propertyStore is valid for the current object type.
			//a.)Root Tag matches class name?
			if(propertyStore.DocumentElement.Name == rootTagName)
				elt = propertyStore.DocumentElement as XmlElement;
			else
				elt = propertyStore.SelectSingleNode(String.Format("/Instance/{0}",rootTagName)) as XmlElement;

			if(elt==null)
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RootElementMismatch", this.Context.ClientId), propertyStore.DocumentElement.Name, rootTagName));
			if(elt.HasAttribute("tablename"))
				if(elt.GetAttribute("tablename") !="" && elt.GetAttribute("tablename") != this.TableName)
                    throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.SuppTableMismatch", this.Context.ClientId), elt.GetAttribute("tablename"), this.TableName));
			elt = null;

			//b.) Document Validates?
            try
            {
                if (propertyStore.DocumentElement.Name == "Instance") //Then this is the first time we're seeing this document, (Worth doing the validation) 
                {
                    XmlUtilityFunctions.ExtractInstanceInfo(ref propertyStore, rootTagName, ref xmlValidator);
                }
            }
			catch(Exception e)
			{
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.SchemaValidationException", this.Context.ClientId), rootTagName), e);
			}
			finally
			{
				if(xmlValidator !=null)
					xmlValidator.Close();
				if(SerializationContext.RWLock.IsReaderLockHeld)
					SerializationContext.RWLock.ReleaseReaderLock();
			}

			//2.) If propertyStore represents an existing object then Navigate this object to the existing record.
			// NinetyNine percent of the time we will already be on the proper record but this won't hurt...
			try{elt = propertyStore.DocumentElement.SelectSingleNode(this.KeyName) as XmlElement;}
			catch(Exception e)
			{
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.KeyFieldNameMissing", this.Context.ClientId), rootTagName + "." + this.KeyName), e);
			}
			
			bAddingNew =(elt.InnerText == "0" && this.KeyValue == 0);

			if(!bAddingNew && !bKeepCurrentData)//Optional 1:1 record added on the fly...
				try{this.MoveTo(Int32.Parse(elt.InnerText));}//Test if record is in DB - if not, treat as new.
				catch(RecordNotFoundException){bAddingNew=true;(this as IDataModel).IsStale=false;this.DataChanged=false;}

			//BSB 05.23.2005  I don't think this is an error per se.  It's really just a condition
			// that tells us to stop loading this supp object from instance XML???  The 
			// reasoning is that additional xml is not sufficient to signal the addition of 
			// supplemental data.  
			//(Existing data could be EDITED but a full record cannot currently be added in this manner.)

			// BSB 08.30.2005 This was previously disabled to prevent the situation where a user could call
			// Save directly on this object w/o having set the primary key from the "parent" record.
			// This causes problems creating both a new parent and it's associated supp data in one request.
			// Instead of prohibiting this here where it causes problems, 
			// let's just check for it during save on Supplementals and Acord classes where it matters.
			// Otherwise we won't be able to add new admin tracking either.
			//			if(bAddingNew)
			//				return true;
			
			//throw new Riskmaster.ExceptionTypes.XmlOperationException(Globalization.GetString("Supplementals.PopulateObject.Exception.NoCreateNewSupp"));


			//3.) (if not new) Compare top level TimeStamp? 
			//BSB Supplementals don't have independent date time stamps currently.
			//			{
			//				elt = propertyStore.SelectSingleNode("/*/DttmRcdLastUpd") as XmlElement;
			//
			//				if(elt !=null)
			//					if(this.GetProperty("DttmRcdLastUpd").ToString() !=elt.InnerText)
			//						throw new Riskmaster.ExceptionTypes.DataModelException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RecordAlreadyChanged"), this.GetType().Name + "." + this.KeyFieldName, this.KeyFieldValue));
			//			}

			//4.) For all children object data in the propertyStore.
			//a.) IF child is SimpleList - then treat this as a scalar and handle it in the PopulateScalar function.
			//b.) IF child is Singleton - then recursively call PopulateObject with the child object data Xml sub-tree.
			//c.) IF child is DataCollection - then call PopulateObject on the collection?
			//d.) IF child is Supplementals - then call PopulateObject on the Supplementals class.
			//e.) IF child is Scalar - then apply property values to this object using PopulateScalar function.
			foreach(XmlElement item in propertyStore.SelectNodes(String.Format("/{0}/*",rootTagName)))
			{
				string sCodeId = "";
				//All contents of supplemental tables should be scalar in nature. (DataSimpleList is ok)
				// JP 11.08.2005   sProp = item.LocalName;
				sProp = XmlConvert.DecodeName(item.LocalName);  // JP 11.08.2005   Decode if supp field name had bad-xml name chars in it (#, space, etc.)
				/*** Scalar Field Value ***/
				SupplementalField oProp = this.m_SuppFields[sProp] as SupplementalField;
                if (oProp == null)
                {
                    if (bNodeRemoved)//asharma326 JIRA 6422
                    {
                        bNodeRemoved = false;
                        continue;
                    }
                }
				System.Diagnostics.Trace.WriteLine("Populating Supplemental Scalar: " + sProp);

                // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                // Populate the Values for User lookup as well.
				if(oProp.IsMultiValue || oProp.IsUserLookup)
				{
					DataSimpleList lst = oProp.Values;
					sCodeId =  item.Attributes["codeid"].Value.ToString();
					if(lst!=null && sCodeId!="" && sCodeId!="0" && lst.ToString() != sCodeId) //It's a DataSimpleList
					{
						lst.ClearAll();
						foreach(string sSingleCodeId in sCodeId.Split(' '))
							lst.Add(Conversion.IsNumeric(sSingleCodeId) ? Int32.Parse(sSingleCodeId): 0);
						//return true;
					}
                        //bgupta22 MITS 36263 start
                    else if (String.IsNullOrEmpty(sCodeId) && String.Compare(sCodeId,"0") != 0) 
                    {
                        lst.ClearAll();
                    }
                    //bgupta22 MITS 36263 end
				}				
				else if(item.HasAttribute("codeid"))
					oProp.Value = Conversion.ConvertStrToInteger(item.Attributes["codeid"].Value);
                //asharma326 JIRA# 6422 Starts 
                else if (oProp.IsHTMLValue)
                {
                    //oProp.Value = item.InnerText + SupplementalField.HTMLDELIMITER + propertyStore.SelectSingleNode(rootTagName + "//" + item.LocalName + "_HTMLComments").InnerText;
                    //XmlNodeList nodes = propertyStore.GetElementsByTagName(item.LocalName + "_HTMLComments");
                    //XmlNode node = nodes[0];
                    //node.ParentNode.RemoveChild(nodes[0]);//dont remove the node from xmldoc just set flag to ignore the next element in case of HTMLType.
                    //oProp.HTMLContents = new string[2];
                    //oProp.HTMLContents[0] = item.InnerText;
                    oProp.Value = item.InnerText;
                    oProp.HTMLContents = propertyStore.SelectSingleNode(rootTagName + "//" + item.LocalName + "_HTMLComments").InnerText;
                    bNodeRemoved = true;
                }
                //asharma326 JIRA# 6422 Ends
				else
					oProp.Value =item.InnerText;			
			} //End "For Each Property"				
				
			return true;
		}
		public string SerializeObject()
		{
			SerializationContext objContext = new SerializationContext();
			objContext.RequestDom= new XmlDocument();
			objContext.RequestDom.LoadXml(String.Format("<{0} />",this.GetType().Name));
            //Start by Shivendu for R5
            objContext.ElementTag = this.GetType().Name;
            objContext.RequestXPath = String.Format("/{0}", objContext.ElementTag);
            //End by Shivendu for R5
			return SerializeObject(objContext);
		}
		public string SerializeObject(XmlDocument dom)
		{
			SerializationContext objContext = new SerializationContext();
			objContext.RequestDom= dom;
            //Start by Shivendu for R5
            objContext.ElementTag = this.GetType().Name;
            objContext.RequestXPath = String.Format("/{0}", objContext.ElementTag);
            //End by Shivendu for R5
			return SerializeObject(objContext);
		}
		public string SerializeObject(string sTag, XmlDocument dom)
		{
			SerializationContext objContext = new SerializationContext();
			objContext.RequestDom= dom;
			objContext.ElementTag = sTag;
			return SerializeObject(objContext);
		}
		internal string SerializeObject(SerializationContext objContext)
		{
			string s="";
			bool bEntryCall=false;

			if(objContext.ElementTag ==null || objContext.ElementTag=="" )
			{
				objContext.ElementTag =this.GetType().Name;
				if(objContext.RequestXPath == null || objContext.RequestXPath =="") //Initialize XPath iff this is the entry point
				{
					objContext.RequestXPath = objContext.ElementTag;
					bEntryCall = true;
				}
			}
			
			//Add Special Case Constant Tag Name for AdmTable RecordId
//			s += String.Format("<RECORD_ID>{0}</RECORD_ID>",this.KeyValue);

			foreach(SupplementalField o in this)
			{
				s += o.SerializeObject();
			}
			s = String.Format("<{0} RECORD_ID=\"{2}\">{1}</{0}>", objContext.ElementTag,s,this.KeyValue); 
				
			if(bEntryCall) 	//"Pre Processing" occurs only if this is the entry point for a Serialization Request
			{
				return  String.Format(@"
					<Instance  
						xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
						xsi:noNamespaceSchemaLocation=""schema-1.xsd" + @""">
						{0}</Instance>",s);
			}
			else
				return  s;

		}

		class SupplementalEnumerator : IEnumerator
		{
			int m_iPos = -1;
			ADMTable m_parent = null;
			object[] keys = null;

			//BSB Hacked to ensure that enumerating supp fields 
			// are returned in Ascending order by SeqNum.
			internal SupplementalEnumerator(ADMTable parent,int iClientId=0)
			{
				try
				{
					int i=0;
					m_parent = parent;

					SupplementalField[] suppFields = new SupplementalField[m_parent.m_SuppFields.Count()];
					keys = new object[m_parent.m_SuppFields.Count()];
				
					foreach(string sKey in m_parent.m_SuppFields)
						suppFields[i++]=m_parent.m_SuppFields[sKey] as SupplementalField;
				Array.Sort(suppFields);

					for(int j=0;j<i;j++)
						keys[j]=suppFields[j].FieldName;

					//keys = m_parent.m_SuppFields.GetKeyArray();
				}
				catch(Exception e)
                { throw new DataModelException(Globalization.GetString("SupplementalIterator.Exception", iClientId), e); }
			}
			#region IEnumerator Members

			public void Reset()
			{
				m_iPos =-1;
			}

			public object Current
			{
				get
				{
					if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
						return null;
					return m_parent.m_SuppFields[(string)keys[m_iPos]];
				}
			}

			public bool MoveNext()
			{
				m_iPos++;

				if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
					return false;
				else
					return true;
			}

			#endregion

		}
//		class SupplementalEnumerator : IEnumerator
//		{
//			int m_iPos = -1;
//			ADMTable m_parent = null;
//			object[] keys = null;
//
//			internal SupplementalEnumerator(ADMTable parent)
//			{
//				m_parent = parent;
//				keys = m_parent.m_SuppFields.GetKeyArray();
//			}
//			#region IEnumerator Members
//
//			public void Reset()
//			{
//				m_iPos =-1;
//			}
//
//			public object Current
//			{
//				get
//				{
//					if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
//						return null;
//					return m_parent.m_SuppFields[(string)keys[m_iPos]];
//				}
//			}
//
//			public bool MoveNext()
//			{
//				m_iPos++;
//
//				if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
//					return false;
//				else
//					return true;
//			}
//
//			#endregion
//
//		}
	}
}



