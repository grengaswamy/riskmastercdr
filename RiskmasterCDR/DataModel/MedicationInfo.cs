﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("EVENT_X_MEDICATION_PSO", "MEDICATION_ROW_ID", "DrugName")]
    public class MedicationInfo : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"DttmRcdLastUpd",           "DTTM_RCD_LAST_UPD"},
														{"UpdatedByUser",            "UPDATED_BY_USER"},
														{"DttmRcdAdded",             "DTTM_RCD_ADDED"},
														{"AddedByUser",              "ADDED_BY_USER"},
                                                        {"MedRowId",                 "MEDICATION_ROW_ID"},
                                                        {"EvtPsoRowId", "EVT_PSO_ROW_ID"},
                                                        {"DrugName",                 "PSO_MED_DRUG_NAME_TEXT"},
                                                        {"BrandName",                "PSO_MED_BRAND_NAME_TEXT"},
                                                        {"ManufacturerName",         "PSO_MED_MANUFACTURER_TEXT"},
                                                        {"ProductStrength",         "PSO_PROD_STRN_TEXT"},
                                                        {"Dosage",                   "PSO_MED_DOSE_TEXT"},
                                                        {"MedicationPrescribedCode", "PSO_MED_PRESC_CODE"},
                                                        {"MedicationGivenCode",      "PSO_MED_GVN_CODE"},
                                                        
														
		};

        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public int MedRowId { get { return GetFieldInt("MEDICATION_ROW_ID"); } set { SetField("MEDICATION_ROW_ID", value); } }
        public int EvtPsoRowId { get { return GetFieldInt("EVT_PSO_ROW_ID"); } set { SetField("EVT_PSO_ROW_ID", value); } }
        public string DrugName { get { return GetFieldString("PSO_MED_DRUG_NAME_TEXT"); } set { SetField("PSO_MED_DRUG_NAME_TEXT", value); } }
        public string BrandName { get { return GetFieldString("PSO_MED_BRAND_NAME_TEXT"); } set { SetField("PSO_MED_BRAND_NAME_TEXT", value); } }
        public string ManufacturerName { get { return GetFieldString("PSO_MED_MANUFACTURER_TEXT"); } set { SetField("PSO_MED_MANUFACTURER_TEXT", value); } }
        public string ProductStrength { get { return GetFieldString("PSO_PROD_STRN_TEXT"); } set { SetField("PSO_PROD_STRN_TEXT", value); } }
        public string Dosage { get { return GetFieldString("PSO_MED_DOSE_TEXT"); } set { SetField("PSO_MED_DOSE_TEXT", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int MedicationPrescribedCode { get { return GetFieldInt("PSO_MED_PRESC_CODE"); } set { SetField("PSO_MED_PRESC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int MedicationGivenCode { get { return GetFieldInt("PSO_MED_GVN_CODE"); } set { SetField("PSO_MED_GVN_CODE", value); } }

        #endregion

        internal MedicationInfo(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();

        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "EVENT_X_MEDICATION_PSO";
            this.m_sKeyField = "MEDICATION_ROW_ID";
            this.m_sFilterClause = string.Empty;
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            this.m_sParentClassName = "EventPSO";
            //Add all object Children into the Children collection from our "sChildren" list.
            base.Initialize();

        }
    }
}
