﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIMANT_MMSEA", "CLAIMANT_ROW_ID")]
   public class ClaimantMmsea : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                        {"ClaimantRowID", "CLAIMANT_ROW_ID"},              
                        {"IsBenificaryCode", "MMSEA_BENEFIY_CODE"},	
		                {"IsPaymentNonMedical","MMSEA_NON_MED_CODE"},
                        {"ConfirmedDate", "MMSEA_CONFIRM_DATE"}	,			
                        {"HealthClaimNumber", "MMSEA_HICN_TEXT"},			
                        {"ICDEventCode", "ICD_EVENT_CODE"},		
                        {"ProductLiabilityCode","MMSEA_PRO_LIY_CODE"},	
                        {"ProductGenericName","MMSEA_PRO_GEN_TEXT"},			
                        {"ProductBrandName","MMSEA_PRO_BRD_TEXT"},		
                        {"ProductManufacturer", "MMSEA_PRO_MAN_TEXT"},		
                        {"ProductHarmText", "MMSEA_PRO_ALL_TEXT"},		
                        {"ResponsiblityForMedicalsCode", "MMSEA_ORM_CODE"},	
                        {"ResponsibilityTerminationDate", "MMSEA_ORM_DATE"},
                        {"FirstExtractDate","MMSEA_FRT_EXT_DATE"},			
                        {"LastExtractDate","MMSEA_LST_EXT_DATE"},			
                        {"MMSEAEditedFlag","MMSEA_EDITED_FLAG"},			
                        {"MSPEffectiveDate","MMSEA_MSP_EFF_DATE"},			
                        {"MSPTerminationDate", "MMSEA_MSP_TER_DATE"},		
                        {"MSPTypeIndicator", "MMSEA_MSP_TYP_CODE"},			
                        {"DispositionCode", "MMSEA_MSP_DIS_CODE"},			
                        {"AddedByUser","ADDED_BY_USER"},					
                        {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},			
                        {"UpdatedByUser","UPDATED_BY_USER"},				
                        {"DelMDCRERCDFlag","DEL_MDCRE_RCD_FLAG"},
                        {"DelMDCRERCDDate","DEL_MDCRE_RCD_DATE"},
                        {"NoFaultIndicator","NO_FAULT_INDICATOR"},
                        {"NoFaultInsuranceLimit","NO_FAULT_INSURANCE_LIMIT"},
                        {"NoFaultExhaustDate","NO_FAULT_EXHAUST_DATE"},
                        {"ClaimantRepresentativeCode","MMSEA_CLMT_REP_CODE"},
                        {"ICD10EventCode", "ICD10_EVENT_CODE"}, //prashbiharis MITS 32423 ICD-10 changes
                        {"ClaimantRepresentativeEID","MMSEA_CLMT_REP_EID"}
                        };//Added by Amitosh for Mits 24380 (03/16/2011)
                         //Added by Kiran for Mits 24985(02/22/2013)
                        //Added by asingh263 for mits 32440
                        



        public int ClaimantRowID { get { return GetFieldInt("CLAIMANT_ROW_ID"); } set { SetField("CLAIMANT_ROW_ID", value); } }
        public int IsPaymentNonMedical { get { return GetFieldInt("MMSEA_NON_MED_CODE"); } set { SetField("MMSEA_NON_MED_CODE", value); } }
        
        public int IsBenificaryCode { get { return GetFieldInt("MMSEA_BENEFIY_CODE"); } set { SetField("MMSEA_BENEFIY_CODE", value); } }
        public string ConfirmedDate { get { return GetFieldString("MMSEA_CONFIRM_DATE"); } set { SetField("MMSEA_CONFIRM_DATE", value); } }
        public string HealthClaimNumber { get { return GetFieldString("MMSEA_HICN_TEXT"); } set { SetField("MMSEA_HICN_TEXT", value); } }
        public int ICDEventCode { get { return GetFieldInt("ICD_EVENT_CODE"); } set { SetField("ICD_EVENT_CODE", value); } }
        public int ProductLiabilityCode { get { return GetFieldInt("MMSEA_PRO_LIY_CODE"); } set { SetField("MMSEA_PRO_LIY_CODE", value); } }
        public string ProductGenericName { get { return GetFieldString("MMSEA_PRO_GEN_TEXT"); } set { SetField("MMSEA_PRO_GEN_TEXT", value); } }
        public string ProductBrandName { get { return GetFieldString("MMSEA_PRO_BRD_TEXT"); } set { SetField("MMSEA_PRO_BRD_TEXT", value); } }
        public string ProductManufacturer { get { return GetFieldString("MMSEA_PRO_MAN_TEXT"); } set { SetField("MMSEA_PRO_MAN_TEXT", value); } }
        public string ProductHarmText { get { return GetFieldString("MMSEA_PRO_ALL_TEXT"); } set { SetField("MMSEA_PRO_ALL_TEXT", value); } }
        public int ResponsiblityForMedicalsCode { get { return GetFieldInt("MMSEA_ORM_CODE"); } set { SetField("MMSEA_ORM_CODE", value); } }
        public string ResponsibilityTerminationDate { get { return GetFieldString("MMSEA_ORM_DATE"); } set { SetField("MMSEA_ORM_DATE", value); } }
        public string FirstExtractDate { get { return GetFieldString("MMSEA_FRT_EXT_DATE"); } set { SetField("MMSEA_FRT_EXT_DATE", value); } }
        public string LastExtractDate { get { return GetFieldString("MMSEA_LST_EXT_DATE"); } set { SetField("MMSEA_LST_EXT_DATE", value); } }
        public int MMSEAEditedFlag { get { return GetFieldInt("MMSEA_EDITED_FLAG"); } set { SetField("MMSEA_EDITED_FLAG", value); } }
        public string MSPEffectiveDate { get { return GetFieldString("MMSEA_MSP_EFF_DATE"); } set { SetField("MMSEA_MSP_EFF_DATE", value); } }
        public string MSPTerminationDate { get { return GetFieldString("MMSEA_MSP_TER_DATE"); } set { SetField("MMSEA_MSP_TER_DATE", value); } }
        public int MSPTypeIndicator { get { return GetFieldInt("MMSEA_MSP_TYP_CODE"); } set { SetField("MMSEA_MSP_TYP_CODE", value); } }
        public int DispositionCode { get { return GetFieldInt("MMSEA_MSP_DIS_CODE"); } set { SetField("MMSEA_MSP_DIS_CODE", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public int DelMDCRERCDFlag { get { return GetFieldInt("DEL_MDCRE_RCD_FLAG"); } set { SetField("DEL_MDCRE_RCD_FLAG", value); } }
        public string DelMDCRERCDDate { get { return GetFieldString("DEL_MDCRE_RCD_DATE"); } set { SetField("DEL_MDCRE_RCD_DATE", value); } }
        //Added by Amitosh for Mits 24380 (03/16/2011)
        public int NoFaultIndicator { get { return GetFieldInt("NO_FAULT_INDICATOR"); } set { SetField("NO_FAULT_INDICATOR", value); } }
        public double NoFaultInsuranceLimit { get { return GetFieldDouble("NO_FAULT_INSURANCE_LIMIT"); } set { SetField("NO_FAULT_INSURANCE_LIMIT", value); } }
        public string NoFaultExhaustDate { get { return GetFieldString("NO_FAULT_EXHAUST_DATE"); } set { SetField("NO_FAULT_EXHAUST_DATE", value); } }
             //end Amitosh
         //Added by Kiran for Mits 24985(02/22/2013)
        public int ClaimantRepresentativeCode { get { return GetFieldInt("MMSEA_CLMT_REP_CODE"); } set { SetField("MMSEA_CLMT_REP_CODE", value); } } 
        //end Kiran
        public int ICD10EventCode { get { return GetFieldInt("ICD10_EVENT_CODE"); } set { SetField("ICD10_EVENT_CODE", value); } } //prashbiharis MITS 32423 ICD-10 changes
        //Added by asingh263 for mits 32440
        //Rijul 340
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int ClaimantRepresentativeEID { get { return GetFieldInt("MMSEA_CLMT_REP_EID"); } set { SetFieldAndNavTo("MMSEA_CLMT_REP_EID", value, "ClaimantRepEntity"); } }
        //public int ClaimantRepresentativeEID { get { return GetFieldInt("MMSEA_CLMT_REP_EID"); } set { SetField("MMSEA_CLMT_REP_EID", value); } } 
        //Rijul 340 end
        //end
 #endregion


        #region Child Implementation

        private string[,] sChildren = {{"ClaimantMMSEAXPartyList","ClaimantMMSEAXPartyList"},
                                          {"ClmtMmseaTpocList","ClmtMmseaTpocList"},
                                          {"ClaimantRepEntity","Entity"}    //Rijul 340
                                      };



        override internal void OnChildInit(string childName, string childType)
        {
            Entity objEnt = null;   //Rijul 340
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Rijul 340
            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "Entity":
                    switch (childName)
                    {
                        case "ClaimantRepEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                    }
                    break;
            }
            //Rijul 340 end
        }
        public override bool IsNew
        {
            get
            {
				return (this.KeyFieldValue <=0);
            }
        }
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {

                case "ClaimantMMSEAXPartyList":
                    itemValue.Parent = this;
                    (itemValue as ClaimantMMSEAXParty).ClaimantRowID = this.ClaimantRowID;
                    break;
                case "ClmtMmseaTpocList":
                    itemValue.Parent = this;
                    (itemValue as ClmtMmseaTpoc).ClaimantRowID = this.ClaimantRowID;
                    break;
            }

            base.OnChildItemAdded(childName, itemValue);
        }

        //Rijul 340
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            this.SetFieldAndNavTo("MMSEA_CLMT_REP_EID", this.ClaimantRepresentativeEID, "ClaimantRepEntity", true);
        }

        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            int intTableID;
            Dictionary<string, int> objReturnVal = new Dictionary<string, int>();
            // If Entity is involved we save this first (in case we need the EntityId.)
            if (childName == "ClaimantRepEntity")
            {
                if (!int.Equals(ClaimantRepresentativeEID, 0))
                {
                    Entity objEntity = ((childValue as Entity));
                    intTableID = Context.LocalCache.GetTableId(GetSystemTabelName());
                    objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                    if (objReturnVal != null)
                        this.m_Fields["MMSEA_CLMT_REP_EID"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
                }
            }
            objReturnVal = null;
        }

        public Entity ClaimantRepEntity
        {
            get
            {
                Entity objItem = base.m_Children["ClaimantRepEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("ClaimantRepEntity", (objItem as DataObject), this.ClaimantRepresentativeEID) as Entity;
                return objItem;
            }
        }
        //Rijul 340 end
        #endregion
        internal ClaimantMmsea(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}

        public ClaimantMMSEAXPartyList ClaimantMMSEAXPartyList
        {
            get
            {
                ClaimantMMSEAXPartyList objList = base.m_Children["ClaimantMMSEAXPartyList"] as ClaimantMMSEAXPartyList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }

        public ClmtMmseaTpocList ClmtMmseaTpocList
        {
            get
            {
                ClmtMmseaTpocList objList = base.m_Children["ClmtMmseaTpocList"] as ClmtMmseaTpocList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }		
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIMANT_MMSEA";
            this.m_sKeyField = "CLAIMANT_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
           base.InitChildren(sChildren);
            this.m_sParentClassName = "";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
        }

        //Rijul 340
        private string GetSystemTabelName()
        {
            string sReturnVal = string.Empty;
            switch (this.Context.LocalCache.GetShortCode(this.ClaimantRepresentativeCode).ToUpper())
            {
                case "A":
                    sReturnVal = Globalization.EntityGlossaryTableNames.ATTORNEYS.ToString();
                    break;
                case "G":
                    sReturnVal = Globalization.EntityGlossaryTableNames.GUARDIAN_TYPE.ToString();
                    break;
                case "P":
                    sReturnVal = Globalization.EntityGlossaryTableNames.POWOFATTORNEY_TYPE.ToString();
                    break;
                case "O":
                    sReturnVal = Globalization.EntityGlossaryTableNames.OTHER_PEOPLE.ToString();
                    break;
                case "F":
                    sReturnVal = Globalization.EntityGlossaryTableNames.ATTORNEY_FIRMS.ToString();
                    break;
            }
            return sReturnVal;
        }
        //Rijul 340 End

    }
}
