﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("FUNDS_X_DED_SPLIT_MAP", "FUNDS_X_DED_SPLIT_MAP_ID")]
    public class FundsXDedSplitMap : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"FundsXDedSplitMapId", "FUNDS_X_DED_SPLIT_MAP_ID"},
                                        {"ClaimId", "CLAIM_ID"},
                                        {"PolcvgRowId", "POLCVG_ROW_ID"},
                                        {"DedPayTransId", "DED_PAY_TRANS_ID"},
                                        {"PosSplitRowId", "POS_SPLIT_ROW_ID"},
                                        {"NegSplitRowId", "NEG_SPLIT_ROW_ID"}
                                    };

        public int FundsXDedSplitMapId { get { return GetFieldInt("FUNDS_X_DED_SPLIT_MAP_ID"); } set { SetField("FUNDS_X_DED_SPLIT_MAP_ID", value); } }
        public int PolcvgRowId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        public int ClaimId { get { return GetFieldInt("POLCVG_ROW_ID"); } set { SetField("POLCVG_ROW_ID", value); } }
        public int DedPayTransId { get { return GetFieldInt("DED_PAY_TRANS_ID"); } set { SetField("DED_PAY_TRANS_ID", value); } }
        public int PosSplitRowId { get { return GetFieldInt("POS_SPLIT_ROW_ID"); } set { SetField("POS_SPLIT_ROW_ID", value); } }
        public int NegSplitRowId { get { return GetFieldInt("NEG_SPLIT_ROW_ID"); } set { SetField("NEG_SPLIT_ROW_ID", value); } }
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            base.Save();
        }

        internal FundsXDedSplitMap(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "FUNDS_X_DED_SPLIT_MAP";
            this.m_sKeyField = "FUNDS_X_DED_SPLIT_MAP_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            base.Initialize();
        }
    }
}
