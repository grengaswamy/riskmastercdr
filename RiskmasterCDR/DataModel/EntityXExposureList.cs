using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXSelfInsuredList.
	/// </summary>
	public class EntityXExposureList : DataCollection
	{
		internal EntityXExposureList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EXPOSURE_ROW_ID";
			this.SQLFromTable =	"ENTITY_EXPOSURE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EntityXExposure";
		}
		public new EntityXExposure this[int keyValue]{get{return base[keyValue] as EntityXExposure;}}
		public new EntityXExposure AddNew(){return base.AddNew() as EntityXExposure;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}