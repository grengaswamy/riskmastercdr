﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class ClaimXSiteLossList : DataCollection
    {
        internal ClaimXSiteLossList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "CLAIM_X_SITELOSS";
            this.TypeName = "ClaimXSiteLoss";
        }

        public new ClaimXSiteLoss this[int keyValue] { get { return base[keyValue] as ClaimXSiteLoss; } }
        public new ClaimXSiteLoss AddNew() { return base.AddNew() as ClaimXSiteLoss; }
        public ClaimXSiteLoss Add(ClaimXSiteLoss obj) { return base.Add(obj) as ClaimXSiteLoss; }
        public new ClaimXSiteLoss Add(int keyValue) { return base.Add(keyValue) as ClaimXSiteLoss; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
