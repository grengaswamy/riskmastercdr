using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCheckStockList.
	/// </summary>
	public class CheckStockList : DataCollection
	{
		internal CheckStockList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"STOCK_ID";
			this.SQLFromTable =	"CHECK_STOCK";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CheckStock";
		}
		public new CheckStock this[int keyValue]{get{return base[keyValue] as CheckStock;}}
		public new CheckStock AddNew(){return base.AddNew() as CheckStock;}
		public  CheckStock Add(CheckStock obj){return base.Add(obj) as CheckStock;}
		public new CheckStock Add(int keyValue){return base.Add(keyValue) as CheckStock;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}