using System;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;


namespace Riskmaster.DataModel
{
	/// <summary>
	/// Collection of ADM Tracking Tables.
	/// </summary>
	public class ADMTableList : DataRoot, IEnumerable
	{
		private Hashtable m_Tables = new Hashtable();
		private Hashtable m_UserNames = new Hashtable();
		private int m_LangId = 1033;
		const int ADM_TYPE = 468;
        
		internal ADMTableList(bool isLocked,Context context):base(isLocked, context){(this as IDataModel).Initialize(isLocked, context);}

		int Count{get{return m_Tables.Count;}}
		
		public IEnumerator GetEnumerator(){return m_Tables.GetEnumerator();}
		
		ADMTable this[string sysTableName]
		{
			get
			{
				if(m_Tables.Contains(sysTableName))
				{	
					ADMTable obj = (this.Context.Factory.GetDataModelObject("ADMTable",true) as ADMTable);
					obj.TableName = sysTableName;
					return obj;
				}
				throw new DataModelException(Globalization.GetString("ADMTableList.this[].get.Exception.ADMTableNotFound",this.Context.ClientId));
			}
		}
		public void LoadData()
		{
			m_Tables = new Hashtable();
			m_UserNames = new Hashtable();
			DbReader objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString,"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE=" + ADM_TYPE);
			while(objReader.Read())
				m_Tables.Add(objReader.GetString("SYSTEM_TABLE_NAME"),objReader.GetString("SYSTEM_TABLE_NAME"));
			objReader.Close();
		}

		public string GetTableName(int index) {return (string) new ArrayList(m_Tables.Values)[index];}
		public ArrayList TableUserNames()
		{
			ArrayList ret = new ArrayList();
			string[] retItem = new string[3];
                       
            //MITS 23632 Start vsharma65
            DbReader objReader = null;
            if (!this.Context.RMDatabase.Status || this.Context.RMUser == null)
            {
                string SQL = @"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME 
										FROM GLOSSARY, GLOSSARY_TEXT WHERE 
										GLOSSARY.GLOSSARY_TYPE_CODE={0}  AND 
										GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND 
										GLOSSARY_TEXT.LANGUAGE_CODE={1}";
                 objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, String.Format(SQL, ADM_TYPE, m_LangId));
            }
            else
            { 
                //if(this.Context.RMDatabase.Status)
                string SQL = @"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME 
										FROM GLOSSARY, GLOSSARY_TEXT WHERE 
										GLOSSARY.GLOSSARY_TYPE_CODE={0}  AND 
										GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND 
										GLOSSARY_TEXT.LANGUAGE_CODE={1}
                                        AND (SELECT COUNT(*)
                                        FROM USER_MEMBERSHIP, GROUP_PERMISSIONS  WHERE USER_MEMBERSHIP.USER_ID ={2} 
                                        AND USER_MEMBERSHIP.GROUP_ID = GROUP_PERMISSIONS.GROUP_ID
                                        AND FUNC_ID = (5000000+GLOSSARY.TABLE_ID*20)
                                        ) > 0";//deb Select have access tables 
                objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, String.Format(SQL, ADM_TYPE, m_LangId, this.Context.RMUser.UserId));
            }
            //MITS 23632 End here vsharma65
			while(objReader.Read())
			{
					retItem = new string[3];	
					retItem[0] = objReader.GetString("SYSTEM_TABLE_NAME") ;
					retItem[1]  =objReader.GetString("TABLE_NAME");
					retItem[2]  =objReader.GetInt("TABLE_ID").ToString();
					ret.Add(retItem);
			}
				objReader.Close();
		return ret;
		}

        //Start averma62 MITS - 27770
        public ArrayList TableUserNamesForPV()
        {
            ArrayList ret = new ArrayList();
            string[] retItem = new string[3];

            DbReader objReader = null;
            
            string SQL = @"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME 
									FROM GLOSSARY, GLOSSARY_TEXT WHERE 
									GLOSSARY.GLOSSARY_TYPE_CODE={0}  AND 
									GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND 
									GLOSSARY_TEXT.LANGUAGE_CODE={1}";
            objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, String.Format(SQL, ADM_TYPE, m_LangId));
            
            while (objReader.Read())
            {
                retItem = new string[3];
                retItem[0] = objReader.GetString("SYSTEM_TABLE_NAME");
                retItem[1] = objReader.GetString("TABLE_NAME");
                retItem[2] = objReader.GetInt("TABLE_ID").ToString();
                ret.Add(retItem);
            }
            objReader.Close();
            objReader.Dispose();
            return ret;
        }
        //End averma62 MITS - 27770
		public string SerializeTableList()
		{
			
			ArrayList arrList = this.TableUserNames();
			string sRet="";
			foreach(string[] arrItem in arrList)
			{
				int secBase = 5000000 + Conversion.ConvertStrToInteger(arrItem[2]) * 20;
				sRet += string.Format(@"<ADMTable>
											<SystemTableName>{0}</SystemTableName>
											<TableName>{1}</TableName>
											<TableId>{2}</TableId>
											<TableSecurityId>{3}</TableSecurityId>
											<SysCmd>{4}</SysCmd>
										</ADMTable>",arrItem[0],arrItem[1],arrItem[2],secBase.ToString() ,
					//TR 2309 Pankaj 2/27/06: Only view permission for Adm Tables giving error. Pass on SysCmd=1 in this case
					(this.Context.RMUser.IsAllowedEx(secBase,RMPermissions.RMO_VIEW) && !this.Context.RMUser.IsAllowedEx(secBase,RMPermissions.RMO_CREATE))? "1":"");
			}
			return string.Format("<ADMTableList>{0}</ADMTableList>",sRet);
		}
	}
}
