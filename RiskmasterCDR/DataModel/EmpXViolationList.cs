
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forEmpXViolationList.
	/// </summary>
	public class EmpXViolationList : DataCollection
	{
		internal EmpXViolationList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"VIOLATION_ID";
			this.SQLFromTable =	"EMP_X_VIOLATION";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EmpXViolation";
		}
		public new EmpXViolation this[int keyValue]{get{return base[keyValue] as EmpXViolation;}}
		public new EmpXViolation AddNew(){return base.AddNew() as EmpXViolation;}
		public  EmpXViolation Add(EmpXViolation obj){return base.Add(obj) as EmpXViolation;}
		public new EmpXViolation Add(int keyValue){return base.Add(keyValue) as EmpXViolation;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}