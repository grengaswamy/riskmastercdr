﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public class BankingStatusHistlList: DataCollection
	{
        internal BankingStatusHistlList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "BANK_HIST_ROW_ID";
            this.SQLFromTable = "BANK_STATUS_HIST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "BankingStatusHist";
		}
        public new BankingStatusHist this[int keyValue] { get { return base[keyValue] as BankingStatusHist; } }
        public new BankingStatusHist AddNew() { return base.AddNew() as BankingStatusHist; }
        public BankingStatusHist Add(ClaimStatusHist obj) { return base.Add(obj) as BankingStatusHist; }
        public new BankingStatusHist Add(int keyValue) { return base.Add(keyValue) as BankingStatusHist; }
        public BankingStatusHist LastStatusChange()
		{
			//BSB "fake SQL filter  1=0" may not have been set yet.
			// Note that SQL Filter is initialized to in order to indicate that the datacollection is not ready.
            string SQL = "SELECT MAX(BANK_HIST_ROW_ID) FROM BANK_STATUS_HIST ";
			if(this.SQLFilter =="")
				this.SQLFilter = " 1=0 ";
			SQL += "WHERE " +  this.SQLFilter;
			int lastChangeId = (int)Context.DbConnLookup.ExecuteInt(SQL);
			if(lastChangeId==0 && this.Count!=0)//Try to get "In Memory Only" item if available.
				foreach(object iKey in this.m_keySet.Keys)
					lastChangeId=Math.Min((int)iKey,lastChangeId);
			return this[lastChangeId];
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}