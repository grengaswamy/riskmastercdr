using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCustRateDetailList.
	/// </summary>
	public class CustRateDetailList : DataCollection
	{
		internal CustRateDetailList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CUST_RATE_DETAIL_ROW_ID";
			this.SQLFromTable =	"CUST_RATE_DETAIL";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CustRateDetail";
		}
		public new CustRateDetail this[int keyValue]{get{return base[keyValue] as CustRateDetail;}}
		public new CustRateDetail AddNew(){return base.AddNew() as CustRateDetail;}
		public  CustRateDetail Add(CustRateDetail obj){return base.Add(obj) as CustRateDetail;}
		public new CustRateDetail Add(int keyValue){return base.Add(keyValue) as CustRateDetail;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}