using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPhysicianCertificationList.
	/// </summary>
	public class PhysicianCertificationList : DataCollection
	{
		internal PhysicianCertificationList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CERT_ID";
			this.SQLFromTable =	"PHYS_CERTS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PhysicianCertification";
		}
		public new PhysicianCertification this[int keyValue]{get{return base[keyValue] as PhysicianCertification;}}
		public new PhysicianCertification AddNew(){return base.AddNew() as PhysicianCertification;}
		public  PhysicianCertification Add(PhysicianCertification obj){return base.Add(obj) as PhysicianCertification;}
		public new PhysicianCertification Add(int keyValue){return base.Add(keyValue) as PhysicianCertification;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}