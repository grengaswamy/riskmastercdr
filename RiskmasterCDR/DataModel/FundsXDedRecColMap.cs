﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("FUNDS_X_DED_REC_COL_MAP", "FUNDS_X_DED_REC_COL_MAP_ID")]
    public class FundsXDedRecColMap : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"FundsXDedRecColMapId","FUNDS_X_DED_REC_COL_MAP_ID"},
                                        {"ClaimId","CLAIM_ID"},
                                        {"PolCvgRowId","POLCVG_ROW_ID"},
                                        {"RecColTransId","REC_COL_TRANS_ID"},
                                        {"DedColTransId","DED_COL_TRANS_ID"}
                                    };

        public int FundsXDedRecColMapId { get { return GetFieldInt("FUNDS_X_DED_REC_COL_MAP_ID"); } set { SetField("FUNDS_X_DED_REC_COL_MAP_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        public int PolCvgRowId { get { return GetFieldInt("POLCVG_ROW_ID"); } set { SetField("POLCVG_ROW_ID", value); } }
        public int RecColTransId { get { return GetFieldInt("REC_COL_TRANS_ID"); } set { SetField("REC_COL_TRANS_ID", value); } }
        public int DedColTransId { get { return GetFieldInt("DED_COL_TRANS_ID"); } set { SetField("DED_COL_TRANS_ID", value); } }
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            base.Save();
        }

        internal FundsXDedRecColMap(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "FUNDS_X_DED_REC_COL_MAP";
            this.m_sKeyField = "FUNDS_X_DED_REC_COL_MAP_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            base.Initialize();
        }
    }
}
