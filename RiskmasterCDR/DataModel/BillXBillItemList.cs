
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXDsbrsmntList.
	/// </summary>
	public class BillXBillItemList : DataCollection
	{
		internal BillXBillItemList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"BILLING_ITEM_ROWID";
			this.SQLFromTable =	"BILL_X_BILL_ITEM";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXBillItem";
		}
		public new BillXBillItem this[int keyValue]{get{return base[keyValue] as BillXBillItem;}}
		public new BillXBillItem AddNew(){return base.AddNew() as BillXBillItem;}
        public BillXBillItem Add(BillXBillItem obj) { return base.Add(obj, obj.KeyFieldValue > 0) as BillXBillItem; }
		public new BillXBillItem Add(int keyValue){return base.Add(keyValue) as BillXBillItem;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}