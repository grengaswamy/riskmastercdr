﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class MedInfoList : DataCollection
    {
        internal MedInfoList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"MEDICATION_ROW_ID";
            this.SQLFromTable = "EVENT_X_MEDICATION_PSO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "MedicationInfo";
		}
        public new MedicationInfo this[int keyValue] { get { return base[keyValue] as MedicationInfo; } }
        public new MedicationInfo AddNew() { return base.AddNew() as MedicationInfo; }
        public MedicationInfo Add(MedicationInfo obj) { return base.Add(obj) as MedicationInfo; }
        public new MedicationInfo Add(int keyValue) { return base.Add(keyValue) as MedicationInfo; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
