using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Linq;

namespace Riskmaster.DataModel

{
	/// <summary>
	/// Summary description for DataSimpleList.
	/// </summary>
	public class DataSimpleList : DataRoot, IEnumerable
	{

		private  Hashtable m_values = new Hashtable();
		private string m_valueFieldName="";
		private string m_valueTableName = "";
		private int m_keyFieldValue=0;
		private string m_keyFieldName="";
		private bool m_isInitialized = false;

		/// <summary>
		/// This constructor works for the default case in which a single key field suffices 
		/// to identify the contents of the SimpleList.  In all other cases, the Load, Save and Delete
		/// methods will need to be passed custom SQL.
		/// </summary>
		/// <param name="valueFieldName">DB Field containing the values of this collection.</param>
		/// <param name="valueTableName">DB Table name where the values are stored.</param>
		/// <param name="keyFieldName">DB Table foreign key into the DB Table used 
		/// to identify all rows that should be in this collection.</param>
		internal DataSimpleList(bool isLocked, Context context):base(isLocked, context){}
		/*
		public DataSimpleList(string valueFieldName, string valueTableName):base(true){Initialize(valueFieldName,valueTableName,"");}
		public DataSimpleList(string valueFieldName, string valueTableName, string keyFieldName):base(true){Initialize(valueFieldName,valueTableName,keyFieldName);}
		*/
		internal void Initialize(string valueFieldName, string valueTableName, string keyFieldName)
		{
			if (m_isInitialized)
				return;
			m_keyFieldName = keyFieldName;
			m_valueTableName = valueTableName;
			m_valueFieldName = valueFieldName;
			m_isInitialized = true;
		}

		//Added for Schema Generation.
		public  string  TableName{get{return m_valueTableName;}}
		public   string FieldName{get{return m_valueFieldName;}}
        //avipinsrivas Start : Worked for Jira-340
        //Need to set from PersonInvolved if want to skip SetKeyValue() Method.
        public bool NeedToSetKeyValue { get { return m_keyFieldValue <= 0; } }
        //avipinsrivas End
		internal bool IsInitialized{get{return m_isInitialized;}}
		public  void ClearAll()
		{
			if (m_values.Count > 0)
			{
				m_values= new Hashtable();
				base.m_DataChanged = true;
			}
		}

		//ONLY For use in the situation that the parent record is new
		// and items have been added to this list before the 
		// key value was generated using GetNextUID.
		internal void SetKeyValue(int keyValue)
		{
			//Event if list is stale, don't allow reset of keyvalue, the whole list should be Reloaded.
			if (m_keyFieldValue !=0)
                throw new DataModelException(Globalization.GetString("DataSimpleList.SetKeyValue.Exception.ValueAlreadySet", this.Context.ClientId));
			else
				m_keyFieldValue = keyValue;

			//BSB 09.21.2005 Bug fix for simple list values not saved on new record.
			//(LoadSimpleList is never called but we are not truly "stale" in this sense either.
			// Incorrect Stale flag was causing us to lose simplelist values from new records.
			(this as IDataModel).IsStale = false;

		}
		internal void LoadSimpleList(int keyValue)
		{
			LoadSimpleList(String.Format("SELECT {0} FROM {1} WHERE {2} = {3}",m_valueFieldName, m_valueTableName,m_keyFieldName,keyValue), 0);
			m_keyFieldValue = keyValue;
		}
		internal void LoadSimpleList(string SQL){LoadSimpleList(SQL, 0);}
		internal void LoadSimpleList(string SQL, object columnNameIdx)
		{
			DbReader rdr = null;
			object objValue =0;
			try
			{
				ClearAll();
				
				rdr = DbFactory.GetDbReader(base.Context.DbConn.ConnectionString, SQL);
				
				while( rdr.Read())
				{
					if(columnNameIdx is int)
						objValue = rdr.GetValue((int)columnNameIdx);
					if(columnNameIdx is string)
						objValue = rdr.GetValue((string)columnNameIdx);
					if((!(objValue is int) || ((int)objValue != 0)) && (!(objValue is System.DBNull)) )
						Add(objValue);
				}
				m_keyFieldValue =0;
				m_DataChanged = false;
				(this as IDataModel).IsStale = false;
			}
			catch(Exception e)
			{
                throw new RMAppException(Globalization.GetString("DataSimpleList.LoadSimpleList.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if (rdr != null)
					rdr.Close();
				rdr = null;
			}
		}
		internal void SaveSimpleList()
		{
			string SQL = String.Format("INSERT INTO {0} ({1}, {2}) VALUES({3},{4})",m_valueTableName,m_keyFieldName,m_valueFieldName,m_keyFieldValue, "{SIMPLELISTVALUE}");
			SaveSimpleList(SQL);
		}
		internal void SaveSimpleList(string SQLTemplate)
		{
			string SQL = "";
			if((this as IDataModel).IsStale)
				return;

			if(!(this as IDataModel).DataChanged)
				return;

			try
			{
				
				//BSB 07.29.2004 (TKR Spotted Issue)
				//Fix for throwing error during save of Multi-Value Supplemental, AdmTracking and Jurisdictionals.
				// In these cases the "delete" is handled by the caller before entering this function.  
				// This is because there is no single key field for those cases it takes a "supp_field_id" and a "row_id" to uniquely
				// specify a row.  This is not generally an error condition and we can just avoid the extra call if the target table has no single key.
				if(this.m_keyFieldValue !=0)
					this.DeleteSimpleList();

				SQL = SQLTemplate.Replace("{SIMPLELISTVALUE}", "~SIMPLELISTVALUE~");
				DbCommand DbCmd = base.Context.DbConn.CreateCommand();
				DbCmd.CommandText = SQL;

				DbParameter DbParam = DbCmd.CreateParameter();
				DbParam.ParameterName = "SIMPLELISTVALUE";
				DbCmd.Parameters.Add(DbParam);

				if(Context.DbTrans !=null)
					DbCmd.Transaction = Context.DbTrans;
				
				foreach( object v in m_values.Values)
				{
					DbParam.Value = v;
					DbCmd.ExecuteNonQuery();
				}
				m_DataChanged = false;
			}
            catch (Exception e) { throw new RMAppException(Globalization.GetString("DataSimpleList.SaveSimpleList.Exception", this.Context.ClientId), e); }
		}
		internal void DeleteSimpleList()
		{
			if (m_keyFieldValue == 0)
                throw new RMAppException(Globalization.GetString("DataSimpleList.DeletSimpleList.Exception", this.Context.ClientId)); //Key value unavailable... use custom SQL.
			DeleteSimpleList(String.Format("DELETE FROM {0} WHERE {1} = {2}",m_valueTableName, m_keyFieldName, m_keyFieldValue));
		}
		internal void DeleteSimpleList(string SQL)
		{
			try
			{
					if(Context.DbTrans !=null)
						base.Context.DbConn.ExecuteNonQuery(SQL,Context.DbTrans);
					else
						base.Context.DbConn.ExecuteNonQuery(SQL);

//					m_DataChanged = false;
			}
            catch (Exception e) { throw new RMAppException(Globalization.GetString("DataSimpleList.DeleteSimpleList.Exception", this.Context.ClientId), e); }
		}

		public IEnumerator GetEnumerator(){return m_values.GetEnumerator();	}

		public void Add(object targetValue)
		{
			base.m_DataChanged= true;
			m_values.Add(targetValue.ToString() ,targetValue );
		}

		public int Count{get{return m_values.Count;}}
		//Added for Froi Migration- Mits 33585,33586,33587
        public object this[object indexKey]
        {
            get
            {
                bool success;
                int index = Conversion.CastToType<int>(indexKey.ToString(), out success);
                if (success)
                {
                    int count = 1;
                    string key = string.Empty;
                    foreach (string itemKey in m_values.Keys)
                    {
                        if (count == index)
                        {
                            key = itemKey;
                            break;
                        }
                        count++;
                    }
                    if (string.IsNullOrEmpty(key))
                    {
                        return null;
                    }
                    else
                    {
                        return m_values[key];
                    }
                }
                else
                {
                    return m_values[indexKey];
                }
            }
        }
		public void Remove(object indexKey)
		{
			m_values.Remove(indexKey.ToString());
			base.m_DataChanged = true;
		}
		override public string ToString()
		{
			string ret="";
			foreach (object o in m_values.Values)
				ret += o.ToString().ToUpper() + " ";
			
			return ret.TrimEnd(' ');		}
	}
}
