
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forAcctRecDetailList.
	/// </summary>
	public class AcctRecDetailList : DataCollection
	{
		internal AcctRecDetailList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"AR_DETAIL_ROW_ID";
			this.SQLFromTable =	"ACCT_REC_DETAIL";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "AcctRecDetail";
		}
		public new AcctRecDetail this[int keyValue]{get{return base[keyValue] as AcctRecDetail;}}
		public new AcctRecDetail AddNew(){return base.AddNew() as AcctRecDetail;}
		public  AcctRecDetail Add(AcctRecDetail obj){return base.Add(obj) as AcctRecDetail;}
		public new AcctRecDetail Add(int keyValue){return base.Add(keyValue) as AcctRecDetail;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}