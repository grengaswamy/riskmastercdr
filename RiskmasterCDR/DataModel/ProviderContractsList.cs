using System;

namespace Riskmaster.DataModel
{
	///************************************************************** 
	///* $File		: ProviderContractsList.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 28-Jan-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	/// Provides the list of Provider Contracts.
	/// </summary>
	public class ProviderContractsList: DataCollection
	{
		/// <summary>
		///	Initializes the Provider Contracts List class
		/// </summary>
		/// <param name="p_bIsLocked">Whether the current Provider Contract object is locked</param>
		/// <param name="p_objContext">Global context information for the current Provider Contract object</param>
		internal ProviderContractsList(bool p_bIsLocked,Context p_objContext):base(p_bIsLocked, p_objContext)
		{
			(this as IDataModel).Initialize(p_bIsLocked, p_objContext);
			this.SQLKeyColumn =	"PRVD_CONT_ROW_ID";
			this.SQLFromTable =	"PROVIDER_CONTRACTS";
			//this.SQLFilter = "???" //Must be set by parent at runtime for current record.
			this.TypeName = "ProviderContracts";
		}

		/// <summary>
		///	Returns the provider contract object instance at the given key position
		/// </summary>
		/// <param name="p_iKeyValue">key position</param>
		public new ProviderContracts this[int p_iKeyValue]{get{return base[p_iKeyValue] as ProviderContracts;}}

		/// <summary>
		///	Add a new provider contract object to the list
		/// </summary>
		public new ProviderContracts AddNew(){return base.AddNew() as ProviderContracts;}
		
		/// <summary>
		///	Adds the provider contract object to the list
		/// </summary>
		/// <param name="p_objProviderContract">Provider Contract object</param>
		public  ProviderContracts Add(ProviderContracts p_objProviderContract){return base.Add(p_objProviderContract) as ProviderContracts;}

		/// <summary>
		///	Adds the provider contract object at the given key position
		/// </summary>
		/// <param name="p_iKeyValue">key position</param>
		public new ProviderContracts Add(int p_iKeyValue){return base.Add(p_iKeyValue) as ProviderContracts;}
	}
}
