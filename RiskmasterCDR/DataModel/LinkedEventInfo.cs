﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("EVENT_X_LINK_EVT_INFO_PSO", "LNKEVT_ROW_ID", "EventLnkReasonCode")]
    public class LinkedEventInfo : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"AddedByUser", "ADDED_BY_USER"},
                                                        {"LnkEventRowId", "LNKEVT_ROW_ID"},
                                                        {"EvtPsoRowId", "EVT_PSO_ROW_ID"},
                                                        {"LnkEventNumber", "PSO_LNK_EVTNUM"},
                                                        {"EventLnkReasonCode", "PSO_LINK_RES_CODE"},
                                                        {"EventLnkReasonText", "PSO_LINK_RES_MEMO"},
														
		};

        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public int LnkEventRowId { get { return GetFieldInt("LNKEVT_ROW_ID"); } set { SetField("LNKEVT_ROW_ID", value); } }
        public int EvtPsoRowId { get { return GetFieldInt("EVT_PSO_ROW_ID"); } set { SetField("EVT_PSO_ROW_ID", value); } }
        public string LnkEventNumber { get { return GetFieldString("PSO_LNK_EVTNUM"); } set { SetField("PSO_LNK_EVTNUM", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_LINK_REASON")]
        public int EventLnkReasonCode { get { return GetFieldInt("PSO_LINK_RES_CODE"); } set { SetField("PSO_LINK_RES_CODE", value); } }
        public string EventLnkReasonText { get { return GetFieldString("PSO_LINK_RES_MEMO"); } set { SetField("PSO_LINK_RES_MEMO", value); } }

        #endregion

        internal LinkedEventInfo(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();

        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "EVENT_X_LINK_EVT_INFO_PSO";
            this.m_sKeyField = "LNKEVT_ROW_ID";
            this.m_sFilterClause = string.Empty;
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            this.m_sParentClassName = "EventPSO";
            //Add all object Children into the Children collection from our "sChildren" list.
            base.Initialize();

        }

    }
}
