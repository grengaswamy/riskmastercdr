using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forDisClassWaitList.
	/// </summary>
	public class DisClassWaitList : DataCollection
	{
		internal DisClassWaitList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"WAIT_ROW_ID";
			this.SQLFromTable =	"DIS_CLASS_WAIT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "DisClassWait";
		}
		public new DisClassWait this[int keyValue]{get{return base[keyValue] as DisClassWait;}}
		public new DisClassWait AddNew(){return base.AddNew() as DisClassWait;}
		public  DisClassWait Add(DisClassWait obj){return base.Add(obj) as DisClassWait;}
		public new DisClassWait Add(int keyValue){return base.Add(keyValue) as DisClassWait;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}