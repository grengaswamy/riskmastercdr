﻿
namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_POL_DED_AGG_LIMIT_HIST", "DED_AGG_LIMIT_HIST_ID")]
    public class ClaimXPolDedAggLimitHist : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"DedAggLimitHistId", "DED_AGG_LIMIT_HIST_ID"},
                                        {"ClaimXPolDedAggLimitId", "CLAIM_X_POL_DED_AGG_LIMIT_ID"},
                                        {"CovGroupId", "COV_GROUP_CODE"},
                                        {"AggLimitAmt", "AGG_LIMIT_AMT"},
                                        {"DateAggLimitChgd", "DATE_AGG_LIMIT_CHGD"},
                                        {"AggLimitChgdBy", "AGG_LIMIT_CHGD_BY"},
                                        {"SirDedPreventAmt", "SIR_DED_PEREVENT_AMT"},
                                        {"ExcludeExpenseFlag", "EXCLUDE_EXPENSE_FLAG"} //ExpenseFlagAddition - nbhatia6 - 07/03/14
                                    };

        public int DedAggLimitHistId { get { return GetFieldInt("DED_AGG_LIMIT_HIST_ID"); } set { SetField("DED_AGG_LIMIT_HIST_ID", value); } }
        public int ClaimXPolDedAggLimitId { get { return GetFieldInt("CLAIM_X_POL_DED_AGG_LIMIT_ID"); } set { SetField("CLAIM_X_POL_DED_AGG_LIMIT_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COV_GROUP_CODE")]
        public int CovGroupId { get { return GetFieldInt("COV_GROUP_CODE"); } set { SetField("COV_GROUP_CODE", value); } }
        public string DateAggLimitChgd { get { return GetFieldString("DATE_AGG_LIMIT_CHGD"); } set { SetField("DATE_AGG_LIMIT_CHGD", value); } }
        public string AggLimitChgdBy { get { return GetFieldString("AGG_LIMIT_CHGD_BY"); } set { SetField("AGG_LIMIT_CHGD_BY", value); } }
        public double AggLimitAmt { get { return GetFieldDouble("AGG_LIMIT_AMT"); } set { SetField("AGG_LIMIT_AMT", value); } }
        public double SirDedPreventAmt { get { return GetFieldDouble("SIR_DED_PEREVENT_AMT"); } set { SetField("SIR_DED_PEREVENT_AMT", value); } }
        //ExpenseFlagAddition - nbhatia6 - 07/03/14
        public bool ExcludeExpenseFlag { get { return GetFieldBool("EXCLUDE_EXPENSE_FLAG"); } set { SetField("EXCLUDE_EXPENSE_FLAG", value); } }
        #endregion

        internal ClaimXPolDedAggLimitHist(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}

        new private void Initialize()
        {
            this.m_sTableName = "CLAIM_X_POL_DED_AGG_LIMIT_HIST";
            this.m_sKeyField = "DED_AGG_LIMIT_HIST_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);

            this.m_sParentClassName = "ClaimXPolDedAggLimit";
            base.Initialize(); 

        }
    }
}
