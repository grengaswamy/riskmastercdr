using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for ClaimProgressNoteList.
	/// </summary>
	public class ClaimProgressNoteList : DataCollection
	{
		internal ClaimProgressNoteList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CL_PROG_NOTE_ID";
			this.SQLFromTable =	"CLAIM_PRG_NOTE";
			this.TypeName = "ClaimProgressNote";
		}
		public new ClaimProgressNote this[int keyValue]{get{return base[keyValue] as ClaimProgressNote;}}
		public new ClaimProgressNote AddNew(){return base.AddNew() as ClaimProgressNote;}
		public  ClaimProgressNote Add(ClaimProgressNote obj){return base.Add(obj) as ClaimProgressNote;}
		public new ClaimProgressNote Add(int keyValue){return base.Add(keyValue) as ClaimProgressNote;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}