
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXInvoiceDetList.
	/// </summary>
	public class BillXInvoiceDetList : DataCollection
	{
		internal BillXInvoiceDetList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"INVOICE_DET_ROWID";
			this.SQLFromTable =	"BILL_X_INVOICE_DET";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXInvoiceDet";
		}
		public new BillXInvoiceDet this[int keyValue]{get{return base[keyValue] as BillXInvoiceDet;}}
		public new BillXInvoiceDet AddNew(){return base.AddNew() as BillXInvoiceDet;}
		public  BillXInvoiceDet Add(BillXInvoiceDet obj){return base.Add(obj) as BillXInvoiceDet;}
		public new BillXInvoiceDet Add(int keyValue){return base.Add(keyValue) as BillXInvoiceDet;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}