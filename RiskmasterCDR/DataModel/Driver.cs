﻿
using System;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary of PiDriver  Class.
       /// </summary>
    /// ///Created by skhare7 for PolicyInterface
    [Riskmaster.DataModel.Summary("DRIVER", "DRIVER_ROW_ID")]
    public class Driver : DataObject
    {
        public override string Default
        {
            get
            {
                if (this.DriverRowId > 0)
                    return Context.LocalCache.GetEntityLastFirstName(this.DriverEId);
                return "";
            }
        }
        private string[,] sChildren = {	{"DriverEntity","Entity"}
           };
        
        #region Database Field List
        private string[,] sFields = {
														{"DriverEId", "DRIVER_EID"},
														{"DriverTypeCode", "DRIVER_TYPE"},
														{"LicenceDate", "LICENCE_DATE"},
                                                   
                                                        {"DriversLicNo", "LICENCE_NUMBER"},
                                                        {"DriverRowId", "DRIVER_ROW_ID"},
                                                       {"MaritalStatCode", "MARITAL_STAT_CODE"},
                                                        // mits 35925 start
                                                        {"InsuranceLine", "PS_INS_LINE"},
                                                        {"RiskLocation", "PS_RISK_LOC"},
                                                        {"RiskSubLocation","PS_RISK_SUB_LOC"},
                                                        {"Product", "PS_PRODUCT"},                                                                                                                {"MaritalStatCode", "MARITAL_STAT_CODE"},
                                                        {"DriverID", "PS_DRIVER_ID"},
                                                        {"RecordStatus", "PS_RECORD_STATUS"},
                                                        //mits 35925 end
														
		};

        public int DriverEId
        {
            get
            {
                return GetFieldInt("DRIVER_EID");
            } 
            set
            {
                SetField("DRIVER_EID", value);
                SetFieldAndNavTo("DRIVER_EID", value, "DriverEntity");
            } 
        }
    //    [ExtendedTypeAttribute(RMExtType.Code, "DRIVER_TYPE")]
        public int DriverTypeCode { get { return GetFieldInt("DRIVER_TYPE"); } set { SetField("DRIVER_TYPE", value); } }
        public string LicenceDate { get { return GetFieldString("LICENCE_DATE"); } set { SetField("LICENCE_DATE", Conversion.GetDate(value)); } }

        public string DriversLicNo { get { return GetFieldString("LICENCE_NUMBER"); } set { SetField("LICENCE_NUMBER", value); } }
        public int DriverRowId { get { return GetFieldInt("DRIVER_ROW_ID"); } set { SetField("DRIVER_ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "MARITAL_STATUS")]
        public int MaritalStatCode { get { return GetFieldInt("MARITAL_STAT_CODE"); } set { SetField("MARITAL_STAT_CODE", value); } }
        // mits 35925 start
        public string InsuranceLine { get { return GetFieldString("PS_INS_LINE"); } set { SetField("PS_INS_LINE", value); } }
        public string RiskLocation { get { return GetFieldString("PS_RISK_LOC"); } set { SetField("PS_RISK_LOC", value); } }
        public string RiskSubLocation { get { return GetFieldString("PS_RISK_SUB_LOC"); } set { SetField("PS_RISK_SUB_LOC", value); } }
        public string Product { get { return GetFieldString("PS_PRODUCT"); } set { SetField("PS_PRODUCT", value); } }
        public int DriverID { get { return GetFieldInt("PS_DRIVER_ID"); } set { SetField("PS_DRIVER_ID", value); } }
        public string RecordStatus { get { return GetFieldString("PS_RECORD_STATUS"); } set { SetField("PS_RECORD_STATUS", value); } }
        //mits 35925 end
        #endregion

        internal Driver(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
                      this.SetFieldAndNavTo("DRIVER_EID", this.DriverEId, "DriverEntity", true);
        }
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            
            if (childName == "DriverEntity")
            {
                
                if ((childValue as DataObject).WillSave())
                    (childValue as Entity).EntityTableId = Context.LocalCache.GetTableId("DRIVERS");
                (childValue as IPersistence).Save();
                this.m_Fields["DRIVER_EID"] = (childValue as Entity).EntityId;
            }
            base.OnChildPreSave(childName, childValue);
        }
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            if (childValue is IPersistence)
               (childValue as IPersistence).Save();
        }
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {               
                case "Entity": //Is Entity
                    Entity objEnt = (obj as Entity);
                    objEnt.EntityTableId = Context.LocalCache.GetTableId("DRIVERS");
                    //avipinsrivas Start : Worked for Jira-340
                    if (objEnt.Context.InternalSettings.SysSettings.UseEntityRole)
                        objEnt.LockEntityTableChange = false;
                    else
                        objEnt.LockEntityTableChange = true;
                    //avipinsrivas End

                    //no real way to lock entity class navigation from internal access.
                    break;

                default:
                    //No default process.
                    break;
            }
        }
        protected override void OnPreCommitDelete()
        {
            // BSB Go Ahead and Flag our Entity since by virtue of being in this particular
            // override we KNOW that the user initiated a deletion against us directly.
            //avipinsrivas start : Worked on JIRA - 7767
            if (this.Context.InternalSettings.SysSettings.UseEntityRole)
                base.Delete();
            else
                this.DriverEntity.Delete(); // This call simply flags the entity in the RMX database.
            //avipinsrivas end
            // True record deletion is not supported for entities within the 
            // base product by ANY MEANS.
        }
        public Entity DriverEntity
        {
            get
            {
                Entity objItem = base.m_Children["DriverEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("DriverEntity", (objItem as DataObject), this.DriverEId) as Entity;
                return objItem;
            }
        }
        protected override void Clear()
        {
            //			m_PiRowId = 0;
            base.Clear();
        }
        public override void Refresh()
        {
            int l = this.DriverRowId;
            //				int l1 = m_PiRowId;
            int lEID = this.DriverRowId;

            base.Refresh();

            this.DriverRowId = l;
            //				m_PiRowId = l1;
            this.DriverRowId = lEID;
        }
        new private void Initialize()
        {

            this.m_sTableName = "DRIVER";
            this.m_sKeyField = "DRIVER_ROW_ID";
            this.m_sFilterClause = "";

            
            base.InitFields(sFields);
           
            base.InitChildren(sChildren);
            this.m_sParentClassName = "";
            base.Initialize();  
            base.InitFields(sFields);
      
        
         
            base.Initialize(); 

        }
        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        //Amandeep Driver Supplementals added
        internal override string OnBuildSuppTableName()
        {
            return "DRIVER_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        //protected override void OnBuildNewUniqueId()
        //{
        //    this.KeyFieldValue = Context.GetNextUID("DRIVERS");
        //}
        #endregion
       
     
    }
}