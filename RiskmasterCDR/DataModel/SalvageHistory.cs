﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("SalvageHistory", "SALVAGE_HIST_ROW_ID")]
 public   class SalvageHistory: DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        		
														{"SalvageHistRowId", "SALVAGE_HIST_ROW_ID"},
														{"salvagerowid", "SALVAGE_ROW_ID"},
                                                        {"InPosEid", "POSSESSION_OF"},
														{"SalvageAddress","SALVAGE_ADDRESS"},
                                                        {"DailyFees","DAILYFEES"},
                                                        {"MoveDate","MOVE_DATE"}

		};

        
        public int SalvageHistRowId { get { return GetFieldInt("SALVAGE_HIST_ROW_ID"); } set { SetField("SALVAGE_HIST_ROW_ID", value); } }
        public int salvagerowid { get { return GetFieldInt("SALVAGE_ROW_ID"); } set { SetField("SALVAGE_ROW_ID", value); } }
      [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
      public int InPosEid { get { return GetFieldInt("POSSESSION_OF"); } set { SetField("POSSESSION_OF", value); } }
      [ExtendedTypeAttribute(RMExtType.EntityAddr)]
        public int SalvageAddress { get { return GetFieldInt("SALVAGE_ADDRESS"); } set { SetField("SALVAGE_ADDRESS", value); } }
        
        public string MoveDate { get { return GetFieldString("MOVE_DATE"); } set { SetField("MOVE_DATE", value); } }
      
         public double DailyFees { get { return GetFieldDouble("DAILYFEES"); } set { SetField("DAILYFEES", value); } }
        
        #endregion

        #region Child Implementation
       
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
         
        }
        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
              
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }


        //Child Property Accessors
        

        #endregion
        internal SalvageHistory(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            this.m_sTableName = "SALVAGEHISTORY";
            this.m_sKeyField = "SALVAGE_HIST_ROW_ID";
            this.m_sFilterClause = "";

        
            base.InitFields(sFields);
         
            this.m_sParentClassName = "Salvage";
            base.Initialize(); 
        }
        
    }
}


