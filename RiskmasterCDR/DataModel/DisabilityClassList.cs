using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forDisabilityClassList.
	/// </summary>
	public class DisabilityClassList : DataCollection
	{
		internal DisabilityClassList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CLASS_ROW_ID";
			this.SQLFromTable =	"DISABILITY_CLASS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "DisabilityClass";
		}
		public new DisabilityClass this[int keyValue]{get{return base[keyValue] as DisabilityClass;}}
		public new DisabilityClass AddNew(){return base.AddNew() as DisabilityClass;}
		public  DisabilityClass Add(DisabilityClass obj){return base.Add(obj) as DisabilityClass;}
		public new DisabilityClass Add(int keyValue){return base.Add(keyValue) as DisabilityClass;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}