﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public class ClaimXLiabilityLossList: DataCollection
    {
        internal ClaimXLiabilityLossList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "CLAIM_X_LIABILITYLOSS";
            this.TypeName = "ClaimXLiabilityLoss";
        }

        public new ClaimXLiabilityLoss this[int keyValue] { get { return base[keyValue] as ClaimXLiabilityLoss; } }
        public new ClaimXLiabilityLoss AddNew() { return base.AddNew() as ClaimXLiabilityLoss; }
        public ClaimXLiabilityLoss Add(ClaimXLiabilityLoss obj) { return base.Add(obj) as ClaimXLiabilityLoss; }
        public new ClaimXLiabilityLoss Add(int keyValue) { return base.Add(keyValue) as ClaimXLiabilityLoss; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }
}
