
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forEmpXViolationList.
	/// </summary>
	public class EntityList : DataCollection
	{
		internal EntityList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"ENTITY_ID";
			this.SQLFromTable =	"ENTITY";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Entity";
		}
		public new Entity this[int keyValue]{get{return base[keyValue] as Entity;}}
		public new Entity AddNew(){return base.AddNew() as Entity;}
		public  Entity Add(Entity obj){return base.Add(obj) as Entity;}
		public new Entity Add(int keyValue){return base.Add(keyValue) as Entity;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}