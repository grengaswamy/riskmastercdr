﻿using System;
using System.Data;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Scripting;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;   // JP 10.06.2005   For ReaderWriterLock class.

namespace Riskmaster.DataModel
{

    internal class XmlUtilityFunctions
    {
        /// <summary>
        /// Extracts instance information from the XmlDocument
        /// </summary>
        /// <param name="propertyStore"></param>
        /// <param name="rootTagName"></param>
        /// <param name="xmlValidator"></param>
        internal static void ExtractInstanceInfo(ref XmlDocument propertyStore, string rootTagName, ref XmlReader xmlValidator)
        {
            XmlReaderSettings xmlRdrSettings = new XmlReaderSettings();

            xmlRdrSettings.ValidationType = ValidationType.Schema;
            xmlRdrSettings.Schemas = SerializationContext.XML_SCHEMAS;

            XmlTextReader xmlTxtRdr = new XmlTextReader(new System.IO.StringReader(propertyStore.OuterXml));

            //Skip the schema checking for R5
            //xmlValidator = XmlReader.Create(xmlTxtRdr, xmlRdrSettings);

            SerializationContext.RWLock.AcquireReaderLock(-1);

            //Useless loop for reading through XmlReader values
           /* while (xmlValidator.Read())  Disabling schema validation
            {
                ;
            }*/
            //Strip off top level Instance wrapper.
            propertyStore = Utilities.XPointerDoc(String.Format("/Instance/{0}", rootTagName), propertyStore);
        }//method: ExtractInstanceInfo()
    }
}
