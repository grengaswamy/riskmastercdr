﻿using System;
namespace Riskmaster.DataModel
{
    public class CaseManagementList:DataCollection
    {
        internal CaseManagementList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "CASEMGT_ROW_ID";
            this.SQLFromTable = "CASE_MANAGEMENT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "CaseManagement";
		}
        public new CaseManagement this[int keyValue] { get { return base[keyValue] as CaseManagement; } }
        public new CaseManagement AddNew() { return base.AddNew() as CaseManagement; }
       public CaseManagement Add(CmXCmgrHist obj) { return base.Add(obj) as CaseManagement; }
        public new CaseManagement Add(int keyValue) { return base.Add(keyValue) as CaseManagement; }
        internal void SetObject(int keyValue, CaseManagement objCaseManagement) { base.m_keySet[keyValue] = objCaseManagement; }
		//Find the "most current" Case Manager Record - but don't create if none found.
        public CaseManagement GetCurrentCaseManagement()
		{

			if(this.m_keySet.Count==0)
				return null;

			if(this.SQLFilter=="")
				return null;

            int key = Context.DbConnLookup.ExecuteInt("SELECT CASEMGT_ROW_ID FROM CASEMANAGEMENT WHERE " + this.SQLFilter);
			
			if(key !=0)
				return this[key];
			else
			{ //Attempt to pick the last case mangement added.
				int maxRowId=99999; 
				foreach(int curKey in this.m_keySet.Keys)
					if(maxRowId==99999 || curKey>maxRowId)
						maxRowId=curKey;
				return this[maxRowId];
			}
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
