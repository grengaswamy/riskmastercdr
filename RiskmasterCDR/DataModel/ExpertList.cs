using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forExpertList.
	/// </summary>
	public class ExpertList : DataCollection
	{
		internal ExpertList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EXPERT_ROW_ID";
			this.SQLFromTable =	"EXPERT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Expert";
		}
		public new Expert this[int keyValue]{get{return base[keyValue] as Expert;}}
		public new Expert AddNew(){return base.AddNew() as Expert;}
		public  Expert Add(Expert obj){return base.Add(obj) as Expert;}
		public new Expert Add(int keyValue){return base.Add(keyValue) as Expert;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}