﻿/**********************************************************************************************
 *   Date     |  JIRA   | Programmer | Description                                            *
 **********************************************************************************************
 * 04/27/2013 | 7810  | svinchurkar   |Created (Entity Approval and Payment Approval )
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class HoldReasonList : DataCollection
    {
        internal HoldReasonList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "HOLD_REASON";

            this.TypeName = "HoldReason";
        }
        public new HoldReason this[int keyValue] { get { return base[keyValue] as HoldReason; } }
        public new HoldReason AddNew() { return base.AddNew() as HoldReason; }
        public HoldReason Add(HoldReason obj) { return base.Add(obj) as HoldReason; }
        public new HoldReason Add(int keyValue) { return base.Add(keyValue) as HoldReason; }


        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
