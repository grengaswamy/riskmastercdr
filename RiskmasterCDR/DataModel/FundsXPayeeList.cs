﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
 public  class FundsXPayeeList : DataCollection
    {
        internal FundsXPayeeList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "PAYEE_ROW_ID";
            this.SQLFromTable = "FUNDS_X_PAYEE";

            this.TypeName = "FundsXPayee";
		}
        public new FundsXPayee this[int keyValue] { get { return base[keyValue] as FundsXPayee; } }
        public new FundsXPayee AddNew() { return base.AddNew() as FundsXPayee; }
        public FundsXPayee Add(FundsAutoXPayee obj) { return base.Add(obj) as FundsXPayee; }
        public new FundsXPayee Add(int keyValue) { return base.Add(keyValue) as FundsXPayee; }

		
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
