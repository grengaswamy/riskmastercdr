using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forVehicleXAccDateList.
	/// </summary>
	public class VehicleXAccDateList : DataCollection
	{
		internal VehicleXAccDateList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"VEH_X_ACC_DATE_ROW_ID";
			this.SQLFromTable =	"VEHICLE_X_ACC_DATE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "VehicleXAccDate";
		}
		public new VehicleXAccDate this[int keyValue]{get{return base[keyValue] as VehicleXAccDate;}}
		public new VehicleXAccDate AddNew(){return base.AddNew() as VehicleXAccDate;}
		public  VehicleXAccDate Add(VehicleXAccDate obj){return base.Add(obj) as VehicleXAccDate;}
		public new VehicleXAccDate Add(int keyValue){return base.Add(keyValue) as VehicleXAccDate;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}