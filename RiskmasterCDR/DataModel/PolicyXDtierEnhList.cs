
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXDtierEnhList.
	/// </summary>
	public class PolicyXDtierEnhList : DataCollection
	{
		internal PolicyXDtierEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"DISCOUNT_TIER_ID";
			this.SQLFromTable =	"POLICY_X_DTIER_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXDtierEnh";
		}
		public new PolicyXDtierEnh this[int keyValue]{get{return base[keyValue] as PolicyXDtierEnh;}}
		public new PolicyXDtierEnh AddNew(){return base.AddNew() as PolicyXDtierEnh;}
		public  PolicyXDtierEnh Add(PolicyXDtierEnh obj){return base.Add(obj) as PolicyXDtierEnh;}
		public new PolicyXDtierEnh Add(int keyValue){return base.Add(keyValue) as PolicyXDtierEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}