using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for ClaimProgressNote AutoGenerated Class.
	/// </summary>
	[Riskmaster.DataModel.Summary("CLAIM_PRG_NOTE","CL_PROG_NOTE_ID")]
	public class ClaimProgressNote : DataObject 
	{
		#region Database Field List
		private string[,] sFields = {
														{"ClaimProgNoteId", "CL_PROG_NOTE_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"EventId", "EVENT_ID"},
                                                        {"PolicyId", "POLICY_ID"}, //Added for Williams Enhanced Notes:nehagoel: MITS 21704
														{"EnteredBy", "ENTERED_BY"},
                                                        {"Subject", "SUBJECT"},  //zmohammad MITS 30218
														{"DateEntered", "DATE_ENTERED"},
														{"DateCreated", "DATE_CREATED"},
														{"TimeCreated", "TIME_CREATED"},
														{"NoteTypeCode", "NOTE_TYPE_CODE"},
														{"UserTypeCode", "USER_TYPE_CODE"},
														{"NoteMemo", "NOTE_MEMO"},
														{"NoteMemoCareTech", "NOTE_MEMO_CARETECH"},
														{"EnteredByName", "ENTERED_BY_NAME"},
                                                        {"TemplateId","TEMPLATE_ID"}, //Added for R6:Notepad Screens Templates
                                                        {"AttachRecordId","ATTACH_RECORD_ID"}, //Added by GBINDRA MITS#34104 02102014 WWIG GAP15
                                                        {"NotesAttachedTable","ATTACH_TABLE"}, //Added by GBINDRA MITS#34104 02102014 WWIG GAP15
                                                         {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"},
                                                         {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"}, //added by tmalhotra3- JIRA 4727
                                                         {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                         {"AddedByUser", "ADDED_BY_USER"},
                                                         {"UpdatedByUser", "UPDATED_BY_USER"}
														
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
		public int ClaimProgNoteId{get{return GetFieldInt("CL_PROG_NOTE_ID");}set{SetField("CL_PROG_NOTE_ID",value);}}
		public int ClaimId{get{return GetFieldInt("CLAIM_ID");}set{SetField("CLAIM_ID",value);}}
		public int EventId{get{return GetFieldInt("EVENT_ID");}set{SetField("EVENT_ID",value);}}
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }//williams:neha goel:MITS 21704
		public int EnteredBy{get{return GetFieldInt("ENTERED_BY");}set{SetField("ENTERED_BY",value);}}
        public string Subject { get { return GetFieldString("SUBJECT"); } set { SetField("SUBJECT", value); } } //zmohammad MITS 30218
		public string DateEntered{get{return GetFieldString("DATE_ENTERED");}set{SetField("DATE_ENTERED",Riskmaster.Common.Conversion.GetDate(value));}}
		public string DateCreated{get{return GetFieldString("DATE_CREATED");}set{SetField("DATE_CREATED",Riskmaster.Common.Conversion.GetDate(value));}}
		public string TimeCreated{get{return GetFieldString("TIME_CREATED");}set{SetField("TIME_CREATED",Riskmaster.Common.Conversion.GetTime(value));}}
		public int NoteTypeCode{get{return GetFieldInt("NOTE_TYPE_CODE");}set{SetField("NOTE_TYPE_CODE",value);}}
		public int UserTypeCode{get{return GetFieldInt("USER_TYPE_CODE");}set{SetField("USER_TYPE_CODE",value);}}
		public string NoteMemo{get{return GetFieldString("NOTE_MEMO");}set{SetField("NOTE_MEMO",value);}}
		public string NoteMemoCareTech{get{return GetFieldString("NOTE_MEMO_CARETECH");}set{SetField("NOTE_MEMO_CARETECH",value);}}
		public string EnteredByName{get{return GetFieldString("ENTERED_BY_NAME");}set{SetField("ENTERED_BY_NAME",value);}}
        public int TemplateId { get { return GetFieldInt("TEMPLATE_ID"); } set { SetField("TEMPLATE_ID", value); } } //Added for R6:Notepad Screens Templates
        public int AttachRecordId { get { return GetFieldInt("ATTACH_RECORD_ID"); } set { SetField("ATTACH_RECORD_ID", value); } } //Added by GBINDRA MITS#34104 02102014 WWIG GAP15
        public string NotesAttachedTable { get { return GetFieldString("ATTACH_TABLE"); } set { SetField("ATTACH_TABLE", value); } } //Added by GBINDRA MITS#34104 02102014 WWIG GAP15
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        #endregion
		
		internal ClaimProgressNote(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "CLAIM_PRG_NOTE";
			this.m_sKeyField = "CL_PROG_NOTE_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
		}
	}
}
