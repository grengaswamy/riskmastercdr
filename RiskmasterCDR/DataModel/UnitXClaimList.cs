using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forUnitXClaimList.
	/// </summary>
	public class UnitXClaimList : DataCollection
	{
		internal UnitXClaimList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"UNIT_ROW_ID";
			this.SQLFromTable =	"UNIT_X_CLAIM";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "UnitXClaim";
		}
		public new UnitXClaim this[int keyValue]{get{return base[keyValue] as UnitXClaim;}}
		public new UnitXClaim AddNew(){return base.AddNew() as UnitXClaim;}
		public  UnitXClaim Add(UnitXClaim obj){return base.Add(obj) as UnitXClaim;}
		public new UnitXClaim Add(int keyValue){return base.Add(keyValue) as UnitXClaim;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}