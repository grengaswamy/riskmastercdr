
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forEventXMedwTestList.
	/// </summary>
	public class EventXMedwTestList : DataCollection
	{
		internal EventXMedwTestList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EV_MW_TEST_ROW_ID";
			this.SQLFromTable =	"EVENT_X_MEDW_TEST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EventXMedwTest";
		}
		public new EventXMedwTest this[int keyValue]{get{return base[keyValue] as EventXMedwTest;}}
		public new EventXMedwTest AddNew(){return base.AddNew() as EventXMedwTest;}
		public  EventXMedwTest Add(EventXMedwTest obj){return base.Add(obj) as EventXMedwTest;}
		public new EventXMedwTest Add(int keyValue){return base.Add(keyValue) as EventXMedwTest;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}