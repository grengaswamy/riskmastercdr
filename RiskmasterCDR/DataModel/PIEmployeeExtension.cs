﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// PI Employee Extension class extends PI Employee class to make PI Employee compatible with PDS forms.
    /// </summary>
    public class PIEmployeeExtension: PiEmployee
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PIEmployeeExtension"/> class.
        /// </summary>
        /// <param name="isLocked">if set to <c>true</c> [is locked].</param>
        /// <param name="context">The context.</param>
        internal PIEmployeeExtension(bool isLocked, Context context): base( isLocked, context)
        {
        }

        /// <summary>
        /// Gets the pix physician.
        /// </summary>
        /// <value>
        /// The pix physician.
        /// </value>
        public DataSimpleList PIXPhysician
        {
            get { return base.Physicians; }
        }
    }
}
