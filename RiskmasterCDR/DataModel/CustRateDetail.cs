using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of CustRateDetail AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	//TODO: Complete Summary
	[Riskmaster.DataModel.Summary("CUST_RATE_DETAIL","CUST_RATE_DETAIL_ROW_ID", "Rate")]
	public class CustRateDetail  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"RateTableId", "RATE_TABLE_ID"},
														{"CustRateDetailRowId", "CUST_RATE_DETAIL_ROW_ID"},
														{"TransTypeCode", "TRANS_TYPE_CODE"},
														{"Unit", "UNIT"},
														{"Rate", "RATE"},
														{"AddedByUser", "ADDED_BY_USER"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
		};

		public int CustRateDetailRowId{get{return GetFieldInt("CUST_RATE_DETAIL_ROW_ID");}set{SetField("CUST_RATE_DETAIL_ROW_ID",value);}}
		public int RateTableId{get{return GetFieldInt("RATE_TABLE_ID");}set{SetField("RATE_TABLE_ID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"TRANS_TYPES")]
        public int TransTypeCode{get{return GetFieldInt("TRANS_TYPE_CODE");}set{SetField("TRANS_TYPE_CODE",value);}}
		public string Unit{get{return GetFieldString("UNIT");}set{SetField("UNIT",value);}}
		public double Rate{get{return GetFieldDouble("RATE");}set{SetField("RATE",value);}}
		public string AddedByUser{get{return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		public string UpdatedByUser{get{return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string DttmRcdAdded{get{return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		#endregion
		

		internal CustRateDetail(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "CUST_RATE_DETAIL";
			this.m_sKeyField = "CUST_RATE_DETAIL_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}

        public override bool IsNew
        {//MGaba2:MITS 13140:Preventing insertion of new record while editing a rate table
            get
            {
                return (this.KeyFieldValue <= 0);
            }
        }
	}
}