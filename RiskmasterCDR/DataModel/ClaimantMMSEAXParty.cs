﻿using System;
using System.Collections.Generic;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for ClaimantMMSEAXPartyList AutoGenerated Class.
    /// </summary>
    [Riskmaster.DataModel.Summary("CLAIMANT_X_MMS_CLT", "MMSEA_CLMT_ROW_ID")]
    public class ClaimantMMSEAXParty : DataObject	
    {
        #region Database Field List
        private string[,] sFields = {
                                    
                                    {"MMSEAClaimantRowID", "MMSEA_CLMT_ROW_ID"},
                                    {"PartyToClaimRelationshipCode","MMSEA_RELATEN_CODE"},
									{"PartyToClaimEntityEID","MMSEA_CLAIMANT_EID"},
                                    {"PartyToClaimReprsesentativeTypeCode","MMSEA_RP_TYPE_CODE"},
                                    {"PartyToClaimReprsesentativeEntityEID","MMSEA_CLMNT_RP_EID"},
                                    {"ClaimantRowID","CLAIMANT_ROW_ID"},
                                    {"DttmRcdAdded","DTTM_RCD_ADDED"},
                                    {"AddedByUser ","ADDED_BY_USER"},
                                    {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                    {"UpdatedByUser","UPDATED_BY_USER"}

		};

        public int MMSEAClaimantRowID { get { return GetFieldInt("MMSEA_CLMT_ROW_ID"); } set { SetField("MMSEA_CLMT_ROW_ID", value); } }
        public int PartyToClaimRelationshipCode { get { return GetFieldInt("MMSEA_RELATEN_CODE"); } set { SetField("MMSEA_RELATEN_CODE", value); } }
        //Rijul 340
        //public int PartyToClaimEntityEID { get { return GetFieldInt("MMSEA_CLAIMANT_EID"); } set { SetField("MMSEA_CLAIMANT_EID", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "MMSEA_CLMPRTY_TYPE")]
        public int PartyToClaimEntityEID { get { return GetFieldInt("MMSEA_CLAIMANT_EID"); } set { SetFieldAndNavTo("MMSEA_CLAIMANT_EID", value, "PartyToClaimEntity"); } }
        //Rijul 340 end
        public int PartyToClaimReprsesentativeTypeCode { get { return GetFieldInt("MMSEA_RP_TYPE_CODE"); } set { SetField("MMSEA_RP_TYPE_CODE", value); } }
        //Rijul 340
        //public int PartyToClaimReprsesentativeEntityEID { get { return GetFieldInt("MMSEA_CLMNT_RP_EID"); } set { SetField("MMSEA_CLMNT_RP_EID", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int PartyToClaimReprsesentativeEntityEID { get { return GetFieldInt("MMSEA_CLMNT_RP_EID"); } set { SetFieldAndNavTo("MMSEA_CLMNT_RP_EID", value, "PartyToClaimRepEntity"); } }
        //Rijul 340 end
        public int ClaimantRowID { get { return GetFieldInt("CLAIMANT_ROW_ID"); } set { SetField("CLAIMANT_ROW_ID", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        
        #endregion

        //Rijul 340 
        #region Child Implementation
        private string[,] sChildren = {
                                      {"PartyToClaimEntity","Entity"},
                                      {"PartyToClaimRepEntity","Entity"}
									  };

        override internal void OnChildInit(string childName, string childType)
        {
            Entity objEnt = null;
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "Entity":
                    switch (childName)
                    {
                        case "PartyToClaimEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                        case "PartyToClaimRepEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                    }
                    break;
            }
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            this.SetFieldAndNavTo("MMSEA_CLAIMANT_EID", this.PartyToClaimEntityEID, "PartyToClaimEntity", true);
            this.SetFieldAndNavTo("MMSEA_CLMNT_RP_EID", this.PartyToClaimReprsesentativeEntityEID, "PartyToClaimRepEntity", true);
        }

        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            int intTableID;
            Dictionary<string, int> objReturnVal = new Dictionary<string, int>();
            // If Entity is involved we save this first (in case we need the EntityId.)
            if (childName == "PartyToClaimEntity")
            {
                if (!int.Equals(PartyToClaimEntityEID, 0))
                {
                    Entity objEntity = ((childValue as Entity));
                    intTableID = Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.MMSEA_CLMPRTY_TYPE.ToString());
                    objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                    if (objReturnVal != null)
                        this.m_Fields["MMSEA_CLAIMANT_EID"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
                }
            }
            if (childName == "PartyToClaimRepEntity")
            {
                if (!int.Equals(PartyToClaimReprsesentativeEntityEID, 0))
                {
                    Entity objEntity = ((childValue as Entity));
                    intTableID = Context.LocalCache.GetTableId(GetSystemTabelName());
                    objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                    if (objReturnVal != null)
                        this.m_Fields["MMSEA_CLMNT_RP_EID"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
                }
            }
            objReturnVal = null;
        }

        public Entity PartyToClaimEntity
        {
            get
            {
                Entity objItem = base.m_Children["PartyToClaimEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("PartyToClaimEntity", (objItem as DataObject), this.PartyToClaimEntityEID) as Entity;
                return objItem;
            }
        }
        public Entity PartyToClaimRepEntity
        {
            get
            {
                Entity objItem = base.m_Children["PartyToClaimRepEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("PartyToClaimRepEntity", (objItem as DataObject), this.PartyToClaimReprsesentativeEntityEID) as Entity;
                return objItem;
            }
        }
        #endregion
        //Rijul 340 end

        internal ClaimantMMSEAXParty(bool isLocked, Context context): base(isLocked, context)
		{
			this.Initialize();
		}

        public override bool IsNew
        {
            get
            {
                return (this.KeyFieldValue <= 0);
            }
        }
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIMANT_X_MMS_CLT";
            this.m_sKeyField = "MMSEA_CLMT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);   //Rijul 340
            this.m_sParentClassName = "ClaimantMmsea";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
		}
        //Rijul 340
        private string GetSystemTabelName()
        {
            string sReturnVal = string.Empty;
            switch (this.Context.LocalCache.GetShortCode(this.PartyToClaimReprsesentativeTypeCode).ToUpper())
            {
                case "A":
                    sReturnVal = Globalization.EntityGlossaryTableNames.ATTORNEYS.ToString();
                    break;
                case "G":
                    sReturnVal = Globalization.EntityGlossaryTableNames.GUARDIAN_TYPE.ToString();
                    break;
                case "P":
                    sReturnVal = Globalization.EntityGlossaryTableNames.POWOFATTORNEY_TYPE.ToString();
                    break;
                case "O":
                    sReturnVal = Globalization.EntityGlossaryTableNames.OTHER_PEOPLE.ToString();
                    break;
                case "F":
                    sReturnVal = Globalization.EntityGlossaryTableNames.ATTORNEY_FIRMS.ToString();
                    break;
            }
            return sReturnVal;
        }
        //Rijul 340 End
    }
}