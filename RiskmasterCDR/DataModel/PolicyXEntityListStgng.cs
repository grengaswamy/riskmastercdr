﻿using System;

namespace Riskmaster.DataModel
{
    public class PolicyXEntityListStgng:DataCollection 
    {
        internal PolicyXEntityListStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "POLICYENTITY_ROWID";
            this.SQLFromTable = "POLICY_X_ENTITY_STGNG";
            this.TypeName = "PolicyXEntityStgng";
        }
        public new PolicyXEntityStgng this[int keyValue] { get { return base[keyValue] as PolicyXEntityStgng; } }
        public new PolicyXEntityStgng AddNew() { return base.AddNew() as PolicyXEntityStgng; }
        public PolicyXEntityStgng Add(PolicyXEntity obj) { return base.Add(obj) as PolicyXEntityStgng; }
        public new PolicyXEntityStgng Add(int keyValue) { return base.Add(keyValue) as PolicyXEntityStgng; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
