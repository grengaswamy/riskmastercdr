﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for PiPatientProcedureList.
    /// Created by : pmittal5
    /// Date : 07/15/08
    /// MITS : 12030
    /// </summary>
    public class PiXProcedureList : DataCollection
    {
        internal PiXProcedureList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "PI_PROC_ROW_ID";
            this.SQLFromTable = "PI_X_PROCEDURE";
			this.TypeName = "PiXProcedure";
		}
        public new PiXProcedure this[int keyValue] { get { return base[keyValue] as PiXProcedure; } }
        public new PiXProcedure AddNew() { return base.AddNew() as PiXProcedure; }
        public PiXProcedure Add(PiXProcedure obj) { return base.Add(obj) as PiXProcedure; }
        public new PiXProcedure Add(int keyValue) { return base.Add(keyValue) as PiXProcedure; }
		
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
