﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_CVG_TYPE_STGNG", "POLCVG_ROW_ID", "CoverageTypeCode")]
    public class PolicyXCvgTypeStgng:DataObject
    {
        private int m_MultiCoverage = 0;
        /// <summary>
        ///skhare7: R8: Supervisory Approval
        /// </summary>
        public int iMultiCoverage
        {
            get
            {
                return m_MultiCoverage;
            }
            set
            {
                m_MultiCoverage = value;
            }
        }
        #region Database Field List
        private string[,] sFields = {
														{"ClaimLimit", "CLAIM_LIMIT"},
														{"NotificationUid", "NOTIFICATION_UID"},
														{"PolcvgRowId", "POLCVG_ROW_ID"},
														{"PolicyId", "POLICY_ID"},
														{"CoverageTypeCode", "COVERAGE_TYPE_CODE"},
														{"PolicyLimit", "POLICY_LIMIT"},
														{"OccurrenceLimit", "OCCURRENCE_LIMIT"},
														{"TotalPayments", "TOTAL_PAYMENTS"},
														{"Remarks", "REMARKS"},
														{"Exceptions", "EXCEPTIONS"},
														{"CancelNoticeDays", "CANCEL_NOTICE_DAYS"},
														{"SelfInsureDeduct", "SELF_INSURE_DEDUCT"},
														{"NextPolicyId", "NEXT_POLICY_ID"},
														{"BrokerName", "BROKER_NAME"},
                                                        {"SectionNumCode", "SECTION_NUMBER_CODE"},
                                                        {"PolicyUnitRowId", "POLICY_UNIT_ROW_ID"},
                                                        {"PerPersonLimit","PER_PERSON_LIMIT"} ,  //Added by amitosh for Policy interface
                                                        {"EffectiveDate", "EFFECTIVE_DATE"},
														{"ExpirationDate", "EXPIRATION_DATE"},
														{"Limit", "LIMIT"},
														{"OrginialPremium", "ORIGINAL_PREMIUM"},
														{"WrittenPremium", "WRITTEN_PREMIUM"},
														{"FullTermPremium", "FULL_TERM_PREMIUM"},
														{"TotalWrittenPremium", "TOTAL_WRITTEN_PREMIUM"},
														{"Exposure", "EXPOSURE"},
														{"ChangeDate", "CHANGE_DATE"},
                                                        {"CvgSequenceNo","CVG_SEQUENCE_NO"},
                                                        {"TransSequenceNo","TRANS_SEQ_NO"},
                                                        {"CoverageText", "COVERAGE_TEXT"},
                                                        {"Class", "CLASS"},
                                                        {"SubLine", "SUBLINE"},
                                                        {"ProductLine", "PROD_LINE"},
                                                        {"TransDate", "TRANS_DATE"},
                                                        {"RetroDate", "RETRO_DATE"},
                                                        {"Extenddate", "EXTEND_DATE"},
                                                        {"WCDedAmnt", "WC_DED_AMNT"},
                                                        {"LimCovA", "LIMIT_CVG_A"},
                                                        {"LimCovB", "LIMIT_CVG_B"},
                                                        {"LimCovC", "LIMIT_CVG_C"},
                                                        {"LimCovD", "LIMIT_CVG_D"},
                                                        {"LimCovE", "LIMIT_CVG_E"},
                                                        {"LimCovF", "LIMIT_CVG_F"},
                                                        {"CvgStatus", "CVG_STATUS"},
                                                        {"State", "STATE_ID"},
                                                        {"EntryDate", "ENTRY_DATE"},
                                                        {"AccountingDate", "ACCOUNTING_DATE"},
                                                        {"AnnualStmnt", "ANNUAL_STMNT"},
                                                        {"WCDedAggr", "WC_DED_AGGREGATE"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
                                    };

        public double ClaimLimit { get { return GetFieldDouble("CLAIM_LIMIT"); } set { SetField("CLAIM_LIMIT", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "RM_SYS_USERS")]
        public int NotificationUid { get { return GetFieldInt("NOTIFICATION_UID"); } set { SetField("NOTIFICATION_UID", value); } }
        public int PolcvgRowId { get { return GetFieldInt("POLCVG_ROW_ID"); } set { SetField("POLCVG_ROW_ID", value); } }
        //parag
        //[ExtendedTypeAttribute(RMExtType.ChildLink,"Policy")]
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        
        [ExtendedTypeAttribute(RMExtType.Code, "COVERAGE_TYPE")]
        public string CoverageTypeCode { get { return GetFieldString("COVERAGE_TYPE_CODE"); } set { SetField("COVERAGE_TYPE_CODE", value); } }
        public double PolicyLimit { get { return GetFieldDouble("POLICY_LIMIT"); } set { SetField("POLICY_LIMIT", value); } }
        public double OccurrenceLimit { get { return GetFieldDouble("OCCURRENCE_LIMIT"); } set { SetField("OCCURRENCE_LIMIT", value); } }
        public double TotalPayments { get { return GetFieldDouble("TOTAL_PAYMENTS"); } set { SetField("TOTAL_PAYMENTS", value); } }
        public string Remarks { get { return GetFieldString("REMARKS"); } set { SetField("REMARKS", value); } }
        public string Exceptions { get { return GetFieldString("EXCEPTIONS"); } set { SetField("EXCEPTIONS", value); } }
        public int CancelNoticeDays { get { return GetFieldInt("CANCEL_NOTICE_DAYS"); } set { SetField("CANCEL_NOTICE_DAYS", value); } }
        public double SelfInsureDeduct { get { return GetFieldDouble("SELF_INSURE_DEDUCT"); } set { SetField("SELF_INSURE_DEDUCT", value); } }
        [ExtendedTypeAttribute(RMExtType.ChildLink, "Policy")]
        public int NextPolicyId { get { return GetFieldInt("NEXT_POLICY_ID"); } set { SetField("NEXT_POLICY_ID", value); } }
        public string BrokerName { get { return GetFieldString("BROKER_NAME"); } set { SetField("BROKER_NAME", value); } }
        //Abhay - Added SectionNumCode
        [ExtendedTypeAttribute(RMExtType.Code, "SECTION_NUMBER")]
        public string SectionNumCode { get { return GetFieldString("SECTION_NUMBER_CODE"); } set { SetField("SECTION_NUMBER_CODE", value); } }
        public int PolicyUnitRowId { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        //Added by Amitosh for Policy interface
        public double PerPersonLimit { get { return GetFieldDouble("PER_PERSON_LIMIT"); } set { SetField("PER_PERSON_LIMIT", value); } }

        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public double Limit { get { return GetFieldDouble("LIMIT"); } set { SetField("LIMIT", value); } }
        public double OrginialPremium { get { return GetFieldDouble("ORIGINAL_PREMIUM"); } set { SetField("ORIGINAL_PREMIUM", value); } }
        public double WrittenPremium { get { return GetFieldDouble("WRITTEN_PREMIUM"); } set { SetField("WRITTEN_PREMIUM", value); } }
        public double FullTermPremium { get { return GetFieldDouble("FULL_TERM_PREMIUM"); } set { SetField("FULL_TERM_PREMIUM", value); } }
        public double TotalWrittenPremium { get { return GetFieldDouble("TOTAL_WRITTEN_PREMIUM"); } set { SetField("TOTAL_WRITTEN_PREMIUM", value); } }
        public double Exposure { get { return GetFieldDouble("EXPOSURE"); } set { SetField("EXPOSURE", value); } }
        public string ChangeDate { get { return GetFieldString("CHANGE_DATE"); } set { SetField("CHANGE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string CvgSequenceNo { get { return GetFieldString("CVG_SEQUENCE_NO"); } set { SetField("CVG_SEQUENCE_NO", value); } }
        public string TransSequenceNo { get { return GetFieldString("TRANS_SEQ_NO"); } set { SetField("TRANS_SEQ_NO", value); } }
        public string CoverageText { get { return GetFieldString("COVERAGE_TEXT"); } set { SetField("COVERAGE_TEXT", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public string Class { get { return GetFieldString("CLASS"); } set { SetField("CLASS", value); } }
        public string SubLine { get { return GetFieldString("SUBLINE"); } set { SetField("SUBLINE", value); } }
        public string ProductLine { get { return GetFieldString("PROD_LINE"); } set { SetField("PROD_LINE", value); } }
        public string TransDate { get { return GetFieldString("TRANS_DATE"); } set { SetField("TRANS_DATE", value); } }
        public string RetroDate { get { return GetFieldString("RETRO_DATE"); } set { SetField("RETRO_DATE", value); } }
        public string Extenddate { get { return GetFieldString("EXTEND_DATE"); } set { SetField("EXTEND_DATE", value); } }
        public double WCDedAmnt { get { return GetFieldDouble("WC_DED_AMNT"); } set { SetField("WC_DED_AMNT", value); } }
        public double LimCovA { get { return GetFieldDouble("LIMIT_CVG_A"); } set { SetField("LIMIT_CVG_A", value); } }
        public double LimCovB { get { return GetFieldDouble("LIMIT_CVG_B"); } set { SetField("LIMIT_CVG_B", value); } }
        public double LimCovC { get { return GetFieldDouble("LIMIT_CVG_C"); } set { SetField("LIMIT_CVG_C", value); } }
        public double LimCovD { get { return GetFieldDouble("LIMIT_CVG_D"); } set { SetField("LIMIT_CVG_D", value); } }
        public double LimCovE { get { return GetFieldDouble("LIMIT_CVG_E"); } set { SetField("LIMIT_CVG_E", value); } }
        public double LimCovF { get { return GetFieldDouble("LIMIT_CVG_F"); } set { SetField("LIMIT_CVG_F", value); } }
        public string CvgStatus { get { return GetFieldString("CVG_STATUS"); } set { SetField("CVG_STATUS", value); } }
        public string State { get { return GetFieldString("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string EntryDate { get { return GetFieldString("ENTRY_DATE"); } set { SetField("ENTRY_DATE", value); } }
        public string AccountingDate { get { return GetFieldString("ACCOUNTING_DATE"); } set { SetField("ACCOUNTING_DATE", value); } }
        public double AnnualStmnt { get { return GetFieldDouble("ANNUAL_STMNT"); } set { SetField("ANNUAL_STMNT", value); } }
        public double WCDedAggr { get { return GetFieldDouble("WC_DED_AGGREGATE"); } set { SetField("WC_DED_AGGREGATE", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Ends
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        internal PolicyXCvgTypeStgng(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "POLICY_X_CVG_TYPE_STGNG";
			this.m_sKeyField = "POLCVG_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			//this.m_sParentClassName = "Policy";
            if (this.iMultiCoverage == 1)
            {
                this.m_sParentClassName = "PolicyXUnitStgng";
            }
            else
            {
                this.m_sParentClassName = "PolicyStgng";
            }

			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
        //#region Supplemental Fields Exposed
        //internal override string OnBuildSuppTableName()
        //{
        //    return "CVG_TYPE_STGNG_SUPP";
        //}

        ////Supplementals are implemented in the base class but not exposed externally.
        ////This allows the derived class to control whether the object appears to have
        ////supplementals or not. 
        ////Entity exposes Supplementals.
        //public new Supplementals Supplementals
        //{	
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        //#endregion
    }
}
