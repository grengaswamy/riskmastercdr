﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_UNIT_STGNG", "POLICY_UNIT_ROW_ID")]
    public class PolicyXUnitStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"PolicyUnitRowId", "POLICY_UNIT_ROW_ID"},
														{"UnitId", "UNIT_ID"},
														{"PolicyId", "POLICY_ID"},
                                                        {"UnitType", "UNIT_TYPE"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
		};

        public int PolicyUnitRowId { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public int UnitId { get { return GetFieldInt("UNIT_ID"); } set { SetField("UNIT_ID", value); } }
        
        public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        internal PolicyXUnitStgng(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}
       
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "POLICY_X_UNIT_STGNG";
            this.m_sKeyField = "POLICY_UNIT_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //base.InitChildren(sChildren);
            //base.InitChildren(sChildren);
            this.m_sParentClassName = "PolicyStgng";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        
       
    }
}
