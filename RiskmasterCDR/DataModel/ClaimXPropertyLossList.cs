﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public class ClaimXPropertyLossList : DataCollection
    {
        internal ClaimXPropertyLossList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "CLAIM_X_PROPERTYLOSS";
            this.TypeName = "ClaimXPropertyLoss";
        }

        public new ClaimXPropertyLoss this[int keyValue] { get { return base[keyValue] as ClaimXPropertyLoss; } }
        public new ClaimXPropertyLoss AddNew() { return base.AddNew() as ClaimXPropertyLoss; }
        public ClaimXPropertyLoss Add(ClaimXPropertyLoss obj) { return base.Add(obj) as ClaimXPropertyLoss; }
        public new ClaimXPropertyLoss Add(int keyValue) { return base.Add(keyValue) as ClaimXPropertyLoss; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }
}
