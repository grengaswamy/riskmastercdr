﻿using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Created By:     Debabrata Biswas
    /// Date:           09/01/2011
    /// Description:    Event PSO
	/// </summary>
    [Riskmaster.DataModel.Summary("EVENT_PSO", "EVT_PSO_ROW_ID", "psocdEventcategory")]
	public class EventPSO  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
                                                        {"EventId",                             "EVENT_ID"},
                                                        {"EvtPsoRowId",                           "EVT_PSO_ROW_ID"},
                                                        {"DateTimeRecAdded",                    "DTTM_RCD_ADDED"},
                                                        {"AddedByUser",                         "ADDED_BY_USER"},
                                                        {"DateTimeRecLastUpdated",              "DTTM_RCD_LAST_UPD"},
                                                        {"UpdatedByUser",                       "UPDATED_BY_USER"},
                                                        {"DateTimeRecLastExp",                  "DTTM_RCD_LAST_EXP"},
                                                        {"psocdWhatReported",	            "PSO_SAF_REP_TYP_CODE"},
                                                        {"dtEvtDiscoveryDate",	            "PSO_DATE_OF_EVENT_INC"},
                                                        {"dtEvtDiscoveryDateNM",	            "PSO_DATE_OF_EVENT_NM"},
                                                        {"txtReporterJobPosition",          "PSO_REPPOS_TEXT"},
                                                        {"psocdEvidenceReport",	                "PSO_PATIENT_HARM_CODE"},
                                                        {"psocdEventcategory",	                "PSO_IND_CODE"},
                                                        {"psocdEventcategoryNM",	                "PSO_IND_NM_CODE"},
                                                        {"psocdEventcategoryUSC",	                "PSO_IND_USC_CODE"},
                                                        {"txtOtherEventcategory",	                "PSO_IND_MEMO"},
                                                        {"psocdEventLocation",	                "PSO_LOC_CODE"},
                                                        {"txtOtherEventLocation",	                "PSO_LOC_CODE_MEMO"},
                                                        {"psocdReportedUnsafe",	                "PSO_REP_TYP_CODE"},
                                                        {"psocdHealthcareProf",	            "PSO_HLTH_PROF_CODE"},
                                                        {"txtAdditionalDetails",	        "PSO_EVT_DESC_MEMO"},
                                                        {"cdPreventedMiss",	                "PSO_MISS_ACTION_CODE"},
                                                        {"cdHandoverINC",                        "PSO_HAND_OVER_INC_CODE"},
                                                        {"cdHandoverNM",                        "PSO_HAND_OVER_NM_CODE"},
                                                        {"cdPreventableIncident",	        "PSO_PREVENTABILITY_CODE"},
                                                        {"psocdEventFactor",	    "PSO_CONTRIB_FAC_KN_CODE"},
                                                        {"psocdEventFactorNM",	    "PSO_CONTRIB_FAC_KN_NM_CODE"},
                                                        {"txtOtherFactors",	    "PSO_CONTRIB_FAC_MEMO"},
                                                        {"txtOtherFactorsNM",	    "PSO_CONTRIB_FAC_NM_MEMO"},
                                                        {"cdHITImplicated",	    "PSO_HLTH_TECH_IMPL_CODE"},
                                                        {"cdHITImplicatedNM",	    "PSO_HLTH_TECH_IMPL_NM_CODE"},
                                                        {"psocdNQFEvent",            "PSO_NFQ_SERIOUS_CODE"},
                                                        {"psocdNQFEventNM",            "PSO_NFQ_SERIOUS_NM_CODE"},
                                                        {"psocdSeriousEvent",	                        "PSO_NQF_TYP_CODE"},
                                                        {"psocdSeriousEventNM",	                        "PSO_NQF_TYP_NM_CODE"},
                                                        {"psocdTypeBldProduct",	            "PSO_BLOOD_PROD_CODE"},
                                                        {"txtOtherTypeBldProduct",	            "PSO_BLOOD_PROD_MEMO"},
                                                        {"psocdISBTProductCode",	                        "PSO_ISBT_CODE"},
                                                        {"psocdDescribeEvent",	            "PSO_BLOOD_PROD_EVT_CODE"},
                                                        {"psocdDescribeEventNM",	            "PSO_BLOOD_PROD_EVT_NM_CODE"},
                                                        {"psocdIncorrectAction",	    "PSO_BLOOD_INCRT_ACTION_CODE"},
                                                        {"txtOtherIncorrectAction",	    "PSO_BLOOD_INCRT_ACTION_MEMO"},
                                                        {"psocdChkDocument",	        "PSO_TWO_PERSN_CODE"},
                                                        {"psocdBloodVolume",	                        "PSO_BLOOD_VOL_CODE"},
                                                        {"psocdAdminRate",	        "PSO_RATE_ADMIN_CODE"},
                                                        {"psocdProcessDiscovered",	            "PSO_DISC_PROC_CODE"},
                                                        {"txtOtherProcessDiscovered",	            "PSO_DISC_PROC_MEMO"},
                                                        {"psocdProcessOriginated",	            "PSO_ORIG_PROC_CODE"},
                                                        {"txtOtherProcessOriginated",	            "PSO_ORIG_PROC_MEMO"},
                                                        {"psocdDeviceEventDesc",               "PSO_DEVICE_DESC_CODE"},
                                                        {"psocdDeviceFailureType",        "PSO_DEVICE_FAIL_DESC_CODE"},
                                                        {"psocdOperatorErrorType",	                "PSO_OPR_ERR_CODE"},
                                                        {"txtOtherOperatorErrorType",	                "PSO_OPR_ERR_MEMO"},
                                                        {"psocdDeviceReuse",	                "PSO_DEVISE_REUSE_CODE"},
                                                        {"psocdFallAssist",	                "PSO_FALL_ASSIST_CODE"},
                                                        {"psocdFallObserved",                    "PSO_FALL_CHECK_CODE"},
                                                        {"psocdWhoObserved",	                "PSO_FALL_OBSVR_CODE"},
                                                        {"psocdPhysicalInjury",           "PSO_IS_FALL_INJURY_CODE"},
                                                        {"psocdInjuryType",	                    "PSO_INJURY_TYP_CODE"},
                                                        {"txtOtherInjuryType",	                    "PSO_INJURY_TYP_MEMO"},
                                                        {"psocdPatientDoing",	        "PSO_PAT_ACT_FALL_CODE"},
                                                        {"txtOtherPatientDoing",	        "PSO_PAT_ACT_FALL_MEMO"},
                                                        {"psocdRiskAssessment",	    "PSO_RISK_DOC_CODE"},
                                                        {"psocdDeterminedForFall",	                    "PSO_FALL_RSK_CODE"},                                                        
                                                        {"psotxtOtherProtocolFall",	                "PSO_INTERVEN_MEMO"},
                                                        {"psocdMedicationFall",	        "PSO_UNDER_MED_RISK_CODE"},
                                                        {"psocdMedicationContributed",     "PSO_MED_FALL_CONTRIB_CODE"},
                                                        {"psocdSubstanceTypeINC",               "PSO_SUBS_INV_INC_CODE"},  
                                                        {"psocdSubstanceTypeNM",               "PSO_SUBS_INV_NM_CODE"},  
                                                        {"psocdSubstanceTypeUSC",               "PSO_SUBS_INV_USC_CODE"},  
                                                        {"txtOtherSubstanceType",	            "PSO_SUBS_INV_MEMO"}, 
                                                        {"psocdMedicationTypeINC",	                        "PSO_MED_TYPE_INC_CODE"},
                                                        {"psocdMedicationTypeNM",	                        "PSO_MED_TYPE_NM_CODE"},
                                                        {"psocdMedicationTypeUSC",	                        "PSO_MED_TYPE_USC_CODE"},
                                                        {"txtIngredients",	        "PSO_PREP_INGRD_MEMO"}, 
                                                        {"psocdBiologicalType",	            "PSO_BIO_PROD_CODE"}, 
                                                        {"txtLotNum",	                        "PSO_LOT_NUMBER_TEXT"}, 
                                                        {"psocdNutritionalProduct",	            "PSO_NUT_PROD_CODE"}, 
                                                        {"txtOtherNutritionalProd",	            "PSO_NUT_PROD_MEMO"},
                                                        {"psocdBestEventINC",	    "PSO_MED_DESC_INC_CODE"}, 
                                                        {"psocdBestEventNM",	    "PSO_MED_DESC_NM_CODE"}, 
                                                        {"psocdBestEventUSC",	    "PSO_MED_DESC_USC_CODE"}, 
                                                        {"txtOtherIncorrAction",	            "PSO_MED_INCRT_ACTION_MEMO"}, 
                                                        {"cdIncorrectDose",	                "PSO_MED_INCRT_DOSE_CODE"}, 
                                                        {"cdIncorrTiming",	                "PSO_MED_INCRT_TIME_CODE"}, 
                                                        {"cdIncorrRate",	                "PSO_MED_INCRT_RATE_CODE"}, 
                                                        {"cdIncorrStrength",	            "PSO_MED_INCRT_STRN_CODE"},
                                                        {"dtExpireDate",	                    "EXPIRATION_DATE"}, 
                                                        {"cdDocHistory",	            "PSO_HIST_DOC_CODE"}, 
                                                        {"psocdContradictions",	    "PSO_MED_CONTR_CODE"}, 
                                                        {"txtContradictions",	    "PSO_MED_CONTR_MEMO"}, 
                                                        {"psocdEventOriginate",	        "PSO_EVNT_STG_CODE"},
                                                        {"txtOtherEventOriginate",	        "PSO_EVNT_STG_MEMO"},   
                                                        {"psocdIntendedRoute",                   "PSO_INT_ROUTE_CODE"},  
                                                        {"txtOtherIntendedRoute",	                "PSO_INT_ROUTE_MEMO"}, 
                                                        {"psocdActualRoute",                     "PSO_ACT_ROUTE_CODE"},  
                                                        {"txtOtherActualRoute",	                    "PSO_ACT_ROUTE_MEMO"}, 
                                                        {"cdICD9CM",	            "PSO_SURG_PROC_CODE"},
                                                        {"psocdDocumentedASA",              "PSO_DOCUMNTD_ASA_CODE"},
                                                        {"txtDescProcdure",	        "PSO_EVNT_PROC_MEMO"},
                                                        {"cdProcEmergency",	    "PSO_EMR_SURG_CODE"},
                                                        {"psocdEventDiscovered",	            "PSO_TIME_SURGEVT_DISC_CODE"},
                                                        {"psocdProcLength",	            "PSO_LNGTH_PROC_CODE"},
                                                        {"psocdAnesthesiaType",            "PSO_SED_ANES_TYP_CODE"},
                                                        {"psocdSedationLevel",                 "PSO_LVL_SEDTN_CODE"},
                                                        {"psocdAdminAnesthesia",           "PSO_ANES_PRSN_CODE"},
                                                        {"psocdAnesthesiologist",	"PSO_ANES_SUPVSN_CODE"},
                                                        {"psocdSugicalSpeciality",	            "PSO_SPCLTY_PROVD_CODE"},
                                                        {"txtOtherSugicalSpeciality",	            "PSO_SPCLTY_PROVD_MEMO"},
                                                        {"psocdBestDecs",	            "PSO_EVNT_TYP_CODE"},
                                                        {"psocdMajorComplications",	            "PSO_MAJ_COMPL_CODE"},
                                                        {"txtOtherMajorComplications",	            "PSO_MAJ_COMPL_MEMO"},
                                                        {"psocdRespiratorySupport",          "PSO_RESP_SUPP_CODE"},
                                                        {"txtOtherRespiratorySupport",	        "PSO_RESP_SUPP_MEMO"},
                                                        {"psocdRestainedObject",     "PSO_UNINT_RETOBJ_CODE"},
                                                        {"psocdObjectRestained",	            "PSO_RET_OBJ_TYP_CODE"},
                                                        {"txtOtherObjectRestained",	            "PSO_RET_OBJ_TYP_MEMO"},
                                                        {"psocdCountPerformed",	            "PSO_WAS_CNT_PERF_CODE"},
                                                        {"psocdCountStatus",	                    "PSO_REP_CNT_STAT_CODE"},
                                                        {"psocdXRayObtained",	                "PSO_XRAY_OBTN_CODE"},
                                                        {"psocdRadiopaque",	    "PSO_RDIOPQ_OBJ_RET_CODE"},
                                                        {"psocdCharacterizedSurgery",	        "PSO_CHARC_SURG_OUTC_CODE"},
                                                        {"txtOtherCharSurgery",	        "PSO_CHARC_SURG_OUTC_MEMO"},
                                                        {"psocdWhichOccured",	        "PSO_BURN_ROOMFIRE_OCC_CODE"},
                                                        {"psocdIncorrectProc",	"PSO_SURG_INV_INCRT_ACTN_CODE"},
                                                        {"txtOtherIncorrectProc",	"PSO_SURG_INV_INCRT_ACTN_MEMO"},
                                                        {"psocdEventAnesthesia",	"PSO_ANES_EVNT_CHAREC_CODE"},
                                                        {"txtOtherEventAnesthesia",	"PSO_ANES_EVNT_CHAREC_MEMO"},
                                                        {"psocdAirwayMngmnt",	        "PSO_AIRWAY_PROB_CODE"},
                                                        {"txtOtherAirwayMngmnt",	        "PSO_AIRWAY_PROB_MEMO"},//OTHER AIRWAY MGT
                                                        {"txtVersionNumber",	        "PSO_VERSION_NUM"},//OTHER AIRWAY MGT
                                                        {"psocdExtractStatus",	        "PSO_EXTRACT_STATUS_CODE"},//OTHER AIRWAY MGT

		};

        public int EventId{get{return GetFieldInt("EVENT_ID");}set{SetField("EVENT_ID",value);}}
        public int EvtPsoRowId { get { return GetFieldInt("EVT_PSO_ROW_ID"); } set { SetField("EVT_PSO_ROW_ID", value); } }
        public string DateTimeRecAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DateTimeRecLastUpdated { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DateTimeRecLastExp { get { return GetFieldString("DTTM_RCD_LAST_EXP"); } set { SetField("DTTM_RCD_LAST_EXP", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SAFETY_EVT_TYP")]
        public int psocdWhatReported { get { return GetFieldInt("PSO_SAF_REP_TYP_CODE"); } set { SetField("PSO_SAF_REP_TYP_CODE", value); } }
        public string dtEvtDiscoveryDate { get { return GetFieldString("PSO_DATE_OF_EVENT_INC"); } set { SetField("PSO_DATE_OF_EVENT_INC", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string dtEvtDiscoveryDateNM { get { return GetFieldString("PSO_DATE_OF_EVENT_NM"); } set { SetField("PSO_DATE_OF_EVENT_NM", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string txtReporterJobPosition { get { return GetFieldString("PSO_REPPOS_TEXT"); } set { SetField("PSO_REPPOS_TEXT", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdEvidenceReport { get { return GetFieldInt("PSO_PATIENT_HARM_CODE"); } set { SetField("PSO_PATIENT_HARM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVENT_CAT")]
        public int psocdEventcategory { get { return GetFieldInt("PSO_IND_CODE"); } set { SetField("PSO_IND_CODE", value); } }
        public string txtOtherEventcategory { get { return GetFieldString("PSO_IND_MEMO"); } set { SetField("PSO_IND_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVENT_CAT")]
        public int psocdEventcategoryNM { get { return GetFieldInt("PSO_IND_NM_CODE"); } set { SetField("PSO_IND_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVENT_CAT")]
        public int psocdEventcategoryUSC { get { return GetFieldInt("PSO_IND_USC_CODE"); } set { SetField("PSO_IND_USC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVT_LOC")]
        public int psocdEventLocation { get { return GetFieldInt("PSO_LOC_CODE"); } set { SetField("PSO_LOC_CODE", value); } }
        public string txtOtherEventLocation { get { return GetFieldString("PSO_LOC_CODE_MEMO"); } set { SetField("PSO_LOC_CODE_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_REPORTER_TYPE")]
        public int psocdReportedUnsafe { get { return GetFieldInt("PSO_REP_TYP_CODE"); } set { SetField("PSO_REP_TYP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HLTH_PROF")]
        public int psocdHealthcareProf { get { return GetFieldInt("PSO_HLTH_PROF_CODE"); } set { SetField("PSO_HLTH_PROF_CODE", value); } }
        public string txtAdditionalDetails { get { return GetFieldString("PSO_EVT_DESC_MEMO"); } set { SetField("PSO_EVT_DESC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MISS_ACTION")]
        public int cdPreventedMiss { get { return GetFieldInt("PSO_MISS_ACTION_CODE"); } set { SetField("PSO_MISS_ACTION_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdHandoverINC { get { return GetFieldInt("PSO_HAND_OVER_INC_CODE"); } set { SetField("PSO_HAND_OVER_INC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdHandoverNM { get { return GetFieldInt("PSO_HAND_OVER_NM_CODE"); } set { SetField("PSO_HAND_OVER_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVT_PREVENT")]
        public int cdPreventableIncident { get { return GetFieldInt("PSO_PREVENTABILITY_CODE"); } set { SetField("PSO_PREVENTABILITY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdEventFactor { get { return GetFieldInt("PSO_CONTRIB_FAC_KN_CODE"); } set { SetField("PSO_CONTRIB_FAC_KN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdEventFactorNM { get { return GetFieldInt("PSO_CONTRIB_FAC_KN_NM_CODE"); } set { SetField("PSO_CONTRIB_FAC_KN_NM_CODE", value); } }
        public string txtOtherFactors { get { return GetFieldString("PSO_CONTRIB_FAC_MEMO"); } set { SetField("PSO_CONTRIB_FAC_MEMO", value); } }
        public string txtOtherFactorsNM { get { return GetFieldString("PSO_CONTRIB_FAC_NM_MEMO"); } set { SetField("PSO_CONTRIB_FAC_NM_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdHITImplicated { get { return GetFieldInt("PSO_HLTH_TECH_IMPL_CODE"); } set { SetField("PSO_HLTH_TECH_IMPL_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdHITImplicatedNM { get { return GetFieldInt("PSO_HLTH_TECH_IMPL_NM_CODE"); } set { SetField("PSO_HLTH_TECH_IMPL_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdNQFEvent { get { return GetFieldInt("PSO_NFQ_SERIOUS_CODE"); } set { SetField("PSO_NFQ_SERIOUS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdNQFEventNM { get { return GetFieldInt("PSO_NFQ_SERIOUS_NM_CODE"); } set { SetField("PSO_NFQ_SERIOUS_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_NQF_TYP")]
        public int psocdSeriousEvent { get { return GetFieldInt("PSO_NQF_TYP_CODE"); } set { SetField("PSO_NQF_TYP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_NQF_TYP")]
        public int psocdSeriousEventNM { get { return GetFieldInt("PSO_NQF_TYP_NM_CODE"); } set { SetField("PSO_NQF_TYP_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLOOD_PROD")]
        public int psocdTypeBldProduct { get { return GetFieldInt("PSO_BLOOD_PROD_CODE"); } set { SetField("PSO_BLOOD_PROD_CODE", value); } }
        public string txtOtherTypeBldProduct { get { return GetFieldString("PSO_BLOOD_PROD_MEMO"); } set { SetField("PSO_BLOOD_PROD_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ISBT_CODE")]
        public int psocdISBTProductCode { get { return GetFieldInt("PSO_ISBT_CODE"); } set { SetField("PSO_ISBT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLD_PROD_EVENT")]
        public int psocdDescribeEvent { get { return GetFieldInt("PSO_BLOOD_PROD_EVT_CODE"); } set { SetField("PSO_BLOOD_PROD_EVT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLD_PROD_EVENT")]
        public int psocdDescribeEventNM { get { return GetFieldInt("PSO_BLOOD_PROD_EVT_NM_CODE"); } set { SetField("PSO_BLOOD_PROD_EVT_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLOOD_INCRT_AC")]
        public int psocdIncorrectAction { get { return GetFieldInt("PSO_BLOOD_INCRT_ACTION_CODE"); } set { SetField("PSO_BLOOD_INCRT_ACTION_CODE", value); } }
        public string txtOtherIncorrectAction { get { return GetFieldString("PSO_BLOOD_INCRT_ACTION_MEMO"); } set { SetField("PSO_BLOOD_INCRT_ACTION_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdChkDocument { get { return GetFieldInt("PSO_TWO_PERSN_CODE"); } set { SetField("PSO_TWO_PERSN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLOOD_VOL")]
        public int psocdBloodVolume { get { return GetFieldInt("PSO_BLOOD_VOL_CODE"); } set { SetField("PSO_BLOOD_VOL_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BLOOD_TRANS_RT")]
        public int psocdAdminRate { get { return GetFieldInt("PSO_RATE_ADMIN_CODE"); } set { SetField("PSO_RATE_ADMIN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DISC_PROC")]
        public int psocdProcessDiscovered { get { return GetFieldInt("PSO_DISC_PROC_CODE"); } set { SetField("PSO_DISC_PROC_CODE", value); } }
        public string txtOtherProcessDiscovered { get { return GetFieldString("PSO_DISC_PROC_MEMO"); } set { SetField("PSO_DISC_PROC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ORIG_PROC")]
        public int psocdProcessOriginated { get { return GetFieldInt("PSO_ORIG_PROC_CODE"); } set { SetField("PSO_ORIG_PROC_CODE", value); } }
        public string txtOtherProcessOriginated { get { return GetFieldString("PSO_ORIG_PROC_MEMO"); } set { SetField("PSO_ORIG_PROC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DEVIC_DESC")]
        public int psocdDeviceEventDesc { get { return GetFieldInt("PSO_DEVICE_DESC_CODE"); } set { SetField("PSO_DEVICE_DESC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DEVIC_FAIL_DES")]
        public int psocdDeviceFailureType { get { return GetFieldInt("PSO_DEVICE_FAIL_DESC_CODE"); } set { SetField("PSO_DEVICE_FAIL_DESC_CODE", value); } }
        //public string psocdDeviceFailureType { get { return GetFieldString("PSO_DEVICE_FAIL_DESC_CODE"); } set { SetField("PSO_DEVICE_FAIL_DESC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_OPR_ERR")]
        public int psocdOperatorErrorType { get { return GetFieldInt("PSO_OPR_ERR_CODE"); } set { SetField("PSO_OPR_ERR_CODE", value); } }
        public string txtOtherOperatorErrorType { get { return GetFieldString("PSO_OPR_ERR_MEMO"); } set { SetField("PSO_OPR_ERR_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdDeviceReuse { get { return GetFieldInt("PSO_DEVISE_REUSE_CODE"); } set { SetField("PSO_DEVISE_REUSE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_FALL_ASSIST")]
        public int psocdFallAssist { get { return GetFieldInt("PSO_FALL_ASSIST_CODE"); } set { SetField("PSO_FALL_ASSIST_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdFallObserved { get { return GetFieldInt("PSO_FALL_CHECK_CODE"); } set { SetField("PSO_FALL_CHECK_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_FALLOBSVR")]
        public int psocdWhoObserved { get { return GetFieldInt("PSO_FALL_OBSVR_CODE"); } set { SetField("PSO_FALL_OBSVR_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdPhysicalInjury { get { return GetFieldInt("PSO_IS_FALL_INJURY_CODE"); } set { SetField("PSO_IS_FALL_INJURY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INJ_TYPE")]
        public int psocdInjuryType { get { return GetFieldInt("PSO_INJURY_TYP_CODE"); } set { SetField("PSO_INJURY_TYP_CODE", value); } }
        public string txtOtherInjuryType { get { return GetFieldString("PSO_INJURY_TYP_MEMO"); } set { SetField("PSO_INJURY_TYP_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PAT_ACTION")]
        public int psocdPatientDoing { get { return GetFieldInt("PSO_PAT_ACT_FALL_CODE"); } set { SetField("PSO_PAT_ACT_FALL_CODE", value); } }
        public string txtOtherPatientDoing { get { return GetFieldString("PSO_PAT_ACT_FALL_MEMO"); } set { SetField("PSO_PAT_ACT_FALL_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdRiskAssessment { get { return GetFieldInt("PSO_RISK_DOC_CODE"); } set { SetField("PSO_RISK_DOC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdDeterminedForFall { get { return GetFieldInt("PSO_FALL_RSK_CODE"); } set { SetField("PSO_FALL_RSK_CODE", value); } }
        public string psotxtOtherProtocolFall { get { return GetFieldString("PSO_INTERVEN_MEMO"); } set { SetField("PSO_INTERVEN_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdMedicationFall { get { return GetFieldInt("PSO_UNDER_MED_RISK_CODE"); } set { SetField("PSO_UNDER_MED_RISK_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdMedicationContributed { get { return GetFieldInt("PSO_MED_FALL_CONTRIB_CODE"); } set { SetField("PSO_MED_FALL_CONTRIB_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SUBS_TYP")]
        public int psocdSubstanceTypeINC { get { return GetFieldInt("PSO_SUBS_INV_INC_CODE"); } set { SetField("PSO_SUBS_INV_INC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SUBS_TYP")]
        public int psocdSubstanceTypeNM { get { return GetFieldInt("PSO_SUBS_INV_NM_CODE"); } set { SetField("PSO_SUBS_INV_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SUBS_TYP")]
        public int psocdSubstanceTypeUSC { get { return GetFieldInt("PSO_SUBS_INV_USC_CODE"); } set { SetField("PSO_SUBS_INV_USC_CODE", value); } }
        public string txtOtherSubstanceType { get { return GetFieldString("PSO_SUBS_INV_MEMO"); } set { SetField("PSO_SUBS_INV_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_TYPE")]
        public int psocdMedicationTypeINC { get { return GetFieldInt("PSO_MED_TYPE_INC_CODE"); } set { SetField("PSO_MED_TYPE_INC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_TYPE")]
        public int psocdMedicationTypeNM { get { return GetFieldInt("PSO_MED_TYPE_NM_CODE"); } set { SetField("PSO_MED_TYPE_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_TYPE")]
        public int psocdMedicationTypeUSC { get { return GetFieldInt("PSO_MED_TYPE_USC_CODE"); } set { SetField("PSO_MED_TYPE_USC_CODE", value); } }
        public string txtIngredients{ get { return GetFieldString("PSO_PREP_INGRD_MEMO"); } set { SetField("PSO_PREP_INGRD_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BIO_PROD_TYP")]
        public int psocdBiologicalType { get { return GetFieldInt("PSO_BIO_PROD_CODE"); } set { SetField("PSO_BIO_PROD_CODE", value); } }
        public string txtLotNum { get { return GetFieldString("PSO_LOT_NUMBER_TEXT"); } set { SetField("PSO_LOT_NUMBER_TEXT", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_NUT_PROD_TYP")]
        public int psocdNutritionalProduct { get { return GetFieldInt("PSO_NUT_PROD_CODE"); } set { SetField("PSO_NUT_PROD_CODE", value); } }
        public string txtOtherNutritionalProd { get { return GetFieldString("PSO_NUT_PROD_MEMO"); } set { SetField("PSO_NUT_PROD_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_EVNT_CHARC")]
        public int psocdBestEventINC { get { return GetFieldInt("PSO_MED_DESC_INC_CODE"); } set { SetField("PSO_MED_DESC_INC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_EVNT_CHARC")]
        public int psocdBestEventNM { get { return GetFieldInt("PSO_MED_DESC_NM_CODE"); } set { SetField("PSO_MED_DESC_NM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_EVNT_CHARC")]
        public int psocdBestEventUSC { get { return GetFieldInt("PSO_MED_DESC_USC_CODE"); } set { SetField("PSO_MED_DESC_USC_CODE", value); } }
        public string txtOtherIncorrAction { get { return GetFieldString("PSO_MED_INCRT_ACTION_MEMO"); } set { SetField("PSO_MED_INCRT_ACTION_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_INCRT_DOSE")]
        public int cdIncorrectDose { get { return GetFieldInt("PSO_MED_INCRT_DOSE_CODE"); } set { SetField("PSO_MED_INCRT_DOSE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_INCRT_TIME")]
        public int cdIncorrTiming { get { return GetFieldInt("PSO_MED_INCRT_TIME_CODE"); } set { SetField("PSO_MED_INCRT_TIME_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_INCRT_RATE")]
        public int cdIncorrRate { get { return GetFieldInt("PSO_MED_INCRT_RATE_CODE"); } set { SetField("PSO_MED_INCRT_RATE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_INCRT_STRN")]
        public int cdIncorrStrength { get { return GetFieldInt("PSO_MED_INCRT_STRN_CODE"); } set { SetField("PSO_MED_INCRT_STRN_CODE", value); } }
        //public string dtExpireDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", value); } }
        public string dtExpireDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdDocHistory { get { return GetFieldInt("PSO_HIST_DOC_CODE"); } set { SetField("PSO_HIST_DOC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MED_CONTRAIND")]
        public int psocdContradictions { get { return GetFieldInt("PSO_MED_CONTR_CODE"); } set { SetField("PSO_MED_CONTR_CODE", value); } }
        public string txtContradictions { get { return GetFieldString("PSO_MED_CONTR_MEMO"); } set { SetField("PSO_MED_CONTR_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ORIGIN_STAGE")]
        public int psocdEventOriginate { get { return GetFieldInt("PSO_EVNT_STG_CODE"); } set { SetField("PSO_EVNT_STG_CODE", value); } }
        public string txtOtherEventOriginate { get { return GetFieldString("PSO_EVNT_STG_MEMO"); } set { SetField("PSO_EVNT_STG_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INT_ROUTE")]
        public int psocdIntendedRoute { get { return GetFieldInt("PSO_INT_ROUTE_CODE"); } set { SetField("PSO_INT_ROUTE_CODE", value); } }
        public string txtOtherIntendedRoute { get { return GetFieldString("PSO_INT_ROUTE_MEMO"); } set { SetField("PSO_INT_ROUTE_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ACT_ROUTE")]
        public int psocdActualRoute { get { return GetFieldInt("PSO_ACT_ROUTE_CODE"); } set { SetField("PSO_ACT_ROUTE_CODE", value); } }
        public string txtOtherActualRoute { get { return GetFieldString("PSO_ACT_ROUTE_MEMO"); } set { SetField("PSO_ACT_ROUTE_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SURG_PROC")]
        public int cdICD9CM { get { return GetFieldInt("PSO_SURG_PROC_CODE"); } set { SetField("PSO_SURG_PROC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ASA_CLASS")]
        public int psocdDocumentedASA{ get { return GetFieldInt("PSO_DOCUMNTD_ASA_CODE"); } set { SetField("PSO_DOCUMNTD_ASA_CODE", value); } }
        public string txtDescProcdure{ get { return GetFieldString("PSO_EVNT_PROC_MEMO"); } set { SetField("PSO_EVNT_PROC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdProcEmergency { get { return GetFieldInt("PSO_EMR_SURG_CODE"); } set { SetField("PSO_EMR_SURG_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_TIME_EVNT_DISC")]
        public int psocdEventDiscovered { get { return GetFieldInt("PSO_TIME_SURGEVT_DISC_CODE"); } set { SetField("PSO_TIME_SURGEVT_DISC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PROC_DURTN")]
        public int psocdProcLength { get { return GetFieldInt("PSO_LNGTH_PROC_CODE"); } set { SetField("PSO_LNGTH_PROC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SED_ANES_TYP")]
        public int psocdAnesthesiaType { get { return GetFieldInt("PSO_SED_ANES_TYP_CODE"); } set { SetField("PSO_SED_ANES_TYP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SED_LVL")]
        public int psocdSedationLevel { get { return GetFieldInt("PSO_LVL_SEDTN_CODE"); } set { SetField("PSO_LVL_SEDTN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ANES_ADMIN")]
        public int psocdAdminAnesthesia { get { return GetFieldInt("PSO_ANES_PRSN_CODE"); } set { SetField("PSO_ANES_PRSN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdAnesthesiologist { get { return GetFieldInt("PSO_ANES_SUPVSN_CODE"); } set { SetField("PSO_ANES_SUPVSN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PROVD_SPCL")]
        public int psocdSugicalSpeciality { get { return GetFieldInt("PSO_SPCLTY_PROVD_CODE"); } set { SetField("PSO_SPCLTY_PROVD_CODE", value); } }
        public string txtOtherSugicalSpeciality { get { return GetFieldString("PSO_SPCLTY_PROVD_MEMO"); } set { SetField("PSO_SPCLTY_PROVD_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EVNT_DESC_TYP")]
        public int psocdBestDecs { get { return GetFieldInt("PSO_EVNT_TYP_CODE"); } set { SetField("PSO_EVNT_TYP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SURG_COMPL")]
        public int psocdMajorComplications { get { return GetFieldInt("PSO_MAJ_COMPL_CODE"); } set { SetField("PSO_MAJ_COMPL_CODE", value); } }
        public string txtOtherMajorComplications { get { return GetFieldString("PSO_MAJ_COMPL_MEMO"); } set { SetField("PSO_MAJ_COMPL_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_RESP_SUPP_TYP")]
        public int psocdRespiratorySupport { get { return GetFieldInt("PSO_RESP_SUPP_CODE"); } set { SetField("PSO_RESP_SUPP_CODE", value); } }
        public string txtOtherRespiratorySupport { get { return GetFieldString("PSO_RESP_SUPP_MEMO"); } set { SetField("PSO_RESP_SUPP_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int psocdRestainedObject { get { return GetFieldInt("PSO_UNINT_RETOBJ_CODE"); } set { SetField("PSO_UNINT_RETOBJ_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_OBJ_TYP")]
        public int psocdObjectRestained { get { return GetFieldInt("PSO_RET_OBJ_TYP_CODE"); } set { SetField("PSO_RET_OBJ_TYP_CODE", value); } }
        public string txtOtherObjectRestained { get { return GetFieldString("PSO_RET_OBJ_TYP_MEMO"); } set { SetField("PSO_RET_OBJ_TYP_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_CNNT_PERF")]
        public int psocdCountPerformed { get { return GetFieldInt("PSO_WAS_CNT_PERF_CODE"); } set { SetField("PSO_WAS_CNT_PERF_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_CNT_STAT")]
        public int psocdCountStatus { get { return GetFieldInt("PSO_REP_CNT_STAT_CODE"); } set { SetField("PSO_REP_CNT_STAT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdXRayObtained { get { return GetFieldInt("PSO_XRAY_OBTN_CODE"); } set { SetField("PSO_XRAY_OBTN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdRadiopaque { get { return GetFieldInt("PSO_RDIOPQ_OBJ_RET_CODE"); } set { SetField("PSO_RDIOPQ_OBJ_RET_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SURG_ADV_OUTC")]
        public int psocdCharacterizedSurgery { get { return GetFieldInt("PSO_CHARC_SURG_OUTC_CODE"); } set { SetField("PSO_CHARC_SURG_OUTC_CODE", value); } }
        public string txtOtherCharSurgery { get { return GetFieldString("PSO_CHARC_SURG_OUTC_MEMO"); } set { SetField("PSO_CHARC_SURG_OUTC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_BURN_FIRE_OCC")]
        public int psocdWhichOccured { get { return GetFieldInt("PSO_BURN_ROOMFIRE_OCC_CODE"); } set { SetField("PSO_BURN_ROOMFIRE_OCC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SURGINV_INC_AC")]
        public int psocdIncorrectProc { get { return GetFieldInt("PSO_SURG_INV_INCRT_ACTN_CODE"); } set { SetField("PSO_SURG_INV_INCRT_ACTN_CODE", value); } }
        public string txtOtherIncorrectProc { get { return GetFieldString("PSO_SURG_INV_INCRT_ACTN_MEMO"); } set { SetField("PSO_SURG_INV_INCRT_ACTN_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ANESEVNT_CHARC")]
        public int psocdEventAnesthesia { get { return GetFieldInt("PSO_ANES_EVNT_CHAREC_CODE"); } set { SetField("PSO_ANES_EVNT_CHAREC_CODE", value); } }
        public string txtOtherEventAnesthesia { get { return GetFieldString("PSO_ANES_EVNT_CHAREC_MEMO"); } set { SetField("PSO_ANES_EVNT_CHAREC_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_AIR_PROB")]
        public int psocdAirwayMngmnt { get { return GetFieldInt("PSO_AIRWAY_PROB_CODE"); } set { SetField("PSO_AIRWAY_PROB_CODE", value); } }
        public string txtOtherAirwayMngmnt { get { return GetFieldString("PSO_AIRWAY_PROB_MEMO"); } set { SetField("PSO_AIRWAY_PROB_MEMO", value); } }
        public int txtVersionNumber { get { return GetFieldInt("PSO_VERSION_NUM"); } set { SetField("PSO_VERSION_NUM", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_EXTRACT_STATUS")]
        public int psocdExtractStatus { get { return GetFieldInt("PSO_EXTRACT_STATUS_CODE"); } set { SetField("PSO_EXTRACT_STATUS_CODE", value); } }
		#endregion
		
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);
		}

		internal EventPSO(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "EVENT_PSO";
            this.m_sKeyField = "EVT_PSO_ROW_ID";
            this.m_sFilterClause = string.Empty;
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            base.InitChildren(sChildren);
            //Add all object Children into the Children collection from our "sChildren" list.
            this.m_sParentClassName = "EventPSO";
            //Add all object Children into the Children collection from our "sChildren" list.
            base.Initialize();

			//Anu Tennyson : Commneted No need for belo line since this is not a child class of the event'
			//this.m_sParentClassName = "Event";
		}
        #region Child Implementation
        private string[,] sChildren = {{"DeviceInfoList","DeviceInfoList"},
                                       {"MedInfoList","MedInfoList"},
                                       {"LinkedEventInfoList","LinkedEventInfoList"},
                                       //{"PatientPSOList","PatientPSOList"},
                                       //{"RescueInterventionCodes","DataSimpleList"},    //Patient table child field--neha goel
                                       {"psolstProtocolFall","DataSimpleList"}, 
                                      //{"MotherAdvOutcomeCodes","DataSimpleList"},     //Patient table child field--neha goel
                                       //{"MaternalInjuryTypeCodes","DataSimpleList"},   //Patient table child field--neha goel
                                       //{"NeoNatalOutcomeTypeCodes","DataSimpleList"},  //Patient table child field--neha goel                                       
                                       {"psolstIncorrectAction","DataSimpleList"}, 
                                       //{"InterventionTypeCodes","DataSimpleList"}, 
                                       {"psolstFactors","DataSimpleList"},
                                       {"psolstFactorsNM","DataSimpleList"},
									  };

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "DataSimpleList":
                    DataSimpleList objList = obj as DataSimpleList;
                    switch (childName)
                    {
                        
                        //case "RescueInterventionCodes":   
                        //    objList.Initialize("PSO_RESCUE_INTV_TYP_CODE", "PAT_X_RESC_INERVEN_PSO", "PSO_ROW_ID"); //Patient table child field--neha goel
                        //    break;
                        case "psolstProtocolFall":
                            objList.Initialize("PSO_INTERVEN_CODE", "EVENT_X_INTERVEN_PSO", "EVT_PSO_ROW_ID");
                            break;
                        //case "MotherAdvOutcomeCodes":
                        //    objList.Initialize("PSO_MAT_OUTCM_CODE", "PAT_X_MAT_OUTCM", "PSO_ROW_ID"); //Patient table child field--neha goel
                        //    break;
                        //case "MaternalInjuryTypeCodes":
                       //    objList.Initialize("PSO_MAT_INJ_CODE", "PAT_X_MAT_INJ", "PSO_ROW_ID");//Patient table child field--neha goel
                        //    break;
                        //case "NeoNatalOutcomeTypeCodes":
                       //    objList.Initialize("PSO_NN_ADV_OUTCM_CODE", "PAT_X_NN_OUTCM_TYP", "PSO_ROW_ID"); //Patient table child field--neha goel
                        //    break;                        
                        case "psolstIncorrectAction":
                            objList.Initialize("PSO_MED_INCRT_ACTION_CODE", "EVENT_X_MED_INV_PSO", "EVT_PSO_ROW_ID");
                            break;
                        //case "InterventionTypeCodes":
                        //    objList.Initialize("PSO_INTRVN_TYP_CODE", "PAT_X_PRESS_ULC_PSO", "PSO_ROW_ID"); //Patient table child field--neha goel
                        //    break;
                        case "psolstFactors":
                            objList.Initialize("PSO_CONTRIB_FAC_CODE", "EVENT_X_CONTRI_FAC_PSO", "EVT_PSO_ROW_ID");
                            break;
                        case "psolstFactorsNM":
                            objList.Initialize("PSO_CONTRIB_FAC_NM_CODE", "EVENT_X_CONTRI_FAC_NM_PSO", "EVT_PSO_ROW_ID");
                            break;
                        
                    }
                    break;
            }
            
        }
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "DeviceInfoList":
                    (itemValue as DeviceInfo).EvtPsoRowId = this.EvtPsoRowId;
                    break;
                case "MedInfoList":
                    (itemValue as MedicationInfo).EvtPsoRowId = this.EvtPsoRowId;
                    break;
                case "LinkedEventInfoList":
                    (itemValue as LinkedEventInfo).EvtPsoRowId = this.EvtPsoRowId;
                    break;
                //case "PatientPSOList":
                //    (itemValue as PatientPSO).EvtPsoRowId = this.EvtPsoRowId;
                //    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }
        
        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "DeviceInfoList":
                    foreach (DeviceInfo item in (childValue as DeviceInfoList))
                    {
                        if (item.IsNew)
                        {
                            item.EvtPsoRowId = this.EvtPsoRowId;
                        }
                    }
                    (childValue as IPersistence).Save();
                    break;
                case "MedInfoList":
                    foreach (MedicationInfo item in (childValue as MedInfoList))
                    {
                        if (item.IsNew)
                        {
                            item.EvtPsoRowId = this.EvtPsoRowId;
                        }
                    }
                    (childValue as IPersistence).Save();
                    break;
                case "LinkedEventInfoList":
                    foreach (LinkedEventInfo item in (childValue as LinkedEventInfoList))
                    {
                        if (item.IsNew)
                        {
                            item.EvtPsoRowId = this.EvtPsoRowId;
                        }
                    }
                    (childValue as IPersistence).Save();
                    break;
                //case "PatientPSOList":
                //    foreach (PatientPSO item in (childValue as PatientPSOList))
                //    {
                //        if (item.IsNew)
                //        {
                //            item.EvtPsoRowId = this.EvtPsoRowId; ;
                //        }
                //    }
                //    (childValue as IPersistence).Save();
                //    break;
                //case "RescueInterventionCodes":  //Patient table child field--neha goel
                //    if (isNew)
                //    {
                //        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                //    }
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                case "psolstProtocolFall":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                //case "MotherAdvOutcomeCodes":  //Patient table child field--neha goel
                //    if (isNew)
                //    {
                //        (childValue as DataSimpleList).SetKeyValue(this.PsoRowId);
                //    }
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                //case "MaternalInjuryTypeCodes":  //Patient table child field--neha goel
                //    if (isNew)
                //    {
                //        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                //    }
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                //case "NeoNatalOutcomeTypeCodes":  //Patient table child field--neha goel
                //    if (isNew)
                //    {
                //        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                //    }
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                case "psolstIncorrectAction":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                //case "InterventionTypeCodes":   //Patient table child field--neha goel
                //    if (isNew)
                //    {
                //        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                //    }
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                case "psolstFactors":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstFactorsNM":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.EvtPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;

            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        public DeviceInfoList DeviceInfoList
        {
            get
            {
                DeviceInfoList objList = base.m_Children["DeviceInfoList"] as DeviceInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public MedInfoList MedInfoList
        {
            get
            {
                MedInfoList objList = base.m_Children["MedInfoList"] as MedInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public LinkedEventInfoList LinkedEventInfoList
        {
            get
            {
                LinkedEventInfoList objList = base.m_Children["LinkedEventInfoList"] as LinkedEventInfoList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        //public PatientPSOList PatientPSOList
        //{
        //    get
        //    {
        //        PatientPSOList objList = base.m_Children["PatientPSOList"] as PatientPSOList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
        //        return objList;
        //    }
        //}
        //[ExtendedTypeAttribute(RMExtType.CodeList, "PSO_RESCUE_INTV")]
        //public DataSimpleList RescueInterventionCodes    //Patient table child field--neha goel
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["RescueInterventionCodes"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_INTERVENTION")]
        public DataSimpleList psolstProtocolFall
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstProtocolFall"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        //[ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_OUTCM")]   //Patient table child field--neha goel
        //public DataSimpleList MotherAdvOutcomeCodes
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["MotherAdvOutcomeCodes"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        //[ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_INJ_TYP")]   //Patient table child field--neha goel
        //public DataSimpleList MaternalInjuryTypeCodes
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["MaternalInjuryTypeCodes"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        //[ExtendedTypeAttribute(RMExtType.CodeList, "PSO_NN_OUTCM_TYP")]      //Patient table child field--neha goel
        //public DataSimpleList NeoNatalOutcomeTypeCodes
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["NeoNatalOutcomeTypeCodes"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MED_INCRT_AC")]
        public DataSimpleList psolstIncorrectAction
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstIncorrectAction"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        //[ExtendedTypeAttribute(RMExtType.CodeList, "PSO_INTERVN_TYP")]        //Patient table child field--neha goel
        //public DataSimpleList InterventionTypeCodes
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["InterventionTypeCodes"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_CONTRIB_FAC")]
        public DataSimpleList psolstFactors
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstFactors"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_CONTRIB_FAC")]
        public DataSimpleList psolstFactorsNM
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstFactorsNM"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        #endregion
	}
}
