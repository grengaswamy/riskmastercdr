﻿using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for DemandOfferList.
    /// </summary>
    public class DemandOfferList : DataCollection
    {
        internal DemandOfferList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "DEMAND_OFFER_ROW_ID";
            this.SQLFromTable = "DEMAND_OFFER";
            this.TypeName = "DemandOffer";
        }
        public new DemandOffer this[int keyValue] { get { return base[keyValue] as DemandOffer; } }
        public new DemandOffer AddNew() { return base.AddNew() as DemandOffer; }
        public DemandOffer Add(DemandOffer obj) { return base.Add(obj) as DemandOffer; }
        public new DemandOffer Add(int keyValue) { return base.Add(keyValue) as DemandOffer; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}