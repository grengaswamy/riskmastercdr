
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPhysicianPrivilegeList.
	/// </summary>
	public class PhysicianPrivilegeList : DataCollection
	{
		internal PhysicianPrivilegeList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PRIV_ID";
			this.SQLFromTable =	"PHYS_PRIVS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PhysicianPrivilege";
		}
		public new PhysicianPrivilege this[int keyValue]{get{return base[keyValue] as PhysicianPrivilege;}}
		public new PhysicianPrivilege AddNew(){return base.AddNew() as PhysicianPrivilege;}
		public  PhysicianPrivilege Add(PhysicianPrivilege obj){return base.Add(obj) as PhysicianPrivilege;}
		public new PhysicianPrivilege Add(int keyValue){return base.Add(keyValue) as PhysicianPrivilege;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}