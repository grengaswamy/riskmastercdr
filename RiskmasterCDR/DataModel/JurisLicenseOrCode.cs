
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for JurisAndLicenseCodes.cs.
	/// </summary>
	//TODO: Verify that CodeLicenseNumber is an acceptable default.
	[Riskmaster.DataModel.Summary("ENTITY_X_CODELICEN","TABLE_ROW_ID", "CodeLicenseNumber")]
	public class JurisLicenseOrCode : DataObject
	{
		#region Database Field List
			private string[,] sFields = {
					{"TableRowId","TABLE_ROW_ID"},
					{"DttmRcdAdded","DTTM_RCD_ADDED"},
					{"AddedByUser","ADDED_BY_USER"},
					{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
					{"UpdatedByUser","UPDATED_BY_USER"},
					{"EntityId","ENTITY_ID"},
					{"DeletedFlag","DELETED_FLAG"},
					{"Jurisdiction","JURIS_ROW_ID"},
					{"CodeLicenseNumber","CODELICENSE_NUMBER"},
					{"EffectiveDate","EFFECTIVE_DATE"},
					{"ExpirationDate","EXPIRATION_DATE"},
					{"LobAll","LOB_ALL"},
					{"LobWC","LOB_WC"},
					{"LobSTD","LOB_STD"},
					{"LobVA","LOB_VA"},
					{"LobGL","LOB_GL"},
			};
		public int  TableRowId{get{  return GetFieldInt("TABLE_ROW_ID");}set{ SetField("TABLE_ROW_ID",value);}}
		public string DttmRcdAdded{get{ return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string AddedByUser{get{ return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		public string DttmRcdLastUpd{get{ return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{ return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public int EntityId{get{ return GetFieldInt("ENTITY_ID");}set{SetField("ENTITY_ID",value);}}
		public bool DeletedFlag{get{ return GetFieldBool("DELETED_FLAG");}set{SetField("DELETED_FLAG",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"STATES")]
		public int  Jurisdiction{get{  return GetFieldInt("JURIS_ROW_ID");}set{ SetField("JURIS_ROW_ID",value);}}
		public string CodeLicenseNumber{get{ return GetFieldString("CODELICENSE_NUMBER");}set{SetField("CODELICENSE_NUMBER",value);}}
		public string EffectiveDate{get{ return GetFieldString("EFFECTIVE_DATE");}set{SetField("EFFECTIVE_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string ExpirationDate{get{ return GetFieldString("EXPIRATION_DATE");}set{SetField("EXPIRATION_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int LobAll{get{ return GetFieldInt("LOB_ALL");}set{SetField("LOB_ALL",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int LobWC{get{ return GetFieldInt("LOB_WC");}set{SetField("LOB_WC",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int LobSTD{get{ return GetFieldInt("LOB_STD");}set{SetField("LOB_STD",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int LobVA{get{ return GetFieldInt("LOB_VA");}set{SetField("LOB_VA",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int LobGL{get{ return GetFieldInt("LOB_GL");}set{SetField("LOB_GL",value);}}
		#endregion

		internal JurisLicenseOrCode(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}

		new private void Initialize()
		{

			this.m_sTableName = "ENTITY_X_CODELICEN";
			this.m_sKeyField = "TABLE_ROW_ID";
			this.m_sFilterClause = "";

			//Add all object Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize(); //Moved after most init logic so that scripting can be called successfully.
		}

        /// <summary>
        /// Loads the data entity juris.Added for Froi Migration- Mits 33585,33586,33587

        /// </summary>
        /// <param name="iEntityId">The i entity identifier.</param>
        /// <param name="iJurisRowID">The i juris row identifier.</param>
        /// <returns></returns>
        public int LoadDataEntityJuris(int iEntityId, int iJurisRowID)
        {
            if (iEntityId > 0 && iJurisRowID > 0)
            {
                bool success = false;
                string sqlStatement = string.Format("SELECT TABLE_ROW_ID FROM ENTITY_X_CODELICEN WHERE ENTITY_ID={0} AND JURIS_ROW_ID = {1} AND (DELETED_FLAG <> 1 OR DELETED_FLAG <> -1) ORDER BY EFFECTIVE_DATE DESC", iEntityId, iJurisRowID);
                int tableRowId = 0;
                using (Db.DbReader dbReader = this.Context.DbConnLookup.ExecuteReader(sqlStatement))
                {
                    if (!object.ReferenceEquals(dbReader, null) && dbReader.Read())
                    { 
                        tableRowId = Common.Conversion.CastToType<int>(dbReader.GetValue("TABLE_ROW_ID").ToString(), out success);
                    }
                }
                
                if (success)
                {
                    this.MoveTo(tableRowId);
                }
            }

            return -1;
        }
	}
}