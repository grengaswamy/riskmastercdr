
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forInvoiceList.
	/// </summary>
	public class BrsInvoiceList : DataCollection
	{
		internal BrsInvoiceList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"INVOICE_ID";
			this.SQLFromTable =	"INVOICE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BrsInvoice";
		}
		public new BrsInvoice this[int keyValue]{get{return base[keyValue] as BrsInvoice;}}
		public new BrsInvoice AddNew(){return base.AddNew() as BrsInvoice;}
		public  BrsInvoice Add(BrsInvoice obj){return base.Add(obj) as BrsInvoice;}
		public new BrsInvoice Add(int keyValue){return base.Add(keyValue) as BrsInvoice;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}