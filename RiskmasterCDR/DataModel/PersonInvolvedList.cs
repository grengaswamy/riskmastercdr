using System;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// Note: Unlike legacy version, this list is fully indexed by PI_ROW_ID.
	/// </summary>
	public class PersonInvolvedList : DataCollection
	{
		internal PersonInvolvedList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_ROW_ID";
			this.SQLFromTable =	"PERSON_INVOLVED";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PersonInvolved";
		}

		public new PersonInvolved this[int keyValue]{get{return base[keyValue] as PersonInvolved;}}

		public PersonInvolved AddNewPiByType(PiType ePiType)
		{
			string typeName = "";
			switch(ePiType)
			{
				case	PiType.PiEmployee:
					typeName = "PiEmployee";
					break;
				case	PiType.PiMedicalStaff:
					typeName = "PiMedicalStaff";
					break;
				case	PiType.PiOther:
					typeName = "PiOther";
					break;
                case PiType.PiDriver:
                    typeName = "PiDriver"; // mbahl3 Driver changes
                    break;
				case	PiType.PiPatient:
					typeName = "PiPatient";
					break;
				case	PiType.PiPhysician:
					typeName = "PiPhysician";
					break;
				case	PiType.PiWitness:
					typeName = "PiWitness";
					break;
				default:
					throw new DataModelException("PersonInvolvedList.Exception.PiTypeNotImplemented");
			}
			PersonInvolved objPI = Context.Factory.GetDataModelObject(typeName,true) as PersonInvolved;
			return this.Add(objPI) as PersonInvolved;
		}
		//Throw error - must use AddNewPiType()
        public override DataObject AddNew() { throw new DataModelException(Globalization.GetString("PersonInvolvedList.Exception.SpecificPiTypeRequired", this.Context.ClientId)); }
		protected override string DerivedTypeNameFromKey(int keyValue)
		{
			int piTypeCode = (int)Context.DbConnLookup.ExecuteInt("SELECT PI_TYPE_CODE FROM PERSON_INVOLVED WHERE PI_ROW_ID=" + keyValue);
			switch(Context.LocalCache.GetShortCode(piTypeCode))
			{
				case	"E":
					return "PiEmployee";
				case	"MED":
					return "PiMedicalStaff";
				case	"O":
					return  "PiOther";
                case "D":
                    return "PiDriver"; // Driver changes
				case	"P":
					return  "PiPatient";
				case	"PHYS":
					return  "PiPhysician";
				case	"W":
					return  "PiWitness";
				default:
					throw new DataModelException("PersonInvolvedList.Exception.PiTypeNotImplemented");
			}		
		}


		//Specific Add by Pi Type
		public  PiEmployee Add(PiEmployee obj){return base.Add(obj) as PiEmployee;}
		public  PiMedicalStaff Add(PiMedicalStaff obj){return base.Add(obj) as PiMedicalStaff;}
		public  PiOther Add(PiOther obj){return base.Add(obj) as PiOther;}
        public PiDriver Add(PiDriver obj) { return base.Add(obj) as PiDriver; }
		public  PiPatient Add(PiPatient obj){return base.Add(obj) as PiPatient;}
		public  PiPhysician Add(PiPhysician obj){return base.Add(obj) as PiPhysician;}
		public  PiWitness Add(PiWitness obj){return base.Add(obj) as PiWitness;}

		public new PersonInvolved Add(int keyValue){return base.Add(keyValue) as PersonInvolved;}

		//Fetch PI Logic
		public PersonInvolved GetPrimaryPi()
		{
			int minRowId=-1; 
			foreach(int PiRowId in this.m_keySet.Keys)
				if(minRowId==-1 || PiRowId<minRowId)
					minRowId=PiRowId;

			if(minRowId==-1)
				return null;
			else
				return this[minRowId]; //-1 Returns Null
		}
		public Entity GetPiEntityByPiRowId(int PIRowId)
		{
			PersonInvolved objPI = this[PIRowId];
			if(objPI != null)
				return objPI.PiEntity;
			else
				return null;
		}
		public PersonInvolved GetPiByEID(int EID)
		{
			foreach(PersonInvolved objPI in this)
				if(objPI.PiEid == EID)
					return objPI;

			return null;
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}

        /// <summary>
        /// Gets the index of the pi entity identifier by.Added for Froi Migration- Mits 33585,33586,33587

        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public int GetPiEntityIdByIndex(int index)
        {
            int count = 1;
            foreach (PersonInvolved objPI in this)
            {
                if (count == index)
                {
                    return objPI.PiEid;
                }
                count++;
            }
            return 0;
        }

        // akaushik5 Added for JIRA 1108 Starts
        #region FROI Functions
        /// <summary>
        /// Gets the pi entity.
        /// </summary>
        /// <param name="objPersonInvolved">The object person involved.</param>
        /// <returns></returns>
        public Entity GetPiEntity(object objPersonInvolved)
        {
            return GetPiEntityByPiRowId((objPersonInvolved as PersonInvolved).PiRowId);
        }
        #endregion
        // akaushik5 Added for JIRA 1108 Ends
    }
}

