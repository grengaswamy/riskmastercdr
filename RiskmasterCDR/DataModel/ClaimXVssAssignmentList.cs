﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for ClaimXArbitrationList.
    /// </summary>
    public class ClaimXVssAssignmentList : DataCollection
    {
        internal ClaimXVssAssignmentList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "CLAIM_X_VSSASSIGNMENT";
            this.TypeName = "ClaimXVssAssignment";
        }

        public new ClaimXVssAssignment this[int keyValue] { get { return base[keyValue] as ClaimXVssAssignment; } }
        public new ClaimXVssAssignment AddNew() { return base.AddNew() as ClaimXVssAssignment; }
        public ClaimXVssAssignment Add(ClaimXVssAssignment obj) { return base.Add(obj) as ClaimXVssAssignment; }
        public new ClaimXVssAssignment Add(int keyValue) { return base.Add(keyValue) as ClaimXVssAssignment; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }
}
