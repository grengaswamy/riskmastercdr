﻿using System;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("DRIVER_STGNG", "DRIVER_ROW_ID")]
    public class DriverStgng:DataObject
    {
        public override string Default
        {
            get
            {
                if (this.DriverRowId > 0)
                    return Context.LocalCache.GetEntityLastFirstName(this.DriverEId);
                return "";
            }
        }
        private string[,] sChildren = {	{"DriverEntity","EntityStgng"}
           };
        
         #region Database Field List
        private string[,] sFields = {
														{"DriverEId", "DRIVER_EID"},
														{"DriverTypeCode", "DRIVER_TYPE"},
														{"LicenceDate", "LICENCE_DATE"},                                                   
                                                        {"DriversLicNo", "LICENCE_NUMBER"},
                                                        {"DriverRowId", "DRIVER_ROW_ID"},
                                                        {"MaritalStatCode", "MARITAL_STAT_CODE"},
                                                        {"RelToInsured","RELTN_INSRD"},
                                                        {"LicenceState","LICENCE_STATE"},
                                                        {"MVRIndicator","MVR_IND"},
                                                        {"DriverStatus","DRIVER_STATUS"},
                                                        {"SR22","SR"},
                                                        {"DriverClass","DRIVER_CLASS"},
                                                        {"PrincipalOprtr1","PO1"},
                                                        {"PrincipalOprtr2","PO2"},
                                                        {"PrincipalOprtr3","PO3"},
                                                        {"PartTimeOprtr1","PTO1"},
                                                        {"PartTimeOprtr2","PTO2"},
                                                        {"PartTimeOprtr3","PTO3"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
		};

        public int DriverEId
        {
            get
            {
                return GetFieldInt("DRIVER_EID");
            } 
            set
            {
                SetField("DRIVER_EID", value);
                SetFieldAndNavTo("DRIVER_EID", value, "DriverEntity");
            } 
        }
    //    [ExtendedTypeAttribute(RMExtType.Code, "DRIVER_TYPE")]
        public string DriverTypeCode { get { return GetFieldString("DRIVER_TYPE"); } set { SetField("DRIVER_TYPE", value); } }
        public string LicenceDate { get { return GetFieldString("LICENCE_DATE"); } set { SetField("LICENCE_DATE", Conversion.GetDate(value)); } }
        public string DriversLicNo { get { return GetFieldString("LICENCE_NUMBER"); } set { SetField("LICENCE_NUMBER", value); } }
        public int DriverRowId { get { return GetFieldInt("DRIVER_ROW_ID"); } set { SetField("DRIVER_ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "MARITAL_STATUS")]
        public string MaritalStatCode { get { return GetFieldString("MARITAL_STAT_CODE"); } set { SetField("MARITAL_STAT_CODE", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public string RelToInsured { get { return GetFieldString("RELTN_INSRD"); } set { SetField("RELTN_INSRD", value); } }
        public string LicenceState { get { return GetFieldString("LICENCE_STATE"); } set { SetField("LICENCE_STATE", value); } }
        public string MVRIndicator { get { return GetFieldString("MVR_IND"); } set { SetField("MVR_IND", value); } }
        public string DriverStatus { get { return GetFieldString("DRIVER_STATUS"); } set { SetField("DRIVER_STATUS", value); } }
        public string SR22 { get { return GetFieldString("SR"); } set { SetField("SR", value); } }
        public string DriverClass { get { return GetFieldString("DRIVER_CLASS"); } set { SetField("DRIVER_CLASS", value); } }
        public string PrincipalOprtr1 { get { return GetFieldString("PO1"); } set { SetField("PO1", value); } }
        public string PrincipalOprtr2 { get { return GetFieldString("PO2"); } set { SetField("PO2", value); } }
        public string PrincipalOprtr3 { get { return GetFieldString("PO3"); } set { SetField("PO3", value); } }
        public string PartTimeOprtr1 { get { return GetFieldString("PTO1"); } set { SetField("PTO1", value); } }
        public string PartTimeOprtr2 { get { return GetFieldString("PTO2"); } set { SetField("PTO2", value); } }
        public string PartTimeOprtr3 { get { return GetFieldString("PTO3"); } set { SetField("PTO3", value); } }
        // Pradyumna 1/14/2014 - Ends
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        internal DriverStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
                      this.SetFieldAndNavTo("DRIVER_EID", this.DriverEId, "DriverEntity", true);
        }
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            
            if (childName == "DriverEntity")
            {
                
                if ((childValue as DataObject).WillSave())
                    (childValue as EntityStgng).EntityTableId = Context.LocalCache.GetTableId("DRIVERS");
                (childValue as IPersistence).Save();
                this.m_Fields["DRIVER_EID"] = (childValue as EntityStgng).EntityId;
            }
            base.OnChildPreSave(childName, childValue);
        }
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            if (childValue is IPersistence)
               (childValue as IPersistence).Save();
        }
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "EntityStgng": //Is Entity
                    EntityStgng objEnt = (obj as EntityStgng);
                    objEnt.EntityTableId = Context.LocalCache.GetTableId("DRIVERS");
                    objEnt.LockEntityTableChange = true;
                    //no real way to lock entity class navigation from internal access.
                    break;

                default:
                    //No default process.
                    break;
            }
        }
        protected override void OnPreCommitDelete()
        {
            // BSB Go Ahead and Flag our Entity since by virtue of being in this particular
            // override we KNOW that the user initiated a deletion against us directly.
            this.DriverEntity.Delete(); // This call simply flags the entity in the RMX database.
            // True record deletion is not supported for entities within the 
            // base product by ANY MEANS.
        }
        public EntityStgng DriverEntity
        {
            get
            {
                EntityStgng objItem = base.m_Children["DriverEntity"] as EntityStgng;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("DriverEntity", (objItem as DataObject), this.DriverEId) as EntityStgng;
                return objItem;
            }
        }
        protected override void Clear()
        {
            //			m_PiRowId = 0;
            base.Clear();
        }
        public override void Refresh()
        {
            int l = this.DriverRowId;
            //				int l1 = m_PiRowId;
            int lEID = this.DriverRowId;

            base.Refresh();

            this.DriverRowId = l;
            //				m_PiRowId = l1;
            this.DriverRowId = lEID;
        }
        new private void Initialize()
        {

            this.m_sTableName = "DRIVER_STGNG";
            this.m_sKeyField = "DRIVER_ROW_ID";
            this.m_sFilterClause = "";

            
            base.InitFields(sFields);
           
            base.InitChildren(sChildren);
            this.m_sParentClassName = "";
            base.Initialize();  
            base.InitFields(sFields);
      
        
         
            base.Initialize(); 

        }
        //#region Supplemental Fields Exposed
        ////Supplementals are implemented in the base class but not exposed externally.
        ////This allows the derived class to control whether the object appears to have
        ////supplementals or not. 
        ////Entity exposes Supplementals.
        ////Amandeep Driver Supplementals added
        //internal override string OnBuildSuppTableName()
        //{
        //    return "DRIVER_SUPP";
        //}

        //public new Supplementals Supplementals
        //{
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        ////protected override void OnBuildNewUniqueId()
        ////{
        ////    this.KeyFieldValue = Context.GetNextUID("DRIVERS");
        ////}
        //#endregion
    }
}
