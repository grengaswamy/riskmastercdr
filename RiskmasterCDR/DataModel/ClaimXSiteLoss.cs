﻿using System;
using Riskmaster.Common;


namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_SITELOSS", "ROW_ID","CLAIM_ID")]
    public class ClaimXSiteLoss:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"RowId", "ROW_ID"},                                                        
														{"ClaimId", "CLAIM_ID"},
                                                        {"SiteId","SITE_ID"}, 
														{"LossPreventionRepresentative", "LOSS_PREVENTION_REPRESENTATIVE"},
                                                        {"LossPreventionContactName", "LOSS_PREVENTION_CONTACT_NAME"},
														{"FaxNumber", "FAX_NUMBER"},
														{"Email", "EMAIL"},														
														{"PreQuoteSurveyOrderedDate", "PRE_QUOTE_SURVEY_ORDERED_DATE"},
														{"PostBindSurveyOrderedDate", "POST_BIND_SURVEY_ORDERED_DATE"},
														{"LossPreventionRating", "LOSS_PREVENTION_RATING"},
														{"LossPreventionLastVisitDate", "LOSS_PREVENTION_LASTVISITDATE"},
                                                        {"InterimAuditorId", "INTERIM_AUDITOR_ID"},
														{"InterimAuditor", "INTERIM_AUDITOR"},
                                                        {"CheckAuditorId", "CHECK_AUDITOR_ID"},
                                                        {"CheckAuditor", "CHECK_AUDITOR"},
                                                        {"FinalAuditorId", "FINAL_AUDITOR_ID"},                                                        
                                                        {"FinalAuditor", "FINAL_AUDITOR"},                                                        
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                                        {"Insured", "ISINSURED"},
                                                        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                                                        {"AdjusterEId", "ASSIGNADJ_EID"}
                                                        //Ankit End
		};


        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.ChildLink, "SiteUnit")]
        public int SiteId { get { return GetFieldInt("SITE_ID"); } set { SetField("SITE_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }       
        public string LossPreventionRepresentative { get { return GetFieldString("LOSS_PREVENTION_REPRESENTATIVE"); } set { SetField("LOSS_PREVENTION_REPRESENTATIVE", value); } }        
        public string LossPreventionContactName { get { return GetFieldString("LOSS_PREVENTION_CONTACT_NAME"); } set { SetField("LOSS_PREVENTION_CONTACT_NAME", value); } }
        public string FaxNumber { get { return GetFieldString("FAX_NUMBER"); } set { SetField("FAX_NUMBER", value); } }
        public string Email { get { return GetFieldString("EMAIL"); } set { SetField("EMAIL", value); } }        
        public string PreQuoteSurveyOrderedDate { get { return GetFieldString("PRE_QUOTE_SURVEY_ORDERED_DATE"); } set { SetField("PRE_QUOTE_SURVEY_ORDERED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string PostBindSurveyOrderedDate { get { return GetFieldString("POST_BIND_SURVEY_ORDERED_DATE"); } set { SetField("POST_BIND_SURVEY_ORDERED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string LossPreventionRating { get { return GetFieldString("LOSS_PREVENTION_RATING"); } set { SetField("LOSS_PREVENTION_RATING", value); } }
        public string LossPreventionLastVisitDate { get { return GetFieldString("LOSS_PREVENTION_LASTVISITDATE"); } set { SetField("LOSS_PREVENTION_LASTVISITDATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string InterimAuditorId { get { return GetFieldString("INTERIM_AUDITOR_ID"); } set { SetField("INTERIM_AUDITOR_ID", value); } }
        public string InterimAuditor { get { return GetFieldString("INTERIM_AUDITOR"); } set { SetField("INTERIM_AUDITOR", value); } }
        public string CheckAuditorId { get { return GetFieldString("CHECK_AUDITOR_ID"); } set { SetField("CHECK_AUDITOR_ID", value); } }
        public string CheckAuditor { get { return GetFieldString("CHECK_AUDITOR"); } set { SetField("CHECK_AUDITOR", value); } }
        public string FinalAuditorId { get { return GetFieldString("FINAL_AUDITOR_ID"); } set { SetField("FINAL_AUDITOR_ID", value); } }
        public string FinalAuditor { get { return GetFieldString("FINAL_AUDITOR"); } set { SetField("FINAL_AUDITOR", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
        public bool Insured { get { return GetFieldBool("ISINSURED"); } set { SetField("ISINSURED", value); } }
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        [ExtendedTypeAttribute(RMExtType.Entity, "ADJUSTERS")]
        public int AdjusterEId { get { return GetFieldInt("ASSIGNADJ_EID"); } set { SetField("ASSIGNADJ_EID", value); } }
        //Ankit End
        #endregion

        #region Child Implementation
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        private string[,] sChildren = { { "SiteUnit", "SiteUnit" } };

        public SiteUnit SiteUnit
        {
            get
            {
                SiteUnit objItem = base.m_Children["SiteUnit"] as SiteUnit;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("SiteUnit", (objItem as DataObject), this.SiteId) as SiteUnit;
                return objItem;
            }
        }
        //Ankit End
        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
         
            Entity objEnt = null;
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {           
            base.OnChildItemAdded(childName, itemValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            // If Entity is involved we save this first (in case we need the EntityId.)
            ;
        }       

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }
      
        #endregion

        internal ClaimXSiteLoss(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        public override IDataModel Parent
        {
            get
            {                
                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

        new private void Initialize()
        {

            this.m_sTableName = "CLAIM_X_SITELOSS";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            base.InitChildren(sChildren);
            //Ankit End
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }

        public override void Save()
        {
            base.Save();

            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            if (!this.IsMigrationImport)
            {
                int iAssignmentLvlCodeID = Context.LocalCache.GetCodeId("UN", "ASSIGNMENT_LEVEL");
                string sAttachTable = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.SITELOSS.ToString();
                int iAttachRecordID = this.RowId;

                CommonFunctions.UpdateAdjAssignment(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.ClaimId, iAssignmentLvlCodeID, sAttachTable, iAttachRecordID, this.Context.ClientId);

                if (this.Context.InternalSettings.SysSettings.AdjAssignmentAutoDiary.Equals(-1))
                {
                    string sEntryName = "Diary for Claim ";
                    string sClaimNumber = string.Empty;
                    int iAdjUserID = CommonFunctions.GetAdjusterUserID(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.Context.ClientId);
                    string sAssignedUser = string.Empty;

                if (!int.Equals(iAdjUserID, 0))
                {
                    //Getting Login Name for Adjuster******************************************
                    sAssignedUser = Context.LocalCache.GetSystemLoginName(iAdjUserID, Context.RMUser.DatabaseId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId),Context.ClientId);
                    //End for Login Name*******************************************************

                        if (!string.IsNullOrEmpty(sAssignedUser))
                        {
                            Claim objClaim = this.Parent as Claim;
                            if (objClaim == null && this.ClaimId != 0)
                            {
                                objClaim = (Claim)Context.Factory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(this.ClaimId);
                            }
                            if ((objClaim != null) && (this.ClaimId != 0))
                            {
                                sClaimNumber = objClaim.ClaimNumber;
                            }

                            sEntryName = string.Concat(sEntryName, sClaimNumber, ", New Site Assignment, ", SiteUnit.SiteNumber);

                            //Creation of Diary for Assigned Adjuster**********************************
                            CommonFunctions.CreateWPADiary(Context.LocalCache.DbConnectionString, sEntryName, null, 2, -1, sAssignedUser, "SYSTEM", string.Empty, -1, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.CLAIM.ToString(), this.ClaimId, null, Conversion.GetTime(DateTime.Now.ToString()), Conversion.ToDbDate(System.DateTime.Today), "0|0|0|0|0|0|0|0|0", this.Context.ClientId);
                            //End Diary Creation*******************************************************
                        }
                    }
                }
                //Ankit End
            }
        }

        public override void Delete()
        {
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            string sSql = string.Empty;
            sSql = String.Format("DELETE FROM CLAIM_ADJ_ASSIGNMENT WHERE ATTACH_RECORDID={0} AND ATTACH_TABLE='{1}'", this.RowId, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.SITELOSS.ToString());
            Context.DbConnLookup.ExecuteNonQuery(sSql);
            //Ankit End

            base.Delete();
        }
    }
}
