﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    	[Riskmaster.DataModel.Summary("BANK_STATUS_HIST","BANK_HIST_ROW_ID", "StatusCode")]
   public  class BankingStatusHist  : DataObject	
        {
#region Database Field List
		private string[,] sFields = {
														{"BankStatusRowId", "BANK_HIST_ROW_ID"},
														{"BankEId", "BANKING_ROW_ID"},
														{"DateStatusChgd", "DATE_STATUS_CHGD"},
														{"StatusCode", "BANK_STATUS_CODE"},
														{"StatusChgdBy", "STATUS_CHGD_BY"},
													{"Reason", "REASON_CODE"},
														{"ApprovedBy", "APPROVED_BY"},
		};

		public int BankHistRowId{get{return GetFieldInt("BANK_HIST_ROW_ID");}set{SetField("BANK_HIST_ROW_ID",value);}}
		public int BankingRowId{get{return GetFieldInt("BANKING_ROW_ID");}set{SetField("BANKING_ROW_ID",value);}}
		public string DateStatusChgd{get{return GetFieldString("DATE_STATUS_CHGD");}set{SetField("DATE_STATUS_CHGD",Riskmaster.Common.Conversion.GetDate(value));}}
        [ExtendedTypeAttribute(RMExtType.Code,"BANKING_STATUS")]
        public int StatusCode{get{return GetFieldInt("BANK_STATUS_CODE");}set{SetField("BANK_STATUS_CODE",value);}}
		public string StatusChgdBy{get{return GetFieldString("STATUS_CHGD_BY");}set{SetField("STATUS_CHGD_BY",value);}}
		public string Reason{get{return GetFieldString("REASON_CODE");}set{SetField("REASON_CODE",value);}}
		public string ApprovedBy{get{return GetFieldString("APPROVED_BY");}set{SetField("APPROVED_BY",value);}}
		#endregion


        internal BankingStatusHist(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "BANK_STATUS_HIST";
            this.m_sKeyField = "BANK_HIST_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Bankinginfo";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
	}
}