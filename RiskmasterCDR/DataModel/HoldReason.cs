﻿/**********************************************************************************************
 *   Date     |  JIRA   | Programmer | Description                                            *
 **********************************************************************************************
 * 04/27/2013 | 7810  | svinchurkar   |Created (Entity Approval and Payment Approval )
 **********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("HOLD_REASON", "ROW_ID")]
    public  class HoldReason : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
									                    {"RowId", "ROW_ID"},					
                                                        {"TableID", "ON_HOLD_TABLE_ID"},
														{"HoldRowID", "ON_HOLD_ROW_ID"},                                                      
														{"HoldReasonCode", "HOLD_REASON_CODE"},														
                                                        {"HoldStatusCode", "HOLD_STATUS_CODE"},                                                     
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"AddedByUser", "ADDED_BY_USER"},
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"}													
                                    };

        [ExtendedTypeAttribute(RMExtType.Code, "ANY")]
        public int TableID { get { return GetFieldInt("ON_HOLD_TABLE_ID"); } set { SetField("ON_HOLD_TABLE_ID", value); } }
        public int HoldRowID { get { return GetFieldInt("ON_HOLD_ROW_ID"); } set { SetField("ON_HOLD_ROW_ID", value); } }
        public int HoldReasonCode { get { return GetFieldInt("HOLD_REASON_CODE"); } set { SetField("HOLD_REASON_CODE", value); } }      
        public int HoldStatusCode { get { return GetFieldInt("HOLD_STATUS_CODE"); } set { SetField("HOLD_STATUS_CODE", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
       
        #endregion


        internal HoldReason(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            this.m_sTableName = "HOLD_REASON";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";        
            base.InitFields(sFields);         
            this.m_sParentClassName = "";
            base.Initialize(); 
        }
        //public void MoveToRcRowId(int iRcRowId)
        //{
        //    this.MoveTo(Context.DbConnLookup.ExecuteInt("SELECT ROW_ID FROM HOLD_REASON WHERE ON_HOLD_ROW_ID =  " + iRcRowId));
        //}
        public void MoveToRcRowId(int iRcRowId, int iHoldReasonCode, int iTableId)
        {
            try
            { this.MoveTo(Context.DbConnLookup.ExecuteInt(string.Format("SELECT ROW_ID FROM HOLD_REASON WHERE ON_HOLD_ROW_ID = {0} AND HOLD_REASON_CODE={1} AND ON_HOLD_TABLE_ID={2}", iRcRowId, iHoldReasonCode, iTableId))); }
            catch
            { } //Who cares - it's not in the DB anyway.
        }

        protected override bool OnForceParentSave()
        {
            return false;
        }

    }
}
