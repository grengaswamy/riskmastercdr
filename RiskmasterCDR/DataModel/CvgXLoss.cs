﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("COVERAGE_X_LOSS","CVG_LOSS_ROW_ID", "LossCode")]
   public class CvgXLoss:DataObject
    {

        #region Database Field List
		private string[,] sFields = {
														
														{"CvgLossRowId", "CVG_LOSS_ROW_ID"},
														{"PolCvgRowId", "POLCVG_ROW_ID"},
														{"LossCode", "LOSS_CODE"},
														{"DisablityCategory", "DISABILITY_CAT"}
														
		};

		
		public int CvgLossRowId{get{return GetFieldInt("CVG_LOSS_ROW_ID");}set{SetField("CVG_LOSS_ROW_ID",value);}}
      
		public int PolCvgRowId{get{return GetFieldInt("POLCVG_ROW_ID");}set{SetField("POLCVG_ROW_ID",value);}}
     
        [ExtendedTypeAttribute(RMExtType.Code,"LOSS_CODES")]
        public int LossCode{get{return GetFieldInt("LOSS_CODE");}set{SetField("LOSS_CODE",value);}}

         [ExtendedTypeAttribute(RMExtType.Code,"DISABILITY_CATEGORY")]
        public int DisablityCategory{get{return GetFieldInt("DISABILITY_CAT");}set{SetField("DISABILITY_CAT",value);}}
		
		
		#endregion
		

		internal CvgXLoss(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "COVERAGE_X_LOSS";
			this.m_sKeyField = "CVG_LOSS_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			

			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		
	}
    
}
