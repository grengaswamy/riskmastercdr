﻿using System;

namespace Riskmaster.DataModel
{
     [Riskmaster.DataModel.Summary("POLICY_X_UNIT", "POLICY_UNIT_ROW_ID")]
    public class PolicyXUnit : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"PolicyUnitRowId", "POLICY_UNIT_ROW_ID"},
														{"UnitId", "UNIT_ID"},
														{"PolicyId", "POLICY_ID"},
                                                        {"UnitType", "UNIT_TYPE"},
														// psharma Jira 12759 start
                                                        {"UnitNumber", "UNIT_NUMBER"},
                                                        {"UnitRiskLoc", "UNIT_RISK_LOC"},
                                                        {"UnitRiskSubLoc", "UNIT_RISK_SUB_LOC"},
														{"SiteSeqNumber", "SITE_SEQ_NUMBER"},
                                                        {"Product", "PRODUCT"},
                                                        {"INSLine", "INS_LINE"},
                                                        {"StatUnitNumber", "STAT_UNIT_NUMBER"},
                                                        {"UnitPremium", "UNIT_PREM"},
                                                        {"EffectiveDate", "EFFECTIVE_DATE"},
                                                        {"ExpirationDate", "EXPIRATION_DATE"}
		}  ;                                             // psharma Jira 12759 end

        public int PolicyUnitRowId { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }

        public int UnitId { get { return GetFieldInt("UNIT_ID"); } set { SetField("UNIT_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
		// psharma Jira 12759 start
        public string UnitNumber { get { return GetFieldString("UNIT_NUMBER"); } set { SetField("UNIT_NUMBER", value); } }
        public string UnitRiskLoc { get { return GetFieldString("UNIT_RISK_LOC"); } set { SetField("UNIT_RISK_LOC", value); } }
        public string UnitRiskSubLoc { get { return GetFieldString("UNIT_RISK_SUB_LOC"); } set { SetField("UNIT_RISK_SUB_LOC", value); } }
        public string SiteSeqNumber { get { return GetFieldString("SITE_SEQ_NUMBER"); } set { SetField("SITE_SEQ_NUMBER", value); } }
        public string Product { get { return GetFieldString("PRODUCT"); } set { SetField("PRODUCT", value); } }
        public string INSLine { get { return GetFieldString("INS_LINE"); } set { SetField("INS_LINE", value); } }
        public string StatUnitNumber { get { return GetFieldString("STAT_UNIT_NUMBER"); } set { SetField("STAT_UNIT_NUMBER", value); } }
        public double UnitPremium { get { return GetFieldDouble("UNIT_PREM"); } set { SetField("UNIT_PREM", value); } }
        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        // psharma Jira 12759 end
		#endregion

        internal PolicyXUnit(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}
       
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "POLICY_X_UNIT";
            this.m_sKeyField = "POLICY_UNIT_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //base.InitChildren(sChildren);
            //base.InitChildren(sChildren);
            this.m_sParentClassName = "Policy";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        
        //internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        //{
        //    switch (childName)
        //    {
        //         case "PolicyXCvgTypeList":
        //            (itemValue as PolicyXCvgType).PolicyUnitRowId = this.PolicyUnitRowId;
        //            break;
        //     }
        //    base.OnChildItemAdded(childName, itemValue);
        //}
        //override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        //{
        //    switch (childName)
        //    {
              
        //        case "PolicyXCvgTypeList":
        //            if (isNew)
        //                foreach (PolicyXCvgType item in (childValue as PolicyXCvgTypeList))
        //                    item.PolicyUnitRowId = this.PolicyUnitRowId;
        //            (childValue as IPersistence).Save();
        //            break;             
        //    }
        //    base.OnChildPostSave(childName, childValue, isNew);
        //}	
    }
}
