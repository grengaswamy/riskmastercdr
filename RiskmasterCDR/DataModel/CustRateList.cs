using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCustRateList.
	/// </summary>
	public class CustRateList : DataCollection
	{
		internal CustRateList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"RATE_TABLE_ID";
			this.SQLFromTable =	"CUST_RATE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CustRate";
		}
		public new CustRate this[int keyValue]{get{return base[keyValue] as CustRate;}}
		public new CustRate AddNew(){return base.AddNew() as CustRate;}
		public  CustRate Add(CustRate obj){return base.Add(obj) as CustRate;}
		public new CustRate Add(int keyValue){return base.Add(keyValue) as CustRate;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}