using System;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Scripting;
using System.Xml;
using System.Data;
using System.Text;
using System.Threading;
using System.Collections.Generic;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for Supplementals.IDataModel,
	/// </summary>
	public class SupplementalObject :DataRoot ,  IPersistence, IEnumerable
	{
		private string m_ViewXml="";
		private bool m_isNew = true;
		private string m_TableName = "";
		private bool m_DefinitionLoaded = false;
		private bool m_UsesAssociation=false;
		private DataChildList  m_SuppFields = new DataChildList();
		private SupplementalField m_KeyField = null;
        //Start by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
        private bool m_PowerViewUpgrade = false;

        //Added by Nitin for Supplemental performance issue.
        private int m_CurrentLOB = 0;
        //R7 Perf Imp
        private bool bScriptEnabled = false;
        /// <summary>
        /// Raise an event to call custom script Init function
        /// MITS 12028
        /// </summary>
        override public void InitializeScriptData()
        {
            this.InitData(this, new InitDataEventArgs());
        }

		//DEFAULT EVENT HANDLERS  (customize above to spawn scripts if desired....)
		private void InitObjDefaultHandler(object sender, InitObjEventArgs e)
		{
			Context.RunDataModelScript(ScriptTypes.SCRIPTID_INITIALIZE,sender);
		}
        private void InitDataDefaultHandler(object sender, InitDataEventArgs e)
        {
            //MITS 12028 Only call script when the action is originated from the specified screen
            if (this.FiringScriptFlag != 0)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_INITIALIZE, sender);
            }
        }
		private bool CalculateDataDefaultHandler(object sender, CalculateDataEventArgs e)
		{
            //MITS 12028 Only call script when the action is originated from the specified screen
            if (this.FiringScriptFlag == 2)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_CALCULATE, sender);
                this.FiringScriptFlag++;
            }
            return (Context.ScriptValidationErrors.Count == 0);
		}
		private bool ValidateDefaultHandler(object sender, ValidateEventArgs e)
		{
            //MITS 12028 Only call script when the action is originated from the specified screen
            if (this.FiringScriptFlag == 3)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_VALIDATE, sender);
                this.FiringScriptFlag++;
            }
			return (Context.ScriptValidationErrors.Count == 0);
		}
		private bool BeforeSaveDefaultHandler(object sender, BeforeSaveEventArgs e)
		{
            //MITS 12028 Only call script when the action is originated from the specified screen
            if (this.FiringScriptFlag == 4)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_BEFORESAVE, sender);
                this.FiringScriptFlag++;
            }
            return (Context.ScriptValidationErrors.Count == 0);
		}
		private void AfterSaveDefaultHandler(object sender, AfterSaveEventArgs e)
		{
            //MITS 12028 Only call script when the action is originated from the specified screen
            if (this.FiringScriptFlag == 5)
            {
                Context.RunDataModelScript(ScriptTypes.SCRIPTID_AFTERSAVE, sender);
            }
		}

		internal  SupplementalObject(bool isLocked, Context context) : base(isLocked, context){Initialize();}
		private void Initialize()
		{
            bScriptEnabled = Context.LocalCache.IsScriptEditorEnabled();
            if (bScriptEnabled)
            {
                this.InitObj += new InitObjEventHandler(InitObjDefaultHandler);
                this.InitData += new InitDataEventHandler(InitDataDefaultHandler);
                this.CalculateData += new CalculateDataEventHandler(CalculateDataDefaultHandler);
                this.ValidateData += new ValidateEventHandler(ValidateDefaultHandler);
                this.BeforeSave += new BeforeSaveEventHandler(BeforeSaveDefaultHandler);
                this.AfterSave += new AfterSaveEventHandler(AfterSaveDefaultHandler);
            }
		}

		internal SupplementalField KeyField
		{
			get
			{
				try
				{
					if(m_KeyField == null)
						foreach(string key in m_SuppFields)
                            if ((m_SuppFields[key] as SupplementalField).FieldType == SupplementalFieldTypes.SuppTypePrimaryKey)
                            {
                                m_KeyField = (m_SuppFields[key] as SupplementalField);
                                //the user interface allows for multiple primary keys, mits 10085 filed.
                                //for now the possibility is ignored and the 1st primary key is returned
                                break;
                            }
					
					if(m_KeyField!=null)
						return m_KeyField;
				}
                catch (Exception e) { throw new DataModelException(Globalization.GetString("Supplementals.KeyField.Get.Exception", this.Context.ClientId), e); }
                throw new DataModelException(Globalization.GetString("Supplementals.KeyField.Get.Exception.NoPrimaryKeyFieldFound", this.Context.ClientId));
			}
		}
		internal int KeyValue
		{
			get
			{
				try
				{
					return (int) KeyField.Value;
//					foreach(string key in m_SuppFields)
//						if((m_SuppFields[key] as SupplementalField).FieldType ==SupplementalFieldTypes.SuppTypePrimaryKey)
//							return (int)(m_SuppFields[key] as SupplementalField).Value;
				}
                catch (Exception e) { throw new DataModelException(Globalization.GetString("Supplementals.KeyValue.Get.Exception", this.Context.ClientId), e); }
                throw new DataModelException(Globalization.GetString("Supplementals.KeyValue.Get.Exception.NoPrimaryKeyFieldFound", this.Context.ClientId));
			}
			set
			{
				try
				{
					foreach(string key in m_SuppFields)
						if((m_SuppFields[key] as SupplementalField).FieldType ==SupplementalFieldTypes.SuppTypePrimaryKey)
							(m_SuppFields[key] as SupplementalField).Value = value;
				}
                catch (Exception e) { throw new DataModelException(Globalization.GetString("Supplementals.KeyValue.Set.Exception", this.Context.ClientId), e); }
                
			}

		}
		
		public string TableName
		{
			get{return m_TableName;}
			set
			{
				if (value.ToUpper() != m_TableName)
				{
					m_TableName = value.ToUpper();
					LoadTableDefinition();
                    //nadim MITS 11453
                    //m_TableDefinitions.Clear();

				}
				if(!DefinitionLoaded)
					LoadTableDefinition();
                //nadim MITS 11453
                //m_TableDefinitions.Clear();
			}
		}
        //Start by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool

        public bool PowerViewUpgrade
        {
            get { return m_PowerViewUpgrade; }

            set
              {
               m_PowerViewUpgrade = value;
              }
        }
        //End by Shivendu

        //Added by Nitin for Safeway performance issue ,incase of powerview creation
        public string LOBForPowerView
        {
            get
            {
                return m_CurrentLOB.ToString();
            }
            set
            {
                if (value.ToLower() == "claimgc")
                {
                    m_CurrentLOB = 241;
                }
                else if (value.ToLower() == "claimwc")
                {
                    m_CurrentLOB = 243;
                }
                else if (value.ToLower() == "claimva")
                {
                    m_CurrentLOB = 242;
                }
                else if (value.ToLower() == "claimdi")
                {
                    m_CurrentLOB = 844;
                }
                //Sumit - MITS#18230- 10/28/2009 :Start
                //Added for Property Claim (PC)
                else if (value.ToLower() == "claimpc")
                {
                    m_CurrentLOB = 845;
                }
                //Sumit - MITS#18230- 10/28/2009 :End
                else
                {
                    m_CurrentLOB = 0;
                }
            }
        }

        //BSB Performance Hack - Fix the horrible repeating queries for table\field\association info.
		//Note: The context block is unloaded between Page Requests leaving the static collection's
		// suppfield objects "impotent" w/o access to a valid Context.  Must duplicate the object's 
		// values within a new instance with the current Context...(See LoadTableDefinition for details.)
		static private Hashtable m_TableDefinitions = new Hashtable(10);
		static private ReaderWriterLock m_TableDefinitionsLock = new ReaderWriterLock();
		
		//BSB 11.28.2006 Address customer complaint that new supp fields are not picked up until
		// system is reset.  Add this method here and allow callers to clear out the 
		// static cache - this will cause new supp fields to be added\removed from the table 
		// definition w/o process reset.
		static public void ClearTableDefinitionCache()
		{
			m_TableDefinitionsLock.AcquireWriterLock(-1);
			try{m_TableDefinitions = new Hashtable(10);}
			finally{m_TableDefinitionsLock.ReleaseWriterLock();}
		}
		private class TableDefinition
		{
			private DataChildList m_SuppFields = new DataChildList();
			public DataChildList SuppFields
			{
				get
				{
					return m_SuppFields;
				}
				set{m_SuppFields = value;}
			} 
			public bool UsesAssociation = false;
		}

		public bool DefinitionLoaded{get{return m_DefinitionLoaded;}}
		private void LoadTableDefinition()
		{
			m_isNew = true;
			string SQL = "";
            string groupAssocQuery = string.Empty;
            string userLookupQuery = string.Empty;
			SupplementalField objSupp = null;
			TableDefinition objTable =null;
			DbReader objReader =null;
            DataSet dSet = null;
            DataSet dSetUserLookup = null;
            DataRow[] dRowCollection = null;
            Claim objClaim = null;
			m_KeyField = null;
            
			try
			{
				string sKey = string.Concat(this.Context.DbConnLookup.ConnectionString 
                    , this.TableName);
                //smishra25:perf Imp,caching table definitions of claim supp
                if (string.Compare(this.TableName,"CLAIM_SUPP") == 0)
                {
                    if (this.m_Parent != null)
                    {
                        objClaim = this.m_Parent as Claim;
                        if (objClaim != null)
                        {
                            this.m_CurrentLOB = objClaim.LineOfBusCode;
                        }
                    }
                    sKey = string.Concat(sKey, this.m_CurrentLOB.ToString());
                }
				
				m_TableDefinitionsLock.AcquireReaderLock(-1);
				try
				{
					if(m_TableDefinitions.Contains(sKey))
						objTable = m_TableDefinitions[sKey] as TableDefinition;
				}
				finally{m_TableDefinitionsLock.ReleaseReaderLock();}

				if(objTable!=null)
				{
					m_SuppFields = new DataChildList();
                    foreach(string sSuppFieldName in objTable.SuppFields)
					{
						objSupp = Context.Factory.GetDataModelObject("SupplementalField",true) as SupplementalField;
						m_SuppFields[sSuppFieldName] = (objTable.SuppFields[sSuppFieldName] as SupplementalField).CopyTo(objSupp);
					}
					m_UsesAssociation = objTable.UsesAssociation;
					m_DefinitionLoaded = true;
					m_ViewXml = "";
                    //rsolanki2: avoid script procesiing when LoadTableDefinition is called from PowerviewUpgrade tool
                    //R7 Perf Imp
                    //if (!this.m_PowerViewUpgrade)
                    if (!this.m_PowerViewUpgrade && bScriptEnabled)
                    {
                        InitObj(this, new InitObjEventArgs());
                    }
                    //if(this.TableName != "CLAIM_SUPP")
					    return;
				}

				m_SuppFields = new DataChildList();
				m_UsesAssociation = false;

                //rsolanki2 :
                //objTable = new TableDefinition();
                //DataChildList objSuppFields = new DataChildList();

                //Added by Nitin for supplemental performance issue

                if (this.TableName == "CLAIM_SUPP")
                {
                    //smishra25:Moving this code up
                    //if (this.m_Parent != null)
                    //{
                    //    objClaim = this.m_Parent as Claim;
                    //    if (objClaim != null)
                    //    {
                    //        this.m_CurrentLOB = objClaim.LineOfBusCode;
                    //    }
                    //}

                    switch (this.m_CurrentLOB)
                    {
                        case 241:  //for GC
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 AND HIDE_LOB_GC_FLAG=0  ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_GC_FLAG=0  ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_GC_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                        case 243:  //for WC
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 AND HIDE_LOB_WC_FLAG=0 ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_WC_FLAG=0 ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_WC_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                        case 242:  //for VA
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 AND HIDE_LOB_VA_FLAG=0 ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_VA_FLAG=0 ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_VA_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                        case 844:  //for NOC
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 AND HIDE_LOB_NO_FLAG=0 ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_NO_FLAG=0 ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_NO_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                        //Start:Neha Suresh Jain, 04/21/2010, for PC Claims
                        //for PC
                        case 845:  
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 AND HIDE_LOB_PC_FLAG=0 ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_PC_FLAG=0 ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND HIDE_LOB_PC_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                        //End:Neha Suresh Jain
                        default:
                            //SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 ORDER BY SEQ_NUM", this.TableName);
                            SQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 ORDER BY SEQ_NUM";
                            groupAssocQuery = "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='CLAIM_SUPP' AND DELETE_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))";
                            break;
                    }
                }
                else
                {
                    SQL = String.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='{0}' AND DELETE_FLAG=0 ORDER BY SEQ_NUM"
                        , this.TableName);
                    groupAssocQuery = string.Concat( "SELECT ASSOC_CODE_ID,FIELD_ID FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='" 
                        , this.TableName 
                        , "' AND DELETE_FLAG=0 AND (GRP_ASSOC_FLAG=1 OR GRP_ASSOC_FLAG=-1))");
                }
                userLookupQuery = String.Concat("SELECT USER_GROUP_IND,FIELD_ID FROM SUPP_USERGROUP_DICT WHERE FIELD_ID IN (SELECT FIELD_ID FROM  SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='"
                        , this.TableName
                        , "' AND DELETE_FLAG=0)");

                dSet = DbFactory.GetDataSet(this.Context.DbConn.ConnectionString, groupAssocQuery,Context.ClientId);
                
                // npadhy - Creating a Query from within an open connection (inside data reader) causes transaction to hang. So changing the implementation same as group association
                dSetUserLookup = DbFactory.GetDataSet(this.Context.DbConn.ConnectionString, userLookupQuery, Context.ClientId);
                
                objReader = Context.DbConnLookup.ExecuteReader(SQL);
				while(objReader.Read())
				{
					objSupp = Context.Factory.GetDataModelObject("SupplementalField",true) as SupplementalField;
					
                    //Load Field Description
					objSupp.DesignMode = true;
					objSupp.AssocField = objReader.GetString("ASSOC_FIELD").ToUpper();
					objSupp.Caption    = objReader.GetString("USER_PROMPT");
					objSupp.CodeFileId = objReader.GetInt("CODE_FILE_ID");
					objSupp.SetFieldId(objReader.GetInt("FIELD_ID"));
					objSupp.FieldName = objReader.GetString("SYS_FIELD_NAME").ToUpper();
					objSupp.FieldSize = objReader.GetInt("FIELD_SIZE");
					objSupp.FieldType = (SupplementalFieldTypes)objReader.GetInt("FIELD_TYPE");
					objSupp.Format    = objReader.GetString("PATTERN");
					objSupp.Lookup    = ((int)objReader.GetInt16("LOOKUP_FLAG") !=0);
					objSupp.Required  = ((int)objReader.GetInt16("REQUIRED_FLAG") !=0);
                    objSupp.IsHtmlConfigurable = ((int)objReader.GetInt16("HTMLTXTCNFG") != 0); //prop fill for HTML flag asharma326 JIRA 6422
                    objSupp.WebLinkId = objReader.GetInt("WEB_LINK_ID");//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                    //sharishkumar Jira 6415
                    objSupp.Multiselect = objReader.GetInt16("MULTISELECT");

                    if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeUserLookup)
                    {
                        // npadhy Jira 6415 Retrieve the UserGroup for the selected Field id
                        //SQL = String.Format("SELECT USER_GROUP_IND FROM SUPP_USERGROUP_DICT WHERE FIELD_ID = {0}", objSupp.FieldId);
                        if(dSetUserLookup.Tables[0].Select("FIELD_ID = '" + objSupp.FieldId + "'") != null && dSetUserLookup.Tables[0].Select("FIELD_ID = '" + objSupp.FieldId + "'").Length > 0)
                        objSupp.UserGroups = Conversion.ConvertObjToInt(dSetUserLookup.Tables[0].Select("FIELD_ID = '" + objSupp.FieldId + "'")[0]["USER_GROUP_IND"], Context.ClientId);
                    }

                    //sharishkumar Jira 6415 ends

                    //by Nitin, checking if GroupAssociation is LOB , then making GrpAssoc flag to false
                    if (objReader.GetValue("ASSOC_FIELD") != null)
                    {
                        if (objReader.GetValue("ASSOC_FIELD").ToString() == "CLAIM.LINE_OF_BUS_CODE" && this.TableName == "CLAIM_SUPP")
                        {
                            objSupp.SetGrpAssocFlag(false);
                            objSupp.AssocField = string.Empty;
                        }
                        else
                        {
                            objSupp.SetGrpAssocFlag(((int)objReader.GetInt16("GRP_ASSOC_FLAG") != 0));
                        }
                    }
                    else
                    {
                        objSupp.SetGrpAssocFlag(((int)objReader.GetInt16("GRP_ASSOC_FLAG") != 0));
                    }
                    //commented by Nitin
                    //objSupp.SetGrpAssocFlag(((int)objReader.GetInt16("GRP_ASSOC_FLAG") != 0));
                    objSupp.NetVisibleFlag = ((int)objReader.GetInt16("NETVISIBLE_FLAG") != 0);
                    objSupp.SeqNum = ((int)objReader.GetInt("SEQ_NUM"));
                    objSupp.DesignMode = false;
                    objSupp.DataChanged = false;
                    if (!objSupp.IsKeyValue)
                        objSupp.Visible = true;
                    if (objSupp.GrpAssocFlag)
                    {
                        this.m_UsesAssociation = true;
                        //Added by Nitin for Supplemental performance issue
                        if (objSupp.AssocField != "" && objSupp.GrpAssocFlag)
                        {
                            dRowCollection = dSet.Tables[0].Select("FIELD_ID = '" + objSupp.FieldId + "'");
                            foreach (DataRow dRow in dRowCollection)
                            {
                                objSupp.AddAssocCode(Convert.ToInt32(dRow["ASSOC_CODE_ID"]));
                            }
                        }

                    }
                    //Toss into the Collection
                    this.Add(objSupp);
                }
				objReader.Close();

                //Commneted by Nitin for Safeway performance issue

				//Handle loading any associated codes.
                //if(this.m_UsesAssociation)
                //    foreach(string key in m_SuppFields)
                //    {
                //        objSupp = (m_SuppFields[key] as SupplementalField);
                //        if(objSupp.AssocField != "" && objSupp.GrpAssocFlag)
                //        {
                //            SQL = "SELECT ASSOC_CODE_ID FROM SUPP_ASSOC WHERE FIELD_ID=" + objSupp.FieldId;
                //            objReader = Context.DbConnLookup.ExecuteReader(SQL);
                //            while(objReader.Read())
                //                objSupp.AddAssocCode(objReader.GetInt("ASSOC_CODE_ID"));
                //            objReader.Close();
                //        }
                //    }

				m_DefinitionLoaded = true;
				m_ViewXml = "";
                //If added by Shivendu to avoid scripting processing 
                //when LoadTableDefinition is called from PowerviewUpgrade tool
                //R7 Perf Imp
                //if (!this.m_PowerViewUpgrade)
                if (!this.m_PowerViewUpgrade && bScriptEnabled)
                {
                    InitObj(this, new InitObjEventArgs());
                }

                m_TableDefinitionsLock.AcquireWriterLock(-1);
                try
                {
                    if (!m_TableDefinitions.Contains(sKey))
                    {
                        objTable = new TableDefinition();
                        DataChildList objSuppFields = new DataChildList();
                        foreach (string sSuppFieldName in this.m_SuppFields)
                        {
                            objSupp = Context.Factory.GetDataModelObject("SupplementalField", true) as SupplementalField;
                            objSuppFields[sSuppFieldName] = (this.m_SuppFields[sSuppFieldName] as SupplementalField).CopyTo(objSupp);
                            (objSuppFields[sSuppFieldName] as SupplementalField).DeActivateForCache();
                        }

                        objTable.SuppFields = objSuppFields;
                        objTable.UsesAssociation = m_UsesAssociation;
                        m_TableDefinitions.Add(sKey, objTable);
                    }
                }
                finally { m_TableDefinitionsLock.ReleaseWriterLock(); }
			}
            catch (Exception e) { throw new DataModelException(Globalization.GetString("Supplementals.LoadTableDefinition.Exception", this.Context.ClientId), e); }
			finally
			{
				if (objReader!=null)
					objReader.Close();
				objReader = null;
			}

		}
		protected virtual string OnBuildSuppSectionName(){return "suppgroup";}
		protected virtual string OnBuildSuppSectionTitle(){return "Supplementals";}
		public XmlDocument ViewXml
		{
			get
			{
				try
				{

					XmlDocument objXML = new XmlDocument();
					XmlElement objXMLRoot;
					XmlElement objXMLElem;
                    StringBuilder suppFildString = null;
                    string sType = this.GetType().Name;

					//We can cache this information...
					if(m_ViewXml!="" && !this.UsesAssociation)
					{
						objXML.LoadXml(m_ViewXml);
						return objXML;
					}

					// Start by creating XML definition
					//	<group name="generalinfo" title="General Information">

					objXMLRoot = objXML.CreateElement("group");
					objXMLElem = objXMLRoot;
					objXMLElem.SetAttribute("name", OnBuildSuppSectionName());
					objXMLElem.SetAttribute("title", OnBuildSuppSectionTitle());
					objXML.AppendChild(objXMLRoot);

					// Loop through all fields and create XML definition
					int i=0;
					int max = 1000;
                    //Added by Nitin to replace string with stringBuilder for Safeway performance issue
                    suppFildString = new StringBuilder();
                    if (sType.ToLower() =="supplementals")
                    {
                        //Mona:MITS 22993: Supp fields do not line-up correctly in PowerView for Fund_trans_split 
                        SupplementalField.SuppTableName = TableName;
                        foreach (SupplementalField objField in this)
                        {
                            objField.SuppType = OnBuildSuppSectionName();

                            //-- ABhateja, 08.08.2006, MITS 7627
                            //-- We need group assoc. fields to go to UI even if they are not
                            //-- visible.UI will in turn handle these fields and make them
                            //-- visible/invisible based on group association.
                            //if(OnBuildSuppSectionName() != "suppgroup")
                            //    if(!objField.Visible)
                            //        continue;

                            if (i == max)
                                break;
                            i++;
                            suppFildString.Append("<displaycolumn>");
                            suppFildString.Append(objField.ViewXML.OuterXml);
                            suppFildString.Append("</displaycolumn>");
                        }
                    }
                    else
                    {
                        foreach (SupplementalField objField in this)
                        {
                            objField.SuppType = OnBuildSuppSectionName();

                            //-- ABhateja, 08.08.2006, MITS 7627
                            //-- We need group assoc. fields to go to UI even if they are not
                            //-- visible.UI will in turn handle these fields and make them
                            //-- visible/invisible based on group association.
                            //if(OnBuildSuppSectionName() != "suppgroup")
                            //    if(!objField.Visible)
                            //        continue;

                            if (i == max)
                                break;
                            i++;
                            suppFildString.Append("<displaycolumn>");
                            suppFildString.Append(objField.ViewXML.OuterXml.Replace("Supplementals", sType));
                            suppFildString.Append("</displaycolumn>");
                        }
                    }
					
                    objXMLRoot.InnerXml = suppFildString.ToString();
                    //Ended by Nitin to replace string with stringBuilder for Safeway performance issue
					m_ViewXml = objXML.OuterXml;
					return objXML;
				}
				catch{}
				return new XmlDocument();
			}
		}

        /// <summary>
        ///Clear all supp field values that are not the primary key.
        ///Add supp field values that are primary key to SQL statement that loads
        ///the supp field values from the database.
        ///If primary key value == 0, do not run the query that loads the supp object from
        ///the database (thus leave all values null except primary key).
        /// </summary>
		internal void Load()
		{
			string SQL = "";
			string sWhere = "";
			this.m_isNew =true;
            bool isPrimaryKeyValueZero = false;
            //Start by Shivendu for Supplemental grid
            int recordId = 0;

            //End by Shivendu for Supplemental grid
			try
			{
                if (m_SuppFields.Count() == 0)
                {
                    this.DataChanged = false;
                    return;
                }

                foreach (string key in this.m_SuppFields.Keys)
                {
                    using (SupplementalField supp = m_SuppFields[key] as SupplementalField)
                    {
                        if (! (supp.FieldType == SupplementalFieldTypes.SuppTypePrimaryKey))
                        {
                            supp.Value = null;
                            if (supp.IsMultiValue || supp.IsUserLookup)
                            {
                                supp.Values.ClearAll();
                                if (this.KeyValue != 0)
                                {
                                    supp.Values.LoadSimpleList(String.Format("SELECT CODE_ID FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}"
                                        , supp.FieldId
                                        , this.KeyValue));
                                }
                            }
                        }
                        else if (supp.FieldType == SupplementalFieldTypes.SuppTypePrimaryKey)
                        {
                            //the utilities screen will allow users to add multiple primary keys to a supp table (mits 10085 filed).
                            //this logic takes care of the rare possibility, but it should be removed when mits 10085 removes the need for it
                            //specifically, this entire "else if" block should be removed, and the SQL statement below should
                            //be created using the KeyField property.
                            if (Conversion.ConvertObjToInt(supp.Value, this.Context.ClientId) == 0)
                            {
                                isPrimaryKeyValueZero = true;
                            }
                            else
                            {
                                if (sWhere != "")
                                    sWhere += " AND ";
                                sWhere = String.Format("{0} {1} = {2}", sWhere, supp.FieldName, supp.Value);
                            }
                        }
                    }
                }

                //mits 10073  do not run this query if it looks for [Primary Key Field] = 0
                //when mits 10085 is fixed do the SQL statement using the .KeyField property,
                //and don't run it if .KeyValue == 0.
                if (!isPrimaryKeyValueZero)
                {
                    SQL = String.Format("SELECT * FROM {0} WHERE {1}"
                        , this.TableName
                        , sWhere);
                    using (DbReader objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                    {
                        while (objReader.Read())
                        {
                            this.m_isNew = false;
                            foreach (string key in this.m_SuppFields)
                            {
                                using (SupplementalField objSupp = m_SuppFields[key] as SupplementalField)
                                {
                                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                                    // Need to handle User Lookup as well.
                                    //Modified by Shivendu for Supplemental grid
                                    if (!(objSupp.FieldType == SupplementalFieldTypes.SuppTypePrimaryKey 
                                        || objSupp.IsMultiValue 
                                        || objSupp.IsUserLookup
                                        || objSupp.FieldType == (SupplementalFieldTypes)17))
                                    {
                                        objSupp.Value = objReader.GetValue(objSupp.FieldName);
                                        objSupp.Visible = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //Start by Shivendu for supplemental grid
                recordId = this.KeyValue;
                foreach (string key in this.m_SuppFields)
                {
                    using (SupplementalField objSupp = m_SuppFields[key] as SupplementalField)
                    {
                        if (objSupp.FieldType == (SupplementalFieldTypes)17)
                        {

                            SQL = "SELECT * FROM GRID_SYS_PARMS";
                            using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                            {
                                while (objRdr.Read())
                                {
                                    objSupp.minRows = objRdr["MIN_ROWS"].ToString();
                                    objSupp.maxRows = objRdr["MAX_ROWS"].ToString();

                                }
                            }
                            if (recordId != 0)
                            {

                                int colCount = 0;
                                //Raman: Changing Location of columns.xml from bin to Appfiles/Supp - 02/04/2009
                                string path = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                                //string path = AppDomain.CurrentDomain.BaseDirectory + "bin/columns.xml";

                                XmlElement objXMLElem = null;
                                XmlElement objXMLTitleElement = null;
                                XmlElement objXMLCellElement = null;
                                XmlElement objXMLRowElement = null;
                                XmlNode objXMLFieldsNode = null;
                                XmlNode objXMLRowsNode = null;
                                XmlNode objXMLRowNode = null;
                                bool bRow = false;
                                bool flag = true;
                                objSupp.gridXML = new XmlDocument();
                                objSupp.gridXML.Load(path);
                                SQL = string.Concat("SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" 
                                    , objSupp.FieldId 
                                    , "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC");
                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    while (objRdr.Read())
                                    {
                                        colCount++;
                                        objXMLElem = objSupp.gridXML.CreateElement("field");
                                        objXMLTitleElement = objSupp.gridXML.CreateElement("title");
                                        objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();
                                        //Start by Shivendu for MITS 17162
                                        objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");
                                        //End by Shivendu for MITS 17162
                                        objXMLElem.AppendChild(objXMLTitleElement);
                                        objXMLFieldsNode = objSupp.gridXML.SelectSingleNode("/grid/table/fields");
                                        objXMLFieldsNode.AppendChild(objXMLElem);
                                    }
                                }
                                SQL = "SELECT * FROM SUPP_GRID_VALUE  WHERE FIELD_ID = '" 
                                    + objSupp.FieldId 
                                    + "' AND RECORD_ID = '" 
                                    + recordId 
                                    + "' ORDER BY ROW_NUM,COL_NUM"; //Order By added by Tushar on 6/2/2009 for REM
                            
                                //SQL = "SELECT * FROM SUPP_GRID_VALUE  WHERE FIELD_ID = '" + objSupp.FieldId + "' AND RECORD_ID = '" + recordId + "'";
                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    objXMLRowNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows/row");
                                    objSupp.gridXML.SelectSingleNode("/grid/table/rows").RemoveChild(objXMLRowNode);
                                    objSupp.Value = "";
                                    long lRowNoPrev = 0;
                                    long lRow = 0;
                                    long lCol = 0;
                                    long lColNo = 0;
                                    objXMLRowElement = objSupp.gridXML.CreateElement("row");
                                    while (flag)
                                    {
                                        if (objRdr.Read())
                                        {
                                            lCol = Convert.ToInt64(objRdr["COL_NUM"].ToString());
                                            lRow = Convert.ToInt64(objRdr["ROW_NUM"].ToString());
                                            if (lRowNoPrev != lRow)
                                            {
                                                for (int i = Convert.ToInt32(lColNo); i < colCount; i++)
                                                {
                                                    objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                                    objXMLCellElement.InnerText = "";
                                                    objSupp.Value = string.Concat(objSupp.Value , "KNALB" , "~"); //ybhaskar: MITS 23349
                                                    objXMLRowElement.AppendChild(objXMLCellElement);
                                                    objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                                    objXMLRowsNode.AppendChild(objXMLRowElement);
                                                    bRow = true;
                                                }
                                                lColNo = 0;
                                                objXMLRowElement = objSupp.gridXML.CreateElement("row");
                                            }
                                            objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                            objXMLCellElement.InnerText = objRdr["CELL_VALUE"].ToString();
                                            if (objRdr["CELL_VALUE"].ToString() == "")
                                                objSupp.Value = objSupp.Value + "KNALB" + "~"; //ybhaskar: MITS 23349
                                            else
                                                objSupp.Value = objSupp.Value + objRdr["CELL_VALUE"].ToString() + "~"; //ybhaskar: MITS 23349

                                            objXMLRowElement.AppendChild(objXMLCellElement);
                                            objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                            objXMLRowsNode.AppendChild(objXMLRowElement);
                                            bRow = true;
                                        }
                                        else
                                        {
                                            for (int i = Convert.ToInt32(lColNo); i < colCount; i++)
                                            {

                                                objXMLCellElement = objSupp.gridXML.CreateElement("cell");
                                                objXMLCellElement.InnerText = "";
                                                objSupp.Value = string.Concat(objSupp.Value , "KNALB" , "~"); //ybhaskar: MITS 23349
                                                objXMLRowElement.AppendChild(objXMLCellElement);
                                                objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                                objXMLRowsNode.AppendChild(objXMLRowElement);
                                                bRow = true;

                                            }
                                            break;
                                        }
                                        lColNo = lColNo + 1;
                                        lRowNoPrev = lRow;
                                    }
                                }

                                if (!bRow)
                                {
                                    objXMLRowsNode = objSupp.gridXML.SelectSingleNode("/grid/table/rows");
                                    objXMLRowsNode.AppendChild(objXMLRowElement);

                                }
                                if (objSupp.Value != null)
                                    if (objSupp.Value.ToString() != "")
                                        objSupp.Value = objSupp.Value.ToString().Substring(0, objSupp.Value.ToString().Length - 1);

                            }
                            else
                            {
                                //Raman: Changing Location of columns.xml from bin to Appfiles/Supp - 02/04/2009
                                string path = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                                //string path = AppDomain.CurrentDomain.BaseDirectory + "bin/columns.xml";
                                XmlElement objXMLElem = null;
                                XmlNode objXMLFieldsNode = null;
                                XmlElement objXMLTitleElement = null;
                                objSupp.gridXML = new XmlDocument();
                                objSupp.gridXML.Load(path);
                                SQL = string.Concat("SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" 
                                    , objSupp.FieldId 
                                    , "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC");
                                using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                                {
                                    while (objRdr.Read())
                                    {
                                        objXMLElem = objSupp.gridXML.CreateElement("field");
                                        objXMLTitleElement = objSupp.gridXML.CreateElement("title");
                                        objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();
                                        //Start by Shivendu for MITS 17162
                                        objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");
                                        //End by Shivendu for MITS 17162
                                        objXMLElem.AppendChild(objXMLTitleElement);
                                        objXMLFieldsNode = objSupp.gridXML.SelectSingleNode("/grid/table/fields");
                                        objXMLFieldsNode.AppendChild(objXMLElem);
                                    }
                                }
                            }

                        }

                        //asharma326 JIRA# 6422 Starts 
                        else if (objSupp.FieldType == (SupplementalFieldTypes)25)
                        {
                            SQL = string.Concat("SELECT HTML_TEXT FROM SUPP_TEXT_VALUES WHERE FIELD_ID = '"
                                   , objSupp.FieldId
                                   , "' AND RECORD_ID = '" + recordId + "'");
                            using (DbReader objRdr = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL))
                            {
                                if (objRdr.Read())
                                {
                                    // read table column values
                                    //objSupp.Value = objRdr["PLAIN_TEXT"].ToString() + SupplementalField.HTMLDELIMITER + objRdr["HTML_TEXT"].ToString();
                                    //objSupp.HTMLContents = new string[2];
                                    //objSupp.HTMLContents[0] = Convert.ToString(objRdr["PLAIN_TEXT"]);
                                    objSupp.HTMLContents = Convert.ToString(objRdr["HTML_TEXT"]);
                                }
                            }
                        }
                        //asharma326 JIRA# 6422 Ends
                        
        
                    }
                }
                //End by Shivendu for supplemental grid
				//Set Assoc Fields Visible\Non-Visible according to parent codes...
				if(this.m_UsesAssociation)
					CheckAssociations();

				this.DataChanged = false;
				
				(this as IDataModel).IsStale = false;

			}
			catch(Exception e)
            { throw new DataModelException(Globalization.GetString("Supplementals.Load.Exception", this.Context.ClientId), e); }
            //Added by Shivendu for supplemental grid
            finally
            {
            }
		}
		public bool UsesAssociation{get{return m_UsesAssociation;}}

		private void CheckAssociations()
		{
			string s = "";
			string assocTableName = "";
			string assocFieldName = "";
			string sLobFilter = "";
			string tmpOrgShort = "";
			string tmpOrgDesc = "";
			int	orgParentEid =0;
			Entity objOrgEntity = this.Context.Factory.GetDataModelObject("Entity",false) as Entity;
			Claim objClaim = null;
			DataObject objTarget = null;

			//Check uses association
			if (!m_UsesAssociation)
				return;
				
			//Check valid Parent available.
			if (this.Parent ==null)
				return;

			//TODO CheckAssociations Implementation.
			//Default Case
			foreach(SupplementalField objSupp in  this)
			{
				if(!objSupp.GrpAssocFlag)
				{
					objSupp.Visible = true;
					continue;		// Added by RMH on 7/8/04
					//goto hSkip;	// Commented out by RMH on 7/8/04
				}
				s = objSupp.AssocField;
                //Sanand5 04/12/07
                if ((s != "") && (s != "<NONE>"))
                {
				assocTableName =s.Split('.')[0];
				assocFieldName =s.Split('.')[1];
                }
				objSupp.Visible = false;
                

				//TODO Follow the example implementation of Event group association checking for 
				// the appropriate DataModel classes as/after they are implemented and available.
				if(this.Parent is Event)
				{
					Event objEvent = (this.Parent as Event);
					switch(assocTableName)
					{
						case "EVENT":
							objSupp.Visible = objSupp.IsAssociatedCode(objEvent);
							break;
						case "EVENT_QM":
							objSupp.Visible = objSupp.IsAssociatedCode(objEvent.EventQM);
							break;
						case "ENTITY":
							objSupp.Visible = objSupp.IsAssociatedCode(objEvent.ReporterEntity);
							break;
					}
				}
					/*
					else if(this.Parent is Claim)
					else if(this.Parent is ClaimXLitigation)
					else if(this.Parent is PIEmployee)
					else if(this.Parent is PIOther)
					else if(this.Parent is PIWitness)
					else if(this.Parent is Policy)
					else if(this.Parent is PolicyEnh)
					else if(this.Parent is Employee)
					else if(this.Parent is Vehicle)
					else if(this.Parent is Entity)
					*/
				else if(this.Parent is Claim)
				{
					objClaim = (this.Parent as Claim);
					objTarget = objClaim as DataObject;

					//Pick a parent object.
					switch(assocTableName)
					{
						case "CLAIM":
							objTarget = objClaim as DataObject;
							break;
						case "EVENT":
							objTarget = objClaim.Parent as DataObject;
							//Pick a more specific parent object if necessary.
							// Support group assoc on levels higher than just dept.
							switch(assocFieldName)
							{
								case "DEPT_EID(FACILITY)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("FACILITY"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(LOCATION)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("LOCATION"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(DIVISION)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("DIVISION"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(REGION)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("REGION"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(OPERATION)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("OPERATION"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(COMPANY)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("COMPANY"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
								case "DEPT_EID(CLIENT)": 
									Context.LocalCache.GetParentOrgInfo((objClaim.Parent as Event).DeptEid,Common.Conversion.GetOrgHierarchyLevel("CLIENT"),ref tmpOrgShort,ref tmpOrgDesc,ref orgParentEid);
									objOrgEntity.MoveTo(orgParentEid);
									objTarget = objOrgEntity as DataObject;
									break;
							}
							break;
						case "ENTITY":
                            //GC and VA screens used to crash in case the supplemental field was associated with
                            //Worker compensation Employee state
                            if (objClaim.PrimaryPiEmployee != null)
                            {
                                objTarget = (objClaim.PrimaryPiEmployee.PiEntity as DataObject);
                            }
							break;
						default:
							objTarget = null;
							break;
					}

					// BSB 11.18.2005 This implementation was scary in old RMObjLib.
					// In the case of duplicate fields across LOB's, the first 
					// match always wins.  
					// For example, "EVENT.COUNTRY_CODE"
					// The intent is that this could be validly associated with either 
					// a VA and WC claim.  The implementation however checked only the first match
					// in an If/Else logic flow.  Thus even though an ElseIf case was added for 
					// "EVENT.COUNTRY_CODE" for VA first and WC second, it will only be shown when
					// associated with a VA claim.  The WC possibility will be ignored.
					//
					// To get around this I'm extending the LOB Filter string to inclusively 
					// accept multiple Lines of Business for a single Grp Association check.
					
					//Special Case VA LOB Only Fields
					sLobFilter = "";

					switch(assocFieldName)
					{
						case "ACCIDENT_DESC_CODE":
						case "ACCIDENT_TYPE_CODE":
						case "COUNTRY_CODE":
						case "STATE_ID":
							sLobFilter += "|VA|";
							break;
					}
					//Special Case WC LOB Fields
					switch(assocFieldName)
					{
						case "CAUSE_CODE":
						case "COST_CENTER_CODE":
						case "COUNTRY_CODE": //Handles both Emp Entity and Event
						case "EMAIL_TYPE_CODE":
						case "SEX_CODE":
						case "STATE_ID": //Handles both Emp Entity and Event
						case "FILING_STATE_ID":
							sLobFilter += "|WC|";
							break;
					}

					//Conduct the Association Test...
					objSupp.Visible = objSupp.IsAssociatedCode(objTarget,sLobFilter);

				}
				else if(this.Parent is ClaimXLitigation)
				{
					ClaimXLitigation objClaimXLitigation = (this.Parent as ClaimXLitigation);
					switch(assocTableName)
					{
						case "CLAIM_X_LITIGATION":
                            objSupp.Visible = objSupp.IsAssociatedCode(objClaimXLitigation);
							break;
						case "ATTORNEY":
							objSupp.Visible = objSupp.IsAssociatedCode(objClaimXLitigation.CoAttorneyEntity);
							break;
						case "JUDGE":
							objSupp.Visible = objSupp.IsAssociatedCode(objClaimXLitigation.JudgeEntity);
							break;
                        //Start By Shivendu for MITS 9467
                        case "CLAIM":
                            objTarget = (this.Parent.Parent as Claim);
                            sLobFilter = "";
                            objSupp.Visible = objSupp.IsAssociatedCode(objTarget, sLobFilter);
                            break;
                        //End By Shivendu for MITS 9467
					}
				}
				else if(this.Parent is PiEmployee)
				{
					switch(assocTableName)
					{
						case "PERSON_INVOLVED":
							PiEmployee objPiEmployee = (this.Parent as PiEmployee);
							objSupp.Visible = objSupp.IsAssociatedCode(objPiEmployee);
							break;
						case "ENTITY":
							Entity objEntity = (this.Parent as Entity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;
					}
					
				}
				else if(this.Parent is PiOther)
				{
					switch(assocTableName)
					{
						case "ENTITY":
							objSupp.Visible = objSupp.IsAssociatedCode((this.Parent as PiOther).PiEntity);
							break;
						case "PERSON_INVOLVED":
							objSupp.Visible = objSupp.IsAssociatedCode(this.Parent as PiOther);
							break;

					}
				}
				else if(this.Parent is PiWitness)
				{
					switch(assocTableName)
					{
						case "ENTITY":
							Entity objEntity = ((this.Parent as PiWitness).PiEntity as Entity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;
						case "PERSON_INVOLVED":
							PiWitness objPiWitness = (this.Parent as PiWitness);
							objSupp.Visible = objSupp.IsAssociatedCode(objPiWitness);
							break;
					}
				}
				else if(this.Parent is Policy)
				{
					switch(assocTableName)
					{
						case "POLICY":
							Policy objPolicy = (this.Parent as Policy);
							objSupp.Visible = objSupp.IsAssociatedCode(objPolicy);
							break;
						case "ENTITY":
							Entity objEntity = ((this.Parent as Policy).InsurerEntity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;
					}
				}
				else if(this.Parent is PolicyEnh)
				{
					switch(assocTableName)
					{
						case "POLICY_ENH":
							PolicyEnh objPolicyEnh = (this.Parent as PolicyEnh);
							objSupp.Visible = objSupp.IsAssociatedCode(objPolicyEnh);
							break;
						case "ENTITY":
							Entity objEntity = ((this.Parent as PolicyEnh).InsurerEntity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;

					}
				}
				else if(this.Parent is Employee)
				{
					switch(assocTableName)
					{
						case "EMPLOYEE":
							Employee objEmployee = (this.Parent as Employee);
							objSupp.Visible = objSupp.IsAssociatedCode(objEmployee);
							break;
						case "ENTITY":
							Entity objEntity = ((this.Parent as Employee).EmployeeEntity as Entity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;
					}
				}
				else if(this.Parent is Vehicle)
				{
					switch(assocTableName)
					{
						case "VEHICLE":
							Vehicle objVehicle = (this.Parent as Vehicle);
							objSupp.Visible = objSupp.IsAssociatedCode(objVehicle);
							break;
					}
				}
				else if(this.Parent is Entity)
				{
					switch(assocTableName)
					{
						case "ENTITY":
							Entity objEntity = (this.Parent as Entity);
							objSupp.Visible = objSupp.IsAssociatedCode(objEntity);
							break;
					}
				}
				else
				{
					DataObject objData = (this.Parent as DataObject);
					if(assocTableName==objData.Table)
						objSupp.Visible = objSupp.IsAssociatedCode(objData);
				}

				//hSkip: // Commented out by RMH on 7/8/04
				//; //Null statement for the label to use when skipping to next SuppField.
			} //foreach
		}
		public  void Delete()
		{
			string SQL ="";
			string sWhere="";
			SupplementalField objSupp =null;
			bool isOurDbTrans = false;

			//Check for supp table definition
			if(!this.DefinitionLoaded)
				return;

			// Check for Stale Data  
			// Means data is out of sync with parent object and the new data was never touched to be loaded.
			// MITS 20761 Don't need to load the record when try to delete it
            //if((this as IDataModel).IsStale)
			//	return;

			foreach(string key in m_SuppFields)
			{
				objSupp = (m_SuppFields[key] as SupplementalField);
				if(	objSupp.IsKeyValue)
				{
					if(sWhere != "")
						sWhere += " AND ";
					sWhere += string.Concat(objSupp.FieldName , "=" , objSupp.Value);
				}
			}

			//Check for any Supp Fields defined
			if(sWhere=="")
				return;
			
			try
			{
			
				//If we're not already in a transaction, start one.
				if(this.Context.DbTrans == null)
				{
					this.Context.DbTrans = Context.DbConn.BeginTransaction();
					isOurDbTrans= true;
				}

				SQL = String.Format("DELETE FROM {0} WHERE {1}",this.TableName,sWhere);

				DbCommand cmd = this.Context.DbConn.CreateCommand();
				cmd.Transaction = Context.DbTrans;
				cmd.CommandText = SQL;
				cmd.ExecuteNonQuery();

				//Handle Multi-Value Fields
				object[] arr = this.m_SuppFields.GetKeyArray();
				int oldKey = this.KeyValue;
				for(int i=0; i< arr.Length;i++)
				{
					objSupp = (m_SuppFields[(string)arr[i]] as SupplementalField);
				{
					objSupp.Value = null;
                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // Delete the records corresponding to User Lookup
					if(	objSupp.IsMultiValue || objSupp.IsUserLookup)
					{
						objSupp.Values.ClearAll();
						SQL = String.Format("DELETE FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID = {1}"
                            ,objSupp.FieldId
                            ,oldKey);
						cmd.CommandText = SQL;
						cmd.ExecuteNonQuery();//Shares cmd and Transaction as necessary.
					}
				}
				}

				//Book-Keeping
				if(isOurDbTrans)
					Context.DbTrans.Commit();
			}
			catch(Exception e)
			{
				this.Context.DbTrans.Rollback();
                throw new DataModelException(Globalization.GetString("Supplementals.Delete.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if(isOurDbTrans)
					this.Context.DbTrans = null;

			}
		}

		public int Count(){return m_SuppFields.Count();}
        // akaushik5 Changed for FROI Migration Starts - Added for Froi Migration- Mits 33585,33586,33587

        public SupplementalField this[string sFieldName] 
        { 
            get 
            { 
                //return (m_SuppFields[sFieldName] as SupplementalField);
                SupplementalField objSuppField = (m_SuppFields[sFieldName] as SupplementalField);

                if (objSuppField == null)
                { 
                    bool success;
                    int index = Conversion.CastToType<int>(sFieldName, out success);
                    string key = string.Empty;
                    if (success)
                    {
                        int count = 1;
                        foreach (string itemKey in m_SuppFields.Keys)
                        {
                            if (count == index)
                            {
                                key = itemKey;
                                break;
                            }
                            count++;
                        }
                        if (!string.IsNullOrEmpty(key))
                        {
                            objSuppField = m_SuppFields[key] as SupplementalField;
                        }
                    }
                }

                return objSuppField;
            } 
        }
        // akaushik5 Changed for FROI Migration Ends

		// Implemented for classes like Claimant where we have 
		// Claim_ROW_ID field which is not listed in supplemental table definition.
		internal void Add(SupplementalField objSupp){m_SuppFields[objSupp.FieldName] = objSupp;}
		public bool IsAnyRequired
		{
			get
			{
				SupplementalField objSupp=null;
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
					if(objSupp.Required &&  objSupp.Visible)
						return true;
				}
				return false;
			}
		}
		#region Serialization Implementation
		const string SCHEMA_FILE= "schema-1.xsd";
		public bool PopulateObject(XmlDocument propertyStore)
		{
			return PopulateObject(propertyStore,this.GetType().Name,false);
		}
		public bool PopulateObject(XmlDocument propertyStore,bool bKeepCurrentData)
		{
			return PopulateObject(propertyStore,this.GetType().Name,bKeepCurrentData);
		}
		internal bool PopulateObject(XmlDocument propertyStore, string rootTagName)
		{
			return PopulateObject(propertyStore,rootTagName,false);
				
		}
		internal bool PopulateObject(XmlDocument propertyStore, string rootTagName,bool bKeepCurrentData)
		{
			bool bAddingNew = false;
            bool bNodeRemoved = false;//asharma326 JIRA 6422
			string sProp = "";
			XmlElement elt = null;
			XmlReader 	xmlValidator=null;

			//1.) Determine if propertyStore is valid for the current object type.
			//a.)Root Tag matches class name?
			if(propertyStore.DocumentElement.Name == rootTagName)
				elt = propertyStore.DocumentElement as XmlElement;
			else
				elt = propertyStore.SelectSingleNode(String.Format("/Instance/{0}",rootTagName)) as XmlElement;

			if(elt==null)
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RootElementMismatch", this.Context.ClientId)
                    ,propertyStore.DocumentElement.Name
                    , rootTagName));
			if(elt.HasAttribute("tablename"))
				if(elt.GetAttribute("tablename") !="" && elt.GetAttribute("tablename") != this.TableName)
                    throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.SuppTableMismatch", this.Context.ClientId)
                        ,elt.GetAttribute("tablename")
                        ,this.TableName));
			elt = null;

			//b.) Document Validates?
            try
            {
                if (propertyStore.DocumentElement.Name == "Instance") //Then this is the first time we're seeing this document, (Worth doing the validation) 
                {
                    XmlUtilityFunctions.ExtractInstanceInfo(ref propertyStore, rootTagName, ref xmlValidator);
                }
            }
			catch(Exception e)
			{
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.SchemaValidationException", this.Context.ClientId), rootTagName), e);
			}
			finally
			{
				if(xmlValidator !=null)
					xmlValidator.Close();
				if(SerializationContext.RWLock.IsReaderLockHeld)
					SerializationContext.RWLock.ReleaseReaderLock();
			}
			//2.) If propertyStore represents an existing object then Navigate this object to the existing record.
			// NinetyNine percent of the time we will already be on the proper record but this won't hurt...
			try{elt = propertyStore.DocumentElement.SelectSingleNode(this.KeyField.FieldName) as XmlElement;}
			catch(Exception e)
			{
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.KeyFieldNameMissing", this.Context.ClientId), rootTagName 
                    + "." 
                    + this.KeyField.FieldName),e);
			}
			
			bAddingNew =(elt.InnerText == "0" && this.KeyValue == 0);

			//BSB 05.23.2005  I don't think this is an error per se.  It's really just a condition
			// that tells us to stop loading this supp object from instance XML???  The 
			// reasoning is that additional xml is not sufficient to signal the addition of 
			// supplemental data.  
			//(Existing data could be EDITED but a full record cannot currently be added in this manner.)

			// BSB 08.30.2005 This was previously disabled to prevent the situation where a user could call
			// Save directly on this object w/o having set the primary key from the "parent" record.
			// This causes problems creating both a new parent and it's associated supp data in one request.
			// Instead of prohibiting this here where it causes problems, 
			// let's just check for it during save on Supplementals and Acord classes where it matters.
			// Otherwise we won't be able to add new admin tracking either.
			//			if(bAddingNew)
			//				return true;
			
			//throw new Riskmaster.ExceptionTypes.XmlOperationException(Globalization.GetString("Supplementals.PopulateObject.Exception.NoCreateNewSupp"));

			//3.) (if not new) Compare top level TimeStamp? 
			//BSB Supplementals don't have independent date time stamps currently.
			//			{
			//				elt = propertyStore.SelectSingleNode("/*/DttmRcdLastUpd") as XmlElement;
			//
			//				if(elt !=null)
			//					if(this.GetProperty("DttmRcdLastUpd").ToString() !=elt.InnerText)
			//						throw new Riskmaster.ExceptionTypes.DataModelException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RecordAlreadyChanged"), this.GetType().Name + "." + this.KeyFieldName, this.KeyFieldValue));
			//			}

			//4.) For all children object data in the propertyStore.
			//a.) IF child is SimpleList - then treat this as a scalar and handle it in the PopulateScalar function.
			//b.) IF child is Singleton - then recursively call PopulateObject with the child object data Xml sub-tree.
			//c.) IF child is DataCollection - then call PopulateObject on the collection?
			//d.) IF child is Supplementals - then call PopulateObject on the Supplementals class.
			//e.) IF child is Scalar - then apply property values to this object using PopulateScalar function.
			foreach(XmlElement item in propertyStore.SelectNodes(String.Format("/{0}/*",rootTagName)))
			{
				 string sCodeId = "";
				//All contents of supplemental tables should be scalar in nature. (DataSimpleList is ok)
				// JP 11.08.2005   sProp = item.LocalName;
				sProp = XmlConvert.DecodeName(item.LocalName);  // JP 11.08.2005   Decode if supp field name had bad-xml name chars in it (#, space, etc.)
				/*** Scalar Field Value ***/
				SupplementalField oProp = this.m_SuppFields[sProp] as SupplementalField;
				
				// BSB If the supp key indicates it's a new record and the 
				// incoming key value also indicates "new record" but may vary slightly (0,-1,-2 etc)
				// DO NOT CHANGE THE CURRENT KEY VALUE.  This ensures that the DataChanged flag is not 
				// triggered w/o need.

                //Added by Manika : XML Import
                if (oProp == null)
                {
                    if (bNodeRemoved)//asharma326 JIRA 6422
                    {
                        bNodeRemoved = false;
                        continue;
                    }
                    else
                        throw new Riskmaster.ExceptionTypes.DataModelException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.InvalidSupplementalField", this.Context.ClientId), sProp));
                }
                // End Manika
                if (oProp.IsKeyValue && this.KeyValue <= 0)
                {
                    if (item.HasAttribute("codeid"))
                    {
                        if (0 >= Conversion.ConvertStrToInteger(item.Attributes["codeid"].Value))
                            continue;
                    }
                    else if (0 >= Conversion.ConvertStrToInteger(item.InnerText))
                        continue;
                }
                System.Diagnostics.Trace.WriteLine("Populating Supplemental Scalar: " + sProp);
                // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                // Need to populate object for user lookup as well.
                if (oProp.IsMultiValue || oProp.IsUserLookup)
                {
                    DataSimpleList lst = oProp.Values;
                    if (item.HasAttribute("codeid")) //mkaran2 - MITS 36756
                    sCodeId = item.Attributes["codeid"].Value.ToString();
                    //Pankaj\BSB 02.21.2006 Fix for removing a lone single code from a multi-value supp field.
                    //TR# 2205
                    //if(lst!=null && sCodeId!="" && sCodeId!="0" && lst.ToString() != sCodeId) //It's a DataSimpleList
                    if (lst != null && lst.ToString() != sCodeId) //It's a DataSimpleList
                    {
                        lst.ClearAll();
                        foreach (string sSingleCodeId in sCodeId.Split(' '))
                            lst.Add(Conversion.IsNumeric(sSingleCodeId) ? Int32.Parse(sSingleCodeId) : 0);
                        //11.28.2005 Mihika Fix for supp save issue. Changes lost after first modified
                        // SimpleList value.
                        //return true;
                    }
                }
                else if (item.HasAttribute("codeid"))
                    oProp.Value = Conversion.ConvertStrToInteger(item.Attributes["codeid"].Value);

                //asharma326 JIRA# 6422 Starts 
                else if (oProp.IsHTMLValue)
                {
                    //oProp.Value = item.InnerText + SupplementalField.HTMLDELIMITER + propertyStore.SelectSingleNode(rootTagName + "//" + item.LocalName + "_HTMLComments").InnerText;
                    //XmlNodeList nodes = propertyStore.GetElementsByTagName(item.LocalName + "_HTMLComments");
                    //XmlNode node = nodes[0];
                    //node.ParentNode.RemoveChild(nodes[0]);//dont remove the node from xmldoc just set flag to ignore the next element in case of HTMLType.
                    //oProp.HTMLContents = new string[2];
                    //oProp.HTMLContents[0] = item.InnerText;
                    oProp.Value = item.InnerText;
                    oProp.HTMLContents= propertyStore.SelectSingleNode(rootTagName + "//" + item.LocalName + "_HTMLComments").InnerText;
                    bNodeRemoved = true;
                }
                //asharma326 JIRA# 6422 Ends
                
        
                else
                    oProp.Value = item.InnerText;
                
			} //End "For Each Property"				
				
			return true;
		}

		public string SerializeObject()
		{
			SerializationContext objContext = new SerializationContext();
			objContext.RequestDom= new XmlDocument();
			objContext.RequestDom.LoadXml(String.Format("<{0} />",this.GetType().Name));
			return SerializeObject(objContext);
		}
		public string SerializeObject(string sTag, XmlDocument dom)
		{
			SerializationContext objContext = new SerializationContext();
			objContext.RequestDom= dom;
			objContext.ElementTag = sTag;
			return SerializeObject(objContext);
		}
		internal string SerializeObject(SerializationContext objContext)
		{
            //string s="";
            
            //rsolanki2: replacing direct string concats with stringbuilder
            StringBuilder sb = new StringBuilder();

			bool bEntryCall=false;

			if(objContext.ElementTag ==null || objContext.ElementTag=="" )
			{
				objContext.ElementTag =this.GetType().Name;
				if(objContext.RequestXPath == null || objContext.RequestXPath =="") //Initialize XPath iff this is the entry point
				{
					objContext.RequestXPath = objContext.ElementTag;
					bEntryCall = true;
				}
			}

            sb.Append("<");
            sb.Append( objContext.ElementTag);
            sb.Append( " tablename='");
            sb.Append( this.TableName);
            sb.Append( "'>");

			foreach(SupplementalField o in this)
			{
                //s += o.SerializeObject();
                sb.Append(o.SerializeObject());
			}
            //s = String.Format("<{0} tablename='{2}'>{1}</{0}>", objContext.ElementTag, s, this.TableName);

            //s = String.Concat("<"
            //    , objContext.ElementTag
            //    , " tablename='"
            //    , this.TableName
            //    , "'>"
            //    , s
            //    , "</"
            //    , objContext.ElementTag
            //    , ">");

            sb.Append("</");
            sb.Append( objContext.ElementTag);
            sb.Append(">");

            //, objContext.ElementTag, s, this.TableName);            

            if (bEntryCall) 	//"Pre Processing" occurs only if this is the entry point for a Serialization Request
            {
                //return String.Format(@"<Instance xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.riskmaster.com/wizard "
                //        + SCHEMA_FILE.Replace("\\", "//")
                //        + @""">{0}</Instance>", s);
                return 
                    String.Concat(@"<Instance xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.riskmaster.com/wizard "
                        , SCHEMA_FILE.Replace("\\", "//")
                        , @""">"
                        , sb.ToString()
                        , @"</Instance>");
            }
            else
            {
                //return s;
                return sb.ToString();
            }

		}
		#endregion
		IEnumerator IEnumerable.GetEnumerator(){return new SupplementalEnumerator(this,this.Context.ClientId);}

		#region IPersistence Members
		public event InitObjEventHandler InitObj;

        public event InitDataEventHandler InitData;

		public event Riskmaster.DataModel.CalculateDataEventHandler CalculateData;

		public event Riskmaster.DataModel.ValidateEventHandler ValidateData;

		public event Riskmaster.DataModel.BeforeSaveEventHandler BeforeSave;

		public event Riskmaster.DataModel.AfterSaveEventHandler AfterSave;

        public event Riskmaster.DataModel.BeforeDeleteEventHandler BeforeDelete;
        DbWriter objWriter = null;//mbahl3 Mit 23349 
		public bool IsNew
		{
			get{return (m_isNew || this.KeyValue ==0);}
		}

		override public  bool DataChanged
		{
			get
			{
				foreach(string key in m_SuppFields)
					if((m_SuppFields[key] as SupplementalField).DataChanged)
						return true;
				return false;
			}
			set
			{
				if(this.IsDataChangedFlagLocked())
					return;
				foreach(string key in m_SuppFields)
					(m_SuppFields[key] as SupplementalField).DataChanged = value;
			}
		}
		internal override void LockDataChangedFlag()
		{
			base.LockDataChangedFlag ();
			foreach(string key in m_SuppFields)
				(m_SuppFields[key] as SupplementalField).LockDataChangedFlag();

		}
		internal override void UnlockDataChangedFlag()
		{
			base.UnlockDataChangedFlag ();
			foreach(string key in m_SuppFields)
				(m_SuppFields[key] as SupplementalField).UnlockDataChangedFlag();
		}


		override public bool AnyDataChanged
		{
			get
			{
				return this.DataChanged;
			}
		}


		public virtual void Save()
		{
			string SQL = "";
			string sWhere = "";
			SupplementalField objSupp = null;
			bool isOurDbTrans = false;
			DbWriter objWriter = null;
            //added by shivendu for Supplemental grid
            DbReader objReader = null;
			int recordId= 0;
			bool tmpFlag = false;
            Dictionary<string, object> dictParams=null;
            DbCommand objCommand = null;
			try
			{
				this.m_DataModelState=DataObjectState.IsSaving;

				// Check for Stale Data  
				// Means data is out of sync with parent object and the new data was never touched to be loaded.
				if((this as IDataModel).IsStale)
					return;

				//Check for data changed.
				if(!this.DataChanged)
					return;


				//Trigger Calculate, Validate and BeforeSave script events.
				//TODO Error Trapped & stronger typed Calculate Data Script Call
                if (bScriptEnabled)//Added by shivendu for MITS 24420
                {
                    this.CalculateData(this, new CalculateDataEventArgs());
                }
				//TODO Fully Error trapped Validate Data Script Call
                if (bScriptEnabled)//Added by shivendu for MITS 24420
                {
                    if (!this.ValidateData(this, new ValidateEventArgs())) throw new RMAppException("DataObject.Save.ValidationFailure");
                }


				// Where Clause Creation
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
					if(objSupp.IsKeyValue || objSupp.DataChanged)
						if(objSupp.FieldType == SupplementalFieldTypes.SuppTypePrimaryKey)
						{
							if(sWhere !="")
								sWhere += " AND ";
							if((int)objSupp.Value ==0)
								objSupp.Value = Context.GetNextUID(this.TableName);
							else
								sWhere += string.Concat( objSupp.FieldName , "=" , objSupp.Value);
						}
				}
				
				
				//TODO Error Trapped & stronger typed BeforeSave Data Script Call
                if (bScriptEnabled)//Added by shivendu for MITS 24420
                {
                    this.BeforeSave(this, new BeforeSaveEventArgs());
                }

				//Ensure we're in a  transaction.
				if (Context.DbTrans ==null)
				{
					Context.DbTrans = Context.DbConn.BeginTransaction();
					isOurDbTrans = true;
				}

				
				//Begin Preparing Writer
				objWriter = DbFactory.GetDbWriter(Context.DbConn);

				objWriter.Tables.Add(this.TableName);

				//Fix for new records not added due to where clause...
				if(!this.IsNew)
					objWriter.Where.Add(sWhere);

				//Save single value fields.
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // We need to handle the save of User lookup differently
                    // if((objSupp.IsKeyValue || objSupp.DataChanged) && ! objSupp.IsMultiValue)
                    //Modified by Shivendu for Supplemental Grid
                    if (!objSupp.IsMultiValue && !objSupp.IsUserLookup && !(objSupp.FieldType == SupplementalFieldTypes.SuppTypeGrid))
                        objWriter.Fields.Add(objSupp.FieldName, objSupp.Value);
                }
				objWriter.Execute(Context.DbTrans);

				//Save Multi-value fields.
				recordId = this.KeyValue;
				foreach(string key in m_SuppFields)
				{
					objSupp = (m_SuppFields[key] as SupplementalField);
                    // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                    // Handle the save of UserLookup
					if((objSupp.IsMultiValue || objSupp.IsUserLookup) && objSupp.DataChanged && recordId != 0)
					{
						SQL = String.Format("DELETE FROM SUPP_MULTI_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}"
                            ,objSupp.FieldId
                            ,recordId);
						Context.DbConn.ExecuteNonQuery(SQL,Context.DbTrans);

						SQL = String.Format(@"INSERT INTO SUPP_MULTI_VALUE (FIELD_ID,RECORD_ID,CODE_ID) VALUES({0},{1},{2})"
                            ,objSupp.FieldId
                            ,recordId
                            ,"{SIMPLELISTVALUE}");
						objSupp.Values.SaveSimpleList(SQL);
					}
                    //Start by Shivendu to save Supplemental grid values
                    else if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeGrid && objSupp.DataChanged && recordId != 0)
                    {
                        SQL = String.Format("DELETE FROM SUPP_GRID_VALUE WHERE FIELD_ID={0} AND RECORD_ID={1}"
                            , objSupp.FieldId
                            , recordId);
                        Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                        if (objSupp.Value.ToString() != "")
                        {
                            int rowID = 0;
                            int colCount = 0;
                            long nextRowId = 0;
                            int counter = -1;
                            bool nextRow = true;
                            string[] gridValue = null;
                            ArrayList colNumber = new ArrayList();
                            gridValue = objSupp.Value.ToString().Split('~'); //MITS 23349: ybhaskar
                            SQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" 
                                + objSupp.FieldId 
                                + "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC";
                            objReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, SQL);
                            while (objReader.Read())
                            {

                                colNumber.Add(objReader["COL_NO"].ToString());
                                colCount++;


                            }


                            while (nextRow)
                            {
                                foreach (object item in colNumber)
                                {
                                    counter = counter + 1;
                                    if (counter == gridValue.Length)
                                    {
                                        nextRow = false;
                                        break;
                                    }
                                    if (gridValue[counter] == "KNALB")
                                        gridValue[counter] = "";
                                    objWriter = DbFactory.GetDbWriter(Context.DbConn);
                                    objWriter.Tables.Add("SUPP_GRID_VALUE");

                                    nextRowId = Utilities.GetNextUID(Context.DbConn.ConnectionString, "SUPP_GRID_VALUE", this.Context.ClientId);
                                    //for (int i = 0; i < arr.Length; i++)
                                    //objWriter.Fields.Add((string)arr[i], this.GetField((string)arr[i]));
                                    objWriter.Fields.Add((string)"GRID_VALUE_ROW_ID", nextRowId);
                                    objWriter.Fields.Add((string)"FIELD_ID", objSupp.FieldId);
                                    objWriter.Fields.Add((string)"RECORD_ID", recordId);
                                    objWriter.Fields.Add((string)"ROW_NUM", rowID);
                                    objWriter.Fields.Add((string)"COL_NUM", item.ToString());
                                    objWriter.Fields.Add((string)"CELL_VALUE", gridValue[counter]);
                                    //mbahl3 mit:- 23349

                                    objWriter.Execute(Context.DbTrans);

                                    //smishra25: Added apostrophe SQL equivalent for MITS 21605
                                   // SQL = "INSERT INTO SUPP_GRID_VALUE (GRID_VALUE_ROW_ID,FIELD_ID,RECORD_ID,ROW_NUM,COL_NUM,CELL_VALUE) VALUES(" + nextRowId + "," + objSupp.FieldId + "," + recordId + "," + rowID + "," + item.ToString() + ",'" + gridValue[counter].Replace("'","''") + "')";
                                    //Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                                }
                                rowID++;
                            }


                        }

                    }
                    //End by Shivendu to save Supplemental grid values
					//asharma326 JIRA# 6422 Starts  
                    else if (objSupp.FieldType == SupplementalFieldTypes.SuppTypeHTML && objSupp.DataChanged && recordId != 0)
                    {
                        SQL = String.Format("DELETE FROM SUPP_TEXT_VALUES WHERE FIELD_ID={0} AND RECORD_ID={1}"
                               , objSupp.FieldId
                               , recordId);
                        Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
                        if (!String.IsNullOrEmpty(objSupp.HTMLContents))
                        {
                            dictParams = new Dictionary<string, object>();
                            objCommand = Context.DbConn.CreateCommand();
                            SQL = String.Format(@"INSERT INTO SUPP_TEXT_VALUES (FIELD_ID,RECORD_ID,HTML_TEXT) VALUES({0},{1},{2})", "~FieldId~", "~recordId~", "~HTMLContents~");
                                //, objSupp.FieldId
                                //, recordId
                                //, objSupp.HTMLContents);
                            dictParams.Add("FieldId", objSupp.FieldId);
                            dictParams.Add("recordId", recordId);
                            dictParams.Add("HTMLContents", objSupp.HTMLContents);
                            objCommand.CommandText = SQL;
                            objCommand.Transaction = Context.DbTrans;
                            DbFactory.ExecuteNonQuery(objCommand, dictParams);

                            //Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans,);
                        }
                    }
                    //asharma326 JIRA# 6422 Ends
                }



                //Book-Keeping
				if(isOurDbTrans)
					Context.DbTrans.Commit();

				tmpFlag = this.IsDataChangedFlagLocked();
				if(tmpFlag)
					this.UnlockDataChangedFlag();
				
				this.DataChanged = false;
				
				if(tmpFlag)
					this.LockDataChangedFlag();
			

				//TODO Error Trapped & stronger typed BeforeSave Data Script Call
                if (bScriptEnabled)//Added by shivendu for MITS 24420
                {
                    this.AfterSave(this, new AfterSaveEventArgs());
                }

			}
			catch(Exception e)
			{
				this.Context.DbTrans.Rollback();
                throw new DataModelException(Globalization.GetString("Supplementals.Save.Exception", this.Context.ClientId), e);
			}
			finally
			{
				if(isOurDbTrans)
					this.Context.DbTrans = null;
                if (dictParams != null)
                    dictParams = null;
				this.m_DataModelState=DataObjectState.IsReady;
               //Added by Shivendu for supplemental grid
                if (objReader != null)
                    objReader.Dispose();

			}
		}

		public void Refresh()
		{
			Load();
		}
		public bool Validate(){return this.ValidateData(this,new ValidateEventArgs());}

		internal string Dump()
		{
			string s = "";
			string sIndent = "";
			for(int i=1;i< DataObject.m_NestedDumpLevel;i++)
				sIndent+="\t";

			foreach(SupplementalField objSupp in this)
			{
                // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
                // Handle the User lookup as well
				if(objSupp.IsMultiValue || objSupp.IsUserLookup)
				{
					s += string.Concat(sIndent , objSupp.Caption , "(" , objSupp.FieldName , ")=\n");
					foreach(DictionaryEntry o in objSupp.Values)
					{
						if(o.Value==null)
							s += "null\n";
						else
							s += o.Value.ToString() + "\n";
					}
				}
				else
				{	
					if(objSupp.Value==null)
						s+= string.Concat(sIndent 
                            , objSupp.Caption 
                            , "(" 
                            , objSupp.FieldName 
                            , ")="  
                            , "null\n");
					else
						s+= string.Concat(sIndent 
                            , objSupp.Caption 
                            , "(" 
                            , objSupp.FieldName 
                            , ")="  
                            , (string)(objSupp.Value).ToString() 
                            , "\n");
				}
			}
			return s;
		}
		#endregion

		class SupplementalEnumerator : IEnumerator
		{
			int m_iPos = -1;
			SupplementalObject m_parent = null;
			object[] keys = null;

			//BSB Hacked to ensure that enumerating supp fields 
			// are returned in Ascending order by SeqNum.
			internal SupplementalEnumerator(SupplementalObject parent,int iClientId =0)
			{
				try
				{
					int i=0;
					m_parent = parent;

					SupplementalField[] suppFields = new SupplementalField[m_parent.m_SuppFields.Count()];
					keys = new object[m_parent.m_SuppFields.Count()];
				
					foreach(string sKey in m_parent.m_SuppFields)
						suppFields[i++]=m_parent.m_SuppFields[sKey] as SupplementalField;
					Array.Sort(suppFields);

					for(int j=0;j<i;j++)
						keys[j]=suppFields[j].FieldName;

					//keys = m_parent.m_SuppFields.GetKeyArray();
				}
				catch(Exception e)
                { throw new DataModelException(Globalization.GetString("SupplementalIterator.Exception", iClientId), e); }
			}
			#region IEnumerator Members

			public void Reset()
			{
				m_iPos =-1;
			}

			public object Current
			{
				get
				{
					if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
						return null;
					return m_parent.m_SuppFields[(string)keys[m_iPos]];
				}
			}

			public bool MoveNext()
			{
				m_iPos++;

				if(m_iPos <0 || m_iPos >=m_parent.m_SuppFields.Count())
					return false;
				else
					return true;
			}

			#endregion

		}
	}



}
