using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClaimStatusHistList.
	/// </summary>
	public class ClaimStatusHistList : DataCollection
	{
		internal ClaimStatusHistList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CL_STATUS_ROW_ID";
			this.SQLFromTable =	"CLAIM_STATUS_HIST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ClaimStatusHist";
		}
		public new ClaimStatusHist this[int keyValue]{get{return base[keyValue] as ClaimStatusHist;}}
		public new ClaimStatusHist AddNew(){return base.AddNew() as ClaimStatusHist;}
		public  ClaimStatusHist Add(ClaimStatusHist obj){return base.Add(obj) as ClaimStatusHist;}
		public new ClaimStatusHist Add(int keyValue){return base.Add(keyValue) as ClaimStatusHist;}
		public ClaimStatusHist LastStatusChange()
		{
			//BSB "fake SQL filter  1=0" may not have been set yet.
			// Note that SQL Filter is initialized to in order to indicate that the datacollection is not ready.
			string SQL  = "SELECT MAX(CL_STATUS_ROW_ID) FROM CLAIM_STATUS_HIST ";
			if(this.SQLFilter =="")
				this.SQLFilter = " 1=0 ";
			SQL += "WHERE " +  this.SQLFilter;
			int lastChangeId = (int)Context.DbConnLookup.ExecuteInt(SQL);
			if(lastChangeId==0 && this.Count!=0)//Try to get "In Memory Only" item if available.
				foreach(object iKey in this.m_keySet.Keys)
					lastChangeId=Math.Min((int)iKey,lastChangeId);
			return this[lastChangeId];
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}