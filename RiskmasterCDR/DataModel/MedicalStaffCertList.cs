using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forMedicalStaffCertList.
	/// </summary>
	public class MedicalStaffCertList : DataCollection
	{
		internal MedicalStaffCertList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"STAFF_CERT_ID";
			this.SQLFromTable =	"STAFF_CERTS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "MedicalStaffCert";
		}
		public new MedicalStaffCert this[int keyValue]{get{return base[keyValue] as MedicalStaffCert;}}
		public new MedicalStaffCert AddNew(){return base.AddNew() as MedicalStaffCert;}
		public  MedicalStaffCert Add(MedicalStaffCert obj){return base.Add(obj) as MedicalStaffCert;}
		public new MedicalStaffCert Add(int keyValue){return base.Add(keyValue) as MedicalStaffCert;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}