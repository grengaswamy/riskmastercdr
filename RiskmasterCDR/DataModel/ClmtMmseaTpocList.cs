﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class ClmtMmseaTpocList : DataCollection
    {

        internal ClmtMmseaTpocList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "TPOC_ROW_ID";
            this.SQLFromTable = "CLMT_MMSEA_TPOC";
            this.TypeName = "ClmtMmseaTpoc";
		}

        public new ClmtMmseaTpoc this[int keyValue] { get { return base[keyValue] as ClmtMmseaTpoc; } }
        public new ClmtMmseaTpoc AddNew() { return base.AddNew() as ClmtMmseaTpoc; }
        public ClmtMmseaTpoc Add(ClmtMmseaTpoc obj) { return base.Add(obj) as ClmtMmseaTpoc; }
        public new ClmtMmseaTpoc Add(int keyValue) { return base.Add(keyValue) as ClmtMmseaTpoc; }
		


        //TODO Remove after debugging this could be a security hole.
        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }
}
