﻿using System;
using Riskmaster.Common;
using Riskmaster.Settings;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_STGNG", "POLICY_ID", "PolicyName")]
    public class PolicyStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"AddedByUser", "ADDED_BY_USER"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"PolicyId", "POLICY_ID"},
														{"PolicyName", "POLICY_NAME"},
														{"PolicyNumber", "POLICY_NUMBER"},
														{"PolicyStatusCode", "POLICY_STATUS_CODE"},
														{"InsurerEid", "INSURER_EID"},
														{"IssueDate", "ISSUE_DATE"},
                                                        {"EffectiveDate", "EFFECTIVE_DATE"},
														{"ExpirationDate", "EXPIRATION_DATE"},
                                                        {"CancelledDate", "CANCELLED_DATE"},
                                                        {"Premium", "PREMIUM"},														
														{"PrimaryPolicyFlg", "PRIMARY_POLICY_FLG"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
														{"BrokerEid", "BROKER_EID"},
                                                        {"AllStatesFlag","ALL_STATES_FLAG"},
                                                        {"PolicySystemId","POLICY_SYSTEM_ID"},
                                                        {"PolicyLOB","POLICY_LOB_CODE"},
                                                        {"PolicySymbol","POLICY_SYMBOL"},
                                                        {"CurrencyCode","CURRENCY_CODE"},
                                                        {"Module","MODULE"},
                                                        {"MasterCompany","MASTER_COMPANY"},
                                                        {"LocCompany","LOCATION_COMPANY"}
                                    };
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public string PolicyName { get { return GetFieldString("POLICY_NAME"); } set { SetField("POLICY_NAME", value); } }
        public string PolicyNumber { get { return GetFieldString("POLICY_NUMBER"); } set { SetField("POLICY_NUMBER", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "POLICY_STATUS")]
        public string PolicyStatusCode { get { return GetFieldString("POLICY_STATUS_CODE"); } set { SetField("POLICY_STATUS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "INSURERS")]
        public int InsurerEid { get { return GetFieldInt("INSURER_EID"); } set { SetFieldAndNavTo("INSURER_EID", value, "InsurerEntity"); } }
        public string IssueDate { get { return GetFieldString("ISSUE_DATE"); } set { SetField("ISSUE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public string CancelledDate { get { return GetFieldString("CANCELLED_DATE"); } set { SetField("CANCELLED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string MasterCompany { get { return GetFieldString("MASTER_COMPANY"); } set { SetField("MASTER_COMPANY", value); } }
        public string LocCompany { get { return GetFieldString("LOCATION_COMPANY"); } set { SetField("LOCATION_COMPANY", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Ends
        public double Premium { get { return GetFieldDouble("PREMIUM"); } set { SetField("PREMIUM", value); } }
        public bool PrimaryPolicyFlg { get { return GetFieldBool("PRIMARY_POLICY_FLG"); } set { SetField("PRIMARY_POLICY_FLG", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "BROKER")]
        public int BrokerEid { get { return GetFieldInt("BROKER_EID"); } set { SetFieldAndNavTo("BROKER_EID", value, "BrokerEntity"); } }
        public bool AllStatesFlag { get { return GetFieldBool("ALL_STATES_FLAG"); } set { SetField("ALL_STATES_FLAG", value); } }
        public string PolicySystemId { get { return GetFieldString("POLICY_SYSTEM_ID"); } set { SetField("POLICY_SYSTEM_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "POLICY_CLAIM_LOB")]
        public string PolicyLOB { get { return GetFieldString("POLICY_LOB_CODE"); } set { SetField("POLICY_LOB_CODE", value); } }
        public string PolicySymbol { get { return GetFieldString("POLICY_SYMBOL"); } set { SetField("POLICY_SYMBOL", value); } }
        public string CurrencyCode { get { return GetFieldString("CURRENCY_CODE"); } set { SetField("CURRENCY_CODE", value); } }
        public string Module { get { return GetFieldString("MODULE"); } set { SetField("MODULE", value); } }
        private Riskmaster.Settings.SysParms m_objSysParms = null;
        #endregion

		#region Child Implementation
		private string[,] sChildren = {{"PolicyXCvgTypeListStgng","PolicyXCvgTypeListStgng"},
																//{"PolicyXMcoList","PolicyXMcoList"},
																{"PolicyXInsuredStgng","DataSimpleList"},
                                                                //{"PolicyXState","DataSimpleList"},//added by rkaur7 on 05/14/2009 -MITS 16668
																{"InsurerEntityStgng","EntityStgng"},
																{"BrokerEntityStgng","EntityStgng"},
																{"PolicyXInsurerListStgng","PolicyXInsurerListStgng"}, // Mihika MITS 8656 - Multiple Insurers
                                                                //Gagan Safeway Retrofit Policy Jursidiction : START
            													//{"PolicyXState","DataSimpleList"}, 
                                                                //Gagan Safeway Retrofit Policy Jursidiction : END
                                                                {"PolicyXUnitListStgng","PolicyXUnitListStgng"},
                                                                  {"PolicyXEntityListStgng","PolicyXEntityListStgng"}
									  };

		// Lock Entity Tables
		// Also Initialize SimpleList with desired table, field details.
		override internal  void OnChildInit(string childName, string childType)
		{
			EntityStgng objEnt =  null;
			//Do default per-child processing.
			base.OnChildInit(childName, childType);
			
			//Do any custom per-child processing.
			object obj = base.m_Children[childName];
			switch(childType)
			{
				case "EntityStgng": 
				switch(childName)
				{
					case "InsurerEntityStgng":
                        objEnt = (obj as EntityStgng);
						objEnt.EntityTableId= Context.LocalCache.GetTableId("INSURERS");
						objEnt.LockEntityTableChange = true;
						objEnt.DataChanged=false;
						//no real way to lock entity class navigation from internal access.
						break;
					case "BrokerEntityStgng":
                        objEnt = (obj as EntityStgng);
						objEnt.EntityTableId= Context.LocalCache.GetTableId("BROKER");
						objEnt.LockEntityTableChange = true;
						objEnt.DataChanged=false;
						//no real way to lock entity class navigation from internal access.
						break;
				}
					break;
				case "DataSimpleList": //There's currently only one so just assume it's policyInsured.
                    //Gagan Safeway Retrofit Policy Jursidiction : START
                    //"if" condition added as new datasimplelist PolicyXState also added
                    //if (childName == "PolicyXInsured")
                    //{
                    //    DataSimpleList objList = base.m_Children[childName] as DataSimpleList;
                    //    objList.Initialize("INSURED_EID", "POLICY_X_INSURED", "POLICY_ID");
                    //}
                    //Added code for PolicyState
                    //else if (childName == "PolicyXState")
                    //{
                    //    DataSimpleList objList = base.m_Children[childName] as DataSimpleList;
                    //    objList.Initialize("STATE_ID", "POLICY_X_STATE", "POLICY_ID");
                    //}
                    //Gagan Safeway Retrofit Policy Jursidiction : END
					//break;
                    //modidfied by rkaur7 on 05/15/2009 -MITS 16668
                    DataSimpleList objList = obj as DataSimpleList;
                    switch (childName)
                    {
                        case "PolicyXInsuredStgng":
                            objList.Initialize("INSURED_EID", "POLICY_X_INSURED_STGNG", "POLICY_ID");
                            break;
                        //case "PolicyXState":
                        //    objList.Initialize("STATE_ID", "POLICY_X_STATE", "POLICY_ID");
                        //    break;

                    }
                    break;
                    //end rkaur7

			}
		}
		//Handle adding the our key to any additions to the record collection properties.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			switch(childName)
			{
                //case "PolicyXMcoList" :
                //    (itemValue as PolicyXMco).PolicyId= this.PolicyId;
                //    break;
				case "PolicyXCvgTypeListStgng" :
					(itemValue as PolicyXCvgTypeStgng).PolicyId= this.PolicyId;
					break;
                // Mihika MITS 8656 - Multiple Insurers
                case "PolicyXInsurerListStgng":
					(itemValue as PolicyXInsurerStgng).PolicyId= this.PolicyId;
					break;
                case "PolicyXUnitListStgng" :
                    (itemValue as PolicyXUnitStgng).PolicyId= this.PolicyId;
					break;
                    //skhare7 Point Policy Interface
                case "PolicyXEntityListStgng":
                    (itemValue as PolicyXEntityStgng).PolicyId = this.PolicyId;
					break;
			}
			base.OnChildItemAdded (childName, itemValue);
		}					
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);
			//BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
            this.SetFieldAndNavTo("BROKER_EID", this.BrokerEid, "BrokerEntityStgng", true);
            this.SetFieldAndNavTo("INSURER_EID", this.InsurerEid, "InsurerEntityStgng", true);
		}

		//Handle any sub-object(s) for which we need the key in our own table.
		internal override void OnChildPreSave(string childName, IDataModel childValue)
		{
			// If Entity is involved we save this first (in case we need the EntityId.)
			if(childName=="BrokerEntityStgng")
			{
				(childValue as IPersistence).Save();
				this.m_Fields["BROKER_EID"]= (childValue as EntityStgng).EntityId;
			}
			// If Entity is involved we save this first (in case we need the EntityId.)
			if(childName=="InsurerEntityStgng")
			{
				(childValue as IPersistence).Save();
                this.m_Fields["INSURER_EID"] = (childValue as EntityStgng).EntityId;
			}
		}
		//Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			switch(childName)
			{
				case "PolicyXInsuredStgng":
					if(isNew)
						(childValue as DataSimpleList).SetKeyValue(this.PolicyId);
                    (childValue as DataSimpleList).SaveSimpleList();
					break;
                //Gagan Safeway Retrofit Policy Jursidiction : START - onchildpostsave written for PolicyXState
                //case "PolicyXState":
                //    if (isNew)
                //        (childValue as DataSimpleList).SetKeyValue(this.PolicyId);
                //    (childValue as DataSimpleList).SaveSimpleList();
                //    break;
                //Gagan Safeway Retrofit Policy Jursidiction : END
                //case "PolicyXMcoList":
                //    if(isNew)
                //        foreach(PolicyXMco item in (childValue as PolicyXMcoList))
                //            item.PolicyId= this.PolicyId;
                //    (childValue as IPersistence).Save();
                //    break;
				case "PolicyXCvgTypeListStgng":
					if(isNew)
						foreach(PolicyXCvgTypeStgng item in (childValue as PolicyXCvgTypeListStgng))
							item.PolicyId= this.PolicyId;
					(childValue as IPersistence).Save();
					break;
                // Mihika MITS 8656 - Multiple Insurers
                case "PolicyXInsurerListStgng":
					if(isNew)
						foreach(PolicyXInsurerStgng item in (childValue as PolicyXInsurerListStgng))
							item.PolicyId= this.PolicyId;
					(childValue as IPersistence).Save();
					break;
                case "PolicyXUnitListStgng":
                    if (isNew)
                        foreach (PolicyXUnitStgng item in (childValue as PolicyXUnitListStgng))
                            item.PolicyId = this.PolicyId;
                    (childValue as IPersistence).Save();
                    break;
                    //policy Interface skhare7
                case "PolicyXEntityListStgng":
                    if (isNew)
                        foreach (PolicyXEntityStgng item in (childValue as PolicyXEntityListStgng))
                            item.PolicyId = this.PolicyId;
                    (childValue as IPersistence).Save();
                    break;
			}
			base.OnChildPostSave (childName, childValue, isNew);
		}		
		//Protect Entities that should not be deleted.
		internal override void OnChildDelete(string childName, IDataModel childValue)
		{
			if(childName == "InsurerEntityStgng")
				return;
			if(childName== "BrokerEntityStgng")
				return;
			base.OnChildDelete (childName, childValue);
		}

        // Mihika MITS 8656 - Multiple Insurers
        public override void  Save()
        {
            // Manish Jain for Internal Policy
            if (this.PolicySystemId == string.Empty )
            {
                m_objSysParms = new SysParms(Context.DbConn.ConnectionString,Context.ClientId);
                this.CurrencyCode = Context.LocalCache.GetCodeDesc(m_objSysParms.SysSettings.BaseCurrencyType);
               
            }

            if (!IsNew)
                Context.DbConn.ExecuteNonQuery(String.Format("UPDATE POLICY_X_INSURER_STGNG SET INSURER_CODE = {0} WHERE POLICY_ID = {1} AND PRIMARY_INSURER = -1", this.InsurerEid, this.PolicyId), Context.DbTrans);

            // npadhy Start MITS 18111 Moved the Code of Adding the Insurer to onUpdateobject as we are updating the object and 
            // That code should not be called in case of Cloning. So Removed the code from here
            base.Save();
            //MGaba2:MITS 16441:End
        }

        // Mihika MITS 8656 - Multiple Insurers
        protected override void OnPreCommitDelete()
        {
            base.OnPreCommitDelete();
            Context.DbConn.ExecuteNonQuery(String.Format("DELETE FROM POLICY_X_INSURER_STGNG WHERE POLICY_ID = {0}", this.PolicyId), Context.DbTrans);
        }
      
  		//Child Property Accessors
        public EntityStgng InsurerEntity
		{
			get
			{
                EntityStgng objItem = base.m_Children["InsurerEntityStgng"] as EntityStgng;
				if((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("InsurerEntityStgng", (objItem as DataObject), this.InsurerEid) as EntityStgng;

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					objItem.MoveTo(this.InsurerEid);
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//				}
				return objItem;
			}
		}
        public EntityStgng BrokerEntity
		{
			get
			{
                EntityStgng objItem = base.m_Children["BrokerEntityStgng"] as EntityStgng;
				if((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("BrokerEntityStgng", (objItem as DataObject), this.BrokerEid) as EntityStgng;
//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					objItem.MoveTo(this.BrokerEid);
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//				}
				return objItem;
			}
		}

        //MGaba2:MITS 26249:Adding Abbreviation 
        //[ExtendedTypeAttribute(RMExtType.EntityList,"ANY")]       
        [ExtendedTypeAttribute(RMExtType.EntityList, "ANY", Constants.RenderHints.PREFIX_ENTITY_ABBR)]
		public DataSimpleList PolicyXInsuredStgng
		{
			get
			{
                DataSimpleList objList = base.m_Children["PolicyXInsuredStgng"] as DataSimpleList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.LoadSimpleList((int)this.KeyFieldValue);
				return objList;
			}
		}
        //Gagan Safeway Retrofit Policy Jursidiction : START
        //[ExtendedTypeAttribute(RMExtType.CodeList, "STATES")]
        //public DataSimpleList PolicyXState
        //{
        //    get
        //    {
        //        DataSimpleList objList = base.m_Children["PolicyXState"] as DataSimpleList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.LoadSimpleList((int)this.KeyFieldValue);
        //        return objList;
        //    }
        //}
        //Gagan Safeway Retrofit Policy Jursidiction : END
        //public PolicyXMcoList PolicyXMcoList
        //{
        //    get
        //    {
        //        PolicyXMcoList objList = base.m_Children["PolicyXMcoList"] as PolicyXMcoList;
        //        if (!base.IsNew && (objList as IDataModel).IsStale)
        //            objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
        //        return objList;
        //    }
        //}
        public PolicyXCvgTypeListStgng PolicyXCvgTypeListStgng
		{
			get
			{
                PolicyXCvgTypeListStgng objList = base.m_Children["PolicyXCvgTypeListStgng"] as PolicyXCvgTypeListStgng;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}
        // Mihika MITS 8656 - Multiple Insurers
		public PolicyXInsurerListStgng PolicyXInsurerListStgng
		{
			get
			{
                PolicyXInsurerListStgng objList = base.m_Children["PolicyXInsurerListStgng"] as PolicyXInsurerListStgng;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}
        public PolicyXUnitListStgng PolicyXUnitListStgng
        {
            get
            {
                PolicyXUnitListStgng objList = base.m_Children["PolicyXUnitListStgng"] as PolicyXUnitListStgng;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
        public PolicyXEntityListStgng PolicyXEntityListStgng
        {
            get
            {
                PolicyXEntityListStgng objList = base.m_Children["PolicyXEntityListStgng"] as PolicyXEntityListStgng;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }
		//BSB 02.25.2006 Added to simplify applying payments to coverages in funds.
		public PolicyXCvgTypeStgng GetCvgByTypeCode(int CvgTypeCode)
		{
			int itemKey = Context.DbConnLookup.ExecuteInt(String.Format("SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE_STGNG WHERE POLICY_ID={0} AND COVERAGE_TYPE_CODE={1}",
				PolicyId,CvgTypeCode));
			return(PolicyXCvgTypeListStgng[itemKey]);
		}
		#endregion


		internal PolicyStgng(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "POLICY_STGNG";
			this.m_sKeyField = "POLICY_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}

        //#region Supplemental Fields Exposed
        ////Supplementals are implemented in the base class but not exposed externally.
        ////This allows the derived class to control whether the object appears to have
        ////supplementals or not. 
        ////Entity exposes Supplementals.
        //public new Supplementals Supplementals
        //{	
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        //#endregion
    }
}
