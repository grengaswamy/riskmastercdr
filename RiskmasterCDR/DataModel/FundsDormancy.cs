﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Author: Rupal Kotak
    /// Date Created : 5 Sep 2011
    /// Desc : Defined dormany period after which the fund can be declared as unclaimed or escheat
    /// </summary>
    /// 

    [Riskmaster.DataModel.Summary("FUNDS_DORMANCY", "DORMANCY_ROW_ID", "CtlNumber")]
    public class FundsDormancy : DataObject
    {
        #region Database Feild List
        private string[,] sFields = {
														{"DormancyRowId", "DORMANCY_ROW_ID"},
														{"JurisdictionId", "JURISDICTION_ID"},
														{"UnclaimedDays", "UNCLAIMED_DAYS"},
														{"EscheatDays", "ESCHEAT_DAYS"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"AddedByUser", "ADDED_BY_USER"},
		};

        public string DormancyRowId { get { return GetFieldString("DORMANCY_ROW_ID"); } set { SetField("DORMANCY_ROW_ID", value); } }
        public int jurisdictionId { get { return GetFieldInt("JURISDICTION_ID"); } set { SetField("JURISDICTION_ID", value); } }
        public string UnclaimedDays { get { return GetFieldString("UNCLAIMED_DAYS"); } set { SetField("UNCLAIMED_DAYS", value); } }
        public string EscheatDays { get { return GetFieldString("ESCHEAT_DAYS"); } set { SetField("ESCHEAT_DAYS", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }

        #endregion

        new private void Initialize()
        {
            this.m_sTableName = "FUNDS_DORMANCY";
            this.m_sKeyField = "DORMANCY_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);            
            this.m_sParentClassName = "";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
        }

        internal FundsDormancy(bool isLocked, Context context) : base(isLocked, context)
		{
			this.Initialize();
		}
        
    }
}
