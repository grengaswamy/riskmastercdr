﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class FundsAutoXPayeeList : DataCollection
    {

        internal FundsAutoXPayeeList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "PAYEE_ROW_ID";
            this.SQLFromTable = "FUNDS_AUTO_X_PAYEE";

            this.TypeName = "FundsAutoXPayee";
        }
        public new FundsAutoXPayee this[int keyValue] { get { return base[keyValue] as FundsAutoXPayee; } }
        public new FundsAutoXPayee AddNew() { return base.AddNew() as FundsAutoXPayee; }
        public FundsAutoXPayee Add(FundsAutoXPayee obj) { return base.Add(obj) as FundsAutoXPayee; }
        public new FundsAutoXPayee Add(int keyValue) { return base.Add(keyValue) as FundsAutoXPayee; }


        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }

}