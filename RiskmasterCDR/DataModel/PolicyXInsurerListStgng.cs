﻿using System;

namespace Riskmaster.DataModel
{
    public class PolicyXInsurerListStgng:DataCollection 
    {
        internal PolicyXInsurerListStgng(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"IN_ROW_ID";
            this.SQLFromTable = "POLICY_X_INSURER_STGNG";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXInsurerStgng";
		}
        public new PolicyXInsurerStgng this[int keyValue] { get { return base[keyValue] as PolicyXInsurerStgng; } }
        public new PolicyXInsurerStgng AddNew() { return base.AddNew() as PolicyXInsurerStgng; }
        public PolicyXInsurerStgng Add(PolicyXInsurer obj) { return base.Add(obj) as PolicyXInsurerStgng; }
        public new PolicyXInsurerStgng Add(int keyValue) { return base.Add(keyValue) as PolicyXInsurerStgng; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
