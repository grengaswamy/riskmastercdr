using System;

namespace Riskmaster.DataModel
{
	/// <summary>
    /// Summary description for LeaveList
	/// </summary>
	public class LeaveList : DataCollection
	{
		internal LeaveList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"LEAVE_ROW_ID";
			this.SQLFromTable =	"CLAIM_LEAVE";			
			this.TypeName = "Leave";
		}
		public new Leave this[int keyValue]{get{return base[keyValue] as Leave;}}
		public new Leave AddNew(){return base.AddNew() as Leave;}
		public  Leave Add(Leave obj){return base.Add(obj) as Leave;}
		public new Leave Add(int keyValue){return base.Add(keyValue) as Leave;}		
		
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}