
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXInvoiceList.
	/// </summary>
	public class BillXInvoiceList : DataCollection
	{
		internal BillXInvoiceList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"";
			this.SQLFromTable =	"BILL_X_INVOICE";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXInvoice";
		}
		public new BillXInvoice this[int keyValue]{get{return base[keyValue] as BillXInvoice;}}
		public new BillXInvoice AddNew(){return base.AddNew() as BillXInvoice;}
		public  BillXInvoice Add(BillXInvoice obj){return base.Add(obj) as BillXInvoice;}
		public new BillXInvoice Add(int keyValue){return base.Add(keyValue) as BillXInvoice;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}