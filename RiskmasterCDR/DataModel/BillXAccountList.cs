
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXDsbrsmntList.
	/// </summary>
	public class BillXAccountList : DataCollection
	{
		internal BillXAccountList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"BILL_ACCOUNT_ROWID";
			this.SQLFromTable =	"BILL_X_ACCOUNT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXAccount";
		}
		public new BillXAccount this[int keyValue]{get{return base[keyValue] as BillXAccount;}}
		public new BillXAccount AddNew(){return base.AddNew() as BillXAccount;}
		public  BillXAccount Add(BillXAccount obj){return base.Add(obj) as BillXAccount;}
		public new BillXAccount Add(int keyValue){return base.Add(keyValue) as BillXAccount;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}