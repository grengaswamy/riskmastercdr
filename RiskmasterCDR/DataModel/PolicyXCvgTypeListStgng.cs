﻿using System;

namespace Riskmaster.DataModel
{
    public class PolicyXCvgTypeListStgng:DataCollection
    {
        internal PolicyXCvgTypeListStgng(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POLCVG_ROW_ID";
            this.SQLFromTable = "POLICY_X_CVG_TYPE_STGNG";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "PolicyXCvgTypeStgng";
		}
        public new PolicyXCvgTypeStgng this[int keyValue] { get { return base[keyValue] as PolicyXCvgTypeStgng; } }
        public new PolicyXCvgTypeStgng AddNew() { return base.AddNew() as PolicyXCvgTypeStgng; }
        public PolicyXCvgTypeStgng Add(PolicyXCvgType obj) { return base.Add(obj) as PolicyXCvgTypeStgng; }
        public new PolicyXCvgTypeStgng Add(int keyValue) { return base.Add(keyValue) as PolicyXCvgTypeStgng; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
