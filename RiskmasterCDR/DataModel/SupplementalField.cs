using System;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Xml;
using System.Dynamic;
using System.Xml.Linq;
//Created: 05.10.2004
//Author: BSB
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for SupplementalField.
	/// </summary>
	public class SupplementalField : DataRoot, IDataModel,IComparable
	{
		internal  SupplementalField(bool isLocked, Context context) : base(isLocked, context){Initialize();}
		private void Initialize()
		{
			//Nothing to do here.
		}

        //Mona:MITS 22993: Supp fields do not line-up correctly in PowerView for Fund_trans_split 
        private static string m_suppTableName;
        public static string SuppTableName
        {
            get;
            set;
        }


		//True allow other properties to be changed,
		// False only Value can be changed
		//True is used inside parent objects to load this object or in case
		//when this object in designed by user
		private bool m_DesignMode ;
		private int m_FieldId ;//FIELD_ID
		private string m_Caption ;//USER_PROMPT
		
		//-- ABhateja, 08.08.2006, MITS 7627
		//-- Type of Supp. field i.e. ACORD or normal supp field.
		private string m_SuppType ;
		private string m_FieldName ;// SYS_FIELD_NAME
		private SupplementalFieldTypes m_FieldType ;//FIELD_TYPE
		private int m_FieldSize ;//FIELD_SIZE
		private bool m_Required  ;//REQUIRED_FLAG
		private bool m_Lookup  ;//LOOKUP_FLAG
		private string m_Format  ;//PATTERN
		private int m_CodeFileId  ;// CODE_FILE_ID
		private string m_AssocField="" ;//ASSOC_FIELD
		private bool m_GrpAssocFlag ;//GRP_ASSOC_FLAG
		private bool m_Visible; // If Field is associated field we need to hide/show it
		private object m_Value;  // Field value from Supplemental table
		private int m_SeqNum;  // Sequence Number from Supp Dictionary
		private DataSimpleList m_Values;   // JP 4/30/2002   Support for true-multicode,state,entity
		private string m_AssocCodes="";
		private bool m_NetVisibleFlag  ;//RISKMASTER.Net visibility flag
		private const string ACDELIMITER  = "|";
        public XmlDocument SuppHTMLText; //asharma326 JIRA 6422
        public XmlDocument gridXML;//Added by Shivendu for supplemental grid
        public string minRows;//Added by Shivendu for supplemental grid
        public string maxRows;//Added by Shivendu for supplemental grid
        //sharishkumar Jira 6415
        private int m_multiselect;
        private int m_usergroups;
        //sharishkumar Jira 6415 ends
        private int m_WebLinkId;//WWIG GAP20A - agupta298 - MITS 36804 - WEB_LINK_ID - JIRA - 4691
		internal SupplementalField CopyTo(SupplementalField objSupp)
		{
			objSupp.DesignMode = true;
			objSupp.AssocField = this.AssocField;
			//-- ABhateja, 08.08.2006, MITS 7627
			//-- AssocCodes required for Group Association implementation.
			objSupp.AssocCodes = this.AssocCodes;
            objSupp.HTMLContents = this.HTMLContents;
			objSupp.Caption= this.Caption;
			objSupp.CodeFileId= this.CodeFileId;
			objSupp.SetFieldId(this.FieldId);
			objSupp.FieldName= this.FieldName;
			objSupp.FieldSize = this.FieldSize;
			objSupp.FieldType= this.FieldType;
			objSupp.Format= this.Format;
			objSupp.Lookup= this.Lookup;
			objSupp.Required= this.Required;
			objSupp.SetGrpAssocFlag(this.GrpAssocFlag);
			objSupp.NetVisibleFlag= (this.NetVisibleFlag);
			objSupp.SeqNum = (this.SeqNum);
            objSupp.WebLinkId = this.WebLinkId;//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
			objSupp.DesignMode = false;
			objSupp.DataChanged = false;
			objSupp.m_AssocCodes = this.m_AssocCodes;
            objSupp.m_ViewXML = this.m_ViewXML;
            //sharishkumar Jira 6415
            objSupp.m_usergroups = this.UserGroups;
            objSupp.m_multiselect = this.Multiselect;
            //sharishkumar Jira 6415 ends
            objSupp.IsHtmlConfigurable = this.IsHtmlConfigurable;

			if(	!objSupp.IsKeyValue)
				objSupp.Visible = true;
			return objSupp;
			
		}
		//BSB Note: SetAttribute() will substitute "&amp;" for "&" in the value string automatically.
		// It will however leave "'" alone which is valid for xml but causes us trouble later in the 
		// javascript rendered to the html control where "'" is used as a string delimiter.
		// For now, just swap this value out (w/o swapping any others that the dom objects will do automatically.)
        private XmlDocument m_ViewXML = null; 
		public XmlDocument ViewXML
		{
			get
			{
                //BSB 11.14.2007 Cache the view XML for performance.
                if (m_ViewXML != null)
                    return m_ViewXML;
				XmlDocument objXML = new XmlDocument();
				XmlElement objXMLElem = objXML.CreateElement("control");
                string sSql = string.Empty;
                int iGlossarytype = 0;
				
				string sGroupAssoc = "";


				if (m_AssocField != "" && m_AssocCodes != "")
					sGroupAssoc = string.Concat(m_AssocField , ":" , m_AssocCodes);

				//Set properties common to all SuppTypes
				
				//-- ABhateja, 08.08.2006, MITS 7627
				//-- All supplemental fields should start with "supp_".This is required 
				//-- in order to distinguish them from ACORD fields.The logic is being
				//-- used for Group Association in JScript just like RMNet.
                if (m_SuppType == "suppgroup")
                {
                    objXMLElem.SetAttribute("name", "supp_" + FieldName.ToLower());
                    objXMLElem.SetAttribute("fieldtype", "supplementalfield");
                }
                else
                    objXMLElem.SetAttribute("name", FieldName.ToLower());
						
				objXMLElem.SetAttribute("title", Caption);

				//-- ABhateja, 08.08.2006, MITS 7627
				if (sGroupAssoc != "")
					objXMLElem.SetAttribute("GroupAssoc", sGroupAssoc);
                
                //TabIndex is crashing whenever supplementals assign large integers
                //06/08/2009 :Raman Bhatia
                
                //objXMLElem.SetAttribute("tabindex", SeqNum.ToString());
                //while (SeqNum > 32767)
                //{
                //   m_SeqNum = m_SeqNum - 32767;
                //}
                
                //rsolanki2: modulus oepration.
                m_SeqNum = m_SeqNum % 32767;

                objXMLElem.SetAttribute("tabindex", SeqNum.ToString());
                //Mona:MITS 22993: Supp fields do not line-up correctly in PowerView for Fund_trans_split 
                if (SuppTableName != "FUNDS_TRANS_SPLIT_SUPP")
                {
                    //Raman: MITS 17476 and 17479
                    objXMLElem.SetAttribute("class", "half");
                }
                else
                {
                    objXMLElem.SetAttribute("class", "popuprow");
                }
                
				// JP 11.08.2005   objXMLElem.SetAttribute("ref", "/Instance/*/Supplementals/" + this.FieldName.ToUpper());
				objXMLElem.SetAttribute("ref", string.Concat("/Instance/*/Supplementals/" , XmlConvert.EncodeName(this.FieldName.ToUpper())));
				if(Required)
					objXMLElem.SetAttribute("required", "yes");
				
				switch (FieldType)
				{
					case SupplementalFieldTypes.SuppTypePrimaryKey:
					case SupplementalFieldTypes.SuppTypeSecondaryKey:
						objXMLElem.SetAttribute("type", "id");
						break;
					case SupplementalFieldTypes.SuppTypeCode:
						objXMLElem.SetAttribute("type", "code");
						objXMLElem.SetAttribute("codetable", Context.LocalCache.GetTableName(CodeFileId).Replace("'","\\'"));
						break;
					case SupplementalFieldTypes.SuppTypeCurrency:
						objXMLElem.SetAttribute("type", "currency");
						break;
					case SupplementalFieldTypes.SuppTypeDate:
						objXMLElem.SetAttribute("type", "date");
						break;
					case SupplementalFieldTypes.SuppTypeEntity:
						//Check to see if the entity table is an orgh table.
                        if (Conversion.EntityTableIdToOrgTableName(this.CodeFileId) != "")
                        {
                            //pmahli MITS 10982 1/17/2008 - Start
                            //Added a new attribute in case of supplemental Org Hierarchy
                            string sTableName = string.Empty;
                            using (Riskmaster.Common.LocalCache objLocalCache = new LocalCache(Context.DbConn.ConnectionString,Context.ClientId))
                            sTableName = objLocalCache.GetTableName(this.CodeFileId);
                            objXMLElem.SetAttribute("SupTitle", sTableName);
                            //pmahli MITS 10982 1/17/2008 - End
                            objXMLElem.SetAttribute("type", "orgh");
                            // akaushik5 Added for MITS 37798 Starts
                            objXMLElem.SetAttribute("orglevel", sTableName);
                            // akaushik5 Added for MITS 37798 Ends
                        }
						else
							objXMLElem.SetAttribute("type", "eidlookup");
                       
						objXMLElem.SetAttribute("tableid", Context.LocalCache.GetTableName(CodeFileId).Replace("'","\\'"));
						 //Start by Shivendu for MITS 14619
                        const string EMPLOYEE = "EMPLOYEE";
                        const string EMPLOYEES = "EMPLOYEES";
                        if (objXMLElem.Attributes["type"].Value.CompareTo("eidlookup") == 0 && objXMLElem.Attributes["tableid"].Value.CompareTo(EMPLOYEES) == 0)
                        {
                            objXMLElem.Attributes["tableid"].Value = EMPLOYEE;
                            objXMLElem.SetAttribute("viewid", "-1");
                        }

                        //End by Shivendu for MITS 14619
                        //spahariya MITS 30426 - start 12/06/2012
                        objXMLElem.SetAttribute("entityNev", "1");
                        iGlossarytype = 0;
                        iGlossarytype = Context.LocalCache.GetGlossaryType(CodeFileId);
                        if(iGlossarytype == 4)
                            objXMLElem.SetAttribute("entitytype", "entitymaint");
                        else if (iGlossarytype == 7)
                            objXMLElem.SetAttribute("entitytype", "people");
                        //spahariya MITS 30426 - End
                        break;
					case SupplementalFieldTypes.SuppTypeFreeText:
						objXMLElem.SetAttribute("type", "textml");
						//objXMLElem.SetAttribute("cols", TextMlCols); 'tkr 7/2003
						//objXMLElem.SetAttribute("rows", TextMlRows);
						break;
					case SupplementalFieldTypes.SuppTypeMemo:
						if(CodeFileId != 0)
						{    
							objXMLElem.SetAttribute("type", "freecode");
							//objXMLElem.SetAttribute("cols", FreecodeCols);  'tkr 7/2003
							//objXMLElem.SetAttribute("rows", FreecodeRows);
							objXMLElem.SetAttribute("codetable", Context.LocalCache.GetTableName(CodeFileId).Replace("'","\\'"));
						}						
						else
						{
							objXMLElem.SetAttribute("type", "textml");
							//objXMLElem.SetAttribute("cols", TextMlCols  'tkr 7/2003
							//objXMLElem.SetAttribute("rows", TextMlRows
						}
						break;
					case SupplementalFieldTypes.SuppTypeNumber:
						objXMLElem.SetAttribute("type", "numeric");
						break;
					case SupplementalFieldTypes.SuppTypeState:
						objXMLElem.SetAttribute("type", "code");
						objXMLElem.SetAttribute("codetable", "states");
						break;
					case SupplementalFieldTypes.SuppTypeText:
						if(Format.Trim() == "###-##-####")
							objXMLElem.SetAttribute("type", "ssn");
						else if(Format.Trim()== "(###) ###-####   Ext. #####" ||
							Format.Trim()== "(###) ###-####")
							objXMLElem.SetAttribute("type", "phone");
						else if(Format.Trim()== "#####-####")
							objXMLElem.SetAttribute("type", "zip");
						else
						{
							objXMLElem.SetAttribute("type", "text");
							// vaibhav : has added the maxlength attribute to take care the fieldsize.
							objXMLElem.SetAttribute( "maxlength" , FieldSize.ToString() );
						}
						break;
					case SupplementalFieldTypes.SuppTypeTime:
						objXMLElem.SetAttribute("type", "time");
						break;
					case SupplementalFieldTypes.SuppTypeClaimLookup:
						objXMLElem.SetAttribute("type", "claimnumberlookup");
						break;
					case SupplementalFieldTypes.SuppTypeEventLookup:
						objXMLElem.SetAttribute("type", "eventnumberlookup");
						break;
					case SupplementalFieldTypes.SuppTypeVehicleLookup:
						objXMLElem.SetAttribute("type", "SuppTypeVehicleLookup");
						break;
                    //Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                    //Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618
					//case SupplementalFieldTypes.SuppTypePolicyLookup:
                    //  objXMLElem.SetAttribute("type", "SuppTypePolicyLookup");
					case SupplementalFieldTypes.SuppTypePolicyTrackingLookup:
                        objXMLElem.SetAttribute("type", "SuppTypePolicyTrackingLookup");
                        break;
                    case SupplementalFieldTypes.SuppTypePolicyManagementLookup:
                        objXMLElem.SetAttribute("type", "SuppTypePolicyManagementLookup");
                        break;
                    //End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
					case SupplementalFieldTypes.SuppTypeMultiCode:
						objXMLElem.SetAttribute("type", "codelist");
						objXMLElem.SetAttribute("codetable", Context.LocalCache.GetTableName(CodeFileId).Replace("'","\\'"));
						break;
					case SupplementalFieldTypes.SuppTypeMultiState:
						objXMLElem.SetAttribute("type", "codelist");
						objXMLElem.SetAttribute("codetable", Context.LocalCache.GetTableName(Context.LocalCache.GetTableId("STATES")).Replace("'","\\'"));
						break;
					case SupplementalFieldTypes.SuppTypeMultiEntity:
						objXMLElem.SetAttribute("type", "entitylist");
						objXMLElem.SetAttribute("tableid", CodeFileId.ToString());
                        //Start by Shivendu for MITS 14619
                        const string EMPLOYEE_CODE = "1060";
                        if (objXMLElem.Attributes["tableid"].Value.CompareTo(EMPLOYEE_CODE) == 0)
                        {
                            objXMLElem.Attributes["tableid"].Value = EMPLOYEE;
                            objXMLElem.SetAttribute("viewid", "-1");
                        }

                        //End by Shivendu for MITS 14619
                        //spahariya MITS 30426 - start 12/06/2012
                        objXMLElem.SetAttribute("entityNev", "1");
                        iGlossarytype = 0;
                        iGlossarytype = Context.LocalCache.GetGlossaryType(CodeFileId);
                        if (iGlossarytype == 4)
                            objXMLElem.SetAttribute("entitytype", "entitymaint");
                        else if (iGlossarytype == 7)
                            objXMLElem.SetAttribute("entitytype", "people");
                        //spahariya MITS 30426 - End
						break;
                        //Added for Supplemental Grid by Shivendu
                    case SupplementalFieldTypes.SuppTypeGrid:
                        string xmlID = string.Concat("supp_" , FieldName.ToLower().Trim() , "_value");
                        objXMLElem.SetAttribute("type", "ZapatecGrid");
                        if(gridXML!=null)
                            objXMLElem.SetAttribute("gridXML", gridXML.InnerXml);
                        objXMLElem.SetAttribute("valueXML", xmlID);
                        objXMLElem.SetAttribute("minRows", minRows);
                        objXMLElem.SetAttribute("maxRows", maxRows);
                        break;
                    case SupplementalFieldTypes.SuppTypeCheckBox://MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                        objXMLElem.SetAttribute("type", "checkbox");
                        break;
                    //sharishkumar Jira 6415 starts
                    case SupplementalFieldTypes.SuppTypeUserLookup:
                        objXMLElem.SetAttribute("type", "UserLookup");
                        objXMLElem.SetAttribute("multiselect", Multiselect.ToString());
                        objXMLElem.SetAttribute("usergroups", UserGroups.ToString());
                        break;
                    //sharishkumar Jira 6415 ends
					 case SupplementalFieldTypes.SuppTypeHTML://asharma326 JIRA 6422
                        objXMLElem.SetAttribute("type", "htmltext");
                        if(this.IsHtmlConfigurable)
                            objXMLElem.SetAttribute("fieldtype", "configurablesupphtmlfield");
                        else
                            objXMLElem.SetAttribute("fieldtype", "supphtmlfield");
                        break;
                     //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                     case SupplementalFieldTypes.SuppTypeHyperlink:
                        objXMLElem.SetAttribute("type", "SuppTypeHyperlink");
                        if (WebLinkId != 0)
                        {
                            string[] arrData = Context.LocalCache.GetHyperlinkByWebLinkId(WebLinkId);
                            objXMLElem.SetAttribute("Text", arrData[0]);//For the HyperLink text which is retrieved from SYS_SETTING table

                            string[] arrURLData = Context.LocalCache.GetWebLinkDataBySuppFieldId(FieldId);
                            objXMLElem.SetAttribute("NavigateURL", arrURLData[1]); //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                        }
                        break;
                     //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
					default:
						objXMLElem.SetAttribute("type", "text");
						// vaibhav : has added the maxlength attribute to take care the fieldsize.
						objXMLElem.SetAttribute( "maxlength" , FieldSize.ToString() );
						break;
				}
				objXML.AppendChild(objXMLElem);
                m_ViewXML = objXML;
				return objXML;
			}
		}
        public bool IsHtmlConfigurable { get; set; }
        private string _HTMLContents = string.Empty;
        // Property made public so that projects outside datamodel can also use this property
        public string HTMLContents
        {
            get { return _HTMLContents; }
            set
            {
                if (value != _HTMLContents)
                {
                    _HTMLContents = value;
                    base.DataChanged = true;
                }
            }
        }
        
        //asharma326 JIRA# 6422 Ends
		internal string SerializeObject()
		{
            var IsHTMLContent = false;
			XmlDocument dom = new XmlDocument();
			XmlElement xmlElem = null;
			ExtendedTypeAttribute ext =null;
			// JP 11.08.2005   dom.LoadXml(base.SerializeScalar(ext,this.FieldName,this.Value));
			string sEncodedXmlFieldName = XmlConvert.EncodeName(this.FieldName.ToUpper());

			switch(this.FieldType)
			{
				case	SupplementalFieldTypes.SuppTypeCode:  
				case  SupplementalFieldTypes.SuppTypeState:
					ext = new ExtendedTypeAttribute(RMExtType.Code, Context.LocalCache.GetTableName(this.CodeFileId));
					//Log.Write(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Value));
					dom.LoadXml(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Value));
					xmlElem = dom.DocumentElement;
					break;
				case(SupplementalFieldTypes.SuppTypeEntity):
					//Check to see if the entity table is an orgh table.
					if(Conversion.EntityTableIdToOrgTableName(this.CodeFileId)!="")
						ext = new ExtendedTypeAttribute(RMExtType.OrgH, Context.LocalCache.GetTableName(this.CodeFileId));
					else
						ext = new ExtendedTypeAttribute(RMExtType.Entity, Context.LocalCache.GetTableName(this.CodeFileId));

					dom.LoadXml(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Value));
					xmlElem = dom.DocumentElement;
					break;
				case SupplementalFieldTypes.SuppTypeMultiCode:
				case SupplementalFieldTypes.SuppTypeMultiState:                
                ext = new ExtendedTypeAttribute(RMExtType.CodeList, Context.LocalCache.GetTableName(this.CodeFileId));
					dom.LoadXml(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Values));
					xmlElem = dom.DocumentElement;
					break;
				case(SupplementalFieldTypes.SuppTypeMultiEntity):
					//Check to see if the entity table is an orgh table.
//					if(Conversion.EntityTableIdToOrgTableName(this.CodeFileId)!="")
//						ext = new ExtendedTypeAttribute(RMExtType.OrgH, Context.LocalCache.GetTableName(this.CodeFileId));
//					else
						ext = new ExtendedTypeAttribute(RMExtType.EntityList, Context.LocalCache.GetTableName(this.CodeFileId));
					dom.LoadXml(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Values));
					xmlElem = dom.DocumentElement;
					break;
                //npadhy Jira 6415 Handle the User / Group Lookup. Retreive the Proper Values from DB
                case SupplementalFieldTypes.SuppTypeUserLookup:
                    ext = new ExtendedTypeAttribute(RMExtType.SysUser, this.UserGroups == 0 ? "RM_SYS_USERS" : "RM_SYS_GROUPS", this.Multiselect == -1 ? true: false);
                    dom.LoadXml(base.SerializeScalar(ext, sEncodedXmlFieldName, this.Values));
                    xmlElem = dom.DocumentElement;
                    break;
				//asharma326 JIRA# 6422 Starts 
				case (SupplementalFieldTypes.SuppTypeHTML):
                    
                    IsHTMLContent = true;
                    //if (!string.IsNullOrEmpty(this.Value))
                    //{
                        dom.LoadXml(base.SerializeScalar(null, sEncodedXmlFieldName, this.Value));
                        xmlElem = dom.DocumentElement;
                    //}
                    //else
                    //{
                    //    dom.LoadXml(base.SerializeScalar(null, sEncodedXmlFieldName,string.Empty));
                    //    xmlElem = dom.DocumentElement;
                    //}
                    break;
                //sharishkumar Jira 6415 - end
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case (SupplementalFieldTypes.SuppTypeHyperlink):

                    string[] arrData = Context.LocalCache.GetHyperlinkByWebLinkId(this.WebLinkId);
                    if (this.Value == null)
                        this.Value = arrData[1];

                    ext = new ExtendedTypeAttribute(RMExtType.Hyperlink, "");//need to add weblinkid property and get its value from DB
                    dom.LoadXml(base.SerializeScalar(ext, sEncodedXmlFieldName, this.Value));
                    xmlElem = dom.DocumentElement;

                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
				default:
					ext = null;
					dom.LoadXml(base.SerializeScalar(ext,sEncodedXmlFieldName,this.Value));
					xmlElem = dom.DocumentElement;
					break;
			}
			xmlElem.SetAttribute("caption",this.Caption);
			xmlElem.SetAttribute("visible",this.Visible.ToString());
			xmlElem.SetAttribute("format",this.Format);
			xmlElem.SetAttribute("fieldtype",this.FieldType.ToString());
			xmlElem.SetAttribute("codefileid",this.CodeFileId.ToString());
			xmlElem.SetAttribute("assocfield",this.AssocField);
			//-- ABhateja, 08.08.2006, MITS 7627
			xmlElem.SetAttribute("assoccodes",this.AssocCodes);
			xmlElem.SetAttribute("grpassocflag",this.GrpAssocFlag.ToString());
			xmlElem.SetAttribute("fieldid",this.FieldId.ToString());
			xmlElem.SetAttribute("fieldsize",this.FieldSize.ToString());
            // npadhy Jira - RMA - 6415 - Need to support User/Group lookup
            // Get the values for User lookup
			if(this.IsMultiValue || this.IsUserLookup)
				xmlElem.SetAttribute("codeid",this.Values.ToString());
            //asharma326 JIRA 6422
            xmlElem.SetAttribute("weblinkid", this.WebLinkId.ToString()); //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            if (IsHTMLContent)
            {
                XElement Xele = XElement.Parse(xmlElem.OuterXml);
                //if (!string.IsNullOrEmpty(this.HTMLContents))
                //{
                    Xele.Value = this.HTMLContents;
                    Xele.Name = Xele.Name + "_HTMLComments";
                //}
                //else
                //{
                //    Xele.Value = string.Empty;
                //    Xele.Name = Xele.Name + "_HTMLComments";
                //}
                return xmlElem.OuterXml + Convert.ToString(Xele);
            }
            else
            {
                return xmlElem.OuterXml;
            }
		}
		public  void AddAssocCode(int l){m_AssocCodes += ACDELIMITER + l + ACDELIMITER;}
		public void ClearAssocCodes(){m_AssocCodes = "";}
		public bool IsAssocCode(int l)
		{
			string  s = ACDELIMITER + l + ACDELIMITER;
			return (m_AssocCodes.IndexOf(s) >= 0);
		}
		public void RemoveAssocCode(int l )
		{
			string 	s = ACDELIMITER + l + ACDELIMITER;
			m_AssocCodes.Replace(s,"");
		}
		public int FieldId{get{return m_FieldId;}}
		internal void SetFieldId(int Value)
		{
			if(m_DesignMode)
			{
				m_FieldId = Value;
				base.DataChanged = true;
			}
			else
                throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
		}
		public string Caption
		{
			get{return m_Caption;}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_Caption)
					{
						m_Caption = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}
		}

		public string FieldName
		{
			get{return m_FieldName;}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_FieldName)
					{
						m_FieldName = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}
		}
		public SupplementalFieldTypes FieldType
		{
			get
			{
				return m_FieldType;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_FieldType)
					{
						m_FieldType = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}
		public int FieldSize
		{
			get
			{
				return m_FieldSize;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_FieldSize)
					{
						m_FieldSize = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}			

		public int SeqNum
		{
			get
			{
				return m_SeqNum;
			}
			set
			{
				if(m_DesignMode)
				{
					m_SeqNum = value;
					base.DataChanged = true;
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}
		}

        //sharishkumar Jira 6415 starts
        public int Multiselect
        {
            get
            {
                return m_multiselect;
            }
            set
            {
                if (m_DesignMode)
                {
                    if (value != m_multiselect)
                    {
                        m_multiselect = value;
                        base.DataChanged = true;
                    }
                }
                else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
            }
        }

        public int UserGroups
        {
            get
            {
                return m_usergroups;
            }
            set
            {
                if (m_DesignMode)
                {
                    if (value != m_usergroups)
                    {
                        m_usergroups = value;
                        base.DataChanged = true;
                    }
                }
                else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
            }
        }
        //sharishkumar Jira 6415 ends

		public bool GrpAssocFlag{get{return m_GrpAssocFlag;}}
		internal void SetGrpAssocFlag(bool Value){m_GrpAssocFlag = Value;}
		public bool Required
		{
			get
			{
				return m_Required;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_Required)
					{
						m_Required = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}			
		public bool Lookup
		{
			get
			{
				return m_Lookup;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_Lookup)
					{
						m_Lookup = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}			

		public string Format
		{
			get
			{
				return m_Format;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_Format)
					{
						m_Format = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}			
		public object Value
		{
			get
			{
				if(this.IsKeyValue ||
					FieldType == SupplementalFieldTypes.SuppTypeCode ||
					FieldType == SupplementalFieldTypes.SuppTypeEntity ||
					FieldType == SupplementalFieldTypes.SuppTypeState)
                    try { return (int)Conversion.ConvertObjToInt(m_Value, this.Context.ClientId); }
					 catch{return 0;}
				else if (FieldType == SupplementalFieldTypes.SuppTypeNumber ||
						 FieldType == SupplementalFieldTypes.SuppTypeCurrency)
                    try { return (double)Conversion.ConvertObjToDouble(m_Value, this.Context.ClientId); }
					catch{return (double) 0.0;}
				return m_Value;
			}
			set
			{
				object oTmp = null;

				
				//BSB Implement Text Trimming.
				if(this.FieldType== SupplementalFieldTypes.SuppTypeText)
					if(value!=null && value.ToString().Length >this.FieldSize)
						value=value.ToString().Substring(0,this.FieldSize);

				// Conversion to the right type
				if(this.FieldType== SupplementalFieldTypes.SuppTypeDate)
					oTmp = Conversion.GetDate(value==null ? "":value.ToString());
				else if(this.FieldType == SupplementalFieldTypes.SuppTypeTime)
					oTmp = Conversion.GetTime(value==null ? "":value.ToString());
				else if(	FieldType == SupplementalFieldTypes.SuppTypeCode ||
					FieldType == SupplementalFieldTypes.SuppTypeEntity ||
					FieldType == SupplementalFieldTypes.SuppTypeState)
                    // akaushik5 Changed for MITS 35846 Starts
                    //try{oTmp =  Int32.Parse(value==null ? "0":value.ToString());}
                    try { oTmp = Int32.Parse(value == null || string.IsNullOrEmpty(value.ToString()) ? "0" : value.ToString()); }
                    // akaushik5 Changed for MITS 35846 Ends
					catch{}
				else if(this.FieldType == SupplementalFieldTypes.SuppTypeCurrency ||
						this.FieldType == SupplementalFieldTypes.SuppTypeNumber)
					try{oTmp =  Conversion.ConvertStrToDouble(value==null ? "0":value.ToString());}
					catch{}

				else
					oTmp = value;

				if((oTmp==null ? "":oTmp.ToString()) != (m_Value==null ? "" :m_Value.ToString()))
				{
					m_Value = oTmp;
					base.DataChanged = true;
				}
			}			
		}			


		// JP 4/30/2002   BEGIN    Support for true-multi valued supp fields (code,state,entity)
		public DataSimpleList Values
		{
			get
			{
				if (m_Values == null)
					m_Values = Context.Factory.GetDataModelObject("DataSimpleList",true) as DataSimpleList;     ;//' we defer creation of the multi-valued object so we don't add overhead to normal supp fields
				return m_Values;
			}
		} 
		
		override public bool DataChanged
		{
			get
			{
				if(m_Values ==null)
					return base.DataChanged;
				else                            
					return base.DataChanged || m_Values.DataChanged;
			}
			set
			{
				base.DataChanged = value;
				if(m_Values !=null)
					m_Values.DataChanged = value;
			}
		}
		internal override void LockDataChangedFlag()
		{
			base.LockDataChangedFlag ();
			if(m_Values !=null)
				m_Values.LockDataChangedFlag();
		}
		internal override void UnlockDataChangedFlag()
		{
			base.UnlockDataChangedFlag ();
			if(m_Values !=null)
				m_Values.LockDataChangedFlag();
		}


//		internal void  SetDataChanged(bool Value)
//		{
//			m_DataChanged = Value;
//			if(m_Values !=null)
//				m_Values.DataChanged = Value;
//		}

		internal bool DesignMode
		{
			get{return m_DesignMode;}
			set{m_DesignMode = value;}
		}
		public int CodeFileId
		{
			get
			{
				return m_CodeFileId;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_CodeFileId)
					{
						m_CodeFileId = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}			

		public string AssocField
		{
			get
			{
				return m_AssocField;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_AssocField)
					{
						m_AssocField = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}
					
		//-- ABhateja, 08.08.2006, MITS 7627 -START-
		public string AssocCodes
		{
			get
			{
				return m_AssocCodes;
			}
			set
			{
				if(m_DesignMode)
				{
					if( value != m_AssocCodes)
					{
						m_AssocCodes = value;
						base.DataChanged = true;
					}
				}
				else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
			}			
		}					
		//-- ABhateja, 08.08.2006, MITS 7627 -END-
		public bool Visible
		{
			get
			{
				return m_Visible;
			}
			set
			{
					m_Visible = value;
			}
		}		
		public bool IsMultiValue
		{
			get
			{
				return (this.FieldType == SupplementalFieldTypes.SuppTypeMultiCode || 
					this.FieldType == SupplementalFieldTypes.SuppTypeMultiEntity ||
					this.FieldType == SupplementalFieldTypes.SuppTypeMultiState); 
					
			}
		}

        // npadhy Jirs 6415 - Need to support User/group Lookup
        public bool IsUserLookup
        {
            get
            {
                return (this.FieldType == SupplementalFieldTypes.SuppTypeUserLookup);
            }
        }
		//asharma326 JIRA# 6422 Starts 
        internal bool IsHTMLValue
        {
            get
            {
                return (this.FieldType == SupplementalFieldTypes.SuppTypeHTML);
            }
        }
        //asharma326 JIRA# 6422 Ends
        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
        public int WebLinkId
        {
            get
            {
                return m_WebLinkId;
            }
            set
            {
                if (m_DesignMode)
                {
                    if (value != m_WebLinkId)
                    {
                        m_WebLinkId = value;
                        base.DataChanged = true;
                    }
                }
                else
                    throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly")); //This property is currently read-only (Error 40076)
            }
        }
        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
		public bool IsKeyValue
		{
			get
			{
				return (this.FieldType == SupplementalFieldTypes.SuppTypePrimaryKey || 
					this.FieldType == SupplementalFieldTypes.SuppTypeSecondaryKey);
			}
		}

		internal bool IsAssociatedCode(DataObject parent)
		{
			return IsAssociatedCode(parent, "");
		}
		internal bool IsAssociatedCode(DataObject parent, string LOB)
		{
			string sClaimLOB = "";
			//Raman 08/26/2009 : MITS 17702
            //Adding null check
            if (this.AssocField.Split('.').Length < 2)
                return false;
            string sFieldName = this.AssocField.Split('.')[1];
			if(sFieldName.IndexOf("DEPT_EID(")>=0)
				sFieldName = "ENTITY_ID";

			if(parent==null)
				return false;

			if(LOB !="" && LOB !=null && !(parent is Claim)) //LOB filtering desired but no Claim available.
				return false;

			if(LOB != null && LOB !="")//Get Current Claim LOB for filtering.
				sClaimLOB = Context.LocalCache.GetShortCode(((parent as Claim).LineOfBusCode));

			if(LOB.IndexOf(sClaimLOB) <0) //LOB Filtering says the current claim is not of a proper LOB.
				return false;

			//Perform the Association Check (LOB Filter Passed or N/A)
			return this.IsAssocCode(parent.GetFieldInt(sFieldName));
		}

		public bool NetVisibleFlag
				{
					get
					{
						return m_NetVisibleFlag;
					}
					set
					{
						if(m_DesignMode)
						{
							if( value != m_NetVisibleFlag)
							{
								m_NetVisibleFlag = value;
								base.DataChanged = true;
							}
						}
						else
                            throw new ReadOnlyException(Globalization.GetString("SupplementalFieldExceptionReadOnly", this.Context.ClientId)); //This property is currently read-only (Error 40076)
					}			
		
		}	

		//-- ABhateja, 08.08.2006, MITS 7627 -START-
		public string SuppType
		{
			get
			{
				return m_SuppType;
			}
			set
			{
				m_SuppType = value;
			}
		}	
		//-- ABhateja, 08.08.2006, MITS 7627 -END-

		#region IComparable Members

		public int CompareTo(object obj)
		{
			SupplementalField objSupp = obj as SupplementalField;
			if(objSupp==null)
				return 0;

			if(this.SeqNum<objSupp.SeqNum)
				return -1;
			else if(this.SeqNum>objSupp.SeqNum)
				return 1;
			return 0;
		}

		#endregion
        //BSB Disconnect from Context.
        // This is used when this field instance is used in a type cache only.
        // Fixes a memory leak associated with the DM Context linkage from inside the 
        // SuppTableDefinition cache.
        internal void DeActivateForCache()
        {
            this.Context = null;//This is the only time you can set Context equal to anything and expect it to work.
            this.m_Parent = null;
            base.m_Parent = null;
        }
	}
}
