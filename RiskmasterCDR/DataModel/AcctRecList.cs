using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forAcctRecList.
	/// </summary>
	public class AcctRecList : DataCollection
	{
		internal AcctRecList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"AR_ROW_ID";
			this.SQLFromTable =	"ACCT_REC";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "AcctRec";
		}
		public new AcctRec this[int keyValue]{get{return base[keyValue] as AcctRec;}}
		public new AcctRec AddNew(){return base.AddNew() as AcctRec;}
		public  AcctRec Add(AcctRec obj){return base.Add(obj) as AcctRec;}
		public new AcctRec Add(int keyValue){return base.Add(keyValue) as AcctRec;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}