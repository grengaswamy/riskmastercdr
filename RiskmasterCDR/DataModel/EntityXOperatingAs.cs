
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXOperatingAs.
	/// </summary>
	//TODO: Complete Summary
	[Riskmaster.DataModel.Summary("ENT_X_OPERATINGAS","OPERATING_ID", "OperatingAs")]
	public class EntityXOperatingAs : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"OperatingId","OPERATING_ID"},
									   {"EntityId","ENTITY_ID"},
									   {"OperatingAs","OPERATING_AS"},
									   //{"Initial","INITIAL"}, MITS 9079
									   {"Initials","INITIALS"},
		};
		public int  OperatingId{get{  return GetFieldInt("OPERATING_ID");}set{ SetField("OPERATING_ID",value);}}
		public int EntityId{get{ return GetFieldInt("ENTITY_ID");}set{SetField("ENTITY_ID",value);}}
		public string OperatingAs{get{ return GetFieldString("OPERATING_AS");}set{SetField("OPERATING_AS",value);}}
		public string Initials{get{ return GetFieldString("INITIALS");}set{SetField("INITIALS",value);}}
		// MITS 9079
		//public string Initial{get{ return GetFieldString("INITIAL");}set{SetField("INITIAL",value);}}
		#endregion

		internal EntityXOperatingAs(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENT_X_OPERATINGAS";
			this.m_sKeyField = "OPERATING_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
