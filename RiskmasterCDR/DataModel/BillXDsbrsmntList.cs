
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXDsbrsmntList.
	/// </summary>
	public class BillXDsbrsmntList : DataCollection
	{
		internal BillXDsbrsmntList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"";
			this.SQLFromTable =	"BILL_X_DSBRSMNT";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXDsbrsmnt";
		}
		public new BillXDsbrsmnt this[int keyValue]{get{return base[keyValue] as BillXDsbrsmnt;}}
		public new BillXDsbrsmnt AddNew(){return base.AddNew() as BillXDsbrsmnt;}
		public  BillXDsbrsmnt Add(BillXDsbrsmnt obj){return base.Add(obj) as BillXDsbrsmnt;}
		public new BillXDsbrsmnt Add(int keyValue){return base.Add(keyValue) as BillXDsbrsmnt;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}