﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Collections;
using System.Data;
using Riskmaster.Db;


namespace Riskmaster.DataModel
{
   /// <summary>
    /// Summary description for EntityXRole.cs.
    /// </summary>
    //Created by skhare7:  Entity Role enhJIRA 340 MITS 35818
    [Riskmaster.DataModel.Summary("ENTITY_X_ROLES", "ER_ROW_ID")]
    public class EntityXRole : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
										{"ERRowId","ER_ROW_ID"},
										
										{"EntityId","ENTITY_ID"},
										{"EntityTableId","ENTITY_TABLE_ID"},
										//{"DeletedFlag","DELETED_FLAG"},
										
										{"DttmRcdAdded","DTTM_RCD_ADDED"},
										{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
										{"AddedByUser","ADDED_BY_USER"},
                                        {"UpdatedByUser","UPDATED_BY_USER"},
									};
        public int ERRowId { get { return GetFieldInt("ER_ROW_ID"); } set { SetField("ER_ROW_ID", value); } }

        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }

        public int EntityTableId { get { return GetFieldInt("ENTITY_TABLE_ID"); } set { SetField("ENTITY_TABLE_ID", value); } }

        //public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }

        #endregion

        internal EntityXRole(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "ENTITY_X_ROLES";
            this.m_sKeyField = "ER_ROW_ID";
            this.m_sFilterClause = "";
            base.InitFields(sFields);
            this.m_sParentClassName = "Entity";
            base.Initialize();
        }
        new public bool Validate()
        {
            bool bResult = false;
            if (Context.InternalSettings.SysSettings.UseEntityRole)
            {
                bResult = true;
            }
            return bResult;

        }

        //Rijul 340
        /// <summary>
        ///  Function will update new role in Entity_X_Roles Table 
        /// </summary>
        /// <param name="objEntityXRole"></param>
        /// <param name="iEntityTableId"></param>
        /// <param name="iEntityID"></param>
        /// <param name="sUserName"></param>
        public int UpdateEntityXRole(int iEntityTableId, int iEntityID, string sUserName = "", Entity objEntity = null)
        {
            int iErRowID = 0;
            StringBuilder sbQuery = null;
            Dictionary<string, string> objDictParams = null;
            bool bSuccess;
            bool bEntityRoleExist = true;
            try
            {
                if (this.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    if (string.IsNullOrEmpty(sUserName))
                        sUserName = Context.RMUser.LoginName;

                    if (objEntity == null)
                    {
                        sbQuery = new StringBuilder();
                        objDictParams = new Dictionary<string, string>();

                        sbQuery.Append(" SELECT ER_ROW_ID ");
                        sbQuery.Append(" FROM ENTITY_X_ROLES ");
                        sbQuery.Append(" WHERE ENTITY_TABLE_ID = {0} ");
                        sbQuery.Append(" AND ENTITY_ID = {1} ");

                        sbQuery.Replace("{0}", "~TABLEID~");
                        sbQuery.Replace("{1}", "~ENTITYID~");

                        objDictParams.Add("TABLEID", iEntityTableId.ToString());
                        objDictParams.Add("ENTITYID", iEntityID.ToString());

                        using (DataSet ds = DbFactory.ExecuteDataSet(this.Context.DbConn.ConnectionString, sbQuery.ToString(), objDictParams,this.Context.ClientId))
                        {
                            if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["ER_ROW_ID"] != null)
                            {
                                bEntityRoleExist = true;
                                iErRowID = Conversion.CastToType<int>(ds.Tables[0].Rows[0]["ER_ROW_ID"].ToString(), out bSuccess);
                            }
                            else
                            {
                                bEntityRoleExist = false;
                            }
                        }
                    }
                    else
                    {
                        iErRowID = objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId);
                        bEntityRoleExist = (!int.Equals(iErRowID, -1));
                    }

                    if (!bEntityRoleExist)
                    {
                        //  iErRowID = Utilities.GetNextUID(this.Context.DbConn.ConnectionString, "ENTITY_X_ROLES",this.Context.ClientId); //Payal Commented for RMA:9851

                        // this.ERRowId = iErRowID; //Payal Commented for RMA:9851
                        this.EntityId = iEntityID;
                        this.EntityTableId = iEntityTableId;
                        //objEntityXRole.DeletedFlag = Convert.ToBoolean(0);
                        this.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                        this.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                        this.AddedByUser = sUserName;
                        this.UpdatedByUser = sUserName;

                        this.Save();
                        iErRowID = this.ERRowId; //Payal  for RMA:9851
                    }
                }
                return iErRowID;
            }
            finally
            {
                sbQuery = null;
                objDictParams = null;
            }
        }
        //Rijul 340 end 

    }
       

    
    }
