﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ENTITY_X_MMSEA","ENT_MMSEA_ROW_ID")]
   public class CMMSEAXData:DataObject
    {
        #region Database Field List
        private string[,] sFields ={
                                        {"EntMMSEARowID","ENT_MMSEA_ROW_ID"},
                                         {"EntityEID","ENTITY_ID"},
                                         {"MMSEAReporterID","MMSEA_REPRTER_TEXT"},
                                         {"MMSEAOfficeSiteID","MMSEA_OFE_STE_TEXT"},
                                         {"LineOfBusCode","LINE_OF_BUS_CODE"},
                                         {"MMSEATINEditFlag","MMSEA_TIN_EDT_FLAG"},
                                         {"DttmRcdAdded","DTTM_RCD_ADDED"},
                                         {"AddedByUser","ADDED_BY_USER"},
                                         {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                         {"UpdatedByUser","UPDATED_BY_USER"},
                                         {"ScheduleCode","REPORT_SCHED_CODE"},
                                         {"SecondLobCode","SECOND_LOB_CODE"}, //Added Mits 20818
                                 };
        public int EntMMSEARowID{get{return GetFieldInt("ENT_MMSEA_ROW_ID");}set{SetField("ENT_MMSEA_ROW_ID",value);}}
        public int EntityEID{get{return GetFieldInt("ENTITY_ID");}set{SetField("ENTITY_ID",value);}}
        public string MMSEAReporterID{get{return GetFieldString("MMSEA_REPRTER_TEXT");} set{SetField("MMSEA_REPRTER_TEXT",value);}}
        public string MMSEAOfficeSiteID{get{return GetFieldString("MMSEA_OFE_STE_TEXT");} set{SetField("MMSEA_OFE_STE_TEXT",value);}}
        public int LineOfBusCode{get{return GetFieldInt("LINE_OF_BUS_CODE");}set{SetField("LINE_OF_BUS_CODE",value);}}
        public int MMSEATINEditFlag{get{return GetFieldInt("MMSEA_TIN_EDT_FLAG");}set{SetField("MMSEA_TIN_EDT_FLAG",value);}}
        public string DttmRcdAdded{get{return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string AddedByUser{get{return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
        public string UpdatedByUser{get{return GetFieldString("UPDATED_BY_USER");}set {SetField("UPDATED_BY_USER",value);}}
        public int ScheduleCode { get { return GetFieldInt("REPORT_SCHED_CODE"); } set { SetField("REPORT_SCHED_CODE", value); } }
        public int SecondLobCode { get { return GetFieldInt("SECOND_LOB_CODE"); } set { SetField("SECOND_LOB_CODE", value); } } //Added Mits 20818
#endregion
        internal CMMSEAXData(bool isLocked,Context context):base(isLocked,context)
        {
            this.Initialize();
        }
       new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENTITY_X_MMSEA";
			this.m_sKeyField = "ENT_MMSEA_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			
			this.m_sParentClassName = "ENTITY";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
        

    }
}
