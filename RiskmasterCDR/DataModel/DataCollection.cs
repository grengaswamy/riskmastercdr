using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Diagnostics;
using System.Reflection;
using System.Xml;

// TODO Add Collection scripting Calls?  Who actually scripts to the save of a collection???

namespace Riskmaster.DataModel
{
    /// <summary>
    /// The base class for an iteratable collection used to represent 1 to many record 
    /// relationships from the parent side.
    /// </summary>
    public class DataCollection : DataRoot, IDataModel, IEnumerable, IPersistence
    {
        private const string AUTHKEY = "6378b87457a5ecac8674e9bac12e7cd9";
        private const string CodeAccessPublicKey = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

        internal Hashtable m_keySet = new Hashtable();
        private Hashtable m_isDeleted = new Hashtable();
        private Hashtable m_isRemoved = new Hashtable();

        private string m_itemTypeName = "DataObject";
        private string m_SQLFilter = "";
        private string m_SQLFromTable = "";
        private string m_SQLKeyColumn = "";
        private string m_childName = "";
        private int m_newItemCounter = 1;
        private int m_committedCount = 0;

        private bool m_BulkLoadFlag = false;

        private void SwapKey(int origKeyValue, int newKeyValue)
        {
            object objTemp = null;
            if (m_isDeleted.ContainsKey(origKeyValue))
            {
                objTemp = m_isDeleted[origKeyValue];
                m_isDeleted.Remove(origKeyValue);
                m_isDeleted.Add(newKeyValue, objTemp);
            }

            if (m_keySet.ContainsKey(origKeyValue))
            {
                objTemp = m_keySet[origKeyValue];
                m_keySet.Remove(origKeyValue);
                m_keySet.Add(newKeyValue, objTemp);
            }
            //Needs to replace old value in memory with the new one - Sumeet Rathod
            if (m_isRemoved.ContainsKey(origKeyValue))
            {
                objTemp = m_isRemoved[origKeyValue];
                m_isRemoved.Remove(origKeyValue);
                m_isRemoved.Add(newKeyValue, objTemp);
            }
        }
        protected virtual string DerivedTypeNameFromKey(int keyValue)
        {
            return m_itemTypeName;
        }

        //		internal event OnChildItemAddedEventHandler OnChildItemAdded;

        /// <summary>
        /// Checks if the following information has been provided:
        ///   1.) What datatype to use.
        ///   2.) How to select the collection member rows.
        ///   3.) What table the rows are in.
        ///   4.) What field is the table key.
        /// </summary>
        /// <returns></returns>
        internal bool IsReady()
        {
            if (m_SQLKeyColumn == "")
                return false;
            if (m_SQLFromTable == "")
                return false;
            if (this.Parent != null)
                if ((this.Parent as DataObject).IsNew)
                    m_SQLFilter = "1 = 0"; //Fake filter to force an empty keyset but allow us to be considered "Ready"
            if (m_SQLFilter == "")
                return false;
            if (m_itemTypeName == "")
                return false;
            if (m_itemTypeName == "DataObject")
                return false;

            return true;
        }

        internal DataCollection(bool isLocked, Context context) : base(isLocked, context) { (this as IDataModel).Initialize(isLocked, context); }
        public virtual DataObject this[int keyValue]
        {
            get
            {
                //Check for is the collection Prepared? (knows what datatype to use and how to select the collection member rows.)
                if (!IsReady())
                    if (!this.IsNew)
                        throw new DataModelException(Globalization.GetString("DataCollection.this[].get.Exception.CollectionNotReady", this.Context.ClientId));

                //(Allow the first one to be created "on demand.")
                if (m_keySet.Count == 0)
                {
                    if (keyValue <= 0)
                    {
                        keyValue = this.AddNew().KeyFieldValue;
                    }
                    else
                        this.Add(keyValue);
                }
                //Check for keyValue in this collection. (Allow the first one to be created "on demand.")
                if (!m_keySet.ContainsKey(keyValue))
                    return null;

                //Check for keyValue has been deleted from this collection.
                if (IsDeleted(keyValue) || IsRemoved(keyValue))
                    return null;

                //Fetch Cached Instance if any.
                DataObject obj = (m_keySet[keyValue] as DataObject);

                // Child not yet loaded\cached
                if (obj == null)
                {
                    obj = (this.Context.Factory.GetDataModelObject(DerivedTypeNameFromKey(keyValue), true) as DataObject);
                    obj.Parent = this.Parent;
                    try
                    {
                        (obj as INavigation).MoveTo(keyValue);
                    }
                    catch (RecordNotFoundException) //Catch if someone deleted our member object, just add it to the "deleted" list???
                    {
                        m_isDeleted[keyValue] = true;
                        obj = null;
                    }
                    m_keySet[keyValue] = obj;
                }

                return obj;
            }
        }
        public virtual DataObject AddNew()
        {
            DataObject obj = (this.Context.Factory.GetDataModelObject(m_itemTypeName, true) as DataObject);
            obj.Parent = this.Parent;
            return Add(obj);
        }

        public DataObject Add(DataObject obj)
        {
            // Negative indicates a new "in memory only" record (no real key value will be available until save\GetNextUID call)
            // Kept negative because we don't want to conflict with any existing real db record key values.
            int keyValue = -1 * this.m_newItemCounter;
            bool bTmpDataChanged = obj.DataChanged;

            if (!obj.IsNew)
                keyValue = obj.KeyFieldValue;

            if (m_keySet.ContainsKey(keyValue))
                if (IsDeleted(keyValue) || IsRemoved(keyValue))
                    throw new DataModelException(Globalization.GetString("DataCollection.Add.Exception.IncompleteDeletion", this.Context.ClientId));
                else
                    return m_keySet[keyValue] as DataObject;

            obj.Parent = this.Parent;
            obj.m_isParentStale = (this as DataRoot).m_isParentStale;
            //BSB 12.07.2005
            // The KeyFieldValue assignment causes "datachanged" flag to flip on every new member item
            // as it is added.  This causes blank records to be saved even when the 
            // in memory copy was never explicitly populated with any data.
            // Track Record Issue# 619 blank Adjuster and Claimant records being saved.
            obj.KeyFieldValue = keyValue; //Has side effect of changing "dirty flag"
            obj.DataChanged = bTmpDataChanged; //Restore the original dirty flag after forcing the key value change.
            obj.Locked = true;
            m_keySet.Add(keyValue, obj);
            m_isDeleted.Add(keyValue, false);
            m_isRemoved.Add(keyValue, false);
            this.m_newItemCounter++;
            base.m_DataChanged = true;
            if (this.Parent is DataObject)
                (this.Parent as DataObject).CallOnChildItemAdded(GetChildName(), obj);
            return obj;
        }

        public DataObject Add(DataObject obj, bool condition)
        {
            // Negative indicates a new "in memory only" record (no real key value will be available until save\GetNextUID call)
            // Kept negative because we don't want to conflict with any existing real db record key values.
            int keyValue = -1 * this.m_newItemCounter;
            bool bTmpDataChanged = obj.DataChanged;

            if (!obj.IsNew || condition)
                keyValue = obj.KeyFieldValue;

            if (m_keySet.ContainsKey(keyValue))
                if (IsDeleted(keyValue) || IsRemoved(keyValue))
                    throw new DataModelException(Globalization.GetString("DataCollection.Add.Exception.IncompleteDeletion", this.Context.ClientId));
                else
                    return m_keySet[keyValue] as DataObject;

            obj.Parent = this.Parent;
            obj.m_isParentStale = (this as DataRoot).m_isParentStale;
            //BSB 12.07.2005
            // The KeyFieldValue assignment causes "datachanged" flag to flip on every new member item
            // as it is added.  This causes blank records to be saved even when the 
            // in memory copy was never explicitly populated with any data.
            // Track Record Issue# 619 blank Adjuster and Claimant records being saved.
            obj.KeyFieldValue = keyValue; //Has side effect of changing "dirty flag"
            obj.DataChanged = bTmpDataChanged; //Restore the original dirty flag after forcing the key value change.
            obj.Locked = true;
            m_keySet.Add(keyValue, obj);
            m_isDeleted.Add(keyValue, false);
            m_isRemoved.Add(keyValue, false);
            this.m_newItemCounter++;
            base.m_DataChanged = true;
            if (this.Parent is DataObject)
                (this.Parent as DataObject).CallOnChildItemAdded(GetChildName(), obj);
            return obj;
        }


        //Hack to find the name given to this object by it's parent. Ie, the "ChildName."
        private string GetChildName()
        {
            DataObject p = (DataObject)this.Parent;
            if (m_childName == "")
                foreach (string sPropName in p.Properties)
                    if (p.m_Children.ContainsChild(sPropName, false))
                        if (p.m_Children[sPropName] == this)
                        {
                            m_childName = sPropName;
                            break;
                        }

            return m_childName;
        }
        public DataObject Add(int keyValue)
        {
            if (m_keySet.ContainsKey(keyValue))
                if (IsDeleted(keyValue) || IsRemoved(keyValue))
                    throw new DataModelException(Globalization.GetString("DataCollection.Add.Exception.IncompleteDeletion", this.Context.ClientId));
                else
                    return m_keySet[keyValue] as DataObject;

            DataObject obj = (this.Context.Factory.GetDataModelObject(DerivedTypeNameFromKey(keyValue), true) as DataObject);
            obj.Parent = this.Parent;
            obj.KeyFieldValue = keyValue;
            m_keySet.Add(keyValue, obj);
            m_isDeleted.Add(keyValue, false);
            m_isRemoved.Add(keyValue, false);
            base.m_DataChanged = true;
            if (this.Parent is DataObject)
                (this.Parent as DataObject).CallOnChildItemAdded(this.GetType().Name, obj);

            return obj;
        }
        public string GetTypeName() { return TypeName; }
        protected string TypeName
        {
            //[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CodeAccessPublicKey)]
            get
            {
                return m_itemTypeName;
            }
            //[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CodeAccessPublicKey)]
            set
            {
                if (m_itemTypeName != value)
                {
                    m_itemTypeName = value;
                    Reset();
                }
            }
        }


        public string SQLFilter
        {
            get
            {
                return m_SQLFilter;
            }
            set
            {
                if (m_SQLFilter != value)
                {
                    EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
                    m_SQLFilter = value;
                    Reset();
                    (this as IDataModel).IsStale = false;
                }
            }
        }
        internal string SQLKeyColumn
        {
            get
            {
                return m_SQLKeyColumn;
            }
            set
            {
                if (m_SQLKeyColumn != value)
                {
                    m_SQLKeyColumn = value;
                    Reset();
                }
            }
        }
        internal string SQLFromTable
        {
            get
            {
                return m_SQLFromTable;
            }
            set
            {
                if (m_SQLFromTable != value)
                {
                    m_SQLFromTable = value;
                    Reset();
                }
            }
        }
        public bool BulkLoadFlag
        {
            get
            {
                return m_BulkLoadFlag;
            }
            set
            {
                m_BulkLoadFlag = value;
            }
        }
        private void Reset()
        {
            m_keySet = new Hashtable();
            m_isDeleted = new Hashtable();
            m_isRemoved = new Hashtable();
            this.DataChanged = false;
            (this as IDataModel).IsStale = false;
            if (IsReady())
                LoadKeySet();
        }
        internal void ClearContent()
        {
            m_keySet = new Hashtable();
            m_isDeleted = new Hashtable();
            m_isRemoved = new Hashtable();
            m_SQLFilter = "1 = 0"; //Fake filter to force an empty keyset but allow us to be considered "Ready"
            this.DataChanged = false;
            (this as IDataModel).IsStale = false;
            m_committedCount = 0;
        }
        private bool IsDeleted(int keyValue) { return (bool)m_isDeleted[keyValue]; }
        private bool IsRemoved(int keyValue) { return (bool)m_isRemoved[keyValue]; }
        public bool Delete(int keyValue)
        {
            if (!m_isDeleted.ContainsKey(keyValue))
                return false;

            if (keyValue < 0) //Not in DB, was a new record added only to this collection.
            {
                m_isRemoved.Remove(keyValue);
                m_isDeleted.Remove(keyValue);
                m_keySet.Remove(keyValue);
            }
            else
            {
                //BSB 02.14.2006 Fix for deleting "removed" during save.
                // In that case, using the [] accessor can return null.
                IPersistence pIPer = m_keySet[keyValue] as IPersistence;
                if (pIPer == null)//Try to load on demand.
                {
                    DataObject obj = (this.Context.Factory.GetDataModelObject(DerivedTypeNameFromKey(keyValue), true) as DataObject);
                    obj.Parent = this.Parent;
                    try { (obj as INavigation).MoveTo(keyValue); }
                    catch (RecordNotFoundException) { } //Who cares - it's not in the DB anyway.
                }

                if (pIPer != null)
                    pIPer.Delete();

                // Vaibhav\BSB 02.14.2006  
                // AutoChecks found that deleting did not properly affect counts and that
                // this caused problems if the collection continues to be used after a save.
                // Instead of just marking deleted items - try getting rid of them immediately.
                m_isRemoved.Remove(keyValue);
                m_isDeleted.Remove(keyValue);
                m_keySet.Remove(keyValue);

                m_committedCount--;
            }
            return true;
        }

        private void LoadKeySet()
        {
            DbReader objReader = null;
            DataObject objItem = null;
            DbConnection objTempConn = null;
            try
            {
                m_committedCount = 0;
                if (this.BulkLoadFlag) //Uses this single query to populate multiple objects.
                {
                    objTempConn = DbFactory.GetDbConnection(this.Context.DbConnLookup);
                    //BSB 03.03.2006 Fix for if we're inside a Transaction - this can deadlock
                    // if we don't join the existing Transaction Context (found on SQL Server).
                    // Fixes AutoChecks Update Code lock-up.
                    if (this.Context.DbTrans != null)
                        this.Context.SQLBindToExistingTransContext(this.Context.DbConnLookup, objTempConn);
                    if (base.Context.DbConnLookup.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        objReader = DbFactory.ExecuteReader(this.Context.DbConnLookup.ConnectionString, String.Format("SELECT * FROM {0} WHERE {1} ORDER BY {2}", SQLFromTable, SQLFilter, SQLKeyColumn));//7810 fix JIRA 12004
                    }
                    else
                    {
                        objReader = objTempConn.ExecuteReader(String.Format("SELECT * FROM {0} WHERE {1} ORDER BY {2}", SQLFromTable, SQLFilter, SQLKeyColumn));//7810 fix JIRA 12004
                    }
                }
                else
                //7810 start fix JIRA 12004
                {
                    if (base.Context.DbConnLookup.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                    {
                    
                    objReader = DbFactory.ExecuteReader(this.Context.DbConnLookup.ConnectionString, String.Format("SELECT {0} FROM {1} WHERE {2} ORDER BY {3}", SQLKeyColumn, SQLFromTable, SQLFilter, SQLKeyColumn));
                    }
                    else
                    {
                        objReader = this.Context.DbConnLookup.ExecuteReader(String.Format("SELECT {0} FROM {1} WHERE {2} ORDER BY {3}", SQLKeyColumn, SQLFromTable, SQLFilter, SQLKeyColumn));
                    }
                }
                //7810 end fix JIRA 12004
                while (objReader.Read())
                {
                    objItem = null;
                    if (this.BulkLoadFlag) //Uses this single query to populate multiple objects.
                    {
                        objItem = this.Context.Factory.GetDataModelObject(this.TypeName, true) as DataObject;
                        objItem.LoadData(objReader);
                    }
                    m_keySet.Add(objReader.GetInt(SQLKeyColumn.ToUpper()), objItem);
                    m_isDeleted.Add(objReader.GetInt(SQLKeyColumn.ToUpper()), false);
                    m_isRemoved.Add(objReader.GetInt(SQLKeyColumn.ToUpper()), false);
                    m_committedCount++;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("DataCollection.LoadKeySet.Exception", this.Context.ClientId), e); }
            finally
            {
                if (objReader != null)
                    objReader.Close();
                objReader = null;
                if (objTempConn != null)
                    objTempConn.Close();
                objTempConn = null;
            }
        }
        private int DeletedCount
        {
            get
            {
                int i = 0;
                foreach (bool bFlag in this.m_isDeleted.Values)
                    if (bFlag)
                        i++;
                return i;
            }
        }
        /// <summary>
        ///  This function flags the in memory collection  member item as having been removed.
        ///  This will ONLY result in an IPersistence.Delete call at the time this object is saved.
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool Remove(int keyValue)
        {
            if (!m_isRemoved.ContainsKey(keyValue))
                return false;

            m_isRemoved[keyValue] = true;
            //BSB 02.14.2006 Flag the collection itself as changed.
            base.m_DataChanged = true;

            return true;
        }

        private int RemovedCount
        {
            get
            {
                int i = 0;
                foreach (bool bFlag in this.m_isRemoved.Values)
                    if (bFlag)
                        i++;
                return i;
            }
        }
        public int Count
        {
            get
            {
                //BSB\Vaibhav 03.13.2007 Removed Items were not properly taken out of the Collection's Item Count value.
                return this.m_keySet.Count - (this.DeletedCount + this.RemovedCount);
            }
        }
        public int CommittedCount
        {
            get
            {
                return this.m_committedCount;
            }
        }
        #region Enumeration Implementation
        IEnumerator IEnumerable.GetEnumerator() { return new DataCollectionEnumerator(this); }
        class DataCollectionEnumerator : IEnumerator
        {
            int m_iPos = -1;
            DataCollection m_parent = null;
            object[] keys = null;

            internal DataCollectionEnumerator(DataCollection parent)
            {
                m_parent = parent;
                keys = new object[m_parent.m_keySet.Keys.Count];
                m_parent.m_keySet.Keys.CopyTo(keys, 0);
            }
            //TODO IMPLEMENT DataCollection Enum Class.
            public void Reset()
            {
                m_iPos = -1;
            }

            public object Current
            {
                get
                {
                    if (m_iPos < 0 || m_iPos >= keys.Length)
                        return null;
                    return m_parent[(int)keys[m_iPos]];
                }
            }

            public bool MoveNext()
            {
                try
                {
                    m_iPos++;

                    while (!(m_iPos < 0 || m_iPos >= keys.Length))
                        if (m_parent.IsDeleted((int)keys[m_iPos]) || m_parent.IsRemoved((int)keys[m_iPos]))
                            m_iPos++;
                        else
                            return true;
                }
                catch (Exception e)
                {
                    { System.Diagnostics.Trace.WriteLine(e.Message + e.StackTrace); }
                }
                return false;
            }


        }
        #endregion

        #region IPersistence Members

        public event Riskmaster.DataModel.InitObjEventHandler InitObj;

        public event Riskmaster.DataModel.CalculateDataEventHandler CalculateData;

        public event Riskmaster.DataModel.ValidateEventHandler ValidateData;

        public event Riskmaster.DataModel.BeforeSaveEventHandler BeforeSave;

        public event Riskmaster.DataModel.AfterSaveEventHandler AfterSave;

        public bool IsNew
        {
            get
            {
                if (!IsReady())
                    return true;

                foreach (DataObject obj in this)
                    if (!obj.IsNew)
                        return false;
                return true;
            }
        }

        public override bool DataChanged
        {
            get
            {
                if (base.m_DataChanged)
                    return true;

                //foreach(DataObject obj in this)
                //BSB Fix for Iterator causing on-demand load of member objects during save.
                // this is never required since if an object has changes to be saved, it necessarilly must 
                // have already been loaded by this point.
                // Instead, iterate over the keyset at a lower level, leaving un-loaded objects alone.
                DataObject obj = null;
                object[] keys = new object[m_keySet.Keys.Count];
                m_keySet.Keys.CopyTo(keys, 0);
                for (int i = 0; i < keys.Length; i++)
                {
                    obj = (DataObject)m_keySet[keys[i]];
                    //This is where we get to skip "on-demand" load for any member which can obviously never have been changed...
                    if (obj == null)
                        continue;
                    else if (obj.AnyDataChanged)
                        return true;
                }
                return false;
            }
            set
            {
                base.m_DataChanged = value;
                foreach (DataObject obj in this)
                    obj.DataChanged = value;
            }
        }
        internal override void LockDataChangedFlag()
        {
            base.LockDataChangedFlag();
            foreach (DataObject obj in this)
                obj.LockDataChangedFlag();
        }

        internal override void UnlockDataChangedFlag()
        {
            base.UnlockDataChangedFlag();
            foreach (DataObject obj in this)
                obj.UnlockDataChangedFlag();
        }

        public override bool AnyDataChanged
        {
            get
            {
                if (base.m_DataChanged)
                    return true;

                //foreach(DataObject obj in this)
                //BSB Fix for Iterator causing on-demand load of member objects during save.
                // this is never required since if an object has changes to be saved, it necessarilly must 
                // have already been loaded by this point.
                // Instead, iterate over the keyset at a lower level, leaving un-loaded objects alone.
                DataObject obj = null;
                object[] keys = new object[m_keySet.Keys.Count];
                m_keySet.Keys.CopyTo(keys, 0);
                for (int i = 0; i < keys.Length; i++)
                {
                    obj = (DataObject)m_keySet[keys[i]];
                    //This is where we get to skip "on-demand" load for any member which can obviously never have been changed...
                    if (obj == null)
                        continue;
                    else if (obj.AnyDataChanged)
                        return true;
                }
                return false;
            }
        }
        bool IDataModel.IsStale
        {
            get { return m_isStale; }
            set
            {	//If we don't clear this collection out we can end up with a stale object
                // claiming to have "AnyDataChanged==true"
                if (value)
                {
                    this.m_SQLFilter = "1 = 0";
                    m_keySet = new Hashtable();
                    m_isDeleted = new Hashtable();
                    m_isRemoved = new Hashtable();
                    this.DataChanged = false;
                    (this as IDataModel).IsStale = false;
                }
                m_isStale = value;
            }
        }
        public void Delete()
        {
            object trash = null;

            //BSB Pull all items out of "Removed" status so that they get completely deleted.
            //foreach(object keyValue in m_isRemoved.Keys)
            object[] keys = new object[m_isRemoved.Keys.Count];
            m_isRemoved.Keys.CopyTo(keys, 0);
            for (int i = 0; i < keys.Length; i++)
                m_isRemoved[keys[i]] = false;

            //BSB Fix For Lockup on second collection member load causing lockup during delete transaction.
            //Now "pre-load" the entire list before making any table changes within the Delete transaction.
            foreach (DataObject obj in this)
                trash = obj;

            foreach (DataObject obj in this)
            {
                obj.Delete();
                m_isRemoved.Remove(obj.KeyFieldValue);
                m_isDeleted.Remove(obj.KeyFieldValue);
                m_keySet.Remove(obj.KeyFieldValue);

                //				m_isDeleted[obj.KeyFieldValue] = true;
                //				m_isRemoved[obj.KeyFieldValue] = false;
            }
        }

        public void Save()
        {
            try
            {
                this.m_DataModelState = DataObjectState.IsSaving;

                if ((this as IDataModel).IsStale)
                    return;

                if (!this.DataChanged)
                    return;

                //foreach(DataObject obj in this)
                //BSB Fix for Iterator causing on-demand load of member objects during save.
                // this is never required since if an object has changes to be saved, it necessarilly must 
                // have already been loaded by this point.
                // Instead, iterate over the keyset at a lower level, leaving un-loaded objects alone.
                int itemKeyValue = 0;
                DataObject obj = null;
                object[] keys = new object[m_keySet.Keys.Count];
                m_keySet.Keys.CopyTo(keys, 0);

                //Aditya - For backwards compatibility with AutoChecks, save order is significant.
                // Save children in the order that they were added to this collection.  
                Array.Sort(keys);
                try
                {
                    for (int i = keys.Length - 1; i >= 0; i--)
                    //for(int i = 0;i <keys.Length ;i++)
                    {
                        itemKeyValue = (int)keys[i];
                        obj = (DataObject)m_keySet[keys[i]];
                        //This is where we get to skip "on-demand" load for any member which can obviously never have been changed...
                        if (obj == null)
                            continue;
                        if (!IsDeleted(itemKeyValue) && !IsRemoved(itemKeyValue))
                        {
                            if (obj.IsNew) //We are assuming that all collections are saved via this method, otherwise we could end up with invalid keys.
                            {
                                if (obj.AnyDataChanged) //Save will persist a new record to the DB.
                                    m_committedCount++;
                                int orig = obj.KeyFieldValue;
                                obj.Save();

                                //BSB 02.14.2006 
                                // AutoChecks uncovered this issue.
                                // When calling save on an object that is a member of a collection.
                                // IFF that member has a parent AND the save "bubbles up" to that parent THEN
                                // the "SwapKey" routine was being run twice against the new key.  Once
                                // during the parent(s) recursive save call against this collection AND
                                // again after the stack unwinds to the original object save call (initiated here)
                                // The second call to SwapKey throws an exception since the necessary collection items
                                // had already been swapped over to the new key.
                                // In order to work around this problem, if the object is still in an "IsSaving"
                                // state that means that we're the "inner" call and should not call swapkey until
                                // the stack unwinds back to this location in the ORIGINAL stack frame.

                                //BSB 05.15.2007 
                                // The original (2/14) solution is inadequate since a "Save" could be initiated elsewhere on the 
                                // object graph (Claim).  This would put the obj into "IsSaving" state but we would never 
                                // return here to finish the job since this isn't the point of recursion...

                                // Instead - Simply armor the SwapKey routine to check for valid original keys before it
                                // performs any collection access.

                                //if(obj.m_DataModelState != DataObjectState.IsSaving)
                                SwapKey(orig, obj.KeyFieldValue); //Eliminates the "fake zero or negative key" used to track items 
                            }										//  before they get saved to the db and get a unique id of their own.
                            else
                                if (obj.AnyDataChanged)
                                    obj.Save();
                        }
                        else if (IsRemoved(itemKeyValue) && !IsDeleted(itemKeyValue))
                            Delete(obj.KeyFieldValue);
                        //obj.Delete();
                    }
                    base.m_DataChanged = false;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    throw e;
                }
            }
            finally
            {
                this.m_DataModelState = DataObjectState.IsReady;
            }
        }

        public void Refresh()
        {
            Reset();
        }

        #endregion

        //TODO Remove after debugging this could be a security hole.
        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

        #region Work In Progress
        public string SerializeObject()
        {
            SerializationContext objContext = new SerializationContext();
            objContext.RequestDom = new XmlDocument();
            objContext.RequestDom.LoadXml(String.Format("<{0} />", this.GetType().Name));
            return SerializeObject(objContext);
        }

        public string SerializeObject(XmlDocument dom)
        {
            SerializationContext objContext = new SerializationContext();
            objContext.RequestDom = dom;
            objContext.ElementTag = this.GetType().Name;
            objContext.RequestXPath = String.Format("/{0}", objContext.ElementTag);
            return SerializeObject(objContext);
        }

        //BSB Note this case is only hit if the Serialization request is initiated directly on
        // a collection object.  Otherwise, the DataObject "list property" logic will suffice.
        // This case is used to support serializing an xml document suitable for displaying
        // an intermediate navigation screen listing the contents of a collection.
        // For example: PersonsInvolved or Claims from an event...
        internal string SerializeObject(SerializationContext objContext)
        {
            string s = "";
            bool bEntryCall = false;

            if (objContext.ElementTag == null || objContext.ElementTag == "")
            {
                objContext.ElementTag = this.GetType().Name;
                if (objContext.RequestXPath == null || objContext.RequestXPath == "") //Initialize XPath iff this is the entry point
                {
                    objContext.RequestXPath = objContext.ElementTag;
                    bEntryCall = true;
                }
            }
            s += String.Format("<{0} count=\"{1}\" committedcount=\"{2}\">", objContext.ElementTag, this.Count, this.CommittedCount);
            foreach (DataObject objItem in this)
                s += objItem.SerializeObject(objContext.CreateChild(this.TypeName));
            s += String.Format("</{0}>", objContext.ElementTag);

            s = s.Replace(String.Format("<{0}>", this.TypeName), String.Format("<{0} remove=\"false\">", this.TypeName));
            if (bEntryCall) 	//"Pre Processing" occurs only if this is the entry point for a Serialization Request
            {
                return String.Format(@"
					<Instance  
						xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
						xsi:noNamespaceSchemaLocation=""schema-1.xsd" + @""">
						{0}</Instance>", s);
            }
            else
                return s;
        }

        public bool PopulateObject(XmlDocument propertyStore)
        {
            return PopulateObject(propertyStore, this.GetType().Name, false);
        }
        public bool PopulateObject(XmlDocument propertyStore, bool bKeepCurrentData)
        {
            return PopulateObject(propertyStore, this.GetType().Name, bKeepCurrentData);
        }
        internal bool PopulateObject(XmlDocument propertyStore, string rootTagName)
        {
            return PopulateObject(propertyStore, rootTagName, false);

        }

        /// <summary>
        /// TODO Figure out syntax for Internal Deletion where delete is not always a top level user action.
        /// </summary>
        /// <param name="propertyStore"></param>
        /// <param name="rootTagName"></param>
        /// <returns></returns>
        internal bool PopulateObject(XmlDocument propertyStore, string rootTagName, bool bKeepCurrentData)
        {
            //			bool bAddingNew = false;
            string sKeyProp = GetKeyPropertyName();
            int itemKeyValue = 0;
            //			object o =null;
            XmlElement elt = null;
            XmlReader xmlValidator = null;

            //1.) Determine if propertyStore is valid for the current object type.
            //a.)Root Tag matches class name?
            if (propertyStore.DocumentElement.Name != rootTagName &&
                propertyStore.DocumentElement.Name != this.GetType().BaseType.Name &&
                propertyStore.SelectSingleNode(String.Format("/Instance/{0}", rootTagName)) == null)
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataCollection.PopulateObject.Exception.RootElementMismatch", this.Context.ClientId), propertyStore.DocumentElement.Name, rootTagName));

            //b.) Document Validates?
            try
            {
                if (propertyStore.DocumentElement.Name == "Instance") //Then this is the first time we're seeing this document, (Worth doing the validation) 
                {
                    XmlUtilityFunctions.ExtractInstanceInfo(ref propertyStore, rootTagName, ref xmlValidator);
                }
            }
            catch (Exception e)
            {
                throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataCollection.PopulateObject.Exception.SchemaValidationException", this.Context.ClientId), rootTagName), e);
            }
            finally
            {
                if (xmlValidator != null)
                    xmlValidator.Close();
                if (SerializationContext.RWLock.IsReaderLockHeld)
                    SerializationContext.RWLock.ReleaseReaderLock();
            }
            //4.) For all Collection members update, delete, add as appropriate...
            ///Notes: The lack of an xml item corresponding to a collection item is NOT DEEMED SUFFICIENT
            /// to cause a Remove or Delete call.
            /// A collection item to be Removed MUST EXIST AND BE TAGGED WITH THE "remove" attribute.
            /// This removal attribute is strong enough to call a "remove" against the collection but this removal will
            /// NOT be permanent until a save is called against this collection.
            foreach (XmlElement item in propertyStore.SelectNodes(String.Format("/{0}/{1}", rootTagName, this.GetTypeName())))
            {

                try { elt = item.GetElementsByTagName(sKeyProp)[0] as XmlElement; }
                catch (Exception e)
                {
                    throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataCollection.PopulateObject.Exception.KeyFieldNameMissing", this.Context.ClientId), rootTagName + "." + sKeyProp), e);
                }

                itemKeyValue = Int32.Parse(elt.InnerText);

                DataObject objChild = null;
                //If we don't already have this item, add it.
                if (!this.ContainsKey(itemKeyValue))
                {
                    //Asked to be removed but it isn't really here so just move on to next item.
                    if (item.HasAttribute("remove") && item.Attributes["remove"].Value == "True")
                        continue;

                    if (itemKeyValue > 0)
                        this.Add(itemKeyValue);
                    else
                    {
                        itemKeyValue = this.AddNew().KeyFieldValue;
                        item.SelectSingleNode(this[itemKeyValue].FieldNameToProperty(this[itemKeyValue].KeyFieldName)).InnerText = itemKeyValue.ToString();
                    }
                }

                objChild = this[itemKeyValue] as DataObject; //Fetch the item object instance.
                if (objChild != null)  //XML Contains UPDATE or CREATE  information for this child.
                {
                    if (item.HasAttribute("remove") && item.Attributes["remove"].Value == "True")
                        this.Delete(itemKeyValue);
                    else
                        objChild.PopulateObject(Utilities.XmlElement2XmlDoc(item), objChild.GetType().Name, bKeepCurrentData);
                }
            } //End "For Each Property"		

            //Aditya - Found that this was not set on collections during population - it needs to be done 
            // here just as it is done for individual objects during population. See "DataObject.PopulateObject()"
            this.m_isStale = false;

            return true;
        }
        internal bool ContainsObject(object o)
        {
            int itemKeyValue = 0;
            object[] keys = new object[m_keySet.Keys.Count];
            m_keySet.Keys.CopyTo(keys, 0);

            for (int i = 0; i < keys.Length; i++)
            {
                itemKeyValue = (int)keys[i];
                if (o == m_keySet[keys[i]])
                    return true;
            }
            return false;
        }

        public bool ContainsKey(int keyValue) { return (this.m_keySet.ContainsKey(keyValue) && !this.IsDeleted(keyValue) && !this.IsRemoved(keyValue)); }

        /// <summary>
        ///  Privately tries to ask the first member of the current collection what the item's key property name is.
        ///  If no members are available, then a temporary object is created.
        /// </summary>
        /// <returns></returns>
        protected string m_keyPropertyName = "";
        private string GetKeyPropertyName()
        {
            if (m_keyPropertyName != "")
                return m_keyPropertyName;

            DataObject o = null;
            foreach (DataObject o2 in this) //returns the based on the first one.
            {
                o = o2;
                break;
            }

            if (o == null)
                o = (this.Context.Factory.GetDataModelObject(m_itemTypeName, true) as DataObject);

            m_keyPropertyName = o.FieldNameToProperty(o.KeyFieldName);
            return m_keyPropertyName;
        }
        #endregion
        //BSB Not a very meaningfull item as collections aren't currently script-able.
        public bool Validate() { return this.ValidateData(this, new ValidateEventArgs()); }

    }
}
