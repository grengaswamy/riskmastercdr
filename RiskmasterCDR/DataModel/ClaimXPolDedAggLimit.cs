﻿using System;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_POL_DED_AGG_LIMIT", "CLAIM_X_POL_DED_AGG_LIMIT_ID")]
    public class ClaimXPolDedAggLimit : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"ClaimXPolDedAggLimitId", "CLAIM_X_POL_DED_AGG_LIMIT_ID"},
                                        {"CovGroupId", "COV_GROUP_CODE"},
                                        //{"PolEffectiveDate", "POL_EFFECTIVE_DATE"},
                                        //{"PolExpirationDate", "POL_EXPIRATION_DATE"},
                                        {"AggLimitAmt", "AGG_LIMIT_AMT"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"},
                                        {"SirDedPreventAmt", "SIR_DED_PEREVENT_AMT"},
                                        {"ExcludeExpenseFlag", "EXCLUDE_EXPENSE_FLAG"} //ExpenseFlagAddition - nbhatia6 - 07/03/14
                                    };
        public int ClaimXPolDedAggLimitId { get { return GetFieldInt("CLAIM_X_POL_DED_AGG_LIMIT_ID"); } set { SetField("CLAIM_X_POL_DED_AGG_LIMIT_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COV_GROUP_CODE")]
        public int CovGroupId { get { return GetFieldInt("COV_GROUP_CODE"); } set { SetField("COV_GROUP_CODE", value); } }
        //public string PolEffectiveDate { get { return GetFieldString("POL_EFFECTIVE_DATE"); } set { SetField("POL_EFFECTIVE_DATE", value); } }
        //public string PolExpirationDate { get { return GetFieldString("POL_EXPIRATION_DATE"); } set { SetField("POL_EXPIRATION_DATE", value); } }
        public double AggLimitAmt { get { return GetFieldDouble("AGG_LIMIT_AMT"); } set { SetField("AGG_LIMIT_AMT", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public double SirDedPreventAmt { get { return GetFieldDouble("SIR_DED_PEREVENT_AMT"); } set { SetField("SIR_DED_PEREVENT_AMT", value); } }
        //ExpenseFlagAddition - nbhatia6 - 07/03/14
        public bool ExcludeExpenseFlag { get { return GetFieldBool("EXCLUDE_EXPENSE_FLAG"); } set { SetField("EXCLUDE_EXPENSE_FLAG", value); } }
        #endregion

        #region Child Implementation
        private string[,] sChildren = {
                                          {"ClaimXPolDedAggLimitHistList", "ClaimXPolDedAggLimitHistList"}
                                      };

        internal override void OnChildInit(string childName, string childType)
        {
            base.OnChildInit(childName, childType);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "ClaimXPolDedAggLimitHistList":
                    (itemValue as ClaimXPolDedAggLimitHist).ClaimXPolDedAggLimitId = this.ClaimXPolDedAggLimitId;
                    break;
            }
        }

        private void UpdateHistory()
        {
            ClaimXPolDedAggLimitHist objHist = null;
            objHist = this.ClaimXPolDedAggLimitHistList.LastStatusChange();

            if (objHist == null || !objHist.IsNew)
                objHist = this.ClaimXPolDedAggLimitHistList.AddNew();
            objHist.ClaimXPolDedAggLimitId = this.ClaimXPolDedAggLimitId;
            objHist.DateAggLimitChgd = Conversion.ToDbDateTime(DateTime.Now);
            objHist.AggLimitChgdBy = this.UpdatedByUser;
            objHist.CovGroupId = this.CovGroupId;
            objHist.AggLimitAmt = this.AggLimitAmt;
            objHist.SirDedPreventAmt = this.SirDedPreventAmt;
            //ExpenseFlagAddition - nbhatia6 - 07/03/14
            objHist.ExcludeExpenseFlag = this.ExcludeExpenseFlag;

            if (string.IsNullOrEmpty(objHist.AggLimitChgdBy))
            {
                objHist.AggLimitChgdBy = this.Context.RMUser.LoginName;
            }
        }

        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "ClaimXPolDedAggLimitHistList":
                    if (isNew)
                    {
                        //BSB 09.21.2004 Fix for query looking for "last change id" run against the list during save.
                        //Most child list items do not need their filter set before calling save.
                        (childValue as ClaimXPolDedAggLimitHistList).SQLFilter = String.Format("{0}={1}", "CLAIM_X_POL_DED_AGG_LIMIT_ID", this.ClaimXPolDedAggLimitId);
                        foreach (ClaimXPolDedAggLimitHist item in (childValue as ClaimXPolDedAggLimitHistList))
                            item.ClaimXPolDedAggLimitId = this.ClaimXPolDedAggLimitId;
                        this.UpdateHistory();
                    }
                    (childValue as IPersistence).Save();
                    break;
            }
        }

        public ClaimXPolDedAggLimitHistList ClaimXPolDedAggLimitHistList
        {
            get
            {
                ClaimXPolDedAggLimitHistList objList = base.m_Children["ClaimXPolDedAggLimitHistList"] as ClaimXPolDedAggLimitHistList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = String.Format("{0}={1}", "CLAIM_X_POL_DED_AGG_LIMIT_ID", this.ClaimXPolDedAggLimitId);
                return objList;
            }
        }
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            this.UpdateHistory();
            base.Save();
        }

        internal ClaimXPolDedAggLimit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "CLAIM_X_POL_DED_AGG_LIMIT";
            this.m_sKeyField = "CLAIM_X_POL_DED_AGG_LIMIT_ID";
            this.m_sFilterClause = string.Empty;

            base.InitFields(sFields);
            base.InitChildren(sChildren);
            base.Initialize();
        }

        
    }
}
