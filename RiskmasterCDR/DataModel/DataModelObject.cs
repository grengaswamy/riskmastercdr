/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/20/2014 | RMA-1346  | nshah28   | changes for ActivityLog
 **********************************************************************************************/

using System;
using System.Data;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Scripting;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;   // JP 10.06.2005   For ReaderWriterLock class.
using System.Text;
using System.Collections.Generic;
using System.Linq;
namespace Riskmaster
{
	namespace DataModel
	{

		// This attribute is only valid on a class.
		// It is used only to flag the field to be presented as "Summary" information about the current instance.
		// Consumed by the base object property get procedure for "Summary"
		[AttributeUsage(AttributeTargets.Class)]
		public sealed class SummaryAttribute : System.Attribute
		{
			public string DefaultProperty;
			public string TableName;
			public string KeyFieldName;
			//internal SummaryAttribute(string DefaultProp)	{this.DefaultProp = DefaultProp;}
			internal SummaryAttribute(string TableName, string KeyFieldName)	{this.TableName = TableName;this.KeyFieldName = KeyFieldName;}
			internal SummaryAttribute( string TableName, string KeyFieldName,string DefaultProp)	{this.DefaultProperty = DefaultProp; this.TableName = TableName;this.KeyFieldName = KeyFieldName;}
		}

		// This attribute is only valid on a method or property.
		// It is used to flag the field as an "Extended Type" from the point of view of 
		// presentation level services.  This means it may have additional relevant information 
		// that is not returned as part of the resultant data.  
	
		//For example: 
		//	1.) A codeid field returns only an integer id while it still has an associated code table 
		// (needed to help with generating input selections).
		// 2.) A codeid should also (if being presented to the user) be shown as a string.  Therefore,
		// we need to be able to distinguish a codeid from an arbitrary integer field value and behave
		// differently.
		[AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
		public sealed class ExtendedTypeAttribute : System.Attribute
		{
			public RMExtType FieldType;
			public string TableName;
			public string ClassName;
            public string RenderHint;
            public bool IsMultiAllowed;
            // npadhy Jira - 6415. This property will be used to set if the UserLookup is Single or Multiple
            // If this property is true then it is user else group
			public ExtendedTypeAttribute( RMExtType FieldType)	{Init(FieldType,null,null);}
            public ExtendedTypeAttribute(RMExtType FieldType, string Detail, bool IsMultiAllowed = true) { Init(FieldType, Detail, null, IsMultiAllowed); }
            public ExtendedTypeAttribute(RMExtType FieldType, string Detail,string RenderHint){Init(FieldType,Detail,RenderHint);}

            private void Init(RMExtType FieldType, string Detail,string RenderHint, bool IsMultiAllowed = true)
            {
                this.FieldType = FieldType;
                if (Detail != null)
                {
                    switch (this.FieldType)
                    {
                        case RMExtType.ChildLink:
                            this.ClassName = Detail;
                            break;
                        default:
                            this.TableName = Detail;
                            break;
                    }
                }
                if (RenderHint != null)
                    this.RenderHint = RenderHint;

                this.IsMultiAllowed = IsMultiAllowed;
            }
		}
		public enum RMExtType: int
		{
			Code = 1,
			CodeList = 2,
			Entity = 3,
			EntityList = 4,
			OrgH  = 5,
			ChildLink  = 6,
            //Anu Tennyson For MITS 18229 - UAR List Starts
            UARListAL = 7,
            UARListPC = 8,
            //Anu Tennyson For MITS 18229 - UAR List Ends
            EntityAddr = 9, //Added by Amitosh for r8 enhancement of Salvage //sharishkumar Jira 6415(have put comma)
            SysUser = 10,//sharishkumar Jira 6415
            Hyperlink = 11 //WWIG GAP20A - agupta298 - MITS 36804 - JIRA-4691
		}

		public enum DataObjectState: int
		{
			IsReady = 0,
			IsPopulating = 1,
			IsSerializing = 2,
			IsSaving = 4
		}

		#region Event Plumbing Code
		//**************************************************************
		// Script Event Plumbing Classes & Code
		//**************************************************************

		//EVENT ARGUMENT CLASSES
		// (No Data Currently Passed with these events..)
		public class InitObjEventArgs :EventArgs
		{
			public InitObjEventArgs() {	} //No data 
		}

        /// <summary>
        /// event argument for InitDataEvent. MITS 12028
        /// </summary>
        public class InitDataEventArgs : EventArgs
        {
            public InitDataEventArgs() { }
        }
		public class CalculateDataEventArgs :EventArgs
		{
			public CalculateDataEventArgs() {	} //No data 
		}
		public class ValidateEventArgs :EventArgs
		{
			public ValidateEventArgs() {	} //No data 
		}
		public class BeforeSaveEventArgs :EventArgs
		{
			public BeforeSaveEventArgs() {	} //No data 
		}
		public class AfterSaveEventArgs :EventArgs
		{
			public AfterSaveEventArgs() {	} //No data 
		}
        public class BeforeDeleteEventArgs : EventArgs
        {
            public BeforeDeleteEventArgs() { } //No data 
        }


		//EVENT DELEGATES
		public delegate void PreInitHandler(object sender, InitObjEventArgs e);
		public delegate void InitObjEventHandler(object sender, InitObjEventArgs e);
        public delegate void InitDataEventHandler(object sender, InitDataEventArgs e);
		public delegate bool CalculateDataEventHandler(object sender,CalculateDataEventArgs e);
		public delegate bool ValidateEventHandler(object sender,ValidateEventArgs e);
		public delegate bool BeforeSaveEventHandler(object sender, BeforeSaveEventArgs e);
		public delegate void AfterSaveEventHandler(object sender,AfterSaveEventArgs e);
        public delegate bool BeforeDeleteEventHandler(object sender, BeforeDeleteEventArgs e);
		//public delegate void OnChildItemAddedEventHandler(string childName, IDataModel childValue);

		#endregion
	

		//Holds methods\properties required by the
		// factory object creation process.
		public  interface IDataModel
		{
			void Initialize(bool isLocked, Context context);
			IDataModel Parent{get;set;}
			bool Locked{get;set;}
			bool IsStale{get;set;}
			Context Context{get;set;}
			bool DataChanged{get;set;}
			bool AnyDataChanged{get;}
			DataObjectState DataObjectState{get;set;} 

		}
	
		public interface INavigation
		{
			// E V E N T S
			event InitObjEventHandler InitObj;
			// P R O P E R T I E S
			string Filter{get;set;}
			// M E T H O D S
			//		 void MoveAndLoad(int moveDirection);
			//		  void LoadData();
			void MoveFirst(); 
			void MoveLast(); 
			void MoveNext();
			void MovePrevious();
			void MoveTo( params int[] keyValue);
		}
		public interface IPersistence
		{
			// E V E N T S
			event InitObjEventHandler InitObj;
			event CalculateDataEventHandler CalculateData;
			event ValidateEventHandler ValidateData;
			event BeforeSaveEventHandler BeforeSave;
			event AfterSaveEventHandler AfterSave;
			// P R O P E R T I E S
			bool IsNew{get;}
			bool DataChanged{get;set;}
			bool AnyDataChanged{get;}

			// M E T H O D S
			void Delete();
			void Save();
			void Refresh();
			bool Validate();
			//		  void AddNew();
		}


		public class DataRoot : IDisposable, IDataModel
		{
			protected bool m_isLocked;
			private bool m_isDataChangedFlagLocked=false;//BSB Fix for linkages tripping dirty flag and causing blank records to save.

			// BSB This was paranoia from the old COM Ref counting days.
			// GC Algo starts walking references at "roots"
			// and will not be affected adversely by circular references.
			// See MSDN Magazine Article at:
			// http://msdn.microsoft.com/msdnmag/issues/1100/GCI/default.aspx
			// for details.
			// Was allowing parent to be GC'd at innappropriate times and causing data loss.
			//protected WeakReference m_Parent;
			protected Object m_Parent;
			protected string m_sParentClassName;
			private Riskmaster.DataModel.Context m_Context;
			internal bool m_DataChanged;
			protected bool m_isStale;
			// JP 10.10.2005   protected bool m_isParentStale = true;
			internal bool m_isParentStale = true;   // JP 10.10.2005
			internal DataObjectState m_DataModelState = DataObjectState.IsReady;
			

			// C O N S T A N T S
			public const int MOVE_FIRST = 1;
			public const int MOVE_LAST= 2;
			public const int MOVE_NEXT= 3;
			public const int MOVE_PREVIOUS = 4;
			public const int TRANSCMD_BEGIN = 1;
			public const int TRANSCMD_COMMIT = 2;
			public const int TRANSCMD_ABORT = 3;
			public const int TRANSCMD_COMMITALL = 4;
		
			internal DataRoot(bool isLocked,Context context){(this as IDataModel).Initialize(isLocked, context);}

			void IDataModel.Initialize(bool isLocked, Context context)
			{
				m_isLocked = isLocked;
				m_Context = context;
			}

            //MITS 12028 If this is the object where custom script should be called
            private int m_firingScriptFlag = 0;
            /// <summary>
            /// Is this object correspoding to the screen
            /// </summary>
            public int FiringScriptFlag
            {
                get { return m_firingScriptFlag; }
                set { m_firingScriptFlag = value; }
            }
            private bool m_isValidationScriptCalled = false;
            /// <summary>
            /// Check if Validation custom script has already been called. Try to avoid
            /// calling it twice.
            /// </summary>
            public bool IsValidationScriptCalled
            {
                get { return m_isValidationScriptCalled; }
                set { m_isValidationScriptCalled = value; }
            }
            
            public virtual void InitializeScriptData() { }

			DataObjectState IDataModel.DataObjectState
			{
				get{return m_DataModelState;}
				set{m_DataModelState = value;}
			}
			//Make a simple "Best Effort" attempt to load the parent of this object.
			public virtual bool LoadParent()
			{
				if( m_Parent !=null  && !this.m_isParentStale)
					return true;

				if(String.IsNullOrEmpty(m_sParentClassName))
					return false;
				
				DataObject child = this as DataObject;
				if(child==null)
					return false;
                				
				DataObject parent = this.Context.Factory.GetDataModelObject(m_sParentClassName,true) as DataObject;
                //*************************************Beginning of the FIX for MITS # 12143
                String foreignKey;

                if (this is CmXAccommodation)
                    foreignKey = "CM_ROW_ID";
                else if (this is CaseMgrNotes)
                    foreignKey = "CASEMGR_ROW_ID";
                else
                    foreignKey = parent.KeyFieldName;

                int keyValue = Conversion.ConvertObjToInt(child.GetField(foreignKey), this.Context.ClientId);
                //*************************************End of the FIX for MITS # 12143
				if(keyValue ==0)
					return false;
				
				parent.MoveTo(keyValue);
				this.Parent = parent;
				m_isParentStale = false;
				return true;
			}
			
			//Note:  Uses WeakReference to break circular references with children.
			public virtual IDataModel Parent
			{
				get
				{
					
					if( m_Parent !=null)
					{
						if(this.m_isParentStale) //BSB Fix for not reloading stale parents.
						{
							try //To Navigate the Parent Record Properly.
							{
								DataObject parent = (m_Parent as DataObject);
                                int keyValue = Conversion.ConvertObjToInt((this as DataObject).GetField(parent.KeyFieldName), this.Context.ClientId);
								
								if(keyValue ==0)
									return null;
				
								parent.MoveTo(keyValue);
								this.m_isParentStale =false;
							}
							catch
							{return null;}	
						}
						return (m_Parent as IDataModel);
					}
					return null;
				}
				set
				{
						m_Parent = value;
				}
			}
            //avipinsrivas Start : Worked for Jira-340
            public bool SkipRecordNotFoundError { get; set; }
            //avipinsrivas End
			public bool Locked{get{return m_isLocked;}set{m_isLocked = true;}}
            public Context Context
            {
                get { return m_Context; }
                set
                {
                    if (m_Context == null)
                        m_Context = value;

                    //BSB Allow for "deactivation" of supp fields to fix Context leak during caching.
                    if (this is SupplementalField && value == null)
                        m_Context = null;
                }
            }
			bool  IDataModel.IsStale
			{
				get{return m_isStale;}
				set{
					m_isStale=value;
//					if(value && this.GetType().Name=="PersonInvolvedList")
//				System.Diagnostics.Trace.WriteLine("Setting Stale on: " + this.ToString());
				}
			}
			//BSB Fix for object linkages tripping dirty flag.
			public bool IsDataChangedFlagLocked(){return m_isDataChangedFlagLocked;}
			internal virtual void LockDataChangedFlag(){m_isDataChangedFlagLocked =true;}
			internal virtual void UnlockDataChangedFlag(){m_isDataChangedFlagLocked =false;}
			public virtual bool DataChanged{get{return m_DataChanged;}set{if(!IsDataChangedFlagLocked())m_DataChanged =value;}}
			public virtual bool AnyDataChanged{get{return m_DataChanged;}}
			
			protected void EnforceLock(bool isInternal)
			{
				if(! isInternal)
					if(this.Locked)
                        throw new NavigationLockedException(Globalization.GetString("DataModelObject.EnforceLock.Exception.NavigationLocked", this.Context.ClientId));
			}
			#region Work In Progress
			// BSB 1.19.2006 Hacked to protect DataChanged flag during population of 
			// "easily identifiable" foriegn key field values.
			internal bool PopulateScalar(DataObject objTarget, string sProp, XmlElement vValue)
			{
				bool bLockedDataChangedFlag=false;
				DataObject objParent=null;
				string strValue="";
				try
				{
					object oProp = objTarget.GetProperty(sProp);
					DataSimpleList lst = oProp as DataSimpleList;
					Trace.WriteLine("Populating Scalar: " + sProp);

					if(lst!=null) //It's a DataSimpleList
					{
						lst.ClearAll();
						foreach(string sCodeId in vValue.Attributes["codeid"].Value.ToString().Split(' '))
							if(Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0) //BSB 01.25.2005 don't add zero value code ids.
								lst.Add(Conversion.IsNumeric(sCodeId) ? Int32.Parse(sCodeId): 0);
						return true;
					}
				

					if(vValue.HasAttribute("codeid"))
						strValue=vValue.Attributes["codeid"].Value;
					else
						strValue=vValue.InnerText;
					
					//BSB Hack for "new record id's" floating. Primarily happens on claim and children of
					// claim.  Basically if the incoming value indicates the incoming object is still "new"
					// then don't let the assignment affect the object's data changed status.
					// Fix for assignment of "-1" to a "0" key field causing extra (blank) record saves.
					
					objParent =(this.m_Parent as DataObject);
					if(objParent!=null)
					{
						//Try to identify parent to child key linkages by matching field name.  
						// Not perfect but probably good enough to solve this problem.
						bLockedDataChangedFlag = (objParent.KeyFieldName==objTarget.PropertyNameToField(sProp));
						if(bLockedDataChangedFlag) //Lock datachanged for "new id" changing to another "new id".
							bLockedDataChangedFlag = (Int32.Parse(strValue) <=0);
					}
					if(bLockedDataChangedFlag)
						objTarget.LockDataChangedFlag();

					objTarget.SetProperty(sProp,strValue);

				
					//							//TODO Special Case for bad Db Table Design on Violations.
					//							EmpXViolationList objViolationList	=vValue as EmpXViolationList;
					//							if(objViolationList !=null)
					//								foreach(EmpXViolation o in objViolationList)
					//								{
					//									Context.InternalSettings.CacheFunctions.GetCodeInfo(o.ViolationCode,ref sTmp1,ref sTmp2);
					//
					//									xmlItem = dom.CreateElement("Item");
					//									xmlItem.SetAttribute("value", o.ViolationCode.ToString());
					//									xmlItem.InnerXml	= CData((sTmp1	+ " "	+	sTmp2).Trim());
					//									xmlElem.AppendChild(xmlItem);
					//								}
					return true;
				}
				finally
				{
					if(bLockedDataChangedFlag)
						objTarget.UnlockDataChangedFlag();
				}
			}
			#endregion
			
			
			//Takes 
			//1.) Extended Type information
			//2.) The Object to be serialized 
			//3.) The property name under which is is to be serialized.
			// Returns information from vValue (Scalar\SimpleList) serialized as an xml string with
			// sufficient intance detail to drive a UI X-Form... (Or at least that's the goal)
            // BSB 11.14.2007 Switch to StringBuilder for performance.
			internal string SerializeScalar(ExtendedTypeAttribute ext, string sProp, object vValue)
			{
				string sTmp1="";
				string sTmp2="";
				//string sOutXML = "";  // JP 10-05-2005     Output XML string
                System.Text.StringBuilder sb = new System.Text.StringBuilder(1024);
				
				if(vValue ==null)
					vValue= "";

				/*** STEP 1 ***/
				/*** Prepare XML tags w/o user data or values***/
			
				// Start XML - leave element open so code can add attributes    JP 10-05-2005
                sb.Append("<");
                sb.Append(SerializationContext.XML_NAMESPACE);
                sb.Append(sProp);	

				//Scalar Field	Value
				if(ext ==null)		
				{
					/*** Simple List***/
					DataSimpleList objSimpleList = vValue as DataSimpleList;
					if(objSimpleList != null)
					{
						sb.Append(" tablename=\"\" codeid=\"");
                        sb.Append(objSimpleList.ToString());
                        sb.Append("\">");

						int i = 0;
						foreach(DictionaryEntry obj in objSimpleList)
						{
							i = Int32.Parse(obj.Value.ToString());
                            sb.Append("<Item value=\"");
                            sb.Append(i.ToString());
                            sb.Append("\">");
                            sb.Append(CData(GetCodeString("", i)));
							sb.Append("</Item>");
						}
					}
					else
					{
						sb.Append(">");					
						sb.Append(CData(vValue.ToString()));
					}
				}//RMExtended	Type
				else 
				{
					switch(ext.FieldType)
					{
						case RMExtType.Code:
                            sb.Append(" tablename=\"");
                            sb.Append(GetAttSafeString(ext.TableName));
                            sb.Append("\" codeid=\"");
                            sb.Append(vValue.ToString());
                            sb.Append("\">");
							sb.Append(GetXmlSafeCodeString(ext.TableName,Int32.Parse(vValue.ToString())));
							break;
						case RMExtType.Entity:
							sTmp1 = "";
                            if (Conversion.IsNumeric(vValue) && vValue.ToString().Length > 0)
                            {
                                //sTmp1 = "";
                                if (ext.RenderHint == Constants.RenderHints.PREFIX_ENTITY_ABBR)
                                    sTmp1 = Context.LocalCache.GetEntityAbbreviation(Int32.Parse(vValue.ToString()));
                                sTmp1 += Context.LocalCache.GetEntityLastFirstName(Int32.Parse(vValue.ToString()));
                            }
                            else
                                vValue = 0;

                            //sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">",GetAttSafeString(ext.TableName),vValue.ToString());
                            
                            //rsolanki2: Append tends to be faster than Appendformat 
                            sb.Append(" tablename=\"");
                            sb.Append(GetAttSafeString(ext.TableName));
                            sb.Append("\" codeid=\"");
                            sb.Append(vValue.ToString());
                            sb.Append("\">");

							sb.Append(CData(sTmp1.Trim()));

							break;
						case RMExtType.CodeList: //Expects children of the form: <item value="id#">My String Representation</item>
                            //sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">", GetAttSafeString(ext.TableName), vValue.ToString());

                            //rsolanki2: Append tends to be faster than Appendformat 
                            sb.Append(" tablename=\"");
                            sb.Append(GetAttSafeString(ext.TableName));
                            sb.Append("\" codeid=\"");
                            sb.Append(vValue.ToString());
                            sb.Append("\">");
                            //sOutXML += " tablename=\"" + GetAttSafeString(ext.TableName) + "\" codeid=\"" + vValue.ToString() + "\">";	// JP 10-05-2005   Rewrite to not use DOM.

						{ //Process Child Item Values

							//Special Case for bad Db Table Design on Violations.
							EmpXViolationList objViolationList	=vValue as EmpXViolationList;
							if(objViolationList !=null)
								foreach(EmpXViolation o in objViolationList)
								{
									Context.InternalSettings.CacheFunctions.GetCodeInfo(o.ViolationCode,ref sTmp1,ref sTmp2);

                                    sb.AppendFormat("<Item value=\"{0}\">",o.ViolationCode.ToString());	
									sb.Append(CData((string.Concat(sTmp1	, " "	,	sTmp2)).Trim()));				
									sb.Append("</Item>");											
								}
							else
							{
								DataSimpleList objList = vValue as DataSimpleList;
								int i = 0;
                                //added by rkaur7 to show comma separated states on 06/03/2009- MITS 16668
                                int iListCount = 0;
								foreach(DictionaryEntry obj in objList)
								{
                                    //rkaur7
                                    iListCount+=1;
                                    i = Int32.Parse(obj.Value.ToString());
                                    sb.AppendFormat("<Item value=\"{0}\">", i.ToString());
									sb.Append(GetXmlSafeCodeString(ext.TableName,i));
                                    //By rkaur7
                                    if (objList.Count > iListCount) 
                                        sb.Append(", ");
                                    //code end rkaur7
									sb.Append("</Item>");
								}
							}
						} //End of Processing Child Items
							break;
						case RMExtType.EntityList: //Expects children of the form: <item value="id#">My String Representation</item>
						{
                            //sOutXML += " tablename=\"" + GetAttSafeString(ext.TableName) + "\" codeid=\"" + vValue.ToString() + "\">";	
                            sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">",GetAttSafeString(ext.TableName), vValue.ToString());

							DataSimpleList objList = vValue as DataSimpleList;
							int i = 0;
							foreach(DictionaryEntry obj in objList)
							{
								i = Int32.Parse(obj.Value.ToString());
								sTmp1 = "";
                                sb.AppendFormat("<Item value=\"{0}\">", i.ToString());
                               //MGaba2:MITS 26249:Adding Abbreviation 
                                if (ext.RenderHint == Constants.RenderHints.PREFIX_ENTITY_ABBR)
                                    sTmp1=Context.LocalCache.GetEntityAbbreviation(i) + "-";

                                sTmp1 += Context.LocalCache.GetEntityLastFirstName(i);
                                sb.Append(CData(sTmp1));
                                //sb.Append(CData(Context.LocalCache.GetEntityLastFirstName(i)));
								//MGaba2:MITS 26249:Adding Abbreviation 
								sb.Append("</Item>");
							}
						}
							break;
						case RMExtType.OrgH:
                            //sOutXML += " tablename=\"" + GetAttSafeString(ext.TableName) + "\" codeid=\"" + vValue.ToString() + "\">";
                            sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">", GetAttSafeString(ext.TableName), vValue.ToString());

							//Fetch UI Info
							sTmp1 = ""; sTmp2 = "";
							Context.InternalSettings.CacheFunctions.GetOrgInfo(Int32.Parse(vValue.ToString()),ref sTmp1,ref sTmp2);
							
							if(!String.IsNullOrEmpty(sTmp1) || !String.IsNullOrEmpty(sTmp2))
								//mjain8 Removed extra space as it was showing space in aspx control
                                sb.Append(CData(sTmp1 + " - " + sTmp2));

							break;
						
						case RMExtType.ChildLink:  //BSB Added to support UI strings for "link" fields that
												   // represent a "child object" but where that child may not
												   // be implemented as an in memory sub-object.
												   // Example: Physician.PolicyId would contain 
													// the policynumber in an attribute so that the UI can show
													// appropriate user friendly strings (class.default)
							//Fetch the "defaultdetail" attribute value.
							if(	Conversion.IsNumeric(vValue)  && vValue.ToString().Length >0)
								sTmp1 = Context.GetRecordDefault(ext.ClassName,Int32.Parse(vValue.ToString()));
							else
								vValue = 0;

							//Apply the defaultDetail attribute value.
							sb.AppendFormat(" defaultdetail=\"{0}\">",GetAttSafeString(sTmp1));
							sb.Append(CData(vValue.ToString()));

							break;
                       //Anu Tennyson for MITS 18229 Starts UAR List
                        case RMExtType.UARListAL: //Expects children of the form: <item value="id#">My String Representation</item>
                            {
                                sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">", GetAttSafeString(ext.TableName), vValue.ToString());

                                DataSimpleList objList = vValue as DataSimpleList;
                                int i = 0;
                                foreach (DictionaryEntry obj in objList)
                                {
                                    i = Int32.Parse(obj.Value.ToString());
                                    sTmp1 = "";
                                    sb.AppendFormat("<Item value=\"{0}\">", i.ToString());
                                    sb.Append(CData(Context.LocalCache.GetUARDetailVIN(i,false)));
                                    sb.Append("</Item>");
                                }
                            }
                            break;
                       //Anu Tennyson for MITS 18229 Ends UAR List
                        //Anu Tennyson for MITS 18229 Starts UAR List
                        case RMExtType.UARListPC: //Expects children of the form: <item value="id#">My String Representation</item>
                            {
                                sb.AppendFormat(" tablename=\"{0}\" codeid=\"{1}\">", GetAttSafeString(ext.TableName), vValue.ToString());

                                DataSimpleList objList = vValue as DataSimpleList;
                                int i = 0;
                                foreach (DictionaryEntry obj in objList)
                                {
                                    i = Int32.Parse(obj.Value.ToString());
                                    sTmp1 = "";
                                    sb.AppendFormat("<Item value=\"{0}\">", i.ToString());
                                    sb.Append(CData(Context.LocalCache.GetUARDetailPIN(i)));
                                    sb.Append("</Item>");
                                }
                            }
                            break;
                        //Anu Tennyson for MITS 18229 Ends UAR List
                        //Added by Amitosh for R8 enhancement of Salvage
                        case RMExtType.EntityAddr:
                            sTmp1 = "";
                              string sAddr1=null;
            string sAddr2= null;
            string sAddr3 = null;
            string sAddr4 = null;
            string sCity= null;
            string sCounty= null;
            string sCountry= null;
            string sZipCode= null;
            string sState= null;
                            if (Conversion.IsNumeric(vValue) && vValue.ToString().Length > 0)
                            {
                                Context.LocalCache.GetAddressInFo(Int32.Parse(vValue.ToString()), ref sAddr1, ref sAddr2, ref sAddr3, ref sAddr4, ref sCity, ref sCounty, ref sState, ref sCountry, ref sZipCode);
                                sTmp1 = sAddr1 + " " + sAddr2 + " " + sAddr3 + " " + sAddr4 + " " + sCity + " " + sCounty + " " + sState + " " + sCountry + " " + " " + sZipCode;
                            }
                            else
                                vValue = 0;

                       
                            sb.Append(" codeid=\"");
                            sb.Append(vValue.ToString());
                            sb.Append("\">");

							sb.Append(CData(sTmp1.Trim()));

							break;
                            //end Amitosh

                        //npadhy Jira 6415 Retrive Proper Values for User and Group
                        case RMExtType.SysUser:
                            if (ext.IsMultiAllowed)
                            {
                                sb.Append(" tablename=\"");
                                sb.Append(GetAttSafeString(ext.TableName));
                                sb.Append("\" codeid=\"");
                                sb.Append(vValue.ToString());
                                sb.Append("\">");
                                DataSimpleList objUsers = vValue as DataSimpleList;
                                int value = 0;
                                int count = 0;
                                foreach (DictionaryEntry obj in objUsers)
                                {
                                    //rkaur7
                                    count += 1;
                                    value = Int32.Parse(obj.Value.ToString());
                                    sb.AppendFormat("<Item value=\"{0}\">", value.ToString());
                                    sb.Append(GetXmlSafeCodeString(ext.TableName, value, true));
                                    //By rkaur7
                                    if (objUsers.Count > count)
                                        sb.Append(", ");
                                    //code end rkaur7
                                    sb.Append("</Item>");
                                }
                            }
                            else
                            {
                                if (!(Conversion.IsNumeric(vValue) && vValue.ToString().Length > 0))
                                {
                                    vValue = 0;
                                }
                                sb.Append(" tablename=\"");
                                sb.Append(GetAttSafeString(ext.TableName));
                                sb.Append("\" codeid=\"");
                                sb.Append(vValue.ToString());
                                sb.Append("\">");

                                sb.Append(GetXmlSafeCodeString(ext.TableName, Int32.Parse(vValue.ToString()), true));
                            }
							break;
                        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA-4691
                        case RMExtType.Hyperlink:
                            sb.Append(">");
                            sb.Append(CData(vValue.ToString()));
                            break;
                        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA-4691
					}

	
				}
				//BSB 01.25.2005 Hack to add in empty item if needed for orbeon itemset usage.
				if(	 vValue is  DataSimpleList || vValue is EmpXViolationList)
				{
					DataSimpleList  oList = vValue as DataSimpleList;
					EmpXViolationList  oVList = vValue as EmpXViolationList;
					bool bEmpty = false;
					bEmpty = (oList==null && oVList.Count == 0);
					bEmpty = bEmpty || (oVList==null && oList.Count == 0);
					if(bEmpty)
					{
						sb.Append("<Item value=\"0\">");
                        sb.Append(CData(CommonFunctions.FilterBusinessMessage(Globalization.GetString("DataRoot.SerializeScalar.NoneSelected",this.Context.ClientId)))); //MITS 31060
						sb.Append("</Item>");
					}
				}

				// Close XML
				sb.AppendFormat("</{0}{1}>", SerializationContext.XML_NAMESPACE, sProp);

				return sb.ToString();

				//
				//			/*** STEP 2 ***/
				//		else if( sType	== POLICYNUMBER_FLDTYPE)
				//		{
				//			xmlElem.InnerText =	Context.DbConnLookup.ExecuteString("SELECT POLICY_NAME FROM POLICY	WHERE POLICY_ID=" +	vValue);
				//			xmlElem.SetAttribute(ATT_CODEID,	 vValue.ToString());
				//		}
				//		else if( sType	== PLANNUMBER_FLDTYPE)
				//		{
				//				
				//			xmlElem.InnerText =	Context.DbConnLookup.ExecuteString("SELECT PLAN_NAME FROM DISABILITY_PLAN WHERE	PLAN_ID=" + vValue);
				//			xmlElem.SetAttribute(ATT_CODEID,	 vValue.ToString());
				//			//xmlElem.SetAttribute("lookup", "lookupclaimplan.asp");	 //TODO
				//		}
				//	else if( sType	==POLICYLOOKUP_FLDTYPE) 
				//{
				//	xmlElem.SetAttribute( "codeid", vValue.ToString());
				//	xmlElem.InnerText =	sValue;
				//}
				//	else if( sType	==PLANLOOKUP_FLDTYPE) 
				//{
				//	xmlElem.SetAttribute( "codeid", vValue.ToString());
				//	xmlElem.InnerText =	sValue;
				//	//xmlElem.SetAttribute( "lookup",	"lookupclaimplan.asp"); //TODO
				//}
				//	else if( sType	==POLICYSEARCH_FLDTYPE)	
				//{
				//	xmlElem.SetAttribute( "codeid", "0");
				//	objPolicy = (Policy) Context.Factory.GetDataModelObject("Policy",false);
				//	try{objPolicy.MoveTo(Int32.Parse(vValue.ToString()));}	
				//	catch{}
				//				
				//	if(objPolicy.PolicyId	== Int32.Parse(vValue.ToString()))
				//{
				//	xmlElem.SetAttribute( "codeid", vValue.ToString());
				//	xmlElem.InnerText =	objPolicy.PolicyName;
				//}
				//}
				//else if(sType ==	ACCOUNTLIST_FLDTYPE)
				//{
				//	xmlElem.SetAttribute(ATT_CODEID,	 vValue.ToString());
				//	sSQL	= "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE	ACCOUNT_ID=" + vValue	+	" ORDER	BY ACCOUNT_ID	";
				//	xmlElem.InnerText =	Context.DbConnLookup.ExecuteString(sSQL);
				//}
				//else
				//{
				//	xmlElem.InnerText =	vValue.ToString();
				//	/*	TODO RM_DEBUG
				//#if RM_DEBUG 
				//								LogError "SetValue", Erl, 3000,	"FormDataManager.SetValue", "Unknown	XML type found:	" & sType
				//#endif*/
				//}
				//			
				//	//change the type to	readonly after	formatting	is complete
				//	if(bReadOnly)
				//	xmlElem.SetAttribute("type",	READONLY_FLDTYPE);
			}
			public string GetXmlSafeCodeString(string tableName, int codeId, bool shortCodeOnly = false)
			{
                return CData(GetCodeString(tableName, codeId, shortCodeOnly));
			}
			public string GetAttSafeString(string tableName)
			{
				return  tableName.Replace("&","&amp;").Replace("'","&apos;");
			}
			internal string CData(string s) 
            {
                // //rsolanki2: replacing with string.Concat which is faster than string.format
                return String.Concat("<![CDATA["
                    , s
                    ,"]]>");

                //return String.Format("<![CDATA[{0}]]>",s);
            }           
            public string GetCodeString(string tableName, int codeId)
            {
                //string sName = tableName.ToLower();
                string sTmp1 = "", sTmp2 = "", sSQL;
                Riskmaster.Db.DbReader objReader;

                if (tableName.Equals("states", StringComparison.OrdinalIgnoreCase))
                {
                    this.Context.InternalSettings.CacheFunctions.GetStateInfo(codeId, ref	sTmp1, ref sTmp2);
                    //return (sTmp1 + " " + sTmp2);
                    return (string.Concat(sTmp1, " ", sTmp2));

                }
                else if (tableName.Equals("rm_sys_users", StringComparison.OrdinalIgnoreCase))
                {
                    //sSQL = "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME";
                    //sSQL += "	FROM USER_TABLE,USER_DETAILS_TABLE	WHERE USER_TABLE.USER_ID	=	USER_DETAILS_TABLE.USER_ID";
                    //sSQL += "	AND USER_DETAILS_TABLE.USER_ID = " + codeId;
                    //        //rsolanki2: replacing with string.Concat
                    sSQL = string.Concat("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
                        , "	FROM USER_TABLE,USER_DETAILS_TABLE	WHERE USER_TABLE.USER_ID	=	USER_DETAILS_TABLE.USER_ID"
                        , "	AND USER_DETAILS_TABLE.USER_ID = "
                        , codeId);

                    objReader = Riskmaster.Db.DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId), sSQL);
                    if (objReader.Read())
                    {
                        sTmp1 = objReader.GetString("FIRST_NAME");
                        if (sTmp1 != "") sTmp1 += " ";
                        sTmp1 += objReader.GetString("LAST_NAME");
                        return sTmp1 + " " + objReader.GetString("LOGIN_NAME");
                    }
                    objReader.Close();
                    return "";
                }
                // npadhy Jira 6415 Handle the Data for Grouo as well
                else if (tableName.Equals("rm_sys_groups", StringComparison.OrdinalIgnoreCase))
                {
                    sSQL = string.Concat("SELECT GROUP_NAME from USER_GROUPS WHERE GROUP_ID = "
                        , codeId);

                    return this.Context.DbConnLookup.ExecuteString(sSQL);
                }
                //neha goel MITS#34287  Swiss re gap 28--START
                else if (tableName.Equals("RM_SYS_USERS_OVRDRESTCLAIM", StringComparison.OrdinalIgnoreCase))
                {   
                        sSQL = string.Concat("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
                            , "	FROM USER_TABLE,USER_DETAILS_TABLE	WHERE USER_TABLE.USER_ID	=	USER_DETAILS_TABLE.USER_ID"
                            , "	AND USER_DETAILS_TABLE.USER_ID = "
                            , codeId);

                        objReader = Riskmaster.Db.DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId), sSQL);
                        if (objReader.Read())
                        {
                            sTmp1 = objReader.GetString("FIRST_NAME");
                            if (sTmp1 != "") sTmp1 += " ";
                            sTmp1 += objReader.GetString("LAST_NAME");                            
                            string sSQL1 = string.Empty;
                            sSQL1 = "SELECT COUNT(*) FROM CLM_RSTRCTD_USR_OVRD WHERE CLAIM_ID = 0 AND USER_ID=" + codeId;
                            int iOvrdUserCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSQL1), this.Context.ClientId);
                            if (iOvrdUserCount > 0)
                            {
                                sTmp1 += " " + objReader.GetString("LOGIN_NAME");
                                return sTmp1 += " (System Override)";
                            }
                            else
                            {
                                return sTmp1 + " " + objReader.GetString("LOGIN_NAME");
                            }
                        }
                        objReader.Close();
                        return "";                                     
                }  //neha goel MITS#34287  Swiss re gap 29--END
                else if (tableName.StartsWith("glossary_", StringComparison.OrdinalIgnoreCase))
                {
                    //return this.Context.DbConnLookup.ExecuteString("SELECT TABLE_NAME	FROM GLOSSARY_TEXT WHERE	TABLE_ID=" + codeId + " AND LANGUAGE_CODE=1033");
                    return this.Context.DbConnLookup.ExecuteString(string.Concat("SELECT TABLE_NAME	FROM GLOSSARY_TEXT WHERE	TABLE_ID="
                        , codeId
                        , " AND LANGUAGE_CODE=1033"));
                }
                else if (tableName.Equals("orghall", StringComparison.OrdinalIgnoreCase))
                {
                    this.Context.InternalSettings.CacheFunctions.GetOrgInfo(codeId, ref sTmp1, ref sTmp2);
                    //return (sTmp1 + " " + sTmp2);
                    return (string.Concat(sTmp1, " ", sTmp2));

                }
                else
                {
                    this.Context.InternalSettings.CacheFunctions.GetCodeInfo(codeId, ref	sTmp1, ref sTmp2, this.Context.RMUser.objUser.NlsCode);  //Amandeep ML Change                 
                    //return (sTmp1 + " " + sTmp2);                   
                    return (string.Concat(sTmp1, " ", sTmp2));

                }
            }

            // npadhy Start MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen
            /// <summary>
            /// It returns the Short Code if the bShortCodeOnly is true. Otherwise same as GetCodeString
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="codeId"></param>
            /// <param name="bShortCodeOnly"></param>
            /// <returns></returns>
            public string GetCodeString(string tableName, int codeId, bool bShortCodeOnly)
            {
                //string sName = tableName.ToLower();
                if (bShortCodeOnly)
                {
                    string sTmp1 = "", sTmp2 = "", sSQL;
                    Riskmaster.Db.DbReader objReader;

                    if (tableName.Equals("states", StringComparison.OrdinalIgnoreCase))
                    {
                        this.Context.InternalSettings.CacheFunctions.GetStateInfo(codeId, ref	sTmp1, ref sTmp2);
                        return (sTmp1);
                    }
                    else if (tableName.Equals("rm_sys_users", StringComparison.OrdinalIgnoreCase))
                    {
                        //rsolanki2: replacing with string.Concat
                        sSQL = string.Concat( "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
                            , "	FROM USER_TABLE,USER_DETAILS_TABLE	WHERE USER_TABLE.USER_ID	=	USER_DETAILS_TABLE.USER_ID"
                            , "	AND USER_DETAILS_TABLE.USER_ID = " 
                            , codeId);
                        objReader = Riskmaster.Db.DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId), sSQL);
                        if (objReader.Read())
                        {
                            return (objReader.GetString("LOGIN_NAME"));
                        }
                        objReader.Close();
                        return "";
                    }
                    // npadhy Jira 6415 Handle the Data for Grouo as well
                    else if (tableName.Equals("rm_sys_groups", StringComparison.OrdinalIgnoreCase))
                    {
                        sSQL = string.Concat("SELECT GROUP_NAME from USER_GROUPS WHERE GROUP_ID = "
                            , codeId);

                        return this.Context.DbConnLookup.ExecuteString(sSQL);
                    }
                              //neha goel MITS#34287
                    else if (tableName.Equals("RM_SYS_USERS_OVRDRESTCLAIM", StringComparison.OrdinalIgnoreCase))
                    {
                       
                        sSQL = string.Concat("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
                            , "	FROM USER_TABLE,USER_DETAILS_TABLE	WHERE USER_TABLE.USER_ID	=	USER_DETAILS_TABLE.USER_ID"
                            , "	AND USER_DETAILS_TABLE.USER_ID = "
                            , codeId);
                        objReader = Riskmaster.Db.DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId), sSQL);
                        if (objReader.Read())
                        {
                            string sSQL1 = string.Empty;
                            sSQL1 = "SELECT COUNT(*) FROM CLM_RSTRCTD_USR_OVRD WHERE CLAIM_ID = 0 AND USER_ID=" + codeId;
                            int iOvrdUserCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSQL1), this.Context.ClientId);
                            if (iOvrdUserCount > 0)
                            {
                                string sTemp = string.Empty;
                                sTemp = objReader.GetString("LOGIN_NAME") + "(System Override)";
                                return sTemp;                                
                            }
                            else
                            {
                                return (objReader.GetString("LOGIN_NAME"));
                            }
                            
                        }
                        objReader.Close();
                        return "";
                    }
                    else if (tableName.StartsWith("glossary_", StringComparison.OrdinalIgnoreCase))
                    {
                        return this.Context.DbConnLookup.ExecuteString("SELECT TABLE_NAME	FROM GLOSSARY_TEXT WHERE	TABLE_ID=" + codeId + " AND LANGUAGE_CODE=1033");
                    }
                    else if (tableName.Equals("orghall", StringComparison.OrdinalIgnoreCase))
                    {
                        this.Context.InternalSettings.CacheFunctions.GetOrgInfo(codeId, ref sTmp1, ref sTmp2);
                        return (sTmp1);
                    }
                    else
                    {
                        this.Context.InternalSettings.CacheFunctions.GetCodeInfo(codeId, ref	sTmp1, ref sTmp2);
                        return (sTmp1);
                    }
                }
                else
                {
                    return GetCodeString(tableName, codeId);
                }
            }
            // npadhy End MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen
            
            #region IDisposable Members
			public void Dispose()
			{
				// TODO:  Add DataModelRoot.Dispose implementation
			}

			#endregion
		}
		/// <summary>
		/// Base class for all ported RMObjLib classes.
		///Only holds "disconnected" data items.
		/// </summary>
		public class DataObject : DataRoot, INavigation, IPersistence
		{
			internal static int m_NestedDumpLevel = 0;
			//protected static Hashtable m_PropertyDefaultsMap = new Hashtable();  // JP 10.06.2005   Hash table of hash tables that contain default values for various classes. This is being statically cached for performance reasons.
            protected static Hashtable m_PropertyInfoMap = new Hashtable(StringComparer.OrdinalIgnoreCase);  // JP 10.10.2005   Static hash table of PROPERTYINFO classes.
			private static ReaderWriterLock m_PropertyInfoMapLock = new ReaderWriterLock();  // JP 10.06.2005   Lock to protect writes of static PropertyInfo hash table.
            private static Hashtable m_ClassMetaInfoMap = new Hashtable(StringComparer.OrdinalIgnoreCase);  // BSB 01.08.2006   Static hash table of ClassMetaInfo objects.
			private static ReaderWriterLock m_ClassMetaInfoMapLock = new ReaderWriterLock();  // BSB 01.10.06 Lock to protect writes of static MetaInfo.
            private static Hashtable m_DeletionProhibitedChildTypes = new Hashtable(StringComparer.OrdinalIgnoreCase);
			// Allow ppl to discover but not modify the list of Protected Child Types.
			// Currently used by unit-test software to generate CSV describing system deletion behavior.
            // npadhy Start MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen
			public static Hashtable DeletionProhibitedChildTypes
			{
				get{return m_DeletionProhibitedChildTypes;}
			}
            // npadhy End MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen
			//DATA
			protected string m_Summary; //BSB 11.08.2004 Added to denote the property name to check for GUI summary info.
			protected string m_sKeyField;
			protected string m_sTableName;
			protected string m_sFilterClause;
			protected string m_sSQL;
			internal DataFieldList m_Fields=new DataFieldList(); 
			internal DataChildList m_Children = new DataChildList();
			internal Supplementals m_Supplementals = null;
			internal Hashtable m_PropertyMap = new Hashtable(StringComparer.OrdinalIgnoreCase);
			protected string m_sDateTimeStamp = ""; //Holds the timestamp being applied during a save before 
			// the transaction is committed and the in memory record is updated.
			// Used in derived classes (ENTITY) to update the glossary timestamp.
			private bool m_isNew = true;

			private bool m_isActionOwner = false;
			private bool m_isTransactionOwner = false;

			public bool IsActionOwner{get{return m_isActionOwner;}}
			public bool IsTransactionOwner{get{return m_isTransactionOwner;}}

            public bool m_bUseLegacyComments = false;
            private int m_iExistingClaimPolicy = -1;
			
			//EVENTS
			public event InitObjEventHandler InitObj;
            public event InitDataEventHandler InitData;
			public event CalculateDataEventHandler CalculateData;
			public event ValidateEventHandler ValidateData;
			public event BeforeSaveEventHandler BeforeSave;
			public event AfterSaveEventHandler AfterSave;
            public event BeforeDeleteEventHandler BeforeDelete;

            private bool bIsMigrationImport = false;
            public bool IsMigrationImport
            {
                get
                {
                    return bIsMigrationImport;
                }
                set
                {
                    bIsMigrationImport = value;
                }
            }

            private bool bDefaultDateTimeAndUser = true;
            public bool DefaultDateTimeAndUser
            {
                get
                {
                    return bDefaultDateTimeAndUser;
                }
                set
                {
                    bDefaultDateTimeAndUser = value;
                }
            }
            private bool bByPassPostTransCommit = false;
            public bool ByPassPostTransCommit 
            {
                get
                {
                    return bByPassPostTransCommit;
                }
                set
                {
                    bByPassPostTransCommit = value;
                }
            }

            /// <summary>
            /// Raise an event to call custom script Init function
            /// MITS 12028
            /// </summary>
            public override void InitializeScriptData()
            {
                this.InitData(this, new InitDataEventArgs());
            }

			//DEFAULT EVENT HANDLERS  (customize above to spawn scripts if desired....)
			private void InitObjDefaultHandler(object sender, InitObjEventArgs e)
			{
				//BSB 06.09.2006 "Fix the Fix" there are times when the object could 
				// validly be locked by this point so we must honor that.  Fixes collection behavior during save
				// which was noticed affecting Closed Claim Reserve Adjustment.
				bool bWasLocked = m_isLocked;
				//BSB 10.10.2006 Allow for 'PrePopulated' field values before firing Init Script
				//The prime examples of this are from RMWorld where LOB and EventId may be pre-populated on a claim.
				// In that implementation, the "AddNew" routine simply didn't clear the values set by client code
				// before it calls the CInitScript.  That allows the script to potentially make decisions based
				// on these values.
				this.Context.PreInitThrow(this,e);

                //MITS 12028 The Init script should be not call for every object creation. 
                //It should be called only for a new object (a new record in the database)
                //if (this.FiringScriptFlag != 0)
                //{
                //    Context.RunDataModelScript(ScriptTypes.SCRIPTID_INITIALIZE, sender);
                //}
				//BSB 05.26.2006 Scripting Fix - do not allow DataObject to become locked for 
				//navigation during Initialization.
				// 
				this.m_isLocked = bWasLocked;
			}

            private void InitDataDefaultHandler(object sender, InitDataEventArgs e)
            {
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag != 0)
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_INITIALIZE, sender);
                }
            }
            private bool CalculateDataDefaultHandler(object sender, CalculateDataEventArgs e)
			{
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag == 2)
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_CALCULATE, sender);
                    this.FiringScriptFlag++;
                }
                return (Context.ScriptValidationErrors.Count == 0);
			}
			private bool ValidateDefaultHandler(object sender, ValidateEventArgs e)
			{
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag == 3 && (!this.IsValidationScriptCalled))
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_VALIDATE, sender);
                    this.IsValidationScriptCalled = true;
                    this.FiringScriptFlag++;
                }
				return (Context.ScriptValidationErrors.Count == 0);
			}
			private bool BeforeSaveDefaultHandler(object sender, BeforeSaveEventArgs e)
			{
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag == 4)
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_BEFORESAVE, sender);
                    this.FiringScriptFlag++;
                }
                return (Context.ScriptValidationErrors.Count == 0);
			}
			private void AfterSaveDefaultHandler(object sender, AfterSaveEventArgs e)
			{
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag == 5)
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_AFTERSAVE, sender);
                }
			}
            private bool BeforeDeleteDefaultHandler(object sender, BeforeDeleteEventArgs e)
			{
                //MITS 12028 Only call script when the action is originated from the specified screen
                if (this.FiringScriptFlag == 6)
                {
                    Context.RunDataModelScript(ScriptTypes.SCRIPTID_BEFOREDELETE, sender);
                }
                return (Context.ScriptValidationErrors.Count == 0);
			}

			//INNER CLASSES
			public ArrayList SerializationSequence
			{
				get
				{
					this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
					try
					{
						return (ArrayList)this.MetaInfo.SerializationSequence.Clone();
					}
					finally{this.MetaInfo.PropertyLock.ReleaseReaderLock();}
				}

			}
			internal ClassMetaInfo MetaInfo
			{
				get
				{
					string sClassName =this.GetType().Name;
					
					m_ClassMetaInfoMapLock.AcquireReaderLock(-1);
					try
					{
						if(m_ClassMetaInfoMap.ContainsKey(sClassName))
							return m_ClassMetaInfoMap[sClassName] as ClassMetaInfo;
					}
					finally{m_ClassMetaInfoMapLock.ReleaseReaderLock();}
					
					//Not already loaded - must prepare, store and return a valid ClassMetaInfo...
					m_ClassMetaInfoMapLock.AcquireWriterLock(-1);
					try
					{
						// BSB 1.27.2005 
						// Threading Issue Fix -Add another check in case between the end of the read 
						// and the start of the write lock some other thread has 
						// finished populating the Map.
						ClassMetaInfo ret=null;
						if(m_ClassMetaInfoMap.ContainsKey(sClassName))
							ret = m_ClassMetaInfoMap[sClassName] as ClassMetaInfo;
						else
						{
							ret = new ClassMetaInfo(this);
							m_ClassMetaInfoMap.Add(sClassName,ret);
						}
						return ret;

					}
					finally{m_ClassMetaInfoMapLock.ReleaseWriterLock();}


				}
			}
			private DataTable GetSchemaTable()
			{
				DbReader objReader = null;
				try
				{
                    //objReader = base.Context.DbConnLookup.ExecuteReader(String.Format("SELECT * FROM {0} WHERE 1=0", this.m_sTableName));
                    //rsolanki2: replacing with equivalent string.concat
                    objReader = base.Context.DbConnLookup.ExecuteReader(String.Concat("SELECT * FROM "
                        , this.m_sTableName
                        , " WHERE 1=0"
                        ));
					return objReader.GetSchemaTable();
				}
				finally
				{
					if(objReader !=null)
						objReader.Close();
					objReader = null;
				}
			}

			// Get SerializationSequence from any base object(s)
			// Then wait for InitChilren\InitFields to populate the 
			// remaining slots.
			private ArrayList GetBaseSerializationSequence()
			{
//				string callerName=new StackFrame(1,false).GetMethod().ReflectedType.Name;
				bool bAlreadyLocked = false;
				Type b = this.GetType().BaseType;
				
				ArrayList ret = new ArrayList(this.Properties.Count);
				ClassMetaInfo baseClassMetaInfo;
				if(b.Name !="DataObject")
				{
					//Fetch SerializationSequence of Immediate Base object.
					bAlreadyLocked = (m_ClassMetaInfoMapLock.IsReaderLockHeld | m_ClassMetaInfoMapLock.IsWriterLockHeld);
					if(!bAlreadyLocked)
						m_ClassMetaInfoMapLock.AcquireReaderLock(-1);
					try
					{
						baseClassMetaInfo = DataObject.m_ClassMetaInfoMap[b.Name] as ClassMetaInfo;
					}
					finally{if(!bAlreadyLocked)m_ClassMetaInfoMapLock.ReleaseReaderLock();}

					if(baseClassMetaInfo !=null) //Apply BaseSerializationSequence (to be extended by our own InitXXX routines.)
					{
						baseClassMetaInfo.PropertyLock.AcquireReaderLock(-1);
						try
						{
							ret = baseClassMetaInfo.SerializationSequence.Clone() as ArrayList;
						}finally{baseClassMetaInfo.PropertyLock.ReleaseReaderLock();}
					}// Otherwise: we're being invoked on the way 
					// to building a derived class before the base class has 
					// been added yet.  Let the Initxxx routines run in order and this should be fine...
				}
				return ret;
			}

			private Hashtable GetPropertyMetaInfoMap()
			{
				bool bAlreadyLocked = false;
				Type b = this.GetType().BaseType;
				Hashtable ret = new Hashtable(this.Properties.Count);
				ClassMetaInfo baseClassMetaInfo;
				if(b.Name !="DataObject")
				{
					//Fetch SerializationSequence of Immediate Base object.
					bAlreadyLocked = (m_ClassMetaInfoMapLock.IsReaderLockHeld | m_ClassMetaInfoMapLock.IsWriterLockHeld);
					if(!bAlreadyLocked)
						m_ClassMetaInfoMapLock.AcquireReaderLock(-1);
					try
					{
						baseClassMetaInfo = DataObject.m_ClassMetaInfoMap[b.Name] as ClassMetaInfo;
					}finally{if(!bAlreadyLocked)m_ClassMetaInfoMapLock.ReleaseReaderLock();}

					//Apply Base PropertyMetaInfoMap (to be extended by our own InitXXX routines.)
					if(baseClassMetaInfo !=null) //Apply PropertyMetaInfoMap (to be extended by our own InitXXX routines.)
					{
						baseClassMetaInfo.PropertyLock.AcquireReaderLock(-1);
						try
						{
							ret = baseClassMetaInfo.PropertyMetaInfoMap.Clone() as Hashtable;
						}finally{baseClassMetaInfo.PropertyLock.ReleaseReaderLock();}

					}// Otherwise: we're being invoked on the way 
					// to building a derived class before the base class has 
					// been added yet.  Let the Initxxx routines run in order and this should be fine...
				}
				return ret;
				
			}
			public class ClassMetaInfo
			{
				private ReaderWriterLock m_PropertyLock= new ReaderWriterLock();
				public ReaderWriterLock PropertyLock{get{return m_PropertyLock;}}

				//private DataObject m_buildFrom = null;
				//private DataTable m_SchemaTable=null;
                private Hashtable m_SchemaTableMap= null;
				private ArrayList m_SerializationSequence =null;//SerializationSequence (ArrayList of PropertyMetaInfo)
				private Hashtable m_PropertyMetaInfoMap = null; //Hash of PropertyMetaInfo(s) on PropertyName
				private Hashtable m_PropertyDefaultsMap=null;//Hash of PropertyDefaults on PropertyName
				public bool IsFieldsPrepared;
				public bool IsChildrenPrepared;

				

				public bool IsFullyPrepared{get{return (IsChildrenPrepared && IsFieldsPrepared);}}
				
				public DataTable LazyLoadedSchemaTable(DataObject buildFrom)
				{
					LockCookie cookie = new LockCookie();
					bool bDownGradeOnly =false;

					if(!m_SchemaTableMap.ContainsKey(buildFrom.Context.DbConn.ConnectionString))
					{
						if(PropertyLock.IsReaderLockHeld)
						{
							cookie = PropertyLock.UpgradeToWriterLock(-1);
							bDownGradeOnly = true;
						}
						else
							PropertyLock.AcquireWriterLock(-1);

						try
						{
							m_SchemaTableMap.Add(buildFrom.Context.DbConn.ConnectionString,buildFrom.GetSchemaTable());
						}
						finally
						{
							// Ensure that the lock is released.
							if(bDownGradeOnly)
								PropertyLock.DowngradeFromWriterLock(ref cookie);
							else
								PropertyLock.ReleaseWriterLock();
						}
					}
					return (DataTable)m_SchemaTableMap[buildFrom.Context.DbConn.ConnectionString];
				} 
				public ArrayList SerializationSequence{get{return m_SerializationSequence;}}
                public Hashtable PropertyMetaInfoMap { get { return m_PropertyMetaInfoMap; } }
             
                public Hashtable LazyLoadedPropertyDefaultsMap(DataObject objBuildFrom)
				{
						LockCookie cookie = new LockCookie();
						bool bDownGradeOnly =false;

						if(m_PropertyDefaultsMap==null)
						{
							if(PropertyLock.IsReaderLockHeld)
							{
								cookie = PropertyLock.UpgradeToWriterLock(-1);
								bDownGradeOnly = true;
							}
							else
								PropertyLock.AcquireWriterLock(-1);

							try
							{
								m_PropertyDefaultsMap = objBuildFrom.GetPropertyDefaults(); //objTmp;		 
							}
							finally
							{
								// Ensure that the lock is released.
								if(bDownGradeOnly)
									PropertyLock.DowngradeFromWriterLock(ref cookie);
								else
									PropertyLock.ReleaseWriterLock();
							}
						}
						return m_PropertyDefaultsMap;
					} 

				internal ClassMetaInfo(DataObject buildFrom)
				{
					this.PropertyLock.AcquireWriterLock(-1);
					try
					{
						//m_buildFrom = buildFrom;
						m_SchemaTableMap = new Hashtable(50);
						m_SchemaTableMap.Add(buildFrom.Context.DbConn.ConnectionString,buildFrom.GetSchemaTable());
						//m_SchemaTable = buildFrom.GetSchemaTable();
						m_SerializationSequence = buildFrom.GetBaseSerializationSequence();
						m_PropertyMetaInfoMap = buildFrom.GetPropertyMetaInfoMap();
						//m_PropertyDefaultsMap = buildFrom.GetPropertyDefaults();
						//m_PropertyInfoMap = DataObject.m_PropertyInfoMap;
                    

						IsFieldsPrepared=false;
						IsChildrenPrepared=false;
					}finally{this.PropertyLock.ReleaseWriterLock();}
				}

			}

			//Represent an Item in the "Per Derived Class Static" Property Meta Info Cache
			//Supports Perf improvement for Serialize\Populate Routines
			public class PropertyMetaInfo
			{
                private ExtendedTypeAttribute m_ExtendedTypeAttribute = null;
                public ExtendedTypeAttribute ExtendedTypeAttribute { get { return m_ExtendedTypeAttribute; } }

                private string m_PropertyName = "";
                public string PropertyName { get { return m_PropertyName; } }

                private bool m_IsDbField = false;
                public bool IsDbField { get { return m_IsDbField; } }

                internal PropertyMetaInfo(string propertyName, bool isDbField) //,object defaultValue)
                {
                    Init(propertyName, isDbField, null);
                }
                internal PropertyMetaInfo(string propertyName, bool isDbField, ExtendedTypeAttribute ext) //,object defaultValue)
                {
                    Init(propertyName, isDbField, ext);
                }
                private void Init(string propertyName,bool isDbField, ExtendedTypeAttribute ext) //,object defaultValue)
				{
                    m_ExtendedTypeAttribute = ext;
					m_PropertyName=propertyName;
					m_IsDbField=isDbField;
				}
				
			}

			//METHODS
			#region Constructor Logic
			static DataObject()
			{
				// BSB 04.17.2007 These types cannot be deleted by their parents.
				// The developer must explicitly call delete on each
				// object if they wish to force deletion.
				// In some cases (Veh, Entity) deletion may in fact mean
				// simply flagging the record as deleted.
				m_DeletionProhibitedChildTypes.Add("Entity","Entity");
				m_DeletionProhibitedChildTypes.Add("Policy","Policy");
				m_DeletionProhibitedChildTypes.Add("PolicyEnh","PolicyEnh");
				m_DeletionProhibitedChildTypes.Add("DisabilityPlan","DisabilityPlan");
				m_DeletionProhibitedChildTypes.Add("FundsList","FundsList");
				m_DeletionProhibitedChildTypes.Add("ReserveCurrentList","ReserveCurrentList");
				m_DeletionProhibitedChildTypes.Add("FundsTransSplitList","FundsTransSplitList");
				m_DeletionProhibitedChildTypes.Add("BrsInvoice","BrsInvoice");
				m_DeletionProhibitedChildTypes.Add("BrsInvoiceDetail","BrsInvoiceDetail");
				m_DeletionProhibitedChildTypes.Add("MedicalStaff","MedicalStaff");
				m_DeletionProhibitedChildTypes.Add("Patient","Patient");
				m_DeletionProhibitedChildTypes.Add("Physician","Physician");
				m_DeletionProhibitedChildTypes.Add("Employee","Employee");
				m_DeletionProhibitedChildTypes.Add("Vehicle","Vehicle");
				m_DeletionProhibitedChildTypes.Add("VehicleXAccDateList","VehicleXAccDateList");
				m_DeletionProhibitedChildTypes.Add("VehicleXInspctList","VehicleXInspctList");
			}
			internal DataObject(Context context):base(true,context)
			{
				Configure();
			}
			internal DataObject(bool isLocked, Context context)
				:base(isLocked,context)
			{
				Configure();
			}
			#endregion

			#region IPersistence Implementation
			/// <summary>
			/// TODO Delete
			/// </summary>
			public virtual void Delete()
			{
                if (!this.BeforeDelete(this, new BeforeDeleteEventArgs()))
                {
                    throw new RMAppException("DataObject.Delete.Exception");
                }
				//BSB 07.29.2004 "Fix" for collection member objects that are locked.  They should not be deleted independent of 
				// their collection.
				//Note the following example discovered by TKR:
				// While this works ok (the collection can handle book-keeping about object state)
				//		pol.PolicyXCvgTypeList.Delete(14);
				// The following similar lines will cause problems
				//		Riskmaster.DataModel.PolicyXCvgType cvg = pol.PolicyXCvgTypeList[79];
				//		cvg.Delete();
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);

				//bool isOurDbTrans = false;
				this.m_isTransactionOwner = false;
				string SQL = "";

				//Record not in DB yet. Just clear memory and exit.
				if(this.IsNew)
				{
					this.Clear();
					return;
				}

				//We cannot in good conscience delete a stale object since it may be hanging off of a parent left over 
				// from a previous and unrelated parent record.  This is because children don't automatically load until their property is 
				// accessed from the parent.  
				// Therefore, during the parent delete, we should make sure that all child properties get touched before the 
				// delete call is propogated or else are deleted in some other way. (See OnChildDelete())
				if((this as IDataModel).IsStale)
				{
					this.Clear();
					return;
				}

				try
				{
					if(this.Context.DbTrans ==null)
					{
						this.Context.DbTrans = this.Context.DbConn.BeginTransaction();				
						this.m_isTransactionOwner = true;
						//isOurDbTrans = true;
					}
                    //nsachdeva - MITS: 26956 - 1/3/2012
                    //LogClaimActivity("DEL"); //nitin sachdeva: Mits:26418 : Claim Activity log
                    LogClaimActivity("DEL");
                    //End MITS: 26956
					//	For each Child object, alert our derived class implementation 
					// that we are ready to delete objects
					object[] arr = this.m_Children.GetKeyArray();
					string sChildTypeName = "";
                    // npadhy Start MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen
                    string sChildName = string.Empty;
					for(int i=0; i< arr.Length;i++)
					{
						//BSB Protect specified child type objects here - do not delete any entity by default.
						// Overriding classes can call this impl and then delete entities
						// explicitly if desired. 
						sChildTypeName = this.m_Children[(string)arr[i]].GetType().Name;

                        sChildName = (string)arr[i];
                        if (!(DataObject.m_DeletionProhibitedChildTypes.ContainsKey(sChildTypeName) || 
                            DataObject.m_DeletionProhibitedChildTypes.ContainsValue(sChildName)))
							OnChildDelete((string)arr[i], this.m_Children[(string)arr[i]] as IDataModel );
					}
                    // npadhy End MITS 21945 RMX hangs when user tries to delete a Renew transaction on Policy  management screen

                    // Conditionally Delete our Supplementals
                    if (OnSuppDelete())
                    {
                        //object trash = this.Supplementals;
                        m_Supplementals.Delete();
                    }

					//Delete our table Row
					SQL = OnBuildDeleteSQL();
					DbCommand cmd = Context.DbConn.CreateCommand();
					cmd.Transaction = this.Context.DbTrans;
					cmd.CommandText = SQL;
					cmd.ExecuteNonQuery();

					if(this.IsTransactionOwner)
					{
						//BSB 04.17.2007 Implement Pre\Post CommitDelete.
						this.OnPreCommitDelete();
						Context.DbTrans.Commit();
						this.OnPostCommitDelete();
						
					}
					this.Clear();
				}		
				catch(Exception e)
				{
					if (Context.DbTrans !=null )
						Context.DbTrans.Rollback();
					throw new RMAppException("DataObject.Delete.Exception", e);
				}
				finally
				{
					if(this.IsTransactionOwner)
					{
						this.Context.DbTrans = null;
						this.m_isTransactionOwner = false;
					}
				}
			}

			/// <summary>
			/// Provides an interception point for derived classes  to decide whether 
			/// or not the save should be bubbled to a parent object and handled there instead.
			/// 
			/// Overridden only in implementing classes that choose NOT to bubble saves.
			/// </summary>
			protected virtual  bool OnForceParentSave()
			{
				return true;
			}
			/// <summary>
			/// Provides an interception point for derived classes  to generate and assign 
			/// a new unique key from the glossary (or not as desired.)
			/// 
			/// Overridden only in implementing classes that choose NOT to generate a glossary unique row id.
			/// </summary>
			protected virtual  void OnBuildNewUniqueId()
			{
				this.KeyFieldValue = Context.GetNextUID(this.Table); 
			}

			/// <summary>
			/// Provides an interception point for derived classes to handle 
			/// singleton activities required before the full save can be committed.
			/// </summary>
			protected virtual void OnPreCommitSave()
			{
				//Nothing here just a hook for derived classes.
			}

			/// <summary>
			/// Provides an interception point for derived classes to handle 
			/// singleton activities required immediately after the full save has been committed.
			/// </summary>
			protected virtual void OnPostCommitSave()
			{
				//Nothing here just a hook for derived classes.
			}


			/// <summary>
			/// Provides an interception point for derived classes to handle 
			/// singleton activities required before the full delete can be committed.
			/// This code is run IF AND ONLY IF the delete is INITIATED on this object.
			/// It ONLY runs on the object upon which client code initiated the deletion.
			/// It is NEVER invoked on a "cascading" deleted object. 
			/// </summary>
			protected virtual void OnPreCommitDelete()
			{
				//Nothing here just a hook for derived classes.
			}

			/// <summary>
			/// Provides an interception point for derived classes to handle 
			/// singleton activities required immediately after the full delete has been committed.
			/// This code is run IF AND ONLY IF the delete is INITIATED on this object.
			/// It ONLY runs on the object upon which client code initiated the deletion.
			/// It is NEVER invoked on a "cascading" deleted object. 
			/// </summary>
			protected virtual void OnPostCommitDelete()
			{
				//Nothing here just a hook for derived classes.
			}


			public virtual bool WillSave()
			{
				//Check for Dirty
				if(! this.AnyDataChanged)
					return false;

				//Check for Stale
				if((this as IDataModel).IsStale)
					return false;

				return true;
			}

			/// <summary>
			/// Save provides a basic save functionality calling out to overridden methods for custom logic in 
			/// the implementing classes.
			/// </summary>
			public virtual void Save()
			{
				bool isNew=false;
				bool isStamped = this.m_Fields.ContainsField("DTTM_RCD_LAST_UPD");
				bool bThisRoutineWroteToDb = false;
                this.m_isActionOwner = true;

				//BSB 04.26.2007 
				// Use class level instance variables to record this state since
				// it is meaningfull (read-only) for understanding the situation from scripting.
				//bool isOurDbTrans = false;
				//bool bThisRoutineIsSaving = true;

				m_sDateTimeStamp=Common.Conversion.ToDbDateTime(DateTime.Now);
				IPersistence pIPer;
				try
				{
					
					if(this.m_DataModelState== DataObjectState.IsSaving)
						this.m_isActionOwner = false;
						//	bThisRoutineIsSaving=false;
                    
					this.m_DataModelState= DataObjectState.IsSaving;

					//Check for Dirty
					if(! this.AnyDataChanged)
						return;

					//Check for Stale
					if((this as IDataModel).IsStale)
						return;


                    //**************************{MITS 12236
                    //R7 Perf Improvement
                    //if (this.m_isActionOwner)
                    if (this.m_isActionOwner && Context.LocalCache.IsScriptEditorEnabled())
                    {
                        //Trigger Calculate, Validate and BeforeSave script events.
                        //TODO Error Trapped & stronger typed Calculate Data Script Call
                        if (!this.CalculateData(this, new CalculateDataEventArgs()))
                        {
                            throw new RMAppException("DataObject.Save.ValidationFailure");
                        }
                        //TODO Fully Error trapped Validate Data Script Call
                        if (!this.ValidateData(this, new ValidateEventArgs()))
                        {
                            throw new RMAppException("DataObject.Save.ValidationFailure");
                        }
                        //TODO Error Trapped & stronger typed BeforeSave Data Script Call
                        if (!this.BeforeSave(this, new BeforeSaveEventArgs()))
                        {
                            throw new RMAppException("DataObject.Save.ValidationFailure");
                        }

                        //nsachdeva2: MITS:25797 09/22/2011 - moved Nima changes below outside the script editor enabled validation.
                        //Nima; MITS 20168
                        //if (this is Claim && this.IsNew && ((Claim)this).AdjusterList != null && ((Claim)this).AdjusterList.Count == 0 && !((IDataModel)((Claim)this).AdjusterList).IsStale)
                        //{
                        //    AutoAssignAdjuster();
                        //}
                    }
                    if (this.m_isActionOwner)
                    {
                        //Nima; MITS 20168
                        if (!IsMigrationImport)
                        {
                            if (this is Claim && this.IsNew && ((Claim)this).AdjusterList != null && ((Claim)this).AdjusterList.Count == 0 && !((IDataModel)((Claim)this).AdjusterList).IsStale)
                            {
                                AutoAssignAdjuster();
                            }
                        }
                    }
                    //End MITS:25797
                    //**************************MITS 12236}
                    //BSB 12.14.2006 Clear ScriptWarnings Explicitly (Don't rely on GC)
                   // if (this.Context.DbTrans == null)
                        //Commented by Shivendu for MITS 12258
                        //this.Context.ScriptWarningErrors.Clear();

					DataObject objParent = this.Parent as DataObject; 
					if(OnForceParentSave())
						// Check whether Parent is Available
						if (objParent != null)
						{
							pIPer = objParent as IPersistence;
							//Check for Parent can be saved and is not the current Caller.
							//callerName=new StackFrame(1,false).GetMethod().ReflectedType.Name;
							// 10.17.2005 Fix by Nikhil for case where DataCollection contains the
							// child being saved. (child.Parent) points to the desired object, 
							// not the DataCollection but DataCollection will be between parent and child 
							// in the call stack for this case.
							//if (callerName.ToLower()=="datacollection")
							//	callerName=new StackFrame(2,false).GetMethod().ReflectedType.Name;

							//if(pIPer != null && callerName!=Parent.GetType().Name)
							if(objParent.m_DataModelState!= DataObjectState.IsSaving)
								if(pIPer.IsNew || pIPer.AnyDataChanged)
								{
									pIPer.Save();
									//BSB  08.13.2005 Yes this looks bizarre!
									// This is set up to properly handle two situations.
									// 1.) The parent saved us and we can abort our save.
									//		This will be implemented by running the preceeding
									//		data changed checks again recursively. We can't just fall
									//		into the remaining save logic cuz we won't hit the checks
									//		again that way...
									// 2.) The parent saved but didn't save us cuz we're not linked up completely.
									//	   This can happen if the parent is loaded using "LoadParent()"
									//	   and doesn't actually have bidirectional knowledge about this child.
									//	   Again, in this situation the recursive call will correctly 
									//	   complete the save based on the preceeding datachanged checks 
									//     and the parent will no longer have dirty data (thus skipping this block
									//	   of code) - allowing the rest of the save to complete.
									this.Save(); 
									return;
								}
						}


					//If we're not already in a transaction, start one.
					if(this.Context.DbTrans == null)
					{
						this.Context.DbTrans = Context.DbConn.BeginTransaction();
						this.m_isTransactionOwner= true;
					}
					//For each Child object, alert our derived class implementation 
					// that we are ready to save any objects which should be saved before the parent.
					object[] arr = this.m_Children.GetKeyArray();
					for(int i=0; i< arr.Length;i++)
						if(!(this.m_Children[(string)arr[i]] as IDataModel).IsStale)
							CallOnChildPreSave((string)arr[i], this.m_Children[(string)arr[i]] as IDataModel );
				
					//Save the Database Field properties of our current object.
					if(this.DataChanged || this.IsNew)
					{
						DbWriter objWriter=null;
						bThisRoutineWroteToDb =true;
						//Test to see whether we are saving a record to a "stamped" db table.
						if(isStamped)
							objWriter = DbFactory.GetDbWriter(Context.DbConn,m_sDateTimeStamp,Context.RMUser.LoginName,this.GetFieldString("DTTM_RCD_LAST_UPD"));
						else
							objWriter = DbFactory.GetDbWriter(Context.DbConn);

						objWriter.Tables.Add(this.Table);

                        objWriter.DefaultDateTimeAndUser = this.DefaultDateTimeAndUser;

						// New record
						if (this.IsNew)
						{
							OnBuildNewUniqueId();
							isNew = true;
							// This is handled inside of the Writer....
							//objWriter.Fields.Add("ADDED_BY_USER", Context.RMUser.LoginName);
							//objWriter.Fields.Add("DTTM_RCD_ADDED", sDateTimeStamp);
							
							//BSB 01.03.2006 Fix for floating Pi Code fields.
							// This occurs in general whenever the underlying DB table has fields
							// that are not represented in the DM class being used.  In the Pi case
							// this is intentional as PiOther and PiEmployee have different fields
							// but are stored in a single physical db table.
							string ColumnName="";
							
							this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
							try
							{	
								DataTable schemaTable = this.MetaInfo.LazyLoadedSchemaTable(this);
								if(schemaTable.Rows.Count!=m_Fields.Count())
									foreach(DataRow row in schemaTable.Rows)
									{
										ColumnName = row["ColumnName"].ToString();
										if(!this.m_Fields.ContainsField(ColumnName))
											objWriter.Fields.Add(ColumnName,DataObject.GetCLRTypeDefault((string)row["DataType"].ToString()));
									}
							}
							finally{this.MetaInfo.PropertyLock.ReleaseReaderLock();}
						}
						else
						{
							objWriter.Where.Add(String.Format(" {0}={1} ",this.KeyFieldName,this.KeyFieldValue));
						}

						arr = this.m_Fields.GetKeyArray();
						for(int i=0; i< arr.Length;i++)
							objWriter.Fields.Add((string)arr[i],this.GetField((string)arr[i]));
						objWriter.Execute(this.Context.DbTrans);


					}
                    //JIRA RMA-1346 nshah28 start
                    if (this.Table == "RESERVE_CURRENT")
                    {
                        string sResStatusShortCode=Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper();
                        if(sResStatusShortCode=="H" || sResStatusShortCode=="O" || sResStatusShortCode=="RE")
                          m_isNew = true; //setting this condtion, because getting m_isnew=false here, but we need it true to get when supervisor reject reserve
                    }

                    //If new claim created, updated date is blank. so set m_isNew=true
                    if (this.Table == "CLAIM" && ((Claim)(this)).DttmRcdLastUpd=="")
                    {
                        m_isNew = true;
                    }

                    if (this.Table == "FUNDS")
                    {
                        if (((Funds)(this)).VoidFlag || ((Funds)(this)).ReissueFlag || ((Funds)(this)).OffSetFlag || ((Funds)(this)).FinalPaymentFlag)
                        {
                            m_isNew = true;
                        }
                        
                    }
                    //JIRA RMA-1346 nshah28 End
                    //Nitin Sachdeva: Mits:26418 - Claim Activity log
                    if (!IsMigrationImport)
                    {
                        LogClaimActivity("New");
                    }
					//Book-Keeping Now that we should have the current table data saved, let's say we're not "new" anymore.
					m_isNew = false;

					//For each Child object, simply notify our derived class implementation for each known object.
					arr = this.m_Children.GetKeyArray();
					for(int i=0; i< arr.Length;i++)
						if(!(this.m_Children[(string)arr[i]] as IDataModel).IsStale)
							CallOnChildPostSave((string)arr[i], this.m_Children[(string)arr[i]] as IDataModel,isNew );

                    if (!IsMigrationImport)
                    {
                        LogActivityForPolicyInterface(isNew);

                        LogActivityForFASInterface(isNew);//averma62 - MITS 32386 - rmA - FAS Interface
                    }
					//************************************************************************
					// SUPPLEMENTALS IMPLEMENTATION
					//************************************************************************
					if(m_Supplementals.DefinitionLoaded)
					{	
						CallOnSetSupplementalKeyValues();
						//BSB Moved after Key Assignment (Key value assignment should not affect DataChanged status)
						// Fix for an issue where if the user changes a claim immediately after creating it 
						// with no intervening navigation, the serialized Supp key does not reflect that 
						// of the new claim.  This causes any further save attempt to actually create
						// a supp row unnecessarily.
						if(m_Supplementals.DataChanged)
							this.Supplementals.Save();
					}

					if(this.m_isTransactionOwner)
					{
						OnPreCommitSave();
						Context.DbTrans.Commit();
						Context.DbTrans = null;
					}
					// BSB 08.21.2006 Book-keep in-memory locking stamps immediately
					// after commit (if committing) in case something in 
					// PostTransThrow or OnPostCommitSave
					// needs to update these same records we need the stamps to be
					// up to date.
					if(isStamped && bThisRoutineWroteToDb)
					{	
						if (isNew)
						{
							this.m_Fields["DTTM_RCD_ADDED"] = m_sDateTimeStamp;
							this.m_Fields["ADDED_BY_USER"] = Context.RMUser.LoginName;
						}
						this.m_Fields["DTTM_RCD_LAST_UPD"] = m_sDateTimeStamp;
						this.m_Fields["UPDATED_BY_USER"] = Context.RMUser.LoginName;
					}	
					if(this.IsTransactionOwner)
                    {
                        if (!ByPassPostTransCommit)
                        {
                            Context.PostTransThrow();
                        }
                        else
                        {
                            Context.DestroyPostTransForMigration();
                        }
						OnPostCommitSave();
					}

					//Update Book-Keeping Details. (Even if DataChangeFlag is locked.)
					this.m_DataChanged= false;

					//TODO Error Trapped & stronger typed AfterSave Data Script Call
                   
                        this.AfterSave(this, new AfterSaveEventArgs());
                    
				}
                //Deb: throw the RM Exeception
                catch (RMAppException p_objRmAEx)
                {
                    if (Context.DbTrans != null)
                    {
                        try { Context.DbTrans.Rollback(); }
                        catch { }
                        Context.DbTrans = null;
                    }
                    throw p_objRmAEx;
                }
                //Deb: throw the RM Exeception
				catch(Exception e)
				{
					if (Context.DbTrans !=null )
					{
						try{Context.DbTrans.Rollback();}
						catch{}
						Context.DbTrans = null;
					}
					throw new RMAppException("DataObject.Save.Exception", e);
				}
				finally
				{
					if(this.IsTransactionOwner)
					{
							this.Context.DbTrans = null;
							this.m_isTransactionOwner = false;
					}
					if(this.IsActionOwner)
					{
						this.m_DataModelState=DataObjectState.IsReady;
						this.m_isActionOwner = false;
					}
				}
			}

            //Start averma62 MITS 32386 rmA - FAS Integration
            private void LogActivityForFASInterface(bool isNew)
            {
                //asharma326 Insert data to fas table when FAS is ON MITS 34849 
                if (this.Context.InternalSettings.SysSettings.EnableFAS == -1)
                {
                    string sSelectSQL = string.Empty;
                    int iClaimId = 0;
                    int iRowId = 0;

                    try
                    {
                        //asharma326 MITS 34785
                        if (this.Table == "RESERVE_CURRENT" || this.Table == "EXPERT" || this.Table == "PERSON_INVOLVED" 
                            || this.Table == "CLAIM_ADJUSTER" || this.Table == "CLAIM_X_PROPERTYLOSS" || this.Table == "UNIT_STAT" 
                            || this.Table == "EVENT" || this.Table == "CLAIM" || this.Table == "CLAIMANT" || this.Table == "CLAIM_X_POLICY" 
                            || this.Table == "CLAIM_X_LITIGATION" || this.Table == "DEFENDANT" || this.Table == "FUNDS"
                            || this.Table == "CLAIM_X_PROPERTY" || this.Table == "CLAIM_X_LIABILITYLOSS" || this.Table == "ENTITY_X_ADDRESSES" || this.Table == "UNIT_X_CLAIM")
                        {
                            switch (this.Table)
                            {
                                case "CLAIM":
                                    iClaimId = (this as Claim).ClaimId;
                                    break;

                                case "CLAIMANT":
                                    iClaimId = (this as Claimant).ClaimId;
                                    break;

                                case "CLAIM_X_LITIGATION":

                                    iClaimId = (this as ClaimXLitigation).ClaimId;
                                    break;

                                case "DEFENDANT":
                                    iClaimId = (this as Defendant).ClaimId;
                                    break;

                                case "FUNDS":
                                    iClaimId = (this as Funds).ClaimId;
                                    break;

                                case "UNIT_STAT":
                                    iClaimId = (this as UnitStat).ClaimId;
                                    break;

                                case "CLAIM_X_LIABILITYLOSS":
                                    iClaimId = (this as ClaimXLiabilityLoss).ClaimId;
                                    break;

                                case "CLAIM_X_PROPERTYLOSS":
                                    iClaimId = (this as ClaimXPropertyLoss).ClaimId;
                                    break;

                                case "UNIT_X_CLAIM":
                                    iClaimId = (this as UnitXClaim).ClaimId;
                                    break;

                                case "CLAIM_X_POLICY":
                                    iClaimId = (this as ClaimXPolicy).ClaimId;
                                    break;

                                case "CLAIM_ADJUSTER":
                                    iClaimId = (this as ClaimAdjuster).ClaimId;
                                    break;

                                //case "person_involved":
                                //    //int ieventid = 0;
                                //    //sselectsql = "select event_id from person_involved where pi_type_code='"+ context.localcache.getcodeid("e","person_inv_type")+"' and pi_row_id ="+ (this as personinvolved).pirowid;
                                //    //ieventid = conversion.convertobjtoint(dbfactory.executescalar(context.dbconn.connectionstring, sselectsql));

                                //    //sselectsql = string.empty;

                                //    //sselectsql = "select claim_id from claim where evemt_id="+ieventid;
                                //    //iclaimid = conversion.convertobjtoint(dbfactory.executescalar(context.dbconn.connectionstring, sselectsql));
                                //   ssql = "select claim_id from claim where claim.event_id =" + ((personinvolved)(this)).eventid;
                                //                oreader = dbfactory.getdbreader(context.dbconn.connectionstring, ssql);
                                //                sbclaimids = new stringbuilder();
                                //                while (oreader.read())
                                //                {
                                //                    if (sbclaimids.length > 0)
                                //                        sbclaimids.append(",");
                                //                    sbclaimids.append(oreader.getint32("claim_id").tostring());
                                //                }
                                //                recordid = sbclaimids.tostring();
                                //                oentity = ((entity)(this.children["pientity"]));
                                //                sbtext.append(context.localcache.getcodedesc(((personinvolved)(this)).pitypecode));
                                //                sbtext.append(":");
                                //                sbtext.append(this.default);


                                //    sselectsql = "select claim.claim_id from claim,event,person_involved where person_involved.event_id =claim.event_id and person_involved.pi_type_code='" + context.localcache.getcodeid("e", "person_inv_type") + "' and person_involved.pi_row_id=" + (this as personinvolved).pirowid;
                                //    iclaimid = conversion.convertobjtoint(dbfactory.executescalar(context.dbconn.connectionstring, sselectsql));
                                //    break;    

                                case "EXPERT":
                                    sSelectSQL = "SELECT CLAIM_ID FROM CLAIM_X_LITIGATION WHERE LITIGATION_ROW_ID =" + (this as Expert).LitigationRowId;
                                    iClaimId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);
                                    break;

                                case "RESERVE_CURRENT":
                                    iClaimId = (this as ReserveCurrent).ClaimId;
                                    break;
                            }
                            if (iClaimId > 0)
                            {
                                sSelectSQL = string.Empty;
                                sSelectSQL = "SELECT ROW_ID FROM RMA_FAS_CLAIM_EXP WHERE CLAIM_ID=" + iClaimId;
                                iRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);
                                CommonFunctions.CreateLogForFASInterface(Context.DbConn.ConnectionString, iRowId, iClaimId, this.Context.ClientId);
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        throw new RMAppException("LogActivityForFASInterface.Save.Exception", e);
                    }
                    finally
                    {

                    }
                }
            }
            //End averma62 MITS 32386 rmA - FAS Integration


            /// <summary>
            /// Capture the claim activity log data and save to database.
            /// </summary>
            /// <param name="sOptType">Operation type</param>
            private void LogClaimActivity(string sOptType)
            {
                Claim oClaim = null;
                string recordId = string.Empty;
                int iTableId = 0;
                int iOptType = 0;
                bool bAllowLog = false;
                string sLogText = string.Empty;
                int iActType = 0;
                string sRecordNumber = string.Empty;
                Entity oEntity = null;
                Dictionary<string, string> objPlaceHolder = new Dictionary<string, string>();
                string sSql = string.Empty;
                StringBuilder sbClaimIds = null;
                StringBuilder sbText = null;
                DbReader oReader = null;
                string sTable = string.Empty;
                int iPrimaryPolicyId = 0;
                bool bUseEnhPolicy = false;
                //JIRA RMA-1346 nshah28 start
                int baseCurrencyCode = this.Context.InternalSettings.SysSettings.BaseCurrencyType;
                //JIRA RMA-1346 nshah28 end

                
                try
                {
                    oClaim = this.Parent as Claim;
                    //if (this.Table == "RESERVE_CURRENT" || this.Table == "FUNDS_TRANS_SPLIT" || this.Table == "RESERVE_HISTORY" || this.Table == "CLAIM_SUPP" || this.Table == "CLAIM_X_POLICY" || this.Table == "CLAIM" || this.Table == "PERSON_INVOLVED" || this.Table == "SALVAGE" || this.Table == "UNIT_X_CLAIM" || this.Table == "POLICY_X_CVG_TYPE" || this.Table == "POLICY_X_UNIT" || this.Table == "POLICY_X_CVG_ENH" || this.Table == "FUNDS" || this.Table == "DEFENDANT" || this.Table == "FUNDS_AUTO" ||  oClaim != null)
                    //JIRA RMA-1346 nshah28 start
                    if (this.Table == "RESERVE_CURRENT" || this.Table == "FUNDS_TRANS_SPLIT" || this.Table == "RESERVE_HISTORY" || this.Table == "CLAIM_SUPP" || this.Table == "CLAIM_X_POLICY" || this.Table == "CLAIM" || this.Table == "PERSON_INVOLVED" || this.Table == "SALVAGE" || this.Table == "UNIT_X_CLAIM" || this.Table == "POLICY_X_CVG_TYPE" || this.Table == "POLICY_X_UNIT" || this.Table == "POLICY_X_CVG_ENH" || this.Table == "FUNDS" || this.Table == "DEFENDANT" || this.Table == "FUNDS_AUTO" || this.Table == "ENTITY" || this.Table == "ENT_ID_TYPE" || this.Table == "PMT_APPROVAL_HIST" || oClaim != null)
                    //JIRA RMA-1346 nshah28 end
                    {
                        sTable = this.Table;
                        if (oClaim == null) oClaim = this as Claim;

                       if (oClaim != null)
                        {
                            if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm==-1)
                            {
                            if (this.Context.InternalSettings.ColLobSettings[oClaim.LineOfBusCode].UseEnhPolFlag != 0)
                            {
                                iPrimaryPolicyId = oClaim.PrimaryPolicyIdEnh;
                                bUseEnhPolicy = true;
                            }
                            else
                            {
                                iPrimaryPolicyId = oClaim.PrimaryPolicyId;
                            }

                            //JIRA RMA-6190 nshah28  Commenting below lines to stop duplicate entry
                         /*   if (iPrimaryPolicyId != m_iExistingClaimPolicy && m_iExistingClaimPolicy != -1)
                            {
                                if ((iPrimaryPolicyId > 0) && (m_iExistingClaimPolicy == 0))
                                {
                                    
                                    LogClaimActivityForPolicy("New", oClaim.ClaimId, iPrimaryPolicyId, bUseEnhPolicy);
                                    bAllowLog = false;
                                   
                                }
                                else if ((iPrimaryPolicyId == 0) && (m_iExistingClaimPolicy > 0))
                                {
                                    
                                    LogClaimActivityForPolicy("DEL", oClaim.ClaimId, m_iExistingClaimPolicy, bUseEnhPolicy);
                                    bAllowLog = false;
                                    
                                }
                                else if ((iPrimaryPolicyId > 0) && (m_iExistingClaimPolicy > 0))
                                {
                                    
                                    LogClaimActivityForPolicy("New", oClaim.ClaimId, iPrimaryPolicyId, bUseEnhPolicy);
                                     LogClaimActivityForPolicy("DEL", oClaim.ClaimId, m_iExistingClaimPolicy, bUseEnhPolicy);
                                     bAllowLog = false;
                                    
                                }
                                
                                } */
                            }
                        }

                       
                        if (sOptType == "New")
                            bAllowLog = (m_isNew && this.Context.InternalSettings.SysSettings.ClaimActivityLog);
                        else
                            bAllowLog = this.Context.InternalSettings.SysSettings.ClaimActivityLog;
                        
                        if (bAllowLog)
                        {
                            iTableId = this.Context.LocalCache.GetTableId(sTable);
                            iOptType = this.Context.LocalCache.GetCodeId(sOptType, "OPERATION_TYPE");
                            bAllowLog = CommonFunctions.bClaimActLogSetup(Context.DbConn.ConnectionString, iTableId, iOptType, ref sLogText, ref iActType, this.Context.ClientId);
                            if (bAllowLog)
                            {
                                sbText = new StringBuilder();
                                //if (oClaim == null) oClaim = this as Claim;
                                if (oClaim != null)
                                {
                                    recordId = oClaim.ClaimId.ToString();
                                    sRecordNumber = oClaim.ClaimNumber;
                                    switch (sTable)
                                    {
                                        case "CLAIM_SUPP":
                                            break;
                                        case "CLAIM_STATUS_HIST":
                                            objPlaceHolder.Add("<CLAIMSTATUS>", Context.LocalCache.GetCodeDesc(oClaim.ClaimStatusCode));
                                            //JIRA RMA-1346 nshah28 Start
                                            //sbText.Append("New Claim Status: ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.New", this.Context.ClientId)));
                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Claim",this.Context.ClientId)));
                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Status", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //JIRA RMA-1346 nshah28 End
                                            sbText.Append(Context.LocalCache.GetCodeDesc(oClaim.ClaimStatusCode));
                                            break;
                                        case "CLAIM_X_POLICY":
                                           
                                                LogClaimActivityForPolicy(sOptType, oClaim.ClaimId, ((ClaimXPolicy)(this)).PolicyId, bUseEnhPolicy);
                                            
                                            bAllowLog = false;
                                            //sSql = "SELECT POLICY_NAME, POLICY_NUMBER FROM POLICY WHERE POLICY_ID =" + ((ClaimXPolicy)(this)).PolicyId;
                                            //oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            //while (oReader.Read())
                                            //{
                                            //    sbText.Append("Policy: ");
                                            //    sbText.Append(oReader.GetString("POLICY_NAME"));
                                            //    sbText.Append(" (");
                                            //    sbText.Append(oReader.GetString("POLICY_NUMBER"));
                                            //    sbText.Append(")");
                                            //}
                                            //oReader.Dispose();
                                            break;
                                        case "CLAIM_X_PROPERTYLOSS":
                                            sbText.Append("Property: ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXPropertyLoss)(this)).PropertyType));
                                            break;
                                        case "CLAIM":
                                            //JIRA RMA-1346 nshah28 Start
                                            if (sOptType == "New")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Created", this.Context.ClientId)));
                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));

                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Claim", this.Context.ClientId)));
                                            sbText.Append(":");
                                            sbText.Append(sRecordNumber);
                                            //JIRA RMA-1346 nshah28 End
                                            break;
                                        case "CLAIM_X_LITIGATION":
                                            /* sbText.Append("Litigation for (");
                                             sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXLitigation)(this)).LitStatusCode));
                                             sbText.Append(") ");
                                             sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXLitigation)(this)).LitTypeCode));*/
                                            //JIRA RMA-1346 nshah28 Start, Need to check whether to include LitTypeCode or not
                                            //sbText.Append("Recorded: Litigation, Status: ");

                                            if (sOptType == "New")
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Recorded", this.Context.ClientId)));

                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));

                                            sbText.Append(": ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Litigation", this.Context.ClientId)));
                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Status", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXLitigation)(this)).LitStatusCode));
                                            //JIRA RMA-1346 nshah28 End

                                            break;
                                        case "CLAIM_X_SUBRO":
                                            sbText.Append("Subrogation for (");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXSubrogation)(this)).SubStatusCode));
                                            sbText.Append(") ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXSubrogation)(this)).SubTypeCode));
                                            break;
                                        case "CLAIM_X_ARB":
                                            sbText.Append("Arbitration for (");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXArbitration)(this)).ArbStatusCode));
                                            sbText.Append(") ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ClaimXArbitration)(this)).ArbTypeCode));
                                            break;
                                        case "CLAIM_ADJUSTER":
                                            //JIRA RMA-1346 nshah28 START
                                            if (sOptType == "New")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId)));
                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));
                                            //JIRA RMA-1346 nshah28 END

                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Adjuster", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            oEntity = ((Entity)(this.Children["AdjusterEntity"]));
                                            sbText.Append(oEntity.LastName);
                                            sbText.Append(", ");
                                            sbText.Append(oEntity.FirstName);
                                            break;
                                        case "CLAIMANT":
                                            //JIRA RMA-1346 nshah28 START
                                            if (sOptType == "New")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId)));
                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));
                                            //JIRA RMA-1346 nshah28 END

                                             sbText.Append(" ");
                                             sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Claimant", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //sbText.Append("Claimant: ");
                                            oEntity = ((Entity)(this.Children["ClaimantEntity"]));
                                            sbText.Append(oEntity.LastName);
                                            sbText.Append(", ");
                                            sbText.Append(oEntity.FirstName);
                                            break;
                                        //JIRA RMA-1346 nshah28 START
                                        case "DEFENDANT":
                                            if (sOptType == "New")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId)));
                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));
                                            

                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Defendant", this.Context.ClientId)));
                                            sbText.Append(": ");

                                            recordId = ((Defendant)(this)).ClaimId.ToString();
                                            //Deb: Table gets locked on DB server if keyfield value is fetched directly from Default property
                                            //sbText.Append(this.Default);
                                            sbText.Append(((Defendant)this).DefendantEntity.LastName + ", " + ((Defendant)this).DefendantEntity.FirstName);
                                            //Deb
                                            break;
                                        //JIRA RMA-1346 nshah28 END
                                        case "FUNDS":
                                            string lTransNumber = ((Funds)(this)).CtlNumber;
                                            double dAmount = ((Funds)(this)).Amount;
                                            oEntity = ((Entity)this.Children["PayeeEntity"]);
                                            /*  sbText.Append("Payment - Amount: ");
                                              sbText.Append(dAmount);
                                              sbText.Append(", CTL_Number: ");
                                              sbText.Append(lTransNumber);*/
                                            //JIRA RMA-1346 nshah28 Start
                                            string flagType = string.Empty;
                                            string ssType = string.Empty;

                                            //some where this 2 fields are set as "1" OR "-1", so "0" always be false rest is True
                                            if (((Funds)(this)).CollectionFlag.ToString() == "True")
                                                flagType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Collection", this.Context.ClientId));
                                            if (((Funds)(this)).PaymentFlag.ToString() == "True")
                                                flagType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Payment", this.Context.ClientId));

                                            if (((Funds)(this)).VoidFlag.ToString() == "True")
                                                ssType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Void",this.Context.ClientId)); // "Void";
                                            if (((Funds)(this)).FinalPaymentFlag.ToString() == "True")
                                                ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Finalpayment",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Finalpayment",this.Context.ClientId)); //If condition

                                            if (((Funds)(this)).OffSetFlag.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Offset",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Offset",this.Context.ClientId)); //If condition 
                                                 //ssType += (ssType == "") ? "Offset" : "/Offset";

                                            if (((Funds)(this)).ReissueFlag.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reissue",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reissue",this.Context.ClientId)); //If condition 
                                                 
                                            if (((Funds)(this)).IsEFTPayment.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.EFT",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.EFT",this.Context.ClientId)); //If condition 
                                                 
                                            //if (((Funds)(this)).SettlementFlag.ToString() == "True")
                                            //     ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Settlement",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Settlement",this.Context.ClientId)); //If condition 
                                                 

                                            sbText.Append(flagType);
                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, dAmount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.CtlNumber", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //sbText.Append(", Control number: ");
                                            sbText.Append(((Funds)(this)).CtlNumber);
                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Type", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            sbText.Append(ssType);
                                            recordId = ((Funds)(this)).ClaimId.ToString();
                                            //JIRA RMA-1346 nshah28 End

                                            break;
                                        case "RESERVE_CURRENT":
                                            recordId = ((ReserveCurrent)(this)).ClaimId.ToString();
                                            /* sbText.Append("Added reserves for reserve type: ");
                                             sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ReserveTypeCode));
                                             sbText.Append(" with Amount: ");
                                             sbText.Append(((ReserveCurrent)(this)).IncurredAmount); */

                                            //JIRA RMA-1346 nshah28 Start
                                            string sAction = "";

                                            sSql = "select RES_STATUS_CODE from RSV_SUP_APPROVAL_HIST where RC_ROW_ID=" + ((ReserveCurrent)(this)).RcRowId + " order by DTTM_RCD_ADDED desc";
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            if (oReader.Read())
                                            {
                                                if (oReader.GetValue("RES_STATUS_CODE") != DBNull.Value)
                                                {
                                                    sAction = Context.LocalCache.GetCodeDesc(oReader.GetInt32("RES_STATUS_CODE"));

                                                    if(Context.LocalCache.GetShortCode(oReader.GetInt32("RES_STATUS_CODE")).ToUpper() == "H")
                                                    {
                                                        if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O")
                                                        {
                                                            sAction = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Approved", this.Context.ClientId));
                                                        }
                                                        else
                                                        {
                                                            sAction = ""; //Setting blank, because Hold status we need to get only from below code
                                                        }
                                                    }
                                                }
                                            }

                                            if (sAction == "")
                                            {
                                                if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O" && ((ReserveCurrent)(this)).DttmRcdLastUpd == "") 
                                                {
                                                    sAction = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId));
                                                }
                                                else
                                                {
                                                    sAction = Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ResStatusCode);
                                                    if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O")
                                                    {
                                                        bAllowLog = false; //setting this false because "Open reserve..." this entry we are getting from reserve_history
                                                    }
                                                }
                                            }

                                           // sbText.Append(sAction + " Reserve: ");
                                            sbText.Append(sAction + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reserve", this.Context.ClientId)) + ": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ReserveTypeCode));
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)) + ": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, ((ReserveCurrent)(this)).IncurredAmount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            //sbText.Append(", Date: ");
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Date", this.Context.ClientId)) + ": ");
                                            sbText.Append(DateTime.Now.ToShortDateString());

                                            if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                                            {
                                                sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Policy", this.Context.ClientId)) + ": ");
                                                //Getting Policy Name from POLICY
                                                sSql = "select POLICY_NAME,POLICY_NUMBER from COVERAGE_X_LOSS inner join POLICY_X_CVG_TYPE on COVERAGE_X_LOSS.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID inner join POLICY_X_UNIT on POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID inner join POLICY on POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID where COVERAGE_X_LOSS.CVG_LOSS_ROW_ID=" + ((ReserveCurrent)(this)).CoverageLossId;

                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                while (oReader.Read())
                                                {
                                                    sbText.Append(oReader.GetString("POLICY_NAME"));

                                                } /**/
                                            }

                                            //JIRA RMA-1346 nshah28 End
                                            break;
                                    }
                                    oClaim = null;
                                }
                                else
                                {
                                    switch (sTable)
                                    {
                                        case "FUNDS":
                                            
                                            double dAmount = ((Funds)(this)).Amount;
                                            oEntity = ((Entity)this.Children["PayeeEntity"]);
                                            //JIRA RMA-1346 nshah28 Start
                                            string flagType = string.Empty;
                                            string ssType = string.Empty;

                                            //some where this 2 fields are set as "1" OR "-1", so "0" always be false rest is True
                                            if (((Funds)(this)).CollectionFlag.ToString() == "True")
                                                flagType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Collection", this.Context.ClientId));
                                            if (((Funds)(this)).PaymentFlag.ToString() == "True")
                                                flagType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Payment", this.Context.ClientId));

                                            if (((Funds)(this)).VoidFlag.ToString() == "True")
                                                ssType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Void",this.Context.ClientId)); // "Void";
                                            if (((Funds)(this)).FinalPaymentFlag.ToString() == "True")
                                                ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Finalpayment",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Finalpayment",this.Context.ClientId)); //If condition

                                            if (((Funds)(this)).OffSetFlag.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Offset",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Offset",this.Context.ClientId)); //If condition 
                                                 //ssType += (ssType == "") ? "Offset" : "/Offset";

                                            if (((Funds)(this)).ReissueFlag.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reissue",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reissue",this.Context.ClientId)); //If condition 
                                                 
                                            if (((Funds)(this)).IsEFTPayment.ToString() == "True")
                                                 ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.EFT",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.EFT",this.Context.ClientId)); //If condition 
                                                 
                                            //if (((Funds)(this)).SettlementFlag.ToString() == "True")
                                            //     ssType += (ssType == "") ? CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Settlement",this.Context.ClientId)) : "/"+CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Settlement",this.Context.ClientId)); //If condition 
                                                 
                                            //JIRA RMA-1346 nshah28 End
                                            if (((Funds)(this)).AutoCheckFlag)
                                            {
                                                sbText.Append(((Funds)(this)).AutoCheckDetail);
                                                /*  sbText.Append(" of amount: ");
                                                  sbText.Append(dAmount);
                                                  sbText.Append(" CTL Number: ");
                                                  sbText.Append(((Funds)(this)).CtlNumber); */
                                            }
                                            else
                                            {
                                                /* sbText.Append("Payment - Amount: ");
                                                 sbText.Append(dAmount);
                                                 sbText.Append(", CTL_Number: ");
                                                 sbText.Append(((Funds)(this)).CtlNumber); */
                                            }

                                            //JIRA RMA-1346 nshah28 Start
                                           sbText.Append(flagType);
                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, dAmount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.CtlNumber", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            //sbText.Append(", Control number: ");
                                            sbText.Append(((Funds)(this)).CtlNumber);
                                            sbText.Append(", ");
                                            if (!string.IsNullOrEmpty(ssType))
                                            {
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Type", this.Context.ClientId)));
                                                sbText.Append(": ");
                                                sbText.Append(ssType);
                                            }
                                            //JIRA RMA-1346 nshah28 End

                                            recordId = ((Funds)(this)).ClaimId.ToString();
                                            break;
                                        case "FUNDS_TRANS_SPLIT":
                                            string sCtlNumber = string.Empty;
                                            //JIRA RMA-1346 nshah28 Start
                                            string sClaimNumber = string.Empty;
                                            string sType = string.Empty;
                                            //JIRA RMA-1346 nshah28 End

                                            if (this.Parent != null)
                                            {
                                                recordId = ((Funds)(this.Parent)).ClaimId.ToString();
                                                sCtlNumber = ((Funds)(this.Parent)).CtlNumber;
                                                //JIRA RMA-1346 nshah28 Start
                                                sClaimNumber = ((Funds)(this.Parent)).ClaimNumber;

                                                if (((Funds)(this.Parent)).CollectionFlag.ToString() == "True")
                                                    sType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Collection", this.Context.ClientId)); //sType = "Collection";
                                                   
                                                if (((Funds)(this.Parent)).PaymentFlag.ToString() == "True")
                                                    sType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Payment", this.Context.ClientId)); //sType = "Payment";
                                                //JIRA RMA-1346 nshah28 End
                                            }
                                            else
                                            {
                                                //JIRA RMA-1346 nshah28 Start
                                                sSql = "select FUNDS.CLAIM_ID,FUNDS.CTL_NUMBER,CLAIM_NUMBER,CLAIMANT_EID,VOID_FLAG,COLLECTION_FLAG,PAYMENT_FLAG,FINAL_PAYMENT_FLAG,REISSUE_FLAG FROM FUNDS where FUNDS.TRANS_ID=" + Convert.ToString(((FundsTransSplit)(this)).TransId);
                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                while (oReader.Read())
                                                {
                                                    recordId = oReader.GetInt32("CLAIM_ID").ToString();
                                                    sCtlNumber = oReader.GetValue("CTL_NUMBER").ToString();

                                                    sClaimNumber = oReader.GetString("CLAIM_NUMBER");
                                                    if (oReader.GetString("COLLECTION_FLAG").ToString() == "True")
                                                        sType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Collection", this.Context.ClientId)); //sType = "Collection";
                                                    if (oReader.GetString("PAYMENT_FLAG").ToString() == "True")
                                                        sType = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Payment", this.Context.ClientId)); //sType = "Payment";
                                                }
                                                //oReader.Dispose();
                                                //JIRA RMA-1346 nshah28 End
                                            }
                                            /* sbText.Append("Split of amount: ");
                                             sbText.Append(((FundsTransSplit)(this)).Amount);
                                             sbText.Append(" on CTL Number: ");
                                             sbText.Append(sCtlNumber); */

                                            //JIRA RMA-1346 nshah28 Start

                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Recorded", this.Context.ClientId)) + ": ");
                                            sbText.Append(sType);
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)) + ": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, ((FundsTransSplit)(this)).Amount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.CtlNumber", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            sbText.Append(sCtlNumber);
                                            //sbText.Append(", Transaction Type: ");
                                            sbText.Append(", ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.TransType", this.Context.ClientId)));
                                            sbText.Append(": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((FundsTransSplit)(this)).TransTypeCode));
                                            //JIRA RMA-1346 nshah28 End
                                            break;
                                        case "RESERVE_HISTORY":
                                            //nsachdeva - MITS: 26955 - 1/3/2012
                                            //recordId = ((ReserveCurrent)(this.Parent)).ClaimId.ToString();
                                            recordId = ((ReserveHistory)(this)).ClaimId.ToString();
                                            //End MITS: 26955
                                            /*  sbText.Append("Updated reserve type: ");
                                               sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveHistory)(this)).ReserveTypeCode));

                                               sbText.Append(" with Amount:");
                                               sbText.Append(((ReserveHistory)(this)).ChangeAmount);
                                               */
                                            //JIRA RMA-1346 nshah28 Start
                                            //sbText.Append("Updated Reserve: ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Updated", this.Context.ClientId)) + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reserve", this.Context.ClientId)) + ": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveHistory)(this)).ReserveTypeCode));
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)) + ": ");

                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, ((ReserveHistory)(this)).ChangeAmount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            //sbText.Append(", Status: ");
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Status", this.Context.ClientId)) + ": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveHistory)(this)).ResStatusCode));
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Date", this.Context.ClientId)) + ": ");
                                            sbText.Append(DateTime.Now.ToShortDateString());

                                            if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                                            {
                                                sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Policy", this.Context.ClientId)) + ": ");
                                                //Getting Policy Name from POLICY
                                                sSql = "select POLICY_NAME,POLICY_NUMBER from COVERAGE_X_LOSS inner join POLICY_X_CVG_TYPE on COVERAGE_X_LOSS.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID inner join POLICY_X_UNIT on POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID inner join POLICY on POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID where COVERAGE_X_LOSS.CVG_LOSS_ROW_ID=" + ((ReserveHistory)(this)).CoverageLossId;

                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                while (oReader.Read())
                                                {
                                                    sbText.Append(oReader.GetString("POLICY_NAME"));

                                                } /**/
                                            }
                                            //JIRA RMA-1346 nshah28 End
                                            break;
                                        case "RESERVE_CURRENT":
                                            recordId = ((ReserveCurrent)(this)).ClaimId.ToString();
                                            /* sbText.Append("Reserve type: ");
                                             sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ReserveTypeCode));
                                             sbText.Append(" with Amount: ");
                                             sbText.Append(((ReserveCurrent)(this)).IncurredAmount); */

                                            //JIRA RMA-1346 nshah28 Start
                                            string sAction = "";

                                            sSql = "select RES_STATUS_CODE from RSV_SUP_APPROVAL_HIST where RC_ROW_ID=" + ((ReserveCurrent)(this)).RcRowId + " order by DTTM_RCD_ADDED desc";
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            if (oReader.Read())
                                            {
                                                if (oReader.GetValue("RES_STATUS_CODE") != DBNull.Value)
                                                {
                                                    sAction = Context.LocalCache.GetCodeDesc(oReader.GetInt32("RES_STATUS_CODE"));

                                                    if(Context.LocalCache.GetShortCode(oReader.GetInt32("RES_STATUS_CODE")).ToUpper() == "H")
                                                    {
                                                        if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O")
                                                        {
                                                            sAction = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Approved", this.Context.ClientId));
                                                        }
                                                        else
                                                        {
                                                            sAction = ""; //Setting blank, because Hold status we need to get only from below code
                                                        }
                                                    }

                                                    //JIRA RMA-6544(part of RMA-1346) nshah28 start
                                                    if (Context.LocalCache.GetShortCode(oReader.GetInt32("RES_STATUS_CODE")).ToUpper() == "A" || Context.LocalCache.GetShortCode(oReader.GetInt32("RES_STATUS_CODE")).ToUpper() == "R")
                                                    {
                                                        if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "H")
                                                        {
                                                            sAction = Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ResStatusCode); //Hold
                                                        }
                                                        else
                                                        {
                                                            bAllowLog = false; //If user enters amount lower then limit
                                                        }
                                                    }
                                                    //JIRA RMA-6544(part of RMA-1346) nshah28 end

                                                    //JIRA RMA-6077(part of RMA-1346) nshah28 start
                                                    //Stop to log the duplicate entry of Reject reserve 
                                                    if (Context.LocalCache.GetShortCode(oReader.GetInt32("RES_STATUS_CODE")).ToUpper() == "R")
                                                    {
                                                        if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O")
                                                            bAllowLog = false;
                                                    }
                                                    //JIRA RMA-6077(part of RMA-1346) nshah28 end
                                                }
                                            }

                                            if (sAction == "")
                                            {
                                                if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O" && ((ReserveCurrent)(this)).DttmRcdLastUpd == "") 
                                                {
                                                    sAction = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId));
                                                }
                                                else
                                                {
                                                    sAction = Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ResStatusCode);
                                                    if (Context.LocalCache.GetRelatedShortCode(((ReserveCurrent)(this)).ResStatusCode).ToUpper() == "O")
                                                    {
                                                        bAllowLog = false; //setting this false because "Open reserve..." this entry we are getting from reserve_history
                                                    }
                                                }
                                            }

                                            sbText.Append(sAction + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Reserve", this.Context.ClientId)) + ": ");
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((ReserveCurrent)(this)).ReserveTypeCode));
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)) + ": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, ((ReserveCurrent)(this)).IncurredAmount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            //sbText.Append(", Date: ");
                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Date", this.Context.ClientId)) + ": ");
                                            sbText.Append(DateTime.Now.ToShortDateString());

                                            if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                                            {
                                                sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Policy", this.Context.ClientId)) + ": ");
                                                //Getting Policy Name from POLICY
                                                sSql = "select POLICY_NAME,POLICY_NUMBER from COVERAGE_X_LOSS inner join POLICY_X_CVG_TYPE on COVERAGE_X_LOSS.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID inner join POLICY_X_UNIT on POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID inner join POLICY on POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID where COVERAGE_X_LOSS.CVG_LOSS_ROW_ID=" + ((ReserveCurrent)(this)).CoverageLossId;

                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                while (oReader.Read())
                                                {
                                                    sbText.Append(oReader.GetString("POLICY_NAME"));

                                                } /**/
                                            }

                                            //JIRA RMA-1346 nshah28 End

                                            break;

                                        //JIRA RMA-1346 nshah28 Start
                                        case "PMT_APPROVAL_HIST":
                                            //((PmtApprovalHist)(this)).PAHRowId.ToString();
                                            sCtlNumber = string.Empty;
                                            sClaimNumber = string.Empty;

                                            if (((PmtApprovalHist)(this)).DeniedFlag.ToString() == "True")
                                            {

                                               // sbText.Append("Denied ");
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Denied", this.Context.ClientId)));

                                            }
                                            else
                                            {
                                                if (Context.LocalCache.GetShortCode(((PmtApprovalHist)(this)).PmtApprovalStatus).ToUpper() == "R")
                                                {
                                                    sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Approved", this.Context.ClientId)));
                                                }
                                                if (Context.LocalCache.GetShortCode(((PmtApprovalHist)(this)).PmtApprovalStatus).ToUpper() == "H")
                                                {
                                                    sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Hold", this.Context.ClientId)));
                                                }
                                            }

                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Payment", this.Context.ClientId)) + " ");

                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Amount", this.Context.ClientId)) + ": ");
                                            //This used to get amount with baseCurrency
                                            sbText.Append(CommonFunctions.ConvertByCurrencyCode(baseCurrencyCode, ((PmtApprovalHist)(this)).Amount, Context.DbConn.ConnectionString, this.Context.ClientId));

                                            sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.CtlNumber", this.Context.ClientId) + ": "));
                                            sbText.Append(((PmtApprovalHist)(this)).CtlNumber);
                                            recordId = ((PmtApprovalHist)(this)).ClaimId.ToString();

                                            break;
                                        //JIRA RMA-1346 nshah28 END
                                        case "DEFENDANT":
                                            //JIRA RMA-1346 nshah28 START
                                            if (sOptType == "New")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId)));
                                            if (sOptType == "DEL")
                                                sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));
                                            //JIRA RMA-1346 nshah28 END

                                            sbText.Append(" ");
                                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Defendant", this.Context.ClientId)));
                                            sbText.Append(": ");

                                            recordId = ((Defendant)(this)).ClaimId.ToString();
                                            //Deb: Table gets locked on DB server if keyfield value is fetched directly from Default property
                                            //sbText.Append(this.Default);
                                            sbText.Append(((Defendant)this).DefendantEntity.LastName + ", " + ((Defendant)this).DefendantEntity.FirstName);
                                            //Deb
                                            break;
                                        case "PERSON_INVOLVED":
                                            sSql = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM.EVENT_ID =" + ((PersonInvolved)(this)).EventId;
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            sbClaimIds = new StringBuilder();
                                            while (oReader.Read())
                                            {
                                                if (sbClaimIds.Length > 0)
                                                    sbClaimIds.Append(",");
                                                sbClaimIds.Append(oReader.GetInt32("CLAIM_ID").ToString());
                                            }
                                            recordId = sbClaimIds.ToString();
                                            oEntity = ((Entity)(this.Children["PiEntity"]));
                                            sbText.Append(Context.LocalCache.GetCodeDesc(((PersonInvolved)(this)).PiTypeCode));
                                            sbText.Append(":");
                                            sbText.Append(this.Default);
                                            break;
                                        case "UNIT_X_CLAIM":
                                            recordId = ((UnitXClaim)(this)).ClaimId.ToString();
                                            sLogText = "Unit: " + ((UnitXClaim)(this)).Vehicle.VehicleModel + " " + ((UnitXClaim)(this)).Vehicle.VehicleMake + " - " + sLogText;
                                            break;
                                        case "SALVAGE":
                                            string sParentname = string.Empty;
                                            sParentname = ((Salvage)(this)).ParentName;
                                            if (sParentname == "UnitXClaim")
                                                sSql = "SELECT CLAIM.CLAIM_ID, VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_MAKE FROM CLAIM, UNIT_X_CLAIM, VEHICLE WHERE CLAIM.CLAIM_ID = UNIT_X_CLAIM.CLAIM_ID AND VEHICLE.UNIT_ID = UNIT_X_CLAIM.UNIT_ID AND UNIT_X_CLAIM.UNIT_ROW_ID =" + ((Salvage)(this)).ParentId;
                                            else
                                                sSql = "SELECT CLAIM.CLAIM_ID, CLAIM_X_PROPERTYLOSS.PROPERTYTYPE FROM CLAIM, CLAIM_X_PROPERTYLOSS WHERE CLAIM.CLAIM_ID = CLAIM_X_PROPERTYLOSS.CLAIM_ID and CLAIM_X_PROPERTYLOSS.ROW_ID= " + ((Salvage)(this)).ParentId;
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            sbClaimIds = new StringBuilder();
                                            while (oReader.Read())
                                            {
                                                if (sbClaimIds.Length > 0)
                                                    sbClaimIds.Append(",");
                                                sbClaimIds.Append(oReader.GetInt32("CLAIM_ID").ToString());
                                                if (sParentname == "UnitXClaim")
                                                {
                                                    sbText.Append("Salvage for Unit: ");
                                                    sbText.Append(oReader.GetString("VEHICLE_MODEL"));
                                                    sbText.Append(" ");
                                                    sbText.Append(oReader.GetString("VEHICLE_MAKE"));
                                                }
                                                else
                                                {
                                                    sbText.Append("Salvage for Property: ");
                                                    sbText.Append(Context.LocalCache.GetCodeDesc(oReader.GetInt("PROPERTYTYPE")));
                                                }
                                            }
                                            recordId = sbClaimIds.ToString();
                                            break;
                                        case "FUNDS_AUTO":
                                            recordId = ((FundsAuto)(this)).ClaimId.ToString();
                                            sbText.Append("Payments scheduled - Amount: ");
                                            sbText.Append(((FundsAuto)(this)).Amount);
                                            break;
                                        case "POLICY_X_UNIT":
                                            sSql = "SELECT CLAIM_ID FROM CLAIM_X_POLICY WHERE POLICY_ID=" + ((PolicyXUnit)(this)).PolicyId;
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            sbClaimIds = new StringBuilder();
                                            while (oReader.Read())
                                            {
                                                if (sbClaimIds.Length > 0)
                                                    sbClaimIds.Append(",");
                                                sbClaimIds.Append(oReader.GetInt32("CLAIM_ID").ToString());
                                            }
                                            oReader = null;
                                            if (((PolicyXUnit)(this)).UnitType == "V")
                                                sSql = "SELECT VEHICLE_MAKE, VEHICLE_MODEL FROM VEHICLE WHERE UNIT_ID=" + ((PolicyXUnit)(this)).UnitId;
                                            else
                                                sSql = "SELECT ADDR1, ADDR2, ADDR3, ADDR4 FROM PROPERTY_UNIT WHERE PROPERTY_ID=" + ((PolicyXUnit)(this)).UnitId;
                                            oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                            while (oReader.Read())
                                            {
                                                sbText.Append("Policy Unit: ");
                                                sbText.Append(oReader.GetString(0));
                                                sbText.Append(" ");
                                                sbText.Append(oReader.GetString(1));
                                                if (((PolicyXUnit)(this)).UnitType != "V")
                                                {
                                                    sbText.Append(" ");
                                                    sbText.Append(oReader.GetString(2));
                                                    sbText.Append(" ");
                                                    sbText.Append(oReader.GetString(3));
                                                }
                                            }
                                            recordId = sbClaimIds.ToString();
                                            break;
                                        case "POLICY_X_CVG_ENH":
                                        case "POLICY_X_CVG_TYPE":
                                            if (Context.InternalSettings.SysSettings.MultiCovgPerClm != 0 && sTable == "POLICY_X_CVG_TYPE")
                                            {
                                                int iPolicyUnit = ((PolicyXCvgType)(this)).PolicyUnitRowId;
                                                string sUnitType = string.Empty;
                                                sSql = "SELECT CLAIM_ID, UNIT_TYPE FROM CLAIM_X_POLICY, POLICY_X_UNIT WHERE CLAIM_X_POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + iPolicyUnit;
                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                sbClaimIds = new StringBuilder();
                                                while (oReader.Read())
                                                {
                                                    if (sbClaimIds.Length > 0)
                                                        sbClaimIds.Append(",");
                                                    sbClaimIds.Append(oReader.GetInt32("CLAIM_ID").ToString());
                                                    sUnitType = oReader.GetString("UNIT_TYPE");
                                                }
                                                recordId = sbClaimIds.ToString();
                                                oReader = null;
                                                if (sUnitType == "V")
                                                    sSql = "SELECT VEHICLE_MAKE, VEHICLE_MODEL FROM VEHICLE, POLICY_X_UNIT WHERE VEHICLE.UNIT_ID =POLICY_X_UNIT.UNIT_ID AND  POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + iPolicyUnit;
                                                else
                                                    sSql = "SELECT ADDR1, ADDR2, ADDR3, ADDR4 FROM PROPERTY_UNIT, POLICY_X_UNIT WHERE PROPERTY_UNIT.PROPERTY_ID =POLICY_X_UNIT.UNIT_ID AND  POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + iPolicyUnit;
                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                while (oReader.Read())
                                                {
                                                    if (sUnitType == "V")
                                                        sLogText = string.Format("Unit: {0} {1} -",oReader.GetString(0),oReader.GetString(1)) + sLogText;
                                                    else
                                                        sLogText = string.Format("Unit: {0} {1} {2} {3} -", oReader.GetString(0), oReader.GetString(1), oReader.GetString(2), oReader.GetString(3)) + sLogText;
                                                }
                                                sbText.Append("Coverage Type: ");
                                                sbText.Append(Context.LocalCache.GetShortCode(((PolicyXCvgType)(this)).CoverageTypeCode));
                                                sbText.Append(" ");
                                            }
                                            else
                                            {
                                                sbText.Append("Coverage Type: ");
                                                if(this.Table == "POLICY_X_CVG_ENH")
                                                {
                                                    sSql = "SELECT CLAIM_ID FROM CLAIM WHERE PRIMARY_POLICY_ID =" + ((PolicyXCvgEnh)(this)).PolicyId;
                                                    sbText.Append(Context.LocalCache.GetShortCode(((PolicyXCvgEnh)(this)).CoverageTypeCode));
                                                }
                                                else
                                                {
                                                    sSql = "SELECT CLAIM_ID FROM CLAIM WHERE PRIMARY_POLICY_ID =" + ((PolicyXCvgType)(this)).PolicyId;
                                                    sbText.Append(Context.LocalCache.GetShortCode(((PolicyXCvgType)(this)).CoverageTypeCode));
                                                }
                                                oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                                                sbClaimIds = new StringBuilder();
                                                while (oReader.Read())
                                                {
                                                    if (sbClaimIds.Length > 0)
                                                        sbClaimIds.Append(",");
                                                    sbClaimIds.Append(oReader.GetInt32("CLAIM_ID").ToString());
                                                }
                                                recordId = sbClaimIds.ToString();
                                            }
                                            break;
                                    }
                                }
                                if(sbText.Length > 0)
                                    sbText.Append(" - ");
                                sbText.Append(sLogText);
                                if (oEntity != null)
                                {
                                    objPlaceHolder.Add("<ADDR1>", oEntity.Addr1);
                                    objPlaceHolder.Add("<ADDR2>", oEntity.Addr2);
                                    objPlaceHolder.Add("<ADDR3>", oEntity.Addr3);
                                    objPlaceHolder.Add("<ADDR4>", oEntity.Addr4);
                                    objPlaceHolder.Add("<CITY>", oEntity.City);
                                    objPlaceHolder.Add("<STATE>", Context.LocalCache.GetStateCode(oEntity.StateId));
                                    objPlaceHolder.Add("<PSTCDE>", oEntity.ZipCode);
                                    objPlaceHolder.Add("<COUNTRY>", Context.LocalCache.GetCodeDesc(oEntity.CountryCode));
                                    objPlaceHolder.Add("<COUNTY>", oEntity.County);
                                }
                                objPlaceHolder.Add("<CLAIMNUMBER>", sRecordNumber);

                                foreach (KeyValuePair<string, string> opair in objPlaceHolder)
                                {
                                    if (sLogText.Contains(opair.Key))
                                    {
                                        sLogText.Replace(opair.Key, opair.Value);
                                    }
                                }
                    
                                oEntity = null;
                                objPlaceHolder = null;
                                if(oReader != null)
                                    oReader.Dispose();
                                sbClaimIds = null;
                                if(sbText.Length > 0)
                                    sLogText = sbText.ToString();
                                if(bAllowLog)
                                    CommonFunctions.CreateClaimActivityLog(Context.DbConn.ConnectionString, Context.RMUser.LoginName, recordId, iActType, sLogText, this.Context.ClientId);
                                bAllowLog = false;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new RMAppException("LogClaimActivity.Save.Exception",e);
                }
                finally
                {
                    oEntity = null;
                    oClaim = null;
                    sbText = null;
                    objPlaceHolder = null;
                    if (oReader != null)
                        oReader.Dispose();
                }
            }

            private void LogActivityForPolicyInterface(bool isNew)
            {
                int iForeignTableId = 0;
                string sSelectSQL = string.Empty;
                int iClaimId =0;
                int iForeignTableKey=0;
                int iActivityRowId = 0;
                Claim objClaim = null;
                int iActivityType = 0;
                double iChangeAmount = 0;
                StringBuilder strSQL = null;
                Dictionary<string, object> dictParams = null;
                bool bUploadActivity = true;
                try
                {
                    //  oClaim = this.Parent as Claim;
                    if (Context.InternalSettings.SysSettings.UsePolicyInterface && (this.Table == "RESERVE_CURRENT"  || this.Table == "FUNDS" || this.Table == "CLAIM")) //|| this.Table == "EVENT"  || this.Table == "EVENT_SUPP" 
                    {
                        // sTable = this.Table;
                        switch (this.Table)
                       {
                           
                            case "FUNDS":
                                iForeignTableId = this.Context.LocalCache.GetTableId("FUNDS");
                               // if (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")) || (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") )&& (this as Funds).VoidFlag) || (this as Funds).CollectionFlag)
                                //if (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")) || (this as Funds).VoidFlag)
                                //{
                                //aaded for aaggarwal29 : MITS 3773, RMA-7822 start
                                string sSelectSQLNew = string.Empty;
                                int iActivityRowIdNew = Int32.MinValue;
                                int iActivityTypeExistng, iAccountIdExistng, iCheckStatusExistng, iVoidFlagExistng;
                                bool bUploadPayment = true;
                                // aaggarwal29 : End for MITS 3773, RMA-7822
                                    iClaimId = (this as Funds).ClaimId;
                                    iForeignTableKey = this.KeyFieldValue;
                                    objClaim = (Claim)this.Context.Factory.GetDataModelObject("Claim", false);
                                    objClaim.MoveTo(iClaimId);
                                    foreach (FundsTransSplit objFundsTransSplit in (this as Funds).TransSplitList)
                                {
                                      ReserveCurrent objRsvCurr= (ReserveCurrent)this.Context.Factory.GetDataModelObject("ReserveCurrent", false);
                                      objRsvCurr.MoveTo(objFundsTransSplit.RCRowId);
                                      CvgXLoss objCvgXLoss= (CvgXLoss)this.Context.Factory.GetDataModelObject("CvgXLoss", false);
                                      objCvgXLoss.MoveTo(objRsvCurr.CoverageLossId);
                                    PolicyXCvgType objPolicyXCvgType = (PolicyXCvgType)this.Context.Factory.GetDataModelObject("PolicyXCvgType", false);
                                    objPolicyXCvgType.MoveTo(objCvgXLoss.PolCvgRowId);
                                    PolicyXUnit objPolUnit = (PolicyXUnit)this.Context.Factory.GetDataModelObject("PolicyXUnit", false);

                                    objPolUnit.MoveTo(objPolicyXCvgType.PolicyUnitRowId);
                                //foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                                //{
                                    Policy objPolicy = (Policy)this.Context.Factory.GetDataModelObject("Policy", false);
                                    objPolicy.MoveTo(objPolUnit.PolicyId);

                                        

                                        if (objPolicy.PolicySystemId > 0)
                                        {
                                            
                                            if (((this as Funds).VoidFlag) && ((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")))
                                            {
                                              
                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("OA", "ACTIVITY_TYPE");
                                            }
                                            else
                                            {
                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("PP", "ACTIVITY_TYPE");
                                            }


                                            sSelectSQL = "SELECT ACTIVITY_ROW_ID FROM ACTIVITY_TRACK WHERE POLICY_SYSTEM_ID =" + objPolicy.PolicySystemId + " AND CLAIM_ID=" + iClaimId + " AND FOREIGN_TABLE_ID =" + iForeignTableId + " AND FOREIGN_TABLE_KEY = " + iForeignTableKey + " AND UPLOAD_FLAG = -1 AND CHECK_STATUS <> " + this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS");

                                            iActivityRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);
                                            //aaggarwal29 : Start for MITS 3773, RMA-7822
                                            if (iActivityRowId == 0) // no record found -- either (i) record is already uploaded once or (ii) it is a new record entering first time
                                            {
                                                sSelectSQLNew = "SELECT * FROM ACTIVITY_TRACK WHERE POLICY_SYSTEM_ID =" + objPolicy.PolicySystemId + " AND CLAIM_ID=" + iClaimId + " AND FOREIGN_TABLE_ID =" + iForeignTableId + " AND FOREIGN_TABLE_KEY = " + iForeignTableKey + " ORDER BY ACTIVITY_ROW_ID DESC"; 
                                                using (DbReader objReader = DbFactory.ExecuteReader(Context.DbConn.ConnectionString, sSelectSQLNew))
                                                {
                                                    if (objReader.Read()) // case (i)
                                                    {
                                                        iActivityRowIdNew = Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"),this.Context.ClientId);
                                                        iActivityTypeExistng = Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), this.Context.ClientId);
                                                        iAccountIdExistng = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), this.Context.ClientId);
                                                        // iReserveStatusExistng = Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), this.Context.ClientId); //removing this criteria as it is not updated properly while printing checks
                                                        iCheckStatusExistng = Conversion.ConvertObjToInt(objReader.GetValue("CHECK_STATUS"), this.Context.ClientId);
                                                        iVoidFlagExistng = Conversion.ConvertObjToInt(objReader.GetValue("VOID_FLAG"), this.Context.ClientId);
                                                        if (iActivityTypeExistng == iActivityType && iAccountIdExistng == (this as Funds).AccountId && iCheckStatusExistng == (this as Funds).StatusCode && (iVoidFlagExistng == Conversion.ConvertBoolToInt((this as Funds).VoidFlag,this.Context.ClientId)))
                                                        {
                                                            // if any one of previous data is changed, we need to upload again.
                                                            bUploadPayment = false;
                                                        }
                                                    }
                                                    //else //case (ii)
                                                    //{
                                                    //}
                                                }

                                            }
                                            //aaggarwal29 : End for MITS 3773, RMA-7822
                                            //aaggarwal29 : MITS 35586 start
                                            if (bUploadPayment) // added If clause for MITS 3773, RMA-7822: aaggarwal29
                                            {
                                                if ((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("RD", "CHECK_STATUS"))
                                                {
                                                CommonFunctions.CreateLogForPolicyInterface(Context.DbConn.ConnectionString, iActivityRowId, Context.RMUser.LoginName, iClaimId, iForeignTableId, iForeignTableKey, 0, objPolicy.PolicySystemId, iActivityType, (this as Funds).AccountId, 0, objRsvCurr.ResStatusCode, 0, (this as Funds).StatusCode, Conversion.ConvertBoolToInt((this as Funds).VoidFlag, this.Context.ClientId), Conversion.ConvertBoolToInt((this as Funds).CollectionFlag, this.Context.ClientId), true, this.Context.ClientId,0,0,string.Empty);
                                                }
                                                else
                                                {
                                                CommonFunctions.CreateLogForPolicyInterface(Context.DbConn.ConnectionString, iActivityRowId, Context.RMUser.LoginName, iClaimId, iForeignTableId, iForeignTableKey, 0, objPolicy.PolicySystemId, iActivityType, (this as Funds).AccountId, 0, objRsvCurr.ResStatusCode, 0, (this as Funds).StatusCode, Conversion.ConvertBoolToInt((this as Funds).VoidFlag, this.Context.ClientId), Conversion.ConvertBoolToInt((this as Funds).CollectionFlag, this.Context.ClientId), false, this.Context.ClientId,0,0,string.Empty);
                                                }
                                            }
											//aaggarwal29 : MITS 35586 end
                                            //Ankit Start : Work on MITS - 34569
                                            objPolicy.Dispose();
                                            objRsvCurr.Dispose();
                                            objCvgXLoss.Dispose();
                                            objPolicyXCvgType.Dispose();
                                            objPolUnit.Dispose();
                                            break;
                                            //Ankit End
                                        }
                                        objPolicy.Dispose();
                                        objRsvCurr.Dispose();
                                        objCvgXLoss.Dispose();
                                        objPolicyXCvgType.Dispose();
                                        objPolUnit.Dispose();
                                    }
                        
                                    objClaim.Dispose();

                                
                                break;

                            case "RESERVE_CURRENT":

                                //rupal:mits 33043
                               //if (!string.Equals((this as ReserveCurrent).sUpdateType, "Funds", StringComparison.InvariantCultureIgnoreCase))
                                if ((!string.Equals((this as ReserveCurrent).sUpdateType, "Funds", StringComparison.InvariantCultureIgnoreCase)) && ((this as ReserveCurrent).IsFirstFinalReserve != -1) && (!(this as ReserveCurrent).OnlyReserveReasonChanged) && (this as ReserveCurrent).SaveActivityTrackForReserve)
                                {
                                    if (!(this.Context.LocalCache.GetShortCode((this as ReserveCurrent).ResStatusCode) == "H") && !(this.Context.LocalCache.GetShortCode((this as ReserveCurrent).ResStatusCode) == "R"))
                                    {
                                        iForeignTableId = this.Context.LocalCache.GetTableId("RESERVE_CURRENT");
                                        iClaimId = (this as ReserveCurrent).ClaimId;
                                        iForeignTableKey = this.KeyFieldValue;
                                        int maxReserveHistory = 0;
                                        if ((this as ReserveCurrent).ReserveHistoryList != null)
                                        {
                                            maxReserveHistory = (this as ReserveCurrent).ReserveHistoryList.Cast<ReserveHistory>().Max(x => x.RsvRowId);
                                            ReserveHistory objRsvHist = (ReserveHistory)this.Context.Factory.GetDataModelObject("ReserveHistory", false);
                                            objRsvHist.MoveTo(maxReserveHistory);
                                            iChangeAmount = objRsvHist.ChangeAmount;

                                            objRsvHist.Dispose();
                                            objRsvHist = null;
                                        }



                                        objClaim = (Claim)this.Context.Factory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(iClaimId);
                                        CvgXLoss objCvgXLoss = (CvgXLoss)this.Context.Factory.GetDataModelObject("CvgXLoss", false);
                                        objCvgXLoss.MoveTo((this as ReserveCurrent).CoverageLossId);
                                        PolicyXCvgType objPolicyXCvgType = (PolicyXCvgType)this.Context.Factory.GetDataModelObject("PolicyXCvgType", false);
                                        objPolicyXCvgType.MoveTo(objCvgXLoss.PolCvgRowId);
                                        PolicyXUnit objPolUnit = (PolicyXUnit)this.Context.Factory.GetDataModelObject("PolicyXUnit", false);

                                        objPolUnit.MoveTo(objPolicyXCvgType.PolicyUnitRowId);
                                        //foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                                        //{
                                        Policy objPolicy = (Policy)this.Context.Factory.GetDataModelObject("Policy", false);
                                        objPolicy.MoveTo(objPolUnit.PolicyId);

                                        if (objPolicy.PolicySystemId > 0)
                                        {
                                            if (isNew)
                                            {
                                                if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "O")
                                                {

                                                    iActivityType = objPolicy.Context.LocalCache.GetCodeId("OC", "ACTIVITY_TYPE");

                                                }
                                                else if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "R")
                                                {
                                                    iActivityType = objPolicy.Context.LocalCache.GetCodeId("RO", "ACTIVITY_TYPE");

                                                }
                                                else if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "C")
                                                {
                                                    sSelectSQL = "SELECT COUNT(1) FROM FUNDS_TRANS_SPLIT  WHERE RC_ROW_ID=" + iForeignTableKey;
                                                    int iTransCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);

                                                    if (iTransCount > 0)
                                                        iActivityType = objPolicy.Context.LocalCache.GetCodeId("CL", "ACTIVITY_TYPE");
                                                    else
                                                        iActivityType = objPolicy.Context.LocalCache.GetCodeId("NC", "ACTIVITY_TYPE");

                                                }
                                                else
                                                {
                                                    iActivityType = objPolicy.Context.LocalCache.GetCodeId("OC", "ACTIVITY_TYPE");

                                                }
                                            }
                                            else
                                            {
                                                if (this.Context.LocalCache.GetShortCode((this as ReserveCurrent).ResStatusCode) == "A")
                                                {

                                                    if (iChangeAmount == 0)
                                                    {
                                                        ReserveHistory objRsvHist = (ReserveHistory)this.Context.Factory.GetDataModelObject("ReserveHistory", false);
                                                        objRsvHist.MoveTo(maxReserveHistory-1);
                                                        iChangeAmount = objRsvHist.ChangeAmount;

                                                        objRsvHist.Dispose();
                                                        objRsvHist = null;
                                                    }
                                                    if ((this as ReserveCurrent).ReserveHistoryList.Count == 2)
                                                    {
                                                         if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "C")
                                                        {
                                                            sSelectSQL = "SELECT COUNT(1) FROM FUNDS_TRANS_SPLIT  WHERE RC_ROW_ID=" + iForeignTableKey;
                                                            int iTransCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);

                                                            if (iTransCount > 0)
                                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("CL", "ACTIVITY_TYPE");
                                                            else
                                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("NC", "ACTIVITY_TYPE");
                                                        }
                                                        else
                                                        {
                                                            iActivityType = objPolicy.Context.LocalCache.GetCodeId("OC", "ACTIVITY_TYPE");

                                                        }
                                                    }
                                                    else if ((this as ReserveCurrent).ReserveHistoryList.Count > 2)
                                                    {
                                                        if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "C")
                                                        {
                                                            sSelectSQL = "SELECT COUNT(1) FROM FUNDS_TRANS_SPLIT  WHERE RC_ROW_ID=" + iForeignTableKey;
                                                            int iTransCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);

                                                            if (iTransCount > 0)
                                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("CL", "ACTIVITY_TYPE");
                                                            else
                                                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("NC", "ACTIVITY_TYPE");
                                                        }
                                                        else
                                                        {
                                                            iActivityType = objPolicy.Context.LocalCache.GetCodeId("RC", "ACTIVITY_TYPE");

                                                        }
                                                    }
                                                }
                                                else   if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "C")
                                                {
                                                    sSelectSQL = "SELECT COUNT(1) FROM FUNDS_TRANS_SPLIT  WHERE RC_ROW_ID=" + iForeignTableKey;
                                                    int iTransCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);

                                                    if (iTransCount > 0)
                                                        iActivityType = objPolicy.Context.LocalCache.GetCodeId("CL", "ACTIVITY_TYPE");
                                                    else
                                                        iActivityType = objPolicy.Context.LocalCache.GetCodeId("NC", "ACTIVITY_TYPE");

                                                }
                                                else if (objPolicy.Context.LocalCache.GetShortCode(objPolicy.Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode)) == "R")
                                                {
                                                    iActivityType = objPolicy.Context.LocalCache.GetCodeId("RO", "ACTIVITY_TYPE");

                                                }

                                                else
                                                {
                                                    iActivityType = objPolicy.Context.LocalCache.GetCodeId("RC", "ACTIVITY_TYPE");

                                                }
                                            }

                                            //rsharma220 JIRA 4368
                                            double iBalanceAmount = 0;
                                            double iChangeAmt = 0;
                                            int iRsvStatus = 0;

                                         //  sSelectSQL = "SELECT * FROM ACTIVITY_TRACK WHERE CLAIM_ID=" + iClaimId + " AND FOREIGN_TABLE_ID =" + iForeignTableId + " AND FOREIGN_TABLE_KEY = " + iForeignTableKey + " AND UPLOAD_FLAG = -1 ORDER BY ACTIVITY_ROW_ID DESC ";

                                                dictParams = new Dictionary<string, object>();
                                                 strSQL = new StringBuilder();

                                                strSQL.AppendFormat("SELECT * FROM ACTIVITY_TRACK WHERE CLAIM_ID= {0} AND FOREIGN_TABLE_ID ={1}  AND FOREIGN_TABLE_KEY = {2} ", "~CLAIM_ID~", "~FOREIGN_TABLE_ID~", "~FOREIGN_TABLE_KEY~");
                                                strSQL.AppendFormat("AND UPLOAD_FLAG = -1 ");
                                                strSQL.AppendFormat("ORDER BY ACTIVITY_ROW_ID DESC");

                                                dictParams.Add("CLAIM_ID", iClaimId);
                                             dictParams.Add("FOREIGN_TABLE_ID", iForeignTableId);
                                             dictParams.Add("FOREIGN_TABLE_KEY", iForeignTableKey);

            //TODO: Migrate this code over to the Riskmaster.Security.Authentication library
                                             using (DbReader objrdr = DbFactory.ExecuteReader(Context.DbConn.ConnectionString, strSQL.ToString(), dictParams))
                                             {


                                                 //rsharma220 JIRA 4368
                                                 if (objrdr.Read())
                                                 {
                                                     iBalanceAmount = Conversion.ConvertObjToDouble(objrdr.GetValue("RESERVE_AMOUNT"), this.Context.ClientId);
                                                     iRsvStatus = Conversion.ConvertObjToInt(objrdr.GetValue("RESERVE_STATUS"), this.Context.ClientId);
                                                     iChangeAmt = Conversion.ConvertObjToDouble(objrdr.GetValue("CHANGE_AMOUNT"), this.Context.ClientId);

                                                 }
                                             }

                                            //if (iActivityRowId == 0)
                                            if(((this as ReserveCurrent).BalanceAmount != iBalanceAmount) || ( Context.LocalCache.GetRelatedCodeId((this as ReserveCurrent).ResStatusCode) != Context.LocalCache.GetRelatedCodeId( iRsvStatus))||  (iChangeAmount != iChangeAmt) )
                                            {
												//aaggarwal29 : MITS 35586 start
                                                CommonFunctions.CreateLogForPolicyInterface(Context.DbConn.ConnectionString, iActivityRowId, Context.RMUser.LoginName, iClaimId, iForeignTableId, iForeignTableKey, 0, objPolicy.PolicySystemId, iActivityType, 0, (this as ReserveCurrent).BalanceAmount, (this as ReserveCurrent).ResStatusCode, iChangeAmount, 0, 0, 0, false, this.Context.ClientId,0,0,string.Empty);
												//aaggarwal29 : MITS 35586 end

                                            }
                                            // }
                                            objPolicy.Dispose();
                                            objCvgXLoss.Dispose();
                                            objPolicyXCvgType.Dispose();
                                            objPolUnit.Dispose();
                                        }
                                        objClaim.Dispose();
                                    }
                                }
                        
                                break;

                            case "CLAIM":
                                iForeignTableId = 0;
                                iClaimId = this.KeyFieldValue;
                                iForeignTableKey = 0;
                                objClaim = (Claim)this.Context.Factory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(iClaimId);

                                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                                {
                                    if (objClaimXPolicy != null) //RMA:14824
                                    {
                                        bUploadActivity = true;
                                        Policy objPolicy = (Policy)this.Context.Factory.GetDataModelObject("Policy", false);
                                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);

                                        if (objClaim.ReserveCurrentList.Count == 0 && GetPolicySystemTypeText(objPolicy.PolicySystemId) == CommonFunctions.GetPolicySystemTypeIndicatorText(Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
                                            bUploadActivity = false;
                                        else if (GetPolicySystemTypeText(objPolicy.PolicySystemId) == CommonFunctions.GetPolicySystemTypeIndicatorText(Constants.POLICY_SYSTEM_TYPE.STAGING)) //RMA-10039
                                            bUploadActivity = false; //RMA-10039


                                        /*if (objPolicy.PolicySystemId > 0 && 
                                            GetPolicySystemTypeText(objPolicy.PolicySystemId)!= CommonFunctions.GetPolicySystemTypeIndicatorText(Constants.POLICY_SYSTEM_TYPE.INTEGRAL)) //sanoopsharma - Integral does not support claim related activity uploads.*/
                                        if (objPolicy.PolicySystemId > 0 && bUploadActivity)
                                        {
                                            sSelectSQL = "SELECT ACTIVITY_ROW_ID FROM ACTIVITY_TRACK WHERE POLICY_SYSTEM_ID = " + objPolicy.PolicySystemId + " AND CLAIM_ID=" + iClaimId + " AND FOREIGN_TABLE_ID =" + iForeignTableId + " AND FOREIGN_TABLE_KEY = " + iForeignTableKey + " AND UPLOAD_FLAG = -1";

                                            iActivityRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(Context.DbConn.ConnectionString, sSelectSQL), this.Context.ClientId);

                                            if (iActivityRowId == 0)
                                            {
                                                //aaggarwal29 : MITS 35586 start
                                                CommonFunctions.CreateLogForPolicyInterface(Context.DbConn.ConnectionString, iActivityRowId, Context.RMUser.LoginName, iClaimId, iForeignTableId, iForeignTableKey, 0, objPolicy.PolicySystemId, 0, 0, 0, 0, 0, 0, 0, 0, false, this.Context.ClientId, 0, 0, string.Empty);
                                                //aaggarwal29 : MITS 35586 end

                                            }
                                        }
                                        objPolicy.Dispose();
                                    }
                                }
                                
                                
                                objClaim.Dispose();


                                break;

                        }



                    }
                }
                catch (Exception e)
                {
                    throw new RMAppException("LogActivityForPolicyInterface.Save.Exception", e);
                }
                finally
                {
                    if (objClaim != null)
                        objClaim.Dispose();
                    dictParams = null;
                    strSQL = null;

                }

            }
           private void LogClaimActivityForPolicy(string sOptType, int irecordId, int iPolicy, bool bUseEnhPolicy)
           {
               bool bAllowLog = false;
               int iTableId = 0;
               int iOptType = 0;
               string sLogText = string.Empty;
               int iActType = 0;
               DbReader oReader = null;
               string sSql = string.Empty;
               //bool bUseEnhPolicy = false;
               string sTable = string.Empty;
               bool bAllowUnit = false;
               string sTempLog = string.Empty;
               int iTempActType = 0;
               string sText = string.Empty;
                //JIRA RMA-1346 nshah28 Start
                StringBuilder sbText = null;
                sbText = new StringBuilder();
                //JIRA RMA-1346 nshah28 End
               try
               {
                   if (this.Context.InternalSettings.SysSettings.ClaimActivityLog)
                   {
                       iTableId = this.Context.LocalCache.GetTableId("CLAIM_X_POLICY");
                       iOptType = this.Context.LocalCache.GetCodeId(sOptType, "OPERATION_TYPE");
                       bAllowLog = CommonFunctions.bClaimActLogSetup(Context.DbConn.ConnectionString, iTableId, iOptType, ref sLogText, ref iActType, this.Context.ClientId);
                       if (bUseEnhPolicy)
                       {
                           sTable = "POLICY_X_CVG_ENH";
                       }
                       else
                       {
                           sTable = "POLICY_X_CVG_TYPE";
                       }
                       if (bAllowLog)
                       {
                           if (bUseEnhPolicy)
                               sSql = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = ";
                           else
                           {
                                if (Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                                    //JIRA RMA-1346 nshah28 Start
                                    // sSql = "SELECT POLICY_NAME + ' (' + POLICY_NUMBER + ')'  FROM POLICY WHERE POLICY_ID = ";
                                    sSql = "SELECT CASE WHEN POLICY_NUMBER IS NULL THEN POLICY_NAME ELSE POLICY_NAME + ' (' + POLICY_NUMBER + ')' END AS policy_NameNumber,Policy_Symbol,Master_Company,Module,EFFECTIVE_DATE,EXPIRATION_DATE  FROM POLICY WHERE POLICY_ID = ";
                                //JIRA RMA-1346 nshah28 End
                                else
                                    //JIRA RMA-1346 nshah28 Start
                                    // sSql = "SELECT POLICY_NAME || ' (' || POLICY_NUMBER || ')' FROM POLICY WHERE POLICY_ID = ";
                                    sSql = "SELECT CASE WHEN POLICY_NUMBER IS NULL THEN POLICY_NAME ELSE POLICY_NAME || ' (' || POLICY_NUMBER || ')' END AS policy_NameNumber,Policy_Symbol,Master_Company,Module,EFFECTIVE_DATE,EXPIRATION_DATE FROM POLICY WHERE POLICY_ID = ";
                                //JIRA RMA-1346 nshah28 End
                           }
                           oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql + iPolicy);
                           while (oReader.Read())
                           {
                                //sLogText = string.Format("POLICY: {0} -", oReader.GetString(0)) + sLogText;
                                //JIRA RMA-1346 nshah28 Start
                                if (sOptType == "New")
                                    sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Add", this.Context.ClientId)));
                                if (sOptType == "DEL")
                                    sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Del", this.Context.ClientId)));

                                sbText.Append(" " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.Policy", this.Context.ClientId)) + ": ");

                                sbText.Append(oReader.GetString("policy_NameNumber"));

                                // string sEFFECTIVE_DATE = Conversion.ToDate(((ReserveHistory)(this)).DttmRcdAdded).ToShortDateString()
                                //DateTime.Parse(sEFFECTIVE_DATE).ToString("MM/dd/yyyy")); //If dateformat is not in MM/DD/YYYY
                                string sEFFECTIVE_DATE = Conversion.GetDBDateFormat(oReader.GetString("EFFECTIVE_DATE"), "d");
                                string sEXPIRATION_DATE = Conversion.GetDBDateFormat(oReader.GetString("EXPIRATION_DATE"), "d");

                                sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.EffDate", this.Context.ClientId)) + ": "); //sbText.Append(", Eff Date: ");
                                sbText.Append(sEFFECTIVE_DATE);
                                sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.ExpDate", this.Context.ClientId)) + ": ");  //sbText.Append(", Exp Date: ");
                                sbText.Append(sEXPIRATION_DATE);
                                if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 && this.Context.InternalSettings.SysSettings.UsePolicyInterface)
                                {
                                    if (oReader.GetValue("Policy_Symbol") != DBNull.Value)
                                    {
                                        sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.PolicySymbol", this.Context.ClientId)) + ": "); //sbText.Append(", Policy Symbol: ");                                    
                                        sbText.Append(Convert.ToString(oReader.GetString("Policy_Symbol")));
                                    }
                                    if (oReader.GetValue("Master_Company") != DBNull.Value)
                                    {
                                        sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MasterCompany", this.Context.ClientId)) + ": ");  //sbText.Append(", Master Company: ");
                                        sbText.Append(Convert.ToString(oReader.GetString("Master_Company")));
                                    }
                                    if (oReader.GetValue("Module") != DBNull.Value)
                                    {
                                        sbText.Append(", " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.ModuleNo", this.Context.ClientId)) + ": "); //sbText.Append(", Module No: ");
                                        sbText.Append(Convert.ToString(oReader.GetString("Module")));
                                    }
                                }
                            }

                            sbText.Append(sLogText);

                            if (sbText.Length > 0)
                                sLogText = sbText.ToString();
                            //JIRA RMA-1346 nshah28 End

                            CommonFunctions.CreateClaimActivityLog(Context.DbConn.ConnectionString, Context.RMUser.LoginName, irecordId.ToString(), iActType, sLogText, this.Context.ClientId);
                            oReader = null;

                       }
                       sLogText = string.Empty;
                       iActType = 0;
                       if (Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                       {
                           iTableId = this.Context.LocalCache.GetTableId(sTable);
                           iOptType = this.Context.LocalCache.GetCodeId(sOptType, "OPERATION_TYPE");
                           bAllowLog = CommonFunctions.bClaimActLogSetup(Context.DbConn.ConnectionString, iTableId, iOptType, ref sLogText, ref iActType, this.Context.ClientId);

                           if (bAllowLog)
                           {

                               sSql = "SELECT COVERAGE_TYPE_CODE FROM " + sTable + " WHERE POLICY_ID = " + iPolicy;
                               oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                               while (oReader.Read())
                               {
                                   sText = string.Format("Coverage Type: {0} -", Context.LocalCache.GetShortCode(oReader.GetInt32(0))) + sLogText;
                                   CommonFunctions.CreateClaimActivityLog(Context.DbConn.ConnectionString, Context.RMUser.LoginName, irecordId.ToString(), iActType, sText, this.Context.ClientId);
                                   sText = string.Empty;
                               }

                               oReader = null;
                           }
                       }
                       else
                       {
                           bAllowUnit = CommonFunctions.bClaimActLogSetup(Context.DbConn.ConnectionString, Context.LocalCache.GetTableId("POLICY_X_UNIT"), iOptType, ref sTempLog, ref iTempActType, this.Context.ClientId);
                           bAllowLog = CommonFunctions.bClaimActLogSetup(Context.DbConn.ConnectionString, Context.LocalCache.GetTableId(sTable), iOptType, ref sLogText, ref iActType, this.Context.ClientId);
                           int iLastUnit = 0;
                           if (bAllowLog || bAllowUnit)
                           {
                               if (Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                                   sSql = "SELECT PC.COVERAGE_TYPE_CODE, CASE PU.UNIT_TYPE WHEN 'V' THEN VEHICLE.VEHICLE_MAKE + ' ' + VEHICLE.VEHICLE_MODEL ELSE PROPERTY_UNIT.ADDR1 + ' ' + ISNULL(PROPERTY_UNIT.ADDR2, '') + ' ' + ISNULL(PROPERTY_UNIT.ADDR3, '') + ' ' + ISNULL(PROPERTY_UNIT.ADDR4, '')";
                               else if (Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                                   sSql = "SELECT PC.COVERAGE_TYPE_CODE, CASE PU.UNIT_TYPE WHEN 'V' THEN VEHICLE.VEHICLE_MAKE || ' ' || VEHICLE.VEHICLE_MODEL ELSE PROPERTY_UNIT.ADDR1 || ' ' || NVL(PROPERTY_UNIT.ADDR2, '') || ' ' || NVL(PROPERTY_UNIT.ADDR3, '') || ' ' || NVL(PROPERTY_UNIT.ADDR4, '')";
                               sSql = sSql + " END AS UNIT, PU.POLICY_UNIT_ROW_ID FROM POLICY_X_CVG_TYPE PC, POLICY_X_UNIT PU LEFT OUTER JOIN VEHICLE ON VEHICLE.UNIT_ID = PU.UNIT_ID";
                               sSql = sSql + " LEFT OUTER JOIN PROPERTY_UNIT ON PU.UNIT_ID = PROPERTY_UNIT.PROPERTY_ID WHERE PC.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID";
                               sSql = sSql + " AND PU.POLICY_ID =" + iPolicy + " ORDER BY PU.POLICY_UNIT_ROW_ID";
                               oReader = DbFactory.GetDbReader(Context.DbConn.ConnectionString, sSql);
                               while (oReader.Read())
                               {
                                   if (oReader.GetInt32("POLICY_UNIT_ROW_ID") != iLastUnit && bAllowUnit)
                                   {
                                       sText = string.Format("Unit: {0} -", oReader.GetString("UNIT")) + sTempLog;
                                       CommonFunctions.CreateClaimActivityLog(Context.DbConn.ConnectionString, Context.RMUser.LoginName, irecordId.ToString(), iTempActType, sText, this.Context.ClientId);
                                       sText = string.Empty;
                                   }
                                   if (bAllowLog)
                                   {
                                       sText = string.Format("Coverage Type: {0} - Unit: {1} - ", Context.LocalCache.GetShortCode(oReader.GetInt32("COVERAGE_TYPE_CODE")), oReader.GetString("UNIT")) + sLogText;
                                       CommonFunctions.CreateClaimActivityLog(Context.DbConn.ConnectionString, Context.RMUser.LoginName, irecordId.ToString(), iActType, sText, this.Context.ClientId);
                                       sText = string.Empty;
                                   }
                                   iLastUnit = oReader.GetInt32("POLICY_UNIT_ROW_ID");
                               }
                               oReader = null;
                           }
                       }
                   }
               }
               catch (Exception e)
               {
                   throw e;
               }
               finally
               {
                   if (oReader != null)
                   {
                       oReader.Dispose();
                   }
               }
           }
			/// <summary>
			/// Refresh clears the values for all fields and children.
			/// </summary>
			public virtual void Refresh(bool bClearChildren)
			{
                //Start(06/08/2010)-Sumit - MITS# 20483 - If Children are required bClearChildren should be set as False. 
                bool m_isNew = true;
                if (bClearChildren == true)
                {
                    this.Clear();
                    m_isNew = false;
                }
                //Sumit - End

				if(!this.IsNew && m_isNew == false)
				{
					//BSB This is probably dead code - key field would be cleared to zero by Clear()...
					this.LoadData();
					this.ForceLoadAllChildren();
				}
				else
				{
					(this as IDataModel).IsStale = false;
					//BSB Added to avoid leaving "datachanged flag" set - can cause severe infinite recursion
					// if this flag is not correct during save.
					(this as IDataModel).DataChanged= false; 
				}
			}

            /// <summary>
            /// Author: Sumit Kumar
            /// Date : 06/08/2010
            /// MITS: 20483
            /// Description : If Refresh is called directly then Child values will be cleared.
            /// </summary>
            public virtual void Refresh()
            {
                Refresh(true);
            }

			//This function touches all child properties via their property get routines.
			// Could be a performance issue but is required before a delete can be properly performed.
			protected void ForceLoadAllChildren()
			{
				foreach( string s in this.m_Children)
					this.GetProperty(s);
				object o = this.Supplementals;
			}
			#endregion

			#region Generic Property code.
			protected void SetFieldAndNavTo(string fieldName, object targetValue,string navChildName)
			{SetFieldAndNavTo(fieldName,targetValue,navChildName,false);}
			protected void SetFieldAndNavTo(string fieldName, object targetValue,string navChildName,bool bForceLoad)
			{
				int keyValue=0;
				bool bPrevDataChanged = false;
				bool bFieldDataChanged = false;

				if(targetValue !=null)
					keyValue = (int)targetValue;
 
				bPrevDataChanged = this.m_DataChanged;
				this.m_DataChanged = false;

				try
				{
					SetField(fieldName, targetValue);
					bFieldDataChanged = this.m_DataChanged;
				}
				finally
				{ this.m_DataChanged |= bPrevDataChanged;	}

				

				if(keyValue != 0 &&  (bFieldDataChanged || bForceLoad))
					try{(this.m_Children[navChildName] as DataObject).MoveTo(keyValue);} 
					catch(Exception e)
					{
						(this.m_Children[navChildName] as DataObject).Clear();
                        throw new DataModelException(String.Format(Globalization.GetString("DataModel.SetFieldAndNavTo.Exception", this.Context.ClientId), this.Table, navChildName, keyValue) + e.Message, e);
					}
                else if (!bFieldDataChanged)
                    ;
                else
                {
                    //srajindersin 01/15/2013 - MITS 30601
                    if ((!(this.m_Children[navChildName] as DataObject).AnyDataChanged) && !bFieldDataChanged)
                        (this.m_Children[navChildName] as DataObject).Clear();
                }
			}
			protected void SetField(string fieldName, object targetValue)
			{
				bool bChangedValue = false;
				//Do the Trim Silently for now, could throw an error message...
				if(targetValue is string)
				{
					int MaxLength;
					this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
					try
					{
						MaxLength = (int)this.MetaInfo.LazyLoadedSchemaTable(this).Select(String.Format("ColumnName='{0}'",fieldName))[0]["ColumnSize"];
					}
					finally{this.MetaInfo.PropertyLock.ReleaseReaderLock();}
					if(MaxLength < (targetValue as string).Length)
						targetValue = (targetValue as string).Substring(0,MaxLength);
					bChangedValue = (targetValue.ToString() != this.m_Fields[fieldName].ToString());
				
				}
				//For Numerics, allow 0 to test equivalent to ""...
                //Abhishek MITS 11072, added (targetValue is long)
                else if (targetValue is int || targetValue is double || targetValue is float || targetValue is Int16 || targetValue is long)
                {
                        bChangedValue = (targetValue.ToString() != this.m_Fields[fieldName].ToString());
                }
                else if (targetValue is bool)
                {
                    bChangedValue = (targetValue.ToString() != Conversion.ConvertObjToBool(this.m_Fields[fieldName], this.Context.ClientId).ToString());
                }

				if(bChangedValue||IsNew)
				{	
					this.m_Fields[fieldName] = targetValue;
					this.DataChanged = true;
				}
					

			}
            //Deb MITS 25775 
            //mbahl3 mits:29316
            protected void SetFieldForTaxId(string fieldName, object targetValue, string sTaxTypeField)
            {
                bool bChangedValue = false;
                //Do the Trim Silently for now, could throw an error message...
                if (targetValue is string)
                {
                    int MaxLength;
                    this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
                    try
                    {
                        MaxLength = (int)this.MetaInfo.LazyLoadedSchemaTable(this).Select(String.Format("ColumnName='{0}'", fieldName))[0]["ColumnSize"];
                    }
                    finally { this.MetaInfo.PropertyLock.ReleaseReaderLock(); }
                    if (MaxLength < (targetValue as string).Length)
                        targetValue = (targetValue as string).Substring(0, MaxLength);
                    bChangedValue = (targetValue.ToString() != this.m_Fields[fieldName].ToString());
                }
               
                if (bChangedValue)
                {
                    if ((this.m_Fields[sTaxTypeField] != null) && (this.m_Fields[sTaxTypeField].ToString() == this.Context.LocalCache.GetCodeId("S", "TAX_ID_TYPES").ToString()))
                    {
                        if ("###-##-####" == targetValue.ToString())
                            ;
                        else
                            this.m_Fields[fieldName] = targetValue;
                    }
                    else
                    {
                           if ("##-#######" == targetValue.ToString())
                            ;
                        else
                            this.m_Fields[fieldName] = targetValue;
                    }
                    this.DataChanged = true;
                }
                //mbahl3 mits:29316

            }//Deb MITS 25775 
			//Gets the Current Child Value using the Property Accessor.
			public object GetProperty(string propertyName, params object[] propertyParams)
			{
				try
				{
					PropertyInfo prop;
					m_PropertyInfoMapLock.AcquireReaderLock(-1);
					try
					{
						// JP 10.10.2005   BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.Public;
						// JP 10.10.2005   return this.GetType().InvokeMember(propertyName,bindingFlags,null,this,propertyParams);
						// JP 10.10.2005   BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.Public;
						string sClass = this.GetType().Name;
						//GetPropertyDefaults(sClass);
						prop = (PropertyInfo) m_PropertyInfoMap[sClass + "." + propertyName];
					}
					finally
					{
						m_PropertyInfoMapLock.ReleaseReaderLock();
					}
					
					return prop.GetValue(this, propertyParams);
				}catch(Exception e)
                { throw new DataModelException(String.Format(Globalization.GetString("Datamodel.GetProperty.Exception", this.Context.ClientId), propertyName, this.GetType().Name, this.KeyFieldValue), e); }
	
			}	

			//		//Gets the Current Child Value using the Property Accessor.
			//		public object GetProperty(string propertyName)
			//		{
			//			try
			//			{
			//				object[] propertyParams = {};
			//				BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.Public;
			//				return this.GetType().InvokeMember(propertyName,bindingFlags,null,this,propertyParams);
			//			}
			//			catch(Exception e)
			//			{throw new Riskmaster.ExceptionTypes.RMAppException(Globalization.GetString("DataModelObject.GetProperty.Exception"),e);}
			//	
			//		}	
			//Sets the Current Child Value using the Property Accessor.
			public bool SetProperty(string propertyName, object propertyValue)
			{
				PropertyInfo propInfo = null;
				object propertyParams;
				try
				{
					//BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.SetProperty| BindingFlags.Public | BindingFlags.OptionalParamBinding;
					propInfo = this.GetType().GetProperty(propertyName);
					propertyParams = propertyValue;
					if(!propInfo.CanWrite)
						return false;

					if(propertyValue.GetType().Name == "String")  //We recieved a string (try to pick a more specific type base on Property Type
					{
						string sPropertyValue = propertyValue as string;
						if(propInfo.PropertyType.Name == "Boolean")
							propertyParams = Conversion.ConvertStrToBool(sPropertyValue);
						else if(propInfo.PropertyType.Name == "Int16")
							propertyParams = Convert.ToInt16(sPropertyValue);
						else if(propInfo.PropertyType.Name == "Int32")
							propertyParams = Conversion.ConvertStrToInteger(sPropertyValue);
						else if(propInfo.PropertyType.Name == "Int64")
							propertyParams = Convert.ToInt64(sPropertyValue);
						else if(propInfo.PropertyType.Name == "Double")
							propertyParams = Conversion.ConvertStrToDouble(sPropertyValue);
						else if(propInfo.PropertyType.Name == "Decimal")
							propertyParams = Convert.ToDecimal(sPropertyValue);

					}

					propInfo.SetValue(this,propertyParams,null);
					return true;
					//return this.GetType().InvokeMember(propertyName,bindingFlags,null,this,propertyParams);
				}
				catch(Exception e)
                { throw new Riskmaster.ExceptionTypes.RMAppException(Globalization.GetString("DataModelObject.SetProperty.Exception", this.Context.ClientId), e); }
	
			}
			internal object GetField(string fieldName){	return this.m_Fields[fieldName];}
            //MITS 11072 Abhishek
			//Function converts string value to long value
            internal long GetFieldlong(string fieldName)
			{
				return Conversion.ConvertStrToLong(this.m_Fields[fieldName].ToString());
			}
			internal int GetFieldInt(string fieldName)
			{
				/*
					* Changed by Tanuj 29-Aug-2005
					* This function will now return the value properly.
					* If the value is more then the maximum database can accept, insertion will fail with the following exception->
					* "Arithmetic overflow error for data type"
					* */
                return Conversion.ConvertObjToInt(this.m_Fields[fieldName], this.Context.ClientId);
			}
			internal string GetFieldStringFallback(string fieldName, string fallBackFieldName)
			{
				string sRet = GetFieldString(fieldName);
				if(sRet=="" && fallBackFieldName !="")
				{
					sRet = GetFieldString(fallBackFieldName).Replace("\n", "").Replace("\r", "<br/>");
				}
				sRet = sRet.Replace('\a', ' ');
				return sRet;
			}
			internal string GetFieldString(string fieldName)
			{
				object ret = this.m_Fields[fieldName];
				if(!(ret is string) || ret == null)
					ret = "";

				return (string) ret;
			}
            //Deb MITS 25775 
            //mbahl3 mits:29316
            internal string GetFieldStringForTaxId(string fieldName,string sTaxTypeField)
            {
                object ret = this.m_Fields[fieldName];
                if (!(ret is string) || ret == null)
                    ret = "";
                //rupal:start, mits 27480
                //if (this.Context.InternalSettings.SysSettings.MaskSSN && !string.IsNullOrEmpty((string)ret))
                if (this.Context.InternalSettings.SysSettings.MaskSSN && !string.IsNullOrEmpty((string)ret) && (!this.IsNew))
                {
                    if((this.m_Fields[sTaxTypeField] != null) && (this.m_Fields[sTaxTypeField].ToString() == this.Context.LocalCache.GetCodeId("S", "TAX_ID_TYPES").ToString())) 
                    ret = "###-##-####";
                    else
                      ret = "##-#######";
                }
                //mbahl3 mits:29316
                //rupal:end, mits 27480
                return (string)ret;
            }//Deb MITS 25775 
			internal bool GetFieldBool(string fieldName)
			{
				object ret = this.m_Fields[fieldName];

				if(ret ==null)
					return false;
				if (!(ret is bool))
				{
					if(Conversion.IsNumeric(ret))
						return (ret.ToString() != "0");
					else
						return false;
				}
				else
					return (bool)ret;
			}
			internal float GetFieldFloat(string fieldName)
			{
				object ret = this.m_Fields[fieldName];
				if (!(ret is float) || ret == null)
					return 0;
				else
					return (float)ret;
			}
			internal double GetFieldDouble(string fieldName)
			{
				object ret = this.m_Fields[fieldName];
				if (!(ret is double) || ret == null)
					return 0;
				else
					return (double)ret;
			}

			#endregion

			#region IDataModelObject Implementation
			public string Table
			{
				get{return m_sTableName;}
				set
				{
					if(m_sTableName != value)
					{	
						m_sTableName = value;
						m_Supplementals.TableName = OnBuildSuppTableName();
					}
				}

			}
			internal virtual Supplementals Supplementals
			{	
				get
				{
					if(! m_Supplementals.DefinitionLoaded)
					{
						// Fix for parent reload destroying current data during creation of supplementals
						// object.  The parent property was being left marked stale
						// because execution does not pass through "OnChildInit" for 
						// supplementals.
						(m_Supplementals as DataRoot).Parent = this;  // BSB 11.18.2005
						(m_Supplementals as DataRoot).m_isParentStale = false;  // BSB 10.18.2005
						m_Supplementals.TableName = OnBuildSuppTableName();
						m_Supplementals.LockDataChangedFlag();
                        // ABhateja, 08.03.2007
                        // MITS 10049, assign the KeyValue using the function.
                        // This function is overridden in PiPatient.cs
                        OnSetSupplementalKeyValues();
                        m_Supplementals.UnlockDataChangedFlag();
						m_Supplementals.Refresh();
					}

					if((m_Supplementals as IDataModel).IsStale)
					{
                        // ABhateja, 08.03.2007
                        // MITS 10049, assign the KeyValue using the function.
                        // This function is overridden in PiPatient.cs
                        OnSetSupplementalKeyValues();
                        m_Supplementals.Refresh();
					}
					return m_Supplementals;
				}
			}
			public virtual string KeyFieldName{get{return m_sKeyField;}set{m_sKeyField = value;}}
			public virtual int KeyFieldValue{get{return GetFieldInt(m_sKeyField);}set{SetField(m_sKeyField,value);}}
			internal DataFieldList Fields{get{return m_Fields;}}
			internal DataChildList Children{get{return m_Children;}}
		
			protected void Configure()
			{
				if(this.m_Supplementals==null)this.m_Supplementals = Context.Factory.GetDataModelObject("Supplementals",true) as Supplementals;
			{	
				m_Supplementals.Parent = this;
				this.m_Supplementals.InitObj+=  new InitObjEventHandler(InitObjDefaultHandler);
				this.m_Supplementals.CalculateData+= new CalculateDataEventHandler(CalculateDataDefaultHandler);
				this.m_Supplementals.ValidateData+= new ValidateEventHandler(ValidateDefaultHandler);
				this.m_Supplementals.BeforeSave+=new BeforeSaveEventHandler( BeforeSaveDefaultHandler);
				this.m_Supplementals.AfterSave+=new AfterSaveEventHandler(AfterSaveDefaultHandler);
                this.m_Supplementals.BeforeDelete += new BeforeDeleteEventHandler(BeforeDeleteDefaultHandler);
			}
				//Add the default Event Handlers
				if(InitObj==null)InitObj+=  new InitObjEventHandler(InitObjDefaultHandler);
                if (InitData == null) InitData += new InitDataEventHandler(InitDataDefaultHandler);
				if(CalculateData==null)CalculateData+= new CalculateDataEventHandler(CalculateDataDefaultHandler);
				if(ValidateData==null)ValidateData+= new ValidateEventHandler(ValidateDefaultHandler);
				if(BeforeSave==null)BeforeSave+=new BeforeSaveEventHandler( BeforeSaveDefaultHandler);
				if(AfterSave==null)AfterSave+=new AfterSaveEventHandler(AfterSaveDefaultHandler);
                if (BeforeDelete == null) BeforeDelete += new BeforeDeleteEventHandler(BeforeDeleteDefaultHandler);
			
				//				(this as DataObject).Clear();
				//				//Call Initialization Script
				//				this.InitObj(this,new InitObjEventArgs());

			}

			//The "callback" point for derived object implementors to kick off 
			// the initialization script once the derived object is fully prepared.
			protected void Initialize()
			{
				(this as DataObject).Clear();
				//Call Initialization Script
				this.InitObj(this,new InitObjEventArgs());
			}
			#endregion
		
			#region Property & Child Naming, Enumeration and Membership Tests
			public ICollection Properties{get{return m_PropertyMap.Keys;}}
			public bool HasChildProperty(string propertyName, bool ignoreCase)
			{
				if(this.m_Children.ContainsChild(propertyName,ignoreCase))
					return true;
				else //Check for fake children added only to the ProperyMap but not to the childList. BSB 12.13.2004
					return (!this.HasFieldProperty(this.PropertyNameToField(propertyName),ignoreCase) && this.HasProperty(propertyName,ignoreCase));
			}

			public bool HasProperty(string propertyName){return m_PropertyMap.ContainsKey(propertyName);}
			public bool HasProperty(string propertyName, bool ignoreCase)
			{
				bool ret = false;

				if(!ignoreCase)
					return this.HasProperty(propertyName);

				//Check Object Field Properties
				string sPropertyName = propertyName.ToUpper();
                ret = m_PropertyMap.ContainsKey(propertyName.ToUpper()); //MITS 32157
                //MITS 32157
                //foreach(string s in m_PropertyMap.Keys)
                //    if(s.ToUpper() == sPropertyName)
                //        return true;
			
				return ret;
			}
			public bool HasFieldProperty(string fieldName)
			{
				return this.m_Fields.ContainsField(fieldName);
			}
			public bool HasFieldProperty(string fieldName, bool ignoreCase)
			{
				bool ret = false;
			
				if(!ignoreCase)
					return this.HasFieldProperty(fieldName);

				//Check Object Field Properties
//				string sFieldName = fieldName.ToUpper();  // JP 10.10.2005
				foreach(string s in m_Fields.Keys)
					if(0==String.Compare(s, fieldName,ignoreCase))
						return true;
			
				return ret;
			}
			public string PropertyNameToField(string propertyName)
			{
				string ret = "";
				ret = this.m_PropertyMap[propertyName] as string;
				if(ret == null)
					ret = "";
				return ret;
			}
			internal string FieldNameToProperty(string fieldName)
			{
				//If of the form TABLE.FIELD_NAME
				// we only want the .FIELD_NAME part.
				string s = "";
				if(fieldName.IndexOf('.') >0)
					s = fieldName.Split('.')[1];
				else
					s = fieldName;

				//s =s.ToUpper();

				foreach(DictionaryEntry mapping in m_PropertyMap)
				{
					if((string)mapping.Value==s)
						return (string) mapping.Key;
				}
                throw new DataModelException(String.Format(Globalization.GetString("DataObject.FieldNameToProperty.Exception.UnrecognizedField", this.Context.ClientId), s));
			}

			internal override void LockDataChangedFlag()
			{
				base.LockDataChangedFlag ();
				if(m_Supplementals !=null && m_Supplementals.DefinitionLoaded)
					m_Supplementals.LockDataChangedFlag();
			}
			internal override void UnlockDataChangedFlag()
			{
				base.UnlockDataChangedFlag ();
				if(m_Supplementals !=null && m_Supplementals.DefinitionLoaded)
					m_Supplementals.UnlockDataChangedFlag();
			}



			//Try to pull out a summary property name and use it to get a summary value.
			// Otherwise, just return "n/a".
			virtual public string Default
			{
				get
				{
					string sField = "n/a";
					SummaryAttribute objSummary;
				
					object[] obj = this.GetType().GetCustomAttributes(typeof(Riskmaster.DataModel.SummaryAttribute),false);
				
					if(obj != null && obj.Length != 0)
					{
						objSummary = obj[0] as SummaryAttribute;
						sField = objSummary.DefaultProperty;
					}

					if(!this.HasProperty(sField,true))
						return sField;
					else
						return this.GetProperty(sField).ToString();
				}
			}
			#endregion

			#region XML Serialization Logic
			#region Work In Progress
			public bool PopulateObject(XmlDocument propertyStore)
			{
				return PopulateObject(propertyStore,this.GetType().Name,false);
			}
			public bool PopulateObject(XmlDocument propertyStore,bool bKeepCurrentData)
			{
				return PopulateObject(propertyStore,this.GetType().Name,bKeepCurrentData);
			}
			internal bool PopulateObject(XmlDocument propertyStore, string rootTagName)
			{
				return PopulateObject(propertyStore,rootTagName,false);
				
			}
			//BSB Added bKeepCurrent to allow caller to prevent record data from being refreshed from the 
			// Database.  Kind of a fringe case currently only for Claim(WC) which meddles around in the 
			// in-memory Event object, then later calls PopulateObject against the parent Event expecting
			// to keep the earlier changes.
			internal virtual bool PopulateObject(XmlDocument propertyStore, string rootTagName,bool bKeepCurrentData)
			{
//				PerfTimer objSchemaTimer = new PerfTimer(String.Format("PopulateObject.SchemaValidation({0})",rootTagName));
//				PerfTimer objPopulateWithChildren = new PerfTimer(String.Format("PopulateObject.WithChildren({0})",rootTagName));
				bool bAddingNew = false;
                string sId = string.Empty, sProp = string.Empty;
				object o =null;
				XmlElement elt = null;
				XmlReader xmlValidator=null;

//				objPopulateWithChildren.StartTimer();
				
				//BSB Outer Try\Catch used to ensure Object State is kept accurately.
				try
				{
					this.m_DataModelState = DataObjectState.IsPopulating;
					//1.) Determine if propertyStore is valid for the current object type.
					//a.)Root Tag matches class name?
					if(	propertyStore.DocumentElement.Name != rootTagName && 
						propertyStore.SelectSingleNode(String.Format("/Instance/{0}",rootTagName))==null)
					{
						Type objBaseType = this.GetType();
						while(objBaseType.Name != propertyStore.DocumentElement.Name && 
							objBaseType.Name != "Object")
							objBaseType = objBaseType.BaseType;

						if(propertyStore.DocumentElement.Name == objBaseType.Name)
						{
							rootTagName = objBaseType.Name;
						}
						else
                            throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RootElementMismatch", this.Context.ClientId), propertyStore.DocumentElement.Name, rootTagName));
					}
					//b.) Document Validates?
					try
					{
						if(propertyStore.DocumentElement.Name == "Instance") //Then this is the first time we're seeing this document, (Worth doing the validation) 
						{
                            XmlUtilityFunctions.ExtractInstanceInfo(ref propertyStore, rootTagName, ref xmlValidator);
						}
					}
					catch(Exception e)
					{
                        throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.SchemaValidationException", this.Context.ClientId), rootTagName), e);
					}
					finally
					{
						if(xmlValidator !=null)
							xmlValidator.Close();
						if(SerializationContext.RWLock.IsReaderLockHeld)
							SerializationContext.RWLock.ReleaseReaderLock();
					}
					//2.) If propertyStore represents an existing object then Navigate this object to the existing record.
					try{elt = propertyStore.DocumentElement.SelectSingleNode(this.FieldNameToProperty(this.KeyFieldName)) as XmlElement;}
					catch(Exception e)
					{
                        throw new Riskmaster.ExceptionTypes.XmlOperationException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.KeyFieldNameMissing", this.Context.ClientId), rootTagName + "." + this.KeyFieldName), e);
					}
			
					//BSB Bug Fix: Some tables use an EID as a key (PHYSICIAN).
					// In this case, the numeric value of the field may be in the codeid attribute rather than the 
					// element text.
					if(elt.HasAttribute("codeid"))
						sId = elt.Attributes["codeid"].Value;
					else
						sId = elt.InnerText;

					//This test works for one to many children but fails for 
					// 1:{1|0} relations since they share the same key.
					bAddingNew =(Conversion.ConvertStrToInteger(sId) <=0);
				
					if(!bAddingNew && !bKeepCurrentData)//Optional 1:1 record added on the fly...
						try{this.MoveTo(Int32.Parse(sId));}//Test if record is in DB - if not, treat as new.
						catch(RecordNotFoundException){bAddingNew=true;(this as IDataModel).IsStale=false;this.DataChanged=false;}

					//3.) (if not new) Compare top level TimeStamp?
					if(!bAddingNew)
					{
						elt = propertyStore.SelectSingleNode("/*/DttmRcdLastUpd") as XmlElement;

						if(elt !=null)
							if(this.GetProperty("DttmRcdLastUpd").ToString() !=elt.InnerText)
                                throw new Riskmaster.ExceptionTypes.DataModelException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RecordAlreadyChanged", this.Context.ClientId), this.GetType().Name + "." + this.KeyFieldName, this.KeyFieldValue));
					}

					//4.) For all children object data in the propertyStore.
					//a.) IF child is SimpleList - then treat this as a scalar and handle it in the PopulateScalar function.
					//b.) IF child is Singleton - then recursively call PopulateObject with the child object data Xml sub-tree.
					//c.) IF child is DataCollection - then call PopulateObject on the collection?
					//d.) IF child is Supplementals - then call PopulateObject on the Supplementals class.
					//e.) IF child is Scalar - then apply property values to this object using PopulateScalar function.
					foreach(XmlElement item in propertyStore.SelectNodes(String.Format("/{0}/*",rootTagName)))
					{
						sProp = item.LocalName;
					
						//if(sProp =="Parent" || sProp =="Supplementals")
                        if (sProp .Equals( "Parent") || sProp.Equals( "Supplementals")) //  MiTS 32157
							o = null;
						else
							o = this.GetProperty(sProp);
					
						//if((this.HasChildProperty(sProp,true) || sProp =="Parent" || sProp == "Supplementals") && !(o is DataSimpleList)) //It's a sub-object
                        if ((this.HasChildProperty(sProp, true) || sProp.Equals("Parent") || sProp.Equals("Supplementals")) && !(o is DataSimpleList))//  MiTS 32157
						{

							/*** Special Case for Parent reference as Child Object***/
							//if(sProp=="Parent")
                            if (sProp.Equals("Parent")) //  MiTS 32157
							{
								//Don't do anything to parent from here.
								continue;
							}

							/*** Special Case for Supplementals  reference as Child Object***/
							//if(sProp=="Supplementals")
                            if (sProp.Equals("Supplementals")) //  MiTS 32157
							{
								if(this.GetType().GetProperty("Supplementals") !=null)
									if(this.Supplementals !=null)
										this.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item),sProp,bKeepCurrentData);
							}

							/*** Singleton Child Object***/
							DataObject objChild = o as DataObject;
							if(!item.HasChildNodes) //XML Does not contain information about this child
								continue;
							else if(objChild != null) //XML Contains UPDATE or CREATE  information for this child.
								objChild.PopulateObject(Utilities.XmlElement2XmlDoc(item),sProp,bKeepCurrentData); //Note that child creation will be handled automatically via the propery accesor.

							/*** Supplementals based Child Object (Juris\Accord) ***/
							SupplementalObject objSuppBase = o as SupplementalObject;
							if(!item.HasChildNodes) //XML Does not contain information about this child
								continue;
							else if(objSuppBase != null && objSuppBase.TableName==item.GetAttribute("tablename")) //XML Contains UPDATE or CREATE  information for this child.
								objSuppBase.PopulateObject(Utilities.XmlElement2XmlDoc(item),sProp,bKeepCurrentData); //Note that child creation will be handled automatically via the propery accesor.

	
							/*** Data Object Collection ***/
							DataCollection objList = o as DataCollection;
							if(objList != null)
							{
								if(!item.HasChildNodes) //XML Does not contain information about this child
									continue;
								else if(objList != null) //XML Contains UPDATE or CREATE  information for this child.
									objList.PopulateObject(Utilities.XmlElement2XmlDoc(item),sProp,bKeepCurrentData); //Note that list creation will be handled automatically via the propery accesor.
							}
						}
						else 
						{
							/*** Scalar Field Value ***/
							PopulateScalar(this,sProp, item);
						}
					} //End "For Each Property"				
//					objPopulateWithChildren.StopTimer();
					Trace.WriteLine(String.Format("Populated {0} DataChanged: {1}",rootTagName,this.DataChanged));
					return true;
				}
				finally
				{
					this.m_DataModelState = DataObjectState.IsReady;
					
				}
			}

			#endregion
            private Hashtable Trim = null;
			public string SerializeObject(Hashtable TrimProperty)
			{
				SerializationContext objContext = new SerializationContext();
				objContext.RequestDom= new XmlDocument();
                this.Trim = TrimProperty;
				objContext.RequestDom.LoadXml(String.Format("<{0} />",this.GetType().Name));
				return SerializeObject(objContext);
			}
            public string SerializeObject()
            {
                SerializationContext objContext = new SerializationContext();
                objContext.RequestDom = new XmlDocument();
                this.Trim = new Hashtable();
                objContext.RequestDom.LoadXml(String.Format("<{0} />", this.GetType().Name));
                return SerializeObject(objContext);
            }
            public string SerializeObject(XmlDocument dom, Hashtable TrimProperty)
			{
				SerializationContext objContext = new SerializationContext();
				objContext.RequestDom= dom;
                this.Trim = TrimProperty;
				objContext.ElementTag = this.GetType().Name;
				objContext.RequestXPath = String.Format("/{0}",objContext.ElementTag);
				return SerializeObject(objContext);
			}
            public string SerializeObject(XmlDocument dom)
            {
                SerializationContext objContext = new SerializationContext();
                objContext.RequestDom = dom;
                this.Trim = new Hashtable(); 
                objContext.ElementTag = this.GetType().Name;
                objContext.RequestXPath = String.Format("/{0}", objContext.ElementTag);
                return SerializeObject(objContext);
            }
            //BSB 11.14.2007 Performance Change
            // 20-30 ms Per Request spent on string concatenation here.
            // Switch to String Builder
			internal string SerializeObject(SerializationContext objContext)
			{
                System.Text.StringBuilder sb = new System.Text.StringBuilder(4096);
				bool bEntryCall = false;
				string sReturn="";
				DataRoot objRoot = this;
				DataObject objData = objRoot as DataObject;
				ExtendedTypeAttribute ext = null;
				
				//BSB Outer Try\Catch used to ensure Object State is kept accurately.
				try
				{
					this.m_DataModelState = DataObjectState.IsSerializing;

					//if(objContext.ElementTag ==null || objContext.ElementTag=="" )
                    if (objContext.ElementTag == null || string.IsNullOrEmpty(objContext.ElementTag)) // MITS 32157
					{
						objContext.ElementTag =this.GetType().Name;
						//if(objContext.RequestXPath == null || objContext.RequestXPath =="") //Initialize XPath iff this is the entry point
                        if (objContext.RequestXPath == null || string.IsNullOrEmpty(objContext.RequestXPath )) // MITS 32157
						{
							objContext.RequestXPath = objContext.ElementTag;
							bEntryCall = true;
						}
					}

					if(objRoot is DataObject)
					{

						object o = null;
						string sProp="";
//						foreach(string sProp in objData.Properties)
						this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
                        try
                        {
                            foreach (DataObject.PropertyMetaInfo objPropMeta in objData.MetaInfo.SerializationSequence)
                            {
                                sProp = objPropMeta.PropertyName;// See note on TrimSerialization Implementation.

                                if (objData.OnTrimSerialization(sProp))
                                    continue;
                                if (Trim != null)
                                {
                                    if (Trim.ContainsKey(sProp))
                                        continue;
                                }
                                ext = GetExtendedTypeAttribute(objData.GetType(), sProp);
                                try { o = objData.GetProperty(sProp); }
                                catch (Exception e)
                                {
                                    throw new DataModelException(String.Format(Globalization.GetString("Serialization.PropertyDataException", this.Context.ClientId), sProp, objData.GetType().Name, objData.KeyFieldValue), e);
                                }

                              //  if ((objData.HasChildProperty(sProp, true) || sProp == "Parent") && !(o is Riskmaster.DataModel.DataSimpleList)) //It's a sub-object
                                if ((objData.HasChildProperty(sProp, true) || sProp.Equals("Parent")) && !(o is Riskmaster.DataModel.DataSimpleList)) // MITS 32157
                                {

                                    /*** Special Case for Parent reference as Child Object***/
                                    // if (sProp == "Parent") MITS 32157
                                    if (sProp.Equals( "Parent"))
                                    {
                                        if (o != null)
                                            sb.Append(SerializeScalar(null, sProp, (o as DataObject).Default));
                                        else
                                            sb.Append(SerializeScalar(null, sProp, ""));

                                        /*** Ensure that Supps always occur in a consistent spot. ***/
                                        /*** Special Case for Supplementals reference as Child Object***/
                                        if (objData.GetType().GetProperty("Supplementals") != null && objContext.IsRequested("Supplementals"))
                                        {

                                            if (objData.Supplementals != null)
                                                sb.Append(objData.Supplementals.SerializeObject(objContext.CreateChild("Supplementals")));
                                            else
                                                sb.Append(SerializeScalar(null, "Supplementals", ""));
                                        }
                                        continue;
                                    }

                                    /*** Singleton Child Object***/
                                    DataObject objChild = o as DataObject;
                                    if (objChild != null)
                                        sb.Append(objContext.IsRequested(sProp) ? objChild.SerializeObject(objContext.CreateChild(sProp)) : "");

                                    /*** Data Object Collection ***/
                                    DataCollection objList = o as DataCollection;
                                    if (objList != null)
                                    {
                                        //s += String.Format("<{0} count=\"{1}\">",sProp,objList.Count);
                                        sb.Append(String.Format("<{0} count=\"{1}\" committedcount=\"{2}\">", sProp, objList.Count, objList.CommittedCount));

                                        if (objContext.IsRequested(sProp))
                                            foreach (DataObject objItem in objList)
                                                sb.Append(objItem.SerializeObject(objContext.CreateChild("")));

                                        sb.Append(String.Format("</{0}>", sProp));
                                    }

                                    /*** Additional "Supplementals Based" Child Object (Juris\Accord...)***/
                                    SupplementalObject objSuppBase = o as SupplementalObject;
                                    if (objSuppBase != null)
                                        sb.Append(objContext.IsRequested(sProp) ? objSuppBase.SerializeObject(objContext.CreateChild(sProp)) : "");

                                }
                                else
                                {
                                    /*** Scalar Field Value ***/
                                    sb.Append(SerializeScalar(ext, sProp, o));
                                }

                            } //End "For Each Property"
                        }
                        catch (Exception e)
                        {
                            string s = e.ToString();
                        }
						finally
                        {
                            if (this.MetaInfo.PropertyLock.IsReaderLockHeld)
                            {
                                this.MetaInfo.PropertyLock.ReleaseReaderLock();
                            }
                        }


						sReturn = String.Format("<{0}>{1}</{0}>", objContext.ElementTag,sb.ToString()); 
					}	//End "If DataRoot is DataObject Type"
				}
				finally
				{
					this.m_DataModelState = DataObjectState.IsReady;
				}
				if(bEntryCall) 	//"Pre Processing" occurs only if this is the entry point for a Serialization Request
				{
                    return  String.Format(@"<Instance>{0}</Instance>", sReturn);
				}
				else
                    return sReturn;
			}
			//BSB 08.10.2005 To be overloaded by a data class when for whatever reason
			// the property needs to be excluded from XML representations of the 
			// in memory object.  
			// The first example of this is the Pi Derived class implementations 
			// that need to eliminate PiEntity which is always a duplicate of the
			// more specific entity linked from the derived type.  For example:
			// The physician entity "overrides" the PiEntity but the derived class
			// has both of them hanging off of it.  The derived code can ensure that 
			// the same entity is always used but it cannot "remove" the duplicated property.
			// this is confusing users and causing data integrity\overwrite problems when 
			// repopulating the objects. 
			public virtual bool OnTrimSerialization(string sProp){return false;}
			
			internal virtual void CallOnSetSupplementalKeyValues()
			{
				try
				{
					(m_Supplementals as DataRoot).LockDataChangedFlag();
					OnSetSupplementalKeyValues();//implmented by Derived classes.
				}
				finally
				{
					(m_Supplementals as DataRoot).UnlockDataChangedFlag();
				}
			}
            internal virtual void OnSetSupplementalKeyValues() { m_Supplementals.KeyValue = (int)this.KeyFieldValue; }//(m_Supplementals.KeyField as DataRoot).DataChanged = false;}

			private ExtendedTypeAttribute GetExtendedTypeAttribute(Type t, string s)
			{
				ExtendedTypeAttribute ext = null;
				PropertyInfo prop = null;

				m_PropertyInfoMapLock.AcquireReaderLock(-1);
				try 
				{
					string sClass = t.Name;
					prop = (PropertyInfo) m_PropertyInfoMap[sClass.ToUpper() + "." + s.ToUpper()];
				}
				finally
				{
					m_PropertyInfoMapLock.ReleaseReaderLock();
				}
				// try{ext =inf.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute),false)[0] as Riskmaster.DataModel.ExtendedTypeAttribute;}
				try
                {
                    //srajindersin MITS 32606 dt:05/14/2013
                    if (prop.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute), false).Length > 0)
                    {
                        ext = prop.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute), false)[0] as Riskmaster.DataModel.ExtendedTypeAttribute;
                    }
                }
				catch(IndexOutOfRangeException){}
				return ext;
			}
	
			#endregion

			//Add all object Fields into the Field Collection from our Field List.
			protected virtual void InitFields(string[,] sFields)
			{ 
                if (this.MetaInfo.PropertyLock.IsReaderLockHeld)
                {
                    this.MetaInfo.PropertyLock.UpgradeToWriterLock(-1);
                }
                else
                {
                    this.MetaInfo.PropertyLock.AcquireWriterLock(-1);
                }
				try 
				{
					bool bMetaInfoPrepared = this.MetaInfo.IsFieldsPrepared;
					PropertyMetaInfo objPropInfo;

					for(int i =0; i< sFields.GetLength(0);i++)
					{
						this.m_Fields[sFields[i,1]]="";
						this.m_PropertyMap[sFields[i,0]] = sFields[i,1];
						if(!bMetaInfoPrepared)
						{
							//Create a PropertyMetaInfo Object.
							objPropInfo = new PropertyMetaInfo(sFields[i,0],true);
							if(!this.MetaInfo.PropertyMetaInfoMap.ContainsKey(sFields[i,0]))
							{
								//Add it to the Sequence.
								this.MetaInfo.SerializationSequence.Add(objPropInfo);
								//Add it to the Map.
								this.MetaInfo.PropertyMetaInfoMap.Add(sFields[i,0],objPropInfo);
							}
						}
					}
					InitParent();
					this.MetaInfo.IsFieldsPrepared=true;
				}
                finally
                {
                    if (this.MetaInfo.PropertyLock.IsWriterLockHeld)
                    {
                        this.MetaInfo.PropertyLock.ReleaseWriterLock();
                    }
                }
			}
			// If appropriate - add Parent to mappings.
			// Note: This is also used as an anchor point for Serializing Supps if present.
			// Therefore there is an assumption that "Parent" will always be the last 
			// child or field serialized (followed by Supps.)
			protected virtual void InitParent()
			{
				this.MetaInfo.PropertyLock.AcquireWriterLock(-1);
				try 
				{

					bool bMetaInfoPrepared = this.MetaInfo.IsFullyPrepared;
					PropertyMetaInfo objPropInfo;
				
					this.m_PropertyMap["Parent"]="PARENT";
					if(!bMetaInfoPrepared && !MetaInfo.PropertyMetaInfoMap.Contains("Parent"))
					{
						objPropInfo = new PropertyMetaInfo("Parent",true);
						this.MetaInfo.SerializationSequence.Add(objPropInfo);
						this.MetaInfo.PropertyMetaInfoMap.Add("Parent",objPropInfo);
					}
				}
				finally{this.MetaInfo.PropertyLock.ReleaseWriterLock();}
			}
			// If appropriate - add a Logical Child to mappings.
			// Note: This "child" is not a db representation - it's only an "in memory" short-cut.
			protected virtual void InitLogicalChild(string sProp)
			{
				this.MetaInfo.PropertyLock.AcquireWriterLock(-1);
				try 
				{

					bool bMetaInfoPrepared = this.MetaInfo.IsFullyPrepared;
					PropertyMetaInfo objPropInfo;
				
					this.m_PropertyMap[sProp]=sProp.ToUpper();
					if(!bMetaInfoPrepared && !MetaInfo.PropertyMetaInfoMap.Contains(sProp))
					{
						objPropInfo = new PropertyMetaInfo(sProp,false);
						this.MetaInfo.SerializationSequence.Add(objPropInfo);
						this.MetaInfo.PropertyMetaInfoMap.Add(sProp,objPropInfo);
					}
				}
				finally{this.MetaInfo.PropertyLock.ReleaseWriterLock();}
			}
			//Add all child objects into the Field Collection from our Field List.
			protected virtual void InitChildren(string[,] sChildren)
			{ 
				this.MetaInfo.PropertyLock.AcquireWriterLock(-1);
				try 
				{

					bool bMetaInfoPrepared = this.MetaInfo.IsChildrenPrepared;
					PropertyMetaInfo objPropInfo;

					//Add all object Children into the Children collection from our "sChildren" list.
					for (int i = 0; i< sChildren.GetLength(0);i++)
					{
						CallOnChildInit(sChildren[i,0],sChildren[i,1]);
						this.m_PropertyMap[sChildren[i,0]] = sChildren[i,0].ToUpper();
						if(!bMetaInfoPrepared)
						{
							//Create a PropertyMetaInfo Object.
							objPropInfo = new PropertyMetaInfo(sChildren[i,0],false);
							if(!this.MetaInfo.PropertyMetaInfoMap.ContainsKey(sChildren[i,0]))
							{
								//Add it to the Sequence.
								this.MetaInfo.SerializationSequence.Add(objPropInfo);
								//Add it to the Map.
								this.MetaInfo.PropertyMetaInfoMap.Add(sChildren[i,0],objPropInfo);
							}
						}
					}
					InitParent();
					this.MetaInfo.IsChildrenPrepared=true;
				}
				finally{this.MetaInfo.PropertyLock.ReleaseWriterLock();}
			}

			protected virtual void Dispose(bool disposing) 
			{
				if (disposing) 
				{
					// Free other state (managed objects).
				}
				// Free your own state (unmanaged objects).
			}

			#region Draft Implementation complete
			override public bool AnyDataChanged
			{
				get
				{
					if(this.DataChanged)
						return true;

					if(m_Supplementals.DataChanged)
						return true;

					//Check all object Children from the Children collection.
					object[] arr = this.m_Children.GetKeyArray();
					for(int i=0; i< arr.Length;i++)
						if ((this.m_Children[(string)arr[i]] as IDataModel).AnyDataChanged)
							return true;
					return false;
				}
			}		
		
			virtual public bool IsNew
			{
				get{return (m_isNew || this.KeyFieldValue <=0);}
			}
			protected void MoveAndLoad(int moveDirection)
			{
				DbReader rdr= null;
				string sSQL="";
				int lNewID=0;

				try
				{
					// TODO: SetAllChanged False
					switch (moveDirection)
					{
						case MOVE_FIRST:
							sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0",this.m_sKeyField,this.m_sTableName, this.m_sKeyField);
							break;
						case MOVE_LAST:
							sSQL = String.Format("SELECT MAX({0}) FROM {1}",this.m_sKeyField,this.m_sTableName);
							break;
						case MOVE_NEXT:
							sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,GetFieldInt(m_sKeyField));
							break;
						case MOVE_PREVIOUS:
							if((int)GetFieldInt(m_sKeyField)==0)
								sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,GetFieldInt(m_sKeyField));
							else
								sSQL = String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,GetFieldInt(m_sKeyField));
							break;
					}

					//Apply Filter Clause
					sSQL = OnApplyFilterClauseSQL(sSQL);

					///Fetch the RowId
                    try { rdr = base.Context.DbConnLookup.ExecuteReader(sSQL); }
                    catch (Exception e) {throw new DataModelException(String.Format("DataModel could not fetch the next row id to load using the following SQL\n\n {0}\n\nCheck the Filter clause.",sSQL),e);}
					if (rdr.Read())
						lNewID = rdr.GetInt(0);
					rdr.Close();
					rdr = null;
				
					//Only if New Unique ID is not 0 and New Unique ID different than old one
					if (lNewID != 0 && (lNewID != GetFieldInt(m_sKeyField) || (this as IDataModel).IsStale))
					{
						this.Clear();
						m_Fields[m_sKeyField] = lNewID;
						LoadData();
					}
                    else 
                    {
                        //MITS 12028 working on a new record and call Init custom script
                        if (this.FiringScriptFlag != 0)
                        {
                            this.InitializeScriptData();
                        }
                    }
				}
				catch(Exception e)
				{
                    throw new RMAppException(Globalization.GetString("DataModelObject.MoveAndLoad.Exception", this.Context.ClientId), e);
				}
				finally
				{
					if(rdr !=null)
						rdr.Close();
				}
			}
			// Allow Derived classes to hook in and add further specialized "default" values here.
			// Primary Example is: PiEmployee (or other specialized Pi types) where the PiTypeCode
			// should be defaulted to something meaningfull.  Occurs AFTER the default 
			// clear implementation which at this time will always be run.
			internal virtual object OnGetPropertyDefaultValue(string propName){return null;}

			//The "interception" point for object implementors to handle
			//modifying SQL statement for delete.
			internal virtual string OnBuildDeleteSQL()
			{
				return String.Format("DELETE FROM {0} WHERE {1}={2}" ,this.m_sTableName,this.m_sKeyField, GetFieldInt(m_sKeyField).ToString());
			}
			//The "interception" point for object implementors to handle
			// inclusion\exclusion of supplementals for delete.
			internal virtual bool OnSuppDelete()
			{
				return true;
			}
			//The "interception" point for object implementors to handle
			//modifying the default supp table name.
			internal virtual string OnBuildSuppTableName(){return this.Table + "_SUPP";	}
		
			//The "interception" point for object implementors to handle
			//modifying SQL statment for navigation.
			internal virtual string OnApplyFilterClauseSQL(string CurrentSQL)
			{
				string sSQL = CurrentSQL;
				if (m_sFilterClause != "") 
					if(sSQL.IndexOf(" WHERE ") > 0)
						sSQL += " AND (" + m_sFilterClause + ")";
					else
						sSQL +=" WHERE " + m_sFilterClause;
				return sSQL;
			}
		
			//These methods are the "interception" point for object implementors to handle
			// custom requirements of each child object during the associated "event" processing.
			internal virtual void OnChildInit(string childName, string childType)
			{
				IDataModel obj = null;
				obj = Context.Factory.GetDataModelObject(childType,true); 
				obj.Parent = this;

				(obj as DataRoot).m_isParentStale = false;  // JP 10.10.2005

				//BSB Hack to set stale flag properly for child records of a "new" object.
				obj.IsStale= false;
				this.m_Children[childName]= obj;

			}
			internal virtual void CallOnChildInit(string childName, string childType)
			{
				OnChildInit(childName,childType);//implmented by Derived classes.
				//BSB 12.14.2005 Fix for DataChanged flipped by child initialization routines that 
				// may trigger the DataChanged flag.  Example: Entity where TableId is set and 
				// entity type is then locked. (Previously caused infinite save loop\stack overflow on 
				// Physician and Medstaff)
				(this.m_Children[childName] as IDataModel).DataChanged=false;
			}
			internal virtual void OnChildLoad(string childName, IDataModel childValue)
			{
				childValue.IsStale = true;
			}
			
			/// <summary>
			/// Used as a spot to ensure that DataChangedFlag gets locked before the child object 
			/// linkages are updated.  Ensures that the simple act of setting linkages does not cause an 
			/// otherwise blank record to wind up looking "changed" and hence add a blank row to 
			/// the database.
			/// </summary>
			/// <param name="childName"></param>
			/// <param name="childValue"></param>
			internal virtual void CallOnChildPreSave(string childName, IDataModel childValue)
			{
				try
				{
					(childValue as DataRoot).LockDataChangedFlag();
					OnChildPreSave(childName,childValue);//implmented by Derived classes.
				}
				finally
				{
					(childValue as DataRoot).UnlockDataChangedFlag();
				}
			}
			/// <summary>
			/// Called during save before the top level class saves it's fields.
			/// </summary>
			/// <param name="childName"></param>
			/// <param name="childValue"></param>
			internal virtual void OnChildPreSave(string childName, IDataModel childValue)
			{
				//implmented by Derived classes.
			}

			/// <summary>
			/// Used as a spot to ensure that DataChangedFlag gets locked before the child object 
			/// linkages are updated.  Ensures that the simple act of setting linkages does not cause an 
			/// otherwise blank record to wind up looking "changed" and hence add a blank row to 
			/// the database.
			/// </summary>
			/// <param name="childName"></param>
			/// <param name="childValue"></param>
			internal virtual void CallOnChildPostSave(string childName, IDataModel childValue, bool isNew)
			{
				try
				{
					(childValue as DataRoot).LockDataChangedFlag();
					OnChildPostSave(childName,childValue,isNew);//implmented by Derived classes.
				}
				finally
				{
					(childValue as DataRoot).UnlockDataChangedFlag();
				}
			}
			/// <summary>
			/// Called during save after the top level class saves it's fields.
			/// </summary>
			/// <param name="childName"></param>
			/// <param name="childValue"></param>
			internal virtual void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
			{
				if(childValue.DataChanged)
					if(childValue is IPersistence)
						(childValue as IPersistence).Save();
				//Re-implemented by Derived classes when special behavior is desired..
	
			}
			/// <summary>
			/// Used as a spot to ensure that DataChangedFlag gets locked before the child object 
			/// linkages are updated.  Ensures that the simple act of setting linkages does not cause an 
			/// otherwise blank record to wind up looking "changed" and hence add a blank row to 
			/// the database.
			/// </summary>
			/// <param name="childName"></param>
			/// <param name="childValue"></param>
			internal virtual void CallOnChildItemAdded(string childName, IDataModel childValue)
			{
				try
				{
					(childValue as DataRoot).LockDataChangedFlag();
					OnChildItemAdded(childName,childValue);//implmented by Derived classes.
				}
				finally
				{
					(childValue as DataRoot).UnlockDataChangedFlag();
				}
			}
			internal virtual void OnChildItemAdded(string childName, IDataModel itemValue)
			{
				//Re-implemented by Derived classes when special behavior is desired..
				itemValue.Parent = this;
				// Fix for parent reload destroying current data during save of 
				// collection member.  The parent property was being left marked stale
				// because execution does not pass through "OnChildInit" for 
				// child list items.
				(itemValue as DataRoot).m_isParentStale = false;  // BSB 10.18.2005

			}
			internal virtual void OnChildDelete(string childName, IDataModel childValue)
			{
				//Re-implemented by Derived classes when special behavior is desired..
				IPersistence pIPer = childValue as IPersistence;
				if(pIPer ==null && !(childValue is DataSimpleList))
					return;
			
				//Be sure to refresh so that child is not stale and cause delete to be skipped.)
				if(pIPer !=null  && childValue.IsStale)
				{
					//BSB Must use property accessor in order to get any parent info applied properly on the child/list.
					this.GetProperty(childName);
					//	pIPer.Refresh();
				}
				if(childValue is DataSimpleList)
				{
					//BSB Must use property accessor in order to get any parent info applied properly on the child/list.
					this.GetProperty(childName);
					(childValue as DataSimpleList).DeleteSimpleList();
					return;
				}
				pIPer.Delete();
			}

			protected virtual void LoadData()
			{
				LoadData(null);
			}

			//May be called by DataCollection Class during load in order to "bunch up" the read queries for 
			// loading a collection if desired.
			internal virtual void LoadData(DbReader objReader)
			{
				Riskmaster.Db.DbReader rdr  = objReader;
                bool bNoRecordFound = false;
				try
				{
					if(rdr ==null)
					{
                        //this.m_sSQL = String.Format("SELECT {0} FROM {1} WHERE {2}={3}", this.m_Fields.ToString(), this.m_sTableName, this.m_sKeyField, GetFieldInt(m_sKeyField).ToString());
                        //rsolanki2 : replacing with equivalent strin.concat
                        this.m_sSQL = String.Concat("SELECT "
                            , this.m_Fields.ToString()
                            , " FROM "
                            , this.m_sTableName
                            , " WHERE "
                            , this.m_sKeyField
                            , "="
                            , GetFieldInt(m_sKeyField).ToString());
                        if (base.Context.DbConnLookup.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            // rdr = DbFactory.ExecuteReader(base.Context.DbConnLookup.ConnectionString, this.m_sSQL);
                            if (base.Context.DbConnLookup.State == ConnectionState.Closed)
                                base.Context.DbConnLookup.Open();
                        }
                        //else
                        //{
                            rdr = base.Context.DbConnLookup.ExecuteReader(this.m_sSQL);
                        //}
					}
				}
				catch(Exception e)
				{
					if(objReader ==null) //If passed in a reader, leave it for the caller to manage.
						if (rdr != null)  
							rdr.Close();
					throw HandleLoadDataException(e);
				}

				//(No Try\Catch) so that we can throw an exception w/o having to catch and re-throw it.
				//Load the Connected Reader row into object properties.
				if(objReader ==null) //If passed in a reader, use the current record
					if(!rdr.Read()) //Data not found loading object {0} from table {1}.  Record with key field {2}={3} could not be found.  Possibly deleted by another user.
					{
						if(objReader ==null) //If passed in a reader, leave it for the caller to manage.
							if (rdr != null)  
								rdr.Close();
                        bNoRecordFound = true;
                        //avipinsrivas Start : Worked for Jira-340
                        if (this.SkipRecordNotFoundError)
                        {
                            rdr = null;
                            this.SkipRecordNotFoundError = false;
                        }
                        else
						    throw new RecordNotFoundException(String.Format(Globalization.GetString("DataObject.LoadData.Exception.RecordNotFound", this.Context.ClientId),this.GetType().Name, this.m_sTableName,this.KeyFieldName,this.KeyFieldValue));
                        //avipinsrivas End
					}
				
				try
				{
					//Ugly hack because it is difficult to iterate over the keys collection making changes...
					object[] arr = this.m_Fields.GetKeyArray();
                    //avipinsrivas Start : Worked for Jira-340
                    if (rdr != null)
                    {
                        for (int i = 0; i < arr.Length; i++)
                            this.m_Fields[(string)arr[i]] = rdr.GetValue((string)arr[i]);
                    }
			        //avipinsrivas End
					//For each Child object, simply notify our derived class implementation for each known object.
					arr = this.m_Children.GetKeyArray();
					for(int i=0; i< arr.Length;i++)
						OnChildLoad((string)arr[i], this.m_Children[(string)arr[i]] as IDataModel );

					//Discard the Reader.
                    //avipinsrivas Start : Worked for Jira-340
                    if (objReader == null && rdr != null) //If passed in a reader, leave it for the caller to manage.
						rdr.Close();

					//Get the Supplementals
					if(! m_Supplementals.DefinitionLoaded)
						m_Supplementals.TableName = OnBuildSuppTableName();
					
					(m_Supplementals as IDataModel).IsStale = true;
					(m_Supplementals as DataRoot).m_isParentStale = false;
                    // ABhateja, 08.03.2007
                    // MITS 10049, assign the KeyValue using the function.
                    // This function is overridden in PiPatient.cs
                    //m_Supplementals.KeyValue = this.KeyFieldValue;
                    OnSetSupplementalKeyValues();
					m_Supplementals.DataChanged = false;



					//Book Keeping.
					if (this.Locked)	// JP 10.10.2005
					{
						m_isParentStale = false;
						
						//BSB 10.04.2006
						// Fringe Case: 
						// Parent could have been created via init script using ".LoadParent()".
						// This is fine for initializing values but we need to trash this scripted
						// new parent if we're navigating among existing records.
						// (Which by definition we are doing here in the "LoadData" routine)
						// This will allow the "lazy loading" of the correct parent as needed 
						// at an appropriate time.
						DataObject objParent = (this.m_Parent as DataObject);
						if(objParent !=null && objParent.IsNew)
							this.m_Parent=null; //Trash it 
												
					}
					else
						m_isParentStale = true;
                    if (!bNoRecordFound)
                    {
					m_isNew = false;
                       // this.AnyDataChanged = true;
                    }
					//BSB HACK Attempt fix for Properties always noted as stale.
					//TODO: Verify this fix.
					(this as IDataModel).IsStale = false;
                    if (this.Table == "CLAIM")
                    {
                        if (this.Context.InternalSettings.SysSettings.MultiCovgPerClm==-1)
                        {
                        if (this.Context.InternalSettings.ColLobSettings[((Claim)this).LineOfBusCode].UseEnhPolFlag != 0)
                            m_iExistingClaimPolicy = ((Claim)this).PrimaryPolicyIdEnh;
                        else
                        m_iExistingClaimPolicy = ((Claim)this).PrimaryPolicyId;
                        }
                    }
				}
				catch(Exception e)
				{
					throw HandleLoadDataException(e);
					//throw new DataModelException(String.Format(Globalization.GetString("DataObject.LoadData.Exception"),this.Table, e.Message),e);
				}
				finally
				{
					if(objReader ==null) //If passed in a reader, leave it for the caller to manage.
                        if (rdr != null)
                        {
                            rdr.Close();
                            rdr.Dispose(); //srajindersin MITS 36539
                        }
				}
			}
			private System.Exception HandleLoadDataException(Exception e)
			{
				System.Diagnostics.Trace.WriteLine("Exception caught in LoadData: " + e.Message);
				
				//Not A Riskmaster Specific Error yet.
				if(e.GetType().FullName.IndexOf("Riskmaster.")<=0)
                    return new DataModelException(String.Format(Globalization.GetString("DataObject.LoadData.Exception", this.Context.ClientId), this.Table, e.Message), e);
				else
					return e;
			}
			public virtual void MoveFirst()
			{
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
				MoveAndLoad(MOVE_FIRST);} 
			public virtual void MoveLast()
			{
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
				MoveAndLoad(MOVE_LAST);
			} 
			public virtual void MoveNext()
			{
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
				MoveAndLoad(MOVE_NEXT);
			} 
			public virtual void MovePrevious()
			{
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
				MoveAndLoad(MOVE_PREVIOUS);
			}

			public virtual void MoveTo( params int[] keyValue)
			{ 
				EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
				if(keyValue.Length == 0)
                    throw new DataModelException(String.Format(Globalization.GetString("DataObject.MoveTo.Exception.MissingKeyValue", this.Context.ClientId), this.FieldNameToProperty(this.KeyFieldName)));
				
				//BSB 03.01.2006 Hack to fix unintentional dependency on zero key value DB records.
				//Make this routine behave the same way as "move and load".
				// See property accessor changes and CreateableMoveTo() for related changes.
				if(keyValue[0]>0)
				{
					Clear();
					m_Fields[m_sKeyField] = keyValue[0];
					LoadData();
				}
				else
				{
					//TODO: Throw an exception?
				}

			} 

			internal virtual DataObject CreatableMoveChildTo( string sChildName, DataObject objChild, int keyValue)
			{ 
				bool bCreate=true;
				if(keyValue >0)
				{
					try
					{
						objChild.MoveTo(keyValue);
						bCreate=false;
					}	
					catch(RecordNotFoundException){}
				}
				
				// 12.27.2005 BSB Allow for creation of new record on the fly.
				if(bCreate)
				{
					CallOnChildInit(sChildName,objChild.GetType().Name);
					objChild = m_Children[sChildName] as DataObject;
				}
				return objChild;
			}
 
			string INavigation.Filter
			{
				get{return this.m_sFilterClause;}
				set
				{
					EnforceLock(Assembly.GetCallingAssembly().FullName == Assembly.GetExecutingAssembly().FullName);
					m_sFilterClause = value;
				}
			}

			static object GetCLRTypeDefault(string sTypeName)
			{
				object ret = null;
				string trimmedTypeName = sTypeName.Replace("System.","");
				switch(trimmedTypeName)
				{
					case "Int16":
					case "Int32":
						return 0;
					case "Single":
					case "Double":
						return 0.0;
					case "Boolean":
						return false;
				}
				return ret;
			}

			// JP 10.06.2005 *START*  Function to get numeric properties from a class. Will cache results statically since this operation is expensive and gets called a lot by the DataObject.Clear function.
			private Hashtable GetPropertyDefaults()
			{
				string sClassName=this.GetType().Name;
				LockCookie cookie = new LockCookie();
				bool bDownGradeOnly =false;
				Hashtable h = new Hashtable();
				try
				{
					// Build and cache defaults 1st time through
					PropertyInfo[] props = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.Public);
					if(m_PropertyInfoMapLock.IsReaderLockHeld)
					{
						cookie = m_PropertyInfoMapLock.UpgradeToWriterLock(-1);
						bDownGradeOnly = true;
					}
					else
						m_PropertyInfoMapLock.AcquireWriterLock(-1);

					for (int i = 0; i < props.Length; i++)
					{
						// Cache numeric default values for properties
							
						//BSB Specialty Case - Allow Derived class to supply 
						// a the default value to be cached. Example: PiTypeCode
						object obj = OnGetPropertyDefaultValue(props[i].Name);
						if(obj!=null)
							h[this.PropertyNameToField(props[i].Name)] = obj;
						else
							switch(props[i].PropertyType.Name)
							{
								case "Int32":
									h[this.PropertyNameToField(props[i].Name)] = 0;
									break;
								case "Single":
								case "Double":
									h[this.PropertyNameToField(props[i].Name)] = 0.0;
									break;
								case "Boolean":
									h[this.PropertyNameToField(props[i].Name)] = false;
									break;
							}							
							
						// Cache property info objects
						m_PropertyInfoMap[sClassName + "." + props[i].Name] = props[i];
					}
				}
				finally
				{
					// Ensure that the lock is released.
					if(bDownGradeOnly)
						m_PropertyInfoMapLock.DowngradeFromWriterLock(ref cookie);
					else
						m_PropertyInfoMapLock.ReleaseWriterLock();
				}

					// Return hashtable
					return h;
			}			
			
			/// <summary>
			/// Simplistic implementation clears all standard Field data from the object.
			/// </summary>
			///<remarks>Not previously standard but common enough to include here.</remarks>
			protected virtual void Clear()
			{
				this.m_Fields.Clear();
				this.m_Children.Clear(this.IsNew);

				
				//Set Numeric and Boolean fields to their defaults 
				//Don't save nulls on new DB record for these fields.
				
				//List out the current property return types.
				if (m_Fields.Keys.Count > 0)  // JP 10.06.2005   Don't bother with any of this if m_Fields is not initialized yet (.Clear is called sometimes before m_Fields has been populated).
				{
					//For each property default the value according to return type.
					object[] arr = this.m_Fields.GetKeyArray();
					this.MetaInfo.PropertyLock.AcquireReaderLock(-1);
					try
					{
						Hashtable h = MetaInfo.LazyLoadedPropertyDefaultsMap(this); //GetPropertyDefaults(this.GetType().Name);  // JP 10.06.2005   Get hash table with numeric defaults for this class.

						for(int i=0; i< arr.Length;i++)
						{
							if (h.ContainsKey((string)arr[i]))
								this.m_Fields[(string)arr[i]] = h[(string)arr[i]];
						}
					}finally{this.MetaInfo.PropertyLock.ReleaseReaderLock();}
				}
			}

			#endregion
			public string Dump()
			{
				m_NestedDumpLevel++;
				string s = "";
				string sIndent ="";
				for(int i=1;i<m_NestedDumpLevel;i++)
					sIndent+="\t";

				foreach(DictionaryEntry mapping in m_PropertyMap)
					if (m_Fields.ContainsField((string)mapping.Value))
						s+= sIndent + (string)mapping.Key + "="  + GetField((string)(string)mapping.Value).ToString() + "\n";
					else if ( (m_Children[(string)mapping.Value] as IDataModel)!=null)
						s+= sIndent + (string)mapping.Key + "=\n"  + (m_Children[(string)mapping.Value] as IDataModel).ToString()+ "\n";
					else if(m_Children[(string)mapping.Value] == null)
						s += sIndent + (string)mapping.Value + " -  was null.";
					else 
						s += sIndent + (string)mapping.Value + " -  " + (m_Children[(string)mapping.Value]).GetType().Name;
				//s+= sIndent + (string)mapping.Key + "=\n"  + (m_Children[(string)mapping.Value] as DataCollection).ToString()+ "\n";
				//			s += this.Supplementals.Dump();
				m_NestedDumpLevel--;
				return s;
			}
			public bool Validate(){return this.ValidateData(this,new ValidateEventArgs());}

            /// <summary>
            /// Put the Comments/HTMLComments in the SysExData if the values are set in the InitCClaim script
            /// or if Legacy_Comments is true
            /// </summary>
            /// <param name="oPropertyStore">Serialized XML for claim object</param>
            /// <param name="RecordId">Record Id associated with the comments</param>
            public virtual void GetCommentsXml(ref XmlDocument oSysExData, int RecordId)
            {
                if (oSysExData == null)
                    return;

                if (m_bUseLegacyComments || RecordId <= 0)
                {
                    XmlNode objComments = oSysExData.SelectSingleNode("/SysExData/Comments");
                    Object oProperty = this.GetProperty("Comments");
                    if (oProperty != null)
                    {
                        string sComments = oProperty.ToString();    //this.Comments
                        if (objComments == null)
                        {
                            objComments = oSysExData.CreateElement("Comments");
                            objComments.AppendChild(oSysExData.CreateCDataSection(sComments));
                            oSysExData.DocumentElement.AppendChild(objComments);
                        }
                        else
                            objComments.InnerText = sComments;
                    }

                    XmlNode objHTMLComments = oSysExData.SelectSingleNode("/SysExData/HTMLComments");
                    oProperty = this.GetProperty("HTMLComments");
                    if( oProperty != null )
                    {
                        string sHTMLComments = oProperty.ToString();    //this.HTMLComments
                        if (objHTMLComments == null)
                        {
                            objHTMLComments = oSysExData.CreateElement("HTMLComments");
                            objHTMLComments.AppendChild(oSysExData.CreateCDataSection(sHTMLComments));
                            oSysExData.DocumentElement.AppendChild(objHTMLComments);
                        }
                        else
                            objHTMLComments.InnerText = sHTMLComments;
                    }
                }
            }


            /// <summary>
            /// Set the Comments/HTMLComments based on the value in the SysExData
            /// or if Legacy_Comments is true
            /// </summary>
            /// <param name="oPropertyStore">Serialized XML for claim object</param>
            /// <param name="RecordId">Record Id associated with the comments</param>
            public virtual void UpdateObjectComments(XmlDocument oSysExData, int RecordId)
            {
                if (oSysExData == null)
                    return;

                if (m_bUseLegacyComments || RecordId <= 0)
                {
                    XmlNode objNode = oSysExData.SelectSingleNode("/SysExData/Comments");
                    Object oProperty = this.GetProperty("Comments");
                    if (objNode != null)
                    {
                        this.SetProperty("Comments", objNode.InnerText);
                    }

                    objNode = oSysExData.SelectSingleNode("/SysExData/HTMLComments");
                    if (objNode != null)
                    {
                        this.SetProperty("HTMLComments", objNode.InnerText);
                    }
                }
            }


            //Nima; MITS 20168
            private void AutoAssignAdjuster()
            {
                int autoAssignFlag = 0;
                Event objEvent = null;
                ClaimAdjuster objAdjuster = null;
                Riskmaster.Db.DbReader objReader = null;
                StringBuilder strSql = new StringBuilder();
                StringBuilder strSkipSql = new StringBuilder();
                bool bIsOracle = false;
                int adjEId = 0;
                int SWI = 0;
                long lastDTTM = 0;

                if (!((IDataModel)((Claim)this).AdjusterList).IsStale)
                {
                    using (objReader = DbFactory.GetDbReader(this.Context.DbConn.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ADJ_AUTOASSIGN_FLAG'"))
                    {
                        if (objReader != null && objReader.Read())
                        {
                            autoAssignFlag = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[0], this.Context.ClientId);
                        }
                    }

                    if (autoAssignFlag > 0)
                    {
                        this.LoadParent();
                        objEvent = (Event)this.Parent;

                        bIsOracle = DbFactory.IsOracleDatabase(this.Context.DbConn.ConnectionString);

                        strSql.Append(string.Format("SELECT {0}, {1}, {2}, {3}, {4}"
                            , "AUTO_ASSIGN_ID" //0
                            , "ADJ_EID" //1
                            , "FORCE_WORK_ITEMS" //2
                            , "SKIP_WORK_ITEMS" //3
                            , "DTTM_LAST_ASSIGNED") //4
                        );

                        if (autoAssignFlag == 2) //auto assign based on Work Items
                        {
                            //mdhamija: MITS 27407
                            //strSql.Append(string.Format(", (SELECT COUNT(*) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (ADJ.DEPT_EIDS IS NULL OR {0}) AND (ADJ.LOB_CODES IS NULL OR {1}) AND (ADJ.CLAIM_TYPE_CODES IS NULL OR {2}) AND (ADJ.JURISDICTION_CODES IS NULL OR {3})) CLAIM_COUNTER", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.DEPT_EIDS", "EVT.DEPT_EID", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.LOB_CODES", "CLM.LINE_OF_BUS_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.CLAIM_TYPE_CODES", "CLM.CLAIM_TYPE_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.JURISDICTION_CODES", "EVT.STATE_ID", bIsOracle))); 
                            //  strSql.Append(string.Format(", (SELECT COUNT(CAJ.CLAIM_ID) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (ADJ.DEPT_EIDS IS NULL OR {0}) AND (ADJ.LOB_CODES IS NULL OR {1}) AND (ADJ.CLAIM_TYPE_CODES IS NULL OR {2}) AND (ADJ.JURISDICTION_CODES IS NULL OR {3})) CLAIM_COUNTER", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.DEPT_EIDS", "EVT.DEPT_EID", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.LOB_CODES", "CLM.LINE_OF_BUS_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.CLAIM_TYPE_CODES", "CLM.CLAIM_TYPE_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.JURISDICTION_CODES", "CLM.FILING_STATE_ID", bIsOracle)));
                            //MITS  27407 End //skhare7 MITS 29108
                            //sbSQL = sbSQL.Append(string.Format(", (SELECT COUNT(*) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (ADJ.DEPT_EIDS IS NULL OR {0}) AND (ADJ.LOB_CODES IS NULL OR {1}) AND (ADJ.CLAIM_TYPE_CODES IS NULL OR {2}) AND (ADJ.JURISDICTION_CODES IS NULL OR {3})) CLAIM_COUNTER", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.DEPT_EIDS", "EVT.DEPT_EID", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.LOB_CODES", "CLM.LINE_OF_BUS_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.CLAIM_TYPE_CODES", "CLM.CLAIM_TYPE_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.JURISDICTION_CODES", "CLM.FILING_STATE_ID", bIsOracle)));
                            if (!bIsOracle)
                            {
                                strSql = strSql.Append(",(SELECT COUNT(CAJ.CLAIM_ID) FROM CLAIM_ADJUSTER CAJ  INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID  INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID  INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID  WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID  AND (ADJ.DEPT_EIDS IS NULL OR ADJ.DEPT_EIDS = '' OR (CHARINDEX(CONVERT(varchar,EVT.DEPT_EID), ADJ.DEPT_EIDS) > 0))  AND (ADJ.LOB_CODES IS NULL OR ADJ.LOB_CODES = '' OR (CHARINDEX(CONVERT(varchar,CLM.LINE_OF_BUS_CODE), ADJ.LOB_CODES) > 0))  AND (ADJ.CLAIM_TYPE_CODES IS NULL OR ADJ.CLAIM_TYPE_CODES='' OR (CHARINDEX(convert(varchar,CLM.CLAIM_TYPE_CODE) , ADJ.CLAIM_TYPE_CODES) > 0))  AND (ADJ.JURISDICTION_CODES IS NULL OR ADJ.JURISDICTION_CODES = '' OR (CHARINDEX(convert(varchar ,CLM.FILING_STATE_ID) , ADJ.JURISDICTION_CODES) > 0)) ) CLAIM_COUNTER ");
                            }
                            else
                            {
                                strSql = strSql.Append(",(SELECT COUNT(CAJ.CLAIM_ID) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (Trim(ADJ.DEPT_EIDS) IS NULL OR (INSTR(ADJ.DEPT_EIDS,EVT.DEPT_EID)>0)) AND (TRIM(ADJ.LOB_CODES) IS NULL OR (INSTR(ADJ.LOB_CODES,CLM.LINE_OF_BUS_CODE)>0)) AND (TRIM(ADJ.CLAIM_TYPE_CODES) IS NULL OR (INSTR(ADJ.CLAIM_TYPE_CODES,CLM.CLAIM_TYPE_CODE)>0)) AND (TRIM(ADJ.JURISDICTION_CODES) IS NULL OR (INSTR(ADJ.JURISDICTION_CODES,CLM.FILING_STATE_ID)>0)) ) CLAIM_COUNTER ");
                            }
                            //skhare7 MITS 29108
                        }
                        //skhare7 MITS 28499
                        strSql.Append(string.Format(" FROM {0} WHERE  {1} AND {2} AND  {3} AND   {4} AND {5} NOT IN"
                            , "AUTO_ASSIGN_ADJ ADJ" //0
							//, "LOB_CODES = ''  OR LOB_CODES is null " //1   Added  by bsharma33 "  OR LOB_CODES is null" for MTIS 27080
							, Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("LOB_CODES", ((Claim)this).LineOfBusCode.ToString(), bIsOracle) //2
							//, "DEPT_EIDS = ''  OR DEPT_EIDS is null " //3   Added  by bsharma33 "OR DEPT_EIDS is null" for MTIS 27080
							, Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("DEPT_EIDS", objEvent.DeptEid.ToString(), bIsOracle) //4
                           // , "CLAIM_TYPE_CODES = ''  OR CLAIM_TYPE_CODES IS NULL" //5
                            , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("CLAIM_TYPE_CODES", ((Claim)this).ClaimTypeCode.ToString(), bIsOracle) //6
                           // , "JURISDICTION_CODES = ''  OR JURISDICTION_CODES IS NULL" //7
                               , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("JURISDICTION_CODES", ((Claim)this).FilingStateId.ToString(), bIsOracle) //8  //Added by Amitosh for MITS 26355 
                            , "ADJ_EID") //9
                        );
                        //skhare7 MITS 28499
                        strSql.Append(string.Format(" (SELECT {0} FROM {1} WHERE {2} >= {3} AND {2} <= {4}) ORDER BY {5} DESC"
                            , "DISTINCT ENTITY_ID" //0
                            , "ENTITY_NON_AVL" //1
                            , Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) //2
                            , "ENTITY_NON_AVL_START" //3
                            , "ENTITY_NON_AVL_END" //4
                            , "FORCE_WORK_ITEMS") //5
                        );

                        if (autoAssignFlag == 2) //auto assign based on Work Items
                        {
                            strSql.Append(", CLAIM_COUNTER ASC, DTTM_LAST_ASSIGNED ASC");
                        }
                        else
                        {
                            strSql.Append(", DTTM_LAST_ASSIGNED ASC");
                        }

                        using (objReader = DbFactory.GetDbReader(this.Context.DbConn.ConnectionString, strSql.ToString()))
                        {
                            while (objReader != null && objReader.Read())
                            {
                                if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], this.Context.ClientId) > 0)
                                {
                                    //update AUTO_ASSIGN_ADJ table and decrease ASSIGNMENTS_TO_SKIP by 1
                                    if (strSkipSql.Length == 0)
                                    {
                                        adjEId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[1], this.Context.ClientId);
                                        SWI = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], this.Context.ClientId);
                                        lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], this.Context.ClientId);

                                        strSkipSql.Append(string.Format(" WHERE {0} = {1}"
                                            , "AUTO_ASSIGN_ID" //0
                                            , Riskmaster.Common.Conversion.ConvertObjToInt(objReader[0], this.Context.ClientId)) //1
                                        );
                                    }
                                    else
                                    {
                                        if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], this.Context.ClientId) < SWI)
                                        {
                                            adjEId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[1], this.Context.ClientId);
                                            SWI = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], this.Context.ClientId);
                                            lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], this.Context.ClientId);
                                        }
                                        else if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], this.Context.ClientId) == SWI && Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], this.Context.ClientId) < lastDTTM)
                                        {
                                            adjEId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[1], this.Context.ClientId);
                                            lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], this.Context.ClientId);
                                        }
                                        strSkipSql.Append(string.Format(" OR {0} = {1}"
                                            , "AUTO_ASSIGN_ID" //0
                                            , Riskmaster.Common.Conversion.ConvertObjToInt(objReader[0], this.Context.ClientId)) //1
                                        );
                                    }
                                }
                                else
                                {
                                    if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[2], this.Context.ClientId) > 0)
                                    {
                                        //update AUTO_ASSIGN_ADJ table and decrease FORCE_WORK_ITEMS by 1
                                        if (strSql.Length > 0)
                                        {
                                            strSql.Remove(0, strSql.Length);
                                        }
                                        strSql.Append(string.Format("UPDATE {0} SET {1} = {1} - 1 WHERE {2} = {3}"
                                            , "AUTO_ASSIGN_ADJ" //0
                                            , "FORCE_WORK_ITEMS" //1
                                            , "AUTO_ASSIGN_ID" //2
                                            , Riskmaster.Common.Conversion.ConvertObjToInt(objReader[0], this.Context.ClientId)) //3
                                        );
                                        this.Context.DbConn.ExecuteNonQuery(strSql.ToString());
                                    }
                                    adjEId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[1], this.Context.ClientId);
                                    SWI = 1;
                                    break;
                                }
                            }
                            if (adjEId != 0)
                            {
                                objAdjuster = (ClaimAdjuster)this.Context.Factory.GetDataModelObject("ClaimAdjuster", false);
                                objAdjuster.ClaimId = ((Claim)this).ClaimId;
                                objAdjuster.AdjusterEid = adjEId;
                                objAdjuster.AutoAssignFlag = true;
                                //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER
                                //objAdjuster.ReferenceNumber = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(this.Context.DbConn.ConnectionString, " SELECT  REFERENCE_NUMBER FROM ENTITY WHERE  ENTITY_ID=" + adjEId));
                                ((Claim)this).AdjusterList.Add(objAdjuster);
                            }
                            if (strSkipSql.Length > 0)
                            {

                                strSkipSql.Insert(0, string.Format("UPDATE {0} SET {1} = {1} - {2}, {3} = {4}"
                                    , "AUTO_ASSIGN_ADJ" //0
                                    , "SKIP_WORK_ITEMS" //1
                                    , SWI //2
                                    , "DTTM_LAST_ASSIGNED" //3
                                    , Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now)) //4
                                );
                                this.Context.DbConn.ExecuteNonQuery(strSkipSql.ToString());
                            }
                        }
                        //objReader.Close();
                    }
                }
            }
            //sanoopsharma start

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sParamter"></param>
            /// <param name="iPolicySystemId"></param>
            /// <returns></returns>
           public string GetPolicySystemTypeText(int iPolicySystemId)
            {
                string sReturnValue = string.Empty;
                Riskmaster.Db.DbReader objReader = null;
                StringBuilder strSql = new StringBuilder();
                Dictionary<string, object> dictParams = new Dictionary<string, object>();

                strSql.AppendFormat("SELECT SHORT_CODE FROM CODES"+ 
                    " INNER JOIN POLICY_X_WEB ON POLICY_X_WEB.POLICY_SYSTEM_CODE = CODES.CODE_ID"+
                    " WHERE POLICY_X_WEB.POLICY_SYSTEM_ID = {0}", "~iPolicySystemId~");

                dictParams.Add("iPolicySystemId", iPolicySystemId);

                try
                {
                    using (DbReader objRdr = DbFactory.ExecuteReader(Context.DbConn.ConnectionString, strSql.ToString(),dictParams))
                    {
                        if (objRdr.Read())
                            sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }


                }
                catch (Exception e)
                {
                    throw e;
                }

                return sReturnValue;
            }

            //sanoopsharma end
		}
	
		internal class SerializationContext
		{
			public static ReaderWriterLock RWLock = new ReaderWriterLock();  // BSB 12.19.2005   Lock to protect static XmlSchema.
			public static XmlSchema XML_SCHEMA=null;
            public static XmlSchemaSet XML_SCHEMAS = new XmlSchemaSet();

            public string RequestXPath; //XPath string for the current location in the objRequestDom.
            public XmlDocument RequestDom; //XML Document explicitly tagged for all requested children.
            public string ElementTag; //XML Element Tag name to use instead of defaulting to the object type.

            /// <summary>
            /// Gets an empty string for the XML Namespace information 
            /// </summary>
            public static string XML_NAMESPACE
            {
                get
                {
                    return string.Empty;
                }//get
            }//property: XML_NAMESPACE

			//Prepare a new Context for a Recursive call.
			//1.) Fix the RequestXPath for the child context.
			//2.) Apply a new elementTag
			//3.) Copy the remaining info from the parent Context. (this)
			public SerializationContext CreateChild(string elementTag)
			{
				SerializationContext objTmpContext = new SerializationContext();
				objTmpContext.RequestDom = this.RequestDom;
				objTmpContext.RequestXPath = this.RequestXPath + "/" +  elementTag; 
				objTmpContext.ElementTag = elementTag;
				return objTmpContext;
			}
			internal bool IsRequested(string sProp)
			{
				return(this.RequestDom.SelectSingleNode(this.RequestXPath + "/" +sProp) != null);
			}
		}
	}
}
