using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Settings;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Just holds a couple of helper routines for Script writers.  
	/// Much simpler now that our DataReader object can be exposed directly.
	/// </summary>
	public class ScriptEnvironment  : DataRoot
	{
		//METHODS
		#region Constructor Logic
		internal ScriptEnvironment(Context context):base(true,context)
		{
			//			Initialize();
		}
		internal ScriptEnvironment(bool isLocked, Context context):base(isLocked,context)
		{
			//		Initialize();
		}
		#endregion
		//BSB Now that Recordsets are truly "object oriented" we can leave out all the other 
		//Data Access wrapper functions (Script can safely access them directly via the reader.).
		public DbReader ExecuteReader(string SQL)
		{
			return Context.DbConnLookup.ExecuteReader(SQL);
		}

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for lookupUserID
        /// </summary>     
        public string lookupUserID(int userId)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return  lookupUserID( userId,0);
            }
            else
                throw new Exception("lookupUserID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

		public string lookupUserID(int userId, int p_iClientId)//rkaur27 : Cloud Change
		{
			DbReader objReader =null;
			string  sSQL=""; 
			string userName = "";
			sSQL = String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE DSNID = {0} AND USER_ID = {1}",Context.RMDatabase.DataSourceId,userId);
			objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(p_iClientId), sSQL);// rkaur27
			// Rem find user in security db
			if(objReader.Read())
				userName = objReader.GetString("LOGIN_NAME");
			objReader.Close();

			return userName;
		}
		
		public Entity GetEntityParent(int entityId, int levelId ) 
		{
			Entity  obj = Context.Factory.GetDataModelObject("Entity",false) as Entity;
			obj.MoveTo(entityId);

			if (obj.ParentEid == 0)
				return (Entity) null;

			while( obj.EntityTableId != levelId && obj.EntityId != 0)
				obj.MoveTo(obj.ParentEid);

			if(obj.EntityId != 0)
				return  obj;
			else
				return (Entity) null;
		}
	}
}