﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_ENTITY_STGNG", "POLICYENTITY_ROWID", "PolicyEid")]
    public class PolicyXEntityStgng:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                                        {"EntityId","ENTITY_ID"},
                                                        {"PolicyId","POLICY_ID"},
                                                        {"PolicyEid","POLICYENTITY_ROWID"},
                                                        {"TypeCode","TYPE_CODE"},
                                                        {"PolicyUnitRowid","POLICY_UNIT_ROW_ID"},
                                                        {"ExternalRole","EXTERNAL_ROLE"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"}
                                    };
        
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int PolicyEid { get { return GetFieldInt("POLICYENTITY_ROWID"); } set { SetField("POLICYENTITY_ROWID", value); } }
        public string TypeCode { get { return GetFieldString("TYPE_CODE"); } set { SetField("TYPE_CODE", value); } }
        public int PolicyUnitRowid { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        public string ExternalRole { get { return GetFieldString("EXTERNAL_ROLE"); } set { SetField("EXTERNAL_ROLE", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        #endregion

        internal PolicyXEntityStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "POLICY_X_ENTITY_STGNG";
            this.m_sKeyField = "POLICYENTITY_ROWID";
            this.m_sParentClassName = "PolicyStgng";
            this.m_sFilterClause = string.Empty;
                     base.InitFields(sFields);
           
            base.Initialize();
        }
    }
}
