using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of ClientLimits AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("CLIENT_LIMITS","CLIENT_LIMITS_ROWID")]
	public class ClientLimits  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"ClientEid", "CLIENT_EID"},
														{"LobCode", "LOB_CODE"},
														{"ReserveTypeCode", "RESERVE_TYPE_CODE"},
														{"PaymentFlag", "PAYMENT_FLAG"},
														{"MaxAmount", "MAX_AMOUNT"},
														{"ClientLimitsRowId","CLIENT_LIMITS_ROWID"},
		};

		public int ClientEid{get{return GetFieldInt("CLIENT_EID");}set{SetField("CLIENT_EID",value);}}
		public int LobCode{get{return GetFieldInt("LOB_CODE");}set{SetField("LOB_CODE",value);}}
		public int ReserveTypeCode{get{return GetFieldInt("RESERVE_TYPE_CODE");}set{SetField("RESERVE_TYPE_CODE",value);}}
		public int PaymentFlag{get{return GetFieldInt("PAYMENT_FLAG");}set{SetField("PAYMENT_FLAG",value);}}
        //Start MITS ID 31950 6/13/2013 psaiteja@csc.com
		//  public int MaxAmount{get{return GetFieldInt("MAX_AMOUNT");}set{SetField("MAX_AMOUNT",value);}}
        public double MaxAmount { get { return GetFieldDouble("MAX_AMOUNT"); } set { SetField("MAX_AMOUNT", value); } }
        //End MITS ID 31950 6/13/2013 psaiteja@csc.com
		public int ClientLimitsRowId{get{return GetFieldInt("CLIENT_LIMITS_ROWID");}set{SetField("CLIENT_LIMITS_ROWID",value);}}
		
		#endregion
		

		internal ClientLimits(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			//base.Initialize();


			this.m_sTableName = "CLIENT_LIMITS";
			this.m_sKeyField = "CLIENT_LIMITS_ROWID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize(); 
		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}