﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public class CvgXLossList:DataCollection
    {

        internal CvgXLossList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "CVG_LOSS_ROW_ID";
            this.SQLFromTable = "COVERAGE_X_LOSS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CvgXLoss";
		}
        public new CvgXLoss this[int keyValue] { get { return base[keyValue] as CvgXLoss; } }
        public new CvgXLoss AddNew() { return base.AddNew() as CvgXLoss; }
        public CvgXLoss Add(CvgXLoss obj) { return base.Add(obj) as CvgXLoss; }
        public new CvgXLoss Add(int keyValue) { return base.Add(keyValue) as CvgXLoss; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
