﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{

    [Riskmaster.DataModel.Summary("CLAIM_X_LIABILITYLOSS", "ROW_ID")]
  public  class ClaimXLiabilityLoss : DataObject 
    {
        #region Database Field List
        private string[,] sFields = {   

                                                        {"RowId", "ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"LiabilityType", "LIABILITY_TYPE_CODE"},
                                                        {"PartyAffected", "PARTY_AFFECTED"},
														{"DamageType", "DAMAGE_TYPE_CODE"},
														{"PracticeArea", "PRACTICE_AREA_CODE"},
														{"InjuryType", "INJURY_TYPE_CODE"},
														{"PremiseType", "PREMISE_TYPE_CODE"},
														{"OtherPremises", "OTHER_PREMISES"},
														{"ProductType", "PRODUCT_TYPE_CODE"},
														{"ProductIndicator", "PRODUCT_INDICATOR_CODE"},
														{"DateofIncidence", "DATE_OF_INCIDENCE"},
                                                        {"Manufacturer", "MANUFACTURER"},
                                                        {"AllegedHarm", "ALLEGED_HARM"},
                                                        {"BrandName", "BRANDNAME"},
                                                        {"GenericName", "GENERIC_NAME"},
                                                        {"ProductSeenAt", "PRODUCT_SEEN_AT"},
                                                        {"OtherProduct", "OTHERPRODUCT"},
														{"DateOfEmployement", "DATE_OF_EMPLOYEMENT"},
														{"PlainTiffClaimantType", "PLAINTIFF_CLAIMANT_TYPE_CODE"},
														{"CvgFormType", "CVG_FORM_TYPE_CODE"},
                                                        {"EmpTitle", "EMP_TITLE_CODE"},
                                                        {"BackgroundCheck", "BACKGROUND_CHECK_CODE"},
                                                        {"DateOfTermination", "DATE_OF_TERMINATION"},
                                                        {"ScopeOfBusiness", "SCOPE_OF_BUSINESS_CODE"},
                                                        {"GeneralDamage", "GENERAL_DAMAGE"},
                                                        {"SpecialDamage", "SPECIAL_DAMAGE"},
                                                        {"YearsinPosition", "YEARS_IN_POSITION"},
                                                        {"OtherInsurance", "OTHERINSURANCE"},
                                                        {"PolholderIsPropertyOwner", "POLHOLDERISPROPERTYOWNER"},
                                                        {"PolicyHolder", "POLICYHOLDER"},
                                                        {"Company", "COMPANY"},
                                                        {"PolicyNumber", "POLICYNUMBER"},
                                                        {"Coverages", "COVERAGES"},
                                                        {"Limits", "LIMITS"}, 
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"}
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
       
        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
           [ExtendedTypeAttribute(RMExtType.Code, "LIABILITY_TYPE")]
        public int LiabilityType { get { return GetFieldInt("LIABILITY_TYPE_CODE"); } set { SetField("LIABILITY_TYPE_CODE", value); } }
        //Sachin Start : Worked for JIRA - 340 (Entity Role)
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")] 	//RUPAL, MITS 27481
           public int PartyAffected { get { return GetFieldInt("PARTY_AFFECTED"); } set { SetFieldAndNavTo("PARTY_AFFECTED", value, "PartyAffectedEntity"); } }
        //public int PartyAffected { get { return GetFieldInt("PARTY_AFFECTED"); } set { SetField("PARTY_AFFECTED", value); } }
        //Sachin End
        [ExtendedTypeAttribute(RMExtType.Code, "DAMAGE_TYPE")]
        public int DamageType { get { return GetFieldInt("DAMAGE_TYPE_CODE"); } set { SetField("DAMAGE_TYPE_CODE", value); } }
          [ExtendedTypeAttribute(RMExtType.Code, "AREA_OF_PRACTICE")]
        public int PracticeArea { get { return GetFieldInt("PRACTICE_AREA_CODE"); } set { SetField("PRACTICE_AREA_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "INJURY_TYPE")]
          public int InjuryType { get { return GetFieldInt("INJURY_TYPE_CODE"); } set { SetField("INJURY_TYPE_CODE", value); } }
           [ExtendedTypeAttribute(RMExtType.Code, "TYPE_OF_PREMISES")]
        public int PremiseType { get { return GetFieldInt("PREMISE_TYPE_CODE"); } set { SetField("PREMISE_TYPE_CODE", value); } }
        public string OtherPremises { get { return GetFieldString("OTHER_PREMISES"); } set { SetField("OTHER_PREMISES", value); } }
           [ExtendedTypeAttribute(RMExtType.Code, "TYPE_OF_PRODUCT")]
        public int ProductType { get { return GetFieldInt("PRODUCT_TYPE_CODE"); } set { SetField("PRODUCT_TYPE_CODE", value); } }
    [ExtendedTypeAttribute(RMExtType.Code, "PRODUCT_INDICATOR")]
           public int ProductIndicator { get { return GetFieldInt("PRODUCT_INDICATOR_CODE"); } set { SetField("PRODUCT_INDICATOR_CODE", value); } }

    public string DateofIncidence { get { return GetFieldString("DATE_OF_INCIDENCE"); } set { SetField("DATE_OF_INCIDENCE", Riskmaster.Common.Conversion.GetDate(value)); } }

        public string Manufacturer { get { return GetFieldString("MANUFACTURER"); } set { SetField("MANUFACTURER", value); } }

        public string AllegedHarm { get { return GetFieldString("ALLEGED_HARM"); } set { SetField("ALLEGED_HARM", value); } }
        public string BrandName { get { return GetFieldString("BRANDNAME"); } set { SetField("BRANDNAME", value); } }
        public string GenericName { get { return GetFieldString("GENERIC_NAME"); } set { SetField("GENERIC_NAME", value); } }
        public string ProductSeenAt { get { return GetFieldString("PRODUCT_SEEN_AT"); } set { SetField("PRODUCT_SEEN_AT", value); } }

        public string OtherProduct { get { return GetFieldString("OTHERPRODUCT"); } set { SetField("OTHERPRODUCT", value); } }
        public string DateOfEmployement { get { return GetFieldString("DATE_OF_EMPLOYEMENT"); } set { SetField("DATE_OF_EMPLOYEMENT", Riskmaster.Common.Conversion.GetDate(value)); } }
            [ExtendedTypeAttribute(RMExtType.Code, "PLAINTIFF_CLAIMANT_TYPE")]
        public int PlainTiffClaimantType { get { return GetFieldInt("PLAINTIFF_CLAIMANT_TYPE_CODE"); } set { SetField("PLAINTIFF_CLAIMANT_TYPE_CODE", value); } }
          [ExtendedTypeAttribute(RMExtType.Code, "COVERAGE_FORM_TYPE")]
            public int CvgFormType { get { return GetFieldInt("CVG_FORM_TYPE_CODE"); } set { SetField("CVG_FORM_TYPE_CODE", value); } }
            [ExtendedTypeAttribute(RMExtType.Code, "EMPLOYEE_TITLE")]
          public int EmpTitle { get { return GetFieldInt("EMP_TITLE_CODE"); } set { SetField("EMP_TITLE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "BACKGROUNG_CHECK")]
            public int BackgroundCheck { get { return GetFieldInt("BACKGROUND_CHECK_CODE"); } set { SetField("BACKGROUND_CHECK_CODE", value); } }

        public string DateOfTermination { get { return GetFieldString("DATE_OF_TERMINATION"); } set { SetField("DATE_OF_TERMINATION", Riskmaster.Common.Conversion.GetDate(value)); } }
          [ExtendedTypeAttribute(RMExtType.Code, "WITHIN_SCOPE_OF_BUSINESS")]
        public int ScopeOfBusiness { get { return GetFieldInt("SCOPE_OF_BUSINESS_CODE"); } set { SetField("SCOPE_OF_BUSINESS_CODE", value); } }
        public string GeneralDamage { get { return GetFieldString("GENERAL_DAMAGE"); } set { SetField("GENERAL_DAMAGE", value); } }
        public string SpecialDamage { get { return GetFieldString("SPECIAL_DAMAGE"); } set { SetField("SPECIAL_DAMAGE", value); } }
        public int YearsinPosition { get { return GetFieldInt("YEARS_IN_POSITION"); } set { SetField("YEARS_IN_POSITION", value); } }

        public bool OtherInsurance { get { return GetFieldBool("OTHERINSURANCE"); } set { SetField("OTHERINSURANCE", value); } }
        public bool PolholderIsPropertyOwner { get { return GetFieldBool("POLHOLDERISPROPERTYOWNER"); } set { SetField("POLHOLDERISPROPERTYOWNER", value); } }
       
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")] //RUPAL, MITS 27481
        //Sachin Start : Worked for JIRA - 340 (Entity Role)
        //public int PolicyHolder { get { return GetFieldInt("POLICYHOLDER"); } set { SetField("POLICYHOLDER", value); } }
        public int PolicyHolder { get { return GetFieldInt("POLICYHOLDER"); } set { SetFieldAndNavTo("POLICYHOLDER", value, "PolicyHolderEntity"); } }
        //Sachin End
            [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        //Sachin Start : Worked for JIRA - 340 (Entity Role)
          //public int Company { get { return GetFieldInt("COMPANY"); } set { SetField("COMPANY", value); } }
        public int Company { get { return GetFieldInt("COMPANY"); } set { SetFieldAndNavTo("COMPANY", value, "CompanyEntity"); } }
            //Sachin End
        public string PolicyNumber { get { return GetFieldString("POLICYNUMBER"); } set { SetField("POLICYNUMBER", value); } }

        public string Coverages { get { return GetFieldString("COVERAGES"); } set { SetField("COVERAGES", value); } }

        public string Limits { get { return GetFieldString("LIMITS"); } set { SetField("LIMITS", value); } }

        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        #endregion

        //Sachin Start : Worked for JIRA - 340 (Entity Role)
        #region Child Implementation
        private string[,] sChildren = {
                                            {"PartyAffectedEntity","Entity"},
                                            {"PolicyHolderEntity","Entity"},
                                            {"CompanyEntity","Entity"}
									  };

        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            Entity objEnt = null;
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "Entity":
                    switch (childName)
                    {
                        case "PartyAffectedEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                        case "PolicyHolderEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                        case "CompanyEntity":
                            objEnt = (base.m_Children[childName] as Entity);
                            objEnt.LockEntityTableChange = false;
                            objEnt.LockMoveNavigation = true;
                            objEnt.DataChanged = false;
                            break;
                    }
                    break;
            }
        }
        //Sachin End Jira 340

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        //Sachin Start : Worked for JIRA - 340 (Entity Role)
        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            int intTableID;
            Dictionary<string, int> objReturnVal = new Dictionary<string, int>();
            if (childName == "PartyAffectedEntity")
            {
                Entity objEntity = ((childValue as Entity));
                intTableID = Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.OTHER_PEOPLE.ToString());
                objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                if (objReturnVal != null)
                    this.m_Fields["PARTY_AFFECTED"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
            }
            if (childName == "PolicyHolderEntity")
            {
                Entity objEntity = ((childValue as Entity));
                intTableID = Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.OTHER_PEOPLE.ToString());
                objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                if (objReturnVal != null)
                    this.m_Fields["POLICYHOLDER"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
            }
            if (childName == "CompanyEntity")
            {
                Entity objEntity = ((childValue as Entity));
                intTableID = Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.OTHER_PEOPLE.ToString());
                objEntity.UpdateEntityRoles(childValue, intTableID, objReturnVal);
                if (objReturnVal != null)
                    this.m_Fields["COMPANY"] = objReturnVal[Globalization.ConstReturnValues.EntityID.ToString()];
            }
            objReturnVal = null;
        }
        //Sachin JIRA - 340 Ends(Entity Role)

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            base.OnChildPostSave(childName, childValue, isNew);
        }

        //Sachin Start : Worked for JIRA - 340 (Entity Role)
        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            if (childName == "PartyAffectedEntity")
                return;
            if (childName == "PolicyHolderEntity")
                return;
            if (childName == "CompanyEntity")
                return;
            base.OnChildDelete(childName, childValue);
        }       

        //Child Property Accessors
        public Entity PartyAffectedEntity
        {
            get
            {
                Entity objItem = base.m_Children["PartyAffectedEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("PartyAffectedEntity", (objItem as DataObject), this.PartyAffected) as Entity;
                return objItem;
            }
        }
        public Entity PolicyHolderEntity
        {
            get
            {
                Entity objItem = base.m_Children["PolicyHolderEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("PolicyHolderEntity", (objItem as DataObject), this.PolicyHolder) as Entity;
                return objItem;
            }
        }
        public Entity CompanyEntity
        {
            get
            {
                Entity objItem = base.m_Children["CompanyEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("CompanyEntity", (objItem as DataObject), this.Company) as Entity;
                return objItem;
            }
        }
        #endregion
        //Sachin JIRA - 340 Ends(Entity Role)

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            //Sachin Start : Worked for JIRA - 340 (Entity Role)
            this.SetFieldAndNavTo("PARTY_AFFECTED", this.PartyAffected, "PartyAffectedEntity", true);
            this.SetFieldAndNavTo("POLICYHOLDER", this.PolicyHolder, "PolicyHolderEntity", true);
            this.SetFieldAndNavTo("COMPANY", this.Company, "CompanyEntity", true);
            //Sachin JIRA - 340 Ends(Entity Role)
        }

        internal ClaimXLiabilityLoss(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        public override IDataModel Parent
        {
            get
            {


                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

        new private void Initialize()
        {
            this.m_sTableName = "CLAIM_X_LIABILITYLOSS";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

         
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);           //Sachin Worked on JIRA - 340 (Entity Role)
            this.m_sParentClassName = "Claim";
            base.Initialize();  

        }

        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        internal override string OnBuildSuppTableName()
        {
            return "CLAIM_X_LIABILITYLOSS_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }


        #endregion
    }
}
