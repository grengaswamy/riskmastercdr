
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forReserveHistoryList.
	/// </summary>
	public class ReserveHistoryList : DataCollection
	{
		internal ReserveHistoryList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"RSV_ROW_ID";
			this.SQLFromTable =	"RESERVE_HISTORY";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ReserveHistory";
		}
		public new ReserveHistory this[int keyValue]{get{return base[keyValue] as ReserveHistory;}}
		public new ReserveHistory AddNew(){return base.AddNew() as ReserveHistory;}
		public  ReserveHistory Add(ReserveHistory obj){return base.Add(obj) as ReserveHistory;}
		public new ReserveHistory Add(int keyValue){return base.Add(keyValue) as ReserveHistory;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}