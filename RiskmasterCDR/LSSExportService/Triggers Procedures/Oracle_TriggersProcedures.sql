--------------------------------------------------------------------------------------------------------1):
create or replace TRIGGER trCodes_IU BEFORE INSERT or update ON CODES   
  				FOR EACH ROW    
  				DECLARE  
                                   sSysTabName varchar2(50);    
BEGIN

select g.system_table_name into sSysTabName from glossary g 
where g.table_id=:new.table_id and g.system_table_name in ('CLAIM_TYPE','INJURY_TYPE');

If ((:new.short_code is not null OR :new.deleted_flag is not null) AND sSysTabName is not null) THEN
 
If updating  THEN
 
	insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED,
        	DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID)
	values(:new.code_id, :old.short_code, to_char(sysdate, 'yyyymmddhh24mmss'), null, 
decode(sSysTabName, 'CLAIM_TYPE','LINEOFBUSINESS','INJURY_TYPE','NATUREOFINJURY'),
0,null);

--END IF;
ELSIf inserting  THEN
	insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED,
        	DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID)
	values (:new.code_id, :new.short_code, to_char(sysdate, 'yyyymmddhh24mmss'), null, 
decode(sSysTabName, 'CLAIM_TYPE','LINEOFBUSINESS','INJURY_TYPE','NATUREOFINJURY'),
0,null);
END IF;
END IF;
exception


	WHEN others THEN
    raise_application_error(-20001,'Unexpected error in trigger trCodes_IU');
	ROLLBACK;
END;
/

-----------------------------------------------------------------------------------------------------------------2):

create or replace TRIGGER trCodes_Text_U BEFORE update ON Codes_Text  
  				FOR EACH ROW    
  				DECLARE  
                                   sSysTabName varchar2(50); 
				dtdif number;   
BEGIN

select g.system_table_name into sSysTabName from codes c,  glossary g where c.code_id=:new.code_id and c.table_id=g.table_id and g.system_table_name in ('CLAIM_TYPE','INJURY_TYPE');

select to_char(sysdate, 'yyyymmddhh24mmss' ) - to_char(nvl(a.DTTM_CHANGED,0), 'yyyymmddhh24mmss' ) into dtdif from RM_LSS_CODE_EXP a
where a.code_id=:new.code_id;

 
If (:new.CODE_DESC is not null)  AND (sSysTabName is not null) and dtdif > 5 THEN
 
insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED, DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID) 
values (:new.code_id, :old.short_code, to_char(sysdate, 'yyyymmddhh24mmss'), null,decode(sSysTabName, 'CLAIM_TYPE','LINEOFBUSINESS','INJURY_TYPE','NATUREOFINJURY'),0,null);
 
END IF;
exception
	WHEN others THEN
    raise_application_error(-20001,'Unexpected error in trigger trCodes_Text_U');
	ROLLBACK;
END;
/

--------------------------------------------------------------------------------------------------------------------3):
create or replace TRIGGER trEntity_U BEFORE update ON ENTITY  
  				FOR EACH ROW    
  				DECLARE  

 sEntityType varchar2(50);
 sUpdatedByUser varchar2(16);
 sLSSRMXUser varchar2(16);
 sDeletedFlag varchar2(2);

BEGIN

SELECT UPPER(G.SYSTEM_TABLE_NAME) INTO sEntityType FROM GLOSSARY G 
WHERE G.TABLE_ID = :new.ENTITY_TABLE_ID; 

IF sEntityType = 'ADJUSTERS' THEN
	 sEntityType := 'ADJUSTER';
END IF;

sUpdatedByUser := :new.UPDATED_BY_USER;
sDeletedFlag := :new.DELETED_FLAG;

SELECT RMX_LSS_USER INTO sLSSRMXUser FROM SYS_PARMS;

IF (sDeletedFlag = '-1') THEN
 
	IF sUpdatedByUser <> sLSSRMXUser THEN
	 
		IF sEntityType IS NOT NULL THEN
		 
			IF sEntityType = 'ADJUSTER'  THEN
			 
INSERT INTO RM_LSS_ADMIN_EXP (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) 
VALUES (:new.ENTITY_ID, to_char(sysdate, 'yyyymmddhh24mmss'), sEntityType , 0 );

ELSE
	INSERT INTO RM_LSS_PAYEE_EXP (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) 
	values (:new.ENTITY_ID, to_char(sysdate, 'yyyymmddhh24mmss'), sEntityType , 0);


			END IF;	
		END IF;
	END IF;
END IF;
exception
	WHEN others THEN
    raise_application_error(-20001,'Unexpected error in trigger trEntity_U');
	ROLLBACK;
END;
/


