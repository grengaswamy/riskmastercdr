using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

	/**************************************************************
	 * $File		: LSSExportServiceInstaller.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 09/12/2006
	 * $Author		: Manoj Agrawal
	 * $Comment		: Win Service Installer class
	 * $Source		: 
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 **************************************************************/

namespace Riskmaster.LSSExportService
{
	/// <summary>
	/// Summary description for LSSExportServiceInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class LSSExportServiceInstaller : Installer
	{
		private ServiceProcessInstaller serviceProcessInstaller1;
		private ServiceInstaller serviceInstaller1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        /// <summary>
        /// Class constructor
        /// </summary>
		public LSSExportServiceInstaller()
		{
			// This call is required by the Designer.
			InitializeComponent();


			// TODO: Add any initialization after the InitializeComponent call
			this.serviceProcessInstaller1 = new ServiceProcessInstaller();
			this.serviceInstaller1 = new ServiceInstaller();

			// 
			// serviceProcessInstaller1
			// 
			this.serviceProcessInstaller1.Password = null;
			this.serviceProcessInstaller1.Username = null;
			this.serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
			// 
			// serviceInstaller1
			// 
			this.serviceInstaller1.ServiceName = "LSSExportService";
			this.serviceInstaller1.StartType = ServiceStartMode.Automatic;
			this.serviceInstaller1.DisplayName = "RMX LSS Export Service";

			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(
				new System.Configuration.Install.Installer[]{this.serviceProcessInstaller1, this.serviceInstaller1}
				);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
