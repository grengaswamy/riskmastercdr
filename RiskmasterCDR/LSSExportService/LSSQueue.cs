﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.LSSExportService
{
    /// <summary>
    /// Provides a framework for queuing LSS records for processing
    /// </summary>
    /// <remarks>This class is currently assuming only a single thread.  If multiple threads need to 
    /// be managing this class, the class must first be made thread-safe through Mutex, Monitor, lock synchronization etc.</remarks>
    public class LSSQueue
    {
        private Queue<LSSRecord> objLSSQueue = new Queue<LSSRecord>();

        public void AddLSSRecord(LSSRecord lssRec)
        {
            objLSSQueue.Enqueue(lssRec);
        }//method: AddLSSRecord

        public LSSRecord GetLSSRecord()
        {
            LSSRecord lssRec = null;

            lssRec = objLSSQueue.Dequeue();

            return lssRec;
        }//method: GetLSSRecord()

        public bool IsLSSRecordAvailable
        {
            get
            {
                return objLSSQueue.Count > 0;
            }//get
        }//property: IsLSSRecordAvailable

    }//class LSSQueue

    /// <summary>
    /// Provides a preliminary shell for storing LSS records
    /// </summary>
    /// <remarks>This current implementation may not fully encompass the required class structure to fully represent an LSS record</remarks>
    public class LSSRecord
    {
        public string LSSCodes { get; set; }
        public DateTime LSSTimestamp { get; set; }
        public string LSSData { get; set; }
    }//class: LSSRecord
}
