using System;
using System.Security.Principal;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Security
{
	/// <summary>
	/// Riskmaster.Security.User class classifies User�s details on context of Riskmaster, 
	/// it contain all the information about the user like Name,City,Country Phone etc..
	/// </summary>
	[Serializable] 
	public partial class User
	{
		private int m_UserId, m_DsnID;
		private string m_FirstName;
		private string m_LastName;
		private string m_Address1, m_Address2;
		private string m_City;
		private string m_State;
		private string m_Country;
		private string m_ZipCode;
		private string m_Email;
		private int m_ManagerId;
		private string m_HomePhone;
		private string m_OfficePhone;
		private string m_Title;
		private int m_NlsCode;
		private string m_CompanyName;
        private string m_Dsn = string.Empty, m_CheckSum = string.Empty;
		private string m_ManagerEmail;
        private bool m_bEnforceCheckSum = true;
		internal  bool m_DataChanged=false;
        private string m_sUserName = string.Empty;    //gagnihotri MITS 11995 Changes made for Audit table
        //R5..Raman Bhatia March 16 2009..new member to store SMS access flag
        private bool m_bSMSAccess = false;
        //skhare7
        private bool m_bUPSAccess = false;
        private int m_iClientId = 0;

		/// <summary>
		/// Riskmaster.Security.User is class constructor.
		/// </summary>        
        public User()
        { }

		public User(int p_iClientId)
		{// ClientID added as required argument to avoid conflict with UserId
            m_iClientId = p_iClientId;
		}       

		/// <summary>
		/// Riskmaster.Security.User construcotr with parameter. 
		/// This constructor calls the function Load() and serves as a wrapper.
		/// </summary>
		/// <param name="userId">Value of the userid column</param>
		public User(int userId, int p_iClientId)
        {// ClientID added as required argument to avoid conflict with UserId
            m_iClientId = p_iClientId;
			this.Load(userId);
		}
        public User(int userId, bool p_bEnforceCheckSum, int p_iClientId)
		{
			m_bEnforceCheckSum = p_bEnforceCheckSum;
            m_iClientId = p_iClientId;
			this.Load(userId);
		}
		
        //gagnihotri MITS 11995 Changes made for Audit table
        public User(string p_sUserName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Override the Equals method available on the base object class
        /// to compare the values present in both objects
        /// </summary>
        /// <param name="obj">object instance to be used for comparison</param>
        /// <returns>boolean indicating whether or not the objects are equivalent</returns>
        public override bool Equals(object obj)
        {
            Type tUserType = typeof(User);
            PropertyInfo[] tUserProps = tUserType.GetProperties();
            bool blnValueDiffers = false;
            const string MANAGER_PROP = "Manager", IDENTITY_PROP = "Identity";

            foreach (PropertyInfo item in tUserProps)
            {
                //Verify that only value type properties are compared and not Reference types
                if (string.Compare(item.Name, MANAGER_PROP, true) != 0 || string.Compare(item.Name, IDENTITY_PROP, true) != 0)
                {
                    object actual = item.GetValue(obj, null);
                    object expected = item.GetValue(this, null);

                    if (!actual.Equals(expected))
                    {
                        blnValueDiffers = true;
                        break;
                    } // if
                } // if
            }//foreach
            
            return blnValueDiffers;
        }//method: Equals

		/// <summary>
		/// Riskmaster.Security.Load uses DbFactory to establish connection with the database. Executes the query to retrieve the
		/// entire contents of the USER_TABLE for the input userId to a DbReader object. 
		/// Invokes LoadData() passing the DbReader object as parameter.
		/// </summary>
		/// <param name="userId">Value of the userid column</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
		public void Load(int userId)
		{
            // To hold Manager's Email address query result,Added by Tanuj Narula on 20/5/2004   
            object managerEmailAddress = null;

            using (DbReader reader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(m_iClientId), string.Format("SELECT USER_TABLE.* FROM USER_TABLE WHERE USER_TABLE.USER_ID={0}", userId)))
            {
                while (reader.Read())
                {
                    m_UserId = System.Convert.ToInt32(reader["USER_ID"]);
                    m_LastName = reader["LAST_NAME"].ToString(); //implicit .ToString() called?
                    m_FirstName = reader.GetString("FIRST_NAME");
                    m_Address1 = reader.GetString("ADDRESS1");
                    m_Address2 = reader.GetString("ADDRESS2");
                    m_City = reader.GetString("CITY");
                    m_State = reader.GetString("STATE_ABBR");
                    m_Country = reader.GetString("COUNTRY");
                    m_ZipCode = reader.GetString("ZIP_CODE");
                    m_Email = reader.GetString("EMAIL_ADDR");
                    m_ManagerId = reader.GetInt("MANAGER_ID");
                    m_HomePhone = reader.GetString("HOME_PHONE");
                    m_OfficePhone = reader.GetString("OFFICE_PHONE");
                    m_Title = reader.GetString("TITLE");
                    m_NlsCode = reader.GetInt("NLS_CODE");
                    m_CompanyName = reader.GetString("COMPANY_NAME");
                    m_CheckSum = reader.GetString("CHECKSUM");
                    //Raman: reading SMS access value
                    m_bSMSAccess = reader.GetBoolean("IS_SMS_USER");
                    //skhare7
                    m_bUPSAccess = reader.GetBoolean("IS_USRPVG_ACCESS");
                    //Set up Manager's Email
                    if (m_ManagerId != 0)
                    {
                        /*Following lines of code will ensure if DB contains Null for manager Email address,
                         * m_ManagerEmail field will be set to empty string.Added by Tanuj Narula on 20/5/2004 
                        */
                        managerEmailAddress = DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(m_iClientId), String.Format("SELECT EMAIL_ADDR FROM USER_TABLE WHERE USER_ID={0}", m_ManagerId));

                        //Raman Bhatia: managerEmailAddress has to be checked with DBNull instead of null
                        if (managerEmailAddress is DBNull)
                        {
                            m_ManagerEmail = string.Empty;
                        }
                        else
                        {
                            m_ManagerEmail = (string)managerEmailAddress;
                        }//else

                    }//if    
                }//while

            }//using


            m_DataChanged = false;
            if (m_bEnforceCheckSum)
            {
                if (!ChecksumMatches())
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    throw new Riskmaster.ExceptionTypes.InvalidChecksumException(Globalization.GetString("User.LoadData.InvalidUserInfo",m_iClientId));
            }//if
			
		}//method: Load()


		/// <summary>
		/// Riskmaster.Security.Save check for the flag true, than gets a DbWriter object using DbFactory instance and invokes SaveData().
		/// </summary>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
		public void Save()
		{
            if (!m_DataChanged)
            {
                return;
            } // if


			DbWriter writer=DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(m_iClientId));
			writer.Connection = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
			writer.Connection.Open();
			SaveData(writer);
			writer.Connection.Close();
		}

		/// <summary>
		/// Riskmaster.Security.SaveData check for the flag is true, this function updates the user record 
		/// in the database else creates a new one using writer.
		/// </summary>
		/// <param name="writer">The native writer to wrap</param>
		/// <returns>None</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
		public virtual void SaveData(DbWriter writer)
		{
			// Data Clean - No Save Required.
			if (!m_DataChanged)
				return;
			
			//Make Sure Writer is Clean for our Use - Empty the field, table collections etc.
			writer.Reset(true);

			if(m_UserId==0)
			{
				// Get the next unique ID
				m_UserId = writer.Connection.ExecuteInt("SELECT MAX(USER_TABLE.USER_ID) FROM USER_TABLE");
				if(m_UserId ==0)
					m_UserId=2;
				else
					m_UserId++;
			}
			else
				writer.Where.Add(String.Format("USER_ID={0}", m_UserId));

			writer.Tables.Add("USER_TABLE");
			writer.Fields.Add("USER_ID",m_UserId);
			writer.Fields.Add("LAST_NAME",m_LastName);
			writer.Fields.Add("FIRST_NAME",m_FirstName);
			writer.Fields.Add("ADDRESS1",m_Address1);
			writer.Fields.Add("ADDRESS2",m_Address2);
			writer.Fields.Add("CITY",m_City);
			writer.Fields.Add("STATE_ABBR",m_State);
			writer.Fields.Add("COUNTRY",m_Country);
			writer.Fields.Add("ZIP_CODE",m_ZipCode);
			writer.Fields.Add("EMAIL_ADDR",m_Email);
			writer.Fields.Add("MANAGER_ID",m_ManagerId);
			writer.Fields.Add("HOME_PHONE",m_HomePhone);
			writer.Fields.Add("OFFICE_PHONE",m_OfficePhone);
			writer.Fields.Add("TITLE",m_Title);
			writer.Fields.Add("NLS_CODE",m_NlsCode);
			writer.Fields.Add("COMPANY_NAME",m_CompanyName);
			writer.Fields.Add("CHECKSUM",GetCheckSum());
            //gagnihotri MITS 11995 Changes made for Audit table
            writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
            //Raman Bhatia: writing SMS access flag
            writer.Fields.Add("IS_SMS_USER", Conversion.ConvertBoolToInt(m_bSMSAccess, m_iClientId));
            //skhare7
			// start : atavaragiri MITS 28449:for setting IS_USRPVG_ACCESS to -1 or 0 //
           //   writer.Fields.Add("IS_USRPVG_ACCESS", Conversion.ConvertBoolToInt(m_bUPSAccess));
            writer.Fields.Add("IS_USRPVG_ACCESS", Conversion.ConvertBoolToInt(m_bUPSAccess, m_iClientId).Equals(1) ? -1 : 0);
			// end:atavaragiri
			writer.Execute();
		}

		/// <summary>
		/// Riskmaster.Security.UserId property accesses the value of the database field USER_ID of USER_TABLE.
		/// </summary>
		public int UserId
		{
			get 
            {
                return m_UserId;
            }
			set
			{
				m_UserId=value;
				m_DataChanged=true;
			}
		}

		/// <summary>
		/// Riskmaster.Security.UserId property accesses the value of the database field First Name of USER_TABLE.
		/// </summary>
		public string FirstName
		{
			get {return m_FirstName;}
			set
			{
				m_FirstName=value;
				m_DataChanged=true;
			}
		}
	
		public string LastName
		{
			get {return m_LastName;}
			set
			{
				m_LastName=value;
				m_DataChanged=true;
			}
		}

		public string Address1
		{
			get {return m_Address1;}
			set
			{
				m_Address1=value;
				m_DataChanged=true;
			}
		}

		public string Address2
		{
			get {return m_Address2;}
			set
			{
				m_Address2=value;
				m_DataChanged=true;
			}
		}

		public string City
		{
			get {return m_City;}
			set
			{
				m_City=value;
				m_DataChanged=true;
			}
		}

		public string State
		{
			get {return m_State;}
			set
			{
				m_State=value;
				m_DataChanged=true;
			}
		}

		public string Country
		{
			get {return m_Country;}
			set
			{
				m_Country=value;
				m_DataChanged=true;
			}
		}

		
		public string ZipCode
		{
			get {return m_ZipCode;}
			set
			{
				m_ZipCode=value;
				m_DataChanged=true;
			}
		}

	
		public string Email
		{
			get {return m_Email;}
			set
			{
				m_Email=value;
				m_DataChanged=true;
			}
		}

		public string HomePhone
		{
			get {return m_HomePhone;}
			set
			{
				m_HomePhone=value;
				m_DataChanged=true;
			}
		}

		public string OfficePhone
		{
			get {return m_OfficePhone;}
			set
			{
				m_OfficePhone=value;
				m_DataChanged=true;
			}
		}

		public string Title
		{
			get {return m_Title;}
			set
			{
				m_Title=value;
				m_DataChanged=true;
			}
		}

		
		public int NlsCode
		{
			get {return m_NlsCode;}
			set
			{
				m_NlsCode=value;
				m_DataChanged=true;
			}
		}

		/// <summary>
		/// Riskmaster.Security.UserId property accesses the value of the database field Company Name of USER_TABLE.
		/// </summary>
		public string CompanyName
		{
			get {return m_CompanyName;}
			set
			{
				m_CompanyName=value;
				m_DataChanged=true;
			}
		}

		public User Manager
		{
			get
			{
				return null;
			}
		}
		
		
		public int ManagerId
		{
			get { return m_ManagerId;}
			set
			{
				m_ManagerId=value;
				m_DataChanged=true;
			}
				
		}	
	
		
		public string ManagerEmail
		{
			get { return m_ManagerEmail;}
			set
			{
				m_ManagerEmail=value;
				m_DataChanged=true;
			}
		
		}

		
		public string Dsn
		{
			get {return m_Dsn;}
			set {m_Dsn=value;}
		}

        /// <summary>
        /// Gets and sets the DSN ID
        /// for the currently authenticated User
        /// </summary>
        public int DsnID
        {
            get{return m_DsnID;}
            set{m_DsnID = value;}
        } // property DsnID

        

		public bool EnforceCheckSum
		{
			get
			{
				return m_bEnforceCheckSum;
			}
			set
			{
				m_bEnforceCheckSum = value;
			}
		}

        /// <summary>
        /// Gets and Sets the SMS Access property
        /// </summary>
        public bool SMSAccess
        {
            get
            {
                return m_bSMSAccess;
            }
            set
            {
                m_bSMSAccess = value;
            }
        }

         /// <summary>
        /// Gets and Sets the User Pri setup Access property: skhare7
        /// </summary>
        public bool UPSAccess
        {
            get
            {
                return m_bUPSAccess;
            }
            set
            {
                m_bUPSAccess = value;
            }
        }
       
        /// <summary>
        /// Gets the CheckSum for the object
        /// </summary>
        /// <returns>string containing the CheckSum for the object</returns>
        internal string GetCheckSum()
        {
            byte[] byteArr = new byte[24];
            ushort iHI, iLO;

            byteArr[0] = Conversion.GetAscii(m_LastName);
            byteArr[1] = Conversion.GetAscii(m_FirstName);
            byteArr[2] = Conversion.GetAscii(m_Email);
            byteArr[3] = Conversion.GetAscii(m_Address1);
            byteArr[4] = Conversion.GetAscii(m_Address2);
            byteArr[5] = Conversion.GetAscii(m_City);
            byteArr[6] = Conversion.GetAscii(m_HomePhone);
            byteArr[7] = Conversion.GetAscii(m_OfficePhone);
            byteArr[8] = Conversion.GetAscii(m_ZipCode);
            byteArr[9] = Conversion.GetAscii(m_Country);
            byteArr[10] = Conversion.GetAscii(m_State);
            byteArr[11] = Conversion.GetAscii(m_Title);

            // Get Manager ID
            Conversion.GetHILOWord(m_ManagerId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[12], out byteArr[13]);
            Conversion.GetHILOByte(iHI, out byteArr[14], out byteArr[15]);

            // Get USER ID
            Conversion.GetHILOWord(m_UserId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[16], out byteArr[17]);
            Conversion.GetHILOByte(iHI, out byteArr[18], out byteArr[19]);

            // And NLS Code
            Conversion.GetHILOWord(m_NlsCode, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[20], out byteArr[21]);
            Conversion.GetHILOByte(iHI, out byteArr[22], out byteArr[23]);

            return RMCryptography.ComputeHashAsString(byteArr);
        }
		
		/// <summary>
		/// Riskmaster.Security.ChecksumMatches internal method compares the stored checksum attribute that with a newly computed hash value for the user attributes
		/// </summary>
		/// <returns>It returns computed hash value.</returns>
		internal bool ChecksumMatches()
		{
            string sHash = string.Empty;
			sHash = GetCheckSum();
			return (sHash == m_CheckSum);
		}
      }

    public partial class User : IIdentity, IPrincipal
    {
        #region IIdentity Members

        /// <summary>
        /// Gets the AuthenticationType used to authenticate the user
        /// </summary>
        public string AuthenticationType
        {
            get 
            { 
                //TODO: Pass the authentication type as part of the selected Membership Provider
                return "Forms";
            }
        }

        /// <summary>
        /// Gets whether or not the user has been authenticated against the system
        /// </summary>
        public bool IsAuthenticated
        {
            get 
            {
                //TODO: Determine if the user has been logged out of the system
                return true; 
            }
        }

        /// <summary>
        /// Gets the user name
        /// </summary>
        public string Name
        {
            get 
            {
                return this.m_sUserName;
            }
        }

        #endregion



        #region IPrincipal Members


        /// <summary>
        /// Gets the Identity for the User
        /// </summary>
        public IIdentity Identity
        {
            get { return new GenericIdentity(this.Name); }
        }

        /// <summary>
        /// Determines whether the user is a member of a particular role
        /// </summary>
        /// <param name="role">string containing the name of the group name</param>
        /// <returns>boolean indicating whether or not the user is a member 
        /// of the specified grouop</returns>
        public bool IsInRole(string role)
        {
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            bool blnIsInRole = false;

            strSQL.Append("SELECT GROUP_NAME ");
            strSQL.Append("FROM USER_GROUPS UG ");
            strSQL.Append("INNER JOIN USER_MEMBERSHIP UM ");
            strSQL.Append("ON UG.GROUP_ID = UM.GROUP_ID ");
            strSQL.Append("WHERE UM.USER_ID = ~USERID~ ");
            strSQL.Append("AND UG.GROUP_NAME = ~GROUPNAME~");

            //Obtain a handle to the Riskmaster Database
            RiskmasterDatabase rmDb = new RiskmasterDatabase(this.DsnID, m_iClientId);
            
            strDictParams.Add("USERID", this.UserId.ToString());
            strDictParams.Add("GROUPNAME", role);

            //TODO: Verify that the actual/correct Riskmaster Database Connection string is retrieved
            object objGroupName = DbFactory.ExecuteScalar(rmDb.ConnectionString, strSQL.ToString(), strDictParams);

            //If the object value is not empty
            if (! string.IsNullOrEmpty(objGroupName.ToString()))
            {
                blnIsInRole = true;
            } // if

            return blnIsInRole;
        }
        /// <summary>
        /// Get the Login Name of the Manager
        /// </summary>
        /// <param name="userManagerId"></param>
        /// <param name="dsnId"></param>
        /// <returns></returns>
        public string GetManagerLoginName(int userManagerId, int dsnId)
        { 
           string sManagerLoginName = string.Empty ;

            using (DbReader reader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(m_iClientId), string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}", userManagerId, dsnId)))
            {
                while (reader.Read())
                {
                    sManagerLoginName = reader.GetString("LOGIN_NAME");
                }
            }

            return sManagerLoginName;
        }
        #endregion
    }//class

    

}
