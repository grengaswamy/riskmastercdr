﻿using System.Runtime.InteropServices;
using System.Configuration;
using System;
using System.Reflection;
using System.Collections.Generic;
using Riskmaster.Db;
using Riskmaster.Common;

namespace Riskmaster.Security
{
    [ComVisible(false)]
    /// <summary>
    /// Manages connectivity to the Security Database for the .Net Application
    /// </summary>
    public class SecurityDatabaseManager
    {
        internal static string AssemblyPath
        {
            get
            {
                Assembly currentAssembly = Assembly.GetExecutingAssembly();

                string strAssemblyLocation = currentAssembly.Location;

                return strAssemblyLocation;
            }//get
        }//internal

        /// <summary>
        /// Assigns the DSN for the Security Database
        /// </summary>
        /// to the RISKMASTER Security Database</param>
        /// Making this function public as this can be used from outside the riskmaster solution.
        public static void CreateSecurityDatabase()
        {
            string strSecConnString = string.Empty;
            SecurityDatabase.Dsn = string.Empty;
            try
            {
                ConnectionStringSettings connSettings = ConfigurationManager.ConnectionStrings["RMXSecurity"];

                if (connSettings != null)
                {
                    strSecConnString = connSettings.ConnectionString;
                }//if
                //Try an alternative to retrieve the connection string based on the current assembly path
                else
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(AssemblyPath);

                    ConnectionStringsSection connStrings = config.GetSection("connectionStrings") as ConnectionStringsSection;

                    strSecConnString = connStrings.ConnectionStrings["RMXSecurity"].ConnectionString;
                }//else


                //Set the security database connection string
                SecurityDatabase.Dsn = strSecConnString;
            }//try
            catch (Exception ex)
            {
                //SecurityDatabase.Dsn = string.Empty;
               // throw new ConfigurationErrorsException("Security database connection string could not be found.  Please verify your settings.", ex);
            }//catch
        } // method: CreateSecurityDatabase

        public static string GetSecDsn(int p_iClientId)
        {
            if (p_iClientId == 0) return SecurityDatabase.Dsn;//ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
            string strSecConnString = Riskmaster.Cache.ConfigurationInfo.GetSecurityConnectionString(p_iClientId);
            try
            {
                if (string.IsNullOrEmpty(strSecConnString))
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(AssemblyPath);
                    ConnectionStringsSection connStrings = config.GetSection("connectionStrings") as ConnectionStringsSection;
                    string sBaseConnString = connStrings.ConnectionStrings["rmATenantSecurity"].ConnectionString;
                    Dictionary<string, int> objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", p_iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", "RMXSecurity", "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    strSecConnString = Convert.ToString(objClientConnString);
                }
            }

            catch (Exception ex)
            {
                throw new ConfigurationErrorsException(Globalization.GetString("SecurityConfig.Error", p_iClientId), ex);
            }
            return strSecConnString;
        }

        /// <summary>
        /// Assigns the DSN for the Security Database
        /// </summary>
        /// <param name="strSecConnString">string containing the database connection string
        /// to the RISKMASTER Security Database</param>
        /// Making this function public as this is used from taskmanager service solution.
        public static void CreateSecurityDatabase(string strSecConnString)
        {

            if (string.IsNullOrEmpty(strSecConnString))
            {
                throw new ConfigurationErrorsException("Security database connection string could not be found.  Please verify your settings.");
            }//if
            else
            {
                SecurityDatabase.Dsn = strSecConnString;
            }//else
        } // method: CreateSecurityDatabase

        

    }
}
