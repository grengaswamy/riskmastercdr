﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Security
{
    public class CommonSecurity
    {
        public static bool CheckRestrictedClaim(int iClaimID, string p_sConnectionString, int p_userid, ref bool bRestrictedClaimFlagValue)
        {
            return CheckRestrictedClaim(iClaimID, p_sConnectionString, p_userid, ref bRestrictedClaimFlagValue, 0);
        }
        // Neha Goel MITS# 33409 - Start Moved to common functions from all files
        /// <summary>
        /// Checks if a claim is a Restricted Claim or not for the logged in user
        /// </summary>
        /// <param name="iClaimID"></param>
        /// <param name="bRestrictedClaimFlagValue"></param>
        /// <param name="p_sConnectionString"></param>
        /// <param name="p_userid"></param>
        /// <returns></returns>
        public static bool CheckRestrictedClaim(int iClaimID, string p_sConnectionString, int p_userid, ref bool bRestrictedClaimFlagValue, int iClientId)
        {
            bRestrictedClaimFlagValue = false;
            return true;
            /* DbConnection objSecConn = null;
               //LocalCache objCache = null;
               DbConnection objConn = DbFactory.GetDbConnection(p_sConnectionString);
               objConn.Open();
               //objCache = new LocalCache(m_sConnectionString);
               string sCustSQL = string.Empty;
               string sAdjSQL = string.Empty;
               string sResSQL = string.Empty;
               string sAuthUsersSQL = string.Empty;
               string sSecConnString = string.Empty;
               sSecConnString = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId);
               objSecConn = DbFactory.GetDbConnection(sSecConnString);
               objSecConn.Open();
               int iRestrictCode = 0;
               bool Cancel = false;
               bool bSuccess = false;

               try
               {
                   //Fetching Restricted Claim Status                
                   sCustSQL = "SELECT RESTRICTED_CLAIM_FLAG FROM CLAIM WHERE CLAIM_ID = " + iClaimID;
                   iRestrictCode = Conversion.CastToType<int>(objConn.ExecuteString(sCustSQL), out bSuccess); //Conversion.ConvertStrToInteger(objConn.ExecuteString(sCustSQL));

                   //Checking if Claim is a Restricted Claim
                   if (iRestrictCode != 0)
                   {
                       bRestrictedClaimFlagValue = true;

                       ArrayList alAuthUsers = new ArrayList();
                       string sAdjuster = string.Empty;
                       //Fetching all Adjuster's RM Login for the selected Claim
                       sAdjSQL = "Select RM_USER_ID from ENTITY INNER JOIN CLAIM_ADJUSTER ON ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID WHERE CLAIM_ADJUSTER.CLAIM_ID = " + iClaimID;
                       using (DbReader objDBReader = objConn.ExecuteReader(sAdjSQL))
                       {
                           while (objDBReader.Read())
                           {
                               alAuthUsers.Add(Conversion.ConvertObjToStr(objDBReader.GetValue(0)));
                               if (string.IsNullOrEmpty(sAdjuster))
                               {
                                   sAdjuster = (Conversion.ConvertObjToStr(objDBReader.GetValue(0)));
                               }
                               else
                               {
                                   sAdjuster = sAdjuster + "," + (Conversion.ConvertObjToStr(objDBReader.GetValue(0)));
                               }
                           }
                       }

                       // Check if Logged in user is not a authorized to access the claim
                       if (alAuthUsers.Count > 0 && alAuthUsers.Contains(p_userid.ToString()))
                       {
                           Cancel = true;
                       }
                       else
                       {

                           //Fetching all users defined in CLM_RSTRCTD_USR_OVRD
                           ArrayList alAuthUsersInResTable = new ArrayList();
                           sResSQL = "Select USER_ID from CLM_RSTRCTD_USR_OVRD WHERE CLAIM_ID = 0 OR CLAIM_ID =" + iClaimID;
                           using (DbReader objDBReader = objConn.ExecuteReader(sResSQL))
                           {
                               while (objDBReader.Read())
                               {
                                   alAuthUsersInResTable.Add(Conversion.ConvertObjToStr(objDBReader.GetValue(0)));
                               }
                           }

                           if (alAuthUsersInResTable.Count > 0 && alAuthUsersInResTable.Contains(p_userid.ToString()))
                           {
                               Cancel = true;
                           }
                           else if (!string.IsNullOrEmpty(sAdjuster))
                           {

                               sAuthUsersSQL = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID IN (" + sAdjuster + ")";
                               using (DbReader objDBReader = objSecConn.ExecuteReader(sAuthUsersSQL))
                               {
                                   while (objDBReader.Read())
                                   {
                                       alAuthUsers.Add(Conversion.ConvertObjToStr(objDBReader.GetValue(0)));
                                   }
                               }

                               if (alAuthUsers.Count > 0 && alAuthUsers.Contains(p_userid.ToString()))
                               {
                                   Cancel = true;
                               }
                           }
                       }
                   }
                   else
                   {
                       Cancel = true;
                       bRestrictedClaimFlagValue = false;
                   }

                   return Cancel;
               }
               catch (RMAppException p_objExp)
               {
                   throw p_objExp;
               }
               catch (Exception p_objExp)
               {
                   throw new RMAppException(Globalization.GetString("CommonFunctions.CheckRestrictedClaim"),
                       p_objExp);
               }
               finally
               {
                   if (objConn != null)
                   {
                       if (objConn.State == System.Data.ConnectionState.Open)
                       {
                           objConn.Close();
                       }
                       objConn.Dispose();
                   }
                   if (objSecConn != null)
                   {
                       objSecConn.Dispose();
                   }
                   //if (objCache != null)
                   //{
                   //    objCache.Dispose();
                   //}
               }
           }
           // MITS# 33409 - End
           */
        }
    }
}
