﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Riskmaster.Security.Encryption;
using System.Reflection;

namespace Riskmaster.Security
{

    /// <summary>
    /// Provides a set of internal utility functions
    /// for managing common actions needed for security
    /// </summary>
    internal class UtilityFunctions
    {

        /// <summary>
        /// Gets the CodeAccess public key used for Security Permissions
        /// </summary>
        internal const string CODE_ACCESS_PUBLICKEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
        
        /// <summary>
        /// Gets a connection string delimter
        /// </summary>
        public static string ConnectionStringDelimiter 
        {
            get
            {
                const string DELIMITER = ";";
                return DELIMITER;
            }//get
        }//property: ConnectionStringDelimiter

        /// <summary>
        /// Checks whether the particular string is an empty string
        /// </summary>
        /// <param name="strValue">string to check and compare for empty or null values</param>
        /// <returns>boolean indicating whether or not the string is empty or null</returns>
        internal static bool IsEmptyString(string strValue)
        {
            if (string.IsNullOrEmpty(strValue))
            {
                return true;
            } // if
            else
            {
                return false;
            } // else
        } // method: IsEmptyString


        [Obsolete("This method should not be used since these settings should be stored in the database.")]
        /// <summary>
        /// Retrieves the appropriate Riskmaster.config settings from Riskmaster.config
        /// </summary>
        /// <returns>StringDictionary containing possible Riskmaster.config Document Path settings</returns>
        public static StringDictionary PopulateRMConfigSettings()
        {
            //StringDictionary dictRMConfigSettings = new StringDictionary();

            //if (RMConfigurator.NamedNode("OverrideDocumentPath") != null)
            //{
            //    if (RMConfigurator.Value("OverrideDocumentPath").Trim() == "true")
            //    {
            //        dictRMConfigSettings.Add("DocumentPath", RMConfigurator.Value("StorageDestination").Trim());
            //        dictRMConfigSettings.Add("GlobalDocumentPath", RMConfigurator.Value("StorageDestination").Trim());

            //        if (RMConfigurator.Value("StorageType").Trim() == "FileSystem")
            //        {
            //            dictRMConfigSettings.Add("DocPathType", "0");
            //        }//if
            //        else
            //        {
            //            dictRMConfigSettings.Add("DocPathType", "1");
            //        }//else
            //    }//if
            //}//if

            //return dictRMConfigSettings;
            throw new NotImplementedException();
        }//method: PopulateRMConfigSettings()

        /// <summary>
        /// Builds a database/DSN connection string given the specified parameters
        /// </summary>
        /// <param name="arrConnStrParams">generic string list containing all of the 
        /// connection string parameters</param>
        /// <returns>complete database connection string</returns>
        public static string BuildConnectionString(List<string> arrConnStrParams)
        {
            StringBuilder strBldrConnStr = new StringBuilder();

            foreach (string strConnParam in arrConnStrParams)
            {
                if (!string.IsNullOrEmpty(strConnParam))
                {
                    strBldrConnStr.Append(strConnParam);
                }//method
            }//foreach

            return strBldrConnStr.ToString();
        }//method: BuildConnectionString()

        /// <summary>
        /// Builds a connection string given the credentials 
        /// from the Data Source table in the RISKMASTER Security database
        /// </summary>
        /// <param name="strBaseConnStr">string containing the base connection string to the database</param>
        /// <param name="username">string containing the user name for the database</param>
        /// <param name="password">string containing the password for the database</param>
        /// <returns>fully qualified database connection string that can be used in tools and utilities</returns>
        public static string BuildConnectionString(string strBaseConnStr, string username, string password)
        {
            //Build the resultant connection string prior to adding it to the list of selections
            List<string> arrConnStrParams = new List<string>();

            arrConnStrParams.Add(strBaseConnStr);
            arrConnStrParams.Add(string.Format("UID={0};", RMCryptography.DecryptString(username)));
            arrConnStrParams.Add(string.Format("PWD={0};", RMCryptography.DecryptString(password)));

            return BuildConnectionString(arrConnStrParams);
        } // method: BuildConnectionString


        /// <summary>
        /// Creates a basic DSN-based connection string
        /// </summary>
        /// <param name="dsn">string containing the name of the DSN</param>
        /// <param name="username">string containing the name of the database user</param>
        /// <param name="password">string containing the name of the database password</param>
        /// <returns>string containing the DSN-based connection string</returns>
        public static string BuildDSNConnectionString(string dsn, string username, string password)
        {
            //Builds a basic DSN based connection string
            StringBuilder strBldrDSN = new StringBuilder();

            //Verify that the DSN does not already contain the text "DSN"
            if (!dsn.Contains(DSNRegistryKeys.DSN))
            {
                strBldrDSN.Append(DSNRegistryKeys.DSN);
                strBldrDSN.Append(dsn);
            } // if
            else
            {
                strBldrDSN.Append(dsn);
            } // else

            strBldrDSN.Append(UtilityFunctions.ConnectionStringDelimiter);
            strBldrDSN.Append(DSNRegistryKeys.UID + username);
            strBldrDSN.Append(UtilityFunctions.ConnectionStringDelimiter);
            strBldrDSN.Append(DSNRegistryKeys.PWD + password);
            strBldrDSN.Append(UtilityFunctions.ConnectionStringDelimiter);

            return strBldrDSN.ToString();
        }
 
        #region User Methods
        /// <summary>
        /// Compares the properties of two objects
        /// through the use of Reflection
        /// </summary>
        /// <typeparam name="T">Generic Type for a class instance</typeparam>
        /// <param name="objCompare1">object class instance</param>
        /// <param name="objCompare2">object class instance for comparison</param>
        /// <returns>boolean indicating whether or not the properties for 
        /// each object exactly match</returns>
        public static bool CompareObjects<T>(T objCompare1, T objCompare2)
        {
            int intCompareValue = 0;
            bool blnAreEqual = true;

            //Get type information for the class instance
            Type t = objCompare1.GetType();

            //Use Reflection to obtain all of the available properties on the class
            PropertyInfo[] objProps = t.GetProperties();

            //Iterate through all of the properties in the class
            foreach (PropertyInfo objProp in objProps)
            {
                //Cast to IComparable to allow comparison of each property value
                IComparable valx = objProp.GetValue(objCompare1, null) as IComparable;

                if (valx == null)
                {
                    continue;
                } // if

                //Obtain the property for the second class instance
                object valy = objProp.GetValue(objCompare2, null);

                //Compare the 2 values to verify they are equal
                intCompareValue = valx.CompareTo(valy);

                //Determine if the classes are equal
                if (intCompareValue !=0)
                {
                    blnAreEqual = false;
                    break;
                } // if

            } // foreach

            //return the boolean value for comparison of the objects
            return blnAreEqual;
        }//method: CompareObjects

        #endregion







    }
}
