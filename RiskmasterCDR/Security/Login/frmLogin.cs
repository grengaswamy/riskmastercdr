using System.Drawing;

namespace Riskmaster.Security
{
	/// <summary>
	/// Riskmaster.Security.frmLogin is used to login into the system by entering userid,password and users database.
	/// </summary>
	public unsafe class frmLogin : System.Windows.Forms.Form
	{

		private LoginDlgInfo* m_DialogInfo;
		private System.Windows.Forms.TextBox txtUser;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.ComboBox lstDSN;
		private System.Windows.Forms.Label lblDSN;
		private System.Windows.Forms.PictureBox picLogo;
		private System.Windows.Forms.Label lblMultiDSN;
        
        private const int VERTICAL_SIZE_OF_DSN_SELECT = 27;

        /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Riskmaster.Security.frmLogin is the constructor without parameter which initialize the components.
		/// </summary>
		internal  frmLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Riskmaster.Security.frmLogin is the constructor with parameter which passes the objects of the struct LoginDlgInfo. 
		/// It takes the username , password from login form and passes it to the object DialogInfo and also shows the data source 
		/// associated with the user. 
		/// </summary>
		/// <param name="DialogInfo">Pinter to LoginDlgInfo</param>
		internal frmLogin(LoginDlgInfo* DialogInfo)
		{
			string[] DSNs = null;
			
			//BSB 04.22.2003
			// There are significant impediments to passing an object in the LoginDlgInfo struct.
			// Instead lets create a new one and always use default security DSN.  (Even it coming in from MemMap.)
            Login objLogin = new Login(DialogInfo->ClientId);
			
			int i;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			// If we have this either from the Memory Map or the Registry - apply it.
			this.txtUser.Focus();
			if(this.txtUser.Text!="")
				this.txtPassword.Focus();
			unsafe
			{
				this.txtUser.Text =DialogInfo->szUserName;
				this.txtPassword.Text =DialogInfo->szPassword;
			}
			
			//Manage Login Screen State
			if (!DialogInfo->bSecureLogin) //Always show a DSN List (not always "per user" though.) 
			{
				///if(this.txtUser.Text!="")
				//	unsafe {DSNs = objLogin.GetUserDatabases(txtUser.Text,txtPassword.Text);}
				//else
				unsafe {DSNs = objLogin.GetDatabases();}
				this.lstDSN.Visible = true;
				this.lblDSN.Visible = true;
			}
			else  //Conditionally show a DSN List if available (always "per user")
				if(this.txtUser.Text!="")
			{
				this.txtUser.Enabled = false;
				this.txtPassword.Enabled = false;
				//this.lblMultiDSN.Visible = true;
				this.lstDSN.Visible = true;
				this.lblDSN.Visible = true;
				this.lstDSN.Focus();
				unsafe {DSNs = objLogin.GetUserDatabases(txtUser.Text,txtPassword.Text);}
			}
			else  //Don't show DSN input yet.
			{
				this.picLogo.Top = 80;
                this.Height = (this.Height - VERTICAL_SIZE_OF_DSN_SELECT);	
			}

			//Fill DSN List.
            if (DSNs != null && DSNs.Length != 0) //BSB 06.01.2007 If No Items in the list the AddRange throws an exception.
			    this.lstDSN.Items.AddRange(DSNs);
			
			//Default the DSN selection
			unsafe { i = this.lstDSN.FindStringExact(  DialogInfo->szDSN);}
			if (i >=0)
				this.lstDSN.SelectedIndex = i;

			//Save this off for later use in cmdOK event.
			m_DialogInfo = DialogInfo;

            //Get the logo from the Resource file for the assembly
            this.picLogo.Image = GetLogo();

			//Lock Size
			this.MaximumSize = this.MinimumSize = this.Size;
		}

        /// <summary>
        /// Retrieves the log from the Resource file
        /// </summary>
        /// <returns>returns the logo from the Global resource file</returns>
        private Image GetLogo()
        {
            return Global.oemlogo as Image;
        }//method: GetLogo()

		/// <summary>
		/// Riskmaster.Security.Dispose clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDSN = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lstDSN = new System.Windows.Forms.ComboBox();
            this.lblMultiDSN = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUser
            // 
            resources.ApplyResources(this.txtUser, "txtUser");
            this.txtUser.Name = "txtUser";
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.Name = "txtPassword";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblDSN
            // 
            resources.ApplyResources(this.lblDSN, "lblDSN");
            this.lblDSN.Name = "lblDSN";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cmdOK
            // 
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.cmdOK, "cmdOK");
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.cmdCancel, "cmdCancel");
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // picLogo
            // 
            resources.ApplyResources(this.picLogo, "picLogo");
            this.picLogo.Name = "picLogo";
            this.picLogo.TabStop = false;
            // 
            // lstDSN
            // 
            resources.ApplyResources(this.lstDSN, "lstDSN");
            this.lstDSN.Name = "lstDSN";
            // 
            // lblMultiDSN
            // 
            resources.ApplyResources(this.lblMultiDSN, "lblMultiDSN");
            this.lblMultiDSN.Name = "lblMultiDSN";
            // 
            // frmLogin
            // 
            this.AcceptButton = this.cmdOK;
            resources.ApplyResources(this, "$this");
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Dialog;
            this.CancelButton = this.cmdCancel;
            this.ControlBox = false;
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.lblMultiDSN);
            this.Controls.Add(this.lstDSN);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblDSN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUser);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// Private utility Riskmaster.Security.cmdOK_Click allows he user to enter into the application. 
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event argument</param>
		private unsafe void cmdOK_Click(object sender, System.EventArgs e)
		{
            // BSB 06.01.2007 Catch No DSN selected case - can happen if bSecure and no username
            // was provided yet.  In that case just save off the empty string and let the calling logic
            // sort out the proper control flow.  Previously was throwing an exception.
            if (this.lstDSN.Items.Count > 0)
                m_DialogInfo->szDSN = this.lstDSN.SelectedItem.ToString();
            else
                m_DialogInfo->szDSN = "";

			m_DialogInfo->szUserName =  this.txtUser.Text;
			m_DialogInfo->szPassword = this.txtPassword.Text;
			//Done in Designer
			//this.DialogResult =  DialogResult.OK;
			this.Close();
		}

		/// <summary>
		/// private utility Riskmaster.Security.cmdCancel_Click cancel the operation by user.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event argument</param>
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			//Done in Designer
			//this.DialogResult =  DialogResult.Cancel;
			this.Close();
		}
		
		private void frmLogin_Load(object sender, System.EventArgs e)
		{
		
		}


	}
}
