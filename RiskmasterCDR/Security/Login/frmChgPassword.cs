using System.Drawing;
using System.Windows.Forms;
using Riskmaster.Common;

namespace Riskmaster.Security
{
	/// <summary>
	/// Riskmaster.Security.frmChgPassword class is used to change the existing pasword.
	/// </summary>
	public class frmChgPassword : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox picLogo;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblNew2;
		private System.Windows.Forms.TextBox txtNew1;
		private System.Windows.Forms.TextBox txtOrig;
		private System.Windows.Forms.TextBox txtNew2;
		
			
		internal string m_OldPassword="";
		private string m_NewPassword ="";
        private int m_iClientId = 0;

		/// <summary>
		/// Riskmaster.Security.NewPassword is the readonly property, gets the new password.
		/// </summary>
		
		//Populated after form is shown...
		public string NewPassword
		{
			get
			{
				return m_NewPassword;
			}
		}
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Riskmaster.Security.frmChgPassword constructor with parameters.
		/// </summary>
		/// <param name="sOldPassword">Old Password</param>
		/// <param name="sDSN">DSN Name</param>
		/// <param name="sUsername">User Name</param>

        public frmChgPassword(string sOldPassword, string sDSN, string sUsername, int p_iClientId)
		{
			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			m_OldPassword = sOldPassword;
            m_iClientId = p_iClientId;
			//Try to pick up any custom Logo if present.
			if (System.IO.File.Exists(System.Environment.CurrentDirectory + "\\myndclaim.bmp"))
				this.picLogo.Image = Image.FromFile(System.Environment.CurrentDirectory + "\\myndclaim.bmp");
			
			//Lock Size
			this.MaximumSize = this.MinimumSize = this.Size;

		}

		/// <summary>
		/// Riskmaster.Security.Dispose clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmChgPassword));
			this.picLogo = new System.Windows.Forms.PictureBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.cmdOK = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.lblNew2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtNew1 = new System.Windows.Forms.TextBox();
			this.txtOrig = new System.Windows.Forms.TextBox();
			this.txtNew2 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// picLogo
			// 
			this.picLogo.AccessibleDescription = ((string)(resources.GetObject("picLogo.AccessibleDescription")));
			this.picLogo.AccessibleName = ((string)(resources.GetObject("picLogo.AccessibleName")));
			this.picLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picLogo.Anchor")));
			this.picLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picLogo.BackgroundImage")));
			this.picLogo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picLogo.Dock")));
			this.picLogo.Enabled = ((bool)(resources.GetObject("picLogo.Enabled")));
			this.picLogo.Font = ((System.Drawing.Font)(resources.GetObject("picLogo.Font")));
			this.picLogo.Image = ((System.Drawing.Bitmap)(resources.GetObject("picLogo.Image")));
			this.picLogo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picLogo.ImeMode")));
			this.picLogo.Location = ((System.Drawing.Point)(resources.GetObject("picLogo.Location")));
			this.picLogo.Name = "picLogo";
			this.picLogo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picLogo.RightToLeft")));
			this.picLogo.Size = ((System.Drawing.Size)(resources.GetObject("picLogo.Size")));
			this.picLogo.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picLogo.SizeMode")));
			this.picLogo.TabIndex = ((int)(resources.GetObject("picLogo.TabIndex")));
			this.picLogo.TabStop = false;
			this.picLogo.Text = resources.GetString("picLogo.Text");
			this.picLogo.Visible = ((bool)(resources.GetObject("picLogo.Visible")));
			// 
			// cmdCancel
			// 
			this.cmdCancel.AccessibleDescription = ((string)(resources.GetObject("cmdCancel.AccessibleDescription")));
			this.cmdCancel.AccessibleName = ((string)(resources.GetObject("cmdCancel.AccessibleName")));
			this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdCancel.Anchor")));
			this.cmdCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdCancel.BackgroundImage")));
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdCancel.Dock")));
			this.cmdCancel.Enabled = ((bool)(resources.GetObject("cmdCancel.Enabled")));
			this.cmdCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdCancel.FlatStyle")));
			this.cmdCancel.Font = ((System.Drawing.Font)(resources.GetObject("cmdCancel.Font")));
			this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
			this.cmdCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdCancel.ImageAlign")));
			this.cmdCancel.ImageIndex = ((int)(resources.GetObject("cmdCancel.ImageIndex")));
			this.cmdCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdCancel.ImeMode")));
			this.cmdCancel.Location = ((System.Drawing.Point)(resources.GetObject("cmdCancel.Location")));
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdCancel.RightToLeft")));
			this.cmdCancel.Size = ((System.Drawing.Size)(resources.GetObject("cmdCancel.Size")));
			this.cmdCancel.TabIndex = ((int)(resources.GetObject("cmdCancel.TabIndex")));
			this.cmdCancel.Text = resources.GetString("cmdCancel.Text");
			this.cmdCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdCancel.TextAlign")));
			this.cmdCancel.Visible = ((bool)(resources.GetObject("cmdCancel.Visible")));
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AccessibleDescription = ((string)(resources.GetObject("cmdOK.AccessibleDescription")));
			this.cmdOK.AccessibleName = ((string)(resources.GetObject("cmdOK.AccessibleName")));
			this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdOK.Anchor")));
			this.cmdOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdOK.BackgroundImage")));
			this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cmdOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdOK.Dock")));
			this.cmdOK.Enabled = ((bool)(resources.GetObject("cmdOK.Enabled")));
			this.cmdOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdOK.FlatStyle")));
			this.cmdOK.Font = ((System.Drawing.Font)(resources.GetObject("cmdOK.Font")));
			this.cmdOK.Image = ((System.Drawing.Image)(resources.GetObject("cmdOK.Image")));
			this.cmdOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdOK.ImageAlign")));
			this.cmdOK.ImageIndex = ((int)(resources.GetObject("cmdOK.ImageIndex")));
			this.cmdOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdOK.ImeMode")));
			this.cmdOK.Location = ((System.Drawing.Point)(resources.GetObject("cmdOK.Location")));
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdOK.RightToLeft")));
			this.cmdOK.Size = ((System.Drawing.Size)(resources.GetObject("cmdOK.Size")));
			this.cmdOK.TabIndex = ((int)(resources.GetObject("cmdOK.TabIndex")));
			this.cmdOK.Text = resources.GetString("cmdOK.Text");
			this.cmdOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdOK.TextAlign")));
			this.cmdOK.Visible = ((bool)(resources.GetObject("cmdOK.Visible")));
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// label3
			// 
			this.label3.AccessibleDescription = ((string)(resources.GetObject("label3.AccessibleDescription")));
			this.label3.AccessibleName = ((string)(resources.GetObject("label3.AccessibleName")));
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label3.Anchor")));
			this.label3.AutoSize = ((bool)(resources.GetObject("label3.AutoSize")));
			this.label3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label3.Dock")));
			this.label3.Enabled = ((bool)(resources.GetObject("label3.Enabled")));
			this.label3.Font = ((System.Drawing.Font)(resources.GetObject("label3.Font")));
			this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
			this.label3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.ImageAlign")));
			this.label3.ImageIndex = ((int)(resources.GetObject("label3.ImageIndex")));
			this.label3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label3.ImeMode")));
			this.label3.Location = ((System.Drawing.Point)(resources.GetObject("label3.Location")));
			this.label3.Name = "label3";
			this.label3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label3.RightToLeft")));
			this.label3.Size = ((System.Drawing.Size)(resources.GetObject("label3.Size")));
			this.label3.TabIndex = ((int)(resources.GetObject("label3.TabIndex")));
			this.label3.Text = resources.GetString("label3.Text");
			this.label3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.TextAlign")));
			this.label3.Visible = ((bool)(resources.GetObject("label3.Visible")));
			// 
			// lblNew2
			// 
			this.lblNew2.AccessibleDescription = ((string)(resources.GetObject("lblNew2.AccessibleDescription")));
			this.lblNew2.AccessibleName = ((string)(resources.GetObject("lblNew2.AccessibleName")));
			this.lblNew2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblNew2.Anchor")));
			this.lblNew2.AutoSize = ((bool)(resources.GetObject("lblNew2.AutoSize")));
			this.lblNew2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblNew2.Dock")));
			this.lblNew2.Enabled = ((bool)(resources.GetObject("lblNew2.Enabled")));
			this.lblNew2.Font = ((System.Drawing.Font)(resources.GetObject("lblNew2.Font")));
			this.lblNew2.Image = ((System.Drawing.Image)(resources.GetObject("lblNew2.Image")));
			this.lblNew2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNew2.ImageAlign")));
			this.lblNew2.ImageIndex = ((int)(resources.GetObject("lblNew2.ImageIndex")));
			this.lblNew2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblNew2.ImeMode")));
			this.lblNew2.Location = ((System.Drawing.Point)(resources.GetObject("lblNew2.Location")));
			this.lblNew2.Name = "lblNew2";
			this.lblNew2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblNew2.RightToLeft")));
			this.lblNew2.Size = ((System.Drawing.Size)(resources.GetObject("lblNew2.Size")));
			this.lblNew2.TabIndex = ((int)(resources.GetObject("lblNew2.TabIndex")));
			this.lblNew2.Text = resources.GetString("lblNew2.Text");
			this.lblNew2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNew2.TextAlign")));
			this.lblNew2.Visible = ((bool)(resources.GetObject("lblNew2.Visible")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = ((string)(resources.GetObject("label1.AccessibleDescription")));
			this.label1.AccessibleName = ((string)(resources.GetObject("label1.AccessibleName")));
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// txtNew1
			// 
			this.txtNew1.AccessibleDescription = ((string)(resources.GetObject("txtNew1.AccessibleDescription")));
			this.txtNew1.AccessibleName = ((string)(resources.GetObject("txtNew1.AccessibleName")));
			this.txtNew1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNew1.Anchor")));
			this.txtNew1.AutoSize = ((bool)(resources.GetObject("txtNew1.AutoSize")));
			this.txtNew1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNew1.BackgroundImage")));
			this.txtNew1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNew1.Dock")));
			this.txtNew1.Enabled = ((bool)(resources.GetObject("txtNew1.Enabled")));
			this.txtNew1.Font = ((System.Drawing.Font)(resources.GetObject("txtNew1.Font")));
			this.txtNew1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNew1.ImeMode")));
			this.txtNew1.Location = ((System.Drawing.Point)(resources.GetObject("txtNew1.Location")));
			this.txtNew1.MaxLength = ((int)(resources.GetObject("txtNew1.MaxLength")));
			this.txtNew1.Multiline = ((bool)(resources.GetObject("txtNew1.Multiline")));
			this.txtNew1.Name = "txtNew1";
			this.txtNew1.PasswordChar = ((char)(resources.GetObject("txtNew1.PasswordChar")));
			this.txtNew1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNew1.RightToLeft")));
			this.txtNew1.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNew1.ScrollBars")));
			this.txtNew1.Size = ((System.Drawing.Size)(resources.GetObject("txtNew1.Size")));
			this.txtNew1.TabIndex = ((int)(resources.GetObject("txtNew1.TabIndex")));
			this.txtNew1.Text = resources.GetString("txtNew1.Text");
			this.txtNew1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNew1.TextAlign")));
			this.txtNew1.Visible = ((bool)(resources.GetObject("txtNew1.Visible")));
			this.txtNew1.WordWrap = ((bool)(resources.GetObject("txtNew1.WordWrap")));
			// 
			// txtOrig
			// 
			this.txtOrig.AccessibleDescription = ((string)(resources.GetObject("txtOrig.AccessibleDescription")));
			this.txtOrig.AccessibleName = ((string)(resources.GetObject("txtOrig.AccessibleName")));
			this.txtOrig.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtOrig.Anchor")));
			this.txtOrig.AutoSize = ((bool)(resources.GetObject("txtOrig.AutoSize")));
			this.txtOrig.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtOrig.BackgroundImage")));
			this.txtOrig.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtOrig.Dock")));
			this.txtOrig.Enabled = ((bool)(resources.GetObject("txtOrig.Enabled")));
			this.txtOrig.Font = ((System.Drawing.Font)(resources.GetObject("txtOrig.Font")));
			this.txtOrig.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtOrig.ImeMode")));
			this.txtOrig.Location = ((System.Drawing.Point)(resources.GetObject("txtOrig.Location")));
			this.txtOrig.MaxLength = ((int)(resources.GetObject("txtOrig.MaxLength")));
			this.txtOrig.Multiline = ((bool)(resources.GetObject("txtOrig.Multiline")));
			this.txtOrig.Name = "txtOrig";
			this.txtOrig.PasswordChar = ((char)(resources.GetObject("txtOrig.PasswordChar")));
			this.txtOrig.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtOrig.RightToLeft")));
			this.txtOrig.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtOrig.ScrollBars")));
			this.txtOrig.Size = ((System.Drawing.Size)(resources.GetObject("txtOrig.Size")));
			this.txtOrig.TabIndex = ((int)(resources.GetObject("txtOrig.TabIndex")));
			this.txtOrig.Text = resources.GetString("txtOrig.Text");
			this.txtOrig.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtOrig.TextAlign")));
			this.txtOrig.Visible = ((bool)(resources.GetObject("txtOrig.Visible")));
			this.txtOrig.WordWrap = ((bool)(resources.GetObject("txtOrig.WordWrap")));
			// 
			// txtNew2
			// 
			this.txtNew2.AccessibleDescription = ((string)(resources.GetObject("txtNew2.AccessibleDescription")));
			this.txtNew2.AccessibleName = ((string)(resources.GetObject("txtNew2.AccessibleName")));
			this.txtNew2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNew2.Anchor")));
			this.txtNew2.AutoSize = ((bool)(resources.GetObject("txtNew2.AutoSize")));
			this.txtNew2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNew2.BackgroundImage")));
			this.txtNew2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNew2.Dock")));
			this.txtNew2.Enabled = ((bool)(resources.GetObject("txtNew2.Enabled")));
			this.txtNew2.Font = ((System.Drawing.Font)(resources.GetObject("txtNew2.Font")));
			this.txtNew2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNew2.ImeMode")));
			this.txtNew2.Location = ((System.Drawing.Point)(resources.GetObject("txtNew2.Location")));
			this.txtNew2.MaxLength = ((int)(resources.GetObject("txtNew2.MaxLength")));
			this.txtNew2.Multiline = ((bool)(resources.GetObject("txtNew2.Multiline")));
			this.txtNew2.Name = "txtNew2";
			this.txtNew2.PasswordChar = ((char)(resources.GetObject("txtNew2.PasswordChar")));
			this.txtNew2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNew2.RightToLeft")));
			this.txtNew2.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNew2.ScrollBars")));
			this.txtNew2.Size = ((System.Drawing.Size)(resources.GetObject("txtNew2.Size")));
			this.txtNew2.TabIndex = ((int)(resources.GetObject("txtNew2.TabIndex")));
			this.txtNew2.Text = resources.GetString("txtNew2.Text");
			this.txtNew2.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNew2.TextAlign")));
			this.txtNew2.Visible = ((bool)(resources.GetObject("txtNew2.Visible")));
			this.txtNew2.WordWrap = ((bool)(resources.GetObject("txtNew2.WordWrap")));
			// 
			// frmChgPassword
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("$this.Anchor")));
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.txtNew2,
																		  this.picLogo,
																		  this.cmdCancel,
																		  this.cmdOK,
																		  this.label3,
																		  this.lblNew2,
																		  this.label1,
																		  this.txtNew1,
																		  this.txtOrig});
			this.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("$this.Dock")));
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximizeBox = false;
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmChgPassword";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Visible = ((bool)(resources.GetObject("$this.Visible")));
			this.ResumeLayout(false);

		}
		#endregion
		static int AttemptCount = 0;


		/// <summary>
		/// Private utility Riskmaster.Security.cmdOK_Click confirms the user action and it change the user password, it also checks for the old password entered by the user.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event argument</param>
		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			AttemptCount++;
			bool bSuccess = false;

			if(m_OldPassword =="")
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                MessageBox.Show(this, Globalization.GetString("frmChgPassword.cmdOK_Click.OldPwdNA", m_iClientId), Globalization.GetString("frmChgPassword.cmdOK_Click.ChgPwdError", m_iClientId), MessageBoxButtons.OK, MessageBoxIcon.Stop);	
			else if( m_OldPassword != this.txtOrig.Text)
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                MessageBox.Show(this, Globalization.GetString("frmChgPassword.cmdOK_Click.OldPwdNotMatched", m_iClientId), Globalization.GetString("frmChgPassword.cmdOK_Click.ChgPwdError", m_iClientId), MessageBoxButtons.OK, MessageBoxIcon.Warning);	
			else if( this.txtNew1.Text!= this.txtNew2.Text )
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                MessageBox.Show(this, Globalization.GetString("frmChgPassword.cmdOK_Click.NewPwdsNotMatched", m_iClientId), Globalization.GetString("frmChgPassword.cmdOK_Click.ChgPwdError", m_iClientId), MessageBoxButtons.OK, MessageBoxIcon.Warning);	
			else if( this.txtNew1.Text== m_OldPassword)
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                MessageBox.Show(this, Globalization.GetString("frmChgPassword.cmdOK_Click.NewOldPwdSame", m_iClientId), Globalization.GetString("frmChgPassword.cmdOK_Click.ChgPwdError", m_iClientId), MessageBoxButtons.OK, MessageBoxIcon.Warning);	
			else if(this.txtNew1.Text.Length < 3)
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                MessageBox.Show(this, Globalization.GetString("frmChgPassword.cmdOK_Click.InvalidNewPwdLen", m_iClientId), Globalization.GetString("frmChgPassword.cmdOK_Click.ChgPwdError", m_iClientId), MessageBoxButtons.OK, MessageBoxIcon.Warning);	
			else
				bSuccess = true;

			if(bSuccess)
				m_NewPassword = this.txtNew1.Text;

			if (bSuccess) // Success - return OK.
				this.Close();
			else if(AttemptCount == 3) //No success - Keep trying until 3 attempts have expired, then cancel..
				this.DialogResult = DialogResult.Cancel;
			else
				this.DialogResult = DialogResult.None;
		}

		/// <summary>
		/// Private utility Riskmaster.Security.cmdCancel_Click cancel the user action.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event argument</param>
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();		
		}

	}
}
