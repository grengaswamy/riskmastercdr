using System;
using System.DirectoryServices;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Security
{    
    /// <summary>
    /// Contains methods to authenticate from Active directory
    /// <author>rsolanki2</author>
    /// </summary>
    public class ADSI
    {
        /// <summary>
        /// string specifying user name
        /// </summary>
        private string strUser;

        /// <summary>
        /// string specifying user password
        /// </summary>
        private string strPass;

        /// <summary>
        /// string specifying user domain
        /// </summary>
        private string strDomain;

        /// <summary>
        /// AuthenticationTypes specifying the security 
        /// protocol to use, i.e. Secure, SSL
        /// </summary>
        private AuthenticationTypes atAuthentType;

        /// <summary>
        /// default constructor
        /// </summary>
        public ADSI()
        {
        }

        /// <summary>
        /// function that sets the domain name
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns>It returns true, if user passed 
        ///          something; otherwise, false</returns>
        public bool SetDomain(string strValue)
        {
            if (strValue.Length <= 0)
                return false;

            this.strDomain = "LDAP://" + strValue;
            return true;
        }

        /// <summary>
        /// function that sets user name
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns>It returns true, if user passed 
        ///          something; otherwise, false</returns>
        public bool SetUser(string strValue)
        {
            if (strValue.Length <= 0)
                return false;

            this.strUser = strValue;
            return true;
        }

        /// <summary>
        /// function that sets user password
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns>It returns true, if user passed 
        ///          something; otherwise, false</returns>
        public bool SetPass(string strValue)
        {
            if (strValue.Length <= 0)
                return false;

            this.strPass = strValue;
            return true;
        }

        /// <summary>
        /// function that sets user authentication type
        /// </summary>
        /// <param name="bValue"></param>
        public void SetAuthenticationType(bool bValue)
        {
            // set ssl to true if true is found
            if (bValue)
                atAuthentType = AuthenticationTypes.SecureSocketsLayer;
            // otherwise set it to secure  
            else
                atAuthentType = AuthenticationTypes.Secure;
        }

        /// <summary>
        /// function that performs login task
        /// and welcomes user if they are verified
        /// </summary>
        public bool Login()
        {
            // now create the directory entry to establish connection
            using (DirectoryEntry deDirEntry = new DirectoryEntry(this.strDomain,
                                                                 this.strUser,
                                                                 this.strPass,
                                                                 this.atAuthentType))
            {
                // if user is verified then it will welcome then  
                try
                {
                    return true;
                }
                catch (Exception exp)
                {
                    return false;
                }
            }

        }

        /// <summary>
        ///  Authenticates the username/password from domain   
        /// </summary>
        /// <returns></returns>
        public static bool Authenticate(string p_sDomain, string p_sUser, string p_sPass)
        {
            string sDomain = string.Empty;
            string stemp = string.Empty; 

            if (p_sDomain.Length > 0)
                sDomain = "LDAP://" + p_sDomain;
            

            // now create the directory entry to establish connection
            using (DirectoryEntry deDirEntry = new DirectoryEntry(sDomain, p_sUser, p_sPass, AuthenticationTypes.Secure))
            {
                // if user is verified then it will welcome then  
                try
                {
                    stemp = deDirEntry.Name;
                    stemp = deDirEntry.NativeGuid; 
                    return true;
                }
                catch (Exception exp)
                {
                    return false;
                }
            }
        }
    }
}
