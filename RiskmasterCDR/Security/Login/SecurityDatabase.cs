using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Riskmaster.Security
{
    /// <summary>
    /// Riskmaster.Security.SecurityDatabase class is designed for the database security, 
    /// It check for the database security on the basis of autorization key and code access public key
    /// </summary>
    [ComVisible(false)]
    public class SecurityDatabase
    {
        /// <summary>
        /// Static constructor used to initialize
        /// static variables within this class
        /// </summary>
        /// <exception cref="NullReferenceException">NullReferenceException is thrown if the specified
        /// Security Configuration information is not available within the Configuration file</exception>
        /// <remarks>This is specifically needed to initialize the value of the Dsn
        /// property the first time it is referenced.  Since this is a static property
        /// and not an instance based property, a static constructor is required.</remarks>
        static SecurityDatabase()
        {
            //Initialize the value of the Security Database DSN
            SecurityDatabaseManager.CreateSecurityDatabase();
        } // method: SecurityDatabase


        private const int FUNC_LIST_SIZE = 4527;
        //Explict Code Access Control

        //BSB 08.05.2005 Set up a static hash table cache for the parent relations of the 
        // Function List entries from the Security Database.
        // This will be used to avoid passing "parent" security id's around FDM and closes a security
        // hole based on that pattern.

        private static Hashtable m_FuncHashTable = null;

        private static void LoadFuncHashTable(int p_iClientId)
        {
            if (m_FuncHashTable != null)
                return;
            else
                m_FuncHashTable = new Hashtable(FUNC_LIST_SIZE);

            string SQL = "SELECT FUNC_ID, PARENT_ID FROM FUNCTION_LIST";

            using (DbReader rdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(p_iClientId), SQL))
            {
                try
                {
                    if (rdr.Read())
                    {
                        do
                        {
                            try { m_FuncHashTable.Add(rdr.GetInt("FUNC_ID"), rdr.GetInt("PARENT_ID")); }
                            catch (Exception) { ;}
                        }
                        while (rdr.Read());
                    }
                }
                catch (Exception e)
                {
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    //Replaced general exception with SecuritySettingException.Added by Tanuj Narula on 27/5/2004
                    throw new SecuritySettingException(Globalization.GetString("SecurityDatabase.PopulateSecurityHashTable.CannotGetFuncList", p_iClientId), e);
                }
                finally
                {
                    if (rdr != null)
                        rdr.Close();
                }
            }
        }


        public static int GetParentFuncId(int funcId, int p_iClientId)
        {
            if (m_FuncHashTable == null)
                LoadFuncHashTable(p_iClientId);
            return (int)(m_FuncHashTable[funcId]);
        }

        ///<summary>Gets and sets the Dsn for the RISKMASTER Security Database</summary>
        /// <returns>It returns the DSN name as a string</returns>
        /// <remarks>The current implementation is too highly coupled with the Riskmaster.config file for
        /// retrieving security credentials/application information especially since this is implemented 
        /// as a static property.  Because this is additionally implemented statically, it will also be
        /// invisible to COM libraries thus making it unavailable to legacy applications.  In order for this 
        /// library to be usable by both .Net and COM libraries, the entire Riskmaster.Security library
        /// needs to be implemented based on instance methods rather than static methods and properties
        /// </remarks>
        public static string Dsn
        {
            get;
            internal set;
        }//property: Dsn
        public static string GetSecurityDsn(int p_iClientId)
        {
           return SecurityDatabaseManager.GetSecDsn(p_iClientId);
        }
    }
}
