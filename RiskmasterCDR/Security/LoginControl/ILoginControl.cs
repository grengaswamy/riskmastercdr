﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.Security
{
    public interface ILoginControl
    {

        string Dsn { get; }
        string UserID { get; }
        string Password { get;}
        string ConnectionString {get; set;}
        bool LoginSuccess { get; }
    }
}
