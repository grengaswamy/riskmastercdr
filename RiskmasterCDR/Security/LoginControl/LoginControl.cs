﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Riskmaster.Security;


namespace Riskmaster.Security
{
    /// <summary>
    /// Re-usable Windows Forms Login control
    /// that can be used for client-server applications
    /// </summary>
    [ToolboxBitmap(typeof(Properties.Resources), "Login")]
    public partial class LoginControl : UserControl, ILoginControl
    {

        /// <summary>
        /// Default Class constructor
        /// </summary>
        public LoginControl()
        {
            InitializeComponent();
        }//constructor

        private bool m_blnLoginSuccess = false;


        #region ILoginControl Members

        public string Dsn
        {
            get
            {
                return cmbDSNs.SelectedValue.ToString();
            }//get
        }

        public string UserID
        {
            get
            {
                return txtUserName.Text.Trim();
            }
        }

        public string Password
        {
            get
            {
                return txtPassword.Text.Trim();
            }
            

        }

        public string ConnectionString
        {
            get;
            set;
        }

        public bool LoginSuccess
        {
            get
            {
                return m_blnLoginSuccess;
            }//get
        }

        #endregion

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                Riskmaster.Security.Login objLogin = new Login("DSN=DTG Security32;UID=sa;PWD=riskmaster.2k8;");
                string[] arrUserDatabases = objLogin.GetUserDatabases(this.UserID, this.Password);

                cmbDSNs.DataSource = arrUserDatabases;

                //m_blnLoginSuccess = true;

            }

        }

        private void txtUserName_Validating(object sender, CancelEventArgs e)
        {

            // validate the content of the user name textbox control.
            txtUserName_Validated(sender, e);    // evaluate any errors that may have occurred.

            if (string.IsNullOrEmpty(errValidation.GetError(txtUserName)))
            {

                // no error, do not cancel.

                e.Cancel = false;

            }

            else
            {

                // an error occurred, therefore cancel.

                e.Cancel = true;

            }

        }

        private void txtPassword_Validating(object sender, CancelEventArgs e)
        {
            // validate the content of the password textbox control.
            txtPassword_Validated(sender, e);    // evaluate any errors that may have occurred.

            if (string.IsNullOrEmpty(errValidation.GetError(txtPassword)))
            {

                // no error, do not cancel.

                e.Cancel = false;

            }

            else
            {

                // an error occurred, therefore cancel.

                e.Cancel = true;

            }
        }

        private void txtUserName_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.UserID))
            {
                errValidation.SetError(txtUserName, "Please enter a user name.");
                txtUserName.Focus();
            }//if
            else
            {
                errValidation.SetError(txtUserName, string.Empty);
            } // else
        }

        private void txtPassword_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.Password))
            {
                errValidation.SetError(txtPassword, "Please enter a password.");
                txtUserName.Focus();
            }//if
            else
            {
                errValidation.SetError(txtPassword, string.Empty);
            } // else
        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            Riskmaster.Security.UserLogin m_objLogin = new UserLogin(this.UserID, this.Password, this.Dsn);
            string m_ConnectionString = m_objLogin.objRiskmasterDatabase.ConnectionString;
            if (! string.IsNullOrEmpty(m_ConnectionString))
            {
                MessageBox.Show(m_ConnectionString);
                m_blnLoginSuccess = true;
            } // if

        }


        
    }//class
}//namespace

