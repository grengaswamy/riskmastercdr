﻿using System;
namespace Riskmaster.Security.Authentication
{
    public interface IRiskmasterUserLogin
    {
        int DatabaseId { get; set; }
        string DocumentPath { get; set; }
        bool EnforceCheckSum { get; set; }
        int ForceChangePwd { get; set; }
        string MonEnd { get; set; }
        string MonStart { get; set; }
        string Password { get; set; }
        string SatEnd { get; set; }
        string SatStart { get; set; }
        string SunEnd { get; set; }
        string SunStart { get; set; }
        string ThuEnd { get; set; }
        string ThuStart { get; set; }
        string TueEnd { get; set; }
        string TueStart { get; set; }
        string WedEnd { get; set; }
        string WedStart { get; set; }
        string FriEnd { get; set; }
        string FriStart { get; set; }
        int GroupId { get; }
        bool IsAllowed(int FunctionId);
        bool IsAllowedEx(int secId);
        bool IsAllowedEx(int secBase, int secOffset);
        bool IsAllowedSuppEx(int secBase, int secOffset);
        DateTime PasswordExpire { get; set; }
        DateTime PrivilegesExpire { get; set; }
        bool IsOrgSecAdmin { get; }
        bool IsTerminalServer { get; }
        bool IsTimePermittedForLogin();
        string LoginName { get; set; }
        
    }
}
