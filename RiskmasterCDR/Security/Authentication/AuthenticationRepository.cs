﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Riskmaster.Security.Authentication
{
    internal class AuthenticationRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static string GetUserID()
        {
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append("SELECT ut.USER_ID ");
            //srajindersin 05/25/2012 MITS 26800
            //strSQL.Append("FROM USER_TABLE ut");
            strSQL.Append("FROM USER_TABLE ut " );
            strSQL.Append("INNER JOIN USER_DETAILS_TABLE udt ");
            strSQL.Append("ON ut.USER_ID = udt.USER_ID ");
            strSQL.AppendFormat(string.Format("WHERE udt.LOGIN_NAME = {0}", "~USER_NAME~"));

            return strSQL.ToString();
        } // method: GetUserID


        /// <summary>
        /// Provides checks for a standard User Login event
        /// without Enhanced Security turned on
        /// </summary>
        /// <returns></returns>
        internal static string VerifyUser()
        {
            StringBuilder strSQL = new StringBuilder();
            
            //Raman Bhatia 04/22/2009: Current Date and expiry date validations should be a part of AUTHORIZATION and not AUTHENTICATION
            //These settings work at a DataSource level and NOT during Authentication
            //Commenting all such code from here
            
            //List<string> arrWeekdayColumns = UtilityFunctions.MapWeekdayColumns();
            //string strCurrTimeOfDay = UtilityFunctions.GetCurrentTimeOfDay();

            strSQL.Append("SELECT USER_ID FROM USER_DETAILS_TABLE ");
            strSQL.AppendFormat(string.Format("WHERE LOGIN_NAME={0} AND PASSWORD={1} ", "~UID1~", "~PWD1~"));

            

            //strSQL.AppendFormat(string.Format("AND (PRIVS_EXPIRE > {0} ", "~CURR_DATE~"));
            //strSQL.Append("OR PRIVS_EXPIRE IS NULL) ");
            //strSQL.Append(string.Format("AND (({0} BETWEEN {1} AND {2}) OR ({1} = '000000' AND {2} = '000000')) ", strCurrTimeOfDay, arrWeekdayColumns[0], arrWeekdayColumns[1]));

            return strSQL.ToString();
        } // method: VerifyUser()

        /// <summary>
        /// Provides checks for a SMS user
        /// created by Raman Bhatia
        /// </summary>
        /// <returns></returns>
        internal static string VerifySMSUser()
        {
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append("SELECT a.USER_ID FROM USER_DETAILS_TABLE a, USER_TABLE b ");
            strSQL.AppendFormat(string.Format("WHERE LOGIN_NAME={0} AND PASSWORD={1} ", "~UID1~", "~PWD1~"));
            strSQL.Append(" AND a.USER_ID = b.USER_ID AND b.IS_SMS_USER = 1 ");
            
            return strSQL.ToString();
        } // method: VerifyUser()

        /// <summary>
        /// Provides checks for a User Login event
        /// when Enhanced Security is turned on
        /// </summary>
        /// <returns></returns>
        internal static string VerifyEnhancedSecurityUser()
        {
            StringBuilder strSQL = new StringBuilder();
            List<string> arrWeekdayColumns = UtilityFunctions.MapWeekdayColumns();
            string strCurrTimeOfDay = UtilityFunctions.GetCurrentTimeOfDay();
            const int ACCOUNT_LOCKED_OUT = 1; //1 = Account is Locked out, 0 = Account is not Locked out

            strSQL.Append("SELECT ut.USER_ID FROM USER_TABLE ut ");
            strSQL.Append("INNER JOIN USER_DETAILS_TABLE udt ");
	        strSQL.Append("ON ut.USER_ID = udt.USER_ID ");
            strSQL.AppendFormat(string.Format("WHERE udt.LOGIN_NAME={0} AND udt.PASSWORD={1} ", "~UID1~", "~PWD1~"));
            //strSQL.AppendFormat(string.Format("AND (udt.PRIVS_EXPIRE > {0} OR udt.PRIVS_EXPIRE IS NULL) ", "~CURR_DATE~"));
            strSQL.Append(string.Format("AND (('{0}' BETWEEN udt.{1} AND udt.{2}) OR (udt.{1} = '000000' AND udt.{2} = '000000')) ", strCurrTimeOfDay, arrWeekdayColumns[0], arrWeekdayColumns[1]));
            strSQL.AppendFormat(string.Format("AND (ut.ACCOUNT_LOCK_STATUS != {0} OR ut.ACCOUNT_LOCK_STATUS IS NULL)", ACCOUNT_LOCKED_OUT)); //TODO: Determine if this needs to be a parameter
            strSQL.AppendFormat(string.Format("AND (udt.PASSWD_EXPIRE > {0} OR udt.PASSWD_EXPIRE IS NULL)", "~CURR_DATE~"));

            return strSQL.ToString();
        }//method: VerifyEnhancedSecurityUser()

        /// <summary>
        /// Gets the query to determine if an authentication 
        /// provider other than the standard RISKMASTER MembershipProvider
        /// has been enabled
        /// </summary>
        /// <returns></returns>
        internal static string GetEnabledAuthenticationProvider()
        {
            const int ENABLED = 1; //1 = True, 0 = False

            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("SELECT apt.AUTH_PROVIDER_TYPE_DESC, ");
            strSQL.Append("ap.CONN_STRING ");
            strSQL.Append("FROM AUTH_PROVIDER ap ");
            strSQL.Append("INNER JOIN AUTH_PROVIDER_TYPE apt ");
            strSQL.Append("ON apt.AUTH_PROVIDER_TYPE_ID = ap.AUTH_PROVIDER_TYPE_ID ");
            strSQL.AppendFormat(string.Format("WHERE ap.ENABLED={0}", ENABLED));

            return strSQL.ToString();
        } // method: GetEnabledAuthenticationProvider

        /// <summary>
        /// Gets the relevant attributes required for the specified SSO provider
        /// </summary>
        /// <param name="strSSOProvider">string containing the name of the specified SSO provider</param>
        /// <returns>string containing the SQL statement to be executed against the database</returns>
        /// <remarks>currently this method is only relevant for LDAP repositories</remarks>
        internal static string GetSSOProviderAttributes(string strSSOProvider)
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("SELECT ap.USER_ATTRIBUTE_MAPPING, ");
            strSQL.Append("ap.ADMIN_UID, ");
            strSQL.Append("ap.ADMIN_PWD ");
            strSQL.Append("FROM AUTH_PROVIDER ap ");
            strSQL.Append("INNER JOIN AUTH_PROVIDER_TYPE apt ");
            strSQL.Append("ON ap.AUTH_PROVIDER_TYPE_ID = apt.AUTH_PROVIDER_TYPE_ID ");
            strSQL.AppendFormat(string.Format("WHERE apt.AUTH_PROVIDER_TYPE_DESC='{0}'", strSSOProvider));

            return strSQL.ToString();
        } // method: GetSSOProviderAttributes


        /// <summary>
        /// Determines if a current user's account
        /// has been locked out due to various reasons
        /// including multiple failed password attempts
        /// </summary>
        /// <returns></returns>
        internal static string GetLockedOutStatus()
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("SELECT ACCOUNT_LOCK_STATUS ");
            strSQL.AppendFormat("FROM USER_TABLE ut ");
            strSQL.AppendFormat(string.Format("WHERE ut.USER_ID = {0}", "~USER_ID~"));

            return strSQL.ToString();
        } // method: GetLockedOutStatus


        /// <summary>
        /// Look up a user's login name by their 
        /// currently configured e-mail address
        /// </summary>
        /// <returns></returns>
        internal static string GetUserNameByEmail()
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("SELECT udt.LOGIN_NAME ");
            strSQL.Append("FROM USER_TABLE ut ");
            strSQL.Append("INNER JOIN USER_DETAILS_TABLE udt ");
	        strSQL.Append("ON ut.USER_ID = udt.USER_ID ");
            strSQL.AppendFormat(string.Format("WHERE ut.EMAIL_ADDR = {0}", "~EMAIL_ADDRESS~"));

            return strSQL.ToString();

        } // method: GetUserNameByEmail

        /// <summary>
        /// Updates the Login History table
        /// for all successful or failed logins
        /// </summary>
        /// <returns></returns>
        /// <remarks>This table can simply be used for auditing purposes</remarks>
        internal static string UpdateLoginHistory()
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("INSERT INTO LOGIN_HISTORY ");
            strSQL.Append("(USER_ID, DTTM_LOGIN, STATUS)");
            strSQL.AppendFormat(string.Format("VALUES({0}, {1}, {2})", "~USER_ID~", "~LOGIN_DATE~", "~LOGIN_STATUS~"));

            return strSQL.ToString();
        }//method: UpdateLoginHistory()

        /// <summary>
        /// Updates the User Login Failure table
        /// if any logins have failed authentication
        /// </summary>
        /// <returns></returns>
        internal static string UpdateUserLoginFailure()
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("INSERT INTO USER_LOGIN_FAILURE ");
            strSQL.Append("(USER_LOGIN, USER_PASSWORD, DTTM_FAILED)");
            strSQL.AppendFormat(string.Format("VALUES({0}, {1}, {2})", "~USER_LOGIN~", "~USER_PASSWORD~", "~LOGIN_DATE~"));

            return strSQL.ToString();
        }//method: UpdateUserLoginFailure()

        /// <summary>
        /// Updates the User Login Track table
        /// for all successful or failed logins
        /// </summary>
        /// <returns></returns>
        /// <remarks>This table can simply be used for auditing purposes</remarks>
        internal static string UpdateUserLoginTrack()
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("INSERT INTO USER_LOGIN_TRACK ");
            strSQL.Append("(USER_ID, USER_LOGIN, DTTM_LOGIN)");
            strSQL.AppendFormat(string.Format("VALUES({0}, {1}, {2})", "~USER_ID~", "~USER_LOGIN~", "~LOGIN_DATE~"));

            return strSQL.ToString();
        }//method: UpdateUserLoginTrack()



    }
}
