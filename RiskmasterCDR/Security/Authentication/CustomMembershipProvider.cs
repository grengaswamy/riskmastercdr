﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Runtime.Serialization;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Implement a Custom Membership Provider which inherits from the base MembershipProvider
    /// class so as to provide additional concrete attributes not currently available
    /// in the base MembershipProvider class
    /// </summary>
    /// <remarks>Creating a CustomMembershipProvider class facilitates creating a Factory method since
    /// all subclasses all conform to the same interface and therefore does not require explicit casting</remarks>
    [DataContract]
    public abstract class CustomMembershipProvider: IMembershipProvider, IMembershipProviderExtensions, ISSOProvider
    {
        

        #region IMembershipProviderExtensions Members

       
        public virtual string connectionStringName
        {
            get;
            set;
        }

        public virtual string connectionUsername
        {
            get;
            set;
        }

        public virtual string connectionPassword
        {
            get;
            set;
        }

        #endregion

        #region ISSOProvider Members

        [DataMember]
        public string AdminPwd
        {
            get;
            set;
        }

        [DataMember]
        public string AdminUserName
        {
            get;
            set;
        }

        [DataMember]
        public string ConnectionString
        {
            get;
            set;
        }

        [DataMember]
        public string ConnectionType
        {
            get;
            set;
        }

        [DataMember]
        public string HostName
        {
            get;
            set;
        }

        [DataMember]
        public bool IsEnabled
        {
            get;
            set;
        }

        [DataMember]
        public int ProviderID
        {
            get;
            set;
        }

        #endregion

        #region IMembershipProvider Members


        public abstract string ApplicationName
        {
            get;
            set;
        }

        public abstract bool ChangePassword(string username, string oldPassword, string newPassword);
        

        public abstract bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer);
        

        public abstract MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status);


        public abstract bool DeleteUser(string username, bool deleteAllRelatedData);


        public abstract bool EnablePasswordReset
        {
            get;
        }

        public abstract bool EnablePasswordRetrieval
        {
            get;
        }

        public abstract MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords);
        

        public abstract MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords);


        public abstract MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords);
        

        public abstract int GetNumberOfUsersOnline();
        

        public abstract string GetPassword(string username, string answer);
        

        public abstract MembershipUser GetUser(string username, bool userIsOnline);
        
        public abstract MembershipUser GetUser(object providerUserKey, bool userIsOnline);
        

        public abstract string GetUserNameByEmail(string email);
        

        public abstract int MaxInvalidPasswordAttempts
        {
            get;
        }

        public abstract int MinRequiredNonAlphanumericCharacters
        {
            get;
        }

        public abstract int MinRequiredPasswordLength
        {
            get;
        }

        public abstract int PasswordAttemptWindow
        {
            get;
        }

        public abstract MembershipPasswordFormat PasswordFormat
        {
            get;
        }

        public abstract string PasswordStrengthRegularExpression
        {
            get;
        }

        public abstract bool RequiresQuestionAndAnswer
        {
            get;
        }

        public abstract bool RequiresUniqueEmail
        {
            get;
        }

        public abstract string ResetPassword(string username, string answer);
        

        public abstract bool UnlockUser(string userName);
        

        public abstract void UpdateUser(MembershipUser user);
        

        public abstract bool ValidateUser(string username, string password);

        public abstract bool ValidateSMSUser(string username, string password);
        

        #endregion
    }
}
