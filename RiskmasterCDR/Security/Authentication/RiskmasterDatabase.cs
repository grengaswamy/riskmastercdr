using System;
using System.Collections.Generic;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Security.Authentication
{
	/// <summary>
	/// Riskmaster.Security.RiskmasterDatabase class is an Public class is used for connecting,identifying 
	/// and setting various parameters for riskmaster database. This also checks license grants, checks security for 
	/// userid password, identifies datetime.
	/// </summary>
    [DataContract]
    [StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
	public class RiskmasterDatabase
	{
		
        
		// Fields
		private string m_DataSourceName; //  DSN[51];
		private int m_DataSourceId; //DSN_ID;
		private short m_Status; //STATUS;
        private string m_RMUserId = string.Empty, m_RMPassword = string.Empty; //RM_USERID[51];
		private eDatabaseType m_DbType; //DBTYPE;
		private int m_OrgSecFlag;//ORGSEC_FLAG;
		private string m_ConnectionString=string.Empty, m_Checksum=string.Empty; //CONNECTION_STRING[1024];
		private int m_NumLicenses; //NUM_LICENSES;
		private System.DateTime m_LicUpdDate; //LIC_UPD_DATE[9];
        private string m_CRC2 = string.Empty, m_GlobalDocPath = string.Empty; //CRC2[129]; //GLOBAL_DOC_PATH[512];
		private int m_DocPathType; //DOC_PATH_TYPE;
		//Implementation Storage
		internal bool m_DataChanged=false;

         
		 /*Addded by Tanuj on 2-Sep-2005,
		  * variable to flag whether to connect to Database for finding the its type or not.
		 */
		private bool m_bFindDBType=true;
		private bool m_bEnforceCheckSum = true;
        private string m_sUserName = string.Empty;    //gagnihotri MITS 11995 Changes made for Audit table			
        private int m_iClientId = 0;//rkaur27	

        #region Constructors
        /// <summary>
        /// Riskmaster.Security.RiskmasterDatabase is internal constructor without any parameter.
        /// </summary>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public RiskmasterDatabase(int p_iClientId)
        {
            m_iClientId = p_iClientId;//rkaur27
        }

        /* First Code Access Security Usage
         * This attribute requires that any caller have a strongname proving that it's CSC Riskmaster Code. 
         * To ensure that your assembly has an appropriate strongname, place the following line in the 
         *  AssemblyInfo.cs file:
         *  [assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")]
         * It is used because this is a usefull class\constructor for other CSC modules but too dangerous to 
         *  make truly public.*/

        /// <summary>
        /// Riskmaster.Security.RiskmasterDatabase is the constructor with parameter. Constructor call after the attribute 
        /// [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        /// This constructor calls the function Load() and serves as a wrapper.
        /// </summary>
        /// <param name="DsnId">DSN ID</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public RiskmasterDatabase(int DsnId, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            this.Load(DsnId);
        }
        /*Added by Tanuj on 2-Sep-2005
         * This constructor is added to initialize  'FindDBType' field and mainly added for Security mgt system(SMS) for loading all the datasources present in security Db.
         * Now the instance of this class will check this flag('FindDBType') to connect to the database for finding its type.
         * By default, value of this flag will be set to 'True' so will/should not change/break the existing functionality.
         */
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public RiskmasterDatabase(int DsnId, bool p_bFindDBType, int p_iClientId)
        {
            m_bFindDBType = p_bFindDBType;
            m_iClientId = p_iClientId;
            this.Load(DsnId);
        }

        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public RiskmasterDatabase(int DsnId, bool p_bFindDBType, bool p_bEnforceCheckSum, int p_iClientId)
        {
            m_bEnforceCheckSum = p_bEnforceCheckSum;
            m_bFindDBType = p_bFindDBType;
            m_iClientId = p_iClientId;
            this.Load(DsnId);
        }

        //gagnihotri MITS 11995 Changes made for Audit table Start
        public RiskmasterDatabase(string p_sUserName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
        }

        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public RiskmasterDatabase(int DsnId, bool p_bFindDBType, string p_sUserName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_bFindDBType = p_bFindDBType;
            m_iClientId = p_iClientId;
            this.Load(DsnId);
        }
        //gagnihotri MITS 11995 Changes made for Audit table End
        /// <summary>
        /// Riskmaster.Security.Load is the internal function uses DbFactory class  & datareader object 
        /// query local mdb settings to validate  dsnid as per  database. 
        /// Throws InvalidOperationException if not matched.
        /// </summary>
        /// <param name="DsnId">DSN ID</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public void Load(int DsnId)
        {
            DbConnection conn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));//rkaur27
            conn.Open();
            DbReader reader = conn.ExecuteReader("SELECT DATA_SOURCE_TABLE.* FROM DATA_SOURCE_TABLE WHERE DATA_SOURCE_TABLE.DSNID=" + DsnId);
            try
            {
                if (reader.Read())
                    LoadData(reader);
                else
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    throw new InvalidOperationException(AuthenticationCredentials.DatabaseNotFound);
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }

        /// <summary>
        /// Riskmaster.Security.RiskmasterDatabase overload allows internal data layer classes to wrap a native reader 
        /// </summary>
        /// <param name="reader">The native reader to wrap</param>
        internal RiskmasterDatabase(DbReader reader)
        {
            LoadData(reader);
        } 
        #endregion

        #region Public Serializable properties
        [DataMember]
        /// <summary>
        /// Riskmaster.Security.DataSourceName access the value of data source name.
        /// </summary>
        public string DataSourceName
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.DataSourceId access the value of data source id.
        /// </summary>
        public int DataSourceId
        {
            get;
            set;
        }//property DataSourceId

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.Status access the value of status.
        /// </summary>
        public bool Status
        {
            get { return Convert.ToBoolean(m_Status); }
            set
            {
                if (!Convert.ToBoolean(m_Status).Equals(value))
                {
                    m_Status = (Convert.ToInt16(value));
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.RMUserId access the value of RM USer ID.
        /// </summary>
        public string RMUserId
        {
            get { return m_RMUserId; }
            [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
            set
            {
                if (!String.Equals(m_RMUserId, value))
                {
                    m_RMUserId = value;
                }
            }
        }


        [DataMember]
        /// <summary>
        /// Riskmaster.Security.DbType access the vale of database type.
        /// </summary>
        public eDatabaseType DbType
        {
            get { return m_DbType; }
            set
            {
                if (m_DbType != (value))
                {
                    m_DbType = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.Checksum access the value of Check sum value to ensure the data integrity.
        /// </summary>
        public string Checksum
        {
            get { return m_Checksum; }
            set
            {
                if (!String.Equals(m_Checksum, value))
                {
                    m_Checksum = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.OrgSecFlag access the value of security flag.
        /// </summary>
        public bool OrgSecFlag
        {
            get { return Convert.ToBoolean(m_OrgSecFlag); }
            set
            {
                if (!Convert.ToInt32(m_OrgSecFlag).Equals(value))
                {
                    // riskmaster stores -1 as an indicator of true value traditionally
                    // for orgsec flag in data source table
                    m_OrgSecFlag = Convert.ToInt32(value) == 1 ? -1 : Convert.ToInt32(value);
                    m_DataChanged = true;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.FindDBType of 'FindDBType' flag
        /// </summary>
        public bool FindDBType
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.ConnectionString access the value of connection string.
        /// </summary>
        public string ConnectionString
        {
            get { return m_ConnectionString; }
            set
            {
                if (!String.Equals(m_ConnectionString, value))
                {
                    m_ConnectionString = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.NumLicenses access the value of number of license.
        /// </summary>
        public int NumLicenses
        {
            get;
            set;
        }//property NumLicenses

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.LicUpdDate access the value of updated license.
        /// </summary>
        public DateTime LicUpdDate
        {
            get { return m_LicUpdDate; }
            set
            {
                if (!DateTime.Equals(m_LicUpdDate, value))
                {
                    m_LicUpdDate = value;
                }
            }
        }//property LicUpdDate

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.CRC2 access the value of CRC
        /// </summary>
        public string CRC2
        {
            get { return m_CRC2; }
            set
            {
                if (!String.Equals(m_CRC2, value))
                {
                    m_CRC2 = value;
                }
            }
        }//property CRC2

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.DocPathType access the value of document path.
        /// </summary>
        public int DocPathType
        {
            get { return m_DocPathType; }
            set
            {
                if (m_DocPathType != value)
                {
                    m_DocPathType = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.GlobalDocPath access the value of global document path.
        /// </summary>
        public string GlobalDocPath
        {
            get { return m_GlobalDocPath; }
            set
            {
                if (!String.Equals(m_GlobalDocPath, value))
                {
                    m_GlobalDocPath = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.RMPassword access the value of RM password.
        /// </summary>
        public string RMPassword
        {

            get { return m_RMPassword; }
            set
            {
                if (!String.Equals(m_RMPassword, value))
                {
                    m_RMPassword = value;
                }
            }
        }//property RMPassword

        [DataMember]
        /// <summary>
        /// Enforces CheckSum constraints
        /// </summary>
        public bool EnforceCheckSum
        {
            get;
            set;
        }//property EnforceCheckSum 
        #endregion

        #region Methods
        /// <summary>Riskmaster.Db.DbConnection.ParseDocPathCredentials supports the Document Database
        /// by decrypting the uid and pwd sections of the connection string and 
        /// returning the fully modified connection string.</summary>
        /// <param name="docPath">The connection string in which to effect the change.</param>
        /// <returns>An updated version of the connection string from docPath.</returns>
        /// <remarks>There may be ODBC niceties like {} or escape sequences that are not handled properly here yet.</remarks>
        private string ParseDocPathCredentials(string docPath)
        {
            //If it's not a Document Database Connection String - Just Leave.
            if (this.DocPathType != 1)
                return docPath;

            string[] sPairs = docPath.Split(';');
            string[] sPair = null;
            for (int i = 0; i < sPairs.Length; i++)
            {
                sPair = sPairs[i].Split('=');
                if (sPair[0].Trim().ToUpper() == "UID" || sPair[0].Trim().ToUpper() == "PWD")
                    sPairs[i] = String.Format("{0}={1}", sPair[0], UtilityFunctions.DecryptString(sPair[1]));
            }
            return String.Join(";", sPairs);
        }

        /// <summary>
        /// Riskmaster.Security.LoadData is the internal function Internal functions gets the values of various 
        /// class parameters from the passed reader object. Then checks those database  parameters by making a 
        /// connection string. Calls ChecksumMatches() and CRCMatches() function to check those parameters . 
        /// If these parameters not found matching it raises Riskmaster.ExceptionTypes.InvalidChecksumException. 
        /// </summary>
        /// <param name="reader">The native reader to wrap</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        protected void LoadData(DbReader reader)
        {
            m_DataSourceName = reader.GetString("DSN");
            m_DataSourceId = reader.GetInt("DSNID"); //DSNID;
            m_Status = reader.GetInt16("STATUS"); //STATUS;
            m_RMUserId = UtilityFunctions.DecryptString(reader.GetString("RM_USERID")); //RM_USERID[51];
            m_RMPassword = UtilityFunctions.DecryptString(reader.GetString("RM_PASSWORD")); //RM_PASSWORD[51];
            m_DbType = (eDatabaseType)reader.GetInt("DBTYPE"); //DBTYPE; --No longer maintained in DB -used only for checksum - get real type later from function.
            m_Checksum = reader.GetString("CHECKSUM");//CHECKSUM[51];
            m_OrgSecFlag = reader.GetInt("ORGSEC_FLAG");//ORGSEC_FLAG;
            m_ConnectionString = reader.GetString("CONNECTION_STRING"); //CONNECTION_STRING[1024];
            m_NumLicenses = reader.GetInt("NUM_LICENSES"); //NUM_LICENSES;
            m_LicUpdDate = Riskmaster.Common.Conversion.ToDate(reader.GetString("LIC_UPD_DATE")); //LIC_UPD_DATE[9];
            m_CRC2 = reader.GetString("CRC2"); //CRC2[129];
            m_DocPathType = reader.GetInt("DOC_PATH_TYPE"); //DOC_PATH_TYPE;
            m_GlobalDocPath = Utilities.ParseDocPathCredentials(reader.GetString("GLOBAL_DOC_PATH"), m_DocPathType); //GLOBAL_DOC_PATH[512];


            //create a DSN-less based connection string if the string only currently contains server & database information
            List<string> arrConnStrParams = new List<string>();

            arrConnStrParams.Add(m_ConnectionString);
            arrConnStrParams.Add(AuthenticationCredentials.UID + m_RMUserId + ";");
            arrConnStrParams.Add(AuthenticationCredentials.PWD + m_RMPassword + ";");

            //Use a standardized function to build the connection string
            //TODO: Create a standard helper function for these common connection string builder operations using the .Net ConnectionStringBuilder library
            m_ConnectionString = UtilityFunctions.BuildConnectionString(arrConnStrParams);


            if (m_bEnforceCheckSum)
            {
                if (!ChecksumMatches())
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    throw new Riskmaster.ExceptionTypes.InvalidChecksumException(AuthenticationCredentials.DatabaseNotFound);
            }
            if (!CRCMatches())
                //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                throw new Riskmaster.ExceptionTypes.InvalidChecksumException(AuthenticationCredentials.LicenseCheckFailed);

            m_DbType = (eDatabaseType)2;   // JP Force this to 2 (ODBC). We don't support access any longer.
            m_DataChanged = false;

            //Set up Database Type
            //This is now a RiskMaster.Db.Connection property.
            /*Added by Tanuj on 2-Sep-2005
             * The following 'if' loop checks the 'FindDBType' flag and accordingly executes. 
             * */
            if (m_bFindDBType)
            {
                DbConnection cn = null;
                try
                {
                    cn = DbFactory.GetDbConnection(m_ConnectionString);
                    cn.Open();
                    m_DbType = cn.DatabaseType;
                }
                finally { cn.Close(); }
            }

        }


        /*
// BSB 04.07.2003
// We don't want every client script kitty to be able
// to use this interface and change security table rows...
*/

        /// <summary>
        /// Riskmaster.Security.Save check for the flag true, than gets a DbWriter object using DbFactory 
        /// instance and invokes SaveData().
        /// </summary>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public void Save()//rkaur27 : Cloud Change
        {
            if (!m_DataChanged)
                return;

            DbWriter writer = DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(m_iClientId));
            writer.Connection = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            writer.Connection.Open();
            SaveData(writer);
            writer.Connection.Close();
        }


        /// <summary>
        /// Riskmaster.Security.SaveData check for the flag is true, this function updates the user record 
        /// in the database else creates a new one using writer.
        ///</summary>
        /// <param name="writer">The native writer to wrap</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public virtual void SaveData(DbWriter writer)
        {
            // Data Clean - No Save Required.
            if (!m_DataChanged)
                return;

            //Make Sure Writer is Clean for our Use - Empty the field, table collections etc.
            writer.Reset(true);

            if (m_DataSourceId == 0)
            {
                // Get the next unique ID
                m_DataSourceId = writer.Connection.ExecuteInt("SELECT MAX(DATA_SOURCE_TABLE.DSNID) FROM DATA_SOURCE_TABLE");
                if (m_DataSourceId == 0)
                    m_DataSourceId = 2;
                else
                    m_DataSourceId++;

            }
            else
                writer.Where.Add("DSNID=" + m_DataSourceId);

            writer.Tables.Add("DATA_SOURCE_TABLE");
            writer.Fields.Add("DSN", m_DataSourceName);
            writer.Fields.Add("DSNID", m_DataSourceId);
            writer.Fields.Add("STATUS", m_Status);
            writer.Fields.Add("RM_USERID", UtilityFunctions.EncryptString(m_RMUserId));
            writer.Fields.Add("RM_PASSWORD", UtilityFunctions.EncryptString(m_RMPassword));
            // BSB 07.18.2006 The DBTYPE value must be "2".  
            // This used to be 1 for Access or 2 for ODBC and there are utilities 
            // (particularly Reserve calculations) that fail when the value is not "2"
            //
            writer.Fields.Add("DBTYPE", 2); // --No longer a maintained part of the DB design -used only for checksum - get real type later from function.
            m_DbType = (eDatabaseType)2; // JP Force it. This is so hash gets created correctly.
            writer.Fields.Add("ORGSEC_FLAG", m_OrgSecFlag);
            writer.Fields.Add("CONNECTION_STRING", m_ConnectionString);
            writer.Fields.Add("NUM_LICENSES", m_NumLicenses);
            writer.Fields.Add("LIC_UPD_DATE", Conversion.ToDbDate(m_LicUpdDate));
            writer.Fields.Add("DOC_PATH_TYPE", m_DocPathType);
            writer.Fields.Add("GLOBAL_DOC_PATH", Utilities.EncryptDocPathCredentials(m_GlobalDocPath, m_DocPathType));
            writer.Fields.Add("CHECKSUM", GetHash());
            writer.Fields.Add("CRC2", GetCRC());
            //gagnihotri MITS 11995 Changes made for Audit table
            writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
            writer.Execute();
            m_DataChanged = false;
        }


        /// <summary>
        /// Riskmaster.Security.GrantLicense Internal function  checks  bSuppressLicense is true and NumLicenses 
        /// parameter is �1 else checks no of connection to the database is less then  no of connection license 
        /// granted . Returns false if any of above criteria fails.  Also if this license checking  mechanism 
        /// fails raises exception.
        /// </summary>
        /// <param name="sUserName">Username of the context user</param>
        /// <param name="bSuppressLicense">Flag whether to suppress license</param>
        internal bool GrantLicense(string sUserName, bool bSuppressLicense)
        {
            DbConnection db = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));//rkaur27 : Cloud Change
            DbReader rdr = null;
            DbWriter wr = null;
            bool ret = false;
            int numLicensesUsed = 0;

            if (this.NumLicenses == -1 || bSuppressLicense)
                return true;
            else
            {
                try
                {
                    db.Open();
                    rdr = db.ExecuteReader("SELECT * FROM MACHINE_LOGIN WHERE MACHINE_NAME='" + Environment.MachineName + "'");
                    if (rdr.Read())
                    {
                        wr = DbFactory.GetDbWriter(rdr, true);  //Note: Do not execute writer until reader is closed?
                        wr.Fields["USER_NAME"].Value = sUserName;
                        wr.Fields["DTTM_LOGIN"].Value = Conversion.ToDbDateTime(DateTime.Now);
                        //wr.Where.Add("MACHINE_NAME='" + Environment.MachineName + "'");
                        ret = true;
                    }
                    else
                    {
                        rdr.Close(); rdr = null;
                        // Problems on Oracle   numLicensesUsed = (int) db.ExecuteScalar("SELECT COUNT(*) FROM MACHINE_LOGIN");
                        numLicensesUsed = db.ExecuteInt("SELECT COUNT(*) FROM MACHINE_LOGIN");
                        if (numLicensesUsed >= this.NumLicenses)
                            ret = false;
                        else
                        {
                            ret = true;
                            wr = DbFactory.GetDbWriter(db.ConnectionString);
                            wr.Tables.Add("MACHINE_LOGIN");
                            wr.Fields.Add(new DbField("USER_NAME", sUserName));
                            wr.Fields.Add(new DbField("DTTM_LOGIN", Conversion.ToDbDateTime(DateTime.Now)));
                            wr.Fields.Add(new DbField("MACHINE_NAME", Environment.MachineName));
                        }
                    }
                    if (ret)
                        wr.Execute();
                }
                catch (Exception e)
                {
                    //Replaced general exception with LicenseCheckFailedException.Added by Tanuj Narula on 27/5/2004
                    throw new LicenseCheckFailedException(AuthenticationCredentials.LicenseCheckFailed, e);
                }
                finally
                {
                    if (rdr != null)
                        rdr.Close();
                    db.Close();
                }
                return ret;
            }

        }

        /// <summary>
        /// Riskmaster.Security.GetHash computes the hash code for the user information using Encryption.DTGCrypt32 object.
        /// </summary>
        private string GetHash()
        {
            byte[] dest = new byte[1 + m_RMUserId.Length + m_RMPassword.Length + 10];
            int cBytes = 0;

            dest[0] = Conversion.GetAscii(m_DataSourceName);
            cBytes = 1;
            Conversion.GetByteArr(m_RMUserId + m_RMPassword).CopyTo(dest, 1);
            cBytes = cBytes + m_RMUserId.Length + m_RMPassword.Length;

            Conversion.GetByteArr(m_DataSourceId).CopyTo(dest, cBytes);
            Conversion.GetByteArr((int)m_DbType).CopyTo(dest, cBytes + 4);
            Conversion.GetHILOByte(Convert.ToUInt16(m_Status), out dest[cBytes + 8], out dest[cBytes + 9]);

            return RMCryptography.ComputeHashAsString(dest);
        }


        /// <summary>
        /// Riskmaster.Security.ChecksumMatches
        /// </summary>
        internal bool ChecksumMatches()
        {
            string sHash = string.Empty;
            sHash = GetHash();
            return (sHash == m_Checksum);
        }

        /// <summary>
        /// Riskmaster.Security.GetCRC
        /// </summary>
        private string GetCRC()
        {
            byte[] buff = null;
            string sLicUpdDate = Conversion.ToDbDateTime(m_LicUpdDate).Substring(0, 8);
            string sDataSourceId = Convert.ToString(m_DataSourceId, 10);
            string sNumLicenses = Convert.ToString(m_NumLicenses, 10);

            buff = Conversion.GetByteArr(sDataSourceId + sNumLicenses + sLicUpdDate);
            return RMCryptography.ComputeHashAsString(buff);
        }

        /// <summary>
        /// Riskmaster.Security.CRCMatches matches the Crc code in the database  with the computed CRC code.
        /// </summary>
        /// <returns>Returns true if both matches else returns false.</returns>
        internal bool CRCMatches()
        {
            return (m_CRC2 == GetCRC());
        }
        #endregion

        

		/*
				internal virtual void SaveData(DbWriter writer)
				{
					if(!m_DataChanged)
						return;
					writer.Fields.Add("DSN",m_DataSourceName);
					writer.Fields.Add("DSNID",m_DataSourceId); //DSNID;
					writer.Fields.Add("STATUS",m_Status);//STATUS;
					writer.Fields.Add("RM_USERID",m_UserId); //RM_USERID[51];
					writer.Fields.Add("RM_PASSWORD",m_Password); //RM_PASSWORD[51];
					writer.Fields.Add("DBTYPE",m_DbType); //DBTYPE;
					writer.Fields.Add("CHECKSUM",m_Checksum);//CHECKSUM[51];
					writer.Fields.Add("ORGSEC_FLAG",m_OrgSecFlag);//ORGSEC_FLAG;
					writer.Fields.Add("CONNECTION_STRING",m_ConnectionString);//CONNECTION_STRING[1024];
					writer.Fields.Add("NUM_LICENSES",m_NumLicenses); //NUM_LICENSES;
					writer.Fields.Add("LIC_UPD_DATE",m_LicUpdDate); //LIC_UPD_DATE[9];
					writer.Fields.Add("CRC2",m_CRC2); //CRC2[129];
					writer.Fields.Add("DOC_PATH_TYPE",m_DocPathType); //DOC_PATH_TYPE;
					writer.Fields.Add("GLOBAL_DOC_PATH",m_GlobalDocPath); //GLOBAL_DOC_PATH[512];
					m_DataChanged=false;

				}
		*/
		
    
/*
		private Db.eDatabaseType GetDBMake()
		{
			// return: 0==ACCESS, 1==MS SQL Server, 2==Sybase, 3==Informix, 4==generic ODBC (unknown)

		Db.DbConnection db =  Factory.GetDbConnection(m_ConnectionString);
		if (db.ConnectionType)

			char[] tempstr = new char[50];
		WORD wMake = DBMS_IS_ODBC;

		tempstr[0] = 0x00;
		if (::SQLGetInfo(hdbc, SQL_DBMS_NAME, tempstr, 49, NULL) == SQL_SUCCESS)
		{
		strupr(tempstr);

		if (!stricmp(tempstr,"Microsoft SQL Server")) wMake = DBMS_IS_SQLSRVR;
		else if (!stricmp(tempstr, "SQL Server")) wMake = DBMS_IS_SYBASE;
		else if (!stricmp(tempstr, "Informix")) wMake = DBMS_IS_INFORMIX;
		else if (!stricmp(tempstr, "ACCESS")) wMake = DBMS_IS_ACCESS;
		else if (strstr(tempstr, "ORACLE")) wMake = DBMS_IS_ORACLE;
		else if (strstr(tempstr, "DB2")) wMake = DBMS_IS_DB2;
		}
		else
		wMake = DBMS_IS_ODBC;
			
		return wMake;
		}
*/	}
}
