using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Runtime.Serialization;
using System.Configuration;
using System.Collections.Generic;
using System.Reflection;


namespace Riskmaster.Security.Authentication
{
    [DataContract]
    /// <summary>
    /// Riskmaster.Security.SecurityDatabase class is designed for the database security, 
    /// It check for the database security on the basis of autorization key and code access public key
    /// </summary>
    public class SecurityDatabase
    {

        private static int FUNC_LIST_SIZE = Convert.ToInt32(AuthenticationCredentials.FUNCTION_LIST_SIZE);
        //BSB 08.05.2005 Set up a static hash table cache for the parent relations of the 
        // Function List entries from the Security Database.
        // This will be used to avoid passing "parent" security id's around FDM and closes a security
        // hole based on that pattern.
        private static Hashtable m_FuncHashTable = null;
        static SecurityDatabase()
        {
            //Initialize the value of the Security Database DSN
            SecurityDatabaseManager.CreateSecurityDatabase();
        } // method: SecurityDatabase

        [DataMember]
        /// <summary>
        /// Riskmaster.Security.Dsn check for the security permission on the basis of PublicKey provided. 
        /// It gets the value from registry, pick up the database option, decrypt the values.
        /// </summary>
        /// <returns>It returns the DSN name as a string</returns>
        /// <remarks>The current implementation is too highly coupled with the Riskmaster.config file for
        /// retrieving security credentials/application information especially since this is implemented 
        /// as a static property.  Because this is additionally implemented statically, it will also be
        /// invisible to COM libraries thus making it unavailable to legacy applications.  In order for this 
        /// library to be usable by both .Net and COM libraries, the entire Riskmaster.Security library
        /// needs to be implemented based on instance methods rather than static methods and properties
        /// </remarks>
        public static string Dsn
        {
            get;
            set;
        }//property Dsn


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSecurityDsn
        /// </summary>     
        public static string GetSecurityDsn()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSecurityDsn(0);
            }
            else
                throw new Exception("GetSecurityDsn" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static string GetSecurityDsn(int p_iClientId)
        {
            return SecurityDatabaseManager.GetSecDsn(p_iClientId);
        }
        /// <summary>
        /// 
        /// </summary>
        private static void LoadFuncHashTable(int p_iClientId)
        {
            if (m_FuncHashTable != null)
                return;
            else
                m_FuncHashTable = new Hashtable(FUNC_LIST_SIZE);

            string SQL = "SELECT FUNC_ID, PARENT_ID FROM FUNCTION_LIST";

            using (DbReader rdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(p_iClientId), SQL))
            {
                try
                {
                    if (rdr.Read())
                    {
                        do
                        {
                            try { m_FuncHashTable.Add(rdr.GetInt("FUNC_ID"), rdr.GetInt("PARENT_ID")); }
                            catch (Exception) { ;}
                        }
                        while (rdr.Read());
                    }
                }
                catch (Exception e)
                {
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    //Replaced general exception with SecuritySettingException.Added by Tanuj Narula on 27/5/2004
                    throw new SecuritySettingException(AuthenticationCredentials.FunctionListNotFound, e);
                }
                finally
                {
                    if (rdr != null)
                        rdr.Close();
                }
            }
        }

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetParentFuncId
        /// </summary>     
        public static int GetParentFuncId(int funcId)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetParentFuncId(funcId, 0);
            }
            else
                throw new Exception("GetParentFuncId" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// 
        /// </summary>
        /// <param name="funcId"></param>
        /// <returns></returns>
        public static int GetParentFuncId(int funcId, int p_iClientId)
        {
            if (m_FuncHashTable == null)
            {
                LoadFuncHashTable(p_iClientId);
            } // if
            return (int)(m_FuncHashTable[funcId]);
        }
    }//class
    public class SecurityDatabaseManager
    {
        internal static string AssemblyPath
        {
            get
            {
                Assembly currentAssembly = Assembly.GetExecutingAssembly();

                string strAssemblyLocation = currentAssembly.Location;

                return strAssemblyLocation;
            }//get
        }//internal

        /// <summary>
        /// Assigns the DSN for the Security Database
        /// </summary>
        /// to the RISKMASTER Security Database</param>
        /// Making this function public as this can be used from outside the riskmaster solution.
        public static void CreateSecurityDatabase()
        {
            string strSecConnString = string.Empty;

            try
            {
                ConnectionStringSettings connSettings = ConfigurationManager.ConnectionStrings["RMXSecurity"];

                if (connSettings != null)
                {
                    strSecConnString = connSettings.ConnectionString;
                }//if
                //Try an alternative to retrieve the connection string based on the current assembly path
                else
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(AssemblyPath);

                    ConnectionStringsSection connStrings = config.GetSection("connectionStrings") as ConnectionStringsSection;

                    strSecConnString = connStrings.ConnectionStrings["RMXSecurity"].ConnectionString;
                }//else


                //Set the security database connection string
                SecurityDatabase.Dsn = strSecConnString;
            }//try
            catch (ConfigurationErrorsException ex)
            {
                //throw new ConfigurationErrorsException("Security database connection string could not be found.  Please verify your settings.", ex);
            }//catch
        } // method: CreateSecurityDatabase


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSecDsn
        /// </summary>     
        public static string GetSecDsn()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSecDsn(0);
            }
            else
                throw new Exception("GetSecDsn" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static string GetSecDsn(int p_iClientId)
        {
            if (p_iClientId == 0) return SecurityDatabase.Dsn;
            string strSecConnString = Riskmaster.Cache.ConfigurationInfo.GetSecurityConnectionString(p_iClientId);
            try
            {
                if (string.IsNullOrEmpty(strSecConnString))
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(AssemblyPath);
                    ConnectionStringsSection connStrings = config.GetSection("connectionStrings") as ConnectionStringsSection;
                    string sBaseConnString = connStrings.ConnectionStrings["rmATenantSecurity"].ConnectionString;
                    Dictionary<string, int> objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", p_iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", "RMXSecurity", "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    strSecConnString = Convert.ToString(objClientConnString);
                }
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException(Globalization.GetString("SecurityConfig.Error", p_iClientId), ex);
            }
            return strSecConnString;
        }
    }

}//namespace
