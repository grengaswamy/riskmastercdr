﻿using System;
using System.IdentityModel.Selectors;
using System.Web.Security;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Provides extension methods to allow custom user name/password
    /// credentials to be passed to Web Service calls
    /// </summary>
    public class CustomUserNameValidator : UserNamePasswordValidator
    {
        public CustomUserNameValidator()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public bool IsAuthenticated { get; internal set; }

        public override void Validate(string userName, string password)
        {
            MembershipProvider membProvider = Membership.Providers["RiskmasterMembershipProvider"];
            bool blnIsAuthenticated = false;


            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("Username or password credentials were not provided.");
            } // if
            else
            {
                blnIsAuthenticated = membProvider.ValidateUser(userName, password);
            } // else

            this.IsAuthenticated = blnIsAuthenticated;
        }
    } 
}
