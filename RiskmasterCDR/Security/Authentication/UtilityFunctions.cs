﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Riskmaster.Common;
using Riskmaster.Security.Encryption;
using System.Reflection;
using Riskmaster.Db;

namespace Riskmaster.Security.Authentication
{

    /// <summary>
    /// Provides a set of internal utility functions
    /// for managing common actions needed for security
    /// </summary>
    internal class UtilityFunctions
    {

        /// Gets the CodeAccess public key used for Security Permissions
        /// </summary>
        internal const string CODE_ACCESS_PUBLICKEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
        

        /// <summary>
        /// Gets a connection string delimter
        /// </summary>
        public static string ConnectionStringDelimiter 
        {
            get
            {
                const string DELIMITER = ";";
                return DELIMITER;
            }//get
        }//property: ConnectionStringDelimiter

        /// <summary>
        /// Checks whether the particular string is an empty string
        /// </summary>
        /// <param name="strValue">string to check and compare for empty or null values</param>
        /// <returns>boolean indicating whether or not the string is empty or null</returns>
        internal static bool IsEmptyString(string strValue)
        {
            if (string.IsNullOrEmpty(strValue))
            {
                return true;
            } // if
            else
            {
                return false;
            } // else
        } // method: IsEmptyString


        [Obsolete("This method should not be used since these settings should be stored in the database.")]
        /// <summary>
        /// Retrieves the appropriate Riskmaster.config settings from Riskmaster.config
        /// </summary>
        /// <returns>StringDictionary containing possible Riskmaster.config Document Path settings</returns>
        public static StringDictionary PopulateRMConfigSettings()
        {
            //StringDictionary dictRMConfigSettings = new StringDictionary();

            //if (RMConfigurator.NamedNode("OverrideDocumentPath") != null)
            //{
            //    if (RMConfigurator.Value("OverrideDocumentPath").Trim() == "true")
            //    {
            //        dictRMConfigSettings.Add("DocumentPath", RMConfigurator.Value("StorageDestination").Trim());
            //        dictRMConfigSettings.Add("GlobalDocumentPath", RMConfigurator.Value("StorageDestination").Trim());

            //        if (RMConfigurator.Value("StorageType").Trim() == "FileSystem")
            //        {
            //            dictRMConfigSettings.Add("DocPathType", "0");
            //        }//if
            //        else
            //        {
            //            dictRMConfigSettings.Add("DocPathType", "1");
            //        }//else
            //    }//if
            //}//if

            //return dictRMConfigSettings;
            throw new NotImplementedException();
        }//method: PopulateRMConfigSettings()

        /// <summary>
        /// Builds a database/DSN connection string given the specified parameters
        /// </summary>
        /// <param name="arrConnStrParams">generic string list containing all of the 
        /// connection string parameters</param>
        /// <returns>complete database connection string</returns>
        public static string BuildConnectionString(List<string> arrConnStrParams)
        {
            StringBuilder strBldrConnStr = new StringBuilder();

            foreach (string strConnParam in arrConnStrParams)
            {
                if (!string.IsNullOrEmpty(strConnParam))
                {
                    strBldrConnStr.Append(strConnParam);
                }//method
            }//foreach

            return strBldrConnStr.ToString();
        }//method: BuildConnectionString()

        /// <summary>
        /// Returns a Decrypted string using the DTGCrypt library
        /// </summary>
        /// <param name="strEncrypted">string containing the currently encrypted value</param>
        /// <returns>string containing the decrypted value</returns>
        public static string DecryptString(string strEncrypted)
        {
            string strDecrypted = string.Empty;

            strDecrypted = RMCryptography.DecryptString(strEncrypted);

            return strDecrypted;
        } // method: DecryptString

        /// <summary>
        /// Returns a Encrypted string using the DTGCrypt library
        /// </summary>
        /// <param name="strDecrypted">string containing the currently decrypted value</param>
        /// <returns>string containing the encrypted value</returns>
        public static string EncryptString(string strDecrypted)
        {
            string strEncrypted = string.Empty;            

            strEncrypted = RMCryptography.EncryptString(strDecrypted);

            return strEncrypted;
        } // method: EncryptString

        
        #region User Methods
        /// Computes the hash code for the user information using Encryption.DTGCrypt32 object.
        /// </summary>
        /// <returns>It returns the object in encrypted form</returns>
        internal static string CalculateCheckSum(RiskmasterUser objRMUser)
        {
            byte[] byteArr = new byte[24];
            ushort iHI, iLO;

            byteArr[0] = Conversion.GetAscii(objRMUser.LastName);
            byteArr[1] = Conversion.GetAscii(objRMUser.FirstName);
            byteArr[2] = Conversion.GetAscii(objRMUser.Email);
            byteArr[3] = Conversion.GetAscii(objRMUser.Address1);
            byteArr[4] = Conversion.GetAscii(objRMUser.Address2);
            byteArr[5] = Conversion.GetAscii(objRMUser.City);
            byteArr[6] = Conversion.GetAscii(objRMUser.HomePhone);
            byteArr[7] = Conversion.GetAscii(objRMUser.OfficePhone);
            byteArr[8] = Conversion.GetAscii(objRMUser.ZipCode);
            byteArr[9] = Conversion.GetAscii(objRMUser.Country);
            byteArr[10] = Conversion.GetAscii(objRMUser.State);
            byteArr[11] = Conversion.GetAscii(objRMUser.Title);

            // Get Manager ID
            Conversion.GetHILOWord(objRMUser.ManagerId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[12], out byteArr[13]);
            Conversion.GetHILOByte(iHI, out byteArr[14], out byteArr[15]);

            // Get USER ID
            Conversion.GetHILOWord(objRMUser.UserId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[16], out byteArr[17]);
            Conversion.GetHILOByte(iHI, out byteArr[18], out byteArr[19]);

            // And NLS Code
            Conversion.GetHILOWord(objRMUser.NlsCode, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[20], out byteArr[21]);
            Conversion.GetHILOByte(iHI, out byteArr[22], out byteArr[23]);

            return RMCryptography.ComputeHashAsString(byteArr);
        }//method: CalculateCheckSum

        /// <summary>
        /// Riskmaster.Security.ChecksumMatches internal method compares the stored checksum attribute that with a newly computed hash value for the user attributes
        /// </summary>
        /// <returns>It returns computed hash value.</returns>
        internal bool ChecksumMatches(RiskmasterUser objRMUser)
        {
            string sHash = string.Empty;
            sHash = CalculateCheckSum(objRMUser);
            return sHash.Equals(objRMUser.CheckSum);
        }//method: ChecksumMatches()

        /// <summary>
        /// Compares the properties of two objects
        /// through the use of Reflection
        /// </summary>
        /// <typeparam name="T">Generic Type for a class instance</typeparam>
        /// <param name="objCompare1">object class instance</param>
        /// <param name="objCompare2">object class instance for comparison</param>
        /// <returns>boolean indicating whether or not the properties for 
        /// each object exactly match</returns>
        public static bool CompareObjects<T>(T objCompare1, T objCompare2)
        {
            int intCompareValue = 0;
            bool blnAreEqual = true;

            //Get type information for the class instance
            Type t = objCompare1.GetType();

            //Use Reflection to obtain all of the available properties on the class
            PropertyInfo[] objProps = t.GetProperties();

            //Iterate through all of the properties in the class
            foreach (PropertyInfo objProp in objProps)
            {
                //Cast to IComparable to allow comparison of each property value
                IComparable valx = objProp.GetValue(objCompare1, null) as IComparable;

                if (valx == null)
                {
                    continue;
                } // if

                //Obtain the property for the second class instance
                object valy = objProp.GetValue(objCompare2, null);

                //Compare the 2 values to verify they are equal
                intCompareValue = valx.CompareTo(valy);

                //Determine if the classes are equal
                if (intCompareValue !=0)
                {
                    blnAreEqual = false;
                    break;
                } // if

            } // foreach

            //return the boolean value for comparison of the objects
            return blnAreEqual;
        }//method: CompareObjects

        /// <summary>
        /// Updates the appropriate table to allow auditing authentication/login events
        /// </summary>
        /// <param name="strSecConnStr">string containing the Security database connection string</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's encrypted password</param>
        /// <param name="intUserID">integer containing the user's current User ID</param>
        /// <param name="blnIsAuthenticated">boolean indicating whether or not the user was successfully authenticated</param>
        public static void AuditUserLogins(string strSecConnStr, string strUserName, string strPassword,
            int intUserID, bool blnIsAuthenticated)
        {
            Dictionary<string, object> dictParams = new Dictionary<string,object>();
            int STATUS_CODE = 1; //successful authentication
            string strLoginDate = Conversion.ToDbDateTime(DateTime.Now);
            
            //If the user was successfully authenticated
            if (! blnIsAuthenticated)
            {
                STATUS_CODE = 0;  //failed authentication

                //Add the parameters
                dictParams.Add("USER_LOGIN", strUserName);
                dictParams.Add("USER_PASSWORD", strPassword);
                dictParams.Add("LOGIN_DATE", strLoginDate);
                
                //Consecutively execute the queries to update the login audit trail
                DbFactory.ExecuteNonQuery(strSecConnStr, AuthenticationRepository.UpdateUserLoginFailure(), dictParams);
            }//if

            //Clear the existing set of parameters (if any)
            dictParams.Clear();

            //Add the parameters
            dictParams.Add("USER_ID", intUserID);
            dictParams.Add("LOGIN_DATE", strLoginDate);
            dictParams.Add("LOGIN_STATUS", STATUS_CODE);

            //Consecutively execute the queries to update the login audit trail
            DbFactory.ExecuteNonQuery(strSecConnStr, AuthenticationRepository.UpdateLoginHistory(), dictParams);

            //Clear the existing set of parameters
            dictParams.Clear();

            dictParams.Add("USER_ID", intUserID);
            dictParams.Add("USER_LOGIN", strUserName);
            dictParams.Add("LOGIN_DATE", strLoginDate);

            DbFactory.ExecuteNonQuery(strSecConnStr, AuthenticationRepository.UpdateUserLoginTrack(), dictParams);

            
        }//method: UpdateLoginHistory()

        #endregion

        /// <summary>
        /// Determines whether or not Enhanced Security has been 
        /// enabled through the Configuration file
        /// </summary>
        /// <returns>boolean indicating whether or not Enhanced Security was enabled</returns>
        public static bool IsEnhancedSecurityEnabled()
        {
            bool bSuccess = false;
            return Conversion.CastToType<bool>(RMConfigurationManager.GetAppSetting("PasswordPolicy") , out bSuccess);
        }



        /// <summary>
        /// Gets the Unique ID 
        /// </summary>
        /// <param name="p_sTableName"></param>
        /// <returns></returns>
        public static int GetNextRecordId(string p_secConnectionString, string p_sTableName)
        {
            bool blnSuccess = false;
            int iNextRecordId = 1; //Default the value to 1 to always allow inserting 1st record
            
            object objRecordId = DbFactory.ExecuteScalar(p_secConnectionString, string.Format("Select Max(Record_Id) + 1 RecordId from {0}", p_sTableName));

            if (objRecordId != DBNull.Value)
            {
                //Increment the value of the Record ID
                iNextRecordId = Conversion.CastToType<int>(objRecordId.ToString(), out blnSuccess) + 1;
            } // if

            return iNextRecordId;
        }

        /// <summary>
        /// Maps the current day of the week to their corresponding
        /// columns in the USER_DETAILS_TABLE
        /// </summary>
        /// <returns>Generic string List collection containing 
        /// the mapped/mathing column names</returns>
        public static List<string> MapWeekdayColumns()
        {
            //Get the 3 letter code abbreviation for the day of the week
            string strDayOfWeek = DateTime.Now.ToString("ddd").ToUpper();

            List<string> arrColumnNames = new List<string>();
            List<string> arrMappedColumns = new List<string>();

            arrColumnNames.AddRange(new string[] {"SUN_START", "SUN_END",
            "MON_START", "MON_END", "TUES_START", "TUES_END", 
            "WEDS_START", "WEDS_END", "THURS_START", "THURS_END",
            "FRI_START", "FRI_END", "SAT_START", "SAT_END"});

            foreach (string  strColumnName in arrColumnNames)
            {
                if (strColumnName.Contains(strDayOfWeek))
                {
                    //Add the specified column to the mapped columns collection
                    arrMappedColumns.Add(strColumnName);
                } // if

            }//foreach

            return arrMappedColumns;
        }//method: MapWeekdayColumns

        /// <summary>
        /// Gets the current time of day in 24-hour military time format
        /// </summary>
        /// <returns>string containing the current time in 24-hour format</returns>
        public static string GetCurrentTimeOfDay()
        {
            //Get the current time of day in 24-hour format hour minute seconds
            //Ex:173130 i.e. 5:31:30 P.M.
            return DateTime.Now.ToString("HHmmss"); 
        } // method: GetCurrentTimeOfDay


        public static bool VerifyExistingPassword(string p_sLoginName, string p_sOldPassword, out string p_sCurrentPasswordInDB, int p_iClientId)
        {
            bool bRetVal = false;
            DbReader objReader = null;
            string sCurrentPasswordInDB = string.Empty, sCurrentPasswordFrmUser = string.Empty;
            p_sCurrentPasswordInDB = string.Empty;

            try
            {

                //MITS-10662 - Replaced the ' with '' for SQL query to run properly
                //objReader = DbFactory.GetDbReader( SecurityDatabase.GetSecurityDsn(0) ,"Select * from User_Details_Table where Login_Name = '"+ sLoginName +"'") ;
                sCurrentPasswordFrmUser = p_sOldPassword;

                objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), String.Format("Select * from User_Details_Table where Login_Name = '{0}'", p_sLoginName.Replace("'", "''")));
                if (objReader.Read())
                {
                    sCurrentPasswordInDB = RMCryptography.DecryptString(objReader.GetString("PASSWORD"));
                    objReader.Close();
                }
                else
                {
                    //throw new RMAppException(String.Format(Globalization.GetString("UserLoginsAdaptor.VerifyExistingPassword.LoginNameError"), sLoginName));
                }
                if (sCurrentPasswordInDB == sCurrentPasswordFrmUser)
                {
                    bRetVal = true;
                    //Raman: MITS 12315
                    p_sCurrentPasswordInDB = sCurrentPasswordInDB;
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }
            return bRetVal;
        }

        /// Name		: IsPwdPolEnabled
        /// Author		: Raman Bhatia
        /// Date Created	: 30 Oct 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the password policy is enforced or not.
        /// It will read this from Config file.
        /// </summary>
        /// <returns></returns>
        public static bool IsPwdPolEnabled()
        {
            bool bSuccess = false;
            return Conversion.CastToType<bool>(RMConfigurationManager.GetAppSetting("PasswordPolicy"), out bSuccess);
        }

    }
}

