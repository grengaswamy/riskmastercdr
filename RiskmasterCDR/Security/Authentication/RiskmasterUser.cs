using System;
using System.Security.Principal;
using Riskmaster.Common;
using System.Text;
using System.Collections.Generic;
using System.Web.Security;
using System.Runtime.Serialization;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security.Encryption;


namespace Riskmaster.Security.Authentication
{
    #region Base Class Implementation

    [DataContract]
    /// <summary>
    /// Riskmaster.Security.User class classifies User�s details on context of Riskmaster, 
    /// it contain all the information about the user like Name,City,Country Phone etc..
    /// </summary>
    public partial class RiskmasterUser 
    {
        private string m_Dsn = string.Empty;   
        private string AUTHKEY = AuthenticationCredentials.AUTH_CODE;
        private string m_sUserName = string.Empty;    //gagnihotri MITS 11995 Changes made for Audit table
        private GenericIdentity m_GenIdentity = null;


        #region Public Serializable Properties
        [DataMember]
        /// <summary>
        /// Gets and sets the User's ID
        /// </summary>
        public int UserId
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's title
        /// </summary>
        public string Title
        {
            get;
            set;
        }


        [DataMember]
        /// <summary>
        /// Gets and sets the User's First Name
        /// </summary>
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Last Name
        /// </summary>
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Primary Address
        /// </summary>
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Secondary Address
        /// </summary>
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's City
        /// </summary>
        public string City
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's State
        /// </summary>
        public string State
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Zip Code
        /// </summary>
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public string Country
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's e-mail address
        /// </summary>
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Home Phone Number
        /// </summary>
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Office Phone Number
        /// </summary>
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Company Name
        /// </summary>
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's NlsCode
        /// </summary>
        public int NlsCode
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the DSN name
        /// for the currently authenticated User
        /// </summary>
        public string Dsn
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the DSN ID
        /// for the currently authenticated User
        /// </summary>
        public int DsnID
        {
            get;
            set;
        } // property DsnID

        [DataMember]
        /// <summary>
        /// Gets and sets the User CheckSum
        /// </summary>
        /// <remarks>CheckSum information
        /// is used to verify if user information
        /// has been altered outside the system</remarks>
        internal string CheckSum { get; set; }


        [DataMember]
        /// <summary>
        /// Gets and sets whether or not to 
        /// enforce CheckSum constraints
        /// on User changes
        /// </summary>
        public bool EnforceCheckSum
        {
            get;
            set;
        }


        #region Serializable Manager Information
        [DataMember]
        /// <summary>
        /// Gets and sets the User's Manager
        /// </summary>
        public RiskmasterUser Manager
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the Manager's ID
        /// </summary>
        public int ManagerId
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the Manager's e-mail address
        /// </summary>
        public string ManagerEmail
        {
            get;
            set;
        }

        #endregion //Manager Serializable properties
        #endregion //Serializable properties

    } 
    #endregion

    public partial class RiskmasterUser : IIdentity, IPrincipal
    {
        #region IIdentity Members

        /// <summary>
        /// Gets the AuthenticationType used to authenticate the user
        /// </summary>
        public string AuthenticationType
        {
            get
            {
                //TODO: Pass the authentication type as part of the selected Membership Provider
                return "Forms";
            }
        }

        /// <summary>
        /// Gets whether or not the user has been authenticated against the system
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
                //TODO: Determine if the user has been logged out of the system
                return true;
            }
        }

        [DataMember]
        /// <summary>
        /// Gets the user name
        /// </summary>
        public string Name
        {
            get
            {
                return this.m_sUserName;
            }

            set
            {
                this.m_sUserName = value;
            }
        }

        #endregion



        #region IPrincipal Members

        /// <summary>
        /// Gets the Identity for the User
        /// </summary>
        public IIdentity Identity
        {
            get 
            {
                m_GenIdentity = new GenericIdentity(this.Name);
                return m_GenIdentity;
            }
        }

        /// <summary>
        /// Determines whether the user is a member of a particular role
        /// </summary>
        /// <param name="role">string containing the name of the group name</param>
        /// <returns>boolean indicating whether or not the user is a member 
        /// of the specified group</returns>
        public bool IsInRole(string role)
        {
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            bool blnIsInRole = false;



            strSQL.Append("SELECT GROUP_NAME ");
            strSQL.Append("FROM USER_GROUPS UG ");
            strSQL.Append("INNER JOIN USER_MEMBERSHIP UM ");
            strSQL.Append("ON UG.GROUP_ID = UM.GROUP_ID ");
            strSQL.Append("WHERE UM.USER_ID = ~USERID~ ");
            strSQL.Append("AND UG.GROUP_NAME = ~GROUPNAME~");

            //Obtain a handle to the Riskmaster Database
            //RiskmasterDatabase rmDb = new RiskmasterDatabase(this.DsnID);

            strDictParams.Add("USERID", this.UserId.ToString());
            strDictParams.Add("GROUPNAME", role);

            //TODO: Verify that the actual/correct Riskmaster Database Connection string is retrieved
            //object objGroupName = DbFactory.ExecuteScalar(rmDb.ConnectionString, strSQL.ToString(), strDictParams);

            object objGroupName = null;

            //If the object value is not empty
            if (!string.IsNullOrEmpty(objGroupName.ToString()))
            {
                blnIsInRole = true;
            } // if

            return blnIsInRole;
        }

        #endregion
    }//class 

    public partial class RiskmasterUser : MembershipUser
    {

        /// <summary>
        /// NOTE: Need to provide a parameterless constructor
        /// for the default XML Serialization to work properly
        /// </summary>
        private RiskmasterUser()
        {
            
        } // method: RiskmasterUser



        public RiskmasterUser(string strUserName)
        {
            m_sUserName = strUserName;
        } // constructor


        /// <summary>
        /// OVERLOADED constructor that inherits 
        /// attributes from the MembershipUser 
        /// base class
        /// </summary>
        /// <param name="mu">base MembershipUser instance</param>
        public RiskmasterUser(MembershipUser mu):
        
            base(mu.ProviderName, mu.UserName, mu.ProviderUserKey,
                mu.Email, mu.PasswordQuestion, mu.Comment, mu.IsApproved,
                mu.IsLockedOut, mu.CreationDate, mu.LastLoginDate,
                mu.LastActivityDate, mu.LastPasswordChangedDate, mu.LastLockoutDate)
        {
        } // constructor

        /// <summary>
        /// Gets the UserName for the currently authenticated User
        /// </summary>
        public override string UserName
        {
            get
            {
                return m_sUserName;
            }

        }

        public override string Comment
        {
            get
            {
                return base.Comment;
            }
            set
            {
                base.Comment = value;
            }
        }

        public override DateTime CreationDate
        {
            get
            {
                return base.CreationDate;
            }
        }

        public override string GetPassword()
        {
            //TODO: retrieve the user's password
            return "MyPassword";
        }

        public override bool IsLockedOut
        {
            get
            {
                return base.IsLockedOut;
            }
        }

        [DataMember]
        public override DateTime LastActivityDate
        {
            get
            {
                return base.LastActivityDate;
            }
            set
            {
                base.LastActivityDate = value;
            }
        }

    
        public override DateTime LastLockoutDate
        {
            get
            {
                return base.LastLockoutDate;
            }
        }

        public override bool IsApproved
        {
            get
            {
                return base.IsApproved;
            }
            set
            {
                base.IsApproved = value;
            }
        }

        [DataMember]
        public override DateTime LastLoginDate
        {
            get
            {
                return base.LastLoginDate;
            }
            set
            {
                base.LastLoginDate = value;
            }
        }

        public override DateTime LastPasswordChangedDate
        {
            get
            {
                return base.LastPasswordChangedDate;
            }
        }

        
        public override string ProviderName
        {
            get
            {
                return base.ProviderName;
            }
        }

        public override object ProviderUserKey
        {
            get
            {
                return base.ProviderUserKey;
            }
        }

        

    }//class: RiskmasterUser

    /// <summary>
    /// Riskmaster.Security.User class classifies User�s details on context of Riskmaster, 
    /// it contain all the information about the user like Name,City,Country Phone etc..
    /// </summary>
    public partial class RiskmasterUser : IRiskmasterUserLogin
    {
        private string m_LoginName = string.Empty, m_Password = string.Empty;
        private DateTime m_PrivilegesExpire = System.DateTime.MinValue;
        private DateTime m_PasswordExpire = System.DateTime.MinValue;
        private string m_DocumentPath = string.Empty, m_CheckSum = string.Empty;
        private string m_SunStart = string.Empty, m_SunEnd = string.Empty;
        private string m_MonStart = string.Empty, m_MonEnd = string.Empty;
        private string m_TueStart = string.Empty, m_TueEnd = string.Empty;
        private string m_WedStart = string.Empty, m_WedEnd = string.Empty;
        private string m_ThuStart = string.Empty, m_ThuEnd = string.Empty;
        private string m_FriStart = string.Empty, m_FriEnd = string.Empty;
        private string m_SatStart = string.Empty, m_SatEnd = string.Empty;
        //private RiskmasterDatabase m_objRMDB = new RiskmasterDatabase();

        //gagnihotri MITS 11995 Changes made for Audit table End

        // JP 6/15/2005    Moved out of UserLogin because this is RM specific and not needed by AC.
        //		private UserLoginLimits m_objLimits = null;
        private Hashtable m_SecHashTable = null;
        private Hashtable m_SecSuppHashTable = null;
        private int m_GroupId = 0, m_DatabaseId = 0;



        #region Public Serializable properties

        [DataMember]
        /// <summary>
        /// Gets and sets the User's currently logged
        /// in Riskmaster Database
        /// </summary>
        public RiskmasterDatabase RiskmasterUserDatabase
        {
            get;
            set;
        } // property RiskmasterUserDatabase


        /// <summary>
        /// Gets whether or not the server is 
        /// running Terminal Services
        /// </summary>
        /// <remarks>Not sure why this is needed
        /// or if it properly populates based on OS configuration</remarks>
        public bool IsTerminalServer
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets whether or not the User is a member
        /// of the BES Administrator's group
        /// </summary>
        public bool IsOrgSecAdmin
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's DatabaseId
        /// </summary>
        public int DatabaseId
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's GroupId
        /// </summary>
        public int GroupId
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Login Name
        /// </summary>
        public string LoginName
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's password
        /// </summary>
        public string Password
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets whether or not the user 
        /// is required to change his/her password
        /// </summary>
        /// <remarks>This should already be handled
        /// by the Membership Provider</remarks>
        public int ForceChangePwd
        {
            get;
            set;
        }

        [DataMember]
        /// <summary>
        /// Gets and sets whether or not
        /// the User's Privileges will expire
        /// </summary>
        public DateTime PrivilegesExpire
        {
            get { return m_PrivilegesExpire; }
            set
            {
                if (!DateTime.Equals(m_PrivilegesExpire, value))
                {
                    m_PrivilegesExpire = value;
                }
            }
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Password Expiration Date
        /// </summary>
        public DateTime PasswordExpire
        {
            get { return m_PasswordExpire; }
            set
            {
                if (!DateTime.Equals(m_PasswordExpire, value))
                {
                    m_PasswordExpire = value;

                }
            }
        }

        [DataMember]
        /// <summary>
        /// Gets and sets the User's Document Path
        /// </summary>
        public string DocumentPath
        {
            get;
            set;
        }



        [DataMember]
        public string SunStart
        {
            get;
            set;

        }

        [DataMember]
        public string SunEnd
        {
            get;
            set;
        }

        [DataMember]
        public string MonStart
        {
            get;
            set;
        }

        [DataMember]
        public string MonEnd
        {
            get;
            set;
        }

        [DataMember]
        public string TueStart
        {
            get;
            set;
        }

        [DataMember]
        public string TueEnd
        {
            get;
            set;
        }

        [DataMember]
        public string WedStart
        {
            get;
            set;
        }

        [DataMember]
        public string WedEnd
        {
            get;
            set;
        }

        [DataMember]
        public string ThuStart
        {
            get;
            set;
        }

        [DataMember]
        public string ThuEnd
        {
            get;
            set;
        }

        [DataMember]
        public string FriStart
        {
            get;
            set;
        }

        [DataMember]
        public string FriEnd
        {
            get;
            set;
        }

        [DataMember]
        public string SatStart
        {
            get;
            set;

        }

        [DataMember]
        public string SatEnd
        {
            get;
            set;
        }

        // ******************************************************************************
        //  Database specific properties (wrapped from RiskmasterDatabase object.)
        // ******************************************************************************		

        ///// <summary>
        ///// Riskmaster.Security.RMDatabase is the read only property it takes accessor for Riskmaster.Security.RiskmasterDatabase
        ///// </summary>
        ///// <returns>It returns object of riskmaster securuty database</returns>
        //public  RiskmasterDatabase RMDatabase
        //{
        //    get{ return m_objRMDB;}
        //}	 
        #endregion


        #region RiskmasterUserLogin methods
        /// <summary>
        /// Riskmaster.Security.PopulateSecurityHashTable returns Hashtable object for group permissions associated with the user
        /// </summary>
        /// <returns>It returns the hash table object for group permission</returns>
        private Hashtable PopulateSecurityHashTable()
        {
            //DbConnection db = DbFactory.GetDbConnection(m_objRMDB.ConnectionString);
            DbConnection db = DbFactory.GetDbConnection(string.Empty);
            DbReader rdr = null;
            Hashtable ret = new Hashtable();
            string sSQL = "SELECT GROUP_PERMISSIONS.FUNC_ID,USER_MEMBERSHIP.GROUP_ID FROM USER_MEMBERSHIP, GROUP_PERMISSIONS " +
                " WHERE USER_MEMBERSHIP.USER_ID =" + this.UserId +
                " AND USER_MEMBERSHIP.GROUP_ID = GROUP_PERMISSIONS.GROUP_ID";
            try
            {
                db.Open();
                rdr = db.ExecuteReader(sSQL);
                if (rdr.Read())
                {
                    m_GroupId = rdr.GetInt("GROUP_ID");
                    do
                    {
                        try { ret.Add(rdr.GetInt("FUNC_ID"), true); }
                        catch (Exception) { ;}
                    }
                    while (rdr.Read());
                }
            }
            catch (Exception e)
            {
                //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                //Replaced general exception with SecuritySettingException.Added by Tanuj Narula on 27/5/2004
                throw new SecuritySettingException(AuthenticationCredentials.SecSettingsNotFound, e);
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                db.Close();
            }
            return ret;
        }
       
        /// <summary>
        /// Riskmaster.Security.IsAllowed is an absolutely basic check
        /// whether or not the security function list for this login contains
        /// a particular function id.
        /// </summary>
        /// <param name="FunctionId">Function ID</param>
        /// <returns>It returns value returned by ContainsKey()</returns>
        public bool IsAllowed(int FunctionId)
        {
            return m_SecHashTable.ContainsKey(FunctionId);
        }

        public bool IsAllowedEx(int secId) { return IsAllowedEx(secId, 0); }
        /// <summary>
        /// Riskmaster.Security.IsAllowedEx is a more sophisticated check
        /// whether or not the user "should" according to application configuration settings
        /// AND the current user's function list have access to the requested function.
        /// </summary>
        /// <param name="FunctionId">Function ID</param>
        /// <returns>It returns true if the user should be allowed to perform the requested function.</returns>

        public bool IsAllowedEx(int secBase, int secOffset)
        {
            int RMO_ACCESS = 0;

            //If security is turned off for this DSN, always return TRUE
            //if(!this.RMDatabase.Status || secBase == 0)
            //    return true;

            //		  JP 2/20/2003    admin tracking tables broken out in this range
            //        First, check and see if the old AT permissions (13,000+) are still in use. If so, apply those as an override to the individual permissions.
            //        This is done because this feature is being rolled out midstream and not all clients will be using individual AT security.
            //        If they're using the new individual security permissions, the old ones (13,000+) won't be there and the first bIsAllowed will always fail, deferring to the new permissions.
            //        At some point in the post-6.0 future, if the old permissions are officially gone, this code can be removed.
            if (secBase >= 5000000 && secBase <= 6000000) //Admin Tracking Tables
            {
                if (IsAllowed(13000 + secOffset))
                    return true;
                else  //Now, check new individual permissions                 
                {
                    if (!IsAllowed(secBase + RMO_ACCESS))
                        return false;
                    else // check form security here                           
                        return (IsAllowed(secBase + secOffset));
                }
            }
            else if (secBase == 62500)
            {//TR :-001565: for Non Occ payments
                return (IsAllowed(secBase + RMO_ACCESS));
            }
            else  // All other tables
            {
                if (!IsAllowed(secBase + RMO_ACCESS))
                    return false;
                else // check form security here                           
                    return (IsAllowed(secBase + secOffset));
            }
        }

        /// function added by Geeta Sharma for mits 10729
        public bool IsAllowedSuppEx(int secBase, int secOffset)
        {
            int RMO_ACCESS = 0;

            //If security is turned off for this DSN, always return TRUE
            //if (!this.RMDatabase.Status || secBase == 0)
            //    return true;

            bool bSuppFlag = CheckForSuppField(secBase + RMO_ACCESS);

            if (!bSuppFlag)
            {
                return true;
            }
            else
            {
                if (!IsAllowed(secBase + RMO_ACCESS))
                    return false;
                else // check form security here                           
                    return (IsAllowed(secBase + secOffset));
            }
        }

        private bool CheckForSuppField(int FunctionId)
        {
            return m_SecSuppHashTable.ContainsKey(FunctionId);
        }


        /// <summary>
        /// Riskmaster.Security.IsTimePermittedForLogin returns whether the current time lies in the permissible 
        /// time span for the day, for login.
        /// </summary>
        /// <returns>It returns the boolean value true or false</returns>
        public bool IsTimePermittedForLogin()
        {
            System.DateTime CurDateTime = System.DateTime.Now;
            string sStart = "000000";
            string sEnd = "240000";
            string sCur = Conversion.ToDbDateTime(CurDateTime).Substring(8);
            //Pick the permissible access time span for today.
            switch ((int)CurDateTime.DayOfWeek)
            {
                case 0:
                    sStart = m_SunStart;
                    sEnd = m_SunEnd;
                    break;
                case 1:
                    sStart = m_MonStart;
                    sEnd = m_MonEnd;
                    break;
                case 2:
                    sStart = m_TueStart;
                    sEnd = m_TueEnd;
                    break;
                case 3:
                    sStart = m_WedStart;
                    sEnd = m_WedEnd;
                    break;
                case 4:
                    sStart = m_ThuStart;
                    sEnd = m_ThuEnd;
                    break;
                case 5:
                    sStart = m_FriStart;
                    sEnd = m_FriEnd;
                    break;
                case 6:
                    sStart = m_SatStart;
                    sEnd = m_SatEnd;
                    break;
            }
            /*SMS database stores 'null' if user is not allowed for a particular day and 
             * corresponding module level variables(m_ThuStart etc) initializes to blank("") in case of 'null' in SMS DB, 
             * so removing that condition from following 'if' loops. -Tanuj on 5-Jan-2006  
             * */
            if (sStart == "0" || sStart == null)
                sStart = "000000";
            if (sEnd == "0" || sEnd == null || sEnd == "000000")
                sEnd = "240000";

            //Run the test...
            if (sCur.CompareTo(sStart) > 0 && sCur.CompareTo(sEnd) < 0)
                return true;
            else
                return false;
        }

        private string GetHash()
        {
            byte[] byteArr = null;
            byte[] tmpArr = null;
            int cBytes;
            string sStringBuff = "";
            ushort iHI, iLO;

            // Fill er up
            sStringBuff = m_LoginName + Conversion.ToDbDate(m_PrivilegesExpire);

            if (Conversion.ToDbDate(m_PrivilegesExpire).Length == 0)
                sStringBuff += "";
            else
                sStringBuff += "000000";

            sStringBuff = sStringBuff +
                m_Password +
                Conversion.ToDbDate(m_PrivilegesExpire) +
                m_SunStart +
                m_SunEnd +
                m_MonStart +
                m_MonEnd +
                m_TueStart +
                m_TueEnd +
                m_WedStart +
                m_WedEnd +
                m_ThuStart +
                m_ThuEnd +
                m_FriStart +
                m_FriEnd +
                m_SatStart +
                m_SatEnd;
            tmpArr = Conversion.GetByteArr(sStringBuff);
            cBytes = tmpArr.Length;
            byteArr = new byte[tmpArr.Length + 8];
            tmpArr.CopyTo(byteArr, 0);

            Conversion.GetHILOWord(m_DatabaseId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[cBytes], out byteArr[cBytes + 1]);
            Conversion.GetHILOByte(iHI, out byteArr[cBytes + 2], out byteArr[cBytes + 3]);

            Conversion.GetHILOWord(this.UserId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[cBytes + 4], out byteArr[cBytes + 5]);
            Conversion.GetHILOByte(iHI, out byteArr[cBytes + 6], out byteArr[cBytes + 7]);

            return RMCryptography.ComputeHashAsString(byteArr);
        }
        #endregion


    }//Class
}
