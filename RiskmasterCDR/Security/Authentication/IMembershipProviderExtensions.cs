﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Public interface to provide properties and methods
    /// provided through the abstract MembershipProvider provider 
    /// class as well as extension properties
    /// </summary>
    /// <remarks>This functionality had to be moved into an interface
    /// because WCF Serialization is not supported for the abstract base 
    /// class MembershipProvider</remarks>
    public interface IMembershipProviderExtensions
    {
        /// <summary>
        /// Gets and sets connection string name
        /// which may be configured in the Web.config or
        /// similar configuration file
        /// </summary>
        string connectionStringName { get; set; }

        /// <summary>
        /// Gets and sets connection string user name
        /// information such as might be required for
        /// Active Directory or LDAP authentication
        /// </summary>
        string connectionUsername { get; set; }
        /// <summary>
        /// Gets and sets connection string password
        /// information such as might be required for
        /// Active Directory or LDAP authentication
        /// </summary>
        string connectionPassword { get; set; }
    }
}
