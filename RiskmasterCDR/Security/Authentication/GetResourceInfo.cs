﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Collections;
using System.Diagnostics;

namespace Riskmaster.Security.Authentication
{
    public class GetResourceInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strResKey"></param>
        /// <returns></returns>
        public static string GetResourceValue(string strResFile, string strResKey)
        {
            //ResourceManager resMgr = ResourceManager.CreateFileBasedResourceManager(strResFile, strBaseDir, null);

            //return resMgr.GetString(strResKey);

            // Create a ResXResourceReader for the file items.resx.
            ResXResourceReader rsxr = new ResXResourceReader(strResFile);
            DictionaryEntry dictEntry = new DictionaryEntry();

            // Iterate through the resources and display the contents to the console.
            foreach (DictionaryEntry d in rsxr)
            {
                dictEntry = d;
                Debug.Print(d.Key.ToString() + ":\t" + d.Value.ToString());
            }

            //Close the reader.
            rsxr.Close();

            return dictEntry.Value.ToString();

            
        } // method: GetResourceValue

    }
}
