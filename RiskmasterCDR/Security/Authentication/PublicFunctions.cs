﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Raman Bhatia: 02/10/2010
    /// Provides a set of public utility functions
    /// for managing common actions needed for security
    /// </summary>
    public class PublicFunctions
    {

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetFullNameFromLoginName
        /// </summary>     
        public static bool GetFullNameFromLoginName(string p_sLoginName, out string p_sFullName)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return  GetFullNameFromLoginName( p_sLoginName, out  p_sFullName,0);
            }
            else
                throw new Exception("GetFullNameFromLoginName" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Function would return the full name if we pass the login name as a parameter
        /// </summary>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_sFullName"></param>
        /// <returns></returns>
        public static bool GetFullNameFromLoginName(string p_sLoginName, out string p_sFullName, int p_iClientId)
        {
            bool bRetVal = false;
            //DbReader objReader = null;
            int iUserId = 0;
            p_sFullName = string.Empty;
            using (DbReader objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), String.Format("Select USER_ID from User_Details_Table where Login_Name = '{0}'", p_sLoginName.Replace("'", "''"))))
            {
                if (objReader.Read())
                {
                    iUserId = objReader.GetInt32("USER_ID");

                    using (DbReader objDbReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), String.Format("Select FIRST_NAME , LAST_NAME from User_Table where USER_ID = '{0}'", iUserId.ToString())))
                    {
                        if (objDbReader.Read())
                        {
                            p_sFullName = string.Format("{0}" + " " + "{1}", objDbReader.GetString("FIRST_NAME"), objDbReader.GetString("LAST_NAME"));
                            bRetVal = true;
                        }
                    }
                }
            }

            return bRetVal;
        }

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetFullNameDsFromLoginName
        /// </summary>     
        public static DataSet GetFullNameDsFromLoginName()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetFullNameDsFromLoginName(0);
            }
            else
                throw new Exception("GetFullNameDsFromLoginName" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
               // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static DataSet GetFullNameDsFromLoginName(int p_iClientId)
        {
            return DbFactory.GetDataSet(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId)
                ,"SELECT DISTINCT UDT.LOGIN_NAME , UT.LAST_NAME, UT.FIRST_NAME "
                    + " FROM USER_DETAILS_TABLE UDT INNER JOIN USER_TABLE UT ON UT.USER_ID =UDT.USER_ID", p_iClientId);                    
        }

    }
}
