using System;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;
using System.Runtime.Serialization;
using System.Web.Security;
using System.Collections.Generic;
using Riskmaster.Security.Encryption;


namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Implements the 
    /// </summary>
    public partial class RiskmasterMembershipUser: MembershipUser
    {
        private static bool m_bIsAdminUser = false;  //Initialize value to false
        private string m_strUserName = string.Empty, m_strProviderName = string.Empty;
        private int m_intUserID = 0;
        private const int SMS_USER_ENABLED = 1; //1 = Enabled, 0 = Disabled
        private const int SMS_USER_DISABLED = 0; //1 = Enabled, 0 = Disabled

        /// <summary>
        /// OVERLOADED: Class constructor to initalize all necessary
        /// values for the RiskmasterMembershipUser
        /// </summary>
        /// <param name="strUserName">string containing the user name</param>
        /// <param name="strDbConnString">string containing the RISKMASTER Security database
        /// connection string</param>
        public RiskmasterMembershipUser(string strUserName, string strProviderName, string strDbConnString)
        {
            m_strUserName = strUserName;
            m_strProviderName = strProviderName;
            this.ConnectionString = strDbConnString;
            
        } // constructor


        public string ConnectionString { get; set; }

        public int UserID 
        {
            get
            {
                bool blnParsed = false;
                Dictionary<string, string> dictParams = new Dictionary<string, string>();

                dictParams.Add("USER_NAME", this.UserName);

                object objUserID = DbFactory.ExecuteScalar(this.ConnectionString, AuthenticationRepository.GetUserID(), dictParams);

                m_intUserID = Conversion.CastToType<int>(objUserID.ToString(), out blnParsed);

                return m_intUserID;
            }//get
        }

        #region IRiskmasterMembershipUser Members

        public override bool ChangePassword(string oldPassword, string newPassword)
        {
            UpdatePasswordHistory(this.UserID, this.ConnectionString, newPassword, this.UserName, false);

            return true;
        }

        public override string Comment
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override DateTime CreationDate
        {
            get 
            { 
                //TODO: Get the date when the user was created through SMS
                throw new NotImplementedException(); 
            }
        }

        public override string Email
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string GetPassword()
        {
            throw new NotSupportedException();
        }

        public override bool IsApproved
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        
        /// <summary>
        /// Gets whether or not the user's account has been locked/locked out
        /// </summary>
        public override bool IsLockedOut
        {
            get 
            { 
                bool blnParsed = false, blnIsLockedOut = false;
                Dictionary<string, int> dictParams = new Dictionary<string, int>();

                dictParams.Add("USER_ID", this.UserID);

                blnIsLockedOut = Conversion.CastToType<bool>(DbFactory.ExecuteScalar(this.ConnectionString, AuthenticationRepository.GetLockedOutStatus(), dictParams).ToString(), out blnParsed);

                return blnIsLockedOut;
            }//get
        }

        public override DateTime LastActivityDate
        {
            get
            {
                //TODO: Return either the last login date or any other security audit trails records
                throw new NotImplementedException();
            }
            set
            {
                this.LastActivityDate = value;
            }
        }

        public override DateTime LastLockoutDate
        {
            get 
            { 
                //TODO: Determine when the user's account was last locked out by the ACCOUNT_LOCK_STATUS flag
                throw new NotImplementedException(); 
            }
        }


        /// <summary>
        /// Determines the last date that the user has logged into the system
        /// </summary>
        public override DateTime LastLoginDate
        {
            get
            {
                string strSQL = string.Format("SELECT DTTM_LOGIN FROM LOGIN_HISTORY lh WHERE lh.USER_ID = {0}", this.UserID);

                object objLastLoginDate = DbFactory.ExecuteScalar(this.ConnectionString, strSQL);

                //TODO: Determine what format this will return
                return DateTime.Parse(objLastLoginDate.ToString());
            }
            set
            {
                //TODO: Determine if this needs to have the INSERT record code instead
                this.LastLoginDate = value;

                
            }
        }

        public override DateTime LastPasswordChangedDate
        {
            get 
            { 
                //TODO: Determine when the last password was changed from the Password History table
                throw new NotImplementedException(); 
            }
        }

        public override string ProviderName
        {
            get 
            {
                return m_strProviderName;
            }
        }

        public override object ProviderUserKey
        {
            get 
            {
                return m_strUserName;
            }
        }

        public override bool UnlockUser()
        {
            throw new NotImplementedException();
        }

        public override string UserName
        {
            get 
            {
                return m_strUserName;
            }
        }

        #endregion
    }//class

    public partial class RiskmasterMembershipUser
    {
        /// <summary>
        /// Determines if a specified user is a valid RISKMASTER User
        /// </summary>
        /// <param name="strSecConnectionString">string RISKMASTER Security database connection string</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strMessage">string containing warning message if any</param>
        /// <returns>boolean indicating whether or not the user is a valid Riskmaster User</returns>
        public static bool IsValidRiskmasterUser(string strSecConnectionString, string strUserName, out string strMessage, int p_iClientId)
        {
            bool blnIsValidUser = false;
            StringBuilder strSQL = new StringBuilder();
            string strCurrentDate = DateTime.Now.ToString("yyyyMMdd");
            strMessage = (int)Constants.ACCOUNT_STATUS.Default + "|";

            strSQL.Append("SELECT udt.LOGIN_NAME ");
            strSQL.Append("FROM USER_DETAILS_TABLE udt ");
            strSQL.AppendFormat(string.Format("WHERE udt.LOGIN_NAME = {0} ", "~LOGIN_NAME~"));
            strSQL.AppendFormat(string.Format("AND (udt.PRIVS_EXPIRE > {0} ", "~PRIVS_EXPIRE_DATE~"));
            strSQL.Append("OR udt.PRIVS_EXPIRE IS NULL) ");

            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("LOGIN_NAME", strUserName);
            dictParams.Add("PRIVS_EXPIRE_DATE", strCurrentDate);

            object objLoginName = DbFactory.ExecuteScalar(strSecConnectionString, strSQL.ToString(), dictParams);

            if (objLoginName != null)
            {
                blnIsValidUser = true;
            } // if

            //Raman Bhatia: 11/03/2009
            //Enhanced Security Changes
            if (UtilityFunctions.IsPwdPolEnabled())
            {
                AuthorizeUserAccount(strSecConnectionString, strUserName, ref blnIsValidUser, out strMessage, p_iClientId);
                if (blnIsValidUser)
                {
                    CheckPasswordExpr(strSecConnectionString, strUserName, out strMessage, ref blnIsValidUser, p_iClientId);

                }


            }


            return blnIsValidUser;
        } // method: IsValidRiskmasterUser

        //TODO: Replace/remove this function
        /// <summary>
        /// Determines password policy checks for Riskmaster User
        /// </summary>
        /// <param name="strSecConnectionString">string RISKMASTER Security database connection string</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strMessage">string containing warning message if any</param>
        /// <returns>boolean indicating whether or not the user is a valid Riskmaster User</returns>
        public static void CheckPasswordPolicy(string strSecConnectionString, string strUserName, ref bool p_blnIsAuthenticated, out string strMessage, int p_iClientId)
        {
            strMessage = (int)Constants.ACCOUNT_STATUS.Default + "|";

            //Raman Bhatia: 11/03/2009
            //Enhanced Security Changes
            if (UtilityFunctions.IsEnhancedSecurityEnabled())
            {
                AuthorizeUserAccount(strSecConnectionString, strUserName, ref p_blnIsAuthenticated, out strMessage, p_iClientId);
                if (p_blnIsAuthenticated)
                {
                    CheckPasswordExpr(strSecConnectionString, strUserName, out strMessage, ref p_blnIsAuthenticated, p_iClientId);

                }
            }

        } // method: IsValidRiskmasterUser

        //TODO: Replace/remove this function
        /// Name		    : CheckPasswordExpr
        /// Author		    : Raman Bhatia
        /// Date Created	: 26 Mar 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether password is expired 
        /// </summary>
        /// <param name="strSecConnectionString"></param>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_sMessage"></param>
        /// <param name="blnIsValidUser"></param>
        /// <returns>User Account status, which includes 
        /// account lockout, expiration etc</returns>
        private static bool CheckPasswordExpr(string strSecConnectionString, string p_sLoginName , out string p_sMessage , ref bool blnIsValidUser, int p_iClientId)
        {
            string sPassword = "";
            int iUserID = 0;
            int iForceChangePwd = 0;
            string sQuery = "";
            int iPwdExpDays = 0;
            int iPwdExprWarnDays = 0;
            DateTime dtPwdSMSExpr = DateTime.Now;
            p_sMessage = (int)Constants.ACCOUNT_STATUS.Default + "|";
            bool blnSuccess = false;
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, String.Format("SELECT * FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}'", p_sLoginName)))
                {
                    if (objReader.Read())
                    {
                        iUserID = Conversion.CastToType<int>(objReader["USER_ID"].ToString(), out blnSuccess);
                        sPassword = UtilityFunctions.DecryptString(objReader["PASSWORD"].ToString());
                        iForceChangePwd = Conversion.CastToType<int>(objReader["FORCE_CHANGE_PWD"].ToString(), out blnSuccess);
                        dtPwdSMSExpr = Conversion.ToDate(objReader["PASSWD_EXPIRE"].ToString());
                        objReader.Close();

                        if (dtPwdSMSExpr != DateTime.MinValue && dtPwdSMSExpr < System.DateTime.Now)
                        {
                            p_sMessage = (int)Constants.ACCOUNT_STATUS.ForcedPasswordExpired + "|" + Globalization.GetString("Login.AuthUser.ForcedPwdExpired", p_iClientId);
                            blnIsValidUser = false;
                            return true;
                        }
                        if (dtPwdSMSExpr == DateTime.MinValue)
                            dtPwdSMSExpr = DateTime.MaxValue;
                    }
                }


                bool bIsAdminUser = false;
                bIsAdminUser = IsSMSAdminUser(strSecConnectionString , p_sLoginName);
                if (iUserID <= 0 || bIsAdminUser)
                    return false;

                if (iForceChangePwd != 0)
                {
                    p_sMessage = (int)Constants.ACCOUNT_STATUS.UserPasswordExpired + "|" + Globalization.GetString("LoginAdaptor.CheckPasswordExpr.UserPwdExpired", p_iClientId);
                    return true;
                }

                sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, sQuery))
                {
                    if (objReader.Read())
                    {
                        blnSuccess = false;
                        iPwdExpDays = Conversion.CastToType<int>(objReader["PWD_EXPR_DAYS"].ToString(), out blnSuccess);
                        iPwdExprWarnDays = Conversion.CastToType<int>(objReader["PWD_EXPR_WARNING_DAYS"].ToString(), out blnSuccess);
                        objReader.Close();
                    }
                }

                sQuery = String.Format("SELECT DTTM_CREATED FROM PASSWORD_HISTORY WHERE USER_ID = {0} ORDER BY DTTM_CREATED desc", iUserID);
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, sQuery))
                {
                    if (objReader.Read())
                    {
                        DateTime dtDatabaseDate = Conversion.ToDate(objReader["DTTM_CREATED"].ToString());
                        DateTime DTPwdExprDate = dtDatabaseDate.AddDays(iPwdExpDays);
                        DateTime DTPwdExprWarnDate = dtDatabaseDate.AddDays(iPwdExpDays - iPwdExprWarnDays);


                        if (DTPwdExprDate <= DateTime.Now || dtPwdSMSExpr <= DateTime.Now)
                        {
                            //Setting status & Error message for password expiration.
                            p_sMessage = (int)Constants.ACCOUNT_STATUS.UserPasswordExpired + "|" + Globalization.GetString("LoginAdaptor.CheckPasswordExpr.UserPwdExpired", p_iClientId);
                            return true;
                        }
                        else if (DTPwdExprWarnDate <= DateTime.Now)
                        {
                            int iDiff = ((TimeSpan)DTPwdExprDate.Subtract(dtPwdSMSExpr)).Days;

                            if (iDiff >= 0)
                                iDiff = ((TimeSpan)dtPwdSMSExpr.Subtract(DateTime.Now)).Days;
                            else if (iDiff < 0)
                                iDiff = ((TimeSpan)DTPwdExprDate.Subtract(DateTime.Now)).Days;

                            if (iDiff == 0)
                                iDiff = 1;
                            //Setting status & Error message for warning of password expiration.
                            p_sMessage = (int)Constants.ACCOUNT_STATUS.PasswordExpiringSoon + "|" + iDiff.ToString();
                            return true;
                        }

                    }
                    else
                    {
                        UpdatePasswordHistory(iUserID, strSecConnectionString, sPassword, "", false);
                    }

                    if (objReader != null)
                        objReader.Close();
                }
            }
            finally
            {

            }
            return false;
        }

        //TODO: Replace/remove this function
        /// Name		: AuthorizeUserAccount
        /// Author		: Anurag Agarwal
        /// Date Created	: 20 Jul 2006		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check 
        /// </summary>
        /// <param name="strSecConnectionString"></param>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_blnIsValidUser"></param>
        /// <param name="p_sMessage"></param>
        /// <returns>User Account status, which includes 
        /// account lockout, expiration etc</returns>
        private static bool AuthorizeUserAccount(string strSecConnectionString, string p_sLoginName, ref bool p_blnIsValidUser, out string p_sMessage, int p_iClientId)
        {
            int iUserID = 0;
            string sQuery = "";
            int iLockOutDur = 0;
            int iAllowedLogin = 0;
            DbConnection objConn = null;
            p_sMessage =  (int)Constants.ACCOUNT_STATUS.Default + "|";
            bool bAccountLockStatus = false;
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, String.Format("SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}'", p_sLoginName)))
                {
                    if (objReader.Read())
                    {
                        iUserID = Conversion.ConvertStrToInteger(objReader["USER_ID"].ToString());
                        objReader.Close();
                    }
                }

                if (iUserID <= 0)
                    return false;

                objConn = DbFactory.GetDbConnection(strSecConnectionString);
                objConn.Open();
                if (p_blnIsValidUser)
                {
                    objConn.ExecuteNonQuery("INSERT INTO LOGIN_HISTORY (USER_ID, DTTM_LOGIN, STATUS)" +
                        " VALUES(" + iUserID + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "',1)");
                }
                else
                    objConn.ExecuteNonQuery("INSERT INTO LOGIN_HISTORY (USER_ID, DTTM_LOGIN, STATUS)" +
                        " VALUES(" + iUserID + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "',0)");
                objConn.Close();

                bool bIsAdminUser = false;
                bIsAdminUser = IsSMSAdminUser(strSecConnectionString, p_sLoginName);
                if (iUserID <= 0 || bIsAdminUser)
                    return false;

                sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, sQuery))
                {
                    if (objReader.Read())
                    {
                        iLockOutDur = Conversion.ConvertStrToInteger(objReader["PWD_LOCKOUT_DUR"].ToString());
                        iAllowedLogin = Conversion.ConvertStrToInteger(objReader["ALLOWED_LOGIN_ATTEMPTS"].ToString());
                        objReader.Close();
                    }
                }
                
                sQuery = "SELECT * FROM USER_TABLE WHERE USER_ID = " + iUserID;
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, sQuery))
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertStrToInteger(objReader["ACCOUNT_LOCK_STATUS"].ToString()) == 1)
                        {
                            //Raman Bhatia MITS 18594 : Lockout Duration does not work
                            DateTime dtDttmAccountLock = Conversion.ToDate(objReader["DTTM_ACCOUNT_LOCK"].ToString());
                            if (DateTime.Now > (dtDttmAccountLock.AddHours(iLockOutDur)))
                            {
                                bAccountLockStatus = false;
                                objConn.Open();
                                objConn.ExecuteNonQuery("UPDATE USER_TABLE SET ACCOUNT_LOCK_STATUS = 0 , DTTM_ACCOUNT_LOCK = '' WHERE USER_ID = " + iUserID);
                                objConn.Close();
                            }
                            else
                            {
                                bAccountLockStatus = true;
                                p_blnIsValidUser = false;
                                p_sMessage = (int)Constants.ACCOUNT_STATUS.Locked + "|" + Globalization.GetString("LoginAdaptor.AuthorizeUserAccount.UserAccountLocked", p_iClientId);
                                return false;
                            }
                        }
                        objReader.Close();
                    }
                }

                //Raman Bhatia MITS 18594 : Lockout Duration does not work
                //Lockout duration has been handled above.This implementation seems wrong.
                
                //DateTime dtDate = DateTime.Now.AddHours(-1 * iLockOutDur);
                int iLoopCount = 0;

                //Raman Bhatia MITS 18594 : Lockout Duration does not work
                //Lockout duration has been handled above.This implementation seems wrong.

                //string sDate = Conversion.ToDbDateTime(dtDate);
                //sQuery = "SELECT STATUS FROM LOGIN_HISTORY WHERE USER_ID = " + iUserID + " AND DTTM_LOGIN > '" + sDate + "' ORDER BY DTTM_LOGIN desc";
                sQuery = "SELECT STATUS FROM LOGIN_HISTORY WHERE USER_ID = " + iUserID + " ORDER BY DTTM_LOGIN desc";
                using (DbReader objReader = DbFactory.GetDbReader(strSecConnectionString, sQuery))
                {

                    iLoopCount = 0;

                    while (objReader.Read())
                    {
                        if (Conversion.ConvertStrToInteger(objReader["STATUS"].ToString()) == 1)
                        {
                            bAccountLockStatus = false;
                            break;
                        }

                        iLoopCount++;
                        if (iLoopCount >= iAllowedLogin)
                        {
                            bAccountLockStatus = true;
                            break;
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                }

                if (bAccountLockStatus)
                {
                    //Raman Bhatia MITS 18594 : Lockout Duration does not work
                    //Added DTTM_ACCOUNT_LOCK to maintain datetime user was locked

                    string sCurrentDateTime = Conversion.ToDbDateTime(DateTime.Now);

                    objConn.Open();
                    objConn.ExecuteNonQuery("UPDATE USER_TABLE SET ACCOUNT_LOCK_STATUS = 1 , DTTM_ACCOUNT_LOCK = " + sCurrentDateTime + " WHERE USER_ID = " + iUserID);
                    objConn.Close();
                    p_sMessage = (int)Constants.ACCOUNT_STATUS.Locked + "|" + Globalization.GetString("LoginAdaptor.AuthorizeUserAccount.UserAccountLocked", p_iClientId);
                    p_blnIsValidUser = false;
                    return false;
                }
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                    objConn = null;
                }
            }
            return true;
        }

        //TODO: Replace/remove this function
        /// <summary>
        /// Updates Password History
        /// </summary>
        /// <param name="p_iUserId"></param>
        /// <param name="p_sConnectionString"></param>
        /// <param name="p_sNewPassword"></param>
        /// <param name="p_sCreatedBy"></param>
        /// <param name="p_bExistingPwdUpdate"></param>
        private static void UpdatePasswordHistory(int p_iUserId, string p_sConnectionString, string p_sNewPassword, string p_sCreatedBy, bool p_bExistingPwdUpdate)
        {
            
            StringBuilder sbSql = null;
            
            sbSql = new StringBuilder();
            sbSql.Append("INSERT INTO PASSWORD_HISTORY VALUES (");
            sbSql.Append(UtilityFunctions.GetNextRecordId(p_sConnectionString , "PASSWORD_HISTORY").ToString() + "," + p_iUserId.ToString() + ",'" + UtilityFunctions.EncryptString(p_sNewPassword) + "','" + Conversion.ToDbDateTime(DateTime.Now) + "')");
            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            DbFactory.ExecuteNonQuery(p_sConnectionString, sbSql.ToString(), dictParams);
        }

        //TODO: Replace/remove this function
        /// <summary>
        /// Determines if an existing user is already a member of the SMS 
        /// Security Administrators
        /// </summary>
        /// <param name="p_sLoginName">string containing the user's login name</param>
        /// <returns>boolean indicating whether or not the user is a member of the SMS Administrators group</returns>
        private static bool IsSMSAdminUser(string strsecConnectionString , string p_sLoginName)
        {
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            StringBuilder strSQLBldr = new StringBuilder();

            strSQLBldr.Append("SELECT IS_SMS_USER ");
            strSQLBldr.Append("FROM USER_TABLE ut ");
            strSQLBldr.Append("INNER JOIN USER_DETAILS_TABLE udt ");
            strSQLBldr.Append("ON ut.USER_ID = udt.USER_ID ");
            strSQLBldr.AppendFormat(string.Format("WHERE udt.LOGIN_NAME = {0}", "~LOGIN_NAME~"));

            dictParams.Add("LOGIN_NAME", p_sLoginName);

            object objResult = DbFactory.ExecuteScalar(strsecConnectionString, strSQLBldr.ToString(), dictParams);

            //If the resultant value is not null
            if (objResult != null)
            {
                //Changed Rakhi for Mits 17468:Force user to change password on next login does not  work-Start
                //In Sql the datatype of IS_SMS_User is bit and so returns true/false whereas
                //In Oracle the datatype of IS_SMS_USer is Number and so returns the value 1/0
                // int intIsSMSUser = Conversion.CastToType<int>(objResult.ToString(), out blnSuccess);
                //the above line in case of SQL always returns 0 even if the user is a SMS USER.
                //Therefore modifying the code to handle both SQL and Oracle.

                //bool blnSuccess = false;

                ////Verify that the bit value can be converted to an integer
                //int intIsSMSUser = Conversion.CastToType<int>(objResult.ToString(), out blnSuccess);

                ////Verify that the conversion succeeded
                //if (blnSuccess)
                //{
                //    //Verify and enable the user as an SMS Security Administrator
                //    if (intIsSMSUser == SMS_USER_ENABLED)
                //    {
                //        m_bIsAdminUser = true;
                //    } // if
                //    //Raman 11/19/2009: Since this variable is static hence we should 
                //    //specifically reset it to false if its not an admin user
                //    else
                //    {
                //        m_bIsAdminUser = false;
                //    }
                //} // if

                
                if (objResult.ToString() == SMS_USER_ENABLED.ToString() || objResult.ToString().ToLower()=="true")
                    m_bIsAdminUser = true;
                else
                    m_bIsAdminUser = false;

                //Changed Rakhi for Mits 17468:Force user to change password on next login does not  work-End

            } // if

            return m_bIsAdminUser;
        }

    }
} //Namespace
