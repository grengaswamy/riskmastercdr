﻿using System;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Interface which encapsulates all common 
    /// properties required for SSO Providers
    /// </summary>
    public interface ISSOProvider
    {
        string AdminPwd { get; set; }
        string AdminUserName { get; set; }
        string ConnectionString { get; set; }
        string ConnectionType { get; set; }
        string HostName { get; set; }
        bool IsEnabled { get; set; }
        int ProviderID { get; set; }
    }
}
