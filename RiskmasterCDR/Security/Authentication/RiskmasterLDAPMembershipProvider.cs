﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.DirectoryServices;
using System.Web;
using System.Web.Security;
using System.Web.Configuration;
using System.Runtime.Serialization;
using Riskmaster.Common;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Membership Provider implementation for LDAP Authorization repositories
    /// </summary>
    [DataContract]
    public class RiskmasterLDAPMembershipProvider : CustomMembershipProvider
    {
        
        private string m_strMembershipProviderName = "RiskmasterLDAPMembershipProvider";
        string m_strLDAPUserName = string.Empty, m_strLDAPPassword = string.Empty;

        private int m_iClientId = 0;

        /// <summary>
        /// Default Class constructor
        /// </summary>
        public RiskmasterLDAPMembershipProvider(int p_iClientId)
        {
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// <param name="strLDAPConnString"></param>
        public RiskmasterLDAPMembershipProvider(string strLDAPConnString, int p_iClientId)
        {
            this.ConnectionString = strLDAPConnString;
            m_iClientId = p_iClientId;
        } // constructor

        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// <param name="strLDAPConnString">string containing the connection string to LDAP</param>
        /// <param name="strUserAttribMapping">string containing the user attribute mapping</param>
        /// <param name="strAdminUID">string containing the admin user id to connect to LDAP repository</param>
        /// <param name="strAdminPwd">string containing the admin pwd to connect to LDAP repository</param>
        public RiskmasterLDAPMembershipProvider(string strLDAPConnString, string strUserAttribMapping, 
            string strAdminUID, string strAdminPwd, int p_iClientId)
        {
            this.ConnectionString = strLDAPConnString;
            this.UserAttribute = strUserAttribMapping;
            this.AdminUserName = strAdminUID;
            this.AdminPwd = strAdminPwd;
            m_iClientId = p_iClientId;
        } // constructor


        /// <summary>
        /// Gets and sets the Application Name for the Membership Provider
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return m_strMembershipProviderName;
            }
            set
            {
                m_strMembershipProviderName = value;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validates a user in the LDAP repository/store
        /// </summary>
        /// <param name="username">string containing the LDAP username</param>
        /// <param name="password">string containing the LDAP user's password</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public override bool ValidateUser(string username, string password)
        {
            bool blnIsAuthenticated = false;
            string strLDAPUserName = String.Empty;

            try
            {
                //Bind with admin credentials first
                using (DirectoryEntry dirEntryAdminLDAP = new DirectoryEntry(this.ConnectionString, this.AdminUserName, this.AdminPwd, AuthenticationTypes.None))
                {
                    //Search for the specified user name
                    using (DirectorySearcher dirEntrySearcher = new DirectorySearcher(dirEntryAdminLDAP))
                    {
                        //Specify the user filter
                        dirEntrySearcher.Filter = string.Format("(&(objectClass=person)({0}={1}))", this.UserAttribute, username);

                        //Determine if the specified user can be found
                        SearchResult srUserFound = dirEntrySearcher.FindOne();

                        //The specified user based on the filter was found
                        if (srUserFound != null)
                        {
							//MITS 24729 Sometime client set to user other attribute instead of cn
                            string sPath = srUserFound.Path;
                            string sDN = sPath.Substring(sPath.LastIndexOf("/")+1);
                            using (DirectoryEntry dirEntryLDAP = new DirectoryEntry(this.ConnectionString, sDN, password, AuthenticationTypes.None))
                            {
                                //NOTE: This can be expressed more succintly in code through the ternary operator, but makes the code less readable
                                //Example: blnIsAuthenticated = !string.IsNullOrEmpty(dirEntryLDAP.Name) ? true : false;
                                if (!string.IsNullOrEmpty(dirEntryLDAP.Name))
                                {
                                    blnIsAuthenticated = true;
                                } // if
                            }//using
                        }//if
                    }//using
                } //using
            } // try
            catch (Exception ex)
            {
				//MITS 24729 Log error for troubleshooting
                Log.Write(string.Format("LDAP authentication error: {0}. Strack trace: {1}", ex.Message, ex.StackTrace.ToString()), m_iClientId);
                blnIsAuthenticated = false;
            } // catch

            return blnIsAuthenticated;

        }//method: ValidateUser()

        public override bool ValidateSMSUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        //public override bool ValidateUser(string username, string password)
        //{
        //    string strLDAPConnString = WebConfigurationManager.ConnectionStrings["LDAPConnectionString"].ConnectionString;

        //    //Find the person in the directory to determine their distinct name 

        //    try
        //    {


        //        DirectoryEntry root = new DirectoryEntry(strLDAPConnString, null, null, AuthenticationTypes.None);

        //        DirectorySearcher searcher = new DirectorySearcher(root);
        //        searcher.SearchScope = SearchScope.Subtree;
        //        searcher.Filter = "uid=" + username;

        //        SearchResult findResult = searcher.FindOne();

        //        string distinctName = "uid=" + username;

        //        //// Inverse the ou order found in LDAP to build distinct name 

        //        //for (int i = findResult.Properties["ou"].Count - 1; i >= 0; i--)
        //        //{

        //        //    distinctName += ",ou=" + findResult.Properties["ou"][i];
        //        //}

        //        //distinctName += ",o=YourCompanyName.com,c=US";

        //        // Find the person as Employee 

        //        DirectoryEntry root2 = new DirectoryEntry(strLDAPConnString,distinctName, password, AuthenticationTypes.ServerBind);

        //        DirectorySearcher searcher2 = new DirectorySearcher(root2);
        //        searcher2.SearchScope = SearchScope.Subtree;
        //        searcher2.Filter = "uid=" + username;

        //        try
        //        {


        //            SearchResult resultEmployee = searcher2.FindOne();

        //            if (resultEmployee.Properties["uid"].Count == 1) { return true; } else { return false; }
        //        }


        //        catch (Exception ex)
        //        {
        //            return false;
        //        }//catch

        //    }


        //    catch (Exception ex)
        //    {
        //        return false;
        //    }

        //} 

        #region IMembershipProviderExtensions Members
       

        public override string connectionStringName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string connectionUsername
        {
            get
            {
                if (!string.IsNullOrEmpty(m_strLDAPUserName))
                {
                    return m_strLDAPUserName;
                } // if
                else
                {
                    throw new NotImplementedException();
                } // elsethrow new NotImplementedException();
            }
            set
            {
                m_strLDAPUserName = value;
            }
        }

        public override string connectionPassword
        {
            get
            {
                if (!string.IsNullOrEmpty(m_strLDAPPassword))
                {
                    return m_strLDAPPassword;
                } // if
                else
                {
                    throw new NotImplementedException();
                } // else
            }
            set
            {
                m_strLDAPPassword = value;
            }
        }

        #endregion


        /// <summary>
        /// Gets and sets the LDAP Server types
        /// </summary>
        [DataMember]
        public Dictionary<string, string> LDAPServerType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the Base DN/Host Name
        /// for the LDAP Server
        /// </summary>
        [DataMember]
        public string BaseDN
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the user attribute mapping
        /// for the LDAP repository
        /// </summary>
        [DataMember]
        public string UserAttribute
        {
            get;
            set;
        }
    }
    
}