using System;
using System.Text;
using System.Globalization;

namespace Riskmaster.Security.Encryption
{
	internal class CCSCDES
	{
		protected uint[,] m_Keys = new uint[16,2];      //  "Cooked" DES keys

		//========== DES Key Schedule (ANSI X3.92 1981) ==========
		//      Used to select 56 bits out of 64 bits
		byte[] K56 =
		{
				56, 48, 40, 32, 24, 16,  8,  0, 57, 49, 41, 33, 25, 17,
			9,  1, 58, 50, 42, 34, 26, 18, 10,  2, 59, 51, 43, 35,
			62, 54, 46, 38, 30, 22, 14,  6, 61, 53, 45, 37, 29, 21,
			13,  5, 60, 52, 44, 36, 28, 20, 12,  4, 27, 19, 11,  3   };

		//      Used to rotate key-halves
		byte[] KROT =
		{  1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28  };

		//      Used to permute & select 48 bits out of 56 bits
		byte[] K48 =
		{
				13, 16, 10, 23,  0,  4,  2, 27, 14,  5, 20,  9,
			2, 18, 11,  3, 25,  7, 15,  6, 26, 19, 12,  1,
			40, 51, 30, 36, 46, 54, 29, 39, 50, 44, 32, 47,
			43, 48, 38, 55, 33, 52, 45, 41, 49, 35, 28, 31   };

		byte[] ByteMask = { 128, 64, 32, 16, 8, 4, 2, 1  };

		uint[] KeyMask =
		{
			0x800000, 0x400000, 0x200000, 0x100000,
			0x080000, 0x040000, 0x020000, 0x010000,
			0x008000, 0x004000, 0x002000, 0x001000,
			0x000800, 0x000400, 0x000200, 0x000100,
			0x000080, 0x000040, 0x000020, 0x000010,
			0x000008, 0x000004, 0x000002, 0x000001
		};

		//========== Lookup tables for Substitution Boxes ==========
		uint[] S0 =
		{
			0x00820200, 0x00020000, 0x80800000, 0x80820200,
			0x00800000, 0x80020200, 0x80020000, 0x80800000,
			0x80020200, 0x00820200, 0x00820000, 0x80000200,
			0x80800200, 0x00800000, 0x00000000, 0x80020000,

			0x00020000, 0x80000000, 0x00800200, 0x00020200,
			0x80820200, 0x00820000, 0x80000200, 0x00800200,
			0x80000000, 0x00000200, 0x00020200, 0x80820000,
			0x00000200, 0x80800200, 0x80820000, 0x00000000,

			0x00000000, 0x80820200, 0x00800200, 0x80020000,
			0x00820200, 0x00020000, 0x80000200, 0x00800200,
			0x80820000, 0x00000200, 0x00020200, 0x80800000,
			0x80020200, 0x80000000, 0x80800000, 0x00820000,

			0x80820200, 0x00020200, 0x00820000, 0x80800200,
			0x00800000, 0x80000200, 0x80020000, 0x00000000,
			0x00020000, 0x00800000, 0x80800200, 0x00820200,
			0x80000000, 0x80820000, 0x00000200, 0x80020200
		};

		uint[] S1 =
		{
			0x10042004, 0x00000000, 0x00042000, 0x10040000,
			0x10000004, 0x00002004, 0x10002000, 0x00042000,
			0x00002000, 0x10040004, 0x00000004, 0x10002000,
			0x00040004, 0x10042000, 0x10040000, 0x00000004,

			0x00040000, 0x10002004, 0x10040004, 0x00002000,
			0x00042004, 0x10000000, 0x00000000, 0x00040004,
			0x10002004, 0x00042004, 0x10042000, 0x10000004,
			0x10000000, 0x00040000, 0x00002004, 0x10042004,

			0x00040004, 0x10042000, 0x10002000, 0x00042004,
			0x10042004, 0x00040004, 0x10000004, 0x00000000,
			0x10000000, 0x00002004, 0x00040000, 0x10040004,
			0x00002000, 0x10000000, 0x00042004, 0x10002004,

			0x10042000, 0x00002000, 0x00000000, 0x10000004,
			0x00000004, 0x10042004, 0x00042000, 0x10040000,
			0x10040004, 0x00040000, 0x00002004, 0x10002000,
			0x10002004, 0x00000004, 0x10040000, 0x00042000
		};

		uint[] S2 =
		{
			0x41000000, 0x01010040, 0x00000040, 0x41000040,
			0x40010000, 0x01000000, 0x41000040, 0x00010040,
			0x01000040, 0x00010000, 0x01010000, 0x40000000,
			0x41010040, 0x40000040, 0x40000000, 0x41010000,

			0x00000000, 0x40010000, 0x01010040, 0x00000040,
			0x40000040, 0x41010040, 0x00010000, 0x41000000,
			0x41010000, 0x01000040, 0x40010040, 0x01010000,
			0x00010040, 0x00000000, 0x01000000, 0x40010040,

			0x01010040, 0x00000040, 0x40000000, 0x00010000,
			0x40000040, 0x40010000, 0x01010000, 0x41000040,
			0x00000000, 0x01010040, 0x00010040, 0x41010000,
			0x40010000, 0x01000000, 0x41010040, 0x40000000,

			0x40010040, 0x41000000, 0x01000000, 0x41010040,
			0x00010000, 0x01000040, 0x41000040, 0x00010040,
			0x01000040, 0x00000000, 0x41010000, 0x40000040,
			0x41000000, 0x40010040, 0x00000040, 0x01010000
		};

		uint[] S3 =
		{
			0x00100402, 0x04000400, 0x00000002, 0x04100402,
			0x00000000, 0x04100000, 0x04000402, 0x00100002,
			0x04100400, 0x04000002, 0x04000000, 0x00000402,
			0x04000002, 0x00100402, 0x00100000, 0x04000000,

			0x04100002, 0x00100400, 0x00000400, 0x00000002,
			0x00100400, 0x04000402, 0x04100000, 0x00000400,
			0x00000402, 0x00000000, 0x00100002, 0x04100400,
			0x04000400, 0x04100002, 0x04100402, 0x00100000,

			0x04100002, 0x00000402, 0x00100000, 0x04000002,
			0x00100400, 0x04000400, 0x00000002, 0x04100000,
			0x04000402, 0x00000000, 0x00000400, 0x00100002,
			0x00000000, 0x04100002, 0x04100400, 0x00000400,

			0x04000000, 0x04100402, 0x00100402, 0x00100000,
			0x04100402, 0x00000002, 0x04000400, 0x00100402,
			0x00100002, 0x00100400, 0x04100000, 0x04000402,
			0x00000402, 0x04000000, 0x04000002, 0x04100400
		};

		uint[] S4 =
		{
			0x02000000, 0x00004000, 0x00000100, 0x02004108,
			0x02004008, 0x02000100, 0x00004108, 0x02004000,
			0x00004000, 0x00000008, 0x02000008, 0x00004100,
			0x02000108, 0x02004008, 0x02004100, 0x00000000,

			0x00004100, 0x02000000, 0x00004008, 0x00000108,
			0x02000100, 0x00004108, 0x00000000, 0x02000008,
			0x00000008, 0x02000108, 0x02004108, 0x00004008,
			0x02004000, 0x00000100, 0x00000108, 0x02004100,

			0x02004100, 0x02000108, 0x00004008, 0x02004000,
			0x00004000, 0x00000008, 0x02000008, 0x02000100,
			0x02000000, 0x00004100, 0x02004108, 0x00000000,
			0x00004108, 0x02000000, 0x00000100, 0x00004008,

			0x02000108, 0x00000100, 0x00000000, 0x02004108,
			0x02004008, 0x02004100, 0x00000108, 0x00004000,
			0x00004100, 0x02004008, 0x02000100, 0x00000108,
			0x00000008, 0x00004108, 0x02004000, 0x02000008
		};

		uint[] S5 =
		{
			0x20000010, 0x00080010, 0x00000000, 0x20080800,
			0x00080010, 0x00000800, 0x20000810, 0x00080000,
			0x00000810, 0x20080810, 0x00080800, 0x20000000,
			0x20000800, 0x20000010, 0x20080000, 0x00080810,

			0x00080000, 0x20000810, 0x20080010, 0x00000000,
			0x00000800, 0x00000010, 0x20080800, 0x20080010,
			0x20080810, 0x20080000, 0x20000000, 0x00000810,
			0x00000010, 0x00080800, 0x00080810, 0x20000800,

			0x00000810, 0x20000000, 0x20000800, 0x00080810,
			0x20080800, 0x00080010, 0x00000000, 0x20000800,
			0x20000000, 0x00000800, 0x20080010, 0x00080000,
			0x00080010, 0x20080810, 0x00080800, 0x00000010,

			0x20080810, 0x00080800, 0x00080000, 0x20000810,
			0x20000010, 0x20080000, 0x00080810, 0x00000000,
			0x00000800, 0x20000010, 0x20000810, 0x20080800,
			0x20080000, 0x00000810, 0x00000010, 0x20080010
		};

		uint[] S6 =
		{
			0x00001000, 0x00000080, 0x00400080, 0x00400001,
			0x00401081, 0x00001001, 0x00001080, 0x00000000,
			0x00400000, 0x00400081, 0x00000081, 0x00401000,
			0x00000001, 0x00401080, 0x00401000, 0x00000081,

			0x00400081, 0x00001000, 0x00001001, 0x00401081,
			0x00000000, 0x00400080, 0x00400001, 0x00001080,
			0x00401001, 0x00001081, 0x00401080, 0x00000001,
			0x00001081, 0x00401001, 0x00000080, 0x00400000,

			0x00001081, 0x00401000, 0x00401001, 0x00000081,
			0x00001000, 0x00000080, 0x00400000, 0x00401001,
			0x00400081, 0x00001081, 0x00001080, 0x00000000,
			0x00000080, 0x00400001, 0x00000001, 0x00400080,

			0x00000000, 0x00400081, 0x00400080, 0x00001080,
			0x00000081, 0x00001000, 0x00401081, 0x00400000,
			0x00401080, 0x00000001, 0x00001001, 0x00401081,
			0x00400001, 0x00401080, 0x00401000, 0x00001001
		};

		uint[] S7 =
		{
			0x08200020, 0x08208000, 0x00008020, 0x00000000,
			0x08008000, 0x00200020, 0x08200000, 0x08208020,
			0x00000020, 0x08000000, 0x00208000, 0x00008020,
			0x00208020, 0x08008020, 0x08000020, 0x08200000,

			0x00008000, 0x00208020, 0x00200020, 0x08008000,
			0x08208020, 0x08000020, 0x00000000, 0x00208000,
			0x08000000, 0x00200000, 0x08008020, 0x08200020,
			0x00200000, 0x00008000, 0x08208000, 0x00000020,

			0x00200000, 0x00008000, 0x08000020, 0x08208020,
			0x00008020, 0x08000000, 0x00000000, 0x00208000,
			0x08200020, 0x08008020, 0x08008000, 0x00200020,
			0x08208000, 0x00000020, 0x00200020, 0x08008000,

			0x08208020, 0x00200000, 0x08200000, 0x08000020,
			0x00208000, 0x00008020, 0x08008020, 0x08200000,
			0x00000020, 0x08208000, 0x00208020, 0x00000000,
			0x08000000, 0x08200020, 0x00008000, 0x00208020
		};


		//////////////////////////////////////////////////////////////////////////

		//  Permute
		//      Helper function for performing Initial/Final Permutation
		//      (IN/OUT) unsigned long &left    Left half of block
		//      (IN/OUT) unsigned long &right   Right half of block
		//      (IN) WORD bits                  Number of bits to shift
		//      (IN) unsigned long mask         Bit mask
		void Permute (ref uint left, ref uint right, ushort bits, uint mask)
		{
			uint temp = ((left >> bits) ^ right) & mask;
			right ^= temp;
			left ^= (temp << bits);
		}

		//  More information on Initial and Final Permutations:
		//  The DES Initial permutation permutes the input block bits. We
		//  can view this as a 2-D geometric problem of transforming:
		//
		//         0  1  2  3  4  5  6  7      62 54 46 38 30 22 14  6
		//         8  9 10 11 12 13 14 15      60 52 44 36 28 20 12  4
		//        16 17 18 19 20 21 22 23      58 50 42 34 26 18 10  2
		//        24 25 26 27 28 29 30 31  to  56 48 40 32 24 16  8  0
		//        32 33 34 35 36 37 38 39      63 55 47 39 31 23 15  7
		//        40 41 42 43 44 45 46 47      61 53 45 37 29 21 13  5
		//        48 49 50 51 52 53 54 55      59 51 43 35 27 19 11  3
		//        56 57 58 59 60 61 62 63      57 49 41 33 25 17  9  1
		//
		//  Note that the image has been subject to permutations of the form
		//          a b  =>  d b
		//          c d  =>  c a
		//  with the odd and even bits put into different words.
		//  
		//  The main trick is to remember that:
		//  
		//  t = ((left >> bits) ^ right) & mask;
		//  right ^= t;
		//  left ^=(t << bits);
		//  
		//  can be used to swap and move bits between words, So
		//
		//  left =  0  1  2  3  right = 16 17 18 19
		//          4  5  6  7          20 21 22 23
		//          8  9 10 11          24 25 26 27
		//         12 13 14 15          28 29 30 31
		//
		//  becomes (for bits == 2 and mask == 0x3333)
		//
		//  left =  0  1 16 17  right =  2  3 18 19
		//          4  5 20 21           6  7 22 23
		//          8  9 24 25          10 11 24 25
		//         12 13 28 29          14 15 28 29
		//------------------------------------------------------------------------

		//  DES_Round
		//      Helper function, does one round of DES
		//      (IN/OUT) unsigned long &left    Left half of block
		//      (IN) unsigned long right        Right half of block
		//      (IN) unsigned long key1         current key in key schedule
		//      (IN) unsigned long key2         next key in key schedule
		void DES_Round (ref uint left, uint right, uint key1, uint key2)
		{
			uint u = right ^ key1;
			uint v = right ^ key2;
			v = (v >> 4)|(v << 28);
			left ^= S1[ v        & 0x3f] |
				S3[(v >>  8) & 0x3f] |
				S5[(v >> 16) & 0x3f] |
				S7[(v >> 24) & 0x3f] |
				S0[ u        & 0x3f] |
				S2[(u >>  8) & 0x3f] |
				S4[(u >> 16) & 0x3f] |
				S6[(u >> 24) & 0x3f];            
		}
		//------------------------------------------------------------------------

		//////////////////////////////////////////////////////////////////////////
		//
		//      class CDES Implementation
		//
		//////////////////////////////////////////////////////////////////////////

		//  CookKeys
		//      Create DES key schedule from given user keys
		//      (IN) char * key     8-byte user key
		//      (IN) BOOL encrypt   TRUE to create encryption key schedule
		//                          FALSE to create decryption key schedule
		void CookKeys (byte[] key, bool encrypt)
		{
			//  Extract initial 56-bit key
			ushort bitPos, ix;
			byte[] work = new byte[56];
			ushort j;

			for (j = 0; j < 56; j++)
			{
				bitPos = K56[j];
				ix = (ushort) (bitPos & 0x07);
				work[j] = ((key[bitPos >> 3] & ByteMask[ix]) != 0) ? (byte) 1 : (byte) 0;
			}

			// Create 48-bit DES keys
			byte[] temp = new byte[56];
			for (ushort i = 0; i < 16; i++)
			{
				ix = encrypt ? (ushort) i : (ushort) (15 - i);
				m_Keys[ix,0] = m_Keys[ix,1] = 0;
				for (j = 0; j < 28; j++)
				{
					bitPos = (ushort) (j + KROT[i]);
					temp[j] = (bitPos < 28) ? work[bitPos] : work[bitPos - 28];
				}
				for (j = 28; j < 56; j++)
				{
					bitPos = (ushort) (j + KROT[i]);
					temp[j] = (bitPos < 56) ? work[bitPos] : work[bitPos - 28];  
				}
				for (j = 0; j < 24; j++)
				{
					if (temp[K48[j]] != 0)
						m_Keys[ix,0] |= KeyMask[j];
					if (temp[K48[j+24]] != 0)
						m_Keys[ix,1] |= KeyMask[j];
				}
			}
		      
			// assemble DES key schedule
			uint k0, k1;
			for (ushort i = 0; i < 16; i++)
			{
				k0 = m_Keys[i,0];
				k1 = m_Keys[i,1];
		        
				m_Keys[i,0] = ((k0 & 0x00fc0000) <<  6) |
					((k0 & 0x00000fc0) << 10) |
					((k1 & 0x00fc0000) >> 10) |
					((k1 & 0x00000fc0) >>  6);
		        
				m_Keys[i,1] = ((k0 & 0x0003f000) << 12) |
					((k0 & 0x0000003f) << 16) |
					((k1 & 0x0003f000) >>  4) |
					((k1 & 0x0000003f));
			}

		}
	
		public void put_Key (byte[] key, bool encrypt)
		{
			CookKeys(key, encrypt);
		}
	//------------------------------------------------------------------------

		//  DES
		//      Encrypt one 64-bit block using current keys
		//      (IN) void * data    Data to encrypt
		public void DES (ref byte[] data)
		{
			uint left = (uint) ((uint) data[0] + (uint) (data[1] << 8) + (uint) (data[2] << 16) + (uint) (data[3] << 24));
			uint right = (uint) ((uint) data[4] + (uint) (data[5] << 8) + (uint) (data[6] << 16) + (uint) (data[7] << 24));
		    
			// Initial Permutation
			Permute(ref right, ref left,  4, 0x0f0f0f0f);
			Permute(ref left, ref right, 16, 0x0000ffff);
			Permute(ref right, ref left,  2, 0x33333333);
			Permute(ref left, ref right,  8, 0x00ff00ff);
			Permute(ref right, ref left,  1, 0x55555555);

			// At this point, right and left are reversed. Fix
			// this below:

			uint temp = ((left << 1) | (left >> 31)) & 0xffffffff;
			left = ((right << 1) | (right >> 31)) & 0xffffffff;
			right = temp;

			// Encryption: 16 rounds. Loop is unwound for speed.
			DES_Round(ref left, right, m_Keys[ 0,0], m_Keys[ 0,1]);
			DES_Round(ref right, left, m_Keys[ 1,0], m_Keys[ 1,1]);
			DES_Round(ref left, right, m_Keys[ 2,0], m_Keys[ 2,1]);
			DES_Round(ref right, left, m_Keys[ 3,0], m_Keys[ 3,1]);
			DES_Round(ref left, right, m_Keys[ 4,0], m_Keys[ 4,1]);
			DES_Round(ref right, left, m_Keys[ 5,0], m_Keys[ 5,1]);
			DES_Round(ref left, right, m_Keys[ 6,0], m_Keys[ 6,1]);
			DES_Round(ref right, left, m_Keys[ 7,0], m_Keys[ 7,1]);
			DES_Round(ref left, right, m_Keys[ 8,0], m_Keys[ 8,1]);
			DES_Round(ref right, left, m_Keys[ 9,0], m_Keys[ 9,1]);
			DES_Round(ref left, right, m_Keys[10,0], m_Keys[10,1]);
			DES_Round(ref right, left, m_Keys[11,0], m_Keys[11,1]);
			DES_Round(ref left, right, m_Keys[12,0], m_Keys[12,1]);
			DES_Round(ref right, left, m_Keys[13,0], m_Keys[13,1]);
			DES_Round(ref left, right, m_Keys[14,0], m_Keys[14,1]);
			DES_Round(ref right, left, m_Keys[15,0], m_Keys[15,1]);

			left = ((left >> 1)|(left << 31)) & 0xffffffff;
			right = ((right >> 1)|(right << 31)) & 0xffffffff;

			// Final Permutation
			Permute(ref right, ref left,  1, 0x55555555);
			Permute(ref left, ref right,  8, 0x00ff00ff);
			Permute(ref right, ref left,  2, 0x33333333);
			Permute(ref left, ref right, 16, 0x0000ffff);
			Permute(ref right, ref left,  4, 0x0f0f0f0f);

			// left and right have been switched again. Fix this
			// when reassigning to output block:
			data[3] = (byte) ((left >> 24) & 0xff);
			data[2] = (byte) ((left >> 16) & 0xff);
			data[1] = (byte) ((left >> 8) & 0xff);
			data[0] = (byte) ((left) & 0xff);

			data[7] = (byte) ((right >> 24) & 0xff);
			data[6] = (byte) ((right >> 16) & 0xff);
			data[5] = (byte) ((right >> 8) & 0xff);
			data[4] = (byte) ((right) & 0xff);
		}
		//------------------------------------------------------------------------
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//      class CCrypt Implementation
	//
	//////////////////////////////////////////////////////////////////////////
	internal class CCrypt
	{
		byte[][] m_Keys;           // "Raw" Keys
		byte[] m_LastBlock = new byte[8];         // Last cipher block (for CBC mode)
		CCSCDES E1 = new CCSCDES(), D1 = new CCSCDES(), E2 = new CCSCDES();                      // DES "engines"
		bool m_Encrypting;                    // TRUE if encrypting
		bool UserKeyLoaded;
		byte[][] UserKey;

		byte[] UserKeyMaster_1 = { 0x47, 0x15, 0x16, 0x8D, 0x29, 0xCD, 0x64, 0xB9};
		byte[] UserKeyMaster_2 = { 0xA0, 0x77, 0xEF, 0xEC, 0xAE, 0xD2, 0x78, 0x35};
		byte[] UserKeyMaster_3 = { 0xBE, 0x58, 0xF9, 0x16, 0xDA, 0xE4, 0x85, 0x4B};
		

		//========== Initial vector ========
		byte[] IV = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef };

		public CCrypt()
		{
			UserKeyLoaded = false;
		}

		//------------------------------------------------------------------------

		//  SetKey
		//      Set User DES keys
		//      RETURNS:    TRUE if successful, FALSE if couldn't read key file
		public bool SetKey ()
		{
			if (!UserKeyLoaded)
			{
				// Decrypt user key
				UserKey = new byte[3][];
				UserKey[0] = UserKeyMaster_1;
				UserKey[1] = UserKeyMaster_2;
				UserKey[2] = UserKeyMaster_3;

				UserKeyLoaded = true;
			}

			m_Keys = new byte[3][];
			m_Keys[0] = UserKey[0];
			m_Keys[1] = UserKey[1];
			m_Keys[2] = UserKey[2];

			return true;
		}
		//------------------------------------------------------------------------

		//  Encrypt
		//      Encrypt buffer in-place, using currently set key
		//      (IN/OUT) void * Data    Data to encrypt
		//      (IN) int len            Length of Data buffer
		public void Encrypt (ref byte[] Data, int len)
		{
			E1.put_Key(m_Keys[0], true);
			D1.put_Key(m_Keys[1], false);
			E2.put_Key(m_Keys[2], true);
			IV.CopyTo(m_LastBlock, 0);
			m_Encrypting = true;
			D3DES_CBC(ref Data, len);
		}
		//------------------------------------------------------------------------
		  
		//  Decrypt
		//      Decrypt buffer in-place, using currently set key
		//      (IN/OUT) void * Data    Data to decrypt
		//      (IN) WORD length        Length of Data buffer
		public void Decrypt (ref byte[] Data, int len)
		{
			E1.put_Key(m_Keys[2], false);
			D1.put_Key(m_Keys[1], true);
			E2.put_Key(m_Keys[0], false);
			IV.CopyTo(m_LastBlock, 0);
			m_Encrypting = false;
			D3DES_CBC(ref Data, len);
		}
		//------------------------------------------------------------------------

		//  D3DES_CBC
		//      Perform Triple-DES in CBC mode, using current state
		//      Requires Data buffer to be multiple of 8 bytes in length.
		//      (IN/OUT) void * Data    Data to transform
		//      (IN) WORD length        length of Data buffer
		void D3DES_CBC (ref byte[] Data, int length)
		{
			byte[] work = new byte[8];
			uint ix;

			for (uint pos = 0; pos < length; pos += 8)
			{
				// Copy 64 bits from buffer for encryption (we encrypt 64 bits at a time)
				for (ix = 0; ix < 8; ix++)
					work[ix] = Data[pos + ix];

				//  Cipher Block Chaining
				if (m_Encrypting)
					for (ix = 0; ix < 8; ix++)
						work[ix] ^= m_LastBlock[ix];

				//  Run Triple-DES
				E1.DES(ref work);
				D1.DES(ref work);
				E2.DES(ref work);

				if (!m_Encrypting)
					for (ix = 0; ix < 8; ix++)
						work[ix] ^= m_LastBlock[ix];

				if (m_Encrypting)
					for (ix = 0; ix < 8; ix++)
						m_LastBlock[ix] = work[ix];
				else
					for (ix = 0; ix < 8; ix++)
						m_LastBlock[ix] = Data[pos + ix];

				// Copy encrypted results (64 bits) back into source data (this is an in place encryption)
				for (ix = 0; ix < 8; ix++)
					Data[pos + ix] = work[ix];
			}
		}
	}
//------------------------------------------------------------------------

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class DTGCryptException : Exception
	{
		public DTGCryptException()
		{
		}

		public DTGCryptException(string sMsg) : base(sMsg)
		{
		}

		public DTGCryptException(string sMsg, Exception inner) : base(sMsg, inner)
		{
		}

	}
	public class DTGCryptFormatException : DTGCryptException
	{
		 public DTGCryptFormatException()
		 {
		 }

		 public DTGCryptFormatException(string sMsg) : base(sMsg)
		 {
		 }

		 public DTGCryptFormatException(string sMsg, Exception inner) : base(sMsg, inner)
		 {
		 }
	 }
	
    /// <summary>
    /// Encapsulates the legacy Cryptography algorithm
    /// required for backwards compliance with RISKMASTER World/Riskmaster Net
    /// applications
    /// </summary>
	public class DTGCrypt32
	{

        private static string AUTHKEY
        {
            get
            {
                return "6378b87457a5ecac8674e9bac12e7cd9";
            }//get
        }//property: AUTHKEY

		private CCrypt m_Mach = new CCrypt();       // Encryption/Decryption

		public DTGCrypt32()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private byte[] GetCopy(string str, int bias)
			/* 
			 * Returns a byte array representing the string padded to specified bias.
			 * 
			 */
		{
			UTF8Encoding pEncode = new UTF8Encoding();

			byte[] work = pEncode.GetBytes(str);
			byte[] work2 = new byte[work.Length + bias + 1];
			work.CopyTo(work2, 0);
			for (int i=work.Length; i < (work.Length+bias+1); i++)
				work2[i] = 0;

			return work2;
		}
		//Break an uint Array into an equivalent byte Array.
		public static byte[] GetByteArr(uint[] uintArr)
		{
			byte[] byteArr = new byte[4* uintArr.Length];
			ushort[] wordArr = new ushort[2];
			int iSrc;
			int i=0;
			foreach(uint Item in uintArr)
			{
				//It is perfectly acceptable to us
				// to have the value "roll negative" during conversion,
				unchecked{iSrc = (int)(Item);}
				GetHILOWord(iSrc,out wordArr[0],out wordArr[1]);
				GetHILOByte(wordArr[0],out byteArr[(i*4) + 0],out byteArr[(i*4) + 1]);
				GetHILOByte(wordArr[1],out byteArr[(i*4) + 2],out byteArr[(i*4) + 3]);
				i++;
			}
			return byteArr;
		}

		public static void GetHILOWord(int Src, out ushort iLO,out ushort iHI)
		{
			unchecked
			{
				iLO =  (ushort)(Src & 0x0000FFFF);
				iHI = (ushort)((Src >> 16) & 0x0000FFFF);
			}
		}
		public static void GetHILOByte(ushort Src, out byte iLO,out byte iHI)
		{
			unchecked
			{
				iLO = (byte)(Src & 0x00FF);
				iHI = (byte)((Src >> 8) & 0x00FF);
			}
		}
		private string PutWithTransparencyChars(byte[] buf, int len)  
			/* 
			 * Turns 'buf' containing 'real' data into a hexadecimal string suitable for database
			 * storage. Implemented because storing NUL characters (and possibly other binary data)
			 * in a database creates problems.
			 */
		{
			string sWork = "";

			for(int i=0;i<len;i++)
				sWork += String.Format("{0:x2}", buf[i]);

			return sWork;
		}

		private byte[] RemoveTransparencyChars(string szStr)
			/* 
			 * Assumes 'str' is a string of two-character hex values; converts each pair int one
			 * ASCII character. Result comes back in 'dest'.
			 */
		{                              
			byte[] sArr = new Byte[szStr.Length / 2];

			int length = szStr.Length;
			for(int i=0;i<length;i+=2)
			{
				try{sArr[i / 2] = Byte.Parse(szStr.Substring(i, 2), NumberStyles.HexNumber);}
				catch(Exception e)
				{throw new DTGCryptFormatException("Decryption failed for value:"+ szStr);}
			}

			return sArr;
		}

		private string EncryptStringA(string str)
			/* 
			* Encrypt 'clear' data in 'str', and pad with transparency characters. We assume that
			* the contents of 'str' are non binary, IE, alphanumeric.
			*/
		{   
			/* 
			* Transfer 'str' contents into 'buf', pad to an 8 
			* character aligned boundary.
			*/

			byte[] buf = GetCopy(str,8);

			// Make length a multiple of 8 since we encrypt/decrypt in 64bit chunks
			int len = str.Length;
			if( (len % 8) != 0)
				len += (8 - (len % 8));

			// encrypt
			m_Mach.SetKey();
			m_Mach.Encrypt(ref buf, len);

			// return encrypted buffer encoded as hex ascii
			return PutWithTransparencyChars(buf, len);
		}

        /// <summary>
        /// Encrypts the specified clear text string and returns
        /// the encrypted string
        /// </summary>
        /// <param name="sString">string containing the clear text/plain text</param>
        /// /// <param name="sAuthPwd">string containing the salt key to be used for symmetric key encryption</param>
        /// <returns>string containing the encrypted text</returns>
		internal string EncryptString(string sString, string sAuthPwd)
		{
			if (sAuthPwd != AUTHKEY)
				throw(new DTGCryptException("Invalid Authorization Key. You don't have permission to use this library."));

			if (sString==null || sString.Length == 0)
				return "";

			return EncryptStringA(sString);
		}

		private string DecryptStringA(string str)
		/* 
		* Decrypt data in 'str', which is padded with transparency characters.
		*/
		{
			// Convert from hex ascii string back to byte array
			byte[] buf = RemoveTransparencyChars(str);

			// Pad for decryption
			byte[] buf2 = new byte[buf.Length + 8 + 1];
			buf.CopyTo(buf2, 0);
			for (int i=buf.Length; i < (buf.Length+8+1); i++)
				buf2[i] = 0;
			
			// Make length a multiple of 8 since we encrypt/decrypt in 64bit chunks
			int len = str.Length / 2;
			if( (len % 8) != 0 )
				len += (8 - (len % 8));

			// decrypt
			m_Mach.SetKey();
			m_Mach.Decrypt(ref buf, len);

			// Take decrypted data from byte buffer & turn back into UTF8 string
			UTF8Encoding oEncode = new UTF8Encoding();
			
			int count=0;
			for(int i=buf.Length-1;i>=0;i--)
			{
				if(buf[i]!=0)
				{
					count=i+1;
					break;
				}
			}
            
			return oEncode.GetString(buf,0,count);
		}

        /// <summary>
        /// Decrypts the specified encrypted string and returns
        /// the string in clear text
        /// </summary>
        /// <param name="sEncString">string containing the currently encrypted contents</param>
        /// <param name="sAuthPwd">string containing the salt key to be used for symmetric key encryption</param>
        /// <returns>string in clear text form/decrypted</returns>
		internal string DecryptString(string sEncString, string sAuthPwd)
		{
			if (sAuthPwd != AUTHKEY)
				throw(new DTGCryptException("Invalid Authorization Key. You don't have permission to use this library."));

			if (sEncString==null || sEncString.Length == 0)
				return "";

			return DecryptStringA(sEncString);
		}

		internal uint[] ComputeHash(byte[] byteArray, string sAuthPwd)
		{
			CSCMD5 oHash = new CSCMD5();

			if (sAuthPwd != AUTHKEY)
				throw(new DTGCryptException("Invalid Authorization Key. You don't have permission to use this library."));

			return oHash.MD5Hash(byteArray, (uint) byteArray.Length);
		}

        /// <summary>
        /// Computes the Hash value for a byte array
        /// </summary>
        /// <param name="byteArray">byte array containing a value to be hashed</param>
        /// <param name="sAuthPwd">string containing the Hash key</param>
        /// <returns>Hashed value for the byte array</returns>
		internal string ComputeHashAsString(byte[] byteArray, string sAuthPwd)
		{
			if (sAuthPwd != AUTHKEY)
				throw(new DTGCryptException("Invalid Authorization Key. You don't have permission to use this library."));

			// convert return buffer into string
            return PutWithTransparencyChars(GetByteArr(ComputeHash(byteArray, sAuthPwd)), 16);
		}//method: ComputeHashAsString()

	}
}
