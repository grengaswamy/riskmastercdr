﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Riskmaster.Security.Encryption
{
    /// <summary>
    /// Encapsulates the CSC RISKMASTER Cryptography algorithms
    /// for encrypting string contents
    /// </summary>
    public class RMCryptography
    {
        private const string CODE_ACCESS_PUBLICKEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";
        
        #region Riskmaster Legacy TripleDES Cryptography Algorithms
        private static string CRYPTKEY
        {
            get
            {
                return "6378b87457a5ecac8674e9bac12e7cd9";
            }//get
        }//property: CRYPTKEY

        /// <summary>
        /// Encrypts the specified clear text string and returns
        /// the encrypted string
        /// </summary>
        /// <param name="strUnencrypted">string containing the clear text/plain text</param>
        /// <returns>string containing the encrypted text</returns>
        [StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLICKEY)]
        public static string EncryptString(string strUnencrypted)
        {
            string strEncrypted = string.Empty;

            DTGCrypt32 objDTGCrypt = new DTGCrypt32();
            strEncrypted = objDTGCrypt.EncryptString(strUnencrypted, CRYPTKEY);

            //Clean up
            objDTGCrypt = null;

            return strEncrypted;
        } // method: EncryptString

        /// <summary>
        /// Decrypts the specified encrypted string and returns
        /// the string in clear text
        /// </summary>
        /// <param name="strEncrypted">string containing the currently encrypted contents</param>
        /// <returns>string in clear text form/decrypted</returns>
        [StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLICKEY)]
        public static string DecryptString(string strEncrypted)
        {
            string strDecrypted = string.Empty;

            DTGCrypt32 objDTGCrypt = new DTGCrypt32();
            strDecrypted = objDTGCrypt.DecryptString(strEncrypted, CRYPTKEY);

            //Clean up
            objDTGCrypt = null;

            return strDecrypted;
        } // method: DecryptString

        /// <summary>
        /// Computes the Hash value for a byte array
        /// </summary>
        /// <param name="bytArray">byte array containing a value to be hashed</param>
        /// <returns>Hashed value for the byte array</returns>
        [StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLICKEY)]
        public static string ComputeHashAsString(byte[] bytArray)
        {
            string strHashValue = string.Empty;

            DTGCrypt32 objDTGCrypt = new DTGCrypt32();
            strHashValue = objDTGCrypt.ComputeHashAsString(bytArray, CRYPTKEY);

            //Clean up
            objDTGCrypt = null;

            return strHashValue;
        }//method: ComputeHashAsString()

        /// <summary>
        /// Computes a Hash Value given a byte array
        /// </summary>
        /// <param name="bytArray">byte array containing value to be hashed</param>
        /// <returns>string containing the resultant computed Hash</returns>
        [StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLICKEY)]
        public static uint[] ComputeHash(byte[] bytArray)
        {
            uint[] intHashValue;

            DTGCrypt32 objCrypt = new DTGCrypt32();

            intHashValue = objCrypt.ComputeHash(bytArray, CRYPTKEY);

            //Clean up
            objCrypt = null;

            return intHashValue;
        } // method: ComputeHash 
        #endregion


        #region Enterprise Library Cryptography Algorithms
        private static string HASH_PROVIDER
        {
            get
            {
                return "SHA256Managed";
            }//get
        }//property: HASH_PROVIDER

        private static string CRYPTOGRAPHY_PROVIDER
        {
            get
            {
                return "RijndaelManaged";
            }//get
        }//property: CRYPTOGRAPHY_PROVIDER

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPlainText"></param>
        /// <returns></returns>
        public static string EncryptSymmetric(string strPlainText)
        {
            return Cryptographer.EncryptSymmetric(CRYPTOGRAPHY_PROVIDER, strPlainText);
        }//method: EncryptSymmetric

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEncrypted"></param>
        /// <returns></returns>
        public static string DecryptSymmetric(string strEncrypted)
        {
            return Cryptographer.DecryptSymmetric(CRYPTOGRAPHY_PROVIDER, strEncrypted);
        }//method: DecryptSymmetric

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPlainText"></param>
        /// <returns></returns>
        public static string CreateHash(string strPlainText)
        {
            return Cryptographer.CreateHash(HASH_PROVIDER, strPlainText);
        }//method: CreateHash

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPlainText"></param>
        /// <param name="strHashedText"></param>
        /// <returns></returns>
        public static bool CompareHash(string strPlainText, string strHashedText)
        {
            return Cryptographer.CompareHash(HASH_PROVIDER, strPlainText, strHashedText);
        }//method: CompareHash 
        #endregion

    }//class

    public class CryptographyKeyManager
    {
        //private class members
        private const string SECURITYKEYFILE = "XFileSecurity.key";
        private const string KEYIMPORTFILE = "ColorIndex.txt";
        private const string KEYIMPORTFILE_PW = "color2007";

        private bool ApplicationSecured(string strKeyDirectoryPath)
        {
            bool secure = false;

            string importFile = Path.Combine(strKeyDirectoryPath, KEYIMPORTFILE);
            string keyFile = Path.Combine(strKeyDirectoryPath, SECURITYKEYFILE);

            if (File.Exists(keyFile))
            {
                secure = true;
            }//if
            else if (!File.Exists(keyFile))
            {
                ProtectedKey key = KeyManager.RestoreKey(File.Open(importFile, FileMode.Open, FileAccess.Read), KEYIMPORTFILE_PW, System.Security.Cryptography.DataProtectionScope.LocalMachine);
                KeyManager.Write(File.Open(keyFile, FileMode.Create), key);

                secure = true;
            }//if

            return secure;

            //Private Function ApplicationSecured() As Boolean
            //Dim secure As Boolean = False
            //Dim sCurrentPath As String = Application.ExecutablePath
            //Do Until sCurrentPath.EndsWith("\")
            //sCurrentPath = sCurrentPath.Remove(sCurrentPath.Length - 1)
            //Loop
            //Dim importFile As String = sCurrentPath & KEYIMPORTFILE
            //Dim keyFile As String = sCurrentPath & SECURITYKEYFILE
            //If Not My.Computer.FileSystem.FileExists(keyFile) Then
            //Dim key As ProtectedKey = KeyManager.RestoreKey(File.Open(importFile, FileMode.Open, FileAccess.Read), KEYIMPORTFILE_PW, DataProtectionScope.LocalMachine)
            //KeyManager.Write(File.Open(keyFile, FileMode.Create), key)
            //End If
            //Dim cfg As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            //If cfg.HasFile Then
            //Dim cryptoSection As ConfigurationSection = cfg.Sections("securityCryptographyConfiguration")
            //If Not cryptoSection Is Nothing Then
            //Dim cryptoSettings As CryptographySettings = cryptoSection
            //Dim data As SymmetricProviderData = cryptoSettings.SymmetricCryptoProviders.Get(0)
            //Dim sSetPath As String = data.ElementInformation.Properties("protectedKeyFilename").Value.ToString()
            //If sSetPath <> keyFile Then
            //data.ElementInformation.Properties("protectedKeyFilename").Value = keyFile
            //cfg.Save(ConfigurationSaveMode.Minimal)
            //End If
            //secure = True
            //End If
            //End If
            //Return secure
            //End Function
        }//method: ApplicationSecured
    }//class


}
