﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Configuration;
using ComponentSoft.Saml2;
using System.Security.Cryptography.X509Certificates;





namespace Riskmaster.Security.Authentication
{
    public class SamlAuthentication
    {
        public SamlAuthentication()
        {

        }//constructor


        /// <summary>
        /// Generates a Saml Assertion to be used by Windows Identify Foundation-supported
        /// applications such as Stratacare
        /// </summary>
        /// <param name="samlInfo">SamlAssertionInfo object instance</param>
        /// <returns>instance of a SamlAssertion to be utilized by Windows Identity Foundation</returns>
        public static Assertion GenerateSamlAssertion(SamlAssertionInfo samlInfo)
        {
            Assertion samlAssertion = new Assertion { Issuer = samlInfo.SamlIssuer, Conditions = new Conditions() };
            samlAssertion.Conditions.NotBefore = DateTime.UtcNow;
            samlAssertion.Conditions.NotOnOrAfter = DateTime.UtcNow.AddHours(4.0);
            AudienceRestriction ad = new AudienceRestriction();
            ad.Audiences.Add(new Audience(samlInfo.AudienceUri));
            samlAssertion.Conditions.ConditionsList.Add(ad);

            // Use the specified user name to establish the identity
            Subject subject = new Subject(new NameId(samlInfo.UserName));
            SubjectConfirmation subjectConfirmation = new SubjectConfirmation(SamlSubjectConfirmationMethod.Bearer);
            SubjectConfirmationData subjectConfirmationData = new SubjectConfirmationData { Recipient = samlInfo.SubjectRecipient };
            subjectConfirmation.SubjectConfirmationData = subjectConfirmationData;
            subject.SubjectConfirmations.Add(subjectConfirmation);
            samlAssertion.Subject = subject;
            samlAssertion.IssueInstant = DateTime.UtcNow;

            // Create a new authentication statement.
            AuthnStatement authnStatement = new AuthnStatement { AuthnContext = new AuthnContext() };
            authnStatement.AuthnContext.AuthnContextClassRef = new AuthnContextClassRef(SamlAuthenticateContext.Password);
            authnStatement.SessionNotOnOrAfter = DateTime.UtcNow.AddHours(4.0);
            samlAssertion.Statements.Add(authnStatement);

            //Add additional Saml Assertion statements
            foreach (KeyValuePair<string, string> kvp in samlInfo.ClaimSetInfo)
            {
                samlAssertion.Statements.Add(CreateAttributeStatement(kvp.Key, kvp.Value));
            }//foreach

            //Sign the certificate
            samlAssertion.Sign(samlInfo.SigningCertificate);

            return samlAssertion;
        }//method: GenerateSamlAssertion();

        /// <summary>
        /// Creates a set of Attributes required for a SamlAssertion
        /// </summary>
        /// <param name="claimName">string containing the claim name</param>
        /// <param name="claimValue">string containing the claim value</param>
        /// <returns>AttributeStatement</returns>
        private static AttributeStatement CreateAttributeStatement(string claimName, string claimValue)
        {

            AttributeStatement statement = new AttributeStatement();
            ComponentSoft.Saml2.Attribute attribute = new ComponentSoft.Saml2.Attribute() { Name = claimName };
            attribute.Values.Add(new AttributeValue(claimValue));

            statement.Attributes.Add(attribute);
            return statement;

        }//method: CreateAttributeStatement();

    }
}

