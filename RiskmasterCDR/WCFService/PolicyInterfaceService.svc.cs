﻿using System;
using System.Data;
using System.ServiceModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.Collections.Specialized;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using Riskmaster.BusinessAdaptor.RMUtilities;	//abhal3 MITS 36046 


namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PolicyInterfaceService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(
     RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif

    public class PolicyInterfaceService : RMService, IPolicyInterfaceService
    {
        int m_ClientId = 0;
        public void GetPolicySystemList(PolicySystemList oRequest, out PolicySystemList oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicySystemList";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                bresult = objPolicySystemAdapter.GetPolicySystemList(out oResponse, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicySearchResult(PolicySearch oSearchRequest, out PolicySearch oSearchResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicySearchAcordRequest,GetPolicySearchFormattedResult";
            bool bresult = false;
            string oSearchAcordRequest = string.Empty;
            string oSearchAcordResponse = string.Empty;
            oSearchResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSearchRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSearchRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSearchRequest.ClientId);
                oSearchAcordRequest = objPolicySystemAdapter.GetPolicySearchAcordRequest(oSearchRequest, ref oSystemErrors);
                if (!string.IsNullOrEmpty(oSearchAcordRequest))
                {
                    oSearchAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSearchRequest.objSearchFilters.PolicySystemId, oSearchAcordRequest, ref oSystemErrors);
                    //Temporary we are getting static response from local methods
                    //oSearchAcordResponse = objPolicySystemAdapter.GetPolicySearchAcordResponse(oSearchAcordRequest, ref oSystemErrors);
                    //To Do: End
                    if (!string.IsNullOrEmpty(oSearchAcordResponse))
                    {
                        bresult = objPolicySystemAdapter.GetPolicySearchFormattedResult(oSearchAcordResponse, out oSearchResponse, ref oSystemErrors);
                        DataTable oDT = objPolicySystemAdapter.GetPolicySystemInfoById(oSearchRequest.objSearchFilters.PolicySystemId, ref oSystemErrors);
                        if (oDT != null && oDT.Rows.Count > 0)
                        {
                            string sSystemTypeName = oDT.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();
                            if (string.Equals(sSystemTypeName, "POINT"))
                            {
                                string sPointClientFileEnable = oDT.Rows[0]["CLIENTFILE_FLAG"].ToString();
                                oSearchResponse.ClientFile = sPointClientFileEnable == "1" ? "true" : "false";
                            }
                        }
                        oDT = null;
                    }
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        
        public void GetXml(RMServiceType oRMServiceType, out DisplayFileData oDisplayFileData)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetXml";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRMServiceType.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRMServiceType, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRMServiceType.ClientId);
                bresult = objPolicySystemAdapter.GetXml(out oDisplayFileData, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            //return bresult;
        }
        public void GetUnitXml(RMServiceType oRMServiceType, out DisplayFileData oDisplayFileData)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetXml";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRMServiceType.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRMServiceType, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRMServiceType.ClientId);
                bresult = objPolicySystemAdapter.GetUnitXml(out oDisplayFileData, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            //return bresult;
        }
        public void GetDuplicateOptions(SaveDownloadOptions oSaveDownloadOptions, out DisplayFileData oDisplayFileData)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetDuplicateOptions";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSaveDownloadOptions.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSaveDownloadOptions, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSaveDownloadOptions.ClientId);
                bresult = objPolicySystemAdapter.GetDuplicateOptions(oSaveDownloadOptions, out oDisplayFileData, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            //return bresult;
        }

        public bool SavePolicyDataForUtility(ref SaveDownloadOptions oSaveDownloadOptions)
        {
            XmlDocument xmlRequest = null;
            XmlDocument objDocument;

            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;

            string sFunctionName = "SavePolicyDataForUtility";
            string sFileData = string.Empty;
            string[] sUnitNumberList;
            string[] sRiskLocList;
            string[] sRiskSubLocList;
            string[] sStatUnitNumberList;
            string sUnitNumber, sRiskLoc, sRiskSubLoc, sStatUnitNumber;
            string sAccordRequest = string.Empty, sAccordResponse = string.Empty;
            string sSelectedValue = string.Empty, sUnitRowId = string.Empty;
            string sCommaSeperatdUnitValues = string.Empty;

            bool bresult = false;

            //int iCounter = 0;
            int iCtIndex = 0;
            int iInsertedId = Int32.MinValue;

            XmlNodeList objNodeList = null;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSaveDownloadOptions.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSaveDownloadOptions, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSaveDownloadOptions.ClientId);
                //oSaveDownloadOptions.PolicySystemId = objPolicySystemAdapter.GetPolicySystemId(oSaveDownloadOptions.PolicyId, ref oSystemErrors);

                Log.Write("Save entity begin...", oSaveDownloadOptions.ClientId);
                sFileData = objPolicySystemAdapter.ReadFileContent("EntityList");
                objDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(sFileData))
                {
                    objDocument.LoadXml(sFileData);
                    if (objDocument != null)
                    {
                        objNodeList = objDocument.SelectNodes("//SequenceNumber");
                        //oSaveDownloadOptions.Mode = "entity";

                        foreach (XmlNode objNode in objNodeList)
                        {
                            //  oSaveDownloadOptions.SelectedValue = objNode.InnerText;
                            sSelectedValue = objNode.InnerText;
                            sAccordRequest = objPolicySystemAdapter.GetPolicyInterestDetailSaveAcordRequest(sSelectedValue, sFileData, oSaveDownloadOptions, ref sUnitRowId, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                iInsertedId = objPolicySystemAdapter.GetEntitySaveFormattedResult(sAccordResponse, oSaveDownloadOptions, sSelectedValue, string.Empty, ref oSystemErrors);
                            }
                        }
                    }
                }
                else
                {
                    Log.Write("EntityList file is empty. No record to save.", oSaveDownloadOptions.ClientId);
                }
                Log.Write("Save entity end...", oSaveDownloadOptions.ClientId);


                Log.Write("Save driver begin...", oSaveDownloadOptions.ClientId);
                sFileData = objPolicySystemAdapter.ReadFileContent("DRIVERLIST");
                objDocument = new XmlDocument();
                sSelectedValue = string.Empty;
                if (!string.IsNullOrEmpty(sFileData))
                {
                    objDocument.LoadXml(sFileData);

                    if (objDocument != null)
                    {
                        //oSaveDownloadOptions.Mode = "driver";
                        objNodeList = objDocument.SelectNodes("//SequenceNumber");
                        foreach (XmlNode objNode in objNodeList)
                        {
                            //oSaveDownloadOptions.SelectedValue = objNode.InnerText;
                            //SaveOptions(ref oSaveDownloadOptions);
                            sSelectedValue = objNode.InnerText;
                            sAccordRequest = objPolicySystemAdapter.GetPolicyDriverDetailSaveAcordRequest(sSelectedValue, sFileData, oSaveDownloadOptions, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                iInsertedId = objPolicySystemAdapter.GetDriverDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions, ref oSystemErrors);
                            }
                        }
                    }
                }
                else
                {
                    Log.Write("DriverList file is empty. No record to save.", oSaveDownloadOptions.ClientId);
                }
                Log.Write("Save driver end...", oSaveDownloadOptions.ClientId);


                Log.Write("Save Unit begin...", oSaveDownloadOptions.ClientId);
                sFileData = objPolicySystemAdapter.ReadFileContent("UnitList");
                objDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(sFileData))
                {
                    objDocument.LoadXml(sFileData);

                    if (objDocument != null)
                    {
                        objNodeList = objDocument.SelectNodes("Units/Instance/Unit");
                        oSaveDownloadOptions.Mode = "unit";

                        sUnitNumberList = oSaveDownloadOptions.UnitNumber.Split('|');
                        sRiskLocList = oSaveDownloadOptions.RiskLocation.Split('|');
                        sRiskSubLocList = oSaveDownloadOptions.RiskSubLocation.Split('|');
                        sStatUnitNumberList = oSaveDownloadOptions.StatUnitNumber.Split('|');

                        sCommaSeperatdUnitValues = string.Empty;
                        iCtIndex = 0;

                        if (oSaveDownloadOptions.StatUnitNumber.Trim() == string.Empty)                        //if (oSaveDownloadOptions.UnitNumber.Trim() == string.Empty)        
                        {
                            // means  oSaveDownloadOptions.UnitNumber = "" ;
                            //we do not have any unit number or risk loc or risk sub loc mentioned in excel and need to download all units
                            foreach (XmlNode objNode in objNodeList)
                            {
                                if (sCommaSeperatdUnitValues == string.Empty)
                                    sCommaSeperatdUnitValues = objNode.SelectSingleNode("SequenceNumber").InnerText;
                                else
                                    sCommaSeperatdUnitValues = sCommaSeperatdUnitValues + "," + objNode.SelectSingleNode("SequenceNumber").InnerText;
                            }
                        }
                        else
                        {
                            while (iCtIndex < sStatUnitNumberList.Length)
                            {
                                sStatUnitNumber = sStatUnitNumberList[iCtIndex].Trim();
                                sUnitNumber = sUnitNumberList[iCtIndex].Trim();
                                sRiskLoc = sRiskLocList[iCtIndex].Trim();
                                sRiskSubLoc = sRiskSubLocList[iCtIndex].Trim();

                                if (sStatUnitNumber != "00000") // this is a stat unit or site unit
                                {
                                    foreach (XmlNode objNode in objNodeList)
                                    {
                                        if (sStatUnitNumber == objNode.SelectSingleNode("StatUnitNo").InnerText)
                                        {
                                            if (sCommaSeperatdUnitValues == string.Empty)
                                                sCommaSeperatdUnitValues = objNode.SelectSingleNode("SequenceNumber").InnerText;
                                            else
                                                sCommaSeperatdUnitValues = sCommaSeperatdUnitValues + "," + objNode.SelectSingleNode("SequenceNumber").InnerText;
                                            break;
                                        }
                                    }
                                }
                                else if (sStatUnitNumber == "00000") // use risk loc, risk sub loc and unit number combination
                                {
                                    foreach (XmlNode objNode in objNodeList)
                                    {
                                        if (sUnitNumber == objNode.SelectSingleNode("UnitNo").InnerText &&
                                               sRiskLoc == objNode.SelectSingleNode("RiskLoc").InnerText &&
                                               sRiskSubLoc == objNode.SelectSingleNode("RiskSubLoc").InnerText)
                                        {
                                            if (sCommaSeperatdUnitValues == string.Empty)
                                                sCommaSeperatdUnitValues = objNode.SelectSingleNode("SequenceNumber").InnerText;
                                            else
                                                sCommaSeperatdUnitValues = sCommaSeperatdUnitValues + "," + objNode.SelectSingleNode("SequenceNumber").InnerText;
                                            break;
                                        }
                                    }
                                }

                                iCtIndex++;
                            }
                        }
                        #region
                        //else if (oSaveDownloadOptions.RiskLocation.Trim() == string.Empty && oSaveDownloadOptions.RiskSubLocation.Trim() == string.Empty && oSaveDownloadOptions.UnitNumber.Trim() != string.Empty)
                        //{
                        //    // means we have unit numbers like 00001|00002 format and risk location, risk sub location is empty in excel file; WCV case..
                        //    while (iCtIndex < sUnitNumberList.Length)
                        //    {
                        //        sUnitNumber = sUnitNumberList[iCtIndex];
                        //        foreach (XmlNode objNode in objNodeList)
                        //        {
                        //            if (sUnitNumber == objNode.SelectSingleNode("StatUnitNo").InnerText)
                        //            {
                        //                if (sCommaSeperatdUnitValues == string.Empty)
                        //                    sCommaSeperatdUnitValues = objNode.SelectSingleNode("SequenceNumber").InnerText;
                        //                else
                        //                    sCommaSeperatdUnitValues = sCommaSeperatdUnitValues + "," + objNode.SelectSingleNode("SequenceNumber").InnerText;
                        //                break;
                        //            }
                        //        }
                        //        iCtIndex++;
                        //    }
                        //}
                        //else
                        //{
                        //    while (iCtIndex < sUnitNumberList.Length)
                        //    {
                        //        // means we have equal sets of unit number, risk loc and risk sub loc.. 
                        //        sUnitNumber = sUnitNumberList[iCtIndex];
                        //        sRiskLoc = sRiskLocList[iCtIndex];
                        //        sRiskSubLoc = sRiskSubLocList[iCtIndex];
                        //        foreach (XmlNode objNode in objNodeList)
                        //        {
                        //            if (sUnitNumber == objNode.SelectSingleNode("UnitNo").InnerText &&
                        //                   sRiskLoc == objNode.SelectSingleNode("RiskLoc").InnerText &&
                        //                   sRiskSubLoc == objNode.SelectSingleNode("RiskSubLoc").InnerText)
                        //            {
                        //                if (sCommaSeperatdUnitValues == string.Empty)
                        //                    sCommaSeperatdUnitValues = objNode.SelectSingleNode("SequenceNumber").InnerText;
                        //                else
                        //                    sCommaSeperatdUnitValues = sCommaSeperatdUnitValues + "," + objNode.SelectSingleNode("SequenceNumber").InnerText;
                        //                break;
                        //            }
                        //        }
                        //        iCtIndex++;
                        //    }
                        //}
                        #endregion
                        oSaveDownloadOptions.SelectedValue = sCommaSeperatdUnitValues;
                        SaveOptions(ref oSaveDownloadOptions);
                    }
                }
                else
                {
                    Log.Write("UnitList file is empty. No record to save.", oSaveDownloadOptions.ClientId);
                }
                Log.Write("Save Unit end...", oSaveDownloadOptions.ClientId);


                Log.Write("Save unit interest begin..", oSaveDownloadOptions.ClientId);
                sFileData = objPolicySystemAdapter.ReadFileContent("UnitInterestList");
                objDocument = new XmlDocument();
                sSelectedValue = string.Empty;
                if (!string.IsNullOrEmpty(sFileData))
                {
                    objDocument.LoadXml(sFileData);

                    if (objDocument != null)
                    {
                        objNodeList = objDocument.SelectNodes("//SequenceNumber");
                        //oSaveDownloadOptions.Mode = "unitinterest";
                        //TODO begin: Add code for additional insured here
                        //oSaveDownloadOptions.EntityRole = "O";// "Additional Insured Code here";  //oSaveDownloadOptions.AddEntityAs.Split(',')[iCounter];
                        //TODO end:
                        foreach (XmlNode objNode in objNodeList)
                        {
                            //oSaveDownloadOptions.SelectedValue = objNode.InnerText;
                            //SaveOptions(ref oSaveDownloadOptions);

                            sSelectedValue = objNode.InnerText;
                            sAccordRequest = objPolicySystemAdapter.GetPolicyInterestDetailSaveAcordRequest(sSelectedValue, sFileData, oSaveDownloadOptions, ref sUnitRowId, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                //oSaveDownloadOptions.EntityRole = oSaveDownloadOptions.AddEntityAs.Split(',')[iCounter];
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                iInsertedId = objPolicySystemAdapter.GetEntitySaveFormattedResult(sAccordResponse, oSaveDownloadOptions, sSelectedValue, sUnitRowId, ref oSystemErrors);

                            }
                        }
                    }
                }
                else
                {
                    Log.Write("UnitInterestList file is empty. No record to save.", oSaveDownloadOptions.ClientId);
                }

                Log.Write("Save unit interest end...", oSaveDownloadOptions.ClientId);



                Log.Write("Save coverage list begin..", oSaveDownloadOptions.ClientId);
                sFileData = objPolicySystemAdapter.ReadFileContent("CoverageList");
                objDocument = new XmlDocument();
                sSelectedValue = string.Empty;
                if (!string.IsNullOrEmpty(sFileData))
                {
                    objDocument.LoadXml(sFileData);

                    if (objDocument != null)
                    {
                        objNodeList = objDocument.SelectNodes("//SequenceNumber");

                        foreach (XmlNode objNode in objNodeList)
                        {
                            //oSaveDownloadOptions.SelectedValue = objNode.InnerText;
                            //SaveOptions(ref oSaveDownloadOptions);
                            sSelectedValue = objNode.InnerText;
                            objPolicySystemAdapter.SaveCoveragesForWC(oSaveDownloadOptions.PolicyId, sSelectedValue, sFileData, ref oSystemErrors, oSaveDownloadOptions.LossDate);
                        }
                    }
                    bresult = objPolicySystemAdapter.DeleteOldFiles("CoverageList", ref oSystemErrors);
                    if (bresult)
                        bresult = objPolicySystemAdapter.DeleteTempCoverageFiles(ref oSystemErrors, sFileData);
                }
                else
                {
                    Log.Write("CoverageList file is empty. No record to save.", oSaveDownloadOptions.ClientId);
                }

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (oSystemErrors.Count == 0)
            {
                bresult = objPolicySystemAdapter.DeleteOldFiles(oSaveDownloadOptions.Mode, ref oSystemErrors);
                bresult = true;
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bresult;
        }

        public bool SaveOptions(ref SaveDownloadOptions oSaveDownloadOptions)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "SaveOptions";
            //SavedRecords oSavedRecords = null;
            bool bresult = false;
            string[] arrSelectedValues = null;
            string sAccordRequest = string.Empty;
            string sAccordResponse = string.Empty;
            string sFormattedResult = string.Empty;
            int iinsertedId = 0;
            string sUpdatedIds = string.Empty;
            string sLOB = string.Empty;
            int iCounter = 0;
            string sUnitRowId = string.Empty;
            StringBuilder objUnitInterestList = null;
            StringBuilder objCovList = null;
            int iSequenceNumber = 1;
            XElement oAcordResponseXElem = null; //bsharma33 MITS # 34196 Insurance line save for manual rated policy
            XElement oElementInsLnElem = null; //bsharma33 MITS # 34196 Insurance line save for manual rated policy
            string sCovListFileContent = string.Empty;
            XmlDocument xmlDoc = null;
            XElement xeleAccordReq = null;
            XElement xeleAccordRes = null;
            XElement xeleClientSeqNum = null;
            XElement xeleAddressSeqNum = null;
            string sClientSeqNum = string.Empty;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSaveDownloadOptions.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSaveDownloadOptions, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSaveDownloadOptions.ClientId);
                arrSelectedValues = oSaveDownloadOptions.SelectedValue.Split(',');
                oSaveDownloadOptions.PolicySystemId = objPolicySystemAdapter.GetPolicySystemId(oSaveDownloadOptions.PolicyId, ref oSystemErrors);
                objUnitInterestList = new StringBuilder();
                objCovList = new StringBuilder();
                foreach (string sSelectedValue in arrSelectedValues)
                {
                    switch (oSaveDownloadOptions.Mode.ToLower())
                    {
                        case "entity":

                            sAccordRequest = objPolicySystemAdapter.GetPolicyInterestDetailSaveAcordRequest(sSelectedValue, objPolicySystemAdapter.ReadFileContent("EntityList"), oSaveDownloadOptions, ref sUnitRowId, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                oSaveDownloadOptions.EntityRole = oSaveDownloadOptions.AddEntityAs.Split(',')[iCounter];
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                // mits 35925 start
                                xeleAccordReq = XElement.Parse(sAccordRequest);
                                xeleAccordRes = XElement.Parse(sAccordResponse);

                                xeleClientSeqNum = xeleAccordReq.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId");
                                if (xeleClientSeqNum != null)
                                {
                                    xeleAccordRes.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId").Value = xeleClientSeqNum.Value;
                                }
                                xeleAddressSeqNum = xeleAccordReq.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId");
                                if (xeleAddressSeqNum != null)
                                {
                                    xeleAccordRes.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId").Value = xeleAddressSeqNum.Value;
                                }
                                sAccordResponse = xeleAccordRes.ToString();
                                ////mits 35925 end
                                iinsertedId = objPolicySystemAdapter.GetEntitySaveFormattedResult(sAccordResponse, oSaveDownloadOptions, sSelectedValue, string.Empty, ref oSystemErrors);
                                //if (string.IsNullOrEmpty(sUpdatedIds))
                                //    sUpdatedIds = iinsertedId.ToString();
                                //else
                                //    sUpdatedIds = sUpdatedIds + "," + iinsertedId.ToString();
                            }

                            break;
                        case "driver":
                            sAccordRequest = objPolicySystemAdapter.GetPolicyDriverDetailSaveAcordRequest(sSelectedValue, objPolicySystemAdapter.ReadFileContent("DRIVERLIST"), oSaveDownloadOptions, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                iinsertedId = objPolicySystemAdapter.GetDriverDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions, ref oSystemErrors);
                                //if (string.IsNullOrEmpty(sUpdatedIds))
                                //    sUpdatedIds = iinsertedId.ToString();
                                //else
                                //    sUpdatedIds = sUpdatedIds + "," + iinsertedId.ToString();
                            }

                            break;
                        case "unit":
                            string sUnitType = string.Empty;
                            string sAccordCvgListRequest = string.Empty;
                            string sAccordCvgListResponse = string.Empty;
                            //skhare7 Point policy interface unit interest
                            string sAccordUnitInterestListRequest = string.Empty;
                            string sAccordUnitInterestListResponse = string.Empty;
                            string sStatUnitNo = string.Empty;
                            string sUnitNo = string.Empty;
                            string sPolicycomp = string.Empty;
                            string sUnitInterestList = string.Empty;
                            string sCovList = string.Empty;
                            string sUnitDescFmList = string.Empty;
                            //String sAccordCvgDetailRequest = string.Empty;
                            //string sAccordCvgDetailResponse = string.Empty;
                            //XElement oAcordXML = null;
                            //

                            
                            sAccordRequest = objPolicySystemAdapter.GetUnitDetailSaveAcordRequest(sSelectedValue, objPolicySystemAdapter.ReadFileContent("UnitList"), ref oSaveDownloadOptions, ref oSystemErrors, ref sUnitType, ref sUnitDescFmList);

                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                XElement oTemplate = XElement.Parse(sAccordRequest);
                                XElement oStatUnitElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId");
                                if (oStatUnitElement != null)
                                {
                                    sStatUnitNo = oStatUnitElement.Value;
                                    oStatUnitElement = null;

                                }
                                XElement oUnitNoElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                                if (oUnitNoElement != null)
                                {
                                    sUnitNo = oUnitNoElement.Value;
                                    oUnitNoElement = null;

                                }
                                XElement oTempElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                                if (oTempElement != null)
                                {
                                    oSaveDownloadOptions.InsuranceLine = oTempElement.Value;
                                    oTempElement = null;
                                }
                                XElement oLobElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd");
                                if (oLobElement != null)
                                {
                                    sLOB = oLobElement.Value.Trim().ToUpper();
                                    oSaveDownloadOptions.LOBCd = sLOB;
                                }

                                string sIssueCode = objPolicySystemAdapter.GetPolicyIssueCode(oSaveDownloadOptions.PolicyId);
                                if (string.Equals(sIssueCode, "M", StringComparison.InvariantCultureIgnoreCase) || string.Equals(sUnitType, "stat", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                    //Start Changes : bsharma33 MITS # 34196 Insurance line save for manual rated policy
                                    oAcordResponseXElem = XElement.Parse(sAccordResponse);
                                    if (oAcordResponseXElem != null)
                                    {
                                        oElementInsLnElem = oAcordResponseXElem.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                                        if (oElementInsLnElem != null)
                                            oElementInsLnElem.Value = oSaveDownloadOptions.InsuranceLine;

                                        sAccordResponse = oAcordResponseXElem.ToString();
                                    }

                                    oElementInsLnElem = null;
                                    oAcordResponseXElem = null;
                                    //End Changes : bsharma33 MITS # 34196
                                    iinsertedId = objPolicySystemAdapter.GetMRUnitDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors);
                                    if (iinsertedId > 0)
                                    {
                                        sAccordCvgListRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                        sAccordCvgListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgListRequest, ref oSystemErrors);
                                        objPolicySystemAdapter.SaveCoverages(sAccordCvgListResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.LossDate);
                                        //oAcordXML = XElement.Parse(sAccordCvgListResponse);
                                        //foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
                                        //{
                                        //    sAccordCvgDetailRequest = objPolicySystemAdapter.GetCoveragDetailAcordRequest(oSaveDownloadOptions, ref oSystemErrors, objEles.ToString(), "SAVEUNIT");
                                        //    sAccordCvgDetailResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgDetailRequest, ref oSystemErrors);
                                        //    if (!string.IsNullOrEmpty(objEles.ToString()))
                                        //    {
                                        //        objPolicySystemAdapter.GetMRCvgDetailSaveFormattedResult(objEles.ToString(), ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);
                                        //    }
                                        //    sAccordCvgDetailResponse = string.Empty;
                                        //    if (oSystemErrors.Count > 0)
                                        //        break;
                                        //}

                                    }
                                }
                                else
                                {
                                    if (oLobElement != null)
                                    {
                                        //skhare7 Policy interface
                                        string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(sLOB, "POLICY_CLAIM_LOB", ref oSystemErrors);
                                        //skhare7 Policy interface

                                        switch (sPolicyLobRelatedShortCode.Trim().ToUpper())
                                        {
                                            case "AL":
                                                //  case "APV":
                                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, objPolicySystemAdapter.GetPolicySystemId(oSaveDownloadOptions.PolicyId, ref oSystemErrors), sAccordRequest, ref oSystemErrors);
                                                iinsertedId = objPolicySystemAdapter.GetAutoUnitDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors, sUnitDescFmList, sPolicyLobRelatedShortCode.Trim().ToUpper());
                                                if (iinsertedId > 0)
                                                {
                                                    sAccordCvgListRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                                    sAccordCvgListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgListRequest, ref oSystemErrors);
                                                    objPolicySystemAdapter.SaveCoverages(sAccordCvgListResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.LossDate);
                                                    //XElement objTempElement = new XElement("LOBCd");

                                                    //oAcordXML = XElement.Parse(sAccordResponse);
                                                    //if (oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/Policy/LOBCd") != null)
                                                    //    objTempElement.Value = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/Policy/LOBCd").Value;

                                                    //oAcordXML = XElement.Parse(sAccordCvgListResponse);

                                                    //if (oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo") != null)
                                                    //    oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo").AddBeforeSelf(objTempElement);

                                                    //objPolicySystemAdapter.GetALCvgSaveFormattedResult(oAcordXML.ToString(), ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);
                                                    //if (oSystemErrors.Count > 0)
                                                    //    break;
                                                }
                                                break;
                                            case "CL":
                                                //   case "CPP":
                                                //XElement oTempDoc = XElement.Parse(sAccordRequest);
                                                //oTempElement = oTempDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                                                //if (oTempElement != null)
                                                //{
                                                //    oSaveDownloadOptions.InsuranceLine = GetCPPInsuranceLine(objPolicySystemAdapter, oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT");
                                                //    oTempElement.Value = oSaveDownloadOptions.InsuranceLine;
                                                //    oTempElement = null;
                                                //}
                                                //sAccordRequest = oTempDoc.ToString();
                                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                                if (sUnitType.ToUpper() == "VEHICLE")
                                                    iinsertedId = objPolicySystemAdapter.GetAutoUnitDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors, sUnitDescFmList, sPolicyLobRelatedShortCode.Trim().ToUpper());
                                                if (sUnitType.ToUpper() == "PROPERTY")
                                                    iinsertedId = objPolicySystemAdapter.GetPropertyDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors, sUnitDescFmList);
                                                if (iinsertedId > 0)
                                                {
                                                    //oSaveDownloadOptions.InsuranceLine = GetCPPInsuranceLine(objPolicySystemAdapter, oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT");
                                                    sAccordCvgListRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                                    sAccordCvgListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgListRequest, ref oSystemErrors);
                                                    objPolicySystemAdapter.SaveCoverages(sAccordCvgListResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.LossDate);
                                                    //oAcordXML = XElement.Parse(sAccordCvgListResponse);
                                                    //foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
                                                    //{
                                                    //    sAccordCvgDetailRequest = objPolicySystemAdapter.GetCoveragDetailAcordRequest(oSaveDownloadOptions, ref oSystemErrors, objEles.ToString(), "SAVEUNIT");
                                                    //    sAccordCvgDetailResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgDetailRequest, ref oSystemErrors);
                                                    //    if (!string.IsNullOrEmpty(sAccordCvgDetailResponse))
                                                    //    {
                                                    //        objPolicySystemAdapter.GetCLCvgSaveFormattedResult(sAccordCvgDetailResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);
                                                    //    }
                                                    //    sAccordCvgDetailResponse = string.Empty;
                                                    //    if (oSystemErrors.Count > 0)
                                                    //        break;
                                                    //}

                                                }
                                                break;

                                            case "PL":
                                                //  case "HP":
                                                //  case "MHO":
                                                // case "FP":
                                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                                iinsertedId = objPolicySystemAdapter.GetPropertyDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors, sUnitDescFmList);
                                                if (iinsertedId > 0)
                                                {
                                                    //objPolicySystemAdapter.GetPLCvgSaveFormattedResult(sAccordResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);

                                                    sAccordCvgListRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                                    sAccordCvgListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgListRequest, ref oSystemErrors);
                                                    objPolicySystemAdapter.SaveCoverages(sAccordCvgListResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.LossDate);
                                                    //oAcordXML = XElement.Parse(sAccordCvgListResponse);
                                                    //foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
                                                    //{
                                                    //    sAccordCvgDetailRequest = objPolicySystemAdapter.GetCoveragDetailAcordRequest(oSaveDownloadOptions, ref oSystemErrors, objEles.ToString(), "SAVEUNIT");
                                                    //    sAccordCvgDetailResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgDetailRequest, ref oSystemErrors);
                                                    //    if (!string.IsNullOrEmpty(sAccordCvgDetailResponse))
                                                    //    {
                                                    //        objPolicySystemAdapter.GetPLOptionalCvgSaveFormattedResult(sAccordCvgDetailResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);
                                                    //    }
                                                    //    sAccordCvgDetailResponse = string.Empty;
                                                    //    if (oSystemErrors.Count > 0)
                                                    //        break;
                                                    //}
                                                }
                                                //break;

                                                //sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, objPolicySystemAdapter.GetPolicySystemId(oSaveDownloadOptions.PolicyId, ref oSystemErrors), sAccordRequest, ref oSystemErrors);
                                                //iinsertedId = objPolicySystemAdapter.GetPropertyDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions.PolicyId, ref oSystemErrors);
                                                //objPolicySystemAdapter.GetFPCvgSaveFormattedResult();
                                                break;
                                            case "WL":
                                                // case "WCA":
                                                //  case "WCV":
                                                string sRateState = string.Empty;
                                                string sUnitdesc = string.Empty;
                                                string sAcordFileNm = string.Empty;
                                                XElement xTemp, objCovListTemp;
                                                IEnumerable<XElement> oElements = null;
                                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                                //dbisht6 start for mits 35588 

                                                oAcordResponseXElem = XElement.Parse(sAccordResponse);
                                                if (oAcordResponseXElem != null)
                                                {
                                                    oElementInsLnElem = oAcordResponseXElem.XPathSelectElement(" ./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");

                                                    if (oElementInsLnElem != null)
                                                        oElementInsLnElem.Value = oSaveDownloadOptions.InsuranceLine;

                                                    sAccordResponse = oAcordResponseXElem.ToString();
                                                }

                                                oElementInsLnElem = null;
                                                oAcordResponseXElem = null;
                                                //dbisht6

                                                iinsertedId = objPolicySystemAdapter.GetSiteUnitDetailSaveFormattedResult(sAccordResponse, oSaveDownloadOptions, ref oSystemErrors);
                                                if (iinsertedId > 0)
                                                {
                                                    xTemp = XElement.Parse(sAccordResponse);
                                                    sRateState = xTemp.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/Addr/StateProvCd").Value;
                                                    oSaveDownloadOptions.UnitState = sRateState;
                                                    sUnitdesc = xTemp.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/LocationName").Value;
                                                    sAccordCvgListRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                                    sAccordCvgListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgListRequest, ref oSystemErrors);
                                                    if (!string.IsNullOrEmpty(sAccordCvgListResponse))
                                                    {
                                                        objCovListTemp = XElement.Parse(sAccordCvgListResponse);
                                                        oElements = objCovListTemp.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
                                                        if ((oElements != null) && (oElements.Count() > 0))
                                                        {
                                                            // move this to temp xml file and use it later to show coverage list on UI

                                                            sAcordFileNm = objPolicySystemAdapter.GetAcordWLCoverageList(sAccordCvgListResponse, ref oSystemErrors, sStatUnitNo);
                                                            sCovList = objPolicySystemAdapter.GetWLCoverageList(sAccordCvgListResponse, sStatUnitNo, sUnitNo, iinsertedId, ref oSystemErrors, oSaveDownloadOptions.PolicySystemId, ref iSequenceNumber, sAcordFileNm, sUnitdesc);

                                                            objCovList.Append(sCovList);
                                                        }
                                                        //objPolicySystemAdapter.SaveCoverages(sAccordCvgListResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);

                                                        //end


                                                        //oAcordXML = XElement.Parse(sAccordCvgListResponse);
                                                        //foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
                                                        //{
                                                        //    sAccordCvgDetailRequest = objPolicySystemAdapter.GetCoveragDetailAcordRequest(oSaveDownloadOptions, ref oSystemErrors, objEles.ToString(), "SAVEUNIT");
                                                        //    sAccordCvgDetailResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordCvgDetailRequest, ref oSystemErrors);
                                                        //    if (!string.IsNullOrEmpty(sAccordCvgDetailResponse))
                                                        //    {
                                                        //        objPolicySystemAdapter.GetWLCvgSaveFormattedResult(sAccordCvgDetailResponse, ref oSystemErrors, iinsertedId, oSaveDownloadOptions.PolicyId);
                                                        //    }
                                                        //    sAccordCvgDetailResponse = string.Empty;
                                                        //    if (oSystemErrors.Count > 0)
                                                        //        break;
                                                        //}
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }

                                // iinsertedId = objPolicySystemAdapter.GetUnitDetailSaveFormattedResult(sAccordResponse, ref oSystemErrors);
                                //if (string.IsNullOrEmpty(sUpdatedIds))
                                //    sUpdatedIds = iinsertedId.ToString() + "@" + sUniType.Replace("\"","");
                                //else
                                //    sUpdatedIds = sUpdatedIds + "," + iinsertedId.ToString() + "@" + sUniType.Replace("\"", "");



                                SaveEndorsementData(oSaveDownloadOptions, "SAVEUNIT", oSaveDownloadOptions.PolicyId, iinsertedId);
                                if (RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", string.Empty, 0)["SkipUnitInterestDownload"] != null && RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", string.Empty, 0)["SkipUnitInterestDownload"] != "Y")
                                {
                                    sAccordUnitInterestListRequest = objPolicySystemAdapter.GetUnitInterestListAcordRequest(oSaveDownloadOptions, ref oSystemErrors, "SAVEUNIT", sAccordRequest);
                                    sAccordUnitInterestListResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordUnitInterestListRequest, ref oSystemErrors);

                                    sUnitInterestList = objPolicySystemAdapter.GetUnitInterestListFormattedResult(sAccordUnitInterestListResponse, sStatUnitNo, sUnitNo, iinsertedId, ref oSystemErrors, oSaveDownloadOptions.PolicySystemId, ref iSequenceNumber);
                                    objUnitInterestList.Append(sUnitInterestList);
                                }

                            }
                            break;
                        case "unitinterest":

                            sAccordRequest = objPolicySystemAdapter.GetPolicyInterestDetailSaveAcordRequest(sSelectedValue, objPolicySystemAdapter.ReadFileContent("UnitInterestList"), oSaveDownloadOptions, ref sUnitRowId, ref oSystemErrors);
                            if (!string.IsNullOrEmpty(sAccordRequest))
                            {
                                oSaveDownloadOptions.EntityRole = oSaveDownloadOptions.AddEntityAs.Split(',')[iCounter];
                                sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveDownloadOptions.PolicySystemId, sAccordRequest, ref oSystemErrors);
                                // mits 35925 start
                                xeleAccordReq = XElement.Parse(sAccordRequest);
                                xeleAccordRes = XElement.Parse(sAccordResponse);

                                xeleClientSeqNum = xeleAccordReq.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId");
                                if (xeleClientSeqNum != null)
                                {
                                    xeleAccordRes.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId").Value = xeleClientSeqNum.Value;
                                }
                                xeleAddressSeqNum = xeleAccordReq.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId");
                                if (xeleAddressSeqNum != null)
                                {
                                    xeleAccordRes.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId").Value = xeleAddressSeqNum.Value;
                                }
                                sAccordResponse = xeleAccordRes.ToString();
                                //mits 35925 end

                                iinsertedId = objPolicySystemAdapter.GetEntitySaveFormattedResult(sAccordResponse, oSaveDownloadOptions, sSelectedValue, sUnitRowId, ref oSystemErrors);

                            }

                            break;
                        case "coveragelist":
                            // write code to save coverages
                            sCovListFileContent = objPolicySystemAdapter.ReadFileContent("CoverageList");
                            objPolicySystemAdapter.SaveCoveragesForWC(oSaveDownloadOptions.PolicyId, sSelectedValue, sCovListFileContent, ref oSystemErrors, oSaveDownloadOptions.LossDate);

                            break;
                        //case "property":
                        //    //sAccordRequest = objPolicySystemAdapter.GetPropertyDetailAcordRequest(sSelectedValue, ref oSystemErrors);
                        //    sAccordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, objPolicySystemAdapter.GetPolicySystemId(oSaveDownloadOptions.PolicyId, ref oSystemErrors), sAccordRequest, ref oSystemErrors);
                        //    break;
                    }
                    // oSaveDownloadOptions.UpdatedIds 
                    if (oSystemErrors.Count > 0)
                        break;

                    iCounter++;
                }

                if ((objUnitInterestList != null) && !string.IsNullOrEmpty(objUnitInterestList.ToString()))
                {
                    objPolicySystemAdapter.WriteUnitInterestListToFile(objUnitInterestList.ToString(), ref oSystemErrors);
                    objUnitInterestList = null;
                }
                if ((objCovList != null) && !string.IsNullOrEmpty(objCovList.ToString()))
                {
                    objPolicySystemAdapter.WriteCovListToFile(objCovList.ToString(), ref oSystemErrors);
                    objCovList = null;
                }
                //objPolicySystemAdapter.SaveOptions(ref oSaveDownloadOptions,ref oSystemErrors);

                if (oSystemErrors.Count == 0)
                {
                    bresult = objPolicySystemAdapter.DeleteOldFiles(oSaveDownloadOptions.Mode, ref oSystemErrors);

                    if (oSaveDownloadOptions.Mode.ToLower() == "coveragelist")
                    {
                        bresult = objPolicySystemAdapter.DeleteTempCoverageFiles(ref oSystemErrors, sCovListFileContent);
                        // call method to delete coverages acord files.., pass the file names as parameter to it..
                    }
                    bresult = true;
                    // oSaveDownloadOptions.UpdatedIds = sUpdatedIds;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bresult;
        }
        
        public void DeleteOldFiles(DisplayMode oMode)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "DeleteOldFiles";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oMode.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oMode, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oMode.ClientId);
                bresult = objPolicySystemAdapter.DeleteOldFiles(oMode.Mode, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }


        }

        public void GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse)
        {
            XmlDocument xmlRequest = null;
            XElement oElement = null;
            XElement oNodeElement = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyInquireAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oEnquiryResponse = null;
            //tanwar2 - WWIG - Staging - start
            
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oEnquiryRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oEnquiryRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oEnquiryRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetPolicyInquireAcordRequest(oEnquiryRequest, ref oSystemErrors);
                //skhare7 Policy interface

                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                     string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oEnquiryRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);

                        oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                        if (oEnquiryResponse == null)
                            oEnquiryResponse = new PolicyEnquiry();
                        oEnquiryResponse.PolicyDataAcordXML = oAcordResponse;

                        oElement = XElement.Parse(oAcordResponse);
                        oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                        if (oNodeElement != null)
                            oEnquiryRequest.IssueCode = oNodeElement.Value;

                        oAcordRequest = objPolicySystemAdapter.GetUnitListAcordRequest(oEnquiryRequest, ref oSystemErrors, "ENQUIRY");
                        if (!string.IsNullOrEmpty(oAcordRequest))
                        {
                            if (!string.IsNullOrEmpty(sPolicyLobRelatedShortCode))
                            {
                                oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                                oEnquiryResponse.UnitListAcordXML = oAcordResponse;
                                oEnquiryResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy Interface
                                oEnquiryRequest.BaseLOBLine = sPolicyLobRelatedShortCode; //MITS 30142; based on base line LOB, IsDriverRequest flag will be checked in Model.PolicyInterface 
                            }
                        }
                        if (oEnquiryRequest.IsDriverRequest)
                        {
                            oAcordRequest = objPolicySystemAdapter.GetPolicyDriverListAcordRequest(oEnquiryRequest, ref oSystemErrors, "ENQUIRY");
                            if (!string.IsNullOrEmpty(oAcordRequest))
                            {
                                oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                                oEnquiryResponse.DriverListAcordXML = oAcordResponse;
                            }
                        }
                        oAcordRequest = objPolicySystemAdapter.GetPolicyInterestListAcordRequest(oEnquiryRequest, ref oSystemErrors, "ENQUIRY");
                        if (!string.IsNullOrEmpty(oAcordRequest))
                        {
                            oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                            oEnquiryResponse.PolicyInterestListAcordXML = oAcordResponse;
                            bresult = true;
                        }
                    

                    oElement = null;
                    oNodeElement = null;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetUnitDetailResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetUnitDetailAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            XElement oNodeElement = null;
            XElement oElement = null;
            oResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                
                    oAcordRequest = objPolicySystemAdapter.GetUnitDetailAcordRequest(oRequest, ref oSystemErrors);
                    string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                    if (!string.IsNullOrEmpty(oAcordRequest))
                    {
                        //if (sPolicyLobRelatedShortCode == "CL")
                        //{
                        //    oElement = XElement.Parse(oAcordRequest);
                        //    oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        //    if (oNodeElement != null)
                        //    {
                        //        oNodeElement.Value = GetCPPInsuranceLine(objPolicySystemAdapter, oRequest, ref oSystemErrors, "POLICYENQUIRY");
                        //        oAcordRequest = oElement.ToString();
                        //    }
                        //}
                        oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                        if (oResponse == null)
                            oResponse = new PolicyEnquiry();
                        oResponse.ResponseAcordXML = oAcordResponse;
                        oResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy interface
                        bresult = true;
                    }
                
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicyInterestDetailResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyInterestDetailAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetPolicyInterestDetailAcordRequest(oRequest, ref oSystemErrors);

                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                     oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    if (oResponse == null)
                        oResponse = new PolicyEnquiry();
                    oResponse.ResponseAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicyDriverDetailResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyDriverListAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetPolicyDriverDetailAcordRequest(oRequest, ref oSystemErrors);

                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                        oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    
                    if (oResponse == null)
                        oResponse = new PolicyEnquiry();
                    oResponse.ResponseAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        //public void GetPropertyUnitDetailResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse)
        //{
        //    XmlDocument xmlRequest = null;
        //    UserLogin oUserLogin = null;
        //    PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
        //    BusinessAdaptorErrors oSystemErrors = null;
        //    string sFunctionName = "GetPropertyUnitDetailAcordResponse";
        //    bool bresult = false;
        //    string oAcordPropertyRequest = string.Empty;
        //    string oAcordPropertyResponse = string.Empty;
        //    oEnquiryResponse = null;
        //    try
        //    {
        //        oSystemErrors = new BusinessAdaptorErrors();
        //        objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
        //        //This is a common function which intialises/perform the common tasks on each service call
        //        InitiateServiceProcess(oEnquiryRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
        //    }
        //    catch (Exception e)
        //    {
        //        // Throw back error if there is ne error in intialization process.
        //        oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
        //        logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
        //        RMException theFault = new RMException();
        //        theFault.Errors = formatOutputXML(null, false, oSystemErrors);
        //        throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
        //    }
        //    try
        //    {
        //        oSystemErrors = new BusinessAdaptorErrors(oUserLogin);
        //        oAcordPropertyRequest = objPolicySystemAdapter.GetPropertyUnitDetailAcordRequest(oEnquiryRequest, ref oSystemErrors);

        //        if (!string.IsNullOrEmpty(oAcordPropertyRequest))
        //        {
        //            //To Do: Call to CFW/Policy Systems
        //            //Temporary we are getting static response from local methods
        //            oAcordPropertyResponse = objPolicySystemAdapter.GetPropertyUnitDetailAcordResponse(oAcordPropertyRequest, ref oSystemErrors);
        //            //To Do: End
        //            if (!string.IsNullOrEmpty(oAcordPropertyResponse))
        //                bresult = objPolicySystemAdapter.GetPropertyUnitDetailFormattedResult(oAcordPropertyResponse, out oEnquiryResponse, ref oSystemErrors);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        // Throw back error if there is ne error in intialization process.
        //        oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
        //        logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
        //        RMException theFault = new RMException();
        //        theFault.Errors = formatOutputXML(null, false, oSystemErrors);
        //        throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
        //    }

        //    logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

        //    if (oSystemErrors.Count > 0)
        //    {
        //        RMException theFault = new RMException();
        //        theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
        //        throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
        //    }
        //}

        public void GetUnitCoverageListResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetCoverageListAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            XElement oElement, oNodeElement;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetCoverageListAcordRequest(oRequest, ref oSystemErrors, "POLICYENQUIRY", string.Empty);

                 string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                    if (!string.IsNullOrEmpty(oAcordRequest))
                    {
                        //if (oRequest.LOB == "CPP" || oRequest.LOB=="BOP")
                        if (!string.IsNullOrEmpty(sPolicyLobRelatedShortCode))
                        {
                            //if (sPolicyLobRelatedShortCode == "CL")
                            //{
                            //    oElement = XElement.Parse(oAcordRequest);
                            //    oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                            //    if (oNodeElement != null)
                            //    {
                            //        oNodeElement.Value = GetCPPInsuranceLine(objPolicySystemAdapter, oRequest, ref oSystemErrors, "POLICYENQUIRY");
                            //        oAcordRequest = oElement.ToString();
                            //    }
                            //}
                            oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                            if (oResponse == null)
                                oResponse = new PolicyEnquiry();
                            oResponse.ResponseAcordXML = oAcordResponse;
                            oResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy interface
                            objPolicySystemAdapter.SaveCoveragesXMLToFile(oAcordResponse, ref oSystemErrors);
                            bresult = true;
                            //bresult = objPolicySystemAdapter.GetAutoCoverageListFormattedResult(oAcordResponse, out oResponse, ref oSystemErrors);
                        }
                    }
                
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetCoverageDetailResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;

            string sPolicyLobRelatedShortCode = string.Empty;
            
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "ReadFileContent";
            bool bresult = false;
            //string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            //XElement oElement, oNodeElement;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);

                   //oAcordRequest = objPolicySystemAdapter.GetCoveragDetailAcordRequest(oRequest, ref oSystemErrors, "", "POLICYENQUIRY");

                    //tanwar2: Moved string declaration above
                    //string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                    sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                    //tanwar2 - Policy Download from staging - end
                    //if (!string.IsNullOrEmpty(oAcordRequest))
                    //{
                    //    if (!string.IsNullOrEmpty(sPolicyLobRelatedShortCode))
                    //    {
                    //        if (sPolicyLobRelatedShortCode == "CL")
                    //        {
                    //            oElement = XElement.Parse(oAcordRequest);
                    //            oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                    //            if (oNodeElement != null)
                    //            {
                    //                oNodeElement.Value = GetCPPInsuranceLine(objPolicySystemAdapter, oRequest, ref oSystemErrors, "POLICYENQUIRY");
                    //                oAcordRequest = oElement.ToString();
                    //                oResponse = new PolicyEnquiry();
                    //                oResponse.InsLine = oNodeElement.Value;
                    //            }
                    //        }
                    //        oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    //        if (oResponse == null)
                    //            oResponse = new PolicyEnquiry();
                    //        oResponse.ResponseAcordXML = oAcordResponse;
                    //        oResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy interface
                    //        bresult = true;
                    //    }
                    //}
                    oAcordResponse = objPolicySystemAdapter.GetCoverageXMLFromFile(oRequest.CovCode, oRequest.CovSeqNo, oRequest.TransSeq);
                
                if (oResponse == null)
                    oResponse = new PolicyEnquiry();
                oResponse.ResponseAcordXML = oAcordResponse;
                oResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy interface
                bresult = true;
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetEndorsementData(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyEndorsementDataAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            XElement oElement, oNodeElement;
            string sPolicyLobRelatedShortCode = string.Empty;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                //if- else added by swati
                    sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                    oRequest.BaseLOBLine = sPolicyLobRelatedShortCode;
                    oAcordRequest = objPolicySystemAdapter.GetPolicyEndorsementDataAcordRequest(oRequest, ref oSystemErrors, "ENQUIRY");
                    if (!string.IsNullOrEmpty(oAcordRequest))
                    {
                        if (!string.IsNullOrEmpty(sPolicyLobRelatedShortCode))
                        {
                            if (!string.Equals(oRequest.InsLine, "*AL"))
                            {
                                //if (sPolicyLobRelatedShortCode == "CL")
                                //{
                                //    oElement = XElement.Parse(oAcordRequest);
                                //    oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                                //    if (oNodeElement != null)
                                //    {
                                //        oNodeElement.Value = GetCPPInsuranceLine(objPolicySystemAdapter, oRequest, ref oSystemErrors, "POLICYENQUIRY");
                                //        oAcordRequest = oElement.ToString();
                                //    }
                                //}
                            }
                            oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                        }
                    }
                
                if (oResponse == null)
                    oResponse = new PolicyEnquiry();
                oResponse.ResponseAcordXML = oAcordResponse;
                oResponse.BaseLOBLine = sPolicyLobRelatedShortCode;//skhare7 Policy interface
                bresult = true;
            }
            catch (Exception e)
            {
                // Throw back error if there is any error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void SaveEndorsementData(Object oRequest, string sMode, int iPolicyId, int iRecordRowId)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "SavePSEndorsementData";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            PolicySaveRequest oPolicySaveRequest = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            string sTableName = string.Empty;
            int iPolicysystemId = 0;
            //added by swati for staging endorsement data
            //PolicyStagingInterfaceAdaptor objPolicyStgngAdaptor = null;
            try
            {
                //              oSystemErrors = new BusinessAdaptorErrors(oPolicySaveRequest.ClientId);//cloud changes on behalf of ankur
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                switch (sMode.ToUpper())
                {
                    case "SAVEPOLICY":
                        oPolicySaveRequest = (PolicySaveRequest)oRequest;
                        oSystemErrors = new BusinessAdaptorErrors(oPolicySaveRequest.ClientId);//cloud changes on behalf of ankur
                        sTableName = "POLICY";
                        InitiateServiceProcess(oPolicySaveRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
                        iPolicysystemId = oPolicySaveRequest.PolicySystemId;
                        break;
                    case "SAVEUNIT":
                        oSaveDownloadOptions = (SaveDownloadOptions)oRequest;
                        oSystemErrors = new BusinessAdaptorErrors(oSaveDownloadOptions.ClientId);//cloud changes on behalf of ankur
                        sTableName = "POLICY_X_UNIT";
                        InitiateServiceProcess(oSaveDownloadOptions, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
                        iPolicysystemId = oSaveDownloadOptions.PolicySystemId;

                        break;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oPolicySaveRequest.ClientId);
                //rma - 
                if (oSystemErrors == null)
                    oSystemErrors = new BusinessAdaptorErrors(oUserLogin, ((PolicyEnquiry)oRequest).ClientId);

                oAcordRequest = objPolicySystemAdapter.GetPolicyEndorsementDataAcordRequest(oRequest, ref oSystemErrors, sMode.ToUpper());

                oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, iPolicysystemId, oAcordRequest, ref oSystemErrors);
                if (!string.IsNullOrEmpty(oAcordResponse))
                {

                    objPolicySystemAdapter.SavePSEndorsementData(oAcordResponse, iPolicyId, sTableName, iRecordRowId, ref oSystemErrors);
                    //if (oResponse == null)
                    //    oResponse = new PolicyEnquiry();
                    //oResponse.ResponseAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is any error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                oPolicySaveRequest = null;
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        
        public void SaveExternalPolicy(PolicySaveRequest oSaveRequest, out PolicySaveRequest oSaveResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicySaveAcordResponse";
            bool bResult = false;
            string oAcordSavePolicyRequest = string.Empty;
            string oAcordSaveAutoRequest = string.Empty;
            string oAcordSavePropertyRequest = string.Empty;
            string oAcordSaveResponse = string.Empty;
            string oAcordSaveDriverRequest = string.Empty;
            string oAcordSaveDriverResponse = string.Empty;
            string oAcordSaveInterestRequest = string.Empty;
            string oAcordSaveInterestResponse = string.Empty;
            string sAccordUnitRequest = null;
            string sAccordUnitResponse = null;
            //Start:MITS 31601, Neha Suresh Jain,02/27/2013
            string oAcordSavePolicyChangeDateRequest = string.Empty;
            string oAcordSavePolicyChangeDateResponse = string.Empty;
            oSaveResponse = null;
            XElement oElement = null;
            XElement oNodeElement = null;
            int iPolicyId = 0;
            string sCovListFile = string.Empty;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSaveRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSaveRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSaveRequest.ClientId);
                oAcordSavePolicyRequest = objPolicySystemAdapter.GetPolicySaveAcordRequest(oSaveRequest, ref oSystemErrors);

                if (!string.IsNullOrEmpty(oAcordSavePolicyRequest))
                {
                    oAcordSaveResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveRequest.PolicySystemId, oAcordSavePolicyRequest, ref oSystemErrors);
                    //if (oAcordSaveResponse == null)
                    //    oSaveResponse = new PolicySaveRequest();
                    //oSaveResponse.PolicyDataAcordXML = oAcordResponse;
                }

                //To Do: End
                if (!string.IsNullOrEmpty(oAcordSaveResponse))
                {
                    oSaveResponse = new PolicySaveRequest();
                    objPolicySystemAdapter.AddAditionalData(oSaveRequest, oSaveResponse, ref oAcordSaveResponse);
                    bResult = objPolicySystemAdapter.GetPolicySaveFormattedResult(oSaveRequest.ClaimId, oAcordSaveResponse, ref oSaveResponse, ref oSystemErrors, oSaveRequest.PolicySystemId.ToString());
                    // to do - entity write file code
                    if (bResult)
                    {
                        iPolicyId = oSaveResponse.AddedPolicyId;
                        oSaveRequest.BaseLOBLine = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oSaveRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                        //Start: MITS 31601: Neha Suresh Jain,02/27/2013, save policy change effective date,enterdate, reson amended in policy_x_amendHist table
                        if (iPolicyId > 0)
                        {
                            oAcordSavePolicyChangeDateRequest = objPolicySystemAdapter.GetPolicyChangeDateListAcordRequest(oSaveRequest, ref oSystemErrors, "SAVEPOLICY");
                            if (!string.IsNullOrEmpty(oAcordSavePolicyChangeDateRequest))
                            {
                                oAcordSavePolicyChangeDateResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveRequest.PolicySystemId, oAcordSavePolicyChangeDateRequest, ref oSystemErrors);
                                if (!string.IsNullOrEmpty(oAcordSavePolicyChangeDateResponse))
                                {
                                    objPolicySystemAdapter.GetPolicyChangeDateListSaveFormattedResult(iPolicyId, oAcordSavePolicyChangeDateResponse, ref oSystemErrors);
                                }
                            }
                        }
                        //End: MITS 31601
                        SaveEndorsementData(oSaveRequest, "SAVEPOLICY", iPolicyId, iPolicyId);
                        if (oSaveRequest.IsDriverRequest)
                        {
                            oAcordSaveDriverRequest = objPolicySystemAdapter.GetPolicyDriverListAcordRequest(oSaveRequest, ref oSystemErrors, "SAVEPOLICY");
                            if (!string.IsNullOrEmpty(oAcordSaveDriverRequest))
                            {
                                oAcordSaveDriverResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveRequest.PolicySystemId, oAcordSaveDriverRequest, ref oSystemErrors);
                                //oEnquiryResponse.DriverListAcordXML = oAcordSaveDriverResponse;
                                objPolicySystemAdapter.GetDriverListFormattedResult(oAcordSaveDriverResponse, ref oSystemErrors);
                            }
                        }
                        if (RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", string.Empty, 0)["SkipPolicyInterestDownload"] != null && RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", string.Empty, 0)["SkipPolicyInterestDownload"] != "Y")
                        {
                            oAcordSaveInterestRequest = objPolicySystemAdapter.GetPolicyInterestListAcordRequest(oSaveRequest, ref oSystemErrors, "SAVEPOLICY");
                            if (!string.IsNullOrEmpty(oAcordSaveInterestRequest))
                            {
                                oAcordSaveInterestResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveRequest.PolicySystemId, oAcordSaveInterestRequest, ref oSystemErrors);
                                objPolicySystemAdapter.GetInterestListFormattedResult(oAcordSaveInterestResponse, ref oSystemErrors, oSaveRequest.PolicySystemId);
                                //oEnquiryResponse.PolicyInterestListAcordXML = oAcordSaveInterestResponse;
                                //bresult = true;
                            }
                        }
                        oElement = XElement.Parse(oAcordSaveResponse);
                        oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                        if (oNodeElement != null)
                            oSaveRequest.IssueCode = oNodeElement.Value;

                        sAccordUnitRequest = objPolicySystemAdapter.GetUnitListAcordRequest(oSaveRequest, ref oSystemErrors, "SAVEPOLICY");
                        sAccordUnitResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oSaveRequest.PolicySystemId, sAccordUnitRequest, ref oSystemErrors);
                        if (string.Equals(oSaveRequest.IssueCode, "M", StringComparison.InvariantCultureIgnoreCase))
                        {
                            objPolicySystemAdapter.GetMRUnitListFormattedResult(sAccordUnitResponse, ref oSystemErrors);
                        }
                        else
                        {
                            //skhare7 Policy interface
                            string sPolicyLobRelatedShortCode = objPolicySystemAdapter.GetPolicyLOBRelatedShortCode(oSaveRequest.LOB.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref oSystemErrors);
                            switch (sPolicyLobRelatedShortCode.Trim().ToUpper())
                            {
                                case "AL":
                                    // case "APV":
                                    objPolicySystemAdapter.GetUnitListFormattedResult(sAccordUnitResponse, sPolicyLobRelatedShortCode.Trim().ToUpper(), ref oSystemErrors);
                                    break;
                                case "CL":
                                    objPolicySystemAdapter.GetUnitListFormattedResult(sAccordUnitResponse, sPolicyLobRelatedShortCode.Trim().ToUpper(), ref oSystemErrors);

                                    break;

                                case "PL":
                                    // case "MHO":
                                    // case "FP":
                                    objPolicySystemAdapter.GetUnitListFormattedResult(sAccordUnitResponse, sPolicyLobRelatedShortCode.Trim().ToUpper(), ref oSystemErrors);
                                    break;

                                case "WL":
                                    //case "WCA":
                                    // case "WCV":
                                    objPolicySystemAdapter.GetSiteUnitListFormattedResult(sAccordUnitResponse, ref oSystemErrors);
                                    break;

                            }
                        }

                        // delete start-any previous temp files for unit interest list and coverage list
                        objPolicySystemAdapter.DeleteOldFiles("unitinterest", ref oSystemErrors);
                        sCovListFile = objPolicySystemAdapter.ReadFileContent("CoverageList");

                        objPolicySystemAdapter.DeleteOldFiles("coveragelist", ref oSystemErrors);
                        if (!string.IsNullOrEmpty(sCovListFile.Trim()))
                            objPolicySystemAdapter.DeleteTempCoverageFiles(ref oSystemErrors, sCovListFile);
                        // delete end

                        //if (string.Equals(oSaveRequest.ClaimFormName, "claimwc", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //    bResult = objPolicySystemAdapter.SaveWorkLossUnit(iPolicyId, ref oSystemErrors);
                        //    if (bResult)
                        //        bResult = objPolicySystemAdapter.GetEntitySaveFormattedResult(oAcordSaveResponse, out oSaveResponse, ref oSystemErrors);
                        //}
                        //else
                        //{

                        //bResult = objPolicySystemAdapter.GetEntitySaveFormattedResult(oAcordSaveResponse, out oSaveResponse, ref oSystemErrors);
                        //if (bResult)
                        //{
                        //    oAcordSaveAutoRequest = objPolicySystemAdapter.GetAutoUnitSaveAcordRequest(oSaveRequest, ref oSystemErrors);
                        //    if (!string.IsNullOrEmpty(oAcordSaveAutoRequest))
                        //    {
                        //        oAcordSaveResponse = objPolicySystemAdapter.GetUnitSaveAcordResponse(oAcordSavePolicyRequest, ref oSystemErrors);
                        //        //To Do: End
                        //        if (!string.IsNullOrEmpty(oAcordSaveResponse))
                        //        {
                        //            bResult = objPolicySystemAdapter.GetAutoUnitSaveFormattedResult(oAcordSaveResponse, out oSaveResponse, ref oSystemErrors);
                        //        }
                        //    }
                        //    oAcordSavePropertyRequest = objPolicySystemAdapter.GetPropertyUnitSaveAcordRequest(oSaveRequest, ref oSystemErrors);
                        //    if (!string.IsNullOrEmpty(oAcordSavePropertyRequest))
                        //    {
                        //        //To Do: Call to CFW/Policy Systems
                        //        //Temporary we are getting static response from local methods
                        //        oAcordSaveResponse = objPolicySystemAdapter.GetPropertySaveAcordResponse(oAcordSavePolicyRequest, ref oSystemErrors);
                        //        //To Do: End
                        //        if (!string.IsNullOrEmpty(oAcordSaveResponse))
                        //        {
                        //            bResult = bResult && objPolicySystemAdapter.GetPropertySaveFormattedResult(oAcordSaveResponse, out oSaveResponse, ref oSystemErrors);
                        //        }
                        //    }
                        //}
                        //}
                        oSaveResponse.AddedPolicyId = iPolicyId;
                    }
                }
            }

            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bResult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        
        public void PolicyValidation(PolicyValidateInput oPolicyRequest, out PolicyValidateResponse oPolicyResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            ValidatePolicyAdaptor objValidatePolicyAdaptor = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "PolicyValidation";
            bool bresult = false;
            string oAcordSavePolicyRequest = string.Empty;
            string oAcordSaveAutoRequest = string.Empty;
            string oAcordSavePropertyRequest = string.Empty;
            string oAcordSaveResponse = string.Empty;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oPolicyRequest.ClientId);
                objValidatePolicyAdaptor = new ValidatePolicyAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oPolicyRequest, out xmlRequest, sFunctionName, objValidatePolicyAdaptor, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oPolicyRequest.ClientId);
                bresult = objValidatePolicyAdaptor.PolicyValidation(oPolicyRequest, out oPolicyResponse, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

        //tanwar2 - mits 30910 - start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="oRequest"></param>
        public XElement GetDiminishingEvaluationDate(XElement xDoc, PolicyEnquiry oRequest)
        {
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            DeductibleManagementAdaptor oDedManagerAdaptor = null;
            //tanwar2 - JIRA RMA 4824 - Code Review (1a) - start
            //DataTable dtPolicySystemInfo = null;
            //tanwar2 - JIRA RMA 4824 - Code Review (1a) - end
            UserLogin oUserLogin = null;

            bool bDateSet = false;
            bool bIsCvgEvalDateAvailable = false;
            string sAcordRequest = string.Empty;
            string sResult = string.Empty;

            //tanwar2 - JIRA RMA 4824 - Code Review - start
            //XmlDocument xResultDoc = null;
            //XmlNode xPolicyinceptionDate = null;
            //XmlNode xDateOfLoss = null;
            //XmlNode xLastPaymentDate = null;
            XDocument xResultDoc = null;
            XElement xPolicyinceptionDate = null;
            XElement xDateOfLoss = null;
            XElement xLastPaymentDate = null;
            XElement xCvgEvalDate = null;
            //tanwar2 - JIRA RMA 4824 - Code Review - end
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                oDedManagerAdaptor = new DeductibleManagementAdaptor();
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();


                {
                    //Get Date from point system
                    try
                    {
                        InitiateServiceProcess(oRequest, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
                        //tanwar2 - JIRA RMA 4824 - Code Review - start
                        InitiateServiceProcess(oRequest, oDedManagerAdaptor, out oUserLogin, ref oSystemErrors);
                        //tanwar2 - JIRA RMA 4824 - Code Review - end
                    }
                    catch (Exception e)
                    {
                        // Throw back error if there is an error in intialization process.
                        oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                        RMException theFault = new RMException();
                        theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                        throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
                    }
                    try
                    {
                        bIsCvgEvalDateAvailable = oDedManagerAdaptor.IsCvgEvalDateAvailable(xDoc, oRequest, oUserLogin);
                        if (!bIsCvgEvalDateAvailable)
                        {
                            sAcordRequest = objPolicySystemAdapter.GetDimEvalDate(oRequest, ref oSystemErrors);

                            //tanwar2 - JIRA RMA 4824 - Code Review (1a) - start
                            //dtPolicySystemInfo = objPolicySystemAdapter.GetPolicySystemInfoById(oRequest.PolicySystemId, ref oSystemErrors);
                            //sResult = PostRequestToCFW(sAcordRequest, dtPolicySystemInfo, 0, ref oSystemErrors);
                            sResult = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, sAcordRequest, ref oSystemErrors);

                            //xResultDoc = new XDocument();
                            //xResultDoc.LoadXml(sResult);
                            //xLastPaymentDate = xResultDoc.SelectSingleNode("//com.csc_LastPaymntDate");
                            //xDateOfLoss = xResultDoc.SelectSingleNode("//com.csc_DateOfLoss");
                            //xPolicyinceptionDate = xResultDoc.SelectSingleNode("//com.csc_PolicyInceptionDate");
                            xResultDoc = XDocument.Parse(sResult);
                            xLastPaymentDate = xResultDoc.XPathSelectElement("//com.csc_LastPaymntDate");
                            xDateOfLoss = xResultDoc.XPathSelectElement("//com.csc_DateOfLoss");
                            xPolicyinceptionDate = xResultDoc.XPathSelectElement("//com.csc_PolicyInceptionDate");
                            //tanwar2 - JIRA RMA 4824 - Code Review (1a) - end

                            xCvgEvalDate = xDoc.XPathSelectElement("//control[@name='CoverageEvaluationDate']");
                            if (xCvgEvalDate != null)
                            {
                                if (xDateOfLoss != null)
                                {
                                    //if (!string.IsNullOrEmpty(xDateOfLoss.InnerText) && string.Compare(xDateOfLoss.InnerText, "0") != 0)
                                    //{
                                    //    xCvgEvalDate.SetValue(xDateOfLoss.InnerText);
                                    //    bDateSet = true;
                                    //}
                                    if (!string.IsNullOrEmpty(xDateOfLoss.Value) && string.Compare(xDateOfLoss.Value, "0") != 0)
                                    {
                                        xCvgEvalDate.SetValue(xDateOfLoss.Value);
                                        bDateSet = true;
                                    }
                                }
                            }
                            if (!bDateSet)
                            {
                                if (xPolicyinceptionDate != null)
                                {
                                    //if (!string.IsNullOrEmpty(xPolicyinceptionDate.InnerText) && string.Compare(xPolicyinceptionDate.InnerText, "0") != 0)
                                    //{
                                    //    //added 01 as the date returned is in YYYYmm format. No date value is reutrned
                                    //    xCvgEvalDate.SetValue(xPolicyinceptionDate.InnerText + "01");
                                    //    bDateSet = true;
                                    //}
                                    if (!string.IsNullOrEmpty(xPolicyinceptionDate.Value) && string.Compare(xPolicyinceptionDate.Value, "0") != 0)
                                    {
                                        //added 01 as the date returned is in YYYYmm format. No date value is reutrned
                                        xCvgEvalDate.SetValue(xPolicyinceptionDate.Value + "01");
                                        bDateSet = true;
                                    }
                                }
                            }

                        }
                        if (bDateSet || bIsCvgEvalDateAvailable)
                        {
                            oDedManagerAdaptor.GetDiminishingDetails(xDoc, oUserLogin, ref oSystemErrors);
                        }
                        return xDoc;
                    }
                    catch (Exception e)
                    {
                        oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                        RMException theFault = new RMException();
                        theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                        throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
                    }
                }
            }
            catch (FaultException<RMException>)
            {
                //Exception of this type is already handled. No need to handle it again
                throw;
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Start - Added by Nikhil on 09/22/14.Code Review changes
            finally
            {
                if (oSystemErrors != null)
                {
                    oSystemErrors = null;

                }
                if (oDedManagerAdaptor != null)
                {
                    oDedManagerAdaptor = null;

                }
                if (objPolicySystemAdapter != null)
                {
                    objPolicySystemAdapter = null;

                }
            }
        }
        

        private string GetCPPInsuranceLine(PolicySystemInterfaceAdaptor objPolicySystemAdapter, Object oRequest, ref BusinessAdaptorErrors oSystemErrors, string sMode)
        {
            string oAcordReq = string.Empty;
            string oAcordRes = string.Empty;
            string sUnitInsLine = string.Empty;
            XElement oElement = null;
            XElement oNodeElement = null;
            int iPolicySystemId = 0;
            PolicyEnquiry oPolicyEnquiry = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            try
            {
                switch (sMode.ToUpper())
                {
                    case "POLICYENQUIRY":
                        oPolicyEnquiry = (PolicyEnquiry)oRequest;
                        iPolicySystemId = oPolicyEnquiry.PolicySystemId;
                        break;
                    case "SAVEUNIT":
                        oSaveDownloadOptions = (SaveDownloadOptions)oRequest;
                        iPolicySystemId = oSaveDownloadOptions.PolicySystemId;
                        break;
                }

                oAcordReq = objPolicySystemAdapter.GetCPPUnitInsuranceLineAcordRequest(oRequest, ref oSystemErrors, sMode);
                if (!string.IsNullOrEmpty(oAcordReq))
                {
                    oAcordRes = PostRequestToPolicySystem(objPolicySystemAdapter, iPolicySystemId, oAcordReq, ref oSystemErrors);
                    oElement = XElement.Parse(oAcordRes);
                    oNodeElement = oElement.XPathSelectElement("./ClaimsSvcRs/com.csc_CPPUnitInsLineRs/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_InsLineCd']/OtherId");
                    if (oNodeElement != null && !string.IsNullOrEmpty(oNodeElement.Value))
                        sUnitInsLine = oNodeElement.Value;
                }
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
            }
            finally
            {
                oElement = null;
                oNodeElement = null;
            }

            return sUnitInsLine;
        }

        private string PostRequestToPolicySystem(PolicySystemInterfaceAdaptor objPolicySystemAdapter, int iPolicySystemId, string sAcordRequest, ref BusinessAdaptorErrors oSystemErrors)
        {
            DataTable dtPolicySystemInfo = null;
            string sSearchAcordRequest = string.Empty;
            string sSearchAcordResponse = string.Empty;
            string sPolicySystemType = string.Empty;

            try
            {
                dtPolicySystemInfo = objPolicySystemAdapter.GetPolicySystemInfoById(iPolicySystemId, ref oSystemErrors);
                if (dtPolicySystemInfo != null)
                {
                    sPolicySystemType = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["POLICY_SYSTEM_TYPE"]);
                    switch (sPolicySystemType)
                    {
                        case "POINT":
                            sSearchAcordResponse = PostRequestToCFW(sAcordRequest, dtPolicySystemInfo, 0, ref oSystemErrors);
                            break;
                        //to-do: Need to add some more cases for other policy system types like exceed, others
                    }
                }
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //RMException theFault = new RMException();
                //theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                //throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return sSearchAcordResponse;
        }

        private string PostRequestToCFW(string sAcordReq, DataTable dtPolicySystemInfo, int iPostAttempt, ref BusinessAdaptorErrors oSystemErrors)
        {
            WebRequest oRequest;
            byte[] byteReqData = null;
            StreamWriter oStream = null;
            StreamReader oStreamReader = null;
            HttpWebResponse oResponse = null;

            int iTimeOut;
            string sURL = string.Empty;
            string sUsername = string.Empty;
            string sPassword = string.Empty;
            string sProxy = string.Empty;
            string Result = string.Empty;
            bool blnSuccess = false;

            try
            {
                NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface",string.Empty,m_ClientId);
                iTimeOut = Conversion.CastToType<int>(nvCollSettings["PolicySystemTimeout"], out blnSuccess);
                if (iPostAttempt == 0)
                    iPostAttempt = Conversion.CastToType<int>(nvCollSettings["NumberOfTrys"], out blnSuccess);

                sURL = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["URL_PARAMETER"]);
                sProxy = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["PROXY"]);
                sUsername = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["USERNAME"]);
                sPassword = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["PASSWORD"]);

                byteReqData = Encoding.UTF8.GetBytes(sAcordReq);

                oRequest = WebRequest.Create(sURL);
                oRequest.Method = WebRequestMethods.Http.Post;
                oRequest.ContentType = "text/xml";
                oRequest.ContentLength = byteReqData.Length;
                oRequest.Timeout = iTimeOut == 0 ? 20000 : iTimeOut;

                if (!string.IsNullOrEmpty(sProxy))
                {
                    WebProxy oProxy = new WebProxy(sProxy);
                    oRequest.Proxy = oProxy;
                }

                if (!string.IsNullOrEmpty(sUsername))
                {
                    NetworkCredential oCredentials = new NetworkCredential(sUsername, sPassword);
                    oRequest.Credentials = oCredentials;
                }

                oStream = new StreamWriter(oRequest.GetRequestStream());
                oStream.Write(sAcordReq);
                oStream.Close();

                iPostAttempt--;
                oResponse = (HttpWebResponse)oRequest.GetResponse();

                if (oResponse.StatusCode != HttpStatusCode.OK)
                    oSystemErrors.Add("Riskmaster.WebService.PolicyInterface.PostToCFWError",
                        string.Format(Globalization.GetString("Riskmaster.WebService.PolicyInterface.PostToCFWError",m_ClientId), oResponse.StatusCode.ToString(), oResponse.StatusDescription),
                        BusinessAdaptorErrorType.SystemError);
                else
                {
                    oStreamReader = new StreamReader(oResponse.GetResponseStream());
                    Result = oStreamReader.ReadToEnd();
                }
            }
            catch (WebException webex)
            {
                if (webex.Status == WebExceptionStatus.Timeout || (oResponse != null && oResponse.StatusCode == HttpStatusCode.RequestTimeout))
                {
                    if (iPostAttempt > 0)
                    {
                        oSystemErrors.Add(webex, BusinessAdaptorErrorType.SystemError);
                        PostRequestToCFW(sAcordReq, dtPolicySystemInfo, iPostAttempt, ref oSystemErrors);
                    }
                }
                else if (webex.Status == WebExceptionStatus.ProtocolError)
                {
                    throw webex;
                }
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //RMException theFault = new RMException();
                //theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                //throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oStream != null)
                {
                    oStream.Close();
                }
                if (oResponse != null)
                {
                    oResponse.Close();
                }
                if (oStreamReader != null)
                {

                    oStreamReader.Dispose();
                }
            }
            return Result;
        }

        public void GetDownLoadXMLData(PolicyDownload oRequest, out PolicyDownload oResponse)
        {
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            oResponse = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetDownLoadXMLData";

            bool bresult = false;
            try
            {
                oResponse = new PolicyDownload();
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                objPolicySystemAdapter.GetDownLoadXMLData(oRequest, out oResponse, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetEndorsementDataForTracking(PolicyDownload oRequest, out PolicyDownload oResponse)
        {
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            oResponse = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetEndorsementData";

            bool bresult = false;
            try
            {
                oResponse = new PolicyDownload();
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                objPolicySystemAdapter.GetEndorsementData(oRequest, out oResponse, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetUnitInterestListResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetUnitInterestListAcordRequest";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetUnitInterestListAcordRequest(oRequest, ref oSystemErrors, "ENQUIRY", string.Empty);
                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                    oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    if (oResponse == null)
                        oResponse = new PolicyEnquiry();
                    oResponse.ResponseAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        public void SearchPolicyUnits(UnitSearch oUnitSearch, out UnitListing oUnitListing)
        {
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            oUnitListing = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "SearchPolicyUnits";

            bool bresult = false;
            try
            {
                //  oUnitListing = new PolicyDownload();
                oSystemErrors = new BusinessAdaptorErrors(oUnitSearch.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oUnitSearch, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oUnitSearch.ClientId);
                bresult = objPolicySystemAdapter.SearchPolicyUnits(oUnitSearch, out oUnitListing, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicyUnitInterestList(PolicyEnquiry oRequest, out PolicyEnquiry oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyUnitInterestList";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                objPolicySystemAdapter.GetPolicyUnitInterestList(oRequest, out oResponse, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        //Ankit Start : Worked for RedundantEntitiesFixTool
        public void GetPolicyInterestList(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyInterestList";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oEnquiryResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oEnquiryRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oEnquiryRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oEnquiryRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetPolicyInterestListAcordRequest(oEnquiryRequest, ref oSystemErrors, "ENQUIRY");
                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                    oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    if (oEnquiryResponse == null)
                        oEnquiryResponse = new PolicyEnquiry();
                    oEnquiryResponse.PolicyInterestListAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicyDriverList(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyDriverList";
            bool bresult = false;
            string oAcordRequest = string.Empty;
            string oAcordResponse = string.Empty;
            oEnquiryResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oEnquiryRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oEnquiryRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oEnquiryRequest.ClientId);
                oAcordRequest = objPolicySystemAdapter.GetPolicyDriverListAcordRequest(oEnquiryRequest, ref oSystemErrors, "ENQUIRY");
                if (!string.IsNullOrEmpty(oAcordRequest))
                {
                    oAcordResponse = PostRequestToPolicySystem(objPolicySystemAdapter, oEnquiryRequest.PolicySystemId, oAcordRequest, ref oSystemErrors);
                    if (oEnquiryResponse == null)
                        oEnquiryResponse = new PolicyEnquiry();
                    oEnquiryResponse.DriverListAcordXML = oAcordResponse;
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        //Ankit End

        public void GetPolicyDetails(FetchPolicyDetails oRequest, out FetchPolicyDetails oResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            PolicySystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicyDetails";
            bool bresult = false;
            oResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objPolicySystemAdapter = new PolicySystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                objPolicySystemAdapter.GetPolicyDetails(oRequest, out oResponse, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

       public void ImportMappingFromFile(LossCodeMappingFileContent oRequest, out LossCodeMappingOutput oResponse)
        {

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            LossCodeMappingAdaptor objLossCodeMappingAdaptor = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "ImportMappingFromFile";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objLossCodeMappingAdaptor = new LossCodeMappingAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objLossCodeMappingAdaptor, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                bresult = objLossCodeMappingAdaptor.ImportMappingFromFile(oRequest, out oResponse, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void ReplicateLossCodeMapping(ReplicateLossCodeMapping oRequest, out LossCodeMappingOutput oResponse)
        {

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            LossCodeMappingAdaptor objLossCodeMappingAdaptor = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "ReplicateLossCodeMapping";

            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objLossCodeMappingAdaptor = new LossCodeMappingAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objLossCodeMappingAdaptor, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                bresult = objLossCodeMappingAdaptor.RepicateMapping(oRequest, out oResponse, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

        public string ExportMapping(ExportLossCodeMapping oRequest)
        {

            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            LossCodeMappingAdaptor objLossCodeMappingAdaptor = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "ExportMapping";
            string sFileContent = string.Empty;
            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oRequest.ClientId);
                objLossCodeMappingAdaptor = new LossCodeMappingAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRequest, out xmlRequest, sFunctionName, objLossCodeMappingAdaptor, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, 	false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oRequest.ClientId);
                bresult = objLossCodeMappingAdaptor.ExportMapping(Conversion.ConvertStrToInteger(oRequest.PolicySystemId), ref sFileContent, ref oSystemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

            return sFileContent;

        }
    }
}


