﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
//using Riskmaster.Models;

namespace RiskmasterService.DataStreamingServiceForDA
{
    // NOTE: If you change the interface name "IDataStreamingService" here, you must also update the reference to "IDataStreamingService" in Web.config.
    [ServiceContract]
    public interface IDataStreamingService
    {

        [OperationContract]
        [FaultContract(typeof(Riskmaster.Models.RMException))]
        void CreateDocument(StreamedDocumentType request);

        [OperationContract]
        [FaultContract(typeof(Riskmaster.Models.RMException))]
        void UploadBRSFeeScheuleFile(StreamedBRSDocumentType request);

        [OperationContract]
        [FaultContract(typeof(Riskmaster.Models.RMException))]
        Stream RetrieveDocument(string request1, string request2, string screenflag, string token, int iClientId, string sFormName);
        
        [OperationContract]
        [FaultContract(typeof(Riskmaster.Models.RMException))]
        void UploadFile(StreamedUploadDocumentType oStreamedUploadDocumentType);
    }
}
