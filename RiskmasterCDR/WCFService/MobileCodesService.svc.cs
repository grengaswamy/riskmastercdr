﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;


namespace RiskmasterService
{
    // NOTE: If you change the class name "Codes" here, you must also update the reference to "Codes" in Web.config.
    [AspNetCompatibilityRequirements(
         RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class MobileCodesService : RMService, IMobileCodesService
    {
        
        /// <summary>
        /// Get Codes List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public void GetCodes(CodeTypeRequest request, out CodeListType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetCodes"; 
            UserLogin oUserLogin = null;
            CodesListAdaptor objCodes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new CodeListType();
                objCodes = new CodesListAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objCodes, out oUserLogin, ref  systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objCodes.GetCodesNew(request, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        
        public string GetCodesForMobileAdj(string sRequest) 
        {
            CodeTypeRequest objCode = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            string functionName = "GetCodes";
            string sToken = string.Empty;
            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            CodeListType objList = null;
            XmlElement objElement;
            XmlElement objXmlDoc = null;
            string sResponse = "test";
            int iRecordCount = 0;
            try
            {
                xmlRequest.LoadXml(sRequest);
                sToken = xmlRequest.SelectSingleNode("//Authorization").InnerText;
                objCode = new CodeTypeRequest();
                objCode.ClientId = 0; // Windows App is not cloud enabled
                xmlResponse.InnerXml = "<CodeTables></CodeTables>";
                foreach(XmlNode objNode in xmlRequest.SelectNodes("//CodeTables/CodeTable"))
                {
                    objCode.TableName = objNode.Attributes["name"].Value;
                    objCode.FormName = objNode.Attributes["formname"].Value;
                    objCode.TriggerDate = "";
                    objCode.TypeLimits = "";
                    objCode.DeptEId = "";
                    objCode.SessionLOB = "";
                    objCode.EventDate = "";
                    objCode.SessionClaimId = "";
                    objCode.RecordCount = "";
                    objCode.PageNumber = "";
                    objCode.Title = "";
                    objCode.InsuredEid = "";
                    objCode.EventId = "";
                    objCode.ParentCodeID = "";
                    objCode.Filter = "";
                    objCode.ShowCheckBox = "";
                    objCode.Token = sToken;
                    objCode.LOB = "";
                    objCode.bIsMobileAdjuster = true;
					//mbahl3 mits 34651 for mobile adjuster 
                    if(objCode.TableName.Equals("ENTITY"))
                    {
                        objCode.SortColumn = "4";
                    }
                    else
                    {
                        objCode.SortColumn = "1";                        
                    }
                    objCode.SortOrder = "Asc";
							//mbahl3 mits 34651 for mobile adjuster 
                    GetCodes(objCode,out objList); 
                    objXmlDoc = (XmlElement)xmlResponse.SelectSingleNode("//CodeTables");
                    iRecordCount = objList.Codes.Count;
                    objElement = xmlResponse.CreateElement("CodeTable");
                    objElement.SetAttribute("name", objNode.Attributes["name"].Value);
                    objXmlDoc.AppendChild(objElement);

                    for (int i = 0; i < iRecordCount; i++)
                    {
                        objXmlDoc = (XmlElement)xmlResponse.SelectSingleNode("//CodeTable[@name='" + objCode.TableName + "']");
                        objElement = xmlResponse.CreateElement("code");
                        objElement.SetAttribute("shortcode", objList.Codes[i].ShortCode);
                        objElement.SetAttribute("desc", objList.Codes[i].Desc);
                        objElement.SetAttribute("codeid", objList.Codes[i].Id.ToString());
                        objXmlDoc.AppendChild(objElement);

                        if (objCode.TableName.Equals("PERSON_INV_TYPE") && (i == (iRecordCount -1)))
                        {
                            objElement = xmlResponse.CreateElement("code");
                            objElement.SetAttribute("shortcode", "");
                            objElement.SetAttribute("desc", "DRIVER_INSURED");                          
                            objXmlDoc.AppendChild(objElement);

                            objElement = xmlResponse.CreateElement("code");
                            objElement.SetAttribute("shortcode", "");
                            objElement.SetAttribute("desc", "DRIVER_OTHER");
                            objXmlDoc.AppendChild(objElement);
                        }
                    }
                    //objElement.
                }
                return xmlResponse.InnerXml.ToString();
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

	   
    }
}
