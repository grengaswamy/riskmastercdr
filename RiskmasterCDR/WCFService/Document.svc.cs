﻿//#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Collections.Specialized;
using System.Web;
using Riskmaster.Common;

namespace RiskmasterService
{
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class Document : RMService, IDocument
    {
        //rbhatia4:R8: Use Media View Setting : July 08 2011
        //Restructuring Document Management Code for choosing the selected vendor
        /*
        //Mona:PaperVision Merge: Checking whether PaperVision is ON or OFF
        public string GetPaperVisionSetting(DocumentType objDocReq)
        {
            
            DocumentManagementAdaptor objDocument = null;
            UserLogin oUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            string functionName = "GetPaperVisionSetting";
            XmlDocument xmlRequest = null;          
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {               
                systemErrors = new BusinessAdaptorErrors();               
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDocReq, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin);

                bResult = objDocument.GetPaperVisionSetting(ref objDocReq, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objDocReq.IsAtPaperVision;
        }

         */
        //rbhatia4:R8: Use Media View Setting : July 05 2011
        //Making  a general helper method to find out which document management  system has been used

        public DocumentVendorDetails GetDocumentManagementVendorDetails(DocumentVendorDetails oDocumentVendorDetails)
        {

            DocumentManagementAdaptor objDocument = null;
            UserLogin oUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            string functionName = "GetDocumentManagementVendorName";
            XmlDocument xmlRequest = null;
            bool bUsePaperVision = false; //mbahl3 mits 29236
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentVendorDetails objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentVendorDetails.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentVendorDetails, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentVendorDetails.ClientId);

                objDocument.GetDocumentManagementVendorDetails(oDocumentVendorDetails, out objReturn, ref errOut, ref bUsePaperVision); //mbahl3 mits 29236
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
			//mbahl3
            if (bUsePaperVision || (errOut.Count >0) || (systemErrors.Count>0)) //mbahl3 mits 29236
            {

                logErrors(functionName, xmlRequest, bResult, errOut);
            }
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }

        public DocumentType GetDocument(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentType objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objReturn = new DocumentType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.GetDocumentDetailsIntegrated(oDocumentType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public DocumentType UpdateDocumentFile(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "UpdateDocumentFile";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentType objReturn = null;

            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objReturn = new DocumentType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.UpdateDocumentFile(oDocumentType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public DocumentType RetrieveDocument(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentType objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objReturn = new DocumentType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.RetrieveDocument(oDocumentType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public DocumentList GetDocumentsList(DocumentListRequest oDocumentListRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetDocumentsList";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentList objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentListRequest.ClientId);
                objReturn = new DocumentList();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentListRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentListRequest.ClientId);
                bResult = objDocument.GetDocumentsNew(oDocumentListRequest, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public DocumentList GetAttachments(DocumentListRequest oDocumentListRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetDocumentsList";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            DocumentList objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentListRequest.ClientId);
                objReturn = new DocumentList();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentListRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentListRequest.ClientId);
                bResult = objDocument.GetAttachmentsNew(oDocumentListRequest, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public bool CreateFolder(FolderTypeRequest oFolderTypeRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateFolder";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oFolderTypeRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oFolderTypeRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oFolderTypeRequest.ClientId);
                bResult = objDocument.AddFolderNew(oFolderTypeRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public bool Delete(DocumentsDeleteRequest oDocumentsDeleteRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "Delete";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentsDeleteRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentsDeleteRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentsDeleteRequest.ClientId);
                bResult = objDocument.DeleteNew(oDocumentsDeleteRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public bool MoveDocuments(MoveDocumentsRequest oMoveDocumentsRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "MoveDocuments";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oMoveDocumentsRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oMoveDocumentsRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oMoveDocumentsRequest.ClientId);
                bResult = objDocument.MoveDocumentsNew(oMoveDocumentsRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public bool UpdateDocument(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "MoveDocuments";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.UpdateDocumentNew(oDocumentType, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public bool TransferDocuments(TransferDocumentsRequest oTransferDocumentsRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "MoveDocuments";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oTransferDocumentsRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oTransferDocumentsRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oTransferDocumentsRequest.ClientId);
                bResult = objDocument.TransferDocumentsNew(oTransferDocumentsRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public bool CopyDocuments(TransferDocumentsRequest oTransferDocumentsRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "MoveDocuments";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oTransferDocumentsRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oTransferDocumentsRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oTransferDocumentsRequest.ClientId);
                bResult = objDocument.CopyDocumentsNew(oTransferDocumentsRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public UsersList GetUsers(DocumentUserRequest oDocumentUserRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetUsers";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            UsersList objReturn=null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentUserRequest.ClientId);
                objReturn = new UsersList();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentUserRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentUserRequest.ClientId);
                bResult = objDocument.GetUsersNew(oDocumentUserRequest, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public bool EmailDocuments(EmailDocumentsRequest oEmailDocumentsRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "EmailDocuments";
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            UserLogin oUserLogin = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oEmailDocumentsRequest.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oEmailDocumentsRequest, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oEmailDocumentsRequest.ClientId);
                bResult = objDocument.EmailDocumentsNew(oEmailDocumentsRequest, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bResult;
        }
        public List<Folder> GetFolders(Folder oFolder)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetFolders";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            List<Folder> objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oFolder.ClientId);
                objReturn = new List<Folder>();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oFolder, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oFolder.ClientId);
                bResult = objDocument.GetFoldersAsOptionNew(oFolder, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public RMServiceType CreateDocument(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            RMServiceType objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.AddDocument(oDocumentType, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public DocumentType CreateDocumentDetail(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocumentDetail";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            //Initialize the Error Collection for handling the error on Service Layer
            DocumentType objReturn = null;
            try
            {
                objReturn = new DocumentType();
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                objDocument.CreateNewDocument(oDocumentType, out objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            return objReturn;
            // Final log if all went well
            //logErrors(functionName, xmlRequest,, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, errOut);
                logErrors(functionName, xmlRequest, false, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }


        /// <summary>
        /// Gets the file content of Executive Summary for a claim
        /// </summary>
        /// <param name="request"></param>
        /// <param name="objReturn"></param>
        public DocumentType GetExecutiveSummaryFileDetails(DocumentType oDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetExecutiveSummaryFileDetails";
            UserLogin oUserLogin = null;
            EmailDocumentsAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            DocumentType objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                objReturn = new DocumentType();
                objDocument = new EmailDocumentsAdaptor() ;
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.GetExecutiveSummaryFileDetails(oDocumentType, out  objReturn, ref errOut);
                
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
         /// <summary>
         /// Check whether Outlook Setting is on
         /// </summary>
         /// <param name="request"></param>
         /// <param name="bIsUseOutlookEnabled"></param>
        public bool CheckOutlookSetting(DocumentType oDocumentType)
        {
            bool bIsUseOutlookEnabled = false;
            XmlDocument xmlRequest = null;
            string functionName = "CheckOutlookSetting";
            UserLogin oUserLogin = null;
            EmailDocumentsAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDocumentType.ClientId);
                //objReturn = new DocumentType();
                objDocument = new EmailDocumentsAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDocumentType.ClientId);
                bResult = objDocument.CheckOutlookSetting(oDocumentType, out  bIsUseOutlookEnabled, ref errOut);

            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bIsUseOutlookEnabled;
        }
        
                
        //nsharma202 - Overload to add streamed document from mobile 
        public void CreateDocument(StreamedDocumentType request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.AddDocument(request, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
    }
}
