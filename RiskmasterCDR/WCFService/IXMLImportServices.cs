﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    [ServiceContract]
    public interface IXMLImportServices
    {
        /// <summary>
        /// ProcessXML
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/list", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        XMLOutPut ProcessImportXML(XMLInput oXMLInput);

        /// <summary>
        /// GetSuppTemplate
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/template", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        XMLTemplate GetSuppTemplate(XMLTemplate oXMLTemplate);
    
    
    }
}
