﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;


namespace RiskmasterService
{
    // NOTE: If you change the interface name "IPowerViewUpgradeService" here, you must also update the reference to "IPowerViewUpgradeService" in Web.config.
    [ServiceContract]
    public interface IPowerViewUpgradeService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DoWork();
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void UpgradeXmlTagToAspxForm(WCPowerViewUpgrade request, out WCPowerViewUpgrade objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void FetchJurisdictionalData(WCPowerViewUpgrade request, out WCPowerViewUpgrade objReturn);
    }
}
