﻿using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Linq;
using System.Xml.Linq;
//Added by manika : XMLImport 
namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "XMLImport" here, you must also update the reference to "XMLImport" in Web.config.
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class XMLImport : RMService, IXMLImport
    {
        //public void ProcessXML(RMServiceType request, string InputXML)
        public bool ProcessXML(out XMLOutPut objOutPut, XMLInput oXMLInput, bool sVal)//logFileContent Added for MITS 29711 by bsharma33 //edited by prashbiharis
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            ImportXMLAdaptor objxmlimport = null;
            BusinessAdaptorErrors systemErrors = null;
            string sFunctionName = "ProcessXML";
            //Added by Amitosh
            bool bresult = false;
            objOutPut = null;
             try
            {
                systemErrors = new BusinessAdaptorErrors();
              //  objReturn = new ProgressNotesType();
                objxmlimport = new ImportXMLAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oXMLInput, out xmlRequest, sFunctionName, objxmlimport, out oUserLogin, ref systemErrors);
            }
             catch (Exception e)
             {
                 // Throw back error if there is ne error in intialization process.
                 systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                 logErrors(sFunctionName, xmlRequest, false, systemErrors);
                 RMException theFault = new RMException();
                 theFault.Errors = formatOutputXML(null, false, systemErrors);
                 throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
             }
            try
            {
                systemErrors = new BusinessAdaptorErrors(oUserLogin);
                bresult = objxmlimport.XMLImport(oXMLInput.GetXMLInput, ref systemErrors, out objOutPut, sVal);//logFileContent Added for MITS 29711 by bsharma33
                               
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, systemErrors);

            if (systemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return bresult;
        }

        public void GetSuppTemplate(XMLTemplate oXMLInput,out XMLTemplate objReturn)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            ImportXMLAdaptor objXmlImport = null;
            BusinessAdaptorErrors systemErrors = null;
            string sFunctionName = "ProcessXML";
            //Added by Amitosh
            bool bresult = false;
            try
            {
                systemErrors = new BusinessAdaptorErrors();
                //  objReturn = new ProgressNotesType();
                objXmlImport = new ImportXMLAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oXMLInput, out xmlRequest, sFunctionName, objXmlImport, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                systemErrors = new BusinessAdaptorErrors(oUserLogin);
                bresult = objXmlImport.GetSupplementals(oXMLInput,out objReturn, ref systemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, systemErrors);

            if (systemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
           
        }
    }
}
