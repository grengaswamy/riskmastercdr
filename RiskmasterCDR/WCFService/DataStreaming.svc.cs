﻿using System;
using System.ServiceModel;
using System.IO;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDataStreamingService" here, you must also update the reference to "IDataStreamingService" in Web.config.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    public class DataStreaming : RMService,IDataStreaming
    {
        public byte[] RetrieveDocument(StreamedDocumentType oStreamedDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            StreamedDocumentType objReturn = null;
            StreamedDocumentType request = null;
            Stream downloadedStream = null;
            bool bResult = false;
            bool bIsStorageFileSystem = false;
            byte[] OutBytes = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                request=new StreamedDocumentType();
                request.DocumentId = oStreamedDocumentType.DocumentId;
                request.Token = oStreamedDocumentType.Token;
                request.PsId = oStreamedDocumentType.PsId;
                request.ScreenFlag = oStreamedDocumentType.ScreenFlag;
                request.FormName = oStreamedDocumentType.FormName;
                request.ClientId = oStreamedDocumentType.ClientId;
                systemErrors = new BusinessAdaptorErrors(oStreamedDocumentType.ClientId);
                objReturn = new StreamedDocumentType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oStreamedDocumentType.ClientId);
                objDocument.RetrieveLargeDocument(request,out objReturn,ref bIsStorageFileSystem,ref errOut);

                //downloadedStream = objReturn.fileStream;
                downloadedStream = new System.IO.FileStream(objReturn.FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                OutBytes = new byte[downloadedStream.Length];
                downloadedStream.Read(OutBytes, 0, Convert.ToInt32(downloadedStream.Length));
                downloadedStream.Flush();

                if (!bIsStorageFileSystem)
                {
                    OperationContext clientContext = OperationContext.Current;
                    clientContext.OperationCompleted += new EventHandler(delegate(object sender, EventArgs args)
                    {
                        if (downloadedStream != null)
                            downloadedStream.Dispose();
                        File.Delete(objReturn.FilePath);
                    });
                }

            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return OutBytes;

        }

        public void UploadBRSFeeScheuleFile(StreamedBRSDocumentType oStreamedBRSDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oStreamedBRSDocumentType.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(oStreamedBRSDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oStreamedBRSDocumentType.ClientId);
                bResult = objDocument.UploadBRSFeeScheuldeFile(oStreamedBRSDocumentType, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        //mbahl3 jira RMACLOUD-2383
        public void UploadFile(StreamedUploadDocumentType oStreamedUploadDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oStreamedUploadDocumentType.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(oStreamedUploadDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oStreamedUploadDocumentType.ClientId);
                bResult = objDocument.UploadFile(oStreamedUploadDocumentType, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        //mbahl3 jira RMACLOUD-2383

        public void CreateDocument(StreamedDocumentType oStreamedUploadDocumentType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oStreamedUploadDocumentType.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(oStreamedUploadDocumentType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oStreamedUploadDocumentType.ClientId);
                bResult = objDocument.AddDocument(oStreamedUploadDocumentType, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
    }
}
