﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IProgressNoteService" here, you must also update the reference to "IProgressNoteService" in Web.config.
    [ServiceContract]
    public interface IMobileProgressNoteService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DoWork();                                                                                         
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string LoadProgressNotesPartialForMobileAdj(string xmlRequest);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string SaveNotesForMobileAdj(string sRequest);
    }
}
