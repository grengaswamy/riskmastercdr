﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICodesService" here, you must also update the reference to "ICodesService" in Web.config.
    [ServiceContract]
    
    public interface IMobileCodesService
    {       
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string GetCodesForMobileAdj(string xmlRequest);
    }
}
