﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Collections;
using System.ServiceModel.Activation;
using System.Reflection;
using Riskmaster.BusinessAdaptor.RMUtilities;
using Riskmaster.Cache;

namespace RiskmasterService
{
    // 22 August 2011 Ijha: Wrapper to call different services and recover a common response
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MobileUploadService" in code, svc and config file together.
    public class MobileUploadService : IMobileUploadService
    {
        public void DoWork()
        {
        }
        public string UploadRequest(string xmlRequest)
        {
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            XmlDocument xmlDocRequest = null;
            XmlDocument xmlDocResponse = null;
            XmlDocument xmlDocReqUpdateDiary = null;
            XmlDocument xmlDocDiary = null;
            XmlDocument xmlDocDiaryResp = null;
            XmlDocument xmlTmp = null;
            XmlNode objNode = null;
            string xmlDocIn = string.Empty;
            string xmlDocOut = string.Empty;
            XmlElement xmlDocTemp = null;
            XmlElement xmlIn = null;
            XmlNode objType = null;
            MobileCommonWCFService objCommonService = null;
            XmlDocument xmlAggResponses = null;
            XmlElement xmlEleResponse = null;
            XmlNode objResp = null;
            string functionName = string.Empty;
            MobileUploadAdaptor objMobileAdaptor = null;
            UserLogin oUserLogin = null;
            XmlNode objStatus = null;
            string oMessageElement = string.Empty;
            string sNoteText = string.Empty;
            XmlDocument x = null;
            int iClientId = 0;
            try
            {
                functionName = "UploadFromMobile";
                xmlDocRequest = new XmlDocument();
                xmlDocResponse = new XmlDocument();
                systemErrors = new BusinessAdaptorErrors(iClientId);
                string ret = string.Empty;
                xmlTmp = new XmlDocument();
                xmlDocRequest.LoadXml(xmlRequest);
                xmlDocTemp = xmlDocRequest.DocumentElement;

                XmlNodeList XmlList = xmlDocTemp.SelectNodes("/Upload/Save");
                xmlAggResponses = new XmlDocument();
                xmlEleResponse = xmlAggResponses.CreateElement("Responses");
                xmlAggResponses.AppendChild(xmlEleResponse);
                objMobileAdaptor = new MobileUploadAdaptor();
                bool bResult = false;
                x = new XmlDocument();
                foreach (XmlNode xn in XmlList)
                {
                    objNode = xn;
                    string xmldata = string.Empty;
                    objType = xn.FirstChild;
                    xmldata = objType.InnerText;

                    switch (xmldata)
                    {
                        case "savediary":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveDiaryResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objNode = null;
                            objCommonService = null;
                            break;

                        case "updatediary":
                            xmlDocReqUpdateDiary = new XmlDocument();
                            xmlDocDiary = new XmlDocument();
                            objNode = xn.LastChild;

                            x.LoadXml(objNode.OuterXml);

                            xmlDocReqUpdateDiary.LoadXml(objNode.OuterXml);
                            xmlIn = (XmlElement)xmlDocReqUpdateDiary.SelectSingleNode("/Message/Document/*");
                            xmlDocDiary.LoadXml(xmlIn.OuterXml);
                            errOut = new BusinessAdaptorErrors(oUserLogin, iClientId);
                            InitiateServiceProcess(xmlDocReqUpdateDiary, functionName, objMobileAdaptor, out oUserLogin, ref systemErrors);
                            bResult = objMobileAdaptor.GetStatus(xmlDocDiary, ref xmlDocDiaryResp, ref errOut);
                            xmlDocResponse = formatOutputXML(xmlDocDiaryResp, bResult, errOut);
                            objResp = xmlDocResponse.SelectSingleNode("//MsgStatusCd");
                            if (objResp != null && objResp.InnerText == "Success")
                            {
                                objStatus = xmlDocResponse.SelectSingleNode("//Status");
                                if (objStatus != null && objStatus.InnerText == "Open")
                                {
                                    objCommonService = new MobileCommonWCFService();
                                    xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                                    xmlDocResponse.LoadXml(xmlDocOut);

                                    objResp = xmlAggResponses.CreateElement("UpdateDiaryResponse");
                                    objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                                    xmlAggResponses.FirstChild.AppendChild(objResp);

                                    objCommonService = null;
                                }
                                else if (objStatus != null && objStatus.InnerText == "Closed")
                                {
                                    sNoteText = x.SelectSingleNode("//Message/Document/SaveDiary/TaskSubject").InnerText;
                                    oMessageElement = GetNotesTemplate().ToString();
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", x.SelectSingleNode("//Authorization").InnerText);
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", x.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/ACTIVITY_DATE", System.DateTime.Now.ToShortDateString());
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/NOTE_TEXT", "The Diary with the Task Name '" + sNoteText + "' has already been updated in Riskmaster.");

                                    MobileProgressNoteService objProgressService = new MobileProgressNoteService();
                                    xmlDocOut = objProgressService.SaveNotesForMobileAdj(oMessageElement.ToString());

                                    objResp = xmlAggResponses.CreateElement("UpdateDiaryResponse");
                                    objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                                    xmlAggResponses.FirstChild.AppendChild(objResp);

                                    objProgressService = null;
                                }
                                else if (objStatus != null && objStatus.InnerText == "Deleted")
                                {
                                    sNoteText = x.SelectSingleNode("//Message/Document/SaveDiary/TaskSubject").InnerText;
                                    oMessageElement = GetNotesTemplate().ToString();
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", x.SelectSingleNode("//Authorization").InnerText);
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", x.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/ACTIVITY_DATE", System.DateTime.Now.ToShortDateString());
                                    oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/NOTE_TEXT", "The Diary with the Task Name '" + sNoteText + "' does not exist in Riskmaster.");

                                    MobileProgressNoteService objProgressService = new MobileProgressNoteService();
                                    xmlDocOut = objProgressService.SaveNotesForMobileAdj(oMessageElement.ToString());

                                    objResp = xmlAggResponses.CreateElement("UpdateDiaryResponse");
                                    objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                                    xmlAggResponses.FirstChild.AppendChild(objResp);
                                    objProgressService = null;
                                }
                            }
                            else if (objResp != null && objResp.InnerText == "Error")
                            {
                                objResp = xmlAggResponses.CreateElement("UpdateDiaryResponse");
                                objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                                xmlAggResponses.FirstChild.AppendChild(objResp);
                            }

                            break;

                        case "progressnotes":
                            objNode = xn.LastChild;
                            MobileProgressNoteService objProgressnoteService = new MobileProgressNoteService();
                            xmlDocOut = objProgressnoteService.SaveNotesForMobileAdj(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveProgressNotesResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ProgressNote").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objProgressnoteService = null;
                            break;

                        case "addunit":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveUnitResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "employee":
                            objNode = xn.LastChild;
                            x.LoadXml(objNode.OuterXml);

                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objStatus = xmlDocResponse.SelectSingleNode("//DupEmpNumber");
                            sNoteText = x.SelectSingleNode("//EmployeeNumber").InnerText;
                            if (objStatus != null && objStatus.InnerText == "True")
                            {
                                oMessageElement = GetNotesTemplate().ToString();
                                oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", x.SelectSingleNode("//Authorization").InnerText);
                                oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", x.SelectSingleNode("//ClaimNumber").InnerText);
                                oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/ACTIVITY_DATE", System.DateTime.Now.ToShortDateString());
                                oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/NOTE_TEXT", "The Employee With the Employee Number " + sNoteText + " already exists.");

                                MobileProgressNoteService objProgressService = new MobileProgressNoteService();
                                xmlDocOut = objProgressService.SaveNotesForMobileAdj(oMessageElement.ToString());
                            }

                            objResp = xmlAggResponses.CreateElement("SaveEmployeeResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "witness":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveWitnessResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "patient":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SavePatientResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "physician":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SavePhysicianResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "other":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveOtherPersonsResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        case "medicalstaff":
                            objNode = xn.LastChild;
                            objCommonService = new MobileCommonWCFService();
                            xmlDocOut = objCommonService.ProcessRequest(objNode.OuterXml);
                            xmlDocResponse.LoadXml(xmlDocOut);

                            objResp = xmlAggResponses.CreateElement("SaveMedicalStaffResponse");
                            objResp.InnerXml = xmlDocResponse.SelectSingleNode("//ResultMessage").OuterXml;
                            xmlAggResponses.FirstChild.AppendChild(objResp);

                            objCommonService = null;
                            break;

                        default:
                            break;
                    }

                }

            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //logErrors(functionName, xmlRequest, false, systemErrors);
                //RMException theFault = new RMException();
                xmlAggResponses = formatOutputXML(xmlDocResponse, false, systemErrors);
                //throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
                return xmlAggResponses.OuterXml;
            }
            return xmlAggResponses.OuterXml;
        }

        #region Primary Service initialization method
        /// <summary>
        /// This method initialize the Common tasks to be done for each service
        /// </summary>
        /// Author        :     Amandeep Kaur
        /// Date Created  :     08/10/2011
        /// <param name="request"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="functionName"></param>
        /// <param name="adaptor"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        protected bool InitiateServiceProcess(XmlDocument xmlDocRequest, string functionName, BusinessAdaptorBase adaptor,
            out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {

            XmlElement xmlAuth = null;
            string sessionKeySupplied = string.Empty;
            xmlAuth = (XmlElement)xmlDocRequest.SelectSingleNode("/Message/Authorization");

            sessionKeySupplied = xmlAuth.InnerText;

            if (!RMSessionManager.SessionExists(ConfigurationInfo.GetSessionConnectionString(0), sessionKeySupplied,0)) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", 0));
            }
            else //Session Exists - unpack UserLogin Object
            {
                UserSession objUserSession = RMSessionManager.GetSession(ConfigurationInfo.GetSessionConnectionString(0), sessionKeySupplied, string.Empty);
                SessionManager m_oSession = SessionManager.LoadSession(sessionKeySupplied, 0);

                oUserLogin = new UserLogin(0);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(objUserSession.BinaryItems[AppConstants.SESSION_OBJ_USER].BinaryValue);
                systemErrors = new BusinessAdaptorErrors(oUserLogin,0); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null) //BSB Protect for "bypass security" case.
                    adaptor.SetSecurityInfo(oUserLogin,0);

                // Pass session object to business adaptor
                if (m_oSession != null)
                {
                    adaptor.SetSessionObject(m_oSession);
                }//if
            }//else
            return true;
        }

        #endregion

        private XElement GetNotesTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
            <Authorization>2a1e9f69-3663-47d3-80fd-0a72e503db03</Authorization>
            <Document>
            <ProgressNotes>
            <FORM_NAME>Add Note</FORM_NAME>
            <NEW_RECORD>true</NEW_RECORD>
            <CLAIM_NUMBER>WC1990000002</CLAIM_NUMBER>
            <ACTIVITY_DATE>06/30/2011</ACTIVITY_DATE>
            <NOTE_TEXT>Police verification was done for the claim # PROP2011000091 on               30/06/11</NOTE_TEXT>
            <NOTE_TYPE_CODE>5231</NOTE_TYPE_CODE>
            </ProgressNotes>
            </Document> 
            </Message>
            ");

            return oTemplate;
        }
        //rsharma220 MITS 35387
        private XElement GetNotesTemplate_Event()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
            <Authorization></Authorization>
            <Document>
            <ProgressNotes>
            <FORM_NAME>Add Note</FORM_NAME>
            <NEW_RECORD>true</NEW_RECORD>
            <EVENT_NUMBER></EVENT_NUMBER>
            <ACTIVITY_DATE></ACTIVITY_DATE>
            <NOTE_TEXT></NOTE_TEXT>
            <NOTE_TYPE_CODE>5231</NOTE_TYPE_CODE>
            </ProgressNotes>
            </Document> 
            </Message>
            ");

            return oTemplate;
        }
        private string ChangeNotesTemplate(string xml, string nodetochange, string nodevalue)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.SelectSingleNode(nodetochange).InnerText = nodevalue;
            return doc.OuterXml;
        }
        private XmlDocument formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors)
        {
            // Create output envelope doc and root node (ResultMessage)
            XmlDocument xmlOutEnv = new XmlDocument();
            // JP TMP 01.18.2006   XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage", XML_NAMESPACE);
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);

            // Add errors
            formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors);

            // Add output document
            // JP TMP 01.18.2006   XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document", XML_NAMESPACE);
            XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
            xmlRoot.AppendChild(xmlDocEnv);

            if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
                xmlDocEnv.InnerXml = xmlOut.OuterXml;

            // Return output envelope
            return xmlOutEnv;

        }
        private void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors)
        {
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;

            // Create error root element (MsgStatus)
            // JP TMP 01.18.2006   XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus", XML_NAMESPACE);
            XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);

            // Add overall result status
            // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("MsgStatusCd", XML_NAMESPACE);
            xmlElement = xmlDoc.CreateElement("MsgStatusCd");
            if (bCallResult)
                xmlElement.InnerText = "Success";
            else
                xmlElement.InnerText = "Error";

            xmlErrRoot.AppendChild(xmlElement);

            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
                    // JP TMP 01.18.2006   xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus", XML_NAMESPACE);
                    xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                    xmlErrRoot.AppendChild(xmlMsgsRoot);

                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // ... add error code/number
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.ErrorCode;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        xmlElement.InnerText = err.ErrorDescription;
                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        // Determine error code - assembly + exception type
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = String.Format("{0}.{1}", err.oException.Source, err.oException.GetType().Name);
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        if (!string.IsNullOrEmpty(err.ErrorDescription))
                            xmlElement.InnerText = err.ErrorDescription;
                        else
                            xmlElement.InnerText = err.oException.Message;

                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // Exception case

                    // ...add error type
                    // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedMsgType", XML_NAMESPACE);
                    xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            xmlElement.InnerText = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            xmlElement.InnerText = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            xmlElement.InnerText = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            xmlElement.InnerText = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            xmlElement.InnerText = "PopupMessage";
                            break;
                        default:
                            // TODO   - What to do if not a standard error code?
                            break;
                    };
                    xmlMsgsRoot.AppendChild(xmlElement);
                }
            }
        }

        public string ProcessRequest(string xmlRequest)
        {
            XmlDocument xmlDocRequest = new XmlDocument();
            XmlDocument xmlDocResponse = new XmlDocument();
            XmlDocument xmlDocDiaryResp = new XmlDocument();
            XmlNode objResp = null;
            XmlNode objStatus = null;
            string functionName = string.Empty;
            bool bResult = false;
            string xmlDocOut = string.Empty;
            string ret = string.Empty;
            XmlNode multiplecalls;
            MobileCommonWCFService objCommonService = null;

            //Load the specified XML
            xmlDocRequest.LoadXml(xmlRequest);
            multiplecalls = xmlDocRequest.SelectSingleNode("//MultipleCalls");
            if (multiplecalls != null && multiplecalls.InnerText == "true")
            {
                objCommonService = new MobileCommonWCFService();
                UserLogin oUserLogin = null;// Mobile Apps
                MobileUploadAdaptor objMobileAdaptor = new MobileUploadAdaptor();
                BusinessAdaptorErrors errOut = new BusinessAdaptorErrors(oUserLogin,0);
                BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(0);
                string oMessageElement = string.Empty;
                string sNoteText = string.Empty;
                string sTaskName = string.Empty;
                XmlNode objResult = null;

                XmlDocument xmlDocError = new XmlDocument();
                XmlNode objResponse = null;
                XmlNode objResponseParent = null;
                XmlElement objResponsenode = null;
                XmlElement resultmessage = null;
                XmlElement objResponsenodechild = null;

                //here we need to call the progress notes and the document upload services....
                // Ijha: Null check for Notes
                if (!string.IsNullOrEmpty(xmlDocRequest.SelectSingleNode("//Message//Document//ProgressNotes//NOTE_TEXT").InnerText))
                {
                    MobileProgressNoteService progservice = new MobileProgressNoteService();
                    ret = progservice.SaveNotesForMobileAdj(xmlRequest);
                }
                //document upload service
                // Ijha: Null check for File Upload               
                if (!string.IsNullOrEmpty(xmlDocRequest.SelectSingleNode("//Message//Document//Documents//DocumentUpload").Attributes["filename"].Value.ToString()))
                {
                    MobileDocumentService docservice = new MobileDocumentService();
                    //docservice.CreateDocTypeFromXml_MobAdj(xmlRequest);
                    docservice.MobileDocumentUploadRequest(xmlRequest); //Amandeep Mobile Adjuster
                }

                // Ijha: Check the status of the Diary before perfoming Update Diary 
                InitiateServiceProcess(xmlDocRequest, functionName, objMobileAdaptor, out oUserLogin, ref systemErrors);
                bResult = objMobileAdaptor.GetStatus(xmlDocRequest, ref xmlDocDiaryResp, ref errOut);
                xmlDocResponse = formatOutputXML(xmlDocDiaryResp, bResult, errOut);
                objResp = xmlDocResponse.SelectSingleNode("//MsgStatusCd");
                if (objResp != null && objResp.InnerText == "Success")
                {
                    objStatus = xmlDocResponse.SelectSingleNode("//Status");
                    if (objStatus != null && objStatus.InnerText == "Open")
                    {
                        xmlDocResponse = objCommonService.ProcessRequest(xmlDocRequest);
                    }
                    else if (objStatus != null && objStatus.InnerText == "Closed")
                    {
                        // If the diary is closed, generate a Notes with attachment information and Diary details
                        sNoteText = "";
                        sTaskName = xmlDocRequest.SelectSingleNode("//Message/Document/SaveDiary/TaskSubject").InnerText;
                        XmlNodeList XmlList = xmlDocRequest.SelectNodes("//DocumentUpload");
                        foreach (XmlNode xn in XmlList)
                        {
                            sNoteText = sNoteText + " " + "with filename  " + xn.Attributes["filename"].Value + " and title " + xn.Attributes["title"].Value + ",";
                        }
                        if (sNoteText.EndsWith(","))
                        {
                            sNoteText = sNoteText.Substring(0, sNoteText.Length - 1);
                        }
                        //rsharma220 MITS 35387                      
                        // oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);
                        if (xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER") != null)
                        {
                            oMessageElement = GetNotesTemplate().ToString();
                            oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);

                        }
                        else if (xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/EVENT_NUMBER") != null)  //rsharma220: Added for mobile changes
                        {
                            oMessageElement = GetNotesTemplate_Event().ToString();
                            oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/EVENT_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/EVENT_NUMBER").InnerText);

                        }
                        oMessageElement = GetNotesTemplate().ToString();
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", xmlDocRequest.SelectSingleNode("//Authorization").InnerText);
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/ACTIVITY_DATE", System.DateTime.Now.ToShortDateString());
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/NOTE_TEXT", "The Diary with the Task Name '" + sTaskName + " having attachment description as " + sNoteText + "' has already been updated in Riskmaster.");

                    }

                    else if (objStatus != null && objStatus.InnerText == "Deleted")
                    {
                        // If the diary is deleted, generate a Notes with attachment information and Diary details
                        sNoteText = "";
                        sTaskName = xmlDocRequest.SelectSingleNode("//Message/Document/SaveDiary/TaskSubject").InnerText;
                        XmlNodeList XmlList = xmlDocRequest.SelectNodes("//DocumentUpload");
                        foreach (XmlNode xn in XmlList)
                        {
                            sNoteText = sNoteText + " " + "with filename " + xn.Attributes["filename"].Value + " and title " + xn.Attributes["title"].Value + ",";
                        }
                        if (sNoteText.EndsWith(","))
                        {
                            sNoteText = sNoteText.Substring(0, sNoteText.Length - 1);
                        }
                        //rsharma220 MITS 35387
                        // oMessageElement = GetNotesTemplate().ToString();
                        //  oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", xmlDocRequest.SelectSingleNode("//Authorization").InnerText);
                        // oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);
                        if (xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER") != null)
                        {
                            oMessageElement = GetNotesTemplate().ToString();
                            oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/CLAIM_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/CLAIM_NUMBER").InnerText);

                        }
                        else if (xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/EVENT_NUMBER") != null)  //rsharma220: Added for mobile changes
                        {
                            oMessageElement = GetNotesTemplate_Event().ToString();
                            oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/EVENT_NUMBER", xmlDocRequest.SelectSingleNode("//Message/Document/ProgressNotes/EVENT_NUMBER").InnerText);

                        }
                        oMessageElement = GetNotesTemplate().ToString();
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Authorization", xmlDocRequest.SelectSingleNode("//Authorization").InnerText);
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/ACTIVITY_DATE", System.DateTime.Now.ToShortDateString());
                        oMessageElement = ChangeNotesTemplate(oMessageElement, "//Message/Document/ProgressNotes/NOTE_TEXT", "The Diary with the Task Name '" + sTaskName + " having attachment description as " + sNoteText + "' does not exist in Riskmaster.");

                    }

                    if (objStatus != null && objStatus.InnerText == "Deleted" || objStatus.InnerText == "Closed")
                    {
                        MobileProgressNoteService objProgressService = new MobileProgressNoteService();
                        xmlDocOut = objProgressService.SaveNotesForMobileAdj(oMessageElement.ToString());
                        xmlDocResponse.LoadXml(xmlDocOut);

                        // Ijha : Mobile apps - Response XMl for Diary Update Failure
                        objResult = xmlDocResponse.DocumentElement;
                        resultmessage = xmlDocError.CreateElement("ResultMessage");
                        xmlDocError.AppendChild(resultmessage);
                        objResponse = xmlDocError.SelectSingleNode("ResultMessage");
                        objResponsenode = xmlDocError.CreateElement("MsgStatus");
                        objResponse.AppendChild(objResponsenode);
                        objResponsenode = null;
                        objResponse = xmlDocError.SelectSingleNode("ResultMessage//MsgStatus");
                        objResponsenode = xmlDocError.CreateElement("ProgressNotes");
                        objResponse.AppendChild(objResponsenode);
                        foreach (XmlNode e in objResult.ChildNodes)
                        {
                            objResponsenodechild = xmlDocError.CreateElement(e.Name);
                            objResponseParent = xmlDocError.SelectSingleNode("ResultMessage//MsgStatus//ProgressNotes");
                            objResponsenodechild.InnerText = e.InnerText;
                            objResponseParent.AppendChild(objResponsenodechild);

                        }
                        objResponsenode = null;
                        objResponsenode = xmlDocError.CreateElement("MsgStatusCd");
                        objResponsenode.InnerText = "failed";
                        objResponse.AppendChild(objResponsenode);
                        xmlDocResponse.LoadXml(xmlDocError.OuterXml);
                    }               
                }
            }
            else
            {
                objCommonService = new MobileCommonWCFService();   //Aman MITS 31015
                xmlDocResponse = objCommonService.ProcessRequest(xmlDocRequest);
            }
            //return the resultant xml output
            return xmlDocResponse.OuterXml;
        }
    }
}
