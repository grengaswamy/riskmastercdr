﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICodesService" here, you must also update the reference to "ICodesService" in Web.config.
    [ServiceContract]
    [XmlSerializerFormat]
    public interface ICodesService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetCodes(CodeTypeRequest request, out CodeListType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetQuickLookUp(QuickLookupRequest request, out CodeListType objReturn);
        
    }
}
