﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;


namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PVUpgrade : RMService,IPVUpgrade
    {
        public PVUpgradeData UpgradeXmlTagToAspxForm(PVUpgradeData request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "UpgradeXmlTagToAspxForm";
            UserLogin oUserLogin = null;
            PVUpgradeData objReturn = null;
            PVUpgradeAdaptor objPVUpgradeAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //convertedAspx = "";
                objPVUpgradeAdaptor = new PVUpgradeAdaptor();
                objReturn = new PVUpgradeData();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objPVUpgradeAdaptor, out oUserLogin, ref systemErrors);
            
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                bResult = objPVUpgradeAdaptor.UpgradeXmlTagToAspxForm(request, out objReturn, ref errOut);//request, out  objReturn,
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public PVUpgradeData FetchJurisdictionalData(PVUpgradeData request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "FetchJurisdictionalData";
            UserLogin oUserLogin = null;
            PVUpgradeAdaptor objPVUpgradeAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            PVUpgradeData objReturn = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                //jurisdictionalAspx = "";
                objPVUpgradeAdaptor = new PVUpgradeAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objPVUpgradeAdaptor, out oUserLogin, ref systemErrors);

                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                bResult = objPVUpgradeAdaptor.FetchJurisdictionalData(request, out objReturn, ref errOut);//request, out  objReturn,
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            //logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
    }
}
