﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
 //Added by nsharma202 for mobile app
using System.ServiceModel.Web;
using System.IO;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IAuthenticationService" here, you must also update the reference to "IAuthenticationService" in Web.config.
    [ServiceContract]
    public interface IMobileAuthenticationService
    {

         //<summary>
         //Authenticates the user using the Default RISKMASTER Membership Provider
         //</summary>
         //<param name="strUserName">xml containing the user's login name</param>
         //<param name="strUserName">xml containing the user's login password</param>        
         //<param name="strMessage">string containing the exception message if any</param>
         //<returns>boolean indicating whether or not the user has been successfully authenticated</returns>
        
        [OperationContract]
        string AuthenticateMobileUser(string xmlRequest);

         //<summary>
         //Retrieves the list of DSNs for a specific user for Mobile Adjuster
         //</summary>
         //<param name="strUserName">string containing the logged in user name</param>
         //<returns>Generic String Dictionary collection containing all of the User DSNs</returns>        
        
        [OperationContract]
        string GetUserDSNsMobileAdjuster(string xmlRequest);
    }
}
