﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMobile" in both code and config file together.
    [ServiceContract]
    public interface IMobile
    {
       
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Claims", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Claims();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Claims/{claimNumber}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Claim(string claimNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Events", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Events();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Tasks", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Tasks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Tasks/{lastSyncDateTime}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream SyncTasks(String lastSyncDateTime);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Tasks", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream CreateTask(Stream task);
               
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Notes", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Notes(Stream note);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Codes", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Codes();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Attachment", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool Attachment(Stream attachment);
        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Authenticate", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool Authenticate(Stream jsonStream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetUserDSNs", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetUserDSNs(Stream jsonStream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetUserSessionID", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string GetUserSessionID(Stream jsonStream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/LogOut", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        void LogOut();
       
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ChangePassword", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool ChangePassword(Stream jsonStream);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetUserName", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string GetUsername(Stream authData);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetReportees", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream GetReportees();
    }
}
