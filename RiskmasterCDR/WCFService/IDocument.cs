﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDocumentService" here, you must also update the reference to "IDocumentService" in Web.config.
    [ServiceContract]
    public interface IDocument
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/copy", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool CopyDocuments(TransferDocumentsRequest oTransferDocumentsRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/create", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        RMServiceType CreateDocument(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/folder", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool CreateFolder(FolderTypeRequest oFolderTypeRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "DELETE", UriTemplate = "/remove", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool Delete(DocumentsDeleteRequest oDocumentsDeleteRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/email", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool EmailDocuments(EmailDocumentsRequest oEmailDocumentsRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/attachments", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentList GetAttachments(DocumentListRequest oDocumentListRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/fetch", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentType GetDocument(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/list", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentList GetDocumentsList(DocumentListRequest oDocumentListRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/folderlist", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        System.Collections.Generic.List<Folder> GetFolders(Folder oFolder);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/users", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UsersList GetUsers(DocumentUserRequest oDocumentUserRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/move", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool MoveDocuments(MoveDocumentsRequest oMoveDocumentsRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/retrieve", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentType RetrieveDocument(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/transfer", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool TransferDocuments(TransferDocumentsRequest oTransferDocumentsRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "PUT", UriTemplate = "/edit", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateDocument(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "PUT", UriTemplate = "/file", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentType UpdateDocumentFile(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/createdetail", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentType CreateDocumentDetail(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/execsummary", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentType GetExecutiveSummaryFileDetails(DocumentType oDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/outlooksetting", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool CheckOutlookSetting(DocumentType oDocumentType);
        //rbhatia4:R8: Use Media View Setting : July 08 2011
        //Restructuring Document Management Code for choosing the selected vendor
        /*
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string GetPaperVisionSetting(DocumentType objDocReq);
         */
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/vendordetails", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DocumentVendorDetails GetDocumentManagementVendorDetails(DocumentVendorDetails oDocumentVendorDetails);
        
    }

}
