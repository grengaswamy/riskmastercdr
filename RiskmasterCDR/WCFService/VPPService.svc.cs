﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Riskmaster.Models;
using Riskmaster.BusinessAdaptor.VPPAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;

namespace RiskmasterService
{
    // NOTE: If you change the class name "VPPService" here, you must also update the reference to "VPPService" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class VPPService : RMService, IVPPService
    {
        /// <summary>
        /// retrieve aspx page from the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void GetFileContent(VPPFileRequest request, out VPPFileResponse response)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetFileContent";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(request.ClientId);
                bResult = objVPPBusinessAdaptor.GetFileContent(request, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// get the max time stamp for all the pages in the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void GetInitialTimeStamp(RMServiceType request, out VPPTimeStampResponse response)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetInitialTimeStamp";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(request.ClientId);
                bResult = objVPPBusinessAdaptor.GetInitialTimeStamp(request, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// get the time stamp for a specific file in the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void GetFileTimeStamp(VPPFileRequest request, out VPPTimeStampResponse response)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetFileTimeStamp";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(request.ClientId);
                bResult = objVPPBusinessAdaptor.GetFileTimeStamp(request, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// get the list of pages which have been modified during last pooling cycle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void GetModifiedPageList(VPPModifiedPagesRequest request, out VPPModifiedPagesResponse response)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetModifiedPageList";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(request.ClientId);
                bResult = objVPPBusinessAdaptor.GetModifiedPageList(request, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// get the data source id for this session
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void GetDataSourceID(RMServiceType request, out VPPGetDataSourceIDResponse response)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetDataSourceID";
            UserLogin oUserLogin = null;
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            response = new VPPGetDataSourceIDResponse();

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                // objReturn = new MDINavigationTreeNodeProfileResponse();
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(request.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objVPPBusinessAdaptor, out oUserLogin, ref systemErrors);

                if (oUserLogin != null)
                    response.DataSourceID = oUserLogin.DatabaseId;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
    }
}
