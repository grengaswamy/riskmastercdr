﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using Riskmaster.Models;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Application.ReserveWorksheet;

namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    
    // NOTE: If you change the class name "ReserveWorksheetService" here, you must also update the reference to "ReserveWorksheetService" in Web.config.
    public class ReserveWorksheetService : RMService, IReserveWorksheetService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse GetReserveWorkSheet(ReserveWorksheetRequest request)
        {
            string sRswType = request.RSWType;
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetReserveWorsheet";
       
            ReserveWorksheetResponse objResponse = null;
            //ReserveWorksheetAdaptor objReserveWorkSheetAdaptor = null;
            
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReserveWorkSheetAdaptor = new ReserveWorksheetAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName,out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))  //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");
                    objGenResWS.GetReserveWorksheet(request, ref objResponse, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");
                    objGenResWS.GetReserveWorksheet(request, ref objResponse, oUserLogin);
                }

                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
     public ReserveWorksheetResponse OnLoadInformation(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "OnLoadInformation";
            
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                
                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName,out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objResponse = new ReserveWorksheetResponse();
                objGenResWS.OnLoadInformation(request, ref objResponse,oUserLogin.GroupId,oUserLogin);
                return objResponse;
                //return objReserveWorkSheetAdaptor.OnLoadInformation(request, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse SaveReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            string sRswType = request.RSWType;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "SaveReserveWorkSheet";
        //    string rswtype = request.RSWType;
           
            ReserveWorksheetResponse objResponse = null;
            
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                
            // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))    //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                   // objGenResWS.SaveData(request, ref objResponse);
                    objGenResWS.SaveDataWithUserLogin(request, ref objResponse, oUserLogin);  //aaggarwal29 MITS 27763 -- code changed to allow checking of user permissions while saving data.
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    //objGenResWS.SaveData(request, ref objResponse);
                    objGenResWS.SaveDataWithUserLogin(request, ref objResponse, oUserLogin);  //aaggarwal29 MITS 27763 -- code changed to allow checking of user permissions while saving data.
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse SubmitReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "SubmitReserveWorkSheet";
            string sRswType = request.RSWType;
        
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.SubmitWorksheet(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.SubmitWorksheet(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                
                }
                return objResponse;
                //return objReserveWorkSheetAdaptor.OnLoadInformation(request, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }


        }

        /// <summary>
        ///  MGaba2:R6 :Approval of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ApproveReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            string sRswType = request.RSWType;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ApproveReserveWorkSheet";

         
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                   
                    objGenResWS.ApproveReserveWorksheet(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.ApproveReserveWorksheet(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                
                
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        
        /// <summary>
        ///  MGaba2:R6 :Approval of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse RejectReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RejectReserveWorkSheet";
            string sRswType = request.RSWType;
           
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                   
                    objGenResWS.RejectWorksheet(request, ref objResponse, oUserLogin.GroupId);
                }
                else if (sRswType == "Customize")
                {

                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.RejectWorksheet(request, ref objResponse, oUserLogin.GroupId);
                
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  MGaba2:R6 :Printing of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse PrintReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "PrintWorksheet";
            string sRswType = request.RSWType;
         
            ReserveWorksheetResponse objResponse = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                //MGaba2: MITS 22225:Print functionality was not working from Reserve Approval Screen
                //request.RSWType is sent as blank from Reserve Approval Screen
                if (sRswType == "")
                {
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                    sRswType = objGenResWS.GetRswTypeToLaunch(request.ClaimId);
                }

                if (sRswType == "Generic")
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                    objGenResWS.PrintWorksheet(request, ref objResponse);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");

                    objCusResWS.PrintWorksheet(request, ref objResponse);
                
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse DeleteReserveWorkSheet(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "DeleteReserveWorkSheet";
            string sRswType = request.RSWType;
       
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.DeleteWorksheet(request, ref objResponse);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.DeleteWorksheet(request, ref objResponse);
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }


        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ListFundsTransactions(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ListFundsTransactions";
            string sRswType = request.RSWType;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objResponse = new ReserveWorksheetResponse();
                objGenResWS.ListFundsTransactions(request, ref objResponse);
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ApproveReserveWorksheetFromApprovalScreen";
            string sRswType = string.Empty;
          
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");
                
                sRswType = objGenResWS.GetRswTypeToLaunch(request.ClaimId);

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                  //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.ApproveReserveWorksheetFromApprovalScreen(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");


                    objCusResWS.ApproveReserveWorksheetFromApprovalScreen(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }

            
             
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest request)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RejectReserveWorksheetFromApprovalScreen";
            string sRswType = string.Empty;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                sRswType = objGenResWS.GetRswTypeToLaunch(request.ClaimId);

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.RejectReserveWorksheetFromApprovalScreen(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,0);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");


                    objCusResWS.RejectReserveWorksheetFromApprovalScreen(request, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }

            
               
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }
    }
}
