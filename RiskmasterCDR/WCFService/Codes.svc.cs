﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;

namespace RiskmasterService
{
    // NOTE: If you change the class name "Codes" here, you must also update the reference to "Codes" in Web.config.
    [AspNetCompatibilityRequirements(
         RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class Codes : RMService, ICodes
    {
        /// <summary>
        /// Get QuickLookUp List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CodeListType GetQuickLookUp(QuickLookupRequest  oQuickLookupRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetQuickLookUp";
            UserLogin oUserLogin = null;
            CodesListAdaptor objCodes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            CodeListType objReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oQuickLookupRequest.ClientId);
                objReturn = new CodeListType();
                objCodes = new CodesListAdaptor();
                InitiateServiceProcess(oQuickLookupRequest, out xmlRequest, functionName, objCodes, out oUserLogin, ref  systemErrors);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                errOut = new BusinessAdaptorErrors(oUserLogin, oQuickLookupRequest.ClientId);
                bResult = objCodes.QuickLookupData(oQuickLookupRequest, ref  objReturn, ref errOut);
            }
            catch (Exception e) 
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        /// <summary>
        /// Get Codes List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CodeListType GetCodes(CodeTypeRequest oCodeTypeRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetCodes"; 
            UserLogin oUserLogin = null;
            CodesListAdaptor objCodes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            CodeListType objReturn=null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oCodeTypeRequest.ClientId);
                objReturn = new CodeListType();
                objCodes = new CodesListAdaptor();
                InitiateServiceProcess(oCodeTypeRequest, out xmlRequest, functionName, objCodes, out oUserLogin, ref  systemErrors);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                errOut = new BusinessAdaptorErrors(oUserLogin, oCodeTypeRequest.ClientId);
                bResult = objCodes.GetCodesNew(oCodeTypeRequest, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
    }
}
