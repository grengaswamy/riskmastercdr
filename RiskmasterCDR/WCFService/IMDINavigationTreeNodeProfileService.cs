﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{

    [ServiceContract]
    public interface IMDINavigationTreeNodeProfileService
    {
        [OperationContract]
        MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileData(MDINavigationTreeNodeProfileRequest request);

        [OperationContract]
        MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileDataByParent(MDINavigationTreeNodeProfileRequestByParent request);

        [OperationContract]
        InitMDIResponse InitMDI(InitMDIRequest request);
        //nkaranam2 - 34408
        [OperationContract]
        GetSysSettingsResponse GetSysSettings(GetSysSettingsRequest request);
    }
   
}
