﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using Riskmaster.Common;
using System.IO;
using System.ServiceModel.Web;
using System.Text;


namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "Diary" here, you must also update the reference to "Diary" in Gloabl.asax.
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class Diary : RMService, IDiary
    {
        #region Diary Calendar functions
        /// <summary>
        /// Gets the diaries
        /// </summary>
        /// <param name="oDiaryInput">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetDiaryList(DiaryInput oDiaryInput)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDiaries = null;
            DiaryOutput DiaryOutputObject = new DiaryOutput();

            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetDiaryList";

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDiaryInput.ClientId);
                objDiaries = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDiaryInput, out xmlRequest, functionName, objDiaries, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                inputGetDiaryDoc.LoadXml(oDiaryInput.GetDiaryInput);

                objDiaries.GetDairiesForDairyCalender(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);
                

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }
        /// <summary>
        /// Gets the Calendar view
        /// </summary>
        /// <param name="oDiaryInput">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetCalendarView(DiaryInput oDiaryInput)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDairies = null;
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetCalendarView";
            DiaryOutput DiaryOutputObject = new DiaryOutput(); 
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDiaryInput.ClientId);
                objDairies = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDiaryInput, out xmlRequest, functionName, objDairies, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                //inputGetDairyDoc.LoadXml(oDiaryInput.GetDairyInput);

                objDairies.GetCalendarView(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }
        /// <summary>
        /// Gets the information at the time of Diary Calendar load
        /// </summary>
        /// <param name="oDiaryInput">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetOnLoadInformation(DiaryInput oDiaryInput)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDiaries = null;
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetOnLoadInformation";
            DiaryOutput DiaryOutputObject = new DiaryOutput();
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDiaryInput.ClientId);
                objDiaries = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDiaryInput, out xmlRequest, functionName, objDiaries, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                //inputGetDairyDoc.LoadXml(oDiaryInput.GetDairyInput);

                objDiaries.GetOnLoadInformation(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }

        #endregion
        #region Dairy functions
        //Get Reportees Under current user
        public Stream GetReportees()
        {
            int clientId = int.Parse(System.Web.HttpContext.Current.Request.Headers["ClientId"].ToString());
            string session = System.Web.HttpContext.Current.Request.Headers["session"];
            session = session.Replace("\"", "");

            SessionManager oSession = SessionManager.LoadSession(session, clientId);
            UserLogin auth = new UserLogin(clientId);
            auth = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));

            UserLogin user = new UserLogin(auth.LoginName, auth.Password, auth.objRiskmasterDatabase.DataSourceName, clientId);

            WPAAdaptor diaryAdapter = new WPAAdaptor();
            diaryAdapter.SetSecurityInfo(user, clientId);
            diaryAdapter.ClientId = clientId;
            string peekList = string.Empty;
            BusinessAdaptorErrors error = new BusinessAdaptorErrors(clientId);

            String response = string.Empty;
            try
            {
                if (diaryAdapter.GetAvailablePeekList(ref peekList, ref error))
                {
                    response = peekList;
                }
                else
                {
                    response = "booo";
                }
            }
            catch (Exception e)
            {
                Log.Write(e.ToString(), clientId);
                response = e.ToString();
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            return memoryStream;
        }

        //nsharma202 : RMACLOUD-2960
        /// <summary>
        /// Gets Diaires as DiaryList Object
        /// </summary>
        /// <param name="oDiaryInput"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public DiaryList GetDiaries(Riskmaster.Models.DiaryList oDiaryInput,String user)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDiaries = null;
            DiaryOutput DiaryOutputObject = new DiaryOutput();

            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetDiaryList";
            DiaryList output = null;
            
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDiaryInput.ClientId);
                objDiaries = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDiaryInput, out xmlRequest, functionName, objDiaries, out oUserLogin, ref  systemErrors);
                output = new DiaryList();
                objDiaries.DiaryList(user, oDiaryInput.lastSyncDateTime,ref output, ref systemErrors);
                                
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return output;
        }
        #endregion
    }
}
