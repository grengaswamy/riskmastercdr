﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Models;
using RiskmasterService.MDAGuidelinesService;
using Riskmaster.Security; //rkaur27

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MDAservices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MDAservices.svc or MDAservices.svc.cs at the Solution Explorer and start debugging.
    //public class MDAservices : IMDAservices
    //{
    //    public void DoWork()
    //    {
    //    }
    //}


    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class MDAservices : RMService, IMDAservices
    {

        public MDATopic GetMDATopics(MDATopic oMDATopic)
        {
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetMDATopics";
            XmlDocument xmlRequest = null;
            string license = string.Empty;
            // npadhy For the Servers where the Requests are routed though a Proxy, we might need to provide the 
            // Network credentials User Name, Password and Domain
            string sProxyAddress = string.Empty;
            string sProxyPort = string.Empty;
            string sNCUserName = string.Empty;
            string sNCPassword = string.Empty;
            string sNCDomain = string.Empty;
            string sRedirectURL = string.Empty;
            string sReturn = string.Empty;
            // zmohammad Added for RMA-14913 Starts
            string[] icdType = new string[2];
            // zmohammad Added for RMA-14913 Ends
            mdguidelinesWebServiceSoapClient client = null;
            MDATopic objMdaTopic = null;
            XmlNode xmlReturn = null;
            UserLogin oUserLogin = null; //rkaur27

            // Start: rkaur27, Authenticate MDAService request to load userlogin and get connection string
            try
            {
                systemErrors = new BusinessAdaptorErrors(oMDATopic.ClientId);
                InitiateServiceProcess(oMDATopic, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // End rkaur27, Authenticate MDAService request
            try
            {

                systemErrors = new BusinessAdaptorErrors(oUserLogin, oMDATopic.ClientId);
                //NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("MDASettings");
                NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("MDASettings", oUserLogin.objRiskmasterDatabase.ConnectionString, oMDATopic.ClientId);
                license = nvCollSettings["MDALicenseKey"];
                sProxyAddress = nvCollSettings["ProxyAddress"];
                sProxyPort = nvCollSettings["ProxyPort"];
                sNCUserName = nvCollSettings["UserName"];
                sNCPassword = nvCollSettings["Password"];
                sNCDomain = nvCollSettings["Domain"];
                sRedirectURL = nvCollSettings["RedirectURL"];

                ServicePointManager.Expect100Continue = false;
                objMdaTopic = new MDATopic();
                if (!string.IsNullOrEmpty(sProxyAddress))
                {
                    if (!string.IsNullOrEmpty(sProxyPort))
                        sProxyAddress += String.Format(":{0}", sProxyPort);
                    WebProxy wproxy = new WebProxy(sProxyAddress, true);
                    if (!string.IsNullOrEmpty(sNCDomain))
                        wproxy.Credentials = new NetworkCredential(sNCUserName, sNCPassword, sNCDomain);
                    else
                        wproxy.Credentials = new NetworkCredential(sNCUserName, sNCPassword);
                    //or you can construct your own credentials via System.Net.NetworkCredential
                    WebRequest.DefaultWebProxy = wproxy;
                }

                // zmohammad Added for RMA-14913 Starts
                icdType = oMDATopic.Medicalcode.Split('~');
                // zmohammad Added for RMA-14913 Ends

                client = new mdguidelinesWebServiceSoapClient();
                
                // zmohammad Changed for RMA-14913 Starts
                //xmlReturn = client.DisabilityDurationByMedicalCodeRequest(license, oMDATopic.Medicalcode, "icd9", oMDATopic.Jobclass, "");
                xmlReturn = client.DisabilityDurationByMedicalCodeRequest(license, icdType[0], icdType[1], oMDATopic.Jobclass, "");
                // zmohammad Changed for RMA-14913 Ends

                if (xmlReturn != null)
                {
                    sReturn = xmlReturn.OuterXml;

                    //Check if the any valid URL has been retured or not. If not, redirect to default search page
                    if (xmlReturn.SelectSingleNode("//RelatedMonographUrls/RelatedMonographUrl/MonographUrl") == null)
                    {
                        XElement oReturn = XElement.Parse(sReturn);
                        sRedirectURL = sRedirectURL.Replace("[ICD9_code]",oMDATopic.Medicalcode).Replace("[MDALicenseKey]", license);
                        XElement oRedirectUrl = new XElement("RelatedMonographUrls",
                                                    new XElement("RelatedMonographUrl",
                                                        new XElement("MonographUrl", sRedirectURL)));
                        oReturn.Add(oRedirectUrl);
                        sReturn = oReturn.ToString();

                        
                    }
                    objMdaTopic.ReturnValue = sReturn; //mbahl3 
                }

            }
            catch (Exception e)
            {
                //Creating a request xml only when there is error bcoz it is needed to log the request
                XElement MDATopics = new XElement("MDATopics",
                                        new XElement("medicalcode", oMDATopic.Medicalcode),
                                        new XElement("jobclass", oMDATopic.Jobclass));

                xmlRequest = new XmlDocument();
                xmlRequest.LoadXml(MDATopics.ToString());
                //Creating a request xml only when there is error
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                client.Close();
                systemErrors = null;
            }

            return objMdaTopic;
        }
    }
}
