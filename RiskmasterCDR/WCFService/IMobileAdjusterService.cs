﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;
 //Added by nsharma202 for mobile app
using System.Web;
using System.ServiceModel.Web;
using System.IO;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAdjusterService" in both code and config file together.
    [ServiceContract]
    public interface IMobileAdjusterService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DoWork();

        [OperationContract]
        [FaultContract(typeof(RMException))]
        string AdjusterRowId(string xmlRequest);

	   

    }
}
