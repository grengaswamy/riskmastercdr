﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    [ServiceContract]
    public interface IRMXResource
    {
        /// <summary>
        /// GetResourceByLanguageAndKey
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/resbylangcodeandkey", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetResourceByLanguageAndKey(RMResource oRMResource);
        /// <summary>
        /// GetAllResourceByLanguageAndPageId
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/allresbylangcodeandpageid", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetAllResourceByLanguageAndPageId(RMResource oRMResource);
        /// <summary>
        /// GetGlobalResourcesByLanguage
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/globalresbylangcode", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetGlobalResourcesByLanguage(RMResource oRMResource);
        /// <summary>
        /// GetConnectionstring
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/connkey", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetConnectionstring(RMResource oRMResource);
        /// <summary>
        /// GetConnectionstringFromDSNId
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/connstringbydsnid", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetConnectionstringFromDSNId(RMResource oRMResource);
        /// <summary>
        /// GetAllResourceByLanguageAndPageIdAndResType
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/allresbylangcodepageandtype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetAllResourceByLanguageAndPageIdAndResType(RMResource oRMResource);
        /// <summary>
        /// GetTimestampForMDIMenu
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/timestampofmdi", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetTimestampForMDIMenu(RMResource oRMResource);
        /// <summary>
        /// GetMDIMenuXML
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/mdi", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetMDIMenuXML(RMResource oRMResource);
        /// <summary>
        /// GetChangedMDIMenuList
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/mdilist", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        RMResource GetChangedMDIMenuList(RMResource oRMResource);
        /// <summary>
        /// GetPageInfo
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/pageinfo", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetPageInfo(RMResource oRMResource);
        /// <summary>
        /// SaveResource 
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/edit", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string SaveResource(RMResource oRMResource);
        /// <summary>
        /// Is Error Code Enabled
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/showerror", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ShowErrorCode(); 
        /// <summary>
        /// GetGlobalInfo
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/globalinfo", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetGlobalInfo(RMResource oRMResource);
        /// <summary>
        /// GetFormats
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/formats", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetFormats(RMResource oRMResource);
        /// <summary>
        /// GetSearchInfo
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/search", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetSearchInfo(RMResource oRMResource);

        /// <summary>
        /// GetSearchResult
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/searchresult", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetSearchResult(RMResource oRMResource);

        /// <summary>
        /// ClearGlobalErrorMessageCache
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/clear", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void ClearGlobalErrorMessageCache(RMResource oRMResource);

        /// <summary>
        /// GetChildScreenXML
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/childscreen", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetChildScreenXML(RMResource oRMResource);
    }
}
