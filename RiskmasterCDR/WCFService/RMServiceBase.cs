﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Models;
//sanoopsharma - Start MITS 35932 
using System.Security.Cryptography.X509Certificates;
using System.Net;
//sanoopsharma - End MITS 35932 


namespace RiskmasterService
{
    /// <summary>
    /// Base class for all FDM (Form Data Manager) forms used in RMX
    /// </summary>
    public class RMService
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        /// 

        private static string m_SessionDSN = null;
        public RMService()
        {
        }

        #region Primary Service initialization method
        /// <summary>
        /// This method initialize the Common tasks to be done for each service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="functionName"></param>
        /// <param name="adaptor"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        protected bool InitiateServiceProcess(RMServiceType request, out XmlDocument xmlRequest, string functionName, BusinessAdaptorBase adaptor,
            out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {
            return InitiateServiceProcess(request, out xmlRequest, functionName, adaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.XmlSerializer);
        } 
        #endregion

        #region Data Integrator Service Initialization method
        /// <summary>
        /// OVERLOADED: InitiateServiceProcess method specifically for Data Integrator
        /// </summary>
        /// <param name="request"></param>
        /// <param name="adaptor"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        [Obsolete]
        protected bool InitiateServiceProcess(RMServiceType request, BusinessAdaptorBase adaptor,
                                out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {
            string sessionKeySupplied = request.Token;

            SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied, request.ClientId); //check
            if (oSession == null) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", request.ClientId));
            }
            else //Session Exists - deserialize UserLogin Object
            {
                oUserLogin = new UserLogin(request.ClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));
                systemErrors = new BusinessAdaptorErrors(oUserLogin,request.ClientId); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null)
                    adaptor.SetSecurityInfo(oUserLogin, request.ClientId);

                adaptor.SetSessionObject(oSession);
            }
            
            return true;
        }

        protected bool InitiateServiceProcess(RMServiceType request, out XmlDocument xmlRequest, string functionName, BusinessAdaptorBase adaptor,
            out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors, Utilities.SerializationType currentSerialization)
        {
            string sessionKeySupplied = request.Token;
            xmlRequest = new XmlDocument();

            if (currentSerialization == Utilities.SerializationType.XmlSerializer)
            {
                xmlRequest.LoadXml(Utilities.XmlSerialize(request, Utilities.SerializationType.XmlSerializer));
            }
            else if (currentSerialization == Utilities.SerializationType.DataContractSerializer)
            {
                xmlRequest.LoadXml(Utilities.XmlSerialize(request, Utilities.SerializationType.DataContractSerializer));
            }

            SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied, request.ClientId);
            if (oSession == null) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", request.ClientId));
            }
            else //Session Exists - unpack UserLogin Object
            {
                oUserLogin = new UserLogin(request.ClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));
                systemErrors = new BusinessAdaptorErrors(oUserLogin, request.ClientId); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null) //BSB Protect for "bypass security" case.
                    adaptor.SetSecurityInfo(oUserLogin, request.ClientId);

                adaptor.SetSessionObject(oSession);
            }//else

            return true;
        } 
        #endregion

        protected bool InitiateStreamedServiceProcess(RMStreamedServiceType request, out XmlDocument xmlRequest, string functionName, BusinessAdaptorBase adaptor,
           out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {
            string sessionKeySupplied = request.Token;
            xmlRequest = new XmlDocument();

            xmlRequest.LoadXml(Utilities.XmlSerialize(request));

            SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied,request.ClientId);
            if (oSession == null) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", request.ClientId));
            }
            else //Session Exists - unpack UserLogin Object
            {
                oUserLogin = new UserLogin(request.ClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));
                systemErrors = new BusinessAdaptorErrors(oUserLogin,request.ClientId); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null) //BSB Protect for "bypass security" case.
                    adaptor.SetSecurityInfo(oUserLogin,request.ClientId);

                 adaptor.SetSessionObject(oSession);
            }

            return true;
        }

        /// <summary>
        /// Hack for DataStreamingService to still work. Will change later
        /// </summary>
        /// <param name="request"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="functionName"></param>
        /// <param name="adaptor"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        protected bool InitiateStreamedServiceProcess(RMStreamServiceType request, out XmlDocument xmlRequest, string functionName, BusinessAdaptorBase adaptor,
           out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {
            string sessionKeySupplied = request.Token;
            xmlRequest = new XmlDocument();

            xmlRequest.LoadXml(Utilities.XmlSerialize(request));

            SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied, request.ClientId);
            if (oSession == null) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", request.ClientId));
            }
            else //Session Exists - unpack UserLogin Object
            {
                oUserLogin = new UserLogin(request.ClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));
                systemErrors = new BusinessAdaptorErrors(oUserLogin, request.ClientId); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null) //BSB Protect for "bypass security" case.
                    adaptor.SetSecurityInfo(oUserLogin, request.ClientId);

                adaptor.SetSessionObject(oSession);
            }

            return true;
        }

        /// <summary>
        /// This overloaded version of "InitiateServiceProcess" is being used by ReserveWorksheet in order to remove BusinessAdaptors
        /// </summary>
        /// <param name="request"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="functionName"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        protected bool InitiateServiceProcess(RMServiceType request, out XmlDocument xmlRequest, string functionName, 
            out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {
            string sessionKeySupplied = request.Token;
            xmlRequest = new XmlDocument();
        
            xmlRequest.LoadXml(Utilities.XmlSerialize(request));

            SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied,request.ClientId);//check
            if (oSession==null) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", request.ClientId));
            }
            else //Session Exists - unpack UserLogin Object
            {
                oUserLogin = new UserLogin(request.ClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));
                systemErrors = new BusinessAdaptorErrors(oUserLogin,request.ClientId); //Apply Login Info for possible later uncaught execptions.
            }
          
            return true;
        }

        /// <summary>
        /// Formats output XML from the BusinessAdaptor layer
        /// </summary>
        /// <param name="xmlOut"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        protected string formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors)
        {
            // Create output envelope doc and root node (ResultMessage)
            XmlDocument xmlOutEnv = new XmlDocument();
            // JP TMP 01.18.2006   XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage", XML_NAMESPACE);
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);

            // Add errors
            formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors);

            // Add output document
            // JP TMP 01.18.2006   XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document", XML_NAMESPACE);
            XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
            xmlRoot.AppendChild(xmlDocEnv);

            if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
                xmlDocEnv.InnerXml = xmlOut.OuterXml;

            // Return output envelope
            return xmlOutEnv.OuterXml;
        }

        /// <summary>
        /// Formats Error XML from the BusinessAdaptor layer
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlRoot"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        protected void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors)
        {
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;

            // Create error root element (MsgStatus)
            // JP TMP 01.18.2006   XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus", XML_NAMESPACE);
            XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);

            // Add overall result status
            // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("MsgStatusCd", XML_NAMESPACE);
            xmlElement = xmlDoc.CreateElement("MsgStatusCd");
            if (bCallResult)
                xmlElement.InnerText = "Success";
            else
                xmlElement.InnerText = "Error";

            xmlErrRoot.AppendChild(xmlElement);

            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
                    // JP TMP 01.18.2006   xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus", XML_NAMESPACE);
                    xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                    xmlErrRoot.AppendChild(xmlMsgsRoot);

                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // ... add error code/number
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.ErrorCode;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        xmlElement.InnerText = err.ErrorDescription;
                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        // Determine error code - assembly + exception type
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.oException.Source + "." + err.oException.GetType().Name;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        if (err.ErrorDescription != "")
                            xmlElement.InnerText = err.ErrorDescription;
                        else
                            xmlElement.InnerText = err.oException.Message;

                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // Exception case

                    // ...add error type
                    // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedMsgType", XML_NAMESPACE);
                    xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            xmlElement.InnerText = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            xmlElement.InnerText = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            xmlElement.InnerText = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            xmlElement.InnerText = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            xmlElement.InnerText = "PopupMessage";
                            break;
                        default:
                            // TODO   - What to do if not a standard error code?
                            break;
                    };
                    xmlMsgsRoot.AppendChild(xmlElement);
                }
            }

        }


        /// <summary>
        /// Responsible for logging any errors which occur within the WCF Service Layer
        /// </summary>
        /// <param name="sAdaptorMethod"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        protected void logErrors(string sAdaptorMethod, XmlDocument xmlRequest, bool bCallResult, BusinessAdaptorErrors errors)
        {
            //Change by kuladeep for Cloud-Start
            int iclientId = 0;
            bool bIsSuccess = false;
            if (xmlRequest != null && xmlRequest.SelectSingleNode("//ClientId") != null)
                iclientId = Conversion.CastToType<int>(xmlRequest.SelectSingleNode("//ClientId").InnerText,out bIsSuccess);
            //Change by kuladeep for Cloud-End
            ServiceLogger.LogErrors(sAdaptorMethod, xmlRequest, bCallResult, errors,iclientId);

        } // method: logErrors

        protected bool ValidateSession(string sessionKeySupplied,int iClientId)
        {
            try
            {
                SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied,iClientId);
                if (oSession == null) // If user not in session'
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ee)
            {
                return false;
            }
        }
    }//class

    //sanoopsharma - Start MITS 35932   
    // Class to forcelly accept SSL certificate for Integral webservice 
    public class certificate : ICertificatePolicy
    {
        public bool CheckValidationResult(
              ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {

            //Return True to force the certificate to be accepted.
            return true;

        }
    }
    //sanoopsharma - End MITS 35932 
}
