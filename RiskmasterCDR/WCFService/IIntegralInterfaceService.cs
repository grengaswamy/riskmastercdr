﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPolicyInterfaceService" in both code and config file together.
    [ServiceContract]
    public interface IIntegralInterfaceService
    {                
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicySearchResult(PolicySearch oSearchRequest, out PolicySearch oSearchResponse);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyEnquiryResult(PolicyEnquiry oSearchRequest, out ExternalPolicyData oSearchResponse); //SIKV
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void PolicyValidation(PolicyValidateInput oPolicyRequest, out PolicyValidateResponse oPolicyResponse);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SavePolicyAllData(ExternalPolicyData oSaveRequest, out PolicySaveRequest oSaveResponse);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void UploadClaimData(ref UploadClaimData oUploadClaimData); //Payal: RMA-6471 
    }
}


