﻿using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;
using Riskmaster.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Application.ReserveWorksheet;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RSWService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RSWService.svc or RSWService.svc.cs at the Solution Explorer and start debugging.
    public class RSWService : RMService,IRSWService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse GetReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            string sRswType = oReserveWorksheetRequest.RSWType;
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetReserveWorsheet";
            int iClientId = oReserveWorksheetRequest.ClientId;

            ReserveWorksheetResponse objResponse = null;
            //ReserveWorksheetAdaptor objReserveWorkSheetAdaptor = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);
                //objReserveWorkSheetAdaptor = new ReserveWorksheetAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))  //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId, iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");
                    objGenResWS.GetReserveWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");
                    objGenResWS.GetReserveWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin);
                }

                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse OnLoadInformation(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "OnLoadInformation";

            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            int iClientId = oReserveWorksheetRequest.ClientId;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objResponse = new ReserveWorksheetResponse();
                objGenResWS.OnLoadInformation(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                return objResponse;
                //return objReserveWorkSheetAdaptor.OnLoadInformation(request, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse SaveReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            string sRswType = oReserveWorksheetRequest.RSWType;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "SaveReserveWorkSheet";
            //    string rswtype = request.RSWType;

            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))    //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    // objGenResWS.SaveData(request, ref objResponse);
                    objGenResWS.SaveDataWithUserLogin(oReserveWorksheetRequest, ref objResponse, oUserLogin);  //aaggarwal29 MITS 27763 -- code changed to allow checking of user permissions while saving data.
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    //objGenResWS.SaveData(request, ref objResponse);
                    objGenResWS.SaveDataWithUserLogin(oReserveWorksheetRequest, ref objResponse, oUserLogin);  //aaggarwal29 MITS 27763 -- code changed to allow checking of user permissions while saving data.
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse SubmitReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "SubmitReserveWorkSheet";
            string sRswType = oReserveWorksheetRequest.RSWType;

            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.SubmitWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.SubmitWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);

                }
                return objResponse;
                //return objReserveWorkSheetAdaptor.OnLoadInformation(request, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }


        }

        /// <summary>
        ///  MGaba2:R6 :Approval of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ApproveReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            string sRswType = oReserveWorksheetRequest.RSWType;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ApproveReserveWorkSheet";


            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.ApproveReserveWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.ApproveReserveWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);


                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }


        /// <summary>
        ///  MGaba2:R6 :Approval of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse RejectReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RejectReserveWorkSheet";
            string sRswType = oReserveWorksheetRequest.RSWType;

            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.RejectWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId);
                }
                else if (sRswType == "Customize")
                {

                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.RejectWorksheet(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId);

                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  MGaba2:R6 :Printing of Reserve WorkSheet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse PrintReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "PrintWorksheet";
            string sRswType = oReserveWorksheetRequest.RSWType;

            ReserveWorksheetResponse objResponse = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            int iClientId = oReserveWorksheetRequest.ClientId;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                //MGaba2: MITS 22225:Print functionality was not working from Reserve Approval Screen
                //request.RSWType is sent as blank from Reserve Approval Screen
                if (sRswType == "")
                {
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                    sRswType = objGenResWS.GetRswTypeToLaunch(oReserveWorksheetRequest.ClaimId);
                }

                if (sRswType == "Generic")
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                    objGenResWS.PrintWorksheet(oReserveWorksheetRequest, ref objResponse);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");

                    objCusResWS.PrintWorksheet(oReserveWorksheetRequest, ref objResponse);

                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse DeleteReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "DeleteReserveWorkSheet";
            string sRswType = oReserveWorksheetRequest.RSWType;

            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.DeleteWorksheet(oReserveWorksheetRequest, ref objResponse);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactory = null;
                    ReserveWorksheetCustomize objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Customize");


                    objGenResWS.DeleteWorksheet(oReserveWorksheetRequest, ref objResponse);
                }
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }


        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ListFundsTransactions(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ListFundsTransactions";
            string sRswType = oReserveWorksheetRequest.RSWType;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objResponse = new ReserveWorksheetResponse();
                objGenResWS.ListFundsTransactions(oReserveWorksheetRequest, ref objResponse);
                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "ApproveReserveWorksheetFromApprovalScreen";
            string sRswType = string.Empty;

            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                sRswType = objGenResWS.GetRswTypeToLaunch(oReserveWorksheetRequest.ClaimId);

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.ApproveReserveWorksheetFromApprovalScreen(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");


                    objCusResWS.ApproveReserveWorksheetFromApprovalScreen(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }



                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        /// <summary>
        ///  gagnihotri:R6 :List of Funds Transactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest oReserveWorksheetRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RejectReserveWorksheetFromApprovalScreen";
            string sRswType = string.Empty;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            int iClientId = oReserveWorksheetRequest.ClientId;
            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oReserveWorksheetRequest, out xmlRequest, functionName, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                objResponse = new ReserveWorksheetResponse();
                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                sRswType = objGenResWS.GetRswTypeToLaunch(oReserveWorksheetRequest.ClaimId);

                if (sRswType == "Generic" || string.IsNullOrEmpty(sRswType))        //tmalhotra2 mits 27272
                {
                    //ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
                    //  ReserveWorksheetGeneric objGenResWS = null;
                    objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");


                    objGenResWS.RejectReserveWorksheetFromApprovalScreen(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }
                else if (sRswType == "Customize")
                {
                    ReserveWorksheetFactory<ReserveWorksheetCustomize> objResWSFactoryCust = null;
                    ReserveWorksheetCustomize objCusResWS = null;
                    objResWSFactoryCust = new ReserveWorksheetFactory<ReserveWorksheetCustomize>(oUserLogin.LoginName, oUserLogin.Password, oUserLogin.UserId, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.objRiskmasterDatabase.ConnectionString, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), oUserLogin.DatabaseId, oUserLogin.objUser.ManagerId,iClientId);
                    objCusResWS = objResWSFactoryCust.GetReserveWorksheet("Customize");


                    objCusResWS.RejectReserveWorksheetFromApprovalScreen(oReserveWorksheetRequest, ref objResponse, oUserLogin.GroupId, oUserLogin);
                }



                return objResponse;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }
    }
}
