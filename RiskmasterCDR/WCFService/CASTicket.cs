﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;

namespace RiskmasterService
{
    public class CASTicket
    {
        public string GetCASTicket(string sURL, string sService, string sUser, string sPassword)
        {
            string sTicketURL = GetTicketGeneratingTicket(sUser, sPassword, sURL);
            string sTicket = GetTicket(sTicketURL, sService);
            return sTicket;
        }       

        //Gets TGT ticket for ST ticket generation request
        private string GetTicketGeneratingTicket(string sUserName, string sPassword, string sServerURL)
        {

            var request = (HttpWebRequest)WebRequest.Create(sServerURL);

            var postData = "username=" + sUserName;
            postData += "&password=" + sPassword;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
                        
            ServicePointManager.CertificatePolicy = new certificate();
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string pattern = @"<\s*form[^>]*\s+action=([""'])(.*?)\1";
            Regex mysReg = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);

            MatchCollection form = mysReg.Matches(responseString);

            GroupCollection groups = form[0].Groups;
            string sTicketURL = groups["0"].Value.Replace(@"<form action=""", " ").Trim();
            sTicketURL = sTicketURL.Substring(0, sTicketURL.Length - 1);
            return sTicketURL;
        }

        //Requests and returns ST ticket required for Integral web service request.
        private string GetTicket(string sTicketURL, string sService)
        {

            var request = (HttpWebRequest)WebRequest.Create(sTicketURL);

            var postData = "service=" + sService;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            ServicePointManager.CertificatePolicy = new certificate();
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }
    }
}