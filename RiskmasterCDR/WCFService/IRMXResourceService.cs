﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRMXResourceService" in both code and config file together.
    [ServiceContract]
    public interface IRMXResourceService
    {
        /// <summary>
        /// GetResourceByLanguageAndKey
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Key"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        [OperationContract]
        string GetResourceByLanguageAndKey(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Key, string p_Type, int iClientId = 0);
        /// <summary>
        /// GetAllResourceByLanguageAndPageId
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetAllResourceByLanguageAndPageId(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Type);
        /// <summary>
        /// GetGlobalResourcesByLanguage
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetGlobalResourcesByLanguage(int p_DataSourceId, string p_LanguageCode, int iClientId);
        /// <summary>
        /// GetConnectionstring
        /// </summary>
        /// <param name="strConnString"></param>
        /// <returns></returns>
        [OperationContract]
        string GetConnectionstring(string strConnString);
        /// <summary>
        /// GetConnectionstringFromDSNId
        /// </summary>
        /// <param name="DataSourceId"></param>
        /// <returns></returns>
        [OperationContract]
        string GetConnectionstringFromDSNId(int DataSourceId, int ClientId);
        /// <summary>
        /// GetAllResourceByLanguageAndPageIdAndResType
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetAllResourceByLanguageAndPageIdAndResType(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Type);
        /// <summary>
        /// GetTimestampForMDIMenu
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="iLangCode"></param>
        /// <returns></returns>
        [OperationContract]
        string GetTimestampForMDIMenu(string connectionString, int iLangCode);
        /// <summary>
        /// GetMDIMenuXML
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="iLangCode"></param>
        /// <returns></returns>
        [OperationContract]
        string GetMDIMenuXML(string connectionString, int iLangCode);
        /// <summary>
        /// GetChangedMDIMenuList
        /// </summary>
        /// <param name="p_LastUpdated"></param>
        /// <param name="sMaxLastUpdated"></param>
        /// <returns></returns>
        [OperationContract]
        List<string> GetChangedMDIMenuList(string p_LastUpdated, out string sMaxLastUpdated);
        /// <summary>
        /// GetPageInfo
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        [OperationContract]
        string GetPageInfo(RMResource request, string p_sXMLDocument);
        /// <summary>
        /// SaveResource
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_Message"></param>
        /// <returns></returns>
        [OperationContract]
        bool SaveResource(RMResource request, out string p_Message);
        /// <summary>
        /// Is Error Code Enabled
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string ShowErrorCode();
        /// <summary>
        /// GetGlobalInfo
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        [OperationContract]
        string GetGlobalInfo(RMResource request, string p_sXMLDocument);
        /// <summary>
        /// GetFormats
        /// </summary>
        /// <param name="iDsnId"></param>
        /// <param name="p_sCountryId"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetFormats(int iDsnId, string p_sCountryId);
        /// <summary>
        /// GetSearchInfo
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        [OperationContract]
        bool GetSearchInfo(RMResource request, ref string p_sXMLDocument);
        /// <summary>
        /// ClearGlobalErrorMessageCache
        /// </summary>
        /// <param name="sResId"></param>
        [OperationContract]
        void ClearGlobalErrorMessageCache(string sResId);
        /// <summary>
        /// GetSearchResult
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <param name="iDsnId"></param>
        /// <param name="sSearchType"></param>
        /// <param name="iCatId"></param>
        /// <param name="sDisplayCat"></param>
        /// <param name="iClientId"></param>
        /// <returns></returns>
        [OperationContract]
        string GetSearchResult(RMResource request, string p_sXMLDocument, int iDsnId, string sSearchType, int iCatId, string sDisplayCat, int iClientId); 
    }
}
