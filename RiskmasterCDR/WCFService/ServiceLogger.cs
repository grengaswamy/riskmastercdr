﻿using System;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace RiskmasterService
{
    /// <summary>
    /// Centralized logging class for the Windows Communication Foundation Service layer
    /// </summary>
    public class ServiceLogger
    {
        /// <summary>
        /// Logs any errors which have bubbled up through the Business Adapter layer
        /// </summary>
        /// <param name="sAdaptorMethod"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors">collection of all the BusinessAdaptor errors which have occurred</param>
        public static void LogErrors(string sAdaptorMethod, XmlDocument xmlRequest, bool bCallResult, BusinessAdaptorErrors errors,int p_iClientId)
        {

            // First, log the error to the error log

            // Add overall result status
            string sCall = sAdaptorMethod;
            if (bCallResult)
                sCall += " (Success)";
            else
                sCall += " (Error)";

            // Log the overall call failure and the input envelope (only if call failed)
            if (bCallResult == false)
            {
                LogItem oLogItem = new LogItem();
                oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                oLogItem.Category = "CommonWebServiceLog";
                oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                oLogItem.Message = sAdaptorMethod + " call failed.";
                if (xmlRequest != null)
                    try
                    {
                        //mkaran2 : MITS 33856 : Start 
                        // We could not encrypt admin user and pwd on taskmanager.cs as delegate 
                        // passed temp xmlTmp and we use here original xmlRequest.
                        XmlNode xNode = xmlRequest.SelectSingleNode("//AdminLogin");
                        if (xNode != null)
                        {
                            xNode.InnerText = Riskmaster.Security.Encryption.RMCryptography.EncryptString(xmlRequest.SelectSingleNode("//AdminLogin").InnerText);
                        }

                        xNode = xmlRequest.SelectSingleNode("//AdminPassword");
                        if (xNode != null)
                        {
                            xNode.InnerText = Riskmaster.Security.Encryption.RMCryptography.EncryptString(xmlRequest.SelectSingleNode("//AdminPassword").InnerText);
                        }

                        //mkaran2 : MITS 33856 : End

                        oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
                    }
                    catch (Exception)
                    {

                        oLogItem.RMParamList.Add("XML Input Envelope", "xmlRequest contents could not be logged - likely too large for available memory.");
                    }
                Log.Write(oLogItem,p_iClientId);
            }

            // Iterate through individual errors and log them. If exceptions are present, those will be walked (via InnerException) and all exceptions in the list will be logged.
            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... build error type
                    string sErrorType = null;
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            sErrorType = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            sErrorType = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            sErrorType = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            sErrorType = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            sErrorType = "PopupMessage";
                            break;
                        default:
                            sErrorType = "SystemError";
                            break;
                    };

                    // ... add individual error messages/warnings
                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // Init logfile item for this error
                        LogItem oLogItem = new LogItem();
                        oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                        oLogItem.Category = "CommonWebServiceLog";
                        oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                        oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                        oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);
                        oLogItem.RMParamList.Add("Call Error Type", sErrorType);

                        // ... add error code/number
                        oLogItem.RMParamList.Add("Error Code", err.ErrorCode);

                        // ... add error description
                        oLogItem.Message = err.ErrorDescription;

                        // Flush entry to log file
                        Log.Write(oLogItem, p_iClientId);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        Exception exc = err.oException;

                        while (exc != null)
                        {
                            // Init logfile item for this error
                            LogItem oLogItem = new LogItem();
                            oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                            oLogItem.Category = "CommonWebServiceLog";
                            oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                            oLogItem.RMParamList.Add("Call Error Type", sErrorType);
                            oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                            oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);


                            // ... determine error code - assembly + exception type
                            oLogItem.RMParamList.Add("Error Code", err.oException.Source + "." + err.oException.GetType().Name);

                            // ... add error description
                            if (err.ErrorDescription != "")
                                oLogItem.Message = err.ErrorDescription;
                            else
                                oLogItem.Message = err.oException.Message;

                            // ... dump exception info to log
                            oLogItem.RMParamList.Add("Exception Source", exc.Source);
                            oLogItem.RMParamList.Add("Exception Type", exc.GetType().Name);
                            oLogItem.RMParamList.Add("Exception Message", exc.Message);
                            oLogItem.RMParamList.Add("Exception StackTrace", exc.StackTrace);

                            // ... flush entry to log file
                            Log.Write(oLogItem, p_iClientId);

                            // ... get next exception in chain (if there is one)
                            exc = exc.InnerException;
                        }
                    }  // Exception case
                }
            }

        }
    }
}
