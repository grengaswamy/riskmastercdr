﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICodesService" here, you must also update the reference to "ICodesService" in Web.config.
    /// <summary>
    /// ICodes
    /// </summary>
    [ServiceContract]
    public interface ICodes
    {
        /// <summary>
        /// GetCodes
        /// </summary>
        /// <param name="oCodeTypeRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/list", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        CodeListType GetCodes(CodeTypeRequest oCodeTypeRequest);
        /// <summary>
        /// GetQuickLookUp
        /// </summary>
        /// <param name="oQuickLookupRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/lookup", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        CodeListType GetQuickLookUp(QuickLookupRequest oQuickLookupRequest);
    }
}
