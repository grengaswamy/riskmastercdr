﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    /// <summary>
    /// IAuthenticate
    /// By : Deb
    /// </summary>
    [ServiceContract]
    public interface IAuthenticate
    {
        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/login", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AuthenticateUser(AuthData oAuthData);

        /// <summary>
        /// Session Token
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/token", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetToken(AuthData oAuthData);

        /// <summary>
        /// Gets Clients
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/clients", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetClients();

        /// <summary>
        /// Get DSNs
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/dsn", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetUserDSNs(AuthData oAuthData);

        /// <summary>
        /// PowerViews
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/view", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetUserViews(AuthData oAuthData);

        /// <summary>
        /// Current Session
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/currentuser", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetUserSessionInfo(AuthData oAuthData);


        /// <summary>
        /// Gets the user's session ID
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// <returns>string containing the user's authenticated Session ID</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/session", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetUserSessionID(AuthData oAuthData);
        
        /// <summary>
        /// Authenticates the SMS user using the Default RISKMASTER Membership Provider
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/loginadmin", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AuthenticateSMSUser(AuthData oAuthData);

        /// <summary>
        /// Validates whether a specific user exists within RISKMASTER
        /// for scenarios that require pass-through credentials from 3rd party Single-Sign On applications
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/sso", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool SingleSignOnUser(AuthData oAuthData);

        /// <summary>
        /// Provides the ability to change a user's RISKMASTER password
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/changepassword", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool ChangePassword(AuthData oAuthData);
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/providers", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        /// <summary>
        /// Gets the list of available Authentication Providers for the system
        /// </summary>
        Dictionary<string, string> GetAuthenticationProviders();

        
        /// <summary>
        /// Logs out a user and removes their session information from the database
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/logout", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void LogOutUser(AuthData oAuthData);
        /// <summary>
        /// GetBOBSetting
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/iscarrier", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool GetBOBSetting(AuthData oAuthData);
        
        /// <summary>
        /// GetLanguageCode
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/languagecode", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetLanguageCode(AuthData oAuthData);
        
        /// <summary>
        /// GetBaseLanguageCode
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/baselanguagecode", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetBaseLanguageCode();
        
        /// <summary>
        /// Fucntion for reseting User password and send mail to user.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/password", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ForgotPasswordGenericResponse ForgotPasswordSendMail(AuthData oAuthData);
        
        /// <summary>
        /// function for resetting password.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ispasswordreset", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool IsPasswordReset(AuthData oAuthData);
        
        /// <summary>
        /// GetDateFormatFromLoginDetails
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/dateformat", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetDateFormatFromLoginDetails(AuthData oAuthData);

        /// <summary>
        /// GetPIMenuList
        /// </summary>
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/pimenulist", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //bool GetPIMenuList(AuthData oAuthData);

        /// <summary>
        /// GetDateFormatFromLangCode
        /// </summary>
        /// <returns>DateFormat</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/dateformatbycode", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetDateFormatFromLangCode(AuthData oAuthData);

        /// <summary>
        /// MobileAuth
        /// </summary>
        /// <param name="xmlRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/mobilelogin", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AuthenticateMobileUser(string xmlRequest);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/IsAllowAutoLogin", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AuthenticateAutoLoginUser(AuthData oAuthData);
    }
}
