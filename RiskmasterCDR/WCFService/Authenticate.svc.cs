﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Models;
using Riskmaster.Cache;
using Riskmaster.Security;
using Riskmaster.Security.Authentication;
using Riskmaster.Security.Encryption;
using Riskmaster.Security.RMApp;
using Riskmaster.Settings;

namespace RiskmasterService
{
    /// <summary>
    /// Authenticate
    /// By: Deb
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Authenticate : IAuthenticate
    {
        /// <summary>
        /// AuthenticateUser
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        public string AuthenticateUser(AuthData oAuthData)
        {
            try
            {
                string strDecodedUserName = string.Empty, strDecodedPassword = string.Empty;
                string strMessage = string.Empty; //int iClientId = 0;
                strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.UserName);
                strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.Password);
                //try{
                //  iClientId = int.Parse(RMCryptography.DecryptString(oAuthData.ClientId));
                //}
                //catch{}
                bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateUser(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), strDecodedUserName, strDecodedPassword, out strMessage, oAuthData.ClientId);

                return blnIsAuthenticated.ToString() + "|^^|" + strMessage;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        /// <summary>
        /// Authenticate Mobile User
        /// </summary>
        /// <param name="xmlRequest"></param>
        /// <param name="xmlRepsonse"></param>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        public string AuthenticateMobileUser(string xmlRequest)
        {
            XmlDocument objXmlRequest = new XmlDocument();

            string strDecodedUserName = string.Empty;
            string strDecodedDSNName = string.Empty;
            string strUserSessionId = string.Empty;
            string strResponse = string.Empty;
            XmlNode objXmlUserName = null;
            XmlNode objXmlDSNName = null;

            try
            {
                objXmlRequest.LoadXml(xmlRequest);

                objXmlUserName = objXmlRequest.SelectSingleNode("//UserName");
                objXmlDSNName = objXmlRequest.SelectSingleNode("//DSNName");

                if (objXmlUserName != null)
                    strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlUserName.InnerText);

                if (objXmlDSNName != null)
                    strDecodedDSNName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlDSNName.InnerText);
                AuthData objAuth = new AuthData()
                {
                    UserName=strDecodedUserName,
                    DsnName=strDecodedDSNName,
                    ClientId=0
                };
                strUserSessionId = GetUserSessionID(objAuth);
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Success</MsgStatusCd></MsgStatus><Document><result category=\"Authorization\"><Token>" + strUserSessionId + "</Token><LoggedUser>" + strDecodedUserName.Trim() + "</LoggedUser><LoginTime>" + DateTime.Now.ToString() + "</LoginTime></result></Document></ResultMessage>";
            }
            catch (Exception ex)
            {
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Error</MsgStatusCd><ExtendedStatus><ExtendedStatusCd></ExtendedStatusCd><ExtendedStatusDesc>" + ex.Message + "</ExtendedStatusDesc><ExtendedMsgType>Error</ExtendedMsgType></ExtendedStatus></MsgStatus></ResultMessage>";
            }

            return strResponse;



        }

        /// <summary>
        /// Authenticates the SMS user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <param name="strMessage">string containing the exception message if any</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public string AuthenticateSMSUser(AuthData oAuthData)
        {
            try
            {
                string strDecodedUserName = string.Empty, strDecodedPassword = string.Empty;

                strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.UserName);
                strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.Password);
                string strMessage = string.Empty;
                bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateSMSUser(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), strDecodedUserName, strDecodedPassword, out strMessage, oAuthData.ClientId);

                return blnIsAuthenticated.ToString() + "|^^|" + strMessage;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
         //<summary>
         //Authenticates the Integral user using the Default RISKMASTER Membership Provider
         //</summary>
         //<param name="UserName">string containing the user's login name</param>
         //<param name="DsnName">string containing DSN namey</param>
         ///// <param name="ClientId">string containing ClientId</param>
         //<returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public string AuthenticateAutoLoginUser(AuthData oAuthData)
        {
            bool bIsAllowAutoLogin = false;
            string sOutResult = string.Empty;
            SysSettings objSysSettings = null;
            UserLogin oUser = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
            if (oUser.objRiskmasterDatabase != null && !string.IsNullOrWhiteSpace(oUser.objRiskmasterDatabase.ConnectionString))
            {
                objSysSettings = new SysSettings(oUser.objRiskmasterDatabase.ConnectionString, oAuthData.ClientId);                

                if (!objSysSettings.AllowAutoLogin)
                {
                    bIsAllowAutoLogin = false;
                    sOutResult = string.Empty;
                }
                else
                {
                    bIsAllowAutoLogin = true;
                    sOutResult = AuthenticateUser(oAuthData);
                }                
            }
            return sOutResult;
        }
        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public bool SingleSignOnUser(AuthData oAuthData)
        {
            try
            {
                string strDecodedUserName = string.Empty;

                strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.UserName);

                bool blnIsAuthenticated = MembershipProviderFactory.SingleSignOnUser(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), strDecodedUserName, oAuthData.ClientId);
                return blnIsAuthenticated;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }//method: SingleSignOnUser()

        /// <summary>
        ///  returns the GetCustomViews
        /// </summary>
        /// <returns></returns>
        public string GetCustomViews(AuthData oAuthData)
        {
            //rsolanki2(may 23,2010): extensibility updates start - retriving views with viewid at -1

            StringBuilder strSQL = new StringBuilder();
            try
            {
                //int selectedDSNId = 0;
                //selectedDSNId = System.Convert.ToInt32(strSelectedDsnId);           
                //Dictionary<string, string> dictUserViews = new Dictionary<string, string>();                        

                strSQL.Append("|");
                Dictionary<string, int> parms = new Dictionary<string, int>();
                parms.Add("DSNID", oAuthData.DsnId);
                using (DbReader reader = DbFactory.ExecuteReader
                    (ConfigurationInfo.GetConnectionString("ViewDataSource", oAuthData.ClientId)
                    , "SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID=-1 AND DATA_SOURCE_ID =~DSNID~"
                        , parms))
                {
                    while (reader.Read())
                    {
                        strSQL.Append(reader["FORM_NAME"].ToString());
                        strSQL.Append("|");
                    } // while

                } // using
                //dictUserViews.Add("ViewList", strSQL.ToString());

                if (strSQL.Length == 1)
                {
                    return string.Empty;
                }
                return strSQL.ToString();
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }//method: GetCustomViews
        /// <summary>
        /// Provides the ability to change a user's password
        /// </summary>
        /// <returns>boolean indicating whether or not the user's password was successfully changed</returns>
        public bool ChangePassword(AuthData oAuthData)
        {
            bool bIsPasswordChanged = false;
            try
            {
                CustomMembershipProvider membProvider = MembershipProviderFactory.CreateMembershipProvider(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), oAuthData.ClientId);

                bIsPasswordChanged = membProvider.ChangePassword(oAuthData.UserName, oAuthData.Password, oAuthData.NewPassword);

                return bIsPasswordChanged;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        /// <summary>
        /// Fucntion for checking whether Email address is configured for userid or not.
        /// </summary>
        public ForgotPasswordGenericResponse ForgotPasswordSendMail(AuthData oAuthData)
        {
            string sretunrvalue = string.Empty;
            Mailer objMailer = null;

            string s_TempPwd = CommonFunctions.GetRandomPassword(5);
            ForgotPasswordGenericResponse objForgotPasswordGenericResponse = new ForgotPasswordGenericResponse();
            string s_EmailID = string.Empty;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            try
            {
                parms.Add("USERNAME", oAuthData.UserName);
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DISTINCT USER_TABLE.EMAIL_ADDR FROM USER_DETAILS_TABLE,USER_TABLE ");
                sbSQL.Append("WHERE USER_DETAILS_TABLE.USER_ID=USER_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME=~USERNAME~");
                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), sbSQL.ToString(),parms))
                {
                    if (reader.Read())
                    {
                        s_EmailID = reader["EMAIL_ADDR"].ToString();
                    }

                }
                if (!string.IsNullOrEmpty(s_EmailID))
                {
                    try
                    {
                        //Update password to DB.
                        //StringBuilder sb_update = new StringBuilder();
                        //sbSQL.Clear();
                        //parms.Add("PWD",Riskmaster.Security.Encryption.RMCryptography.EncryptString(s_TempPwd));
                        //sbSQL.Append("UPDATE USER_DETAILS_TABLE Set PASSWORD=~PWD~ ,IS_PWD_RESET=-1 where LOGIN_NAME=~USERNAME~");


                        // npadhy - JIRA RMA-6364 In Oracle the Parameters are assigned values base on position rather than Parameter Name
                        // So in the query as PWD was the first parameter and Login Name was second, but in the PARAMs collection first parameter was UserName, and secondParam was Password
                        // Thus, it was not able to find a user in System with Pwd as Login Name.
                        DbWriter objDbWriter = DbFactory.GetDbWriter(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId).ToString());

                        objDbWriter.Tables.Add("USER_DETAILS_TABLE");
                        objDbWriter.Fields.Add("PASSWORD", Riskmaster.Security.Encryption.RMCryptography.EncryptString(s_TempPwd));
                        objDbWriter.Fields.Add("IS_PWD_RESET", "-1");
                        objDbWriter.Where.Add(String.Format("LOGIN_NAME= '{0}'",oAuthData.UserName));
                        objDbWriter.Execute();
                        //objDbWriter.Tables
                       // DbFactory.ExecuteNonQuery(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId).ToString(), sbSQL.ToString(), parms);
                        objMailer = new Mailer(oAuthData.ClientId);
                        objMailer.To = s_EmailID;
                        objMailer.From = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdfromMailid", oAuthData.ClientId));
                        objMailer.Subject = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdSubject", oAuthData.ClientId));
                        //objMailer.Body = sb_body.ToString();
                        objMailer.Body = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtPwdMailBody", oAuthData.ClientId).Replace("User_id", oAuthData.UserName).Replace("s_TempPwd", s_TempPwd));
                        objMailer.IsBodyHtml = true;
                        objMailer.SendMail();
                        //       CommonFunctions.SendEmail(s_EmailID, "RiskMasterSupport@CSC.Com", "RMA CREDENTIALS", sb_body.ToString(), true);
                        objForgotPasswordGenericResponse.Status = "Success";
                        objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdSuccessMessage", oAuthData.ClientId));
                    }
                    catch (Exception exe)
                    {

                        objForgotPasswordGenericResponse.Status = "Error";
                        objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(exe.Message.ToString());
                    }
                }
                else
                {
                    objForgotPasswordGenericResponse.Status = "Error";
                    objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdErrorMessage", oAuthData.ClientId));
                }
            }
            catch (Exception exe)
            {
                exe.ToString();
            }
            return objForgotPasswordGenericResponse;
        }
        /// <summary>
        /// check Whether password has been reset for user or not.
        /// </summary>
        public bool IsPasswordReset(AuthData oAuthData)
        {
            try
            {
                Dictionary<string, string> parms = new Dictionary<string, string>();
                parms.Add("LOGINNAME", oAuthData.UserName);
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DISTINCT IS_PWD_RESET FROM USER_DETAILS_TABLE WHERE LOGIN_NAME=~LOGINNAME~");
                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), sbSQL.ToString(),parms))
                {
                    if (reader.Read())
                    {
                        return Conversion.ConvertObjToBool(reader["IS_PWD_RESET"].ToString(),oAuthData.ClientId);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception exe)
            {
                exe.ToString();
            }
            return false;
        }
        //Asharma326 MITS 27586 Ends

        //Added by agupta298 for WWIG GAP 30 MITS#35365 End

        //Rakhel Multi Language Changes
        /// <summary>
        /// GetDateFormat for the Username,DSNName
        /// </summary>
        /// <returns>DateFormat</returns>
        public string GetDateFormatFromLoginDetails(AuthData oAuthData)
        {
            string sDateFormat = string.Empty;
            string sLangCode = string.Empty;
            try
            {
                sLangCode = GetLanguageCode(oAuthData);

                if (sLangCode != string.Empty)
                {
                    sLangCode = sLangCode.Split('|')[0].ToString();
                    sDateFormat = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(oAuthData.ClientId), "SELECT DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode));
                }
                return sDateFormat;
            }
            catch (Exception ee)
            {
                return sDateFormat;
            }
            finally
            {
                sDateFormat = null;
            }
        }

        //Rakhel Multi Language Changes
        /// <summary>
        /// GetDateFormat for the Languade Code
        /// </summary>
        /// <returns>DateFormat</returns>
        public string GetDateFormatFromLangCode(AuthData oAuthData)
        {
            string sDateFormat = string.Empty;
            string sLangCode = string.Empty;
            Dictionary<string, int> parms = new Dictionary<string, int>();
            try
            {
                parms.Add("LANGCODE",oAuthData.LangCode);
                if (Convert.ToString(oAuthData.LangCode) != string.Empty)
                {
                    sDateFormat = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(oAuthData.ClientId), "SELECT DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=~LANGCODE~", parms));
                }
                return sDateFormat;
            }
            catch (Exception ee)
            {
                return sDateFormat;
            }
            finally
            {
                sDateFormat = null;
            }
        }

        /// <summary>
        /// Gets the user's session ID based on their authentication credentials
        /// </summary>
        /// <returns>string containing the user's authenticated Session ID</returns>
        public string GetUserSessionID(AuthData oAuthData)
        {
            try
            {
                string strSessionID = string.Empty;
                strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(oAuthData.ClientId), oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);

                return strSessionID;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }//method: GetUserSessionID()



        /// <summary>
        /// Gets a collection of all available and configured authentication 
        /// providers
        /// </summary>
        /// <returns>Generic Dictionary collection of all available and configured
        /// Authentication Providers</returns>
        public Dictionary<string, string> GetAuthenticationProviders()
        {
            Dictionary<string, string> dictAuthProviders = new Dictionary<string, string>();
            return dictAuthProviders;
            //return MembershipProviderFactory.GetAuthenticationProviders();
        }

        /// <summary>
        /// GetToken
        /// </summary>
        /// <param name="oAuthData"></param>
        /// <returns></returns>
        public string GetToken(AuthData oAuthData)
        {
            string strDecodedUserName = string.Empty, strDecodedPassword = string.Empty;
            string strMessage = string.Empty;
            strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.UserName);
            strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(oAuthData.Password);
            try
            {
                bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateUser(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), oAuthData.UserName, oAuthData.Password, out strMessage, oAuthData.ClientId);
                if (blnIsAuthenticated)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(oAuthData.DsnName))
                        {
                            string strSessionID = string.Empty;
                            strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(oAuthData.ClientId), oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
                            return strSessionID;
                        }
                        else
                        {
                            List<DsnData> objDsnsList = new List<DsnData>();
                            DsnData objDsn = null;
                            foreach (KeyValuePair<string, string> okey in GetUserDSNs(oAuthData))
                            {
                                objDsn = new DsnData();
                                objDsn.DsnId = okey.Value;
                                objDsn.DsnName = okey.Key;
                                objDsnsList.Add(objDsn);
                            }

                            return Newtonsoft.Json.JsonConvert.SerializeObject(objDsnsList);
                        }
                    }
                    catch (Exception ex)
                    {
                        return string.Empty;
                    }

                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        /// <summary>
        /// GetClients
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetClients()
        {
            string sConnString = ConfigurationInfo.GetConnectionString("rmATenantSecurity",0);
            Dictionary<string, string> objDictClients = null;
            try
            {
                objDictClients = new Dictionary<string, string>();
                DbReader objReader = DbFactory.ExecuteReader(sConnString, "SELECT CLIENT_ID FROM CLIENT");
                while (objReader.Read())
                {
                    objDictClients.Add(RMCryptography.EncryptString(Convert.ToString(objReader[0])), Convert.ToString(objReader[0]));
                }
                return objDictClients;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objDictClients = null;
            }
        }
        /// <summary>
        /// GetUserDSNs
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetUserDSNs(AuthData oAuthData)
        {
            Dictionary<string, string> dictUserDbs = new Dictionary<string, string>();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, DATA_SOURCE_TABLE.DSNID ");
            strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE ");
            strSQL.Append("WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            strSQL.AppendFormat("AND USER_DETAILS_TABLE.LOGIN_NAME={0} ", "~USERNAME~");
            strSQL.Append("ORDER BY DATA_SOURCE_TABLE.DSN");

            dictParams.Add("USERNAME", oAuthData.UserName);

            using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), strSQL.ToString(), dictParams))
            {
                while (reader.Read())
                {
                    string strDSNKey = reader["DSN"].ToString();
                    string strDSNID = reader.GetInt32("DSNID").ToString();
                    if (!dictUserDbs.ContainsKey(strDSNKey))
                    {
                        dictUserDbs.Add(strDSNKey, strDSNID);
                    }
                }
                reader.Close();
            }
            return dictUserDbs;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="strDatabaseName"></param>
        /// <param name="strSelectedDsnId"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetUserViews(AuthData oAuthData)
        {
            StringBuilder strSQL = new StringBuilder();
            int selectedDSNId = 0;
            int iUserId = 0;
            int iGroupId = 0;
            string sConnectionString = string.Empty;
            try
            {
                selectedDSNId = System.Convert.ToInt32(oAuthData.DsnId);

                //Create a new UserLogin object that can be utilized for various values
                //smishra25:Start by Shivendu for performance updates
                //UserLogin objUserLogin = new UserLogin(strUserName, strDatabaseName);
                UserLogin objUserLogin = new UserLogin(oAuthData.ClientId); //Ash - cloud
                //objUserLogin.ClientId = oAuthData.ClientId;
                iUserId = objUserLogin.GetUserIdAndGroupId(oAuthData.UserName, oAuthData.DsnName, out iGroupId);

                //smishra25:End by Shivendu for performance updates

                strSQL.Append("SELECT NET_VIEWS.VIEW_NAME,NET_VIEWS.VIEW_ID ");
                strSQL.Append("FROM NET_VIEWS_MEMBERS,NET_VIEWS ");
                strSQL.AppendFormat("WHERE ((MEMBER_ID={0} ", "~USERID~");
                strSQL.AppendFormat("AND ISGROUP=0) OR (MEMBER_ID={0} ", "~GROUPID~");
                strSQL.Append("AND ISGROUP<>0)) ");
                strSQL.Append("AND NET_VIEWS_MEMBERS.VIEW_ID=NET_VIEWS.VIEW_ID ");
                strSQL.AppendFormat("AND NET_VIEWS_MEMBERS.DATA_SOURCE_ID={0} ", "~DSNID~");
                strSQL.AppendFormat("AND NET_VIEWS.DATA_SOURCE_ID={0} ", "~DSNID~");
                strSQL.Append("ORDER BY ISGROUP");

                Dictionary<string, string> dictUserViews = new Dictionary<string, string>();
                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                //dictParams.Add("USERID", objUserLogin.UserId);
                //dictParams.Add("GROUPID", objUserLogin.GroupId);
                dictParams.Add("USERID", iUserId);
                dictParams.Add("GROUPID", iGroupId);
                dictParams.Add("DSNID", selectedDSNId);

                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetConnectionString("ViewDataSource", oAuthData.ClientId), strSQL.ToString(), dictParams))
                {
                    while (reader.Read())
                    {
                        string strViewKey = reader["VIEW_NAME"].ToString();
                        string strViewID = reader["VIEW_ID"].ToString();

                        //Ensure that an ArgumentException is not thrown
                        //by adding the same View to the Dictionary Collection
                        if (!dictUserViews.ContainsKey(strViewKey))
                        {
                            dictUserViews.Add(strViewKey, strViewID);
                        } // if

                    } // while

                } // using

                //Clean up
                objUserLogin = null;

                return dictUserViews;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }//method: GetUserViews

        /// <summary>
        /// GetSessionDBID
        /// </summary>
        /// <param name="strConnString"></param>
        /// <param name="sUserName"></param>
        /// <param name="sDSN"></param>
        /// <returns></returns>
        private string GetSessionDBID(string strConnString, string sUserName, string sDSN,int iClientId)
        {
            UserLogin objUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            if (string.IsNullOrEmpty(sDSN))
            {
                throw new FaultException("An existing DSN was not found for the specified user", new FaultCode("Sender"));
            }
            objUserLogin = new UserLogin(sUserName, sDSN, iClientId);
            if (objUserLogin == null || objUserLogin.LoginName == string.Empty || objUserLogin.DatabaseId == 0)
            {
                throw new FaultException("A user session was not able to be established.", new FaultCode("Sender"));
            }
            if (objUserLogin.objRiskmasterDatabase.Status)
            {
                if (objUserLogin.GroupId == 0 || !objUserLogin.IsAllowedEx(1))
                {
                    RMException theFault = new RMException();
                    theFault.Errors = formatOutputXML(null, false, systemErrors, iClientId);
                    throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("LoginAdaptor.NoPermission", iClientId)), new FaultCode("Sender"));
                }
            }

            if (objUserLogin.PrivilegesExpire != DateTime.MinValue && objUserLogin.PrivilegesExpire < DateTime.Now)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors, iClientId);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.ForcedPwdExpired",iClientId)), new FaultCode("Sender"));
            }

            bool blnIsExpiredLogin = ValidateLoginPermission(objUserLogin);

            if (blnIsExpiredLogin)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors, iClientId);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.DateTimeExpired",iClientId)), new FaultCode("Sender"));
            }

            List<byte[]> arrSessionBinary = new List<byte[]>();

            arrSessionBinary.Add(Utilities.BinarySerialize(objUserLogin));

            UserLoginLimits objLimits = new UserLoginLimits(objUserLogin, iClientId);

            arrSessionBinary.Add(Utilities.BinarySerialize(objLimits));

            return RMSessionManager.CreateSession(strConnString, sUserName, arrSessionBinary, iClientId);
        }
        //PSARIN2 R8 Perf Imp. New Session Info Get Method
        /// <summary>
        /// Gets the user's session info based on their authentication credentials
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// /// <param name="strDsnId">string containing the selected User's DSN id</param>
        /// <returns>Dictionary<string, string> containing the user's authenticated Session Info</returns>
        public Dictionary<string, string> GetUserSessionInfo(AuthData oAuthData)
        {
            Dictionary<string, string> dictSessionInfo = new Dictionary<string, string>();
            string strSessionID = string.Empty;
            string strCustomViews = string.Empty;
            string sLangCode = string.Empty;
            string sEnableVSS = string.Empty;
            string sCurrentDateSetting = string.Empty; //igupta3
            string sAdjAssignmentAutoDiary = string.Empty;      //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            StringBuilder strParmNames = new StringBuilder();
            string strReturnVal = string.Empty;
            string[] strArrReturnVal = null;
            string sEnableFAS = string.Empty;
            string sFileLocationSelection = string.Empty;
            string sSharedLocation = string.Empty;
            string sFASServer = string.Empty;
            string sFASUserId = string.Empty;
            string sFASPassword = string.Empty;
            string sFASFolder = string.Empty;
            //Ankit End
            bool bBOBSetting = false; 
            try
            {
                strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(oAuthData.ClientId), oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
                dictSessionInfo.Add("SessionId", strSessionID);
                bBOBSetting = GetBOBSetting(oAuthData);
                dictSessionInfo.Add("BOBSetting", bBOBSetting.ToString());
                strCustomViews = GetCustomViews(oAuthData.DsnId.ToString(), oAuthData.ClientId);
                dictSessionInfo.Add("CustomViews", strCustomViews);
                //Deb: Multi Language Changes
                sLangCode = GetLanguageCode(oAuthData);

                //Ankit Start : Worked on MITS - 32386 - FAS Integration
                //sEnableFAS = GetFASSettings(strDSNName, strUserName); remove function call
                //sEnableVSS = GetVSSSettings(strDSNName, strUserName);remove function call
                sEnableFAS = "ENABLE_FAS";
                sEnableVSS = "ENABLE_VSS";
                sFileLocationSelection = "FILE_LOCATION";
                sSharedLocation = "SHARED_LOCATION";
                sFASServer = "FAS_SERVER";
                sFASUserId = "FAS_USER_ID";
                sFASPassword = "FAS_PASSWORD";
                sFASFolder = "FAS_FOLDER";
                strParmNames.Append(string.Concat("'", sEnableVSS, "', "));
                strParmNames.Append(string.Concat("'", sEnableFAS, "', "));
                strParmNames.Append(string.Concat("'", sFileLocationSelection, "', "));
                strParmNames.Append(string.Concat("'", sSharedLocation, "', "));
                strParmNames.Append(string.Concat("'", sFASServer, "', "));
                strParmNames.Append(string.Concat("'", sFASUserId, "', "));
                strParmNames.Append(string.Concat("'", sFASPassword, "', "));
                strParmNames.Append(string.Concat("'", sFASFolder, "'"));
                strReturnVal = GetAllSettings(oAuthData.DsnName, oAuthData.UserName, strParmNames.ToString(), oAuthData.ClientId);
                strArrReturnVal = strReturnVal.Split(new string[] { "~!@" }, StringSplitOptions.None);

                for (int i = 0; i < strArrReturnVal.Length; i++)
                {
                    if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sEnableVSS, "@!~").ToUpper()))
                        sEnableVSS = strArrReturnVal[i].Replace(string.Concat(sEnableVSS, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sEnableFAS, "@!~").ToUpper()))
                        sEnableFAS = strArrReturnVal[i].Replace(string.Concat(sEnableFAS, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFileLocationSelection, "@!~").ToUpper()))
                        sFileLocationSelection = strArrReturnVal[i].Replace(string.Concat(sFileLocationSelection, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sSharedLocation, "@!~").ToUpper()))
                        sSharedLocation = strArrReturnVal[i].Replace(string.Concat(sSharedLocation, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASServer, "@!~").ToUpper()))
                        sFASServer = strArrReturnVal[i].Replace(string.Concat(sFASServer, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASUserId, "@!~").ToUpper()))
                        sFASUserId = strArrReturnVal[i].Replace(string.Concat(sFASUserId, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASPassword, "@!~").ToUpper()))
                        sFASPassword = strArrReturnVal[i].Replace(string.Concat(sFASPassword, "@!~"), string.Empty);
                    else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASFolder, "@!~").ToUpper()))
                        sFASFolder = strArrReturnVal[i].Replace(string.Concat(sFASFolder, "@!~"), string.Empty);
                }
                //Ankit End
                sCurrentDateSetting = GetCurrentDateSetting(oAuthData.DsnName, oAuthData.UserName, oAuthData.ClientId); //igupta3
                sAdjAssignmentAutoDiary = GetAAdjAssignmentAutoDiarySettings(oAuthData);      //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                dictSessionInfo.Add("LanguageCode", sLangCode);
                dictSessionInfo.Add("BaseLanguageCode", RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString());
                dictSessionInfo.Add("EnableVSS", sEnableVSS);
                dictSessionInfo.Add("CurrentDateSetting", sCurrentDateSetting); //igupta3
                dictSessionInfo.Add("AdjAssignmentAutoDiary", sAdjAssignmentAutoDiary);     //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                //Ankit Start : Worked on MITS - 32386 - FAS Integration
                //Check FAS Access
                if (sEnableFAS == "-1")
                {
                    UserLogin objUserLogin = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
                    const int FAS_SMS_PERMISSION = 450000;
                    if (objUserLogin.IsAllowedEx(FAS_SMS_PERMISSION))
                    {
                        sEnableFAS = "-1";
                    }
                    else
                    {
                        sEnableFAS = "0";
                    }
                    objUserLogin = null;
                }
                dictSessionInfo.Add("EnableFAS", sEnableFAS);
                dictSessionInfo.Add("FileLocationSelection", sFileLocationSelection);
                dictSessionInfo.Add("SharedLocation", sSharedLocation);
                dictSessionInfo.Add("FASServer", sFASServer);
                dictSessionInfo.Add("FASUserId", sFASUserId);
                dictSessionInfo.Add("FASPassword", sFASPassword);
                dictSessionInfo.Add("FASFolder", sFASFolder);
                //Ankit End
                //Deb: Multi Language Changes
                return dictSessionInfo;
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        /// <summary>
        ///  returns the GetCustomViews
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="strDatabaseName"></param>
        /// <param name="p_strSelectedDsnId"></param>
        /// <returns></returns>
        public string GetCustomViews(string strSelectedDsnId, int p_iClientId)
        {
            //rsolanki2(may 23,2010): extensibility updates start - retriving views with viewid at -1

            StringBuilder strSQL = new StringBuilder();

            //int selectedDSNId = 0;
            //selectedDSNId = System.Convert.ToInt32(strSelectedDsnId);           
            //Dictionary<string, string> dictUserViews = new Dictionary<string, string>();                        

            strSQL.Append("|");

            using (DbReader reader = DbFactory.ExecuteReader
                (ConfigurationInfo.GetConnectionString("ViewDataSource", p_iClientId)
                , "SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID=-1 AND DATA_SOURCE_ID ="
                    + strSelectedDsnId))
            {
                while (reader.Read())
                {
                    strSQL.Append(reader["FORM_NAME"].ToString());
                    strSQL.Append("|");
                } // while

            } // using
            //dictUserViews.Add("ViewList", strSQL.ToString());

            if (strSQL.Length == 1)
            {
                return string.Empty;
            }
            return strSQL.ToString();

        }//method: GetCustomViews
        /// <summary>
        /// GetCurrentDateSetting for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>string for CurrentDateSetting enable or not</returns>
        public string GetCurrentDateSetting(string p_DSNName, string p_Username,int p_iClientId)
        {
            string sCurrentDateSetting = string.Empty;
            UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName, p_iClientId);
            sCurrentDateSetting = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_CUR_DATE_DIARY'"));
            objUserLogin = null;
            return sCurrentDateSetting;
        }
        //added by Amitosh for Adding carrier claim settings in session
        public bool GetBOBSetting(AuthData oAuthData)
        {
            bool bReturnValue = false;
            SysSettings objSettings = null;
            UserLogin objUserLogin = null;
            try
            {
                objUserLogin = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
                objSettings = new SysSettings(objUserLogin.objRiskmasterDatabase.ConnectionString,oAuthData.ClientId);

                bReturnValue = Conversion.ConvertObjToBool(objSettings.MultiCovgPerClm, oAuthData.ClientId);

            }

            catch
            {
                if (objUserLogin != null)
                    objUserLogin = null;
                if (objSettings != null)
                    objSettings = null;

            }


            return bReturnValue;
        }
        //avipinsrivas: Adjuster Assignment Auto Diary setting for MITS - 29721
        /// <summary>
        /// GetAAdjAssignmentAutoDiarySettings for the Username
        /// </summary>
        public string GetAAdjAssignmentAutoDiarySettings(AuthData oAuthData)
        {
            string sAdjAssignmentAutoDiary = string.Empty;
            UserLogin objUserLogin = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
            sAdjAssignmentAutoDiary = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ADJ_ASIGN_AUTO_DIARY'"));
            objUserLogin = null;
            return sAdjAssignmentAutoDiary;
        }

        /// <summary>
        /// GetBaseLanguageCode
        /// </summary>
        /// <returns>BaseLangCode</returns>
        public string GetBaseLanguageCode()
        {
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString();
            return sBaseLangCode;
        }
        /// <summary>
        /// ValidateLoginPermission
        /// </summary>
        /// <param name="objUserLogin"></param>
        /// <returns></returns>
        private bool ValidateLoginPermission(UserLogin objUserLogin)
        {
            string strCurrentTime = DateTime.Now.ToString("HHmmss");
            string strBeginTime = String.Empty, strEndTime = string.Empty;
            bool bSuccess = false;
            bool bDateTimeExpired = false;
            const string PERMITTED_TIME = "000000";

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    strBeginTime = objUserLogin.SunStart;
                    strEndTime = objUserLogin.SunEnd;
                    break;
                case DayOfWeek.Monday:
                    strBeginTime = objUserLogin.MonStart;
                    strEndTime = objUserLogin.MonEnd;
                    break;
                case DayOfWeek.Tuesday:
                    strBeginTime = objUserLogin.TueStart;
                    strEndTime = objUserLogin.TueEnd;
                    break;
                case DayOfWeek.Wednesday:
                    strBeginTime = objUserLogin.WedStart;
                    strEndTime = objUserLogin.WedEnd;
                    break;
                case DayOfWeek.Thursday:
                    strBeginTime = objUserLogin.ThuStart;
                    strEndTime = objUserLogin.ThuEnd;
                    break;
                case DayOfWeek.Friday:
                    strBeginTime = objUserLogin.FriStart;
                    strEndTime = objUserLogin.FriEnd;
                    break;
                case DayOfWeek.Saturday:
                    strBeginTime = objUserLogin.SatStart;
                    strEndTime = objUserLogin.SatEnd;
                    break;
            }

            if (!strBeginTime.Equals(PERMITTED_TIME) || !strEndTime.Equals(PERMITTED_TIME))
            {
                if (strEndTime.Equals(PERMITTED_TIME))
                {
                    if (Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess))
                    {
                        bDateTimeExpired = true;
                    }//if
                }//if
                else if ((Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess)) || (Conversion.CastToType<long>(strCurrentTime, out bSuccess) > Conversion.CastToType<long>(strEndTime, out bSuccess)))
                {
                    bDateTimeExpired = true;
                }//else if
            }//if

            return bDateTimeExpired;
        }//method: ValidateLoginPermission()
        /// <summary>
        /// formatOutputXML
        /// </summary>
        /// <param name="xmlOut"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        protected string formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors, int iClientId)
        {
            XmlDocument xmlOutEnv = new XmlDocument();
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);
            formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors, iClientId);
            XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
            xmlRoot.AppendChild(xmlDocEnv);

            if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
                xmlDocEnv.InnerXml = xmlOut.OuterXml;

            return xmlOutEnv.OuterXml;
        }
        /// <summary>
        /// Use this Fucntion in order to extract any value from DB via authservervice Don't create new fucntion
        /// </summary>
        /// <param name="p_DSNName"></param>
        /// <param name="p_Username"></param>
        /// <param name="sWhereCondition"></param>
        /// <returns></returns>
        private string GetAllSettings(string p_DSNName, string p_Username, string sWhereCondition, int iClientId)
        {
            string sSQL = string.Empty;
            DataSet objDataSet = null;
            string strReturnValues = string.Empty;
            UserLogin objUserLogin = null;
            try
            {
                objUserLogin = new UserLogin(p_Username, p_DSNName, iClientId);
                sSQL = string.Concat("SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME IN (", sWhereCondition, ")");

                objDataSet = DbFactory.GetDataSet(objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL,iClientId);
                if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drMgr in objDataSet.Tables[0].Rows)
                    {
                        if (string.IsNullOrEmpty(strReturnValues))
                            strReturnValues = string.Concat(strReturnValues, string.Concat(drMgr["PARM_NAME"].ToString(), "@!~", drMgr["STR_PARM_VALUE"].ToString()));
                        else
                            strReturnValues = string.Concat(strReturnValues, "~!@", string.Concat(drMgr["PARM_NAME"].ToString(), "@!~", drMgr["STR_PARM_VALUE"].ToString()));
                    }
                }

                return strReturnValues;
            }
            finally
            {
                //clean up
                objUserLogin = null;
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
            }
        }
        /// <summary>
        /// GetLanguageCode for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>LanguageCode</returns>
        public string GetLanguageCode(AuthData oAuthData)
        {
            UserLogin objUserLogin = null;
            string sLangCodeAndCulture = string.Empty;
            string sBaseLangCode = "1033|en-US";
            try
            {
                sBaseLangCode = GetBaseLanguageCode();
                objUserLogin = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);

                if (objUserLogin.objUser.NlsCode != int.Parse(sBaseLangCode.Split('|')[0]))
                {
                    sLangCodeAndCulture = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(oAuthData.ClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + objUserLogin.objUser.NlsCode));
                }
                else
                {
                    sLangCodeAndCulture = sBaseLangCode.Split('|')[1];
                }
                return Convert.ToString(objUserLogin.objUser.NlsCode + "|" + sLangCodeAndCulture);
            }
            catch (Exception ee)
            {
                return sBaseLangCode;
            }
            finally
            {
                objUserLogin = null;
            }
        }
        /// <summary>
        /// Logs out a user and removes their session information from the database
        /// </summary>
        /// <param name="strSessionID">string containing the current user's Session ID</param>
        public void LogOutUser(AuthData oAuthData)
        {
            RMSessionManager.RemoveSession(ConfigurationInfo.GetSessionConnectionString(oAuthData.ClientId), oAuthData.Token, string.Empty);
        } // method: LogOutUser
       
        /// <summary>
        /// formatErrorXML
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlRoot"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        protected void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors, int iClientId)
        {
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;

            XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);

            xmlElement = xmlDoc.CreateElement("MsgStatusCd");
            if (bCallResult)
                xmlElement.InnerText = "Success";
            else
                xmlElement.InnerText = "Error";

            xmlErrRoot.AppendChild(xmlElement);

            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                    xmlErrRoot.AppendChild(xmlMsgsRoot);

                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // ... add error code/number
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.ErrorCode;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        xmlElement.InnerText = err.ErrorDescription;
                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        // Determine error code - assembly + exception type
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.oException.Source + "." + err.oException.GetType().Name;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        if (err.ErrorDescription != "")
                            xmlElement.InnerText = err.ErrorDescription;
                        else
                            xmlElement.InnerText = err.oException.Message;

                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // Exception case

                    // ...add error type
                    xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            xmlElement.InnerText = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            xmlElement.InnerText = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            xmlElement.InnerText = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            xmlElement.InnerText = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            xmlElement.InnerText = "PopupMessage";
                            break;
                        default:
                            // TODO   - What to do if not a standard error code?
                            break;
                    };
                    xmlMsgsRoot.AppendChild(xmlElement);
                }
            }
        }
        
        //nsharma202
        public string GetUserName(AuthData oAuthData)
        {
            string fullName = string.Empty;
            
            Dictionary<string, string> parms = new Dictionary<string, string>();
            try
            {
                parms.Add("USERNAME", oAuthData.UserName);
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT FIRST_NAME, LAST_NAME FROM USER_DETAILS_TABLE,USER_TABLE "+
                "WHERE USER_DETAILS_TABLE.DSNID='"+oAuthData.DsnId+"' AND USER_DETAILS_TABLE.USER_ID=USER_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME='" + oAuthData.UserName + "'");
                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(oAuthData.ClientId), sbSQL.ToString(), parms))
                {
                    if(reader.Read())
                    {
                        fullName = reader["FIRST_NAME"].ToString()+" "+reader["LAST_NAME"].ToString();
                    }

                }
                if (!string.IsNullOrEmpty(fullName))
                {
                    return fullName;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception exe)
            {
                return exe.ToString();
            }
            
        }
    }
}
