﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using Riskmaster.Db;
using System.ServiceModel.Activation;
using Riskmaster.Security;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CustomLogin" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CustomLogin" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CustomLogin.svc or CustomLogin.svc.cs at the Solution Explorer and start debugging.
    public class CustomLogin : ICustomLogin
    {
        public string GetRecordID(AuthData oAuthData)
        {
            string sSQL = string.Empty;
            string sClaimId = string.Empty;
            //UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName);     
            UserLogin objUserLogin = new UserLogin(oAuthData.UserName, oAuthData.DsnName, oAuthData.ClientId);
            //sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + sNumber + "'";
            sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + oAuthData.ClaimNum + "'";
            sClaimId = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
            objUserLogin = null;
            return sClaimId;
        }
    }
}
