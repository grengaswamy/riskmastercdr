﻿//#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Models;
using Riskmaster.Security;
using Riskmaster.Security.Authentication;
using Riskmaster.Security.RMApp;
using Riskmaster.Settings;
using System.Xml;

using Riskmaster.Cache;

namespace RiskmasterService
{
    // NOTE: If you change the class name "AuthenticationService" here, you must also update the reference to "AuthenticationService" in Web.config.
    /// <summary>
    /// Provides the ability to authenticate users against a specified
    /// RISKMASTER Authentication/Authorization repository
    /// </summary>
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class MobileAuthenticationService : RMService, IMobileAuthenticationService
    {
        #region IMobileAuthenticationService Members
        /// <summary>
        /// Authenticate Mobile User
        /// </summary>
        /// <param name="xmlRequest"></param>
        /// <param name="xmlRepsonse"></param>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        public string AuthenticateMobileUser(string xmlRequest)
        {
            XmlDocument objXmlRequest = new XmlDocument();
            
            string strDecodedUserName = string.Empty;
            string strDecodedDSNName = string.Empty;
            string strUserSessionId = string.Empty;
            string strResponse = string.Empty;
            XmlNode objXmlUserName  = null;
            XmlNode objXmlDSNName = null;
                        
            try
            {
                objXmlRequest.LoadXml(xmlRequest);

                objXmlUserName = objXmlRequest.SelectSingleNode("//UserName");
                objXmlDSNName = objXmlRequest.SelectSingleNode("//DSNName");
            
                if (objXmlUserName != null)
                    strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlUserName.InnerText);

                if (objXmlDSNName != null)
                    strDecodedDSNName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlDSNName.InnerText); 
                
                strUserSessionId = GetUserSessionID(strDecodedUserName.Trim(), strDecodedDSNName.Trim(),0); // Windows app is not cloud enabled
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Success</MsgStatusCd></MsgStatus><Document><result category=\"Authorization\"><Token>" + strUserSessionId + "</Token><LoggedUser>" + strDecodedUserName.Trim() + "</LoggedUser><LoginTime>" + DateTime.Now.ToString() + "</LoginTime></result></Document></ResultMessage>";                
            }
            catch (Exception ex)
            {  
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Error</MsgStatusCd><ExtendedStatus><ExtendedStatusCd></ExtendedStatusCd><ExtendedStatusDesc>" + ex.Message + "</ExtendedStatusDesc><ExtendedMsgType>Error</ExtendedMsgType></ExtendedStatus></MsgStatus></ResultMessage>";          
            }

            return strResponse;
            

            
        }          

        /// <summary>
        /// Retrieves the list of DSNs for a specific user
        /// </summary>
        /// <param name="strUserName">string containing the logged in user name</param>
        /// <returns>Generic String Dictionary collection containing all of the User DSNs</returns>
        public Dictionary<string, string> GetUserDSNs(string strUserName, int iClientId)
        {
            Dictionary<string, string> dictUserDbs = new Dictionary<string,string>();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, DATA_SOURCE_TABLE.DSNID ");
            strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE ");
            strSQL.Append("WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            strSQL.AppendFormat("AND USER_DETAILS_TABLE.LOGIN_NAME={0} ", "~USERNAME~");
            strSQL.Append("ORDER BY DATA_SOURCE_TABLE.DSN");

            dictParams.Add("USERNAME", strUserName);

            //TODO: Migrate this code over to the Riskmaster.Security.Authentication library
            using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(iClientId), strSQL.ToString(), dictParams))
            {
                while (reader.Read())
                {
                    string strDSNKey = reader["DSN"].ToString();
                    string strDSNID = reader.GetInt32("DSNID").ToString();

                    //Ensure that an ArgumentException is not thrown
                    //by adding the same DSN to the Dictionary Collection
                    if (! dictUserDbs.ContainsKey(strDSNKey))
                    {
                        dictUserDbs.Add(strDSNKey, strDSNID);
                    } // if

                } // while

                //Close the DataReader
                reader.Close();
            } // using

            return dictUserDbs;
        }//method: GetUserDSNs
        
        /// <summary>
        /// Gets the user's session ID based on their authentication credentials
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// <returns>string containing the user's authenticated Session ID</returns>
        public string GetUserSessionID(string strUserName, string strDSNName, int iClientId)
        {
            string strSessionID = string.Empty;
            strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(iClientId), strUserName, strDSNName,iClientId);

            return strSessionID;
        }//method: GetUserSessionID()

        
        /// <summary>
        /// Gets the necessary Session Database ID based on User Credentials
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="sUserName">string containing the logged in user name</param>
        ///<param name="sDSN">string containing the name of the DSN</param> 
        /// <returns>string containing the Session ID from the RISKMASTER Session database</returns>
        private string GetSessionDBID(string strConnString, string sUserName, string sDSN, int iClientId)
        {
            UserLogin objUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            /***************************************************
                Kick user out if a Data Source not specified.
            **************************************************/
            if (string.IsNullOrEmpty(sDSN))
            {
                throw new FaultException("An existing DSN was not found for the specified user", new FaultCode("Sender"));
            }

            /***************************************************
                Authenticate against the new Data Source
             **************************************************/

            objUserLogin = new UserLogin(sUserName, sDSN, iClientId);
            //Raman 12/18/2009 : Since LSS bypasses our authentication (it is running in trusted mode)
            //hence if the loginname or database name is not correct still userlogin object gets created
            //adding check for loginname and databaseid for raising the exception

            if (objUserLogin == null || objUserLogin.LoginName == string.Empty || objUserLogin.DatabaseId == 0)
            {
                throw new FaultException("A user session was not able to be established.", new FaultCode("Sender"));
            }

            /***************************************************
                Is Module Level Security Active?
             **************************************************/
            if (objUserLogin.objRiskmasterDatabase.Status)
            {
                if (objUserLogin.GroupId == 0 || !objUserLogin.IsAllowedEx(1))//User is not in a	 valid group? Or Group has no permission? 
                {
                    RMException theFault = new RMException();
                    theFault.Errors = formatOutputXML(null, false, systemErrors);
                    throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("LoginAdaptor.NoPermission", iClientId)), new FaultCode("Sender"));
                }
            } // if

            //Raman Bhatia 04/22/2009: Current Date and expiry date validations should be a part of AUTHORIZATION and not AUTHENTICATION
            //These settings work at a DataSource level and NOT during Authentication
            //Implementing logic here

            if (objUserLogin.PrivilegesExpire != DateTime.MinValue && objUserLogin.PrivilegesExpire < DateTime.Now)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.ForcedPwdExpired", iClientId)), new FaultCode("Sender"));
            }

            bool blnIsExpiredLogin = ValidateLoginPermission(objUserLogin);

            if (blnIsExpiredLogin)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.DateTimeExpired", iClientId)), new FaultCode("Sender"));
            }

            List<byte[]> arrSessionBinary = new List<byte[]>();
            
            arrSessionBinary.Add(Utilities.BinarySerialize(objUserLogin));
            
            // JP 6/15/2005   *Added - Begin*
            // Create and serialize other objects that RISKMASTER specifically needs. AC will have its own list and it will be different in the AC LoginAdaptor.   JP 6/15/2005
            // ... create UserLoginLimits object
            UserLoginLimits objLimits = new UserLoginLimits(objUserLogin, iClientId);

            arrSessionBinary.Add(Utilities.BinarySerialize(objLimits));

            return RMSessionManager.CreateSession(strConnString, sUserName, arrSessionBinary, iClientId);
        }//method: GetSessionDBID()

        /// <summary>
        /// Verifies that the user is permitted to log into the system
        /// </summary>
        /// <param name="objUserLogin">User Login object</param>
        /// <returns>boolean indicating whether or not the user is permitted
        /// to log into the system at the current time</returns>
        private bool ValidateLoginPermission(UserLogin objUserLogin)
        {
            string strCurrentTime = DateTime.Now.ToString("HHmmss");
            string strBeginTime = String.Empty, strEndTime = string.Empty;
            bool bSuccess = false;
            bool bDateTimeExpired = false;
            const string PERMITTED_TIME = "000000";

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    strBeginTime = objUserLogin.SunStart;
                    strEndTime = objUserLogin.SunEnd;
                    break;
                case DayOfWeek.Monday:
                    strBeginTime = objUserLogin.MonStart;
                    strEndTime = objUserLogin.MonEnd;
                    break;
                case DayOfWeek.Tuesday:
                    strBeginTime = objUserLogin.TueStart;
                    strEndTime = objUserLogin.TueEnd;
                    break;
                case DayOfWeek.Wednesday:
                    strBeginTime = objUserLogin.WedStart;
                    strEndTime = objUserLogin.WedEnd;
                    break;
                case DayOfWeek.Thursday:
                    strBeginTime = objUserLogin.ThuStart;
                    strEndTime = objUserLogin.ThuEnd;
                    break;
                case DayOfWeek.Friday:
                    strBeginTime = objUserLogin.FriStart;
                    strEndTime = objUserLogin.FriEnd;
                    break;
                case DayOfWeek.Saturday:
                    strBeginTime = objUserLogin.SatStart;
                    strEndTime = objUserLogin.SatEnd;
                    break;
            }

            if (!strBeginTime.Equals(PERMITTED_TIME) || !strEndTime.Equals(PERMITTED_TIME))
            {
                if (strEndTime.Equals(PERMITTED_TIME))
                {
                    if (Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess))
                    {
                        bDateTimeExpired = true;
                    }//if
                }//if
                else if ((Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess)) || (Conversion.CastToType<long>(strCurrentTime, out bSuccess) > Conversion.CastToType<long>(strEndTime, out bSuccess)))
                {
                    bDateTimeExpired = true;
                }//else if
            }//if

            return bDateTimeExpired;
        }//method: ValidateLoginPermission()

        //Amandeep Mobile Adjuster 07/31/2012
        /// <summary>
        /// Retrieves the list of DSNs for a specific user
        /// </summary>
        /// <param name="strUserName">string containing the logged in user name</param>
        /// <returns>Generic String Dictionary collection containing all of the User DSNs</returns>
        public string GetUserDSNsMobileAdjuster(string xmlRequest)
        {
            bool blnIsAuthenticated = false;
            Dictionary<string, string> dictUserDbs = new Dictionary<string, string>();
            XmlDocument objXmlRequest = null;
            XmlDocument objXmlResponse = null;
            XmlNode objUserName = null;
            XmlNode objPassword = null;
            string strDecodedUserName = string.Empty;
            string strDecodedPassword = string.Empty;
            string strMsg = string.Empty;
            string strResponse = string.Empty;
            int iClientId = 0;
            try
            {
                objXmlRequest = new XmlDocument();
                objXmlResponse = new XmlDocument();
                objXmlRequest.LoadXml(xmlRequest);
                objXmlResponse.LoadXml("<ResultMessage><MsgStatus><MsgStatusCd>Success</MsgStatusCd></MsgStatus><Document><DSNList></DSNList></Document></ResultMessage>");
                objUserName = objXmlRequest.SelectSingleNode("//UserName");
                objPassword = objXmlRequest.SelectSingleNode("//Password");

                if (objUserName != null)
                    strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(objUserName.InnerText);
                if (objPassword != null)
                    strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(objPassword.InnerText);
                //windows app is not cloud enabled, clientId=0
                blnIsAuthenticated = MembershipProviderFactory.AuthenticateUser(ConfigurationInfo.GetSecurityConnectionString(0), strDecodedUserName, strDecodedPassword, out strMsg, iClientId);

                if (blnIsAuthenticated)
                {
                    dictUserDbs = GetUserDSNs(strDecodedUserName, 0); //windows app is not cloud enabled, clientId=0
                    string[] sUserDbs = new string[dictUserDbs.Keys.Count];
                    dictUserDbs.Keys.CopyTo(sUserDbs, 0);
                    foreach (string sDSN in sUserDbs)
                    {
                        XmlElement objXElement = objXmlResponse.CreateElement("DSNName");
                        objXElement.InnerText = sDSN;
                        XmlNode objDSNListNode = objXmlResponse.SelectSingleNode("//DSNList");
                        objDSNListNode.AppendChild(objXElement);
                    }
                    strResponse = objXmlResponse.OuterXml.ToString();
                }
                else
                {
                    strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Error</MsgStatusCd><ExtendedStatus><ExtendedStatusCd></ExtendedStatusCd><ExtendedStatusDesc>Your login attempt was not successful. Please try again.</ExtendedStatusDesc><ExtendedMsgType>Error</ExtendedMsgType></ExtendedStatus></MsgStatus><Document><DSNList></DSNList></Document></ResultMessage>";
                }
            }
            catch (Exception ex)
            {
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Error</MsgStatusCd><ExtendedStatus><ExtendedStatusCd></ExtendedStatusCd><ExtendedStatusDesc>" + ex.Message + "</ExtendedStatusDesc><ExtendedMsgType>Error</ExtendedMsgType></ExtendedStatus></MsgStatus><Document><DSNList></DSNList></Document></ResultMessage>";
            }
            return strResponse;
        }
        #endregion
        
    }
}
