﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IVPPService" here, you must also update the reference to "IVPPService" in Web.config.
    [ServiceContract]
    public interface IVPP
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/filecontent", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        VPPFileResponse GetFileContent(VPPFileRequest oVPPFileRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/inittimestamp", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        VPPTimeStampResponse GetInitialTimeStamp(RMServiceType oRMServiceType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/filetimestamp", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        VPPTimeStampResponse GetFileTimeStamp(VPPFileRequest oVPPFileRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/modifiedpages", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        VPPModifiedPagesResponse GetModifiedPageList(VPPModifiedPagesRequest oVPPModifiedPagesRequest);
 
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/dsnid", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        VPPGetDataSourceIDResponse GetDataSourceID(RMServiceType oRMServiceType);
    }
}
