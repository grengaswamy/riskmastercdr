﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICommonWCFService" here, you must also update the reference to "ICommonWCFService" in Web.config.
    [ServiceContract(Namespace = "http://csc.com/Riskmaster/Webservice/Common")]
    public interface ICommonWCFService
    {
        [OperationContract]
        string ProcessRequest(string xmlRequest);
    }
}