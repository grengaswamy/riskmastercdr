﻿using System;
using System.Data;
using System.ServiceModel.Activation;
using Riskmaster.Db;
using Riskmaster.Models;

using Riskmaster.Cache;
using System.IO;
namespace RiskmasterService
{
    /// <summary>
    /// Portal
    /// Created By Deb
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Portal : RMService, IPortal
    {
        /// <summary>
        // {
        //  "oPortalData":
        //  {
        //    "Sql" : "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'",
        //    "ClientId" :1
        //  }
        //}
        /// </summary>
        /// <param name="oPortal"></param>
        /// <returns></returns>
        public string ContentInfo(PortalData oPortalData)
        {
            string Content = string.Empty;
            string strConn = ConfigurationInfo.GetSessionConnectionString(oPortalData.ClientId);
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strConn, oPortalData.Sql))
                {
                    objReader.Read();
                    Content = objReader[0].ToString();
                }
                return Content;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// HomeContentInfo
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        public PortalData HomeContentInfo(PortalData oPortalData)
        {
            string Content = string.Empty;
            string strConn = ConfigurationInfo.GetSessionConnectionString(oPortalData.ClientId);
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strConn, oPortalData.Sql))
                {
                    if (objReader.Read())
                    {
                        //objReader.Read();
                        oPortalData.Content = objReader[0].ToString();
                        oPortalData.FileContent = objReader.GetAllBytes("FILE_CONTENT");
                    }
                }
                return oPortalData;
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// FileContent
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        public byte[] FileContent(PortalData oPortalData)
        {
            byte[] Content = null;
            string strConn = ConfigurationInfo.GetSessionConnectionString(oPortalData.ClientId);
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strConn, oPortalData.Sql))
                {
                    objReader.Read();
                    Content = objReader.GetAllBytes("FILE_CONTENT");
                }
                return Content;
            }
            catch (Exception)
            {
                return Content;
            }
        }

        private string getNextCustomizeId(int iClientId)
        {
            string strConn = ConfigurationInfo.GetSessionConnectionString(iClientId);
            return DbFactory.ExecuteScalar(strConn, "SELECT MAX(ID) + 1 FROM CUSTOMIZE").ToString();
        }

        /// <summary>
        /// ContentUpdate
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        public void ContentUpdate(PortalData oPortalData)
        {
            if (!ValidateSession(oPortalData.Token, oPortalData.ClientId)) return;
            string strConn = ConfigurationInfo.GetSessionConnectionString(oPortalData.ClientId);
            using (DbConnection objConn = DbFactory.GetDbConnection(strConn))
            {
                objConn.Open();

                using (DbCommand objCmd = objConn.CreateCommand())
                {
                    DbParameter objParam = null;
                    string sSql = "UPDATE CUSTOMIZE SET CONTENT = ~XML~ WHERE UPPER(FILENAME)=UPPER('" + oPortalData.FileName + "')";

                    objParam = objCmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = oPortalData.Content;
                    objParam.ParameterName = "XML";
                    objParam.SourceColumn = "CONTENT";
                    objCmd.CommandText = sSql;
                    objCmd.Parameters.Add(objParam);
                    objCmd.ExecuteNonQuery();

                    if (oPortalData.FileContent != null  && oPortalData.FileContent.Length > 0)
                    {
                        sSql = "UPDATE CUSTOMIZE SET FILE_CONTENT = ~FILE~ WHERE UPPER(FILENAME)=UPPER('CUSTOMIZE_HOMEPAGE')";
                        objParam = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = oPortalData.FileContent;
                        objParam.ParameterName = "FILE";
                        objParam.SourceColumn = "FILE_CONTENT";
                        objCmd.CommandText = sSql;
                        objCmd.Parameters.Add(objParam);
                        objCmd.ExecuteNonQuery();
                    }
                }
            }
        }
        /// <summary>
        /// ContentInsert
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        public void ContentInsert(PortalData oPortalData)
        {
            //if (!ValidateSession(oPortalData.Token, oPortalData.ClientId)) return;
            string strConn = ConfigurationInfo.GetSessionConnectionString(oPortalData.ClientId);
            DbConnection objConn = DbFactory.GetDbConnection(strConn); ;
            DbCommand objCmd = null;
            DbParameter objParam = null;

            objConn.Open();

            string sNextCustomizeId = getNextCustomizeId(oPortalData.ClientId);

            string sSql = "INSERT INTO CUSTOMIZE( ID ,FILENAME ,CONTENT, TYPE, IS_BINARY ) VALUES ("
                + sNextCustomizeId
                + ", '" + oPortalData.FileName + "'" //;'customize_Homepage' ,"
                + ",~XML~"
                + ",0,0)";
            //+ ",~XML~)";

            objCmd = objConn.CreateCommand();
            objParam = objCmd.CreateParameter();
            objParam.Direction = ParameterDirection.Input;
            objParam.Value = oPortalData.Content;
            objParam.ParameterName = "XML";
            objParam.SourceColumn = "CONTENT";

            objCmd.CommandText = sSql;
            objCmd.Parameters.Add(objParam);
            objCmd.ExecuteNonQuery();

            //need to update next val in DB
            UpdateId(sNextCustomizeId, oPortalData.ClientId);
        }

        private void UpdateId(string sId, int iClientId)
        {
            string strConn = ConfigurationInfo.GetSessionConnectionString(iClientId);
            int iNextCustomizeId = Convert.ToInt32(sId) + 1;
            string sSql = "UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = "
                + iNextCustomizeId.ToString()
                + " WHERE UPPER(SYSTEM_TABLE_NAME) =UPPER('CUSTOMIZE')";
            DbFactory.ExecuteNonQuery(strConn, sSql);
        }

    }
}
