﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;


namespace RiskmasterService
{
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    public class DataIntegratorService : RMService, IDataIntegratorService
    {
        public void SaveSettings(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "SaveSettings";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objSettings = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objSettings, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                objDIModel = objSettings.SaveSettings(request);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(sFunctionName, xmlRequest, true, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, true, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

        public DataIntegratorModel RetrieveSettings(DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveSettings";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDISettings = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(objDIModel, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
                //objDIModel = objSettings.RetrieveSettings();
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                objDISettings = objSettings.RetrieveSettings(objDIModel, ref objErrOut);
            }
            catch (Exception exp)
            {
                objErrOut.Add(exp, BusinessAdaptorErrorType.Message);
                logErrors(functionName, xmlRequest, false, objErrOut);
                throw exp;
            }
            return objDISettings;
        }

        //sagarwal54 MITS 35386 start
        public string GetrmAPropTypeAndISOPropTypeServiceFunc(DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetrmAPropTypeAndISOPropType";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDISettings = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(objDIModel, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
                //objDIModel = objSettings.RetrieveSettings();
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                objDISettings = objSettings.GetrmAPropTypeAndISOPropType(objDIModel);
            }
            catch (Exception exp)
            {
                objErrOut.Add(exp, BusinessAdaptorErrorType.Message);
                logErrors(functionName, xmlRequest, false, objErrOut);
                throw exp;
            }
            return objDISettings.dsrmAPropCodes.GetXml();
        }
        //sagarwal54 MITS 35386 End

        //Sagarwal54 03/18/2014 MITS 35704 Start
        public string GetAdditionalClaimantRoleType(DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetISOClaimPartyType";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDISettings = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(objDIModel, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
                //objDIModel = objSettings.RetrieveSettings();
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                objDISettings = objSettings.GetAdditionalClaimantRoleCodes(objDIModel);
            }
            catch (Exception exp)
            {
                objErrOut.Add(exp, BusinessAdaptorErrorType.Message);
                logErrors(functionName, xmlRequest, false, objErrOut);
                throw exp;
            }
            return objDISettings.dsAdditionalClaimantCodes.GetXml();
        }
        //Sagarwal54 03/18/2014 MITS 35704 End
        public void GetAccountList(PPAccountList request, out PPAccountList objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetAccountList";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegrator = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new PPAccountList();
                objDataIntegrator = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDataIntegrator, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                //          bResult = objDataIntegrator.test(request);
                bResult = objDataIntegrator.GetPositivePayAccountList(request);
                objReturn = request;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

        public void RetrieveJobFiles(JobFile request, out JobFile objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveJobFiles";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new JobFile();
                objSettings = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objSettings.RetrieveJobFile(request, out  objReturn);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

        public int SetClaimForReplacement(DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDIModel, objDIAdaptor, out oUserLogin, ref systemErrors);
                iResult = objDIAdaptor.SetClaimForReplacement(objDIModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return iResult;
        }

        public int ResetClaimForInitialSubmit(DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDIModel, objDIAdaptor, out oUserLogin, ref systemErrors);
                iResult = objDIAdaptor.ResetClaimForInitialSubmit(objDIModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return iResult;
        }

        public void GetISOLossMappings(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetISOLossMappings(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetRMClaimTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetRMClaimTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetCoverageTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetCoverageTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetLossTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetLossTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void SaveRMLossTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.SaveRMLossTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetClaimantTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetClaimantTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void SaveClaimantTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.SaveClaimantTypeMapping(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        //pmittal5 - Retreive Fields for Admin Tracking Table selected on DIS screen
        public void GetAdminTrackFields(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetAdminTrackFields(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetBankAccountList(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetBankAccountList(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        public void GetReserveList4LOB(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetReserveList4LOB(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void UpdateDAStagingDatabase(DataIntegratorModel objDImodel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "UpdateDAStagingDatabase";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDImodel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDImodel, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIAdaptor.UpdateDAStagingDatabase(objDImodel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

	//kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder

        public void RetrieveFile(DataIntegratorModel objDImodel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RetrieveFile";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDImodel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDImodel, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIAdaptor.UpdateDAStagingDatabase(objDImodel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
	//kkaur25 end UI JIRA 9907 for retrieveing file from db and saving to DataAnalytics folder


        public void PeekDAStagingDatabase(DataIntegratorModel objDImodel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "PeekDAStagingDatabase";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDImodel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDImodel, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIAdaptor.PeekDAStagingDatabase(objDImodel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public string GetRMConfiguratorDATempPath(DataIntegratorModel objDImodel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetRMConfiguratorDATempPath";
            XmlDocument xmlRequest = null;
            string sRMConfiguratorDATempPath = string.Empty;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDImodel.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDImodel, objDIAdaptor, out oUserLogin, ref systemErrors);
                sRMConfiguratorDATempPath = objDIAdaptor.GetRMConfiguratorDATempPath();
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
            }
            return sRMConfiguratorDATempPath;
        }

        //Vsoni5 : MITS 22699: upload the import job file to Business layer.
        public void UploadDAImportFile(DAImportFile objDAImportFile)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDAAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDAImportFile.ClientId);
                //objReturn = new RMServiceType();
                objDAAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(objDAImportFile, out xmlRequest, functionName, objDAAdaptor, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, objDAImportFile.ClientId);
                objDAAdaptor.UploadDAImportFile(objDAImportFile);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="objDIModel">Object of DI Model</param>
        /// <returns></returns>
        /// 
        public void SaveSettingsCleanup(DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "SaveSettingsCleanup";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objerrOut = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objerrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                objSettings.SaveSettingsCleanup(objDIModel, ref objerrOut);
            }
            catch (Exception e)
            {
                //Throw back error if business adaptor isn't trapping its own exceptions.
                //systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, objerrOut);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, objerrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(sFunctionName, xmlRequest, true, objerrOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (objerrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, true, objerrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 25785.
        /// </summary>
        /// <param name="objDIModel">object of DataIntegratorModel Class</param>
        /// <param name="sEntityList">Comma seperated list of Payer Entity Ids selected on UI</param>
        /// <returns>This function return dataset containing TaxID, Abbr. and Last Name of payer</returns>
        public System.Data.DataSet GetTaxIds(DataIntegratorModel objDIModel, string sEntityList)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "GetTaxIds";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objerrOut = null;            
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objerrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                return objSettings.GetTaxIds(sEntityList);
            }
            catch (Exception e)
            {
                //Throw back error if business adaptor isn't trapping its own exceptions.
                //systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, objerrOut);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, objerrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            return null;
        } 

        //mihtesham start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="objDIModel"></param>
        public void GetPolicyLOB(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetPolicyLOB(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        // Developer - abharti5 |MITS 36676| start
        public void GetPolicySystemNames(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetPolicySystemNames(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        // Developer - abharti5 |MITS 36676| end

        public void GetPolicyClaimType(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetPolicyClaimType(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetWCPolicyType(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetWCPolicyType(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public int GetPolicyClaimLOBType(DataIntegratorModel request)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                
                return objDIAdaptor.GetPolicyClaimLOBType(request.iPolicyType);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public void GetPolicyCoverageType(DataIntegratorModel request, out DataIntegratorModel objDIModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDIAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDIAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, objDIAdaptor, out oUserLogin, ref systemErrors);
                objDIModel = objDIAdaptor.GetPolicyCoverageType(request);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        //mihtesham end
        //mihtesham MITS 35711 start
        public string GetrmASPRoleAndISOSPRoleServiceFunc(DataIntegratorModel objDIModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetrmASPRoleAndISOSPRole";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objSettings = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDISettings = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                objSettings = new DataIntegratorAdaptor();
                systemErrors = new BusinessAdaptorErrors(objDIModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(objDIModel, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
                //objDIModel = objSettings.RetrieveSettings();
                InitiateServiceProcess(objDIModel, objSettings, out oUserLogin, ref systemErrors);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, objDIModel.ClientId);
                objDISettings = objSettings.GetrmASPRoleAndISOSPRole(objDIModel);
            }
            catch (Exception exp)
            {
                objErrOut.Add(exp, BusinessAdaptorErrorType.Message);
                logErrors(functionName, xmlRequest, false, objErrOut);
                throw exp;
            }
            return objDISettings.dsrmASPRoleCodes.GetXml();
        }
        //mihtesham MITS 35711 End
    }
}