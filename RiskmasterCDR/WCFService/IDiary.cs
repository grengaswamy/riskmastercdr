﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;
using System.IO;

namespace RiskmasterService
{
    [ServiceContract]
    public interface IDiary
    {        
        /// <summary>
        /// GetDiaryList
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/list", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DiaryOutput GetDiaryList(DiaryInput oDiaryInput);

        /// <summary>
        /// GetCalendarView
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/calendarview", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DiaryOutput GetCalendarView(DiaryInput oDiaryInput);

        /// <summary>
        /// GetOnLoadInformation
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/info", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DiaryOutput GetOnLoadInformation(DiaryInput oDiaryInput);
        
        //nsharma202 : RMA-1638
        /// <summary>
        /// Get Reportees under a manager
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetReportees", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream GetReportees();
    }
}
