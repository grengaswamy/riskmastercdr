﻿using System;
using System.Data;
using System.ServiceModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Runtime.Serialization;
using Riskmaster.Settings;


namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PolicyInterfaceService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(
     RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif

    public class IntegralInterfaceService : RMService, IIntegralInterfaceService
    {
        public void GetPolicySearchResult(PolicySearch oSearchRequest, out PolicySearch oSearchResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            IntegralSystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicySearchSoapRequest";
            bool bresult = false;
            string sSearchSoapRequest = string.Empty;
            string oSearchSoapResponse = string.Empty;
            DataTable dtPolicySystemInfo = null;
            string sPolicySystemtype = string.Empty;
            oSearchResponse = null;
            int iResultCount;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSearchRequest.ClientId);
                objPolicySystemAdapter = new IntegralSystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSearchRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSearchRequest.ClientId);
                dtPolicySystemInfo = objPolicySystemAdapter.GetPolicySystemInfoById(oSearchRequest.objSearchFilters.PolicySystemId, ref oSystemErrors);
                if (dtPolicySystemInfo != null && dtPolicySystemInfo.Rows.Count > 0)
                    sPolicySystemtype = dtPolicySystemInfo.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();

                switch (CommonFunctions.GetPolicySystemTypeIndicator(sPolicySystemtype))
                {
                    case Constants.POLICY_SYSTEM_TYPE.INTEGRAL:
                        
                        //bram4 - getting the policy count set in general parameter setting
                        //INTEGRAL will return these many policy in search if the policy matching search criteria are more then this count
                        iResultCount = (new SysSettings(oUserLogin.objRiskmasterDatabase.ConnectionString, oSearchRequest.ClientId)).PolicySearchCount;

                        //  Generate ticket 
                        oSearchRequest.Ticket = GenerateCASTicket(dtPolicySystemInfo, oUserLogin, ref oSystemErrors);
                            // Get request XML from object
                            sSearchSoapRequest = GetPolicySearchSoapRequest(objPolicySystemAdapter, oSearchRequest, iResultCount, ref oSystemErrors);

                            // Get response XML from webservice call
                            if (!string.IsNullOrEmpty(sSearchSoapRequest))
                            {
                                oSearchResponse = new PolicySearch();
                                oSearchSoapResponse = PostRequestToPolicySystem(dtPolicySystemInfo, sSearchSoapRequest, oSearchRequest.Ticket, oSearchRequest.Token, ref oSystemErrors);
                            }
                            // Get response object from XML
                            if (!(string.IsNullOrEmpty(oSearchSoapResponse)))
                            {
                                oSearchResponse = new PolicySearch();
                                oSearchResponse = GetPolicySearchResponseObject(oSearchSoapResponse, Constants.POLICY_SYSTEM_TYPE.INTEGRAL, oUserLogin.objRiskmasterDatabase.ConnectionString, oSearchRequest.ClientId);
                                bresult = true;
                            }
                       
                        // pending error handling if CAS ticket generation fails.
                        break;
                    default:
                        oSearchResponse = null;
                        break;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest, out ExternalPolicyData oExternalPolicyData)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            IntegralSystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetExternalPolicyDataSoapRequest";
            bool bresult = false;
            string oSoapRequest = string.Empty;
            string oSoapResponse = string.Empty;
            DataTable dtPolicySystemInfo = null;
            string sPolicySystemtype = string.Empty;
            oExternalPolicyData = null;
            //oExternalPolicyData = new ExternalPolicyData();
            
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oEnquiryRequest.ClientId);
                objPolicySystemAdapter = new IntegralSystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oEnquiryRequest, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {

                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oEnquiryRequest.ClientId);

                dtPolicySystemInfo = objPolicySystemAdapter.GetPolicySystemInfoById(oEnquiryRequest.PolicySystemId, ref oSystemErrors);
                if (dtPolicySystemInfo != null && dtPolicySystemInfo.Rows.Count > 0)
                    sPolicySystemtype = dtPolicySystemInfo.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();

                switch (CommonFunctions.GetPolicySystemTypeIndicator(sPolicySystemtype))
                {
                    case Constants.POLICY_SYSTEM_TYPE.INTEGRAL:
                        // Get CAS Ticker
                        oEnquiryRequest.Ticket = GenerateCASTicket(dtPolicySystemInfo, oUserLogin, ref oSystemErrors);
                        // get request XML from request object
                        oSoapRequest = GetPolicyInquireSoapRequest(objPolicySystemAdapter, oEnquiryRequest, ref oSystemErrors);

                        if (!string.IsNullOrEmpty(oSoapRequest))
                        {
                            // Get response XML
                            oSoapResponse = PostRequestToPolicySystem(dtPolicySystemInfo, oSoapRequest, oEnquiryRequest.Ticket, oEnquiryRequest.Token, ref oSystemErrors);
                        }
                        // Get response object from XML
                        if (!(string.IsNullOrEmpty(oSoapResponse)))
                        {
                            //Get response object from response XML
                            oExternalPolicyData = GetExternalPolicyDataResponseObject(oSoapResponse, Constants.POLICY_SYSTEM_TYPE.INTEGRAL, oEnquiryRequest.LOB, ref oSystemErrors);
                            // Set all Interest list roles in response object
                            oExternalPolicyData.InterestListRoles = objPolicySystemAdapter.GetInterestListRoles(ref oSystemErrors);
                            bresult = true;
                        }
                        break;
                    default:
                        oExternalPolicyData = null;
                        break;
                }
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));

            }
            finally
            {
                oUserLogin = null;
                objPolicySystemAdapter = null;
               
            }
                logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

                if (oSystemErrors.Count > 0)
                {
                    RMException theFault = new RMException();
                    theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                    throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            oSystemErrors = null;
            xmlRequest = null;

        }

        //Payal: RMA-6471 --Starts
        public void UploadClaimData(ref UploadClaimData oUploadClaimData)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            IntegralSystemInterfaceAdaptor objPolicySystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "UploadClaimData";
            bool bresult = false;
            string oSoapRequest = string.Empty;
            string oSoapResponse = string.Empty;
            DataTable dtPolicySystemInfo = null;
            string sPolicySystemtype = string.Empty;
            XElement objElememt = null;
            IEnumerable<XElement> oElements;
            string uploadXml = null;
            XElement activityID = null;
            string activityid = string.Empty;
            BusinessAdaptorErrors oPolicyUploadErrors = null;

            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin,oUploadClaimData.ClientId );
                objPolicySystemAdapter = new IntegralSystemInterfaceAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oUploadClaimData, out xmlRequest, sFunctionName, objPolicySystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {

                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oUploadClaimData.ClientId);

                dtPolicySystemInfo = objPolicySystemAdapter.GetPolicySystemInfoById(oUploadClaimData.PolicySystemID, ref oSystemErrors);
                if (dtPolicySystemInfo != null && dtPolicySystemInfo.Rows.Count > 0)
                    sPolicySystemtype = dtPolicySystemInfo.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();

                switch (CommonFunctions.GetPolicySystemTypeIndicator(sPolicySystemtype))
                {
                    case Constants.POLICY_SYSTEM_TYPE.INTEGRAL:                        
                        objElememt = XElement.Parse(oUploadClaimData.UploadXML);
                        PolicySystemResponse lPolicySystemRespose ;
                        oElements = objElememt.XPathSelectElements("./CLAIM");
                        oUploadClaimData.PolicySystemResponses = new List<PolicySystemResponse>();
                        foreach (XElement olement in oElements)
                        {

                            activityID = olement.XPathSelectElement("./ActivityId");
                            if (activityID != null)
                            {
                                activityid = activityID.Value;
                            }
                            
                            uploadXml = CreateUploadXML(olement, objElememt);
                            // Get CAS Ticket
                            oPolicyUploadErrors = new BusinessAdaptorErrors(oUserLogin, oUploadClaimData.ClientId);
                            oUploadClaimData.Ticket = GenerateCASTicket(dtPolicySystemInfo, oUserLogin, ref oPolicyUploadErrors);
                            oSoapResponse = PostRequestToPolicySystem(dtPolicySystemInfo, uploadXml, oUploadClaimData.Ticket, oUploadClaimData.Token, ref oPolicyUploadErrors);
                            lPolicySystemRespose = ParsePolicySystemResponse(oSoapResponse, activityid, ref oPolicyUploadErrors, oUploadClaimData.ClientId);
                            oUploadClaimData.PolicySystemResponses.Add(lPolicySystemRespose);

                        }

                        break;
                    default:
                        oSoapResponse = null;
                        break;
                }



            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));

            }
            finally
            {
                oUserLogin = null;
                objPolicySystemAdapter = null;
            }
            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);

            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));

            }
            oSystemErrors = null;
            xmlRequest = null;
            oPolicyUploadErrors = null;


        }

        private string CreateUploadXML(XElement sClaimNode, XElement  sClaimDocNode)
        {
            XElement objElementHeader = null;
            XElement objElementProcessingTime = null;
            XElement objUploadXML = new XElement(sClaimDocNode);            
            objElementHeader = sClaimDocNode.XPathSelectElement("./HEADER");
            objElementProcessingTime = sClaimDocNode.XPathSelectElement("./PROCESSING_MESSAGE");
            objUploadXML.RemoveAll();
            if (objElementHeader != null)
            {
                objUploadXML.Add(objElementHeader);
               
            }
            if (objElementProcessingTime != null)
            {
                objUploadXML.Add(objElementProcessingTime);
               
            }
            if (sClaimNode != null)
            {
                objUploadXML.Add(sClaimNode);
              
            }
            return objUploadXML.ToString();


        }

        private PolicySystemResponse ParsePolicySystemResponse(string sSoapResponse, string sActivityId, ref BusinessAdaptorErrors oUploadPolicyErrors,int iClientId)
        {
            XElement result = null;
            XElement olements = null;
            XElement oReason = null;
            XElement oErrorCode = null;
            XElement oHttpStatusMsg = null;
            XElement oExceptionRootCause = null;
            string sErrorMsg = string.Empty;
            string sCode = string.Empty;
            PolicySystemResponse oPolicySystemResponse = null;

            try
            {
                
                oPolicySystemResponse = new PolicySystemResponse();
                if (oUploadPolicyErrors.Count != 0)   //case when response is null
                {
                    foreach (BusinessAdaptorError error in oUploadPolicyErrors)
                    {
                        if (string.IsNullOrEmpty(sErrorMsg))
                            sErrorMsg = error.ToString();
                        else
                            sErrorMsg = sErrorMsg + "," + error.ToString();
                    }

                    oPolicySystemResponse.ResponseMsg = sErrorMsg;
                    oPolicySystemResponse.ActivityID = sActivityId;
                    oPolicySystemResponse.Response = false;
                }
                else if (string.IsNullOrEmpty(sSoapResponse))
                {

                    if (oUploadPolicyErrors.Count == 0)  //case when response is empty
                    {
                        oPolicySystemResponse.ActivityID = sActivityId;
                        oPolicySystemResponse.Response = false;
                        oPolicySystemResponse.ResponseMsg = Riskmaster.Common.Globalization.GetString("PolicySystemResponse.Exception", iClientId);
                    }
                }
                else
                {
                    result = XElement.Parse(sSoapResponse);
                    olements = result.XPathSelectElement("./Status");
                    oReason = result.XPathSelectElement("./Reason");
                    oErrorCode = result.XPathSelectElement("./ErrorCode");
                    oHttpStatusMsg = result.XPathSelectElement("./HttpStatusMessage");
                    oExceptionRootCause = result.XPathSelectElement("./ExceptionRootCause");
                    if (olements != null)
                    {
                        sCode = olements.Attribute("code").Value.Trim();
                        if (sCode == "200")  // //case when response is success
                        {
                            oPolicySystemResponse.ActivityID = sActivityId;
                            oPolicySystemResponse.Response = true;

                        }
                    }
                    else if (oReason != null) //case when response is error
                    {
                        sErrorMsg = oHttpStatusMsg.Value + "[" + oErrorCode.Value + "]" + oReason.Value;
                        oPolicySystemResponse.ResponseMsg = sErrorMsg;
                        oPolicySystemResponse.ActivityID = sActivityId;
                        oPolicySystemResponse.Response = false;
                    }

                    else //default case
                    {
                        oPolicySystemResponse.ActivityID = sActivityId;
                        oPolicySystemResponse.Response = false;
                        oPolicySystemResponse.ResponseMsg = oHttpStatusMsg.Value + "[" + oErrorCode.Value + "]" + oExceptionRootCause.Value;
                    }

                }
                
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

                result = null;
                olements = null;
                oReason = null;
                oHttpStatusMsg = null;
                oExceptionRootCause = null;
                oErrorCode = null;
            }
            return oPolicySystemResponse;
        }
        //Payal: RMA-6471 --Ends
        //Payal,RMA:7893 --Starts
        private RiskClause ParseRiskClauses(string xRiskCode, string xRiskDesc)
        {
            RiskClause oriskClauses = null;
            try
            {
                oriskClauses = new RiskClause();
                oriskClauses.RiskCode = xRiskCode;
                oriskClauses.RiskDesc = xRiskDesc;
            }
            catch (Exception e)
            {
                throw e;
            }
            return oriskClauses;
        }
        public void PolicyValidation(PolicyValidateInput oPolicyRequest, out PolicyValidateResponse oPolicyResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            ValidatePolicyAdaptor objValidatePolicyAdaptor = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "PolicyValidation";
            bool bresult = false;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oPolicyRequest.ClientId);
                objValidatePolicyAdaptor = new ValidatePolicyAdaptor();
                InitiateServiceProcess(oPolicyRequest, out xmlRequest, sFunctionName, objValidatePolicyAdaptor, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oPolicyRequest.ClientId);
                bresult = objValidatePolicyAdaptor.PolicyValidation(oPolicyRequest, out oPolicyResponse, ref oSystemErrors);
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            logErrors(sFunctionName, xmlRequest, bresult, oSystemErrors);
            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        public void SavePolicyAllData(ExternalPolicyData oSaveRequest, out PolicySaveRequest oSaveResponse)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            IntegralSystemInterfaceAdaptor objIntegralSystemAdapter = null;
            BusinessAdaptorErrors oSystemErrors = null;
            string sFunctionName = "GetPolicySaveSoapResponse";
            bool bResult = false;
            oSaveResponse = null;
            try
            {
                oSystemErrors = new BusinessAdaptorErrors(oSaveRequest.ClientId);
                objIntegralSystemAdapter = new IntegralSystemInterfaceAdaptor();
                InitiateServiceProcess(oSaveRequest, out xmlRequest, sFunctionName, objIntegralSystemAdapter, out oUserLogin, ref oSystemErrors);
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                if (oSaveRequest != null)
                {
                    oSaveResponse = new PolicySaveRequest();
                    oSystemErrors = new BusinessAdaptorErrors(oUserLogin, oSaveRequest.ClientId);
                    bResult = objIntegralSystemAdapter.SavePolicyAllData(oSaveRequest, ref oSaveResponse, ref oSystemErrors);
                }
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, oSystemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            logErrors(sFunctionName, xmlRequest, bResult, oSystemErrors);
            if (oSystemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, oSystemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        // search request Object to SOAP XML
        private string GetPolicySearchSoapRequest(IntegralSystemInterfaceAdaptor objPolicySystemAdapter, PolicySearch oSearchRequest, int iResultCount, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oSearchSoaprequest = string.Empty;
            SearchFilters oSearchFilters = oSearchRequest.objSearchFilters;
            XElement oElement = null;
            XElement oSearchRequestParams = null;
            XElement oTemplate = GetPolicySearchSoapRequestTemplate();
            DataSet dsPolicyTypes = null;

            try
            {
                oElement = oTemplate.XPathSelectElement("//ResultCount");
                if (oElement != null)
                {
                    oElement.Value = iResultCount.ToString();
                    oElement = null;
                } 

                oElement = oTemplate.XPathSelectElement("//Function");
                if (oElement != null)
                {
                    oElement.Value = "Search";
                    oElement = null;
                }                

                //oElement = oTemplate.XPathSelectElement("//casticket");
                //if (oElement != null)
                //{
                //    oElement.Value = oSearchRequest.Ticket;
                //    oElement = null;
                //}


                //oElement = oTemplate.XPathSelectElement("//guid");
                //if (oElement != null)
                //{
                //    oElement.Value = oSearchRequest.Token;
                //    oElement = null;
                //}

                // Map SOAP request data 
               // oSearchRequestParams = oTemplate.XPathSelectElement("//PolicySearchRequest");
                if (oSearchFilters != null && oSearchRequest.objSearchFilters.PolicySystemId > 0)
                {
                    oElement = oTemplate.XPathSelectElement("./PolicyTypeCodeList");
                    if (oElement != null && oSearchFilters.LOB > 0)
                    {
                        dsPolicyTypes = objPolicySystemAdapter.GetPolicyTypesFromLOB(oSearchFilters.LOB);

                        foreach (DataRow dr in dsPolicyTypes.Tables[0].Rows)
                        {
                            oElement.AddFirst(XElement.Parse("<PolicyTypeCode>" + dr["SHORT_CODE"] + "</PolicyTypeCode>"));
                        }
                    }

                    if (!string.IsNullOrEmpty(oSearchFilters.PolicySymbol))
                    {
                        if (oElement.Value == "")
                            oElement.Value = oSearchFilters.PolicySymbol;
                        else
                            oElement.Value = oElement.Value + "|" + oSearchFilters.PolicySymbol;

                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./DateOfLoss");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.LossDate))
                    {
                        //oElement.Value = oSearchFilters.LossDate;
                        oElement.Value = DateTime.Parse(oSearchFilters.LossDate).ToString("MM-dd-yyyy");
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.PolicyNumber))
                    {
                        oElement.Value = oSearchFilters.PolicyNumber;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./AgentDetails/FullName");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.AgentFirstName))
                    {
                        oElement.Value = oSearchFilters.AgentFirstName;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./AgentDetails/FullName");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.AgentLastName))
                    {
                        oElement.Value = oSearchFilters.AgentLastName;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./AgentDetails/AgentNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.AgentNumber))
                    {
                        oElement.Value = oSearchFilters.AgentNumber;
                        oElement = null;
                    }

                    //need to add agent middlename and agent number as per the search format

                    oElement = oTemplate.XPathSelectElement("./InsuredOrOwnerName/FirstName");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.InsuredFirstName))
                    {
                        oElement.Value = oSearchFilters.InsuredFirstName;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./InsuredOrOwnerName/LastName");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.InsuredLastName))
                    {
                        oElement.Value = oSearchFilters.InsuredLastName;
                        oElement = null;
                    }

                    //need to add insured middle name and insurer company as per the search format

                    oElement = oTemplate.XPathSelectElement("./VehicleChassis");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.VehicleChasisNumber))
                    {
                        oElement.Value = oSearchFilters.VehicleChasisNumber;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./VehicleEngine");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.VehicleEngineNumber))
                    {
                        oElement.Value = oSearchFilters.VehicleEngineNumber;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./VehicleRegistration");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.VehicleRegistrationNumber))
                    {
                        oElement.Value = oSearchFilters.VehicleRegistrationNumber;
                        oElement = null;
                    }
                }
                if (!(string.IsNullOrEmpty(oTemplate.ToString())))
                    oSearchSoaprequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                theFault.Errors = Globalization.GetString("IntegralSystemInterfaceAdaptor.GetPolicySearchSoapRequest.RequestOperationError", oSearchRequest.ClientId);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            finally
            {
                if (oSearchFilters != null)
                    oSearchFilters = null;
                if (oTemplate != null)
                    oTemplate = null;
                if (oElement != null)
                    oElement = null;
            }

            return oSearchSoaprequest;
        }

        // Inquiry request Object to SOAP XML
        private string GetPolicyInquireSoapRequest(IntegralSystemInterfaceAdaptor objPolicySystemAdapter, PolicyEnquiry oPolicyEnquiry, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oSoapRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInquireSoapRequestTemplate();
            try
            {

                // Map SOAP Header
                oElement = oTemplate.XPathSelectElement("//Function");
                if (oElement != null)
                {
                    oElement.Value = "Inquiry";
                    oElement = null;
                }

                //oElement = oTemplate.XPathSelectElement("//casticket");
                //if (oElement != null)
                //{
                //    oElement.Value = oPolicyEnquiry.Ticket;
                //    oElement = null;
                //}

                //oElement = oTemplate.XPathSelectElement("//guid");
                //if (oElement != null)
                //{
                //    oElement.Value = oPolicyEnquiry.Token;
                //    oElement = null;
                //}

                // Map SOAP request data
                if (oPolicyEnquiry.PolicySystemId > 0)
                {
                    oElement = oTemplate.XPathSelectElement("//policyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                    {
                        oElement.Value = oPolicyEnquiry.PolicyNumber;
                        oElement = null;
                    }
                }

                oElement = oTemplate.XPathSelectElement("//DateOfLoss");
                if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolLossDt))
                  {
                      oElement.Value = oPolicyEnquiry.PolLossDt;
                      oElement = null;
                  }

                if (!(string.IsNullOrEmpty(oTemplate.ToString())))
                    oSoapRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                RMException theFault = new RMException();
                theFault.Errors = Globalization.GetString("IntegralSystemInterfaceAdaptor.GetPolicySearchSoapRequest.RequestOperationError", oPolicyEnquiry.ClientId);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oSoapRequest;
        }
        // search response XML to response object
        private PolicySearch GetPolicySearchResponseObject(string sSearchResponse, Constants.POLICY_SYSTEM_TYPE sPolicySystemType, string sConnectionString, int iClientID)
        {
            PolicySearch oSearchResponse = new PolicySearch();
            oSearchResponse.SearchRowList = new System.Collections.Generic.List<SearchRow>();
            SearchRow oSearchRow = null;
            XmlDocument xmlSearchResponse = new XmlDocument();
            xmlSearchResponse.LoadXml(sSearchResponse);
            SysSettings objSettings = null;
            XmlNode oNode = null;

            oSearchResponse.ResponseXML = sSearchResponse;
            objSettings = new SysSettings(sConnectionString, iClientID);

            if (sPolicySystemType.Equals(Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
            {
                oNode = xmlSearchResponse.SelectSingleNode("./Error/Reason");
                if (oNode != null && !string.IsNullOrEmpty(oNode.InnerText))
                {
                    oSearchResponse.ResponseError = oNode.InnerText.Trim();
                    oNode = null;
                }

                foreach (XmlNode xPolicy in xmlSearchResponse.SelectNodes("//Policy"))
                {
                    oSearchRow = new SearchRow();

                    if (xPolicy.SelectSingleNode("./PolicyStatus") != null)
                        oSearchRow.PolicyStatus = xPolicy.SelectSingleNode("./PolicyStatus").Attributes["description"].Value.Trim();

                    oSearchRow.PolicyNumber = xPolicy.Attributes["number"].Value.Trim();

                    //if (xPolicy.SelectSingleNode("./policyNumber") != null)
                    //    oSearchRow.PolicyNumber = xPolicy.SelectSingleNode("./policyNumber").InnerText;

                    if (xPolicy.SelectSingleNode("./renewalNumber") != null)
                        oSearchRow.Module = xPolicy.SelectSingleNode("./renewalNumber").InnerText.Trim();

                    if (string.IsNullOrEmpty(oSearchRow.Module))
                        oSearchRow.Module = "00";

                    if (xPolicy.SelectSingleNode("./Insured") != null)
                    {
                        oSearchRow.InsuredTaxID = xPolicy.SelectSingleNode("./Insured").Attributes["taxID"].Value.Trim();

                        if (xPolicy.SelectSingleNode("./Insured/Name/FirstName") != null)
                            oSearchRow.InsuredFullName = xPolicy.SelectSingleNode("./Insured/Name/FirstName").InnerText.Trim();

                        if (xPolicy.SelectSingleNode("./Insured/Name/LastName") != null)
                        {
                            if (string.IsNullOrEmpty(oSearchRow.InsuredFullName))
                                oSearchRow.InsuredFullName = xPolicy.SelectSingleNode("./Insured/Name/LastName").InnerText.Trim();

                            else
                                oSearchRow.InsuredFullName = oSearchRow.InsuredFullName + " " + xPolicy.SelectSingleNode("./Insured/Name/LastName").InnerText.Trim();
                        }
                    }

                    if (xPolicy.SelectSingleNode("./PolicyType") != null)
                    {
                        oSearchRow.PolicySymbol = xPolicy.SelectSingleNode("./PolicyType").Attributes["code"].Value.Trim();

                        if (string.IsNullOrEmpty(oSearchRow.PolicySymbol))
                            oSearchRow.PolicySymbol = xPolicy.SelectSingleNode("./PolicyType").Attributes["description"].Value.Trim();
                        else
                            oSearchRow.PolicySymbol = oSearchRow.PolicySymbol + " - " + xPolicy.SelectSingleNode("./PolicyType").Attributes["description"].Value.Trim();
                    }

                    if (xPolicy.SelectSingleNode("./EffectiveDate") != null)
                        oSearchRow.EffectiveDate = xPolicy.SelectSingleNode("./EffectiveDate").InnerText.Trim();

                    if (xPolicy.SelectSingleNode("./ExpirationDate") != null)
                        oSearchRow.ExpiryDate = xPolicy.SelectSingleNode("./ExpirationDate").InnerText.Trim();

                    if (xPolicy.SelectSingleNode("./PolicyInceptionDate") != null)
                        oSearchRow.InceptionDate = xPolicy.SelectSingleNode("./PolicyInceptionDate").InnerText.Trim();

                    if (xPolicy.SelectSingleNode("./MasterCompany") != null)
                    {
                        oSearchRow.MasterCompany = xPolicy.SelectSingleNode("./MasterCompany").Attributes["code"].Value.Trim();
                        if (string.IsNullOrEmpty(oSearchRow.MasterCompany))
                            oSearchRow.MasterCompany = xPolicy.SelectSingleNode("./MasterCompany").Attributes["description"].Value.Trim();
                        else
                            oSearchRow.MasterCompany = oSearchRow.MasterCompany + " - " + xPolicy.SelectSingleNode("./MasterCompany").Attributes["description"].Value.Trim();
                    }

                    if (xPolicy.SelectSingleNode("./Branch") != null)
                        oSearchRow.LocationCompany = xPolicy.SelectSingleNode("./Branch").Attributes["description"].Value.Trim();

                    if (xPolicy.SelectSingleNode("./Agent") != null)
                    {
                        oSearchRow.AgentNumber = xPolicy.SelectSingleNode("./Agent").Attributes["number"].Value.Trim();

                        if (xPolicy.SelectSingleNode("./Agent/FullName") != null)
                            oSearchRow.AgentName = xPolicy.SelectSingleNode("./Agent/FullName").InnerText.Trim();
                    }
                    if (objSettings.PolicyCvgType.ToString() == "0")
                        oSearchResponse.IntegralClaimEventSetting = "true";
                    else
                        oSearchResponse.IntegralClaimEventSetting = "false";
                    oSearchResponse.SearchRowList.Add(oSearchRow);
                }
            }
            //else for POINT or other policy systems that doesn't support SOAP standards
            objSettings = null;//vkumar258
            return oSearchResponse;
        }

        private ExternalPolicyData GetExternalPolicyDataResponseObject(string sInquiryResponse, Constants.POLICY_SYSTEM_TYPE PolicySystemType, string sLOB, ref BusinessAdaptorErrors oSystemErrors)
        {
            ExternalPolicyData oResponse = new ExternalPolicyData();
            XElement xmlResponse = XElement.Parse(sInquiryResponse);
            XElement xPolicy = null;
            XElement xNodeElement = null;

            try
            {
                if (PolicySystemType.Equals(Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
                {
                    xNodeElement = xmlResponse.XPathSelectElement("./Reason"); //sanoopsharma - Node path fixed 
                    if (xNodeElement != null && !string.IsNullOrEmpty(xNodeElement.Value))
                    {
                        oResponse.ResponseError = xNodeElement.Value.Trim();
                    }

                // Map Policy Data to Object
                    xPolicy = xmlResponse.XPathSelectElement("./Policy");
                if (xPolicy != null)
                {
                        oResponse.PolicyData = ParsePolicyDataResponse(xPolicy, ref oSystemErrors);
                        oResponse.PolicyData.LOB = sLOB; //Smishra54: Need to look on it
                        // Map Unit Data to Object
                        oResponse.PolicyData.Units = ParseUnitDataResponse(xPolicy, ref oSystemErrors);
                        // Map Entities Under Policy Data to Object
                        oResponse.PolicyData.Entities = ParseEntityData(xPolicy, ref oSystemErrors);
                        //Map Endorcement Data
                        oResponse.PolicyData.Endorsements = ParseEndorsementData(xPolicy, ref oSystemErrors);
                    }
                }
            }
            catch (Exception e)
                    {                
                throw e;
                    }
            finally
                    {
                xmlResponse = null;
                xNodeElement = null;
                    }
            return oResponse;
        }

        private PolicyData ParsePolicyDataResponse(XElement xPolicy, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            PolicyData oPolicyData = null;
            try
            {
                oPolicyData = new PolicyData();
                oPolicyData.PolicyNumber = xPolicy.Attribute("number").Value.Trim();

                xNodeElement = xPolicy.XPathSelectElement("./PolicyType");
                if (xNodeElement != null)
                {
                    oPolicyData.Type = xNodeElement.Attribute("description").Value.Trim();
                    oPolicyData.Symbol = xNodeElement.Attribute("code").Value.Trim();
                }
                //else
                //{
                //    oPolicyData.Type = string.Empty;
                //    oPolicyData.Symbol = string.Empty;
                //}
                //add policy xml into the policy object
                oPolicyData.PolicySoapXML = xPolicy.ToString();

                xNodeElement = xPolicy.XPathSelectElement("./PolicyStatus");
                if (xNodeElement != null)
                    oPolicyData.StatusCode = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oPolicyData.StatusCode = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./ServiceBranch");
                if (xNodeElement != null)
                    oPolicyData.ServiceBranch = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oPolicyData.ServiceBranch = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./EffectiveDate");
                if (xNodeElement != null)
                    oPolicyData.EffectiveDate = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.EffectiveDate = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./ExpiryDate");
                if (xNodeElement != null)
                    oPolicyData.ExpiryDate = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.ExpiryDate = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Renewal");
                if (xNodeElement != null)
                    oPolicyData.RenewalType = xNodeElement.Value.Trim();
                    //oPolicyData.RenewalType = xNodeElement.Attribute("code").Value.Trim(); //sanoopsharma
                //else
                //    oPolicyData.RenewalType = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Renewal/Number");
                if (xNodeElement != null)
                    oPolicyData.RenewalNumber = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.RenewalNumber = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./PaymentPlan");
                if (xNodeElement != null)
                    oPolicyData.PaymentPlan = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oPolicyData.PaymentPlan = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./PaymentMode/Code");
                if (xNodeElement != null)
                    oPolicyData.PaymentMode = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PaymentMode = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./NextBillDate");
                if (xNodeElement != null)
                    oPolicyData.NextBillDate = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.NextBillDate = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Currency");
                if (xNodeElement != null)
                    oPolicyData.Currency = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oPolicyData.Currency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Currency/Rate");
                if (xNodeElement != null)
                    oPolicyData.CurrencyRate = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.CurrencyRate = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./MasterCompany");
                if (xNodeElement != null)
                    oPolicyData.MasterCompany = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oPolicyData.MasterCompany = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Location");
                if (xNodeElement != null)
                    oPolicyData.Country = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oPolicyData.Country = string.Empty;

                    //Map Premium Data
                xNodeElement = xPolicy.XPathSelectElement("./Premium/GrossPremium");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.GrossPremium = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.GrossPremium = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/NetPremium");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.NetPremium = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.NetPremium = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/CalculationMethod");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.CalculationMethod = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;
                xNodeElement = xPolicy.XPathSelectElement("./Premium/ExchangeRate");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.ExchangeRate = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/GrossRetentionOrNetRetention");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.GrossRetentionOrNetRetention = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/PremiumClass");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.PremiumClass = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/OriginalCurrency");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.OriginalCurrency = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/SubAccountCode");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.SubAccountCode = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/SubAccountType");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.SubAccountType = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/TotalSumInsured");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.TotalSumInsured = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.TotalSumInsured = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/TotalCommissionAnnual");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.TotalCommissionAnnual = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.TotalCommissionAnnual = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/TotalCommissionExtra");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.TotalCommissionExtra = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.TotalCommissionExtra = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/TemporaryOrPermanentChange");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.TemporaryOrPermanentChange = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.TotalCommissionExtra = string.Empty;

                xNodeElement = xPolicy.XPathSelectElement("./Premium/AgentCommission");
                if (xNodeElement != null)
                    oPolicyData.PrimiumData.AgentCommission = xNodeElement.Value.Trim();
                //else
                //    oPolicyData.PrimiumData.AgentCommission = string.Empty;

                oPolicyData.PolicySoapXML = xPolicy.ToString();
            }
            catch (Exception e)
                    {
                        throw e;
            }
            finally
            {
                xNodeElement = null;
            }
            
            return oPolicyData;
        }

        private List<UnitData> ParseUnitDataResponse(XElement xPolicy, ref BusinessAdaptorErrors oSystemErrors)
        {
            List<UnitData> lUnits = null;
            UnitData oUnit = null;
            XElement xNodeElement = null;
            Constants.UNIT_TYPE_INDICATOR? UnitTypeInd = null;
            try
            {
                lUnits = new List<UnitData>();
                foreach (XElement xUnit in xPolicy.XPathSelectElements("./Risks/Risk"))
                {
                        oUnit = new UnitData();
                        oUnit.Selected = "False";
                    oUnit.UnitSoapXML = xUnit.ToString();
                    oUnit.UnitNumber = xUnit.Attribute("number").Value.Trim();
                        //Assuming one risk has only one unit
                    xNodeElement = xUnit.XPathSelectElement("./RiskType");
                    if (xNodeElement != null)
                        {
                        oUnit.Type = xNodeElement.Attribute("description").Value.Trim();
                        oUnit.TypeCode = xNodeElement.Attribute("code").Value.Trim();
                        }
                    //else
                    //{
                    //    oUnit.Type = string.Empty;
                    //    oUnit.TypeCode = string.Empty;
                    //}
                    xNodeElement = xUnit.XPathSelectElement("./AttachedDate");
                    if (xNodeElement != null)
                        oUnit.AttachedDate = xNodeElement.Value.Trim();
                    //else
                    //    oUnit.AttachedDate = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./EffectiveDate");
                    if (xNodeElement != null)
                        oUnit.EffectiveDate = xNodeElement.Value.Trim();
                    //else
                    //    oUnit.EffectiveDate = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./ExpiryDate");
                    if (xNodeElement != null)
                        oUnit.ExpiryDate = xNodeElement.Value.Trim();
                    //else
                    //    oUnit.ExpiryDate = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./RatingFlag");
                    if (xNodeElement != null)
                        oUnit.RatingFlag = xNodeElement.Attribute("description").Value.Trim();
                    //else
                    //    oUnit.RatingFlag = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./Currency");
                    if (xNodeElement != null)
                        oUnit.Currency = xNodeElement.Attribute("code").Value.Trim();
                    //else
                    //    oUnit.Currency = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./CurrencyRate");
                    if (xNodeElement != null)
                        oUnit.CurrencyRate = xNodeElement.Value.Trim();
                    //else
                    //    oUnit.CurrencyRate = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Unit");
                    if (xNodeElement != null)
                        {
                        UnitTypeInd = CommonFunctions.GetUnitTypeIndicator(xNodeElement.Attribute("description").Value.Trim());
                        }

                    switch (UnitTypeInd)
                    {
                        case Constants.UNIT_TYPE_INDICATOR.VEHICLE:
                            ParseVehicleData(xUnit, ref oUnit, ref oSystemErrors);
                            break;
                        case Constants.UNIT_TYPE_INDICATOR.PROPERTY:
                            ParsePropertyData(xUnit, ref oUnit, ref oSystemErrors);
                            break;
                    }

                    //Map Premium Data
                    xNodeElement = xUnit.XPathSelectElement("./Premium/GrossPremium");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.GrossPremium = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.GrossPremium = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/NetPremium");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.NetPremium = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.NetPremium = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/CalculationMethod");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.CalculationMethod = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;
                    xNodeElement = xUnit.XPathSelectElement("./Premium/ExchangeRate");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.ExchangeRate = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/GrossRetentionOrNetRetention");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.GrossRetentionOrNetRetention = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/PremiumClass");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.PremiumClass = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/OriginalCurrency");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.OriginalCurrency = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/SubAccountCode");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.SubAccountCode = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/SubAccountType");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.SubAccountType = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.OriginalCurrency = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/TotalSumInsured");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.TotalSumInsured = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.TotalSumInsured = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/TotalCommissionAnnual");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.TotalCommissionAnnual = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.TotalCommissionAnnual = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/TotalCommissionExtra");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.TotalCommissionExtra = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.TotalCommissionExtra = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/TemporaryOrPermanentChange");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.TemporaryOrPermanentChange = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.TotalCommissionExtra = string.Empty;

                    xNodeElement = xUnit.XPathSelectElement("./Premium/AgentCommission");
                    if (xNodeElement != null)
                        oUnit.PrimiumData.AgentCommission = xNodeElement.Value.Trim();
                    //else
                    //    oPolicyData.PrimiumData.AgentCommission = string.Empty;


                    // Map Coverages Under Unit Data to Object
                    oUnit.Coverages = ParseCoverageData(xUnit, ref oSystemErrors);                   

                    // Map Entities Under Unit Data to Object
                    oUnit.Entities = ParseEntityData(xUnit, ref oSystemErrors);
                    
                    lUnits.Add(oUnit);
                }
            }
            catch (Exception e)
                        {
                
                throw e;
            }
            finally
                            {
                UnitTypeInd = null;
                xNodeElement = null;
            }

            return lUnits;
        }

        private void ParseVehicleData(XElement xUnit, ref UnitData oUnit, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            //Payal, RMA:7893 --Starts
            XElement xRiskCode = null; 
            XElement xRiskDesc = null;
            RiskClause lriskclauses = null;
            string oRiskClauses = string.Empty;
            //Payal, RMA:7893 --Ends
            try
            {
                oUnit.UnitTypeInd = CommonFunctions.GetUnitTypeIndicatorText(Constants.UNIT_TYPE_INDICATOR.VEHICLE);
                xNodeElement = xUnit.XPathSelectElement("./Unit/VehicleMake");
                if (xNodeElement != null)
                    oUnit.VehicleMake = xNodeElement.Value.Trim();
                //else
                //    oUnit.VehicleMake = string.Empty;

                xNodeElement = xUnit.XPathSelectElement("./Unit/ModelCode");
                if (xNodeElement != null)
                    oUnit.Model = xNodeElement.Value.Trim();
                //else
                //    oUnit.Model = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/YearOfManufacture");
                if (xNodeElement != null)
                    oUnit.Year = xNodeElement.Value.Trim();
                //else
                //    oUnit.Year = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/PurchaseValue");
                if (xNodeElement != null)
                    oUnit.PurchaseValue = xNodeElement.Value.Trim();
                //else
                //    oUnit.PurchaseValue = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Capacity");
                if (xNodeElement != null)
                    oUnit.Capacity = xNodeElement.Value.Trim();
                //else
                //    oUnit.PurchaseDate = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Seat");
                if (xNodeElement != null)
                    oUnit.Seats = xNodeElement.Value.Trim();
                //else
                //    oUnit.Capacity = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/BodyType");
                if (xNodeElement != null)
                    oUnit.BodyType = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.BodyType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/RegistrationNumber");
                if (xNodeElement != null)
                    oUnit.RegistrationNumber = xNodeElement.Value.Trim();
                //else
                //    oUnit.RegistrationNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/ChasisNumber");
                if (xNodeElement != null)
                    oUnit.ChasisNumber = xNodeElement.Value.Trim();
                //else
                //    oUnit.ChasisNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/EngineNumber");
                if (xNodeElement != null)
                    oUnit.EngineNumber = xNodeElement.Value.Trim();
                //else
                //    oUnit.EngineNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/VehicleClass");
                if (xNodeElement != null)
                    oUnit.VehicleClass = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.VehicleClass = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/CoverType");
                if (xNodeElement != null)
                    oUnit.CoverType = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.CoverType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/UseFor");
                if (xNodeElement != null)
                    oUnit.UseFor = xNodeElement.Value.Trim();
                //else
                //    oUnit.VehicleSumInsured = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Region");
                if (xNodeElement != null)
                    oUnit.Region = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.Region = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/GeogLocation");
                if (xNodeElement != null)
                    oUnit.GeoLocation = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.GeoLocation = string.Empty;                
                xNodeElement = xUnit.XPathSelectElement("./Unit/Garage");
                if (xNodeElement != null)
                    oUnit.Garage = xNodeElement.Value.Trim();
                //else
                //    oUnit.Garage = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/CIType");
                if (xNodeElement != null)
                    oUnit.CIType = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                //Payal, RMA:7893--STARTS
                oUnit.RiskClauses = new List<RiskClause>(); 
                foreach (XElement xRiskCde in xUnit.XPathSelectElements("./RiskClauses/RiskClause"))
                {
                    xRiskCode = xRiskCde.XPathSelectElement("./Code");
                    xRiskDesc = xRiskCde.XPathSelectElement("./Description");
                    if (xRiskCode != null || xRiskDesc != null)
                    {
                        string code = xRiskCode.Value;
                        string desc = xRiskDesc.Value;
                        lriskclauses = ParseRiskClauses(code, desc);
                    }
                    oUnit.RiskClauses.Add(lriskclauses);
                   
                }
                
               
                //Payal, RMA:7893--ENDS
            }
            catch(Exception e)
                                {
                
                throw e;
                                }
            finally
                                {
                xNodeElement = null;
                lriskclauses = null; //Payal,RMA:7893
            }
                                }

        private void ParsePropertyData(XElement xUnit, ref UnitData oUnit, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            //Payal,RMA:7893--Starts
            XElement xRiskCode = null; 
            XElement xRiskDesc = null;
            RiskClause lriskclauses = null;
            string oRiskClauses = string.Empty;
            //Payal,RMA:7893--Ends
            try
            {
                oUnit.UnitTypeInd = CommonFunctions.GetUnitTypeIndicatorText(Constants.UNIT_TYPE_INDICATOR.PROPERTY);
                xNodeElement = xUnit.XPathSelectElement("./Unit/Situation");
                if (xNodeElement != null)
                    oUnit.Situation = xNodeElement.Value.Trim();
                //else
                //    oUnit.VehicleMake = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Business/Description"); //sanoopsharma - Fixed 
                if (xNodeElement != null)
                    oUnit.Business = xNodeElement.Value.Trim();
                    //oUnit.Business = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.Model = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/ContractTitle");
                if (xNodeElement != null)
                    oUnit.ContractTitle = xNodeElement.Value.Trim();
                //else
                //    oUnit.Year = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Premises");
                if (xNodeElement != null)
                    oUnit.Premises = xNodeElement.Value.Trim();
                //else
                //    oUnit.PurchaseValue = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/TerritorialLimit");
                if (xNodeElement != null)
                    oUnit.TerritorialLimit = xNodeElement.Value.Trim();
                //else
                //    oUnit.PurchaseDate = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Retroactive");
                if (xNodeElement != null)
                    oUnit.Retroactive = xNodeElement.Value.Trim();
                //else
                //    oUnit.Capacity = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/MaintenancePeriod");
                if (xNodeElement != null)
                    oUnit.MaintenancePeriod = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.BodyType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/RetroactiveDate");
                if (xNodeElement != null)
                    oUnit.RetroactiveDate = xNodeElement.Value.Trim();
                //else
                //    oUnit.RegistrationNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/DeclaredPeriod");
                if (xNodeElement != null)
                    oUnit.DeclaredPeriod = xNodeElement.Value.Trim();
                //else
                //    oUnit.ChasisNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/PostalCode");
                if (xNodeElement != null)
                    oUnit.PostalCode = xNodeElement.Value.Trim();
                //else
                //    oUnit.EngineNumber = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/RiskAccumulationState");
                if (xNodeElement != null)
                    oUnit.RiskAccumulationState = xNodeElement.Attribute("code").Value.Trim();
                //else
                //    oUnit.VehicleClass = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/RiskAccumulationLocality");
                if (xNodeElement != null)
                    oUnit.RiskAccumulationLocality = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.CoverType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Register");
                if (xNodeElement != null)
                    oUnit.Register = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.VehicleSumInsured = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/ProtectedBy");
                if (xNodeElement != null)
                    oUnit.ProtectedBy = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.Region = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/RiskRating");
                if (xNodeElement != null)
                    oUnit.RiskRating = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.GeoLocation = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/BuildingStorey");
                if (xNodeElement != null)
                    oUnit.BuildingStorey = xNodeElement.Value.Trim();
                //else
                //    oUnit.Garage = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/Attached");
                if (xNodeElement != null)
                    oUnit.Attached = xNodeElement.Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/OccupiedAs");
                if (xNodeElement != null)
                    oUnit.OccupiedAs = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/FloorArea");
                if (xNodeElement != null)
                    oUnit.FloorArea = xNodeElement.Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/HazardGrade");
                if (xNodeElement != null)
                    oUnit.HazardGrade = xNodeElement.Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/ConstructionType");
                if (xNodeElement != null)
                    oUnit.ConstructionType = xNodeElement.Attribute("description").Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                xNodeElement = xUnit.XPathSelectElement("./Unit/ConstructionYear");
                if (xNodeElement != null)
                    oUnit.ConstructionYear = xNodeElement.Value.Trim();
                //else
                //    oUnit.CIType = string.Empty;
                //Payal, RMA:7893--STARTS
                oUnit.RiskClauses = new List<RiskClause>();
                foreach (XElement xRiskCde in xUnit.XPathSelectElements("./RiskClauses/RiskClause"))
                {
                    xRiskCode = xRiskCde.XPathSelectElement("./Code");
                    xRiskDesc = xRiskCde.XPathSelectElement("./Description");
                    if (xRiskCode != null || xRiskDesc != null)
                    {
                        string code = xRiskCode.Value;
                        string desc = xRiskDesc.Value;
                        lriskclauses = ParseRiskClauses(code, desc);
                    }
                    oUnit.RiskClauses.Add(lriskclauses);

                }

                

                //Payal, RMA:7893--ENDS
            }
            catch (Exception e)
                                {                
                throw e;
                                }
            finally
                                {
                xNodeElement = null;
                lriskclauses = null;  //Payal:RMA:7893
            }
                                }

        private List<CoverageData> ParseCoverageData(XElement xUnit, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            XElement xTempCoverage = null;
            List<CoverageData> lCoverages = null;
            CoverageData oCoverage = null;
            try
            {
                lCoverages = new List<CoverageData>();
                foreach (XElement xCoverage in xUnit.XPathSelectElements("./Unit/Coverages/Coverage"))
                {                   
                    xTempCoverage = new XElement(xCoverage);
                    xTempCoverage.XPathSelectElements("./Premiums/Premium").Remove();
                  
                    foreach (XElement xPremium in xCoverage.XPathSelectElements("./Premiums/Premium"))
                    {
                        oCoverage = new CoverageData();
                        oCoverage.Selected = "True";
                        //add the coverage xml - as we are assuming every premium node as one coverage so add the premium node
                        xTempCoverage.XPathSelectElement("./Premiums").AddFirst(xPremium);
                        oCoverage.CoverageSoapXML = xTempCoverage.ToString();
                        xTempCoverage.XPathSelectElements("./Premiums/Premium").Remove();

                        xNodeElement = xCoverage.XPathSelectElement("./Number");
                        if (xNodeElement != null)
                            oCoverage.Number = xNodeElement.Value.Trim();
                        //else
                        //    oCoverage.Number = string.Empty;

                        xNodeElement = xCoverage.XPathSelectElement("./CoverageType/CoverType");
                        if (xNodeElement != null)
                        {
                            oCoverage.CoverageType = xNodeElement.Attribute("code").Value.Trim();
                            oCoverage.CoverageDesc = xNodeElement.Attribute("description").Value.Trim();
                            }
                        //else
                        //{
                        //    oCoverage.CoverageType = string.Empty;
                        //    oCoverage.CoverageDesc = string.Empty;
                        //}
                        xNodeElement = xCoverage.XPathSelectElement("./AOAIndemnityLimits");
                        if (xNodeElement != null)
                        {
                            oCoverage.AOAIndemnityLimits = xNodeElement.Value.Trim();
                        }

                        xNodeElement = xCoverage.XPathSelectElement("./AOPIndemnityLimits");
                        if (xNodeElement != null)
                        {
                            oCoverage.AOPIndemnityLimits = xNodeElement.Value.Trim();
                        }

                        xNodeElement = xCoverage.XPathSelectElement("./BaseLimit");
                        if (xNodeElement != null)
                        {
                            oCoverage.BaseLimit = xNodeElement.Value.Trim();
                        }

                        xNodeElement = xCoverage.XPathSelectElement("./VehicleInterestInsured");
                        if (xNodeElement != null)
                        {
                            oCoverage.VehicleInterestInsured = xNodeElement.Attribute("description").Value.Trim();
                        }

                        xNodeElement = xCoverage.XPathSelectElement("./LimitSumInsured/SumInsured");
                        if (xNodeElement != null)
                        {
                            oCoverage.LimitSumInsured = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./GrossPremium");
                        xNodeElement = xPremium.XPathSelectElement("./GrossPremium"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.GrossPremium = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./NetPremium");
                        xNodeElement = xPremium.XPathSelectElement("./NetPremium"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.NetPremium = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./CalculationMethod");
                        xNodeElement = xPremium.XPathSelectElement("./CalculationMethod"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.CalculationMethod = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./ExchangeRate");
                        xNodeElement = xPremium.XPathSelectElement("./ExchangeRate"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.ExchangeRate = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./GrossRetentionOrNetRetention");
                        xNodeElement = xPremium.XPathSelectElement("./GrossRetentionOrNetRetention"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                            {
                            oCoverage.PrimiumData.GrossRetentionOrNetRetention = xNodeElement.Value.Trim();
                        }

                        xNodeElement = xPremium.XPathSelectElement("./PremiumClass");
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.PremiumClass = xNodeElement.Value;
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./OriginalCurrency");
                        xNodeElement = xPremium.XPathSelectElement("./OriginalCurrency"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.OriginalCurrency = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./SubAccountCode");
                        xNodeElement = xPremium.XPathSelectElement("./SubAccountCode"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.SubAccountCode = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./SubAccountType");
                        xNodeElement = xPremium.XPathSelectElement("./SubAccountType"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.SubAccountType = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./TotalSumInsured");
                        //xNodeElement = xPremium.XPathSelectElement("./TotalSumInsured"); //Node name fix sanoopsharma
                        xNodeElement = xPremium.XPathSelectElement("./CoverageType/TotalSumInsured"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.TotalSumInsured = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./TotalCommissionAnnual");
                        xNodeElement = xPremium.XPathSelectElement("./TotalCommissionAnnual"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.TotalCommissionAnnual = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./TotalCommissionExtra");
                        xNodeElement = xPremium.XPathSelectElement("./TotalCommissionExtra"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.TotalCommissionExtra = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./TemporaryOrPermanentChange");
                        xNodeElement = xPremium.XPathSelectElement("./TemporaryOrPermanentChange"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.TemporaryOrPermanentChange = xNodeElement.Value.Trim();
                        }

                        //xNodeElement = xCoverage.XPathSelectElement("./AgentCommission");
                        xNodeElement = xPremium.XPathSelectElement("./AgentCommission"); //Node name fix sanoopsharma
                        if (xNodeElement != null)
                        {
                            oCoverage.PrimiumData.AgentCommission = xNodeElement.Value.Trim();
                        }

                        lCoverages.Add(oCoverage);
                        oCoverage = null;
                    }
                }
            }
            catch (Exception e)
            {              
                throw e;
                            }
            finally
            {
                xNodeElement = null;
                oCoverage = null;
                xTempCoverage = null;
                        }

            return lCoverages;
                    }

        private List<EntityData> ParseEntityData(XElement xObjElement, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            List<EntityData> lEntityData = null;
            EntityData oEntity = null;
            AddressData oAddress = null;
            string sEntityRole = string.Empty;
            try
            {
                lEntityData = new List<EntityData>();
                foreach (XElement xEntity in xObjElement.XPathSelectElements("./Parties/Party"))
                    {
                        oEntity = new EntityData();
                        oEntity.Selected = "False";
                        oEntity.AddAs = "";
                    //add entity xml to entity object
                    oEntity.PartySoapXML = xEntity.ToString();
                    if(xEntity.Attribute("code")!= null)
                    oEntity.TypeCode = xEntity.Attribute("code").Value.Trim();
                    if (xEntity.Attribute("description") != null)
                    oEntity.TypeDesc = xEntity.Attribute("description").Value.Trim();


                    xNodeElement = xEntity.XPathSelectElement("./PartyNumber/ClientNumber");
                    if (xNodeElement != null)
                        oEntity.ClientNumber = xNodeElement.Value.Trim();

                    xNodeElement = xEntity.XPathSelectElement("./PartyNumber/AgentNumber");
                    if (xNodeElement != null)
                        oEntity.AgentNumber = xNodeElement.Value.Trim();

                    //else
                    //    oEntity.Number = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./PartyRole");
                    if (xNodeElement != null)
                        oEntity.Role = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.Role = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./Name/FirstName");
                    if (xNodeElement != null)
                        oEntity.FirstName = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.FirstName = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./Name/LastName");
                    if (xNodeElement != null)
                        oEntity.LastName = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.LastName = string.Empty;

                    xNodeElement = xEntity.XPathSelectElement("./TaxID");
                    if (xNodeElement != null)
                        oEntity.TaxID = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.TaxID = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./Contact/MobileNumber");
                    if (xNodeElement != null)
                        oEntity.MobileNumber = xNodeElement.Value.Trim();

                    xNodeElement = xEntity.XPathSelectElement("./Contact/OfficeNumber");
                    if (xNodeElement != null)
                        oEntity.OfficeNumber = xNodeElement.Value.Trim();

                    xNodeElement = xEntity.XPathSelectElement("./Contact/ResidenceNumber");
                    if (xNodeElement != null)
                        oEntity.ResidenceNumber = xNodeElement.Value.Trim();

                    xNodeElement = xEntity.XPathSelectElement("./BirthDate");
                    if (xNodeElement != null)
                        oEntity.BirthDate = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.BirthDate = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./Sex");
                    if (xNodeElement != null)
                        oEntity.Sex = xNodeElement.Attribute("code").Value.Trim();
                    //else
                    //    oEntity.Sex = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./EmailAddress");
                    if (xNodeElement != null)
                        oEntity.EmailAddress = xNodeElement.Value.Trim();
                    //else
                    //    oEntity.EmailAddress = string.Empty;
                    xNodeElement = xEntity.XPathSelectElement("./BusinessName");
                    if (xNodeElement != null)
                        oEntity.BusinessName = xNodeElement.Value.Trim();
                    
                    sEntityRole = oEntity.Role.ToLower();
                    if (Constants.listAgent.Contains(sEntityRole) || Constants.listDriver.Contains(sEntityRole) || Constants.listInsured.Contains(sEntityRole))
                        oEntity.Selected = "True";

                    //xNodeElement = xEntity.XPathSelectElement("./ReferenceNumber");
                    xNodeElement = xEntity.XPathSelectElement("./PartyNumber/AgentNumber");
                    if (xNodeElement != null)
                        oEntity.AgentNumber = xNodeElement.Value.Trim();
                    

                    foreach (XElement xAddress in xEntity.XPathSelectElements("./Addresses/Address"))
                        {
                            oAddress = new AddressData();
                            if (xAddress.Attribute("code") != null)
                                oAddress.Type = xAddress.Attribute("code").Value.Trim();
                            else
                            {
                                if (oEntity.TypeCode == "C")
                                    oAddress.Type = "BADDR";
                            }
                        xNodeElement = xAddress.XPathSelectElement("AddressLine1");
                        if (xNodeElement != null)
                            oAddress.AddressLine1 = xNodeElement.Value.Trim();
                        //else
                        //    oAddress.AddressLine1 = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("AddressLine2");
                        if (xNodeElement != null)
                            oAddress.AddressLine2 = xNodeElement.Value.Trim();
                        //else
                        //    oAddress.AddressLine2 = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("City");
                        if (xNodeElement != null)
                            oAddress.City = xNodeElement.Value.Trim();
                        //else
                        //    oAddress.AddressLine3 = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("Telephone");
                        if (xNodeElement != null)
                            oAddress.Telephone = xNodeElement.Value.Trim();
                        //else
                        //    oAddress.AddressLine4 = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("State");
                        if (xNodeElement != null)
                            oAddress.State = xNodeElement.Attribute("code").Value.Trim();
                        //else
                        //    oAddress.State = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("ZipCode");
                        if (xNodeElement != null)
                            oAddress.ZipCode = xNodeElement.Value.Trim();
                        //else
                        //    oAddress.ZIP = string.Empty;
                        xNodeElement = xAddress.XPathSelectElement("Country");
                        if (xNodeElement != null)
                            oAddress.Country = xNodeElement.Attribute("code").Value.Trim();
                        //else
                        //    oAddress.Country = string.Empty;

                            oEntity.Addresses.Add(oAddress);
                        }
                    lEntityData.Add(oEntity);
                }
            }
            catch (Exception e)
            {                
                throw e;
                    }
            finally
                    {
                xNodeElement = null;
                oEntity = null;
                oAddress = null;
            }

            return lEntityData;
        }

        private List<EndorsementData> ParseEndorsementData(XElement xPolicy, ref BusinessAdaptorErrors oSystemErrors)
        {
            XElement xNodeElement = null;
            List<EndorsementData> lEndorsements = null;
            EndorsementData oEndorsement = null;

            try
            {
                lEndorsements = new List<EndorsementData>();
                foreach (XElement xEndorsement in xPolicy.XPathSelectElements("./Endorsements/Endorsement"))
                {
                    oEndorsement = new EndorsementData();
                    oEndorsement.EndorsementID = xEndorsement.Attribute("id").Value.Trim();

                    xNodeElement = xEndorsement.XPathSelectElement("./EffectiveDate");
                    if (xNodeElement != null)
                        oEndorsement.EffectiveDate = xNodeElement.Value.Trim();

                    xNodeElement = xEndorsement.XPathSelectElement("./Reason");
                    if (xNodeElement != null)
                    {
                        oEndorsement.EndorsementReasonDesc = xNodeElement.Attribute("description").Value.Trim();
                        oEndorsement.EndorsementReasonCode = xNodeElement.Attribute("code").Value.Trim();
                    }
                    lEndorsements.Add(oEndorsement);
                }
                    }
            catch (Exception e)
            {            
                throw e;
                }
            finally
            {
                xNodeElement = null;
                oEndorsement = null;
            }
            return lEndorsements;
        }

        #region WebRequest methods

        private string PostRequestToPolicySystem(DataTable dtPolicySystemInfo, string sSoapRequest, string sCASTicket, string sGUID, ref BusinessAdaptorErrors oSystemErrors)
        {
            string sSearchSoapRequest = string.Empty;
            string sSearchSoapResponse = string.Empty;
            string sPolicySystemType = string.Empty;

            try
            {
                sSearchSoapResponse = PostRequestToIntegral(sSoapRequest, dtPolicySystemInfo, sCASTicket, sGUID, 0, ref oSystemErrors);
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
            }

            return sSearchSoapResponse;
        }
        private string PostRequestToIntegral(string sSoapRequest, DataTable dtPolicySystemInfo, string sCASTicket, string sGUID, int iPostAttempt, ref BusinessAdaptorErrors oSystemErrors)
        {

            int iTimeOut;
            string sWSURL = string.Empty;
            string Result = string.Empty;
            bool blnSuccess = false;
            WebResponse webResponse = null;
            XElement soapResp = null;
            XElement soapReq = null;
            HttpWebRequest webRequest = null;
            //Payal: RMA-6471 --Starts
            string XMLValue = string.Empty;
            XElement functionPath = null;
            XElement claimPath = null;
            string sUploadServiceURL = string.Empty;
            //Payal: RMA-6471 --Ends
            try
            {
                NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", string.Empty, 0);
                iTimeOut = Conversion.CastToType<int>(nvCollSettings["PolicySystemTimeout"], out blnSuccess);
                if (iPostAttempt == 0)
                    iPostAttempt = Conversion.CastToType<int>(nvCollSettings["NumberOfTrys"], out blnSuccess);

                soapReq = XElement.Parse(sSoapRequest);                
                iPostAttempt--;
                //Payal: RMA-6471 --Starts
                functionPath = soapReq.XPathSelectElement("//Function");
                claimPath = soapReq.XPathSelectElement("./HEADER/DOC_NAME");
                if (functionPath != null)
                {
                    XMLValue = functionPath.Value;
                }
                if (claimPath != null)
                {
                    XMLValue = claimPath.Value;
                }
                //if (soapReq.XPathSelectElement("//Function").Value == "Search")
                if (string.Equals(XMLValue,"SEARCH",StringComparison.InvariantCultureIgnoreCase) || string.Equals(XMLValue, "CLAIMSUPLOAD",StringComparison.InvariantCultureIgnoreCase))
                {
                    if (string.Equals(XMLValue, "SEARCH", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sWSURL = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["WS_SEARCH_URL"]);
                        webRequest = (HttpWebRequest)WebRequest.Create(sWSURL);
                    }
                    else
                    {
                        sUploadServiceURL = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["UPLOAD_SERVICE_URL"]);
                        webRequest = (HttpWebRequest)WebRequest.Create(sUploadServiceURL);
                    }
                    //Payal: RMA-6471 --Ends
                    webRequest.ContentType = "application/xml;charset=\"utf-8\"";
                    webRequest.Accept = "application/xml";
                    webRequest.Method = "POST";
                    if (sCASTicket.Trim() != "")
                    webRequest.Headers.Add("casticket", sCASTicket.Trim());
                    webRequest.Headers.Add("guid", sGUID.Trim());                  


                    //actual code - start
                    // Code waiting for working Integral webservice 
                    InsertSoapEnvelopeIntoWebRequest(soapReq, webRequest);
                    // begin async call to web request.
                    IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                    // suspend this thread until call is complete. You might want to
                    // do something usefull here like update your UI.
                    asyncResult.AsyncWaitHandle.WaitOne();

                    // get the response from the completed web request.  
                    using (webResponse = webRequest.EndGetResponse(asyncResult))
                    {
                        using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                        {
                            Result = rd.ReadToEnd();
                            soapResp = XElement.Parse(Result);
                            Result = soapResp.ToString();
                        }
                    }
                }
                else
                {
                    //actual code start
                    sWSURL = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["WS_INQUIRY_URL"]);
                    sWSURL = sWSURL + "/" + soapReq.XPathSelectElement("./policyNumber").Value; //sanoopsharma xml node corrected
                    if (! string.IsNullOrEmpty(soapReq.XPathSelectElement("./DateOfLoss").Value))
                        sWSURL = sWSURL + "/" + DateTime.Parse(soapReq.XPathSelectElement("./DateOfLoss").Value).ToString("MM-dd-yyyy");
                    webRequest = (HttpWebRequest)WebRequest.Create(sWSURL);


                    webRequest.ContentType = "application/xml;charset=\"utf-8\"";
                    webRequest.Accept = "application/xml";
                    webRequest.Method = "GET";
                    if (sCASTicket.Trim() != "")
                    webRequest.Headers.Add("casticket", sCASTicket.Trim());
                    webRequest.Headers.Add("guid", sGUID.Trim());

                    

                    //IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                    //// suspend this thread until call is complete. You might want to
                    //// do something usefull here like update your UI.
                    //asyncResult.AsyncWaitHandle.WaitOne();

                    // get the response from the completed web request.  
                    //using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                   using(webResponse = webRequest.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                        {
                            Result = rd.ReadToEnd();
                            soapResp = XElement.Parse(Result);
                            Result = soapResp.ToString();
                        }
                    }
                    //actual code end
                }

            }
            catch (WebException webex)
            {
                if (webex.Status == WebExceptionStatus.Timeout)
                {
                    if (iPostAttempt > 0)
                    {
                        oSystemErrors.Add(webex, BusinessAdaptorErrorType.SystemError);
                        PostRequestToIntegral(sSoapRequest, dtPolicySystemInfo, sCASTicket, sGUID, iPostAttempt, ref oSystemErrors);
                    }
                }
                else if (webex.Status == WebExceptionStatus.ProtocolError)
                    throw webex;
            }
            catch (Exception e)
            {
                oSystemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
            }
            finally
            {
                if (webResponse != null)
                {
                    webResponse.Close();
                    webResponse.Dispose();
                }
                if (soapReq != null)
                    soapReq = null;

                if (soapResp != null)
                    soapResp = null;

                if (webRequest != null)
                    webRequest = null;
            }
            return Result;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XElement soapEnvelopeXml, HttpWebRequest webRequest)
        {

            using (Stream stream = webRequest.GetRequestStream())
            {
                byte[] data = getRequestXMLToSoap(soapEnvelopeXml);
                //webRequest.ContentLength = data.Length;
                Stream writer = webRequest.GetRequestStream();
                writer.Write(data, 0, data.Length);
                writer.Close();
            }
        }
        private static byte[] getRequestXMLToSoap(XElement soapEnvelopeXml)
        {
            MemoryStream msout = new MemoryStream();
            soapEnvelopeXml.Save(msout);

            byte[] buffer = new byte[msout.Length];
            byte[] temp = msout.GetBuffer();
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = temp[i];
            }
            msout.Close(); msout.Dispose();

            return buffer;
        }
        private string GenerateCASTicket(DataTable dtPolicySystemInfo, UserLogin oUserLogin, ref BusinessAdaptorErrors oSystemErrors)
        {
            string sTicket = string.Empty;
            string sUserName = string.Empty;
            string sPassword = string.Empty;
            CASTicket cTicket = null;

            try
            {
                string sCASURL = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["CAS_SERVER_URL"]);
                string sCASservice = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["CAS_SERVICE_URL"]);
                if (sCASURL != "" && sCASservice != "")
                {
                string sRMALogin = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["USE_RMA_USER"]);
                if (string.Equals(sRMALogin, "0"))
                {
                    sUserName = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["CFW_USERNAME"]);
                    sPassword = Conversion.ConvertObjToStr(dtPolicySystemInfo.Rows[0]["CFW_PASSWORD"]);
                }
                else
                {
                    sUserName = Conversion.ConvertObjToStr(oUserLogin.UserId);
                    sPassword = oUserLogin.Password;
                }

                cTicket = new CASTicket();
                sTicket = cTicket.GetCASTicket(sCASURL, sCASservice, sUserName, sPassword);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cTicket != null)
                    cTicket = null;
            }
            return sTicket;
        }
        #endregion


        #region SOAP XML templates
       
        private XElement GetPolicySearchSoapRequestTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <PolicySearchRequest>
<ResultCount></ResultCount>
<Function></Function>
<PolicyTypeCodeList>
</PolicyTypeCodeList>
  <DateOfLoss></DateOfLoss>  
  <PolicyNumber></PolicyNumber>
  <AgentDetails>    
      <FullName></FullName>        
    <AgentNumber></AgentNumber>
  </AgentDetails>
  <InsuredOrOwnerName>
    <FirstName></FirstName>    
    <LastName></LastName>
  </InsuredOrOwnerName>  
  <VehicleRegistration></VehicleRegistration>
  <VehicleChassis></VehicleChassis>
  <VehicleEngine></VehicleEngine>
</PolicySearchRequest>");
            return oTemplate;
        }

        private XElement GetPolicyInquireSoapRequestTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <PolicyInquiryRequest>
                        <Function></Function>                                         
                        <policyNumber></policyNumber>                       
                        <DateOfLoss></DateOfLoss>                      
                </PolicyInquiryRequest>");
            return oTemplate;
        }
        #endregion

    }
}


