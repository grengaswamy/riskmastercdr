﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;

namespace RiskmasterService
{
    // NOTE: If you change the class name "Codes" here, you must also update the reference to "Codes" in Web.config.
    [AspNetCompatibilityRequirements(
         RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Logs : RMService, ILogs
    {
        /// <summary>
        /// Add
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public void Add(Logging oLogging)
        {
            try
            {
                Riskmaster.Common.Log.Write(oLogging.Message, oLogging.Category, oLogging.ClientId);
            }
            catch (Exception e)
            {
            }
        }
    }
}
