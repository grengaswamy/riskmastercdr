﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomLogin" in both code and config file together.
    [ServiceContract]
    public interface ICustomLogin
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetRecordID", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetRecordID(AuthData oAuthData);
    }
}
