﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Models;
using Riskmaster.DataModel;
using Riskmaster.Cache;
using System.Text.RegularExpressions;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RMXResourceService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class RMXResourceService : RMService, IRMXResourceService
    {
        /// <summary>
        /// Get Resource By Language And Key
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Key"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        public string GetResourceByLanguageAndKey(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Key, string p_Type, int iClientId=0)
        {
            string sKeyValue = string.Empty;
            try
            {
                string sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}'", p_PageId, p_LanguageCode, p_Key);
                using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetViewConnectionString(iClientId), sSQL))
                {
                    while (oDBReader.Read())
                    {
                        if (p_Type == "1")
                        {
                            sKeyValue = AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[1].ToString();
                        }
                        else
                        {
                            sKeyValue = oDBReader[1].ToString();
                        }
                    }
                }
                if (string.IsNullOrEmpty(sKeyValue))
                {
                    string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                    sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}'", p_PageId, sBaseLangCode, p_Key);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetViewConnectionString(iClientId), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (p_Type == "1")
                            {
                                sKeyValue = AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[1].ToString();
                            }
                            else
                            {
                                sKeyValue = oDBReader[1].ToString();
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(sKeyValue))
                {
                    sKeyValue = p_Key;
                }
            }
            catch (Exception ex)
            {
            }
            return sKeyValue;
        }
        /// <summary>
        /// Get All Resources By Language And PageId
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetAllResourceByLanguageAndPageId(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Type)
        {
            Dictionary<string, string> oDict = new Dictionary<string, string>();
            string sSQL = string.Empty;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            try
            {
                sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1}", p_PageId, p_LanguageCode);
                using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                {
                    while (oDBReader.Read())
                    {
                        if (oDBReader[3].ToString() == "1")
                        {
                            oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                        }
                        else
                        {
                            oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), oDBReader[2].ToString());
                        }
                    }
                }
                if (oDict.Count == 0)
                {
                    sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1}", p_PageId, sBaseLangCode);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (oDBReader[3].ToString() == "1")
                            {
                                oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                            }
                            else
                            {
                                oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), oDBReader[2].ToString());
                            }
                        }
                    }
                }
                sSQL = string.Format("SELECT COUNT(RESOURCE_KEY) FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1}", p_PageId, sBaseLangCode);
                int iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));
                if (oDict.Count != iCount)
                {
                    sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={2})", p_PageId, sBaseLangCode, p_LanguageCode);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (oDBReader[3].ToString() == "1")
                            {
                                oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                            }
                            else
                            {
                                oDict.Add(p_PageId + "|^|" + oDBReader[3].ToString() + "|^|" + oDBReader[1].ToString(), oDBReader[2].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return oDict;
        }
        /// <summary>
        /// Get All Resources By Language And PageId And ResType
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <param name="p_Type"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetAllResourceByLanguageAndPageIdAndResType(int p_DataSourceId, string p_PageId, string p_LanguageCode, string p_Type)
        {
            Dictionary<string, string> oDict = new Dictionary<string, string>();
            string sSQL = string.Empty;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            try
            {
                sSQL = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_TYPE={2}", p_PageId, p_LanguageCode, p_Type);
                using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                {
                    while (oDBReader.Read())
                    {
                        if (p_Type == "1")
                        {
                            oDict.Add(p_PageId + "|^|" + p_Type + "|^|" + oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                        }
                        else
                        {
                            oDict.Add(p_PageId + "|^|" + p_Type + "|^|" + oDBReader[1].ToString(), oDBReader[2].ToString());
                        }
                    }
                }
                if (oDict.Count == 0)
                {
                    sSQL = string.Format("SELECT RESOURCE_KEY,RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_TYPE={2}", p_PageId, sBaseLangCode, p_Type);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (p_Type == "1")
                            {
                                oDict.Add(p_PageId + "|^|" + p_Type + "|^|" + oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                            }
                            else
                            {
                                oDict.Add(p_PageId + "|^|" + p_Type + "|^|" + oDBReader[1].ToString(), oDBReader[2].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return oDict;
        }
        /// <summary>
        /// Get Global Resources By Language
        /// </summary>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_LanguageCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetGlobalResourcesByLanguage(int p_DataSourceId, string p_LanguageCode,int iClientId)
        {
            Dictionary<string, string> oDict = new Dictionary<string, string>();
            string sSQL = string.Empty;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            int iResourceType = 3;
            try
            {
                sSQL = string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_TYPE <> {1}", p_LanguageCode, iResourceType);
                using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource", iClientId), sSQL))
                {
                    while (oDBReader.Read())
                    {
                        if (oDBReader[3].ToString() == "0")
                        {
                            oDict.Add(oDBReader[1].ToString(),oDBReader[2].ToString());
                        }
                        else
                        {
                            oDict.Add(oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                        }
                    }
                }
                if (oDict.Count == 0)
                {
                    sSQL = string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_TYPE <> {1}", sBaseLangCode, iResourceType);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource", iClientId), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (oDBReader[3].ToString() == "0")
                            {
                                oDict.Add(oDBReader[1].ToString(), oDBReader[2].ToString());
                            }
                            else
                            {
                                oDict.Add(oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                            }
                        }
                    }
                    return oDict;
                }
                sSQL = string.Format("SELECT COUNT(RESOURCE_KEY) FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_TYPE <> {1}", sBaseLangCode,iResourceType);
                int iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));
                if (oDict.Count != iCount)
                {
                    sSQL = string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={1} AND RESOURCE_TYPE <> {2}) AND RESOURCE_TYPE <> {2}", sBaseLangCode, p_LanguageCode, iResourceType);
                    using (DbReader oDBReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource", iClientId), sSQL))
                    {
                        while (oDBReader.Read())
                        {
                            if (oDBReader[3].ToString() == "0")
                            {
                                oDict.Add(oDBReader[1].ToString(), oDBReader[2].ToString());
                            }
                            else
                            {
                                oDict.Add(oDBReader[1].ToString(), AppendErrorCode(oDBReader[0].ToString()) + ":" + oDBReader[2].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ee)
            {
            }
            return oDict;
        }
        /// <summary>
        /// Get Connectionstring
        /// </summary>
        /// <param name="strConnString"></param>
        /// <returns></returns>
        public string GetConnectionstring(string strConnString)
        {
            return ConfigurationInfo.GetConnectionString(strConnString,0);
        }
        /// <summary>
        /// Get Connectionstring From DSNId
        /// </summary>
        /// <param name="DataSourceId"></param>
        /// <returns></returns>
        public string GetConnectionstringFromDSNId(int DataSourceId, int ClientId = 0)
        {
            string sConnectionString = string.Empty;
            RiskmasterDatabase oRMDatabase = new RiskmasterDatabase(DataSourceId,ClientId);
            sConnectionString = oRMDatabase.ConnectionString;
            return sConnectionString;
        }
        /// <summary>
        /// GetTimestampForMDIMenu
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="iLangCode"></param>
        /// <returns></returns>
        public string GetTimestampForMDIMenu(string connectionString,int iLangCode)
        {
            string sTime = string.Empty;
            try
            {
                sTime = Convert.ToString(DbFactory.ExecuteScalar(connectionString, "SELECT MAX(DTTM_RCD_LAST_UPD) FROM MDI_MENU WHERE LANGUAGE_CODE=" + iLangCode));
            }
            catch (Exception ee)
            {
            }
            return sTime;
        }
        /// <summary>
        /// Get MDIMenuXML
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="iLangCode"></param>
        /// <returns></returns>
        public string GetMDIMenuXML(string connectionString, int iLangCode)
        {
            string sMDIXml = string.Empty;
            try
            {
                sMDIXml = Convert.ToString(DbFactory.ExecuteScalar(connectionString, "SELECT MDIMENU_XML FROM MDI_MENU WHERE LANGUAGE_CODE=" + iLangCode));
            }
            catch (Exception ex)
            {
            }
            return sMDIXml;
        }
        /// <summary>
        /// Get Changed MDIMenuList
        /// </summary>
        /// <param name="p_LastUpdated"></param>
        /// <param name="sMaxLastUpdated"></param>
        /// <returns></returns>
        public List<string> GetChangedMDIMenuList(string p_LastUpdated,out string sMaxLastUpdated)
        {
            string sLastUpdated = p_LastUpdated;
            List<string> oModifiedList = new List<string>();
            sMaxLastUpdated = p_LastUpdated;
            try
            {
                string sSQL = "SELECT LANGUAGE_CODE,DTTM_RCD_LAST_UPD FROM MDI_MENU WHERE DTTM_RCD_LAST_UPD > '" + p_LastUpdated + "'";
                using (DbReader oDbReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL))
                {
                    while (oDbReader.Read())
                    {
                        string sLangCode = oDbReader["LANGUAGE_CODE"].ToString();
                        sLastUpdated = oDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        if (!string.IsNullOrEmpty(sLangCode))
                        {
                            string sKeyName = sLangCode;
                            oModifiedList.Add(sKeyName);
                            if (sMaxLastUpdated.CompareTo(sLastUpdated) < 0)
                                sMaxLastUpdated = sLastUpdated;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return oModifiedList;
        }
        /// <summary>
        /// Get Page Information
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        public string GetPageInfo(RMResource request, string p_sXMLDocument)
        {
            XmlDocument xmlOut = new XmlDocument();
            StringBuilder sbSql = new StringBuilder();
            XmlElement objNewElm = null;
            DbReader objReader = null;
            XmlElement objLangElm = null;
            XmlElement objFieldElm = null;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            try
            {
                xmlOut.LoadXml(p_sXMLDocument);
                if (string.IsNullOrEmpty(request.LanguageCode))
                    request.LanguageCode = sBaseLangCode;
                sbSql = sbSql.Append("SELECT LANG_ID,LANGUAGE_NAME FROM SYS_LANGUAGES WHERE SUPPORTED_FLAG = -1 ORDER BY LANGUAGE_NAME");
                objLangElm = (XmlElement)xmlOut.SelectSingleNode("//Document/LanguageList");
                objLangElm.SetAttribute("value", request.LanguageCode);
                if (objLangElm != null)
                {
                    using (objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0), sbSql.ToString()))
                    {
                        while (objReader.Read())
                        {
                            objNewElm = xmlOut.CreateElement("option");
                            objNewElm.SetAttribute("value", objReader.GetInt32("LANG_ID").ToString());
                            objNewElm.InnerText = objReader.GetString("LANGUAGE_NAME");
                            objLangElm.AppendChild(objNewElm);
                            objNewElm = null;
                        }
                    }
                }
                if (sbSql.Length != 0)
                    sbSql = sbSql.Remove(0, sbSql.Length);

                if (request.IsFDMPage == Boolean.TrueString)
                {
                    int ViewId = Constants.Views.BASE_VIEWID;
                    string m_netViewFormsConnString = ConfigurationInfo.GetConnectionString("ViewDataSource",0);
                    string sPageName = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, "SELECT PAGE_NAME FROM PAGE_INFO WHERE PAGE_ID=" + request.PageId));
                    sbSql.Append(String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                        , ViewId, sPageName.ToLower().Substring(0,sPageName.ToLower().IndexOf(".aspx"))+".xml", request.DataSourceId));
                    XmlDocument objXMLlstFields = new XmlDocument();
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                    string sXml = string.Empty;
                    XmlDocument p_objXMLDocument = new XmlDocument();
                    p_objXMLDocument.LoadXml(@"<form>
                                                  <group>
                                                  </group>
                                              </form>");
                    if (objReader.Read())
                    {
                        sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
                        objXMLlstFields.LoadXml(sXml);
                        string sDSNname=Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0),"SELECT DSN FROM DATA_SOURCE_TABLE WHERE DSNID="+request.DataSourceId));
                        UserLogin objUsrlogin = new UserLogin(request.User, sDSNname,0);
                        AddSuppDefinition(objUsrlogin, objXMLlstFields); 
                    }
                    objReader.Dispose();
                    XmlElement objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//group"); 
                    XmlNode objNode = objXMLlstFields.GetElementsByTagName("form").Item(0);
                    foreach (XmlNode objElem in objNode.ChildNodes)
                    {
                        if (objElem.NodeType == XmlNodeType.Element)// NODE_ELEMENT=1
                        {
                            if (((XmlElement)objElem).GetAttribute("type") != "id")
                            {
                                if (((XmlElement)objElem).GetAttribute("type") != "hidden")
                                {
                                    if (((XmlElement)objElem).GetAttribute("type") != "javascript")
                                    {
                                        if (objElem.Name.ToLower() == "control")
                                        {
                                            if (((XmlElement)objElem).GetAttribute("required") != null)
                                            {
                                                if (((XmlElement)objElem).GetAttribute("required").ToLower() != "yes")
                                                {
                                                    WriteFieldOption((XmlElement)objElem, objForm, p_objXMLDocument,request.PageId,request.LanguageCode);
                                                }
                                            }
                                        }
                                        else if (objElem.Name.ToLower() == "group")
                                        {
                                            WriteFieldOption((XmlElement)objElem, objForm, p_objXMLDocument, request.PageId, request.LanguageCode);
                                            foreach (XmlNode objGroupChildNode in objElem.ChildNodes)
                                            {
                                                if (objGroupChildNode.Name.ToLower() == "displaycolumn")
                                                {
                                                    foreach (XmlNode objDisplayColumnChildNode in objGroupChildNode.ChildNodes)
                                                    {
                                                        if (objDisplayColumnChildNode.Name.ToLower() == "control")
                                                        {
                                                            if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != null)
                                                            {
                                                                if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "id")
                                                                {
                                                                    if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "hidden")
                                                                    {
                                                                        if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "javascript")
                                                                        {
                                                                            if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "linebreak")  //Aman MITS 31121
                                                                            {
                                                                                if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "blank") //Aman MITS 31220
                                                                                {
                                                                                    WriteFieldOption((XmlElement)objDisplayColumnChildNode, objForm, p_objXMLDocument, request.PageId, request.LanguageCode);
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    XmlNodeList objNodeList = null;
                    objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//group");
                    objNodeList = objXMLlstFields.SelectNodes("form/button");
                    foreach (XmlElement objElem in objNodeList)
                    {
                        WriteFieldOption((XmlElement)objElem, objForm, p_objXMLDocument, request.PageId, request.LanguageCode);
                    }
                    objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//group");
                    objNodeList = objXMLlstFields.SelectNodes("form/buttonscript");
                    foreach (XmlElement objElem in objNodeList)
                    {
                        WriteFieldOption((XmlElement)objElem, objForm, p_objXMLDocument, request.PageId, request.LanguageCode);
                    }
                    XmlNodeList objBaseNodeList = null;
                    objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//group");
                    objBaseNodeList = objXMLlstFields.SelectNodes("form/toolbar/button");
                    foreach (XmlElement objElem in objBaseNodeList)
                    {
                        WriteFieldOption((XmlElement)objElem, objForm, p_objXMLDocument, request.PageId, request.LanguageCode);
                    }
                    XmlNode objNod = xmlOut.ImportNode(p_objXMLDocument.SelectSingleNode("//group"),true);
                    objFieldElm = (XmlElement)xmlOut.SelectSingleNode("//Document/FieldList");
                    XmlNodeList nodelist = objNod.SelectNodes("//option");
                    foreach (XmlElement obj in nodelist)
                    {
                        objFieldElm.AppendChild(obj);
                    }
                }
                else
                {
                    sbSql = sbSql.Append(string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} ORDER BY RESOURCE_TYPE", request.PageId, request.LanguageCode));
                    objFieldElm = (XmlElement)xmlOut.SelectSingleNode("//Document/FieldList");
                    if (objLangElm != null)
                    {
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                        {
                            while (objReader.Read())
                            {
                                objNewElm = xmlOut.CreateElement("option");
                                objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                                objFieldElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }
                        }
                        if (objFieldElm.ChildNodes.Count == 0)
                        {
                            if (sbSql.Length != 0)
                                sbSql = sbSql.Remove(0, sbSql.Length);

                            //Mona: Resource Id should come even when entry has not been made for that language
                            //sbSql = sbSql.Append(string.Format("SELECT RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} ORDER BY RESOURCE_TYPE", request.PageId, sBaseLangCode));
                            sbSql = sbSql.Append(string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} ORDER BY RESOURCE_TYPE", request.PageId, sBaseLangCode));
                            using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                            {
                                while (objReader.Read())
                                {
                                    objNewElm = xmlOut.CreateElement("option");

                                    //Mona: Resource Id should come even when entry has not been made for that language
                                   // objNewElm.SetAttribute("resIdAndresType", "0" + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                    objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                    objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                    objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                                    objFieldElm.AppendChild(objNewElm);
                                    objNewElm = null;
                                }
                            }
                        }
                        string sSQL = string.Format("SELECT COUNT(RESOURCE_KEY) FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1}", request.PageId, sBaseLangCode);
                        int iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));
                        if (objFieldElm.ChildNodes.Count != iCount)
                        {
                            if (sbSql.Length != 0)
                                sbSql = sbSql.Remove(0, sbSql.Length);
                             //Mona: Resource Id should come even when entry has not been made for that language
                            //sbSql = sbSql.Append(string.Format("SELECT RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={2}) ORDER BY RESOURCE_TYPE", request.PageId, sBaseLangCode, request.LanguageCode));
                            sbSql = sbSql.Append(string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM LOCAL_RESOURCE WHERE PAGE_ID={0} AND LANGUAGE_ID={2}) ORDER BY RESOURCE_TYPE", request.PageId, sBaseLangCode, request.LanguageCode));
                            
                            using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                            {
                                while (objReader.Read())
                                {
                                    objNewElm = xmlOut.CreateElement("option");
                                    //Mona: Resource Id should come even when entry has not been made for that language
                                   // objNewElm.SetAttribute("resIdAndresType", "0" + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                    objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                    objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                    objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                                    objFieldElm.AppendChild(objNewElm);
                                    objNewElm = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return xmlOut.OuterXml;
        }

        /// <summary>
        /// Search on basis of Id/text
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        public bool GetSearchInfo(RMResource request, ref string p_sXMLDocument)
        {
            XmlDocument xmlOut = new XmlDocument();
            bool bFound = false;
            DbReader objReader = null;
            StringBuilder sbSql = new StringBuilder();
            XmlElement objNewElm = null;
            XmlElement objFieldElm = null;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];

            xmlOut.LoadXml(p_sXMLDocument);
            objFieldElm = (XmlElement)xmlOut.SelectSingleNode("//Document/FieldList");
            sbSql.Append(string.Format("(SELECT GLOBAL_RESOURCE_ID,RESOURCE_TYPE,RESOURCE_KEY,RESOURCE_VALUE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_TYPE={2} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={1}  AND RESOURCE_TYPE={2})", sBaseLangCode, request.LanguageCode, request.ResourceType));
            if (string.Compare(request.ResourceId,"0")==1)
            {
                sbSql.Append(string.Format(" AND GLOBAL_RESOURCE_ID LIKE '{0}%')",request.ResourceId));
                sbSql.Append(" UNION ");
                sbSql.Append(string.Format("(SELECT GLOBAL_RESOURCE_ID,RESOURCE_TYPE,RESOURCE_KEY,RESOURCE_VALUE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_TYPE={2} AND GLOBAL_RESOURCE_ID LIKE '{1}%' )", request.LanguageCode, request.ResourceId, request.ResourceType));
              
            }
            else
            {
                request.ResourceValue = request.ResourceValue.Replace("'", "''");
                sbSql.Append(string.Format(" AND UPPER(RESOURCE_VALUE) LIKE UPPER('%{0}%'))", request.ResourceValue));
                sbSql.Append(" UNION ");
                sbSql.Append(string.Format("(SELECT GLOBAL_RESOURCE_ID,RESOURCE_TYPE,RESOURCE_KEY,RESOURCE_VALUE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0}  AND RESOURCE_TYPE={2} AND UPPER(RESOURCE_VALUE) LIKE UPPER('%{1}%'))", request.LanguageCode, request.ResourceValue, request.ResourceType));
            }


            using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
            {
                while (objReader.Read())
                {
                    objNewElm = xmlOut.CreateElement("option");
                    objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("GLOBAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                    objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                    objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                    objFieldElm.AppendChild(objNewElm);
                    objNewElm = null;
                    bFound = true;
                }
            }               

           p_sXMLDocument =  xmlOut.InnerXml;

            return bFound;
        }
       //RMA 2176 start
       /// <summary>
       /// GetSearchResult
       /// </summary>
       /// <param name="request"></param>
       /// <param name="p_sXMLDocument"></param>
       /// <param name="iDsnId"></param>
       /// <param name="sSearchType"></param>
       /// <param name="iCatId"></param>
       /// <param name="sDisplayCat"></param>
       /// <param name="iClientId"></param>
       /// <returns></returns>
       public string GetSearchResult(RMResource request, string p_sXMLDocument, int iDsnId, string sSearchType, int iCatId, string sDisplayCat, int iClientId)             
        {
            XmlDocument xmlOut = new XmlDocument();
            DbReader objReader = null;
            XmlElement objNewElm = null;
            XmlElement objFieldElm = null;
            StringBuilder sbSql = new StringBuilder();

            try
            {
                string sConnString = GetConnectionstringFromDSNId(iDsnId, iClientId); 
                xmlOut.LoadXml(p_sXMLDocument);                
                objFieldElm = (XmlElement)xmlOut.SelectSingleNode("//Document/FieldList");
                eDatabaseType dbType = DbFactory.GetDatabaseType(sConnString); 
                switch (sSearchType)
                    {                       
                        
                        case "5":
                              if (dbType == eDatabaseType.DBMS_IS_ORACLE)
                              {
                                  sbSql.Append(string.Format("SELECT DISPLAY_CAT,FIELD_ID AS SEARCH_VALUE ,(FIELD_DESC ||'|^|'||nvl(FIELD_NAME,'')) AS SEARCH_TEXT ,FIELD_TYPE,('srh' || REPLACE('{0}', ' ','') || REPLACE(FIELD_DESC, ' ','')) AS RESOURCE_KEY FROM SEARCH_DICTIONARY  WHERE CAT_ID = {1} AND DISPLAY_CAT= '{2}' ORDER BY DISPLAY_CAT,FIELD_DESC", sDisplayCat, iCatId, sDisplayCat));
                              }
                             else
                              {
                                  sbSql.Append(string.Format("SELECT DISPLAY_CAT,FIELD_ID AS SEARCH_VALUE ,(FIELD_DESC +'|^|'+ ISNULL(FIELD_NAME,'')) AS SEARCH_TEXT ,FIELD_TYPE,('srh' + REPLACE('{0}', ' ','') + REPLACE(FIELD_DESC, ' ','')) AS RESOURCE_KEY FROM SEARCH_DICTIONARY  WHERE CAT_ID = {1} AND DISPLAY_CAT=  '{2}' ORDER BY DISPLAY_CAT,FIELD_DESC", sDisplayCat, iCatId, sDisplayCat));
                              }
                              break;
                        case "6":
                              if (dbType == eDatabaseType.DBMS_IS_ORACLE)
                              {
                                  sbSql.Append(string.Format("select Distinct (DISPLAY_CAT) as SEARCH_TEXT, CAT_ID AS SEARCH_VALUE , ('srh'|| REPLACE(DISPLAY_CAT, ' ','')) AS RESOURCE_KEY  from SEARCH_DICTIONARY where CAT_ID = {0} ORDER BY SEARCH_TEXT", iCatId));
                              }
                              else
                              {
                                  sbSql.Append(string.Format("select Distinct (DISPLAY_CAT) as SEARCH_TEXT, CAT_ID AS SEARCH_VALUE , ('srh'+ REPLACE(DISPLAY_CAT, ' ','')) AS RESOURCE_KEY  from SEARCH_DICTIONARY where CAT_ID = {0} ORDER BY SEARCH_TEXT", iCatId));
                              }
                              break;
                        case "7":
                              if (dbType == eDatabaseType.DBMS_IS_ORACLE)
                              {
                                  sbSql.Append(string.Format("select CAT_ID  AS SEARCH_VALUE , CAT_NAME AS SEARCH_TEXT,('srh'||REPLACE(CAT_NAME, ' ','')) AS RESOURCE_KEY from SEARCH_CAT ORDER BY CAT_NAME"));
                              }
                              else
                              {
                                  sbSql.Append(string.Format("select CAT_ID AS SEARCH_VALUE , CAT_NAME AS SEARCH_TEXT,('srh'+REPLACE(CAT_NAME, ' ','')) AS RESOURCE_KEY from SEARCH_CAT ORDER BY CAT_NAME"));
                              }
                              break;
                    }  
   
                 
                using (objReader = DbFactory.GetDbReader(sConnString, sbSql.ToString()))
                 {
                    while (objReader.Read())
                     {
                        objNewElm = xmlOut.CreateElement("option");
                        objNewElm.SetAttribute("value", objReader.GetInt32("SEARCH_VALUE").ToString());
                        objNewElm.InnerText = objReader.GetString("SEARCH_TEXT");
                        //objNewElm.SetAttribute("resourcekey", objReader.GetString("RESOURCE_KEY"));
                        objNewElm.SetAttribute("resourcekey", Regex.Replace(objReader.GetString("RESOURCE_KEY"), "[^a-zA-Z0-9]+", " "));
                        objFieldElm.AppendChild(objNewElm);
                        objNewElm = null;
                     }
                 }
                 p_sXMLDocument = xmlOut.InnerXml;
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return p_sXMLDocument;
        }
       //RMA 2176 End
        
        /// <summary>
        /// GetGlobalInfo
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_sXMLDocument"></param>
        /// <returns></returns>
        public string GetGlobalInfo(RMResource request, string p_sXMLDocument)
        {
            XmlDocument xmlOut = new XmlDocument();
            StringBuilder sbSql = new StringBuilder();
            XmlElement objNewElm = null;
            DbReader objReader = null;
            XmlElement objLangElm = null;
            XmlElement objFieldElm = null;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            try
            {
                xmlOut.LoadXml(p_sXMLDocument);
                if (string.IsNullOrEmpty(request.LanguageCode))
                    request.LanguageCode = sBaseLangCode;
                sbSql = sbSql.Append("SELECT LANG_ID,LANGUAGE_NAME FROM SYS_LANGUAGES WHERE SUPPORTED_FLAG = -1 ORDER BY LANGUAGE_NAME");
                objLangElm = (XmlElement)xmlOut.SelectSingleNode("//Document/LanguageList");
                objLangElm.SetAttribute("value", request.LanguageCode);
                if (objLangElm != null)
                {
                    using (objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0), sbSql.ToString()))
                    {
                        while (objReader.Read())
                        {
                            objNewElm = xmlOut.CreateElement("option");
                            objNewElm.SetAttribute("value", objReader.GetInt32("LANG_ID").ToString());
                            objNewElm.InnerText = objReader.GetString("LANGUAGE_NAME");
                            objLangElm.AppendChild(objNewElm);
                            objNewElm = null;
                        }
                    }
                }
                if (sbSql.Length != 0)
                    sbSql = sbSql.Remove(0, sbSql.Length);

                sbSql = sbSql.Append(string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} ORDER BY RESOURCE_TYPE", request.LanguageCode));
                objFieldElm = (XmlElement)xmlOut.SelectSingleNode("//Document/FieldList");
                if (objLangElm != null)
                {
                    using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                    {
                        while (objReader.Read())
                        {
                            objNewElm = xmlOut.CreateElement("option");
                            objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("GLOBAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                            objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                            objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                            objFieldElm.AppendChild(objNewElm);
                            objNewElm = null;
                        }
                    }
                    if (objFieldElm.ChildNodes.Count == 0)
                    {
                        if (sbSql.Length != 0)
                            sbSql = sbSql.Remove(0, sbSql.Length);
                        //Mona: Resource Id should come even when entry has not been made for that language
                        //sbSql = sbSql.Append(string.Format("SELECT RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} ORDER BY RESOURCE_TYPE", sBaseLangCode));
                        sbSql = sbSql.Append(string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} ORDER BY RESOURCE_TYPE", sBaseLangCode));
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                        {
                            while (objReader.Read())
                            {
                                objNewElm = xmlOut.CreateElement("option");
                                //Mona: Resource Id should come even when entry has not been made for that language
                                //objNewElm.SetAttribute("resIdAndresType", "0" + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("GLOBAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                                objFieldElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }
                        }
                    }
                    string sSQL = string.Format("SELECT COUNT(RESOURCE_KEY) FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0}", sBaseLangCode);
                    int iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));
                    if (objFieldElm.ChildNodes.Count != iCount)
                    {
                        if (sbSql.Length != 0)
                            sbSql = sbSql.Remove(0, sbSql.Length);
                        //Mona: Resource Id should come even when entry has not been made for that language
                        //sbSql = sbSql.Append(string.Format("SELECT RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={1}) ORDER BY RESOURCE_TYPE", sBaseLangCode, request.LanguageCode));
                        sbSql = sbSql.Append(string.Format("SELECT GLOBAL_RESOURCE_ID,RESOURCE_KEY,RESOURCE_VALUE,RESOURCE_TYPE FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={0} AND RESOURCE_KEY NOT IN (SELECT RESOURCE_KEY FROM GLOBAL_RESOURCE WHERE LANGUAGE_ID={1}) ORDER BY RESOURCE_TYPE", sBaseLangCode, request.LanguageCode));
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql.ToString()))
                        {
                            while (objReader.Read())
                            {
                                objNewElm = xmlOut.CreateElement("option");
                                //Mona: Resource Id should come even when entry has not been made for that language
                                //objNewElm.SetAttribute("resIdAndresType", "0" + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objNewElm.SetAttribute("resIdAndresType", objReader.GetInt32("GLOBAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objNewElm.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                objNewElm.InnerText = objReader.GetString("RESOURCE_KEY");
                                objFieldElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return xmlOut.OuterXml;
        }
        /// <summary>
        /// Save Resource key to db
        /// </summary>
        /// <param name="request"></param>
        /// <param name="p_Message"></param>
        /// <returns></returns>
        public bool SaveResource(RMResource request, out string p_Message)
        {
            string sSQL = string.Empty;
            int iCount = 0;                  
            bool bRet = false;
            p_Message = string.Empty;
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            try
            {
                if (request.ResourceKey == "MDIMenuXML")
                {
                    iCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), "SELECT COUNT(MDIMENU_XML) FROM MDI_MENU WHERE LANGUAGE_CODE=" + request.LanguageCode),0);
                    if (iCount > 0)
                    {
                        string sConnString = ConfigurationInfo.GetConnectionString("ViewDataSource",0);
                        DbWriter dbWriter = DbFactory.GetDbWriter(sConnString);
                        dbWriter.Tables.Add("MDI_MENU");
                        dbWriter.Where.Add((String.Format("LANGUAGE_CODE={0}", request.LanguageCode)));
                        dbWriter.Fields.Add("MDIMENU_XML", request.ResourceValue);
                        dbWriter.Fields.Add("CHILDSCREEN_XML", DbFactory.ExecuteScalar(sConnString, "SELECT CHILDSCREEN_XML FROM MDI_MENU WHERE LANGUAGE_CODE=" + int.Parse(sBaseLangCode)));
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Execute();
                        bRet = true;
                    }
                    else
                    {
                        string sConnString = ConfigurationInfo.GetConnectionString("ViewDataSource",0);
                        DbWriter dbWriter = DbFactory.GetDbWriter(sConnString);
                        eDatabaseType dbType = DbFactory.GetDatabaseType(sConnString);
                        dbWriter.Tables.Add("MDI_MENU");
                        dbWriter.Fields.Add("MDIMENU_XML", request.ResourceValue);
                        dbWriter.Fields.Add("LANGUAGE_CODE", request.LanguageCode);
                        dbWriter.Fields.Add("CHILDSCREEN_XML", DbFactory.ExecuteScalar(sConnString, "SELECT CHILDSCREEN_XML FROM MDI_MENU WHERE LANGUAGE_CODE=" + int.Parse(sBaseLangCode)));
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Execute();
                        bRet = true;
                    }
                }
                else if (request.PageId == "GlobalMessages")
                {
                    //Mona: Resource Id should come even when entry has not been made for that language
                    //To do: should add dsn here
                    sSQL = string.Format("SELECT COUNT(GLOBAL_RESOURCE_ID) FROM GLOBAL_RESOURCE WHERE GLOBAL_RESOURCE_ID={0} AND RESOURCE_KEY='{1}' AND LANGUAGE_ID={2}", request.ResourceId, request.ResourceKey, request.LanguageCode);
                    iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));

                    //if (request.ResourceId != "0")
                    if (iCount != 0)//Updation
                    {
                        DbWriter dbWriter = DbFactory.GetDbWriter(ConfigurationInfo.GetConnectionString("ViewDataSource",0));
                        dbWriter.Tables.Add("GLOBAL_RESOURCE");
                        dbWriter.Where.Add((String.Format("GLOBAL_RESOURCE_ID={0}", request.ResourceId)));
                        dbWriter.Where.Add((String.Format("AND RESOURCE_KEY='{0}'", request.ResourceKey)));
                        dbWriter.Where.Add((String.Format("AND LANGUAGE_ID={0}", request.LanguageCode)));
                        dbWriter.Fields.Add("RESOURCE_VALUE", request.ResourceValue);
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("ADDED_BY_USER", request.User);
                        dbWriter.Execute();
                        p_Message = request.ResourceId.ToString();//Mona: Returning ResourceId
                        bRet = true;
                    }
                    else//Insertion
                    {
                        string sConnString = ConfigurationInfo.GetConnectionString("ViewDataSource",0);
                        DbWriter dbWriter = DbFactory.GetDbWriter(sConnString);
                        eDatabaseType dbType = DbFactory.GetDatabaseType(sConnString);
                        dbWriter.Tables.Add("GLOBAL_RESOURCE");
                        if (dbType == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            dbWriter.Fields.Add("GLOBAL_RESOURCE_ID", DbFactory.ExecuteScalar(sConnString, "SELECT SEQ_GLOBAL_RESOURCE_ID.NEXTVAL FROM DUAL"));
                        }
                        dbWriter.Fields.Add("DATA_SOURCE_ID", 0);
                        dbWriter.Fields.Add("LANGUAGE_ID", request.LanguageCode);
                        dbWriter.Fields.Add("RESOURCE_KEY", request.ResourceKey);
                        dbWriter.Fields.Add("RESOURCE_TYPE", request.ResourceType);
                        dbWriter.Fields.Add("RESOURCE_VALUE", request.ResourceValue);
                        dbWriter.Fields.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("ADDED_BY_USER", request.User);
                        dbWriter.Execute();
                        //Mona: Resource Id should come even when entry has not been made for that language
                        //To do: should add dsn here
                        sSQL = string.Format("SELECT GLOBAL_RESOURCE_ID FROM GLOBAL_RESOURCE WHERE RESOURCE_KEY='{0}' AND LANGUAGE_ID={1}", request.ResourceKey, request.LanguageCode);
                        p_Message =(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL)).ToString();                        
                        bRet = true;
                    }
                }
                else
                {
                    sSQL = string.Format("SELECT COUNT(LOCAL_RESOURCE_ID) FROM LOCAL_RESOURCE WHERE LOCAL_RESOURCE_ID={0} AND RESOURCE_KEY='{1}' AND LANGUAGE_ID={2} AND PAGE_ID={3}", request.ResourceId, request.ResourceKey, request.LanguageCode, request.PageId);
                    iCount = Convert.ToInt32(DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL));

                    //if (request.ResourceId != "0")
                    if (iCount != 0)//Updation
                    {
                        DbWriter dbWriter = DbFactory.GetDbWriter(ConfigurationInfo.GetConnectionString("ViewDataSource",0));
                        dbWriter.Tables.Add("LOCAL_RESOURCE");
                        dbWriter.Where.Add((String.Format("LOCAL_RESOURCE_ID={0}", request.ResourceId)));
                        dbWriter.Where.Add((String.Format("AND PAGE_ID={0}", request.PageId)));
                        dbWriter.Where.Add((String.Format("AND RESOURCE_KEY='{0}'", request.ResourceKey)));
                        dbWriter.Where.Add((String.Format("AND LANGUAGE_ID={0}", request.LanguageCode)));
                        dbWriter.Fields.Add("RESOURCE_VALUE", request.ResourceValue);
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("ADDED_BY_USER", request.User);
                        dbWriter.Execute();
                        p_Message = request.ResourceId;
                        bRet = true;
                    }
                    else
                    {
                        string sConnString = ConfigurationInfo.GetConnectionString("ViewDataSource",0);
                        DbWriter dbWriter = DbFactory.GetDbWriter(sConnString);
                        eDatabaseType dbType = DbFactory.GetDatabaseType(sConnString);
                        dbWriter.Tables.Add("LOCAL_RESOURCE");
                        if (dbType == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            dbWriter.Fields.Add("LOCAL_RESOURCE_ID", DbFactory.ExecuteScalar(sConnString, "SELECT SEQ_LOCAL_RESOURCE_ID.NEXTVAL FROM DUAL"));
                        }
                        dbWriter.Fields.Add("PAGE_ID", request.PageId);
                        dbWriter.Fields.Add("DATA_SOURCE_ID", 0);
                        dbWriter.Fields.Add("LANGUAGE_ID", request.LanguageCode);
                        dbWriter.Fields.Add("RESOURCE_KEY", request.ResourceKey);
                        dbWriter.Fields.Add("RESOURCE_TYPE", request.ResourceType);
                        dbWriter.Fields.Add("RESOURCE_VALUE", request.ResourceValue);
                        dbWriter.Fields.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        dbWriter.Fields.Add("ADDED_BY_USER", request.User);
                        dbWriter.Execute();
                        //Mona: Resource Id should come even when entry has not been made for that language
                        //To do: should add dsn here
                        sSQL = string.Format("SELECT LOCAL_RESOURCE_ID FROM LOCAL_RESOURCE WHERE RESOURCE_KEY='{0}' AND LANGUAGE_ID={1}  AND PAGE_ID={2}", request.ResourceKey, request.LanguageCode, request.PageId);
                        p_Message = (DbFactory.ExecuteScalar(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sSQL)).ToString();                        
                        bRet = true;
                    }
                }
            }
            catch (Exception ex)
            {
                p_Message = ex.Message.ToString();
            }
            finally
            {

            }
            return bRet;
        }
        /// <summary>
        /// Is Show Error Code Enable
        /// </summary>
        /// <returns></returns>
        public string ShowErrorCode()
        {
            return RMConfigurationManager.GetAppSetting("ShowErrorCode").ToString();
        }
        /// <summary>
        /// Append Error Code to the Resource Value
        /// </summary>
        /// <param name="sKeyID"></param>
        /// <returns></returns>
        private static string AppendErrorCode(string sKeyID)
        {
            if (string.IsNullOrEmpty(sKeyID))
            {
                return string.Empty;
            }
            else
            {
                sKeyID = "rmA-" + sKeyID;
                return sKeyID;
            }
        }
        /// <summary>
        /// Gets the title attribute for the xmlelement passed
        /// </summary>
        /// <param name="p_objElement">XML Element</param>
        /// <returns>Title value</returns>
        private string GetFieldTitle(XmlElement p_objElement)
        {
            string sReturnValue = "";
            if (p_objElement.GetAttribute("title") != null)
            {
                if (p_objElement.GetAttribute("title").Trim() != "")
                {
                    sReturnValue = p_objElement.GetAttribute("title");
                }
            }
            //if (p_objElement.GetAttribute("required") != null)
            //{
            //    if (p_objElement.GetAttribute("required").Trim() == "yes")
            //    {
            //        sReturnValue = "- " + sReturnValue;
            //    }
            //}
            if (p_objElement.GetAttribute("title") == "")
            {
                if (p_objElement.GetAttribute("pvtitle") != null)
                    if (p_objElement.GetAttribute("pvtitle").Trim() != "")
                        sReturnValue = p_objElement.GetAttribute("pvtitle");
                    //else
                    //    sReturnValue = "-";
            }
            return sReturnValue;
        }
        /// <summary>
        /// Inserts XML Element to the Node after doing some formatting to the element values
        /// </summary>
        /// <param name="p_objElement">Element to be copied</param>
        /// <param name="p_objNode">Node in which the element needs to be copied</param>
        /// <param name="p_objXMLDocument">For creating new child elements</param>
        private void WriteFieldOption(XmlElement p_objElement, XmlNode p_objNode, XmlDocument p_objXMLDocument,string p_PageId,string p_LangCode)
        {
            string sName = "";
            XmlElement objElemTemp = null;
            sName = p_objElement.Name.ToLower();
            string sFormName = string.Empty;
            DbReader objReader=null;
            XmlElement objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/FormName");
            if (objSelectElement != null)
            {
                sFormName = objSelectElement.InnerText.Replace("%", "|");

            }
            if (sName == "control")
            {
                if (p_objElement.GetAttribute("type") != null)
                {
                    if (p_objElement.GetAttribute("type") == "message")
                    {
                        //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        //objElemTemp.InnerText = "[MS* " + p_objElement.GetAttribute("title") + " *MS]";
                        //p_objNode.AppendChild(objElemTemp);
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (p_objElement.GetAttribute("type") == "textlabel")
                    {
                        //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        //p_objNode.AppendChild(objElemTemp);
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (p_objElement.GetAttribute("type") == "controlgroup")
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        objElemTemp.InnerText = "[Hidden Control Group]";
                        p_objNode.AppendChild(objElemTemp);
                        //string sKeyName = p_objElement.GetAttribute("name");
                        //objElemTemp = p_objXMLDocument.CreateElement("option");
                        //string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        //using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        //{
                        //    bool bReset = false;
                        //    while (objReader.Read())
                        //    {
                        //        objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                        //        objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                        //        bReset = true;
                        //    }
                        //    if (!bReset)
                        //    {
                        //        objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                        //        objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                        //    }
                        //}
                        //objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        //p_objNode.AppendChild(objElemTemp);

                        foreach (XmlElement objCtl in p_objElement.SelectNodes("//control[@name='" + p_objElement.GetAttribute("name") + "']//control"))
                        {
                            if((objCtl.GetAttribute("type")=="button") ||  (objCtl.GetAttribute("type")=="buttonscript"))
                            {
                                string sKeyname = objCtl.GetAttribute("name");
                                //string sKeyName = p_objElement.GetAttribute("name");
                                objElemTemp = p_objXMLDocument.CreateElement("option");
                                string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyname);
                                using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                                {
                                    bool bReset = false;
                                    while (objReader.Read())
                                    {
                                        objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                        objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                        bReset = true;
                                    }
                                    if (!bReset)
                                    {
                                        objElemTemp.SetAttribute("resIdAndresType", "0|^|4");
                                        objElemTemp.SetAttribute("value", GetFieldTitle(objCtl));
                                    }
                                }
                                objElemTemp.InnerText = objCtl.GetAttribute("name");
                                p_objNode.AppendChild(objElemTemp);

                            }
                           else
                           {
                            //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            //objElemTemp.SetAttribute("value", objCtl.GetAttribute("name"));
                            //objElemTemp.InnerText = "+" + GetFieldTitle(objCtl);
                            //p_objNode.AppendChild(objElemTemp);
                            string sKeyName = p_objElement.GetAttribute("name");
                            objElemTemp = p_objXMLDocument.CreateElement("option");
                            string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                            using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                            {
                                bool bReset = false;
                                while (objReader.Read())
                                {
                                    objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                    objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                    bReset = true;
                                }
                                if (!bReset)
                                {
                                    objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                    objElemTemp.SetAttribute("value", GetFieldTitle(objCtl)); //Aman MITS 31121
                                }
                            }
                            objElemTemp.InnerText = objCtl.GetAttribute("name");  //Aman MITS 31121
                            if (objCtl.GetAttribute("type") != "space")
                            {
                               p_objNode.AppendChild(objElemTemp);
                            }
                          }
                        }
                    }
                    else if (p_objElement.GetAttribute("type") == "space")
                    {
                        if (p_objElement.GetAttribute("name").ToString().Substring(0, 7).Equals("pvspace"))
                        {
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                            objElemTemp.InnerText = "[HS* " + "New Space" + " *HS]";
                            p_objNode.AppendChild(objElemTemp);
                            //string sKeyName = p_objElement.GetAttribute("name");
                            //objElemTemp = p_objXMLDocument.CreateElement("option");
                            //string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                            //using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                            //{
                            //    bool bReset = false;
                            //    while (objReader.Read())
                            //    {
                            //        objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                            //        objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                            //        bReset = true;
                            //    }
                            //    if (!bReset)
                            //    {
                            //        objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                            //        objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            //    }
                            //}
                            //objElemTemp.InnerText = p_objElement.GetAttribute("name");
                            //p_objNode.AppendChild(objElemTemp);
                        }
                    }
                    else if (string.Compare(p_objElement.GetAttribute("type"), "phonetype", true) == 0 && !string.IsNullOrEmpty(p_objElement.GetAttribute("helpmsg")))
                    {
                        //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        //p_objNode.AppendChild(objElemTemp);
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (string.Compare(p_objElement.GetAttribute("mandatory"), "true", true) == 0 && sFormName.StartsWith("admintracking|"))
                    {
                        //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg") + "|" + "mandatory");
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        //p_objNode.AppendChild(objElemTemp);
                        //mbahl3 5/01/2011 Power View Issue---End
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (p_objElement.GetAttribute("type") == "buttonscript" || p_objElement.GetAttribute("type") == "button")
                    {
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|4");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else
                    {
                        //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        //p_objNode.AppendChild(objElemTemp);
                        string sKeyName = p_objElement.GetAttribute("name");
                        objElemTemp = p_objXMLDocument.CreateElement("option");
                        string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                        {
                            bool bReset = false;
                            while (objReader.Read())
                            {
                                objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                                objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                                bReset = true;
                            }
                            if (!bReset)
                            {
                                objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                                objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                            }
                        }
                        objElemTemp.InnerText = p_objElement.GetAttribute("name");
                        p_objNode.AppendChild(objElemTemp);
                    }
                }
            }
            else if (sName == "group")
            {
                //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                //objElemTemp = p_objXMLDocument.CreateElement("option");
                //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                //objElemTemp.InnerText = "[*** " + p_objElement.GetAttribute("title") + " ***]";
                //p_objNode.AppendChild(objElemTemp);
                string sKeyName = p_objElement.GetAttribute("name");
                objElemTemp = p_objXMLDocument.CreateElement("option");
                string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                {
                    bool bReset = false;
                    while (objReader.Read())
                    {
                        objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                        objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                        bReset = true;
                    }
                    if (!bReset)
                    {
                        objElemTemp.SetAttribute("resIdAndresType", "0|^|0");
                        objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                    }
                }
                objElemTemp.InnerText = p_objElement.GetAttribute("name");
                p_objNode.AppendChild(objElemTemp);
            }
            else if (sName == "button" || sName == "buttonscript")
            {
                if (p_objElement.ParentNode.Name == "toolbar")
                {
                    //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                    //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("type"));
                    //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                    //p_objNode.AppendChild(objElemTemp);
                    string sKeyName = p_objElement.GetAttribute("type");
                    objElemTemp = p_objXMLDocument.CreateElement("option");
                    string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                    using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                    {
                        bool bReset = false;
                        while (objReader.Read())
                        {
                            objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                            objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                            bReset = true;
                        }
                        if (!bReset)
                        {
                            objElemTemp.SetAttribute("resIdAndresType", "0|^|2");
                            objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                        }
                    }
                    objElemTemp.InnerText = p_objElement.GetAttribute("type");
                    p_objNode.AppendChild(objElemTemp);
                }
                else
                {
                    //objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                    //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                    //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                    //p_objNode.AppendChild(objElemTemp);
                    string sKeyName = p_objElement.GetAttribute("name");
                    objElemTemp = p_objXMLDocument.CreateElement("option");
                    string sbSql = string.Format("SELECT LOCAL_RESOURCE_ID,RESOURCE_VALUE,RESOURCE_TYPE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                    using (objReader = DbFactory.GetDbReader(ConfigurationInfo.GetConnectionString("ViewDataSource",0), sbSql))
                    {
                        bool bReset = false;
                        while (objReader.Read())
                        {
                            objElemTemp.SetAttribute("resIdAndresType", objReader.GetInt32("LOCAL_RESOURCE_ID").ToString() + "|^|" + objReader.GetInt32("RESOURCE_TYPE").ToString());
                            objElemTemp.SetAttribute("value", objReader.GetString("RESOURCE_VALUE").ToString());
                            bReset = true;
                        }
                        if (!bReset)
                        {
                            objElemTemp.SetAttribute("resIdAndresType", "0|^|4");
                            objElemTemp.SetAttribute("value", GetFieldTitle(p_objElement));
                        }
                    }
                    objElemTemp.InnerText = p_objElement.GetAttribute("name");
                    p_objNode.AppendChild(objElemTemp);
                }
            }
        }
        public static void AddSuppDefinition(UserLogin objUsrLogin, XmlDocument p_objXMLDocument)
        {
            string sTableName = "";
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlElement objSuppGrp = null;
            XmlDocument objSuppDoc = null;
            XmlElement objSec = null;
            string sLOBForPowerView = "";
            try
            {
                sTableName = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("supp");
                sLOBForPowerView = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("name");
                if (sTableName.Trim() == "")
                    return;
                objSuppDoc = new XmlDocument();
                objDataModelFactory = new DataModelFactory(objUsrLogin, 0);
                foreach (XmlNode objSection in p_objXMLDocument.SelectNodes("//section"))
                {
                    switch (((XmlElement)objSection).GetAttribute("name"))
                    {
                        case "suppdata":
                            objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals", false);
                            objSupp.LOBForPowerView = sLOBForPowerView;
                            objSupp.TableName = sTableName;
                            break;
                        //case "jurisdata":
                        //    objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                        //    break;
                        //case "acorddata":
                        //    objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord", false);
                        //    objSupp.TableName = "CLAIM_ACCORD_SUPP";
                        //    break;
                    }
                    if (objSupp != null)
                    {
                        objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                        objSuppGrp = p_objXMLDocument.CreateElement("group");
                        foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                        {
                            if (attr.Value == "jurisgroup")
                            {
                                attr.Value = "pvjurisgroup";
                            }

                            objSuppGrp.SetAttribute(attr.Name, attr.Value);
                        }
                        if (sTableName == "FUNDS_TRANS_SPLIT_SUPP" && !string.IsNullOrEmpty(objSuppDoc.SelectSingleNode("//group").InnerXml))
                        {
                            string sTempInnerXML = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            sTempInnerXML = sTempInnerXML.Replace("/Instance/*/Supplementals", "/option/Supplementals");
                            int iPos2 = 1;
                            int iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            do
                            {
                                iPos2 = sTempInnerXML.IndexOf('"', iPos);
                                string sTemp = sTempInnerXML.Substring(iPos, (iPos2 - iPos));
                                string sTemp2 = sTemp;
                                sTemp = sTemp.ToUpper();
                                sTempInnerXML = sTempInnerXML.Replace(sTemp2, sTemp);
                                iPos = iPos2;
                                iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            } while (iPos > 0);
                            sTempInnerXML = sTempInnerXML.Replace("SUPP_", "");

                            objSuppGrp.InnerXml = sTempInnerXML;
                        }
                        else
                        {
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                        }
                        p_objXMLDocument.SelectSingleNode("//form").InsertAfter(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/group[last()]"));
                        objSupp = null;
                    }
                }
            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                objSec = null;
            }
        }
        /// <summary>
        /// GetFormats
        /// </summary>
        /// <param name="iDsnId"></param>
        /// <param name="p_sCountryId"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetFormats(int iDsnId,string p_sCountryId)
        {
            Dictionary<string, string> resDict = new Dictionary<string, string>();
            string sZipCodeFormat="###-##";
            string sPhoneFormat="(###) ###-####";
            string sTemp=string.Empty;
            try
            {
                string sConnString = GetConnectionstringFromDSNId(iDsnId);
                sTemp = Convert.ToString(DbFactory.ExecuteScalar(sConnString, "SELECT ZIPCODE_FORMATDESC FROM ZIPCODE_FORMAT WHERE COUNTRY_ID=" + p_sCountryId));
                if (!string.IsNullOrEmpty(sTemp))
                {
                    resDict.Add("ZipCodeFormat", sTemp);
                    sTemp = string.Empty;
                }
                else
                {
                    resDict.Add("ZipCodeFormat", sZipCodeFormat);
                }
                sTemp = Convert.ToString(DbFactory.ExecuteScalar(sConnString, "SELECT PHONE_FORMATDESC FROM PHONE_FORMAT WHERE COUNTRY_ID=" + p_sCountryId));
                if (!string.IsNullOrEmpty(sTemp))
                {
                    resDict.Add("PhoneFormat", sTemp);
                    sTemp = string.Empty;
                }
                else
                {
                    resDict.Add("PhoneFormat", sPhoneFormat);
                }
            }
            catch (Exception ee)
            {
                resDict.Add("ZipCodeFormat", sZipCodeFormat);
                resDict.Add("PhoneFormat", sPhoneFormat);
            }
            return resDict;
        }
        /// <summary>
        /// ClearGlobalErrorMessageCache
        /// </summary>
        /// <param name="sResId"></param>
        public void ClearGlobalErrorMessageCache(string sResId)
        {
            GlobalResource.ClearResource(sResId);
        }
    }

}
