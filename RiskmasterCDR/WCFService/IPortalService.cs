﻿using System.ServiceModel;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPortalService" in both code and config file together.
    [ServiceContract]
    public interface IPortalService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string ContentInfo(string strSql);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void ContentUpdate(string strSql, string strRow);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void ContentInsert(string strSql, string strRow);
    }

}
