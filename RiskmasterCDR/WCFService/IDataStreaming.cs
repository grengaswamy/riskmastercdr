﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
using System.ServiceModel.Web;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDataStreamingService" here, you must also update the reference to "IDataStreamingService" in Web.config.
    [ServiceContract]
    public interface IDataStreaming
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/create", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void CreateDocument(StreamedDocumentType oStreamedDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/retrieve", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        byte[] RetrieveDocument(StreamedDocumentType oStreamedDocumentType);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/addbrsfile", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UploadBRSFeeScheuleFile(StreamedBRSDocumentType oStreamedBRSDocumentType);
        //mbahl3 jira RMACLOUD-2383
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/addfile", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UploadFile(StreamedUploadDocumentType oStreamedUploadDocumentType);
        //mbahl3 jira RMACLOUD-2383
    }
}
