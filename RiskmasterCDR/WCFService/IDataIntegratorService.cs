﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    [ServiceContract]
    public interface IDataIntegratorService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveSettings(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        DataIntegratorModel RetrieveSettings(DataIntegratorModel objDIModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetAccountList(PPAccountList request, out PPAccountList objReturn);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void RetrieveJobFiles(JobFile request, out JobFile objReturn);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        int SetClaimForReplacement(DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        int ResetClaimForInitialSubmit(DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetISOLossMappings(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetRMClaimTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetCoverageTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetLossTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveRMLossTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetClaimantTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveClaimantTypeMapping(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetAdminTrackFields(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetBankAccountList(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetReserveList4LOB(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void UpdateDAStagingDatabase(DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void PeekDAStagingDatabase(DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
	    void RetrieveFile(DataIntegratorModel objDIModel);			//kkaur25
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string GetRMConfiguratorDATempPath(DataIntegratorModel objDIModel);
        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void UploadDAImportFile(DAImportFile objDAImportFile);

        /// Vsoni5 : 01/29/2011 : MITS 23347, 23357, 23358,23364, 23365, 23439, 23046
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveSettingsCleanup(DataIntegratorModel objDIModel);

        [OperationContract]
        [FaultContract(typeof(RMException))] 
        System.Data.DataSet GetTaxIds(DataIntegratorModel objDIModel, string sEntityList);

        //mihtesham
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyLOB(DataIntegratorModel request, out DataIntegratorModel objDIModel);

        //Developer – abharti5 |MITS 36676 | start
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicySystemNames(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        //Developer – abharti5 |MITS 36676 | end

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyClaimType(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetWCPolicyType(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        int GetPolicyClaimLOBType(DataIntegratorModel request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyCoverageType(DataIntegratorModel request, out DataIntegratorModel objDIModel);
        //sagarwal54 MITS 35386
        [OperationContract]
        string GetrmAPropTypeAndISOPropTypeServiceFunc(DataIntegratorModel objDIModel);
        
		//mihtesham MITS 35711
        [OperationContract]
        string GetrmASPRoleAndISOSPRoleServiceFunc(DataIntegratorModel objDIModel);
		//sagarwal54 MITS 35704
        [OperationContract]
		string GetAdditionalClaimantRoleType(DataIntegratorModel objDIModel);
    }
}