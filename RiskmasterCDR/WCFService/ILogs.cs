﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICodesService" here, you must also update the reference to "ICodesService" in Web.config.
    /// <summary>
    /// ICodes
    /// </summary>
    [ServiceContract]
    public interface ILogs
    {
        /// <summary>
        /// Add
        /// </summary>
        /// <param name="oLogData"></param>
        /// <returns></returns>
        [OperationContract]
        //[FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/write", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Add(Logging oLogging);
        //[OperationContract]
        //[FaultContract(typeof(RMException))]
        //[WebInvoke(Method = "POST", UriTemplate = "/", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //bool Get(Log oLogData);
    }
}
