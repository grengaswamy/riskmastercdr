﻿using Riskmaster.Models;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISSOAuthService" in both code and config file together.
    /// <summary>
    /// Service used for Active dir and LDAP -
    /// Add by kuladeep for Jira-156
    /// </summary>
    [ServiceContract]
    public interface ISSOAuthService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRMServiceType"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/types", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetSSLAuthTypes(RMServiceType oRMServiceType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRMServiceType"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/provider", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataSet GetADProvider(RMServiceType oRMServiceType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRMServiceType"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ldapprovider", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataSet GetLDAPProvider(RMServiceType oRMServiceType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRiskmasterMembershipProvider"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/add", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void InsertADProvider(RiskmasterMembershipProvider oRiskmasterMembershipProvider);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRiskmasterMembershipProvider"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/addldap", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void InsertLDAPProvider(RiskmasterMembershipProvider oRiskmasterMembershipProvider);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRiskmasterMembershipProvider"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/updateprovider", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateADProvider(RiskmasterMembershipProvider oRiskmasterMembershipProvider);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRiskmasterMembershipProvider"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/updateldapprovider", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateLDAPProvider(RiskmasterMembershipProvider oRiskmasterMembershipProvider);
    }
}
