﻿//#define DEBUG
#undef DEBUG
using System;
using System.Text;
using System.ServiceModel;
using System.Xml;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using RiskmasterService.MDAGuidelinesService;
using System.Net;
using Riskmaster.Common;
using System.Xml.Linq;
namespace RiskmasterService
{
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif
    public class MDAService : RMService, IMDAService
    {
        public string GetMDATopics(string medicalcode, string jobclass)
        {
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetMDATopics";
            XmlDocument xmlRequest = null;
            string license = string.Empty;
            // npadhy For the Servers where the Requests are routed though a Proxy, we might need to provide the 
            // Network credentials User Name, Password and Domain
            string sProxyAddress = string.Empty;
            string sProxyPort = string.Empty;
            string sNCUserName = string.Empty;
            string sNCPassword = string.Empty;
            string sNCDomain = string.Empty;
            string sRedirectURL = string.Empty;
            string sReturn = string.Empty;
            mdguidelinesWebServiceSoapClient client = null;

            XmlNode xmlReturn = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors();
                NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("MDASettings");
                license = nvCollSettings["MDALicenseKey"];
                sProxyAddress = nvCollSettings["ProxyAddress"];
                sProxyPort = nvCollSettings["ProxyPort"];
                sNCUserName = nvCollSettings["UserName"];
                sNCPassword = nvCollSettings["Password"];
                sNCDomain = nvCollSettings["Domain"];
                sRedirectURL = nvCollSettings["RedirectURL"];
                ServicePointManager.Expect100Continue = false;

                if (!string.IsNullOrEmpty(sProxyAddress))
                {
                    if (!string.IsNullOrEmpty(sProxyPort))
                        sProxyAddress += String.Format(":{0}", sProxyPort);
                    WebProxy wproxy = new WebProxy(sProxyAddress, true);
                    if (!string.IsNullOrEmpty(sNCDomain))
                        wproxy.Credentials = new NetworkCredential(sNCUserName, sNCPassword, sNCDomain);
                    else
                        wproxy.Credentials = new NetworkCredential(sNCUserName, sNCPassword);
                    //or you can construct your own credentials via System.Net.NetworkCredential
                    WebRequest.DefaultWebProxy = wproxy;
                }

                client = new mdguidelinesWebServiceSoapClient();
                xmlReturn = client.DisabilityDurationByMedicalCodeRequest(license, medicalcode, "icd9", jobclass, "");
                if (xmlReturn != null)
                {
                    sReturn = xmlReturn.OuterXml;

                    //Check if the any valid URL has been retured or not. If not, redirect to default search page
                    if (xmlReturn.SelectSingleNode("//RelatedMonographUrls/RelatedMonographUrl/MonographUrl") == null)
                    {
                        XElement oReturn = XElement.Parse(sReturn);
                        sRedirectURL = sRedirectURL.Replace("[ICD9_code]", medicalcode).Replace("[MDALicenseKey]", license);
                        XElement oRedirectUrl = new XElement("RelatedMonographUrls",
                                                    new XElement("RelatedMonographUrl",
                                                        new XElement("MonographUrl", sRedirectURL)));
                        oReturn.Add(oRedirectUrl);
                        sReturn = oReturn.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                //Creating a request xml only when there is error bcoz it is needed to log the request
                XElement MDATopics = new XElement("MDATopics",
                                        new XElement("medicalcode", medicalcode),
                                        new XElement("jobclass", jobclass));

                xmlRequest = new XmlDocument();
                xmlRequest.LoadXml(MDATopics.ToString());
                //Creating a request xml only when there is error
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                client.Close();
                systemErrors = null;
            }

            return sReturn;
        }
    }
}
