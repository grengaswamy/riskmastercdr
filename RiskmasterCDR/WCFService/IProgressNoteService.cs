﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IProgressNoteService" here, you must also update the reference to "IProgressNoteService" in Web.config.
    [ServiceContract]
    public interface IProgressNoteService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DoWork();
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void LoadProgressNotesPartial(ProgressNotesType request, out ProgressNotesType objReturn);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void LoadProgressNotes(ProgressNotesType request, out ProgressNotesType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DeleteNote(ProgressNotesType request, out ProgressNotesType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void PrintProgressNotes(ProgressNotesType request, out ProgressNotesType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetNoteDetails(GetNoteDetailsObject request, out GetNoteDetailsObject objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SelectClaim(SelectClaimObject request, out SelectClaimObject objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveNotes(ProgressNote request, out ProgressNote objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void LoadTemplates(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveTemplates(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DeleteTemplate(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetTemplateDetails(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetNotesHeaderOrder(ProgressNoteSettings request, out ProgressNoteSettings objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveNotesSettings(ProgressNoteSettings request, out ProgressNoteSettings objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string LoadProgressNotesPartialForMobileAdj(string xmlRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetNotesCaption(ProgressNotesTypeChange request, out ProgressNotesTypeChange objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string SaveNotesForMobileAdj(string sRequest);
        /*gbindra MITS#34104 WWIG GAP15 02042014 START*/
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SelectClaimant(SelectClaimantObject request, out SelectClaimantObject objReturn);
        /*gbindra MITS#34104 WWIG GAP15 02042014 END*/
    }
}
