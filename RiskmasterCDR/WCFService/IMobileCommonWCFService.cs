﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
 //Added by nsharma202 for mobile app
using System.IO;
using System.ServiceModel.Web;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "ICommonWCFService" here, you must also update the reference to "ICommonWCFService" in Web.config.
    [ServiceContract(Namespace = "http://csc.com/Riskmaster/Webservice/Common")]
    public interface IMobileCommonWCFService
    {
        [OperationContract]
        string ProcessRequest(string xmlRequest);
    }
}