﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;

namespace RiskmasterService
{
    // NOTE: If you change the class name "Codes" here, you must also update the reference to "Codes" in Web.config.
    [AspNetCompatibilityRequirements(
         RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class CodesService : RMService, ICodesService
    {
        /// <summary>
        /// Get QuickLookUp List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        
        public void GetQuickLookUp(QuickLookupRequest request, out CodeListType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetQuickLookUp";
            UserLogin oUserLogin = null;
            CodesListAdaptor objCodes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new CodeListType();
                objCodes = new CodesListAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objCodes, out oUserLogin, ref  systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objCodes.QuickLookupData(request, ref  objReturn, ref errOut);
            }
            catch (Exception e) 
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        /// <summary>
        /// Get Codes List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public void GetCodes(CodeTypeRequest request, out CodeListType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetCodes"; 
            UserLogin oUserLogin = null;
            CodesListAdaptor objCodes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new CodeListType();
                objCodes = new CodesListAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objCodes, out oUserLogin, ref  systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objCodes.GetCodesNew(request, ref  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }



    }
}
