﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDiaryCalendar" here, you must also update the reference to "IDiaryCalendar" in Web.config.
    [ServiceContract]
    public interface IDiaryCalendar
    {
        [OperationContract]
        DiaryOutput GetDiaryList(DiaryInput DiaryInputObject);
        [OperationContract]
        DiaryOutput GetCalendarView(DiaryInput DiaryInputObject);
        [OperationContract]
        DiaryOutput GetOnLoadInformation(DiaryInput DiaryInputObject);
    }
}
