﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Riskmaster.Models;
using Riskmaster.BusinessAdaptor.VPPAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;

namespace RiskmasterService
{
    // NOTE: If you change the class name "VPPService" here, you must also update the reference to "VPPService" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class VPP : RMService, IVPP
    {
        /// <summary>
        /// retrieve aspx page from the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public VPPFileResponse GetFileContent(VPPFileRequest oVPPFileRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetFileContent";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            VPPFileResponse response = null;
            try
            {
                response = new VPPFileResponse();
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oVPPFileRequest.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(oVPPFileRequest.ClientId);
                bResult = objVPPBusinessAdaptor.GetFileContent(oVPPFileRequest, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return response;
        }

        /// <summary>
        /// get the max time stamp for all the pages in the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public VPPTimeStampResponse GetInitialTimeStamp(RMServiceType oRMServiceType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetInitialTimeStamp";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            VPPTimeStampResponse response = null;
            try
            {
                response = new VPPTimeStampResponse();
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oRMServiceType.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(oRMServiceType.ClientId);
                bResult = objVPPBusinessAdaptor.GetInitialTimeStamp(oRMServiceType, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return response;
        }

        /// <summary>
        /// get the time stamp for a specific file in the database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public VPPTimeStampResponse GetFileTimeStamp(VPPFileRequest oVPPFileRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetFileTimeStamp";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            VPPTimeStampResponse response = null;
            try
            {
                response = new VPPTimeStampResponse();
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oVPPFileRequest.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(oVPPFileRequest.ClientId);
                bResult = objVPPBusinessAdaptor.GetFileTimeStamp(oVPPFileRequest, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return response;
        }

        /// <summary>
        /// get the list of pages which have been modified during last pooling cycle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public VPPModifiedPagesResponse GetModifiedPageList(VPPModifiedPagesRequest oVPPModifiedPagesRequest)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetModifiedPageList";
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            VPPModifiedPagesResponse response = null;
            try
            {
                response = new VPPModifiedPagesResponse();
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oVPPModifiedPagesRequest.ClientId);
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(oVPPModifiedPagesRequest.ClientId);
                bResult = objVPPBusinessAdaptor.GetModifiedPageList(oVPPModifiedPagesRequest, out response, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return response;
        }

        /// <summary>
        /// get the data source id for this session
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public VPPGetDataSourceIDResponse GetDataSourceID(RMServiceType oRMServiceType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetDataSourceID";
            UserLogin oUserLogin = null;
            VPPBusinessAdaptor objVPPBusinessAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            VPPGetDataSourceIDResponse response = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                response = new VPPGetDataSourceIDResponse();
                systemErrors = new BusinessAdaptorErrors(oRMServiceType.ClientId);
                // objReturn = new MDINavigationTreeNodeProfileResponse();
                objVPPBusinessAdaptor = new VPPBusinessAdaptor(oRMServiceType.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oRMServiceType, out xmlRequest, functionName, objVPPBusinessAdaptor, out oUserLogin, ref systemErrors);

                if (oUserLogin != null)
                    response.DataSourceID = oUserLogin.DatabaseId;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                logErrors(functionName, xmlRequest, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            return response;
        }
    }
}
