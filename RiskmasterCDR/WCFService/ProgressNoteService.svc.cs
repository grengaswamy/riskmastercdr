﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using System.Text.RegularExpressions;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RiskmasterService
{
    // NOTE: If you change the class name "ProgressNoteService" here, you must also update the reference to "ProgressNoteService" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class ProgressNoteService : RMService, IProgressNoteService
    {
        public void DoWork()
        {
        }
        //rsolanki2: Enhc Notes ajax updates
        public void LoadProgressNotesPartial(ProgressNotesType request, out ProgressNotesType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadProgressNotesPartialWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.OnLoadIntegratePartial(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
		//mbahl3 enhance notes enhancement

        public void GetNotesCaption(ProgressNotesTypeChange request, out ProgressNotesTypeChange objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNotesCaption";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNotesTypeChange();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.GetNotesCaption(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }

	//mbahl3 enhance notes enhancement
        public void LoadProgressNotes(ProgressNotesType request, out ProgressNotesType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadProgressNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.OnLoadIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void DeleteNote(ProgressNotesType request, out ProgressNotesType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "DeleteNoteWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.DeleteProgressNoteIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        public void PrintProgressNotes(ProgressNotesType request, out ProgressNotesType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "PrintProgressNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.PrintProgressNotesIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void GetNoteDetails(GetNoteDetailsObject request, out GetNoteDetailsObject objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNoteDetailsWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new GetNoteDetailsObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.GetNoteDetailsIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void SelectClaim(SelectClaimObject request, out SelectClaimObject objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SelectClaimWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new SelectClaimObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.SelectClaimIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        /*gbindra MITS#34104 WWIG GAP15 02042014 START*/
        public void SelectClaimant(SelectClaimantObject request, out SelectClaimantObject objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SelectClaimantWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new SelectClaimantObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.SelectClaimantIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        /*gbindra MITS#34104 WWIG GAP15 02042014 END*/
        public void SaveNotes(ProgressNote request, out ProgressNote objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNote();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.SaveNotesIntegrate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
           
        }
        public void LoadTemplates(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadTemplates";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.LoadTemplates(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void SaveTemplates(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveTemplates";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.SaveTemplates(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void DeleteTemplate(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "DeleteTemplate";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.DeleteTemplate(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void GetTemplateDetails(ProgressNoteTemplates request, out ProgressNoteTemplates objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetTemplateDetails";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.GetTemplateDetails(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void GetNotesHeaderOrder(ProgressNoteSettings request, out ProgressNoteSettings objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNotesHeaderOrder";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteSettings();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.GetNotesHeaderOrder(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void SaveNotesSettings(ProgressNoteSettings request, out ProgressNoteSettings objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveNotesSettings";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new ProgressNoteSettings();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.SaveNotesSettings(request, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public string LoadProgressNotesPartialForMobileAdj(string sRequest)
        {
            ProgressNotesType objNotes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            string functionName = "LoadProgressNotesPartial";
            string sToken = string.Empty;
            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            XmlElement xmlIn = null;
            ProgressNotesType objNotesList = null;
            XmlElement objElement;
            XmlElement objXmlDoc = null;
            string sResponse = "test";
            string XmlizedString = null;
            MemoryStream memoryStream = null;


            try
            {
                xmlRequest.LoadXml(sRequest);
                sToken = xmlRequest.SelectSingleNode("//Authorization").InnerText;
                objNotes = new ProgressNotesType();
                xmlIn = (XmlElement)xmlRequest.SelectSingleNode("//Message//Document");
               
                objNotes.FormName = xmlRequest.SelectSingleNode("//ProgressNotes//FORM_NAME").InnerText;
                if (objNotes.FormName == "SEARCH")
                {
                    objNotes.ActivateFilter = true;
                    objNotes.objFilter.ActivityFromDate = xmlRequest.SelectSingleNode("//ProgressNotes//ACTIVITY_FROMDATE").InnerText;
                    objNotes.objFilter.ActivityToDate = xmlRequest.SelectSingleNode("//ProgressNotes//ACTIVITY_TODATE").InnerText;

                }
                else
                {
                    objNotes.ActivateFilter = false;
                    objNotes.objFilter.ActivityFromDate = "";
                    objNotes.objFilter.ActivityToDate = "";

                }
                objNotes.ApplySort = "";
                objNotes.Ascending = "";
                objNotes.ApplySort = "";
                objNotes.Ascending = "";
                objNotes.bCreatePermission = true;
                objNotes.bDeletePermission = true;
                objNotes.bEditPermission = true;
               
                objNotes.bPrintPermission = true;
                objNotes.bTemplatesPermission = true;
                objNotes.bViewAllNotesPermission = true;
                objNotes.Claimant = "";
                objNotes.ClaimID = 0;
                objNotes.ClaimProgressNoteId = "";
                
                objNotes.DataType = "";
                objNotes.Direction = "";
                objNotes.EnhancedTimeLimit = "";
                objNotes.EventID = 0;
                objNotes.EventNumber = "";
                
                objNotes.FreezeText = false;
                objNotes.FunctionToCall = "";
                objNotes.LOB = "";
               
                objNotes.NoteTypeCodeId = "";
               
                objNotes.objFilter.ClaimIDList = "";
                objNotes.objFilter.EnteredByList = "";
                objNotes.objFilter.NotesTextContains = "";
                objNotes.objFilter.NoteTypeList = "";
                objNotes.objFilter.SortBy = "";

                objNotes.objFilter.UserTypeList = "";
                objNotes.PageNumber = 0;
                
                objNotes.PrintMultiplePages = true;
                objNotes.PrintOrder1 = "";
                objNotes.PrintOrder2 = "";
                objNotes.PrintOrder3 = "";
                objNotes.ProgressNoteReportPdf = "";
                objNotes.ShowDateStamp = false;
                objNotes.SortCol = "";
                objNotes.SortColumn = "";
                objNotes.Temp = "";
                objNotes.TemplateId = "";
                objNotes.TemplateName = "";
                objNotes.TotalNumberOfPages = 0;
                objNotes.UserEditingRights = false;
                objNotes.UserName = "";
                objNotes.Token = sToken;
                objNotes.bIsMobileAdjuster = true;
                objNotes.ClaimNumber = xmlRequest.SelectSingleNode("//ProgressNotes//CLAIM_NUMBER").InnerText;

                LoadProgressNotesPartial(objNotes, out objNotesList);

                

                memoryStream = new MemoryStream();
                XmlSerializerNamespaces sNameSpace = new XmlSerializerNamespaces();
                sNameSpace.Add("", "");
                XmlSerializer xSerializer = new XmlSerializer(objNotesList.objProgressNoteList.GetType());
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xSerializer.Serialize(xmlTextWriter, objNotesList.objProgressNoteList, sNameSpace);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                UTF8Encoding encoding = new UTF8Encoding();
                XmlizedString = encoding.GetString(memoryStream.ToArray());
                xmlResponse.LoadXml(XmlizedString.Remove(0, 39));

                //This is only for ADD as a sucess message was required.
                if (objNotes.FormName != "SEARCH")
                {
                    XmlNode prgNote = xmlResponse.SelectSingleNode("ProgressNote");

                    XmlNode message = xmlResponse.CreateNode(XmlNodeType.Element, "MsgStatus", null);

                    if (objNotesList.ClaimID > 0)
                    {
                        message.InnerText = "Success";
                    }
                    else
                    {
                        message.InnerText = "Failure";
                    }

                    if (prgNote != null)
                        prgNote.AppendChild(message);

                }
                return xmlResponse.InnerXml.ToString();
            }

            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        public string SaveNotesForMobileAdj(string sRequest)
        {
            ProgressNote objNotes = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            string functionName = "LoadProgressNotesPartial";
            string sToken = string.Empty;
            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            XmlElement xmlIn = null;
            ProgressNote objNotesList = null;
            XmlElement objElement;
            XmlElement objXmlDoc = null;
            string sResponse = "test";
            string XmlizedString = null;
            MemoryStream memoryStream = null;
            //aman   Mobile Adjuster
            XElement xTemp = null;
            string sNoteText = string.Empty;
            string sNode = string.Empty;
            int iStartIndex = -1;
            int iLastIndex = -1;
            int iLenOfNode = 0;
            string sEncodedNoteText = string.Empty;
            int iFrom = -1;
            int iTo = -1;
            //aman Mobile Adjuster

            try
            {
                objNotes = new ProgressNote();   //aman Mobile Adjuster-----start
                sNode = "<NOTE_TEXT>";
                iLenOfNode = sNode.Length;
                iStartIndex = sRequest.IndexOf("<NOTE_TEXT>");
                iLastIndex = sRequest.IndexOf("</NOTE_TEXT>");
                iFrom = iStartIndex + iLenOfNode;
                iTo = iLastIndex - iStartIndex - iLenOfNode;
                sNoteText = sRequest.Substring(iFrom, iTo);
                sEncodedNoteText = Utilities.StringToXMLTags(sNoteText);
                sRequest = sRequest.Replace(sNoteText, sEncodedNoteText);  //aman Mobile Adjuster-----end
                xmlRequest.LoadXml(sRequest);
                sToken = xmlRequest.SelectSingleNode("//Authorization").InnerText;
                xmlIn = (XmlElement)xmlRequest.SelectSingleNode("//Message//Document");
                objNotes.ClaimID = "-1";
                objNotes.ClaimNumber = xmlRequest.SelectSingleNode("//ProgressNotes//CLAIM_NUMBER").InnerText;
                objNotes.DateEntered = xmlRequest.SelectSingleNode("//ProgressNotes//ACTIVITY_DATE").InnerText;
                objNotes.EventID = "0";

                objNotes.IsNoteEditable = "";
                objNotes.NewRecord = "true";
                objNotes.NoteMemo = sNoteText;   //aman
                objNotes.NoteMemoCareTech = objNotes.NoteMemo;

                objNotes.NoteTypeCode = xmlRequest.SelectSingleNode("//ProgressNotes//NOTE_TYPE_CODE").InnerText;
                objNotes.TemplateID = 0;

                objNotes.Token = sToken;
                objNotes.bIsMobileAdjuster = true;
                SaveNotes(objNotes, out objNotesList);

                memoryStream = new MemoryStream();
                XmlSerializerNamespaces sNameSpace = new XmlSerializerNamespaces();
                sNameSpace.Add("", "");
                XmlSerializer xSerializer = new XmlSerializer(objNotesList.GetType());
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xSerializer.Serialize(xmlTextWriter, objNotesList, sNameSpace);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                UTF8Encoding encoding = new UTF8Encoding();
                XmlizedString = encoding.GetString(memoryStream.ToArray());
                xmlResponse.LoadXml(XmlizedString.Remove(0, 39));


                XmlNode PrgNote = xmlResponse.SelectSingleNode("ProgressNote");

                XmlNode Msg = xmlResponse.CreateNode(XmlNodeType.Element, "MsgStatus", null);
                //Adding success message to Progress Notes...
                if (objNotesList.ClaimProgressNoteId > 0)
                    Msg.InnerText = "Success";
                else
                    Msg.InnerText = "Failure";

                PrgNote.AppendChild(Msg);

                return xmlResponse.InnerXml.ToString();
            }

            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);

                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }
    }
}
