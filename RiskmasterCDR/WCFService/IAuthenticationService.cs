﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;//asharma326 MITS 27586

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IAuthenticationService" here, you must also update the reference to "IAuthenticationService" in Web.config.
    [ServiceContract]
    public interface IAuthenticationService
    {

        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <param name="strMessage">string containing the exception message if any</param>
        /// <returns>boolean indicating whether or not the user has been successfully authenticated</returns>
        [OperationContract]
        bool AuthenticateUser(string strUserName, string strPassword, out string strMessage);

 
        /// <summary>
        /// Authenticates the SMS user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <param name="strMessage">string containing the exception message if any</param>
        /// <returns>boolean indicating whether or not the user has been successfully authenticated</returns>
        
        [OperationContract]
        bool AuthenticateSMSUser(string strUserName, string strPassword, out string strMessage);

        /// <summary>
        /// Validates whether a specific user exists within RISKMASTER
        /// for scenarios that require pass-through credentials from 3rd party Single-Sign On applications
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>boolean indicating whether or not the user has been successfully authenticated</returns>
        [OperationContract]
        bool SingleSignOnUser(string strUserName);

        [OperationContract]
        /// <summary>
        /// Provides the ability to change a user's RISKMASTER password
        /// </summary>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_sOldPassword"></param>
        /// <param name="p_sNewPassword"></param>
        /// <returns>boolean indicating whether or not the user's password was successfully changed</returns>
        bool ChangePassword(string p_sLoginName, string p_sOldPassword, string p_sNewPassword);
        //[OperationContract]
        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        //bool AuthenticateUser(string strUserName, string strPassword);

        [OperationContract]
        /// <summary>
        /// Gets the list of available Authentication Providers for the system
        /// </summary>
        /// <returns>Generic Dictionary collection containing all available Authentication/Membership Providers</returns>
        Dictionary<string, string> GetAuthenticationProviders();

        [OperationContract]
        /// <summary>
        /// Gets the list of available DSNs for a particular user name
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>Generic Dictionary collection containing all available User DSNs</returns>
        Dictionary<string, string> GetUserDSNs(string strUserName);

        [OperationContract]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        Dictionary<string, string> GetUserViews(string strUserName, string strDatabaseName, string strSelectedDsnId);
        [OperationContract]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        string GetCustomViews(string strSelectedDsnId);
        [OperationContract]
        /// <summary>
        /// Gets the user's session ID
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// <returns>string containing the user's authenticated Session ID</returns>
        string GetUserSessionID(string strUserName, string strDSNName);

        //PSARIN2 R8 Perf Imp. New Session Info Get Method
        [OperationContract]

        /// <summary>
        /// Gets the user's session info based on their authentication credentials
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// <param name="strDsnId">string containing the selected User's DSN id</param>
        /// <returns>Dictionary<string, string> containing the user's authenticated Session Info</returns>
        Dictionary<string, string> GetUserSessionInfo(string strUserName, string strDSNName, string strDsnId);

        /// <summary>
        /// Logs out a user and removes their session information from the database
        /// </summary>
        /// <param name="strSessionID">string containing the current user's Session ID</param>
        [OperationContract]
        void LogOutUser(string strSessionID);
        [OperationContract]
        bool GetBOBSetting(string p_sloginUserName, string sDataSourceName);
        /// <summary>
        /// GetLanguageCode
        /// </summary>
        /// <param name="p_Username">p_sloginUserName</param>
        /// <param name="p_DSNName">p_DSNName</param>
        /// <returns>LangCode</returns>
        [OperationContract]
        string GetLanguageCode(string p_Username, string p_DSNName);
        /// <summary>
        /// GetBaseLanguageCode
        /// </summary>
        /// <returns>BaseLangCode</returns>
        [OperationContract]
        string GetBaseLanguageCode();
        //Asharma326 MITS 27586 Starts
        /// <summary>
        /// Fucntion for reseting User password and send mail to user.
        /// </summary>
        /// <param name="p_userId"></param>
        /// <returns></returns>
        [OperationContract]
        ForgotPasswordGenericResponse ForgotPasswordSendMail(string s_UserId);
        /// <summary>
        /// function for resetting password.
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <param name="p_password"></param>
        /// <returns></returns>
        [OperationContract]
        bool IsPasswordReset(string p_UserId, int iClientId);
        //Asharma326 MITS 27586 Ends
		 /// <summary>
        /// GetDateFormatFromLoginDetails
        /// </summary>
        /// <returns>DateFormat</returns>
        [OperationContract]
        string GetDateFormatFromLoginDetails(string p_Username, string p_DSNName);

                
        /// <summary>
        /// GetDateFormatFromLangCode
        /// </summary>
        /// <returns>DateFormat</returns>
        [OperationContract]
        string GetDateFormatFromLangCode(string p_LangCode, int iClientId);
        /// <summary>
        /// GetDateFormat
        /// </summary>
        /// <returns>DateFormat</returns>
    }
}
