﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using Riskmaster.Security.Authentication;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "ISSOService" here, you must also update the reference to "ISSOService" in Web.config.
    /// <summary>
    /// Interface for SSO Windows Communication Foundation Service
    /// which defines all of the required Operation Contracts and Service Known Types
    /// </summary>
    [ServiceContract]
    public interface ISSOService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetSSLAuthTypes();

        #region Select Statements
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DataSet GetADProvider();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DataSet GetLDAPProvider(); 
        #endregion

        #region Insert Statements
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objADProvider"></param>
        [OperationContract]
        [ServiceKnownType(typeof(RiskmasterADMembershipProvider))]
        void InsertADProvider(RiskmasterADMembershipProvider objADProvider);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objLDAPProvider"></param>
        [OperationContract]
        [ServiceKnownType(typeof(RiskmasterLDAPMembershipProvider))]
        void InsertLDAPProvider(RiskmasterLDAPMembershipProvider objLDAPProvider);  
        #endregion

        #region Update Statements
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objADProvider"></param>
        [OperationContract]
        [ServiceKnownType(typeof(RiskmasterADMembershipProvider))]
        void UpdateADProvider(RiskmasterADMembershipProvider objADProvider);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objLDAPProvider"></param>
        [OperationContract]
        [ServiceKnownType(typeof(RiskmasterLDAPMembershipProvider))]
        void UpdateLDAPProvider(RiskmasterLDAPMembershipProvider objLDAPProvider); 
        #endregion

    } 
}
