﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;
using System.ServiceModel.Web;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRSWService" in both code and config file together.
    [ServiceContract]
    public interface IRSWService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/GetReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse GetReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/OnLoadInformation", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse OnLoadInformation(ReserveWorksheetRequest oReserveWorksheetRequest);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/SaveReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse SaveReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/SubmitReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse SubmitReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);



        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/ApproveReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse ApproveReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/RejectReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse RejectReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/PrintReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse PrintReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);



        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/DeleteReserveWorkSheet", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse DeleteReserveWorkSheet(ReserveWorksheetRequest oReserveWorksheetRequest);



        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/ListFundsTransactions", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse ListFundsTransactions(ReserveWorksheetRequest oReserveWorksheetRequest);



        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/ApproveReserveWorksheetFromApprovalScreen", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest oReserveWorksheetRequest);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oReserveWorksheetRequest"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/RejectReserveWorksheetFromApprovalScreen", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ReserveWorksheetResponse RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest oReserveWorksheetRequest);

    }
}
