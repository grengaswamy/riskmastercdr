﻿#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Security;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PageCacheService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class PageCacheService : RMService, IPageCacheService
    {
        public string GetConnectionstringFromDSNId(int DataSourceId)
        {
            string sConnectionString = string.Empty;
            RiskmasterDatabase oRMDatabase = new RiskmasterDatabase( DataSourceId );
            sConnectionString = oRMDatabase.ConnectionString;

            return sConnectionString;
        }

        public string GetTableTimestamps(string connectionString, string sTableListConfig)
        {
            string sTableTimestampList = string.Empty;
            XDocument oTableListConfig = XDocument.Parse(sTableListConfig);
            foreach (XElement oPage in oTableListConfig.XPathSelectElements("//pages/page"))
            {
                string sTable = oPage.Attribute("table").Value;
                string sColumn = oPage.Attribute("timecolumn").Value;
                string sSQL = string.Format("SELECT MAX({0}) FROM {1}", sColumn, sTable);
                string sTime = DbFactory.ExecuteScalar(connectionString, sSQL).ToString();
                oPage.Attribute("timestamp").Value = sTime;
            }

            return oTableListConfig.ToString();
        }

        public string GetTimestampForOrgHierarchy(string connectionString)
        {
            string sTime = DbFactory.ExecuteScalar(connectionString, "SELECT MAX(DTTM_RCD_LAST_UPD) FROM ENTITY WHERE ENTITY_TABLE_ID >=1005 AND ENTITY_TABLE_ID <= 1012").ToString();

            return sTime;
        }

        public Dictionary<string, string> GetUserLoginInfo(string sUser, string sDsnName,int iClientId)
        {
            Dictionary<string, string> dictUserProp = new Dictionary<string, string>();
            UserLogin objuser = new UserLogin(sUser, sDsnName, iClientId);
            dictUserProp.Add("iBesGroupId",objuser.BESGroupId.ToString());
            dictUserProp.Add("connectionString", objuser.objRiskmasterDatabase.ConnectionString);
            dictUserProp.Add("iDsnId", objuser.DatabaseId.ToString());
            dictUserProp.Add("iOrgSecFlag", objuser.objRiskmasterDatabase.OrgSecFlag.ToString());

            return dictUserProp;
        }
    }
}
