using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Services.Protocols;
using System.Web.Services.Description;
using System.Web.Services.Configuration;
using System.Xml.Serialization;
using System.Resources;

namespace Riskmaster.WebService.Common
{
	public class FileProcessor :  SoapExtension
	{
		// JP If we need HttpContext  -->>>    HttpContext httpctx = HttpContext.Current;

		protected ArrayList m_ExtractedFiles = null;
		protected ArrayList m_InsertedFiles = null;

		// this GetInitializer is called when the SoapExtension is registered
		// in the config file (web/machine.config), it's called the first time
		// the class is invoked
		public override object GetInitializer(System.Type serviceType)
		{
			return null;
		}

		// this GetInitializer is called when the SoapExtension is configured
		// via a custom SoapExtensionAttribute, it's called the first time the
		// WebMethod is invoked
		public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
		{
			return null;
		}

		// called each a given [WebMethod] is used (will recieve a ValidationContext object)
		public override void Initialize(object initializer)
		{
		}

		protected Stream stmIncoming = null;
		protected Stream stmOutgoing = null;
		bool bPostDeserialize = false;

		public override Stream ChainStream(Stream stream)
		{
			if (!bPostDeserialize) 
			{
				//Accepting Request
				//1.) new MemoryStream is destination of our bits.
				//2.) stream will be where bits arrive for us to use.
			
				stmIncoming = stream;
				stmOutgoing = new MemoryStream();

				bPostDeserialize = true;

				return stmOutgoing;
			}
			else 
			{
				//Sending Response 
				//1.) stream is destination of our bits.
				//2.) new MemoryStream will be where bits arrive for us to use.
			
				stmIncoming = new MemoryStream();
				stmOutgoing = stream;
				bPostDeserialize=false;
				return stmIncoming;
			}
		}
		private void DumpStream(Stream stm)
		{
			long posOrig=stm.Position;
			stm.Position = 0L;
			FileStream fs = new FileStream(System.IO.Path.GetTempPath() + "\\debug.xml",FileMode.Create);
			Riskmaster.Common.Utilities.CopyStream(stm,fs);
			fs.Close();
			stm.Position=posOrig;
		}
		// this will be called four times (before/after serialize/deserialize) for each [WebMethod] 
		public override void ProcessMessage(System.Web.Services.Protocols.SoapMessage message)
		{
			if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
			{
				try
				{
					MemoryStream stmOutput =null;
					try{stmOutput = this.Base64StreamManualScan(stmIncoming);}
					catch (Exception exp) 
					{
						SoapException error = new SoapException(
							"SoapExtension failure: " + exp.Message,
							SoapException.ClientFaultCode, 
							HttpContext.Current.Request.Url.AbsoluteUri,
							exp);
						throw error;
					}
					stmOutput.Flush();

					// Copy either modified (extracted) stream in or original stream if nothing happened (i.e. no image embedded)
					if (m_ExtractedFiles.Count > 0)
					{  // file was extracted
						stmOutput.WriteTo(stmOutgoing);
						stmOutput.Position = 0L;
					}
					else
					{   // no file extracted - play back original stream
						stmIncoming.Position = 0L;
						Riskmaster.Common.Utilities.CopyStream(stmIncoming, stmOutgoing);
					}
				}
				finally
				{
					stmOutgoing.Position=0L;
				}
			}
			else if(message.Stage == System.Web.Services.Protocols.SoapMessageStage.AfterSerialize)
			{
				//Returning Result to Client...
				try
				{
					// 1.)Expand anyURI content into a stream from disk.
					MemoryStream stmOutput =null;
					try{stmOutput = this.Base64StreamManualScan(stmIncoming);}
					catch (Exception exp) 
					{
						SoapException error = new SoapException(
							"SoapExtension failure: " + exp.Message,
							SoapException.ClientFaultCode, 
							HttpContext.Current.Request.Url.AbsoluteUri,
							exp);
						throw error;
					}
					stmOutput.Flush();

					// 2.) Decide whether to use the newly generated result stream
					// Copy either modified (inserted) stream in or original stream 
					// if nothing happened (i.e. no image embedded)
					if (m_InsertedFiles.Count > 0)
					{  // file was extracted
						stmOutput.WriteTo(stmOutgoing);
						stmOutput.Position = 0L;
					}
					else
					{   // no file extracted - play back original stream
						stmIncoming.Position = 0L;
						Riskmaster.Common.Utilities.CopyStream(stmIncoming, stmOutgoing);
					}

					// 3.) Remove temporary disk cache files. 
					//(From Request)
					if(this.m_ExtractedFiles!=null && this.m_ExtractedFiles.Count>0)
					{
						foreach(string sTempFilePath in this.m_ExtractedFiles)
							try{System.IO.File.Delete(sTempFilePath);}
							catch{;}
					}
					//(From Response)
					if(this.m_InsertedFiles!=null && this.m_InsertedFiles.Count>0)
					{
						foreach(string sTempFilePath in this.m_InsertedFiles)
							try{System.IO.File.Delete(sTempFilePath);}
							catch{;}
					}
				}
				finally
				{
					//stmOutgoing.Position=0L;
				}
			}
		}

		public MemoryStream Base64StreamManualScan(Stream stmInput)
		{
			if(m_ExtractedFiles==null)
				m_ExtractedFiles = new ArrayList();
			
			if(m_InsertedFiles==null)
				m_InsertedFiles = new ArrayList();
			
			string sMode = bPostDeserialize ? "EXTRACT":"INSERT";
			string sDiskUri ="";
			
			//Hook Up Input\Output Streams...
			StreamReader stmReader = new StreamReader(stmInput);
			MemoryStream stmResult = new MemoryStream();
			StreamWriter stmXmlWriter = new System.IO.StreamWriter(stmResult);
			FileStream stmTempFile = null;

			//Search for "xs:anyURI" in the stream.
			char[] buffSearchWindow;
			char[] buffBase64;
			char curChar;
			char prevChar='0';
			bool bIsInBase64=false;
			bool bBeginningBase64 = false;
			long iCurPos = 0L;
			long iPosBase64Start = 0L;
			long iPosBase64End=0L;
			
			//Make Sure We Start at the beginning...
			stmInput.Position=0L;
			
			while(stmReader.Peek()>=0)
			{
				iCurPos++;
				curChar=(char)stmReader.Read();
				
				if(curChar=='x' && !bIsInBase64)//Possible Start of "xs:anyURI" search string.
				{
					//Check for xs:anyURI
					buffSearchWindow = new char[9];
					buffSearchWindow[0]='x';

					if(8==stmReader.ReadBlock(buffSearchWindow,1,8))
						if(new String(buffSearchWindow)=="xs:anyURI")
							bBeginningBase64=true;

					stmReader.BaseStream.Position=iCurPos;
					stmReader.DiscardBufferedData();
				}
				else if(bBeginningBase64 && curChar=='>')
				{
					stmXmlWriter.Write(curChar);
					
					if(sMode=="EXTRACT")
					{
						sDiskUri = System.IO.Path.GetTempFileName();
					}
					iPosBase64Start = iCurPos;
					bIsInBase64=true;
					bBeginningBase64=false;
				}
				else if(bIsInBase64 && curChar=='<')
				{
					if('/'!=(char)stmReader.Peek() || (prevChar!='='&& sMode=="EXTRACT")) //No Content Found...
					{
						bIsInBase64=false;
						bBeginningBase64=false;
						sDiskUri="";
						break;
					}

					iPosBase64End = iCurPos-1;
					stmReader.BaseStream.Position=iPosBase64Start;
					stmReader.DiscardBufferedData();

					buffBase64 = new char[iPosBase64End-iPosBase64Start];
					stmReader.Read(buffBase64,0,buffBase64.Length);
					stmReader.BaseStream.Position = iCurPos;
					stmReader.DiscardBufferedData();

					if(sMode=="EXTRACT")
					{
						stmXmlWriter.Write(sDiskUri);
						byte[] decodeData=Convert.FromBase64CharArray(buffBase64,0,buffBase64.Length);
					
						stmTempFile = new System.IO.FileStream(sDiskUri, System.IO.FileMode.Create);
						stmTempFile.Write(decodeData,0,decodeData.Length);
						stmTempFile.Close();
						m_ExtractedFiles.Add(sDiskUri);
					}
					else //"INSERT"
					{
//						DumpStream(stmReader.BaseStream);
						//Get Bytes to Encode
						sDiskUri = new String(buffBase64);
						if(System.IO.File.Exists(sDiskUri))
						{
							stmTempFile = new System.IO.FileStream(sDiskUri, System.IO.FileMode.Open);
							byte[] decodeData = new byte[stmTempFile.Length];
							stmTempFile.Read(decodeData,0,decodeData.Length);
							stmTempFile.Close();

							//Encode and Write Bytes
							stmXmlWriter.Write(Convert.ToBase64String(decodeData));
							m_InsertedFiles.Add(sDiskUri);
						}
					}

					sDiskUri="";
					bIsInBase64=false;
				}
				
				if(!bIsInBase64)
					stmXmlWriter.Write(curChar);
				
				prevChar=curChar;
			}//End While Scanning

			stmXmlWriter.Flush();
			return stmResult;
		}
	}

}
