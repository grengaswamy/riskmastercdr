using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Collections.Specialized;

namespace Riskmaster.WebService.Common
{

	/// <summary>
	/// Summary description for Service1.
	/// </summary>
	[WebService(Namespace="http://csc.com/Riskmaster/Webservice/Common")]
	public class CommonWebService : System.Web.Services.WebService
	{
		public CommonWebService()
		{
			m_oSession = new SessionManager(m_SessionDSN);
		}

		public const string XML_NAMESPACE = "http://csc.com/Riskmaster/Webservice/Common";
		private SessionManager m_oSession = null;
		private static string m_SessionDSN = null;
		private class AdaptorCacheRecord
		{
			public string Name;			// Name key. This is what the common envelope uses to reference the function.
			public string Assembly;		// Assembly the class lives in. Filename of DLL.
			public string Class;			// Class method lives in.
			public string Method;			// Method to call in class.
			public bool bypassSecurity;	// If true, bypass AuthorizationToken check.
		
		}

		private static Hashtable m_AdaptorCache = new Hashtable();



		static CommonWebService()
		{
            XmlNode xmlAdaptors = RMConfigurator.NamedNode("CommonWebServiceHandler/Adaptors");

			XmlNode xmlSessionDSN=RMConfigurator.NamedNode("CommonWebServiceHandler/SessionDataSource");
            NameValueCollection nvColl = RMConfigurator.NameValueCollection("DbContextSettings");

            
            XmlNode xmlExtensibilityAdaptors =null;
            try
            {
                xmlExtensibilityAdaptors = RMConfigurator.NamedNode("Extensibility/Adaptors");
            }//try
            catch (Exception) 
            { 
                ; 
            }

			if (xmlAdaptors == null || xmlSessionDSN==null)
			{
				// TO DO: Throw an exception. This is fatal.
			}

			m_SessionDSN = xmlSessionDSN.InnerText;

			// Cache the <adaptor> tags into a static hashtable
			AdaptorCacheRecord adaptor;
			foreach (XmlNode xmlAdaptor in xmlAdaptors.SelectNodes("./Adaptor"))
			{
				adaptor = new AdaptorCacheRecord();
				adaptor.Name = xmlAdaptor.Attributes["name"].Value;
				adaptor.Assembly = xmlAdaptor.Attributes["assembly"].Value;
				adaptor.Class = xmlAdaptor.Attributes["class"].Value;
				adaptor.Method = xmlAdaptor.Attributes["method"].Value;
				if (xmlAdaptor.Attributes["bypasssecurity"] != null)
					adaptor.bypassSecurity = (xmlAdaptor.Attributes["bypasssecurity"].Value == "true");
				else
					adaptor.bypassSecurity = false;   // default is to never bypass security



				m_AdaptorCache.Add(adaptor.Name, adaptor);
			}
            // BSB 02.08.2007 Add support for custom Business Adaptors
            // Process Adaptor entries from Extensibility\Adaptors
            if (xmlExtensibilityAdaptors != null)
                foreach (XmlNode xmlAdaptor in xmlExtensibilityAdaptors.SelectNodes("./Adaptor"))
                {
                    adaptor = new AdaptorCacheRecord();
                    adaptor.Name = xmlAdaptor.Attributes["name"].Value;
                    adaptor.Assembly = xmlAdaptor.Attributes["assembly"].Value;
                    adaptor.Class = xmlAdaptor.Attributes["class"].Value;
                    adaptor.Method = xmlAdaptor.Attributes["method"].Value;
                    if (xmlAdaptor.Attributes["bypasssecurity"] != null)
                        adaptor.bypassSecurity = (xmlAdaptor.Attributes["bypasssecurity"].Value == "true");
                    else
                        adaptor.bypassSecurity = false;   // default is to never bypass security


                    
                    //Allow for "replacement\override" of system Adaptor entries.
                    if (m_AdaptorCache.Contains(adaptor.Name))
                        m_AdaptorCache.Remove(adaptor.Name);
                    
                    m_AdaptorCache.Add(adaptor.Name, adaptor);
                }


			// Cache the TrustedIPPool into a static hashtable
			
		
		}

		private XmlDocument formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors)
		{
			// Create output envelope doc and root node (ResultMessage)
			XmlDocument xmlOutEnv = new XmlDocument();
			// JP TMP 01.18.2006   XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage", XML_NAMESPACE);
			XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
			xmlOutEnv.AppendChild(xmlRoot);

			// Add errors
			formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors);

			// Add output document
			// JP TMP 01.18.2006   XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document", XML_NAMESPACE);
			XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
			xmlRoot.AppendChild(xmlDocEnv);
			
			if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
				xmlDocEnv.InnerXml =xmlOut.OuterXml;

			// Return output envelope
			return xmlOutEnv;
		}

		private void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors)
		{
			XmlElement xmlElement = null;
			XmlElement xmlMsgsRoot = null;

			// Create error root element (MsgStatus)
			// JP TMP 01.18.2006   XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus", XML_NAMESPACE);
			XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
			xmlRoot.AppendChild(xmlErrRoot);

			// Add overall result status
			// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("MsgStatusCd", XML_NAMESPACE);
			xmlElement = xmlDoc.CreateElement("MsgStatusCd");
			if (bCallResult)
				xmlElement.InnerText = "Success";
			else
				xmlElement.InnerText = "Error";

			xmlErrRoot.AppendChild(xmlElement);

			if (errors != null)
			{
				foreach (BusinessAdaptorError err in errors)
				{
					// ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
					// JP TMP 01.18.2006   xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus", XML_NAMESPACE);
					xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
					xmlErrRoot.AppendChild(xmlMsgsRoot);

					if (err.oException == null)
					{  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
						// ... add error code/number
						// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
						xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
						xmlElement.InnerText = err.ErrorCode;
						xmlMsgsRoot.AppendChild(xmlElement);

						// ... add error description
						// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
						xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
						xmlElement.InnerText = err.ErrorDescription;
						xmlMsgsRoot.AppendChild(xmlElement);
					}  // non-Exception case
					else
					{  // Exception case
						// Determine error code - assembly + exception type
						// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
						xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
						xmlElement.InnerText = err.oException.Source + "." + err.oException.GetType().Name;
						xmlMsgsRoot.AppendChild(xmlElement);

						// ... add error description
						// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
						xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
						if (err.ErrorDescription != "")
							xmlElement.InnerText = err.ErrorDescription;
						else
							xmlElement.InnerText = err.oException.Message;

						xmlMsgsRoot.AppendChild(xmlElement);
					}  // Exception case

					// ...add error type
					// JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedMsgType", XML_NAMESPACE);
					xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
					switch(err.ErrorType)
					{
						case BusinessAdaptorErrorType.SystemError:
							xmlElement.InnerText = "SystemError";
							break;
						case BusinessAdaptorErrorType.Error:
							xmlElement.InnerText = "Error";
							break;
						case BusinessAdaptorErrorType.Warning:
							xmlElement.InnerText = "Warning";
							break;
						case BusinessAdaptorErrorType.Message:
							xmlElement.InnerText = "Message";
							break;
						case BusinessAdaptorErrorType.PopupMessage:
							xmlElement.InnerText = "PopupMessage";
							break;
						default:
							// TODO   - What to do if not a standard error code?
							break;
					};
					xmlMsgsRoot.AppendChild(xmlElement);
				}
			}

		}

		private void logErrors(string sAdaptorMethod, XmlDocument xmlRequest, bool bCallResult, BusinessAdaptorErrors errors)
		{

			// First, log the error to the error log
			
			// Add overall result status
			string sCall = sAdaptorMethod;
			if (bCallResult)
				sCall += " (Success)";
			else
				sCall += " (Error)";

			// Log the overall call failure and the input envelope (only if call failed)
			if (bCallResult == false)
			{
				LogItem oLogItem = new LogItem();
				oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
				oLogItem.Category = "CommonWebServiceLog";
				oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
				oLogItem.Message = sAdaptorMethod + " call failed.";
				if (xmlRequest != null)
				try{
                    oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
				    }
                catch(Exception)
                {

                    oLogItem.RMParamList.Add("XML Input Envelope", "xmlRequest contents could not be logged - likely too large for available memory.");
				}
                Log.Write(oLogItem);
			}

			// Iterate through individual errors and log them. If exceptions are present, those will be walked (via InnerException) and all exceptions in the list will be logged.
			if (errors != null)
			{
				foreach (BusinessAdaptorError err in errors)
				{
					// ... build error type
					string sErrorType = null;
					switch(err.ErrorType)
					{
						case BusinessAdaptorErrorType.SystemError:
							sErrorType = "SystemError";
							break;
						case BusinessAdaptorErrorType.Error:
							sErrorType = "Error";
							break;
						case BusinessAdaptorErrorType.Warning:
							sErrorType = "Warning";
							break;
						case BusinessAdaptorErrorType.Message:
							sErrorType = "Message";
							break;
						case BusinessAdaptorErrorType.PopupMessage:
							sErrorType = "PopupMessage";
							break;
						default:
							sErrorType = "SystemError";
							break;
					};

					// ... add individual error messages/warnings
					if (err.oException == null)
					{  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
						// Init logfile item for this error
						LogItem oLogItem = new LogItem();
						oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
						oLogItem.Category = "CommonWebServiceLog";
						oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
						oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
						oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);
						oLogItem.RMParamList.Add("Call Error Type", sErrorType);

						// ... add error code/number
						oLogItem.RMParamList.Add("Error Code", err.ErrorCode);

						// ... add error description
						oLogItem.Message = err.ErrorDescription;

						// Flush entry to log file
						Log.Write(oLogItem);
					}  // non-Exception case
					else
					{  // Exception case
						Exception exc = err.oException;

						while (exc != null)
						{
							// Init logfile item for this error
							LogItem oLogItem = new LogItem();
							oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
							oLogItem.Category = "CommonWebServiceLog";
							oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
							oLogItem.RMParamList.Add("Call Error Type", sErrorType);
							oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
							oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);


							// ... determine error code - assembly + exception type
							oLogItem.RMParamList.Add("Error Code", err.oException.Source + "." + err.oException.GetType().Name);

							// ... add error description
							if (err.ErrorDescription != "")
								oLogItem.Message = err.ErrorDescription;
							else
								oLogItem.Message = err.oException.Message;

							// ... dump exception info to log
							oLogItem.RMParamList.Add("Exception Source", exc.Source);
							oLogItem.RMParamList.Add("Exception Type", exc.GetType().Name);
							oLogItem.RMParamList.Add("Exception Message", exc.Message);
							oLogItem.RMParamList.Add("Exception StackTrace", exc.StackTrace);

							// ... flush entry to log file
							Log.Write(oLogItem);

							// ... get next exception in chain (if there is one)
							exc = exc.InnerException;
						}
					}  // Exception case
				}
			}

		}
        /// <summary>
        /// Processes the input from the Presentation Layer and generates the appropriate output
        /// </summary>
        /// <param name="xmlRequest">XML Document containing the presentation layer request content</param>
        /// <returns>XMLDocument containing all of the appropriate logic to be used by the Presentation Layer</returns>
        [WebMethod()]
		public XmlDocument ProcessRequest(XmlDocument xmlRequest)
		{
			XmlDocument xmlOutEnvelope = null;       // Will point to outgoing message envelope
			XmlElement xmlIn = null;                 // Will point to incoming XML document (embedded in envelope)
			XmlDocument xmlOut = null;				 // Will point to outgoing XML document from business adaptor
			XmlElement xmlAuth = null;               // Will point to authorization token
			XmlElement xmlFunction = null;           // Will point to function in call
			XmlNamespaceManager nsNamespace = null;  // Used for xpath queries using common envelope namespace
			string functionName;                     // Holds name of business adaptor function to look up
			string sessionKeySupplied;
			UserLogin oUserLogin = null;             // User login retrieved from session and passed to business adaptor functions.
			BusinessAdaptorErrors systemErrors =
				new BusinessAdaptorErrors();         // Collection for code to use to store system errors that need to be thrown back.

			// Create a namespace manager so we can query XPath on the envelope
			nsNamespace = new XmlNamespaceManager(xmlRequest.NameTable);
			nsNamespace.AddNamespace("env", XML_NAMESPACE);

			// Crack the <call> element and lookup the assembly/function
			//  in the configuration file.
			//   If can't find the call, return a "Bad Call" .
			//    Be sure to document in Web Services Guidelines.doc.
			// JP TMP 01.18.2006    xmlFunction = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Call/env:Function", nsNamespace);
			xmlFunction = (XmlElement) xmlRequest.SelectSingleNode("/Message/Call/Function");
			if (xmlFunction == null)
			{
				// Throw back system error if part of envelope not there.
				systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
					Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope"),
					BusinessAdaptorErrorType.SystemError);
			
				logErrors(string.Empty, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}
			functionName = xmlFunction.InnerText;

			// Extract inbound XML message for business adaptor
			// JP 01.18.2006   xmlIn = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Document/*", nsNamespace);
			xmlIn = (XmlElement) xmlRequest.SelectSingleNode("/Message/Document/*");
			if (xmlIn == null)
			{
				// Throw back system error if part of envelope not there.
				systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
					Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope"),
					BusinessAdaptorErrorType.SystemError);
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Extract authorization key from inbound message
			// JP TMP 01.18.2006   xmlAuth = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Authorization", nsNamespace);
			xmlAuth = (XmlElement) xmlRequest.SelectSingleNode("/Message/Authorization");
			if (xmlAuth == null)
			{
				// Throw back system error if part of envelope not there.
				systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
					Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope"),
					BusinessAdaptorErrorType.SystemError);
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}


			// Lookup function in adaptor cache (cached at startup from config file)
			if (!m_AdaptorCache.Contains(functionName))
			{
				// Throw back system error if part of envelope not there.
				systemErrors.Add("Riskmaster.WebService.Common.AdaptorLookupFailed",
					Globalization.GetString("Riskmaster.WebService.Common.AdaptorLookupFailed"),
					BusinessAdaptorErrorType.SystemError);
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}
			AdaptorCacheRecord adaptor = (AdaptorCacheRecord) m_AdaptorCache[functionName];

			

			// Check to see if the assembly has the "bypass security" flag
			//  set. If so, it can be called without security session validation.
			if (!adaptor.bypassSecurity)
			{
	
				// Validate security session key and retrieve UserLogin object
				//  (unless "bypass security" flag set).
				sessionKeySupplied = xmlAuth.InnerText;
		
				//   If validation fails, return "Invalid Security Token" system error.
				if (!m_oSession.SessionExists(sessionKeySupplied)) // If user not in session'
				{
					// Throw back system error if authorization key is invalid.
					systemErrors.Add("Riskmaster.WebService.Common.CallerNotAuthorized",
						Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}
				else //Session Exists - unpack UserLogin Object
				{
					m_oSession.Init(sessionKeySupplied);

					if(!m_oSession.BinaryItemExists(AppConstants.SESSION_OBJ_USER))
					{
						// Throw back system error if authorization key is invalid.
						systemErrors.Add("Riskmaster.WebService.Common.CallerNotAuthorized",
							Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized"),
							BusinessAdaptorErrorType.SystemError);
				
						logErrors(functionName, xmlRequest, false, systemErrors);

						return formatOutputXML(null, false, systemErrors);					
					}

					System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
					System.IO.MemoryStream stm = new MemoryStream(m_oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER),false);
					try
					{
						oUserLogin = (UserLogin) formatter.Deserialize(stm);
						systemErrors = new BusinessAdaptorErrors(oUserLogin); //Apply Login Info for possible later uncaught execptions.
					}
					catch
					{
						oUserLogin = null;
						// Throw back system error if authorization key is invalid.
						systemErrors.Add("Riskmaster.WebService.Common.UserLoginDeserializationFailed",
							Globalization.GetString("Riskmaster.WebService.Common.UserLoginDeserializationFailed"),
							BusinessAdaptorErrorType.SystemError);
				
						logErrors(functionName, xmlRequest, false, systemErrors);

						return formatOutputXML(null, false, systemErrors);
					}

				}

			}

			BusinessAdaptorErrors errOut = null;
			// Obtain an instance of the business adaptor class.
			Object obj = null;
			try
			{
				// Load the assembly
				Assembly a = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + adaptor.Assembly);   // TO DO: Hardwired to go to bin subdirectory right now. Probably needs to change.
				if (a == null)
				{
					// Throw back system error if cannot load assembly.
					systemErrors.Add("Riskmaster.WebService.Common.ErrorLoadingAssembly",
						Globalization.GetString("Riskmaster.WebService.Common.ErrorLoadingAssembly"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}

				// Get the type of the class
				Type t = a.GetType(adaptor.Class);
				if (t == null)
				{
					// Throw back system error if cannot find class in assembly.
					systemErrors.Add("Riskmaster.WebService.Common.ClassNotFound",
						Globalization.GetString("Riskmaster.WebService.Common.ClassNotFound"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}

				// Create an instance of the class
				obj = Activator.CreateInstance(t);
				if (obj == null)
				{
					// Throw back system error if cannot create instance of class.
					systemErrors.Add("Riskmaster.WebService.Common.CreateInstanceFailed",
						Globalization.GetString("Riskmaster.WebService.Common.CreateInstanceFailed"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}
			}
			catch (Exception e)
			{
				// Throw back system error if exception is thrown during load/create class process.
				systemErrors.Add("Riskmaster.WebService.Common.ClassCreationException",
					Globalization.GetString("Riskmaster.WebService.Common.ClassCreationException"),
					BusinessAdaptorErrorType.SystemError);

				systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Call the IBusinessAdaptor interface to initialize security information
			try
			{
				IBusinessAdaptor pBusAdaptor = (IBusinessAdaptor) obj;
				if (pBusAdaptor == null)
				{
					// Throw back system error if can't get IBusinessAdaptor interface.
					systemErrors.Add("Riskmaster.WebService.Common.InterfaceFailed",
						Globalization.GetString("Riskmaster.WebService.Common.InterfaceFailed"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}


				if(oUserLogin !=null) //BSB Protect for "bypass security" case.
					pBusAdaptor.SetSecurityInfo(oUserLogin);

				// Pass session object to business adaptor   JP 6/15/2005
				if (m_oSession != null)
					pBusAdaptor.SetSessionObject(m_oSession);
            }
			catch (Exception e)
			{
				// Throw back system error if exception is thrown during call to SetSecurityInfo.
				systemErrors.Add("Riskmaster.WebService.Common.SetSecurityContextFailed",
					Globalization.GetString("Riskmaster.WebService.Common.SetSecurityContextFailed"),
					BusinessAdaptorErrorType.SystemError);

				systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Create a delegate and bind it to the method we want to call
			BusinessAdaptorMethod methodDelegate = null;
			try
			{
				methodDelegate = 
					(BusinessAdaptorMethod) BusinessAdaptorMethod.CreateDelegate(typeof(BusinessAdaptorMethod), obj, adaptor.Method, true);
				if (methodDelegate == null)
				{
					// Throw back system error if can't create a delegate instance.
					systemErrors.Add("Riskmaster.WebService.Common.CannotCreateDelegate",
						Globalization.GetString("Riskmaster.WebService.Common.CannotCreateDelegate"),
						BusinessAdaptorErrorType.SystemError);
				
					logErrors(functionName, xmlRequest, false, systemErrors);

					return formatOutputXML(null, false, systemErrors);
				}
			}
			catch (Exception e)
			{
				// Throw back system error if can't create a delegate instance.
				systemErrors.Add("Riskmaster.WebService.Common.CannotCreateDelegate",
					Globalization.GetString("Riskmaster.WebService.Common.CannotCreateDelegate"),
					BusinessAdaptorErrorType.SystemError);

				systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Call the business adaptor method
			bool bResult=false;
			try
			{
				errOut = new BusinessAdaptorErrors(oUserLogin); // 8.24.2005  JP   Some business adaptors weren't allocating this on their own. Put this in to avoid rework on all of those adaptors.
				xmlOut = new XmlDocument();           // 8.25.2005  JP   Many business adaptors apparently don't allocate their output DOMs - even though it was clearly documented that this was a requirement. So, do this to let them off easy.

				XmlDocument xmlTmp = new XmlDocument();
				xmlTmp.LoadXml(xmlIn.OuterXml);

				bResult = methodDelegate(xmlTmp, ref xmlOut, ref errOut);
			}
			catch (Exception e)
			{
				// Throw back error if business adaptor isn't trapping its own exceptions.
				systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Format the return message
			try
			{
				xmlOutEnvelope = formatOutputXML(xmlOut, bResult, errOut);
			}
			catch (Exception e)
			{
				// Throw back system error if there was a problem formatting the return envelope.
				systemErrors.Add("Riskmaster.WebService.Common.ErrorFormattingResultEnvelope",
					Globalization.GetString("Riskmaster.WebService.Common.ErrorFormattingResultEnvelope"),
					BusinessAdaptorErrorType.SystemError);

				systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.
				
				logErrors(functionName, xmlRequest, false, systemErrors);

				return formatOutputXML(null, false, systemErrors);
			}

			// Final log if all went well
			logErrors(functionName, xmlRequest, bResult, errOut);
			
			// Return results
			return xmlOutEnvelope;
		}

	}
}
