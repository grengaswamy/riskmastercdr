﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Web;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.ServiceModel;
namespace Riskmaster.Models
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(DAImportFile))]
    public class RMStreamedServiceType
    {
        public RMStreamedServiceType()
        {

        }

        [DataMember]
        public string Token
        {
            get;
            set;
        }
        [DataMember]
        public string Errors;
        [DataMember]
        public int ClientId
        {
            get;
            set;
        }
        [DataMember]
        public string BaseUrl
        {
            get;
            set;
        }

    }

    [MessageContract]
    public class RMStreamServiceType
    {
        public RMStreamServiceType()
        {

        }

        [MessageHeader]
        public string Token
        {
            get;
            set;
        }
        [MessageHeader]
        public string Errors;
        [MessageHeader]
        public int ClientId
        {
            get;
            set;
        }
    }

    //It will eventually become the base class for all Model Types that we return from WCF Service
    [Serializable]
    [DataContract]
    [KnownType(typeof(ProgressNotesType))]
    [KnownType(typeof(ClaimInfo))]
    [KnownType(typeof(ProgressNote))]
    [KnownType(typeof(SelectClaimObject))]
    [KnownType(typeof(GetNoteDetailsObject))]
    [KnownType(typeof(Filter))]
    [KnownType(typeof(DocumentType))]
    [KnownType(typeof(QuickLookupRequest))]
    [KnownType(typeof(CodeTypeRequest))]
    [KnownType(typeof(DocumentList))]
    [KnownType(typeof(DocumentListRequest))]
    [KnownType(typeof(FolderTypeRequest))]
    [KnownType(typeof(DocumentsDeleteRequest))]
    [KnownType(typeof(MoveDocumentsRequest))]
    [KnownType(typeof(DocumentUserRequest))]
    [KnownType(typeof(TransferDocumentsRequest))]
    [KnownType(typeof(EmailDocumentsRequest))]
    [KnownType(typeof(MDINavigationTreeNodeProfileRequest))]
    [KnownType(typeof(DiaryInput))]
    [KnownType(typeof(DiaryOutput))]
    [KnownType(typeof(ReserveWorksheetRequest))]
    [KnownType(typeof(ReserveWorksheetResponse))]
    [KnownType(typeof(XMLInput))]
    [KnownType(typeof(RMResource))]
    [KnownType(typeof(ProgressNoteTemplates))]
    [KnownType(typeof(ProgressNoteSettings))]
    [KnownType(typeof(ProgressNotesTypeChange))]
    [KnownType(typeof(ClaimantInfo))]
    [KnownType(typeof(SelectClaimantObject))]
    [KnownType(typeof(DataIntegratorModel))]
    [KnownType(typeof(JobFile))]
    [KnownType(typeof(JobFile))]
    public class RMServiceType
    {

        public RMServiceType()
        {

        }

        [DataMember]
        public string Token
        {
            get;
            set;
        }
        [DataMember]
        public string Errors;
        [DataMember]
        public int ClientId
        {
            get;
            set;
        }
        [DataMember]
        public string BaseUrl
        {
            get;
            set;
        }

        [DataMember]
        public string Ticket
        {
            get;
            set;
        }

    }
    //Use this return type when webservice doesn't return anything
    [DataContract]
    public class RMServiceResponse
    {
        [DataMember]
        public string Errors;
    }
   
}
