﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
     [DataContract]
    
    public class MDATopic : RMServiceType
    {
               
        [DataMember]
        public string Medicalcode { get; set; }
        [DataMember]
        public string Jobclass { get; set; }

         [DataMember]
        public string ReturnValue { get; set; }
    }
    
    
}
