﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
     [DataContract]
    public class Diary : RMServiceType
    {
         [DataMember]
         public string name { get; set; }
         [DataMember]
         public string type { get; set; }
         [DataMember]
         public string description { get; set; }
         [DataMember]
         public string dueDate { get; set; }
         [DataMember]
         public string dueTime { get; set; }
         [DataMember]
         public string recordId {get; set;}
         [DataMember]
         public string table {get ;set;}
         [DataMember]
         public string activity { get; set; }  
         
         [DataMember]
         public string completeDate { get; set; }
         [DataMember]
         public string completeTime { get; set; }  
         [DataMember]
         public string entryId { get; set; }  
         [DataMember]
         public string entryNotes { get; set; }  
         [DataMember]
         public string attachPrompt { get; set; }  
         [DataMember]
         public string workActivity { get; set; }  
         
     }
    //nsharma202 : RMACLOUD - 2960
     [DataContract]
     public class DiaryList : RMServiceType
     {
         public DiaryList()
        {
            this.Diaries = new List<Diary>();
        }
         [DataMember]
         public List<Diary> Diaries;
         [DataMember]
        public string lastSyncDateTime {get;set;}
     }
     [DataContract]
     //nsharma202 : RMACLOUD - 1638
     public class peekDiaryUsers : RMServiceType
     {
         [DataMember]
         public string userId { get; set; }
         [DataMember]
         public string userName { get; set; }
         [DataMember]
         public string LoginName { get; set; }
         
     }
}
