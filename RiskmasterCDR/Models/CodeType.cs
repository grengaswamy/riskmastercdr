﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Riskmaster.Models
{
    [DataContract]
    public class CodeListType : RMServiceType
    {
        private List<CodeType> objCodes = new List<CodeType>();
        [DataMember]
        public List<CodeType> Codes
        {
            get
            {
                return objCodes;
            }
            set
            {
                objCodes = value;
            }
        }
        [DataMember]
        public String SearchText
        {
            get;
            set; 
        }
        [DataMember]
        public String RecordCount
        {
            get;
            set;
        }
        [DataMember]
        public String PageCount
        {
            get;
            set;
        }
        [DataMember]
        public String TableName
        {
            get;
            set;
        }
        [DataMember]
        public String TriggerDate
        {
            get;
            set;
        }
        [DataMember]
        public String FormName
        {
            get;
            set;
        }
        [DataMember]
        public string Displayname
        {
            get;
            set;
        }
        [DataMember]
        public string PreviousPage
        {
            get;
            set;
        }
        [DataMember]
        public string FirstPage
        {
            get;
            set;
        }
        [DataMember]
        public string NextPage
        {
            get;
            set;
        }
        [DataMember]
        public string ThisPage
        {
            get;
            set;
        }
        [DataMember]
        public string LastPage
        {
            get;
            set;
        }
        //RMA-6871
        [DataMember]
        public bool IsEntityRoleEnabled
        {
            get;
            set;
        }
    }
    [DataContract]
    public class CodeType
    {
        [DataMember]
        public string ShortCode
        {
            get;
            set;
        }
        [DataMember]
        public string Desc
        {
            get;
            set;
        }
        [DataMember]
        public int Id
        {
            get;
            set;
        }
        [DataMember]
        public string ParentCode
        {
            get;
            set;
        }
        [DataMember]
        public string parentDesc
        {
            get;
            set;
        }
        [DataMember]
        public string parentText
        {
            get;
            set;
        }
        [DataMember]
        public string resstatus//skhare7 27920
        {
            get;
            set;
        }
        [DataMember]
        public string CovgSeqNum//rupal:policy system interface
        {
            get;
            set;
        }
        [DataMember]
        public string TransSeqNum//smishra54: MITS 33996
        {
            get;
            set;
        }
 //Isha Start 
         [DataMember]
        public string FieldName
        {
            get;
            set;
        }
         //Isha End
         [DataMember]
         //Ankit Start : Worked on MITS - 34297
         public string CoverageKey
         {
             get;
             set;
         }
        //Ankit End
         //avipinsrivas Start : Worked for Jira-340
         [DataMember]
         public int ErRowID
         {
             get;
             set;
         }
    }
    [DataContract]
    public class QuickLookupRequest : RMServiceType
    {
        [DataMember]
        public string RsvStatusParent
        {
            get;
            set;
        }

        [DataMember]
        public string Index
        {
            get;
            set;
        }
        [DataMember]
        public string LookupString
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
        //added by swati for AIC Gap 7 MITS  #36929
        [DataMember]
        public string Jurisdiction
        {
            get;
            set;
        }
        //change end here
        [DataMember]
        public string LookupType
        {
            get;
            set;
        }
        [DataMember]
        public string TypeLimits
        {
            get;
            set;
        }
        [DataMember]
        public string DescriptionSearch
        {
            get;
            set;
        }
        [DataMember]
        public string DeptEId
        {
            get;
            set;
        }

        [DataMember]
        public string ParentCodeID//added by stara Mits 16667
        {
            get;
            set;
        }        

        [DataMember]
        public string SessionLOB
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }
        [DataMember]
        public string TriggerDate
        {
            get;
            set;
        }
        [DataMember]
        public string EventDate
        {
            get;
            set;
        }
        [DataMember]
        public string Filter
        {
            get;
            set;
        }
        [DataMember]
        public string SessionClaimId
        {
            get;
            set;
        }
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        [DataMember]
        public string orgLevel
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredEid
        {
            get;
            set;
        }
        [DataMember]
        public string EventId
        {
            get;
            set;
        }
        [DataMember]
        public Int32 CurrentPageNumber
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyId
        {
            get;
            set;
        }
        [DataMember]
        public string CovTypeCodeId
        {
            get;
            set;
        }
        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        [DataMember]
        public string LossTypeCodeId
        {
            get;
            set;
        }
        //Ankit End
        [DataMember]
        public string TransId
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimantEid
        {
            get;
            set;
        }
        //start:rupal For First & Final Payment
        [DataMember]
        public string IsFFPmt
        {
            get;
            set;
        }
        [DataMember]
        public string PolUnitRowId
        {
            get;
            set;
        }
        //end:rupal For First & Final Payment
        //start:rupal For Point Policy system interface
        [DataMember]
        public string CovgSeqNum
        {
            get;
            set;
        }
        //rupal:end
        [DataMember]
        public string RcRowId
        {
            get;
            set;
        }

        [DataMember]
        public string PolicyLOB
        {
            get;
            set;
        }
        //smishra54: MITS 33996
        [DataMember]
        public string TransSeqNum
        {
            get;
            set;
        }
        //smishra54:end
 //Isha Start 
        [DataMember]
        public string FieldName
        {
            get;
            set;
        }
        //Isha End
        //Ankit Start : Worked on MITS - 34297
        [DataMember]
        public string CoverageKey
        {
            get;
            set;
        }
        //Ankit End
    }
    [DataContract]
    public class CodeTypeRequest : RMServiceType
    {
        [DataMember]
        public string DeptEId
        {
            get;
            set;
        }
        [DataMember]
        public string TableName
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }

        //added by swati for AIC GAp 7 MITS # 36929
        [DataMember]
        public string Jurisdiction
        {
            get;
            set;
        }
        //change end here

        [DataMember]
        public string ParentCodeID//added by stara Mits 16667
        {
            get;
            set;
        }

        [DataMember]
        public string TypeLimits
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }
        [DataMember]
        public string TriggerDate
        {
            get;
            set;
        }
        [DataMember]
        public string EventDate
        {
            get;
            set;
        }
        [DataMember]
        public string SessionLOB
        {
            get;
            set;
        }
        [DataMember]
        public string SessionClaimId
        {
            get;
            set;
        }
        [DataMember]
        public string RecordCount
        {
            get;
            set;
        }
        [DataMember]
        public string PageNumber
        {
            get;
            set;
        }
        [DataMember]
        public string Filter
        {
            get;
            set;
        }
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredEid
        {
            get;
            set;
        }
        [DataMember]
        public string EventId
        {
            get;
            set;
        } 
       
        //rsushilaggar MITS 21600 Date 09/28/2010
        [DataMember]
        public string ShowCheckBox
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyId
        {
            get;
            set;
        }
        [DataMember]
        public string CovTypeCodeId
        {
            get;
            set;
        }
        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        [DataMember]
        public string LossTypeCodeId
        {
            get;
            set;
        }
        //Ankit End
        [DataMember]
        public string TransId
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimantEid
        {
            get;
            set;
        }
        //start:rupal For First & Final Payment
        //IsFirstFinalPayment
        [DataMember]
        public string IsFFPmt
        {
            get;
            set;
        } 
        [DataMember]
        public string PolUnitRowId
        {
            get;
            set;
        }
        //end:rupal For First & Final Payment
        //start:rupal For Point Policy system interface
        [DataMember]
        public string CovgSeqNum
        {
            get;
            set;
        }
        //rupal:end
        [DataMember]
        public string RcRowId
        {
            get;
            set;
        }
		// ijha: Mobile Adjuster - entity table for codelist
        [DataMember]
		public bool bIsMobileAdjuster
        {
            get;
            set;
        }
        //mbahl3 for mobility code change
        [DataMember]
        public bool bIsMobilityAdjuster
        { get; set; }
        //mbahl3 for mobility code change
        [DataMember]
        public string PolicyLOB
        {
            get;
            set;
        }

        [DataMember]
        public string RsvStatusParent
        {
            get;
            set;
        }
        //smishra54: MITS 33996
        [DataMember]
        public string TransSeqNum
        {
            get;
            set;
        }
        //smishra54:end
//Isha Start 
        [DataMember]
        public string FieldName
        {
            get;
            set;
        }
        //Isha End
        //Ankit Start : Worked on MITS - 34297
        [DataMember]
        public string CoverageKey
        {
            get;
            set;
        }
        //Ankit End
        [DataMember]
        public string SortColumn // MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        {
            get;
            set;
        }
        [DataMember]
        public string SortOrder //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        {
            get;
            set;
        }
    }
}
