﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
     [DataContract]
    public class MobileAttachment : RMServiceType
    {
         [DataMember]
         public string number { get; set; }
         [DataMember]
         public string fileName { get; set; }
         [DataMember]
         public string title { get; set; }
         [DataMember]
         public string subject { get; set; }
         [DataMember]
         public string notes { get; set; }
         [DataMember]
         public string content {get; set;}
      }
}

