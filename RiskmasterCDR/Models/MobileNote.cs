﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
    [DataContract]
    public class MobileNote : RMServiceType
    {
        [DataMember]
        public string claimNumber {get; set;}
        [DataMember]
        public string activityDate { get; set; }
        [DataMember]
        public string noteText { get; set; }
        [DataMember]
        public string noteTypeCode { get; set; } 
    }
}
