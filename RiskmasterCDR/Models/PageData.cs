﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
    [DataContract]
    public class PageData:RMServiceType
    {
        /// <summary>
        /// DsnId
        /// </summary>
        [DataMember]
        public int DsnId { get; set; }
        /// <summary>
        /// ConnKey
        /// </summary>
        [DataMember]
        public string ConnKey { get; set; }
        /// <summary>
        /// TableListConfig
        /// </summary>
        [DataMember]
        public string TableListConfig { get; set; }
        /// <summary>
        /// User
        /// </summary>
        [DataMember]
        public string User { get; set; }
        /// <summary>
        /// DsnName
        /// </summary>
        [DataMember]
        public string DsnName { get; set; }
    }
}
