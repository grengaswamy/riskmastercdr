﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
    [DataContract]
    public class RMResource:RMServiceType
    {
        [DataMember]
        public string PageId
        {
            get;
            set;
        }
        [DataMember]
        public string ResourceId 
        {
            get;
            set;
        }
        [DataMember]
        public string ResourceType
        {
            get;
            set;
        }
        [DataMember]
        public string ResourceKey
        {
            get;
            set;
        }
        [DataMember]
        public string ResourceValue
        {
            get;
            set;
        }
        [DataMember]
        public string LanguageCode
        {
            get;
            set;
        }
        [DataMember]
        public string DataSourceId
        {
            get;
            set;
        }
        [DataMember]
        public string User
        {
            get;
            set;
        }
        [DataMember]
        public string IsFDMPage
        {
            get;
            set;
        }
        [DataMember]
        public string LastUpdated
        {
            get;
            set;
        }
        [DataMember]
        public string ConnStringKey { get; set; }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public List<string> MDIList { get; set; }
        [DataMember]
        public string MaxLastUpdated { get; set; }
        [DataMember]
        public string Document { get; set; }
        [DataMember]
        public string SearchType { get; set; }
        [DataMember]
        public string DisplayCat { get; set; }
        [DataMember]
        public int CatId { get; set; }
        [DataMember]
        public int TableId { get; set; }

    }
}
