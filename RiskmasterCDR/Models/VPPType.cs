﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.Models
{
    [DataContract]
    public class VPPFileRequest : RMServiceType
    {
        [DataMember]
        public int DataSourceId
        {
            get;
            set;
        }

        [DataMember]
        public int ViewId
        {
            get;
            set;
        }

        [DataMember]
        public string PageName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class VPPFileResponse : RMServiceType
    {
        [DataMember]
        public string FileContent
        {
            get;
            set;
        }
    }


    [DataContract]
    public class VPPTimeStampResponse : RMServiceType
    {
        [DataMember]
        public string TimeStamp
        {
            get;
            set;
        }
    }

    [DataContract]
    public class VPPModifiedPagesRequest : RMServiceType
    {
        [DataMember]
        public string LastUpdated
        {
            get;
            set;
        }
    }

    [DataContract]
    public class VPPModifiedPagesResponse : RMServiceType
    {
        [DataMember]
        public List<string> ModifiedPageList
        {
            get;
            set;
        }

        [DataMember]
        public string MaxLastUpdated
        {
            get;
            set;
        }
    }

    [DataContract]
    public class VPPGetDataSourceIDResponse : RMServiceType
    {
        [DataMember]
        public int DataSourceID
        {
            get;
            set;
        }
    }
}
