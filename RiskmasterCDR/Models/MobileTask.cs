﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
     [DataContract]
    public class MobileTask : RMServiceType
    {
         [DataMember]
         public string name { get; set; }
         [DataMember]
         public string type { get; set; }
         [DataMember]
         public string description { get; set; }
         [DataMember]
         public string dueDate { get; set; }
         [DataMember]
         public string dueTime { get; set; }
         [DataMember]
         public string recordId {get; set;}
         [DataMember]
         public string table {get ;set;}
         [DataMember]
         public string activity { get; set; }
         
         



    }
}
