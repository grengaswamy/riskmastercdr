﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riskmaster.Models
{
    [DataContract]
    public class MobileUser : RMServiceType
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string dsn { get; set; }

    }
}
