﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;


namespace Riskmaster.Models
{
    [DataContract]
    public class RMException
    {
        private string _errors;

        [DataMember]

        public string Errors
        {

            get { return _errors; }

            set { _errors = value; }

        }
    }
}
