﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
    /// <summary>
    /// Logging
    /// </summary>
    [DataContract]
    public class Logging : RMServiceType
    {
         [DataMember]
        public int EventId
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Message for event to be logged.
        /// </summary>
        public string Message
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Category of event to be logged.
        /// </summary>
        public string Category
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Priority of event to be logged.
        /// </summary>
        public int Priority
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Riskmaster Exception Message to be logged.
        /// </summary>
        public string RMExeptMessage
        {
            get; set; 
        }
         [DataMember]
        /// <summary>
        /// Riskmaster Exception Type to be logged.
        /// </summary>
        public string RMExceptType
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Riskmaster Exception Source to be logged.
        /// </summary>
        public string RMExceptSource
        {
             get; set; 
        }
         [DataMember]
        /// <summary>
        /// Riskmaster Exception Stack Trace to be logged.
        /// </summary>
        public string RMExceptStack
        {
             get; set; 
        }
    }
}
