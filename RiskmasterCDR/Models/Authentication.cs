﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
    /// <summary>
    /// asharma326 MITS 27586 handling Auth response for forgot password.
    /// </summary>
    [DataContract]
    public class ForgotPasswordGenericResponse : RMServiceType
    {
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Message { get; set; }

    }
}
