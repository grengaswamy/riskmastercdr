﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
    /// <summary>
    /// AuthData
    /// </summary>
    [DataContract]
    public class AuthData : RMServiceType
    {
        /// <summary>
        /// UserName
        /// </summary>
        [DataMember]
        public string UserName { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        [DataMember]
        public string Password { get; set; }
        /// <summary>
        /// New Password
        /// </summary>
        [DataMember]
        public string NewPassword { get; set; }
        /// <summary>
        /// DsnName
        /// </summary>
        [DataMember]
        public string DsnName { get; set; }
        /// <summary>
        /// DsnId
        /// </summary>
        [DataMember]
        public int DsnId { get; set; }
        /// <summary>
        /// PIType
        /// </summary>
        [DataMember]
        public string PIType { get; set; }
        /// <summary>
        /// LangCode
        /// </summary>
        [DataMember]
        public int LangCode { get; set; }
        [DataMember]
        public string ClaimNum { get; set; }
    }
    /// <summary>
    /// Dsn
    /// </summary>
    [DataContract]
    public class DsnData:RMServiceType
    {
        /// <summary>
        /// DsnName
        /// </summary>
        [DataMember]
        public string DsnName { get; set; }
        /// <summary>
        /// DsnId
        /// </summary>
        [DataMember]
        public string DsnId { get; set; }
    }
}
