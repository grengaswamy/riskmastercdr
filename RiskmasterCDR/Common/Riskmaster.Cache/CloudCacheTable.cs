﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
namespace Riskmaster.Cache
{
    internal class CloudCacheTable : ICache
    {
        private bool isDisposed = false;
        private static MemcachedClient mcCacheItems;

        static CloudCacheTable()
        {
            // Retrieve the MemCache Configuration setting and create MemcachedClientConfiguration object

            using (MemcachedClientConfiguration memCacheconfig = new MemcachedClientConfiguration())
            {
                string host = ConfigurationManager.AppSettings["CloudCacheHost"];
                int port = Convert.ToInt32(ConfigurationManager.AppSettings["CloudCachePort"]);
                memCacheconfig.AddServer(host, port);
                memCacheconfig.Protocol = MemcachedProtocol.Binary;

                mcCacheItems = new MemcachedClient(memCacheconfig);
            }
        }
        /// <summary>
        /// Implement the method of IDisposable interface to suppress the finalize
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Override the Dispsose method to dispose the Connection object
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    throw new NotImplementedException();
                } 
            } 
            isDisposed = true;
        }

        /// <summary>
        /// Checks if the Cache is created and if the Key ClientId combination is present in Cache, then removes it
        /// </summary>
        /// <param name="Key">Key of the Item to be deleted</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>If the delete is successful or not. If the cache is not yet created or Key is not present in Cache, then also it returns true</returns>
        public bool Remove(string Key, int p_iClientId)
        {
            try
            {
                return mcCacheItems.Remove(String.Format("{0}_{1}", Key, p_iClientId));
            }
            catch
            {
                return false;
            };
        }

        /// <summary>
        /// If the Cache is not yet created, it creates the cache and inserts the item to the cache
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being inserted.</typeparam>
        /// <param name="Key">Key of the Item to be added</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <param name="Value">The Value which needs to be added to Cache</param>
        /// <returns>If the Insert is successful or not</returns>
        public bool Add<T>(string Key, int p_iClientId, T Value)
        {
            try
            {
                mcCacheItems.Store(StoreMode.Add, String.Format("{0}_{1}", Key, p_iClientId), Value);
                return true;
            }
            catch
            {
                return false;
            };
        }

        /// <summary>
        /// Updates the given item to the corresponding key.
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being updated.</typeparam>
        /// <param name="Key">Key of the Item to be updated</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <param name="Value">The Value which needs to be updated to Cache</param>
        /// <returns>If the update is successful or not</returns>
        public bool Update<T>(string Key, int p_iClientId, T Value)
        {
            try
            {
                mcCacheItems.Store(StoreMode.Replace, String.Format("{0}_{1}", Key, p_iClientId), Value);
                return true;
            }
            catch
            {
                return false;
            };
        }

        /// <summary>
        /// Retrievs the value corresponding to the key in the expected datatype
        /// </summary>
        /// <typeparam name="T">Expected Data Type</typeparam>
        /// <param name="Key">Key of the Item to be updated</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>The Value corresponding to the key present in cache in expected datatype</returns>
        public T Get<T>(string Key, int p_iClientId)
        {
            try
            {
                return mcCacheItems.Get<T>(String.Format("{0}_{1}", Key, p_iClientId));
            }
            catch
            {
                return default(T);
            };
        }

        /// <summary>
        /// Checks if the Cache is created and if the Key Exists in the Cache
        /// </summary>
        /// <param name="Key">Key of the Item to be checked</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>If the Key exists in the cache. If the cache is not yet created or Key is not present in Cache, then also it returns false</returns>
        public bool ContainsKey(string Key, int p_iClientId)
        {
            try
            {
                return mcCacheItems.ContainsKey(String.Format("{0}_{1}", Key, p_iClientId));
            }
            catch
            {
                return false;
            };
        }
        /// <summary>
        /// RemoveKeys
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="p_iClientId"></param>
        /// <returns></returns>
        public bool RemoveKeys(string Key, int p_iClientId)
        {
            try
            {
                if (mcCacheItems != null)
                {
                    mcCacheItems.FlushAll();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}