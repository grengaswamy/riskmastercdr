﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Cache
{
    /// <summary>
    /// Interface to be used to expose the functionality of cache.
    /// </summary>
    internal interface ICache : IDisposable
    {
        /// <summary>
        /// Removes the Item from Cache
        /// </summary>
        bool Remove(string Key, int p_iClientId);
        /// <summary>
        /// RemoveKeys
        /// </summary>
        bool RemoveKeys(string Key, int p_iClientId);

        /// <summary>
        /// Adds the Item to cache
        /// </summary>
        bool Add <T>(string Key, int p_iClientId, T Value);

        /// <summary>
        /// Updates the Item present in the cache. If the Item is not present then it inserts the item as well.
        /// </summary>
        bool Update<T>(string Key, int p_iClientId, T Value);


        /// <summary>
        /// Retrives the Item from cache based on the key
        /// </summary>
        T Get <T>(string Key, int p_iClientId);


        /// <summary>
        /// Checks for the existence of a key in Cache
        /// </summary>
        bool ContainsKey(string Key, int p_iClientId);
    }
    
}
