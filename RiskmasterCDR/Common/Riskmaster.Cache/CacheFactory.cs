﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Riskmaster.Common;
using System.Diagnostics;

namespace Riskmaster.Cache
{
    /// <summary>
    /// Factory responsible for creation of appropriate Cache Type. The Possible values are Loca (Hashtable in memory) or cloud (Elastic Cache)
    /// </summary>
    internal class CacheFactory
    {
        /// <summary>
        /// This function is responsible for instantiating proper Class. If the CacheType in config is Cloud then it instantiates the Class for Cloud
        /// else it instantiates the class for Local i.e HashTable
        /// </summary>
        /// <param name="CacheType">Enum Instance for Cloud or Local</param>
        /// <returns>An object of either LocalCacheTable or CloudCacheTable based on parameter passed</returns>
        public static ICache CreateCacheFactory(CacheTypeEnumeration.eCacheType CacheType)
        {
            ICache objCache = null;

            switch (CacheType)
            {
                case CacheTypeEnumeration.eCacheType.Local:
                    objCache = new LocalCacheTable();
                    break;
                case CacheTypeEnumeration.eCacheType.Cloud:
                    objCache = new CloudCacheTable();
                    break;
                default:
                    break;
            }
            return objCache;
        }

    }

    /// <summary>
    /// Class to store Enumeration for Cache Type.
    /// </summary>
    internal class CacheTypeEnumeration
    {
        /// <summary>
        /// Enumeration for Cache Type. It will be used to Instantiate appropriate Class.
        /// </summary>
        public enum eCacheType
        {
            Local = 1,
            Cloud = 2,
        }
    }

    /// <summary>
    /// This class acts as the wrapper on existing interface functions. We do not want to check if the Cache enumeration is cloud or Local wherever we are
    /// using Cache. So we wrap those functions in one class and will use this class to add/update/delete functions in cache.
    /// </summary>
    public class CacheCommonFunctions
    {

        /// <summary>
        /// This function retrieves the CacheType value from config and then instantiates appropriate class based on value from config.
        /// If this key is not available in config then it will instantiate the Local Cache Type
        /// </summary>
        internal static CacheTypeEnumeration.eCacheType CacheType()
        {
            string sCacheLocation = string.Empty;
            CacheTypeEnumeration.eCacheType enumCacheType;
            sCacheLocation = "Local";
            if (ConfigurationManager.AppSettings.AllKeys.Contains("CacheMode"))
            {
                sCacheLocation = ConfigurationManager.AppSettings["CacheMode"];
            }

            enumCacheType = (CacheTypeEnumeration.eCacheType)Enum.Parse(typeof(CacheTypeEnumeration.eCacheType), sCacheLocation);
            return enumCacheType;
        }

        //mbahl3 scripts work jira RMA-8486
        /// <summary>
        // This function verifies whether assembly name (passed as argument) was called or not, after ProcessRequest() funtion of RiskmasterService assembly
        // For example, When argument passed is "Scripting", then function verifies whether Custom Script is calling assembly or not.
        /// <summary>
        public static bool CheckCallingStack(string Key)
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame[] frames = stackTrace.GetFrames();
            string sServiceAssembly = "RiskmasterService";

            var script = frames.FirstOrDefault(s => s.GetMethod().DeclaringType.Assembly.ToString().Contains(Key) ||s.GetMethod().DeclaringType.Assembly.ToString().Contains(sServiceAssembly));
            return script.GetMethod().DeclaringType.Assembly.ToString().Contains(Key);
            //mbahl3 used linq instead of foreach

            //foreach (var stackFrame in frames)
            //{
            //    string ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly.ToString();
            //    if (ownerAssembly.Contains(Key))
            //        return true;
            //    else if (ownerAssembly.Contains(sServiceAssembly))
            //        return false;
            //}
            //return false;
        }

        //mbahl3 scripts work jira RMA-8486

        /// <summary>
        /// This function first checks the type of Cache (Cloud or Local) and returns the value of key specified
        /// </summary>
        /// <typeparam name="T">The Expected DataType of the Value.</typeparam>
        /// <param name="Key">Key of the Item to be retrieved</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud. If the Application is not running on Cloud then we need to pass 0</param>
        /// <returns>The Value from Cache</returns>
        public static T RetreiveValueFromCache<T>(string Key, int p_iClientId)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                return objCache.Get<T>(Key, p_iClientId);
            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.RetreiveValueFromCache", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }

        /// <summary>
        /// This function first checks the type of Cache (Cloud or Local) and adds the value to proper cache (Hashtable or Elastic Cache)
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being inserted.</typeparam>
        /// <param name="Key">Key of the Item to be added</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud. If the Application is not running on Cloud then we need to pass 0. 
        /// It will be concated with the Key to create a unique Key</param>
        /// <param name="Value">The Value which needs to be added to Cache</param>
        /// <returns>If the Insert is successful or not</returns>
        public static bool AddValue2Cache<T>(string Key, int p_iClientId, T Value)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                return objCache.Add<T>(Key, p_iClientId, Value);

            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.InsertValue2Cache", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }

        /// <summary>
        /// This function first checks the type of Cache (Cloud or Local) and Updates the value to proper cache (Hashtable or Elastic Cache)
        /// If the Key is not present in the cache it adds it to the cache as well.
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being updated/inserted.</typeparam>
        /// <param name="Key">Key of the Item to be updated/added</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud. If the Application is not running on Cloud then we need to pass 0. 
        /// It will be concated with the Key to create a unique Key</param>
        /// <param name="Value">The Value which needs to be updated/added to Cache</param>
        /// <returns>If the update/Insert is successful or not</returns>
        public static bool UpdateValue2Cache<T>(string Key, int p_iClientId, T Value)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                if (CheckIfKeyExists(Key, p_iClientId))
                {
                    return objCache.Update<T>(Key, p_iClientId, Value);
                }
                else
                    return AddValue2Cache<T>(Key, p_iClientId, Value);
            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.InsertValue2Cache", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }

        /// <summary>
        /// Removes the Key value pair from Cache
        /// </summary>
        /// <param name="Key">Key of the Item to be deleted</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud. If the Application is not running on Cloud then we need to pass 0. 
        /// It will be concated with the Key to create a unique Key</param>
        /// <returns>If the delete is successful or not</returns>
        public static bool RemoveValueFromCache(string Key, int p_iClientId)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                return objCache.Remove(Key, p_iClientId);
            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.RemoveValueFromCache", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }

        //mbahl3 jira RMA-8486
       /// <summary>
        /// Scripting assembly overload for CheckIfKeyExists
        /// </summary>     
        public static bool CheckIfKeyExists(string Key)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return CheckIfKeyExists(Key, 0);
            }
            else
                throw new Exception("CheckIfKeyExists" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
       

        /// <summary>
        /// Checks if the Key Exists in the Cache
        /// </summary>
        /// <param name="Key">Key of the Item to be checked</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud. If the Application is not running on Cloud then we need to pass 0. 
        /// It will be concated with the Key to create a unique Key</param>
        /// <returns>If the Key exists in the cache</returns>
        public static bool CheckIfKeyExists(string Key, int p_iClientId)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                return objCache.ContainsKey(Key, p_iClientId);
            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.CheckIfKeyExists", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }
        /// <summary>
        /// Clearcache
        /// </summary>
        /// <param name="sEndsWithhKey"></param>
        /// <param name="p_iClientId"></param>
        /// <returns></returns>
        public static bool ClearCache(string sEndsWithhKey, int p_iClientId)
        {
            ICache objCache = null;
            try
            {
                objCache = CacheFactory.CreateCacheFactory(CacheType());
                return objCache.RemoveKeys(sEndsWithhKey, p_iClientId);
            }
            catch (Exception p_objEx)
            {
                throw new Exception(Globalization.GetString("CacheCommonFunctions.RemoveValueFromCache", p_iClientId), p_objEx);
            }
            finally
            {
                objCache = null;
            }
        }
    }
}