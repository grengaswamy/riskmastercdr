﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Riskmaster.Db;
using Riskmaster.Common;

namespace Riskmaster.Cache
{
    /// <summary>
    /// Manages retrieval of all configuration information 
    /// for the entire Service Layer
    /// </summary>
    public class ConfigurationInfo
    {
        private static string sBaseConnString = string.Empty;
        /// <summary>
        /// Gets the specified connection string from the Web.config file
        /// </summary>
        /// <param name="strConnString">name of the connection string to retrieve
        /// from the Web.config file</param>
        /// <returns>specified connection string from the </returns>


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetConnectionString
        /// </summary>     
        public static string GetConnectionString(string strConnStringKey)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetConnectionString(strConnStringKey, 0);
            }
            else
                throw new Exception("GetConnectionString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        
        
        public static string GetConnectionString(string strConnStringKey, int iClientId)
        {
            if (iClientId == 0) return ConfigurationManager.ConnectionStrings[strConnStringKey].ConnectionString;
            sBaseConnString = ConfigurationManager.ConnectionStrings["rmATenantSecurity"].ConnectionString;
            string sConnString = string.Empty;
            Dictionary<string, int> objParams = null;
            try
            {
                if (!CacheCommonFunctions.CheckIfKeyExists(strConnStringKey, iClientId))
                {
                    objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", strConnStringKey, "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    sConnString = Convert.ToString(objClientConnString);
                    CacheCommonFunctions.AddValue2Cache<string>(strConnStringKey, iClientId, sConnString);
                }
                else
                {
                    sConnString = CacheCommonFunctions.RetreiveValueFromCache<string>(strConnStringKey, iClientId);
                }
                return sConnString;
            }
            catch (Exception ex)
            {
                return sConnString;
            }
            finally
            {
                objParams = null;
            }

        } // method: GetConnectionString



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSecurityConnectionString
        /// </summary>     
        public static string GetSecurityConnectionString()
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSecurityConnectionString(0);
            }
            else
                throw new Exception("GetSecurityConnectionString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        
        /// <summary>
        /// Gets the RISKMASTER Security database connection string
        /// </summary>
        /// <returns>RISKMASTER Security database connection string</returns>
        public static string GetSecurityConnectionString(int iClientId)
        {
            return GetConnectionString("RMXSecurity", iClientId);
        } // method: GetSecurityConnectionString

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSessionConnectionString
        /// </summary>     
        public static string GetSessionConnectionString()
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSessionConnectionString(0);
            }
            else
                throw new Exception("GetSessionConnectionString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets the RISKMASTER Session database connection string
        /// </summary>
        /// <returns>RISKMASTER Session database connection string</returns>
        public static string GetSessionConnectionString(int iClientId)
        {
            return GetConnectionString("SessionDataSource", iClientId);
        } // method: GetSessionConnectionString

        /// <summary>
        /// Retrive the View Connection string for the corresponding Client Id
        /// </summary>
        /// <param name="iClientId">Client Id whose View Connection string needs to be retrieved </param>
        /// <returns>View Connection String</returns>
        public static string GetViewConnectionString(int iClientId)
        {
            return GetConnectionString("ViewDataSource", iClientId);
        } // method: GetViewConnectionString

        /// <summary>
        /// Retrive the Task Manager Connection string for the corresponding Client Id
        /// </summary>
        /// <param name="iClientId">Client Id whose Task Manager Connection string needs to be retrieved </param>
        /// <returns>Task Manager Connection String</returns>
        public static string GetTaskManagerConnectionString(int iClientId)
        {
            return GetConnectionString("TaskManagerDataSource", iClientId);
        } // method: GetTaskManagerConnectionString

        /// <summary>
        /// Retrive the History Tracking Connection string for the corresponding Client Id
        /// </summary>
        /// <param name="iClientId">Client Id whose History Tracking Connection string needs to be retrieved </param>
        /// <returns>History Tracking Connection String</returns>
        public static string GetHistoryTrackingConnectionString(int iClientId)
        {
            return GetConnectionString("HistoryDataSource", iClientId);
        } // method: GetHistoryTrackingConnectionString

        /// <summary>
        /// Retrive the MDA Connection string for the corresponding Client Id
        /// </summary>
        /// <param name="iClientId">Client Id whose MDA Connection string needs to be retrieved </param>
        /// <returns>MDA Connection String</returns>
        public static string GetMDAConnectionString(int iClientId)
        {
            return GetConnectionString("MDADataSource", iClientId);
        } // method: GetMDAConnectionString

        /// <summary>
        /// Retrive the View Connection string for the corresponding Client Id
        /// </summary>
        /// <param name="iClientId">Client Id whose View Connection string needs to be retrieved </param>
        /// <returns>View Connection String</returns>
        public static string GetReportServerConnectionString(int iClientId)
        {
            return GetConnectionString("ReportServer_ConnectionString", iClientId);
        } // method: GetViewConnectionString
    }
}
