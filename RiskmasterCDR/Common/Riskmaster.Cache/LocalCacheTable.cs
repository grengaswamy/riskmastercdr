﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.Cache
{
    /// <summary>
    /// Implement the InMemory Cache in form of HashTable. Will be used primarily in applications which are not cloud.
    /// </summary>
    internal class LocalCacheTable : ICache, IDisposable
    {
        private static Hashtable m_ConnectionCache = new Hashtable();
        private bool isDisposed = false;

        /// <summary>
        /// Implement the method of IDisposable interface to suppress the finalize
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Override the Dispsose method to dispose the Connection object
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    if (m_ConnectionCache != null)
                    {
                        m_ConnectionCache = null;
                    }
                }
            }
            isDisposed = true;
        }

        /// <summary>
        /// Checks if the Cache is created and if the Key ClientId combination is present in Cache, then removes it
        /// </summary>
        /// <param name="Key">Key of the Item to be deleted</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>If the delete is successful or not. If the cache is not yet created or Key is not present in Cache, then also it returns true</returns>
        public bool Remove(string Key, int p_iClientId)
        {
            try
            {
                if (m_ConnectionCache != null && this.ContainsKey(Key, p_iClientId))
                    m_ConnectionCache.Remove(String.Format("{0}_{1}", Key, p_iClientId));
                return true;
            }
            catch
            {
                return false;
            };
        }
        /// <summary>
        /// RemoveKeys
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="p_iClientId"></param>
        /// <returns></returns>
        public bool RemoveKeys(string Key, int p_iClientId)
        {
            try
            {
                if (m_ConnectionCache != null)
                {
                    m_ConnectionCache.Clear();//for safe side removing all
                    //foreach (var k in m_ConnectionCache.Keys)
                    //{
                    //    if (k.ToString().EndsWith(String.Format("{0}_{1}", Key, p_iClientId)))
                    //    {
                    //        m_ConnectionCache.Remove(k);
                    //    }
                    //}
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// If the Cache is not yet created, it creates the cache and inserts the item to the cache
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being inserted.</typeparam>
        /// <param name="Key">Key of the Item to be added</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <param name="Value">The Value which needs to be added to Cache</param>
        /// <returns>If the Insert is successful or not</returns>
        public bool Add<T>(string Key, int p_iClientId, T Value)
        {
            try
            {
                if (m_ConnectionCache == null)
                    m_ConnectionCache = new Hashtable();
                m_ConnectionCache.Add(String.Format("{0}_{1}", Key, p_iClientId), Value);
                return true;
            }
            catch
            {
                return false;
            };
        }

        /// <summary>
        /// Updates the given item to the corresponding key.
        /// </summary>
        /// <typeparam name="T">The DataType of the Value being updated.</typeparam>
        /// <param name="Key">Key of the Item to be updated</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <param name="Value">The Value which needs to be updated to Cache</param>
        /// <returns>If the update is successful or not</returns>
        public bool Update<T>(string Key, int p_iClientId, T Value)
        {
            try
            {
                m_ConnectionCache[String.Format("{0}_{1}", Key, p_iClientId)] = Value;
                return true;
            }
            catch
            {
                return false;
            };
        }

        /// <summary>
        /// Retrievs the value corresponding to the key in the expected datatype
        /// </summary>
        /// <typeparam name="T">Expected Data Type</typeparam>
        /// <param name="Key">Key of the Item to be updated</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>The Value corresponding to the key present in cache in expected datatype</returns>
        public T Get<T>(string Key, int p_iClientId)
        {
            if (m_ConnectionCache[String.Format("{0}_{1}", Key, p_iClientId)] is T)
            {
                return (T)m_ConnectionCache[String.Format("{0}_{1}", Key, p_iClientId)];
            }
            else
            {
                try
                {
                    return (T)Convert.ChangeType(m_ConnectionCache[String.Format("{0}_{1}", Key, p_iClientId)], typeof(T));
                }
                catch (InvalidCastException)
                {
                    return default(T);
                }
            }
        }

        /// <summary>
        /// Checks if the Cache is created and if the Key Exists in the Cache
        /// </summary>
        /// <param name="Key">Key of the Item to be checked</param>
        /// <param name="p_iClientId">Appropriate Client id for Cloud.</param>
        /// <returns>If the Key exists in the cache. If the cache is not yet created or Key is not present in Cache, then also it returns false</returns>
        public bool ContainsKey(string Key, int p_iClientId)
        {
            if (m_ConnectionCache != null)
                return m_ConnectionCache.ContainsKey(String.Format("{0}_{1}", Key, p_iClientId));
            else
                return false;
        }


    }
}