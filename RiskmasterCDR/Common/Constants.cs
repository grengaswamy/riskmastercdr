using System;
using System.Collections.Generic;

namespace Riskmaster.Common
{
	/// <summary>
	/// Author  :   Sumeet Rathod
	/// Dated   :   07 Sep 2004
	/// Purpose :  This is used for setting the constant values. All the constant values should be added in here.
	/// </summary>
	public class Constants
	{

        //BSB 03.23.2007 Let's organize constants here - it is confusing and error prone to duplicate 
        // "constants" across multiple modules.
        public class Views
        { 
            public const int EXTENDED_SCREEN_VIEWID = -1;
            public const int BASE_VIEWID =0;
        }
        public class RenderHints
        { 
            public const string PREFIX_ENTITY_ABBR = "PREFIX_ENTITY_ABBR";
        }
        /// <summary>
		/// This is the default constructor
		/// </summary>
		public Constants()
		{
		}


		/// <summary>
		/// Represents the MS ACCESS Database
		/// </summary>
		public const string DB_ACCESS = "DBMS_IS_ACCESS";
		/// <summary>
		/// Represents the SQLSERVER Database
		/// </summary>
		public const string DB_SQLSRVR = "DBMS_IS_SQLSRVR";
		/// <summary>
		/// Represents the SYBASE Database
		/// </summary>
		public const string DB_SYBASE = "DBMS_IS_SYBASE";
		/// <summary>
		/// Represents the INFORMIX Database
		/// </summary>
		public const string DB_INFORMIX = "DBMS_IS_INFORMIX";
		/// <summary>
		/// Represents the ORACLE Database
		/// </summary>
		public const string DB_ORACLE = "DBMS_IS_ORACLE";
		/// <summary>
		/// Represents any ODBC Database
		/// </summary>
		public const string DB_ODBC = "DBMS_IS_ODBC";
		/// <summary>
		/// Represents the DB2 Database
		/// </summary>
		public const string DB_DB2 = "DBMS_IS_DB2";

		//Field type constants
		/// <summary>
		/// Represents field type Check box
		/// </summary>
		public const string CHECK_BOX = "checkbox";
		/// <summary>
		/// Represents field type Code.
		/// </summary>
		public const string CODE = "code";
		/// <summary>
		/// Represents field type Code list.
		/// </summary>
		public const string CODE_LIST = "codelist";
		/// <summary>
		/// Represents field type organization.
		/// </summary>
		public const string ORGH = "orgh";
		/// <summary>
		/// Represents field type State.
		/// </summary>
		public const string STATE = "state";
		/// <summary>
		/// Represents field type Multi state.
		/// </summary>
		public const string MULTI_STATE = "multistate";
		/// <summary>
		/// Represents field type Entity list.
		/// </summary>
		public const string ENTITY_LIST = "entitylist";
		/// <summary>
		/// Represents field type Entity.
		/// </summary>
		public const string ENTITY = "entity";
		/// <summary>
		/// Represents field type Currency.
		/// </summary>
		public const string CURRENCY = "currency";
		/// <summary>
		/// Represents field type Numeric.
		/// </summary>
		public const string NUMERIC = "numeric";
        /// <summary>
        /// Represents field type Double.
        /// </summary>
        public const string DOUBLE = "double";
		/// <summary>
		/// Represents field type Text.
		/// </summary>
		public const string TEXT = "text";
		/// <summary>
		/// Represents field type SSN.
		/// </summary>
		public const string SSN = "ssn";
		/// <summary>
		/// Represents field type Date.
		/// </summary>
		public const string DATE = "date";
		/// <summary>
		/// Represents field type Time.
		/// </summary>
		public const string TIME = "time";
		/// <summary>
		/// Represents field type Table list.
		/// </summary>
		public const string TABLE_LIST = "tablelist";
		/// <summary>
		/// Represents field type Textml.
		/// </summary>
		public const string TEXTML = "textml";
        /// <summary>
        /// Represents field type Free Code.MITS 16476
        /// </summary>
        public const string FREECODE = "freecode";

        // npadhy 6415 - Implement the Search for User lookup
        /// <summary>
        /// Represents the Field Type for User Lookup
        /// </summary>
        public const string USERLOOKUP = "userlookup";

        /// <summary>
        /// Provides the C# equivalent of a Visual Basic Carriage Return (vbCr)
        /// or a Char(13 character
        /// </summary>
        public static string VBCr = Convert.ToChar(13).ToString();

        /// <summary>
        /// Provides the C# equivalent of a Visual Basic Line Feed (vbLf)
        /// or a Char(10) character
        /// </summary>
        public static string VBLf = Convert.ToChar(10).ToString();

        /// <summary>
        /// Provides the C# equivalient of a Visual Basic Carriage Return Line Feed (vbCrLf)
        /// </summary>
        public static string VBCrLf = Environment.NewLine;
        //skhare7
        /// Represents field type Time.
        /// </summary>
        public const string ATTACHEDRECORD = "attachedrecord";
        public const string listDriver = "driver,ndnamed driver";
        public const string listAgent =  "agent";
        public const string listInsured = "policy owner";
        /// <summary>

		/// <summary>
		/// Enumerated type for Navigation Direction
		/// </summary>		
		public enum NavDir:int
		{
			/// <summary>
			/// No Navigation</summary>
			NavigationNone = 0,
			/// <summary>
			/// Navigate to first record</summary>
			NavigationFirst = 1,
			/// <summary>
			/// Navigate to previous record</summary>
			NavigationPrev = 2,
			/// <summary>
			/// Navigate to next record</summary>
			NavigationNext = 3,
			/// <summary>
			/// Navigate to last record</summary>
			NavigationLast = 4,
			/// <summary>
			/// Navigate to a specific record by specifying the id</summary>
			NavigationGoTo = 99
		}
        /// <summary>
        /// Account Status for login
        /// </summary>
        public enum ACCOUNT_STATUS
        {
            Default = 0,
            Locked = 1,
            UserPasswordExpired = 2,
            ForcedPasswordExpired = 3,
            PasswordExpiringSoon = 4
        }

        /// <summary>
        /// Event category display id used to decide if an Event is a Fall, Medication or Equipment Event
        /// </summary>
        public enum EVENT_CATEGORY_DISPLAY_ID
        {
            Other = 0,
            Fall = 1,
            Medication = 2,
            Equipment = 3
        }
        //rbhatia4:R8: Use Media View Setting : July 05 2011
        public enum DOCUMENT_MANAGEMENT_VENDOR_NAME
        {
            RiskmasterX ,
            PaperVision ,
            MediaView ,
            Acrosoft 
        }
        

        /// <summary>
        /// ATTACH_TABLE Columns Value.
        /// </summary>
        /// Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        public enum WPA_DIARY_ENTRY_ATTACH_TABLE
        {
            ///<summary>ENTITYMAINT</summary>
            ENTITYMAINT,
            ///<summary>CLAIM</summary>
            CLAIM,
            ///<summary>CLAIMANT</summary>
            CLAIMANT,
            ///<summary>COMPL_MGT</summary>
            COMPL_MGT,
            ///<summary>FUNDS</summary>
            FUNDS,
            ///<summary>PROPERTYLOSS</summary>
            PROPERTYLOSS,
            ///<summary>RESERVE</summary>
            RESERVE,
            ///<summary>UNIT</summary>
            UNIT,
            ///<summary>SITELOSS</summary>
            SITELOSS,
            ///<summary>OTHERUNIT</summary>
            OTHERUNIT
        }
        //Ankit End
        //smishra54: MITS 35932
        //Logical representation of Unit Type Indicator to avoid hard coding
        public enum UNIT_TYPE_INDICATOR 
        {
            ///<summary>VECHICLE UNIT</summary>
            VEHICLE,
            ///<summary>PROPERTY UNIT</summary>
            PROPERTY,
            ///<summary>SITE UNIT</summary>
            SITE,
            ///<summary>STAT UNIT</summary>
            STAT 
        }

        //Logical representation of Policy system Types to avoid hard coding
        public enum POLICY_SYSTEM_TYPE 
        {
            ///<summary>Represents Policy System Type POINT</summary>
            POINT,
            ///<summary>Represents Policy System Type INTEGRAL</summary>
            INTEGRAL,
            ///<summary>Represents Staging Policy System</summary>
            STAGING,
        }
        //smishra54: End
	}
}
