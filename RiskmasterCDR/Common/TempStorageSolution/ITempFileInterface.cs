﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Riskmaster.Models;

namespace Riskmaster.Common
{
    public interface ITempFileInterface
    {
        // Remove a file from storage area
        bool Remove(Dictionary<string, string> parms); //abhal3 MITS 36046
        // Add a file to storage area
        FileStorageOutPut Add(Dictionary<string, string> parms, byte[] file);//abhal3 MITS 36046
        FileStorageOutPut Add(Dictionary<string, string> parms, FileStream file);

        //Get a file from storage area
        byte[] Get(Dictionary<string, string> parms);//abhal3 MITS 36046

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // Get Multiple Files from DB in one go. We do not want to query DB multiple times.
        // Rather we fetch all the files in one go
        List<StreamedUploadDocumentType> GetMultiple(Dictionary<string, string> parms);
    }

    public class StorageTypeEnumeration
    {
        public enum TempStorageType
        {
            Database = 1,
            FileSystem = 2
        }
    }

    public class FileStorageOutPut
    {
        public bool Success
        {
            get;
            set;
        }
        public int RowId
        {
            get;
            set;
        }
    }
}
