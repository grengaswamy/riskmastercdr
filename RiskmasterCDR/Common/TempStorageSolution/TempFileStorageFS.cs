﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Riskmaster.Models;

namespace Riskmaster.Common
{
    class TempFileStorageFS : ITempFileInterface
    {
        public bool Remove(Dictionary<string, string> parms)
        {
            bool bSuccess = false;
            string sFilename = "";
            string sFilePath = "";
            try
            {
                parms.TryGetValue("filename", out sFilename);
                parms.TryGetValue("filepath", out sFilePath);
                File.Delete(Path.Combine(sFilePath, sFilename));
                bSuccess = true;
            }
            catch (Exception e)
            {
                throw e;
            }
            return bSuccess;
        }

        public FileStorageOutPut Add(Dictionary<string, string> parms, byte[] file)
        {
            bool bSuccess=false;
            string sFilename = "";
            string sFilePath = "";
            FileStorageOutPut objOutput = null;
       
            try
            {
            parms.TryGetValue("filename",out sFilename );
            parms.TryGetValue("filepath", out sFilePath);
            File.WriteAllBytes(Path.Combine(sFilePath, sFilename), file);
            bSuccess=true;

            objOutput = new FileStorageOutPut();
            objOutput.RowId = 0;
            objOutput.Success = bSuccess;

            }
            catch (Exception e)
            {
                throw e;
            }
            return objOutput;
        }

        public FileStorageOutPut Add(Dictionary<string, string> parms, FileStream file)
        {
            FileStorageOutPut oFileStorageOutPut = null;
            return oFileStorageOutPut;
        }

        public byte[] Get(Dictionary<string, string> parms)
        {
            string sFilename = "";
            string sFilePath = "";
            byte[] file = null;
            try
            {
                parms.TryGetValue("filename", out sFilename);
                parms.TryGetValue("filepath", out sFilePath);
                file = File.ReadAllBytes(Path.Combine(sFilePath, sFilename));

            }
            catch (Exception e)
            {
                throw e;
            }
            return file;

        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // Though right now we save the temp files in DB and do not save it in disk for cloud,
        // But change in Interface requires change in class as well
        public List<StreamedUploadDocumentType> GetMultiple(Dictionary<string, string> parms)
        {
            throw new NotImplementedException();
        }
    }
}
