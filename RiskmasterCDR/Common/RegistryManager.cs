﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Riskmaster.Common
{
    /// <summary>
    /// Manages Registry Operations
    /// </summary>
    public static class RegistryManager
    {
        /// <summary>
        /// Reads the specified registry key
        /// </summary>
        /// <param name="strRegKey">string containing the path to the Registry Key</param>
        /// <param name="strRegKeyName">string containing the name of the Registry Key</param>
        /// <returns>string containing the value for the registry key</returns>
        public static string ReadRegistryKey(string strRegKey, string strRegKeyName)
        {
            string strRegKeyValue = string.Empty;

            RegistryKey key = Registry.LocalMachine.OpenSubKey(strRegKey);

            if (key != null)
            {
                try
                {
                    //Check for an empty registry key name
                    if (strRegKeyName.Equals(string.Empty))
                    {
                        strRegKeyValue = key.GetValue("") as string;
                    }//if
                    else
                    {
                        strRegKeyValue = key.GetValue(strRegKeyName) as string;
                    }//else
                }//try
                finally
                {
                    key.Close();
                    key = null;
                }//finally
            }//if

            //return the value for the registry key
            return strRegKeyValue;
        }//method: ReadRegistryKey()

        /// <summary>
        /// Gets all of the Registry Subkeys for the specified Registry Key
        /// </summary>
        /// <param name="strRegKey">string containing the path to the Registry Key</param>
        /// <returns>string array of Registry Subkeys</returns>
        public static string[] GetRegistrySubKeys(string strRegKey)
        {

            RegistryKey key = Registry.LocalMachine.OpenSubKey(strRegKey);
            string[] arrSubKeys = key.GetSubKeyNames();

            return arrSubKeys;
        }//method: GetRegistrySubKeys

        /// <summary>
        /// Gets all of the Registry Key Values for the specified Registry Key
        /// </summary>
        /// <param name="strRegKey">string containing the path to the Registry Key</param>
        /// <returns>string array of Registry Values</returns>
        public static string[] GetRegistryValues(string strRegKey)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(strRegKey);
            string[] arrKeyValues = key.GetValueNames();

            return arrKeyValues;
        }//method: GetRegistryValues
    }
}
