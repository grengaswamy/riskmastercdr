﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Cache;
using Riskmaster.Db;
using System.Net.Mail;
using System.Data;
using System.Collections;


namespace Riskmaster.Common
{
    /// <summary>
    /// Centrally handles all operations required for RMConfigurator
    /// in terms of retrieving application settings, connection strings etc.
    /// </summary>
    public class RMConfigurationSettings
    {
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSessionDSN
        /// </summary>     
        public static string GetSessionDSN()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSessionDSN(0);
            }
            else
                throw new Exception("GetSessionDSN" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets the connection string for the Riskmaster Session database
        /// </summary>
        /// <returns>string containing the Session DSN</returns>
        /// <exception cref="ConfigurationErrorsException">connectionString is unavailable or incorrectly configured</exception>
        public static string GetSessionDSN(int iClientId)
        {
            try
            {
                return ConfigurationInfo.GetSessionConnectionString(iClientId);
            }
            catch (ConfigurationErrorsException ex)
            {

                throw new ConfigurationErrorsException(ex.Message, ex);
            }
        } // method: GetSessionDSN
        /// <summary>
        /// Gets the connection string for the Riskmaster Security database
        /// </summary>
        /// <returns>string containing the Security DSN</returns>
        /// /// <exception cref="ConfigurationErrorsException">connectionString is unavailable or incorrectly configured</exception>
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSecurityDSN
        /// </summary>     
        public static string GetSecurityDSN()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSecurityDSN(0);
            }
            else
                throw new Exception("GetSecurityDSN" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static string GetSecurityDSN(int iClientId)
        {
            try
            {
                return ConfigurationInfo.GetSecurityConnectionString(iClientId);
            }
            catch (ConfigurationErrorsException ex)
            {

                throw new ConfigurationErrorsException(ex.Message, ex);
            }
        } // method: GetSecurityDSN
        /// <summary>
        /// Gets the currently configured SMTP Server from the RISKMASTER Security database
        /// </summary>
        /// <returns>string containing the value for the SMTP Server</returns>
        /// <exception cref="SmtpException">Throws an SmtpException if the Smtp Server has not yet been configured.</exception>
        /// //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSMTPServer
        /// </summary>     
        public static string GetSMTPServer()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSMTPServer(0);
            }
            else
                throw new Exception("GetSMTPServer" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
              // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        public static string GetSMTPServer(int p_iClientId)
        {
            object objSmtpServer = null;
            string strSmtpServer = string.Empty;

            string strSecConnString = GetSecurityDSN(p_iClientId);

            objSmtpServer = DbFactory.ExecuteScalar(strSecConnString, "SELECT SMTP_SERVER FROM SETTINGS");

            //Throw an exception if an Smtp Server has not been configured in the system
            if (objSmtpServer == DBNull.Value)
            {
                throw new SmtpException("The Smtp Server is not currently configured.  Please verify that your Administrator has properly configured the Smtp Server prior to sending e-mails.");
            } // if
            else
            {
                strSmtpServer = objSmtpServer.ToString();
            } // else


            return strSmtpServer;
        } // method: GetSMTPServer

        /// <summary>
        /// Gets the currently configured SMTP Server settings from the database
        /// </summary>
        /// <returns>DataTable containing the row/record containing SMTP Server settings</returns>
        /// 
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSMTPServerSettings
        /// </summary>     
        public static DataTable GetSMTPServerSettings()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSMTPServerSettings(0);
            }
            else
                throw new Exception("GetSMTPServerSettings" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static DataTable GetSMTPServerSettings(int iClientId)
        {
            string strSecConnString = GetSecurityDSN(iClientId);
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            DataSet dstSMTPSettings = DbFactory.ExecuteDataSet(strSecConnString, "SELECT SMTP_SERVER, ADMIN_EMAIL_ADDR,ALTERNATE_DOMAIN,REPLYTO_DOMAIN FROM SETTINGS", new Dictionary<string, string>(), iClientId);

            return dstSMTPSettings.Tables[0];
        } // method: GetSMTPServer
        /// <summary>
        /// Updates the SMTP Server and Admin E-mail address information in the database
        /// </summary>
        /// <param name="strSMTPServer">string containing the SMTP Server information</param>
        /// <param name="strAdminEmailAddr">string containing the Admin Email Address</param>
        /// <returns>integer containing the number of records updated in the database</returns>
        /// 
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for SetSMTPServer
        /// </summary>     
        public static int SetSMTPServer(string strSMTPServer, string strAdminEmailAddr, string p_sSenderAlternateDomain, string p_sSenderReplytoDomain)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return SetSMTPServer( strSMTPServer,  strAdminEmailAddr,  p_sSenderAlternateDomain,  p_sSenderReplytoDomain, 0);
            }
            else
                throw new Exception("SetSMTPServer" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        public static int SetSMTPServer(string strSMTPServer, string strAdminEmailAddr, string p_sSenderAlternateDomain, string p_sSenderReplytoDomain, int p_iClientId)
        {
            int intRecordsUpdated = 0;
            string strSQLStmt = string.Empty;

            string strSecConnString = GetSecurityDSN(p_iClientId);

            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            dictParams.Add("SMTP_SERVER", strSMTPServer);
            dictParams.Add("ADMIN_EMAIL", strAdminEmailAddr);
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            dictParams.Add("ALTERNATE_DOMAIN", p_sSenderAlternateDomain);
            dictParams.Add("REPLYTO_DOMAIN", p_sSenderReplytoDomain);

            strSQLStmt = string.Format("UPDATE SETTINGS SET SMTP_SERVER={0}, ADMIN_EMAIL_ADDR={1}, ALTERNATE_DOMAIN={2}, REPLYTO_DOMAIN={3}", "~SMTP_SERVER~", "~ADMIN_EMAIL~", "~ALTERNATE_DOMAIN~", "~REPLYTO_DOMAIN~");

            intRecordsUpdated = DbFactory.ExecuteNonQuery(strSecConnString, strSQLStmt, dictParams);

            return intRecordsUpdated;
        } // method: SetSMTPServer

        /// <summary>
        /// Gets all of the configurable RISKMASTER Messages from
        /// a configuration file
        /// </summary>
        /// <returns>Hashtable containing all of the configurable RISKMASTER Messages</returns>
        public static Hashtable GetRMMessages(string sConnString, int iClientId)
        {
            return RMConfigurationManager.GetDictionarySectionSettings("Messages", sConnString, iClientId);
        } // method: RMMessages

    }//class
}//namespace
