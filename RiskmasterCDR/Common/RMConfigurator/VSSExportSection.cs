﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace Riskmaster.Common
{
    /// <summary>
    /// /// ************************************
    /// Author : Anshul Verma
    /// ************************************
    /// ConfigurationSection class for VSS Export settings
    /// </summary>
    /// <remarks>This class is not inherited from ConfigurationSection</remarks>
   public class VSSExportSection
    {
        #region Class Constructor
        /// <summary>
        /// Default class constructor
        /// </summary>
       public VSSExportSection()
        {

        } // constructor 
        #endregion

       #region Static Public Properties
       public static string VSSExportUrl { get; set; }
       public static string VSSExportService { get; set; }
       public static string Logging { get; set; }
       public static string VSSLoginURL {get; set;}

       private static string m_sConnString = string.Empty;
       private static int m_iClientId = 0;
       #endregion

        /// <summary>
        /// ************************************
        /// Author : Anshul Verma
        /// ************************************
        /// Populates all of the VSS Export Section 
        /// static properties through Reflection
        /// by reading the values from the configuration file
        /// </summary>

       public static void GetVSSExportSection()
       {
           NameValueCollection nvCollVSSExport = RMConfigurationManager.GetSectionSettings("VSSInterface", m_sConnString, m_iClientId);

           VSSExportSection objVSSExportSection = new VSSExportSection();

           Type typMediaViewSection = Type.GetType("Riskmaster.Common.VSSExportSection");

           PropertyInfo[] objProps = typMediaViewSection.GetProperties();

           foreach (PropertyInfo objProp in objProps)
           {
               objProp.SetValue(objVSSExportSection, nvCollVSSExport[objProp.Name], null);
           }//foreach
       } // method: GetMediaViewSection
    }
}
