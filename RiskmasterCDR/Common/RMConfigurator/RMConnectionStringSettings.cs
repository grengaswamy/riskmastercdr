﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.Common
{
    /// <summary>
    /// Storage container for .Net Connection String settings
    /// </summary>
    public class RMConnectionStringSettings
    {

        #region Class constructors
        /// <summary>
        /// Default class constructor
        /// </summary>
        internal RMConnectionStringSettings()
        {
        }//class constructor 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets and sets the ConnectionString ProviderName 
        /// </summary>
        /// <example>Possible ADO.Net Providers include System.Data.SqlClient and Oracle.DataAccess.Client</example>
        public string ProviderName { get; set; }

        /// <summary>
        /// Gets and sets the ConnectionString 
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets and sets the ConnectionString Name
        /// </summary>
        public string ConnectionStringName { get; set; }
        #endregion

    }//class: RMConnectionStringsSettings
}
