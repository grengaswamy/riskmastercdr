﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace Riskmaster.Common
{
    /// <summary>
    /// /// ************************************
    /// Author : Raman Bhatia
    /// ************************************
    /// ConfigurationSection class for MediaView settings
    /// </summary>
    /// <remarks>This class is not inherited from ConfigurationSection
    /// since it naturally derives from the NameValueSection class</remarks>
    public class MediaViewSection
    {
        #region Class Constructor
        /// <summary>
        /// Default class constructor
        /// </summary>
        public MediaViewSection()
        {

        } // constructor 
        #endregion

        #region Static Public Properties
        public static string MediaViewUrl { get; set; }
        public static string MediaViewService { get; set; }
        public static string Source { get; set; }
        public static string SourceCLM { get; set; }
        public static string SourceMM { get; set; }
        public static string SourcePOL { get; set; }
        public static string SourceEvent { get; set; }
        public static string Enterprise { get; set; }
        public static string Level { get; set; }
        public static string UserType { get; set; }
        public static string MMAgent { get; set; }
        public static string MMAgency { get; set; }
        public static string CommonUser { get; set; }
        public static string AppConfigList { get; set; }
        //Anu Tennyson : Media View different for Claim and Policy System Starts
        public static string AppConfigListCLM { get; set; }
        public static string AppConfigListPOL { get; set; }
        public static string AppConfigListMM { get; set; }
        public static string AppConfigListEvent { get; set; }
        //Anu Tennyson : Media View different for Claim and Policy System Ends
        private static string m_sConnString = string.Empty;
        private static int m_iClientId = 0;
        #endregion
        /// <summary>
        /// ************************************
        /// Author : Raman Bhatia
        /// ************************************
        /// Populates all of the MediaView Section 
        /// static properties through Reflection
        /// by reading the values from the configuration file
        /// </summary>
        public static void GetMediaViewSection()
        {
            NameValueCollection nvCollMediaView = RMConfigurationManager.GetSectionSettings("MediaView", m_sConnString, m_iClientId);

            MediaViewSection objMediaViewSection = new MediaViewSection();

            Type typMediaViewSection = Type.GetType("Riskmaster.Common.MediaViewSection");

            PropertyInfo[] objProps = typMediaViewSection.GetProperties();

            foreach (PropertyInfo objProp in objProps)
            {
                objProp.SetValue(objMediaViewSection, nvCollMediaView[objProp.Name], null);
            }//foreach
        } // method: GetMediaViewSection

    } // class
}//namespace
