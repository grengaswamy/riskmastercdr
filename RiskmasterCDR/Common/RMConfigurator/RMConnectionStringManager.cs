﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Riskmaster.Db;

namespace Riskmaster.Common
{
    /// <summary>
    /// Manages the population of Connection String Settings from the connectionStrings
    /// section in configuration files
    /// </summary>
    public class RMConnectionStringManager
    {
        /// <summary>
        /// Private class constructor
        /// </summary>
        private RMConnectionStringManager()
        {
        }//class constructor


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConnectionStringSettings
        /// </summary>     
        public static RMConnectionStringSettings ConnectionStringSettings(string strConnectionStringName)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConnectionStringSettings(strConnectionStringName, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("ConnectionStringSettings -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets the available connection string settings for a specified database connection string
        /// </summary>
        /// <param name="strConnectionStringName">string containing the name of the specified connection string</param>
        public static RMConnectionStringSettings ConnectionStringSettings(string strConnectionStringName,int iClientId)
        {
            RMConnectionStringSettings rmconnSettings = null;
            if (iClientId == 0)
            {
                rmconnSettings = new RMConnectionStringSettings
                {
                    ConnectionString = ConfigurationManager.ConnectionStrings[strConnectionStringName].ConnectionString,
                    ConnectionStringName = ConfigurationManager.ConnectionStrings[strConnectionStringName].Name,
                    ProviderName = ConfigurationManager.ConnectionStrings[strConnectionStringName].ProviderName
                };

            }
            else
            {
                rmconnSettings = new RMConnectionStringSettings 
                {
                    ConnectionString = Riskmaster.Cache.ConfigurationInfo.GetConnectionString(strConnectionStringName,iClientId),
                    ConnectionStringName = strConnectionStringName,
                    ProviderName = ConfigurationManager.ConnectionStrings["rmATenantSecurity"].ProviderName
                };
            }
            return rmconnSettings;
        }//method: ConnectionStringSettings
    }//class: RMConnectionStringManager
}

