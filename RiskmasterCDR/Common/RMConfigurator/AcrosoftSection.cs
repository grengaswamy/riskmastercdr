﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace Riskmaster.Common
{
    /// <summary>
    /// ConfigurationSection class for Acrosoft/MCM settings
    /// </summary>
    /// <remarks>This class is not inherited from ConfigurationSection
    /// since it naturally derives from the NameValueSection class</remarks>
    public class AcrosoftSection
    {
        #region Class Constructor
        /// <summary>
        /// Default class constructor
        /// </summary>
        public AcrosoftSection()
        {

        } // constructor 
        #endregion

        #region Static Public Properties
        public static string AcrosoftAttachmentsTypeKey { get; set; }
        public static string AcrosoftUsersTypeKey { get; set; }
        public static string AcrosoftGeneralStorageTypeKey { get; set; }
        public static string AcrosoftPolicyTypeKey { get; set; }
        public static string EventFolderFriendlyName { get; set; }
        public static string ClaimFolderFriendlyName { get; set; }
        public static string UsersFolderFriendlyName { get; set; }
        public static string GeneralFolderFriendlyName { get; set; }
        public static string PolicyFolderFriendlyName { get; set; }
        public static string AcrosoftSkin { get; set; }
        public static string AsAnywhereLink { get; set; }
        public static string AuthService { get; set; }
        public static string ASObjectWebService { get; set; }
        public static string ASBaseWebService { get; set; }
        public static string IndexService { get; set; }
        //skhare7: 
        public static string SearchFolderBeforeCreate { get; set; }

        //rsolanki2 :  start updates for MCM mits 19200 
        public static string UseCommonAcrosoftUser { get; set; }
        public static string AcrosoftUsername { get; set; }
        public static string AcrosoftUserPassword { get; set; }
        // rsolanki2: a reference to the assembly System.configuration will need to be added for access to read AcroUserMapSection section
        public static UserMapSection AcroUserMapSection { get; set; }
		
		// atavaragiri MITS 25696 //
		public static string AcrosoftSubFolderName { get; set; } 

        
        //rsolanki2 :  end updates for MCM mits 19200 


        public static string ShowMCMForFunds { get; set; }  //sachin :For mits 30809

        // akaushik5 Added for MITS 30117 Starts
        /// <summary>
        /// Gets or sets the use RMA document management for entity.
        /// </summary>
        /// <value>
        /// The use RMA document management for entity.
        /// </value>
        public static string UseRMADocumentManagementForEntity { get; set; }
        // akaushik5 Added for MITS 30117 Ends

        private static string m_sConnString = string.Empty;
        private static int m_iClientId = 0;

        #endregion

        
        /// <summary>
        /// Populates all of the Acrosoft Section 
        /// static properties through Reflection
        /// by reading the values from the configuration file
        /// </summary>
        public static void GetAcrosoftSection()
        {
            NameValueCollection nvCollAcrosoft = RMConfigurationManager.GetSectionSettings("Acrosoft", m_sConnString,m_iClientId);

            AcrosoftSection objAcrosoftSection = new AcrosoftSection();
            
            Type typAcrosoftSection = Type.GetType("Riskmaster.Common.AcrosoftSection");

            PropertyInfo[] objProps = typAcrosoftSection.GetProperties();

            foreach (PropertyInfo objProp in objProps)
            {
                objProp.SetValue(objAcrosoftSection, nvCollAcrosoft[objProp.Name], null);
            }//foreach

            //// rsolanki2: start updates for mits 19200
            if ((!string.IsNullOrEmpty(UseCommonAcrosoftUser)) && Convert.ToBoolean(UseCommonAcrosoftUser))
            {
                // rsolanki2: the AcroUserMapSection is retrived only when the UseCommonAcrosoftUser property is set to True in web.config 
                AcroUserMapSection = ConfigurationManager.GetSection("UserMapList") as UserMapSection;               
            }
            //// rsolanki2: end updates for mits 19200
           

        } // method: GetAcrosoftSection

    } // class
}//namespace
