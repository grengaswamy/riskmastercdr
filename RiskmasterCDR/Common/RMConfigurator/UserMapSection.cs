using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

///************************************************************** 
///* $File		: UserMapSection.cs
///* $Date		: Jan-25-2009 
///* $Author	: rahul solanki
///* $Comment	: 
///* $Source	: 
///**************************************************************
/// <summary>	
/// Provides classes for reading custom config file 
/// mapping RMX login id's to MCM login id's
/// </summary>
namespace Riskmaster.Common
{
	public class UserMapSection: ConfigurationSection
	{
		#region Constructors
		static UserMapSection()
		{
            // the name for the UserMapList section is optional and may be utilized afterwards.
			s_SectionName = new ConfigurationProperty("name",typeof(string),null,ConfigurationPropertyOptions.None);

			s_UserMapElement = new ConfigurationProperty("",typeof(UserMapElementCollection),null,ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsDefaultCollection);

			s_properties = new ConfigurationPropertyCollection();

			s_properties.Add(s_SectionName);
			s_properties.Add(s_UserMapElement);
		}
		#endregion

		#region Fields
		private static ConfigurationPropertyCollection s_properties;
		private static ConfigurationProperty s_SectionName;
		private static ConfigurationProperty s_UserMapElement;
		#endregion

		#region Properties
		public string Name
		{
			get { return (string)base[s_SectionName]; }
			set { base[s_SectionName] = value; }
		}

		public UserMapElementCollection UserMap
		{
			get { return (UserMapElementCollection)base[s_UserMapElement]; }
		}

		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				return s_properties;
			}
		}
		#endregion
	}
}