﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Riskmaster.Common
{
    public class KeyValueSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public KeyValueConfigurationCollection Settings
        {
            get { return (KeyValueConfigurationCollection)base[""]; }
        } // property Settings              

    }
}
