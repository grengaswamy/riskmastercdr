using System;
using System.Configuration;
using System.IO;
using System.Xml;
namespace Riskmaster.Common
{

    /// <summary>
    /// Manages configuration file settings
    /// required for file server storage paths
    /// </summary>
    public class RMConfigurator
    {
        
        private const string BASE_PATH_NODE_NAME = "BasePath";
        /// <summary>
        /// Static Constructor.
        /// Loads the custom Riskmaster.config file into m_objDoc
        /// </summary>
        static RMConfigurator()
        {
            LoadFile();
        }


        #region Public Path properties

        /// <summary>
        /// Gets the base installation path for the application
        /// utilized throughout the Riskmaster.config file
        /// </summary>
        public static string BasePath { get; private set; }

        /// <summary>
        /// Gets the appfiles path for the application
        /// utilized throughout the Riskmaster.config file
        /// </summary>
        public static string AppFilesPath { get; private set; }

        /// <summary>
        /// Gets the userdata path for the application
        /// utilized throughout the Riskmaster.config file
        /// </summary>
        public static string UserDataPath { get; private set; }

        /// <summary>
        /// Gets the temporary files path for the application
        /// utilized throughout the Riskmaster.config file
        /// </summary>
        public static string TempPath { get; private set; }

        #endregion

        #region STATIC METHODS
        /// <summary>
        /// Combines a specified base path with a file path to get the resultant
        /// file system path
        /// </summary>
        /// <param name="strBasePath">string containing the base directory path</param>
        /// <param name="strAppendPath">string containing the ending path to a file or directory</param>
        /// <returns>string containing the full path to a file or directory</returns>
        public static string CombineFilePath(string strBasePath, string strAppendPath)
        {
            return Path.Combine(strBasePath, strAppendPath);
        } // method: CombineFilePath



        /// <summary>
        /// Loads the configuration file
        /// First attempt is to look in the applicable .Net config file using the setting APP_CONFIG_PATH
        /// stored there to pick up the desired configuration file location.
        /// Second attempt is to look in the current directory.
        /// Third attempt is taken from the structure of the Source Code in the Riskmaster .Net Solution file.
        /// </summary>
        private static void LoadFile()
        {
            //Set the static path properties
            BasePath = ConfigurationManager.AppSettings[BASE_PATH_NODE_NAME];
            AppFilesPath = Path.Combine(BasePath, "appfiles");
            UserDataPath = Path.Combine(BasePath, "userdata");
            TempPath = Path.Combine(BasePath, "temp");
        }

        #endregion // STATIC METHODS

    }

}

