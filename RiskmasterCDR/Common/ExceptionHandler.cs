﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Riskmaster.Common
{
    /// <summary>
    /// Used to globally handle logging and various other operations
    /// for exceptions thrown by the RISKMASTER Application
    /// </summary>
    public class ExceptionHandler
    {
        /// <summary>
        /// Logs all exceptions using the Exception Handling 
        /// Enterprise Library Application Block
        /// </summary>
        /// <param name="ex">Exception thrown by the application</param>
        /// <param name="strPolicyName">string containing the name of the Exception Handling 
        /// Policy</param>
        public static void LogExceptions(Exception ex, string strPolicyName)
        {
            ExceptionPolicy.HandleException(ex, strPolicyName);
        } // method: LogExceptions

        /// <summary>
        /// Default handler for logging all exceptions using the
        /// Exception Handling Enterprise Library Application Block
        /// </summary>
        /// <param name="ex">Exception thrown by the application</param>
        public static void LogExceptions(Exception ex)
        {
            ExceptionPolicy.HandleException(ex, "Logging Policy");
        } // method: LogExceptions


    }
}
