﻿// Copyright (c) 2008 Eric Eicke
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
//
using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
//using System.Scripting.ActiveScript;

namespace Riskmaster.ActiveScript
{
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("DB01A1E3-A42B-11cf-8F20-00805F2CD064")]       
    public interface IActiveScriptSite
    {        
        void GetLCID(out uint id);
        void GetItemInfo([In, MarshalAs(UnmanagedType.BStr)] string name,[In, MarshalAs(UnmanagedType.U4)] uint returnMask,[Out, MarshalAs(UnmanagedType.IUnknown)] out object item,IntPtr ppti);
        void GetDocVersionString(out string v);
        void OnScriptTerminate(ref object result, ref System.Runtime.InteropServices.ComTypes.EXCEPINFO info);
        void OnStateChange(ScriptState state);
        void OnScriptError([In, MarshalAs(UnmanagedType.IUnknown)] object err);
        void OnEnterScript();
        void OnLeaveScript();    
    }
}
