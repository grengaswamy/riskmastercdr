﻿using System;

namespace Riskmaster.ActiveScript
{
    public class VBScript : ScriptLanguageBase
    {        
        public VBScript()
            : base(new VBScriptEngine())
        {                 
        }

        public VBScript(IntPtr winhandle)
            : base (new VBScriptEngine(),winhandle)
        {            
        }        
    }
}
