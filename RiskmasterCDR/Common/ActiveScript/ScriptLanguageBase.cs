﻿// Copyright (c) 2008 Eric Eicke
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
//
using System;
using System.Collections.Generic;
//using System.Scripting.ActiveScript;

namespace Riskmaster.ActiveScript
{
    public abstract class ScriptLanguageBase: IScriptLanguage
    {
        private ScriptHost _host = null;

        public ScriptLanguageBase(object engine)
        {            
            _host = new ScriptHost(engine);
        }

        public ScriptLanguageBase(object engine, IntPtr winhandle)
        {
            _host = new ScriptHost(engine, winhandle);
        }

        public List<ScriptError> Errorrs
        {
            get
            {
                return _host.Errors;
            }
        }

        public void AddObject(string name, object o)
        {
            _host.AddObject(name, o);
        }

        public void AddScriptFile(string scriptfile)
        {
            string script = "";

            using (System.IO.StreamReader reader = new System.IO.StreamReader(scriptfile))
            {
                script = reader.ReadToEnd();
                script = script.Trim();
                reader.Close();
            }

            if (script.Length > 0)
            {
                AddScript(script);
            }
        }

        public void AddScript(string script)
        {
            _host.AddScript(script);
        }

        public void Open()
        {            
            _host.Open();
        }

        public object ExecuteMethod(string methodname, object[] parameters)
        {
            return _host.ExecuteMethod(methodname, parameters);
        }

        public void Close()
        {
            _host.Close();            
        }

        public object Run()
        {
            return Run(null, new object[] { });
        }

        public object Run(string method)
        {
            return Run(method, new object[] { });
        }

        public object Run(string method, object[] parameters)
        {
            object retval = null;

            Open();

            if ((method != null) && (method.Length > 0))
            {
                ExecuteMethod(method, parameters);
            }

            Close();

            return retval;
        }
    }
}
