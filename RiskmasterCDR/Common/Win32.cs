using System;
using System.Runtime.InteropServices;

namespace Riskmaster.Common.Win32
{
	
	
	/// <summary>
	/// Riskmaster.Common.Win32 contains dll function imports, enumerations and other constructs useful for dealing directly with 
	/// the Win32 API.  All such imports common to Riskmaster code should reside here.
	/// </summary>
	unsafe public class Functions
	{
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.ProcessIdToSessionId.</summary>
		/// <param name="lProcessId"></param>
		/// <param name="lSessionId"></param>
		/// <returns>.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[DllImport("KERNEL32.DLL")]
		public static extern int ProcessIdToSessionId(int lProcessId, ref int lSessionId);
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.GetCurrentProcessId.</summary>
		/// <returns>The Win32 processId of the currently executing process space.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[DllImport("KERNEL32.DLL")]
		public static extern int GetCurrentProcessId();
		
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.MapViewOfFile supports handling the Shared Memory Map Section used by Login.</summary>
		/// <param name="hFileMappingObject"></param>
		/// <param name="dwDesiredAccess"></param>
		/// <param name="dwFileOffsetHigh"></param>
		/// <param name="dwFileOffsetLow"></param>
		/// <param name="dwNumBytesToMap"></param>
		/// <returns>Not specified.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32", SetLastError=true, CharSet=CharSet.Auto) ]
		public static extern IntPtr MapViewOfFile (
			int hFileMappingObject, int dwDesiredAccess, int dwFileOffsetHigh,
			int dwFileOffsetLow, int dwNumBytesToMap );

		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.UnMapViewOfFile supports handling the Shared Memory Map section used by Login.</summary>
		/// <param name="hFileMappingObject"></param>
		/// <returns>Not specified.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32", SetLastError=true, CharSet=CharSet.Auto, EntryPoint="UnmapViewOfFile") ]
		public static extern int UnMapViewOfFile (int hFileMappingObject );

		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.CreateFileMapping supports handling the Shared Memory Map section used by Login..</summary>
		/// <param name="hFile"></param>
		/// <param name="lpAttributes"></param>
		/// <param name="flProtect"></param>
		/// <param name="dwMaximumSizeLow"></param>
		/// <param name="dwMaximumSizeHigh"></param>
		/// <param name="lpName"></param>
		/// <returns>Not specified.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32", SetLastError=true, CharSet=CharSet.Auto) ]
		public static extern int CreateFileMapping ( 
			int hFile, int lpAttributes, int flProtect, 
			int dwMaximumSizeLow, int dwMaximumSizeHigh,
			string lpName );
	
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.CloseHandle.</summary>
		/// <param name="hObject"></param>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32", SetLastError=true) ]
		public static extern void CloseHandle(int hObject);
		
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.GetLastError.</summary>
		/// <returns>Not specified.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32") ]
		public static extern int GetLastError();
		
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.GetDesktopWindow.</summary>
		/// <returns>hWnd of the current desktop.</returns>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("user32") ]
		public static extern int GetDesktopWindow();
		
		
		/// <summary>
		/// Riskmaster.Common.Win32.Functions.CopyMemory.</summary>
		/// <param name="Dest"></param>
		/// <param name="Src"></param>
		/// <param name="cBytes"></param>
		/// <remarks>Consult a Win32 API reference.</remarks>
		[ DllImport("kernel32", EntryPoint="RtlMoveMemory") ]
		public static extern void CopyMemory(
			void* Dest, 
			void* Src,
			int cBytes);
		//		Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

	
		/// <summary>
		/// Used to allocate the environment handle passed as ref.
		/// </summary>
		/// <param name="EnvironmentHandle">Handle passed as ref.</param>
		/// <returns>If an error occurred, the function returns 0 (use GetLastError to get
		/// the error code). If successful, the function returns a non-zero value.
		/// </returns>
		[DllImport("odbc32.dll")]
		public static extern int SQLAllocEnv(ref int EnvironmentHandle);
		
		/// <summary>
		/// Used to free the environment.
		/// </summary>
		/// <param name="EnvironmentHandle">The environment handle.</param>
		/// <returns>If an error occurred, the function returns 0 (use GetLastError to get
		///  the error code). If successful, the function returns a non-zero value.
		/// </returns>
		[DllImport("odbc32.dll")]
		public static extern int SQLFreeEnv(int EnvironmentHandle);

		/// <summary>
		///	SQLDataSources() returns a list of target databases available, one at a time.
		/// </summary>
		/// <param name="EnvironmentHandle">Environment handle.</param>
		/// <param name="Direction">Used by application to request the first data source 
		/// name in the list or the next one in the list. Direction can take on only the 
		/// following values:SQL_FETCH_FIRST, SQL_FETCH_NEXT.
		/// </param>
		/// <param name="ServerName">Pointer to buffer in which to return the data source
		/// name.
		/// </param>
		/// <param name="BufferLength1">Length of the ServerName buffer, in bytes. This 
		/// should be less than or equal to SQL_MAX_DSN_LENGTH + 1.
		/// </param>
		/// <param name="NameLength1Ptr">Pointer to a buffer in which to return the total
		/// number of bytes (excluding the null-termination byte) available to return 
		/// in *ServerName.</param>
		/// <param name="Description">Pointer to buffer where the description of the data
		/// source is returned.</param>
		/// <param name="BufferLength2">Maximum length of the Description buffer.</param>
		/// <param name="NameLength2Ptr">Pointer to a buffer in which to return the total
		/// number of bytes (excluding the null-termination byte) available to return in
		/// *Description.
		/// </param>
		/// <returns>If an error occurred, the function returns 0 (use GetLastError to get 
		/// the error code). If successful, the function returns a non-zero value.
		/// </returns>
		[DllImport("odbc32.dll")]
		public static extern int SQLDataSources(int EnvironmentHandle,int Direction,System.Text.StringBuilder ServerName,int BufferLength1,ref int NameLength1Ptr,System.Text.StringBuilder Description,int BufferLength2,ref int NameLength2Ptr);
		
		/// <summary>
		/// SQLDrivers lists driver descriptions and driver attribute keywords. This 
		/// function is implemented solely by the Driver Manager.
		/// </summary>
		/// <param name="hEnv">[Input]Environment handle.</param>
		/// <param name="fDirection">[Input]Determines whether the Driver Manager fetches
		/// the next driver description in the list (SQL_FETCH_NEXT) or whether the 
		/// search starts from the beginning of the list (SQL_FETCH_FIRST).
		/// </param>
		/// <param name="szDriverDesc">[Output]Pointer to a buffer in which to return the
		/// driver description.
		/// </param>
		/// <param name="cbDriverDescMax">[Input]Length of the *DriverDescription buffer,
		/// in bytes.
		/// </param>
		/// <param name="pcbDriverDesc">[Output]Pointer to a buffer in which to return 
		/// the total number of bytes (excluding the null-termination byte) available to 
		/// return in *DriverDescription. If the number of bytes available to return is 
		/// greater than or equal to BufferLength1, the driver description in 
		/// *DriverDescription is truncated to BufferLength1 minus the length of a 
		/// null-termination character.
		/// </param>
		/// <param name="szDriverAttributes">[Output]Pointer to a buffer in which to 
		/// return the list of driver attribute value pairs.
		/// </param>
		/// <param name="cbDrvrAttrMax">[Input]Length of the *DriverAttributes buffer, in bytes.
		/// If the *DriverDescription value is a Unicode string (when calling SQLDriversW)
		/// , the BufferLength argument must be an even number.
		/// </param>
		/// <param name="pcbDrvrAttr">[Output]Pointer to a buffer in which to return the 
		/// total number of bytes (excluding the null-termination byte) available to 
		/// return in *DriverAttributes. If the number of bytes available to return is 
		/// greater than or equal to BufferLength2, the list of attribute value pairs in 
		/// *DriverAttributes is truncated to BufferLength2 minus the length of the 
		/// null-termination character.</param>
		/// <returns>SQL_SUCCESS, SQL_SUCCESS_WITH_INFO, SQL_NO_DATA, SQL_ERROR, or 
		/// SQL_INVALID_HANDLE.</returns>
		[DllImport("odbc32.dll")]
		public static extern int SQLDrivers(int hEnv,int fDirection,System.Text.StringBuilder szDriverDesc,int cbDriverDescMax,ref int pcbDriverDesc,System.Text.StringBuilder szDriverAttributes,int cbDrvrAttrMax,ref int pcbDrvrAttr);
		
		/// <summary>
		/// GetFileVersionInfoSize determines the length of the version information 
		/// resource stored with a 32-bit executable-type file.
		/// </summary>
		/// <param name="lptstrFilename">The full path and filename of the file.</param>
		/// <param name="lpdwHandle">Receives a value of 0. Although this parameter is 
		/// effectively reserved, you must pass a variable to receive this meaningless 
		/// value.
		/// </param>
		/// <returns>If successful, the function returns the size in bytes of the file's 
		/// version information resource. If an error occurred, the function returns 0 
		/// (use GetLastError to get the error code). 
		/// </returns>
		[DllImport("Version.dll")]
		public static extern int GetFileVersionInfoSize(string lptstrFilename,ref int lpdwHandle);
		
		/// <summary>
		/// GetFileVersionInfo extracts the version information resource stored inside a 
		/// 32-bit executable-type file. 
		/// </summary>
		/// <param name="lptstrFilename">The full path and filename of the file.</param>
		/// <param name="lpdwHandle">Reserved -- set to 0.</param>
		/// <param name="dwlen">The size in bytes of the buffer passed as lpData.</param>
		/// <param name="lpData">A buffer, such as a byte array, that receives the 
		/// version information resource of the file.
		/// </param>
		/// <returns>If successful, the function returns a non-zero value. If an error 
		/// occurred, the function returns 0 (use GetLastError to get the error code).
		/// </returns>
		[DllImport("Version.dll")]
		public static extern int GetFileVersionInfo(string lptstrFilename,string lpdwHandle,int dwlen,ref byte lpData);
		
		/// <summary>
		/// VerQueryValue extracts information from a version information resource. This 
		/// resource stores version-related information about a 32-bit executable-type 
		/// file.
		/// </summary>
		/// <param name="pBlock">The byte array or similar object that holds the version 
		/// information resource extracted from a file. To obtain such a block, use 
		/// GetFileVersionInfo.
		/// </param>
		/// <param name="lpSubBlock">One of the following strings specifying what 
		/// information to extract from the resource. A pointer to the extracted 
		/// information is placed into lplpBuffer.
		/// </param>
		/// <param name="lplpBuffer">Receives a pointer to the data extracted from the 
		/// version information resource. The memory referenced by this pointer is 
		/// automatically freed when the memory used by pBlock is freed.
		/// </param>
		/// <param name="lpData">Pointer to a buffer that receives the length, in 
		/// characters, of the version-information value.
		/// </param>
		/// <returns>If successful, the function returns a non-zero value. If an error 
		/// occurred, the function returns 0.
		/// </returns>
		[DllImport("Version.dll")]
		public static extern int VerQueryValue(ref byte pBlock,string lpSubBlock,ref int lplpBuffer,ref int lpData);
		
		/// <summary>
		/// MoveMemory moves the contents of a portion of memory from one location to 
		/// another. The two locations are identified by pointers to the memory addresses.
		/// After the copy, the original contents in the source are set to zeros. 
		/// </summary>
		/// <param name="dest">A pointer to the memory address to use as the target, 
		/// which receives the transfered data.
		/// </param>
		/// <param name="Source">A pointer to the memory address to use as the source, 
		/// which initially holds the data to be transfered.
		/// </param>
		/// <param name="Length">The number of bytes of data to copy from the source 
		/// memory location to the target memory location.
		/// </param>
		/// <returns>MoveMemory does not return a value.</returns>
		[DllImport("kernel32",EntryPoint="MoveMemory")]
		public static extern void RtlMoveMemory(ref byte dest,int Source,int Length);
		
		/// <summary>
		/// lstrcpy copies the entire contents of one string into another string.
		/// </summary>
		/// <param name="lpString1">String that receives the copied contents of lpString2.
		/// This could be either the string itself or a pointer to the string.
		/// </param>
		/// <param name="lpString2">Either an actual string to copy into lpString1 or a 
		/// pointer to the string to copy into lpString1.
		/// </param>
		/// <returns>If an error occurred, the function returns 0 (use GetLastError to get 
		/// the error code). If successful, the function returns a non-zero value.
		/// </returns>
		[DllImport("kernel32")]
		public static extern int lstrcpy(System.Text.StringBuilder lpString1,int lpString2);

		
		/// <summary>
		/// RegOpenKeyEx opens a key in the Windows registry.
		/// </summary>
		/// <param name="hKey">[in]Handle to an open key.</param>
		/// <param name="lpSubKey">[in]Pointer to a null-terminated string containing 
		/// the name of the subkey to open.
		/// </param>
		/// <param name="ulOptions">Reserved; must be zero.</param>
		/// <param name="samDesired">[in]Access mask that specifies the desired access 
		/// rights to the key.
		/// </param>
		/// <param name="phkResult">[out]Pointer to a variable that receives a handle to 
		/// the opened key. When you no longer need the returned handle, call the 
		/// RegCloseKey function to close it. 
		/// </param>
		/// <returns>The function returns 0 if successful, or a non-zero value error code 
		/// if an error occurred.
		/// </returns>
		[DllImport("advapi32.dll", EntryPoint="RegOpenKeyEx")] 
		public static extern int RegOpenKeyExA(uint hKey, string lpSubKey, int ulOptions, int samDesired, ref int phkResult); 		
		
		/// <summary>
		/// RegQueryValueEx reads a value from a registry key.
		/// </summary>
		/// <param name="hKey">[in]Handle to an open key.</param>
		/// <param name="lpValueName">[in]Pointer to a null-terminated string containing 
		/// the name of the value to query.
		/// </param>
		/// <param name="lpReserved">Reserved; must be NULL.</param>
		/// <param name="lpType">[out] Pointer to a variable that receives a code 
		/// indicating the type of data stored in the specified value. For a list of the 
		/// possible type codes,Example REG_SZ, REG_DWORD etc.
		/// </param>
		/// <param name="lpData">[out] Pointer to a buffer that receives the value's data.
		/// This parameter can be NULL if the data is not required.
		/// </param>
		/// <param name="lpcbData">[in, out] Pointer to a variable that specifies the 
		/// size of the buffer pointed to by the lpData parameter, in bytes. When the
		/// function returns, this variable contains the size of the data copied to 
		/// lpData. 
		/// </param>
		/// <returns>If an error occued, the function returns a non-zero error code 
		/// defined in Winerror.h.If successful, the function returns 0.
		/// </returns>
		[DllImport("advapi32.dll",EntryPoint="RegQueryValueExA")]
		public static extern int RegQueryValueExNULL(int hKey,string lpValueName,int lpReserved,ref int lpType,int lpData,ref int lpcbData);

		/// <summary>
		/// RegQueryValueEx reads a value from a registry key.
		/// </summary>
		/// <param name="hKey">[in]Handle to an open key.</param>
		/// <param name="lpValueName">[in]Pointer to a null-terminated string containing 
		/// the name of the value to query.
		/// </param>
		/// <param name="lpReserved">Reserved; must be NULL.</param>
		/// <param name="lpType">[out] Pointer to a variable that receives a code 
		/// indicating the type of data stored in the specified value. For a list of the 
		/// possible type codes,Example REG_SZ, REG_DWORD etc.
		/// </param>
		/// <param name="lpData">[out] Pointer to a buffer that receives the value's data.
		/// This parameter can be NULL if the data is not required.
		/// </param>
		/// <param name="lpcbData">[in, out] Pointer to a variable that specifies the 
		/// size of the buffer pointed to by the lpData parameter, in bytes. When the
		/// function returns, this variable contains the size of the data copied to 
		/// lpData. 
		/// </param>
		/// <returns>If an error occued, the function returns a non-zero error code 
		/// defined in Winerror.h.If successful, the function returns 0.
		/// </returns>
		[DllImport("advapi32.dll",EntryPoint="RegQueryValueExA")]
		public static extern int RegQueryValueExString(int hKey,string lpValueName,string lpReserved,ref int lpType,System.Text.StringBuilder lpData,ref int lpcbData);
		
		/// <summary>
		/// RegQueryValueEx reads a value from a registry key.
		/// </summary>
		/// <param name="hKey">[in]Handle to an open key.</param>
		/// <param name="lpValueName">[in]Pointer to a null-terminated string containing 
		/// the name of the value to query.
		/// </param>
		/// <param name="lpReserved">Reserved; must be NULL.</param>
		/// <param name="lpType">[out] Pointer to a variable that receives a code 
		/// indicating the type of data stored in the specified value. For a list of the 
		/// possible type codes,Example REG_SZ, REG_DWORD etc.
		/// </param>
		/// <param name="lpData">[out] Pointer to a buffer that receives the value's data.
		/// This parameter can be NULL if the data is not required.
		/// </param>
		/// <param name="lpcbData">[in, out] Pointer to a variable that specifies the 
		/// size of the buffer pointed to by the lpData parameter, in bytes. When the
		/// function returns, this variable contains the size of the data copied to 
		/// lpData. 
		/// </param>
		/// <returns>If an error occued, the function returns a non-zero error code 
		/// defined in Winerror.h.If successful, the function returns 0.
		/// </returns>
		[DllImport("advapi32.dll",EntryPoint="RegQueryValueExA")]
		public static extern int RegQueryValueExLong(int hKey,string lpValueName,string lpReserved,int lpType,int lpData,int lpcbData);
		
		/// <summary>
		/// RegCloseKey closes a registry key that had previously been opened with 
		/// RegCreateKeyEx or RegOpenKeyEx.
		/// </summary>
		/// <param name="hKey">A handle to the registry key to close.</param>
		/// <returns>If successful, the function returns zero. If an error occurred, the 
		/// function returns a non-zero error code.
		/// </returns>
		[DllImport("advapi32.dll")]
		public static extern int RegCloseKey(int hKey);

		/// <summary>
		/// LogonUser attempts to perform a windows logon with the supplied user credentials.
		/// </summary>
		/// <param name="lpszUsername">Logon User Name</param>
		/// <param name="lpszDomain">Logon Domain</param>
		/// <param name="lpszPassword">Logon Password</param>
		/// <param name="dwLogonType">Interactive\Batch\Etc.</param>
		/// <param name="dwLogonProvider">Default provider - currently the only valid value.</param>
		/// <param name="phToken">An IntPtr handle to the userToken (if successfull).</param>
		/// <returns>If successful, the function returns zero. If an error occurred, the 
		/// function returns a non-zero error code.
		/// </returns>
		[DllImport("advapi32.dll", SetLastError=true)]
		public static extern bool LogonUser(
			string lpszUsername, 
			string lpszDomain, 
			string lpszPassword, 
			int dwLogonType, 
			int dwLogonProvider, 
			out IntPtr phToken
			);
	}

	/// <summary>
	/// Riskmaster.Common.Win32.eMemMapRights supports the shared Memory Map section used by Login.</summary>
	public enum eMemMapRights:int 
	{
			
		/// <summary>
		/// Riskmaster.Common.Win32.eMemMapRights.PAGE_READONLY.</summary>
		PAGE_READONLY    =      0x02,     
			
		/// <summary>
		/// Riskmaster.Common.Win32.eMemMapRights.PAGE_READWRITE.</summary>
		PAGE_READWRITE    =     0x04     
	}
	
	/// <summary>
	/// Riskmaster.Common.Win32.eMemViewRights.</summary>
    
	public enum eMemViewRights:int 
	{
		
		/// <summary>
		///Riskmaster.Common.Win32.eMemViewRights.FILE_MAP_WRITE.</summary>
		FILE_MAP_WRITE    =      0x0002,     
	}
	//  Cannot create a file when that file already exists.
	//
	
	/// <summary>
	/// Riskmaster.Common.Win32.Const.</summary>
	public class Const
	{
		/// <summary>Riskmaster.Common.Win32.Const.FILE_ALREADY_EXISTS.</summary>
		public  const int ERROR_ALREADY_EXISTS = 183;
	
		/// <summary>
		/// Direction parameter, used in SQLDatasources() and SQLDrivers() API.
		/// </summary>
		public const int SQL_FETCH_NEXT = 1;

		/// <summary>
		/// Direction parameter, used in SQLDatasources() and SQLDrivers() API.
		/// </summary>
		public const int SQL_FETCH_FIRST = 2;

		/// <summary>
		/// Represents the success condition while loading the ODBC Drivers and 
		/// datasources.
		/// </summary>
		public const int SQL_SUCCESS = 0;
	
		/// <summary>
		/// Null-terminated string. It will be a Unicode or ANSI string, depending on 
		/// whether you use the Unicode or ANSI functions.
		/// </summary>
		public const int REG_SZ = 1;

		/// <summary>
		/// The HKEY_LOCAL_MACHINE\Enum key contains subkeys for the specific hardware 
		/// components your computer uses. 
		/// </summary>
		public const uint HKEY_LOCAL_MACHINE = 0x80000002;

		/// <summary>
		/// Access mask that specifies the desired access rights to the key.Used in 
		/// RegOpenKeyEx API.
		/// </summary>
		public const int KEY_ALL_ACCESS = 0x3F;

		/// <summary>
		/// A 32-bit number.
		/// </summary>
		public const int REG_DWORD = 4;

		/// <summary>
		/// Used when API gives no error.
		/// </summary>
		public const int ERROR_NONE = 0;
	}
	public enum LogonType : int
	{
		/// <summary>
		/// This logon type is intended for users who will be interactively using the computer, such as a user being logged on  
		/// by a terminal server, remote shell, or similar process.
		/// This logon type has the additional expense of caching logon information for disconnected operations; 
		/// therefore, it is inappropriate for some client/server applications,
		/// such as a mail server.
		/// </summary>
		LOGON32_LOGON_INTERACTIVE = 2,

		/// <summary>
		/// This logon type is intended for high performance servers to authenticate plaintext passwords.
		/// The LogonUser function does not cache credentials for this logon type.
		/// </summary>
		LOGON32_LOGON_NETWORK = 3,

		/// <summary>
		/// This logon type is intended for batch servers, where processes may be executing on behalf of a user without 
		/// their direct intervention. This type is also for higher performance servers that process many plaintext
		/// authentication attempts at a time, such as mail or Web servers. 
		/// The LogonUser function does not cache credentials for this logon type.
		/// </summary>
		LOGON32_LOGON_BATCH = 4,

		/// <summary>
		/// Indicates a service-type logon. The account provided must have the service privilege enabled. 
		/// </summary>
		LOGON32_LOGON_SERVICE = 5,

		/// <summary>
		/// This logon type is for GINA DLLs that log on users who will be interactively using the computer. 
		/// This logon type can generate a unique audit record that shows when the workstation was unlocked. 
		/// </summary>
		LOGON32_LOGON_UNLOCK = 7,

		/// <summary>
		/// This logon type preserves the name and password in the authentication package, which allows the server to make 
		/// connections to other network servers while impersonating the client. A server can accept plaintext credentials 
		/// from a client, call LogonUser, verify that the user can access the system across the network, and still 
		/// communicate with other servers.
		/// NOTE: Windows NT:  This value is not supported. 
		/// </summary>
		LOGON32_LOGON_NETWORK_CLEARTEXT = 8,

		/// <summary>
		/// This logon type allows the caller to clone its current token and specify new credentials for outbound connections.
		/// The new logon session has the same local identifier but uses different credentials for other network connections. 
		/// NOTE: This logon type is supported only by the LOGON32_PROVIDER_WINNT50 logon provider.
		/// NOTE: Windows NT:  This value is not supported. 
		/// </summary>
		LOGON32_LOGON_NEW_CREDENTIALS = 9,
	}

	public enum LogonProvider : int
	{
		/// <summary>
		/// Use the standard logon provider for the system. 
		/// The default security provider is negotiate, unless you pass NULL for the domain name and the user name 
		/// is not in UPN format. In this case, the default provider is NTLM. 
		/// NOTE: Windows 2000/NT:   The default security provider is NTLM.
		/// </summary>
		LOGON32_PROVIDER_DEFAULT = 0,
	}
	
	/// <summary>
	/// Riskmaster.Common.Win32.hWnd wraps a Window Handle.</summary>
	/// <remarks>none</remarks>
	public class hWnd : System.Windows.Forms.IWin32Window
	{
		IntPtr hwnd;
		
		/// <summary>
		/// Constructor of Riskmaster.Common.Win32.hWnd.hWnd accepts an IntPtr representing the Window Handle.</summary>
		/// <param name="handle">The IntPtr handle to wrap.</param>
		/// <remarks>.Net defaults handles to IntPtr.</remarks>
		public hWnd(IntPtr handle) { hwnd = handle; }
		
		/// <summary>
		/// Constructor Riskmaster.Common.Win32.hWnd.hWnd accepts an int representing the Window Handle.</summary>
		/// <param name="handle">The integer representing the window handle to wrap.</param>
		/// <remarks>Win32 defaults handles to raw integers.</remarks>
		public hWnd(int handle) { hwnd = new IntPtr(handle); }

		
		/// <summary>
		/// Riskmaster.Common.Win32.hWnd.Handle.</summary>
		/// <returns>IntPtr</returns>
		/// <remarks>Always returns a fully wrapped handle, regardless of the constructor used.</remarks>
		public IntPtr Handle { get { return hwnd; } }
	}
	// Example of HowTo Use this Wrapper.
	//hWnd hwnd = new hWnd( (IntPtr)YourIntHandle );
}
