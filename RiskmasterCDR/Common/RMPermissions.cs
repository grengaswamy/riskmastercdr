using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Principal;


namespace Riskmaster.Common
{
    /// <summary>
    /// Class to control permissions for user actions controlled via Security Management System
    /// </summary>
	public class RMPermissions
	{
        #region Public Constants
        // RM access type permissions
        public const int RMO_ACCESS = 0;
        public const int RMO_VIEW = 1;
        public const int RMO_UPDATE = 2;
        public const int RMO_CREATE = 3;
        public const int RMO_DELETE = 4;
        public const int RMO_DIARY_ROUTE = 5;
        public const int RMO_DIARY_ROLL = 6;
        public const int RMO_DIARY_COMPLETE = 7;
        public const int RMO_DIARY_PEEK = 8;
        public const int RMO_DIARY_VOID = 9;
        public const int RMO_DIARY_HISTORY = 10;
        public const int RMO_ALLOW_PAYMENT_CLOSED_CLM = 11;
        public const int RMO_ALLOW_CHANGE_CHECK_NUMBER = 12;
        public const int RMO_FUNDS_PRINTCHK = 13;
        public const int RMO_PRINT_CHK = 14;
        public const int RMO_IMPORT = 15;
        public const int RMO_PAYMENT_HISTORY = 16;
        public const int RMO_FUNDS_APPPAYCOV = 17;
        public const int RMO_FUNDS_CLEARCHK = 18;
        public const int RMO_FUNDS_VOID_PRINTED_CHECK = 19;
        public const int RMO_USERDOC_COPY = 20;
        public const int RMO_USERDOC_TRANSFER = 21;
        public const int RMO_ATTACHMENT = 22;
        public const int RMO_FUNDS_SUP_APPROVE_PAYMENTS = 23;
        //Added by Nitesh 09Feb2006 For Lookup security starts
        public const int RMO_CLAIM_LOOKUP = 24;
        public const int RMO_EVENT_LOOKUP = 25;
        public const int RMO_EMPLOYEE_LOOKUP = 26;
        public const int RMO_ENTITY_LOOKUP = 27;
        public const int RMO_VEHICLE_LOOKUP = 28;
        public const int RMO_POLICY_LOOKUP = 29;
        public const int RMO_PAYMENT_LOOKUP = 30;
        public const int RMO_PATIENT_LOOKUP = 31;
        public const int RMO_PHYSICIAN_LOOKUP = 32;
        public const int RMO_MED_STAFF_LOOKUP = 33;
        public const int RMO_DIS_PLAN_LOOKUP = 34;
        public const int RMO_AT_LOOKUP = 35;
        public const int RMO_BANK_ACCT_LOOKUP = 36;
        public const int RMO_BANK_ACCT_DEP_LOOKUP = 37;
        //Added by Nitesh 09Feb2006 For Lookup security Ends
        //Begin:Added by Mohit Yadav 09Feb2006 For Search security
        public const int RMO_SEARCH_EVENT = 38;
        public const int RMO_SEARCH_CLAIM = 39;
        public const int RMO_SEARCH_EMPLOYEE = 40;
        public const int RMO_SEARCH_PEOPLE_ENTITY = 41;
        //Arnab: MITS-10998 - Added two new Code for Other people and Witness
        public const int RMO_SEARCH_PEOPLE_OTHER_PERSON = 82;
        public const int RMO_SEARCH_PEOPLE_WITNESS = 83;
        //MITS-10998 End
        public const int RMO_SEARCH_VEHICLE = 42;
        public const int RMO_SEARCH_POLICY = 43;
        public const int RMO_SEARCH_PAYMENT = 44;
        public const int RMO_SEARCH_PATIENT = 45;
        public const int RMO_SEARCH_PHYSICIAN = 46;
        public const int RMO_SEARCH_MED_STAFF = 47;
        public const int RMO_SEARCH_DISABILITY_PLAN = 48;
        public const int RMO_SEARCH_ADMIN_TRACKING = 49;
        public const int RMO_SEARCH_ENH_POLICY = 50;
        public const int RMO_SEARCH_ENH_POLICY_BILLING = 51;
        //Shruti Leave Plan Search
        public const int RMO_SEARCH_LEAVE_PLAN = 70;
        public const int RMO_SEARCH = 52;
        //End:Added by Mohit Yadav 09Feb2006 For Search security

        //skhare7 Diary search
        public const int RMO_SEARCH_DIARY = 8000024;
        //end skhare7
        //Amandeep Driver Search Start
        public const int RMO_SEARCH_DRIVER = 8000025;
        //Amandeep End
        //Begin:Added by Mohit Yadav 22Feb2006 For Claim Notes security
        public const int RMO_ClaimNotes_Access_GC = 53;
        public const int RMO_ClaimNotes_Access_WC = 54;
        public const int RMO_ClaimNotes_Access_VA = 55;
        public const int RMO_ClaimNotes_Access_DI = 56;
        public const int RMO_EventNotes_Access_Event = 57;
        //End:Added by Mohit Yadav 22Feb2006 For Claim Notes security
        //Begin:Added by Mohit Yadav 24Feb2006 For User Document / Attachments security
        public const int RMO_ATT_VIEWPLAYPRINT = 58;		//Ref:RMO_ATTACHMENT=22
        public const int RMO_ATT_ATTACHTO = 59;				//Ref:RMO_ATTACHMENT=22
        public const int RMO_ATT_DETACHFROM = 60;			//Ref:RMO_ATTACHMENT=22
        public const int RMO_ATT_EXPORT = 61;				//Ref:RMO_ATTACHMENT=22
        public const int RMO_UD_VIEWPLAY = 62;
        public const int RMO_UD_PRINT = 63;
        public const int RMO_UD_DELETE = 64;
        //Change by kuladeep for mits:33349 Start
        //public const int RMO_SEARCH_PROPERTY = 65;
        public const int RMO_SEARCH_PROPERTY = 85;
        //Change by kuladeep for mits:33349 End
        //End:Added by Mohit Yadav 24Feb2006 For User Document / Attachments security

        //Defect #2410 - 03/06/2006 - Anurag - Check for permissions to save without claim attached,
        public const int RMO_ALLOW_MANUAL_CHECKS = 65;
        //Nitesh 04/24/2006:Tr-2311 Starts
        public const int RMO_DISPLAY_QUICK_SUMMARY = 66;
        //Nitesh 04/24/2006:Tr-2311 Ends
        //Shruti MITS-7949 starts
        public const int RMO_NoProgressNotes = 67;
        public const int RMO_NoProgressNotesCreate = 68;
        public const int RMO_NoProgressNotesPrint = 69;
        //Shruti MITS-7949 ends
        //Abhishek 14/03/08 : MITS 11403
        public const int RMO_NOPROGRESS_NOTES_DELETE = 84;
        //Geeta 08/23/07 : MITS 10221
        public const int RMO_NoProgressNotesEdit = 81;

        //Added by Gagan for Enhanced Policy Billing : Start
        public const int RMO_BATCHES_CLOSE = 71;
        public const int RMO_BATCHES_REVERSE = 72;
        public const int RMO_BATCHES_ABORT = 73;

        public const int RMO_BILLING_RECEIPTS = 74;
        public const int RMO_BILLING_ADJUSTMENTS = 75;
        public const int RMO_BILLING_DISBURSEMENTS = 76;

        public const int RMO_BILLING_SELECT = 77;
        public const int RMO_BILLING_UNSELECT = 78;
        public const int RMO_BILLING_RECOCNILE = 79;
        public const int RMO_BILLING_PRINT_DISBURSEMENTS = 80;

        //Added by Gagan for Enhanced Policy Billing : End
        public const int RMO_FUNDS_VOIDCHK = 750;//Added by Shivendu for MITS 18518
        public const int RMO_FUNDS_TRANSACT = 150;
        public const int RMO_FUNDS_BNKACCT = 300;
        public const int RMO_FUNDS_AUTOCHK = 900;
        public const int RMO_FUNDS_DEPOSIT = 1150;

        public const int RMO_EMPLOYEE = 14500;
        public const int RMO_EMPLOYEE_VIEW_SSN = 5;

        //placeholder for Claim access
        //TODO: This value needs to be capitalized to coincide with other constant definitions
        public const int RMO_ClaimNotes_Access_PC = 0;

      //skhare7 MITS 18069
        public const int RMO_WC_MMSEA = 1900004;
        public const int RMO_GC_MMSEA = 1900000;
        public const int RMO_VA_MMSEA = 1900008;
        //skhare7 MITS 18069 end

        //nsachdeva2: Mits: 26418
        public const int RMO_ClaimActLog_Access_GC = 93;
        public const int RMO_ClaimActLog_Access_WC = 94;
        public const int RMO_ClaimActLog_Access_VA = 95;
        public const int RMO_ClaimActLog_Access_DI = 96;
        public const int RMO_ClaimActLog_Access_PC = 97;
        //nsachdeva2: Mits: 26418 end
        //Bharani - MITS : 31858 - Start
        public const int RMO_GC_ADJUSTERDATEDTEXT = 1200;
        public const int RMO_WC_ADJUSTERDATEDTEXT = 4050;
        public const int RMO_VA_ADJUSTERDATEDTEXT = 7050;
        public const int RMO_PC_ADJUSTERDATEDTEXT = 41200;
        public const int RMO_DI_ADJUSTERDATEDTEXT = 61050;        
        //Bharani - MITS : 31858 - End
        #endregion
       
		/// <summary>
		/// Generates a permission error message for a single permission violation. Use when
		/// checking for individual "special" permissions like whether or not to allow Claim Closure or not.
		/// </summary>
		/// <param name="permissionID">Absolute id (function id) of permission being checked.</param>
		/// <returns>String message to display to user regarding permission violation.</returns>
		static public string FormatPermissionError(int permissionID, int p_iClientId)
		{
            return FormatPermissionError(-1, permissionID, p_iClientId);
		}

		/// <summary>
		/// Generates a permission error message for a single permission violation. Use when
		/// checking for individual standard permissions on modules like whether or not to allow Update on a Claim Closure or not.
		/// </summary>
		/// <param name="accessType">The type of access that was requested in the permission check. Can be RMO_ACCESS (0), RMO_VIEW(1), etc.</param>
		/// <param name="module">The security module id that was requested in the permission check.</param>
		/// <returns>String message to display to user regarding permission violation.</returns>
		///* Amendment  -->
		///  1.Date		: 7 Feb 06 
		///    Desc		: Adding RMO_ALLOW_CHANGE_CHECK_NUMBER,RMO_FUNDS_PRINTCHK
		///				  for decoding 
		///				  string messages.
		///    Author	: Sumit
		///**************************************************************

		static public string FormatPermissionError(int accessType, int module, int p_iClientId)
		{
			string sPermission="";

			switch(accessType)
			{
				case RMO_ACCESS:
                    sPermission = Globalization.GetString("Permission.NoAccess", p_iClientId);
					break;
				case RMO_VIEW:
                    sPermission = Globalization.GetString("Permission.NoView", p_iClientId);
					break;
				case RMO_UPDATE:
                    sPermission = Globalization.GetString("Permission.NoUpdate", p_iClientId);
					break;
				case RMO_CREATE:
                    sPermission = Globalization.GetString("Permission.NoCreate", p_iClientId);
					break;
				case RMO_DELETE:
                    sPermission = Globalization.GetString("Permission.NoDelete", p_iClientId);
					break;
				case RMO_DIARY_ROUTE:
                    sPermission = Globalization.GetString("Permission.NoDiaryRoute", p_iClientId);
					break;
				case RMO_DIARY_ROLL:
                    sPermission = Globalization.GetString("Permission.NoDiaryRoll", p_iClientId);
					break;
				case RMO_DIARY_COMPLETE:
                    sPermission = Globalization.GetString("Permission.NoDiaryComplete", p_iClientId);
					break;
				case RMO_DIARY_PEEK:
                    sPermission = Globalization.GetString("Permission.NoDiaryPeek", p_iClientId);
					break;
				case RMO_DIARY_VOID:
                    sPermission = Globalization.GetString("Permission.NoDiaryVoid", p_iClientId);
					break;
				case RMO_DIARY_HISTORY:
                    sPermission = Globalization.GetString("Permission.NoDiaryHistory", p_iClientId);
					break;
				case RMO_ALLOW_PAYMENT_CLOSED_CLM:
                    sPermission = Globalization.GetString("Permission.AllowPaymentClosedClm", p_iClientId);
					break;
				case RMO_ALLOW_CHANGE_CHECK_NUMBER:
                    sPermission = Globalization.GetString("Permission.AllowChangeCheckNumber", p_iClientId);
					break;
				case RMO_FUNDS_PRINTCHK:
                    sPermission = Globalization.GetString("Permission.NoFundsPrintCheck", p_iClientId);
					break;
				case RMO_PRINT_CHK:
                    sPermission = Globalization.GetString("Permission.NoPrintCheckPermissions", p_iClientId);
					break;
				case RMO_IMPORT:
                    sPermission = Globalization.GetString("Permission.NoImportPermission", p_iClientId);
					break;
				case RMO_PAYMENT_HISTORY:
                    sPermission = Globalization.GetString("Permission.NoPaymentHistory", p_iClientId);
					break;
				case RMO_FUNDS_APPPAYCOV:
                    sPermission = Globalization.GetString("Permission.NoAppPayCov", p_iClientId);
					break;
				case RMO_FUNDS_CLEARCHK:
                    sPermission = Globalization.GetString("Permission.NoClearCheck", p_iClientId);
					break;
				case RMO_FUNDS_VOID_PRINTED_CHECK:
                    sPermission = Globalization.GetString("Permission.NoVoidPrintedCheck", p_iClientId);
					break;
				case RMO_USERDOC_COPY:
                    sPermission = Globalization.GetString("Permission.NoUserDocCopy", p_iClientId);
					break;
				case RMO_USERDOC_TRANSFER:
                    sPermission = Globalization.GetString("Permission.NoUserDocTransfer", p_iClientId);
					break;
				case RMO_ATTACHMENT:
                    sPermission = Globalization.GetString("Permission.NoAttachment", p_iClientId);
					break;
				case RMO_FUNDS_SUP_APPROVE_PAYMENTS:
                    sPermission = Globalization.GetString("Permission.NoSuperVisoryApprovalPaymnt", p_iClientId);
					break;
					//Added by Nitesh 09Feb2006 For Lookup security starts
				case RMO_CLAIM_LOOKUP:
                    sPermission = Globalization.GetString("Permission.NoClaimLookup", p_iClientId);
					break;
				case RMO_EVENT_LOOKUP:
                    sPermission = Globalization.GetString("Permission.NoEventLookup", p_iClientId);
					break;
				case RMO_EMPLOYEE_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoEmployeeLookup", p_iClientId);
					break;
				case RMO_ENTITY_LOOKUP:
                    sPermission = Globalization.GetString("Permission.NoEntityLookup", p_iClientId);
					break;
				case RMO_VEHICLE_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoVehicleLookup", p_iClientId);
					break;
				case RMO_POLICY_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoPolicyLookup", p_iClientId);
					break;
				case RMO_PAYMENT_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoPaymentLookup", p_iClientId);
					break;
				case RMO_PATIENT_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoPatientLookup", p_iClientId);
					break;
				case RMO_PHYSICIAN_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoPhysicianLookup", p_iClientId);
					break;
				case RMO_MED_STAFF_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoMedStaffLookup", p_iClientId);
					break;
				case RMO_DIS_PLAN_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoDisPlanLookup", p_iClientId);
					break;
				case RMO_AT_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoATLookup", p_iClientId);
					break;
				case RMO_BANK_ACCT_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoBankAcctLookup", p_iClientId);
					break;
				case RMO_BANK_ACCT_DEP_LOOKUP :
                    sPermission = Globalization.GetString("Permission.NoBankAcctDepLookup", p_iClientId);
					break;
					//Added by Nitesh 09Feb2006 For Lookup security Ends
					#region "Search security"
					//Begin:Added by Mohit Yadav 09Feb2006 For Search security
				case RMO_SEARCH_EVENT:
                    sPermission = Globalization.GetString("Permission.NoEventSearch", p_iClientId);
					break;
				case RMO_SEARCH_CLAIM:
                    sPermission = Globalization.GetString("Permission.NoClaimSearch", p_iClientId);
					break;
				case RMO_SEARCH_EMPLOYEE:
                    sPermission = Globalization.GetString("Permission.NoEmployeeSearch", p_iClientId);
					break;
				case RMO_SEARCH_PEOPLE_ENTITY:
                    sPermission = Globalization.GetString("Permission.NoPeopleEntitySearch", p_iClientId);
					break;
                //MITS-10998 - Added two cases for Other Person and Witness                
                case RMO_SEARCH_PEOPLE_OTHER_PERSON:
                    sPermission = Globalization.GetString("Permission.NoOtherPersonSearch", p_iClientId);
                    break;
                case RMO_SEARCH_PEOPLE_WITNESS:
                    sPermission = Globalization.GetString("Permission.NoWitnessSearch", p_iClientId);
                    break;
                //MITS-10998 End
				case RMO_SEARCH_VEHICLE:
                    sPermission = Globalization.GetString("Permission.NoVehicleSearch", p_iClientId);
					break;
				case RMO_SEARCH_POLICY:
                    sPermission = Globalization.GetString("Permission.NoPolicySearch", p_iClientId);
					break;
				case RMO_SEARCH_PAYMENT:
                    sPermission = Globalization.GetString("Permission.NoPaymentSearch", p_iClientId);
					break;
				case RMO_SEARCH_PATIENT:
                    sPermission = Globalization.GetString("Permission.NoPatientSearch", p_iClientId);
					break;
				case RMO_SEARCH_PHYSICIAN:
                    sPermission = Globalization.GetString("Permission.NoPhysicianSearch", p_iClientId);
					break;
				case RMO_SEARCH_MED_STAFF:
                    sPermission = Globalization.GetString("Permission.NoMedStaffSearch", p_iClientId);
					break;
				case RMO_SEARCH_DISABILITY_PLAN:
                    sPermission = Globalization.GetString("Permission.NoDisPlanSearch", p_iClientId);
					break;
				case RMO_SEARCH_ADMIN_TRACKING:
                    sPermission = Globalization.GetString("Permission.NoATSearch", p_iClientId);
					break;
				case RMO_SEARCH_ENH_POLICY:
                    sPermission = Globalization.GetString("Permission.NoEPSearch", p_iClientId);
					break;
				case RMO_SEARCH_ENH_POLICY_BILLING:
                    sPermission = Globalization.GetString("Permission.NoEPBSearch", p_iClientId);
					break;
                //Shruti Leave Plan Search starts
                case RMO_SEARCH_LEAVE_PLAN:
                    sPermission = Globalization.GetString("Permission.NoLPSearch", p_iClientId);
					break;
                //Shruti Leave Plan Search ends
                    //skhare7
                case RMO_SEARCH_DIARY:
                    sPermission = Globalization.GetString("Permission.NoDSearch", p_iClientId);
                    break;
				case RMO_SEARCH:
                    sPermission = Globalization.GetString("Permission.NoSearch", p_iClientId);
					break;
					//End:Added by Mohit Yadav 09Feb2006 For Search security
					#endregion
				case RMO_ClaimNotes_Access_GC:
                    sPermission = Globalization.GetString("Permission.NoGCClaimNotes", p_iClientId);
					break;
				case RMO_ClaimNotes_Access_WC:
                    sPermission = Globalization.GetString("Permission.NoWCClaimNotes", p_iClientId);
					break;
				case RMO_ClaimNotes_Access_VA:
                    sPermission = Globalization.GetString("Permission.NoVAClaimNotes", p_iClientId);
					break;
				case RMO_ClaimNotes_Access_DI:
                    sPermission = Globalization.GetString("Permission.NoDIClaimNotes", p_iClientId);
					break;
				case RMO_EventNotes_Access_Event:
                    sPermission = Globalization.GetString("Permission.NoEventNotes", p_iClientId);
					break;
				case RMO_ATT_VIEWPLAYPRINT:
                    sPermission = Globalization.GetString("Permission.NoAttachmentView", p_iClientId);
					break;
				case RMO_ATT_ATTACHTO:
                    sPermission = Globalization.GetString("Permission.NoAttachmentTo", p_iClientId);
					break;
				case RMO_ATT_DETACHFROM:
                    sPermission = Globalization.GetString("Permission.NoDetachFrom", p_iClientId);
					break;
				case RMO_UD_VIEWPLAY:
                    sPermission = Globalization.GetString("Permission.NoDocumentView", p_iClientId);
					break;
				case RMO_UD_PRINT:
                    sPermission = Globalization.GetString("Permission.NoPrintView", p_iClientId);
					break;
				case RMO_UD_DELETE:
                    sPermission = Globalization.GetString("Permission.NoDeleteDoc", p_iClientId);
					break;
				case RMO_ALLOW_MANUAL_CHECKS:
                    sPermission = Globalization.GetString("Permission.SelectClaimFirst", p_iClientId);
					break;
		//Nitesh 04/24/2006:Tr-2311 Starts
				case RMO_DISPLAY_QUICK_SUMMARY:
                    sPermission = Globalization.GetString("Permission.NoDisplayQuickSummary", p_iClientId);
					break;
		//Nitesh 04/24/2006:Tr-2311 Ends
		//Shruti MITS-7949 starts
				case RMO_NoProgressNotes:
                    sPermission = Globalization.GetString("Permission.NoProgressNotes", p_iClientId);
					break;
				case RMO_NoProgressNotesCreate:
                    sPermission = Globalization.GetString("Permission.NoProgressNotesCreate", p_iClientId);
					break;
				case RMO_NoProgressNotesPrint:
                    sPermission = Globalization.GetString("Permission.NoProgressNotesPrint", p_iClientId);
					break;
		//Shruti MITS-7949 ends
        //Abhishek 14/03/08 : MITS 11403
                case RMO_NOPROGRESS_NOTES_DELETE:
                    sPermission = Globalization.GetString("Permission.NoProgressNotesDelete", p_iClientId);
					break;
        //Geeta 08/22/07 : MITS 10221
                case RMO_NoProgressNotesEdit:
                    sPermission = Globalization.GetString("Permission.NoProgressNotesEdit", p_iClientId);
                    break;
                //MITS 10221 Ends

                //Added by Gagan for Enhanced Policy Billing : Start
                case RMO_BATCHES_CLOSE :
                    sPermission = Globalization.GetString("Permission.NoBatchesClose", p_iClientId);
                    break;
                case RMO_BATCHES_REVERSE :
                    sPermission = Globalization.GetString("Permission.NoBatchesReverse", p_iClientId);
                    break;
                case RMO_BATCHES_ABORT :
                    sPermission = Globalization.GetString("Permission.NoBatchesAbort", p_iClientId);
                    break;
                case RMO_BILLING_RECEIPTS :
                    sPermission = Globalization.GetString("Permission.NoBillingReceipt", p_iClientId);
                    break;
                case RMO_BILLING_ADJUSTMENTS :
                    sPermission = Globalization.GetString("Permission.NoBillingAdjustment", p_iClientId);
                    break;
                case RMO_BILLING_DISBURSEMENTS :
                    sPermission = Globalization.GetString("Permission.NoBillingDisbursement", p_iClientId);
                    break;
                case RMO_BILLING_SELECT :
                    sPermission = Globalization.GetString("Permission.NoBillingSelect", p_iClientId);
                    break;
                case RMO_BILLING_UNSELECT :
                    sPermission = Globalization.GetString("Permission.NoBillingUnselect", p_iClientId);
                    break;
                case RMO_BILLING_RECOCNILE :
                    sPermission = Globalization.GetString("Permission.NoBillingReconcile", p_iClientId);
                    break;
                case RMO_BILLING_PRINT_DISBURSEMENTS :
                    sPermission = Globalization.GetString("Permission.NoBillingPrintDisbursements", p_iClientId);
                    break;
                //Added by Gagan for Enhanced Policy Billing : End
                //nsachdeva2: Mits: 26418
                case RMO_ClaimActLog_Access_GC:
                    sPermission = Globalization.GetString("Permission.NoGCActLog", p_iClientId);
                    break;
                case RMO_ClaimActLog_Access_WC:
                    sPermission = Globalization.GetString("Permission.NOWCActLog", p_iClientId);
                    break;
                case RMO_ClaimActLog_Access_PC:
                    sPermission = Globalization.GetString("Permission.NoPCActLog", p_iClientId);
                    break;
                case RMO_ClaimActLog_Access_DI:
                    sPermission = Globalization.GetString("Permission.NoDIActLog", p_iClientId);
                    break;
                case RMO_ClaimActLog_Access_VA:
                    sPermission = Globalization.GetString("Permission.NoVAActLog", p_iClientId);
                    break;
                //nsachdeva2: Mits: 26418 end 
                //Add by kuladeep for mits:33349 Start
                case RMO_SEARCH_PROPERTY:
                    sPermission = Globalization.GetString("Permission.NoSearch", p_iClientId);
                    break;
                //Add by kuladeep for mits:33349 End
                default:
                    sPermission = Globalization.GetString("Permission.NoPermission", p_iClientId);
					break;
                    
			}
            return string.Format(Globalization.GetString("Permission.FullPermissionMsg1", p_iClientId), sPermission, module, accessType);
				
		}
	}
		
}
