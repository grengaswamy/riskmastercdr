using System;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Web;
using System.Text;
namespace Riskmaster.Common
{

    /// <summary>
    /// SessionManager class contains the properties and methods to manipulate the
    /// Custom Session Object.
    /// </summary>
    public class SessionManager : IDisposable
    {
        private bool isDisposed = false;
        private Hashtable m_BinaryItems = null;
        private string m_sConnectionString = string.Empty, m_sSID = string.Empty;

        /// <summary>
        /// Constructor for SessionManager class.
        /// </summary>
        /// <param name="strSessionConnString">connection string to the RMXSession database</param>
        public SessionManager(string strSessionConnString)
        {
            m_sConnectionString = strSessionConnString;
        }


        /// <summary>
        /// Retrieves and increments the current unique id counter for "tableName" from the Session Glossary.</summary>
        /// <param name="tableName">The name of the table who's next unique row id should be retrieved and incremented.</param>
        /// <returns>Integer containing the requested unique id value.</returns>
        /// <remarks>Collision checking is unnecessary since collisions are so infrequent even 
        /// in high volume transaction databases.  This was more of an issue with MS Access and should no longer
        /// be an issue with platforms such as SQL Server or Oracle.  Collision checking for sessions simply wastes CPU resources.</remarks>
        private int GetNextUID(string tableName, int p_iClientId)
        {
            string SQL = string.Empty;
            int nextUID = 0;
            int origUID = 0;
            int rows = 0;

            if (DbFactory.GetDatabaseType(m_sConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
            {
                switch (tableName.ToUpper())
                {
                    case "SESSIONS":
                        nextUID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SEQ_SESSION_ROW_ID.NEXTVAL FROM DUAL"), p_iClientId);
                        break;
                    case "SESSIONS_X_BINARY":
                        nextUID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SEQ_SESSION_BINARY_ROW_ID.NEXTVAL FROM DUAL"), p_iClientId);
                        break;
                }
            }
            else
            {
                nextUID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, String.Format("SELECT NEXT_UNIQUE_ID FROM SESSION_IDS WHERE SYSTEM_TABLE_NAME = '{0}'", tableName)), p_iClientId);
            }

            //Increment the value
            //Compute next id
            origUID = nextUID;

            nextUID++;

            //try to reserve id (searched update)
            SQL = String.Format("UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = {0} WHERE SYSTEM_TABLE_NAME = '{1}'", nextUID, tableName);

            rows = DbFactory.ExecuteNonQuery(m_sConnectionString, SQL, new Dictionary<string, string>());

            return origUID;
        }


        /// <summary>
        /// Returns the connection string.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
        }

        /// <summary>
        /// Used to get/set the UserData.
        /// </summary>
        public int UserData1
        {
            get;
            set;
        }

        public Hashtable BinaryItems
        {
            get
            {
                return m_BinaryItems;
            }//get
        }//property: BinaryITems

        /// <summary>
        /// Used to get the SessionId.
        /// </summary>
        public string SessionId
        {
            get;
            set;
        }

        public int SessionRowId
        {
            get;
            set;
        }//

        /// <summary>
        /// Used to get/set the time when the Session was last changed.
        /// </summary>
        public string LastChanged
        {
            get;
            set;
        }

        /// <summary>
        /// Checks for the existence of a session with key p_sSessionId in the current
        /// Session Database.
        /// </summary>
        /// <returns>Boolean (True if requested Session Found, False otherwise.)</returns>
        public bool SessionExists(string p_sSessionId)
        {
            bool blnSessionExists = false;

            object objSID = DbFactory.ExecuteScalar(m_sConnectionString, String.Format("SELECT SID FROM SESSIONS WHERE SID='{0}'", p_sSessionId));

            //Verify that the result set is not null
            if (objSID != DBNull.Value || objSID != null)
            {
                blnSessionExists = true;
            } // if


            return blnSessionExists;
        }//method: SessionExists()

        /// <summary>
        /// Dispose method to destroy all open
        /// managed and unmanaged resources in the class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected method following IDisposable pattern
        /// for destroying managed and unmanaged resources
        /// </summary>
        /// <param name="disposing">boolean indicating whether or not the Dispose method was explicitly
        /// called on the class/object</param>
        protected virtual void Dispose(bool disposing)
        {
            
            if (! isDisposed)    
            {
                if (disposing)
                {
                    //Cleanup managed objects
                    m_BinaryItems = null;
                    m_sConnectionString = null;
                    m_sSID = null;
                } // if

            } // if
            isDisposed = true;
        } // method: Dispose


        /// <summary>
        /// Class destructor in the event that the Dispose method
        /// is not called on the class
        /// </summary>
        ~SessionManager()
        {
            Dispose(false);
        }//destructor

        public static SessionManager LoadSession(string strSessionID,int iClientId)
        {
            if (string.IsNullOrEmpty(strSessionID))
            {
                throw new Exception("User's session was not successfully established.  Please verify your settings and try again.");
            }//if

            SessionManager objSessionMgr = new SessionManager(RMConfigurationSettings.GetSessionDSN(iClientId));
            if (!objSessionMgr.LoadSessoinData(strSessionID,iClientId))
            {
                objSessionMgr = null;
            }
            //Start PSARIN2:Session timeout issue 
            if (objSessionMgr != null)
            {
                string sLastChanged = Conversion.ToDbDateTime(DateTime.Now);
                DbFactory.ExecuteNonQuery(RMConfigurationSettings.GetSessionDSN(iClientId), String.Format("UPDATE SESSIONS SET LASTCHANGE = '{0}' WHERE SESSION_ROW_ID={1}", sLastChanged, objSessionMgr.SessionRowId));
            }
            //End PSARIN2:Session timeout issue 
            return objSessionMgr;
        }//method: LoadSession()
       

        /// <summary>
        /// Method used to load actul sessoin data
        /// </summary>
        /// <param name="strSessionID"></param>
        /// <returns></returns>
        private bool LoadSessoinData(string strSessionID,int p_iClientId)
        {
            bool bLoad = false;
            m_BinaryItems = new Hashtable();

            StringBuilder strSessionSQL = new StringBuilder();
            strSessionSQL.Append("SELECT * FROM SESSIONS s ");
            strSessionSQL.Append("INNER JOIN SESSIONS_X_BINARY sxb ON ");
            strSessionSQL.Append("s.SESSION_ROW_ID = sxb.SESSION_ROW_ID ");
            strSessionSQL.Append(string.Format("WHERE SID = {0}", "~SESSION_ID~"));

            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("SESSION_ID", strSessionID);

            using (DbReader objReader = DbFactory.ExecuteReader(RMConfigurationSettings.GetSessionDSN(p_iClientId), strSessionSQL.ToString(), dictParams))
            {
                while (objReader.Read())
                {
                    //Just load the frist one because they are the same
                    if (!bLoad)
                    {
                        this.UserData1 = Conversion.ConvertObjToInt(objReader["USERDATA1"], p_iClientId);
                        this.SessionId = strSessionID;
                        this.LastChanged = Conversion.ConvertObjToStr(objReader["LASTCHANGE"]);
                        this.SessionRowId = Conversion.ConvertObjToInt(objReader["SESSION_ROW_ID"], p_iClientId);
                    }

                    bLoad = true;

                    BinaryItem objItem = new BinaryItem();
                    objItem.IsDirty = false;
                    objItem.IsNew = false;
                    objItem.SessionRowId = this.SessionRowId;
                    objItem.SessionXBinaryRowId = objReader.GetInt("SESSION_X_BIN_ROW_ID");
                    objItem.Bytes = objReader.GetAllBytes("BINARY_VALUE");
                    //objItem.BinaryType = objReader.GetString("BINARY_TYPE");
                    objItem.BinaryName = objReader.GetString("BINARY_NAME");

                    //Avoid duplicate keys from being inserted into the Hashtable, thus causing collisions
                    if (!m_BinaryItems.Contains(objItem.BinaryName))
                    {
                        m_BinaryItems.Add(objItem.BinaryName, objItem);
                    }//if
                }//while
            }

            return bLoad;
        }

        /// <summary>
        /// Loads the required Binary Items for the Session instance
        /// </summary>
        /// <param name="intSessionRowId">integer containing the existing Session Row ID</param>
        private void LoadBinaryItems(int intSessionRowId)
        {
            //Use Lazy Loading to load Binary items
            if (m_BinaryItems == null)
            {

                m_BinaryItems = new Hashtable();

                using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, 
                    String.Format("SELECT * FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID={0}", intSessionRowId)))
                {
                    while (objReader.Read())
                    {
                        BinaryItem objItem = new BinaryItem();
                        objItem.IsDirty = false;
                        objItem.IsNew = false;
                        objItem.SessionRowId = intSessionRowId;
                        objItem.SessionXBinaryRowId = objReader.GetInt("SESSION_X_BIN_ROW_ID");
                        objItem.Bytes = objReader.GetAllBytes("BINARY_VALUE");
                        //objItem.BinaryType = objReader.GetString("BINARY_TYPE");
                        objItem.BinaryName = objReader.GetString("BINARY_NAME");

                        //Avoid duplicate keys from being inserted into the Hashtable, thus causing collisions
                        if (!m_BinaryItems.Contains(objItem.BinaryName))
                        {
                            m_BinaryItems.Add(objItem.BinaryName, objItem);
                        }//if
                    }
                } // using 
            } // if

                       
        }

        /// <summary>
        /// Used to get individual Binary items by name.
        /// </summary>
        public byte[] GetBinaryItem(string name)
        {
            if (m_BinaryItems.ContainsKey(name))
                return (m_BinaryItems[name] as BinaryItem).Bytes;

            return null;
        }


        /// <summary>
        /// Used to get individual Binary items by name.
        /// </summary>
        public void SetBinaryItem(string name, byte[] bytes, int p_iClientId)
        {
            if (m_BinaryItems.ContainsKey(name)) //Existing Item
            {
                BinaryItem tmp = (m_BinaryItems[name] as BinaryItem);
                tmp.BinaryName = name;
                tmp.IsDirty = true;
                tmp.Bytes = (byte[])bytes.Clone();
                tmp.SessionRowId = this.SessionRowId;
                m_BinaryItems[name] = tmp;

                SaveSessionBinary(false, tmp, p_iClientId);
            }
            else //New Item
            {
                BinaryItem tmp = new BinaryItem();
                tmp.BinaryName = name;
                tmp.IsDirty = true;
                tmp.IsNew = true;
                tmp.Bytes = (byte[])bytes.Clone();
              //  tmp.SessionXBinaryRowId = this.GetNextUID("SESSIONS_X_BINARY");//skhare7 Perf violation of binary key
                tmp.SessionRowId = this.SessionRowId;
                m_BinaryItems.Add(name, tmp);

                SaveSessionBinary(true, tmp, p_iClientId);
            }
        }


        /// <summary>
        /// Used to remove individual Binary items by name.
        /// </summary>
        public void RemoveBinaryItem(string name)
        {
            if (m_BinaryItems.ContainsKey(name)) //Existing Item
            {
                DbFactory.ExecuteNonQuery(m_sConnectionString, String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_X_BIN_ROW_ID = {0}", (m_BinaryItems[name] as BinaryItem).SessionXBinaryRowId), new Dictionary<string, string>());

                m_BinaryItems.Remove(name);
            }
        }

        /// <summary>
        /// Saves a Session Binary back into the Sessions_X_Binary table
        /// </summary>
        /// <param name="blnIsNewRecord"></param>
        /// <param name="item"></param>
        private void SaveSessionBinary(bool blnIsNewRecord, BinaryItem item, int p_iClientId)
        {
            StringBuilder strSessionBinarySQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string,object>();

            if (blnIsNewRecord) //A new record needs to be inserted
            {
                //strSessionBinarySQL.Append("INSERT INTO SESSIONS_X_BINARY (SESSION_X_BIN_ROW_ID, SESSION_ROW_ID,");
                //strSessionBinarySQL.Append("BINARY_NAME,BINARY_VALUE)");
                //strSessionBinarySQL.Append("VALUES (");
                //strSessionBinarySQL.Append(string.Format("{0},", "~SESSION_BIN_ROW_ID~"));
                //strSessionBinarySQL.Append(string.Format("{0},", "~SESSION_ROW_ID~"));
                //strSessionBinarySQL.Append(string.Format("{0},", "~BINARY_NAME~"));
                //strSessionBinarySQL.Append(string.Format("{0}", "~BINARY_VALUE~"));
                //strSessionBinarySQL.Append(")");

                //dictParams.Add("SESSION_BIN_ROW_ID", item.SessionXBinaryRowId);
                //dictParams.Add("SESSION_ROW_ID", item.SessionRowId);
                //dictParams.Add("BINARY_NAME", item.BinaryName);
                //dictParams.Add("BINARY_VALUE", item.Bytes);//skhare7 changes for perf actions 
                if (DbFactory.GetDatabaseType(m_sConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
                {
                    int intBinRowID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SEQ_SESSION_BINARY_ROW_ID.NEXTVAL FROM DUAL"), p_iClientId);
                        strSessionBinarySQL.Append("INSERT INTO SESSIONS_X_BINARY (SESSION_X_BIN_ROW_ID, SESSION_ROW_ID, BINARY_NAME, BINARY_VALUE)");
                        strSessionBinarySQL.Append(string.Format("VALUES ({0}, {1}, {2}, {3})", "~SESSION_BINARY_ID~", "~SESSION_ROW_ID~", "~BINARY_NAME~", "~BINARY_VALUE~"));

                        //Set the Session Binary Key that is in the Dictionary collection
                       // strSessionBinaryKey = kvp.Key;
                        //PSARIN2: FIX Session violation issues 
                        dictParams.Add("SESSION_BINARY_ID", intBinRowID);
                        dictParams.Add("SESSION_ROW_ID", item.SessionRowId);
                        dictParams.Add("BINARY_NAME",item.BinaryName);
                        dictParams.Add("BINARY_VALUE", item.Bytes);

                        DbFactory.ExecuteNonQuery(m_sConnectionString, strSessionBinarySQL.ToString(), dictParams);

                        strSessionBinarySQL.Length = 0;
                        dictParams.Clear();
                  //foreach
                }
                else
                {
                   
                        strSessionBinarySQL.Append("INSERT INTO SESSIONS_X_BINARY (SESSION_ROW_ID, BINARY_NAME, BINARY_VALUE)");
                        strSessionBinarySQL.Append(string.Format("VALUES ({0}, {1}, {2})", "~SESSION_ROW_ID~", "~BINARY_NAME~", "~BINARY_VALUE~"));

                        //Set the Session Binary Key that is in the Dictionary collection
                       // strSessionBinaryKey = kvp.Key;
                        //PSARIN2: FIX Session violation issues 
                        //dictParams.Add("SESSION_BINARY_ID", objUserSession.BinaryItems[strSessionBinaryKey].SessionBinaryID);
                       dictParams.Add("SESSION_ROW_ID", item.SessionRowId);
                        dictParams.Add("BINARY_NAME",item.BinaryName);
                        dictParams.Add("BINARY_VALUE", item.Bytes);


                        DbFactory.ExecuteNonQuery(m_sConnectionString, strSessionBinarySQL.ToString(), dictParams);

                        strSessionBinarySQL.Length = 0;
                        dictParams.Clear();
                  }//foreach

               // DbFactory.ExecuteNonQuery(m_sConnectionString, strSessionBinarySQL.ToString(), dictParams);
             

            }//if
            else //Record needs to be updated
            {
                strSessionBinarySQL.Append("UPDATE SESSIONS_X_BINARY ");
                strSessionBinarySQL.Append(string.Format("SET BINARY_VALUE = {0} ", "~BINARY_VALUE~"));
                strSessionBinarySQL.Append(string.Format("WHERE SESSION_ROW_ID = {0} ", "~SESSION_ROW_ID~"));
                strSessionBinarySQL.Append(string.Format("AND BINARY_NAME = {0} ", "~BINARY_NAME~"));

                dictParams.Add("BINARY_VALUE", item.Bytes);
                dictParams.Add("SESSION_ROW_ID", item.SessionRowId);
                dictParams.Add("BINARY_NAME", item.BinaryName);
                
                DbFactory.ExecuteNonQuery(m_sConnectionString, strSessionBinarySQL.ToString(), dictParams);

            }//else
        }//method: SaveSessionBinary


        /// <summary>
        /// BinaryItem Storage class
        /// </summary>
        private class BinaryItem
        {
            public byte[] Bytes;
            //public string BinaryType;
            public string BinaryName;
            public int SessionRowId;
            public int SessionXBinaryRowId;
            public bool IsDirty;
            public bool IsNew;
        }

    }//class

 
    /// <summary>
    /// Stores User Session information
    /// </summary>
    public class UserSession
    {
        public string SessionID
        {
            get;
            set;
        }//property SessionID

        public string SessionRowID
        {
            get;
            set;
        } // property SessionRowID


        public string LoginName
        {
            get;
            set;
        } // property LoginName

        public string Data
        {
            get;
            set;
        } // property Data

        /// <summary>
        /// Used to get/set the UserData.
        /// </summary>
        public int UserData1
        {
            get;
            set;
        }

        public Dictionary<string, UserSessionBinaryItem> BinaryItems
        {
            get;
            set;
        }


        /// <summary>
        /// Used to get/set the time when the Session was last changed.
        /// </summary>
        public string LastChanged
        {
            get;
            set;
        }
    } // class

    public class UserSessionBinaryItem
    {
        public string SessionRowID
        {
            get;
            set;
        } // property SessionRowID

        public string SessionBinaryID
        {
            get;
            set;
        } // property SessionBinaryID

        public string BinaryName
        {
            get;
            set;
        } // property BinaryName

        public byte[] BinaryValue
        {
            get;
            set;
        }//property BinaryValue

        public string LastChanged
        {
            get;
            set;
        } // property LastChanged


    }//class


}

