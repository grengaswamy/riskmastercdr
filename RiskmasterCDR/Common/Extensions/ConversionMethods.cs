/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 09/08/2014 | RMA-345          | achouhan3  | Angular Grid Implementaion 
 **********************************************************************************************/
using System;

namespace Riskmaster.Common.Extensions
{
    /// <summary>
    /// Encapsulates all conversion methods as Extension Methods
    /// </summary>
    /// <remarks>Extension methods were newly introduced as part of .Net 3.0/3.5</remarks>
    public static class ConversionMethods
    {
        /// <summary>
        /// This converts the current DateTime object to Utc Time Format
        /// </summary>
        /// <param name="dtCurrentTime">instance of a DateTime object</param>
        /// <returns>DateTime converted to Utc format</returns>
        public static DateTime ConvertToUtcTime(this DateTime dtCurrentTime)
        {
            return DateTime.SpecifyKind(dtCurrentTime, DateTimeKind.Utc);
        }//method: ConvertToUtcTime();


        /// <summary>
        /// This converts the current DateTime object to Local Time Format
        /// </summary>
        /// <param name="dtCurrentTime">instance of a DateTime object</param>
        /// <returns>DateTime converted to Local Time format</returns>
        public static DateTime ConvertToLocalTime(this DateTime dtCurrentTime)
        {
            return DateTime.SpecifyKind(dtCurrentTime, DateTimeKind.Local);
        }//method: ConvertToLocalTime();

        /// <summary>Gets a common date format for RISKMASTER</summary>
        /// <param name="dtCurrentTime">DateTime to be converted into Riskmaster date format "YYYYMMDD."</param>
        /// <returns>String value for DateTime date in "YYYYMMDD" format.</returns>
        /// <remarks>Returns an empty string if DateTime is equal to DateTime.MinValue.</remarks>
        public static string ConvertToRiskmasterDate(this DateTime dtCurrentTime)
        {
            if (dtCurrentTime == DateTime.MinValue)
            {
                return string.Empty;
            }//if

            return dtCurrentTime.ToString("yyyyMMdd"); //year month day
        }//method: ConvertToRiskmasterDate()

        #region Convert object to Integer
        /// <summary>
        /// Convert passed object to integer value
        /// </summary>
        /// <param name="p_objValue">object to convert</param>
        /// <returns>Converted integer value</returns>
        public static Int32 ConvertToInt32(this object p_objValue)
        {

            if (p_objValue == null || p_objValue is System.DBNull || p_objValue.ToString() == "")
            {
                return 0;
            }

            return Convert.ToInt32(p_objValue);

        }
        #endregion
        /// <summary>
        /// Converts Null or DBNull object to Int64
        /// </summary>
        /// <param name="p_ObjValue">object to convert</param>
        /// <returns>Converted Int64 value</returns>
        public static Int64 ConvertToInt64(this object p_ObjValue)
        {

            if (p_ObjValue == null || p_ObjValue is System.DBNull)
            {
                return 0;
            }

            return Convert.ToInt64(p_ObjValue);

        }


        #region Converts Object to Double
        /// <summary>
        /// Converts Object value to Double
        /// </summary>
        /// <param name="p_objVal">Object to convert to Double</param>
        /// <returns>Converted Double value</returns>
        public static Double ConvertToDouble(this object p_objVal)
        {
            double dblRetVal = 0;

            //Changes made by Neelima
            if (p_objVal == null || p_objVal is System.DBNull)
                dblRetVal = 0;
            else
                dblRetVal = Convert.ToDouble(p_objVal);

            return dblRetVal;

        }
        #endregion

        /// <summary>		
        /// This method will convert the passed object to a string value
        /// </summary>		
        /// <param name="p_obj">The object that will be converted to a string value</param>												
        /// <returns>A string value</returns>
        public static string ConvertToString(this object p_obj)
        {
            string sRetValue = string.Empty;
            try
            {
                if (p_obj != null)
                    sRetValue = p_obj.ToString();
            }
            catch { return ""; }
            return sRetValue;

        }

        #region Converts Object to bool
        /// <summary>
        /// Converts Object value to bool
        /// </summary>
        /// <param name="p_objVal">Object to convert to bool</param>
        /// <returns>Converted bool value</returns>
        public static bool ConvertToBoolean(this object p_objVal)
        {
            bool bRetVal = false;

            if (p_objVal is bool)
                return (bool)p_objVal;

            if (p_objVal is string)
                p_objVal = (p_objVal as string).ToLower().Replace("true", "1").Replace("false", "0");

            bRetVal = p_objVal.ConvertToString().ConvertToLong().ConvertToBoolean();

            return bRetVal;

           

        }


        #endregion

        #region Convert long to bool
        /// <summary>
        /// Converts long value To boolean
        /// </summary>
        /// <param name="p_lValue">long value to convert</param>
        /// <returns>Converted boolean value</returns>
        public static bool ConvertToBoolean(this long p_lValue)
        {


            if ((p_lValue.ToString().Equals("")))
            {
                return false;
            }

            return Convert.ToBoolean(p_lValue);

        }
        #endregion

        /// <summary>		
        /// This method will convert a string to a long value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to a long value</param>												
        /// <returns>A long value</returns>
        public static long ConvertToLong(this string p_sValue)
        {
            long lRetVal = 0;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return lRetVal;
                }
                else
                {
                    lRetVal = Convert.ToInt64(p_sValue);
                }
            }
            catch { return 0; }
            return lRetVal;
        }

        /// <summary>		
        /// This method will convert a string to an integer value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to an integer value</param>												
        /// <returns>An integer value</returns>
        public static int ConvertToInt32(this string p_sValue)
        {
            int iRetVal = 0;
            try
            {

                if (string.IsNullOrEmpty(p_sValue))
                {
                    return iRetVal;
                }
                else if (p_sValue.ToUpper() == "TRUE")//BSB Hack to support legacy habit of hiding booleans as integers.
                    return -1;
                else
                {
                    iRetVal = Convert.ToInt32(p_sValue);
                }
            }
            catch { return 0; }
            return iRetVal;

        }

        /// <summary>
        /// Convert string into double
        /// </summary>
        /// <param name="p_sValue">Input string value</param>
        /// <returns>Double value</returns>
        public static double ConvertToDouble(this string p_sValue)
        {
            double dblRetVal = 0;
            string str = p_sValue;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return dblRetVal;
                }
                else
                {
                    //pmahli MITS 9327 6/15/2007 "()" replaced by "-"
                    str = p_sValue.Replace(",", "").Replace("$", "").Replace("(", "-").Replace(")", "");
                    dblRetVal = Convert.ToDouble(str);
                }
            }
            catch
            {
                return (0);
            }
            return (dblRetVal);
        }

        /// <summary>
        /// Convert string value to boolean value.
        /// </summary>
        /// <param name="p_sValue">Input string to convert to boolean value</param>
        /// <returns>Converted boolean value </returns>
        public static bool ConvertToBoolean(this string p_sValue)
        {
            int iValue = 0;
            bool blnParsed = false;
            if (p_sValue == "" || p_sValue == null)
                return (false);
            try
            {
                blnParsed = Int32.TryParse(p_sValue, out iValue);
                if (blnParsed)
                {
                    iValue = p_sValue.ConvertToInt32();
                    if (iValue == 0)
                        return (false);
                    else
                        return (true);
                }
                else
                {
                    if (p_sValue.ToLower() == "true")
                        return true;
                    else
                        return (false);
                }
            }
            catch
            {
                return (false);
            }
        }//method: ConvertToBoolean()

        //RMA-345       : Method to identiy request whether Ajax request or not Starts
        /// <summary>
        /// Dev Signature      :       achouhan3
        /// Date               :       09/09/2014
        /// Description        :       Extension Method to identify Whether a Request is Ajax request or server request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool IsAjaxRequest(this System.Web.HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            return (request["X-Requested-With"] == "XMLHttpRequest") || ((request.Headers != null) && (request.Headers["X-Requested-With"] == "XMLHttpRequest"));
        }
        //Method to identiy request whether Ajax request or not Ends
    }//class
}//namespace