using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Collections.Specialized;
using System.Configuration;


namespace Riskmaster.Common
{
	/// <summary>
	/// Summary description for LoggingManager.
	/// </summary>
	/// 
		public class Log
		{
		
			
            private const string m_LOG_CATEGORY_SCRIPT = "Scripting";
            private const string m_LOG_CATEGORY_DEFAULT = "Default";

            /// <summary>
            /// Gets the value for the Log Category
            /// </summary>
            static public string LOG_CATEGORY_SCRIPT
            {
                get { return m_LOG_CATEGORY_SCRIPT; }
            }

            
            /// <summary>
            /// Gets the value for the default Logging category
            /// </summary>
            static public string LOG_CATEGORY_DEFAULT
            {
                get { return m_LOG_CATEGORY_DEFAULT; }
            }


            /// <summary>
            /// Log just a message with a timestamp
            /// </summary>
            /// <param name="sMessage">The s message.</param>
            /// <param name="ClientId">The client identifier.</param>
            static public void Write(string sMessage, int ClientId = 0)
            {
                if (ClientId == 0)//Base functionality work as earlier working.
                {
                    if (!string.IsNullOrEmpty(sMessage))
                    {
                        // Creates and fills the log entry with user information
                        LogEntry log = new LogEntry();
                        log.EventId = 1001;
                        log.Message = sMessage;
                        log.Categories.Add(m_LOG_CATEGORY_DEFAULT);
                        log.Priority = 0;

                        // Writes the log entry.
                        Logger.Write(log);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(sMessage))
                    {
                        LogData objLogData = new LogData();
                        objLogData.EventId = 1001;
                        objLogData.Message = sMessage;
                        objLogData.Category = m_LOG_CATEGORY_DEFAULT;
                        objLogData.Priority = 0;
                        objLogData.ClientId = ClientId.ToString();

                        string sCloudLogLocation = ConfigurationManager.AppSettings["LogLocation"];
                        ILogInterface objLog = null;
                        LogStorageTypeEnumeration.LogStorageType enumStoageType;
                        enumStoageType = (LogStorageTypeEnumeration.LogStorageType)Enum.Parse(typeof(LogStorageTypeEnumeration.LogStorageType), sCloudLogLocation);
                        objLog = LogStorageFactory.CreateInstance(enumStoageType);
                        objLog.Add(objLogData);
                    }
                }
            }

            /// <summary>
            /// Log the message as well as a category
            /// </summary>
            /// <param name="sMessage">string containing the specified message</param>
            /// <param name="sCategory">string containing the category of the error</param>
            /// <param name="ClientId">The client identifier.</param>
            static public void Write(string sMessage, string sCategory, int ClientId = 0)
            {
                if (ClientId == 0)//Base functionality work as earlier working.
                {
                    if (!string.IsNullOrEmpty(sMessage))
                    {
                        // Creates and fills the log entry with user information
                        LogEntry log = new LogEntry();
                        log.EventId = 1001;
                        log.Message = sMessage;
                        log.Categories.Add(sCategory);
                        log.Priority = 0;

                        // Writes the log entry.
                        Logger.Write(log);

                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(sMessage))
                    {

                        LogData objLogData = new LogData();
                        objLogData.EventId = 1001;
                        objLogData.Message = sMessage;
                        objLogData.Category = sCategory;
                        objLogData.Priority = 0;
                        objLogData.ClientId = ClientId.ToString();

                        string sCloudLogLocation = ConfigurationManager.AppSettings["LogLocation"];
                        ILogInterface objLog = null;
                        LogStorageTypeEnumeration.LogStorageType enumStoageType;
                        enumStoageType = (LogStorageTypeEnumeration.LogStorageType)Enum.Parse(typeof(LogStorageTypeEnumeration.LogStorageType), sCloudLogLocation);
                        objLog = LogStorageFactory.CreateInstance(enumStoageType);
                        objLog.Add(objLogData);
                    }
                }
            }

            /// <summary>
            /// Log the specific RISKMASTER EventItem
            /// </summary>
            /// <param name="eventItem">EventItem to be logged</param>
            /// <param name="ClientId">The client identifier.</param>
            static public void Write(LogItem eventItem, int ClientId = 0)
            {

                if (ClientId == 0)//Base functionality work as earlier working.
                {
                    if (eventItem != null)
                    {
                        try
                        {
                            // Creates and fills the log entry with user information
                            LogEntry log = new LogEntry();
                            log.EventId = eventItem.EventId;
                            log.Message = eventItem.Message;
                            log.Categories.Add(eventItem.Category);
                            log.Priority = eventItem.Priority;

                            if (!eventItem.RMParamList.Equals(null))
                            {
                                log.ExtendedProperties = eventItem.RMParamList;
                            } // if



                            // Writes the log entry.
                            Logger.Write(log);
                        }
                        finally
                        {
                            eventItem = null;
                        }
                    }//if
                }
                else
                {
                    if (eventItem != null)
                    {
                        LogData objLogData = new LogData();
                        objLogData.EventId = eventItem.EventId;
                        objLogData.Message = eventItem.Message;
                        objLogData.Category = eventItem.Category;
                        objLogData.Priority = eventItem.Priority;
                        if (!eventItem.RMParamList.Equals(null))
                        {
                            objLogData.RMParamList = eventItem.RMParamList;
                        }
                        objLogData.ClientId = ClientId.ToString();

                        string sCloudLogLocation = ConfigurationManager.AppSettings["LogLocation"];
                        ILogInterface objLog = null;
                        LogStorageTypeEnumeration.LogStorageType enumStoageType;
                        enumStoageType = (LogStorageTypeEnumeration.LogStorageType)Enum.Parse(typeof(LogStorageTypeEnumeration.LogStorageType), sCloudLogLocation);
                        objLog = LogStorageFactory.CreateInstance(enumStoageType);
                        objLog.Add(objLogData);
                    }
                }
            }
		}
		
}
