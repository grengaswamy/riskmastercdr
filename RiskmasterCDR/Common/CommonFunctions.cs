﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/06/2013 | 34082  | pgupta93   | Add Function for Get CodeID of Reserve Currency Type
 * 12/06/2014 | 34276  | achouhan3  | Add Function to check uniquneness for Entity ID Number and Entity ID Type
 * 14/01/2015 | RMA-344| achouhan3  | Added Function to save and get common User Preference
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.IO;
using System.Globalization;
using System.Configuration;

using Riskmaster.Cache;
using Riskmaster.Models;
using System.Diagnostics;



namespace Riskmaster.Common
{
    /// <summary>
    /// Author  :   Rahul Solanki
    /// Dated   :   24 Sep 2010
    /// Purpose :  
    /// </summary>
    public static class CommonFunctions
    {
        public enum NavFormType : int
        {
            None = 1,
            Reserve = 2,
            Claim = 3,
            Funds = 4,
            Auto = 5
        }
        public enum MigrationProcessType
        {
            PolicyExtract = 1,
            AcClaimsExtract = 2,
            Prerequisite = 3,
            ACUnReferencedData = 4
        }
        //mbahl3
        public enum StorageType : int
        {
            Temporary = 1,
            Permanent = 2
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We can Retrieve multiple files from DB at a time.
        // This mode specifies if multiple files need to be retrieved or only one file needs to be retrieved
        public enum RetrivalMode
        {
            Single = 1,
            Multiple = 2
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // This enum store the File Type we are storing in DB. We will retrieve the files based on this Parameter
        // If we need to attach the documents then only the Documents with FileType as Attachment shall be retrieved
        // As of now this Enum is being used only in case of DA module, but we can use this for other modules as well.
        public enum FileType
        {
            None = 0,
            Import = 1,
            Attachment = 2
        }


        //mbahl3
        //Deb Multi Currency
        public class CacheTable
        {
            //private static Hashtable m_StaticCache = new Hashtable();
            private int m_iClientId = 0;
            public CacheTable(int p_iClientId)
            {
                m_iClientId = p_iClientId;
            }
            public object this[string sKey]
            {
                get { return CacheCommonFunctions.RetreiveValueFromCache<object>(String.Format("$$|MC|$$_{0}", sKey), m_iClientId); }
                set { CacheCommonFunctions.UpdateValue2Cache<object>(String.Format("$$|MC|$$_{0}", sKey), m_iClientId, value); }
            }
            public bool ContainsKey(object sKey)
            {
                return CacheCommonFunctions.CheckIfKeyExists(String.Format("$$|MC|$$_{0}", sKey), m_iClientId);
            }
            public void Add(object sKey, object objValue)
            {
                try { CacheCommonFunctions.AddValue2Cache<object>(String.Format("$$|MC|$$_{0}", sKey), m_iClientId, objValue); }
                catch { };
            }
            public void Remove(object sKey)
            {
                try { CacheCommonFunctions.RemoveValueFromCache(String.Format("$$|MC|$$_{0}", sKey), m_iClientId); }
                catch { };
            }
        }
        private static CacheTable m_Cache = null;
        //Deb Multi Currency
        /// <summary>
        /// user to update the list of recent records when either a claim child is updated or the associated comment/enhc note is edited
        /// </summary>
        /// <param name="p_connString">conn string to RMX db</param>
        /// <param name="p_userId"></param>
        /// <param name="p_recordType">claim or event</param>
        /// <param name="p_recordId">claim id or event id</param>
        /// <param name="recordNumber"></param>
        public static void UpdateRecentRecords(string p_connString, string p_userId, string p_recordType, string p_recordId, string recordNumber, int p_iClientId)
        {
            //rsolanki2: recent claim updates : MITS 18828

            string sSql = string.Empty;
            string sXml = string.Empty;
            Riskmaster.Db.DbReader objReader = null;
            XmlDocument objPrefXml = null;
            XmlNode recentRecordNode = null;
            XmlNode tempNode = null;
            XmlElement newRecentNode = null;
            XmlNodeList recentRecordList = null;
            XmlAttribute atbp_recordType = null;
            XmlAttribute atbp_recordId = null;
            XmlAttribute atbRecordNumber = null;
            Riskmaster.Db.DbConnection objConn = null;
            Riskmaster.Db.DbCommand objCommand = null;
            Riskmaster.Db.DbParameter objParam = null;
            int recentRecordCount = 0;

            try
            {
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + p_userId;
                objReader = Riskmaster.Db.DbFactory.GetDbReader(p_connString, sSql);
                if (objReader.Read())
                {
                    sXml = objReader.GetValue("PREF_XML").ToString();
                }
                objReader.Close();
                //objReader.NextResult()
                if (sXml != string.Empty)
                {
                    try
                    {
                        objPrefXml = new XmlDocument();
                        objPrefXml.LoadXml(sXml);
                    }
                    catch (Exception)
                    {
                        //delete bad formatted xml string from database
                        sSql = "DELETE FROM USER_PREF_XML WHERE USER_ID=" + p_userId;
                        objCommand.Parameters.Clear();
                        objCommand.CommandText = sSql;
                        objCommand.ExecuteNonQuery();
                        sXml = string.Empty;
                    }
                }

                if (sXml != string.Empty)
                {
                    recentRecordNode = objPrefXml.SelectSingleNode("setting/RecentRecords");

                    if (recentRecordNode != null)
                    {
                        //if same node already exists then don't do any thing and return 
                        if (recentRecordNode.SelectSingleNode("record[(@recordid='"
                            + p_recordId
                            + "') and (@recordtype='"
                            + p_recordType
                            + "')]") != null)
                        {
                            return;
                        }

                        recentRecordList = recentRecordNode.SelectNodes(string.Concat("record[@recordtype='", p_recordType, "']"));
                        recentRecordCount = recentRecordList.Count;

                        if (recentRecordCount == 10)
                        {
                            //remove the very first record and inser this new record at last postition
                            tempNode = recentRecordList.Item(0);
                            recentRecordNode.RemoveChild(tempNode);
                        }
                    }
                    else
                    {
                        recentRecordNode = objPrefXml.CreateNode(XmlNodeType.Element, "RecentRecords", null);
                        objPrefXml.SelectSingleNode("setting").AppendChild(recentRecordNode);
                    }

                    newRecentNode = objPrefXml.CreateElement("record");

                    atbp_recordType = objPrefXml.CreateAttribute("recordtype");
                    atbp_recordType.Value = p_recordType;
                    newRecentNode.Attributes.Append(atbp_recordType);

                    atbp_recordId = objPrefXml.CreateAttribute("recordid");
                    atbp_recordId.Value = p_recordId;
                    newRecentNode.Attributes.Append(atbp_recordId);

                    atbRecordNumber = objPrefXml.CreateAttribute("recordnumber");
                    atbRecordNumber.Value = recordNumber;
                    newRecentNode.Attributes.Append(atbRecordNumber);

                    recentRecordNode.AppendChild(newRecentNode);

                    sXml = objPrefXml.InnerXml;

                    //Update USER_PREF_XML table with the updated XML
                    sSql = "UPDATE USER_PREF_XML SET PREF_XML=~PXML~ WHERE USER_ID=" + p_userId;
                    objCommand.Parameters.Clear();
                    objParam = objCommand.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.Value = sXml;
                    objParam.ParameterName = "PXML";
                    objParam.SourceColumn = "PREF_XML";
                    objCommand.Parameters.Add(objParam);
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                }
                else
                {
                    //if user xml is not present in the database then insert a record for the same
                    sXml = "<setting><RecentRecords><record recordtype='"
                        + p_recordType
                        + "' recordid='"
                        + p_recordId
                        + "' recordnumber='"
                        + recordNumber
                        + "'/></RecentRecords></setting>";

                    sSql = string.Concat("INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES("
                        , p_userId
                        , ", ~PXML~)");
                    objCommand.Parameters.Clear();
                    objParam = objCommand.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.Value = sXml;
                    objParam.ParameterName = "PXML";
                    objParam.SourceColumn = "PREF_XML";
                    objCommand.Parameters.Add(objParam);
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                }
            }
            //Riskmaster.Db is throwing following exception so need to catch the same
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.RecentRecord", p_iClientId),
                    p_objExp);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }

                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }

        /// <summary>
        /// user to update the list of recent records when either a claim child is updated or the associated comment/enhc note is edited
        /// </summary>
        /// <param name="p_connString">conn string to RMX db</param>
        /// <param name="p_userId"></param>
        /// <param name="p_recordType">claim or event</param>
        /// <param name="p_recordId">claim id or event id</param>        
        public static void UpdateRecentRecords(string p_connString, string p_userId, string p_recordType, string p_recordId, int p_iClientId)
        {
            //rsolanki2: recent claim updates : MITS 18828

            string sSql = string.Empty;
            string sXml = string.Empty;
            Riskmaster.Db.DbReader objReader = null;
            Riskmaster.Db.DbConnection objConn = null;
            Riskmaster.Db.DbCommand objCommand = null;
            string recordNumber = string.Empty;

            try
            {
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                if (p_recordType == "claim")
                {
                    sSql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + p_recordId;
                }
                else if (p_recordType == "event")
                {
                    sSql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + p_recordId;
                }

                objReader = Riskmaster.Db.DbFactory.GetDbReader(p_connString, sSql);

                if (objReader.Read())
                {
                    recordNumber = objReader.GetValue(0).ToString();
                }
                else
                {
                    throw new Exception("Invalid record id");
                }

                UpdateRecentRecords(p_connString, p_userId, p_recordType, p_recordId, recordNumber, p_iClientId);
            }
            //Riskmaster.Db is throwing following exception so need to catch the same
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.RecentRecord", p_iClientId),
                    p_objExp);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }

                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }

        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetAdjusterUserID
        /// </summary>     
        public static int GetAdjusterUserID(string p_connString, int p_adjusterEID)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetAdjusterUserID(p_connString, p_adjusterEID, 0);
            }
            else
                throw new Exception("GetAdjusterUserID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is ato Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Return User ID of Adjuster.
        /// </summary>
        /// <param name="p_connString"></param>
        /// <param name="p_adjusterEID"></param>
        public static int GetAdjusterUserID(string p_connString, int p_adjusterEID, int p_iClientId)
        {
            string sSQL = string.Empty;
            int iRMUserID = 0;
            DbReader objReader = null;
            try
            {
                if (p_adjusterEID > 0)
                {
                    sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + p_adjusterEID;
                    objReader = DbFactory.GetDbReader(p_connString, sSQL);
                    using (objReader)
                    {
                        while (objReader.Read())
                        {
                            iRMUserID = Convert.ToInt32(objReader.GetValue("RM_USER_ID"));
                        }
                    }
                }
                return iRMUserID;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateAdjusterSystemDiary.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for CreateWPADiary
        /// </summary>     
        public static void CreateWPADiary(string s_connString, string s_entryName, string s_entryNotes, int i_priority, int i_statusOpen, string s_assignedUser, string s_assigningUser, string s_assignedGroup, int i_isAttached, string s_attachTable, int i_attachRecordID, string s_regarding, string s_completeTime, string s_completeDate, string s_pipeSepValues)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                CreateWPADiary(s_connString, s_entryName, s_entryNotes, i_priority, i_statusOpen, s_assignedUser, s_assigningUser, s_assignedGroup, i_isAttached, s_attachTable, i_attachRecordID, s_regarding, s_completeTime, s_completeDate, s_pipeSepValues, 0);
            }
            else
                throw new Exception("CreateWPADiary" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Diray Creation
        /// </summary>
        /// <param name="s_connString">s_connString</param>
        /// <param name="s_entryName">s_entryName</param>
        /// <param name="s_entryNotes">s_entryNotes</param>
        /// <param name="i_priority">1,2,3</param>
        /// <param name="i_statusOpen">-1,0,1</param>
        /// <param name="s_assignedUser">s_assignedUser</param>
        /// <param name="s_assigningUser">s_assigningUser</param>
        /// <param name="s_assignedGroup">s_assignedGroup</param>
        /// <param name="i_isAttached">-1,0,1</param>
        /// <param name="s_attachTable">s_attachTable</param>
        /// <param name="i_attachRecordID">i_attachRecordID</param>
        /// <param name="s_regarding">s_regarding</param>
        /// <param name="s_completeTime">s_completeTime</param>
        /// <param name="s_completeDate">s_completeDate</param>
        /// <param name="s_pipeSepValues">AUTO_CONFIRM|ATT_FORM_CODE|ATT_SEC_REC_ID|DIARY_VOID|DIARY_DELETED|NOTIFY_FLAG|ROUTE_FLAG|ESTIMATE_TIME|AUTO_ID</param>
        public static void CreateWPADiary(string s_connString, string s_entryName, string s_entryNotes, int i_priority, int i_statusOpen, string s_assignedUser, string s_assigningUser, string s_assignedGroup, int i_isAttached, string s_attachTable, int i_attachRecordID, string s_regarding, string s_completeTime, string s_completeDate, string s_pipeSepValues, int p_iClientId)
        {
            string[] arrPipeSepValues = null;
            //Riskmaster.Db.DbConnection objConn = null;
            //Riskmaster.Db.DbCommand objCommand = null;
            //System.Text.StringBuilder sbSQL = null;
            Riskmaster.Db.DbWriter objWriter = null;
            int iAutoConfirm = 0;
            int iAttFormCode = 0;
            int iAttSecRecId = 0;
            int iDiaryVoid = 0;
            int iDiaryDeleted = 0;
            int iNotifyFlag = 0;
            int iRouteFlag = 0;
            int iEstimateTime = 0;
            int iAutoId = 0;
            try
            {
                arrPipeSepValues = s_pipeSepValues.Split('|');
                if (arrPipeSepValues != null && arrPipeSepValues.Length > 0)
                {
                    iAutoConfirm = Convert.ToInt32(arrPipeSepValues[0]);
                    if (arrPipeSepValues.Length > 1)
                        iAttFormCode = Convert.ToInt32(arrPipeSepValues[1]);
                    if (arrPipeSepValues.Length > 2)
                        iAttSecRecId = Convert.ToInt32(arrPipeSepValues[2]);
                    if (arrPipeSepValues.Length > 3)
                        iDiaryVoid = Convert.ToInt32(arrPipeSepValues[3]);
                    if (arrPipeSepValues.Length > 4)
                        iDiaryDeleted = Convert.ToInt32(arrPipeSepValues[4]);
                    if (arrPipeSepValues.Length > 5)
                        iNotifyFlag = Convert.ToInt32(arrPipeSepValues[5]);
                    if (arrPipeSepValues.Length > 6)
                        iRouteFlag = Convert.ToInt32(arrPipeSepValues[6]);
                    if (arrPipeSepValues.Length > 7)
                        iEstimateTime = Convert.ToInt32(arrPipeSepValues[7]);
                    if (arrPipeSepValues.Length > 8)
                        iAutoId = Convert.ToInt32(arrPipeSepValues[8]);
                }
                //objConn = Riskmaster.Db.DbFactory.GetDbConnection(s_connString);
                //objConn.Open();
                //objCommand = objConn.CreateCommand();

                objWriter = DbFactory.GetDbWriter(s_connString);

                objWriter.Tables.Add("WPA_DIARY_ENTRY");

                objWriter.Fields.Add("ENTRY_ID", Utilities.GetNextUID(s_connString, "WPA_DIARY_ENTRY", p_iClientId));

                objWriter.Fields.Add("ENTRY_NAME", s_entryName);
                objWriter.Fields.Add("ENTRY_NOTES", s_entryNotes);
                objWriter.Fields.Add("CREATE_DATE", Conversion.ToDbDateTime(System.DateTime.Now));
                objWriter.Fields.Add("PRIORITY", i_priority);
                objWriter.Fields.Add("STATUS_OPEN", i_statusOpen);
                objWriter.Fields.Add("AUTO_CONFIRM", iAutoConfirm);

                objWriter.Fields.Add("ASSIGNED_USER", s_assignedUser);
                objWriter.Fields.Add("ASSIGNING_USER", s_assigningUser);
                objWriter.Fields.Add("ASSIGNED_GROUP", s_assignedGroup);
                objWriter.Fields.Add("IS_ATTACHED", i_isAttached);

                objWriter.Fields.Add("ATTACH_TABLE", s_attachTable);
                objWriter.Fields.Add("ATT_FORM_CODE", iAttFormCode);
                objWriter.Fields.Add("ATTACH_RECORDID", i_attachRecordID);
                objWriter.Fields.Add("REGARDING", s_regarding);
                objWriter.Fields.Add("ATT_SEC_REC_ID", iAttSecRecId);
                objWriter.Fields.Add("COMPLETE_TIME", s_completeTime);
                objWriter.Fields.Add("COMPLETE_DATE", s_completeDate);
                objWriter.Fields.Add("DIARY_VOID", iDiaryVoid);
                objWriter.Fields.Add("DIARY_DELETED", iDiaryDeleted);
                objWriter.Fields.Add("NOTIFY_FLAG", iNotifyFlag);
                objWriter.Fields.Add("ROUTE_FLAG", iRouteFlag);
                objWriter.Fields.Add("ESTIMATE_TIME", iEstimateTime);
                objWriter.Fields.Add("AUTO_ID", iAutoId);

                objWriter.Execute();
                //objCommand.Parameters.Clear();
                //objCommand.CommandText = sbSQL.ToString();
                //objCommand.ExecuteNonQuery();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateWPADiary.Error", p_iClientId), p_objException);
            }
            finally
            {
                //sbSQL = null;
                //if (objConn != null)
                //{
                //    if (objConn.State == System.Data.ConnectionState.Open)
                //    {
                //        objConn.Close();
                //    }
                //    objConn.Dispose();
                //}
                objWriter = null;
            }
        }
        /// <summary>
        /// Modify CLAIM_ADJ_ASSIGNMENT Table
        /// </summary>
        /// <param name="p_connString"></param>
        /// <param name="p_adjusterEID"></param>
        /// <param name="p_claimID"></param>
        /// <param name="p_assignmentLvlCodeID"></param>
        /// <param name="p_attachTable"></param>
        /// <param name="p_attachRecordID"></param>
        public static void UpdateAdjAssignment(string p_connString, int p_adjusterEID, int p_claimID, int p_assignmentLvlCodeID, string p_attachTable, int p_attachRecordID, int p_iClientId)
        {
            string sSql = string.Empty;
            Riskmaster.Db.DbConnection objConn = null;
            Riskmaster.Db.DbCommand objCommand = null;
            int iNextUniqueId = 0;
            int iAdjAssignRowID = 0;
            int iAdjRowID = 0;

            try
            {
                //Fetching Claim Adjuster Assignment ID**********************************************************
                sSql = "SELECT CL_ADJ_ASSIGN_ROW_ID FROM CLAIM_ADJ_ASSIGNMENT "
                        + " WHERE ASSIGNMENT_LEVEL_CODE_ID = " + p_assignmentLvlCodeID
                        + " AND ATTACH_TABLE = '" + p_attachTable + "'"
                        + " AND ATTACH_RECORDID = " + p_attachRecordID;
                iAdjAssignRowID = Convert.ToInt32(DbFactory.ExecuteScalar(p_connString, sSql));
                //End Fetching Claim Adjuster Assignment ID******************************************************


                //Fetching Claim Adjuster Row ID*****************************************************************
                if (!int.Equals(p_adjusterEID, 0) && !int.Equals(p_claimID, 0))
                {
                    sSql = String.Format(@"SELECT ADJ_ROW_ID FROM CLAIM_ADJUSTER WHERE ADJUSTER_EID = {0} AND CLAIM_ID = {1}", p_adjusterEID, p_claimID);
                    iAdjRowID = Convert.ToInt32(DbFactory.ExecuteScalar(p_connString, sSql));
                }
                //End Fetching Claim Adjuster Row ID*************************************************************


                //Initializing Connection and Command Objects****************************************************
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                //End Initialization*****************************************************************************


                //Deleting existing record***********************************************************************
                if (!int.Equals(iAdjAssignRowID, 0))
                {
                    sSql = "DELETE FROM CLAIM_ADJ_ASSIGNMENT WHERE CL_ADJ_ASSIGN_ROW_ID = " + iAdjAssignRowID;
                    objCommand.Parameters.Clear();
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                }
                //End Deletion***********************************************************************************


                //Inserting New Record***************************************************************************
                if (!int.Equals(iAdjRowID, 0))
                {
                    iNextUniqueId = Utilities.GetNextUID(p_connString, "CLAIM_ADJ_ASSIGNMENT", p_iClientId);
                    sSql = "INSERT INTO CLAIM_ADJ_ASSIGNMENT (CL_ADJ_ASSIGN_ROW_ID, ADJ_ROW_ID, ASSIGNMENT_LEVEL_CODE_ID, ATTACH_TABLE, ATTACH_RECORDID) "
                        + " VALUES" + " (" + iNextUniqueId + "," + iAdjRowID + "," + p_assignmentLvlCodeID + ",'" + p_attachTable + "'," + p_attachRecordID + ")";

                    objCommand.Parameters.Clear();
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                }
                //End Insertion**********************************************************************************
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateLogForPolicyInterface", p_iClientId), p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }
        //Ankit End

        //avipinsrivas start : Worked for Jira-340
        /// <summary>
        /// Check if ID Exists in table.
        /// </summary>
        /// <param name="pConnString"></param>
        /// <param name="iColumnValue"></param>
        /// <param name="sTableName"></param>
        /// <param name="sColumnName"></param>
        /// <returns>return true if ID exists in Table </returns>
        public static bool IsIDExist(string pConnString, int iColumnValue, string sTableName, string sColumnName)
        {
            bool bReturnVal = false;
            StringBuilder sbQuery = null;
            Dictionary<string, string> objDictParams = null;
            object objCount = null;
            bool bSuccess;
            try
            {
                sbQuery = new StringBuilder();
                objDictParams = new Dictionary<string, string>();

                sbQuery.Append(string.Concat(" SELECT Count(", sColumnName, ") "));
                sbQuery.Append(string.Concat(" FROM  ", sTableName));
                sbQuery.Append(string.Concat(" WHERE ", sColumnName, " = {0} "));

                sbQuery.Replace("{0}", "~COLUMNEVALUE~");

                objDictParams.Add("COLUMNEVALUE", iColumnValue.ToString());

                objCount = DbFactory.ExecuteScalar(pConnString, sbQuery.ToString(), objDictParams);
                if (objCount != null && objCount != System.DBNull.Value && Conversion.CastToType<Int32>(objCount.ToString(), out bSuccess) > 0)
                    bReturnVal = true;
            }
            finally
            {
                sbQuery = null;
                objDictParams = null;
                objCount = null;
            }
            return bReturnVal;
        }
        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        public static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode, int p_iClientId)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", p_iClientId), p_objEx);
            }

        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_iPolicyId">Attribute name</param>
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, int p_iEntityTableId, int p_iClientId)
        {
            try
            {
                XmlElement objChildNode = null;
                CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode, p_iClientId);
                objChildNode.SetAttribute("value", p_iEntityTableId.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", p_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode, int p_iClientId)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode, p_iClientId);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", p_iClientId), p_objEx);
            }
        }
        //avipinsrivas End
        #region Mgaba2: R8: SuperVisory Approval


        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetManagerID
        /// </summary>     
        public static int GetManagerID(int p_iUserId, int p_DataSourceId)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetManagerID(p_iUserId, p_DataSourceId, 0);
            }
            else
                throw new Exception("GetManagerID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// Mgaba2: R8: SuperVisory Approval: Shifted this code from Reserve Worksheet to here
        /// as it is common to ReserveCurrent also
        /// Get the Manager Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        public static int GetManagerID(int p_iUserId, int p_DataSourceId, int p_iClientId)
        {
            int iManagerID = 0;
            StringBuilder sbSQL = null;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID ");
                sbSQL.Append(" FROM USER_DETAILS_TABLE,USER_TABLE");
                sbSQL.Append(" WHERE USER_DETAILS_TABLE.DSNID = " + p_DataSourceId);
                sbSQL.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                sbSQL.Append(" AND USER_TABLE.USER_ID = " + p_iUserId);

                objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), sbSQL.ToString());
                using (objReader)
                {
                    if (objReader.Read())
                    {
                        iManagerID = Convert.ToInt32(objReader.GetValue("MANAGER_ID"));
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetManagerID.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iManagerID;
        }

        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetAdjusterEID
        /// </summary>     
        public static int GetAdjusterEID(int iClaimId, string p_ConnString)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetAdjusterEID(iClaimId, p_ConnString, 0);
            }
            else
                throw new Exception("GetAdjusterEID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Mgaba2: R8: SuperVisory Approval: Shifted this code from Reserve Worksheet to here as it is common to ReserveCurrent also
        /// Get the RM User Id corresponding to Current Adjuster
        /// </summary>
        /// <param name="iClaimId">Claim Id</param>
        /// <param name="p_ConnString">Connection String</param>
        /// <returns></returns>
        public static int GetAdjusterEID(int iClaimId, string p_ConnString, int p_iClientId)
        {
            string sSQL = string.Empty;
            int iCurrentAdjusterID = 0;
            int iRMUserID = 0;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + iClaimId + " AND CURRENT_ADJ_FLAG <> 0";
                objReader = DbFactory.GetDbReader(p_ConnString, sSQL);
                using (objReader)
                {
                    while (objReader.Read())
                    {
                        iCurrentAdjusterID = Convert.ToInt32(objReader.GetValue("ADJUSTER_EID"));
                    }
                }

                if (iCurrentAdjusterID > 0)
                {
                    sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + iCurrentAdjusterID;
                    objReader = DbFactory.GetDbReader(p_ConnString, sSQL);
                    using (objReader)
                    {
                        while (objReader.Read())
                        {
                            iRMUserID = Convert.ToInt32(objReader.GetValue("RM_USER_ID"));
                        }
                    }
                }
                iCurrentAdjusterID = iRMUserID;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetAdjusterEID.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return (iCurrentAdjusterID);
        }

        /// Get the Group Id:Mgaba2: R8: SuperVisory Approval
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        public static int GetGroupID(int iManagerId, string p_sConnString)
        {
            // 12/17/2010 - mcapps2 MITS 22943
            //mcapps2 - The manager may not and most likely will not be in the same group as the last user, we need to get the correct group id
            int iThisGroupID = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iManagerId;
                objReader = DbFactory.GetDbReader(p_sConnString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iThisGroupID = Convert.ToInt32(objReader.GetValue("GROUP_ID"));
                    }
                }
                objReader.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An Error Occured Getting The Group ID For The Manager", p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iThisGroupID;
        }

        /// <summary>
        /// Added for R8 enhancement of Supervisory approval to get claimtype.
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_connString">Connection string to RMX DB</param>
        /// <param name="claim_id">Claim ID</param>
        /// <returns></returns>
        public static int GetClaimType(string p_connString, int p_iClaimId)
        {
            Riskmaster.Db.DbReader objReader = null;
            Riskmaster.Db.DbConnection objConn = null;

            //string sConnectionString = null;
            string sSQL = string.Empty;
            int iClaimTypeCode = 0;
            try
            {
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                sSQL = " SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                objConn.Open();
                objReader = objConn.ExecuteReader(sSQL.ToString());
                if (objReader.Read())
                {
                    iClaimTypeCode = Convert.ToInt32(objReader.GetValue("CLAIM_TYPE_CODE").ToString());

                }
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }

            finally
            {
                objReader.Close();
                objConn.Close();
            }
            return (iClaimTypeCode);
        }

        /// <summary>
        /// Mgaba2: R8: Supervisory Approval
        /// Getting subordinates of a user
        /// </summary>
        /// <param name="p_iManagerId"></param>
        /// <param name="p_DataSourceId"></param>
        /// <param name="p_arrSubordinates"></param>
        public static void GetAllSubordinates(int p_iManagerId, int p_DataSourceId, ref List<string> p_arrSubordinates, int p_iClientId)
        {
            StringBuilder sbSql = null;
            DataSet objDataSet = null;
            try
            {
                sbSql = new StringBuilder();

                sbSql.AppendFormat(@"SELECT USER_TABLE.USER_ID USER_ID FROM USER_TABLE,USER_DETAILS_TABLE  
                                    WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID ={0}  
                                   AND USER_TABLE.MANAGER_ID ={1}", p_DataSourceId, p_iManagerId);

                objDataSet = DbFactory.GetDataSet(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), sbSql.ToString(), p_iClientId);
                if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drMgr in objDataSet.Tables[0].Rows)
                    {
                        p_arrSubordinates.Add(drMgr["USER_ID"].ToString());
                        GetAllSubordinates(Convert.ToInt32(drMgr["USER_ID"]), p_DataSourceId, ref p_arrSubordinates, p_iClientId);
                    }
                }
                else
                    return;
            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetAllSubordinates.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_iRSVHistRowId"></param>
        /// <param name="p_sReason"></param>
        /// <param name="p_iSubmittedTo"></param>
        /// <param name="p_iSubmittedBy"></param>
        /// <param name="p_ResStatusCode"></param>
        /// <param name="p_sConnString"></param>
        //mbahl3 Reserve Approval Enhancement Mits 32471
        public static int SaveToSupHistTable(int p_iRSVHistRowId, int p_iSubmittedTo, int p_iSubmittedBy, int p_iResStatusCode, string p_sConnString, int p_iRcRowId, int p_iOrigStatusCode, double p_dOrigStatusAmount, string p_sEnteredByUser, string p_sDttmRcdAdded, string p_sDateEntered, string p_sApprovedBy, double p_dReserveAmount, int p_iClientId)//mbahl3 Reserve Approval Enhancement Mits 32471 
        {

            DbWriter dbWriter = null;
            int iRSVHistId = 0;
            try
            {
                // StringBuilder sbTempSQL = new StringBuilder();

                //if (p_iSubmittedTo > 0)
                //{

                iRSVHistId = Utilities.GetNextUID(p_sConnString, "RSV_SUP_APPROVAL_HIST", p_iClientId);

                //sbTempSQL.Append("INSERT INTO RSV_SUP_APPROVAL_HIST(RSV_HIST_ROW_ID,RSV_ROW_ID,RES_STATUS_CODE,");
                //sbTempSQL.Append("SUBMITTED_TO,SUBMITTED_BY,DTTM_SUBMISSION) VALUES(");
                //sbTempSQL.AppendFormat("{0},{1},{2},", iRSVHistId, p_iRSVHistRowId, p_ResStatusCode);
                //sbTempSQL.AppendFormat("{0},{1},'{2}'", p_iSubmittedTo, p_iSubmittedBy, Conversion.ToDbDateTime(System.DateTime.Now));
                //sbTempSQL.Append(")");
                // DbFactory.ExecuteNonQuery(p_sConnString, sbTempSQL.ToString());
                dbWriter = DbFactory.GetDbWriter(p_sConnString);

                dbWriter.Tables.Add("RSV_SUP_APPROVAL_HIST");

                dbWriter.Fields.Add("RSV_HIST_ROW_ID", iRSVHistId);
                dbWriter.Fields.Add("RSV_ROW_ID", p_iRSVHistRowId);
                dbWriter.Fields.Add("RES_STATUS_CODE", p_iResStatusCode);
                dbWriter.Fields.Add("SUBMITTED_TO", p_iSubmittedTo);
                dbWriter.Fields.Add("SUBMITTED_BY", p_iSubmittedBy);
                dbWriter.Fields.Add("DTTM_SUBMISSION", Conversion.ToDbDateTime(System.DateTime.Now));
                dbWriter.Fields.Add("RC_ROW_ID", p_iRcRowId);
                dbWriter.Fields.Add("DTTM_RCD_ADDED", p_sDttmRcdAdded);
                dbWriter.Fields.Add("DATE_ENTERED", p_sDateEntered);
                dbWriter.Fields.Add("ORIGINAL_RES_STATUS_CODE", p_iOrigStatusCode);
                dbWriter.Fields.Add("ORIGINAL_RESERVE_AMOUNT", p_dOrigStatusAmount);
                dbWriter.Fields.Add("ADDED_BY_USER", p_sEnteredByUser);
                dbWriter.Fields.Add("APPROVER_ID", p_iSubmittedTo);

                if (!string.IsNullOrEmpty(p_sApprovedBy))
                {
                    dbWriter.Fields.Add("APPROVED_BY", p_sApprovedBy);
                }
                dbWriter.Fields.Add("NEW_RESERVE_AMOUNT", p_dReserveAmount);



                dbWriter.Execute();

            }
            catch (Exception p_objException)
            {
                iRSVHistId = 0;
                throw new RMAppException(Globalization.GetString("CommonFunctions.SaveToSupHistTable.Error", p_iClientId), p_objException);
            }
            finally
            {

                dbWriter = null;
            }
            return iRSVHistId;
        }
        //mbahl3 Reserve Approval Enhancement Mits 32471



        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for CheckReserveLimits
        /// </summary>     
        public static bool CheckReserveLimits(int p_iClaimId, int p_iReserveTypeCode, double p_dAmount, int p_iUserId, int p_iGroupId, int p_iLOB, string p_sConnString)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return CheckReserveLimits(p_iClaimId, p_iReserveTypeCode, p_dAmount, p_iUserId, p_iGroupId, p_iLOB, p_sConnString, 0);
            }
            else
                throw new Exception("CheckReserveLimits" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Mgaba2: R8: SuperVisory Approval:Shifted Code from ReserveCurrent.cs 
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="p_dAmount"></param>
        /// <param name="p_iUserId"></param>
        /// <param name="p_iGroupId"></param>
        /// <param name="p_iLOB"></param>
        /// <param name="p_sConnString"></param>
        /// <returns></returns>
        public static bool CheckReserveLimits(int p_iClaimId, int p_iReserveTypeCode, double p_dAmount, int p_iUserId, int p_iGroupId, int p_iLOB, string p_sConnString, int p_iClientId)
        {
            //double
            double dMaxAmount = 0.0;

            //String Objects
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            bool bReturnValue = true;

            string sConnectionString = p_sConnString;
            bool bUseSupAppReserves = false;
            int iLOBMgr = 0;
            int iCarrierClaims = 0;
            // akaushik5 Added for MITS 30874 Starts
            double dLobMaxAmount = 0.0;
            // akaushik5 Added for MITS 30874 Ends

            try
            {
                sbSQL = new StringBuilder();
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                sbSQL.Append("select STR_PARM_VALUE from PARMS_NAME_VALUE where PARM_NAME like 'MULTI_COVG_CLM'");
                iCarrierClaims = Convert.ToInt32(objConn.ExecuteScalar(sbSQL.ToString()));

                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS,CLAIM ");
                sbSQL.Append(" WHERE (USER_ID = " + p_iUserId);

                if (p_iGroupId != 0)
                    sbSQL.Append(" OR GROUP_ID = " + p_iGroupId);

                sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE");
                sbSQL.Append(" AND (RESERVE_TYPE_CODE=0 OR RESERVE_TYPE_CODE = " + p_iReserveTypeCode + ")");
                //Added by amitosh/shobhana for R8 enhancement of Supervisory Approval
                if (iCarrierClaims == -1)
                {
                    sbSQL.AppendFormat(" AND (RESERVE_LIMITS.CLAIM_TYPE_CODE = 0 OR RESERVE_LIMITS.CLAIM_TYPE_CODE = {0})", Common.CommonFunctions.GetClaimType(sConnectionString, p_iClaimId));
                }

                //end Amitosh


                objReader = objConn.ExecuteReader(sbSQL.ToString());

                if (objReader.Read())
                {
                    if (objReader.GetValue("MAX_AMT") is DBNull)
                        bReturnValue = false;

                    if (bReturnValue)
                    {
                        dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());
                        if (p_dAmount > dMaxAmount)
                            bReturnValue = true;
                        else
                            bReturnValue = false;
                    }
                    objReader.Close();
                }


                //MGaba2: R8: SuperVisory Approval
                //MGaba2:In case LOB Manager is the current user,we need to check his limit :Start
                if (bReturnValue == false)
                {
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append("SELECT USE_SUP_APP_RESERVES FROM CHECK_OPTIONS");
                    objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString());
                    using (objReader)
                    {
                        if (objReader.Read())
                        {
                            bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        }
                    }

                    if (bUseSupAppReserves)
                    {

                        sbSQL.Remove(0, sbSQL.Length);
                        sbSQL.Append("SELECT USER_ID,RESERVE_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + p_iLOB);

                        using (objReader = objConn.ExecuteReader(sbSQL.ToString()))
                        {
                            if (objReader.Read())
                            {
                                iLOBMgr = Convert.ToInt32(objReader.GetValue("USER_ID").ToString());
                                // akaushik5 Changed for MITS 30874 Starts
                                //dMaxAmount = Convert.ToDouble(objReader.GetValue("RESERVE_MAX").ToString());
                                dLobMaxAmount = Convert.ToDouble(objReader.GetValue("RESERVE_MAX").ToString());
                                // akaushik5 Changed for MITS 30874 Ends
                            }
                        }

                        if (p_iUserId == iLOBMgr)
                        {
                            // akaushik5 Added for MITS 30874 Starts
                            dMaxAmount = dLobMaxAmount;
                            // akaushik5 Added for MITS 30874 Ends
                            if (p_dAmount > dMaxAmount)
                                bReturnValue = true;
                            else
                                bReturnValue = false;
                        }
                    }

                    //Asharma326 MITS 30874 For Reserver Limits Starts
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append("SELECT USER_ID,RESERVE_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                    int iTopLevel = 0;
                    using (objReader = objConn.ExecuteReader(sbSQL.ToString()))
                    {
                        if (objReader.Read())
                        {
                            iTopLevel = Convert.ToInt32(objReader.GetValue("USER_ID").ToString());
                        }
                    }

                    if (!bReturnValue && dMaxAmount == 0 && p_iUserId != iTopLevel)
                    {
                        sbSQL.Remove(0, sbSQL.Length);
                        sbSQL.Append("SELECT RES_LMT_USR_FLAG FROM SYS_PARMS_LOB where LINE_OF_BUS_CODE =" + p_iLOB);

                        using (objReader = objConn.ExecuteReader(sbSQL.ToString()))
                        {
                            if (objReader.Read())
                            {
                                bReturnValue = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(objReader.GetValue("RES_LMT_USR_FLAG").ToString()), p_iClientId);
                            }
                        }
                    }
                    //Asharma326 MITS 30874 For Reserver Limits Ends
                }
                objConn.Close();

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CheckReserveLimits.Error", p_iClientId), p_objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();
            }//End finally

            return bReturnValue;
        }

        ///  npadhy implement the optional Client Id logic for Incurred Limits as well
        /// <summary>
        /// Scripting assembly overload for CheckClaimIncurredLimits
        /// </summary>     
        public static bool CheckClaimIncurredLimits(int p_iClaimId, double p_dAmount, int p_holdreservecode,string recoverycode, int p_iUserId, int p_iGroupId, int p_iLOB,bool b_AddRecoveryReservetoTotalIncurredAmount, string p_sConnString)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return CheckClaimIncurredLimits(p_iClaimId, p_dAmount, p_holdreservecode,recoverycode, p_iUserId, p_iGroupId, p_iLOB,b_AddRecoveryReservetoTotalIncurredAmount, p_sConnString, 0);
            }
            else
                throw new Exception("CheckClaimIncurredLimits" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //npadhy  jira RMA-6416

        //Jira 6385- Incurred Limit Start
        public static bool CheckClaimIncurredLimits(int p_iClaimId, double p_dAmount, int p_holdreservecode, string recoverycode, int p_iUserId, int p_iGroupId, int p_iLOB, bool b_AddRecoveryReservetoTotalIncurredAmount, string p_sConnString, int p_iClientId)
        {
            //double
            double dMaxAmount = 0.0;

            //String Objects
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            bool bReturnValue = true;

            string sConnectionString = p_sConnString;
            bool bUseSupAppReserves = false;
            double incurredAmount = 0.0;
            int iLOBMgr = 0;
            int iCarrierClaims = 0;
           
            double dLobMaxAmount = 0.0;
            

            try
            {
                sbSQL = new StringBuilder();
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                sbSQL.Append("select STR_PARM_VALUE from PARMS_NAME_VALUE where PARM_NAME like 'MULTI_COVG_CLM'");
                iCarrierClaims = Convert.ToInt32(objConn.ExecuteScalar(sbSQL.ToString()));

                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM CLAIM_INCURRED_LIMITS,CLAIM ");
                sbSQL.Append(" WHERE (USER_ID = " + p_iUserId);

                if (p_iGroupId != 0)
                    sbSQL.Append(" OR GROUP_ID = " + p_iGroupId);

                sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CLAIM_INCURRED_LIMITS.LINE_OF_BUS_CODE");                
               
                if (iCarrierClaims == -1)
                {
                    sbSQL.AppendFormat(" AND (CLAIM_INCURRED_LIMITS.CLAIM_TYPE_CODE = 0 OR CLAIM_INCURRED_LIMITS.CLAIM_TYPE_CODE = {0})", Common.CommonFunctions.GetClaimType(sConnectionString, p_iClaimId));
                }
                
                
                objReader = objConn.ExecuteReader(sbSQL.ToString());
                
                if (objReader.Read())
                {
                    if (objReader.GetValue("MAX_AMT") is DBNull)
                        bReturnValue = false;

                    if (bReturnValue)
                    {
                        dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());
                        
                        sbSQL.Remove(0, sbSQL.Length);
                        if (b_AddRecoveryReservetoTotalIncurredAmount)
                        { 
                            sbSQL.Append("select SUM(INCURRED_AMOUNT) MAX_INCURRED_AMOUNT from RESERVE_CURRENT where CLAIM_ID = " + p_iClaimId + " and RES_STATUS_CODE not in ( " + p_holdreservecode + ") ");
                            objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString());
                           
                        }
                        else
                        {     // change by sbhatnagar21 for RMA-15305
                            if (!string.IsNullOrEmpty(recoverycode))
                            {
                                sbSQL.Append("select SUM(INCURRED_AMOUNT) MAX_INCURRED_AMOUNT from RESERVE_CURRENT where CLAIM_ID = " + p_iClaimId + " and RES_STATUS_CODE not in ( " + p_holdreservecode + ") and RESERVE_TYPE_CODE not in ('" + recoverycode + "')");
                                objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString());
                            }
                        }
                        if (objReader != null)
                        {
                            using (objReader)
                            {
                                if (objReader.Read())
                                {
                                    if (objReader.GetValue("MAX_INCURRED_AMOUNT") is DBNull)
                                        incurredAmount = 0.0;
                                    else
                                        incurredAmount = Convert.ToDouble(objReader.GetValue("MAX_INCURRED_AMOUNT"));
                                }
                            }
                        }
                        incurredAmount = incurredAmount + p_dAmount;
                       
                        if (incurredAmount > dMaxAmount)
                            bReturnValue = true;
                        else
                            bReturnValue = false;
                    }
                    objReader.Close();
                }


               
                if (bReturnValue == false)
                {
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append("SELECT USE_SUP_APP_RESERVES FROM CHECK_OPTIONS");
                    objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString());
                    using (objReader)
                    {
                        if (objReader.Read())
                        {
                            bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        }
                        objReader.Close();
                    }

                    if (bUseSupAppReserves)
                    {

                        sbSQL.Remove(0, sbSQL.Length);
                        sbSQL.Append("SELECT USER_ID,CLAIM_INCURRED_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + p_iLOB);

                        using (objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString()))
                        {
                            if (objReader.Read())
                            {
                                iLOBMgr = Convert.ToInt32(objReader.GetValue("USER_ID").ToString());

                                if(!(objReader.GetValue("CLAIM_INCURRED_MAX") is DBNull))
                                    dLobMaxAmount = Convert.ToDouble(objReader.GetValue("CLAIM_INCURRED_MAX").ToString());
                               
                            }
                            objReader.Close();
                        }

                        if (p_iUserId == iLOBMgr)
                        {
                            
                            dMaxAmount = dLobMaxAmount;                      
                            if (p_dAmount > dMaxAmount)
                                bReturnValue = true;
                            else
                                bReturnValue = false;
                        }
                    }
                   
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append("SELECT USER_ID,CLAIM_INCURRED_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                    int iTopLevel = 0;
                    using (objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString()))
                    {
                        if (objReader.Read())
                        {
                            iTopLevel = Convert.ToInt32(objReader.GetValue("USER_ID").ToString());
                        }
                        objReader.Close();
                    }

                    if (!bReturnValue && dMaxAmount == 0 && p_iUserId != iTopLevel)
                    {
                        sbSQL.Remove(0, sbSQL.Length);
                        sbSQL.Append("SELECT CLM_INC_LMT_USR_FLAG FROM SYS_PARMS_LOB where LINE_OF_BUS_CODE =" + p_iLOB);

                        using (objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString()))
                        {
                            if (objReader.Read())
                            {
                                bReturnValue = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(objReader.GetValue("CLM_INC_LMT_USR_FLAG").ToString()), p_iClientId);
                            }
                             objReader.Close();
                        }
                    }
                   
                }
                objConn.Close();

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CheckClaimIncurredLimits.Error", p_iClientId), p_objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();
            }

            return bReturnValue;
        }
        //Jira 6385

         ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetLobOrTopLevelManagerID
        /// </summary>     
        public static int GetLobOrTopLevelManagerID(int iLOBCode, bool bNotifyImmediateSup, string p_sConnString, int p_iUserId, double p_dAmount)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetLobOrTopLevelManagerID(iLOBCode, bNotifyImmediateSup, p_sConnString, p_iUserId, p_dAmount, 0);
            }
            else
                throw new Exception("GetLobOrTopLevelManagerID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iLOBCode"></param>
        /// <param name="bNotifyImmediateSup"></param>
        /// <param name="p_sConnString"></param>
        /// <param name="p_iUserId"></param>
        /// <param name="p_dAmount"></param>
        /// <returns></returns>
        public static int GetLobOrTopLevelManagerID(int iLOBCode, bool bNotifyImmediateSup, string p_sConnString, int p_iUserId, double p_dAmount, int p_iClientId)
        {
            string sSQL = string.Empty;
            long lMaxAmount = 0;
            int iApproverID = 0;

            try
            {
                sSQL = "SELECT USER_ID,RESERVE_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;

                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, sSQL))
                {
                    while (objReader.Read())
                    {
                        iApproverID = Convert.ToInt32(objReader.GetValue("USER_ID"));
                        lMaxAmount = Convert.ToInt64(objReader.GetValue("RESERVE_MAX"));


                        if (lMaxAmount < p_dAmount)
                        {
                            //MGaba2:MITS 19099:Reserve Worksheet doesnot reach LOB level Supervisor if amount doesnt lies in his limit
                            //if Immediate supervisor setting is checked and LOB manager is not the current user,return LOB manager
                            //otherwise top level manager
                            if (bNotifyImmediateSup && iApproverID != 0 && p_iUserId != iApproverID)
                            {
                                return iApproverID;
                            }
                            else
                            {
                                iApproverID = 0;
                            }
                        }
                        //}

                        if (iApproverID > 0)
                            return iApproverID;
                    }
                }

                //Get Top level user
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                {
                    if (objReader.Read())
                    {
                        iApproverID = Convert.ToInt32(objReader.GetValue("USER_ID"));
                    }
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetLobOrTopLevelManagerID.Error", p_iClientId), p_objException);
            }
            finally
            {

            }
            return iApproverID;
        }

        #endregion

        ///  npadhy Overload to be called from scripts
        /// <summary>
        /// Scripting assembly overload for GetLobOrTopLevelManagerIDForClaimInc
        /// </summary>     
        public static int GetLobOrTopLevelManagerIDForClaimInc(int iLOBCode, bool bNotifyImmediateSup, string p_sConnString, int p_iUserId, double p_dAmount)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetLobOrTopLevelManagerIDForClaimInc(iLOBCode, bNotifyImmediateSup, p_sConnString, p_iUserId, p_dAmount, 0);
            }
            else
                throw new Exception("GetLobOrTopLevelManagerIDForClaimInc" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }

        //Jira 6385
        public static int GetLobOrTopLevelManagerIDForClaimInc(int iLOBCode, bool bNotifyImmediateSup, string p_sConnString, int p_iUserId, double p_dAmount, int p_iClientId)
        {
            string sSQL = string.Empty;
            long lMaxAmount = 0;
            int iApproverID = 0;

            try
            {
                sSQL = "SELECT USER_ID,CLAIM_INCURRED_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;

                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, sSQL))
                {
                    while (objReader.Read())
                    {
                        iApproverID = Convert.ToInt32(objReader.GetValue("USER_ID"));
                        lMaxAmount = Convert.ToInt64(objReader.GetValue("CLAIM_INCURRED_MAX"));


                        if (lMaxAmount < p_dAmount)
                        {
                            //MGaba2:MITS 19099:Reserve Worksheet doesnot reach LOB level Supervisor if amount doesnt lies in his limit
                            //if Immediate supervisor setting is checked and LOB manager is not the current user,return LOB manager
                            //otherwise top level manager
                            if (bNotifyImmediateSup && iApproverID != 0 && p_iUserId != iApproverID)
                            {
                                return iApproverID;
                            }
                            else
                            {
                                iApproverID = 0;
                            }
                        }
                        //}

                        if (iApproverID > 0)
                            return iApproverID;
                    }
                }

                //Get Top level user
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                {
                    if (objReader.Read())
                    {
                        iApproverID = Convert.ToInt32(objReader.GetValue("USER_ID"));
                    }
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetLobOrTopLevelManagerID.Error", p_iClientId), p_objException);
            }
            finally
            {

            }
            return iApproverID;
        }
        //Jira 6385
        //skhare7 Fns are tranfrd from ReserveWorksheet base for r8 supervisory approval

        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for SendEmail
        /// </summary>     
        public static void SendEmail(string p_sTo, string p_sFrom, string p_sSubject, string p_sBody)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                SendEmail(p_sTo, p_sFrom, p_sSubject, p_sBody, 0);
            }
            else
                throw new Exception("SendEmail" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// This function sends an email given the various parameters
        /// </summary>
        /// <param name="p_sTo">User to whom the email needs to be sent</param>
        /// <param name="p_sFrom">Sending user</param>
        /// <param name="p_sSubject">subject of the mail</param>
        /// <param name="p_sBody">Body of the email</param>
        public static void SendEmail(string p_sTo, string p_sFrom, string p_sSubject, string p_sBody, int p_iClientId)
        {
            Mailer objMailer = null;
            try
            {
                objMailer = new Mailer(p_iClientId);
                objMailer.To = p_sTo;
                objMailer.From = p_sFrom;
                objMailer.Subject = p_sSubject;
                objMailer.Body = p_sBody;

                objMailer.SendMail();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.EmailSending.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objMailer != null)
                    objMailer = null;
            }
        }

        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetUserDetails
        /// </summary>     
        public static void GetUserDetails(string p_sLogInName, string p_sSecConStr, int p_iDSNId, ref string p_sEmail, ref string p_sUserName)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                GetUserDetails(p_sLogInName, p_sSecConStr, p_iDSNId, ref p_sEmail, ref p_sUserName, 0);
            }
            else
                throw new Exception("GetUserDetails" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        /// get the emails address and user name of a user
        /// </summary>
        /// <param name="p_sLogInName">log in name of the user</param>
        /// <param name="p_sSecConStr">security database connection string</param>
        /// <param name="p_iDSNId">DSN id</param>
        /// <param name="p_sEmail">email id to be returned</param>
        /// <param name="p_sUserName">user name to be returned</param>
        public static void GetUserDetails(string p_sLogInName, string p_sSecConStr, int p_iDSNId, ref string p_sEmail,
            ref string p_sUserName, int p_iClientId)
        {
            string sSQL = string.Empty;
            DataSet objDS = null;

            try
            {
                sSQL = "SELECT LAST_NAME, FIRST_NAME, EMAIL_ADDR FROM USER_TABLE, USER_DETAILS_TABLE WHERE "
                    + " USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME = '"
                    + p_sLogInName
                    + "' AND DSNID = " + p_iDSNId;
                objDS = DbFactory.GetDataSet(p_sSecConStr, sSQL, p_iClientId);
                //Vaibhav for Safeway on 03/30/2009: Added condition for checking the dataset contains any row or not
                if (objDS.Tables[0].Rows.Count > 0)
                {
                    p_sEmail = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);
                    p_sUserName = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["FIRST_NAME"]) + " " +
                        Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["LAST_NAME"]);
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GettingUserDetail.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objDS != null)
                    objDS.Dispose();
            }
        }
        //End  skhare7- Fns are tranfrd from ReserveWorksheet base for r8 supervisory approval
        //Neha R8 Funds Utility Enhancement
        /// <summary>
        /// function returns the flag value from the database.
        /// </summary>
        /// <param name="p_sFlagName">name of the flag required</param>
        /// <param name="p_sConnString">security database connection string</param>
        /// <returns>IsScheduleDatesOn</returns>
        public static bool IsScheduleDatesOn(string p_sFlagName, string p_sConnString, int p_iClientId)
        {
            string sSQL = string.Empty;
            bool bIsScheduleDatesOn = false;
            try
            {
                sSQL = "SELECT " + p_sFlagName + " FROM CHECK_OPTIONS";

                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, sSQL))
                {
                    if (objReader.Read())
                    {
                        bIsScheduleDatesOn = objReader.GetBoolean(p_sFlagName);
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.IsScheduleDatesOn.Error", p_iClientId), p_objException);
            }

            return bIsScheduleDatesOn;

        }

        /// <summary>
        /// Function returns the Weekend shcedule.
        /// </summary>
        /// <param name="p_sConnString">security database connection string</param>
        /// <returns>string</returns>
        public static string GetWeekendSchedule(string p_sConnString, int p_iClientId)
        {
            string sSQL = string.Empty;
            string sweekEndSchedule = string.Empty;
            try
            {
                sSQL = "SELECT WEEKEND_SCHEDULE FROM CHECK_OPTIONS";

                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, sSQL))
                {
                    if (objReader.Read())
                    {
                        sweekEndSchedule = objReader.GetString("WEEKEND_SCHEDULE");
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetWeekendSchedule.Error", p_iClientId), p_objException);
            }

            return sweekEndSchedule;

        }
        //Neha end


        /// <summary>
        /// To check  the EFT payments are selected in Payment Parameters Setup
        /// </summary>
        /// <author>Amitosh
        /// </author>
        /// <param name="p_sConnectionString"></param>
        /// <returns></returns>

        public static bool hasEFTBankInfo(int p_iEntityId, int iActiveBankCode, string p_sConnString)
        {
            //LocalCache objCache = null;
            string sBankName = string.Empty;
            bool bReturnValue = false;
            try
            {
                //  objCache = new LocalCache(m_sConnectionString);

                using (DbReader objReader = DbFactory.GetDbReader(p_sConnString, "SELECT BANK_NAME FROM ENTITY_X_BANKING WHERE ENTITY_ID =" + p_iEntityId + "AND BANK_STATUS_CODE = " + iActiveBankCode))
                {
                    if (objReader.Read())
                    {
                        sBankName = objReader.GetValue("BANK_NAME").ToString();


                        bReturnValue = true;
                    }

                }

            }
            catch
            {
            }
            finally
            {
                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //}
            }
            return bReturnValue;
        }
        public static double ExchangeRateSrc2Dest(int iSourceCurrCode, int iDestCurrCode, string p_sConnString, int p_iClientId)
        {
            string sKey = String.Format("{0}_{1}_{2}", iSourceCurrCode, iDestCurrCode, p_sConnString);

            try
            {
                if (IsUseMultiCurrency(p_sConnString, p_iClientId) != 0)
                {
                    if (iSourceCurrCode == 0)
                        iSourceCurrCode = GetBaseCurrencyCode(p_sConnString);
                    if (iDestCurrCode == 0)
                        iDestCurrCode = GetBaseCurrencyCode(p_sConnString);

                    if (m_Cache != null)
                    {
                        if (m_Cache.ContainsKey(sKey))
                        {
                            return Conversion.ConvertObjToDouble(m_Cache[sKey], p_iClientId);
                        }
                        else
                        {
                            return GetExchangeRate(iSourceCurrCode, iDestCurrCode, p_sConnString, sKey, p_iClientId);
                        }
                    }
                    else
                    {
                        return GetExchangeRate(iSourceCurrCode, iDestCurrCode, p_sConnString, sKey, p_iClientId);
                    }
                }
                else
                {
                    return 1;
                }
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.ExchangeRateError", p_iClientId), e);
            }

        }

        //start:rupal,multicurrency
        private static int IsUseMultiCurrency(string p_sConnString, int p_iClientId)
        {
            string sKey = String.Format("{0}_{1}", "UseMultiCurrency", p_sConnString);
            bool bOut = false;
            if (m_Cache != null)
            {
                if (m_Cache.ContainsKey(sKey))
                {
                    return Conversion.CastToType<int>(Conversion.ConvertObjToStr(m_Cache[sKey]), out bOut);
                }
            }
            return GetMultiCurrencyValue(p_sConnString, sKey, p_iClientId);
        }

        private static int GetMultiCurrencyValue(string p_sConnString, string sKey, int p_iClientId)
        {
            int iReturnValue = 0;
            bool bOut = false;
            if (m_Cache == null)
            {
                m_Cache = new CacheTable(p_iClientId);
            }
            object oUseMultiCurr = DbFactory.ExecuteScalar(p_sConnString, string.Format("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_MULTI_CURRENCY'"));
            if (oUseMultiCurr != null)
            {
                m_Cache.Add(sKey, oUseMultiCurr);
                iReturnValue = Conversion.CastToType<int>(Conversion.ConvertObjToStr(oUseMultiCurr), out bOut);
            }
            return iReturnValue;
        }
        //end:rupal

        private static double GetExchangeRate(int iSourceCurrCode, int iDestCurrCode, string p_sConnString, string sKey, int p_iClientId)
        {

            object oExhRate = DbFactory.ExecuteScalar(p_sConnString, string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iSourceCurrCode, iDestCurrCode));
            if (oExhRate == null || Convert.ToDouble(oExhRate) == 0)
            {
                object oSrcCurr = DbFactory.ExecuteScalar(p_sConnString, string.Format("SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID={0}", iSourceCurrCode));
                object oDestCurr = DbFactory.ExecuteScalar(p_sConnString, string.Format("SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID={0}", iDestCurrCode));
                throw new RMAppException(string.Format(Globalization.GetString("CommonFunctions.ExchangeRateError", p_iClientId), Conversion.ConvertObjToStr(oSrcCurr), Conversion.ConvertObjToStr(oDestCurr)));
            }
            else
            {
                m_Cache = new CacheTable(p_iClientId);
                m_Cache.Add(sKey, oExhRate);
                return Conversion.ConvertObjToDouble(oExhRate, p_iClientId);

            }
        }
        public static int GetBaseCurrencyCode(string sConnString)
        {
            object oBaseCurrCode = DbFactory.ExecuteScalar(sConnString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='BASE_CURRENCY_CODE'");
            return int.Parse(Conversion.ConvertObjToStr(oBaseCurrCode));
        }
        public static int GetClaimCurrencyCode(int iClaimId, string sConnString)
        {
            object oClaimCurrCode = DbFactory.ExecuteScalar(sConnString, "SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID =" + iClaimId);
            return int.Parse(Conversion.ConvertObjToStr(oClaimCurrCode));
        }
        public static int GetPaymentCurrencyCode(int iTransId, string sConnString)
        {
            object oPmtCurrCode = DbFactory.ExecuteScalar(sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + iTransId);
            //MITS 31704 asingh263 starts
            if (oPmtCurrCode == null)
            {
                oPmtCurrCode = GetBaseCurrencyCode(sConnString);
            }
            //MITS 31704 asingh263 ends
            return int.Parse(Conversion.ConvertObjToStr(oPmtCurrCode));
        }
        public static string ConvertByCurrencyCode(int iCurrCode, double p_dAmt, string p_sConnString, int p_iClientId)
        {
            string sRet = string.Format("{0:c}", p_dAmt);
            // akaushik5 Changed for MITS 35846 Starts
            //object sCulture = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iCurrCode);
            string sKey = string.Format("CUL_{0}", iCurrCode);
            object sCulture = null;
            if (object.ReferenceEquals(m_Cache, null))
            {
                m_Cache = new CacheTable(p_iClientId);
            }

            if (m_Cache.ContainsKey(sKey))
            {
                sCulture = m_Cache[sKey];
            }
            else
            {
                sCulture = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iCurrCode);
                m_Cache.Add(sKey, sCulture);
            }
            // akaushik5 Changed for MITS 35846 Ends

            string sCurrCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (!string.IsNullOrEmpty(Convert.ToString(sCulture)))
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Convert.ToString(sCulture).Split('|')[1]);
                sRet = string.Format("{0:c}", p_dAmt);
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCurrCulture);
            }
            return sRet;
        }


        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for CreateClaimActivityLog
        /// </summary>     
        public static void CreateClaimActivityLog(string p_connString, string p_userName, string p_ClaimId, int p_ActType, string p_LogText)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                CreateClaimActivityLog(p_connString, p_userName, p_ClaimId, p_ActType, p_LogText, 0);
            }
            else
                throw new Exception("CreateClaimActivityLog" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486        
        /// <summary>
        /// Create the database entry for the claim activity log.
        /// </summary>
        /// <param name="p_connString">database connection string</param>
        /// <param name="p_userName">user id</param>
        /// <param name="p_ClaimId">claim id</param>
        /// <param name="p_ActType">activity type</param>
        /// <param name="p_LogText">Log text</param>
        public static void CreateClaimActivityLog(string p_connString, string p_userName, string p_ClaimId, int p_ActType, string p_LogText, int p_iClientId)
        {
            string sSql = string.Empty;
            Riskmaster.Db.DbConnection objConn = null;
            Riskmaster.Db.DbCommand objCommand = null;
            bool bInSuccess = false;
            try
            {

                string[] arrClaimIds = p_ClaimId.Split(',');
                for (int i = 0; i < arrClaimIds.Length; i++)
                {
                    objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    sSql = string.Concat("INSERT INTO CLAIM_ACTIVITY_LOG(CLAIM_ID, ACTIVITY_TYPE_CODE, ADDED_BY_USER, DTTM_RCD_ADDED, LOG_TEXT) VALUES("
                                        , Conversion.CastToType<int>(arrClaimIds[i].ToString(), out bInSuccess), ",", p_ActType, ",'", p_userName, "',", Conversion.ToDbDateTime(DateTime.Now), ",'", p_LogText.Replace("'", "''"), "')");
                    objCommand.Parameters.Clear();
                    objCommand.CommandText = sSql;
                    objCommand.ExecuteNonQuery();
                }

            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateClaimActivityLog", p_iClientId),
                    p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }

        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for bClaimActLogSetup
        /// </summary>     
        public static bool bClaimActLogSetup(string p_connString, int p_iTableId, int p_iOperationId, ref string p_sLogText, ref int p_iActType)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return bClaimActLogSetup(p_connString, p_iTableId, p_iOperationId, ref p_sLogText, ref p_iActType, 0);
            }
            else
                throw new Exception("bClaimActLogSetup" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486  

        /// <summary>
        /// Verify the claim activity log setup for current table and operation.
        /// </summary>
        /// <param name="p_connString">database connection string</param>
        /// <param name="p_iTableId">Table id of the physical table</param>
        /// <param name="p_iOperationId">Operation performed on table</param>
        /// <param name="p_sLogText">Log text saved in utilities by user</param>
        /// <param name="p_iActType">Activity type mentioned in utilities by user</param>
        /// <returns>True/False based upon setup defined in utilities</returns>
        public static bool bClaimActLogSetup(string p_connString, int p_iTableId, int p_iOperationId, ref string p_sLogText, ref int p_iActType, int p_iClientId)
        {
            string sSql = string.Empty;
            Riskmaster.Db.DbConnection objConn = null;
            Riskmaster.Db.DbCommand objCommand = null;
            Riskmaster.Db.DbReader objReader = null;
            bool bInSucess = false;
            bool bLogActivity = false;
            try
            {
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                sSql = string.Concat("SELECT LOG_TEXT, ACTIVITY_TYPE_CODE FROM CLAIM_ACT_LOG_DEF WHERE TABLE_ID = ", p_iTableId, "AND OPERATION_CODE =", p_iOperationId);
                objReader = Riskmaster.Db.DbFactory.GetDbReader(p_connString, sSql);
                if (objReader.Read())
                {
                    p_sLogText = objReader.GetValue("LOG_TEXT").ToString();
                    p_iActType = Conversion.CastToType<int>(objReader.GetValue("ACTIVITY_TYPE_CODE").ToString(), out bInSucess);
                    bLogActivity = true;
                }
                objReader.Close();

                return bLogActivity;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.bClaimActLogSetup", p_iClientId),
                    p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }
        /// <summary>
        /// Created By: Deb Jena
        /// </summary>
        /// <param name="iGlobalId">Any Global Unique ID</param>
        /// <param name="dAmt">Amount to be converted to other currency</param>
        /// <param name="eNavType">Navigation Type</param>
        /// <param name="p_sConnString">Main DB Connection</param>
        /// <returns></returns>
        public static string ConvertCurrency(int iGlobalId, double dAmt, NavFormType eNavType, string p_sConnString, int p_iClientId)
        {
            string sRet = string.Format("{0:c}", dAmt);
            //NavFormType NavType = (NavFormType)Enum.Parse(typeof(NavFormType),sNavType);
            switch (eNavType)
            {
                case NavFormType.None:
                    object iBaseCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT STR_PARM_VALUE  FROM PARMS_NAME_VALUE WHERE PARM_NAME='BASE_CURRENCY_CODE'");
                    if (Convert.ToInt32(iBaseCurrCode) > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sBaseCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sBaseCurr.ToString().Split('|')[1]);
                            sRet = string.Format("{0:c}", dAmt);
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        }
                    }
                    break;
                case NavFormType.Reserve:
                    //object iRClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT CLAIM_CURR_CODE FROM  RESERVE_CURRENT WHERE CLAIM_CURR_CODE <> 0 AND CLAIM_ID=" + iGlobalId);
                    //Here as per the requirement we will make code changes to optimize it
                    object iRClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + iGlobalId);
                    if (Convert.ToInt32(iRClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iRClaimCurrCode.ToString());
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sRet = string.Format("{0:c}", dAmt);
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        }
                    }
                    break;
                case NavFormType.Auto:
                    object iAClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS_AUTO WHERE AUTO_TRANS_ID=" + iGlobalId);
                    if (Convert.ToInt32(iAClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iAClaimCurrCode.ToString());
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sRet = string.Format("{0:c}", dAmt);
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        }
                    }
                    break;
                case NavFormType.Claim:
                    object iCClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + iGlobalId);
                    if (Convert.ToInt32(iCClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iCClaimCurrCode.ToString());
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sRet = string.Format("{0:c}", dAmt);
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        }
                    }
                    break;
                case NavFormType.Funds:
                    // akaushik5 Added for MITS 35846 Starts
                    //object iFClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + iGlobalId);
                    string sKey = string.Format("CLAIMCURRCODE_{0}", iGlobalId);
                    object iFClaimCurrCode;
                    //if (object.ReferenceEquals(m_Cache, null))
                    //{
                    //    m_Cache = new CacheTable();
                    //}
                    m_Cache = m_Cache ?? new CacheTable(p_iClientId);

                    if (m_Cache.ContainsKey(sKey))
                    {
                        iFClaimCurrCode = m_Cache[sKey];
                    }
                    else
                    {
                        iFClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + iGlobalId);
                        m_Cache.Add(sKey, iFClaimCurrCode);
                    }
                    // akaushik5 Added for MITS 35846 Ends
                    if (Convert.ToInt32(iFClaimCurrCode) > 0)
                    {
                        // akaushik5 Changed for MITS 35846 Starts
                        //object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iFClaimCurrCode.ToString());
                        object sClaimCurr = null;
                        sKey = string.Format("CODEDESC_{0}", iFClaimCurrCode);
                        if (m_Cache.ContainsKey(sKey))
                        {
                            sClaimCurr = m_Cache[sKey];
                        }
                        else
                        {
                            sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iFClaimCurrCode.ToString());
                            m_Cache.Add(sKey, sClaimCurr);
                        }
                        // akaushik5 Changed for MITS 35846 Ends
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sRet = string.Format("{0:c}", dAmt);
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        }
                    }
                    break;
            }
            return sRet;

        }
        /// <summary>
        /// Created By: skhare7 to find the short code
        /// </summary>
        /// <param name="iGlobalId">Any Global Unique ID</param>
        /// <param name="dAmt">Amount to be converted to other currency</param>
        /// <param name="p_sConnString">Main DB Connection</param>
        /// <returns></returns>

        public static string ConvertCurrencyInShortCode(int iGlobalId, double dAmt, string p_sConnString, int p_iClientId)
        {
            string sRet = string.Empty;
            string sClaimCurr = string.Empty;
            string sShortCode = string.Empty;

            object iFClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + iGlobalId);
            if (Convert.ToInt32(iFClaimCurrCode) > 0)
            {
                DataSet dsCurr = DbFactory.GetDataSet(p_sConnString, "SELECT CODE_DESC,SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iFClaimCurrCode.ToString(), p_iClientId);
                if (dsCurr != null && dsCurr.Tables[0].Rows.Count != 0)
                {
                    sClaimCurr = dsCurr.Tables[0].Rows[0][0].ToString();
                    sShortCode = dsCurr.Tables[0].Rows[0][1].ToString();
                }

                if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                    string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                    sRet = string.Format("{0:c}", dAmt);
                    NumberFormatInfo format = new CultureInfo(sCulture).NumberFormat;
                    sShortCode = sShortCode.Replace(format.CurrencySymbol, "");
                    sRet = sRet.Replace(format.CurrencySymbol, sShortCode);
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                }
            }
            return sRet;

        }



        // Manish Jain to get Currency Culture
        public static string GetCulture(int iGlobalId, NavFormType eNavType, string p_sConnString)
        {
            // string sRet = string.Format("{0:c}", dAmt);
            //NavFormType NavType = (NavFormType)Enum.Parse(typeof(NavFormType),sNavType);
            string sCulture = string.Empty;
            switch (eNavType)
            {
                case NavFormType.None:
                    object iBaseCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT STR_PARM_VALUE  FROM PARMS_NAME_VALUE WHERE PARM_NAME='BASE_CURRENCY_CODE'");
                    if (Convert.ToInt32(iBaseCurrCode) > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                        //string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sBaseCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sBaseCurr.ToString().Split('|')[1]);
                            sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                            //sRet = string.Format("{0:c}", dAmt);

                        }
                    }
                    break;
                case NavFormType.Reserve:
                    object iRClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT CLAIM_CURR_CODE FROM  RESERVE_CURRENT WHERE CLAIM_CURR_CODE <> 0 AND CLAIM_ID=" + iGlobalId);
                    if (Convert.ToInt32(iRClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iRClaimCurrCode.ToString());
                        //string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            //sRet = string.Format("{0:c}", dAmt);
                            sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    break;
                case NavFormType.Auto:
                    object iAClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS_AUTO WHERE AUTO_TRANS_ID=" + iGlobalId);
                    if (Convert.ToInt32(iAClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iAClaimCurrCode.ToString());
                        //string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    break;
                case NavFormType.Claim:
                    object iCClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + iGlobalId);
                    if (Convert.ToInt32(iCClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iCClaimCurrCode.ToString());

                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    break;
                case NavFormType.Funds:
                    object iFClaimCurrCode = DbFactory.ExecuteScalar(p_sConnString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + iGlobalId);
                    if (Convert.ToInt32(iFClaimCurrCode) > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(p_sConnString, "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iFClaimCurrCode.ToString());

                        if (!string.IsNullOrEmpty(sClaimCurr.ToString()))
                        {
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sClaimCurr.ToString().Split('|')[1]);
                            sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    break;
            }
            return sCulture;
        }
        //rupal:start, policy system interface changes
        public static string GetSiteUnitDetails(string p_sConnString, int iSiteId)
        {
            string sReturnValue = string.Empty;
            try
            {
                using (DbReader objReader = DbFactory.ExecuteReader(p_sConnString, "SELECT NAME FROM SITE_UNIT WHERE SITE_ID=" + iSiteId))
                {
                    if (objReader.Read())
                    {
                        sReturnValue = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
            }
            catch (Exception e)
            {
            }
            return sReturnValue;
        }

        public static string GetOtherUnitDetails(string p_sConnString, int iOtherUnitId, string pDBType)
        {
            string sReturnValue = string.Empty;
            string sSql = string.Empty;
            try
            {
                //changes for MITS 31997 start
                if (pDBType == Constants.DB_SQLSRVR)
                    //dbisht6 start for mits 35655
                    //sSql = "SELECT  ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'') FROM ENTITY,OTHER_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=" + iOtherUnitId;
                    sSql = string.Format("SELECT  POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) Unit FROM ENTITY,OTHER_UNIT, POINT_UNIT_DATA WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID = {0} AND POINT_UNIT_DATA.UNIT_ID = OTHER_UNIT.OTHER_UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE  and OTHER_UNIT.UNIT_TYPE = 'SU'", iOtherUnitId);
                //dbisht6 end
                else if (pDBType == Constants.DB_ORACLE)
                    //dbisht6 start for mits 35655
                    sSql = string.Format(" SELECT  POINT_UNIT_DATA.STAT_UNIT_NUMBER || ' - ' || nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) Unit FROM ENTITY,OTHER_UNIT, POINT_UNIT_DATA WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID = {0} AND POINT_UNIT_DATA.UNIT_ID = OTHER_UNIT.OTHER_UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE AND OTHER_UNIT.UNIT_TYPE = 'SU'", iOtherUnitId);
                //sSql ="SELECT nvl(ENTITY.FIRST_NAME,'') || ' ' || nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,'') FROM ENTITY,OTHER_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=" + iOtherUnitId;
                //dbisht6 end
                using (DbReader objReader = DbFactory.ExecuteReader(p_sConnString, sSql)) //"SELECT  ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'') FROM ENTITY,OTHER_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=" + iOtherUnitId))
                //changes for MITS 31997 end
                {
                    if (objReader.Read())
                    {
                        sReturnValue = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
            }
            catch (Exception e)
            {
            }
            return sReturnValue;
        }



        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetRCRowId
        /// </summary>     
        public static int GetRCRowId(string p_sConnString, int iClaimId, int iClaimantEid, int iUnitId, int iReserveType, int iPolCovRowId, int iLossCode, bool bCarrier)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetRCRowId(p_sConnString, iClaimId, iClaimantEid, iUnitId, iReserveType, iPolCovRowId, iLossCode, bCarrier, 0);
            }
            else
                throw new Exception("GetRCRowId" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            //throw new RMAppException("GetAdjusterEID" + "Globalization.GetString("Conversion.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486 

        public static int GetRCRowId(string p_sConnString, int iClaimId, int iClaimantEid, int iUnitId, int iReserveType, int iPolCovRowId, int iLossCode, bool bCarrier, int p_iClientId)
        {
            int iRcRowId = 0;
            string sSQL = string.Empty;

            try
            {
                if (bCarrier)
                    sSQL = "SELECT RC_ROW_ID FROM RESERVE_CURRENT,COVERAGE_X_LOSS WHERE RESERVE_CURRENT.CLAIM_ID = " + iClaimId + " AND RESERVE_CURRENT.UNIT_ID=" + iUnitId + " AND RESERVE_CURRENT.CLAIMANT_EID=" + iClaimantEid + " AND RESERVE_CURRENT.RESERVE_TYPE_CODE=" + iReserveType + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + iPolCovRowId + " AND COVERAGE_X_LOSS.LOSS_CODE = " + iLossCode + " AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";
                else
                    sSQL = "SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE RESERVE_CURRENT.CLAIM_ID = " + iClaimId + " AND RESERVE_CURRENT.UNIT_ID=" + iUnitId + " AND RESERVE_CURRENT.CLAIMANT_EID=" + iClaimantEid + " AND RESERVE_CURRENT.RESERVE_TYPE_CODE=" + iReserveType;


                using (DbReader objReader = DbFactory.ExecuteReader(p_sConnString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iRcRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return iRcRowId;
        }

        ///  //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetCovRowIdFromRCRowId
        /// </summary>     
        public static int GetCovRowIdFromRCRowId(string p_sConnString, int p_iRCRowId)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetCovRowIdFromRCRowId(p_sConnString, p_iRCRowId, 0);
            }
            else
                throw new Exception("GetCovRowIdFromRCRowId" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486 

        public static int GetCovRowIdFromRCRowId(string p_sConnString, int p_iRCRowId, int p_iClientId)
        {
            int iPolCvgRowId = 0;
            string sSQL = string.Empty;

            try
            {

                sSQL = "SELECT COVERAGE_X_LOSS.POLCVG_ROW_ID FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RESERVE_CURRENT.RC_ROW_ID=" + p_iRCRowId;


                using (DbReader objReader = DbFactory.ExecuteReader(p_sConnString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iPolCvgRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return iPolCvgRowId;
        }


        public static string GetWcPolicyEmployeeDetails(string p_sConnString)
        {
            string sReturnValue = string.Empty;
            try
            {
                using (DbReader objReader = DbFactory.ExecuteReader(p_sConnString, "SELECT EMPLOYEE_DETAILS FROM WC_POLICY_EMPLOYEE"))
                {
                    if (objReader.Read())
                    {
                        sReturnValue = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
            }
            catch (Exception e)
            {
            }
            return sReturnValue;
        }

        //smishra54: Policy Interface
        public static bool IsValueExistsInArray(string[] arrValues, string value)
        {
            bool isExist = false;
            foreach (string oValue in arrValues)
            {
                if (string.Equals(oValue, value))
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }

        public static int GetUniqueRandomNumber()
        {
            int iURN = 0;
            Random oRandomGenerator = new Random((int)DateTime.Now.Ticks);
            iURN = oRandomGenerator.Next();

            return iURN;
        }
        //smishra54:End
        //srajindersin - Pentesting - Cross site Scripting - MITS 27683//skhare7 Auto Merge issue
        public static string HTMLCustomEncode(string inputString)
        {
            string encodedString = string.Empty;

            // encodedString = inputString.Replace("<", "").Replace(">", "");

            //return Microsoft.Security.Application.Encoder.HtmlEncode(encodedString);
            // encodedString = Microsoft.Security.Application.Encoder.HtmlEncode(encodedString);
            encodedString = System.Web.HttpUtility.HtmlEncode(inputString);

            while (encodedString.Contains("&amp;"))
                encodedString = encodedString.Replace("&amp;", "&");

            return encodedString;
        }
        //END srajindersin - Pentesting - Cross site Scripting - MITS 27683

        public static string HTMLCustomDecode(string inputString)
        {
            string DecodedString = string.Empty;

            DecodedString = System.Web.HttpUtility.HtmlDecode(inputString);

            return DecodedString;
        }
        public static int GetPolCovSeq(int iClaimId, int iClaimantEid, int iCoverageLossId, string sConnectionString)
        {
            StringBuilder sbSQL = null;
            DataView dv = null;
            DataSet objDataSet = null;
            DataTable dtDataTable = null;
            int iMax = 0;
            int iPolicyCvgSeqNo = 0;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT RC.RC_ROW_ID,RC.POLCVG_LOSS_ROW_ID,RC.CLAIMANT_EID,PT.COVERAGE_TYPE_CODE, RC.POLICY_CVG_SEQNO as PolicyCvgSeqNo FROM");
                sbSQL.Append(" RESERVE_CURRENT RC, COVERAGE_X_LOSS CL,POLICY_X_CVG_TYPE PT WHERE RC.CLAIM_ID = " + iClaimId + " and RC.CLAIMANT_EID = " + iClaimantEid + "");
                sbSQL.Append(" AND CL.CVG_LOSS_ROW_ID = RC.POLCVG_LOSS_ROW_ID AND CL.POLCVG_ROW_ID = PT.POLCVG_ROW_ID  ORDER BY RC.POLICY_CVG_SEQNO DESC");
                objDataSet = DbFactory.GetDataSet(sConnectionString, sbSQL.ToString(), 0);
                if (objDataSet != null && objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0) 
                {
                    dv = new DataView();
                    dv = objDataSet.Tables[0].DefaultView;
                    dv.RowFilter = "POLCVG_LOSS_ROW_ID =" + iCoverageLossId;
                    dtDataTable = dv.ToTable();
                    if (dv != null && dv.Count > 0)
                    {
                        iPolicyCvgSeqNo = Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(dtDataTable.Rows[0]["PolicyCvgSeqNo"]));
                    }
                    else
                    {
                        dv.RowFilter = string.Empty;
                        dtDataTable = dv.ToTable();
                        iMax = Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(dtDataTable.Rows[0]["PolicyCvgSeqNo"]));
                        iPolicyCvgSeqNo = iMax + 1;
                    }

                   
                }
                else
                {
                    iPolicyCvgSeqNo = 1;
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                objDataSet = null;
                dv = null;
                dtDataTable = null;
                sbSQL = null;
            }
            return iPolicyCvgSeqNo;
        }
        public static void CreateLogForPolicyInterface(string p_connString, int iActivityRowId, string p_userName, int p_iClaimId, int p_iForeignTableId, int p_iForeignTableKey, int iCheckBatchId, int iPolicySystemId, int iActivityType, int iAccountId, double iReserveAmount, int iReserveStatus, double iChangeAmount, int iCheckStatus, int iVoidFlag, int iIsCollection, bool bSetUploadToZero, int p_iClientId,int iMoveHistKey,int iMoveHistTableId,string sUploadData) //aaggarwal29 : MITS 35586 changes
        {
            string sSql = string.Empty;
            Riskmaster.Db.DbConnection objConn = null;
            int iNextUniqueId = 0;

            try
            {

                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                if (iActivityRowId == 0)
                {
                    iNextUniqueId = Utilities.GetNextUID(p_connString, "ACTIVITY_TRACK", p_iClientId);

                    sSql = "INSERT INTO ACTIVITY_TRACK (ACTIVITY_ROW_ID,CLAIM_ID,FOREIGN_TABLE_ID,FOREIGN_TABLE_KEY,UPLOAD_FLAG,DTTM_RCD_ADDED,ADDED_BY_USER,CHECK_BATCH_ID,POLICY_SYSTEM_ID,ACTIVITY_TYPE,ACCOUNT_ID,RESERVE_AMOUNT,RESERVE_STATUS,CHANGE_AMOUNT,CHECK_STATUS,VOID_FLAG,IS_COLLECTION,MOVE_HIST_TABLE_ID,MOVE_HIST_TABLE_KEY,ADDITIONAL_UPL_DATA) VALUES" + " (" + iNextUniqueId + "," + p_iClaimId + "," + p_iForeignTableId + "," + p_iForeignTableKey + ",-1,'" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + p_userName + "'," + iCheckBatchId + "," + iPolicySystemId + "," + iActivityType + "," + iAccountId + "," + iReserveAmount + " , " + iReserveStatus + "," + iChangeAmount + "," + iCheckStatus + "," + iVoidFlag + "," + iIsCollection + "," + iMoveHistTableId + "," + iMoveHistKey + ",'" + sUploadData + "'" + ")";
                }
                else
                {
                    //aaggarwal29 : MITS 35586 start
                    if (bSetUploadToZero)
                    {
                        sSql = "UPDATE ACTIVITY_TRACK SET CLAIM_ID = " + p_iClaimId + " ,FOREIGN_TABLE_ID = " + p_iForeignTableId + " ,FOREIGN_TABLE_KEY=" + p_iForeignTableKey + ",UPLOAD_FLAG=0,CHECK_BATCH_ID=" + iCheckBatchId + " ,POLICY_SYSTEM_ID=" + iPolicySystemId + " ,ACTIVITY_TYPE=" + iActivityType + " ,ACCOUNT_ID=" + iAccountId + ",RESERVE_AMOUNT=" + iReserveAmount + ",RESERVE_STATUS=" + iReserveStatus + ",CHANGE_AMOUNT=" + iChangeAmount + ",CHECK_STATUS=" + iCheckStatus + ",VOID_FLAG=" + iVoidFlag + ",MOVE_HIST_TABLE_ID=" + iMoveHistTableId + ",MOVE_HIST_TABLE_KEY=" + iMoveHistKey + ",ADDITIONAL_UPL_DATA='" + sUploadData + "'  WHERE ACTIVITY_ROW_ID = " + iActivityRowId;
                    }
                    else
                    {
                        sSql = "UPDATE ACTIVITY_TRACK SET CLAIM_ID = " + p_iClaimId + " ,FOREIGN_TABLE_ID = " + p_iForeignTableId + " ,FOREIGN_TABLE_KEY=" + p_iForeignTableKey + ",UPLOAD_FLAG=-1,CHECK_BATCH_ID=" + iCheckBatchId + " ,POLICY_SYSTEM_ID=" + iPolicySystemId + " ,ACTIVITY_TYPE=" + iActivityType + " ,ACCOUNT_ID=" + iAccountId + ",RESERVE_AMOUNT=" + iReserveAmount + ",RESERVE_STATUS=" + iReserveStatus + ",CHANGE_AMOUNT=" + iChangeAmount + ",CHECK_STATUS=" + iCheckStatus + ",VOID_FLAG=" + iVoidFlag + ",MOVE_HIST_TABLE_ID=" + iMoveHistTableId + ",MOVE_HIST_TABLE_KEY=" + iMoveHistKey + ",ADDITIONAL_UPL_DATA='" + sUploadData + "' WHERE ACTIVITY_ROW_ID = " + iActivityRowId;
                    }
                }

                //aaggarwal29 : MITS 35586 end

                objConn.ExecuteNonQuery(sSql);


            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateLogForPolicyInterface", p_iClientId),
                    p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }

        public static void CreateLogForPolicyInterface(DbConnection objConn, DbTransaction objTrans , int iActivityRowId, string p_userName, int p_iClaimId, int p_iForeignTableId, int p_iForeignTableKey, int iCheckBatchId, int iPolicySystemId, int iActivityType, int iAccountId, double iReserveAmount, int iReserveStatus, double iChangeAmount, int iCheckStatus, int iVoidFlag, int iIsCollection, bool bSetUploadToZero, int p_iClientId, int iMoveHistKey, int iMoveHistTableId,string sUploadData) //aaggarwal29 : MITS 35586 changes
        {
         
            int iNextUniqueId = 0;
            DbWriter objWriter = null;

            try
            {

                //objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objWriter = DbFactory.GetDbWriter(objConn);


                objWriter.Tables.Add("ACTIVITY_TRACK");
                
                objWriter.Fields.Add("CLAIM_ID", p_iClaimId);

                objWriter.Fields.Add("FOREIGN_TABLE_ID  ", p_iForeignTableId);

                objWriter.Fields.Add("FOREIGN_TABLE_KEY  ", p_iForeignTableKey);
            
              
                objWriter.Fields.Add("CHECK_BATCH_ID  ", iCheckBatchId);
                objWriter.Fields.Add("POLICY_SYSTEM_ID  ", iPolicySystemId);
                objWriter.Fields.Add("ACTIVITY_TYPE  ", iActivityType);




                objWriter.Fields.Add("ACCOUNT_ID  ", iAccountId);
                objWriter.Fields.Add("RESERVE_AMOUNT  ", iReserveAmount);
                objWriter.Fields.Add("RESERVE_STATUS  ", iReserveStatus);

                objWriter.Fields.Add("CHANGE_AMOUNT  ", iChangeAmount);
                objWriter.Fields.Add("CHECK_STATUS  ", iCheckStatus);
                objWriter.Fields.Add("VOID_FLAG  ", iVoidFlag);
                objWriter.Fields.Add("IS_COLLECTION  ", iIsCollection);


                objWriter.Fields.Add("MOVE_HIST_TABLE_ID  ", iMoveHistTableId);
                objWriter.Fields.Add("MOVE_HIST_TABLE_KEY  ", iMoveHistKey);

                if (!string.IsNullOrEmpty(sUploadData))
                {
                    objWriter.Fields.Add("ADDITIONAL_UPL_DATA", sUploadData);
                }
                if (iActivityRowId == 0)
                {
                    objWriter.Fields.Add("UPLOAD_FLAG  ", -1);
                    objWriter.Fields.Add("DTTM_RCD_ADDED  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                    objWriter.Fields.Add("ADDED_BY_USER  ", p_userName);

                    iNextUniqueId = Utilities.GetNextUID(objConn, "ACTIVITY_TRACK", objTrans, p_iClientId);
                    objWriter.Fields.Add("ACTIVITY_ROW_ID", iNextUniqueId);
                }
                else
                {
                    if (bSetUploadToZero)
                    {
                        objWriter.Fields.Add("UPLOAD_FLAG  ", 0);
                    }
                    else
                    {
                        objWriter.Fields.Add("UPLOAD_FLAG  ", -1);
                    }

                    objWriter.Where.Add("ACTIVITY_ROW_ID=" + iActivityRowId);
                }
                objWriter.Execute(objTrans);



            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateLogForPolicyInterface", p_iClientId),
                    p_objExp);
            }
            finally
            {
                objWriter = null;
            }
        }
        //Start averma62 MITS 32386 - rmA FAS Integration
        public static void CreateLogForFASInterface(string p_connString, int iRowId, int p_iClaimId, int p_iClientId)
        {
            string sSql = string.Empty;
            Riskmaster.Db.DbConnection objConn = null;
            int iNextUniqueId = 0;

            try
            {
                objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
                objConn.Open();
                if (iRowId == 0)
                {
                    iNextUniqueId = Utilities.GetNextUID(p_connString, "RMA_FAS_CLAIM_EXP", p_iClientId);
                    sSql = "INSERT INTO RMA_FAS_CLAIM_EXP (ROW_ID,CLAIM_ID,FAS_CLAIM_FLAG, DTTM_RCD_LAST_UPD) VALUES" + " (" + iNextUniqueId + "," + p_iClaimId + "," + 0 + "," + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ")";
                }
                else
                {
                    sSql = "UPDATE RMA_FAS_CLAIM_EXP SET CLAIM_ID = " + p_iClaimId + " ,FAS_CLAIM_FLAG = " + 0 + " ,DTTM_RCD_LAST_UPD=" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + " WHERE ROW_ID=" + iRowId;
                }
                objConn.ExecuteNonQuery(sSql);
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.CreateLogForFASInterface", p_iClientId), p_objExp);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
        }
        //End averma62 MITS 32386 - rmA FAS Integration
        public static int GetLossType(string p_connString, int iRcRowId, ref int p_iDisabilityCat, ref int p_iCvgLossId, int p_iClientId)
        {
            int iLossCode = 0;

            string sSQL = "SELECT LOSS_CODE,DISABILITY_CAT,CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS,RESERVE_CURRENT WHERE RC_ROW_ID = " + iRcRowId + " AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID";
            using (DbReader objReader = DbFactory.GetDbReader(p_connString, sSQL))
            {
                if (objReader.Read())
                {
                    iLossCode = Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);
                    p_iDisabilityCat = Conversion.ConvertObjToInt(objReader.GetValue(1), p_iClientId);
                    p_iCvgLossId = Conversion.ConvertObjToInt(objReader.GetValue(2), p_iClientId);
                }
            }
            return iLossCode;
        }

        /// <summary>
        /// Prepares the query for fetching the codes based on the language code of the user who has logged in.
        /// </summary>
        /// Created by Amandeep Kaur 24/12/2012
        /// <returns>XmlDocument</returns>
        public static StringBuilder PrepareMultilingualQuery(string p_sSQL, string p_sTableName, int p_iLangCode)
        {
            int iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
            StringBuilder sReturn = new StringBuilder();
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            string sOrderByCmd = string.Empty;
            int iIndex = p_sSQL.IndexOf("ORDER BY");
            string p_sSQLNew = string.Empty;
            string sGroupCmd = string.Empty;
            string[] arrOrderByQueries = null;
            string sOrderBy = string.Empty;
            string sOrderByQuery = string.Empty;
            int iOrderIndex = -1;
            p_sSQLNew = p_sSQL;
            if (iIndex > 0)
            {
                sOrderByCmd = p_sSQL.Substring(iIndex);
                p_sSQL = p_sSQL.Replace(sOrderByCmd, "");

                if (!string.IsNullOrEmpty(sOrderByCmd))
                {
                    arrOrderByQueries = sOrderByCmd.Split(',');
                    int i = 1;
                    foreach (string sQuery in arrOrderByQueries)
                    {
                        sOrderByQuery = sQuery.Replace("ORDER BY", "");
                        sOrderByQuery = sOrderByQuery.Trim();
                        iOrderIndex = p_sSQL.IndexOf(sOrderByQuery, 0);
                        sOrderByQuery = p_sSQL.Substring(iOrderIndex, sOrderByQuery.Length);
                        p_sSQL = p_sSQL.Remove(iOrderIndex, sOrderByQuery.Length);
                        p_sSQL = p_sSQL.Insert(iOrderIndex, sOrderByQuery + " CODE" + i);
                        sOrderBy = sOrderBy + sQuery.Replace(sOrderByQuery, " CODE" + i) + ",";
                        i++;
                    }
                }
                p_sSQLNew = p_sSQL;
            }
            int iGpByIndex = p_sSQL.IndexOf("GROUP BY");
            if (iGpByIndex > 0)
            {
                sGroupCmd = p_sSQLNew.Substring(iGpByIndex);
                p_sSQLNew = p_sSQLNew.Replace(sGroupCmd, "");
            }
            if (!p_sTableName.Contains(','))
            {
                sReturn.Append(p_sSQL.Replace("1033", p_iLangCode.ToString()) + " UNION " + p_sSQLNew.Replace("1033", sBaseLangCode) + @" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID FROM CODES INNER JOIN CODES_TEXT
                                          ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "') AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode.ToString() + ")");
            }
            else
            {
                sReturn.Append(p_sSQL.Replace("1033", p_iLangCode.ToString()) + " UNION " + p_sSQLNew.Replace("1033", sBaseLangCode) + @" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID FROM CODES INNER JOIN CODES_TEXT
                                          ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES.TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME IN ( " + p_sTableName + " ) ) AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode.ToString() + ")");
            }
            if (iGpByIndex > 0)
            {
                sReturn.Append(sGroupCmd);
            }
            if (iIndex > 0)
            {
                //sReturn.Append(sOrderByCmd);
                string sComma = sOrderBy.Substring(sOrderBy.Length - 1, 1);
                if (!string.IsNullOrEmpty(sComma))
                {
                    sOrderBy = sOrderBy.Substring(0, sOrderBy.Length - 1);
                }
                sReturn.Append(sOrderBy);
            }

            return sReturn;
        }

        /// <summary>
        /// Create Temporary Table for Codes and Codes Text with appropriate Language code. In case a specific code is not available then we have the fallback to pick that code of BaseLanguageCode
        /// Earlier implementation required developers to either have to specify the table names in From and Join condition in Where (Could not use Joins) or use Union inside the query
        /// Which not only made queries difficult to comprehend but also had a significant performance hit.
        /// </summary>
        /// <param name="p_sConnString">Base Riskmaster Connection String</param>
        /// <param name="p_sGlossaryTableName">Name of System Table Name in Glossary</param>
        /// <param name="p_sTempTableName">Unique Alias for the GlossaryTableName. If a query has two joins to same code tables, though the Glossary System Table Name would be same for both, but we need to pass different alias of both. Otherwise the second will drop the first. </param>
        /// <param name="p_iLangCode">The Language Code of the User</param>
        /// <param name="p_iClientId">The Client Id in case of Cloud else 0</param>
        /// <returns>Name of the Temporary table created.</returns>
        public static string CreateTempTable4MultiLingual(string p_sConnString, string p_sGlossaryTableName, string p_sTempTableName, int p_iLangCode, int p_iClientId)
        {
            StringBuilder sbSQL = null;
            StringBuilder sbSQLFromWhere = null;
            string sCurrenDate = String.Empty;
            DbConnection objConn = null;
            int iBaseLangCode = 0;
            string sTempTable = string.Empty;

            try
            {
                iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                sCurrenDate = Conversion.ToDbDate(DateTime.Now);
                sbSQL = new StringBuilder();
                sbSQLFromWhere = new StringBuilder();
                sbSQL.Append("SELECT CODES.CODE_ID, CODES_TEXT.SHORT_CODE, CODE_DESC, CODES.RELATED_CODE_ID  ");
                sbSQLFromWhere.Append(" FROM GLOSSARY INNER JOIN CODES ON GLOSSARY.TABLE_ID = CODES.TABLE_ID INNER JOIN CODES_TEXT");
                sbSQLFromWhere.Append(" ON CODES.CODE_ID = CODES_TEXT.CODE_ID AND GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sGlossaryTableName + "' AND CODES.DELETED_FLAG<>-1");
                sbSQLFromWhere.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                try
                {
                    sTempTable = string.Format("TEMP_{0}", p_sTempTableName);
                    if (objConn == null)
                    {
                        objConn = DbFactory.GetDbConnection(p_sConnString);
                    }
                    objConn.Open();
                    objConn.ExecuteNonQuery("DROP TABLE " + sTempTable);
                }
                catch (Exception p_objEx)
                {
                }
                string sDBType = string.Empty;
                sDBType = objConn.DatabaseType.ToString();
                if (sDBType != Constants.DB_SQLSRVR)
                {
                    string sOracleCreate = string.Empty;
                    string sOracleSelect = sbSQL.ToString();
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sOracleSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode);
                    sbSQL.Append(")");
                    sOracleCreate = sbSQL.ToString();
                    sOracleCreate = "CREATE TABLE " + sTempTable + " AS (" + sOracleCreate;
                    sbSQL = null;
                    sbSQL = new StringBuilder();
                    sbSQL.Append(sOracleCreate);
                    sbSQL.Append(")");
                }
                else
                {
                    string sSqlSelect = sbSQL.ToString();
                    sbSQL.Append(" INTO " + sTempTable + " ");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sSqlSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode);
                    sbSQL.Append(")");
                }
                objConn.ExecuteNonQuery(sbSQL.ToString());
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(string.Format(Globalization.GetString("CommonFunctions.CreateTempTable4MultiLingual", p_iClientId), p_sGlossaryTableName) , p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
            return sTempTable;
        }

        /// <summary>
        /// Update the error message according to the base language code 
        /// </summary>
        /// <param name="p_sKeyValue"></param>
        /// <returns></returns>
        public static string FilterBusinessMessage(string p_sKeyValue)
        {
            try
            {
                string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                return FilterBusinessMessage(p_sKeyValue, sBaseLangCode);
            }
            catch
            {
                return p_sKeyValue;
            }
        }
        /// <summary>
        /// Update the error message according to the language code
        /// </summary>
        /// <param name="p_sKeyValue"></param>
        /// <param name="p_sLangCode"></param>
        /// <returns></returns>
        public static string FilterBusinessMessage(string p_sKeyValue, string p_sLangCode)
        {
            string[] sDataSeparator = { "~*~" };
            string[] ArrDataSeparator = { "|^|" };
            string[] ArrErrMsg = p_sKeyValue.Split(sDataSeparator, StringSplitOptions.None);
            string sReturnMsg = string.Empty;
            string sKeyId = string.Empty;
            try
            {
                if (!p_sKeyValue.Contains("|^|"))
                {
                    return p_sKeyValue;
                }
                string sLangCode = p_sLangCode;
                foreach (string s in ArrErrMsg)
                {
                    if (s.Contains(sLangCode))
                    {
                        sReturnMsg = s;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(sReturnMsg))
                {
                    sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                    foreach (string s in ArrErrMsg)
                    {
                        if (s.Contains(sLangCode))
                        {
                            sReturnMsg = s;
                            break;
                        }
                    }
                }
                string[] sLangSeparator = { sLangCode };
                string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                if (sArr.Length > 1)
                {
                    sKeyId = sArr[0].Replace("|^|", "");
                    sReturnMsg = sArr[sArr.Length - 1].Replace("|^|", "");
                }

            }
            catch (Exception ee)
            {

            }
            return (!string.IsNullOrEmpty(sKeyId) ? sReturnMsg : p_sKeyValue);
        }
        /// <summary>
        /// FilterBusinessMessageRetainErrorCode
        /// </summary>
        /// <param name="p_sKeyValue"></param>
        /// <param name="p_sLangCode"></param>
        /// <returns></returns>
        public static string FilterBusinessMessageRetainErrorCode(string p_sKeyValue, string p_sLangCode)
        {
            string[] sDataSeparator = { "~*~" };
            string[] ArrDataSeparator = { "|^|" };
            string[] ArrErrMsg = p_sKeyValue.Split(sDataSeparator, StringSplitOptions.None);
            string sReturnMsg = string.Empty;
            string sKeyId = string.Empty;
            try
            {
                if (!p_sKeyValue.Contains("|^|"))
                {
                    return p_sKeyValue;
                }
                string sLangCode = p_sLangCode;
                if (sLangCode.Length != 4) sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                foreach (string s in ArrErrMsg)
                {
                    if (s.Contains(sLangCode))
                    {
                        sReturnMsg = s;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(sReturnMsg))
                {
                    sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                    foreach (string s in ArrErrMsg)
                    {
                        if (s.Contains(sLangCode))
                        {
                            sReturnMsg = s;
                            break;
                        }
                    }
                }
            }
            catch (Exception ee)
            {

            }
            return (!string.IsNullOrEmpty(sReturnMsg) ? sReturnMsg : p_sKeyValue);
        }
        //Asharma326 MITS 27586  Starts
        /// <summary>
        /// Func For Generating Random password
        /// </summary>
        /// <param name="length">length of password</param>
        /// <returns>Password</returns>
        public static string GetRandomPassword(int length)
        {
            char[] chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();
            string password = string.Empty;
            Random random = new Random();

            for (int i = 0; i < length; i++)
            {
                int x = random.Next(1, chars.Length);
                //Don't Allow Repetation of Characters
                if (!password.Contains(chars.GetValue(x).ToString()))
                    password += chars.GetValue(x);
                else
                    i--;
            }
            return password;
        }
        //Asharma326 MITS 27586  Ends
        //MITS:34082 MultiCurrency START
        public static int iGetReserveCurrencyCode(string sCurrencyKey, string sConnString, int p_iClientId)
        {
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(sCurrencyKey))
                return 0;
            else
            {
                //rkotak:jira 642
                strDictParams.Add("CurrencyType", sCurrencyKey);
                object oResCurrCode = DbFactory.ExecuteScalar(sConnString, "SELECT CODE_ID FROM CODES WHERE SHORT_CODE = ~CurrencyType~ AND TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='CURRENCY_TYPE')", strDictParams);
                strDictParams = null;
                return Conversion.ConvertObjToInt(oResCurrCode, p_iClientId);
                //object oResCurrCode = DbFactory.ExecuteScalar( (sConnString, "SELECT CODE_ID FROM Codes WHERE SHORT_CODE LIKE" + "'%" + sCurrencyKey + "%'");
                //return int.Parse(Conversion.ConvertObjToStr(oResCurrCode));
            }
        }

        public static string sGetCurrencyShortCode(int iCurrencyID, string sConnString)
        {
            object oResCurrShortCode = DbFactory.ExecuteScalar(sConnString, "SELECT SHORT_CODE  FROM Codes WHERE CODE_ID =" + iCurrencyID);
            return Convert.ToString(Conversion.ConvertObjToStr(oResCurrShortCode));
        }
        public static string sGetCurrencyRateEffectDate(int iSourceCurrCode, int iDestCurrCode, string sConnString)
        {
            //Added:Yukti, DT:06/11/2014, JIRA Defect 202
            object oCurrRateEffDate = null;
            eDatabaseType eDbtype = DbFactory.GetDatabaseType(sConnString);
            if (eDbtype == eDatabaseType.DBMS_IS_ORACLE)
            {
                oCurrRateEffDate = DbFactory.ExecuteScalar(sConnString, "SELECT TO_CHAR(TO_DATE(effective_date, 'yyyymmdd'),'YYYY-MM-DD') from CURRENCY_RATE where DESTINATION_CURRENCY_CODE =" + iDestCurrCode + " AND SOURCE_CURRENCY_CODE = " + iSourceCurrCode);
            }
            else
            {
                oCurrRateEffDate = DbFactory.ExecuteScalar(sConnString, "SELECT CONVERT(Date, EFFECTIVE_DATE, 101) from CURRENCY_RATE where DESTINATION_CURRENCY_CODE =" + iDestCurrCode + " AND SOURCE_CURRENCY_CODE = " + iSourceCurrCode);
            }
            //oCurrRateEffDate = DbFactory.ExecuteScalar(sConnString, "SELECT CONVERT(Date, EFFECTIVE_DATE, 101) from CURRENCY_RATE where DESTINATION_CURRENCY_CODE =" + iDestCurrCode + " AND SOURCE_CURRENCY_CODE = " + iSourceCurrCode);
            //Ended:YUkti, DT:06/11/2014
            string sEffDate = Convert.ToString(Conversion.ConvertObjToStr(oCurrRateEffDate));
            return sEffDate;
        }
        //MITS:34082 MultiCurrency END

        // akaushik5 Added for MITS 35846 Starts
        /// <summary>
        /// Gets the org level field from code identifier.
        /// </summary>
        /// <param name="codeId">The code identifier.</param>
        /// <returns>Returns the org level field from code identifier</returns>
        public static string GetOrgLevelFieldFromCodeId(int codeId)
        {
            string sOrgLevelField = string.Empty;
            if (codeId != default(int))
            {
                switch (codeId)
                {
                    case 1005: sOrgLevelField = "CLIENT_EID"; break;
                    case 1006: sOrgLevelField = "COMPANY_EID"; break;
                    case 1007: sOrgLevelField = "OPERATION_EID"; break;
                    case 1008: sOrgLevelField = "REGION_EID"; break;
                    case 1009: sOrgLevelField = "DIVISION_EID"; break;
                    case 1010: sOrgLevelField = "LOCATION_EID"; break;
                    case 1011: sOrgLevelField = "FACILITY_EID"; break;
                    case 1012: sOrgLevelField = "DEPARTMENT_EID"; break;
                }
            }
            return sOrgLevelField;
        }
        // akaushik5 Added for MITS 35846 Ends

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We need to retrieve multiple files at a time. So we pass the filter from UI
        // For DI modules we need to save Module and Document Type as well which will be used to retrieve the files

        //Add by kuladeep for store file in database temp Storage Start-Jira 527
        /// <summary>
        /// Function for retreive file data from database.
        /// </summary>
        /// <param name="sFileName"></param>
        /// <param name="sFilePath"></param>
        /// <param name="sConnectionString"></param>
        /// <returns></returns>
        public static MemoryStream RetreiveTempFileFromDB(string sFileName, string sFilePath, string sConnectionString, int p_iClientId,
            StorageType objStorageType = StorageType.Temporary, string p_sFilter = "") //mbahl3 
        {
            ITempFileInterface objFile = null;
            Dictionary<string, string> parms = null;
            byte[] bFileContent = null;
            MemoryStream objMemoryStream = null;
            bool bCloudDeployed = false;
            string sCloudTempLocation = string.Empty;
            StorageTypeEnumeration.TempStorageType enumStoageType;

            GetCloudSetting(ref bCloudDeployed, ref sCloudTempLocation);

            try
            {
                if (bCloudDeployed)
                {
                    enumStoageType = (StorageTypeEnumeration.TempStorageType)Enum.Parse(typeof(StorageTypeEnumeration.TempStorageType), sCloudTempLocation);
                    objFile = TempFileStorageFactory.CreateInstance(enumStoageType);
                    if (objFile != null)
                    {
                        parms = new Dictionary<string, string>();

                        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                        // If the Filter is not coming then use the FileName as Filter
                        // Otherwise use the filter being passed
                        if (p_sFilter == string.Empty)
                            parms.Add("sqlFilter", string.Format("FILENAME= '{0}'", sFileName));
                        else
                            parms.Add("sqlFilter", p_sFilter);
                        parms.Add("ConnString", sConnectionString);

                        if (objStorageType == StorageType.Permanent) //mbahl3
                        {
                            parms.Add("StorageType", "Permanent");

                        }

                        bFileContent = objFile.Get(parms);
                        objMemoryStream = new MemoryStream(bFileContent);
                    }
                }
                return objMemoryStream;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.RetreiveTempFileFromDB", p_iClientId), p_objEx);
            }
            finally
            {
                objFile = null;
                parms = null;
                objFile = null;
            }
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud

        /// <summary>
        /// Retrieve multiple files from DB
        /// </summary>
        /// <param name="sFileName">Name of the File</param>
        /// <param name="sFilePath">Path of the file</param>
        /// <param name="sConnectionString">DB against which the query shall be executed. Can be Mail Db or DA Staging DB</param>
        /// <param name="p_iClientId">Client Id. 0 for base installation and Client Id for Cloud Deployment</param>
        /// <param name="objStorageType">Type of Storage. Based on this criteria we will execute the query against appropriate table. If StorageType is temporary, then we need
        ///                             to retrieve files from TEMP_DOCUMENT_STORAGE table otherwise we need to retrieve files from PMT_DOCUMENT_STORAGE tab;e                            
        /// </param>
        /// <param name="p_sFilter">The Filter that shall be used to filter the files. If the Filter is blank then we will use the FileName as filter, 
        ///                         otherwise we will used the filter passed
        /// </param>
        /// <returns>The Collection of StreamedUploadDocumentType objects which contains the FileName, FileContents, ModuleName, RelatedId and DocumentType </returns>
        public static List<StreamedUploadDocumentType> RetrieveMultipleTempFileFromDB(string sFileName, string sFilePath, string sConnectionString, int p_iClientId,
            StorageType objStorageType = StorageType.Temporary, string p_sFilter = "")
        {
            ITempFileInterface objFile = null;
            Dictionary<string, string> parms = null;
            List<StreamedUploadDocumentType> importFiles = null;
            bool bCloudDeployed = false;
            string sCloudTempLocation = string.Empty;
            StorageTypeEnumeration.TempStorageType enumStoageType;
            GetCloudSetting(ref bCloudDeployed, ref sCloudTempLocation);

            try
            {
                if (bCloudDeployed)
                {
                    enumStoageType = (StorageTypeEnumeration.TempStorageType)Enum.Parse(typeof(StorageTypeEnumeration.TempStorageType), sCloudTempLocation);
                    objFile = TempFileStorageFactory.CreateInstance(enumStoageType);
                    if (objFile != null)
                    {
                        parms = new Dictionary<string, string>();
                        if (p_sFilter == string.Empty)
                            parms.Add("sqlFilter", string.Format("FILENAME= '{0}'", sFileName));
                        else
                            parms.Add("sqlFilter", p_sFilter);
                        parms.Add("ConnString", sConnectionString);

                        if (objStorageType == StorageType.Permanent) //mbahl3
                        {
                            parms.Add("StorageType", "Permanent");
                        }

                        importFiles = objFile.GetMultiple(parms);
                    }
                }
                return importFiles;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.RetreiveTempFileFromDB", p_iClientId), p_objEx);
            }
            finally
            {
                objFile = null;
                parms = null;
                objFile = null;
            }
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        /// <summary>
        /// Delete multiple or single file from DB
        /// </summary>
        /// <param name="sConnectionString">Connection string against which the Query shall be executed. Could be main DB or DA staging DB</param>
        /// <param name="p_iClientId">Client Id. 0 for base installation and Client Id for Cloud Deployment</param>
        /// <param name="objStorageType">Type of Storage. Based on this criteria we will execute the query against appropriate table. If StorageType is temporary, then we need
        ///                             to retrieve files from TEMP_DOCUMENT_STORAGE table otherwise we need to retrieve files from PMT_DOCUMENT_STORAGE tab;e                            
        /// </param>
        /// <param name="p_sFilter">The Filter that shall be used to filter the files. If the Filter is blank then we will use the FileName as filter, 
        ///                         otherwise we will used the filter passed
        /// </param>
        /// <returns>If file deletion is successful the true else false</returns>
        public static bool DeleteFilesFromDB(string sConnectionString, int p_iClientId, StorageType objStorageType = StorageType.Temporary, string p_sFilter = "")
        {
            ITempFileInterface objFile = null;
            Dictionary<string, string> parms = null;
            bool bCloudDeployed = false;
            string sCloudTempLocation = string.Empty;
            StorageTypeEnumeration.TempStorageType enumStoageType;
            GetCloudSetting(ref bCloudDeployed, ref sCloudTempLocation);
            bool bFielsDeleted = false;
            try
            {
                if (bCloudDeployed)
                {
                    enumStoageType = (StorageTypeEnumeration.TempStorageType)Enum.Parse(typeof(StorageTypeEnumeration.TempStorageType), sCloudTempLocation);
                    objFile = TempFileStorageFactory.CreateInstance(enumStoageType);
                    if (objFile != null)
                    {
                        parms = new Dictionary<string, string>();
                        parms.Add("sqlFilter", p_sFilter);
                        parms.Add("ConnString", sConnectionString);

                        if (objStorageType == StorageType.Permanent)
                        {
                            parms.Add("StorageType", "Permanent");
                        }

                        bFielsDeleted = objFile.Remove(parms);
                    }
                }
                return bFielsDeleted;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.DeleteTempFileFromDB", p_iClientId), p_objEx);
            }
            finally
            {
                objFile = null;
                parms = null;
                objFile = null;
            }
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        /// <summary>
        /// Function for insert file binary bytes into database.
        /// </summary>
        /// <param name="sFileName">Name of the File</param>
        /// <param name="sFilePath">Path where teh file exists. sFilePath + sFileName is the complete path of the File</param>
        /// <param name="sConnectionString"></param>
        /// <param name="p_iClientId">Client Id. 0 for base installation and Client Id for Cloud Deployment</param>
        /// <param name="objStorageType">Type of Storage. Based on this criteria we will execute the query against appropriate table. If StorageType is temporary, then we need
        ///                             to retrieve files from TEMP_DOCUMENT_STORAGE table otherwise we need to retrieve files from PMT_DOCUMENT_STORAGE tab;e                            
        /// </param>
        /// <param name="p_sModuleName">The Name of the Module corresponding to file. As of now this property is used only in case of DA signifying ISO, MMSEA etc</param>
        /// <param name="p_curFileType">The Type of file.  As of now this property is used only in case of DA signifying Import, Attachment. 
        ///                             This will be used to retrieve only the files required for a particular operation 
        /// </param>
        /// <param name="p_iRelatedId">The Id with which the file is associated. For DA it shall be optionset Id</param>
        public static void InsertTempFileIntoDB(string sFileName, string sFilePath, string sConnectionString, int p_iClientId,
            StorageType objStorageType = StorageType.Temporary, string p_sModuleName = "", FileType p_curFileType = FileType.None, int p_iRelatedId = 0) //mbahl3 
        {
            ITempFileInterface objFile = null;
            Dictionary<string, string> parms = null;
            byte[] file = null;
            FileStream stream = null;
            bool bCloudDeployed = false;
            string sCloudTempLocation = string.Empty;
            StorageTypeEnumeration.TempStorageType enumStoageType;


            GetCloudSetting(ref bCloudDeployed, ref sCloudTempLocation);

            try
            {
                if (bCloudDeployed)
                {
                    enumStoageType = (StorageTypeEnumeration.TempStorageType)Enum.Parse(typeof(StorageTypeEnumeration.TempStorageType), sCloudTempLocation);
                    objFile = TempFileStorageFactory.CreateInstance(enumStoageType);
                    if (objFile != null)
                    {
                        parms = new Dictionary<string, string>();
                        parms.Add("filename", sFileName);
                        parms.Add("ConnString", sConnectionString);
                        if (objStorageType == StorageType.Permanent) //mbahl3
                        {
                            parms.Add("StorageType", "Permanent");

                        }
                        parms.Add("ModuleName", p_sModuleName);
                        parms.Add("DocumentType", p_curFileType.ToString());
                        parms.Add("RelatedId", p_iRelatedId.ToString());
                        stream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read);
                        //file = new byte[stream.Length];
                        //stream.Read(file, 0, (int)stream.Length);
						//stream.Close();
                        objFile.Add(parms, stream);
                        //objFile.Add(parms, file);
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.InsertTempFileIntoDB", p_iClientId), p_objEx);
            }
            finally
            {
                objFile = null;
                parms = null;
                objFile = null;
            }
        }
        /// <summary>
        /// Function for getting Cloud setting for web.config
        /// </summary>
        /// <param name="bCloudDeployed"></param>
        /// <param name="sCloudTempLocation"></param>
        public static void GetCloudSetting(ref bool bCloudDeployed, ref string sCloudTempLocation)
        {
            //Hashtable htCloudSetting = RMConfigurationManager.GetDictionarySectionSettings("Cloud");
            //if (htCloudSetting != null)
            //{
            //    if (htCloudSetting["CloudDeployed"] != null)
            //    {
            //        if (string.Equals(Convert.ToString(htCloudSetting["CloudDeployed"]), "True"))
            //        {
            //            bCloudDeployed = true;
            //        }
            //    }
            //    if (htCloudSetting["CloudTempLocation"] != null)
            //    {

            //        sCloudTempLocation = Convert.ToString(htCloudSetting["CloudTempLocation"]);
            //    }
            //}

            bool bIsSucess = false;
            bCloudDeployed = Conversion.CastToType<bool>(ConfigurationManager.AppSettings["CloudDeployed"], out bIsSucess);
            sCloudTempLocation = ConfigurationManager.AppSettings["CloudTempLocation"];

        }
        //Add by kuladeep for store file in database temp Storage End

        /// <summary>
        /// Function for MultiTenantConnection.
        /// //Add & change by kuladeep for Cloud
        /// </summary>
        public static Dictionary<int, MultiClientConnection> MultiTenantConnection(int p_iClientId)
        {
            DbConnection objConn = null;
            string sSQL = string.Empty;
            string sConnectionTenantSecurity = string.Empty;
            Dictionary<string, string> dictConnString = new Dictionary<string, string>();
            Dictionary<int, MultiClientConnection> dictMultiTenant = new Dictionary<int, MultiClientConnection>();
            MultiClientConnection ClientConnection;
            int iClientId = 0;
            try
            {
                //Fetch all ClientId and connection for run all Utility Clientwise.
                if (p_iClientId != 0)
                    sSQL = "SELECT Client_id,RMXSecurity,TaskManagerDataSource FROM Client_Detail where Client_id=" + p_iClientId + "";
                else
                    sSQL = "SELECT Client_id,RMXSecurity,TaskManagerDataSource FROM Client_Detail";

                sConnectionTenantSecurity = RMConfigurationManager.GetConnectionString("rmATenantSecurity", iClientId);
                objConn = DbFactory.GetDbConnection(sConnectionTenantSecurity);
                objConn.Open();
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        //ClientConnection = new MultiClientConnection();
                        ClientConnection.TMConnectionString = objReader.GetString("TaskManagerDataSource");
                        ClientConnection.SecConnectionString = objReader.GetString("RMXSecurity");
                        iClientId = objReader.GetInt32("Client_id");
                        dictMultiTenant.Add(iClientId, ClientConnection);
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.MultiTenantConnection", iClientId), p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
            }
            return dictMultiTenant;
        }
        /// <summary>
        /// 
        /// </summary>
        public struct MultiClientConnection
        {
            public string TMConnectionString;
            public string SecConnectionString;
        }

        //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
        public static bool IsEntityIDNumberExists(string p_connString, int p_idNumRowID, string p_sTableName, string p_iEntityIDNumber, int p_iEntityIDType)
        {
            bool IsEntituIDNumberExists = false;
            Riskmaster.Db.DbCommand objCommand = null;
            Riskmaster.Db.DbParameter objParam = null;
            Riskmaster.Db.DbConnection objConn = Riskmaster.Db.DbFactory.GetDbConnection(p_connString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            string sql = string.Format("SELECT ID_NUM_ROW_ID FROM {0} WHERE ID_TYPE = {1} AND ID_NUM = ~PIDNUM~ AND DELETED_FLAG = 0", p_sTableName, p_iEntityIDType);
            if (p_idNumRowID != -1)
                sql += string.Format(" AND ID_NUM_ROW_ID <> {0}", p_idNumRowID);
            objCommand.Parameters.Clear();
            objParam = objCommand.CreateParameter();
            objParam.Direction = System.Data.ParameterDirection.Input;
            objParam.Value = p_iEntityIDNumber;
            objParam.ParameterName = "PIDNUM";
            objParam.SourceColumn = "ID_NUM";
            objCommand.Parameters.Add(objParam);
            objCommand.CommandText = sql;
            using (DbReader objReader = objCommand.ExecuteReader())
            {
                while (objReader.Read())
                {
                    IsEntituIDNumberExists = true;
                }
            }
            return IsEntituIDNumberExists;
        }
        //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination

        //smishra54: MITS 35932
        //Logical representation of Unit Type Indicator to avoid hard coding
        public static Constants.UNIT_TYPE_INDICATOR? GetUnitTypeIndicator(string sUnitType)
        {
            Constants.UNIT_TYPE_INDICATOR? UnitTypeInd;

            switch (sUnitType.ToUpper())
            {
                case "V":
                case "VEHICLE":
                    UnitTypeInd = Constants.UNIT_TYPE_INDICATOR.VEHICLE;
                    break;
                case "LIABILITY":
                case "P":
                case "BUILDING":
                    UnitTypeInd = Constants.UNIT_TYPE_INDICATOR.PROPERTY;
                    break;
                case "S":
                    UnitTypeInd = Constants.UNIT_TYPE_INDICATOR.SITE;
                    break;
                case "SU":
                    UnitTypeInd = Constants.UNIT_TYPE_INDICATOR.STAT;
                    break;
                default:
                    UnitTypeInd = null;
                    break;
            }
            return UnitTypeInd;
        }

        public static string GetUnitTypeIndicatorText(Constants.UNIT_TYPE_INDICATOR UnitTypeInd)
        {
            string sUnitType;
            switch (UnitTypeInd)
            {
                case Constants.UNIT_TYPE_INDICATOR.VEHICLE:
                    sUnitType = "V";
                    break;
                case Constants.UNIT_TYPE_INDICATOR.PROPERTY:
                    sUnitType = "P";
                    break;
                case Constants.UNIT_TYPE_INDICATOR.SITE:
                    sUnitType = "S";
                    break;
                case Constants.UNIT_TYPE_INDICATOR.STAT:
                    sUnitType = "SU";
                    break;
                default:
                    sUnitType = string.Empty;
                    break;
            }
            return sUnitType;
        }

        public static Constants.POLICY_SYSTEM_TYPE? GetPolicySystemTypeIndicator(string sPolicySystemTypeName)
        {
            Constants.POLICY_SYSTEM_TYPE? PolicySystemTypeInd;
            switch (sPolicySystemTypeName.ToUpper())
            {
                case "POINT":
                    PolicySystemTypeInd = Constants.POLICY_SYSTEM_TYPE.POINT;
                    break;
                case "INTEGRAL":
                    PolicySystemTypeInd = Constants.POLICY_SYSTEM_TYPE.INTEGRAL;
                    break;
                case "STAGING":
                    PolicySystemTypeInd = Constants.POLICY_SYSTEM_TYPE.STAGING;
                    break;
                default:
                    PolicySystemTypeInd = null;
                    break;
            }
            return PolicySystemTypeInd;
        }
        public static string GetPolicySystemTypeIndicatorText(Constants.POLICY_SYSTEM_TYPE PolicySystemTypeInd)
        {
            string sPolicySystemTypeIndicatorText;
            switch (PolicySystemTypeInd)
            {
                case Constants.POLICY_SYSTEM_TYPE.POINT:
                    sPolicySystemTypeIndicatorText = "POINT";
                    break;
                case Constants.POLICY_SYSTEM_TYPE.INTEGRAL:
                    sPolicySystemTypeIndicatorText = "INTEGRAL";
                    break;
                case Constants.POLICY_SYSTEM_TYPE.STAGING: //RMA-10039
                    sPolicySystemTypeIndicatorText = "STAGING";
                    break;
                default:
                    sPolicySystemTypeIndicatorText = string.Empty;
                    break;
            }
            return sPolicySystemTypeIndicatorText;
        }
        //smishra54: End

        //mkaran2 - JIRA RMA 1774 - MITS 35851 - Start
        /// <summary>
        /// Func For Remove Exponential Value
        /// </summary>
        /// <param name="length">Amount</param>
        /// <returns>Non Exponential Amt</returns>
        public static double RemoveExponentialVal(double amount)
        {
            if (Math.Abs(amount) < 0.000001)
                amount = 0d;

            return amount;
        }
        //mkaran2 - JIRA RMA 1774 -MITS 35851 - End 

        public static string RetrieveBaseURL()
        {
            string sBaseUrl = string.Empty;
            if (CacheCommonFunctions.CheckIfKeyExists("BaseServiceUrl", 0))
                return CacheCommonFunctions.RetreiveValueFromCache<string>("BaseServiceUrl", 0);
            else
            {
                sBaseUrl = ConfigurationManager.AppSettings["BaseServiceUrl"].ToString().EndsWith("/") ? ConfigurationManager.AppSettings["BaseServiceUrl"].ToString() : ConfigurationManager.AppSettings["BaseServiceUrl"].ToString() + "/";
                CacheCommonFunctions.AddValue2Cache<string>("BaseServiceUrl", 0, sBaseUrl);
                return sBaseUrl;
            }


        }

        public static int RetrieveTimeOut()
        {
            int iTimeOut = 0;
            bool blnSuccess;
            if (CacheCommonFunctions.CheckIfKeyExists("TimeOut", 0))
                return CacheCommonFunctions.RetreiveValueFromCache<int>("TimeOut", 0);
            else
            {
                iTimeOut = Conversion.CastToType<int>(ConfigurationManager.AppSettings["TimeOut"], out blnSuccess);
                CacheCommonFunctions.AddValue2Cache<int>("TimeOut", 0, iTimeOut);
                return iTimeOut;
            }


        }

        /// <summary>
        /// Author		: achouhan3
        /// Date Created: 05/09/2014 
        /// Sets the user preference for all in user_pref_xml
        /// </summary>
        /// <param name="p_objInputDoc">XmlDocument</param>
        public static void GetUserPrefXML(XmlDocument p_objInputDoc, ref XmlDocument p_objInputDocOut, int p_UserID, string p_ConString, int iClientId)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", p_UserID.ToString());

                objReader = DbFactory.ExecuteReader(p_ConString, strSQL.ToString(), dictParams);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();

                p_objInputDocOut = new XmlDocument();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    p_objInputDocOut.LoadXml(sUserPrefsXML);

                    objParentNode = (XmlElement)p_objInputDocOut.SelectSingleNode("/setting");
                    if (!(objParentNode == null))
                    {
                        objParentNode.RemoveAll();

                        objUserPrefXML.LoadXml(sUserPrefsXML);

                        if (!(objUserPrefXML.SelectSingleNode("//UserPref") == null))
                        {
                            objTempNode = (XmlElement)p_objInputDocOut.ImportNode(objUserPrefXML.SelectSingleNode("//UserPref"), true);
                            objParentNode.AppendChild(objTempNode);
                        }
                    }//end of if. ("/setting" == null)
                }
            }// end of try block

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                objTempNode = null;
                objParentNode = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }

        public static void SetUserPref(XmlDocument p_objInputDoc, int p_UserID, string p_ConString, int iClientId)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            string strSearchtype;
            XmlNode objNode = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", p_UserID.ToString());

                objReader = DbFactory.ExecuteReader(p_ConString, strSQL.ToString(), dictParams);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                CreateUserPrefXML(objUserPrefXML, iClientId);

                strSearchtype = p_objInputDoc.SelectSingleNode("//UserPref").FirstChild.Name;

                objNode = objUserPrefXML.SelectSingleNode("//UserPref/" + strSearchtype);

                if (!(objNode == null))
                    objUserPrefXML.SelectSingleNode("//UserPref").RemoveChild(objNode);

                objNode = objUserPrefXML.ImportNode(p_objInputDoc.SelectSingleNode("//UserPref").FirstChild, true);

                objUserPrefXML.SelectSingleNode("//UserPref").AppendChild(objNode);

                SaveUserPrefXmlToDb(objUserPrefXML.OuterXml, p_UserID, p_ConString, iClientId);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                objNode = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }

        /// <summary>
        /// Create Default User Prefernce Xml.
        /// </summary>
        /// <param name="p_objUserPrefXML">User Preference Xml Doc</param>
        /// 
        private static void CreateUserPrefXML(XmlDocument p_objUserPrefXML, int iClientId)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            string strSearchtype = string.Empty;

            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }
                // Create search node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//UserPref");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("UserPref");
                    objParentNode.AppendChild(objTempNode);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateUserPreferXML.Err", iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }

        private static void SaveUserPrefXmlToDb(string p_sUserPrefXml, int p_iUserId, string p_Constring, int iClientId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {

                strSQL.AppendFormat("DELETE FROM USER_PREF_XML WHERE USER_ID = {0}", "~USERID~");
                dictParams.Add("USERID", p_iUserId);

                DbFactory.ExecuteNonQuery(p_Constring, strSQL.ToString(), dictParams);

                objWriter = DbFactory.GetDbWriter(p_Constring);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPrefersXmlToDb.Err", iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }
        /// <summary>
        // This function verifies whether assembly name (passed as argument) was called or not, after ProcessRequest() funtion of RiskmasterService assembly
        // For example, When argument passed is "Scripting", then function verifies whether Custom Script is calling assembly or not.
        /// <summary>
        //mbahl3 scripting work jira RMA-8486
        public static bool CheckCallingStack(string Key)
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame[] frames = stackTrace.GetFrames();
            string sServiceAssembly = "RiskmasterService";

            var script = frames.FirstOrDefault(s => s.GetMethod().DeclaringType.Assembly.ToString().Contains(Key) || s.GetMethod().DeclaringType.Assembly.ToString().Contains(sServiceAssembly));
            return script.GetMethod().DeclaringType.Assembly.ToString().Contains(Key);
            //mbahl3 used linq instead of foreach

            //foreach (var stackFrame in frames)
            //{
            //    string ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly.ToString();
            //    if (ownerAssembly.Contains(Key))
            //        return true;
            //    else if (ownerAssembly.Contains(sServiceAssembly))
            //        return false;
            //}
            // return false;
        }

        //mbahl3 scripts work jira RMA-8486

        //JIRA RMA-11122 ajohari2  : Start
        public static int GetChildIDFromParent(int p_ChildTableID, int p_ParentCodeID, string sConnectionString, int p_iClientId)
        {
            int iChildCodeID = 0;
            StringBuilder sbSQL = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.AppendFormat(String.Format("SELECT CODE_ID FROM CODES WHERE TABLE_ID = {0} AND RELATED_CODE_ID= {1}  AND DELETED_FLAG=0 ORDER BY SHORT_CODE", p_ChildTableID, p_ParentCodeID));
                using (DbReader objReader = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString()))
                {
                    while (objReader.Read())
                    {
                        if (objReader.GetValue("CODE_ID") != DBNull.Value)
                        {
                            iChildCodeID = objReader.GetInt32("CODE_ID");
                            break;
                        }
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("CommonFunctions.GetChildIDFromParent.Error", p_iClientId), p_objException);
            }
            finally
            {

            }
            return iChildCodeID;
        }
        //JIRA RMA-11122 ajohari2  : End
        //RMA-8753 nshah28 start
        public static int CheckAddressDuplication(string sSearchString, string sConnectionString, int iClientId)
        {
            StringBuilder sbSql = new StringBuilder();
            int iAddressID = 0;
            bool bOut = false;
            try
            {
                sSearchString = sSearchString.Replace("'", "''");
                sbSql.Append(string.Format("SELECT ADDRESS_ID FROM ADDRESS WHERE SEARCH_STRING = '{0}' AND DELETED_FLAG!=-1 ", sSearchString));
                object oAddressID = DbFactory.ExecuteScalar(sConnectionString, sbSql.ToString());
                iAddressID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(oAddressID), out bOut);

                return iAddressID;

            }

            catch (Exception e)
            {
                throw e;
            }

        }

        public static int CheckAddressDuplication(string sSearchString, string sConnectionString, int iClientId,DbTransaction objTrans)
        {
            //StringBuilder sbSql = new StringBuilder();
            //int iAddressID = 0;
            bool bOut = false;
            try
            {
                sSearchString = sSearchString.Replace("'", "''");
                //sbSql.Append(string.Format("SELECT ADDRESS_ID FROM ADDRESS WHERE SEARCH_STRING = '{0}' ", sSearchString));
                //object oAddressID = DbFactory.ExecuteScalar(sConnectionString, sbSql.ToString());
                //iAddressID = Conversion.CastToType<int>(Conversion.ConvertObjToStr(oAddressID), out bOut);

                using (DbReader objDbReader = objTrans.Connection.ExecuteReader(string.Format("SELECT ADDRESS_ID FROM ADDRESS WHERE SEARCH_STRING = '{0}' AND DELETED_FLAG!=-1 ", sSearchString), objTrans))
                {
                    if (objDbReader.Read())
                        return Conversion.ConvertObjToInt(objDbReader.GetValue("ADDRESS_ID"),iClientId);
                    else
                        return 0;
                }


               // return iAddressID;

            }

            catch (Exception e)
            {
                throw e;
            }

        }
        //RMA-8753 nshah28 end
    }
}
