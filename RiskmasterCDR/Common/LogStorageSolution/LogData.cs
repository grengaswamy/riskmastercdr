using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
namespace Riskmaster.Common
{
	/// <summary>
	/// Summary description for EventInformationForm.
	/// </summary>
		public class LogData
		{
			// Event information fields
			private int eventId;
			private string message;
			private string category;
			private int priority;
			private string RMexceptionMessage;
			private string RMexceptionType;
			private string RMexceptionStackTrace;
			private string RMexceptionSource;
			private IDictionary<string, object> parmList;
            private string logType;
            private string clientId;

            /// <summary>
            /// Default class constructor for LogItem
            /// </summary>
			public LogData()
			{
                parmList = new Dictionary<string, object>();
			}//class constructor

			/// <summary>
			/// Unique identification for event to be logged.
			/// </summary>
			public int EventId
			{
				get { return this.eventId; }
				set { this.eventId = value; }
			}

            /// <summary>
            /// Unique identification for Client.
            /// </summary>
            public string ClientId
            {
                get { return this.clientId; }
                set { this.clientId = value; }
            }

			/// <summary>
			/// Message for event to be logged.
			/// </summary>
			public string Message
			{
				get { return this.message; }
				set { this.message = value; }
			}

			/// <summary>
			/// Category of event to be logged.
			/// </summary>
			public string Category
			{
				get { return this.category; }
				set { this.category = value; }
			}

			/// <summary>
			/// Priority of event to be logged.
			/// </summary>
			public int Priority
			{
				get { return this.priority; }
				set { this.priority = value; }
			}

			/// <summary>
			/// Riskmaster Exception Message to be logged.
			/// </summary>
			public string RMExeptMessage
			{
				get { return this.RMexceptionMessage; }
				set { this.RMexceptionMessage = value; }
			}
			/// <summary>
			/// Riskmaster Exception Type to be logged.
			/// </summary>
			public string RMExceptType
			{
				get { return this.RMexceptionType; }
				set { this.RMexceptionType = value; }
			}
			/// <summary>
			/// Riskmaster Exception Source to be logged.
			/// </summary>
			public string RMExceptSource
			{
				get { return this.RMexceptionSource; }
				set { this.RMexceptionSource = value; }
			}
			/// <summary>
			/// Riskmaster Exception Stack Trace to be logged.
			/// </summary>
			public string RMExceptStack
			{
				get { return this.RMexceptionStackTrace; }
				set { this.RMexceptionStackTrace = value; }
			}
			/// <summary>
			/// Gets and sets the 
            /// Riskmaster StringDictionary list of additional items to be logged.
			/// </summary>
			public IDictionary<string, object> RMParamList
			{
				get 
                {
                    return parmList;
				}
				set 
                {
                    parmList = value;
				
				}
				
			}//property: RMParamList()

            /// <summary>
            /// LogType for event to be logged.
            /// </summary>
            public string LogType
            {
                get { return this.logType; }
                set { this.logType = value; }
            }
		}
	}

