﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Common  
{
    public class LogStorageFactory
    {
        /// <summary>
        /// Create LogStorage instance as per enum-enumStoageType by this Factory for store log.
        /// </summary>
        /// <param name="enumStoageType"></param>
        /// <returns></returns>
        public static ILogInterface CreateInstance(LogStorageTypeEnumeration.LogStorageType enumStoageType)
        {
            ILogInterface objLogStorage = null;

            switch (enumStoageType)
            {
                case LogStorageTypeEnumeration.LogStorageType.Database:
                    objLogStorage = new LogDBStoragecs();
                    break;
                case LogStorageTypeEnumeration.LogStorageType.FileSystem:
                    objLogStorage = new LogFileStorage();
                    break;
                case LogStorageTypeEnumeration.LogStorageType.AmazonS3:
                    objLogStorage = new LogCloudStorage();
                    break;
                default:
                    break;
            }
            return objLogStorage;
        }
      
    }
}
