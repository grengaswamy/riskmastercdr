﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Security.Principal;
using System.Collections.Specialized;
using Riskmaster.Db;
using System.Configuration;
//using System.Xml;
//using System.Xml.Linq;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
//using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
//using Microsoft.Practices.Unity;
//using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
//using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ContainerModel.Unity;
//using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
//using Microsoft.Practices.ServiceLocation;

namespace Riskmaster.Common 
{
    class LogFileStorage : ILogInterface
    {
        /// Code is commented for future reference as per current requirements for cloud.
        /// Before uncomment these function we have to upgrade Microsoft.Practices.EnterpriseLibrary.
        /// <summary>
        /// Function for store file on drive.
        /// </summary>
        /// Add by kuladeep for Multi-tan.
        public void Add(LogData objLogData)
        {
            //IServiceLocator locator = null;
            //string sCategory = string.Empty;
            //if (objLogData != null)
            //{
            //    try
            //    {

            //        SetLogPaths(ref locator, objLogData.LogType,objLogData.ClientId);
            //        EnterpriseLibraryContainer.Current = locator;

            //        LogEntry log = new LogEntry();
            //        log.EventId = objLogData.EventId;
            //        log.Message = objLogData.Message;
            //        log.Categories.Add(objLogData.Category);
            //        log.Priority = objLogData.Priority;

            //        if (!objLogData.RMParamList.Equals(null))
            //        {
            //            log.ExtendedProperties = objLogData.RMParamList;
            //        }
            //        Logger.Write(log);
            //        EnterpriseLibraryContainer.Current.GetInstance<LogWriter>().Dispose();
            //    }
            //    catch (Exception ex)
            //    {
            //     //TODO.
            //    }
            //    finally
            //    {
            //        objLogData = null;
            //    }
            //}
        }
        /// <summary>
        /// Function for Alter Log path as per client.
        /// Add by kuladeep for change log path runtime.
        /// </summary>
        //private static void SetLogPaths(ref IServiceLocator locator, string sLogType,string sClient)
        //    {
        //        TraceListenerData traceListenerData = null;

        //        ConfigurationFileMap objConfigPath = new ConfigurationFileMap();
        //        // App config file path.
        //        string appPath = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
        //        objConfigPath.MachineConfigFilename = appPath;

        //        Configuration entLibConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

        //        LoggingSettings loggingSettings = (LoggingSettings)entLibConfig.GetSection(LoggingSettings.SectionName);

        //        switch (sLogType)
        //        {
        //            case "Scripting":
        //                traceListenerData = loggingSettings.TraceListeners.Get("Scripting Listener");
        //                break;
        //            case "TaskManagerServiceLog":
        //                traceListenerData = loggingSettings.TraceListeners.Get("TaskManager Listener");
        //                break;
        //            case "Tools":
        //                traceListenerData = loggingSettings.TraceListeners.Get("Tools TraceListener");
        //                break;
        //            case "HistoryTrackingLog":
        //                traceListenerData = loggingSettings.TraceListeners.Get("HistTrack Listener");
        //                break;
        //            case "RecordCompletedLog":
        //                traceListenerData = loggingSettings.TraceListeners.Get("RecordCompleted Listener");
        //                break;
        //            case "RecordFailedLog":
        //                traceListenerData = loggingSettings.TraceListeners.Get("RecordFailed Listener");
        //                break;                           
        //            default:
        //                traceListenerData = loggingSettings.TraceListeners.Get("Rolling Flat File Trace Listener");
        //                break;
        //        }

        //        RollingFlatFileTraceListenerData objFlatFileTraceListenerData = traceListenerData as RollingFlatFileTraceListenerData;
                
        //       //Change file location as per client.
        //        string sblogPathLocation = string.Empty;
        //        sblogPathLocation = objFlatFileTraceListenerData.FileName;
        //        string sTempFilePath = sblogPathLocation.Substring(0,sblogPathLocation.LastIndexOf(@"\"));
        //        string sTempFilename = sblogPathLocation.Substring(sblogPathLocation.LastIndexOf(@"\"));
        //        string sFinallogPath = sTempFilePath +"\\" +sClient + sTempFilename;

        //        //objFlatFileTraceListenerData.FileName = string.Format(objFlatFileTraceListenerData.FileName, @"Client1\");
        //        objFlatFileTraceListenerData.FileName = sFinallogPath;
        //        //Need to update this path with new path as per client.

        //        IUnityContainer container = new UnityContainer();
        //        container.AddNewExtension<EnterpriseLibraryCoreExtension>();

        //        // Configurator will read Enterprise Library configuration 
        //        // and set up the container
        //        UnityContainerConfigurator configurator = new UnityContainerConfigurator(container);

        //        var loggingXmlConfigSource = new SerializableConfigurationSource();
        //        loggingXmlConfigSource.Add(LoggingSettings.SectionName, loggingSettings);

        //        // Configure the container with our own custom logging
        //        EnterpriseLibraryContainer.ConfigureContainer(configurator, loggingXmlConfigSource);

        //        // Wrap in ServiceLocator
        //        locator = new UnityServiceLocator(container);

        //    }

	}
    /// <summary>
    /// Class for read configuration section of file.
    /// </summary>
    //public class SerializableConfigurationSource : IConfigurationSource
    //    {
    //        Dictionary<string, ConfigurationSection> sections = new Dictionary<string, ConfigurationSection>();

    //        public SerializableConfigurationSource()
    //        {
    //        }

    //        public ConfigurationSection GetSection(string sectionName)
    //        {
    //            ConfigurationSection configSection;

    //            if (sections.TryGetValue(sectionName, out configSection))
    //            {
    //                SerializableConfigurationSection section = configSection as SerializableConfigurationSection;

    //                if (section != null)
    //                {
    //                    using (StringWriter xml = new StringWriter())
    //                    using (XmlWriter xmlwriter = System.Xml.XmlWriter.Create(xml))
    //                    {
    //                        section.WriteXml(xmlwriter);
    //                        xmlwriter.Flush();

    //                        MethodInfo methodInfo = section.GetType().GetMethod("DeserializeSection", BindingFlags.NonPublic | BindingFlags.Instance);
    //                        methodInfo.Invoke(section, new object[] { XDocument.Parse(xml.ToString()).CreateReader() });

    //                        return configSection;
    //                    }
    //                }
    //            }

    //            return null;
    //        }

    //        public void Add(string sectionName, ConfigurationSection configurationSection)
    //        {
    //            sections[sectionName] = configurationSection;
    //        }

    //        public void AddSectionChangeHandler(string sectionName, ConfigurationChangedEventHandler handler)
    //        {
    //            throw new NotImplementedException();
    //        }

    //        public void Remove(string sectionName)
    //        {
    //            sections.Remove(sectionName);
    //        }

    //        public void RemoveSectionChangeHandler(string sectionName, ConfigurationChangedEventHandler handler)
    //        {
    //            throw new NotImplementedException();
    //        }

    //        public event EventHandler<ConfigurationSourceChangedEventArgs> SourceChanged;

    //        public void Dispose()
    //        {
    //        }
    //    }
}
