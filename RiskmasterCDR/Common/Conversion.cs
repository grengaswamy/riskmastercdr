using System;
using System.Text.RegularExpressions;
using Riskmaster.ExceptionTypes;
using System.Globalization;
using System.Data;
using System.Collections;
using Riskmaster.Cache;
using Newtonsoft.Json;
using System.Collections.Generic;

// <summary> Riskmaster.Common is a namespace containing class libraries 
// of functionality common to many Riskmaster development tasks.
//   All such commonly reusable utility Riskmaster code should reside here.
// </summary>
namespace Riskmaster.Common
{
	
	/// <summary>
	/// Conversion is a static class library of commonly used conversion functionality within Riskmaster .
	/// </summary>
	public partial class Conversion
	{

		/// <summary>
		/// Return string representing the Organizational Hierarchy Entity Table Name matching the 
		/// provided Entity Table Id provided in the "tableIdvalue" parameter.
		/// </summary>
		/// <param name="tableId"></param>
		/// <returns></returns>
		static public string EntityTableIdToOrgTableName(int tableId)
		{
			switch(tableId)
			{
				case 1005:
					return "CLIENT";
				case 1006:
					return "COMPANY";
				case 1007:
					return "OPERATION";
				case 1008:
					return "REGION";
				case 1009:
					return "DIVISION";
				case 1010:
					return "LOCATION";
				case 1011:
					return "FACILITY";
				case 1012:
					return "DEPARTMENT";
				default:
					return "";
			}
		}
        //Deb ML Changes
        public class CacheLanguageTable
        {
            //private static Hashtable m_StaticCache = new Hashtable();
            private int m_iClientId = 0;
            public CacheLanguageTable(int p_iClientId)
            {
                m_iClientId = p_iClientId;
            }
            public object this[string sKey]
            {
                //get { return CacheCommonFunctions.RetreiveValueFromCache<object>(String.Format("{0}", sKey), m_iClientId); }
                get { return CacheCommonFunctions.RetreiveValueFromCache<object>(String.Format("$$|ML|$$_{0}", sKey), m_iClientId); }//rma-4608..when the keys is added or checked or set to the table , it is formatted as "$$|ML|$$_{0} but when get is done from the table, they key was not formatted

                set { CacheCommonFunctions.UpdateValue2Cache<object>(String.Format("$$|ML|$$_{0}", sKey), m_iClientId, value); }
            }
            public bool ContainsKey(object sKey)
            {
                return CacheCommonFunctions.CheckIfKeyExists(String.Format("$$|ML|$$_{0}", sKey), m_iClientId);
            }
            public void Add(object sKey, object objValue)
            {
                try { CacheCommonFunctions.AddValue2Cache<object>(String.Format("$$|ML|$$_{0}", sKey), m_iClientId, objValue); }
                catch { };
            }
            public void Remove(object sKey)
            {
                try { CacheCommonFunctions.RemoveValueFromCache(String.Format("$$|ML|$$_{0}", sKey), m_iClientId); }
                catch { }; 
            }
        }
        private static CacheLanguageTable m_Cache = null;
        //Deb ML Changes
        //BSB 11.16.2007 Perf ~ 4.5% of total execution cycles were being spent 
        // reconstructing this silly regex expression object repeatedly.
        private static Regex m_IsNumericRegEx = new Regex("[^0-9\\-]");


        /// <summary>
        /// Determines whether or not a specified string value
        /// matches a Numeric RegEx expression
        /// </summary>
        /// <param name="s"></param>
        /// <returns>boolean indicating whether or not the string is completely numeric/contains numbers</returns>
        public static bool IsNumeric(string s)
        {
            if (string.IsNullOrEmpty(s))
                return false;

            //Contains non-numeric characters?
            return !(m_IsNumericRegEx.Matches(s).Count > 0);
        }

        /// <summary>
        /// Determines whether or not a specified object
        /// matches a Numeric RegEx expression
        /// </summary>
        /// <param name="o"></param>
        /// <returns>boolean indicating whether or not the object is completely numeric/contains numbers</returns>
        public static bool IsNumeric(object o) 
        { 
            return IsNumeric(o.ToString()); 
        }

        //rupal:start.omig mits 33913
        /// <summary>
        /// Return passed string value as Date in yyyymmdd format
        /// </summary>
        /// <param name="sRMADate">RMA Date in yyyymmdd format</param>        
        /// <returns>Returns Date in Point Format xyymmdd where x will be 0 for 19th centuary and 1 for 20th centuary </returns>
        public static string ConvertRMADateToPointDate(string sRMADate)
        {
            string sYYYY = string.Empty;
            string sCYY = string.Empty;
            string sReturnString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(sRMADate) == false && sRMADate != "0")
                {
                    if (Conversion.GetDate(sRMADate) == string.Empty)
                        throw new Exception();
                    sYYYY = sRMADate.Substring(0, 4);// Left(strPointYYYYMMDD, 4)
                    sCYY = ConvertYearToPoint(sYYYY);
                    sReturnString = sCYY + sRMADate.Substring(sRMADate.Length - 4); // Right(strPointYYYYMMDD, 4)
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }
            return sReturnString;

        }

        private static string ConvertYearToPoint(string strPointYr)
        {
            try
            {
                string strTemp = string.Empty;
                if (string.Equals(strPointYr.Substring(0, 1), "2"))  //if StrComp(Left(strPointYr, 1), "2") = 0 Then
                    strTemp = "1" + strPointYr.Substring(2, 2); // Mid(strPointYr, 3, 2)
                else
                    strTemp = "0" + strPointYr.Substring(2, 2);// Mid(strPointYr, 3, 2)

                return strTemp;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        //rupal:end, omig


		/// <summary>
		/// This overload of Riskmaster.Common.Conversion.GetByteArr breaks an uint Array into an equivalent byte Array.</summary>
		/// <param name="uintArr">Unsigned Integer array  for which to provide an equivalent byte Array.</param>
		/// <returns>byte[] representing uintArr</returns>
		/// <remarks>It is perfectly acceptable to us in this implementation to have the value "roll negative" during conversion.</remarks>
		/// <example>This function might be usefull for encryption.</example>
		public static byte[] GetByteArr(uint[] uintArr)
		{
			byte[] byteArr = new byte[4* uintArr.Length];
			ushort[] wordArr = new ushort[2];
			int iSrc;
			int i=0;
			foreach(uint Item in uintArr)
			{
				//It is perfectly acceptable to us
				// to have the value "roll negative" during conversion,
				unchecked{iSrc = (int)(Item);}

				GetHILOWord(iSrc,out wordArr[0],out wordArr[1]);
				GetHILOByte(wordArr[0],out byteArr[(i*4) + 0],out byteArr[(i*4) + 1]);
				GetHILOByte(wordArr[1],out byteArr[(i*4) + 2],out byteArr[(i*4) + 3]);
				i++;
			}
			return byteArr;
		}
		
		/// <summary>This overload of Riskmaster.Common.Conversion.GetByteArr 
		/// breaks an uint Array into an equivalent byte Array.</summary>
		/// <param name="iSrc">An integer to be converted to a byte[4].</param>
		/// <returns>byte[] representing int iSrc</returns>
		/// <example>This function might be usefull for encryption.</example>
		public static byte[] GetByteArr(int iSrc)
		{
			byte[] byteArr= new byte[4];
			ushort[] wordArr = new ushort[2];

			GetHILOWord(iSrc,out wordArr[0],out wordArr[1]);
			GetHILOByte(wordArr[0],out byteArr[0],out byteArr[1]);
			GetHILOByte(wordArr[1],out byteArr[2],out byteArr[3]);
			return byteArr;
		}

		
		/// <summary>This overload of Riskmaster.Common.Conversion.GetByteArr breaks 
		/// an uint Array into an equivalent byte Array.</summary>
		/// <param name="s">A string  to be converted to a byte[].</param>
		/// <returns>byte[] representing string s.</returns>
		/// <remarks>Uses the ASCII.GetBytes framework routine but checks for an empty 
		/// or null string before making the call returning {0} in that case.</remarks>
		/// <example>This function might be useful for encryption.</example>
		public static byte[] GetByteArr(string s)
		{
			Byte[] ret = {0};
			if(s=="" || s==null)
				return ret;
			return System.Text.Encoding.ASCII.GetBytes(s);
		}
		
		/// <summary>
		/// Riskmaster.Common.Conversion.GetHILOWord.</summary>
		/// <param name="Src">Integer containing the dword size value to split.</param>
		/// <param name="iLO">Unsigned short containing the word size value from the lower position of Src.</param>
		/// <param name="iHI">Unsigned short containing the word size value from the upper position of Src.</param>
		/// <example>This function might be useful for encryption.</example>
		public static void GetHILOWord(int Src, out ushort iLO,out ushort iHI)
		{
			unchecked
			{
				iLO =  (ushort)(Src & 0x0000FFFF);
				iHI = (ushort)((Src >> 16) & 0x0000FFFF);
			}
		}
		
		/// <summary>
		/// Riskmaster.Common.Conversion.GetHILOByte.</summary>
		/// <param name="Src">Word sized value to be split into a high and low byte.</param>
		/// <param name="iLO">Byte sized value to contain the lower byte of Src.</param>
		/// <param name="iHI">Byte sized value to contain the upper byte of Src.</param>
		/// <example>This function might be useful for encryption.</example>
		public static void GetHILOByte(ushort Src, out byte iLO,out byte iHI)
		{
			unchecked
			{
				iLO = (byte)(Src & 0x00FF);
				iHI = (byte)((Src >> 8) & 0x00FF);
			}
		}
		//NOTE: This is the intentionally hacked version that only returns 
		// the first ascii character code of the string "s".
		
		/// <summary>
		/// Deprecated Riskmaster.Common.Conversion.GetAscii.</summary>
		/// <param name="s"></param>
		/// <returns>Not specified.</returns>
		/// <remarks>Deprecated - see source code..</remarks>
		// NOTE: Not public knowledge but for backward compatibility of our encryption\crc hashes
		//  we must keep this intentionally hacked version that only returns 
		// the first ascii character code of the string "s".
        [Obsolete("Please use the Generic CastToType method instead.")]
		public static byte GetAscii(string s)
		{
			if(s=="" || s==null)
				return 0;
			return System.Text.Encoding.ASCII.GetBytes(s.ToCharArray(0,1),0,1)[0];
		}
	
		/// <summary>
		/// Riskmaster.Common.Conversion.InsertTransparencyChars 
		/// turns 'buf' containing 'real' data into a hexadecimal string suitable for database
		/// storage. Implemented because storing NUL characters (and possibly other binary data)
		/// in a database creates problems.</summary>
		/// <param name="buf">Byte array of input data to be sanitized.</param>
		/// <param name="len">Integer number of bytes to sanitize.</param>
		/// <returns>String sanitized version of buf.</returns>
		public static string InsertTransparencyChars(byte[] buf, int len)  
		{
			string sWork = "";

			for(int i=0;i<len;i++)
				sWork += String.Format("{0:x2}", buf[i]);

			return sWork;
		}

		
		/// <summary>
		/// Riskmaster.Common.Conversion.RemoveTransparencyChars "unsanitizes" a string of 
		/// two character hex values converting each pair into a an ASCII character and 
		/// returning the final byte array of converted ASCII characters.  Precisely reverses changes made by 
		/// Riskmaster.Common.Conversion.InsertTransparencyChars</summary>
		/// <param name="szStr">Sanitized string value to be "unsanitized."</param>
		/// <returns>Unsanitized byte[]</returns>
		public static byte[] RemoveTransparencyChars(string szStr)
		{                              
			byte[] sArr = new Byte[szStr.Length / 2];

			int length = szStr.Length;
			for(int i=0;i<length;i+=2)
			{
				sArr[i / 2] = Byte.Parse(szStr.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
			}

			return sArr;
		}

		
		

		/// <summary>Gets OrgHierarchy Level in Long</summary>
		/// <param name="p_sLevel">Org Hierarchy Level string</param>	
		/// <returns>Org Hierarchy Level Integer</returns>
		static public int GetOrgHierarchyLevel(string p_sLevel )
		{
			switch(p_sLevel.ToUpper())
			{
				case "CLIENT":
					return 1005;
				case "COMPANY":
					return 1006;
				case "OPERATION":
					return 1007;
				case "REGION":
					return 1008;
				case "DIVISION":
					return 1009;
				case "LOCATION":
					return 1010;
				case "FACILITY":
					return 1011;
				case "DEPARTMENT":
					return 1012;
				default:
					return 1005;
			}
		}
		
		
		/// <summary>
		/// Create money groups for the given scale.
		/// </summary>
		/// <param name="p_iIntegerPart">Integer Part</param>
		/// <param name="p_sScale">Scale Value, Possible values : "Billion" , "Million" , "Thousand" and "" </param>
		/// <param name="p_sValue">Reference string in which group is need to add</param>
		/// <param name="p_arrUnits">Units Array</param>
		/// <param name="p_arrTens">Tens Array</param>
        private static void CreateMoneyGroup(int p_iIntegerPart, string p_sScale, ref string p_sValue, string[] p_arrUnits, string[] p_arrTens, int p_iClientId)
		{
			try
			{
				if( p_sValue != "" )
					p_sValue += " " ;

				if( p_iIntegerPart >= 100 )
				{
					p_sValue += p_arrUnits[p_iIntegerPart/100] + " Hundred ";
					p_iIntegerPart %= 100 ;
				}

				if( p_iIntegerPart >= 20 )
				{
					p_sValue += p_arrTens[(p_iIntegerPart-20)/10] ;
					if( (p_iIntegerPart %= 10 ) != 0 )
						p_sValue += "-" + p_arrUnits[p_iIntegerPart] + " ";
					else
						p_sValue += " " ;
				}
				else
				{
					if( p_iIntegerPart != 0 )
					{
						p_sValue += p_arrUnits[p_iIntegerPart] + " " ;
					}
				}

				p_sValue += p_sScale ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Conversion.CreateMoneyGroup.Error", p_iClientId) , p_objEx );				
			}
		}
        /// <summary>
        /// Create money groups for the given scale.
        /// </summary>
        /// <param name="p_iIntegerPart">Integer Part</param>
        /// <param name="p_sScale">Scale Value, Possible values : "Billion" , "Million" , "Thousand" and "" </param>
        /// <param name="p_sValue">Reference string in which group is need to add</param>
        /// <param name="p_arrUnits">Units Array</param>
        /// <param name="p_arrTens">Tens Array</param>
        private static void CreateMoneyGroup(long p_lIntegerPart, string p_sScale, ref string p_sValue, string[] p_arrUnits, string[] p_arrTens, int p_iClientId)
        {
            try
            {
                if (p_sValue != "")
                    p_sValue += " ";

                if (p_lIntegerPart >= 100)
                {
                    p_sValue += p_arrUnits[p_lIntegerPart / 100] + " Hundred ";
                    p_lIntegerPart %= 100;
                }

                if (p_lIntegerPart >= 20)
                {
                    p_sValue += p_arrTens[(p_lIntegerPart - 20) / 10];
                    if ((p_lIntegerPart %= 10) != 0)
                        p_sValue += "-" + p_arrUnits[p_lIntegerPart] + " ";
                    else
                        p_sValue += " ";
                }
                else
                {
                    if (p_lIntegerPart != 0)
                    {
                        p_sValue += p_arrUnits[p_lIntegerPart] + " ";
                    }
                }

                p_sValue += p_sScale;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Conversion.CreateMoneyGroup.Error", p_iClientId), p_objEx);
            }
        }

		

        #region Rounds Double values
        /// <summary>
        /// Performs the equivalent translation of the Math.Round function
        /// </summary>
        /// <param name="dAmt"></param>
        /// <param name="iDec"></param>
        /// <returns>doulbe indicating the rounded value</returns>
        public static double Round(Double dAmt, int iDec)
        {

            //     ----------------------------------------------------------------------------
            //'Function : dRound
            //'
            //' Author    : Divya
            //' Purpose   : Round func tion in  rounds differently for Odd and Even values.
            //'             For instance round(1.5) rounds it to 2 but round(2.5) rounds it to 2
            //'             This function is created because we want 2.5 to be rounded to 3 but not 2.
            //'             It implements the same logic as 'dround' function in RMWorld
            //'---------------------------------------------------------------------------------------
            int i = 0, iAfterDecCnt = 0, y = 0, iBeforeDecCnt = 0, d = 0, q = 0, lLen = 0, l = 0;
            bool bDecimalFound = false, bEqualsNine = false, bExpFound = false;
            string strAmt = "";


            //Find the number of digits before and after decimal point

            try
            {
                lLen = dAmt.ToString().Length;
                strAmt = dAmt.ToString();



                for (i = 0; i < lLen; i++)
                {
                    string sVal = strAmt.Substring(i, 1);
                    if (IsNumeric(sVal) == true && bDecimalFound == true && bExpFound == false)
                        iAfterDecCnt = iAfterDecCnt + 1;
                    else
                    {
                        if (((IsNumeric(sVal)) == true) && (bDecimalFound == false))
                            iBeforeDecCnt = iBeforeDecCnt + 1;
                        else
                            if (IsNumeric(sVal) == false)
                            {
                                if (sVal == ".")
                                    bDecimalFound = true;
                                else
                                    if (sVal == "E")
                                        bExpFound = true;

                            }
                    }

                }


                if (bExpFound == true)
                {
                    y = iAfterDecCnt;
                    iAfterDecCnt = iAfterDecCnt + Convert.ToInt32(strAmt.Substring(0, 2));
                }


                if (iAfterDecCnt > iDec)
                    if (iAfterDecCnt == 2 && iDec == 0)
                    {
                        if (Convert.ToInt32(strAmt.Substring(strAmt.Length - 2, 2)) > 49)
                        {
                            dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1 - iAfterDecCnt));

                            if (dAmt >= 0)
                                dAmt = dAmt + 1;
                            else
                                if (dAmt < 0)
                                    dAmt = dAmt - 1;
                        }

                        else
                        {
                            if ((dAmt < 1) && (dAmt > 0))
                                dAmt = 0;
                            else
                                dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1 - iAfterDecCnt));

                        }
                    }
                    else
                    {
                        if (bExpFound == false)
                        {
                            d = Convert.ToInt32(strAmt.Substring(strAmt.Length - 1, 1));
                            if (d > 4)
                            {
                                if (iAfterDecCnt > 1)
                                {
                                    q = Convert.ToInt32(strAmt.Substring(lLen - 2, 1));
                                    if (q == 9)
                                    {
                                        bEqualsNine = true;
                                        while (bEqualsNine == true)
                                        {
                                            l = l + 1;
                                            if (IsNumeric(strAmt.Substring(lLen - 2 - l, 1)) == true)
                                            {
                                                q = Convert.ToInt32(strAmt.Substring(lLen - 2 - l, 1));
                                                if (q != 9)
                                                    bEqualsNine = false;
                                            }
                                            else
                                            {
                                                dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - l - 1)) + 1;
                                                return dAmt;
                                            }

                                        }

                                        dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 2 - l) + Convert.ToString(q + 1));
                                        dAmt = Round(dAmt, iDec);
                                    }
                                    else
                                    {
                                        dAmt = Convert.ToDouble((strAmt.Substring(0, lLen - 2)) + Convert.ToString((Convert.ToDouble(strAmt.Substring(lLen - 2, 1)) + 1)));
                                        dAmt = Round(dAmt, iDec);
                                    }
                                }
                                else
                                {
                                    dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1));
                                    if (dAmt >= 0)
                                        dAmt = dAmt + 1;
                                    else
                                    {
                                        if (dAmt < 0)
                                            dAmt = dAmt - 1;
                                    }
                                }
                            }
                            else
                            {
                                if (iAfterDecCnt == 1 && dAmt < 1 && dAmt > 0)

                                    dAmt = 0;
                                else
                                {
                                    dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1));
                                    dAmt = Round(dAmt, iDec);
                                }
                            }
                        }
                        else
                        {
                            d = Convert.ToInt32(strAmt.Substring(y + iBeforeDecCnt, 1));
                            if (d > 4)
                            {
                                if (dAmt > 0)
                                    /* Dim z As Doublez = 23 ^ 3------------VB
                                       double Doublez = Math.Pow(23,3)------C#
                                       ' The preceding statement sets z to 12167 (the cube of 23).
                                       ' Exponentiation uses the ^ Operator in VB
                                       ' Exponentiation uses the Math.Pow(x,y) in C#
                                     */
                                    //Change by kuladeep for calculate correct round value MITS 24736
                                    //dAmt = dAmt + (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));
                                    dAmt = dAmt + (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (Math.Pow(10, iAfterDecCnt))));
                                else
                                    //dAmt = dAmt - (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));
                                    dAmt = dAmt - (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (Math.Pow(10, iAfterDecCnt))));
                                dAmt = Round(dAmt, iDec);
                            }
                            else
                            {
                                if (dAmt > 0)
                                    //dAmt = dAmt - ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1))) / (10 ^ iAfterDecCnt));
                                    dAmt = dAmt - ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1))) / (Math.Pow(10, iAfterDecCnt)));
                                else
                                    //dAmt = dAmt + ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));
                                    dAmt = dAmt + ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (Math.Pow(10, iAfterDecCnt))));

                                dAmt = Round(dAmt, iDec);
                            }
                        }
                    }

                return dAmt;
            }

            catch
            {
                return dAmt;
            }
        }
        #endregion

        //rkotak: rma 4608, 9681 starts
        /// <summary>
        /// creates user pref xml
        /// </summary>
        /// <param name="lstColDef"></param>    
        /// <returns></returns>
        public static string CreateJSONStringFromDictionary(List<Dictionary<string, object>> dicData)
        {
            return JsonConvert.SerializeObject(dicData);
        }

                /// <summary>
        /// Creates a JSON Node from the supplied values
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <param name="IsFirstNode"></param>
        public static string GetJsonNode(string Key, string Value, bool IsFirstNode=false)
        {
            string strJson = String.Empty;
            if (!String.IsNullOrEmpty(Key.Trim()))
            {
                System.Text.StringBuilder strJsonNode = new System.Text.StringBuilder();
                if (!IsFirstNode)
                    strJsonNode.Append(",");
                strJsonNode.Append("\"");
                strJsonNode.Append(Key);
                strJsonNode.Append("\" : ");
                strJsonNode.Append("\"");
                strJsonNode.Append(Value);
                strJsonNode.Append("\"");
                strJson = strJsonNode.ToString();
                strJsonNode.Remove(0, strJsonNode.Length);
            }
            return strJson;

        }
        


        //rkotak;4608, 9681 ends

    }//class

    public partial class Conversion
    {
        #region Conversion Functions
        /// <summary>
        /// Return passed string value as Date in yyyymmdd format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetDate(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            if (string.IsNullOrEmpty(s))
                return string.Empty;
            //try
            //{
            //    //Get a Date if this is a valid representation for the current culture.
            //    dttm = System.DateTime.Parse(s);
            //}
            //catch (FormatException)
            //{
            //    boolErr = true;
            //}
            
            //Get a Date if this is a valid representation for the current culture.
            //Commented above line and used TryParse because its faster than Parse
            //Changed the if Condition accordingly
            //Deb-Performance Changes 
            boolErr = System.DateTime.TryParse(s,out dttm);
            if (!boolErr) // Try is passed date in yyyymmdd format already ?
                // akaushik5 Changed for MITS 35846 Starts
                //if (s.Length == 8)
                if (s.Length == 8 || s.Length.Equals(14))
                    // akaushik5 Changed for MITS 35846 Ends
                    try { dttm = ToDate(s); }
                    catch { }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMdd");

            //All attempts at conversion failed return blank.
            return "";

        }

        /// <summary>
        /// Converts a string back to a DateTime representation
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetDateTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            //Get a DateTime if this is a valid representation for the current culture.
            //try { dttm = System.DateTime.Parse(s); }
            //catch (FormatException) { boolErr = true; }
            boolErr = System.DateTime.TryParse(s, out dttm);

            if (!boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 8 || s.Length == 14)
                    try { dttm = ToDate(s); }
                    catch { }


            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMddHHmmss");

            //All attempts at conversion failed return blank.
            return "";
        }



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ToDate
        /// </summary>     
        public static string ToDate(string sValue, string p_LangCode)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ToDate(sValue, p_LangCode, 0);
            }
            else
                throw new RMAppException("ToDate" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// ToDate
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sLangCode"></param>
        /// <returns></returns>
        public static string ToDate(string sValue, string sLangCode, int p_iClientId)
        {
            string sUIDate = string.Empty;
            DateTimeFormatInfo currentDateTimeFormat = null;
            DateTime dtFormattedDate;
            string sCulture = "en-US";
            dtFormattedDate = new DateTime();
            if (sLangCode.Length != 4) sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
            if (m_Cache != null)
            {
                if (m_Cache.ContainsKey(sLangCode))
                {
                    sCulture = Conversion.ConvertObjToStr(m_Cache[sLangCode]);
                }
                else
                {
                    sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                    if (sCulture.Split('-').Length == 1)
                    {
                        sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                    }
                    m_Cache = new CacheLanguageTable(p_iClientId);
                    m_Cache.Add(sLangCode, sCulture);
                }
            }
            else
            {
                sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                if (sCulture.Split('-').Length == 1)
                {
                    sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                }
                m_Cache = new CacheLanguageTable(p_iClientId);
                m_Cache.Add(sLangCode, sCulture);
            }
            currentDateTimeFormat = (new CultureInfo(sCulture)).DateTimeFormat;
            try
            {
                if (string.IsNullOrEmpty(sValue))
                {
                    return sUIDate;
                }
                DateTime dtValidDate;
                if (!DateTime.TryParse(sValue, out dtValidDate))
                {
                    dtFormattedDate = DateTime.ParseExact(sValue, "yyyyMMdd", currentDateTimeFormat);
                    if (currentDateTimeFormat.ShortDatePattern == "M" + currentDateTimeFormat.DateSeparator + "d" + currentDateTimeFormat.DateSeparator + "yyyy")
                    {
                        currentDateTimeFormat.ShortDatePattern = "MM" + currentDateTimeFormat.DateSeparator + "dd" + currentDateTimeFormat.DateSeparator + "yyyy";
                    }
                    else if (currentDateTimeFormat.ShortDatePattern == "d" + currentDateTimeFormat.DateSeparator + "M" + currentDateTimeFormat.DateSeparator + "yyyy")
                    {
                        currentDateTimeFormat.ShortDatePattern = "dd" + currentDateTimeFormat.DateSeparator + "MM" + currentDateTimeFormat.DateSeparator + "yyyy";
                    }
                    sUIDate = dtFormattedDate.ToString(currentDateTimeFormat.ShortDatePattern);
                }
                else
                {
                    sUIDate = sValue;
                }
            }
            catch
            {
                sUIDate = string.Empty;
            }
            return sUIDate;
        }

        /// <summary>
        /// improved by nnorouzi; Jan. 08, 2010
        /// Accepts these string formats:
        /// 1- YYYYMMDD (length of 8); for this case it returns the same date and the time of 12:00:00 AM as a DateTime.
        /// 2- YYYYMMDDhhmmss (length of 14); for this case it returns the same date and the same time as a DateTime.
        /// 3- hhmmss (length of 6); for this case it returns today's date and the same time as a DateTime.
        /// 
        /// If the input is invalid it returns DateTime.MinValue.
        /// </summary>
        [Obsolete("Please use the ToDate(string sValue, string sLangCode) method instead.")]
        public static DateTime ToDate(string s)
        {
            //nnorouzi; validating all cases, and improving security.
            DateTime dt = DateTime.MinValue; //if it turns out that the input is invalid we return this value.
            bool bIsDate = false;
            bool bValid = false;
            int YYYY = DateTime.Today.Year;
            int MM = DateTime.Today.Month;
            int DD = DateTime.Today.Day;
            int hh = 0; //12 AM
            int mm = 0; //12:00 AM
            int ss = 0; //12:00:00 AM
            int i, j, l;

            i = 0;
            if (s == null)
            {
                l = 0; //a form of invalid input
            }
            else
            {
                l = s.Length;
            }

            if (l == 8 || l == 14)
            {
                bValid = false;
                for (; i < 8; i++)
                {
                    if ((int)s[i] < 48 || (int)s[i] > 57)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 3) //Year may be complete
                        {
                            YYYY = Convert.ToInt32(s.Substring(0, 4));
                            if (YYYY < 1000)
                            {
                                break;
                            }
                        }
                        else if (i == 5) //Month may be complete
                        {
                            MM = Convert.ToInt32(s.Substring(4, 2));
                            if (MM > 12 || MM == 0)
                            {
                                break;
                            }
                        }
                        else if (i == 7) //Day may be complete
                        {
                            DD = Convert.ToInt32(s.Substring(6, 2));
                            if (DD > 31 || DD == 0)
                            {
                                break;
                            }
                            else
                            {
                                bIsDate = true; //Date is complete
                                bValid = true;
                            }
                        }
                    }//else
                }//for
            }//if

            if ((l == 14 && bIsDate) || l == 6)
            {
                bValid = false;
                //now checking for time
                j = i;
                for (; i < l; i++)
                {
                    if ((int)s[i] < 48 || (int)s[i] > 57)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 9 || i == 1) //Hour may be complete
                        {
                            hh = Convert.ToInt32(s.Substring(j, 2));
                            if (hh > 23)
                            {
                                break;
                            }
                        }
                        else if (i == 11 || i == 3) //Minute may be complete
                        {
                            mm = Convert.ToInt32(s.Substring(j + 2, 2));
                            if (mm > 59)
                            {
                                break;
                            }
                        }
                        else if (i == 13 || i == 5) //Second may be complete
                        {
                            ss = Convert.ToInt32(s.Substring(j + 4, 2));
                            if (ss > 59)
                            {
                                break;
                            }
                            else
                            {
                                //Time is complete
                                bValid = true; 
                            }
                        }
                    }//else
                }//for
            }//if

            if (bValid)
            {
                dt = new DateTime(YYYY, MM, DD, hh, mm, ss);
            }

            return dt;
            //nnorouzi
        }




        /// <summary>
        ///Takes a pure numeric string 1234567 or 1234567890 or 1234567890999...
        /// </summary>
        /// <param name="s"></param>
        /// <returns>Returns a formatted phone number 123-4567 or (123) 456-7890 or (123) 456-7890 Ext:999</returns>
        public static string ToPhoneNumber(string s)
        {
            //Nothing here to work with.
            if (s == "" || s == null)
                return "";

            //Too Short
            if (s.Length < 7)
                return s;

            //Already contains formatting characters.
            Regex reg = new Regex("[^1-9]");
            if (reg.Matches(s).Count > 0)
                return s;

            //pankaj 4/23/07, Issue 109- mcic. Take care of large phone numbers that exceed the int size
            long val = Int64.Parse(s);
            long val1 = 0;
            string s1 = "";

            if (val == 0) //Non-Numeric string return empty.
                return "";

            if (s.Length == 7)
                return val.ToString("(   ) 000-0000");

            if (s.Length == 10)
                return val.ToString("(000) 000-0000");

            s1 = s.Substring(1, 10);
            val1 = Int64.Parse(s1);
            s = s.Substring(11);

            return val1.ToString("(000) 000-0000 Ext:") + s;
        }

        /// <summary>
        /// Gets a string representation of a TimeSpan
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            //if (s == null) srajindersin MITS 32606 dt:05/14/2013
            if (string.IsNullOrEmpty(s))
                return "";
            //Deb:Performance Changes
            //Get a DateTime if this is a valid representation for the current culture.
            //try { dttm = System.DateTime.Parse(s); }
            //catch (FormatException) { boolErr = true; }
            //Deb:Performance Changes Commented above line
            boolErr = System.DateTime.TryParse(s,out dttm);
            if (!boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 14)
                {
                    try { dttm = ToDate(s); }
                    catch { }
                }
                else if (s.Length == 6)
                {
                    //MITS 7778 ..Raman Bhatia ..Following code is not assigning the time portion correctly
                    /*
                    dttm =  DateTime.Today;  //set to the date of this instance, with the time part set to 00:00:00.
                    dttm.AddHours(int.Parse(s.Substring(0,2)));
                    dttm.AddMinutes(int.Parse(s.Substring(2,2)));
                    dttm.AddSeconds(int.Parse(s.Substring(4,2)));
                    */
                    dttm = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, int.Parse(s.Substring(0, 2)), int.Parse(s.Substring(2, 2)), int.Parse(s.Substring(4, 2)));
                }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("HHmmss");

            //All attempts at conversion failed return blank.
            return string.Empty;
        }

        /// <summary>
        /// Returns time in HHMM00 and takes time in HH:MM
        /// </summary>
        /// <param name="s">Format HH:MM</param>
        /// <returns>HHMM00</returns>
        public static string GetTimeHHMM00(string s)
        {
            string sTemp = "000000";

            if (s == null || s == "")
                return sTemp;

            try
            {
                if (s.Length == 5)
                {
                    sTemp = s.Substring(0, 2);
                    sTemp += s.Substring(3, 2);
                    sTemp += "00";
                }
            }
            catch
            {
                return sTemp;
            }

            return sTemp;
        }

        /// <summary>Gets a common date and time format using the 24-hour clock for RISKMASTER</summary>
        /// <param name="date">DateTime to be converted into Riskmaster Database date format "YYYYMMDDHHMMSS."</param>
        /// <returns>String value for DateTime date in "YYYYMMDDHHMMSS" format.</returns>
        /// <remarks>Returns "00000000000000" if DateTime is equal to DateTime.MinValue.
        /// Note: Modified 08.04.2005 to return 14 characters rather than 12.  This means
        /// seconds are included properly now.</remarks>
        public static string ToDbDateTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "00000000000000";
            return date.ToString("yyyyMMddHHmmss");  //year month day hour minute second
        }
        /// <summary>Gets a common 24-hour time format for RISKMASTER</summary>
        /// <param name="date">DateTime to be converted into Riskmaster Database time format "HHMMSS."</param>
        /// <returns>String value for DateTime date in "HHMMSS" format.</returns>
        /// <remarks>Returns "000000" if DateTime is equal to DateTime.MinValue.</remarks>
        public static string ToDbTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "000000";
            return date.ToString("HHmmss"); //hour minute second
        }
        /// <summary>Gets a common date format for RISKMASTER</summary>
        /// <param name="date">DateTime to be converted into Riskmaster Database date format "YYYYMMDD."</param>
        /// <returns>String value for DateTime date in "YYYYMMDD" format.</returns>
        /// <remarks>Returns an empty string if DateTime is equal to DateTime.MinValue.</remarks>
        public static string ToDbDate(DateTime date)
        {
            if (date == DateTime.MinValue)
                return string.Empty;
            return date.ToString("yyyyMMdd"); //year month day
        }

        /// <summary>		
        /// This method will convert a string to a long value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to a long value</param>												
        /// <returns>A long value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static long ConvertStrToLong(string p_sValue)
        {
            long lRetVal = 0;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return lRetVal;
                }
                else
                {
                    lRetVal = Convert.ToInt64(p_sValue);
                }
            }
            catch { return 0; }
            return lRetVal;
        }

        /// <summary>		
        /// This method will convert the passed object to a string value
        /// </summary>		
        /// <param name="p_obj">The object that will be converted to a string value</param>												
        /// <returns>A string value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static string ConvertObjToStr(object p_obj)
        {
            string sRetValue = string.Empty;
            try
            {
                if (p_obj != null)
                    sRetValue = p_obj.ToString();
            }
            catch { return ""; }
            return sRetValue;
        }
        /// <summary>		
        /// This method will convert a string to an integer value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to an integer value</param>												
        /// <returns>An integer value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static int ConvertStrToInteger(string p_sValue)
        {
            int iRetVal = 0;
            bool boolErr = false;   
            //try
            //{

                if (string.IsNullOrEmpty(p_sValue))
                {
                    return iRetVal;
                }
                else if (p_sValue.ToUpper() == "TRUE")//BSB Hack to support legacy habit of hiding booleans as integers.
                    return -1;
                else
                {
                    //iRetVal = Convert.ToInt32(p_sValue);
                    boolErr = Int32.TryParse(p_sValue, out iRetVal);
                }
            //}
            //catch { return 0; }
            return iRetVal;
        }

        /// <summary>		
        /// This method will convert a valid date string in the specified date format		
        /// </summary>		
        /// <param name="p_sDataValue">Valid date string that will be formatted</param>	
        /// <param name="p_sFormat">The format in which the date string will be converted</param>	
        /// <returns>It will return date string in the specified format</returns>
        public static string GetDBDateFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDataValue))
                    return string.Empty;
                else if (!string.IsNullOrEmpty(p_sDataValue.Trim()))
                {
                        //Praveen MLChanges- overwrite Ankit MITS 29909 Changes
                        datDataValue = new DateTime(int.Parse(p_sDataValue.Substring(0, 4)), int.Parse(p_sDataValue.Substring(4, 2)), int.Parse(p_sDataValue.Substring(6, 2)));
                        sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return string.Empty; }
            return sReturnString;
        }
        /// <summary>		
        /// This method will return a valid date and time in the specified format		
        /// </summary>		
        /// <param name="p_sDataValue">A valid date time string that will be formatted</param>	
        /// <param name="p_sDateFormat">The format in which the date part will be formatted</param>	
        /// <param name="p_sTimeFormat">The format in which the time part will be formatted</param>	
        /// <returns>It returns date time string the specified format</returns>
        public static string GetDBDTTMFormat(string p_sDataValue, string p_sDateFormat, string p_sTimeFormat)
        {
            string sReturnString = string.Empty, sDatePart = string.Empty, sTimePart = string.Empty;
            string sTempDataValue = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDataValue))
                    return string.Empty;
                else if (!string.IsNullOrEmpty(p_sDataValue.Trim()))
                {
                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(4, 2) + "-" + sTempDataValue.Substring(6, 2) + "-" + sTempDataValue.Substring(0, 4);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sDatePart = datDataValue.ToString(p_sDateFormat);

                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(8, 2) + ":" + sTempDataValue.Substring(10, 2) + ":" + sTempDataValue.Substring(12, 2);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sTimePart = datDataValue.ToString(p_sTimeFormat);
                    sReturnString = sDatePart + " " + sTimePart;
                }
            }
            catch { return string.Empty; }
            return sReturnString;
        }
        /// <summary>		
        /// This method will return a valid time in the specified format		
        /// </summary>		
        /// <param name="p_sDataValue">A valid date time string that will be formatted</param>			
        /// <param name="p_sFormat">The format in which the time part will be converted</param>	
        /// <returns>It will return time in the specified format</returns>
        public static string GetDBTimeFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = "";
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                    return "";
                else if (p_sDataValue.Trim() != "")
                {
                    p_sDataValue = p_sDataValue.Substring(0, 2) + ":" + p_sDataValue.Substring(2, 2) + ":" + p_sDataValue.Substring(4, 2);
                    datDataValue = System.DateTime.Parse(p_sDataValue);
                    sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return ""; }
            return sReturnString;
        }


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetTime
        /// </summary>     
        public static string GetTime(string sValue, string p_LangCode)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetTime(sValue, p_LangCode, 0);
            }
            else
                throw new RMAppException("GetTime" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// GetTime
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sLangCode"></param>
        /// <returns></returns>
        public static string GetTime(string sValue,string sLangCode, int p_iClientId)
        {
            string sUITime = string.Empty;
            DateTimeFormatInfo currentDateTimeFormat = null;
            string svalueExpected = string.Empty;
            string sCulture = "en-US";
            if (sLangCode.Length != 4) sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
            if (m_Cache != null)
            {
                if (m_Cache.ContainsKey(sLangCode))
                {
                    sCulture = Conversion.ConvertObjToStr(m_Cache[sLangCode]);
                }
                else
                {
                    sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                    if (sCulture.Split('-').Length == 1)
                    {
                        sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                    }
                    m_Cache = new CacheLanguageTable(p_iClientId);
                    m_Cache.Add(sLangCode, sCulture);
                }
            }
            else
            {
                sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                if (sCulture.Split('-').Length == 1)
                {
                    sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                }
                m_Cache = new CacheLanguageTable(p_iClientId);
                m_Cache.Add(sLangCode, sCulture);
            }
            currentDateTimeFormat = (new CultureInfo(sCulture)).DateTimeFormat;
            if (string.IsNullOrEmpty(sValue))
            {
                return sUITime;
            }
            DateTime dtvalue = DateTime.Parse(sValue);
            try
            {
                svalueExpected = dtvalue.ToString(currentDateTimeFormat.ShortTimePattern);
                if (String.Compare(sValue, svalueExpected) != 0)
                {
                    sUITime = svalueExpected;
                }
                else
                {
                    sUITime = sValue;
                }
            }
            catch
            {
                sUITime = string.Empty;
            }

            return sUITime;
        }
        /// <summary>
        /// Provides string formatting for currency
        /// </summary>
        /// <param name="dblCurrency">double containing the amount to be formatted as currency</param>
        /// <returns>currency format specific to the current server Globalization settings</returns>
        public static string GetCurrencyFormat(double dblCurrency)
        {
            return string.Format("{0:C}",dblCurrency);
        } // method: GetCurrencyFormat

        /// <summary>		
        /// This method returns a value in the percent format		
        /// </summary>		
        /// <param name="p_dblValue">Value that has to be converted in percent format</param>					
        /// <returns>It returns value in the percent format</returns>
        public static string GetPercentFormat(double p_dblValue)
        {
            string sReturnValue = "";
            try
            {
                sReturnValue = string.Format("{0:P}", p_dblValue);
            }
            catch { return ""; }
            return sReturnValue;
        }

        /// <summary>
        /// Convert string into double
        /// </summary>
        /// <param name="p_sValue">Input string value</param>
        /// <returns>Double value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static double ConvertStrToDouble(string p_sValue)
        {
            double dblRetVal = 0;
            string str = p_sValue;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return dblRetVal;
                }
                else
                {
                    //pmahli MITS 9327 6/15/2007 "()" replaced by "-"
                    //Anu Tennyson MITS 18229 Replaced "%" by "" 1/10/2010 Starts
                    str = p_sValue.Replace(",", "").Replace("$", "").Replace("(", "-").Replace(")", "").Replace("%","");
                    //Anu Tennyson MITS 18229 Replaced "%" by "" 1/10/2010 Ends
                    dblRetVal = Convert.ToDouble(str);
                }
            }
            catch
            {
                return (0);
            }
            return (dblRetVal);
        }

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertBoolToInt
        /// </summary>     
        public static int ConvertBoolToInt(bool p_bVal)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertBoolToInt(p_bVal, 0);
            }
            else
                throw new RMAppException("ConvertBoolToInt" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        #region Converts bool to int
        /// <summary>
        /// Converts boolean value to integer
        /// </summary>
        /// <param name="p_bVal">boolean value to convert to integer</param>
        /// <returns>Converted integer value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static int ConvertBoolToInt(bool p_bVal, int p_iClientId)
        {
            int iRetVal = 0;
            try
            {
                if (!string.IsNullOrEmpty(p_bVal.ToString()))
                {
                    iRetVal = Convert.ToInt32(p_bVal);
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertBoolToInt.GeneralError", p_iClientId), p_objException);
            }
            return iRetVal;
        }

        

        #endregion

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertLongToBool
        /// </summary>     
        public static bool ConvertLongToBool(long p_lValue)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertLongToBool(p_lValue, 0);
            }
            else
                throw new RMAppException("ConvertLongToBool" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        #region Convert long to bool
        /// <summary>
        /// Converts long value To boolean
        /// </summary>
        /// <param name="p_lValue">long value to convert</param>
        /// <returns>Converted boolean value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static bool ConvertLongToBool(long p_lValue, int p_iClientId)
        {
            try
            {
                if ((p_lValue.ToString().Equals("")))
                {
                    return false;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertLongToBool.GeneralError", p_iClientId), p_objException);
            }
            return Convert.ToBoolean(p_lValue);
        }
        #endregion


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertObjToInt
        /// </summary>     
        public static Int32 ConvertObjToInt(object p_objValue)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertObjToInt(p_objValue, 0);
            }
            else
                throw new RMAppException("ConvertObjToInt" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        #region Convert object to Integer
        /// <summary>
        /// Convert passed object to integer value
        /// </summary>
        /// <param name="p_objValue">object to convert</param>
        /// <returns>Converted integer value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static Int32 ConvertObjToInt(object p_objValue, int p_iClientId)
        {
            try
            {
                if (p_objValue == null || p_objValue is System.DBNull || p_objValue.ToString() == "")
                {
                    return 0;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertObjToInt.GeneralError", p_iClientId), p_objException);
            }
            return Convert.ToInt32(p_objValue);
        }
        #endregion



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertObjToInt
        /// </summary>     
        public static Int64 ConvertObjToInt64(object p_ObjValue)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertObjToInt64(p_ObjValue, 0);
            }
            else
                throw new RMAppException("ConvertObjToInt64" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Converts Null or DBNull object to Int64
        /// </summary>
        /// <param name="p_ObjValue">object to convert</param>
        /// <returns>Converted Int64 value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static Int64 ConvertObjToInt64(object p_ObjValue, int p_iClientId)
        {
            try
            {
                if (p_ObjValue == null || p_ObjValue is System.DBNull)
                {
                    return 0;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertObjToInt64.GeneralError", p_iClientId), p_objException);
            }
            return Convert.ToInt64(p_ObjValue);
        }



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertObjToInt
        /// </summary>     
        public static bool ConvertObjToBool(object p_objVal)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertObjToBool(p_objVal, 0);
            }
            else
                throw new RMAppException("ConvertObjToBool" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        #region Converts Object to bool
        /// <summary>
        /// Converts Object value to bool
        /// </summary>
        /// <param name="p_objVal">Object to convert to bool</param>
        /// <returns>Converted bool value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static bool ConvertObjToBool(object p_objVal, int p_iClientId)
        {
            bool bRetVal = false;

            //BSB Fixes for this conversion.
            if (p_objVal is bool)
                return (bool)p_objVal;

            if (p_objVal is string)
                p_objVal = (p_objVal as string).ToLower().Replace("true", "1").Replace("false", "0");

            try
            {
                bRetVal = ConvertLongToBool(ConvertStrToLong(ConvertObjToStr(p_objVal)), p_iClientId);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertObjToBool.GeneralError", p_iClientId), p_objException);
            }
            return bRetVal;
        }
        #endregion

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertObjToDouble
        /// </summary>     
        public static Double ConvertObjToDouble(object p_objVal)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertObjToDouble(p_objVal, 0);
            }
            else
                throw new RMAppException("ConvertObjToDouble" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
           // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486



        #region Converts Object to Double
        /// <summary>
        /// Converts Object value to Double
        /// </summary>
        /// <param name="p_objVal">Object to convert to Double</param>
        /// <returns>Converted Double value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static Double ConvertObjToDouble(object p_objVal, int p_iClientId)
        {
            double dblRetVal = 0;
            try
            {
                //Changes made by Neelima
                if (p_objVal == null || p_objVal is System.DBNull)
                    dblRetVal = 0;
                else
                    dblRetVal = Convert.ToDouble(p_objVal);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Conversion.ConvertObjToDouble.GeneralError", p_iClientId), p_objException);
            }
            return dblRetVal;
        }
        #endregion

        /// <summary>
        /// Uses Generic types to perform casting to a specified 
        /// output type using the TryParse method as an alternative to 
        /// throwing exceptions since throwing exceptions is a very
        /// expensive operation
        /// </summary>
        /// <typeparam name="T">Generic type parameter indicating the
        /// required output type</typeparam>
        /// <param name="strUncastValue">string containing the value to be strongly typed</param>
        /// <param name="blnSuccess">boolean indicating whether or not the data type
        /// cast succeeded or failed</param>
        /// <returns>Generic type which has been cast to its appropriate data type</returns>
        /// <example>
        /// <code>
        /// int intReturnValue = 0;
        /// int intParamValue = 20;
        /// bool blnSuccess = false;
        /// intReturnValue = CastToType&lt;int&gt;(intParamValue.ToString(), out blnSuccess);
        /// </code>
        /// </example>
        /// <remarks>This conversion method does not yet support DateTime and TimeSpan conversions</remarks>
        public static T CastToType<T>(string strUncastValue, out bool blnSuccess) where T : IConvertible
        {
            //Get the type of the Generic type
            Type typToConvert = typeof(T);
            blnSuccess = false;

            //Get the Generic Type name to determine which cast operation to perform
            switch (typToConvert.Name)
            {
                case "Int32":
                    int intReturnValue = 0;
                    blnSuccess = Int32.TryParse(strUncastValue, out intReturnValue);
                    return (T)Convert.ChangeType(intReturnValue, typeof(T));
                    //break;
                case "Int64":
                    Int64 int64ReturnValue = 0;
                    blnSuccess = Int64.TryParse(strUncastValue, out int64ReturnValue);
                    return (T)Convert.ChangeType(int64ReturnValue, typeof(T));
                    //break;
                case "long": //64-bit Integer
                    long lngReturnValue = 0;
                    blnSuccess = long.TryParse(strUncastValue, out lngReturnValue);
                    return (T)Convert.ChangeType(lngReturnValue, typeof(T));
                    //break;
                case "Double":
                    double dblReturnValue = 0.0;
                    blnSuccess = Double.TryParse(strUncastValue, out dblReturnValue);
                    return (T)Convert.ChangeType(dblReturnValue, typeof(T));
                    //break;
                case "Single":
                    Single sngReturnValue = 0.0F;
                    blnSuccess = Single.TryParse(strUncastValue, out sngReturnValue);
                    return (T)Convert.ChangeType(sngReturnValue, typeof(T));
                    //break;
                case "float":
                    float flReturnValue = 0.0F;
                    blnSuccess = float.TryParse(strUncastValue, out flReturnValue);
                    return (T)Convert.ChangeType(flReturnValue, typeof(T));
                    //break;
                case "Boolean":
                    bool blnReturnValue = false;
                    // akaushik5 Added for MITS 31151 Starts
                    if (!string.IsNullOrEmpty(strUncastValue))
                    {
                        strUncastValue = strUncastValue.Replace("-1", "true").Replace("1", "true").Replace("0", "false");
                        strUncastValue = strUncastValue.ToLower().Replace("yes", "true").Replace("no", "false");
                        // akaushik5 Added for MITS 31151 Ends
                        blnSuccess = Boolean.TryParse(strUncastValue, out blnReturnValue);
                    }
                    return (T)Convert.ChangeType(blnReturnValue, typeof(T));
                    //break;
                case "Decimal": //decimal for money/financial conversions
                    decimal decReturnValue = 0.0M;
                    blnSuccess = Decimal.TryParse(strUncastValue, out decReturnValue);
                    return (T)Convert.ChangeType(decReturnValue, typeof(T));
                default:
                    return (T)Convert.ChangeType(string.Empty, typeof(T));
            }//switch
        }//method: CastToType<T>()

        
        /// <summary>
        /// This method will take in the time string (in the format HH24MMSS).
        /// It will return the time in AM/PM corresponding to that time string.
        /// </summary>
        /// <param name="p_sTimeString">Time string (in HH24MMSS format)</param>
        /// <example>
        ///		Returns 12:00 AM for 120000
        ///		Returns 12:00 AM for 000000
        ///		Returns 5:00  PM  for 170000
        /// </example>
        /// <returns>Time string (in AM/PM format)</returns>
        /// <remarks>This could be accomplished simply using DateTime.ToString
        /// HH is the 24 hour clock format and tt is the 2 letter abbreviation for AM/PM</remarks>
        public static string GetTimeAMPM(string p_sTimeString)
        {
            int iTime = 0;
            string sReturnTimeString = "";
            try
            {
                if ((p_sTimeString == null) || (p_sTimeString.Trim() == ""))
                    return "";

                iTime = ConvertStrToInteger(p_sTimeString.Substring(0, 2));
                if (iTime >= 12)
                {
                    iTime = iTime - 12;
                    if (iTime == 0)
                        sReturnTimeString = String.Format("00:{0} PM", p_sTimeString.Substring(2, 2));
                    else
                    {
                        // akaushik5 Changed for MITS 37849 Starts
                        //sReturnTimeString = String.Format("{0}:{1} PM", iTime, p_sTimeString.Substring(2, 2));
                        sReturnTimeString = String.Format("{0}:{1} PM", iTime<10?string.Format("0{0}",iTime):string.Format("{0}",iTime), p_sTimeString.Substring(2, 2));
                        // akaushik5 Changed for MITS 37849 Ends
                    }
                }
                else
                {
                    if (p_sTimeString.Substring(0, 2) == "00")
                        p_sTimeString = String.Format("12{0}", p_sTimeString.Substring(2, 2));

                    sReturnTimeString = p_sTimeString.Substring(0, 2) + ":"
                        + p_sTimeString.Substring(p_sTimeString.Length - 4, 2) + " AM";
                }
            }
            catch
            {
                return string.Empty;
            }
            return sReturnTimeString;
        }

        /// <summary>
        /// Convert string value to boolean value.
        /// </summary>
        /// <param name="p_sValue">Input string to convert to boolean value</param>
        /// <returns>Converted boolean value </returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static bool ConvertStrToBool(string p_sValue)
        {
            int iValue = 0;
            if (p_sValue == "" || p_sValue == null)
                return (false);
            try
            {
                if (Common.Utilities.IsNumeric(p_sValue))
                {
                    iValue = ConvertStrToInteger(p_sValue);
                    if (iValue == 0)
                        return (false);
                    else
                        return (true);
                }
                else
                {
                    if ((p_sValue.ToLower() == "true") || (p_sValue.ToLower() == "yes"))
                        return true;
                    else
                        return (false);
                }
            }
            catch
            {
                return (false);
            }
        }


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertToMoneyString
        /// </summary>     
        public static string ConvertToMoneyString(int p_iIntgerPart, int p_iDecimalPart)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertToMoneyString(p_iIntgerPart,p_iDecimalPart, 0);
            }
            else
                throw new RMAppException("ConvertToMoneyString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
              // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        /// <summary>
        /// Returns English-like sentence string equivalent To the input integer and decimal part of the money.
        /// Example : 
        /// 1234.89 will be given input by assigning 1234 as Integer part, and 89 as decimal part.
        /// The return string will be "One Thousand Two Hundred Thirty Four and 89/100 Dollars".
        /// </summary>
        /// <param name="p_iIntgerPart">Integer Part</param>
        /// <param name="p_iDecimalPart">Decimal Part</param>
        /// <returns>Returns the English-like sentence string.</returns>
        public static string ConvertToMoneyString(int p_iIntgerPart, int p_iDecimalPart, int p_iClientId)
        {
            string[] arrUnits = {
									"Zero", "One", "Two", "Three", "Four",
									"Five", "Six", "Seven", "Eight", "Nine",
									"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
									"Fifteen", "Sixteen", "Seventeen", "Eighteen",
									"Nineteen"
								};

            string[] arrTens = {
								   "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
								   "Seventy", "Eighty", "Ninety"
							   };

            bool bNegative = false;
            int iTemp = 0;
            string sReturnValue = "";

            try
            {
                if (p_iIntgerPart < 0)
                {
                    p_iIntgerPart *= -1;
                    p_iDecimalPart *= -1;
                    bNegative = true;
                    sReturnValue = "(";
                }

                iTemp = p_iIntgerPart / 1000000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Billion", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_iIntgerPart = p_iIntgerPart % 1000000000;
                }

                iTemp = p_iIntgerPart / 1000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Million", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_iIntgerPart = p_iIntgerPart % 1000000;
                }

                iTemp = p_iIntgerPart / 1000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Thousand", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_iIntgerPart = p_iIntgerPart % 1000;
                }

                CreateMoneyGroup(p_iIntgerPart, "", ref sReturnValue, arrUnits, arrTens, p_iClientId);

                if (sReturnValue == "")
                    sReturnValue = arrUnits[0] + " ";

                sReturnValue += "and " + string.Format("{0:00}", p_iDecimalPart) + "/100";
                sReturnValue += " Dollars";

                if (bNegative)
                    sReturnValue += " )";
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            return (sReturnValue);
        }
		    /// <summary>
        /// Returns English-like sentence string equivalent To the input integer and decimal part of the money.
        /// Example : 
        /// 1234.89 will be given input by assigning 1234 as Integer part, and 89 as decimal part.
        /// The return string will be "One Thousand Two Hundred Thirty Four and 89/100 Dollars".
        /// </summary>
        /// <param name="p_lIntgerPart">Integer Part</param>
        /// <param name="p_iDecimalPart">Decimal Part</param>
        /// <param name="p_sCurrCodeDesc">Code DEsc</param>
        /// <returns>Returns the English-like sentence string.</returns>
        /// 

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertToMoneyString
        /// </summary>     
        public static string ConvertToMoneyString(long p_lIntgerPart, int p_iDecimalPart, string p_sCurrCodeDesc)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertToMoneyString(p_lIntgerPart,p_iDecimalPart,p_sCurrCodeDesc, 0);
            }
            else
                throw new RMAppException("ConvertToMoneyString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
           // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static string ConvertToMoneyString(long p_lIntgerPart, int p_iDecimalPart, string p_sCurrCodeDesc, int p_iClientId)
        {
            string[] arrUnits = {
									"Zero", "One", "Two", "Three", "Four",
									"Five", "Six", "Seven", "Eight", "Nine",
									"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
									"Fifteen", "Sixteen", "Seventeen", "Eighteen",
									"Nineteen"
								};

            string[] arrTens = {
								   "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
								   "Seventy", "Eighty", "Ninety"
							   };

            bool bNegative = false;
            long iTemp = 0;
            string sReturnValue = "";
            int iStrLength;
            string strStar = string.Empty;
            try
            {
                if (p_lIntgerPart < 0)
                {
                    p_lIntgerPart *= -1;
                    p_iDecimalPart *= -1;
                    bNegative = true;
                    sReturnValue = "(";
                }

                iTemp = p_lIntgerPart / 1000000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Billion", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000000000;
                }

                iTemp = p_lIntgerPart / 1000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Million", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000000;
                }

                iTemp = p_lIntgerPart / 1000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Thousand", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000;
                }

                CreateMoneyGroup(p_lIntgerPart, "", ref sReturnValue, arrUnits, arrTens, p_iClientId);

                if (sReturnValue == "")
                    sReturnValue = arrUnits[0] + " ";

                sReturnValue += "and " + string.Format("{0:00}", p_iDecimalPart) + "/100";
               
                // sReturnValue += " Dollars";//skhare7
                sReturnValue +=" " + p_sCurrCodeDesc;//skhare7  p_sCurrCulture
                if (bNegative)
                    sReturnValue += " )";
                iStrLength = 66 - sReturnValue.Length;
                if (iStrLength > 0)
                    strStar = new string('*', iStrLength);
                sReturnValue = sReturnValue + strStar;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            return (sReturnValue);
        }
        /// <summary>
        /// Returns English-like sentence string equivalent To the input integer and decimal part of the money.
        /// Example : 
        /// 1234.89 will be given input by assigning 1234 as Integer part, and 89 as decimal part.
        /// The return string will be "One Thousand Two Hundred Thirty Four and 89/100 Dollars".
        /// </summary>
        /// <param name="p_lIntgerPart">Integer Part</param>
        /// <param name="p_iDecimalPart">Decimal Part</param>
        /// <returns>Returns the English-like sentence string.</returns>
        /// 

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertToMoneyString
        /// </summary>     
        public static string ConvertToMoneyString(long p_lIntgerPart, int p_iDecimalPart)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertToMoneyString(p_lIntgerPart, p_iDecimalPart, 0);
            }
            else
                throw new RMAppException("ConvertToMoneyString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        public static string ConvertToMoneyString(long p_lIntgerPart, int p_iDecimalPart, int p_iClientId)
        {
            string[] arrUnits = {
									"Zero", "One", "Two", "Three", "Four",
									"Five", "Six", "Seven", "Eight", "Nine",
									"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
									"Fifteen", "Sixteen", "Seventeen", "Eighteen",
									"Nineteen"
								};

            string[] arrTens = {
								   "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
								   "Seventy", "Eighty", "Ninety"
							   };

            bool bNegative = false;
            long iTemp = 0;
            string sReturnValue = "";
            int iStrLength;
            string strStar = string.Empty;
            try
            {
                if (p_lIntgerPart < 0)
                {
                    p_lIntgerPart *= -1;
                    p_iDecimalPart *= -1;
                    bNegative = true;
                    sReturnValue = "(";
                }

                iTemp = p_lIntgerPart / 1000000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Billion", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000000000;
                }

                iTemp = p_lIntgerPart / 1000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Million", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000000;
                }

                iTemp = p_lIntgerPart / 1000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Thousand", ref sReturnValue, arrUnits, arrTens, p_iClientId);
                    p_lIntgerPart = p_lIntgerPart % 1000;
                }

                CreateMoneyGroup(p_lIntgerPart, "", ref sReturnValue, arrUnits, arrTens, p_iClientId);

                if (sReturnValue == "")
                    sReturnValue = arrUnits[0] + " ";

                sReturnValue += "and " + string.Format("{0:00}", p_iDecimalPart) + "/100";
                sReturnValue += " Dollars";

                if (bNegative)
                    sReturnValue += " )";
                iStrLength = 66 - sReturnValue.Length;
                if(iStrLength>0)
                    strStar = new string('*', iStrLength);
                sReturnValue = sReturnValue + strStar;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            return (sReturnValue);
        }

        /// <summary>		
        /// By Yatharth :This method will convert GMT to Org Level Time		
        /// </summary>		
        /// <param name="p_sTimeZoneCode">Time Zone of Org Level</param>	
        /// <param name="p_sTime">Time to be changed</param>	
        /// <returns>It returns time string in Org Level Time</returns>
        public static DateTime ConvertGMTToOrgHierarchyTime(string p_sTimeZoneCode, DateTime p_sTime)
        {
            //Code to convert a time of one time zone to another time zone
            DateTime target;
            TimeZoneInfo timezone; //this class is available in .NET 3.5/4.0

            switch (p_sTimeZoneCode.ToUpper())
            {

                case "AT": timezone = TimeZoneInfo.FindSystemTimeZoneById("Atlantic Standard Time"); 
                    break;

                case "EST": timezone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    break;

                case "CST": timezone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                    break;

                case "MST": timezone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
                    break;

                case "PST": timezone = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
                    break;

                case "AL": timezone = TimeZoneInfo.FindSystemTimeZoneById("Alaskan Standard Time"); 
                    break;

                case "HW": timezone = TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time"); 
                    break;

                default: timezone = TimeZoneInfo.Local;
                    break;

            }

            target = TimeZoneInfo.ConvertTime(p_sTime, timezone);

            return target;
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetUIDate
        /// </summary>     
        public static string GetUIDate(string sValue, string p_LangCode)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetUIDate(sValue, p_LangCode, 0);
            }
            else
                throw new RMAppException("GetUIDate" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// GetDate According to Lang Code
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="p_LangCode"></param>
        /// <returns>converts DB date to Culture specific date</returns>        
        public static string GetUIDate(string sValue, string p_LangCode,int p_iClientId)
        {
            string sUIDate = string.Empty;
            string svalueExpected = string.Empty;
            string sDateFormat = string.Empty;
            DateTime dtvalue ;
            try
            {
                if (string.IsNullOrEmpty(sValue))
                {
                    return sUIDate;
                }
                Regex rgx = new Regex("[^a-z0-9]");
                if (rgx.IsMatch(sValue))
                {
                    //true;
                    dtvalue = DateTime.Parse(sValue);
                }
                else
                {
                    // false;
                    if (sValue.Length > 10)
                    {
                        dtvalue = DateTime.ParseExact(sValue, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtvalue = DateTime.ParseExact(sValue, "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                }

               // DateTime dtvalue = DateTime.ParseExact(sValue,"yyyyMMdd",CultureInfo.InvariantCulture);
               // DateTime dtvalue = DateTime.Parse(sValue);
               // DateTime dtvalue = DateTime.ParseExact(sValue, "dd/MM/yyyy", null);
                if (p_LangCode != string.Empty)
                {
                    sDateFormat = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=" + p_LangCode).ToString();
                }
                svalueExpected = dtvalue.ToString(sDateFormat);
                if (String.Compare(sValue, svalueExpected) != 0)
                {
                    sUIDate = svalueExpected;
                }
                else
                {
                    sUIDate = sValue;
                }
            }
            catch
            {
                sUIDate = sValue;
            }
            return sUIDate;
        }


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetUITime
        /// </summary>     
        public static string GetUITime(string sValue, string p_LangCode)
        {
            if (CacheCommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetUITime(sValue, p_LangCode, 0);
            }
            else
                throw new RMAppException("GetUITime" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486  
        
        /// <summary>
        /// Get time According to Lang Code
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sLangCode"></param>
        /// <returns>converts DB time to Culture specific time</returns>        
        public static string GetUITime(string sValue, string sLangCode, int p_iClientId)
        {
            string sUITime = string.Empty;
            DateTimeFormatInfo currentDateTimeFormat = null;
            string svalueExpected = string.Empty;
            string sCulture = "en-US";
            DateTime dtvalue;
            if (sLangCode.Length != 4) sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
            if (m_Cache != null)
            {
                if (m_Cache.ContainsKey(sLangCode))
                {
                    sCulture = Conversion.ConvertObjToStr(m_Cache[sLangCode]);
                }
                else
                {
                    sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                    if (sCulture.Split('-').Length == 1)
                    {
                        sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                    }
                    m_Cache = new CacheLanguageTable(p_iClientId);
                    m_Cache.Add(sLangCode, sCulture);
                }
            }
            else
            {
                sCulture = Db.DbFactory.ExecuteScalar(RMConfigurationSettings.GetSecurityDSN(p_iClientId), "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode).ToString();
                if (sCulture.Split('-').Length == 1)
                {
                    sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
                }
                m_Cache = new CacheLanguageTable(p_iClientId);
                m_Cache.Add(sLangCode, sCulture);
            }
            currentDateTimeFormat = (new CultureInfo(sCulture)).DateTimeFormat;
            if (string.IsNullOrEmpty(sValue))
            {
                return sUITime;
            }
            if (sValue.Length < 7)
            {
                sValue = "20010101" + sValue;
            }
            dtvalue = DateTime.ParseExact(sValue, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
           
            try
            {

                svalueExpected = dtvalue.ToString(currentDateTimeFormat.ShortTimePattern);
                if (String.Compare(sValue, svalueExpected) != 0)
                {
                    sUITime = svalueExpected;
                }
                else
                {
                    sUITime = sValue;
                }
            }
            catch
            {
                sUITime = string.Empty;
            }

            return sUITime;
        }
        #endregion
    }//class
}//namespace
