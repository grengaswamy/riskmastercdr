﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Db;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Newtonsoft.Json;

namespace Riskmaster.Common
{
    public class GridCommonFunctions
    {
        //RMA-9681 Starts

        public enum ClientSideSortFunctions
        {
            DefaultSortingFunction = 0,
            DateTimeSortingFunction = 1,
            NumericSortingFunction = 2
        }


        /// <summary>
        /// Author		: rkotak
        /// Date Created: 05/10/2015
        /// Sets the user preference for all in User_grid_Preferences
        /// </summary>
        /// <param name="p_objResponseOut">XmlDocument</param>
        ///  <param name="p_UserID">Int</param>
        ///  <param name="iClientId">Int</param>
        ///  <param name="p_iPageID">Int</param>
        ///   <param name="p_iDsnID">Int</param>
        public static void GetUserHeaderAndPreference(ref string p_objResponseOut, int p_UserID, int iClientId, string p_sPageName, string p_iDsnID, string p_sGridId)
        {
            DbReader objReader = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();
            string sPageID = "0";
            bool bOut=false;
            try
            {
                sPageID = Globalization.GetPageId(p_sPageName, iClientId);
                strSQL.AppendFormat("SELECT GRID_CONFIG FROM USER_GRID_PREFERENCES WHERE USERID = {0} AND PAGEID= {1} AND DSNID={2} AND GRIDID = {3}", "~USERID~", "~PAGEID~", "~DSNID~", "~GRIDID~");
                dictParams.Add("USERID", p_UserID);
                dictParams.Add("PAGEID", Conversion.CastToType<int>(sPageID, out bOut));
                dictParams.Add("DSNID", Conversion.CastToType<int>(p_iDsnID, out bOut));
                dictParams.Add("GRIDID", p_sGridId);                

                objReader = DbFactory.ExecuteReader(Riskmaster.Cache.ConfigurationInfo.GetViewConnectionString(iClientId), strSQL.ToString(), dictParams);

                if (objReader.Read())
                    p_objResponseOut = Conversion.ConvertObjToStr(objReader.GetValue("GRID_CONFIG"));
            }// end of try block

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("GridPreferenceAdaptor.GetUserHeaderAndPreference.Err", iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }

        public static void SaveUserHeaderAndPreference(string p_sUserPrefXml, int p_iUserId, int iClientId, string s_sPageName, int p_iDsnID, string p_sGridId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();
            string p_ViewConstring = Riskmaster.Cache.ConfigurationInfo.GetViewConnectionString(iClientId);
            string sPageID = "0";
            
            try
            {
                if (DeleteUserPreference(p_iUserId, iClientId, s_sPageName, p_iDsnID, p_sGridId))
                {
                    sPageID = Globalization.GetPageId(s_sPageName, iClientId);
                    //sPageID = "1";                    
                    objWriter = DbFactory.GetDbWriter(p_ViewConstring);                    
                    objWriter.Tables.Add("USER_GRID_PREFERENCES");                   
                    if (DbFactory.GetDatabaseType(p_ViewConstring) == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        objWriter.Fields.Add("ROW_ID", DbFactory.ExecuteScalar(p_ViewConstring, "SELECT SEQ_GRID_ROWID.NEXTVAL FROM DUAL"));
                    }
                    objWriter.Fields.Add("USERID", p_iUserId);
                    objWriter.Fields.Add("PAGEID", sPageID);
                    objWriter.Fields.Add("GRIDID", p_sGridId);
                    objWriter.Fields.Add("DSNID", p_iDsnID);
                    objWriter.Fields.Add("GRID_CONFIG", p_sUserPrefXml);                 
                    objWriter.Execute();
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("GridPreferenceAdaptor.SetUserHeaderAndPreferenceToDb.Err", iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
        }


        public static bool DeleteUserPreference(int p_iUserId, int iClientId, string s_sPageName, int p_iDsnID, string p_sGridId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();
            string p_Constring = Riskmaster.Cache.ConfigurationInfo.GetViewConnectionString(iClientId);
            string sPageID = "0";
            bool bSuccess = false;
            bool bOut = false;
            try
            {
                sPageID = Globalization.GetPageId(s_sPageName, iClientId);
                //sPageID = "1";
                strSQL.AppendFormat("DELETE FROM USER_GRID_PREFERENCES WHERE USERID = {0} AND PAGEID= {1} AND DSNID={2} AND GRIDID={3}", "~USERID~", "~PAGEID~", "~DSNID~", "~GRIDID~");
                dictParams.Add("USERID", p_iUserId);
                dictParams.Add("PAGEID", Conversion.CastToType<int>(sPageID, out bOut));
                dictParams.Add("DSNID", p_iDsnID);
                dictParams.Add("GRIDID", p_sGridId); 
               
                DbFactory.ExecuteNonQuery(p_Constring, strSQL.ToString(), dictParams);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("GridPreferenceAdaptor.DeleteUserPreference.Err", iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
                strSQL = null; //34278
                dictParams = null; //34278
            }
            return bSuccess;
        }

        /// <summary>
        /// creates user pref xml
        /// </summary>
        /// <param name="lstColDef"></param>    
        /// <returns></returns>
        public static string CreateUserPrefJSON(GridPreference oGridPreference)
        {
            try
            {
                return JsonConvert.SerializeObject(oGridPreference);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
        }


        public static string GetSortColumnValueForDateField(string p_sDbValue)
        {
            string sDBstringInYYYYMMDD = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDbValue))
                    sDBstringInYYYYMMDD = string.Empty;
                else if (!string.IsNullOrEmpty(p_sDbValue.Trim()))
                {
                    if (p_sDbValue.Length >= 8)
                        sDBstringInYYYYMMDD = p_sDbValue.Substring(0, 4) + "/" + p_sDbValue.Substring(4, 2) + "/" + p_sDbValue.Substring(6, 2);
                    if (p_sDbValue.Length >= 14)
                        sDBstringInYYYYMMDD = sDBstringInYYYYMMDD + " " + p_sDbValue.Substring(8, 2) + ":" + p_sDbValue.Substring(10, 2) + ":" + p_sDbValue.Substring(12, 2);
                }
            }
            catch (Exception ex) { throw ex; }
            return sDBstringInYYYYMMDD;
        }

        //<param name="p_sDataValue"></param>
        //<param name="p_sFormat"></param>
        //<returns></returns>
        public static void GetSortColumnForDateField(ref Dictionary<string,object> dicGridData,string sDateFeildKey, string p_sDbValue)
        {
            string sDBstringInYYYYMMDD = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDbValue))
                    sDBstringInYYYYMMDD = string.Empty;
                else if (!string.IsNullOrEmpty(p_sDbValue.Trim()))
                {
                    if (p_sDbValue.Length >= 8)
                        sDBstringInYYYYMMDD = p_sDbValue.Substring(0, 4) + "/" + p_sDbValue.Substring(4, 2) + "/" + p_sDbValue.Substring(6, 2);
                    if (p_sDbValue.Length >= 14)
                        sDBstringInYYYYMMDD = sDBstringInYYYYMMDD + " " + p_sDbValue.Substring(8, 2) + ":" + p_sDbValue.Substring(10, 2) + ":" + p_sDbValue.Substring(12, 2);
                }
                dicGridData.Add(sDateFeildKey + ".DbValue", sDBstringInYYYYMMDD);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// gets sort column for currency feilds
        /// </summary>
        /// <param name="p_sDataValue"></param>
        /// <param name="p_sFormat"></param>
        /// <returns></returns>
        public static void GetSortColumnForCurrencyField(ref Dictionary<string, object> dicGridData, string sFeildKey, object dDBValue)
        {
            dicGridData.Add(sFeildKey + ".DbValue", dDBValue);
        }

        //RMA-13782 achouhan3   Added to get class object from string 
        /// <summary>
        /// This method is designed to get the list of GridColumnDef helper entity
        /// </summary>
        /// <param name="strUserPreference"></param>
        /// <returns>List of Grid Column Def Helper Entity</returns>
        public static GridPreference GetListforUserPreference(string strUserPreference)
        {
            GridPreference objGridUserPreference = JsonConvert.DeserializeObject<GridPreference>(strUserPreference);
            return objGridUserPreference;
        }
    }
}
    