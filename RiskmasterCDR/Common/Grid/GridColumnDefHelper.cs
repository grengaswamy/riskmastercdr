﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.Common
{
    public class GridColumnDefHelper
    {// Defines a column properties for header, sorting, filtering, cell template, cell text

        
        private string m_dateTimeSortingFunctionName = "dateTimeClientSideSort";
        private string m_numberStringSortingFunction = "sortNumberStrWithNegativeValue";// use this function when your cell data has formatted number values like formatted currency string or certain row in the grid will have empty string "" for number values. NG will not automatically aply number sorting. we need to tell ng to apply numbersorting
        public string field { get; set; }
        public string displayName { get; set; }
        public string defaultName { get; set; }
        public bool visible { get; set; }
        public string cellTemplate { get; set; }
        public string width { get; set; }
        public string headerCellTemplate { get; set; }
        public int position { get; set; }
        public bool key { get; set; }
        public bool alwaysInvisibleOnColumnMenu { get; set; }
        public string sortingAlgorithm { get; private set; }
        public string sortFn { get; private set; }


        public GridColumnDefHelper(string p_sfield, string p_sdisplayName, bool p_bvisible = true, string p_sCellTemplate = "", string p_sWidth = "", string p_sHeaderCellTemplate = "", bool p_bKey = false, bool p_bAlwaysInvisibleOnColumnMenu = false,  GridCommonFunctions.ClientSideSortFunctions p_Enum_ClientSideSortFunctions=GridCommonFunctions.ClientSideSortFunctions.DefaultSortingFunction)
        {
            field = p_sfield;
            displayName = p_sdisplayName;
            visible = p_bvisible;
            if (p_sWidth == "")
                width = "150";
            else
                width = p_sWidth;
            // template defines filter , sort, header properties
            if (p_sHeaderCellTemplate == "")
            {
                headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                           "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                           "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                           "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                           "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
            }
            else
                headerCellTemplate = p_sHeaderCellTemplate;
            if (p_sCellTemplate == string.Empty)
                cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
            else
                cellTemplate = p_sCellTemplate;
            alwaysInvisibleOnColumnMenu = p_bAlwaysInvisibleOnColumnMenu;
            defaultName = p_sdisplayName;
            key = p_bKey;            
            switch (p_Enum_ClientSideSortFunctions)
            {
                case  GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction:
                    sortingAlgorithm = m_dateTimeSortingFunctionName;
                    sortFn = m_dateTimeSortingFunctionName;
                    break;
                case GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction:
                    sortingAlgorithm = m_numberStringSortingFunction;
                    sortFn = m_numberStringSortingFunction;
                    break;
                case GridCommonFunctions.ClientSideSortFunctions.DefaultSortingFunction:
                    sortingAlgorithm = "";
                    sortFn = "";
                    break;
            }
        }

        #region commented

        //public GridColumnDefHelper(string p_sfield, string p_sdisplayName)
        //{
        //    field = p_sfield;
        //    displayName = p_sdisplayName;
        //    visible = true;
        //    width = "150";
        //    // template defines filter , sort, header properties
        //    headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
        //               "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
        //               "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
        //               "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
        //               "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        //    cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
        //    alwaysInvisibleOnColumnMenu = false;
        //    defaultName = p_sdisplayName;
        //    key = false;
        //    sortFn = "";
        //    sortingAlgorithm = "";
        //}

        //public GridColumnDefHelper(string p_sfield, string p_sdisplayName, int iEnum_ClientSideSortFunctions)
        //{
        //    field = p_sfield;
        //    displayName = p_sdisplayName;
        //    visible = true;
        //    width = "150";
        //    // template defines filter , sort, header properties
        //    headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
        //               "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
        //               "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
        //               "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
        //               "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        //    cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
        //    alwaysInvisibleOnColumnMenu = false;
        //    defaultName = p_sdisplayName;
        //    key = false;
        //    if (iEnum_ClientSideSortFunctions == (int)GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction)
        //    {
        //        sortingAlgorithm = m_dateTimeSortingFunctionName;
        //        sortFn = m_dateTimeSortingFunctionName;
        //    }
        //    else if (iEnum_ClientSideSortFunctions == (int)GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction)
        //    {
        //        sortingAlgorithm = m_numberStringSortingFunction;
        //        sortFn = m_numberStringSortingFunction;
        //    }
        //}

        //public GridColumnDefHelper(string p_sfield, string p_sdisplayName, int iEnum_ClientSideSortFunctions,string sCelTemplate)
        //{
        //    field = p_sfield;
        //    displayName = p_sdisplayName;
        //    visible = true;
        //    width = "150";
        //    // template defines filter , sort, header properties
        //    headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
        //               "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
        //               "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
        //               "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
        //               "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        //    cellTemplate = sCelTemplate;
        //    alwaysInvisibleOnColumnMenu = false;
        //    defaultName = p_sdisplayName;
        //    key = false;
        //    if (iEnum_ClientSideSortFunctions == (int)GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction)
        //    {
        //        sortingAlgorithm = m_dateTimeSortingFunctionName;
        //        sortFn = m_dateTimeSortingFunctionName;
        //    }
        //}

        //public GridColumnDefHelper(string p_sfield, string p_sdisplayName, bool p_alwaysInvisibleOnColumnMenu, bool p_visible)
        //{
        //    field = p_sfield;
        //    displayName = p_sdisplayName;
        //    visible = p_visible;
        //    width = "150";
        //    // template defines filter , sort, header properties
        //    headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
        //               "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
        //               "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
        //               "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
        //               "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        //    cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
        //    alwaysInvisibleOnColumnMenu = p_alwaysInvisibleOnColumnMenu;
        //    defaultName = p_sdisplayName;
        //    key = false;
        //    sortFn = "";
        //    sortingAlgorithm = "";
        //}

        //public GridColumnDefHelper(string p_sfield, string p_sdisplayName, string p_sCellTemplate)
        //{
        //    field = p_sfield;
        //    displayName = p_sdisplayName;
        //    visible = true;
        //    width = "150";
        //    // template defines filter , sort, header properties
        //    headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
        //               "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
        //               "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
        //               "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
        //               "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        //    cellTemplate = p_sCellTemplate;
        //    alwaysInvisibleOnColumnMenu = false;
        //    defaultName = p_sdisplayName;
        //    key = false;
        //    sortFn = "";
        //    sortingAlgorithm = "";
        //}
        #endregion

    }
   
}