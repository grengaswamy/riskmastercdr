﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using Riskmaster.Common;
namespace ClaimBalancing
{
    class AppHelper
    {
        public string GetErrorLogFileName()
        {
            return string.Empty;
        }
        public void WriteErrorToFile(string sErrorMsg,Exception ex)
        {
            try
            {


                string sFolderPath = RMConfigurator.BasePath + "\\userdata\\Claim Balancing\\";
                string sFileName=string.Empty;
                //string sFolderPath = ConfigurationSettings.AppSettings["ErrorLogLocation"].ToString();
                //string sFolderPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + "ClaimBalancingLog";
                if (!Directory.Exists(sFolderPath))
                    Directory.CreateDirectory(sFolderPath);
                //string sFileName = "ErrorLog_" + System.DateTime.Now.Year.ToString() + "-" + System.DateTime.Now.Month.ToString() + "-" + System.DateTime.Now.Day.ToString() + ".txt";
                if (Program.sPolicySystemName == string.Empty)
                    sFileName = "ErrorLog_ClaimBalancing.txt";
                else
                    sFileName = "ErrorLog_" + Program.sPolicySystemName + "_ClaimBalancing.txt";
                string scompletePath = sFolderPath + "\\" + sFileName;
                StringBuilder lines = new StringBuilder();
                StackTrace st = new StackTrace(ex, true);
                //StackFrame[] frames = st.GetFrames();
                // Iterate over the frames extracting the information you need        
                StackFrame frame = st.GetFrame(0);
                StackFrame frame1 = st.GetFrame(st.FrameCount - 1);
                lines.AppendLine("Error Occured On : " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString());
                lines.AppendLine("Error Log :");
                lines.AppendLine("File Name : " + frame.GetFileName());
                lines.AppendLine("Method Name : " + frame.GetMethod().Name);
                lines.AppendLine("Line Number : " + frame.GetFileLineNumber().ToString());
                lines.AppendLine("Detailed Error Message : " + sErrorMsg);
                lines.AppendLine("---------------------------------------------------------------------");
                lines.AppendLine("     ");
                File.AppendAllText(scompletePath, lines.ToString());
            }
            catch (Exception exc)
            {
                //do nothing
            }
        }

        public void WriteErrorToFile(string sErrorMsg)
        {
            try
            {
                //string sFolderPath = ConfigurationSettings.AppSettings["ErrorLogLocation"].ToString();
                //string sFolderPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + "ClaimBalancingLog";
                string sFolderPath = RMConfigurator.BasePath + "\\userdata\\Claim Balancing\\";
                string sFileName = string.Empty;
                if (!Directory.Exists(sFolderPath))
                    Directory.CreateDirectory(sFolderPath);
                //string sFileName = "ErrorLog_" + System.DateTime.Now.Year.ToString() + "-" + System.DateTime.Now.Month.ToString() + "-" + System.DateTime.Now.Day.ToString() + ".txt";
                string scompletePath = sFolderPath + "\\" + sFileName;
                StringBuilder lines = new StringBuilder();                
                lines.AppendLine("Error Occured On : " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString());
                lines.AppendLine("Error Log :");              
                lines.AppendLine("Detailed Error Message : " + sErrorMsg);
                lines.AppendLine("---------------------------------------------------------------------");
                lines.AppendLine("     ");
                File.AppendAllText(scompletePath, lines.ToString());
            }
            catch (Exception exc)
            {
                //do nothing
            }
        }
    }
}
