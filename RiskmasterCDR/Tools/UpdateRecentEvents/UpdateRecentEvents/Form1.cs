﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Common;
using Riskmaster.Db;
namespace UpdateRecentEvents
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string srecordtype="event";
            string srecordid = textBox2.Text;
            string suserid = textBox1.Text;
            string sconnstring= ConfigurationManager.ConnectionStrings["RMX"].ConnectionString;
            int eventid = 0;
            using (Riskmaster.Db.DbConnection db = DbFactory.GetDbConnection(sconnstring))
            {
               db.Open();
               eventid = (int)db.ExecuteScalar("select event_id from event where event_number='" + srecordid + "'");               
            }
            CommonFunctions.UpdateRecentRecords(sconnstring, suserid, srecordtype, eventid.ToString());
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
