﻿using System;
using System.Configuration;
using Riskmaster.Tools.UnitTest;

namespace Riskmaster.Tools.GenerateDataModelSchema
{
    class Program
    {
        static void Main(string[] args)
        {
            const int TOTAL_ARGS = 4;

            try
            {


                //Check if there are any arguments being passed
                if (args.Length == TOTAL_ARGS)
                {
                    SchemaTests objSchemaTest = new SchemaTests(args[0], args[1], args[2], args[3]);

                    //Generate the Data Model schema
                    objSchemaTest.GenerateSchema();

                    //Clean up
                    objSchemaTest = null;
                }//if
                //No arguments have been passed at the command line
                else
                {
                    //Read all of the configuration information from the App.config file instead
                    SchemaTests objSchemaTest = new SchemaTests(ConfigurationManager.AppSettings["WorkPath"], ConfigurationManager.AppSettings["DataSourceTarget"], ConfigurationManager.AppSettings["DataSourceUserId"], ConfigurationManager.AppSettings["DataSourcePassword"]);

                    //Generate the Data Model schema
                    objSchemaTest.GenerateSchema();

                    //Clean up
                    objSchemaTest = null;
                }//else
            }
            catch (Exception ex)
            {

                //Check to see if an InnerException exists
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }//if
                else
                {
                    Console.WriteLine(ex.Message);
                }//else

                //Wait for user response to close the console
                //if the system is being run interactively
                //Console.ReadKey();
            }
 
        }
    }
}
