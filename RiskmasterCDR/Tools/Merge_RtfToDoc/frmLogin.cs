using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Riskmaster.Security;  
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Tools.RtfToDoc
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	public class frmLogin : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtUserName;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cboDSN;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private Login m_objLogin = null;
		private UserLogin m_objUser = null;
		private string m_sDSN = string.Empty;
		private string m_sUserID = string.Empty;
		private string m_sPassword = string.Empty;
		private string m_ConnectionString = string.Empty;
		private System.Windows.Forms.Button btnExit;
		private bool m_bLogin = false;
        private bool m_bLoaded = false;
        private Label lblMessage;
		private eDatabaseType m_DatabaseType = 0;

		public frmLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboDSN = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(112, 19);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(160, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(112, 59);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(160, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(24, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // cboDSN
            // 
            this.cboDSN.Location = new System.Drawing.Point(112, 99);
            this.cboDSN.Name = "cboDSN";
            this.cboDSN.Size = new System.Drawing.Size(160, 21);
            this.cboDSN.TabIndex = 4;
            this.cboDSN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboDSN_KeyPress);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Database:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(32, 147);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(88, 32);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(152, 147);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.ForeColor = System.Drawing.Color.Red;
            this.lblMessage.Location = new System.Drawing.Point(32, 200);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 8;
            // 
            // frmLogin
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(304, 246);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboDSN);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label1);
            this.Name = "frmLogin";
            this.Text = "Login to Riskmaster";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void frmLogin_Load(object sender, System.EventArgs e)
		{
			string[] arrDb;
            if (!m_bLoaded)
            {
                Login objLogin = new Login();
                arrDb = objLogin.GetDatabases();
                cboDSN.DataSource = arrDb;
                objLogin.Dispose();
                objLogin = null;
                m_bLoaded = true;
            }
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
                lblMessage.Text = string.Empty;
				m_sDSN = cboDSN.Text;
				
				//Check for entered values
				m_sUserID = txtUserName.Text.Trim();
				m_sPassword = txtPassword.Text.Trim();
				if( m_sUserID.Length == 0 )
				{
					MessageBox.Show("Please enter a valid user name.");
					txtUserName.Focus();
					return;
				}
				if( m_sPassword.Length == 0 )
				{
					MessageBox.Show("Please enter a valid password.");
					txtPassword.Focus();
					return;
				}

				m_bLogin = true;
				this.Hide();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
            m_bLogin = false;
			this.Close();
		}

		public string UserID
		{
			get{ return m_sUserID; }
		}

		public string Password
		{
			get{ return m_sPassword; }
		}

		public string DSN
		{
			get{ return m_sDSN; }
		}

		public bool LoginSuccess
		{
			get{ return m_bLogin; }
		}

        public string LoginMessage
        {
            set { lblMessage.Text = value; }
        }

        private void cboDSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
	}
}
