VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBISSSO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function DecodeUserIDPWD(ByVal sEncodedSSOString As String, ByRef sUserID As Variant, ByRef sPWD As Variant) As Boolean
    Dim oCrypt As New DTGCrypt
    Dim iPos As Integer
    Dim iPos2 As Integer
    Dim s As String, p As String
    Dim sTmp As String
    
    DecodeUserIDPWD = False
    
    sTmp = oCrypt.DecryptString(sEncodedSSOString, "6378b87457a5ecac8674e9bac12e7cd9")
    
    iPos = InStr(sTmp, "USERID=")
    iPos2 = InStr(sTmp, "##PWD=")
    
    If iPos = 0 Or iPos2 = 0 Then
        Exit Function
    End If
    
    s = oCrypt.DecryptString(Mid$(sTmp, 8, iPos2 - 8), "6378b87457a5ecac8674e9bac12e7cd9")
    p = oCrypt.DecryptString(Mid$(sTmp, iPos2 + 6), "6378b87457a5ecac8674e9bac12e7cd9")
    
    sUserID = s
    sPWD = p
    DecodeUserIDPWD = True
End Function

Function GetUserID(ByVal sEncodedSSOString As String) As String
    Dim sUserID As Variant, sPWD As Variant
    Dim b As Boolean
    
    b = DecodeUserIDPWD(sEncodedSSOString, sUserID, sPWD)
    GetUserID = sUserID & ""
End Function

Function GetPWD(ByVal sEncodedSSOString As String) As String
    Dim sUserID As Variant, sPWD As Variant
    Dim b As Boolean
    
    b = DecodeUserIDPWD(sEncodedSSOString, sUserID, sPWD)
    GetPWD = sPWD & ""
End Function

