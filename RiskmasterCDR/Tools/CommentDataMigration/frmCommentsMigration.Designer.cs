﻿namespace Riskmaster.Tools.C2C
{
    partial class frmCommentsMigration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCommentsMigration));
            this.wizard1 = new DevComponents.DotNetBar.Wizard();
            this.wpWelcome = new DevComponents.DotNetBar.WizardPage();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.wpDSNSelection = new DevComponents.DotNetBar.WizardPage();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.lvDatabases = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.dsn = new System.Windows.Forms.ColumnHeader();
            this.wpCommentsMigration = new DevComponents.DotNetBar.WizardPage();
            this.lblDatabase = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btnStart = new DevComponents.DotNetBar.ButtonX();
            this.txtNotesCount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCommentCount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtProcessingNow = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rtfStatus = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtProgressStatusMessage = new System.Windows.Forms.Label();
            this.prgBrNotesSave = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.wizard1.SuspendLayout();
            this.wpWelcome.SuspendLayout();
            this.wpDSNSelection.SuspendLayout();
            this.wpCommentsMigration.SuspendLayout();
            this.SuspendLayout();
            // 
            // wizard1
            // 
            this.wizard1.BackColor = System.Drawing.Color.White;
            this.wizard1.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.wizard1.Cursor = System.Windows.Forms.Cursors.Default;
            this.wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizard1.FinishButtonTabIndex = 3;
            // 
            // 
            // 
            this.wizard1.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.wizard1.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.wizard1.HeaderDescriptionVisible = false;
            this.wizard1.HeaderHeight = 90;
            this.wizard1.HeaderImage = ((System.Drawing.Image)(resources.GetObject("wizard1.HeaderImage")));
            this.wizard1.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.wizard1.HeaderImageSize = new System.Drawing.Size(699, 52);
            // 
            // 
            // 
            this.wizard1.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.HeaderStyle.BackColorGradientAngle = 90;
            this.wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.wizard1.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.wizard1.HeaderStyle.BorderBottomWidth = 1;
            this.wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.HeaderStyle.BorderLeftWidth = 1;
            this.wizard1.HeaderStyle.BorderRightWidth = 1;
            this.wizard1.HeaderStyle.BorderTopWidth = 1;
            this.wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.HeaderTitleIndent = 713;
            this.wizard1.HelpButtonVisible = false;
            this.wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.wizard1.Location = new System.Drawing.Point(4, 1);
            this.wizard1.Name = "wizard1";
            this.wizard1.Size = new System.Drawing.Size(750, 502);
            this.wizard1.TabIndex = 0;
            this.wizard1.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.wpWelcome,
            this.wpDSNSelection,
            this.wpCommentsMigration});
            this.wizard1.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_CancelButtonClick);
            this.wizard1.WizardPageChanging += new DevComponents.DotNetBar.WizardCancelPageChangeEventHandler(this.wizard1_WizardPageChanging);
            // 
            // wpWelcome
            // 
            this.wpWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpWelcome.AntiAlias = false;
            this.wpWelcome.BackColor = System.Drawing.Color.Transparent;
            this.wpWelcome.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.wpWelcome.Controls.Add(this.labelX7);
            this.wpWelcome.Controls.Add(this.labelX6);
            this.wpWelcome.Location = new System.Drawing.Point(7, 102);
            this.wpWelcome.Name = "wpWelcome";
            this.wpWelcome.PageHeaderImage = ((System.Drawing.Image)(resources.GetObject("wpWelcome.PageHeaderImage")));
            this.wpWelcome.Size = new System.Drawing.Size(736, 342);
            this.wpWelcome.TabIndex = 7;
            // 
            // labelX7
            // 
            this.labelX7.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(15, 14);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(504, 44);
            this.labelX7.TabIndex = 4;
            this.labelX7.Text = "Welcome to the Comments Data Migration Wizard";
            // 
            // labelX6
            // 
            this.labelX6.Location = new System.Drawing.Point(60, 64);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(421, 82);
            this.labelX6.TabIndex = 3;
            this.labelX6.Text = "This wizard will guide you through migrating your current Claim Comments.\r\n\r\nTo c" +
                "ontinue, click Next.";
            // 
            // wpDSNSelection
            // 
            this.wpDSNSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpDSNSelection.AntiAlias = false;
            this.wpDSNSelection.BackColor = System.Drawing.Color.Transparent;
            this.wpDSNSelection.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.wpDSNSelection.Controls.Add(this.labelX5);
            this.wpDSNSelection.Controls.Add(this.lvDatabases);
            this.wpDSNSelection.Location = new System.Drawing.Point(7, 102);
            this.wpDSNSelection.Name = "wpDSNSelection";
            this.wpDSNSelection.PageHeaderImage = ((System.Drawing.Image)(resources.GetObject("wpDSNSelection.PageHeaderImage")));
            this.wpDSNSelection.Size = new System.Drawing.Size(736, 342);
            this.wpDSNSelection.TabIndex = 9;
            // 
            // labelX5
            // 
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(0, 0);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(515, 29);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.labelX5.TabIndex = 1;
            this.labelX5.Text = "Select Database to Upgrade";
            // 
            // lvDatabases
            // 
            // 
            // 
            // 
            this.lvDatabases.Border.Class = "ListViewBorder";
            this.lvDatabases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dsn});
            this.lvDatabases.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lvDatabases.ForeColor = System.Drawing.Color.Sienna;
            this.lvDatabases.FullRowSelect = true;
            this.lvDatabases.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvDatabases.Location = new System.Drawing.Point(0, 35);
            this.lvDatabases.MultiSelect = false;
            this.lvDatabases.Name = "lvDatabases";
            this.lvDatabases.Size = new System.Drawing.Size(706, 239);
            this.lvDatabases.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDatabases.TabIndex = 0;
            this.lvDatabases.UseCompatibleStateImageBehavior = false;
            this.lvDatabases.View = System.Windows.Forms.View.Details;
            // 
            // dsn
            // 
            this.dsn.Width = 200;
            // 
            // wpCommentsMigration
            // 
            this.wpCommentsMigration.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpCommentsMigration.AntiAlias = false;
            this.wpCommentsMigration.BackColor = System.Drawing.Color.Transparent;
            this.wpCommentsMigration.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.wpCommentsMigration.Controls.Add(this.lblDatabase);
            this.wpCommentsMigration.Controls.Add(this.labelX4);
            this.wpCommentsMigration.Controls.Add(this.btnStart);
            this.wpCommentsMigration.Controls.Add(this.txtNotesCount);
            this.wpCommentsMigration.Controls.Add(this.txtCommentCount);
            this.wpCommentsMigration.Controls.Add(this.txtProcessingNow);
            this.wpCommentsMigration.Controls.Add(this.labelX3);
            this.wpCommentsMigration.Controls.Add(this.labelX2);
            this.wpCommentsMigration.Controls.Add(this.labelX1);
            this.wpCommentsMigration.Controls.Add(this.rtfStatus);
            this.wpCommentsMigration.Controls.Add(this.txtProgressStatusMessage);
            this.wpCommentsMigration.Controls.Add(this.prgBrNotesSave);
            this.wpCommentsMigration.Location = new System.Drawing.Point(7, 102);
            this.wpCommentsMigration.Name = "wpCommentsMigration";
            this.wpCommentsMigration.PageHeaderImage = ((System.Drawing.Image)(resources.GetObject("wpCommentsMigration.PageHeaderImage")));
            this.wpCommentsMigration.Size = new System.Drawing.Size(736, 342);
            this.wpCommentsMigration.TabIndex = 8;
            this.wpCommentsMigration.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.wizCommentsMigration_FinishButtonClick);
            // 
            // lblDatabase
            // 
            this.lblDatabase.BackColor = System.Drawing.SystemColors.Window;
            this.lblDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabase.Location = new System.Drawing.Point(95, 96);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(301, 23);
            this.lblDatabase.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.lblDatabase.TabIndex = 18;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.SystemColors.Window;
            this.labelX4.Location = new System.Drawing.Point(26, 96);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(63, 23);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.labelX4.TabIndex = 17;
            this.labelX4.Text = "Database:";
            // 
            // btnStart
            // 
            this.btnStart.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnStart.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStart.Location = new System.Drawing.Point(438, 9);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(84, 23);
            this.btnStart.TabIndex = 16;
            this.btnStart.Text = "&START";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtNotesCount
            // 
            // 
            // 
            // 
            this.txtNotesCount.Border.Class = "TextBoxBorder";
            this.txtNotesCount.Location = new System.Drawing.Point(213, 70);
            this.txtNotesCount.Name = "txtNotesCount";
            this.txtNotesCount.Size = new System.Drawing.Size(183, 20);
            this.txtNotesCount.TabIndex = 15;
            this.txtNotesCount.Text = "0";
            // 
            // txtCommentCount
            // 
            // 
            // 
            // 
            this.txtCommentCount.Border.Class = "TextBoxBorder";
            this.txtCommentCount.Location = new System.Drawing.Point(213, 41);
            this.txtCommentCount.Name = "txtCommentCount";
            this.txtCommentCount.Size = new System.Drawing.Size(183, 20);
            this.txtCommentCount.TabIndex = 14;
            this.txtCommentCount.Text = "0";
            // 
            // txtProcessingNow
            // 
            // 
            // 
            // 
            this.txtProcessingNow.Border.Class = "TextBoxBorder";
            this.txtProcessingNow.Location = new System.Drawing.Point(213, 10);
            this.txtProcessingNow.Name = "txtProcessingNow";
            this.txtProcessingNow.Size = new System.Drawing.Size(183, 20);
            this.txtProcessingNow.TabIndex = 13;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.SystemColors.Window;
            this.labelX3.Location = new System.Drawing.Point(24, 67);
            this.labelX3.Name = "labelX3";
            this.labelX3.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX3.Size = new System.Drawing.Size(180, 23);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.labelX3.TabIndex = 12;
            this.labelX3.Text = "No. of comments extracted so far";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.SystemColors.Window;
            this.labelX2.Location = new System.Drawing.Point(24, 38);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(180, 23);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.labelX2.TabIndex = 11;
            this.labelX2.Text = "No. of records loaded so far";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.SystemColors.Window;
            this.labelX1.Location = new System.Drawing.Point(24, 9);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(180, 23);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.labelX1.TabIndex = 10;
            this.labelX1.Text = "Processing Comments Attached To";
            // 
            // rtfStatus
            // 
            // 
            // 
            // 
            this.rtfStatus.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.rtfStatus.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.rtfStatus.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.rtfStatus.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.rtfStatus.Border.Class = "TextBoxBorder";
            this.rtfStatus.Location = new System.Drawing.Point(26, 151);
            this.rtfStatus.Multiline = true;
            this.rtfStatus.Name = "rtfStatus";
            this.rtfStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtfStatus.Size = new System.Drawing.Size(670, 125);
            this.rtfStatus.TabIndex = 9;
            this.rtfStatus.WatermarkEnabled = false;
            // 
            // txtProgressStatusMessage
            // 
            this.txtProgressStatusMessage.AutoSize = true;
            this.txtProgressStatusMessage.Location = new System.Drawing.Point(159, 167);
            this.txtProgressStatusMessage.Name = "txtProgressStatusMessage";
            this.txtProgressStatusMessage.Size = new System.Drawing.Size(0, 13);
            this.txtProgressStatusMessage.TabIndex = 8;
            // 
            // prgBrNotesSave
            // 
            this.prgBrNotesSave.Location = new System.Drawing.Point(26, 125);
            this.prgBrNotesSave.Name = "prgBrNotesSave";
            this.prgBrNotesSave.Size = new System.Drawing.Size(670, 20);
            this.prgBrNotesSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.prgBrNotesSave.TabIndex = 0;
            this.prgBrNotesSave.Text = "progressBarX1";
            // 
            // frmCommentsMigration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 505);
            this.Controls.Add(this.wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCommentsMigration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comments Data Migration";
            this.Load += new System.EventHandler(this.frmCommentsMigration_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCommentsMigration_FormClosing);
            this.wizard1.ResumeLayout(false);
            this.wpWelcome.ResumeLayout(false);
            this.wpDSNSelection.ResumeLayout(false);
            this.wpCommentsMigration.ResumeLayout(false);
            this.wpCommentsMigration.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Wizard wizard1;
        private DevComponents.DotNetBar.WizardPage wpWelcome;
        private DevComponents.DotNetBar.WizardPage wpCommentsMigration;
        private DevComponents.DotNetBar.WizardPage wpDSNSelection;
        private DevComponents.DotNetBar.Controls.ProgressBarX prgBrNotesSave;
        private System.Windows.Forms.Label txtProgressStatusMessage;
        private DevComponents.DotNetBar.Controls.TextBoxX rtfStatus;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.ColumnHeader dsn;
        public DevComponents.DotNetBar.Controls.ListViewEx lvDatabases;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProcessingNow;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCommentCount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNotesCount;
        private DevComponents.DotNetBar.ButtonX btnStart;
        private DevComponents.DotNetBar.LabelX lblDatabase;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;

    }
}