using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.Security; 
using Riskmaster.Db;
using Riskmaster.Common;
using System.Text;
using System.Text.RegularExpressions;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for frmRemoveDIVTag.
	/// </summary>
	public class frmRemoveDIVTag : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtClaimID;
		private System.Windows.Forms.Button btnRemoveDivTag;

		private string m_sConnectionString = string.Empty;
		private eDatabaseType m_DatabaseType;
		private System.Windows.Forms.Button btnBack;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmRemoveDIVTag()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtClaimID = new System.Windows.Forms.TextBox();
			this.btnRemoveDivTag = new System.Windows.Forms.Button();
			this.btnBack = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(152, 24);
			this.label1.TabIndex = 5;
			this.label1.Text = "Updated Note ID/Claim ID:";
			// 
			// txtClaimID
			// 
			this.txtClaimID.Location = new System.Drawing.Point(16, 96);
			this.txtClaimID.Multiline = true;
			this.txtClaimID.Name = "txtClaimID";
			this.txtClaimID.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtClaimID.Size = new System.Drawing.Size(336, 184);
			this.txtClaimID.TabIndex = 4;
			this.txtClaimID.Text = "";
			// 
			// btnRemoveDivTag
			// 
			this.btnRemoveDivTag.Location = new System.Drawing.Point(104, 8);
			this.btnRemoveDivTag.Name = "btnRemoveDivTag";
			this.btnRemoveDivTag.Size = new System.Drawing.Size(120, 32);
			this.btnRemoveDivTag.TabIndex = 3;
			this.btnRemoveDivTag.Text = "Remove Div Tag";
			this.btnRemoveDivTag.Click += new System.EventHandler(this.btnRemoveDivTag_Click);
			// 
			// btnBack
			// 
			this.btnBack.Location = new System.Drawing.Point(120, 288);
			this.btnBack.Name = "btnBack";
			this.btnBack.Size = new System.Drawing.Size(80, 32);
			this.btnBack.TabIndex = 6;
			this.btnBack.Text = "Back";
			this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
			// 
			// frmRemoveDIVTag
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(376, 334);
			this.Controls.Add(this.btnBack);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtClaimID);
			this.Controls.Add(this.btnRemoveDivTag);
			this.Name = "frmRemoveDIVTag";
			this.Text = "frmRemoveDIVTag";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnRemoveDivTag_Click(object sender, System.EventArgs e)
		{
			btnRemoveDivTag.Enabled = false;

			string sSearchedTag = "id=";
			string sSQL = "SELECT CL_PROG_NOTE_ID, CLAIM_ID, NOTE_MEMO " +
				"FROM CLAIM_PRG_NOTE WHERE ";

			switch( m_DatabaseType )
			{
				case eDatabaseType.DBMS_IS_SQLSRVR:
					sSQL += "CHARINDEX('" + sSearchedTag + "', NOTE_MEMO) > 0";
					break;
				case eDatabaseType.DBMS_IS_ORACLE:
					sSQL += "INSTR(NOTE_MEMO, '" + sSearchedTag + "' +) > 0";
					break;
				default:
					sSQL += "CHARINDEX('" + sSearchedTag + "', NOTE_MEMO) > 0";
					break;
			}

			int iNoteID = 0;
			int iClaimID = 0;
			string sNoteMemo = string.Empty;
			string sNewNoteMemo = string.Empty;
			DbReader oReader = null;
			try
			{
				oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				while(oReader.Read())
				{
					iNoteID = oReader.GetInt32("CL_PROG_NOTE_ID");
					iClaimID = oReader.GetInt32("CLAIM_ID");
					sNoteMemo = oReader.GetString("NOTE_MEMO");

					//Remove the special tags
					sNewNoteMemo = sNoteMemo;
					string sSearched = "id=\"NoteMemoDiv\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "div", sSearched);

					sSearched = "id=\"CreateNote\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "input", sSearched);
				
					sSearched = "id=\"advSearch\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "input", sSearched);

					sSearched = "id=\"EditNote\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "input", sSearched);

					sSearched = "id=\"PrintNotes\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "input", sSearched);

					sSearched = "id=\"CancelNotes\"";
					sNewNoteMemo = RemoveSpecialDivID(sNewNoteMemo, "input", sSearched);
				
					if( sNoteMemo != sNewNoteMemo )
					{
						UpdateNote( iNoteID, sNewNoteMemo);
						txtClaimID.Text += iNoteID.ToString() + ":" + iClaimID.ToString() + "  ";
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message);
			}
			finally
			{
				if( oReader != null )
					oReader.Close();
			}
			
			btnRemoveDivTag.Enabled = true;
		}

		private void UpdateNote(int iNoteId, string sNoteMemo)
		{
			DbWriter objDbWriter = null;
			objDbWriter = DbFactory.GetDbWriter(m_sConnectionString);
				
			objDbWriter.Tables.Add("CLAIM_PRG_NOTE");
				
			//objDbWriter.Fields.Add("CL_PROG_NOTE_ID",iNoteId);
			objDbWriter.Fields.Add("NOTE_MEMO",sNoteMemo);
			objDbWriter.Where.Add("CL_PROG_NOTE_ID = " + iNoteId.ToString());
			objDbWriter.Execute();
		}


		/// <summary>
		/// Remove <div id="NoteMemoDiv"></div> which comes from copy/paste of 
		/// another enhanced note.
		/// </summary>
		/// <param name="sInput">input string</param>
		/// <param name="sSearchedAttribute">the attribute name and value to be searched</param>
		/// <param name="sTag">Html tab name</param>
		/// <returns></returns>
		private string RemoveSpecialDivID(string sInput, string sTag, string sSearchedAttribute)
		{
			string sOutput = sInput;

			//Check if the searched string exists
			if( sInput.IndexOf( sSearchedAttribute ) < 0 )
				return sOutput;

			ArrayList oBeginTagList = new ArrayList();

			//Create regular expression for begin/end tag
			string sEndTag = "</" + sTag + ">";
			int iEngTagLength = sEndTag.Length;
			Regex rTagBeginEnd = new Regex("<" + sTag +  "[^<]*/>");
			Regex rTagBegin = new Regex("<" + sTag +  "[^<]*>");
			Regex rTagEnd = new Regex(sEndTag, RegexOptions.RightToLeft);
			
			try
			{
				//Try to remove the whole tag first, such as <div id='a'/>
				MatchCollection mcBeginEnd = rTagBeginEnd.Matches(sInput);
				for(int iBeginEnd=0; iBeginEnd<mcBeginEnd.Count; iBeginEnd++)
				{
					Match mBeginEnd = mcBeginEnd[iBeginEnd];
					string sBeginEnd = mBeginEnd.ToString();
					if( sBeginEnd.IndexOf(sSearchedAttribute) >= 0)
					{
						sOutput = sOutput.Replace(sBeginEnd, "");
					}
				}

				//Search for begin tag and end tag
				MatchCollection mcBegin = rTagBegin.Matches(sOutput);
				MatchCollection mcEnd = rTagEnd.Matches(sOutput);
				if( mcBegin.Count == mcEnd.Count)
				{
					for(int iBegin=0; iBegin< mcBegin.Count; iBegin++)
					{
						Match mBegin = mcBegin[iBegin];
						string sBegin = mBegin.ToString();

						//If the begin tag contains searched string
						if( sBegin.IndexOf(sSearchedAttribute) >= 0)
						{
							oBeginTagList.Add( sBegin );

							//Remove the end tag
							Match mEnd = mcEnd[iBegin];
							if( mEnd.Groups.Count > 0)
							{
								Group gEnd = mEnd.Groups[0];
								CaptureCollection cc = gEnd.Captures;
								if( cc.Count > 0) 
								{
									Capture c = cc[0];
									int iStartIndex = c.Index;
									sOutput = sOutput.Remove(iStartIndex, iEngTagLength);
								}
							}
						}
					}

					//Remove the begin tag
					for(int i=0; i<oBeginTagList.Count; i++)
					{
						string sTagString = oBeginTagList[i].ToString();
						sOutput = sOutput.Replace(sTagString, "");
					}
				}

				return sOutput;
			}
			catch(Exception p_objException)
			{
				MessageBox.Show(p_objException.Message + " NoetMemo: " + sInput);
			}
			finally
			{
				oBeginTagList = null;
			}

			return sOutput;
		}

		private void btnBack_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		public string ConnectionString
		{
			set{ m_sConnectionString = value; }
		}

		public eDatabaseType DatabaseType
		{
			set{ m_DatabaseType = value; }
		}
	}
}
