using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using Riskmaster.Security;  
using Riskmaster.Common;
using Riskmaster.Db;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	public class frmLogin : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtUserName;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cboDSN;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnExit;
		private System.ComponentModel.Container components = null;
        
		private string m_sDSN = string.Empty;
		private string m_sUserID = string.Empty;
		private string m_sPassword = string.Empty;
		private string m_ConnectionString = string.Empty;
		
		private bool m_bLogin = false;
		private eDatabaseType m_DatabaseType = 0;

		public frmLogin()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboDSN = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(112, 12);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(160, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(112, 40);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(160, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(24, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // cboDSN
            // 
            this.cboDSN.Location = new System.Drawing.Point(112, 71);
            this.cboDSN.Name = "cboDSN";
            this.cboDSN.Size = new System.Drawing.Size(160, 21);
            this.cboDSN.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Database:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(74, 107);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(65, 25);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(145, 107);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(65, 25);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnExit;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(304, 150);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboDSN);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(312, 184);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLogin";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void frmLogin_Load(object sender, System.EventArgs e)
		{
            Login objLogin = new Login();
            string[] arrDb = objLogin.GetDatabases();
			cboDSN.DataSource = arrDb;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				m_sDSN = cboDSN.Text;
				
				//Check for entered values
				m_sUserID = txtUserName.Text.Trim();
				m_sPassword = txtPassword.Text.Trim();
				if( m_sUserID.Length == 0 )
				{
					MessageBox.Show("Please enter a valid user name.");
					txtUserName.Focus();
					return;
				}
				if( m_sPassword.Length == 0 )
				{
					MessageBox.Show("Please enter a valid password.");
					txtPassword.Focus();
					return;
				}

                Login m_objLogin = new Login(m_sDSN);
                UserLogin m_objUser = new UserLogin();
                m_bLogin = m_objLogin.AuthenticateUser(m_sDSN, m_sUserID, m_sPassword, out m_objUser);
				
				m_ConnectionString = m_objUser.objRiskmasterDatabase.ConnectionString;
				m_DatabaseType = m_objUser.objRiskmasterDatabase.DbType;

                int claimId = 0;
                Form main = new frmMain(m_ConnectionString, claimId);
                this.Visible = false;
                main.ShowDialog();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		public string UserID
		{
			get{ return m_sUserID; }
		}

		public string Password
		{
			get{ return m_sPassword; }
		}

		public string DSN
		{
			get{ return m_sDSN; }
		}

		public bool LoginSuccess
		{
			get{ return m_bLogin; }
		}

		public string ConnectionString
		{
			get{ return m_ConnectionString; }
		}

		public eDatabaseType DatabaseType
		{
			get{ return m_DatabaseType; }
		}
	}
}
