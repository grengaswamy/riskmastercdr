using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using System.Threading;
using Riskmaster.Db;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for frmRemoveBadAscii.
	/// 1. Search database to find all records which contains bad
	///    ASCII characters whose value between 0 - 31 (except 9,10,13)
	/// 2. Update the database by removing them
	/// </summary>
	public class frmScanBadAscii : System.Windows.Forms.Form
	{
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProgress;
        private System.Windows.Forms.Label lblMessage;
        private System.ComponentModel.Container components = null;
        private System.Threading.Thread m_ScanThread = null;

        private bool m_Started = false;
        private int m_iClaimId = 0;
		private string m_sConnectionString = string.Empty;
        private const string m_StartScanCaption = "Click to Continue";
        private const string m_PauseScanCaption = "Click to Pause";
		private eDatabaseType m_DatabaseType;
		private BadAsciiCleaner m_AsciiCleaner = null;
        private ProgressBar progressBar1;  //pmittal5 10/14/08

		public frmScanBadAscii()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnBack = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(169, 402);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(88, 32);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(120, 16);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(192, 32);
            this.btnScan.TabIndex = 4;
            this.btnScan.Text = "Start Scan Bad Ascii";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Scan Progress:";
            // 
            // txtProgress
            // 
            this.txtProgress.Location = new System.Drawing.Point(8, 88);
            this.txtProgress.Multiline = true;
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtProgress.Size = new System.Drawing.Size(464, 272);
            this.txtProgress.TabIndex = 6;
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(8, 376);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(464, 16);
            this.lblMessage.TabIndex = 7;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.LightGreen;
            this.progressBar1.Location = new System.Drawing.Point(88, 63);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(380, 13);
            this.progressBar1.Step = 4;
            this.progressBar1.TabIndex = 8;
            // 
            // frmScanBadAscii
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(480, 446);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtProgress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnBack);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(488, 480);
            this.MinimumSize = new System.Drawing.Size(488, 480);
            this.Name = "frmScanBadAscii";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scan Bad Ascii";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnBack_Click(object sender, System.EventArgs e)
		{
			this.Hide();

			//In case the thread is not done, abort it
			if( m_ScanThread != null )
			{
				if( m_ScanThread.IsAlive )
					m_ScanThread.Abort();
			}
		}

		private void btnScan_Click(object sender, System.EventArgs e)
		{
			//Do some initializtion if it's first time
			if( !m_Started )
			{
				string sPath = Application.ExecutablePath;
				int iLength = sPath.LastIndexOf("\\");
				sPath = sPath.Substring(0, iLength + 1);
				//m_AsciiCleaner = new BadAsciiCleaner( m_sConnectionString, m_DatabaseType, sPath );
                m_AsciiCleaner = new BadAsciiCleaner(m_sConnectionString, m_DatabaseType, sPath, m_iClaimId);
				btnScan.Text = m_PauseScanCaption;
			}
			else
			{
				string sCaption = btnScan.Text;
				if( sCaption == m_StartScanCaption )
				{
					m_AsciiCleaner.PauseScan = false;
					btnScan.Text = m_PauseScanCaption;
					lblMessage.Text = "Click button to pause the scan.";
				}
				else
				{
					btnScan.Text = m_StartScanCaption;
					m_AsciiCleaner.PauseScan = true;
				}
				return;
			}

			m_AsciiCleaner.showProgress += new BadAsciiCleaner.progressDelegate( displayProgress );
			m_AsciiCleaner.showMessage += new BadAsciiCleaner.messageDelegate( displayMessage );
			m_AsciiCleaner.showComplete += new BadAsciiCleaner.progressDelegate( scanComplete );

            m_ScanThread = new Thread(new ThreadStart(m_AsciiCleaner.scanBadAscii));
            m_ScanThread.Start();
			m_Started = true;
			return;		
		}

		private void displayProgress(string sMessage)
		{
			//txtProgress.Text += sMessage;
            //pmittal5 10/10/08 - Invoke "Form Thread" to access the Controls on Form
            this.Invoke(new BadAsciiCleaner.progressDelegate(displayProgressInvoked), sMessage);
            //txtProgress.Focus();  
            //txtProgress.AppendText(sMessage);
            //txtProgress.ScrollToCaret();
            //this.Refresh();
            //End - pmittal
		}

        //Above commented code copied - by pmittal5 10/10/08
        private void displayProgressInvoked(string p_sMessage)
        {
            txtProgress.Focus();
            txtProgress.AppendText(p_sMessage);
            txtProgress.ScrollToCaret();
            if (progressBar1.Value >= progressBar1.Maximum)
                progressBar1.Value = 0;
            progressBar1.PerformStep();
            this.Refresh();
        }//End - pmittal5

		private void displayMessage(string sMessage, bool bError)
		{
            //pmittal5 10/10/08 - Invoke "Form Thread" to access the Controls on Form
            this.Invoke(new BadAsciiCleaner.messageDelegate(displayMessageInvoked), sMessage, bError);
            //if (bError)
            //{
            //    lblMessage.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lblMessage.ForeColor = Color.Green;
            //}
			//lblMessage.Text = sMessage;
            //End - pmittal5
		}

        //Above commented code copied - by pmittal5 10/10/08
        private void displayMessageInvoked(string p_sMessage, bool p_bError)
        {
            if (p_bError)
            {
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                lblMessage.ForeColor = Color.Green;
            }
            lblMessage.Text = p_sMessage;
        }//End - pmittal5

		private void scanComplete(string sMessage)
		{
			//btnScan.Hide();
            this.Invoke(new BadAsciiCleaner.progressDelegate(scanCompleteInvoked), sMessage); //pmittal5
		}//pmittal5

        private void scanCompleteInvoked(string p_sMessage)
        {
            while (progressBar1.Value != progressBar1.Maximum)
                progressBar1.PerformStep();
        }//End - pmittal5

		public string ConnectionString
		{
			set
			{ 
				m_sConnectionString = value;
			} 
		}

		public eDatabaseType DatabaseType
		{
			set{ m_DatabaseType = value; }
		}//pmittal5

        public int ClaimId
        {
            get { return m_iClaimId; }
            set { m_iClaimId = value; }
        }//End - pmittal5
	}
}
