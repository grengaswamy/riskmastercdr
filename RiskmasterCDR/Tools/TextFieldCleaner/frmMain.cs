using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
        //private static string m_sDSN = string.Empty;
        //private string m_sUserID = string.Empty;
        //private string m_sPassword = string.Empty;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.Button btnScan;
		private System.Windows.Forms.Button btnRemoveAscii;
		private string m_sConnectionString = string.Empty;
        private eDatabaseType m_DatabaseType = eDatabaseType.DBMS_IS_SQLSRVR;
        private Button btnUpdateData;

        private int m_iClaimId = 0; //pmittal5 10/14/08
         
		public frmMain()
		{
			InitializeComponent();
		}

        public frmMain(string p_sConnString, int p_claimid)
        {
            InitializeComponent();
            m_sConnectionString = p_sConnString;
            m_iClaimId = p_claimid;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnBack = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.btnRemoveAscii = new System.Windows.Forms.Button();
            this.btnUpdateData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(143, 129);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(65, 25);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "E&xit";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(82, 23);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(200, 32);
            this.btnScan.TabIndex = 6;
            this.btnScan.Text = "&Scan Bad Ascii";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // btnRemoveAscii
            // 
            this.btnRemoveAscii.Location = new System.Drawing.Point(82, 73);
            this.btnRemoveAscii.Name = "btnRemoveAscii";
            this.btnRemoveAscii.Size = new System.Drawing.Size(200, 32);
            this.btnRemoveAscii.TabIndex = 5;
            this.btnRemoveAscii.Text = "&Remove Bad Ascii";
            this.btnRemoveAscii.Click += new System.EventHandler(this.btnRemoveAscii_Click);
            // 
            // btnUpdateData
            // 
            this.btnUpdateData.Location = new System.Drawing.Point(269, 145);
            this.btnUpdateData.Name = "btnUpdateData";
            this.btnUpdateData.Size = new System.Drawing.Size(94, 25);
            this.btnUpdateData.TabIndex = 7;
            this.btnUpdateData.Text = "&Insert Bad Data";
            this.btnUpdateData.Visible = false;
            this.btnUpdateData.Click += new System.EventHandler(this.btnUpdateData_Click);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(375, 182);
            this.Controls.Add(this.btnUpdateData);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnRemoveAscii);
            this.Controls.Add(this.btnBack);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(383, 216);
            this.MinimumSize = new System.Drawing.Size(383, 216);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.ResumeLayout(false);

		}
		#endregion

		private void btnRemoveDivTag_Click(object sender, System.EventArgs e)
		{
			frmRemoveDIVTag oDIVTag = new frmRemoveDIVTag();
			oDIVTag.ConnectionString = m_sConnectionString;
			oDIVTag.ShowDialog();
			oDIVTag = null;
		}

		private void btnBack_Click(object sender, System.EventArgs e)
        {
            //this.Hide();
            this.Close();
            Application.Exit();
		}
        
        //private void btnRemoveAscii_Click(object sender, System.EventArgs e)
        //{
        //    this.Hide();
        //    frmScanBadAscii oAsciiForm = new frmScanBadAscii();
        //    oAsciiForm.ConnectionString = m_sConnectionString;
        //    oAsciiForm.DatabaseType = m_DatabaseType;
        //    oAsciiForm.ShowDialog();
        //    oAsciiForm = null;
        //    this.Show();
        //}

		private void btnScan_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmScanBadAscii oAsciiForm = new frmScanBadAscii();
			oAsciiForm.ConnectionString = m_sConnectionString;
			oAsciiForm.DatabaseType = m_DatabaseType;
            oAsciiForm.ClaimId = m_iClaimId;  //pmittal5
			oAsciiForm.ShowDialog();
			oAsciiForm = null;
			this.Show();		
		}

        private void btnRemoveAscii_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmRemoveBadAscii oAsciiForm = new frmRemoveBadAscii();
			oAsciiForm.ConnectionString = m_sConnectionString;
            oAsciiForm.ClaimId = m_iClaimId;  //pmittal5
			oAsciiForm.ShowDialog();
			oAsciiForm = null;
			this.Show();
		}

        private void btnUpdateData_Click(object sender, EventArgs e)
        {
            BadAsciiCleaner obj = new BadAsciiCleaner(m_sConnectionString);
            int iRowsUpdated = obj.writeBadTestData();

            MessageBox.Show(String.Format("{0} row(s) were updated successfully...", iRowsUpdated), "Updated Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
	}
}
