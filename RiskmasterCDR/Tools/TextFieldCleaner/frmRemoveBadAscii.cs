using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using Riskmaster.Db;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for frmRemoveBadAscii.
	/// </summary>
	public class frmRemoveBadAscii : System.Windows.Forms.Form
	{
		private string m_sConnectionString = string.Empty;
		private eDatabaseType m_DatabaseType;
		private BadAsciiCleaner m_AsciiCleaner = null;
		private System.Threading.Thread m_ScanThread = null;

		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.TextBox txtProgress;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.Button btnRemove;
        private int m_iClaimId = 0;
        private ProgressBar progressBar1;  //pmittal5 10/14/08
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmRemoveBadAscii()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(56, 371);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(464, 16);
            this.lblMessage.TabIndex = 12;
            // 
            // txtProgress
            // 
            this.txtProgress.Location = new System.Drawing.Point(56, 83);
            this.txtProgress.Multiline = true;
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtProgress.Size = new System.Drawing.Size(464, 272);
            this.txtProgress.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(56, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "Progress:";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(168, 11);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(192, 32);
            this.btnRemove.TabIndex = 9;
            this.btnRemove.Text = "Remove Bad Ascii";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(216, 403);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(88, 32);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.LightGreen;
            this.progressBar1.Location = new System.Drawing.Point(115, 64);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(395, 13);
            this.progressBar1.Step = 5;
            this.progressBar1.TabIndex = 13;
            // 
            // frmRemoveBadAscii
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(568, 446);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtProgress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnBack);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(576, 480);
            this.MinimumSize = new System.Drawing.Size(576, 480);
            this.Name = "frmRemoveBadAscii";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remove Bad Ascii";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnRemove_Click(object sender, System.EventArgs e)
		{
			string sPath = Application.ExecutablePath;
			int iLength = sPath.LastIndexOf("\\");
			sPath = sPath.Substring(0, iLength + 1);
			//m_AsciiCleaner = new BadAsciiCleaner( m_sConnectionString, m_DatabaseType, sPath );
            m_AsciiCleaner = new BadAsciiCleaner(m_sConnectionString, m_DatabaseType, sPath, m_iClaimId);

			m_AsciiCleaner.showProgress += new BadAsciiCleaner.progressDelegate( displayProgress );
			m_AsciiCleaner.showMessage += new BadAsciiCleaner.messageDelegate( displayMessage );
            m_AsciiCleaner.showComplete += new BadAsciiCleaner.progressDelegate(scanComplete);  //pmittal5

			m_ScanThread = new Thread(new ThreadStart(m_AsciiCleaner.RemoveBadAscii));
			m_ScanThread.Start();

			btnRemove.Hide();
			return;
		}

		private void displayProgress(string sMessage)
		{
			//txtProgress.Text += sMessage;
            //pmittal5 10/10/08 - Invoke "Form Thread" to access the Controls on Form
            this.Invoke(new BadAsciiCleaner.progressDelegate(displayProgressInvoked), sMessage);
            //txtProgress.Focus();
            //txtProgress.AppendText(sMessage);
            //txtProgress.ScrollToCaret();
            //this.Refresh();
		}

        //Above commented code copied - by pmittal5 
        private void displayProgressInvoked(string p_sMessage)
        {
            txtProgress.Focus();
            txtProgress.AppendText(p_sMessage);
            txtProgress.ScrollToCaret();
            if (progressBar1.Value >= progressBar1.Maximum)
                progressBar1.Value = 0;
            progressBar1.PerformStep();
            this.Refresh();
        }//End - pmittal5

		private void displayMessage(string sMessage, bool bError)
		{
            //pmittal5 10/10/08 - Invoke "Form Thread" to access the Controls on Form
            this.Invoke(new BadAsciiCleaner.messageDelegate(displayMessageInvoked), sMessage, bError);
            //if( bError )
            //{
            //    lblMessage.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lblMessage.ForeColor = Color.Green;
            //}
            //lblMessage.Text = sMessage;
		}

        //Above commented code copied - by pmittal5 
        private void displayMessageInvoked(string p_sMessage, bool p_bError)
        {
            if (p_bError)
            {
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                lblMessage.ForeColor = Color.Green;
            }
            lblMessage.Text = p_sMessage;
        }//End - pmittal5

        private void scanComplete(string sMessage)
        {
            //btnScan.Hide();
            this.Invoke(new BadAsciiCleaner.progressDelegate(scanCompleteInvoked), sMessage); //pmittal5
        }
        //pmittal5
        private void scanCompleteInvoked(string p_sMessage)
        {
            while (progressBar1.Value != progressBar1.Maximum)
                progressBar1.PerformStep();
        }//End - pmittal5

		private void btnBack_Click(object sender, System.EventArgs e)
		{
			this.Hide();

			//In case the thread is not done, abort it
			if( m_ScanThread != null )
			{
				if( m_ScanThread.IsAlive )
					m_ScanThread.Abort();
			}
		}

		public string ConnectionString
		{
			set
			{ 
				m_sConnectionString = value;
			} 
		}

		public eDatabaseType DatabaseType
		{
			set{ m_DatabaseType = value; }
		}

        //pmittal5
        public int ClaimId
        {
            get { return m_iClaimId; }
            set { m_iClaimId = value; }
        }//End - pmittal5
	}
}
