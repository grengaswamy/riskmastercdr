[ASSIGN %%1=ADD_JURISDICTION(AZ,Arizona,0,0,0,0,0,0)]
[FORGIVE] UPDATE WCP_FORMS SET FORM_NAME = 'ICA 04-0103' WHERE (FORM_NAME = '103' OR FORM_NAME = 'ICA 103')

;vgupta20 1/21/2010 MITS 19049
[ASSIGN %%1=ADD_STATE_FIELD(WC_AZ_SUPP,Gr Wages Of Employee Since Hire (Employment<12m),GROSS_WAGE_FROM_HIRE,2,20,0,0,0,0,,)]

;akaur9 02/09/2011 MITS# 23794
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'AZ')]
[IF %%1<>0]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Notice Of Supportive Medical Maintenance Benefits,ICA 04-0103,az_103.pdf,0,,-1,243)]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Notice Of Claim Status,ICA 04-0104,az_104.pdf,0,,-1,243)]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Recommended Average Monthly Wage Calculation Of Carrier,ICA 04-0108-75,az_108.pdf,0,,-1,243)]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Notice Of Suspension Of Benefits,ICA 04-0105,az_105_19960601.pdf,0,,-1,243)]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Notice Of Permanent Disability Or Death Benefits,ICA 04-0106,az_106_1994.pdf,0,,-1,243)]
[ASSIGN %%2=ADD_WCP_FORM(%%1,-1,Notice Of Permanent Disability And Request For Determination Of Benefits,ICA 04-0107,az_107_1993.pdf,0,,-1,243)]
[ENDIF]


