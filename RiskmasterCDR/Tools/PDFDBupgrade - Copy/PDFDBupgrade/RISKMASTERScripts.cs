﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using Riskmaster.Security.Encryption;
using System.Security.Cryptography;

namespace RMXdBUpgradeWizard
{
    class RISKMASTERScripts
    {
        /// <summary>
        /// creates a script that gets the DSNID from the security database for a given DSN name
        /// </summary>
        /// <param name="strDSNID">DSN name</param>
        /// <returns>SQL script</returns>
        public static string GetSelectedDSNID(string strDSNID)
        {
            return String.Format("SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN='{0}'", strDSNID);
        }

        /// <summary>
        /// creates a script that gets all DSN info from security database for a specific DSNID
        /// </summary>
        /// <param name="intDSNID">DSNID</param>
        /// <returns>SQL script</returns>
        public static string GetSelectedDSNInfo(int intDSNID)
        {
            return String.Format("SELECT * FROM DATA_SOURCE_TABLE WHERE DSNID={0}", intDSNID);
        }

        /// <summary>
        /// creates a script that gets a list of all DSNs from security database
        /// </summary>
        /// <returns>SQL script</returns>
        public static string GetDSNs()
        {
            return "SELECT * FROM DATA_SOURCE_TABLE ORDER BY DSN";
        }

        /// <summary>
        /// creates a script that gets a list of all DSNs from security database
        /// </summary>
        /// <returns>DSN and DSNID</returns>
        public static string GetOnlyDSNs()
        {
            return "SELECT DSN,DSNID FROM DATA_SOURCE_TABLE ORDER BY DSN";
        }

        /// <summary>
        /// creates a script that gets BES tables from database
        /// </summary>
        /// <returns>SQL script</returns>
        public static string GetBESTables()
        {
            return "SELECT TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES ORDER BY TABLE_SCHEMA";
        }

        /// <summary>
        /// creates SQL script that grants permission to specified view
        /// </summary>
        /// <param name="strTableSchema">schema name ex. dbo</param>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string GrantPermsToBESView(string strTableSchema, string strTableName)
        {
            return String.Format("GRANT ALL ON {0}.{1} TO PUBLIC", strTableSchema, strTableName);
        }

        /// <summary>
        /// create SQL script that retrieves the net unique id from glossary for specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string GetNextUID(string strTableName)
        {
            return String.Format("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='{0}'", strTableName);
        }

        /// <summary>
        /// create SQL script that deletes a specified power view
        /// </summary>
        /// <param name="iViewID">power view ID</param>
        /// <param name="strFormName">form name</param>
        /// <param name="iDSNId">Data Source Id for which the DBUpgrade is running</param>
        /// <returns>SQL script</returns>
        public static string DeletePowerViews(int iViewID, string strFormName, int iDSNId)
        {
            return String.Format("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='{1}' AND DATA_SOURCE_ID={2}", iViewID, strFormName, iDSNId);
        }

        /// <summary>
        /// create SQL script that updates the next unique id field for a specified table
        /// </summary>
        /// <param name="intNextUID">new UID</param>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string UpdateGlossary(int intNextUID, string strTableName)
        {
            return String.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID={0} WHERE SYSTEM_TABLE_NAME='{1}'", intNextUID, strTableName);
        }

        /// <summary>
        /// create SQL to clean up diaries where table like CLAIM+
        /// </summary>
        /// <returns>SQL script</returns>
        public static string UpdateDiary()
        {
            var objSQL = new StringBuilder();

            objSQL.Append("UPDATE WPA_DIARY_ENTRY ");
            objSQL.Append("   SET ATTACH_TABLE = 'CLAIM' ");
            objSQL.Append(" WHERE ATTACH_TABLE LIKE 'CLAIM%' ");
            objSQL.Append("   AND ATTACH_TABLE <> 'CLAIM'");

            return objSQL.ToString();
        }

        /// <summary>
        /// create SQL that returns all FDM views
        /// </summary>
        /// <returns>SQL script</returns>
        public static string GetFDMViews(int iDSNId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("SELECT VIEW_ID FROM NET_VIEW_FORMS ");
            objSQL.Append("WHERE FORM_NAME IN ('claimlist.xml','personinvolvedlist.xml') ");
            objSQL.AppendFormat("AND VIEW_ID <> 0 AND DATA_SOURCE_ID = {0}",iDSNId);

            return objSQL.ToString();
        }

        /// <summary>
        /// create SQL that returns a power view for a specified PV name
        /// </summary>
        /// <param name="intViewID">power view id</param>
        /// <param name="strFormName">form name</param>
        /// <returns>SQL script</returns>
        public static string GetPowerViewsByName(int intViewID, string strFormName, int iDSNId)
        {
            return String.Format("SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='{1}' AND DATA_SOURCE_ID = {2}", intViewID, strFormName, iDSNId);
        }

        /// <summary>
        /// create SQL to retrieve all power views
        /// </summary>
        /// <returns>SQL script</returns>
        public static string GetPowerViews(int iDSNId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("SELECT  VIEW_ID,FORM_NAME FROM NET_VIEW_FORMS ");
            objSQL.Append("WHERE FORM_NAME IN ('eventdatedtext.xml','entitymaint.xml','people.xml','event.xml') ");
            objSQL.AppendFormat("AND VIEW_ID <> 0 AND DATA_SOURCE_ID = {0}", iDSNId);

            return objSQL.ToString();
        }

        // akaushik5 Added for MITS 30290 Starts
        /// <summary>
        /// Gets the dynamic power views.
        /// </summary>
        /// <param name="iDSNId">The i DSN id.</param>
        /// <param name="viewId">The view id.</param>
        /// <param name="views">The views.</param>
        /// <returns>
        /// SQL Script.
        /// </returns>
        public static string GetDynamicPowerViews(int iDSNId, int viewId, string views)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("SELECT * FROM NET_VIEW_FORMS ");
            objSQL.AppendFormat("WHERE FORM_NAME = '{0}' ", views);
            objSQL.AppendFormat("AND VIEW_ID = {0} AND DATA_SOURCE_ID = {1}",viewId, iDSNId);

            return objSQL.ToString();
        }
        // akaushik5 Added for MITS 30290 Ends

        /// <summary>
        /// create SQL to return MAXID from specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string GetMaxID(string strTableName)
        {
            return String.Format("SELECT MAX(FIELD_ID)+1 FROM {0}", strTableName);
        }

        public static string GetMaxRecordID(string strTableName, string sFieldName)
        {
            return String.Format("SELECT CASE WHEN MAX({0}) IS NULL THEN 1 ELSE MAX({0})+1 END FROM {1}", sFieldName, strTableName);
        }
        /// <summary>
        /// create SQL to return datatype and length from specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <param name="strColumnName">column name</param>
        /// <param name="strDbConn">db connection</param>
        /// <param name="strUID">user id</param>
        /// <returns>SQL script</returns>
        public static string GetDataTypeAndLength(string strTableName, string strColumnName, string strDbConn, string strUID)
        {
            int iDBMake = ADONetDbAccess.DbType(strDbConn); // same value as DisplayDBUpgrade.g_dbMake?
            int testDU = DisplayDBUpgrade.g_dbMake;         // same as UpdateFunctions.iDBMake?
            int testUF = UpdateFunctions.iDBMake;

            var objSQL = new StringBuilder();

            switch (iDBMake)
            {
                case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                {
                    objSQL.Append("       SELECT ss.name AS c_datatype, sc.max_length AS c_length ");
                    objSQL.Append("         FROM sys.columns AS sc ");
                    objSQL.Append("         JOIN sys.tables AS st ON st.object_id = sc.object_id ");
                    objSQL.Append("         JOIN sys.systypes AS ss ON ss.xtype = sc.system_type_id ");
                    //objSQL.Append("     --JOIN sys.schemas AS sch ON st.schema_id = sch.schema_id ");
                    objSQL.AppendFormat("  WHERE sc.name = '{0}' ", strColumnName);
                    objSQL.AppendFormat("    AND st.name = '{0}' ", strTableName);
                    //objSQL.AppendFormat("--AND sch.name = '{0}'", strSchemaName); // ie: 'dbo'
                    break;
                }
                case RiskmasterDBTypes.DBMS_IS_ORACLE:
                {
                    objSQL.Append("      SELECT DATA_TYPE AS c_datatype, DATA_LENGTH AS c_length ");
                    objSQL.Append("        FROM all_tab_columns ");
                    objSQL.AppendFormat(" WHERE owner = '{0}' ", strUID.ToUpper());
                    objSQL.AppendFormat("   AND TABLE_NAME = '{0}'", strTableName);
                    objSQL.AppendFormat("   AND COLUMN_NAME = '{0}'", strColumnName);
                    break;
                }
                default:
                    break;
            }

            return objSQL.ToString();
        }

        /// <summary>
        /// create SQL to get table ID from glossary for specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string GetTableID(string strTableName)
        {
            return String.Format("SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='{0}'", strTableName);
        }

        /// <summary>
        /// create SQL to insert a row of data into specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <param name="dictFields">field / value pair</param>
        /// <returns>SQL script</returns>
        public static string InsertNewRow(string strTableName, Dictionary<string, object> dictFields, ref string strErrorDesc,string sConnectionString)
        {
            bool bError = false;
            int iMaxLen = 0;
            const string SQL_DELIMITER = ",";
            string strQuery = String.Empty;
            string strDataType = String.Empty;
            string strMaxLength = String.Empty;
            StringBuilder objSQLFields = new StringBuilder();
            StringBuilder objSQLValues = new StringBuilder();
            StringBuilder objValidSQL = new StringBuilder();

            objValidSQL.AppendFormat("INSERT INTO {0} (", strTableName);

            foreach (KeyValuePair<string, object> kvp in dictFields)
            {
                objSQLFields.AppendFormat("{0}{1}", kvp.Key, SQL_DELIMITER);

                strQuery = GetDataTypeAndLength(strTableName, kvp.Key, sConnectionString, DisplayDBUpgrade.g_UserId);
                //DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connectionstring: " + sConnectionString + " Query: " + strQuery);
                DataSet dsObject = ADONetDbAccess.GetDataSet(sConnectionString, strQuery);
                //DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Query next step: ");

                foreach (DataRow drObject in dsObject.Tables[0].Rows)
                {
                    strDataType = drObject["c_datatype"].ToString();
                    strMaxLength = drObject["c_length"].ToString();
                    int.TryParse(strMaxLength, out iMaxLen);
                }
                dsObject.Dispose();

                bError = false;
                strErrorDesc = String.Empty;
                string sDataType = kvp.Value.GetType().ToString();

                switch (sDataType)
                {
                    case "System.String":
                        string sValueField = kvp.Value.ToString().Replace("'", "''");
                        int iLenValueField = sValueField.Length;
                        //rsushilaggar MITS 25089 Date 07/11/2011
                        if (iLenValueField > iMaxLen && iMaxLen > 0)
                        {
                            bError = true;
                            strErrorDesc = String.Format("[Field: {0}] String to be inserted (length: {1}) is larger than the datatype max (length: {2}).", kvp.Key, iLenValueField, iMaxLen);
                        }
                        else
                        {
                            objSQLValues.AppendFormat("'{0}'", sValueField);
                        }

                        break;
                    case "System.Boolean":
                        //if (kvp.Value.ToString().IndexOf("False") > 0)
                        if(String.Compare(kvp.Value.ToString(),"false",true) == 0)
                        {
                            objSQLValues.Append(0);
                        }
                        else
                        {
                            objSQLValues.Append(1);
                        }
                        break;
                    default:
                        objSQLValues.Append(kvp.Value.ToString());
                        break;
                }

                if (bError)
                {
                    break;
                }

                objSQLValues.Append(SQL_DELIMITER);
            }

            string sTmp = objSQLFields.ToString();
            sTmp = sTmp.Substring(0, sTmp.Length - 1);
            objValidSQL.AppendFormat("{0}) VALUES(", sTmp);

            if (bError)
            {
                return String.Empty;
            }
            else
            {
                sTmp = objSQLValues.ToString();
                sTmp = sTmp.Substring(0, sTmp.Length - 1);
                sTmp = sTmp + ")";
                objValidSQL.Append(sTmp);

                return objValidSQL.ToString();
            }
        }

        /// <summary>
        /// creates a connection string
        /// </summary>
        /// <param name="strDbConnStr">connection</param>
        /// <param name="strUID">user id</param>
        /// <param name="strPW">password</param>
        /// <returns>connection string</returns>
        public static string BuildConnectionString(string strDbConnStr, string strUID, string strPW)
        {
            const string DELIMITER = ";";

            var objConnStr = new StringBuilder();

            if (string.IsNullOrEmpty(strDbConnStr))
            {
                objConnStr.AppendFormat("DSN={0}{1}{0}{2}{0}", DELIMITER, strUID, strPW);
            }
            else
            {
                objConnStr.Append(strDbConnStr);
                
                if (strDbConnStr.LastIndexOf(";") != strDbConnStr.Length - 1)
                {
                    objConnStr.Append(DELIMITER);    
                }

                objConnStr.AppendFormat("UID={0}{1}PWD={2}{1}", strUID, DELIMITER, strPW);
            }

            return objConnStr.ToString();
        }

        /// <summary>
        /// decrypts the Document connection string
        /// </summary>
        /// <param name="strDbConnStr">connection</param>
        /// <returns>connection string</returns>
        public static string DecryptDocumentConnectionString(string strDbConnStr, ref string sDocUserId, ref string sDocDB)
        {
            const string DELIMITER = ";";
            
            string strDriver = String.Empty;
            string strServer = String.Empty;
            string strDatabase = String.Empty;
            string strUID = String.Empty;
            string strPW = String.Empty;

            var objConnStr = new StringBuilder();

            if ((!String.IsNullOrEmpty(strDbConnStr)) && (strDbConnStr.Length > 0))
            {
                string[] arrString = strDbConnStr.Split(DELIMITER.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                strDriver = arrString[0];
                strServer = arrString[1];
                strDatabase = arrString[2];
                string[] arrDB = arrString[2].Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                sDocDB = arrDB[1];
                string[] arrUID = arrString[3].Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                strUID = RMCryptography.DecryptString(arrUID[1]);
                sDocUserId = strUID;
                string[] arrPW = arrString[4].Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                strPW = RMCryptography.DecryptString(arrPW[1]);

                objConnStr.AppendFormat("{1}{0}{2}{0}{3}{0}UID={4}{0}PWD={5}{0}", DELIMITER, strDriver, strServer, strDatabase, strUID, strPW);
            }

            return objConnStr.ToString();
        }
        /// <summary>
        /// create SQL to get table ID from HIST_TRACK_DICTIONARY for specified table
        /// </summary>
        /// <param name="strTableName">table name</param>
        /// <returns>SQL script</returns>
        public static string GetHistTrackTableID(string strTableName)
        {
            return String.Format("SELECT TABLE_ID FROM HIST_TRACK_TABLES WHERE TABLE_NAME='{0}'", strTableName);
        }
        /// <summary>
        /// create SQL to update HIST_TRACK_DICTIONARY
        /// </summary>
        /// <returns>SQL script</returns>
        public static string UpdateHistTrackDictionary(string sColName, string sDataType, int iTableId, int iSize, int? iPrecision, int? iScale)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("UPDATE HIST_TRACK_DICTIONARY SET ");
            objSQL.AppendFormat(" DATATYPE ='{0}',COLUMN_SIZE={1} ,", sDataType, iSize);
            objSQL.AppendFormat(" NUMERIC_PRECISION = {0} ,NUMERIC_SCALE ={1} ", iPrecision == null ? "NULL" : iPrecision.ToString(), iScale == null ? "NULL" : iScale.ToString());
            objSQL.AppendFormat(" WHERE TABLE_ID = {0} AND COLUMN_NAME = '{1}'", iTableId, sColName);
            return objSQL.ToString();
        }
        /// <summary>
        /// create SQL to update HIST_TRACK_DICTIONARY
        /// </summary>
        /// <returns>SQL script</returns>
        public static string UpdateHistTrackDictionary(string sColName, string sToColName, int iTableId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("UPDATE HIST_TRACK_DICTIONARY SET ");
            objSQL.AppendFormat(" COLUMN_NAME ='{0}' ", sToColName);
            objSQL.AppendFormat(" WHERE TABLE_ID = {0} AND COLUMN_NAME = '{1}'", iTableId, sColName);
            return objSQL.ToString();
        }
        /// <summary>
        /// create SQL to delete record from  HIST_TRACK_DICTIONARY
        /// </summary>
        /// <returns>SQL script</returns>
        public static string DeleteHistTrackDictionaryLine(string sColName, int iTableId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("DELETE FROM  HIST_TRACK_DICTIONARY ");
            objSQL.AppendFormat(" WHERE TABLE_ID = {0} AND COLUMN_NAME = '{1}'", iTableId, sColName);
            return objSQL.ToString();
        }
        /// <summary>
        /// Create SQL to update HIST_TRACK_TABLES Flag
        /// </summary>
        /// <param name="iTableId">Table id</param>
        /// <returns>SQL script</returns>
        public static string UpdateHistTrackTableFlag(int iTableId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("UPDATE HIST_TRACK_TABLES SET ");
            objSQL.Append(" UPDATED_FLAG =-1 ");
            objSQL.AppendFormat(" WHERE TABLE_ID = {0}", iTableId);
            return objSQL.ToString();
        }
        /// <summary>
        /// Create SQL to Delete record from HIST_TRACK_COLUMNS
        /// </summary>
        /// <param name="sColName">Column Name</param>
        /// <param name="iTableId">Table Id</param>
        /// <returns></returns>
        public static string DeleteHisttrackClmnLine(string sColName,int iTableId)
        {
            var objSQL = new StringBuilder();

            objSQL.Append("DELETE FROM  HIST_TRACK_COLUMNS ");
            objSQL.Append(" WHERE COLUMN_ID IN (SELECT COLUMN_ID FROM HIST_TRACK_DICTIONARY ");
            objSQL.AppendFormat(" WHERE TABLE_ID = {0} AND COLUMN_NAME ='{1}'", iTableId, sColName);
            return objSQL.ToString();
        }

        /// <summary>
        /// Create SQL to create HIST_TRACK_SYNC table
        /// </summary>
        /// <returns></returns>
        public static string CreateHistTrackSyncTable()
        {
            var objSQL = new StringBuilder();
            if (DisplayDBUpgrade.g_dbMake == RiskmasterDBTypes.DBMS_IS_SQLSRVR)
            {
                objSQL.Append("CREATE TABLE HIST_TRACK_SYNC( ");
                objSQL.Append("ROW_ID int NOT NULL, ");
                objSQL.Append("TABLE_NAME varchar(50) NULL, ");
                objSQL.Append("SQL_STMNT varchar(4000) NULL)");
            }
            else
            {


                objSQL.Append("CREATE TABLE HIST_TRACK_SYNC( ");
                objSQL.Append("ROW_ID NUMBER(38) NOT NULL, ");
                objSQL.Append("TABLE_NAME VARCHAR2(50) NULL, ");
                objSQL.Append("SQL_STMNT VARCHAR2(4000) NULL)");
            }
            
            return objSQL.ToString();

        }
    }
}