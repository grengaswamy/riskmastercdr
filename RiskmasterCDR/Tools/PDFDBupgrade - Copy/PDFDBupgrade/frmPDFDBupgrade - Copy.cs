﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RMXdBUpgradeWizard;
using System.Configuration;
using System.Xml;
using System.Security.Cryptography;
using System.IO;



namespace PDFDBupgrade
{
    public partial class Form1 : Form
    {
        public const string strDSNColumn = "DSN";
        public const string strDSNIDColumn = "DSNID";
        private string _errorCache;
        public static string conn = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        public static void LogErrors(string strLogFilePath, string sMsg)
        {
            File.AppendAllText(strLogFilePath, sMsg);

            //Insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }
       

        private void Form1_Load(object sender, EventArgs e)
        {
            


            lblmsg.Text = "";

            XmlTextReader reader = new XmlTextReader("connectionStrings.config");
            bool bflag = System.IO.File.Exists("connectionStrings.config");
            if (bflag)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);
                
                XmlElement root = doc.DocumentElement;
                XmlNodeList xnList = doc.SelectNodes("/connectionStrings");
                XmlNode xmlEncryptedNode = xnList[0];
                string sChildElement = xmlEncryptedNode.FirstChild.Name;
                XmlNode xmlEncryptedData = xmlEncryptedNode.SelectSingleNode("EncryptedData");
                DpapiProtectedConfigurationProvider objDpapi = new DpapiProtectedConfigurationProvider();
                XmlNode xmlDecryptedData = objDpapi.Decrypt(xmlEncryptedData);
                foreach (XmlNode chldNode in xmlDecryptedData.ChildNodes)
                {
                    if (!(chldNode.Attributes == null))
                    {
                        if (chldNode.Attributes["name"].Value == "RMXSecurity")
                        {
                            conn = chldNode.Attributes["connectionString"].Value;
                            break;
                        }
                    }
                       
                }
                if (DisplayDBUpgrade.VerifyConnection(conn))
                {
                    LoadDSNs(conn);
                }
                else
                {
                    Finalreport("Connectionstring not valid");
                }
            }
            else
            {
                string sNull=string.Empty;
                sNull = "The connectionStrings.config file does not exist";

               Finalreport(sNull);
               btnUpgrade.Enabled = false;


            }
            
        }

        private  bool LoadDSNs(string sConnString)
        {
            DataTable dtListItems = new DataTable();

            dtListItems.Columns.Add(strDSNColumn);
            dtListItems.Columns.Add(strDSNIDColumn);

            //clear list box of any previous items
            DSNList.Items.Clear();


            try
            {
                if (String.IsNullOrEmpty(sConnString))
                {
                    DisplayDBUpgrade.g_SecConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                }
                else
                {
                    DisplayDBUpgrade.g_SecConnectString = sConnString;
                }
                                
                using (var drDSNs = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetOnlyDSNs()))
                {
                    while (drDSNs.Read())
                    {
                        string strKey = drDSNs[DisplayDBUpgrade.strDSNColumn].ToString();
                        string strValue = drDSNs[DisplayDBUpgrade.strDSNIDColumn].ToString();
                        string[] str = new string[2];
                        str[0] = strKey;
                        str[1] = strValue;
                        ListViewItem lvi = new ListViewItem(str);

                        DSNList.Items.Add(lvi);
                       
                    }
                }

                if (DSNList.Items.Count <= 0)
                {
                    _errorCache = "An error has occurred during population of DSNs." +
                                  Environment.NewLine + Environment.NewLine +
                                  "There are no datasources configured in the specified security database. " +
                                  "Please correct and try again later.";
                    ErrorDisplay(_errorCache, String.Empty);

                    return false;
                }

                DSNList.SelectedItems.Clear();

                return true;
            }
            catch (Exception ex)
            {
                _errorCache = "An error has occurred during population of DSNs." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused by not being able to connect to the security database, " +
                              "please double check connection information before trying again.";
                string sNull = ex.Message;
                ErrorDisplay(_errorCache, ex.Message);

                return false;
            }
        }
        public static void ErrorDisplay(string sError, string sException)       
         {       
            
         }
        public static string ValidateselectionDSN(int selecteddsncount)
        {
             string errorReport = string.Empty;
            if (selecteddsncount != 0)            
            {          
                int iFailed = 0;
                string sErrorMsg = String.Empty;

                //reset lists before proceeding
                DisplayDBUpgrade.dictFailedItems.Clear();
                DisplayDBUpgrade.dictPassedItems.Clear();
                //lvFailed.Items.Clear();
                //lvPassed.Items.Clear();

                //test each selected connection for connectivity
                foreach (var kvp in DisplayDBUpgrade.dictSelectedItems)
                {
                    //lblTestingDB.Text = "Checking connectivity for: <font color='#A0522D'>" + kvp.Key + "</font>";
                    
                    //try connection
                    DisplayDBUpgrade.bAborted = DisplayDBUpgrade.SelectDatabase(kvp.Value, ref sErrorMsg);
                    
                    //handle failed connection
                    if (!DisplayDBUpgrade.bAborted)
                    {
                        DisplayDBUpgrade.dictFailedItems.Add(kvp.Key, kvp.Value);
                        iFailed++;
                        
                        //lvFailed.Items.Add(kvp.Key, sErrorMsg);
                        
                        continue;
                    }

                    //handle passed connections
                    DisplayDBUpgrade.dictPassedItems.Add(kvp.Key, kvp.Value);
                    
                    //lvPassed.Items.Add(kvp.Key);
                    
                }

                //display approriate navigation message
                if (iFailed == DisplayDBUpgrade.dictSelectedItems.Count)
                {
                    //errorReport = "<font color='red'>Connections to all of your selections have failed, press <b>Back</b> to retry, or <b>Cancel</b> to exit.</font>";
                    errorReport = "Connections to all of your selections have failed";
                   
                }
                else if (iFailed > 0 && iFailed != DisplayDBUpgrade.dictSelectedItems.Count)
                {
                    //errorReport = "<font color='red'>Connections to some of your selections have failed, press <b>Back</b> to retry, <b>Next</b> to continue with valididated selections (processing will begin immediately), or <b>Cancel</b> to exit.</font>";
                    errorReport = "Connections to some of your selections have failed";
                    
                }
                else
                {
                    //errorReport = "Connections to all of your selections have been verified, press <b>Next</b> to continue (processing will begin immediately), or <b>Cancel</b> to exit";                   

                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, "generic.sql", String.Empty, false);
                    errorReport="";

                }
                
            }
            return errorReport;

            
        }


        private void btnUpgrade_Click(object sender, EventArgs e)
        {
            //clear out any previously added datasources
            DisplayDBUpgrade.dictSelectedItems.Clear();            
            int dsncount = 0;
            string statusMessage;
            dsncount = DSNList.SelectedItems.Count;

            if (dsncount > 0)
            {
                foreach (ListViewItem lvi in DSNList.SelectedItems)
                {
                    DisplayDBUpgrade.dictSelectedItems.Add(lvi.Text.Trim(), Convert.ToInt32(lvi.SubItems[1].Text));
                }
            }           
            
                if (dsncount == 0)
                {
                    statusMessage ="Select DSN";
                }
                else
                {
                    statusMessage = ValidateselectionDSN(dsncount);
                }
                Finalreport(statusMessage);
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        public void Finalreport(string report)
        {
            string status = report.ToString();

            if (status != "")
            {
                DSNList.Enabled = true;
                btnCancel.Text = "Cancel";

                btnUpgrade.Enabled = true;
                lblmsg.Text = status;
                lblmsg.ForeColor = Color.Red;
                
            }
            else
            {
                DSNList.Enabled = false;
                btnCancel.Text = "Finish";

                btnUpgrade.Enabled = false;
                lblmsg.ForeColor = Color.Green;
                lblmsg.Text = "DB upgrade completed";
            }
            
            
        }



        private void DSNList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            lblmsg.Text = "";
        }

    }

}
