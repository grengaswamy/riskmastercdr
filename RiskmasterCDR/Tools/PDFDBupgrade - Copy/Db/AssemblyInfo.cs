using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("RISKMASTER Database Access Abstraction Layer")]
[assembly: AssemblyDescription("RISKMASTER Database Access Abstraction Layer")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CSC")]
[assembly: AssemblyProduct("PDFDBupgrade")]
[assembly: AssemblyCopyright("Copyright (c) 2014, CSC")]
[assembly: AssemblyTrademark("CSC")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dfabb98f-727c-481e-86c0-6fa4a8b20e51")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("14.1.0.0")]
[assembly: AssemblyFileVersion("14.1.0.0")]
[assembly: AssemblyInformationalVersion("14.1.0.0")]

