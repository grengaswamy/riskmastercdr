using System;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Oracle.DataAccess.Client;



namespace Riskmaster.Db
{

    /// <summary>Specifies the DBMS that is in use.  Constants should match those previously used in DTGRocket.</summary>
    public enum eDatabaseType : int
    {
        /// <summary>
        /// Provide support for OleDb driver support for MS Access as well as Microsoft Excel
        /// </summary>
        /// <remarks>MS Access and MS Excel continue to be utilized by BRS (Bill Review System)</remarks>
        DBMS_IS_ACCESS = 0,

        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR indicates that the database management system
        /// is some flavor of Microsoft SQL Server.</summary>
        /// <remarks>Microsoft SQL Server is supported as a native Provider where possible and through ODBC otherwise.
        /// You can use the "driver=RMSQLSERVER" name value in a connection string to indicate this to the Riskmaster Database Layer.
        /// </remarks>
        DBMS_IS_SQLSRVR = 1,

        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_SYBASE indicates that the database management system
        /// is some flavor of Sybase.</summary>
        /// <remarks>Sybase is supported only through the ODBC provider.</remarks>
        DBMS_IS_SYBASE = 2,

        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_INFORMIX indicates that the database management system
        /// is some flavor of Informix.</summary>
        /// <remarks>Informix is supported only through the ODBC provider.</remarks>
        DBMS_IS_INFORMIX = 3,


        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_ORACLE indicates that the database management system
        /// is some flavor of Oracle.</summary>
        /// <remarks>Oracleis supported as a native Provider where possible and through ODBC otherwise.
        /// You can use the "driver=RMORACLE" name value in a connection string to indicate this to the Riskmaster Database Layer.
        /// </remarks>
        DBMS_IS_ORACLE = 4,

        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_ODBC indicates that the specific database management system
        /// could not be determined but is running through the ODBC Provider.</summary>
        /// <remarks>none</remarks>
        DBMS_IS_ODBC = 5,

        /// <summary>Riskmaster.Db.eDatabaseType.DBMS_IS_DB2 indicates that the database management system
        /// is some flavor of DB2.</summary>
        /// <remarks>IBM DB2 is supported as a native Provider where possible and through ODBC otherwise.
        /// You can use the "driver=RMDB2" name value in a connection string to indicate this to the Riskmaster Database Layer.
        /// </remarks>
        DBMS_IS_DB2 = 6
    }


    /// <summary>Specifies the Native .Net Data Provider  that is in use.</summary>
    /// <remarks>This indicates the type of Provider objects being used internally by the Riskmaster Data Layer.
    ///   These objects can be accessed ONLY from inside of this assembly using the AsNative() methods.</remarks>
    public enum eConnectionType : int
    {
        /// <summary>
        /// Managed ODBC Driver
        /// </summary>
        Odbc = 0,
        /// <summary>
        /// Managed SQL Server driver
        /// </summary>
        ManagedSqlServer = 1,
        /// <summary>
        /// Managed Oracle Server driver
        /// </summary>
        ManagedOracleServer = 2,
        /// <summary>
        /// Managed DB2 driver
        /// </summary>
        ManagedDB2Server = 3,
        //Oledb Driver
        OleDb = 4
    }

    //

    /// <summary>Riskmaster.Db.eLockType enumerates the available locking schemes.  
    /// Currently only RMOptimistic or None are supported.</summary>
    /// <remarks>none</remarks>
    public enum eLockType : int
    {

        /// <summary>Riskmaster.Db.eLockType.None is the default behavior in which no locking is enforced.</summary>
        /// <remarks>none</remarks>
        None = 0,

        /// <summary>Riskmaster.Db.eLockType.RMOptimistic is the legacy locking scheme from DTGRocket.</summary>
        /// <remarks>This locking scheme uses DTTM_xxx fields to check for optimistic locking violations.  
        /// Note that these fields MUST be included in the query result record being updated.  
        /// Otherwise there will be no original stamp (as read from DB initially) to which the current (possibly updated) db stamp can be compared.</remarks>
        RMOptimistic = 1
    }

    //

    /// <summary> Event Argument Implementation for Riskmaster.Db.DbInfoMessageArgs.</summary>
    /// <remarks>Passed to Event recipients of the InfoMessage event which can be fired upon opening a connection.</remarks>
    public class DbInfoMessageArgs : EventArgs
    {

        /// <summary>Riskmaster.Db.DbInfoMessageArgs.DbInfoMessageArgs wraps the Native Provider implementations.</summary>
        /// <param name="e">The native provider InfoMessageEventArgs object to wrap.</param>
        /// <remarks>none</remarks>
        internal DbInfoMessageArgs(object e)
        {
            if (e is SqlInfoMessageEventArgs)
            {
                SqlInfoMessageEventArgs native = (e as SqlInfoMessageEventArgs);
                m_message = native.Message;
                m_source = native.Source;
                m_errorCollection = native.Errors;
            }
            if (e is OdbcInfoMessageEventArgs)
            {
                OdbcInfoMessageEventArgs native = (e as OdbcInfoMessageEventArgs);
                m_message = "";
                m_source = "";
                m_errorCollection = native.Errors;
            }
            if (e is OleDbInfoMessageEventArgs)
            {
                OleDbInfoMessageEventArgs native = (e as OleDbInfoMessageEventArgs);
                m_message = "";
                m_source = "";
                m_errorCollection = native.Errors;
            }
            if (e is OracleInfoMessageEventArgs)
            {
                OracleInfoMessageEventArgs native = (e as OracleInfoMessageEventArgs);
                m_message = native.Message;
                m_source = native.Source;
            }

        }
        private string m_message = string.Empty;
        private string m_source = string.Empty;
        private object m_errorCollection;//=null;

        /// <summary>Wraps the Native property of the same name.</summary>
        /// <returns>String containing the message information.</returns>
        /// <remarks>See ADO.NET documentation for details.</remarks>
        /// <example>No example available.</example>
         public string Message { get { return m_message; } }

        /// <summary>Wraps the Native property of the same name.</summary>
        /// <returns>String containing the message source information.</returns>
        /// <remarks>See ADO.NET documentation for details.</remarks>
        /// <example>No example available.</example>
         public string Source { get { return m_source; } }

        /// <summary>Wraps the Native property of the same name.</summary>
        /// <returns>The errors collection returns a Native Provider collection object if available.</returns>
        /// <remarks>See ADO.NET documentation for details.</remarks>
        /// <example>No example available.</example>
         public object Errors { get { return m_errorCollection; } }
    }

    /// <summary>Provides the Delegate Type required of clients who wish to recieve the DbInfoMessage event.</summary>
    /// <remarks>See .NET event handling documentation for details.</remarks>
    public delegate void DbInfoMessageEventHandler(DbConnection sender, DbInfoMessageArgs e);

    /// <summary>
    /// This object is the core data layer object responsible for handing out:
    /// readers, transactions and commands.  Any derived version of this class
    /// will have to take responsibility for handing out sub-objects appropriately (i.e. - remotable proxies)
    /// </summary> 
    /// //Component,
    public class DbConnection : IDisposable, IDbConnection
    {
        /// <summary>Riskmaster Proprietary Connection String SQL Server Driver Constant (RMSQL)</summary>
        public const string RM_PROVIDER_SQLSRVR = "RMSQL";
        /// <summary>Riskmaster Proprietary Connection String DB2 Server Driver Constant (RMDB2)</summary>
        public const string RM_PROVIDER_DB2 = "RMDB2";
        /// <summary>Riskmaster Proprietary Connection String Oracle Server Driver Constant (RMORACLE)</summary>
        public const string RM_PROVIDER_ORACLE = "RMORACLE";

        private string m_ConnectionString = string.Empty, m_nativeConnectionString = string.Empty;
        private IDbConnection m_anyConnection;//=null;
        private eConnectionType m_Type = eConnectionType.Odbc;
        private eDatabaseType m_DbType = eDatabaseType.DBMS_IS_ODBC;

        internal int m_InstanceId;
        static int g_InstanceId;
        internal static int g_OpenCount;
        /// <summary>Event raised for our clients when our internal Data provider sends additional connection information to us via the InfoMessage event.</summary>
        public event DbInfoMessageEventHandler InfoMessage;

        /// <summary>
        /// Riskmaster.Db.DbConnection.DbConnection used internally by DbTransaction object to wrap the returned
        /// native connection object.</summary>
        /// <param name="anyConnection">IDbConnection interface of the Native provider connection to wrap.</param>
        /// <remarks>This is a hack to support getting the DbConnection object through the Connection property of the DbTransaction
        /// object.  It has two main issues: first, the connection string credentials will probably be lost. 
        /// (They are munged for security reasons by most native providers instead of being returned..)
        /// Second, the DatabaseType and ConnectionType properties may not be properly initialized if the connection
        /// was using ODBC and relied upon testing performed in the "Open" method.  </remarks>
        internal DbConnection(IDbConnection anyConnection)
        {
            m_InstanceId = g_InstanceId++;
            m_anyConnection = anyConnection;
            m_ConnectionString = anyConnection.ConnectionString;
            ConnectionStringChanged();
        }

        /// <summary>
        /// Allows only the static DBFactory  class to create new instances of DbConnection class.
        /// </summary>
        /// <param name="connectionstring">Database Connection String</param>
        /// <remarks>The connection string here is checked for a a "Driver=value" pair.  If no such pair exists in the string, then
        ///   we are forced to assume ODBC and the selection process for a .Net data provider is over.
        ///   Otherwise, the pair does exist and is examined further.  First the we check to see if the value is one of the 
        ///   Riskmaster specific driver tokens ("RMSQL","RMDB2","RMORACLE").   If any of these tokens is detected 
        ///   in the "Driver=value" pair then the code will attempt to use the specific .Net Data provider from that DBMS vendor.  
        ///  Otherwise the code falls back to checking if the value specified in the "Driver=value" pair  can be 
        ///  found in the hash tables filled by the static constructor.  If it is found in the hash tables  then 
        ///  we attempt to hueristically "guess" the "best fit" native provider based on the "provider names" from the registry.
        ///</remarks>
        internal DbConnection(string connectionstring)
        {
            m_InstanceId = g_InstanceId++;
            m_ConnectionString = connectionstring;
            ConnectionStringChanged();
        }

        /// <summary>
        /// Allows DbFactory to produce a duplicate connection.  Returns a fully connected copy of the Src  connection object.  
        /// Intended for use when each connection is restricted to hosting a single recordset at a time (SQLServer).
        /// </summary>
        /// <param name="Src">A source DbConnection object to Clone.</param>
        internal DbConnection(DbConnection Src)
        {
            m_InstanceId = g_InstanceId++;

            if (Src == null)
                throw (new InvalidOperationException("Source DbConnection does not exist."));

            this.ConnectionString = Src.ConnectionString;

            if (Src.State == ConnectionState.Open)
                this.Open();
        }


        /// <summary>Riskmaster.Db.DbConnection.ConnectionStringChanged  implements the 
        /// detailed .Net Native Provider selection logic used by the DbConnection constructor(s).</summary>
        /// <remarks>Uses the internal stored connection string for these checks.
        ///  The connection string here is checked for a a "Driver=value" pair.  If no such pair exists in the string, then
        ///   we are forced to assume ODBC and the selection process for a .Net data provider is over.
        ///   Otherwise, the pair does exist and is examined further.  First the we check to see if the value is one of the 
        ///   Riskmaster specific driver tokens ("RMSQL","RMDB2","RMORACLE").   If any of these tokens is detected 
        ///   in the "Driver=value" pair then the code will attempt to use the specific .Net Data provider from that DBMS vendor.  
        ///  Otherwise the code falls back to checking if the value specified in the "Driver=value" pair  can be 
        ///  found in the hash tables filled by the static constructor.  If it is found in the hash tables  then 
        ///  we attempt to hueristically "guess" the "best fit" native provider based on the "provider names" from the registry.
        ///</remarks>         
        protected  void ConnectionStringChanged()
        {

            m_nativeConnectionString = m_ConnectionString;

            if (m_ConnectionString.IndexOf("provider=", StringComparison.OrdinalIgnoreCase) >= 0)
                 m_Type=eConnectionType.OleDb;
            else if (m_ConnectionString.IndexOf("dsn=", StringComparison.OrdinalIgnoreCase) >= 0) //Uses an ODBC DSN - force ODBC Provider to be used.
                m_Type = eConnectionType.Odbc;
            //Is either an ODBC DSN-less connection OR a native connection string with our required driver name\value pair.
            else if (m_ConnectionString.IndexOf("driver=", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                //Pick up the "driver" named value.
                int start; int length; string sConnectionStringDriverValue;

                start = m_ConnectionString.IndexOf("driver=", StringComparison.OrdinalIgnoreCase) + 7;
                length = (m_ConnectionString.IndexOf(';', start)) - start;
                sConnectionStringDriverValue = m_ConnectionString.Substring(start, length).ToUpper();
                //BSB 04.08.2004  Found { and } characters in this value.
                // Evidently this is valid in an ODBC connection string but will break our match if we leave them in.
                sConnectionStringDriverValue = Regex.Replace(sConnectionStringDriverValue, "[{}]", "").Trim();

                //Handle the provider selection based on RM Proprietary "Driver=" connection string values.
                m_DbType = GetRMProprietaryMake(sConnectionStringDriverValue);

                if (m_DbType == eDatabaseType.DBMS_IS_ODBC) //No Proprietary RM string  recognized  - must be an older ODBC DSN-less
                    m_DbType = GetODBCMake(sConnectionStringDriverValue);

                //BSB 05.04.2004 Bug Fix. Keep the "raw" string for use externally.  The native string is only of interest internally.
                if (m_DbType != eDatabaseType.DBMS_IS_ODBC)
                    m_nativeConnectionString = TranslateConnectionStringToNative(m_ConnectionString);
                //				m_ConnectionString = TranslateConnectionStringToNative(m_ConnectionString);

                switch (m_DbType)   //Route to native Provider if available
                {
                    case eDatabaseType.DBMS_IS_DB2:
                        m_Type = eConnectionType.ManagedDB2Server;
                        break;
                    case eDatabaseType.DBMS_IS_ORACLE:
                        m_Type = eConnectionType.ManagedOracleServer;
                        break;
                    case eDatabaseType.DBMS_IS_SQLSRVR:
                        m_Type = eConnectionType.ManagedSqlServer;
                        break;
                    default:  //either we don't recognize the driver string OR there isn't a native provider for it.
                        m_Type = eConnectionType.Odbc;
                        m_DbType = eDatabaseType.DBMS_IS_ODBC; //will attempt specific type match upon connection.open
                        break;
                }
            }
        }
        /// <summary>
        /// Opens connection to database specified by connection string.
        /// </summary>
         public void Open()
        {

            if (m_anyConnection != null)
                throw (new InvalidOperationException("Connection already created."));

            if (DbFactory.PendingGCRequestFlag)
            {
                System.GC.Collect();//All Generations
                System.GC.WaitForPendingFinalizers();
                DbFactory.ClearPendingGCRequestFlag();
            }

            switch (m_Type)
            {
                case eConnectionType.ManagedSqlServer:
                    {
                        m_anyConnection = new SqlConnection(m_nativeConnectionString);
                        (m_anyConnection as SqlConnection).InfoMessage += SQLInfoMessageRecieved;
                        m_DbType = eDatabaseType.DBMS_IS_SQLSRVR;
                        break;
                    }
                case eConnectionType.ManagedOracleServer:
                    {
                        m_anyConnection = new OracleConnection(m_nativeConnectionString);
                        (m_anyConnection as OracleConnection).InfoMessage += OracleInfoMessageRecieved;
                        m_DbType = eDatabaseType.DBMS_IS_ORACLE;
                        break;
                    }
                case eConnectionType.OleDb:
                    {
                        m_anyConnection = new OleDbConnection(m_nativeConnectionString);
                        (m_anyConnection as OleDbConnection).InfoMessage += OleDbInfoMessageReceived;
                        m_DbType = eDatabaseType.DBMS_IS_ACCESS;
                        break;
                    }
                    

                case eConnectionType.Odbc:
                    {
                        m_anyConnection = new OdbcConnection(m_nativeConnectionString);
                        (m_anyConnection as OdbcConnection).InfoMessage += OdbcInfoMessageRecieved;
                        m_DbType = eDatabaseType.DBMS_IS_ODBC;
                        break;
                    }
            }

            string sTmpCallerInfo = "n/a";
            if (DbFactory.IsDbgLevel((int)DbTrace.ConnectionOpenerStack))
            {
                StackTrace objTrace = new StackTrace(true);
                StackFrame objFrame = objTrace.GetFrame(1);
                sTmpCallerInfo = objFrame.GetMethod().ReflectedType.Name + objTrace.ToString();
            }

            DbFactory.Dbg(this.m_InstanceId + " OPEN " + "COUNT:" + g_OpenCount + "DETAIL:" + m_ConnectionString + "CALLER:" + sTmpCallerInfo, DbTrace.Connect);
            g_OpenCount++;

            // BSB 02.22.2006 Apply Instrumentation 
            // Allow for retry of Connection.Open if Pool is Busy. (ProtectPool)
            // Found that indeterminate GC timing and programmers not reliably calling Dispose when necessary
            // can cause all of the Db Connections in the Pool to appear busy.
            // Forcing GC and hence the dispose routines to run should have a fair chance at freeing up
            // a number of connections. The following logic tries this before admitting defeat.
            try { m_anyConnection.Open(); }
            catch (InvalidOperationException e)
            {
                if (DbFactory.ProtectPool)
                {
                    DbFactory.Dbg(this.m_InstanceId + " OPEN " + "POOL FULL - BEGIN GC ATTEMPT - COUNT:" + g_OpenCount + "DETAIL:" + m_ConnectionString, DbTrace.Connect);

                    //Force GC
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    DbFactory.Dbg(this.m_InstanceId + " OPEN " + "POOL FULL - FINISH GC ATTEMPT - COUNT:" + g_OpenCount + "DETAIL:" + m_ConnectionString, DbTrace.Execute);
                    //Retry only once - if there's still a pool error here, let it blow out of the handler.
                    m_anyConnection.Open();
                }
                else
                    throw e;
            }

            //More Logging - Try to Associate Serve PID with Our Connection
            switch (m_Type)
            {
                case eConnectionType.ManagedSqlServer:
                    DbFactory.Dbg(this.m_InstanceId + " OPEN " + "SPID:" + this.ExecuteString("SELECT @@SPID") + " COUNT:" + g_OpenCount + "DETAIL:" + m_ConnectionString, DbTrace.Connect);
                    break;
            }


            if (m_Type == eConnectionType.Odbc)		//Must wait until after connection is opened...
                m_DbType = GetODBCMake();
        }



        /// <summary>Utility function Riskmaster.Db.DbConnection.GetRMProprietaryMake 
        /// maps the Riskmaster proprietary "Driver=value" values to .Net Native Provider types.</summary>
        /// <param name="sProvider">From a connection string, the "value" chunk of the "Driver=value" pair.</param>
        /// <returns>DBMS vendor type which indicates what .Net Native provider should be used.</returns>
        /// <remarks>Will return ODBC for all unknown values of the sProvider parameter.</remarks>
         protected eDatabaseType GetRMProprietaryMake(string sProvider)
        {
            switch (sProvider)
            {
                case RM_PROVIDER_SQLSRVR:
                    return eDatabaseType.DBMS_IS_SQLSRVR;
                case RM_PROVIDER_DB2:
                    return eDatabaseType.DBMS_IS_DB2;
                case RM_PROVIDER_ORACLE:
                    return eDatabaseType.DBMS_IS_ORACLE;
                default:
                    return eDatabaseType.DBMS_IS_ODBC;
            }
        }





        /// <summary>Riskmaster.Db.DbConnection.GetODBCMake attempts to determine from an open ODBC connection
        /// which DBMS system is in use.</summary>
        /// <returns>An eDatabaseType representing the DBMS determined to be in use.</returns>
        /// <remarks> Uses a static Hashtable of Registry Known ODBC Providers\Drivers
        ///  to determine the connection type.
        ///  Steps:
        ///  1.) Find the currently in-use driver from the odbc connection object.
        ///  2.) Pull in it's provider list from the registry Hashtable.
        ///  3.) Search the provider list for a "strongly suggestive" dbmake string.
        ///  		Note: Relies on the static constructor to have populated the Hashtable
        ///  		 called m_SystemOdbcDllToFullProviderList.</remarks>
         protected eDatabaseType GetODBCMake()
        {
            string sDriver = string.Empty;
            OdbcConnection cn = (OdbcConnection)m_anyConnection;
            sDriver = System.IO.Path.GetFileName(cn.Driver).ToUpper();
            return GetODBCMake(sDriver);
        }

        /// <summary>Riskmaster.Db.DbConnection.GetODBCMake attempts to determine from the passed in sDriver string
        /// which DBMS system is in use.</summary>
        /// <param name="sDriver">The odbc driver .dll file name from which to pick a DBMS type.</param>
        /// <returns>An eDatabaseType representing the DBMS determined to be in use.</returns>
        /// <remarks> Uses a static Hashtable of Registry Known ODBC Providers\Drivers
        ///  to determine the connection type.
        ///  Steps:
        ///  1.) Pull in the provider list from the registry Hashtable using the sDriver value as a key.
        ///  2.) Search the provider list for a "strongly suggestive" dbmake string.
        ///  		Note: Relies on the static constructor to have populated the Hashtable
        ///  		 called m_SystemOdbcDllToFullProviderList.</remarks>
         protected eDatabaseType GetODBCMake(string sDriver)
        {
            return DatabaseDrivers.GetDatabaseDriverType(sDriver);
        }//method: GetODBCMake()




        /// <summary>Provides ODBC name\value pair translation to Native name\value pairs for each provider.</summary>
        /// <param name="sConnectionString">Connection string to be translated to Native name\value pairs from ODBC pairs.</param>
        /// <returns>Provider Specific Connection String</returns>
        /// <remarks>This may need to be enhanced as more unusual connection strings are discovered.</remarks>
        protected  string TranslateConnectionStringToNative(string sConnectionString)
        {
            string strParsedConnectionString = string.Empty, strProviderConnectionString = string.Empty;

            strParsedConnectionString = RemoveNameValuePair(sConnectionString, "driver");
            string ORCL_REGEX = "(Server=(?<TNS_NAME>[^;]+);Dbq=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)|(DBQ=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)";
            string ORCL_REPLACE_REGEX = "Data Source=${TNS_NAME};User Id=${UID};Password=${PWD};";

            switch (m_DbType) //Translate for the appropriate target DBMS
            {
                case eDatabaseType.DBMS_IS_SQLSRVR:
                    strProviderConnectionString = ReplaceItemName(strParsedConnectionString, "UID", "User ID");
                    break;
                case eDatabaseType.DBMS_IS_ORACLE:
                    strProviderConnectionString = Regex.Replace(strParsedConnectionString, ORCL_REGEX, ORCL_REPLACE_REGEX, RegexOptions.IgnoreCase);
                    break;
                default:
                    strProviderConnectionString = sConnectionString;
                    break;
            }

            return strProviderConnectionString;

        }


        /// <summary>Riskmaster.Db.DbConnection.ReplaceItemName supports the TranslateConnectionString method
        /// by simply changing the name portion of a name\value pair if found from sOldItemName to sNewItemName and
        /// returning the fully modified connection string.</summary>
        /// <param name="sSource">The connection string in which to effect the change.</param>
        /// <param name="sOldItemName">The ODBC name token to be replaced.</param>
        /// <param name="sNewItemName">The Native name token to substitute.</param>
        /// <returns>An updated version of the connection string from sSource.</returns>
        /// <remarks>There may be ODBC niceties like {} or escape sequences that are not handled properly here yet.</remarks>
        private static string ReplaceItemName(string sSource, string sOldItemName, string sNewItemName)
        {
            string[] sPairs = sSource.Split(';');
            string[] sPair = null;
            for (int i = 0; i < sPairs.Length; i++)
            {
                sPair = sPairs[i].Split('=');
                if (sPair[0].Trim().Equals(sOldItemName, StringComparison.OrdinalIgnoreCase))
                    sPairs[i] = String.Format("{0}={1}", sNewItemName, sPair[1]);
            }
            return String.Join(";", sPairs);
        }


        /// <summary>Riskmaster.Db.DbConnection.RemoveNameValuePair supports the TranslateConnectionString method
        /// by simply removing a name\value pair and returning the fully modified connection string.</summary>
        /// <param name="sSource">The connection string in which to effect the change.</param>
        /// <param name="sItemName">The ODBC name token of the name\value pair to be removed.</param>
        /// <returns>An updated version of the connection string from sSource.</returns>
        /// <remarks>There may be ODBC niceties like {} or escape sequences that are not handled properly here yet.</remarks>
        private static string RemoveNameValuePair(string sSource, string sItemName)
        {
            string[] sPairs = sSource.Split(';');
            string[] sPair = null;
            for (int i = 0; i < sPairs.Length; i++)
            {
                sPair = sPairs[i].Split('=');
                if (sPair[0].Trim().Equals(sItemName, StringComparison.OrdinalIgnoreCase))
                    return String.Join(";", sPairs, 0, i) + String.Join(";", sPairs, i + 1, ((sPairs.Length - i) - 1));
            }
            return sSource;
        }

        /// <summary>Riskmaster.Db.DbConnection.FetchDSN utility function just parses 
        /// out the xxx from a connection string containing DSN=xxx;</summary>
        /// <param name="sConnect">The connection string to be parsed.</param>
        /// <returns>The xxx from the DSN=xxx; named value pair if present.</returns>
        /// <remarks>none</remarks>
         protected string FetchDSN(string sConnect)
        {
            char[] arrTmp = { ';' };
            string[] arrChunks = null;

            arrChunks = sConnect.Split(arrTmp);
            foreach (string sChunk in arrChunks)
                if (sChunk.IndexOf("DSN=") >= 0)
                    return sChunk.Substring(4);
                else if (sChunk.IndexOf("Data Source=") >= 0)
                    return sChunk.Substring(12);
            return "";
        }


        /// <summary>
        /// Executes a SQL statement and returns the number of rows affected.
        /// </summary>
        /// <param name="nonQueryCommand">SQL Query to execute.</param>
        /// <returns></returns>
         public int ExecuteNonQuery(string nonQueryCommand)
        {
            return ExecuteNonQueryImp(nonQueryCommand, null);
        }
        /// <summary>
        /// Executes a SQL statement and returns the number of rows affected.
        /// </summary>
        /// <param name="nonQueryCommand">SQL Query to execute.</param>
        /// <param name="objTrans">SQL Transaction in which to execute.</param>
        /// <returns></returns>
         public int ExecuteNonQuery(string nonQueryCommand, DbTransaction objTrans)
        {
            return ExecuteNonQueryImp(nonQueryCommand, objTrans);
        }
        /// <summary>
        /// Executes a SQL statement and returns the number of rows affected.
        /// </summary>
        /// <param name="nonQueryCommand">SQL Query to execute.</param>
        /// <param name="objTrans">transaction to execute</param>
        /// <returns></returns>
         internal int ExecuteNonQueryImp(string nonQueryCommand, DbTransaction objTrans)
        {
            int ret = -1;
            DbCommand _anyCmd = null;
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            _anyCmd = this.CreateCommand(); //uses fully wrapped command object.
            if (objTrans != null)
                _anyCmd.Transaction = objTrans;

            _anyCmd.CommandText = nonQueryCommand;
            _anyCmd.CommandTimeout = 0;
            try { ret = _anyCmd.ExecuteNonQuery(); }
            finally { _anyCmd.Dispose(false); } //the param to dispose is arbitrary and not used.
            return ret;
        }

        /// <summary>
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
        /// </summary>
        /// <param name="queryCommand">SQL Query to execute.</param>
        /// <returns></returns>
        public  string ExecuteString(string queryCommand)
        {
            object o = ExecuteScalar(queryCommand);
            if (o == null || o == System.DBNull.Value)
                return string.Empty;
            else
                return o.ToString();
        }

        /// <summary>
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
        /// </summary>
        /// <param name="queryCommand">SQL Query to execute.</param>
        /// <returns></returns>
        public  int ExecuteInt(string queryCommand)
        {
            object o = ExecuteScalar(queryCommand);
            if (o == null || o == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(o);
        }
        /// <summary>
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
        /// </summary>
        /// <param name="queryCommand">SQL Query to execute.</param>
        /// <returns></returns>
         public double ExecuteDouble(string queryCommand)
        {
            object o = ExecuteScalar(queryCommand);
            if (o == null || o == System.DBNull.Value)
                return 0;
            else
                return Convert.ToDouble(o);
        }
        /// <summary>
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
        /// </summary>
        /// <param name="queryCommand">SQL Query to execute.</param>
        /// <returns></returns>
         public object ExecuteScalar(string queryCommand)
        {
            DbCommand dbCmd = null;
            object ret = null;
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            dbCmd = this.CreateCommand();
            try
            {
                dbCmd.CommandText = queryCommand;
                ret = dbCmd.ExecuteScalar();
            }
            finally { dbCmd.Dispose(false); }
            return ret;
        }

        /// <summary>
        /// Sends the CommandText to the Connection and builds a DbReader
        /// </summary>
        /// <param name="queryCommand">SQL Query</param>
        /// <returns>DbReader containing the query result set.</returns>
         public DbReader ExecuteReader(string queryCommand)
        {
            return ExecuteReader(queryCommand, null);
        }

        /// <summary>
        /// Sends the CommandText to the Connection and builds a DbReader
        /// in a given transaction context.
        /// </summary>
        /// <param name="queryCommand">SQL Query</param>
        /// <param name="objTrans">Existing transaction context</param>
        /// <returns>DbReader containing the query result set.</returns>
         public DbReader ExecuteReader(string queryCommand, DbTransaction objTrans)
        {

            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));
            DbReader rdr = null;
            DbCommand cmd = this.CreateCommand();
            if (objTrans != null)
                cmd.Transaction = objTrans;
            cmd.CommandText = queryCommand;
            rdr = cmd.ExecuteReader();  //Reader becomes responsible for Disposing of the Command.
            cmd = null;
            return rdr;
        }

        /// <summary>
        /// Disposes this object and closes database connection if needed.
        /// </summary>
         public void Dispose()
        {
            if (m_anyConnection != null)
                this.Close();
        }


        /// <summary>
        /// Closes database connection.
        /// </summary>
         public void Close()
        {
            if (m_anyConnection != null)
            {
                g_OpenCount--;
                DbFactory.Dbg(this.m_InstanceId + " CLOSE " + "COUNT:" + g_OpenCount + "DETAIL:" + m_ConnectionString, DbTrace.Connect);
                if (m_anyConnection.State != ConnectionState.Closed)
                    try
                    {
                        m_anyConnection.Close();
                    }
                    catch (InvalidOperationException e)
                    {
                        if (e.InnerException != null)
                        {
                            Debug.Write(e.InnerException.Message + " " + e.Source + e.TargetSite + e.StackTrace);
                        }
                        else
                        {
                            Debug.Write(e.Message + " " + e.Source + e.TargetSite + e.StackTrace);
                        }//else
                    }
                    catch (Exception e)
                    {
                        DbFactory.Dbg(this.m_InstanceId + " ***Exception Closing Connection: " + e.Message, DbTrace.Connect);
                    }
                m_anyConnection.Dispose();
                m_anyConnection = null;
            }
        }

        /// <summary>
        /// Returns the type of the database connection used.
        /// </summary>
         public eConnectionType ConnectionType
        {
            get { return m_Type; }
        }

        /// <summary>
        /// Returns the database version string for the database in use.
        /// The version is of the form ##.##.####, where 
        /// the first two digits are the major version,
        ///  the next two digits are the minor version, and 
        ///  the last four digits are the release version. 
        ///  The string is of the form major.minor.build, 
        ///  where major and minor are exactly two digits and 
        ///  build is exactly four digits.
        /// The driver must render the product version in this form but 
        /// can also append the product-specific version as a string 
        /// (for example, "04.01.0000 Rdb 4.1"). 
        ///</summary>
         public string DatabaseVersion
        {
            get
            {
                if (m_anyConnection == null)
                    throw (new InvalidOperationException("Connection is not open."));

                //Must wait until after connection is opened...
                switch (m_Type)
                {
                    case eConnectionType.ManagedSqlServer:
                        {
                            return ((SqlConnection)m_anyConnection).ServerVersion;
                        }
                    case eConnectionType.ManagedOracleServer:
                        {
                            return ((OracleConnection)m_anyConnection).ServerVersion;
                        }
                    case eConnectionType.Odbc:
                        {
                            return ((OdbcConnection)m_anyConnection).ServerVersion;
                        }
                    case eConnectionType.OleDb:
                        {
                            return ((OleDbConnection)m_anyConnection).ServerVersion;
                        }
                        break;

                    default:
                        {
                            return "";
                        }
                } //end switch
            } //end get
        }// end property

        /// <summary>
        /// Returns the type of the database connection used.
        /// </summary>
         public eDatabaseType DatabaseType
        {
            get
            {
                return m_DbType;
            }
        }

        /// <summary>
        /// Returns connection string.
        /// </summary>
         public string ConnectionString
        {
            get { return m_ConnectionString; }
            set
            {
                if (m_anyConnection != null)
                    throw new InvalidOperationException("You cannot set this property when connection is open.");
                m_ConnectionString = value;
                ConnectionStringChanged();
            }
        }

        /// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
        /// <returns>DbTransaction</returns>
        IDbTransaction IDbConnection.BeginTransaction() { return BeginTransaction(); }

        /// <summary>Riskmaster.Db.DbConnection.BeginTransaction begins a transaction on this connection.</summary>
        /// <returns>DbTransaction object which must be assigned to individual commands in order to enlist them into this transaction.</returns>
        /// <remarks>none</remarks>
        /// <example>No example available.</example>
         public DbTransaction BeginTransaction()
        {
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            return new DbTransaction(m_anyConnection.BeginTransaction());
        }

        /// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
        IDbTransaction IDbConnection.BeginTransaction(IsolationLevel il) { return BeginTransaction(il); }

        /// <summary>Riskmaster.Db.DbConnection.BeginTransaction wraps the native method of the same signature.</summary>
        /// <param name="il">See the ADO.Net documentation for details.</param>
        /// <returns>DbTransaction object.</returns>
        /// <remarks>none</remarks>
        /// <example>No example available.</example>
         public DbTransaction BeginTransaction(IsolationLevel il)
        {
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            return new DbTransaction(m_anyConnection.BeginTransaction(il));
        }

        /// <summary>
        /// Wraps the Native method of the same signature.
        /// </summary>
        /// <param name="databaseName">See the ADO.Net documentation.</param>
         public void ChangeDatabase(string databaseName)
        {
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            m_anyConnection.ChangeDatabase(databaseName);
        }

        /// <summary>
        /// Gets the time to wait while trying to establish a connection before terminating the attempt and generating an error.
        /// This is must be specified in the connection string and is read-only through this property.
        /// </summary>
         public int ConnectionTimeout
        {
            get
            {
                if (m_anyConnection == null)
                    throw (new InvalidOperationException("Connection is not open."));

                return m_anyConnection.ConnectionTimeout;
            }
        }

        /// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
        IDbCommand IDbConnection.CreateCommand() { return this.CreateCommand(); }
         internal DbCommand CreateExclusiveCommand()
        {
            DbCommand cmd = null;
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            cmd = new DbCommand(m_anyConnection.CreateCommand(), true);
            cmd.Connection = this;
            return cmd;

        }
        /// <summary>
        /// Creates and returns a Command object associated with the connection.
        /// </summary>
        /// <returns>DbCommand object with the connection property set.</returns>
         public DbCommand CreateCommand()
        {
            DbCommand cmd = null;
            if (m_anyConnection == null)
                throw (new InvalidOperationException("Connection is not open."));

            cmd = new DbCommand(m_anyConnection.CreateCommand());
            cmd.Connection = this;
            return cmd;
        }

        /// <summary>
        /// Gets the name of the current database or the database to be used once a connection is open.
        /// </summary>
         public string Database
        {
            get
            {
                if (m_anyConnection == null)
                    throw (new InvalidOperationException("Connection is not open."));
                return m_anyConnection.Database;
            }
        }

        /// <summary>
        /// Gets the current state of the connection.
        /// </summary>
         public ConnectionState State
        {
            get
            {
                if (m_anyConnection == null) //Not yet open.
                    return ConnectionState.Closed;	//throw(new InvalidOperationException("Connection is not open."));
                return m_anyConnection.State;
            }
        }


        //

        /// <summary>Riskmaster.Db.DbConnection.AsNative provides our internal provider classes
        ///  access to the ACTUAL original provider object.
        ///  NOT to be shared outside this assembly...</summary>
        /// <returns>Generic Interface on the Native provider connection object. </returns>
        /// <remarks>none</remarks>
         internal IDbConnection AsNative() { return m_anyConnection; }


        /// <summary>Riskmaster.Db.DbConnection.SQLInfoMessageRecieved is internal handler for SQLInfoMessage event.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>none</remarks>
         protected void SQLInfoMessageRecieved(object sender, SqlInfoMessageEventArgs e) { DbInfoMessageRecieved(sender, e); }

        /// <summary>Riskmaster.Db.DbConnection.OracleInfoMessageRecieved is internal handler for OracleInfoMessage event.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>none</remarks>
         protected void OracleInfoMessageRecieved(object sender, OracleInfoMessageEventArgs e) { DbInfoMessageRecieved(sender, e); }
        


         protected void OleDbInfoMessageReceived(object sender, OleDbInfoMessageEventArgs e) { DbInfoMessageRecieved(sender, e); }

        /// <summary>Riskmaster.Db.DbConnection.OdbcInfoMessageRecieved is internal handler for ODBCInfoMessage event.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>none</remarks>
         protected void OdbcInfoMessageRecieved(object sender, OdbcInfoMessageEventArgs e) { DbInfoMessageRecieved(sender, e); }



        /// <summary>Riskmaster.Db.DbConnection.DbInfoMessageRecieved is the end recipient of all the Provider specific event recieved handlers
        /// and is responsible for preparing the arguments and raising the DbInfoMessageRecievedEvent accordingly.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Event Implementation (Handler for all Provider types)</remarks>
         protected void DbInfoMessageRecieved(object sender, System.EventArgs e)
        {
            if (InfoMessage != null)
            {
                DbInfoMessageArgs args = new DbInfoMessageArgs(e);
                InfoMessage(this, args);
            }
        }
         
    }
   
}
