using System;
using System.Collections;
using System.Data;
using System.Globalization;
using Oracle.DataAccess.Client;

namespace Riskmaster.Db
{
	/// <summary>
	/// This Class wraps a parameter collection.  
	/// It uses an internal IParameterCollection interface from 
	/// whatever ParameterCollection type it is passed in it's constructor.
	/// The constructor is only available internally and is only used internally by the 
	/// DbCommand implementation.
	/// 
	/// If we need provider specific behaviors we can use the "as" or "is" keywords to case 
	/// out the actual object type behind the IParameterCollection interface.  Then using this strongly typed 
	///  actual object reference we can provide different provider specific implementation details.
	/// </summary>
	
	public class DbParameterCollection: IDataParameterCollection
	{

		IDataParameterCollection m_anyParameterCollection;
		
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.DbParameterCollection.</summary>
         /// <param name="anyParameterCollection"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         internal DbParameterCollection(IDataParameterCollection anyParameterCollection)
		{
			m_anyParameterCollection = anyParameterCollection;
		}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.this.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.this.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public object this[int idx]
		{
			get{return new DbParameter(m_anyParameterCollection[idx] as IDbDataParameter);}
			set
			{
				DbParameter p = value as DbParameter;
				if (p==null)
					throw new System.Exception(String.Format("DbParameterCollection recieved a {0} type parameter where a DbDataParameter type is required.",value.GetType().FullName));
				m_anyParameterCollection[idx] = p.AsNative();
			}
		}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.this.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.this.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public object this[string index] 
		{
			get{return new DbParameter(m_anyParameterCollection[IndexOf(index)] as IDbDataParameter);}
			set
			{
				DbParameter p = value as DbParameter;
				if (p==null)
					throw new System.Exception(String.Format("DbParameterCollection recieved a {0} type parameter where a DbDataParameter type is required.",value.GetType().FullName));
				m_anyParameterCollection[IndexOf(index)] = p.AsNative();
			}
		}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Count.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.Count.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public int Count	{get{return m_anyParameterCollection.Count;}}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IsReadOnly.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.IsReadOnly.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public bool IsReadOnly 	{get{return m_anyParameterCollection.IsReadOnly;}}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IsFixedSize.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.IsFixedSize.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public bool IsFixedSize		{get{return m_anyParameterCollection.IsFixedSize;}}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IsSynchronized.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.IsSynchronized.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public bool IsSynchronized		{get{return m_anyParameterCollection.IsSynchronized;}}
		//TODO Learn more about SyncRoot and whether we will eventually need it???
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.SyncRoot.</summary>
         /// <returns>Not specified.</returns>
         /// <value>TODO: Value of Riskmaster.Db.DbParameterCollection.SyncRoot.</value>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public object SyncRoot		{get{  throw new System.NotImplementedException("No Implementation available for SyncRoot property on DbParameterCollection");}}//return m_anyParameterCollection.SyncRoot;}}
		
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.GetEnumerator.</summary>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public IEnumerator GetEnumerator(){return new DbParameterCollectionEnumerator(this);}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.CopyTo.</summary>
         /// <param name="arr"></param>
         /// <param name="idx"></param>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void CopyTo(System.Array arr, int idx)
		{
			foreach(IDbDataParameter p in m_anyParameterCollection)
			{
				arr.SetValue( new DbParameter(p),idx);
				idx++;	
			}
		}


		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IList.Contains.</summary>
         /// <param name="o"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         bool IList.Contains(object o){return this.Contains(o.ToString());}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Contains.</summary>
         /// <param name="parameterName"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public bool Contains(string parameterName)
		{
			return(-1 != IndexOf(parameterName));
		}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IList.IndexOf.</summary>
         /// <param name="o"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         int IList.IndexOf(object o){return this.IndexOf(o.ToString());}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IndexOf.</summary>
         /// <param name="parameterName"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public int IndexOf(string parameterName)
		{
			int index = 0;
			foreach(IDataParameter item in m_anyParameterCollection) 
			{
				if (0 == _cultureAwareCompare(item.ParameterName, this.ToNativeParameterName(parameterName)))
				{
					return index;
				}
				index++;
			}
			return -1;
		}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Insert.</summary>
         /// <param name="idx"></param>
         /// <param name="value"></param>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void Insert(int idx, object value)
		{
			DbParameter p = value as DbParameter;
			if (p==null)
				throw new System.Exception(String.Format("DbParameterCollection recieved a {0} type parameter where a DbDataParameter type is required.",value.GetType().FullName));
			m_anyParameterCollection.Insert(idx, p.AsNative());
		}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.IList.Remove.</summary>
         /// <param name="value"></param>
         /// <remarks>none</remarks>
         void IList.Remove(object value)
		{
			DbParameter p = value as DbParameter;
			if (p==null)
				throw new System.Exception(String.Format("DbParameterCollection recieved a {0} type parameter where a DbDataParameter type is required.",value.GetType().FullName));
			this.Remove(p);
		}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Remove.</summary>
         /// <param name="p"></param>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void Remove(DbParameter p){this.RemoveAt(p.ParameterName);}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.RemoveAt.</summary>
         /// <param name="idx"></param>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void RemoveAt(int idx){m_anyParameterCollection.RemoveAt(idx);}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.RemoveAt.</summary>
         /// <param name="parameterName"></param>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void RemoveAt(string parameterName){m_anyParameterCollection.RemoveAt(IndexOf(parameterName));}

		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Add.</summary>
         /// <param name="value"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public  int Add(object value)
		{
			DbParameter p = value as DbParameter;
			if (p==null)
				throw new System.Exception(String.Format("DbParameterCollection recieved a {0} type parameter where a DbDataParameter type is required.",value.GetType().FullName));
			return m_anyParameterCollection.Add(p.AsNative());
		}
		
		// void IList.Clear(){this.Clear();}
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection.Clear.</summary>
         /// <remarks>none</remarks>
         /// <example>No example available.</example>
          public void Clear(){m_anyParameterCollection.Clear();}
		
		
         /// <summary>TODO: Summary of Riskmaster.Db.DbParameterCollection._cultureAwareCompare.</summary>
         /// <param name="strA"></param>
         /// <param name="strB"></param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         private static int _cultureAwareCompare(string strA, string strB)
		{
			return CultureInfo.CurrentCulture.CompareInfo.Compare(strA, strB, CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.IgnoreCase);
		}

		internal string ToNativeParameterName(string sName)
		{
			string sTemp  = sName;
			//Clear off Name Delimiters if Present (To ensure only one is used.)
			sTemp = sTemp.Replace(DbParameter.NameDelimiter,""); //Wrapper Tokens
			sTemp = sTemp.Replace("@",""); //SQL Server Native Tokens
			sTemp = sTemp.Replace("?",""); //ODBC Token
			sTemp = sTemp.Replace(":",""); //Oracle Server Native Tokens
			sTemp = sTemp.Replace("@",""); //DB2 Server Native Token
			
			if (m_anyParameterCollection is System.Data.SqlClient.SqlParameterCollection)
				return String.Format("@{0}",sTemp);
			if (m_anyParameterCollection is System.Data.Odbc.OdbcParameterCollection)
				return "?";
			if (m_anyParameterCollection is OracleParameterCollection) 
				return String.Format(":{0}",sTemp);
			throw new SystemException("Unknown Native Provider type.");

		}
		class DbParameterCollectionEnumerator : IEnumerator
		{
			private int index;
			private DbParameterCollection m_ParameterCollection = null;
			internal DbParameterCollectionEnumerator(DbParameterCollection parameterCollection)
			{
				m_ParameterCollection = parameterCollection;
				index=-1;
			}
			public bool MoveNext()	{ return (++index < m_ParameterCollection.Count);}

			public object Current
			{
				get
				{
					if (index == -1)
						throw new System.InvalidOperationException("Must use MoveNext before calling Current");

					if(index < m_ParameterCollection.Count)
						return m_ParameterCollection[index]; //Note wrapping Native to DbParam type is done in indexer of parent.
					else
						return null;
				}
			}
			public void Reset(){index = -1;}
		}
	}
}
