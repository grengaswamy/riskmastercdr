'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.0
'
' NAME: VBScript to detect the Java and Tomcat Installation paths and then pass 
' those values as parameters to the required batch files and command lines
' in order to support SSL for encrypted communications for Riskmaster X
'
' AUTHOR: CSC , Computer Sciences Corporation (CSC)
' DATE  : 03/14/2007
'
' COMMENT: 
'
'==========================================================================
Dim strComputer, strComputerValue, strPortValue, strReplaceValue
Dim strTomcatPath, strJavaPath, strTomcatISAPIPath
Dim strJavaSecurityCerts, strJavaKeyStore
Dim intIndex
Const strHTTPPrefix = "http://"
Const strHTTPSPrefix = "https://"

'Set the value as the local computer
strComputer = "."

'Call the function to retrieve the computer name
'strComputerName = GetComputerName()

'Get the string value for the JDK installation path
strJavaPath = GetJavaPath(strComputer)

'Get the string value for the Apache Tomcat 5.0 installation path
strTomcatInstallPathValue = GetTomcatPath(strComputer)

'Set the path to the Tomcat ISAPI Filter DLL
strTomcatISAPIPath = strTomcatInstallPathValue & "\bin\win32\i386" 

'Obtain the path to the Java Security file
strJavaSecurityCerts = strJavaPath & "\jre\lib\security\cacerts"
strJavaKeyStore = strJavaPath & "\jre\bin\keytool"

'Call the subroutine to import the SSL certificates
InstallJavaCertificates strTomcatInstallPathValue, strJavaSecurityCerts, strJavaKeyStore

'Prompt for the required search and replacement values
strPortValue = InputBox("Please enter the current path", "Current Path", LCase(GetComputerName()) & ":8080")
strReplaceValue = InputBox("Please enter the desired path", "Computer/DNS Name", LCase(GetComputerName()))

'Retrieve the index of the ":" in the string
intIndex = FindIndex(strPortValue, ":")

'Retrieve the name of the computer
strComputerValue = ExtractLeftString(strPortValue, intIndex)

'Call the subroutine to edit the configuration files
'EditConfigFiles strTomcatInstallPathValue
GetFilePaths strComputerValue, strPortValue, strReplaceValue


'Get the current script path location
Function GetPath()
	' Return path to the current script
	DIM path
	path = WScript.ScriptFullName  ' script file name
	GetPath = Left(path, InstrRev(path, "\"))
End Function

'Function to determine the index of a string search expression
'with a specified String
Function FindIndex(strSearch, strExpression)
	Dim lngFoundPos 

	'Find the index of the search expression within the string	
	lngFoundPos = InStr(1, strSearch, strExpression)

	'Return the index at which the specified expression was found
	FindIndex = lngFoundPos
End Function

'Function to extract the left most portion of a String
'based on a specified search index
Function ExtractLeftString(strSearch, intSearchIndex)
	Dim strExtract
	
	'A port number has not been specified
	'Example: Wish to do a replace for an existing SSL configuration
	If (intSearchIndex > 0) Then
		'Extract the required string
		strExtract = Left(strSearch, intSearchIndex - 1)
	Else
		'Configure the extracted string as the originally specified search string
		strExtract = strSearch
	End If
	
	'Return the extracted string
	ExtractLeftString = strExtract	
End Function

'Function to retrieve an array containing all of the relevant file paths
'that are required for editing
Sub GetFilePaths(strComputerValue, strPortValue, strReplaceValue)
	Dim arrFiles(6)
	Dim strScriptPath
	
	'Retrieve the path to the current script location
	strScriptPath = GetPath()
	
	REM SSO files
	arrFiles(0) = strScriptPath & "SSO\META-INF\cas_jaas.conf"
	
	REM oxf files
	arrFiles(1) = strScriptPath & "oxf\WEB-INF\web.xml"
	
	arrFiles(2) = strScriptPath & "oxf\WEB-INF\resources\config\global-parameters.xml"
	
	REM Jetspeed files
	REM Specify the path to the web.xml file
	'TODO: Replace both the http:// path and the :8080 path
	arrFiles(3) = strScriptPath & "jetspeed\WEB-INF\web.xml"
		
	REM Specify the path To the CSC_JetspeedSecurity.properties file
	arrFiles(4) = strScriptPath & "jetspeed\WEB-INF\conf\CSC_JetspeedSecurity.properties"
	
	REM Specify the path To the JSP template for editing
	arrFiles(5) = strScriptPath & "jetspeed\WEB-INF\templates\jsp\navigations\html\top_default.jsp"
	
	REM Open the PSML file for editing
	arrFiles(6) = strScriptPath & "jetspeed\WEB-INF\psml\role\user\html\default.psml"
	
	
	'Call the StringReplace function to replace the relevant values
	'in each of the files by iterating over all of them
	For Each arrFileName In arrFiles
		'Presentation Layer
		'Replace each instance of the computer name with its appropriate value
		StringReplaceSSL arrFileName, strPortValue, strReplaceValue
		
		'Check if the replacement value already contains http or https
		If (ContainsHttp(strReplaceValue)= False) Then
			'Perform the appropriate https replacement
			StringReplaceSSL arrFileName, "http://", "https://"
		End If

		'Business Layer
		StringReplaceSSL arrFileName, strComputerValue, strReplaceValue 
	Next
	
	'TODO: Open the Riskmaster.config file for editing by determining the path to the file
	'Notify the user that they must still modify the Riskmaster.config manually with the appropriate URLs
	MsgBox "Do not forget that you must still manually edit the Riskmaster.config file", vbOKOnly, "Riskmaster.config Reminder"
	
	'Return the array of files as the value of the Function
	'GetFilePaths = arrFiles
End Sub

'Perform a string replacement on a specified string in a file
Sub StringReplaceSSL(strFileName, strSearchValue, strReplaceValue)
	Dim objFSO, objFile, objReadFile
	Dim strFileText, strNewText
	
	'Declare and assign the necessary constants for file processing	
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8

	'Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Check if the file exists on the File System
	If (objFSO.FileExists(strFileName)) Then
	      'MsgBox strFileName & " exists."
	   Else
	      'MsgBox strFileName & " doesn't exist."
	 End If
	
	'Obtain a handle on the file for reading
	Set objReadFile = objFSO.GetFile(strFileName)
	
	'Begin reading the contents of the file
	Set objFile =objReadFile.OpenAsTextStream(ForReading)
	
	'Read in the entire contents of the file
	strFileText = objFile.ReadAll
	
	'Close the file from further reading
	objFile.Close

	'Get an updated string created by replacing the old string with the new string
	strNewText = Replace(strFileText, strSearchValue, strReplaceValue)
	
	'Open the file for writing
	Set objFile = objReadFile.OpenAsTextStream(ForWriting)

	'Write out the updated contents of the file
	objFile.WriteLine strNewText	

	'Close the file from further writing
	objFile.Close

	'Clean up
	Set objFile = Nothing
	Set objReadFile = Nothing
	Set objFSO = Nothing
End Sub

'Function to determine whether the specific string 
'already contains a value of Http
Function ContainsHttp(strValue)
	Const strHTTP = "http"
	
	'If the specified string contains the value of http or https
	If InStr(strValue, strHTTP) > 0 Then
		'Indicate that the string contains the Http prefix
		ContainsHttp = True
	Else
		'Indicate that the string does not contain the Http prefix
		ContainsHttp = False
	End If
End Function

'Function to determine whether or not the host OS is Server 2003
Function CheckOS()
	Dim objWMIService
	Dim blnIsServer2003
	
	'Initialize the boolean to a false
	blnIsServer2003 = False
	
	'Create the WMI Object to determine the OS
	Set objWMIService = GetObject("winmgmts:" _
	    & "{impersonationLevel=impersonate}!\\" _
	    & strComputer & "\root\cimv2")
	Set colOperatingSystems = objWMIService.ExecQuery _
	    ("Select * from Win32_OperatingSystem")
	For Each objOperatingSystem in colOperatingSystems
	    'Wscript.Echo objOperatingSystem.Caption & _
	    '"  " & objOperatingSystem.Version
	    strOS = objOperatingSystem.Caption
	Next
	
	'Determine if the Operating System is Windows Server 2003
	If (InStr(strOS, "Server 2003")) Then
		'Set the boolean to a value of true
		blnIsServer2003 = True
	End If
	
	'Clean up
	Set objWMIService = Nothing
	
	'Return the boolean variable as the value of the function
	CheckOS = blnIsServer2003
End Function

'Subroutine to retrieve the local computer name
Function GetComputerName()
	Dim SWBemlocator, objWMIService

	Set SWBemlocator = CreateObject("WbemScripting.SWbemLocator")
	Set objWMIService = SWBemlocator.ConnectServer(strComputer,"root\CIMV2",UserName,Password)
	Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48)
	For Each objItem in colItems
		'Obtain the NetBIOS name of the local computer
		strComputerName = objItem.Name
	Next
	
	'Clean up
	Set SWBemlocator = Nothing
	Set objWMIService = Nothing
	
	'Return the Host Computer Name as the value of the function
	GetComputerName = strComputerName
End Function

'Function to display a prompt to retrieve the host computer name
Function PromptForComputerName()
	Dim strComputerName, strNetBIOSName
	
	'Call the function to retrieve the Host computer name
	strComputerName = GetComputerName()

	'Display a prompt to the user for Computer Name information	
	strNetBIOSName = InputBox("Please enter the computer name, FQDN or IP Address of this web server", "Computer Name", strComputerName)

	'Return the Host Computer Name as the value of the function
	PromptForComputerName = strNetBIOSName
End Function

'Get the path to the Java directory
Function GetJavaPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg

	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Java 1.5
	strKeyPath = "SOFTWARE\JavaSoft\Java Development Kit\1.5"
	'Read out the JavaHome registry key
	strEntryName = "JavaHome"
	
	'Get the string value for the JDK Installation path
	objReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strEntryName,strValue
	
	'Clean up
	Set objReg = Nothing
	
	'Return the Java installation path as the value of the function
	GetJavaPath = strValue
End Function

'Get the path to the Tomcat directory
Function GetTomcatPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg

	'Create the Registry object for reading out from the registry
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Tomcat 5.0
	strTomcatPath = "SOFTWARE\Apache Software Foundation\Tomcat\5.0"
	
	'Read out the Tomcat 5.0 registry key
	strTomcatInstallPath = "InstallPath"
	
	'Get the string value for the Tomcat installation
	objReg.GetStringValue HKEY_LOCAL_MACHINE, strTomcatPath, strTomcatInstallPath, strTomcatInstallPathValue
	
	'Clean up
	Set objReg = Nothing
	
	'Return the Tomcat installation path as the value of the function
	GetTomcatPath = strTomcatInstallPathValue
End Function

'Subroutine to determine whether or not the user wishes to import
'the SSL Certificate into the Java Security Certificate store
Sub InstallJavaCertificates(strTomcatInstallPath, strJavaSecurityCerts, strJavaKeyStore)
	Dim intResponse 
	
	'Prompt to determine whether or not to import the certificate for Java
	intResponse = MsgBox("Do you wish to import the SSL certificate into Java at this time?", vbYesNo, "Import Java Certificate")
	
	'If the user wishes to import the SSL certificate into Java
	If (intResponse = vbYes) Then
		'Call the Subroutine to import the SSL certificate into Java
		ImportCertificates strTomcatInstallPath, strJavaSecurityCerts, strJavaKeyStore
	Else
		'Exit the subroutine
		Exit Sub
	End If
End Sub

'Subroutine to import the SSL certificates for Java Security Certificate store
Sub ImportCertificates(strTomcatInstallPath, strJavaSecurityCerts, strJavaKeyStore)
	Dim WshShell
	Dim strCmd
	Dim intResponse
	
	'Create the Windows Scripting Host Shell object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Enter the required key store password for importing the required certificates
	strKeyStorePwd = InputBox("Enter the key store password", "Java Key Store Password", "changeit")
	
	'Prompt for a response to determine if this is a VeriSign or similarly purchased SSL Cert
	'to distinguish it from SSL certificates which have been self-generated 
	intResponse = MsgBox("Is this a VeriSign or similarly purchased SSL Cert?", 4, "SSL Certificate Type")
	
	'Allow the user to enter the necessary aliases and certificate paths
	strAlias1 = InputBox("Enter the name of the certificate alias", "Certificate alias")
	strCertPath1 = InputBox("Enter the location of the SSL certificate", "IIS SSL Certificate")
	
	'Determine additional prompting based on MsgBox response
	If (intResponse = 7) Then 'Indicates a response of No
		strAlias2 = InputBox("Enter the name of the certificate authority alias", "Certificate Authority alias")
		strCertPath2 = InputBox("Enter the location of the SSL certificate for the certificate authority", "Certificate Authority SSL Certificate")
	End If
	
	'Concatenate the entire string of the command to be executed
	strCmd = """" & strTomcatInstallPath & "\webapps\cacert-mod.bat" & """"
	strCmd =  strCmd & " """ & strJavaKeyStore & """"
	strCmd = strCmd & " """ & strJavaSecurityCerts & """"
	strCmd = strCmd & " " & strKeyStorePwd & ""
	strCmd = strCmd & " """ & strAlias1 & """"
	strCmd = strCmd & " """ & strCertPath1 & """"
	strCmd = strCmd & " """ & strAlias2 & """"
	strCmd = strCmd & " """ & strCertPath2 & """"
	
	'Pass the value of the registry key path to the batch file
	'as well as the entire string of the command to be executed
	'WScript.Echo strCmd
	WshShell.Run strCmd, 1, True

	'Clean up
	Set WshShell = Nothing

End Sub

'Subroutine to update the registry for the Java Security Certificate store
Sub UpdateRegistry(strTomcatInstallPath)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg
	Dim strKeyPath
	
	strKeyPath = "SOFTWARE\Apache Software Foundation\Jakarta Isapi Redirector\1.0"
	
	'Create the WMI object to gain access to the registry
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
 	strComputer & "\root\default:StdRegProv")
 	
 	'Create the key for the ISAPI Redirector
 	objReg.CreateKey HKEY_LOCAL_MACHINE, strKeyPath
 	
 	'Set the string values required for redirection
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "extension_uri", "/jakarta/isapi_redirect.dll"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "log_file", strTomcatInstallPath & "\logs\iis_redirect.log"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "log_level", "emerg"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "worker_file", strTomcatInstallPath & "\conf\worker.properties"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "worker_mount_file", strTomcatInstallPath & "\conf\uriworkermap.properties"
 	
	'Clean up
	Set objReg = Nothing
End Sub

'Subroutine to edit the configuration files with the SSL path
Sub EditConfigFiles(strTomcatInstallPath)
	Dim WshShell
	Dim strCmd
	Dim strComputerName
	
	'Create the Windows Scripting host Shell object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Retrieve the Host Computer Name
	strComputerName = GetComputerName()
	
	'Display a message box to the user indicating the Host Computer Name
	MsgBox "The name of this computer is: " & strComputerName, 0, "Host computer name"
	
	'Concatenate the entire string of the command to be executed
	strCmd = """" & strTomcatInstallPath & "\webapps\InstallSSL.bat" & """"

	'Run the batch file which opens all of the necessary files for editing
	WshShell.Run strCmd, 1, True
	
	'Clean up
	Set WshShell = Nothing
End Sub

'This subroutine creates the required virtual directory for SSL installations
Sub CreateJakartaVirtualDirectory(strTomcatISAPIPath, strWebsiteName)
	Dim Root
	Dim Dir	
	
	On Error Resume Next

	'Connect to the local server
	Set Root = GetObject("IIS://localhost/W3SVC/" & GetIISWebsiteNumber(strWebsiteName) & "/Root")

 	'Error Handling
 	If (Err.Number <> 0) Then
 		MsgBox "Web site does not exist"
 		WScript.Quit
 	End If
	
	'Begin creating the Virtual Directory
	Set Dir = Root.Create("IIsWebVirtualDir", "jakarta")
	
	'Error Handling
 	If (Err.Number <> 0) Then
 		MsgBox "Virtual directory already exists"
 		WScript.Quit
 	End If
	
	'Set the Tomcat directory path
	Dir.Path = strTomcatISAPIPath

	'Set the IIS virtual directory permissions
	Dir.AccessRead = True
	Dir.AccessExecute = True

	'Specify the friendly display name for the Virtual Directory
	Dir.AppFriendlyName = "jakarta"
	
	'Save all the configuration information
	'and proceed to create the IIS virtual directory
	Dir.AppCreate(True)
	Dir.SetInfo
	
	If Err.Number <> 0 Then
		MsgBox Err.Description
	End If

	'Clean up
	Set Dir = Nothing
	Set Root = Nothing
	
End Sub

'Retrieves the website number for a specified website
Function GetIISWebsiteNumber(strWebsiteName)
	Dim IISObj, IISWebsite
	Dim intWebsiteNumber
	
	'Connect to the web server
	Set IISObj = GetObject("IIS://LocalHost/W3SVC")
	
	'Loop through all of the websites on the web server
	For Each IISWebsite in IISObj
        If (IISWebsite.Class = "IIsWebServer") Then
        	If (strWebsiteName = IISWebsite.ServerComment) Then
        		'Get the website number for the specified Web site
        		intWebsiteNumber = IISWebsite.Name
        	End If
        End If
    Next
    
    'Return the ID of the website as the value of the function
    GetIISWebsiteNumber = intWebsiteNumber
End Function

'Function to create and enable the Jakarta Web Server Extension
Function CreateWebSvcExtension(strTomcatISAPIPath)
	Dim WshShell
	Dim strCmd
	
	'Create the Windows Scripting Host Object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Create the command to be executed
	strCmd = "cscript %SYSTEMROOT%\system32\iisext.vbs /AddFile "
	strCmd = strCmd & """" & strTomcatISAPIPath & "\isapi_redirect.dll" & """ " 'Path to the DLL
	strCmd = strCmd & "1 " 'Enable the extension
	strCmd = strCmd & "jakarta " 'ID of the web service extension
	strCmd = strCmd & "1 " 'Deletable flag
	strCmd = strCmd & "jakarta " 'Short description of the web service extension
	
	'Execute the command to create and enable the Jakarta Web Service Extension
	WshShell.Run strCmd, 7, True
	
	'Clean up
	Set WshShell = Nothing
End Function

'Function to install the ISAPI filter on the relevant web site in IIS
Sub InstallISAPIFilter(strWebsiteName, strISAPIFilterName, strISAPIFilterPath)
	Dim WshShell
	Dim strCmd
	Dim intWebsiteNumber
	
	'Create the Windows Scripting Host Object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	intWebsiteNumber = GetIISWebsiteNumber(strWebsiteName)
	
	'Create the command to be executed
	strCmd = "cscript FiltTool.js -site:W3SVC/" & intWebsiteNumber &  " -name:" & strISAPIFilterName & " -dll:" & strISAPIFilterPath & " -action:add"
	
	'Execute the command to create the ISAPI filter on the relevant website
	WshShell.Run strCmd, 7, True
	
	'Clean up
	Set WshShell = Nothing
End Sub

'Function to install the ISAPI filter on the relevant web site in IIS
Sub AddISAPIFilter(strWebsiteName, strISAPIFilterName, strISAPIFilterDescription, strISAPIFilterPath)
	Dim FiltersObj 
	Dim FilterObj 
	Dim LoadOrder 
	Dim FilterName 
	Dim FilterPath 
	Dim FilterDesc 
	Dim intWebsiteNumber
	
	'Set the ISAPI Filter properties
	FilterName = strISAPIFilterName
	FilterPath = strISAPIFilterPath
	FilterDesc = strISAPIFilterDescription
	
	'Retrieve the Website Number for the selected web site
	intWebsiteNumber = GetIISWebsiteNumber(strWebsiteName)
	
	'Connect to the local computer for setting up the ISAPI filter
	Set FiltersObj = GetObject("IIS://localhost/W3SVC/" & intWebsiteNumber & "/Filters") 
	LoadOrder = FiltersObj.FilterLoadOrder 
	If LoadOrder <> "" Then 
	  LoadOrder = LoadOrder & "," 
	End If 

	'Configure the ISAPI Filter in IIS
	LoadOrder = LoadOrder & FilterName
	FiltersObj.FilterLoadOrder = LoadOrder 
	FiltersObj.SetInfo 
	
' 	Set FilterObj = FiltersObj.Create("IIsFilter", FilterName) 
' 	FilterObj.FilterPath = FilterPath 
' 	FilterObj.FilterDescription = FilterDesc 
' 	FilterObj.SetInfo 
End Sub

'Function to remove the ISAPI filter on the relevant web site in IIS
Sub RemoveISAPIFilter(strISAPIFilterName)
	 Dim objFilterProps, objFilters
    Dim strLoadOrder
    Dim intStartFilt

    Set objFilters = GetObject("IIS://LocalHost/W3SVC/Filters")
    strLoadOrder = objFilters.FilterLoadOrder
    If strLoadOrder <> "" Then
       If Right(strLoadOrder, 1) <> "," Then
          strLoadOrder = strLoadOrder & ","
       End If
       intStartFilt = InStr(strLoadOrder, strISAPIFilterName)
       strLoadOrder = Mid(strLoadOrder, 1, intStartFilt - 1) & _
          Mid(strLoadOrder, intStartFilt + Len(strFilterName) + 1, _
             Len(strLoadOrder))
       objFilters.FilterLoadOrder= strLoadOrder
       objFilters.SetInfo
       objFilters.Delete "IIsFilter", strISAPIFilterName
    End If
    Set objFilters = Nothing
End Sub