@ECHO OFF
:START
ECHO Check the required SSO files
notepad .\SSO\META-INF\cas_jaas.conf

notepad .\SSO\META-INF\auth-req.xml

ECHO Check the required Jetspeed files
notepad .\jetspeed\WEB-INF\web.xml

notepad .\jetspeed\WEB-INF\conf\CSC_JetspeedSecurity.properties

notepad .\jetspeed\WEB-INF\templates\jsp\navigations\html\top_default.jsp

notepad .\jetspeed\WEB-INF\psml\role\user\html\default.psml

ECHO Check the required oxf files
notepad .\oxf\WEB-INF\web.xml

notepad .\oxf\WEB-INF\resources\config\global-parameters.xml
ECHO This Batch file was created with the Easy Batch Builder from Octopussy Software.