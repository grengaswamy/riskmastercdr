IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_DeleteTables   ') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_DeleteTables   
go
CREATE PROCEDURE USP_DeleteTables   
AS  
BEGIN  

IF OBJECT_ID('deletetables') IS NULL                
begin
create table deletetables( tablename varchar(200))
end
truncate table deletetables
DECLARE @vSQL VARCHAR(1000);   
DECLARE @TableName VARCHAR(200);
DECLARE @EntityTableName VARCHAR(200);
declare tables cursor for SELECT distinct t.name AS table_name
FROM sys.tables AS t
INNER JOIN sys.columns c ON t.OBJECT_ID = c.OBJECT_ID
WHERE c.name in( 'trans_id','claim_id','event_id','funds_trans_id','comment_id','policy_id','OTHER_UNIT_ID','Unit_id','Property_id','SITE_ID')
open tables
fetch next from tables into @TableName
while @@fetch_status = 0
begin
    SET @vSQL='DELETE FROM '+@TableName;   
    EXEC (@vSQL);
    SELECT @vsql   
    --Exec ('Select * into '+@TableName+'_Temp from '+@TableName)
    insert into deletetables values (@TableName)
    fetch next from tables into @TableName   
end
CLOSE tables 
DEALLOCATE tables 
  SET @vSQL='DELETE FROM WPA_DIARY_ACT';   
  EXEC (@vSQL); 
  SELECT @vsql   
  SET @vSQL='DELETE FROM WPA_DIARY_ENTRY';   
  EXEC (@vSQL); 
  SELECT @vsql   
  SET @vSQL='DELETE FROM USER_PREF_XML';   
  EXEC (@vSQL); 
  SELECT @vsql   
    SET @vSQL='DELETE FROM FUNDS_TRANS_SPLIT_SUPP';   
  EXEC (@vSQL); 
  SELECT @vsql   
  SET @vSQL='DELETE FROM EMPLOYEE';
  EXEC (@vSQL)
  SELECT @vsql   
  SET @EntityTableName='''CLIENT''' + ','+ '''COMPANY'''+','+'''OPERATION'''+','+'''REGION'''+','+'''DIVISION'''+','
  +'''LOCATION'''+','+'''FACILITY'''+','+'''DEPARTMENT'''+','+ '''BANKS'''+','+ '''ADJUSTERS''' +','+ '''CASE_MANAGER'''+','
  + '''" "'''
  SET @vSQL='DELETE from ENTITY where ENTITY_TABLE_ID in ( select TABLE_ID from glossary where SYSTEM_TABLE_NAME NOT IN ('+@EntityTableName +'))'    
  EXEC (@vSQL);
  SELECT @vsql   
END  


 






 




