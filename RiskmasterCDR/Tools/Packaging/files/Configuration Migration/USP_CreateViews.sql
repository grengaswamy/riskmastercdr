IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_CreateViews   ') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_CreateViews   
go

CREATE PROCEDURE USP_CreateViews
(
@traindsnid int,
@proddsnid int
)
AS  
BEGIN
declare @count int
declare @maxcount int
declare @tmpviewid int
select @count=COUNT(*) from NET_VIEW_FORMS where DATA_SOURCE_ID=@proddsnid 
if @count > 0 
return -1

select  @maxcount=(max(view_id)+1) from NET_VIEWS
Select * into JURIS_VIEWS_Temp from JURIS_VIEWS where DATA_SOURCE_ID=@traindsnid  
Select * into NET_VIEW_FORMS_Temp from NET_VIEW_FORMS where DATA_SOURCE_ID=@traindsnid
Select * into NET_VIEW_LOGS_Temp from NET_VIEW_LOGS where DATA_SOURCE_ID=@traindsnid
Select * into NET_VIEWS_Temp from NET_VIEWS where DATA_SOURCE_ID=@traindsnid
Select * into NET_VIEWS_MEMBERS_Temp from NET_VIEWS_MEMBERS where DATA_SOURCE_ID=@traindsnid

declare tables cursor for SELECT View_ID from NET_VIEWS where
DATA_SOURCE_ID=@traindsnid
open tables
fetch next from tables into @tmpviewid
while @@fetch_status = 0
Begin

update NET_VIEW_FORMS set DATA_SOURCE_ID=@proddsnid,  View_Id= @maxcount where DATA_SOURCE_ID=@traindsnid and View_Id =@tmpviewid

update NET_VIEW_LOGS set DATA_SOURCE_ID=@proddsnid,  View_Id= @maxcount  where DATA_SOURCE_ID=@traindsnid and View_Id =@tmpviewid

update NET_VIEWS set DATA_SOURCE_ID=@proddsnid,  View_Id= @maxcount where DATA_SOURCE_ID=@traindsnid and View_Id =@tmpviewid

update NET_VIEWS_MEMBERS set DATA_SOURCE_ID=@proddsnid,  View_Id= @maxcount where DATA_SOURCE_ID=@traindsnid and View_Id =@tmpviewid

Set @maxcount = @maxcount+1
fetch next from tables into @tmpviewid  
end
CLOSE tables 
DEALLOCATE tables 

update JURIS_VIEWS set DATA_SOURCE_ID=@proddsnid where DATA_SOURCE_ID=@traindsnid

update NET_VIEW_FORMS set DATA_SOURCE_ID=@proddsnid where DATA_SOURCE_ID=@traindsnid and View_Id=0

update NET_VIEW_LOGS set DATA_SOURCE_ID=@proddsnid where DATA_SOURCE_ID=@traindsnid and View_Id =0

update NET_VIEWS set DATA_SOURCE_ID=@proddsnid where DATA_SOURCE_ID=@traindsnid and View_Id =0

update NET_VIEWS_MEMBERS set DATA_SOURCE_ID=@proddsnid where DATA_SOURCE_ID=@traindsnid and View_Id=0


insert into NET_VIEW_FORMS select * from NET_VIEW_FORMS_Temp
insert into JURIS_VIEWS select * from JURIS_VIEWS_Temp
insert into NET_VIEW_LOGS select * from NET_VIEW_LOGS_Temp
insert into NET_VIEWS select * from NET_VIEWS_Temp
insert into NET_VIEWS_MEMBERS select * from NET_VIEWS_MEMBERS_Temp

drop table JURIS_VIEWS_Temp
drop table NET_VIEW_FORMS_Temp
drop table NET_VIEW_LOGS_Temp
drop table NET_VIEWS_Temp
drop table NET_VIEWS_MEMBERS_Temp

END  


 






