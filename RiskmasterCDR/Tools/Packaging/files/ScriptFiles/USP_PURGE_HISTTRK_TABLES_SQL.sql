DECLARE @spSQL AS nVARCHAR(max)

IF EXISTS(SELECT TYPE, NAME FROM sys.objects WHERE type = 'P' AND name = 'USP_PURGE_HISTTRK_TABLES')
BEGIN
	DROP PROCEDURE USP_PURGE_HISTTRK_TABLES
END

	SET @spSQL = '
CREATE PROCEDURE USP_PURGE_HISTTRK_TABLES 
@sTableName VARCHAR(50),
@sToDate VARCHAR(20),
@sFromDate VARCHAR(20),
@out_err VARCHAR(500) OUTPUT 
		
AS
	DECLARE @iMinID INT,@iMaxID INT
	DECLARE @iLoopCount INT ,@sSql NVARCHAR(500),@dSql NVARCHAR(500)
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON
BEGIN TRY	
	SET @iLoopCount=0
	SET @sToDate = @sToDate +''235959'';
	
	SET @sSql = ''SELECT  @iMinID =MIN(RECORD_ID), @iMaxID = MAX(RECORD_ID) 
	FROM ''+ @sTableName + 
	'' WHERE DATE_TIMESTAMP <= ''+ @sToDate 
	IF @sFromDate <> '' ''
	BEGIN
		SET @sFromDate = @sFromDate + ''000000''
		SET @sSql = @sSql + '' AND DATE_TIMESTAMP >=''+ @sFromDate
	END
	EXECUTE sp_executesql @sSql,N''@iMinID INT OUTPUT ,@iMaxID INT OUTPUT'',@iMinID OUTPUT,@iMaxID OUTPUT
	SET @iLoopCount = @iMinID
	WHILE @iLoopCount<=@iMaxID
	BEGIN
		SET @dSql = ''DELETE FROM ''+  @sTableName + 
			'' WHERE DATE_TIMESTAMP <= ''+ @sToDate+''
		AND RECORD_ID BETWEEN ''+ CAST(@iLoopCount AS VARCHAR)+'' AND ''+ CAST((@iLoopCount + 4999) AS VARCHAR) 
		IF @sFromDate != '' '' 
		BEGIN
			SET @dSql = @dSql + ''AND DATE_TIMESTAMP >=''+ @sFromDate
		END	
		EXECUTE sp_executesql @dSql
		SET @iLoopCount = @iLoopCount + 5000
	END
END TRY
BEGIN CATCH
	SELECT	@out_err = error_message()
END CATCH '
	EXECUTE sp_executesql @spSQL
