USE [msdb]
GO

/****** Object:  Job [Expire RMX Session]    Script Date: 03/02/2010 16:27:24 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Expire RMX Session')
EXEC msdb.dbo.sp_delete_job @job_name = N'Expire RMX Session', @delete_unused_schedule=1
GO
USE [msdb]
GO
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 03/02/2010 16:27:24 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Expire RMX Session', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'(SESSIONDBUSERNAME)', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Log off idle users.  (session_timeout_seconds, default=300)]    Script Date: 03/02/2010 16:27:24 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Log off idle users.  (session_timeout_seconds, default=300)', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @session_timeout_seconds int

--SET THE DEFAULT TIMEOUT VALUE
SET @session_timeout_seconds=7200

/*********
BSB 03.25.2006 
Comment\Uncomment the following TSQL and modify job definition to 
log query results in order to troubleshoot session
expiration problems.
**********/
SELECT ''RMX Current Session Timeout is:'' + STR(@session_timeout_seconds) + '' seconds.''

--BEGIN THE SQL STATEMENT WHICH WILL PERFORM THE CLEANUP
SELECT * FROM SESSIONS_X_BINARY 
WHERE 
SESSION_ROW_ID IN 
	(SELECT SESSION_ROW_ID FROM SESSIONS 
	 WHERE DATEDIFF(SECOND,dbo.UDF_PROCESSDATE(LASTCHANGE),GETDATE()) > @session_timeout_seconds)

SELECT * FROM SESSIONS
WHERE DATEDIFF(SECOND,dbo.UDF_PROCESSDATE(LASTCHANGE),GETDATE()) > @session_timeout_seconds
--End of Troubleshooting Section


DELETE FROM SESSIONS_X_BINARY 
WHERE 
SESSION_ROW_ID IN 
	(SELECT SESSION_ROW_ID FROM SESSIONS 
	 WHERE DATEDIFF(SECOND,dbo.UDF_PROCESSDATE(LASTCHANGE),GETDATE()) > @session_timeout_seconds)

DELETE FROM SESSIONS
WHERE DATEDIFF(SECOND,dbo.UDF_PROCESSDATE(LASTCHANGE),GETDATE()) > @session_timeout_seconds', 
		@database_name=N'(DATABASE NAME)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Expire RMX Session', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=2, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100101, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(SERVER NAME)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


