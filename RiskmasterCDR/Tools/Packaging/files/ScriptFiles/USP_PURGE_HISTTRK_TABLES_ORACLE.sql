create or replace
PROCEDURE USP_PURGE_HISTTRK_TABLES (sTableName VARCHAR2,sFromDate VARCHAR2,sToDate VARCHAR2)
AS
BEGIN
        DECLARE 
        iMinID INT;
        sSqlMin VARCHAR(2000);
        sSqlMax VARCHAR(2000);
        iLoopCount INT;
        dSql VARCHAR(2000);
        iMaxID INT;
        ToDate Varchar2(14);
        FromDate varchar2(14);
        BEGIN
        iLoopCount:=0;
        ToDate := sToDate || '235959';
        FromDate := sFromDate || '000000';
        sSqlMin := 'SELECT   MIN(RECORD_ID)  FROM '
                || sTableName || ' WHERE DATE_TIMESTAMP <='  ||  ToDate ;
                
        IF sFromDate <> ' ' THEN
          sSqlMin := sSqlMin   || ' AND DATE_TIMESTAMP >='  || FromDate;
        END IF;
       
        EXECUTE IMMEDIATE sSqlMin into iMinID;
        
        sSqlMax := 'SELECT MAX(RECORD_ID) FROM '
                || sTableName || ' WHERE DATE_TIMESTAMP <='  ||  ToDate ;
               
        IF sFromDate <> ' ' THEN
          sSqlMax := sSqlMax   || ' AND DATE_TIMESTAMP >='  || FromDate;
        END IF;
        
        EXECUTE IMMEDIATE sSqlMax into iMaxID;

        iLoopCount := iMinID;
          BEGIN
            WHILE iLoopCount<=iMaxID LOOP
            
                    dSql := 'DELETE FROM '  ||  sTableName   || ' WHERE DATE_TIMESTAMP <='  ||  ToDate   ||' AND RECORD_ID BETWEEN '  || Cast(iLoopCount as Varchar)  || ' AND '|| Cast((iLoopCount   + 4999) as Varchar);
                    IF sFromDate != ' ' THEN
                        dSql := dSql   || ' AND DATE_TIMESTAMP >='  || FromDate;
                    END IF;
                    
                    EXECUTE IMMEDIATE dSql;
                    iLoopCount := iLoopCount   || 5000;
             END LOOP ;     
            
          END;
       
        END;
END USP_PURGE_HISTTRK_TABLES;
        

