
CREATE TABLE ASYNC_TRACKER
(
  ASYNCID               VARCHAR2(50 BYTE),
  ASYNCSTATUS           NUMBER(10),
  ASYNCSTATE            BLOB,
  PERCENTAGECOMPLETION  FLOAT(126)
)
GO
CREATE UNIQUE INDEX PK_ASYNC_TRACKER ON ASYNC_TRACKER
(ASYNCID)
GO
ALTER TABLE ASYNC_TRACKER 
ADD (
  CONSTRAINT PK_ASYNC_TRACKER
 PRIMARY KEY
 (ASYNCID))
GO
CREATE TABLE CUSTOMIZE
(
  ID         NUMBER(10),
  FOLDER     VARCHAR2(100 BYTE),
  FILENAME   VARCHAR2(100 BYTE),
  TYPE       NUMBER(5),
  IS_BINARY  NUMBER(1),
  CONTENT    CLOB
)
GO
CREATE UNIQUE INDEX PK_CUSTOMIZE ON CUSTOMIZE
(ID)
GO
ALTER TABLE CUSTOMIZE 
ADD (
  CONSTRAINT PK_CUSTOMIZE
 PRIMARY KEY
 (ID))
GO
CREATE TABLE SESSIONS
(
  SESSION_ROW_ID  NUMBER(10),
  SID             VARCHAR2(40 BYTE),
  LASTCHANGE      VARCHAR2(14 BYTE),
  DATA            CLOB,
  USERDATA1       NUMBER(10),
  SEARCH_XML      CLOB
)
GO
CREATE UNIQUE INDEX PK_SESSIONS ON SESSIONS(SID)
GO
ALTER TABLE SESSIONS 
ADD (
  CONSTRAINT PK_SESSIONS
 PRIMARY KEY
 (SID))
GO
CREATE TABLE SESSIONS_X_BINARY
(
  SESSION_X_BIN_ROW_ID  NUMBER(10),
  SESSION_ROW_ID        NUMBER(10),
  BINARY_TYPE           VARCHAR2(100 BYTE),
  BINARY_NAME           VARCHAR2(100 BYTE),
  BINARY_VALUE          BLOB
)
GO
CREATE UNIQUE INDEX PK_SESSIONS_X_BINARY 
	ON SESSIONS_X_BINARY
(SESSION_X_BIN_ROW_ID, 
SESSION_ROW_ID)
GO
ALTER TABLE SESSIONS_X_BINARY 
ADD (
  CONSTRAINT PK_SESSIONS_X_BINARY
 PRIMARY KEY
 (SESSION_X_BIN_ROW_ID, 
SESSION_ROW_ID))
GO
CREATE TABLE SESSION_IDS
(
  SYSTEM_TABLE_NAME  VARCHAR2(80 BYTE),
  NEXT_UNIQUE_ID     NUMBER(10)
)
GO
CREATE UNIQUE INDEX PK_SESSION_IDS ON SESSION_IDS(SYSTEM_TABLE_NAME)
ALTER TABLE SESSION_IDS 
ADD (
  CONSTRAINT PK_SESSION_IDS
 PRIMARY KEY
 (SYSTEM_TABLE_NAME))
GO
--INSERT DATA INTO THE SESSION DATABASE
DECLARE SESSION_ID_RECORDS int;
BEGIN
    SELECT COUNT(*) INTO SESSION_ID_RECORDS FROM SESSION_IDS;
 
    IF SESSION_ID_RECORDS = 0 THEN
        INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS_X_BINARY',1);
        INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS',1);
        INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('CUSTOMIZE',1);
        --DBMS_OUTPUT.PUT_LINE ( SESSION_ID_RECORDS ); 
    END IF;     
END;
GO
--CREATE FUNCTIONS REQUIRED FOR RUNNING RMX EXPIRE WEB SESSION
CREATE OR REPLACE FUNCTION DATEDIFF (DATE_TYPE IN VARCHAR2, DATE1 IN DATE, DATE2 IN DATE) RETURN NUMBER IS
/******************************************************************************
   NAME:       DATEDIFF
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/17/2007          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     DATEDIFF
      Sysdate:         1/17/2007
      Date and Time:   1/17/2007, 9:50:18 AM, and 1/17/2007 9:50:18 AM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
     DATE_DIFFERENCE NUMBER;
	 
BEGIN
	 IF UPPER(DATE_TYPE) = 'Q' THEN
	 	DATE_DIFFERENCE := ((TO_CHAR(DATE2, 'yyyy') - TO_CHAR(DATE1, 'yyyy')) * 4)
						+ (TO_CHAR(DATE2, 'q') - TO_CHAR(DATE1, 'q'));
	ELSE
		SELECT (DATE2 - DATE1) * DECODE (UPPER(DATE_TYPE),
			   'SS', 24*60*60, 
			   'MI', 24*60,
			   'HH', 24,
			   		 NULL)
		INTO DATE_DIFFERENCE
		FROM DUAL;							
	 END IF;
	 
	 RETURN(DATE_DIFFERENCE);
END DATEDIFF;
GO

CREATE OR REPLACE FUNCTION UDF_PROCESSDATE (LASTCHANGE IN VARCHAR2) RETURN DATE IS
/******************************************************************************
   NAME:       UDF_PROCESSDATE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/16/2007          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     UDF_PROCESSDATE
      Sysdate:         1/16/2007
      Date and Time:   1/16/2007, 6:12:51 PM, and 1/16/2007 6:12:51 PM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
	--DECLARE ALL REQUIRE VARIABLES FOR PROCESSING THE DATE STRING
	LASTCHANGEDATE varchar2(50);
	YEARDATE varchar2(4);
	MONTHDATE varchar2(2);
	DAYDATE varchar2(2);
	HOURDATE varchar2(2);
	MINUTES varchar2(2);
	SECONDS varchar2(2);
	SESSIONDATE date;

BEGIN

	--SET UP ALL THE REQUIRED VALUES FOR CONSTRUCTING THE PROPER DATETIME STRING
	SELECT SUBSTR(LASTCHANGE, 1, 4) INTO YEARDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 5,2) INTO MONTHDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 7,2) INTO DAYDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 9,2) INTO HOURDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 11,2) INTO MINUTES FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 13,2) INTO SECONDS FROM DUAL;

	--CHECK IF THE SECONDS FIELD IS EMPTY TO PROVIDE
	--BACKWARDS COMPATIBILITY WITH THE OLDER WEBFARMSESSION DATABASE
	IF SECONDS = '' THEN
	   SECONDS := '00';
	END IF;
			
	--ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH12:MI:SS';

	--CONCATENATE THE DATE TIME STRING SO THAT IT CAN BE PROPERLY CAST TO A DATE TIME DATA TYPE
	LASTCHANGEDATE := YEARDATE || '-' || MONTHDATE || '-' || DAYDATE || ' ' || HOURDATE || ':' || MINUTES || ':' || SECONDS;

	SELECT TO_DATE(LASTCHANGEDATE,'YYYY-MM-DD HH24:MI:SS') INTO SESSIONDATE FROM DUAL;
	
	RETURN(SESSIONDATE);
	


END UDF_PROCESSDATE;
GO

CREATE OR REPLACE PROCEDURE EXPIRE_RMX_SESSION (SESSION_TIMEOUT_SECONDS NUMBER) IS SESSIONS_TIMED_OUT NUMBER;
/******************************************************************************
   NAME:       EXPIRE_RMX_SESSION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/17/2007          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     EXPIRE_RMX_SESSION
      Sysdate:         1/17/2007
      Date and Time:   1/17/2007, 10:15:02 AM, and 1/17/2007 10:15:02 AM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
CURRENT_SYSTEM_DATE DATE;
 

BEGIN
	 --SET A DEFAULT VALUE OF 2 HRS FOR THE SESSION TIMEOUT
	 --SESSION_TIMEOUT_SECONDS := 7200;
	 
	 --ASSIGN THE CURRENT SYSTEM DATE TO A VARIABLE
	 SELECT SYSDATE INTO CURRENT_SYSTEM_DATE FROM DUAL;
	 
	 
	 --BEGIN THE SQL STATEMENT WHICH WILL PERFORM THE CLEANUP
	 /*
	  SELECT * FROM SESSIONS_X_BINARY 
	 WHERE 
	 SESSION_ROW_ID IN 
	 	(SELECT SESSION_ROW_ID FROM SESSIONS WHERE DATA IS NULL OR
			(DATEDIFF('SS',UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS));
			
	 SELECT * FROM SESSIONS WHERE DATA IS NULL OR
 	 (DATEDIFF('SS',UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS);
	 */


	 --DELETE THE EXPIRED SESSIONS
	 DELETE FROM SESSIONS_X_BINARY 
	 WHERE 
	 SESSION_ROW_ID IN 
	 (SELECT SESSION_ROW_ID FROM SESSIONS WHERE DATA IS NULL OR
	 (DATEDIFF('SS', UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS));

	 DELETE FROM SESSIONS WHERE DATA IS NULL OR
	 (DATEDIFF('SS', UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS);
	
END EXPIRE_RMX_SESSION;
GO

CREATE OR REPLACE PROCEDURE INSERT_CUSTOMIZE IS
customize_data CLOB;
/******************************************************************************
   NAME:       INSERT_CUSTOMIZE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        08/14/2007          1. Modified for RMX R3 Support

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     INSERT_CUSTOMIZE
      Sysdate:         08/14/2007
      Date and Time:   08/14/2007, 12:53:47 PM, and 08/14/2007 12:53:47 PM

******************************************************************************/
BEGIN
	customize_data := '<customize_captions>
				<RMAdminSettings title="System Customization">
					<Captions title="Captions/Messages">
						<CompanyName default="***" title="Company Name:">THE COMPANY</CompanyName>
						<AppTitle default="Riskmaster.Net" title="Application Title:">Riskmaster.Net</AppTitle>
						<ReportAppTitle default="Sortmaster" title="Application Title:">Sortmaster</ReportAppTitle>
						<AppCopyright default=") 2004 by CSC, All Rights Reserved." title="Application Title:">) 2004 by CSC, All Rights Reserved.</AppCopyright>
						<ErrContact default="For assistance please consult the System Administrator." title="Contact on Error:">For assistance please consult the System Administrator.</ErrContact>
						<AlertExistingRec default="You are working on a new record and this functionality is available for existing records only. Please save the data and try again." title="Alert Existing Record Required:">You are working on a new record and this functionality is available for existing records only. Please save the data and try again.</AlertExistingRec>
					</Captions>
					<Paths title="Paths">
						<OverrideDocPath default="***" title="Override Documents Path:"></OverrideDocPath>
					</Paths>
				</RMAdminSettings>
			</customize_captions>';

	--INSERT THE PREVIOUSLY CONTRUCTED XML STRING INTO THE TABLE
	INSERT INTO CUSTOMIZE
	   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
	 VALUES
	   (0, 'customize_captions', 0, 0, customize_data);

   customize_data := '<customize_settings>
	<RMAdminSettings title="System Customization">
		<TextAreaSize title="Text Area Size">
			<TextML title="TextMl">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</TextML>
			<FreeCode title="Freecode">
				<Width default="30">30</Width>
				<Height default="8">8</Height>
			</FreeCode>
			<ReadOnlyMemo title="Readonly Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</ReadOnlyMemo>
			<Memo title="Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</Memo>
			<HtmlText>
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</HtmlText>
		</TextAreaSize>
		<Buttons title="Buttons">
			<Document title="Documents">-1</Document>
			<Search title="Search">-1</Search>
			<File title="Files">-1</File>
			<Diary title="Diaries">-1</Diary>
			<Report title="Reports">-1</Report>
		</Buttons>
		<Other>
			<Soundex title="Choose Soundex on Searches">-1</Soundex>
			<ShowName title="Show User Name on Menu">-1</ShowName>
			<ShowLogin title="Show Login on Menu">-1</ShowLogin>
			<SaveShowActiveDiary title="Save Show Active Diary">-1</SaveShowActiveDiary>
		</Other>
		<Funds title="Funds Page Settings">
			<OFAC title="OFAC Check:">-1</OFAC>
		</Funds>
	</RMAdminSettings>
</customize_settings>';

   --INSERT THE PREVIOUSLY CONTRUCTED XML STRING INTO THE TABLE
   INSERT INTO CUSTOMIZE
	   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
	 VALUES
	   (1, 'customize_settings', 0, 0, customize_data);
	   
    customize_data := '<customize_reports>
				<RMAdminSettings title="System Customization">
					<ReportEmail title="Report Email Settings">
						<From title="Override From:"/>
						<FromAddr title="Override From Address:"/>
					</ReportEmail>
					<ReportMenu title="Report Menu Links">
						<BILabel title="Business Intelligence" default="Business Intelligence" value="-1" />
						<ReportLabel title="Std Reports Queue" default="Std Reports Queue" value="-1">
							<AvlReports title="Available Reports:">-1</AvlReports>
							<JobQueue title="Job Queue:">-1</JobQueue>
							<NewReport title="Post New Report:">-1</NewReport>
							<DeleteReport title="Delete Report:">-1</DeleteReport>
							<ScheduleReport title="Schedule Reports:">-1</ScheduleReport>
							<ViewSchedReport title="View Scheduled Reports:">-1</ViewSchedReport>
						</ReportLabel>
						<SMLabel title="Std Reports Designers" default="Std Reports Designers" value="-1">
							<Designer title="Designer:">-1</Designer>
							<DraftReport title="Draft Reports:">-1</DraftReport>
							<PostDraftRpt title="Post Draft Reports:">-1</PostDraftRpt>
						</SMLabel>
						<ExecSummLabel title="Exec. Summary" default="Exec. Summary" value="-1">
							<Configuration title="Configuration:">-1</Configuration>
							<Claim title="Claim:">-1</Claim>
							<Event title="Event:">-1</Event>
						</ExecSummLabel>
						<OtherLabel title="Other Reports" default="Other Reports" value="-1">
							<OSHA300 title="OSHA 200:">-1</OSHA300>
							<OSHA301 title="OSHA 301:">-1</OSHA301>
							<OSHA300A title="OSHA 300A:">-1</OSHA300A>
							<OSHASharpsLog title="OSHA Sharps Log:">-1</OSHASharpsLog>
							<DCCLabel title="DCC" default="DCC">-1</DCCLabel>
						</OtherLabel>
						<ReportQueue title="Report Queue Buttons">
							<Archive title="OSHA 200:">-1</Archive>
							<Email title="OSHA 200:">-1</Email>
							<Delete title="OSHA 200:">-1</Delete>
						</ReportQueue>
					</ReportMenu>
				</RMAdminSettings>
			</customize_reports>';

   --INSERT THE PREVIOUSLY CONTRUCTED XML STRING INTO THE TABLE
   INSERT INTO CUSTOMIZE
    (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
    VALUES
   	(2, 'customize_reports', 0, 0, customize_data);
	
	customize_data := '<customize_search>
	<RMAdminSettings title="System Customization">
		<Search title="Search Links">
			<Claim title="Claims">-1</Claim>
			<Event title="Events">-1</Event>
			<Employee title="Employees">-1</Employee>
			<Entity title="Entities">-1</Entity>
			<Vehicle title="Vehicles">-1</Vehicle>
			<Policy title="Policies">-1</Policy>
			<Fund title="Funds">-1</Fund>
			<Patient title="Patients">-1</Patient>
			<Physician title="Physicians">-1</Physician>
			<LeavePlan title="LeavePlans">-1</LeavePlan>
		</Search>
	</RMAdminSettings>
</customize_search>';
	
  --INSERT THE PREVIOUSLY CONTRUCTED XML STRING INTO THE TABLE
  INSERT INTO CUSTOMIZE
   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
    VALUES
   (3, 'customize_search', 0, 0, customize_data);
END INSERT_CUSTOMIZE;
GO

--CALL THE PROCEDURE TO INSERT DEFAULT VALUES INTO THE CUSTOMIZE TABLE
BEGIN
	INSERT_CUSTOMIZE;	
END;
GO

--REMOVE ANY EXISTING EXPIRE WEB SESSION JOBS IN ORACLE
BEGIN 
  SYS.DBMS_JOB.REMOVE(999);
COMMIT;
END;
GO

--CREATE THE JOB IN ORACLE
--AND SPECIFY THAT IT SHOULD RUN STARTING TODAY
--AND THEN RUN EVERY 2 HOURS SUBSEQUENT TO THE INITIAL RUN

--NOTE: THIS JOB CAN TESTED AND RUN THROUGH THE FOLLOWING COMMAND:
--EXEC DBMS_JOB.RUN(<JOB NUMBER>);  EX: EXEC DBMS_JOB.RUN(999);

BEGIN
DBMS_JOB.SUBMIT(
   job => 999, 
   what => 'EXPIRE_RMX_SESSION(7200);', 
   next_date => SYSDATE,
   interval => 'trunc(SYSDATE+2/24,''HH'')');
COMMIT;
END;
GO
