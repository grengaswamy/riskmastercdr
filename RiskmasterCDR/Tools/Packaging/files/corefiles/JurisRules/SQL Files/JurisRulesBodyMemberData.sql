;File Name JurisRulesBodyMemberData.sql
;
;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;#################################################################################
;#################################################################################
;*********************************************************************************
;California
[ASSIGN %%1=ADD_JURISDICTION(CA,California,0,0,0,0,0,0)]
;as of 09/01/2004 California does not use body member jtodd22
;*********************************************************************************
;Connecticut
[ASSIGN %%1=ADD_JURISDICTION(CT,Connecticut,0,0,0,0,0,0)]
;*********************************************************************************
;Delaware
[ASSIGN %%1=ADD_JURISDICTION(DE,Delaware,0,0,0,0,0,0)]
;*********************************************************************************
;FLorida
[ASSIGN %%1=ADD_JURISDICTION(FL,FLorida,0,0,0,0,0,0)]
;*********************************************************************************
;Hawaii
[ASSIGN %%1=ADD_JURISDICTION(HI,Hawaii,0,0,0,0,0,0)]
;*********************************************************************************
;Idaho
[ASSIGN %%1=ADD_JURISDICTION(ID,Idaho,0,0,0,0,0,0)]
;*********************************************************************************
;Illinois
[ASSIGN %%1=ADD_JURISDICTION(IL,Illinois,0,0,0,0,0,0)]
;*********************************************************************************
;Indiana
[ASSIGN %%1=ADD_JURISDICTION(IN,Indiana,0,0,0,0,0,0)]
;******************************************************************************
;Kentucky
[ASSIGN %%1=ADD_JURISDICTION(KY,Kentucky,0,0,0,0,0,0)]
;*********************************************************************************
;Louisiana
[ASSIGN %%1=ADD_JURISDICTION(LA,Louisiana,0,0,0,0,0,0)]
;*********************************************************************************
;Massachusetts
[ASSIGN %%1=ADD_JURISDICTION(MA,Massachusetts,0,0,0,0,0,0)]
;*********************************************************************************
;Maryland
[ASSIGN %%1=ADD_JURISDICTION(MD,Maryland,0,0,0,0,0,0)]
;*********************************************************************************
;Maine
[ASSIGN %%1=ADD_JURISDICTION(ME,Maine,0,0,0,0,0,0)]
;*********************************************************************************
;Michigan
[ASSIGN %%1=ADD_JURISDICTION(MI,Michigan,0,0,0,0,0,0)]
;*********************************************************************************
;Minnesota
[ASSIGN %%1=ADD_JURISDICTION(MN,Minnesota,0,0,0,0,0,0)]
;*********************************************************************************
;Mississippi
[ASSIGN %%1=ADD_JURISDICTION(MS,Mississippi,0,0,0,0,0,0)]
;*********************************************************************************
;Missouri
[ASSIGN %%1=ADD_JURISDICTION(MO,Missouri,0,0,0,0,0,0)]
;*********************************************************************************
;Montana
[ASSIGN %%1=ADD_JURISDICTION(MT,Montana,0,0,0,0,0,0)]
;*********************************************************************************
;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;*********************************************************************************
;North Dakota
[ASSIGN %%1=ADD_JURISDICTION(ND,North Dakota,0,0,0,0,0,0)]
;*********************************************************************************
;Nebraska
[ASSIGN %%1=ADD_JURISDICTION(NE,Nebraska,0,0,0,0,0,0)]
;*********************************************************************************
;New Hampshire
[ASSIGN %%1=ADD_JURISDICTION(NH,New Hampshire,0,0,0,0,0,0)]
;*********************************************************************************
;New Jersey
[ASSIGN %%1=ADD_JURISDICTION(NJ,New Jersey,0,0,0,0,0,0)]
;*********************************************************************************
;South Dakota 62-4-6.   Additional compensation for specific bodily injuries
[ASSIGN %%1=ADD_JURISDICTION(SD,South Dakota,0,0,0,0,0,0)]
 
;For injuries in the following schedule, an employee shall receive in addition to compensation provided by �� 62-4-1, 62-4-3, and 62-4-5.1, compensation for the following further periods, subject to the limitations as to rate and amounts fixed in � 62-4-3, for the specific medical impairment herein mentioned, but may not receive any compensation under any other provisions of this title: 
;
;For the loss of a thumb, or the permanent and complete loss of its use, 0,50,0,0 
;For the loss of a first finger, commonly called the index finger, or the permanent and complete loss of its use, 0,35,0,0
;For the loss of a second finger, or the permanent and complete loss of its use, 0,30,0,0 
;For the loss of a third finger, or the permanent and complete loss of its use, 0,20,0,0 
;For the loss of fourth finger, commonly called the little finger, or the permanent and complete loss of its use, 0,15,0,0
;The loss of the first phalange of the thumb, or of any finger, shall be considered to be equal to the loss 
;of one-half of such thumb or finger and compensation shall be one-half of the amounts specified
; compensation for the loss of less than the first phalange of a thumb or finger shall be in such
; proportion as the partial loss bears to the loss of the first phalange; 
;The loss of more than one phalange, or fraction thereof, shall be considered
; as the loss of the entire finger or thumb, but in no case shall the amount received
; for more than one finger exceed the amount provided in this schedule for the loss of a hand; 
;For the loss of a great toe, thirty weeks of compensation; 
;For the loss of one or more of the toes other than the great toe, ten weeks, and for the additional loss of one or more toes other than the great toe, an additional ten weeks of compensation; 
;The loss of the first phalange of any toe shall be considered to be equal to the loss of one-half of such toe, and compensation shall be one-half the amount above specified; compensation for the loss of less than the first phalange of a toe shall be in such proportion as the partial loss bears to the loss of the first phalange; 
;The loss of more than one phalange, or fraction thereof, shall be considered as the loss of the entire toe; 
;For the loss of a hand, or the permanent and complete loss of its use,0,150 
; For the loss of an arm, or the permanent and complete loss of its use,0,200
;Amputation of the arm below the elbow shall be considered the loss of a hand, if enough of the forearm remains to permit the use of an effective artificial member; otherwise it shall be considered as the loss of an arm; 
; For the loss of a foot, or the permanent and complete loss of its use,0,125 
;For the loss of a leg, or the permanent and complete loss of its use,0,160 
;Amputation of the leg below the knee shall be considered as the loss of a foot, if enough of the lower leg remains to permit the use of an effective artificial member; otherwise it shall be considered as the loss of a leg; 
;For the loss of the sight of an eye,0,150
;For the permanent and complete loss of hearing in one ear,0,50
;For the permanent and complete loss of hearing in both ears, 0,150
;For permanent partial disability resulting from injury to the back, 
;compensation for that proportion of three hundred twelve weeks which is represented 
;by the percentage that such permanent partial disability bears to the body as a whole; 
;In all cases in the above schedule under this section, if the medical impairment is partial and permanent, the compensation shall bear such relation to the maximum amount for complete and permanent loss as defined in this section as the medical impairment bears to the complete loss; 
;The loss of both hands or both arms, or both feet, or both legs, or both eyes or of any two thereof, 
;or complete and permanent paralysis, or total and permanent loss of mental faculties, 
;or any other injury which totally incapacitates the employee 
;from working at any occupation which brings him an income, 
;shall constitute total disability, to be compensated according 
;to the compensation fixed by � 62-4-7. These specific cases of total and permanent 
;disability shall not be construed as excluding other cases of total or permanent disability; 
;For permanent disfigurement, or permanent disability resulting from injury to 
;any part of the body not hereinbefore listed, compensation for that portion of 
;three hundred twelve weeks which is represented by the percentage that such 
;permanent partial disability or permanent disfigurement bears to the body as a whole. 

'*********************************************************************************
;Nevada
[ASSIGN %%1=ADD_JURISDICTION(NV,Nevada,0,0,0,0,0,0)]
;*********************************************************************************
;Ohio
[ASSIGN %%1=ADD_JURISDICTION(OH,Ohio,0,0,0,0,0,0)]
;*********************************************************************************
;Oregon
[ASSIGN %%1=ADD_JURISDICTION(OR,Oregon,0,0,0,0,0,0)]
;*********************************************************************************
;Pennsylvania
[ASSIGN %%1=ADD_JURISDICTION(PA,Pennsylvania,0,0,0,0,0,0)]
;*********************************************************************************
;Rhode Island
[ASSIGN %%1=ADD_JURISDICTION(RI,Rhode Island,0,0,0,0,0,0)]
;*********************************************************************************
;South Carolina
[ASSIGN %%1=ADD_JURISDICTION(SC,South Carolina,0,0,0,0,0,0)]
;*********************************************************************************
;South Dakota
[ASSIGN %%1=ADD_JURISDICTION(SD,South Dakota,0,0,0,0,0,0)]
;*********************************************************************************
;Tennessee
[ASSIGN %%1=ADD_JURISDICTION(TN,Tennessee,0,0,0,0,0,0)]
;*********************************************************************************
;Texas
[ASSIGN %%1=ADD_JURISDICTION(TX,Texas,0,0,0,0,0,0)]
;*********************************************************************************
;Virginia
[ASSIGN %%1=ADD_JURISDICTION(VA,Virginia,0,0,0,0,0,0)]
;*********************************************************************************
;Vermont
[ASSIGN %%1=ADD_JURISDICTION(VT,Vermont,0,0,0,0,0,0)]
;*********************************************************************************
;Washington, City of
[ASSIGN %%1=ADD_JURISDICTION(DC,Washington DC,0,0,0,0,0,0)]
;*********************************************************************************
;Washington, State of 
[ASSIGN %%1=ADD_JURISDICTION(WA,Washington,0,0,0,0,0,0)]
;*********************************************************************************
;Wisconsin
[ASSIGN %%1=ADD_JURISDICTION(WI,Wisconsin,0,0,0,0,0,0)]
;*********************************************************************************
;West Virginia
[ASSIGN %%1=ADD_JURISDICTION(WV,West Virginia,0,0,0,0,0,0)]
;*********************************************************************************
