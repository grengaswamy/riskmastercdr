;**********************************************************************************************************************
;**********************************************************************************************************************
;BEWARE--BEWARE
;Tax status must follow the jurisdiction where tax status is used
;Jurisdictional Benefit tax status is NOT always the same as Jurisdictional EDI tax status
;jtodd22 11/24/2004
[FORGIVE] INSERT INTO FAKE_ERROR (Beginning_Of_File) VALUES ('Begin of JurisRulesDataTaxStatus.sql')
;##################################################################################
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_AK_CODE,Tax Filing Status-Alaska,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Alaska WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_AK_CODE,0)]
[ASSIGN %%1=ADD_CODE(M,1033,Married,TAX_STATS_AK_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Alaska of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_CT_CODE,Tax Filing Status-Connecticut,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Connecticut WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_CT_CODE,0)]
[ASSIGN %%1=ADD_CODE(HOH,1033,Head of Household,TAX_STATS_CT_CODE,0)]
[ASSIGN %%1=ADD_CODE(MJ,1033,Married Filing Joint,TAX_STATS_CT_CODE,0)]
[ASSIGN %%1=ADD_CODE(MS,1033,Married Filing Separate,TAX_STATS_CT_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Connecticut of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_IA_CODE,Tax Filing Status-Iowa WCP,3,1033)]
[ASSIGN %%2=ADD_JURISDICTION(IA, Iowa,0,0,0,0,0,0)]
[ASSIGN %%3=QUERY(SELECT COUNT(CODE_ID) FROM CODES WHERE TABLE_ID = %%1)]
[IF %%3<>2]
[FORGIVE] DELETE FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = %%2
[FORGIVE] DELETE FROM CLAIM_AWW WHERE JR_TAX_STATUS_CODE IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID = %%1)
[FORGIVE] DELETE FROM CODES_TEXT WHERE CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID = %%1)
[FORGIVE] DELETE FROM CODES WHERE TABLE_ID = %%1
[ENDIF] 
[ASSIGN %%4=QUERY(SELECT COUNT(DISTINCT TAX_STATUS_CODE) FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = %%2)]
[IF %%4<>2]
[FORGIVE] DELETE FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = %%2
[ENDIF]
[ASSIGN %%5=ADD_CODE(S,1033,Single,TAX_STATS_IA_CODE,0)]
[ASSIGN %%6=ADD_CODE(M,1033,Married,TAX_STATS_IA_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Iowa of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_IL_CODE,Tax Filing Status-Illinois WCP,3,1033)]
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_IL_CODE,0)]
[ASSIGN %%1=ADD_CODE(M,1033,Married,TAX_STATS_IL_CODE,0)]
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_ME_CODE,Tax Filing Status-Maine,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Maine WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_ME_CODE,0)]
[ASSIGN %%1=ADD_CODE(SHOH,1033,Single/Head of Household,TAX_STATS_ME_CODE,0)]
[ASSIGN %%1=ADD_CODE(MJ,1033,Married/Filing Joint,TAX_STATS_ME_CODE,0)]
[ASSIGN %%1=ADD_CODE(MS,1033,Married/Filing Separate,TAX_STATS_ME_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Maine of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_MI_CODE,Tax Filing Status-Michigan,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Michigan WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_MI_CODE,0)]
[ASSIGN %%1=ADD_CODE(SHOH,1033,Single/Head of Household,TAX_STATS_MI_CODE,0)]
[ASSIGN %%1=ADD_CODE(MJ,1033,Married/Filing Joint,TAX_STATS_MI_CODE,0)]
[ASSIGN %%1=ADD_CODE(MS,1033,Married/Filing Separate,TAX_STATS_MI_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Michigan of JurisRulesDataTaxStatus.sql')
;##################################################################################
;New Hampshire uses the federal tax status to compute the max on some work comp benefits
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_NH_CODE,Tax Filing Status-New Hampshire,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-New Hampshire WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_NH_CODE,0)]
[ASSIGN %%1=ADD_CODE(SHOH,1033,Single/Head of Household,TAX_STATS_NH_CODE,0)]
[ASSIGN %%1=ADD_CODE(MJ,1033,Married/Filing Joint,TAX_STATS_NH_CODE,0)]
[ASSIGN %%1=ADD_CODE(MS,1033,Married/Filing Separate,TAX_STATS_NH_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of New Hampshire of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_RI_CODE,Tax Filing Status-Rhode Island,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Rhode Island WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_RI_CODE,0)]
[ASSIGN %%1=ADD_CODE(M,1033,Married,TAX_STATS_RI_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Rhode Island of JurisRulesDataTaxStatus.sql')
;##################################################################################
[ASSIGN %%1=ADD_GLOSSARY(TAX_STATS_WA_CODE,Tax Filing Status-Washington,3,1033)]
[FORGIVE] UPDATE GLOSSARY_TEXT SET TABLE_NAME = 'Tax Filing Status-Washington WCP' WHERE TABLE_ID = %%1
[ASSIGN %%1=ADD_CODE(S,1033,Single,TAX_STATS_WA_CODE,0)]
[ASSIGN %%1=ADD_CODE(M,1033,Married,TAX_STATS_WA_CODE,0)]
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of Washington of JurisRulesDataTaxStatus.sql')
;##################################################################################
[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of JurisRulesDataTaxStatus.sql')
