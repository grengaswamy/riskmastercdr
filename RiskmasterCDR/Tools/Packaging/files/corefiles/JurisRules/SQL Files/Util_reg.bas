Attribute VB_Name = "UTIL_REG"
'*****************************************************************
'*
'*  UTIL_REG.BAS
'*
'*  <Enter Module Description Here>
'*
'*  Date Written:             By:
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Option Explicit


' Declare all Registration Database API's

'---------------------------------------------------------
' Registration database constants
'----------------------------------------------------------
Const REG_SZ = 1
Global Const KEY_LENGTH = 128
Global Const REG_ROOT_KEY = "SOFTWARE\DTG"
Const REG_SUCCESS = 0&
Const REG_BADKEY = 2&

Public Const HKEY_LOCAL_MACHINE = &H80000002

'-----------------------------------------------------
' SORTMASTER Registration database Keys
'------------------------------------------------------

'Declare Function RegOpenKey Lib "SHELL" (ByVal hKey As Long, ByVal SubKey As String, hKeyRtn As Long) As Long
'Declare Function RegQueryValue Lib "SHELL" (ByVal hKey As Long, ByVal SubKey As String, ByVal Buffer As String, lBuffLen As Long) As Long
'Declare Function RegSetValue Lib "SHELL" (ByVal hKey As Long, ByVal SubKey As String, ByVal DataType As Long, ByVal Buffer As String, ByVal lBuffLen As Long) As Long
Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Declare Function RegQueryValue Lib "advapi32.dll" Alias "RegQueryValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal lpValue As String, lpcbValue As Long) As Long
Declare Function RegSetValue Lib "advapi32.dll" Alias "RegSetValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long

Function bInitRegDB() As Integer

On Error GoTo ERROR_TRAP_bInitRegDB

    Dim result As Long
    Dim sRoot As String
    Dim hKey As Long

    sRoot = REG_ROOT_KEY & "\RISKMASTER" '& Trim(app.Title)

    result = RegSetValue(HKEY_LOCAL_MACHINE, REG_ROOT_KEY, REG_SZ, "Computer Sciences Corporation Applications", KEY_LENGTH)
    result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot, REG_SZ, "", KEY_LENGTH)

    If RegOpenKey(HKEY_LOCAL_MACHINE, sRoot & "\Options", hKey) = REG_BADKEY Then
        result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot & "\Options", REG_SZ, "RiskMaster Options", KEY_LENGTH)
    End If

    Rem get security info - initialize if not already setup in reg database
    If RegOpenKey(HKEY_LOCAL_MACHINE, sRoot & "\Security", hKey) = REG_BADKEY Then
        result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot & "\Security", REG_SZ, "RiskMaster Security System", KEY_LENGTH)
    End If
    If RegOpenKey(HKEY_LOCAL_MACHINE, sRoot & "\Security\DefaultUser", hKey) = REG_BADKEY Then
        result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot & "\Security\DefaultUser", REG_SZ, "Administrator", KEY_LENGTH)
    End If
    If RegOpenKey(HKEY_LOCAL_MACHINE, sRoot & "\Security\DefaultDSN", hKey) = REG_BADKEY Then
        result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot & "\Security\DefaultDSN", REG_SZ, "Security", KEY_LENGTH)
    End If
    If RegOpenKey(HKEY_LOCAL_MACHINE, sRoot & "\Security\Directory", hKey) = REG_BADKEY Then
        Dim SecPath As String
        SecPath = InputBox$("Please enter the path of the security directory:", "Enter Security Directory", App.Path)
        result = RegSetValue(HKEY_LOCAL_MACHINE, sRoot & "\Security\Directory", REG_SZ, SecPath, KEY_LENGTH)
    End If

    Exit Function

ERROR_TRAP_bInitRegDB:
    If iGeneralError(Err, Error$, "UTIL_REG.BAS\bInitRegDB") = vbRetry Then
        Resume
    Else
        bInitRegDB = False
    End If
End Function

'*****************************************************************
'*
'*  bRegDBGetValue (sKey As String, sValue As String) As Integer
'*
'*  <Enter Function Description Here>
'*
'*  Inputs: <Enter Input Values Here>
'*
'*  Returns: <Enter Return Values Here>
'*
'*  Date Written:             By:
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Function bRegDBGetValue(sKey As String, sValue As String) As Integer

On Error GoTo ERROR_TRAP_bRegDBGetValue

    Dim sItem As String * KEY_LENGTH
    Dim lrtn As Long
    bRegDBGetValue = False
    Dim sRoot As String
    Dim sTmp As String

    sRoot = REG_ROOT_KEY '   & "\"    ' & Trim(App.Title)
    If sKey <> "" And Left$(sKey, 1) <> "\" Then
        sTmp = sRoot & "\" & sKey
    Else
        sTmp = sRoot & sKey
    End If
    lrtn = RegQueryValue(HKEY_LOCAL_MACHINE, sTmp, sItem, KEY_LENGTH)

    If lrtn = REG_SUCCESS Then
        bRegDBGetValue = True
        sValue = sTrimNull(sItem)
    Else
        bRegDBGetValue = False
    End If

    Exit Function

ERROR_TRAP_bRegDBGetValue:
    If iGeneralError(Err, Error$, "UTIL_REG.BAS\bRegDBGetValue") = vbRetry Then
        Resume
    Else
        bRegDBGetValue = False
    End If
End Function

Function bRegDBSetValue(sKey As String, sValue As String) As Integer


On Error GoTo ERROR_TRAP_bRegDBSetValue
    Dim lrtn As Long
    bRegDBSetValue = False
    Dim sRoot As String
    Dim sTmp As String
    sRoot = REG_ROOT_KEY & "\" & Trim(App.Title)
    If sKey <> "" And Left(sKey, 1) <> "\" Then
        sTmp = sRoot & "\" & sKey
    Else
        sTmp = sRoot & sKey
    End If

    lrtn = RegSetValue(HKEY_LOCAL_MACHINE, sTmp, REG_SZ, sValue, KEY_LENGTH)
    If lrtn = REG_SUCCESS Then
        bRegDBSetValue = True
    End If

    Exit Function

ERROR_TRAP_bRegDBSetValue:
    If iGeneralError(Err, Error$, "bRegDBSetValue") = vbRetry Then
        Resume
    End If
End Function

