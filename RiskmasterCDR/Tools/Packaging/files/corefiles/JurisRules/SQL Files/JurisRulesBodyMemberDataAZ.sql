;*********************************************************************************
;Arizona
[ASSIGN %%1=ADD_JURISDICTION(AZ,Arizona,0,0,0,0,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a thumb,0,0,15,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The loss of a distal or second phalange of the thumb,0,0,7.5,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The loss of more than one phalange of the thumb,0,0,15,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a first (index) finger,0,0,9,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a second finger,0,0,7,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a third finger,0,0,5,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of the fourth (little) finger,0,0, 4,0)]
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The loss of the distal or third phalange of the
; first, second, third or fourth finger, shall be considered equal to the loss finger, 
; and compensation shall be one-half of the amount specified for the loss of the entire  finger.
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The loss of more than one phalange of the thumb or finger shall be considered as the loss of the entire finger or thumb, but in no event shall the amount received for more than one finger exceed the amount provided for the loss of a hand.
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a great toe,0,0,7,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The loss of the first phalange of a great toe,0,0,3.5,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a toe other than the great toe,0,0,2.5,0)]
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of the first phalange of a toe other than the great toe,0,0,1.25,0)]
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of more than one phalange shall be considered as the loss of the entire toe.
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a major hand,0,0,50,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a minor hand,0,0,40,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a major arm,0,0,60,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a minor arm,0,0,50,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a foot,0,0,40,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of a leg,0,0,50,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the loss of an eye by enucleation,0,0,30,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the permanent and complete loss of sight in one eye without enucleation,0,0,25,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For permanent and complete loss of hearing in one ear,0,0,20,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For permanent and complete loss of hearing in both ears,0,0,60,0)]
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,The permanent and complete loss of the use of a finger, toe, arm, hand, foot or leg may be deemed the same as the loss of any such member by separation.
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For the partial loss of use of a finger, toe, arm, hand, foot or leg, or partial loss of sight or hearing, fifty per cent of the average monthly wage during that proportion of the number of months in the foregoing schedule provided for the complete loss of use of such member, or complete loss of sight or hearing, which the partial loss of use thereof bears to the total loss of use of such member or total loss of sight or hearing. In this paragraph, "loss of use" means a loss of physical function of the affected member, sight or hearing. The effect on an employee's ability to return to the employee's occupation at the time of the injury shall not be considered in establishing the percentage of loss under this section, except that if the employee is unable to return to the work the employee was performing at the time the employee was injured due to the total or partial loss of use, compensation pursuant to this section shall be calculated based on seventy-five per cent of the average monthly wage.
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,For permanent disfigurement about the head or face, which shall include injury to or loss of teeth, the commission may, in accordance with the provisions of section 23-1047, allow such sum for compensation thereof as it deems just, in accordance with the proof submitted, for a period of not to exceed eighteen months.
