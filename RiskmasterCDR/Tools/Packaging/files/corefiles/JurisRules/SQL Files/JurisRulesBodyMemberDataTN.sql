;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;#################################################################################
;#################################################################################
;*********************************************************************************
;Tennessee
[ASSIGN %%1=ADD_JURISDICTION(TN,Tennessee,0,0,0,0,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a thumb,0,62,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,thumb,0,60,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,first finger- commonly called an index finger,0,35,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,second finger,0,30,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,third finger,0,20,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,fourth finger- commonly called a little finger,0,15,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,thumb-first phalange,0,30,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,first finger-first phalange- commonly called an index finger,0,17.5,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,second finger-first phalange,0,15,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,third finger-first phalange,0,10,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,fourth finger-first phalange- commonly called a little finger,0,7.5,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,thumb-more than one (1) phalange,0,60,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,first finger-more than one (1) phalange- commonly called an index finger,0,35,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,second finger-more than one (1) phalange,0,30,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,third finger-more than one (1) phalange,0,20,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,fourth finger-more than one (1) phalange- commonly called a little finger,0,15,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,great toe,0,30,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,great toe-first phalange,0,15,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,great toe-more than one phalange,0,30,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,toe other than the great toe,0,10,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,toe other than the great toe-first phalange,0,5,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,toe other than the great toe-more than one phalange,0,10,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,hand,0,150,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,arm,0,200,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,arm-amputated above the wrist joint,0,200,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,foot,0,125,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,leg,0,200,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,leg-amputated above the ankle joint,0,200,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,eye,0,100,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,ears-both complete permanent loss of hearing,0,150,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,eye and a leg,0,350,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,eye and an arm,0,350,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,eye and a hand,0,325,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,eye and a foot,0,300,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,two (2) arms- other than at the shoulder,0,400,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,two (2) hands,0,400,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,two (2) legs,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,two (2) feet,0,400,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,one (1) arm and the other hand,0,400,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,one (1) hand and (1) foot,0,400,0,0)]  
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,one (1) leg and one (1) hand,0,400,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,one (1) arm and one (1) foot,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,one (1) arm and one (1) leg,0,400,0,0)] 
;  (C) When an employee sustains concurrent injuries resulting in concurrent disabilities, 
;such employee shall receive compensation only for the injury that produced the longest period of 
;disability, but this section shall not affect liability for the concurrent loss of more than one 
;(1) member, for which members' compensations are provided in the specific schedule and 
;in subdivision (4)(B). In all cases the permanent and total loss of the use of a member 
;shall be considered as equivalent to the loss of that member, but in such cases the compensation 
;in and by the schedule provided shall be in lieu of all other compensation.  
; (D) In cases of permanent partial disability due to injury to a member resulting in less than 
;total loss of use of such member not otherwise compensated in this schedule, compensation shall 
;be paid at the prescribed rate during that part of the time specified in the schedule for 
;the total loss or total loss of use of the respective member that the extent of injury to 
;the member bears to its total loss. If an injured employee refuses employment suitable to 
;such injured employee's capacity, offered to or procured for such injured employee, the 
;injured employee shall not be entitled to any compensation at any time during the continuance 
;of such refusal, unless at any time in the opinion of a court having jurisdiction over 
;the underlying workers' compensation case such refusal is justifiable. All compensation 
;provided in this subdivision (3) for loss to members, or loss of use of members, is subject 
;to the same limitation as to maximum and minimum as are stated in subdivision (1).  
; 
; (E) For serious disfigurement to the head, face or hands, not resulting from the loss of a member
; or other injury specifically compensated, so altering the personal appearance of the injured employee
; as to materially affect such injured employee's employability in the employment in which such injured
; employee was injured or other employment for which such injured employee is then qualified, 
;sixty-six and two-thirds percent (662/3%) of the average weekly wages for such period as the 
;court may determine, not exceeding two hundred (200) weeks. The benefit herein provided shall 
;not be awarded in any case where the injured employee is compensated under any other provision 
;of the Workers' Compensation Law, compiled in this chapter.  
; (F) All other cases of permanent partial disability enumerated in this subdivision 
;(3) shall be apportioned to the body as a whole, which shall have a value of four hundred (400) weeks, 
;and there shall be paid compensation to the injured employee for the proportionate loss of use 
;of the body as a whole resulting from the injury. Compensation for such permanent partial disability 
;shall be subject to the same limitations as to maximum and minimum as provided in subdivision (1). 
;If an employee has previously sustained an injury compensable under this section for which a court 
;of competent jurisdiction has awarded benefits based on percentage of disability to the body as a 
;whole and suffers a subsequent injury not enumerated in this subdivision (3), the injured employee 
;shall be paid compensation for the period of temporary total disability and only for the degree of 
;permanent disability that results from the subsequent injury. The benefits provided by this 
;subdivision (3)(f) shall not be awarded in any case where benefits for a specific loss are 
;otherwise provided in this chapter;
;  
; (4) (A) (i) Permanent Total Disability. For permanent total disability as defined 
;in subdivision (4)(B), sixty-six and two-thirds percent (662/3%) of the wages 
;received at the time of the injury, subject to the maximum weekly benefit and 
;minimum weekly benefit; provided, that if the employee's average weekly wages 
;are equal to or greater than the minimum weekly benefit, the employee shall receive 
;not less than the minimum weekly benefit; provided, further, that if the employee's 
;average weekly wages are less than the minimum weekly benefit, the employee shall 
;receive the full amount of the employee's average weekly wages, but in no event shall
; the compensation paid be less than the minimum weekly benefit.  This compensation 
;shall be paid during the period of the permanent total disability until the employee is, 
;by age, eligible for full benefits in the Old Age Insurance Benefit Program under 
;the Social Security Act; provided, that with respect to disabilities resulting from 
;injuries that occur after 60 years of age, regardless of the age of the employee, 
;permanent total disability benefits are payable for a period of two hundred sixty (260) weeks. 
;Such compensation payments shall be reduced by the amount of any old age insurance benefit 
;payments attributable to employer contributions that the employee may receive under 
;title II of chapter 7, title 42 of the Social Security Act, 42 U.S.C. � 401 et seq.,  as amended.  
; (ii) Notwithstanding other provisions of the law to the contrary and notwithstanding any 
;agreement of the parties to the contrary, permanent total disability payments shall not 
;be commuted to a lump sum, except in accordance with the following:  
; (a) Benefits may be commuted to a lump sum to pay only the employee's attorney's fees and 
;litigation expenses and to pay pre-injury obligations in arrears;  
; (b) The commuted portion of an award shall not exceed the value of one hundred (100) weeks of 
;the employee's benefits;  
; (c) After the total amount of the commuted lump sum is determined, the amount of the weekly disability
; benefit shall be recalculated to distribute the total remaining permanent total benefits in 
;equal weekly installments beginning with the date of entry of the order and terminating on 
;the date the employee's disability benefits terminate pursuant to subdivision (4)(A)(i).  
; (iii) Attorneys' fees in contested cases of permanent total disability shall be calculated upon 
;the first four hundred (400) weeks of disability only.  
; (iv) In case an employee who is permanently and totally disabled becomes an inmate of a public 
;institution, and provided further, that if no person or persons are wholly dependent upon such 
;employee, then the amounts falling due during the lifetime of such employee shall be paid to 
;such employee or to such employee's guardian, if non compos mentis, to be spent for 
;the ward's benefit; such payments to cease upon the death of such employee.  
; (B) When an injury not otherwise specifically provided for in this chapter, as amended, totally 
;incapacitates the employee from working at an occupation that brings the employee an income, such
;employee shall be considered "totally disabled," and for such disability compensation shall be 
;paid as provided in subdivision (4)(A); provided, that the total amount of compensation payable 
;hereunder shall not exceed the maximum total benefit, exclusive of medical and hospital benefits;
;  
 
 
 
