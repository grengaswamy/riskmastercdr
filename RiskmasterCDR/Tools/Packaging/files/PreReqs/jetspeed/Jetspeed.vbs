'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.0
'
' NAME: Jetspeed.vbs
'
' AUTHOR: UserName , Computer Sciences Corporation (CSC)
' DATE  : 3/14/2007
'
' COMMENT: Manages the easy replacement of information in the Jetspeed files
'
'==========================================================================
Dim strComputer, strComputerValue, strReplaceValue
Const strHTTPPrefix = "http://"

'Set the value as the local computer
strComputer = "."

'Stop the Apache Tomcat service
Call StopService(strComputer, "Apache Tomcat")

'Prompt for the required search and replacement values
strComputerValue = InputBox("Please enter the current path", "Current Path", "localhost:8080")

'Check the input box value
CheckInput(strComputerValue)

strReplaceValue = InputBox("Please enter the desired path", "Computer/DNS Name", LCase(GetComputerName()) & ":8080")

'Check the input box value
CheckInput(strReplaceValue)

'Update the required Jetspeed files
Call UpdateFiles(strComputerValue, strReplaceValue)

'Start the Apache Tomcat service
Call StartService(strComputer, "Apache Tomcat")

'Wait a few seconds before the browser can display the page
WScript.Sleep 5000

'Open the browser to test the Jetspeed installation
If (ContainsHttp(strReplaceValue)) Then
	'Open the browser with the specified URL
	OpenBrowser strReplaceValue & "/jetspeed"
Else
	'Open the browser with the default Http prefix
	OpenBrowser strHTTPPrefix & strReplaceValue & "/jetspeed"
End If


'Subroutine to check the input value from the InputBox
Sub CheckInput(strInputBoxValue)
	'If the input box value is an empty String
	If (strInputBoxValue = "") Then
		'Abort the execution of the script
		WScript.Quit
	End If
End Sub

'Function to determine whether a specific string expression
'contains the specified String
Function ContainsString(strValue, strFindValue)	
	'If the specified string contains the value of http or https
	If InStr(strValue, strFindValue) > 0 Then
		'Indicate that the string contains the specified string
		ContainsString = True
	Else
		'Indicate that the string does not contain the specified string
		ContainsString = False
	End If
End Function

'Function to determine whether the specific string 
'already contains a value of Http
Function ContainsHttp(strValue)
	Const strHTTP = "http"
	
	'If the specified string contains the value of http or https
	If InStr(strValue, strHTTP) > 0 Then
		'Indicate that the string contains the Http prefix
		ContainsHttp = True
	Else
		'Indicate that the string does not contain the Http prefix
		ContainsHttp = False
	End If
End Function

'Function to determine the index of a string search expression
'with a specified String
Function FindIndex(strSearch, strExpression)
	Dim lngFoundPos 

	'Find the index of the search expression within the string	
	lngFoundPos = InStr(1, strSearch, strExpression)

	'Return the index at which the specified expression was found
	FindIndex = lngFoundPos
End Function

'Function to extract the computer name
'from a specified URL
'PSEUDOCODE: Remove http/https from the left portion of the String
'Extract the left portion of the URL containing the computer name/URL and the port
Function ExtractHostName(strURL)
	Dim strHostName
	Dim intIndex
	Const URL_PREFIX = "://"
	
	intIndex = FindIndex(strURL, URL_PREFIX)
	
	'If a match was found
	If (intIndex > 0) Then
		'Retrieve the name of the computer name with the port number
		strHostName = Right(strURL, Len(strURL) - ((intIndex - 1) + Len(URL_PREFIX)))	
	Else
		MsgBox "Host configuration information was not specified correctly.  Please re-try the installation and specify valid host information."
		'Exit the script
		WScript.Quit
	End If

	'Return the extracted String
	ExtractHostName = strHostName
End Function


'Function to extract the URL prefix
'from a specified URL
Function ExtractURLPrefix(strURL)
	Dim strURLPrefix
	Dim intIndex
	Const URL_PREFIX = "://"
	
	intIndex = FindIndex(strURL, URL_PREFIX)
	
	'If a match was found
	If (intIndex > 0) Then
		'Retrieve the name of the computer name with the port number
		strURLPrefix = Left(strURL, (intIndex - 1)+ Len(URL_PREFIX))	
	Else
		MsgBox "URL configuration information was not specified correctly.  Please re-try the installation and specify valid URL information."
		'Exit the script
		WScript.Quit
	End If

	'Return the extracted String
	ExtractURLPrefix = strURLPrefix
End Function

'Get the current script path location
Function GetPath()
	' Return path to the current script
	DIM path
	path = WScript.ScriptFullName  ' script file name
	GetPath = Left(path, InstrRev(path, "\"))
End Function

'Function to retrieve the local computer name
Function GetComputerName()
	Dim SWBemlocator, objWMIService
	Const IS_WORKGROUP_MEMBER = 2
	Const IS_DOMAIN_MEMBER = 3
	Const IS_BACKUP_DOMAIN_CONTROLLER = 4
	Const IS_PRIMARY_DOMAIN_CONTROLLER = 5

	Set SWBemlocator = CreateObject("WbemScripting.SWbemLocator")
	Set objWMIService = SWBemlocator.ConnectServer(strComputer,"root\CIMV2",UserName,Password)
	Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48)
	For Each objItem in colItems
		'If the server is a member of a workgroup
		If (objItem.DomainRole = IS_WORKGROUP_MEMBER) Then
			'Obtain the DNS Host name of the computer
			strComputerName = objItem.DNSHostName
		ElseIf (objItem.DomainRole = IS_DOMAIN_MEMBER) Then
			'Obtain the FQDN name of the computer
			strComputerName = objItem.DNSHostName & "." & objItem.Domain
		ElseIf (objItem.DomainRole = IS_BACKUP_DOMAIN_CONTROLLER Or objItem.DomainRole = IS_PRIMARY_DOMAIN_CONTROLLER) Then
			MsgBox "Riskmaster X cannot be installed on a Domain Controller.  Please install on another server."
			WScript.Quit
		End If
	Next
	
	'Clean up
	Set SWBemlocator = Nothing
	Set objWMIService = Nothing
	
	'Return the Host Computer Name as the value of the function
	GetComputerName = strComputerName
End Function

'Script to automatically start
'a specified Windows Service using WMI
'NOTE: Display Service Name is case sensitive
Sub StartService(strComputer, strServiceName)
	On Error Resume Next
	
	Dim objWMIService, objItem, objService
	Dim colListOfServices, strService
	
	'strService is case sensitive.
	strService = " '" & strServiceName & "' "
	Set objWMIService = GetObject("winmgmts:" _
	& "{impersonationLevel=impersonate}!\\" _
	& strComputer & "\root\cimv2")
	Set colListOfServices = objWMIService.ExecQuery _
	("Select * from Win32_Service Where DisplayName ="_
	& strService & " ")
	
	'Loop through the list of Windows Services
	'Loop through the list of Windows Services
	For Each objService in colListOfServices
		'If the service is currently running
		If objService.State = "Stopped" Then
			'Start the specified Windows Service
			objService.StartService()
		End If
	Next  
End Sub

'Script to automatically stop
'a specified Windows Service using WMI
'NOTE: Display Service Name is case sensitive
Sub StopService(strComputer, strServiceName)
	On Error Resume Next
	
	Dim objWMIService, objItem, objService
	Dim colListOfServices, strService
	
	'strService is case sensitive.
	strService = " '" & strServiceName & "' "
	Set objWMIService = GetObject("winmgmts:" _
	& "{impersonationLevel=impersonate}!\\" _
	& strComputer & "\root\cimv2")
	Set colListOfServices = objWMIService.ExecQuery _
	("Select * from Win32_Service Where DisplayName ="_
	& strService & " ")
	
	'Loop through the list of Windows Services
	For Each objService in colListOfServices
		'If the service is currently running
		If objService.State = "Running" Then
			'Stop the specified Windows Service
			objService.StopService()
		End If
	Next 
End Sub


'Open the browser to test the Jetspeed Portal Installation
Sub OpenBrowser(strURL)
	Dim WshShell
	
	'Create the Windows Scripting Host object
	Set WshShell = CreateObject("WScript.Shell")
	
	'Run the following command to open the browser with the specified URL
	WshShell.Run "iexplore.exe " & strURL, 3, False
	
	'Clean up
	Set WshShell = Nothing
End Sub

'Performs a string replacement using Regular Expressions
Function RegExReplace(strRegExprPattern, strSearchText, strReplaceValue)
	Dim objRegEx
	Dim strReplaceText
	
	'Create the Regular Expression object
	Set objRegEx = CreateObject("VBScript.RegExp")
	
	objRegEx.Global = True
	objRegEx.Pattern = strRegExprPattern
	objRegEx.IgnoreCase = True
	
	'Perform the string replacement using Regular Expressions
	strReplaceText = objRegEx.Replace(strSearchText, strReplaceValue)
		
	'Clean up
	Set objRegEx = Nothing
	
	RegExReplace = strReplaceText
End Function

'Reads the contents of a specified file
Function ReadFile(strFileName)
	Dim objFSO, objFile, objReadFile
	Dim strFileText
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	If (Not objFSO.FileExists(strFileName)) Then
		MsgBox "File path does not exist: " & strFileName
	End If
	'Obtain a handle on the file for reading
	Set objReadFile = objFSO.GetFile(strFileName)
	
	'Begin reading the contents of the file
	Set objFile =objReadFile.OpenAsTextStream(ForReading)
	
	'Read in the entire contents of the file
	strFileText = objFile.ReadAll
	
	'Close the file from further reading
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objReadFile = Nothing
	Set objFile = Nothing
	
	ReadFile = strFileText
End Function

'Writes out the specified text/contents to a specified file
Sub WriteFile(strFileName, strFileText)
	Dim objFSO, objFile, objWriteFile
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Obtain a handle on the file for reading
	Set objWriteFile = objFSO.GetFile(strFileName)
	
	'Open the file for writing
 	Set objFile = objWriteFile.OpenAsTextStream(ForWriting)

	'Write out the updated contents of the file
	objFile.WriteLine strFileText	

	'Close the file from further writing
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objWriteFile = Nothing
	Set objFile = Nothing
End Sub

'Function to retrieve an array containing all of the relevant file paths
'that are required for editing
Sub UpdateFiles(strSearchValue, strReplaceValue)
	Dim arrFiles(3)
	Dim strScriptPath
	
	'Retrieve the path to the current script location
	strScriptPath = GetPath()
	
	REM Specify the path to the web.xml file
	'TODO: Replace both the http:// path and the :8080 path
	arrFiles(0) = strScriptPath & "WEB-INF\web.xml"
	
	REM Specify the path To the CSC_JetspeedSecurity.properties file
	arrFiles(1) = strScriptPath & "WEB-INF\conf\CSC_JetspeedSecurity.properties"
	
	REM Specify the path To the JSP template for editing
	arrFiles(2) = strScriptPath & "WEB-INF\templates\jsp\navigations\html\top_default.jsp"
	
	REM Open the PSML file for editing
	arrFiles(3) = strScriptPath & "WEB-INF\psml\role\user\html\default.psml"
	
	'Call the StringReplace function to replace the relevant values
	'in each of the files by iterating over all of them
	For Each arrFileName In arrFiles
		Call UpdateFileContents(arrFileName, strSearchValue, strReplaceValue)
	Next
End Sub

'Perform a string replacement on a specified string in a file
Sub UpdateFileContents(strFileName, strSearchValue, strReplaceValue)
	Dim strOriginalFileText, strReplacementFileText
	Dim PRES_REGEXP, PRES_REGEXP_URL, URL_PREFIX
	Const HTTP_PREFIX = "http://"
			
	'Check the search value for the presence of the http or https prefix
	If (ContainsHttp(strSearchValue)) Then
		URL_PREFIX = ExtractURLPrefix(strSearchValue)
		strSearchValue = ExtractHostName(strSearchValue)
	Else
		URL_PREFIX = HTTP_PREFIX
	End If
			
	
	'Set the values for the Presentation Server Regular Expression
	'If the replacement string already contains an http or https prefix
	If (ContainsHttp(strReplaceValue)) Then
		PRES_REGEXP = ExtractHostName(strReplaceValue)
		PRES_REGEXP_URL = strReplaceValue
	Else
		PRES_REGEXP = strReplaceValue
		PRES_REGEXP_URL = HTTP_PREFIX & strReplaceValue
	End If
	
	'Handle all files which are not web.xml
	If (ContainsString(strFileName, "web.xml") = False) Then
		strOriginalFileText = ReadFile(strFileName)
				
		strReplacementFileText = RegExReplace(URL_PREFIX & strSearchValue, strOriginalFileText, PRES_REGEXP_URL)
		
		Call WriteFile(strFileName, strReplacementFileText)
	Else 'The file name contains web.xml
		'NOTE:web.xml is a unique case which involves replacement of both http(s):// URLs
		'and computer name/DNS URLs
		strOriginalFileText = ReadFile(strFileName)
				
		strReplacementFileText = RegExReplace(URL_PREFIX & strSearchValue, strOriginalFileText, PRES_REGEXP_URL)
				
		'Perform an additional replacement to handle the case of standalone computer names/DNS names
		'without the http(s):// prefix
		strReplacementFileText = RegExReplace(strSearchValue, strReplacementFileText, PRES_REGEXP)
				
		Call WriteFile(strFileName, strReplacementFileText)
	End If
End Sub