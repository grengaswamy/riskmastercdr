<%--
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.jetspeed.services.resources.JetspeedResources" %>

<%
  String BgImage = JetspeedResources.getString("topnav.bg.image");
  String BgColor = JetspeedResources.getString("topnav.bg.color");
  String FontFace = JetspeedResources.getString("topnav.font.face");
  String FontSize = JetspeedResources.getString("topnav.font.size");
  String FontColor = JetspeedResources.getString("topnav.font.color");
  String FontStyle = null;
  String FontStyleW = null;
  
  if (BgImage == null || BgImage.equals("")) {
    BgImage = null;
  }  
  if (BgColor == null || BgColor.equals("")) {
    BgColor = null;
  }  
  if (FontFace == null || FontFace.equals("")) {
    FontFace = null;
  }  
  if (FontSize == null || FontSize.equals("")) {
    FontSize = null;
  }  
  if (FontColor == null || FontColor.equals("")) {
    FontColor = null;
  }  
  
  
FontStyleW = "style=color:white;font-size:10;";
FontStyle = "style=color:black;font-size:10;";
%>
<div>
  <table cellspacing="0" cellpadding="0" width="100%" <%if(BgColor !=null){%>bgcolor="<%=BgColor%>" <%}%> <%if(BgImage !=null){%>background="<%=BgImage%>" <%}%> >
    <tr valign="middle">
      <%
      //
      // Display the welcome message.  This is typicaly where a Banner AD is displayed
      //
      %>
      <td align="left" valign="top"><img src="images/Form_logo_CSC.gif"/></td>
      <td align="center" valign="top" background="images/welcome_tile.gif" width="100%" style="background-repeat: repeat-x;background-position: top;"/>
      <td align="center">
<!-- JP Want image not text        <h2><jetspeed:l10n key="TOP_TITLE"/></h2> -->
        <!-- <img src="images/welcome.gif"> -->
<!-- BSB Logout only works when the host part of the link href matches the one typed into the browser.
         Otherwise, the "logout" will be performed against a new session since the browser will not share the 
		original JSESSIONID cookie with the .logout page making it execute against a totally new (and unrelated)
		oxf servlet session. -->
<link href="../oxf/.logout" type="text/css" rel="stylesheet">

      </td>

      <%
      //
      // Right column of top navigation bar.
      // Optionally allow user login. Required for user creation
      // Optionally allow user creation.
      //
      %>
      <%if( JetspeedResources.getBoolean("topnav.user_login.enable") == true) { %>
      <td align="right" valign="bottom" width="30%">
          <form style="margin:0" method="POST" action="<jetspeed:dynamicUri/>" enctype="application/x-www-form-urlencoded">
           <input name="action" type="hidden" value="<%=JetspeedResources.getString("action.login")%>">
		<table cellspacing="2" cellpadding="0">
            <tr>
              <td <%=FontStyleW%>><jetspeed:l10n key="USERFORM_USERNAMEMSG"/></td>
              <td>
                <input value="" name="username" maxlength="25" type="text" tabindex="1" <%=FontStyle%>>
              </td>
              <td rowspan="2" align="center">
                <input name="submit" type="submit" value="<jetspeed:l10n key="USERFORM_LOGIN"/>" tabindex="4"  <%=FontStyle%> />
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td <%=FontStyleW%>><jetspeed:l10n key="USERFORM_PASSWORDMSG"/></td>
              <td>
                <input value="" name="password" maxlength="25" type="password" tabindex="2" style="font-size:10">
              </td>
            </tr>
            <%if ( JetspeedResources.getBoolean("automatic.logon.enable") == true ) {%>
            <tr>
              <td <%=FontStyle%> colspan="2">
                <input name="rememberme" value="true" type="checkbox" tabindex="3" /><jetspeed:l10n key="USERFORM_REMEMBERME"/>
              </td>
            </tr>
            <%}%>
            <!--<tr>
              <td align="center" colspan="3">
                <%if( JetspeedResources.getBoolean("topnav.user_creation.enable") == true &&
                      JetspeedResources.getBoolean("newuser.confirm.enable") == true ) { %>
                <a href="<jetspeed:link template="NewAccount"/>" <%=FontStyle%>><jetspeed:l10n key="TOP_CREATENEWACCOUNT"/></a>
                &nbsp;|&nbsp;
                <%}%>
                <a href="<jetspeed:link template="LoginHelp"/>" <%=FontStyle%>><jetspeed:l10n key="TOP_LOGINHELP"/></a>
              </td>
            </tr>-->
            <%
            // Leave space between last line and content
            %>
            <!-- <tr>
              <td>&nbsp;</td>
            </tr> -->
          </table>
  	</form>

 	</td>
      <%}%>
    </tr>
  </table>
      
 </div>
