#*
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*#

##<!-- begin jetspeed-menu.vm -->
#if (! $data.Customized )
#if ( ! $tabs )
#parse ("jetspeed.vm")
#else
#set ($width = $conf.getInitParameter("tab-width") )
#if (!$width)
  #set ($width = "20%" )
#end
<table cellpadding="0" cellspacing="0" border="0" width="100%" #if (${skin.ControllerStyleClass}) class="$skin.ControllerStyleClass" #end>
  <tr>
    <td valign="top" width="$width">
    <div #if (${skin.PortletSkinClass}) class="${skin.PortletSkinClass}" #end>
        <table cellspacing="0" border="0" cellpadding="0" width="100%" #if (${skin.TabStyleClass}) class="$skin.TabStyleClass" #end>
#foreach ( $tab in $tabs )
#if ($tab.isSelected() )
  #set ($bgcolor = $!{skin.HighlightBackgroundColor} )
  #set ($color = $!{skin.HighlightTextColor} )
  #set ($titleclass = $!{skin.HighlightTitleStyleClass} )
#else
  #set ($bgcolor = $!{skin.TitleBackgroundColor} )
  #set ($color = $!{skin.TitleTextColor} )
  #set ($titleclass = $!{skin.TitleStyleClass} )
#end
#if (${bgcolor})
  #if (${color})
    #set ($titlestyle = "background-color: $bgcolor; color: $color;" )
  #else
    #set ($titlestyle = "background-color: $bgcolor;" )
  #end
#else
  #if (${color})
    #set ($titlestyle = "color: $color;" )
  #else
    #set ($titlestyle = "" )
  #end
#end
          <tr>
            <td #if ($tab.Link && ($tab.isSelected() == false))  class="MenuLeftLow" #else class="MenuLeft" #end></td>
            <td valign="middle" #if (${skin.PortletSkinClass}) #if ($tab.Link && ($tab.isSelected() == false))  class="MenuMiddleLow" #else class="MenuMiddle" #end #elseif ($titleclass) class="$titleclass" #end #if ($titlestyle) style="${titlestyle}" #end>
              #if ($tab.Link && ($tab.isSelected() == false))
                <a href="$tab.Link"  #if (${skin.PortletSkinClass}) class="TabLink" #end #if (${color}) style="color:$color" #end>$tab.Title</a>
              #else
                $tab.Title
              #end</td>
            <td #if ($tab.Link && ($tab.isSelected() == false))  class="MenuRightLow" #else class="MenuRight" #end></td>
          </tr>
          <tr height="2"><td><img height="2" width="2" src="images/dot.gif" /></td></tr>
#end
          <tr>    
            <td colspan="3" valign="bottom" align="right">
#foreach ( $action in $actions )
              <a href="${action.Link}" title="${action.Name}"><img src="${skin.getImage($action.Name,"images/${action.Name}.gif")}" alt="${action.Name}" border="0"></a>
#end
            </td>
          </tr>      
        </table>
    </td>
    </div>
    <td width="2" #if (${skin.BackgroundColor}) style="background-color: ${skin.BackgroundColor}" #end ><img height="2" width="2" src="images/dot.gif" /></td>
    <td width="100%" valign="top" #if (${skin.ContentStyleClass}) class="$skin.ContentStyleClass" #end #if (${skin.BackgroundColor}) style="background-color:${skin.BackgroundColor}" #end >
      $portlet.getContent($data)
    </td>
  </tr>
</table>
#end
#else
    $portlet.getContent($data)
#end
##<!-- end jetspeed-menu.vm -->
