<%--
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%-- This screen will be shown if the user tries to access a screen that only logged in users may see --%>

<table border=1 cellpadding="5"> 

  <tr> 
    <td>
      <br>
      <h2>Please login first!</h2>
       Sorry, you must be logged in in order to access this screen.
    </td>
  </tr> 

</table>