<?xml version="1.0"?>
<!--
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.1//EN" "http://www.wapforum.org/DTD/wml_1.1.xml">

<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<% String screenJsp = (String)request.getAttribute("screenJsp"); %>

<wml>
  <jetspeed:navigation  defaultTemplate="wml/top.jsp" />
  <jsp:include page="<%= screenJsp %>" flush="true" /> 
  <jetspeed:navigation  defaultTemplate="wml/bottom.jsp" />
</wml>