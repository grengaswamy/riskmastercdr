/*
 * Copyright 2004-2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

function submitForm()
{
    return false;
}

//======================================================================
// isWhitespace(s) return true if all characters are whitespace.
//====================================================================== */
function isWhitespace(s)
{   var i;
    var whitespace = " \t\n\r";
    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}

//======================================================================
// confirmSubmit() is used to request confirmation before continuing with action(i.e. delete).
//====================================================================== */
function confirmSubmit(msg)
{
    var agree=confirm(msg);
    if (agree)
       return true ;
    else
       return false ;
    return true;
} 


function findFrameRecursive( winOrFrame, frameName ) {
    // 1. CHECK THIS FRAME (winOrFrame)
	// home string is being checked to take care of the complex
	// frameset in PE homepage. Need to fix this in a Generic way later.
    if ( (winOrFrame.name && (winOrFrame.name == frameName)) ||
	winOrFrame.name == "home" )
        return winOrFrame;

    // 2. SEARCH SUBFRAMES.  note: when there are no sub-frames,
    // the frames array has 1 entry which is this frame,
    // hense this check for 2+ subframes
    if ( winOrFrame.frames.length < 2 )
        return null;

    // recurse
    for ( i= 0 ; i < winOrFrame.frames.length ; i++ ) {
    var x= findFrameRecursive( winOrFrame.frames[i], frameName );
    if ( x )
        return x;
    }
    return null;
}

function findFrame( frameName ) {
    return findFrameRecursive( top, frameName );
}

function refreshTree() {
    var treeFrame = findFrame("index");
    if (treeFrame == null)
        return true;
    treeFrame.document.forms["treeForm"].submit();
    //refreshMastHead();
    return true;
}
function refreshMastHead() {
    synchronizeLeft();
    var headerFrame = findFrame("header");
    if (headerFrame == null)
        return true;
    if (headerFrame.document == null)
        return true;
    if (headerFrame.document.forms == null)
        return true;
    if (headerFrame.document.forms[0] == null)
        return true;
    headerFrame.document.forms[0].submit();
    return true;
}

function getRightSidePage() {
    var treeFrame = findFrame("index");
    if (treeFrame == null)
        return null;

    var form = treeFrame.document.forms["treeForm"];
    if (form == null) 
        return null;

    hidden = form.elements["Index.RightSidePage"];
    if (hidden == null)
        return null;

    var page = hidden.value;
    if (page == null || page == "")
        return null;
    hidden.value = "";
    return page;
}

function loadRightSide() {
    var rightFrame = findFrame("main");
    if (rightFrame != null) {
        if (rightFrame.document != null) {
            var page = getRightSidePage();
            if (page != null) {
                rightFrame.document.location.href=page;
            }
        }
    }
    return true;
}

function updateStatus() {
    var f= findFrame( "content" );
    if ( !f ) {
        f= findFrame( "main" );
        if ( !f ) {
            f= findFrame( "main-header");
            if ( !f ) {
                return true;
            }
        }
    }
    form = f.document.forms[0];
    if (!form) {
        defaultStatus="";
        return true;
    }
    st = form.elements["status"];
    if ( !st ) {
        defaultStatus="";
        return true;
    }
    defaultStatus = st.value;
    return true;
}

function skipOverTree() {
    var f= findFrame( "content" );  // 1st search for iWS content frame
    if ( !f ) //
    f= findFrame( "main" )  // then search for iAS content frame
    if ( !f )
        return;
    f.focus();
}

function openInHelpWindow(targetSupported, newURL ) {
	if (targetSupported) {

		win = window.open("../help/ee/index.html?"+newURL, "HelpWindow",
					"width=800,height=530,resizable," );
	}
	else {
		win = window.open("../help/index.html?"+newURL, "HelpWindow", 
					"width=800,height=530,resizable," );
	}
	win.focus();
}  

function setCookieValue( val, cookieName ) {
    document.cookie= cookieName + "=" + val;
}

function getCookieValue( cookieName ) {
    s= document.cookie;
    pos= s.indexOf(cookieName+"=");
    if (pos == -1)
        return null;
    start= pos+cookieName.length+1;
    end= s.indexOf(";", start );
    if ( end == -1 )
    end= s.length;
    return s.substring(start,end);
}


var ns4 = (document.layers) ? 1 : 0;

if ( ns4 )
  window.onresize= updateHighlight();

function rememberScrollbarPosition( theWindow, cookieName ) {
    setCookieValue( theWindow.document.body.scrollLeft + "." +
            theWindow.document.body.scrollTop, cookieName );
}

function restoreScrollbarPosition( theWindow, cookieName ) {
    var s=getCookieValue( cookieName );
    if ( s ) {
        var a= s.split(".");
        theWindow.scrollTo(parseInt(a[0]),parseInt(a[1]));
    }
}

function rememberScrollbarPositionOnUnload( aWin, scrollPosCookieName ) {
    if ( ! isNS4() ) {
        rememberScrollbarPosition( aWin, scrollPosCookieName );
    }
    return true;
}

function restoreScrollbarPositionOnLoad( aWin, scrollPosCookieName ) {
    if ( ! isNS4() ) {
        restoreScrollbarPosition( aWin, scrollPosCookieName );
    }
    return true;
}


//======================================================================
// getHiddenValue() Returns the value of the hidden variable with the give name.
//      Returns the given name if the hiden variable could not be found.
//====================================================================== */
function getHiddenValue(name) {
    var contentFrame = findFrame( "main" );
    if (! contentFrame) {
        return name;
    }
    var form = contentFrame.document.forms[0];
    if (! form) {
        return name;
    }
    hidden = form.elements[name];
    if (! hidden) {
        return name;
    }
    return hidden.value;
}

//======================================================================
// button processing
//======================================================================

function checkAndSubmit(button) {
    if (ps_CheckRequiredFields) { 
        if (ps_CheckRequiredFields()) {
            return submitAndDisable(button);
        } 
        return false;
    } else { 
        submitAndDisable(button); 
    }
}

function submitAndDisable(button) {
    button.className='Btn1Dis'; // the LH styleClass for disabled buttons.
    button.disabled=true; 
    button.form.action += "?" + button.name + "=" + button.value;
    button.value=getHiddenValue('msg.wait');
    button.form.submit(); 
    return true; 
}

/*===========================================================================*/
/* Tree Highlighting */
/*===========================================================================*/

function synchronizeLeft() {
    var treeFrame= findFrame( "index" );
    var contentFrame = findFrame( "main-header" );
    if (contentFrame == null) {
        contentFrame = findFrame( "main" );
    }
    if (treeFrame == null || contentFrame == null) {
        //alert("cant find frames");
        return true;
    }
    var form = contentFrame.document.forms[0];
    if (!form){
        //alert("no form:"+contentFrame);
        return true;
    }
    hlid = form.elements["highlightid"];
    if (!hlid) {
        //alert("no highligh id");
        return true;
    }
    //alert("hlid="+hlid.value);
    if (treeFrame.highlightNode) {
        var path = hlid.value;
        if (path == null)
            return true;
        var arr = path.split(".");
        if (arr == null)
            return true;

        treeFrame.clearAllHighlight();
        treeFrame.highlightNode(arr[0]);

        var i = 1;
        while ((i < arr.length) && (treeFrame.highlightParentNode(arr[i]) == false)) {
            i++
        }
    }
    return true;
}

/*===========================================================================*/
/* Update "restartRequired" in Header */
/*===========================================================================*/
function synchronizeRestartRequired(current) {
    var headerFrame = findFrame("header");
    if (headerFrame == null)
        return true;
    var doc = headerFrame.document;
    if (doc == null)
        return true;
    var form = doc.forms[0];
    if (form == null)
        return true;
    var field = form.elements["Header2.restartRequired"];
    if (field == null) {
        field = form.elements["Header.restartRequired"];
        if (field == null)
        return true;
    }
    if (current != field.value) {
        parent.parent.frames["header"].location.reload();
        parent.frames["index"].location.reload();
    }
    return true;
}

/*===========================================================================*/
/* commonHtmlHeaderOnLoad */
/*===========================================================================*/
function commonHtmlHeaderOnLoad(currentRestartStatus) {
    synchronizeLeft();
    synchronizeRestartRequired(currentRestartStatus);
    return true;
}


var currentHighlightNode = 0;    // remembers id of last highlighted node
var currentHighlightParent = 0;  // remembers id of last highlighted parent

function isLH21() {
    var contentFrame = findFrame( "index" );
    if (! contentFrame) {
        return true;
    }
    var form = contentFrame.document.forms[0];
    if (! form) {
        return true;
    }
    hidden = form.elements["isLH21"];
    if (! hidden) {
        return true;
    }
    return false;
}

function getNormalTreeTextColor() {
    if (isLH21()) {
        return "#3a2eb5";  // sun blue 
    } else {
        return "#003399";
    }
}

function getHighlightTreeBgColor() {
    if (isLH21()) {
        return "#FFFFFF";  // white
    } else {
        return "#CBDCAF";  // ~greenish color
    }
}

function getHighlightTreeTextColor() {
    return "#000000";  // black
}

function isNS4() {
    if (document.layers) 
        return true;
    else
        return false;
}

function onTreeNodeClick( id ) {
    //alert("onTreeNodeClick");
    // first highlight is as a parent
    // when the left frame loads the nodes will be hightlighted correctly
    clearAllHighlight();
    highlightParentNode(id);

    return true;	// necessary for on click handler to proceed with hyperlink
}


function updateHighlight() {
    if (currentHighlightNode) {
        var treeFrame= findFrame( "index" );
        if (treeFrame) {
            treeFrame.highlightNode(currentHighlightNode);
            treeFrame.highlightParentNode(currentHighlightParent);
        }
    } else {
        synchronizeLeft();
    }
    return true;
}

function clearHighlight(id) {
    if (id == 0) {
        return true;
    }
    if (isNS4()) {
        return true; // NS4clearHighlight(id);
    }
    var cell = document.getElementById("c"+id);
    if (cell) {
        cell.style.backgroundColor="transparent";
        cell.style.fontWeight = "normal";
        cell.style.color = getNormalTreeTextColor();

        cell = document.getElementById("a"+id);
        if (cell) {
            cell.style.color = getNormalTreeTextColor();
        }
    }
    cell = document.getElementById("r"+id);
    if (cell) {
        cell.style.fontWeight = "normal";
        cell.style.color = getNormalTreeTextColor();

        cell = document.getElementById("a"+id);
        if (cell) {
            cell.style.color = getNormalTreeTextColor();
        }
    }
    return true;
}

function clearAllHighlight() {
    clearHighlight(currentHighlightNode);
    currentHighlightNode = 0;

    clearHighlight(currentHighlightParent);
    currentHighlightParent = 0;

    return true;
}

function highlightNode(id) {
    if (id == 0) {
        return true;
    }
    if (isNS4()) {
        return true; // NS4highlightNode(id);
    }
    var cell = document.getElementById("c"+id);
    if (cell) {
        cell.style.backgroundColor = getHighlightTreeBgColor();
        cell.style.fontWeight = "bold";
        cell.style.color = getHighlightTreeTextColor();
        currentHighlightNode= id;

        cell = document.getElementById("a"+id);
        if (cell) {
            cell.style.color = getHighlightTreeTextColor();
        }
        return true;
    }    
    cell = document.getElementById("r"+id);
    if (cell) {
        cell.style.fontWeight = "bold";
        cell.style.color = getHighlightTreeTextColor();
        currentHighlightNode = id;

        cell = document.getElementById("a"+id);
        if (cell) {
            cell.style.color = getHighlightTreeTextColor();
        }
        return true;
    }
    return false;
}

function highlightParentNode(id) {
    if (isNS4()) { // don't do anything for NS4
        return true;
    }
    if (id == 0)
        return true;
    var cell = document.getElementById("c"+id);
    if (cell) {
        cell.style.fontWeight = "bold";
        currentHighlightParent = id;
        return true;
    }    
    cell = document.getElementById("r"+id);
    if (cell) {
      cell.style.fontWeight = "bold";
      currentHighlightParent = id;
      return true;
    }
    return false;
}

/*
Icon highlighting is no longer supported for NS4.
Lockhart doesn't even support NS4

function NS4clearHighlight(id) {
    img = document.images["i"+id];
    if (img) {
        //alert("have img "+img.src);
        img.src= unhighlightedIconFN( img.src )
        //alert("new  img "+img.src);
    }
    return true;
}

function NS4highlightNode(id) {
    var img = document.images["i"+id];
    if (img) {
        //alert("have img "+img.src);
        img.src=highlightedIconFN(img.src, "00");
        //alert("new  img "+img.src);
        currentHighlightNode = id;
        return true;
    }
    return true;
}


function highlightedIconFN( currentUnhighlightedFN, extension ) {
  pathcomponents= currentUnhighlightedFN.split("/");
  fn= pathcomponents.pop();
  path= pathcomponents.join("/");
  filecomponents= fn.split( "." );
  if ( filecomponents.length != 2 ) {
    return currentUnhighlightedFN;  // just in case
  }
  //alert("return: "+path+"/"+filecomponents[0]+"_"+extension+"."+filecomponents[1]);
  return path+"/"+filecomponents[0]+"_"+extension+"."+filecomponents[1];
}

function unhighlightedIconFN( currentHighlightedFN ) {
  pathcomponents= currentHighlightedFN.split("/");
  fn= pathcomponents.pop();
  path= pathcomponents.join("/");
  filecomponents= fn.split( "." );
  if ( filecomponents.length != 2 ) {
    return currentHighlightedFN;
  }
  parts = filecomponents[0].split("_");
  if (parts.length != 2) {
    return currentHighlightedFN;
  }
  //alert("return:: "+ path+"/"+parts[0]+"."+filecomponents[1]);
  return path+"/"+parts[0]+"."+filecomponents[1];
}
*/
/////////////////////////////////////////////////////
// Value checking functions

function isInCharSet(str, charSet) {
    var i;
    for (i = 0; i < str.length; i++) {
        var c = str.charAt(i);
        if (charSet.indexOf(c) < 0) {
            return false;
        }
    }
    return true;
}

function checkForNumericValue(formField) {
    var result = (formField.value != '') && isInCharSet(formField.value, "0123456789.");
    if (result == false) formField.select();
    return result;
}

function checkForIntValue(formField) {
    var result = (formField.value != '') && isInCharSet(formField.value, "0123456789");
    if (result == false) formField.select();
    return result;
}

function checkObjectName(formField) {
    var result = (formField.value != '') && 
        isInCharSet(formField.value, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.");
    if (result == false) formField.select();
    return result;
}

function checkForNumericValueOrEmpty(formField) {
    if (formField.value == '')
        return true;
    return checkForNumericValue(formField);
}

function checkNumbericRange(min, max, value) {
    var num = 0 + value;
    if (num < min || num > max)
        return false;
    return true;
}

function checkValidPortNumber(formField) {
    if (formField.value != null && formField.value.indexOf('$') == 0)
        return true;
    if (checkForIntValue(formField) == false)
        return false;
    var result = checkNumbericRange(0, 65536, formField.value);
    if (result == false) formField.select();
    return result;
}

function checkForNonZeroNumericValue(formField) {
    if (checkForIntValue(formField) == false)
        return false;
    var num = 0 + formField.value;
    var result = (num != 0);
    if (result == false) formField.select();
    return result;
}

function checkForValue(formField) {
    var result = (formField.value != '') && (isWhitespace(formField.value) == false);
    if (result == false) formField.select();
    return result;
}

function checkPasswords(formField) {
    // the formField is the 'confirm password'.
    var result = checkForValue(formField);
    if (result == false) return false;
    
    // check the password and the confirm password to make sure they are the same.
    var passwd = document.getElementById("password");
    if (passwd.value != formField.value) {
        formField.select();
        return false;
    }
    return true;
}

function checkForSelectedValue(formField) {
    var index = formField.selectedIndex;
    if (formField.options[index].value == '' || isWhitespace(formField.options[index].value)) {
        return false;
    }

    return true;
}

/////////////////////////////////////////////////////
// The following functions are used by the HTTP and IIOP listener pages.

function ps_CheckRequiredFieldsPlus() {
    return ps_CheckRequiredFields() && checkSSL();
}

function setSSLChecks(checkBoxItem) {
    if (checkBoxItem.checked==false) {
        document.getElementById("ssl1").disabled=true;
        document.getElementById("ssl2").disabled=true;
        document.getElementById("ssl4").disabled=true;
        document.getElementById("ssl5").disabled=true;
        document.getElementById("ssl25").disabled=true;
        disableCipherCheckboxes();
    } else  {
        document.getElementById("ssl1").disabled=false;
        document.getElementById("ssl2").disabled=false;
        document.getElementById("ssl4").disabled=false;  // ssl3
        document.getElementById("ssl5").disabled=false;  // tls

        if ((document.getElementById("ssl25").checked==true) || 
            (document.getElementById("ssl4").checked==false && 
             document.getElementById("ssl5").checked==false)) {
                disableCipherCheckboxes();
        } else {
                enableCipherCheckboxes()} 

        if (document.getElementById("ssl4").checked==true || 
            document.getElementById("ssl5").checked==true) {
                document.getElementById("ssl25").disabled=false;
        } else {
                document.getElementById("ssl25").disabled=true;
        }
    }
    return true;
}

function setNewSSLChecks(checkBoxItem) {
/*
    if (checkBoxItem.checked==false) {
        document.getElementById("ssl1").disabled=true;
        document.getElementById("ssl2").disabled=true;
        document.getElementById("ssl4").disabled=true;
        document.getElementById("ssl5").disabled=true;
        document.getElementById("ssl25").disabled=true;
        disableCipherCheckboxes();
    } else  {
        document.getElementById("ssl1").disabled=false;
        document.getElementById("ssl2").disabled=false;
        document.getElementById("ssl4").disabled=false;  // ssl3
        document.getElementById("ssl5").disabled=false;  // tls

        document.getElementById("ssl4").checked=true;
        document.getElementById("ssl5").checked=true;
        document.getElementById("ssl25").disabled=false;
        document.getElementById("ssl25").checked=true;

        disableCipherCheckboxes();
    }
*/
    return true;
}


// based on whether cert nickname is entered.
function checkSSL() {
    var result = checkForValue(document.getElementById("ssl2")); //cert nickname
    if (result == true) {
        if ((document.getElementById("ssl4").checked==true || 
             document.getElementById("ssl5").checked==true) &&
            document.getElementById("ssl14").checked==false &&
            document.getElementById("ssl15").checked==false &&
            document.getElementById("ssl16").checked==false &&
            document.getElementById("ssl17").checked==false &&
            document.getElementById("ssl18").checked==false &&
            document.getElementById("ssl19").checked==false &&
            document.getElementById("ssl20").checked==false &&
            document.getElementById("ssl21").checked==false &&
            document.getElementById("ssl25").checked==false)
        {
            //alert("You must enable one or more Ciphers for SSL3 / TLS.");
            //alert(getHiddenValue("ssl.enableCipher"));
            alert(getHiddenValue("ssl.mustEnable"));
            return false;
        }
    }else{
        if (document.getElementById("ssl4").checked==true || document.getElementById("ssl5").checked==true){
            alert(getHiddenValue("ssl.certRequired"));
            return false;
        }
    }
    return true;
}

function disableCipherCheckboxes() {
    document.getElementById("ssl14").disabled=true;
    document.getElementById("ssl15").disabled=true;
    document.getElementById("ssl16").disabled=true;
    document.getElementById("ssl17").disabled=true;
    document.getElementById("ssl18").disabled=true;
    document.getElementById("ssl19").disabled=true;
    document.getElementById("ssl20").disabled=true;
    document.getElementById("ssl21").disabled=true;
}

function enableCipherCheckboxes() {
    document.getElementById("ssl14").disabled=false;
    document.getElementById("ssl15").disabled=false;
    document.getElementById("ssl16").disabled=false;
    document.getElementById("ssl17").disabled=false;
    document.getElementById("ssl18").disabled=false;
    document.getElementById("ssl19").disabled=false;
    document.getElementById("ssl20").disabled=false;
    document.getElementById("ssl21").disabled=false;
}



function checkGuaranteeIsolation (transactionIsolation) {
    if(transactionIsolation.value == '' ||  isWhitespace(transactionIsolation.value)) {
	document.getElementById("guaranteeIsolation").disabled=true;
    }
    else {
	document.getElementById("guaranteeIsolation").disabled=false;
    }
}

/**
 *  This function returns true if the key exists w/i the given array.
 */
function arrayContainsKey(arr, key) {
    for (idx = 0; idx<arr.length; idx++) {
	if (arr[idx] == key) {
	    return true;
	}
    }
    return false;
}

/**
 *  This function invokes ps_CheckRequiredFields if the given event was
 *  targetted at a HREF or Button.  This function should not be called
 *  directly.  It will normally be called from an onClick event as setup by
 *  the setRequiredFieldDocumentOnclick() function.
 */
function invokeRequiredFieldCheck(eventObject) {
    if (eventObject == null) {
	// IE doesn't pass in eventObject
	if (event == null) {
	    alert("Click occurred, however, there is no associated "+
		  "JavaScript event object!  Browser is not supported, "+
		  "or this is a bug.");
	} else {
	    eventObject = event;
	}
    } else {
	// Only needed for some Nescape versions, otherwise over-riden
	// onClick() will not be invoked
	routeEvent(eventObject);
    }
    target = eventObject.target;
    if (target == null) {
	target = eventObject.srcElement;
    }
    // Make sure we should validate this target
    // First check the skip list
    if (arrayContainsKey(getButtonsToSkipOnclick(), target.name)) {
	return true;
    }

    retVal = true;
    // Make sure we have a button or href
    if ((target.type == "submit") || (target.type == "button") ||
	    (target.type == "image")) {
	// Button
	retVal = ps_CheckRequiredFields(eventObject);
    } else {
	if ((target.href != null) || ((target.parentNode != null) &&
		(target.parentNode.href != null))) {
	    // HREF
	    retVal = ps_CheckRequiredFields(eventObject);
	}
    }
    return retVal;
}

/**
 *  Array of button/href field to skip for onclick.
 */
var _skipButtons = new Array();

/**
 *  This function returns the array of button names to skip the execution of
 *  the required field check for the onClick event.
 */
function getButtonsToSkipOnclick() {
    return _skipButtons;
}

/**
 *  This function sets the onclick for every HREF / Button the page to
 *  invoke ps_CheckRequiredFields (if defined).
 */
function setRequiredFieldDocumentOnclick() {
    if (this.ps_CheckRequiredFields == null) {
	return;
    }

    // Do not re-order the following lines, Netscape 7.0 will have problems.
    document.onclick=invokeRequiredFieldCheck;
    if (document.releaseEvents != null) {
	// I know this is supposed to take an argument, but if done this way
	// N7.0 works.  Don't add the Event.CLICK argument, unless you find
	// another way to prevent N7.0 from invoking onClick 2x
	document.releaseEvents();
    }

    // Set the document onclick()
    document.captureEvents(Event.CLICK);
}

/******************************************************************************
OBSOLETE

//======================================================================
// LTrim: Returns a String containing a copy of a specified 
//        string without leading spaces 
//======================================================================
function LTrim(String)
{
    var i = 0;
    var j = String.length - 1;

    if (String == null)
            return (false);

    for (i = 0; i < String.length; i++)
    {
        if (String.substr(i, 1) != ' ' &&
            String.substr(i, 1) != '\t')
                break;
    }

    if (i <= j)
        return (String.substr(i, (j+1)-i));
    else
        return ('');
}

//======================================================================
// RTrim: Returns a String containing a copy of a specified 
//        string without trailing spaces 
//======================================================================
function RTrim(String)
{
    var i = 0;
    var j = String.length - 1;

    if (String == null)
        return (false);

    for(j = String.length - 1; j >= 0; j--)
    {
        if (String.substr(j, 1) != ' ' &&
                String.substr(j, 1) != '\t')
        break;
    }

    if (i <= j)
        return (String.substr(i, (j+1)-i));
    else
        return ('');
}

//======================================================================
// RTrim: Returns a String containing a copy of a specified 
//        string without both leading and trailing spaces 
//======================================================================
function Trim(String)
{
    if (String == null)
        return (false);

    return RTrim(LTrim(String));
}

//======================================================================
// validateForWhiteSpaces: Returns  false if the field contains leading/tailing white spaces 
//====================================================================== 

function validateForWhiteSpaces(formField,fieldLabel)
{
    var result = true;

    if (Trim(formField.value) != "" && formField.value != Trim(formField.value))
    {      
        alert("No leading/tailing white spaces allowed in "+ fieldLabel);
        formField.focus();		
        result = false;
    }

    return result;
}

function isEmailAddr(email)
{
    var result = false;
    var theStr = new String(email);
    var index = theStr.indexOf("@");
    if (index > 0)
    {
        var pindex = theStr.indexOf(".",index);
        if ((pindex > index+1) && (theStr.length > pindex+1))
            result = true;
    }
    return result;
}

*/
//
