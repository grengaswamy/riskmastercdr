@echo off
REM
REM Copyright 2004-2005 Sun Microsystems, Inc. All rights reserved.
REM Use is subject to license terms.

setlocal
call C:\Sun\AppServer\config\asenv.bat
set first_arg=%1
shift
set second_arg=%1
shift
set third_arg=%1
shift
set PATH=%first_arg%;%second_arg%;%PATH%
call %third_arg%\pk12util.exe %*
