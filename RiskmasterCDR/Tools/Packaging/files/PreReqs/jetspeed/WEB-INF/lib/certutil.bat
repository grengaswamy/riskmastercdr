@echo off
REM
REM Copyright 2004-2005 Sun Microsystems, Inc. All rights reserved.
REM Use is subject to license terms.

setlocal
call C:\Sun\AppServer\config\asenv.bat
set PATH=%6;%7;%PATH%
call %8\certutil.exe %1 %2 %3 %4 %5
