<%@ include file="header.jsp" %>
<body>
<p><b>You have been logged in successfully.</b></p>
<table>
<tr>
 <td colspan="2">
 <center>
 <font color="red" face="Arial,Helvetica">
  <i><b>For security reasons, quit your web browser when you are done
  accessing services that require authentication!</b></i>
 </font>
 </center>
 </td>
</tr>
</table>
</body>
<%@ include file="footer.jsp" %>
