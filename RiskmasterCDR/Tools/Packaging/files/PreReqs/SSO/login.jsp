<%@ include file="header.jsp" %>
<SCRIPT LANGUAGE = "JavaScript">
<!--  Submit to logout    
function logout() {
	document.login_form.action="logout";
	document.login_form.submit();
}
<!--  Submit to login
function login() {
<!--	document.login_form.action="login";
	document.forms.login_form.username.value = document.forms.login_form.username.value.toUpperCase();
<!--	alert("Username:" + document.forms.login_form.username.value);
	document.login_form.submit()
}
//-->
</SCRIPT>
<body onLoad="document.forms.login_form.username.focus()"> 
<div align="left">
<table border="0" cellpadding="0" cellspacing="0"
	style="border-collapse: collapse" bordercolor="#111111" width="600"
	id="AutoNumber1">
	<tr>
		<td width="7%">&nbsp;</td>
		<td colspan="3" style="padding-bottom: 20; padding-top: 20">
		<p class="MsoNormal"><font face="Verdana" size="2">Welcome,&nbsp;</font></p>
		<p class="MsoNormal"><font face="Verdana" size="2">You are about to
		log into a CSC secure system. Once your ID and password have been
		accepted, this single login will eliminate the need for any further
		security code entry.</font></p>
		<p class="MsoNormal"><font face="Verdana" size="2">If you have reached
		this page by mistake, or do not have access rights to this system,
		please click the Cancel button below.</font>
		</td>
		<td width="8%">&nbsp;</td>
	</tr>
</table>		<form method=post name="login_form">  <table>
	<tr>
		<td style="padding-top: 15" width="7%">&nbsp;</td>
		<td style="padding-top: 15" valign="top" width="10%">&nbsp;</td>
		<td style="padding-top: 15" valign="top"><b> <font face="Verdana"
			size="2">User ID:</font></b></td>
		<td style="padding-top: 15" valign="top"><input type="text" tabindex="1"
			name="username" size="30"></input> <font face="Verdana" size="1"> <a
			href="loginAssist.html">I forgot my User ID</a></font></td>
		<td style="padding-top: 15" width="8%">&nbsp;</td>
	</tr>
	<tr>
		<td style="padding-top: 10" width="7%">&nbsp;</td>
		<td style="padding-top: 10" valign="top" width="10%">&nbsp;</td>
		<td style="padding-top: 10" valign="top"><b> <font face="Verdana"
			size="2">Password:</font></b></td>
		<td style="padding-top: 10" valign="top"><input type="password" tabindex="2"
			name="password" size="30"></input> <font face="Verdana" size="1"> <a
			href="loginAssist.html">I forgot my Password</a></font></td>
		<td style="padding-top: 10" width="8%">&nbsp;</td>
	</tr>
	<tr>
		<td style="padding-top: 15" width="7%">&nbsp;</td>
		<td style="padding-top: 30" valign="top" width="10%">&nbsp;</td>
		<td style="padding-top: 30" valign="top">&nbsp;</td>
		<td style="padding-top: 20" valign="top">
		<p align="left"></p>
		<INPUT type="submit" name="Login" value="Login" tabindex="3" onClick="login()"
		style="font-family: Verdana; font-size: 10pt; width: 80px; color: #FFFFFF; background-color: #990000"></INPUT>&nbsp;
		<INPUT onClick="logout()" type="button" name="Cancel" value="Cancel" tabindex="4"
		style="font-family: Verdana; font-size: 10pt; width: 80px; color: #FFFFFF; background-color: #990000"></INPUT>
		</td>
		<td style="padding-top: 15" width="8%">&nbsp;</td>
	</tr>
</table>
	</form>
</div>
<p>
<% if (request.getAttribute("edu.yale.its.tp.cas.badUsernameOrPassword") 
       != null) { %>
<font color="red">Sorry, you entered an invalid UserID or Password. <br />
  Please try again. 
</font>
<% } else if (request.getAttribute("edu.yale.its.tp.cas.service") == null) { %>
  You may establish authentication now in order to access protected
  services later.
<% } else { %>
  You have requested access to a site that requires authentication. 
<% } %>
</p>
</body>
<%@ include file="footer.jsp" %>
