<%@ page session="false" %>
<%@ page import="java.lang.*"%>
<html>
<head>
	<title>CSC Authentication Service</title>
</head>

<%  
  String serviceId = (String) request.getAttribute("serviceId");
  String token = (String) request.getAttribute("token");
  String uid = (String) request.getParameter("username");  
  try 
  {
	  if (uid==null) uid = (String) request.getAttribute("username");	  
  }
  catch(Exception e)
  {
  
  }
  //String uid = (String) request.getAttribute("username");
  String service = null;
  String qstring =  (String) request.getAttribute("querystring");
  if (serviceId.indexOf('?') == -1)
    service = serviceId + "?casticket=" + token;
  else
    service = serviceId + "&casticket=" + token;
  if(qstring != null && qstring.length() > 0)
    {
     service = service + "&" + qstring;
     }
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\n", "");
  service = 
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\r", "");
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\"", "");
	System.out.println("New goService URL =" + service);
	
	int loc =service.indexOf("username")	;
	if (loc>0)
	{			
		//MITS 12635 : Raman & Rahul: we need to remove full username from querystring hence we should use substring to be sure
		//service=service.replaceAll("&username=[a-zA-Z0-9]*","");	
		service=service.substring(0,(loc - 1));
	}
%>
<body>
	<form id='RMXFrmLogin' name='' method='post'>
        <input id='username' name='username' value='' type='hidden' />
    </form>
 <script>
	document.RMXFrmLogin.action = "<%= service %>";
    document.getElementById('username').value="<%=uid%>";    
    document.RMXFrmLogin.submit();
	
 </script>



 <noscript>
  <p>Login successful.</p>
  <p>
   Click <a href="<%= service %>" />"here</a>
   to access the service you requested.
  </p>
 </noscript>
</body>

</html>
