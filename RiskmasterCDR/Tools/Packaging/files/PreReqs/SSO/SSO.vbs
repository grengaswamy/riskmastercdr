'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.0
'
' NAME: SSO.vbs
'
' AUTHOR: CSC , Computer Sciences Corporation (CSC)
' DATE  : 08/14/2007
'
' COMMENT: VBScript to detect the Java Installation path and then pass 
' that value as a parameter to the batch file required for configuring SSO
'
'==========================================================================
Dim strComputer

'Set the value as the local computer
strComputer = "."

'Stop the Apache Tomcat service
Call StopService(strComputer, "Apache Tomcat")

'Prompt for the URL values
strURLValue = InputBox("Please enter the current URL", "Current URL", "http://localhost")

'Check the input box value
CheckInput(strURLValue)

strReplaceValue = InputBox("Please enter the desired URL", "Desired URL", "http://" & LCase(GetComputerName()))

'Check the input box value
CheckInput(strReplaceValue)

'Call the UpdateFileContents function to replace the required URL
Call UpdateFiles(strURLValue, strReplaceValue)

'Start the Apache Tomcat service
Call StartService(strComputer, "Apache Tomcat")

'Wait a few seconds before the browser can display the page
WScript.Sleep 5000

'Open the browser to test the SSO installation
If (ContainsString(strReplaceValue, "https://")) Then
	'Open the browser with the specified URL
	OpenBrowser strReplaceValue & "/SSO/login"
Else
	'Open the browser to test the SSO installation
	OpenBrowser strReplaceValue & ":8080/SSO/login"
End If

'Subroutine to check the input value from the InputBox
Sub CheckInput(strInputBoxValue)
	'If the input box value is an empty String
	If (strInputBoxValue = "") Then
		'Abort the execution of the script
		WScript.Quit
	End If
End Sub

'Function to retrieve the local computer name
Function GetComputerName()
	Dim SWBemlocator, objWMIService
	Const IS_WORKGROUP_MEMBER = 2
	Const IS_DOMAIN_MEMBER = 3
	Const IS_BACKUP_DOMAIN_CONTROLLER = 4
	Const IS_PRIMARY_DOMAIN_CONTROLLER = 5

	Set SWBemlocator = CreateObject("WbemScripting.SWbemLocator")
	Set objWMIService = SWBemlocator.ConnectServer(strComputer,"root\CIMV2",UserName,Password)
	Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48)
	For Each objItem in colItems
		'If the server is a member of a workgroup
		If (objItem.DomainRole = IS_WORKGROUP_MEMBER) Then
			'Obtain the DNS Host name of the computer
			strComputerName = objItem.DNSHostName
		ElseIf (objItem.DomainRole = IS_DOMAIN_MEMBER) Then
			'Obtain the FQDN name of the computer
			strComputerName = objItem.DNSHostName & "." & objItem.Domain
		ElseIf (objItem.DomainRole = IS_BACKUP_DOMAIN_CONTROLLER Or objItem.DomainRole = IS_PRIMARY_DOMAIN_CONTROLLER) Then
			MsgBox "Riskmaster X cannot be installed on a Domain Controller.  Please install on another server."
			WScript.Quit
		End If
	Next
	
	'Clean up
	Set SWBemlocator = Nothing
	Set objWMIService = Nothing
	
	'Return the Host Computer Name as the value of the function
	GetComputerName = strComputerName
End Function

'Retrieves the location of the java.security file installed on the system
'02/20/2007  SSV Patched to resolve the new issue with JDK 1.5.0.11
Function GetJavaInstallPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Const vbOKOnly = 0

	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Java 1.5
	strKeyPath = "SOFTWARE\JavaSoft\Java Development Kit\1.5"
	'Read out the JavaHome registry key
	strEntryName = "JavaHome"
	
	'Retrieve the path to the Java Home directory from the Registry
	objReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strEntryName,strValue
	
	'If the JavaHome directory has not been properly set
	If (strValue = "1") Then
		Dim strMsg, strInputPrompt 
		strMsg = "It seems that the JavaHome directory has not been properly set. " & VbCrLf & "Please configure this value manually."
		
		'Display an error message to the user
		MsgBox  strMsg, vbOKOnly, "No JavaHome Directory"
		
		strInputPrompt = "Please enter the full path to the JDK directory " & VbCrLf & "Ex: C:\Program Files\Java\jdk1.5.0_11"
		
		'Prompt for the value of the JavaHome directory
		strValue = InputBox(strInputPrompt, "JDK Path")
		
		'Set the proper JavaHome in the registry
		objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strEntryName,strValue
	End If
	
	'Obtain the path to the Java Security file
	strJavaSecurityPath = strValue & "\jre\lib\security\java.security"
	
	'Clean up
	Set objReg = Nothing
	
	'Return the Java Security path as the value of the function
	GetJavaInstallPath = strJavaSecurityPath
End Function

'Retrieves the installation location of Apache Tomcat
Function GetTomcatInstallPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002

	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Apache Tomcat
	strKeyPath = "SOFTWARE\Apache Software Foundation\Tomcat\5.0"
	'Read out the Tomcat Installation Path
	strEntryName = "InstallPath"
	
	'Retrieve the path to the Java Home directory from the Registry
	objReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strEntryName,strValue
		
	'Clean up
	Set objReg = Nothing
	
	'Return the Tomcat installation path as the value of the function
	GetTomcatInstallPath = strValue
End Function

'Function to retrieve the extracted path for SSO installations
Function GetPath()
	' Return path to the current script
	Dim path
	path = WScript.ScriptFullName  ' script file name
	GetPath = Left(path, InstrRev(path, "\"))
End Function

'Function to determine whether the specific string 
'contains a specified String
Function ContainsString(strExpression, strSearchExpression)
	'If the specified string contains the value of http or https
	If InStr(strExpression, strSearchExpression) > 0 Then
		'Indicate that the string contains the specified search expression
		ContainsString = True
	Else
		'Indicate that the string does not contain the specified search expression
		ContainsString = False
	End If
End Function

'Script to automatically start
'a specified Windows Service using WMI
'NOTE: Display Service Name is case sensitive
Sub StartService(strComputer, strServiceName)
	On Error Resume Next
	
	Dim objWMIService, objItem, objService
	Dim colListOfServices, strService
	
	'strService is case sensitive.
	strService = " '" & strServiceName & "' "
	Set objWMIService = GetObject("winmgmts:" _
	& "{impersonationLevel=impersonate}!\\" _
	& strComputer & "\root\cimv2")
	Set colListOfServices = objWMIService.ExecQuery _
	("Select * from Win32_Service Where DisplayName ="_
	& strService & " ")
	
	'Loop through the list of Windows Services
	For Each objService in colListOfServices
		'If the service is currently running
		If objService.State = "Stopped" Then
			'Stop the specified Windows Service
			objService.StartService()
		End If
	Next 
End Sub

'Script to automatically stop
'a specified Windows Service using WMI
'NOTE: Display Service Name is case sensitive
Sub StopService(strComputer, strServiceName)
	On Error Resume Next
	
	Dim objWMIService, objItem, objService
	Dim colListOfServices, strService
	
	'strService is case sensitive.
	strService = " '" & strServiceName & "' "
	Set objWMIService = GetObject("winmgmts:" _
	& "{impersonationLevel=impersonate}!\\" _
	& strComputer & "\root\cimv2")
	Set colListOfServices = objWMIService.ExecQuery _
	("Select * from Win32_Service Where DisplayName ="_
	& strService & " ")
	
	'Loop through the list of Windows Services
	For Each objService in colListOfServices
		'If the service is currently running
		If objService.State = "Running" Then
			'Stop the specified Windows Service
			objService.StopService()
		End If
	Next 
End Sub


'Open the browser to test the SSO Installation
Sub OpenBrowser(strURL)
	Dim WshShell
	
	'Create the Windows Scripting Host object
	Set WshShell = CreateObject("WScript.Shell")
	
	'Run the following command to open the browser with the specified URL
	WshShell.Run "iexplore.exe " & strURL, 3, False
	
	'Clean up
	Set WshShell = Nothing
End Sub

'Performs a string replacement using Regular Expressions
Function RegExReplace(strRegExprPattern, strSearchText, strReplaceValue)
	Dim objRegEx
	Dim strReplaceText
	
	'Create the Regular Expression object
	Set objRegEx = CreateObject("VBScript.RegExp")
	
	objRegEx.Global = True
	objRegEx.Pattern = strRegExprPattern
	objRegEx.IgnoreCase = True
	
	'Perform the string replacement using Regular Expressions
	strReplaceText = objRegEx.Replace(strSearchText, strReplaceValue)
		
	'Clean up
	Set objRegEx = Nothing
	
	RegExReplace = strReplaceText
End Function

'Reads the contents of a specified file
Function ReadFile(strFileName)
	Dim objFSO, objFile, objReadFile
	Dim strFileText
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	If (Not objFSO.FileExists(strFileName)) Then
		MsgBox "File path does not exist: " & strFileName
	End If
	'Obtain a handle on the file for reading
	Set objReadFile = objFSO.GetFile(strFileName)
	
	'Begin reading the contents of the file
	Set objFile =objReadFile.OpenAsTextStream(ForReading)
	
	'Read in the entire contents of the file
	strFileText = objFile.ReadAll
	
	'Close the file from further reading
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objReadFile = Nothing
	Set objFile = Nothing
	
	ReadFile = strFileText
End Function

'Writes out the specified text/contents to a specified file
Sub WriteFile(strFileName, strFileText)
	Dim objFSO, objFile, objWriteFile
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Obtain a handle on the file for reading
	Set objWriteFile = objFSO.GetFile(strFileName)
	
	'Open the file for writing
 	Set objFile = objWriteFile.OpenAsTextStream(ForWriting)

	'Write out the updated contents of the file
	objFile.WriteLine strFileText	

	'Close the file from further writing
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objWriteFile = Nothing
	Set objFile = Nothing
End Sub

'Function to retrieve an array containing all of the relevant file paths
'that are required for editing
Sub UpdateFiles(strSearchValue, strReplaceValue)
	Dim strJavaSecurityPath
	Dim strSSOPath, strConfFilePath, strAuthReqPath, strJavaLoginSSOPath
	Dim strComputer
	Dim WshShell
	Const AUTH_REQ_PATH = "C:\\Program Files\\Apache Software Foundation\\Tomcat 5.0\\webapps\\SSO\\META-INF\\auth-req.xml"
	Const DefaultJavaLoginPath = "#login.config.url.1=file:${user.home}/.java.login.config"
	Const JavaLoginPath = "login.config.url.1=file:"
	
	'Get the SSO Path
	strSSOPath = GetPath()

	'Set the computer to the local computer
	strComputer = "."
	
	'Assign the required paths for the files to be edited
	strConfFilePath = strSSOPath & "META-INF\cas_jaas.conf"
	strAuthReqPath = strSSOPath & "META-INF\auth-req.xml"
	
	'Replace the existing authorization request path with 2 backslashes
	'as the required escape sequence required for the Java file
	strAuthReqPath = Replace(strAuthReqPath, "\", "\\")
	
	'Get the Java Security Path
	strJavaSecurityPath = GetJavaInstallPath(strComputer)
	
	'Replace the existing authorization request path with 2 backslashes
	'as the required escape sequence required for the Java file
	strJavaLoginSSOPath = Replace(JavaLoginPath & strConfFilePath, "\", "\\")
	
	'Call the UpdateFileContents function in order to replace the existing path
	Call UpdateFileContents(strConfFilePath, AUTH_REQ_PATH, strAuthReqPath)

	'Call the UpdateFileContents function in order to replace the URL path
	Call UpdateFileContents(strConfFilePath, strSearchValue, strReplaceValue)
	
	'Call the StringReplace function in order to replace the path to SSO
	'within the java.security file
	Call StringReplace(strJavaSecurityPath, DefaultJavaLoginPath, strJavaLoginSSOPath)
End Sub

'Perform a string replacement on a specified string in a file
Sub UpdateFileContents(strFileName, strSearchValue, strReplaceValue)
	Dim strOriginalFileText, strReplacementFileText
	
	'replace all each \ with \\ for regexp pattern to work properly
	'mpalinski 11/30/07
	strSearchValue = Replace(strSearchValue, "\", "\\")
		
	strOriginalFileText = ReadFile(strFileName)
			
	strReplacementFileText = RegExReplace(strSearchValue, strOriginalFileText, strReplaceValue)
	
	Call WriteFile(strFileName, strReplacementFileText)
End Sub

'Perform a string replacement on a specified string in a file
Sub StringReplace(strFileName, strSearchValue, strReplaceValue)
	Dim objFSO, objFile, objReadFile
	Dim strFileText, strNewText, strMsg

	'Declare and assign the necessary constants for file processing	
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8

	'Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Check if the file exists on the File System
	If (objFSO.FileExists(strFileName)) Then
	      strMsg = strFileName & " has been updated successfully with " & strProperty
	   Else
	      strMsg = strFileName & " doesn't exist.  Please check your configuration settings."
	      MsgBox strMsg
	      Exit Sub
	 End If
	
	'Obtain a handle on the file for reading
	Set objReadFile = objFSO.GetFile(strFileName)
	
	'Begin reading the contents of the file
	Set objFile =objReadFile.OpenAsTextStream(ForReading)
	
	'Read in the entire contents of the file
	strFileText = objFile.ReadAll
	
	'Close the file from further reading
	objFile.Close
	
	'Get an updated string created by replacing the old string with the new string
	strNewText = Replace(strFileText, strSearchValue, strReplaceValue)
	
	'Open the file for writing
	Set objFile = objReadFile.OpenAsTextStream(ForWriting)

	'Write out the updated contents of the file
	objFile.WriteLine strNewText

	'Close the file from further writing
	objFile.Close

	'Clean up
	Set objFile = Nothing
	Set objReadFile = Nothing
	Set objFSO = Nothing
End Sub






