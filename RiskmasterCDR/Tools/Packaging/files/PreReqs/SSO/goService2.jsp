<%@ page session="false" %>
<%
  String serviceId = (String) request.getParameter("serviceId");
  String token = (String) request.getParameter("token");
  String service = null;
  if (serviceId.indexOf('?') == -1)
    service = serviceId + "?casticket=" + token;
  else
    service = serviceId + "&casticket=" + token;
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\n", "");
  service = 
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\r", "");
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\"", "");
	System.out.println("New goService URL =" + service);
%>
<html>
<head>
<title>CSC Authentication Service</title>
 <script>
  window.location.href="http://20.15.24.83:8080/jetspeed/portal;jsessionid=""?action=JLoginUser&username=SE31301";
 </script>
</head>

<body>
 <noscript>
  <p>Login successful.</p>
  <p>
   Click <a href="<%= service %>" />"here</a>
   to access the service you requested.
  </p>
 </noscript>
</body>

</html>
