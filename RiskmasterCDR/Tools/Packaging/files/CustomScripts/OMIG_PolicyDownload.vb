
'---------------------------------------------------------------------------------------
' Procedure : CCPPEProc_CoverageAfterSave
' DateTime  : 9/23/2004 16:40
' Author    : mbaum3
' Purpose   : Routine is used to pull additional coverage segments from rating for
'             homeowner policies.
'             Routine is only executed for specific line of business codes.
'             If additional codes are needed, need to add to the LOB code list below
'             If hits are found in rating segments, the original coverage segment that
'             was provided to the routine is replaced with the detailed segments from
'             rating.  If more detailed coverages are found for the stat row, the
'             additional coverage information is pointing to the STAT row is cloned.
'             This means that all additional coverages all report to the same statistical
'             row.
'             PS. Haven't done this before see we'll see what happens on upload to POINT
'
' NOTE: You must preserve the three fields of the legacy key.  However the legacy key
'       must be unique in the XML.  Any split of coverages to greate additional rows
'       must manage the legacy key appropriately.
'
'
'  PNT_MP_X_COV Table Layout	
'	
'	LOB	        Key Field
'	INSLINE	        Key Field
'	PRODUCT	        Key Field
'	MAJ_PERIL       Key Field
'	COVERAGE        Coverages to be combined into a single display line
'			Separate coverage codes with spaces
'			up to 256 characters may be defined (including spaces)
'	SCHED_ITEMS	Specifies that Scheduled Items are to be detailed for the coverages listed in COVERAGE
'			N or Blank:  Ignore Scheduled Item records (BZ00 file)
'			Y:  Create a display line for each associated sheduled item record in BZ00
'	LIMITS		Specify how to handle limits for each display line
'			0 :	Make limits zero (does not write limit lines to the XML).
'			H:	Combine limits from all associated coverage records (BY00) records writing the highest limit found
'			L:	Combine limits from all associated coverage records (BY00) records writing the lowest limit found
'			U:	Writes limits 1 and 2 from unit level (B500)
'			U1:	Writes limit 1 from unit level (B500)
'			U2:	Writes limit 2 from unit level (B500)
'			S:	Writes limits 1 and 2 from the statistical record
'			blank:  Write the limits found on the BY00 record.  If there are multiple BY00 records, only write the last one. 
'			any numeric value:  Writes that value to Limit 1 only
'	DEDUCTIBLES	Specify how to handle deductibles for each display line
'			0 :	Make deductible zero (does not write deductible lines to the XML).
'			H:	Combine deductibles from all associated coverage records (BY00) records writing the highest deductible found
'           H1:   Combine limit 1 from all associated coverage records (BY00) records writing the highest limit found
'           H2:   Combine limit 2 from all associated coverage records (BY00) records writing the highest limit found
'           L:    Combine limits from all associated coverage records (BY00) records writing the lowest limit found
'           L1:   Combine limit 1 from all associated coverage records (BY00) records writing the lowest limit found
'           L2:   Combine limit 2 from all associated coverage records (BY00) records writing the lowest limit found
'			U:	Writes deductible from unit level (B500)
'			S:	Writes deductible from the statistical record
'			P:  Deductible is a percentage.  Routine strips the % from the value. Must code the calculation in the script
'			Blank:  Write the deductible found on the BY00 record. If there are multiple BY00 records, only write the last one.
'			any numeric value:  Writes that value to deductible.
'
'			NOTE:  Any coverage that limits and / or deductibles are to be specially handeled must have an entry in PNT_MP_X_COV
'
' Processing:
'  1:  Read a record from PMSPSA15:  This is a major peril definition.
'
'  2:  Perform special processing for the major peril and exit routine.
'	   Major Perils 002 and 018:  Are split at the unit level, A-D and E-F only.
'
'  3:  Lookup the rating tranlate key from ASB1CPP (activity record).
'	   If not found, write coverage display at major peril level and exit
'
'  4:  Looks up all coverages associated with the major peril from PNT_MP_X_COV
'
'  5:  If no record set up in PNT_MP_X_COV 
'	If the ASB1CPP Coverage Code is '*ALL'
'	   Write display at the major peril level and exit
'
'	The coverage code from ASB1CPP is used to pull records from BY00 and BZ00.  
'	If there are BZ00 records, a display record is written for each BZ00 (scheduled item) record pulling the 
'   limits and deductibles from the BZ00 record.
'	If there are no BZ00 records, a display record is written for the BY00 record and limits and deductibles 
'   are written from the BY00 record.
'	This processing emulates the following options in PNT_MP_X_COV.
'		COVERAGE  =  Coverage code in ASB1CPP
'		SCHED_ITEMS = Y
'		LIMITS = blank
'		DEDUCTIBLES = blank
'
'	The advantage here is that for straight major peril to coverage code processing there does not need to 
'   be an entry in PNT_MP_X_COV.
'	This is ONLY VALID for a 1 to 1 correspondence between major peril and coverage code and the ASB1CPP 
'   record and PMSPSA15 has the coverage code written to them.
'	NOTE:  If the appropriate coverage code is NOT being written to ASB1CPP (a *ALL is present instead), 
'          then only the major peril is displayed in CP.
'
' 6:  For each corresponding record in PNT_MP_X_COV:
'	A coverage display in ClaimsPro will be created for each PNT_MP_X_COV row.
'	Read all BY00 record (coverage records) that correspond to the list of associated coverages in the COVERAGE field.
'	If SCHED_ITEM = N or blank, ignore entries in BZ00 (scheduled item records)
' 	   If LIMITS = :
'		0, remove all limit entries from the XML
'		H or L: Compare limits (if not zero), and write the appropriate one (see table definition above).
'		Any Other Value:  Write the last limit entry (if not zero) to the XML.
'	   If DEDUCTIBLES = :
'		0, remove all deductible entries from the XML
'		H or L: Compare deductible (if not zero), and write the appropriate one (see table definition above).
'		Any Other Value:  Write the last deductible entry if not zero to the XML.
'	If SCHED_ITEM = Y write a display line for each scheduled item entry along with its deductible and limit.
'
'	This processing supports coverage exception processing for major perils. 
'       Primarily coverages that require special handeling such as:
'            Non-Scheduled Item Splits where the Maj Peril is normally split (such as 201)
'            Special deductible and limit handeling
'            Combining of coverages for a Major Peril into one display line.
'	Most coverages that require a scheduled item split do not need entries in this table (see processing 5 above).
'	Entries are needed in the table for those coverages that require special handeling.
'
'	Examples of Not Splitting scheduled items:
'		IM860
'		In this case there are item enteries in BZ00 that need to be ignored.  Set up a PNT_X_MP_COV entry as follows:
'		MAJ_PERIL = 201
'		COVERAGE = IM860
'		SCHED_ITEM = N
'		LIMITS = H
'		DEDUCTIBLES = H
'	This will write a single display row for this combination of coverages.
'
'	Example of combining coverages into one display line
'               HO0756 HO0757 requires a single display line and the highest limit
'		Load PNT_MP_X_COV as follows:
'			MAJ_PERIL = 073
'                       COVERAGE = HO0756 HO0757
'                       SCHED_ITEM = N
'                       LIMITS = H
'                       DEDUCTIBLES = H
'
'
' Note:  You Can Not combine statistical records into a single display line in ClaimsPro.
'        Any special handeling of deductibles or limits must be defined in PNT_MP_X_COV
'---------------------------------------------------------------------------------------



'through out the script, objPolicyXCvgType is the datamodel object that is passed from policy download process and is considered as parent coverage object
'if the coverage is supposed to be split into further coverages then the child coverage objects are represented as objSplitCoverage
'if parent coverage (objPolicyXCvgType) is split into child cobverage then objPolicyXCvgType.SaveDefaultCoverage property will be set to false so that parent coverage is not saved

Sub CoverageAfterSave(objPolicyXCvgType As PolicyXCvgType)
    '#Region "variable declaration"
    Dim objPolicy As Policy = Nothing
    'DataModelFactory objDMF = null;
    Dim objPolicyXUnit As PolicyXUnit = Nothing
    Dim sSQL As String = String.Empty
    Dim objCache As LocalCache = Nothing
    Dim sb As StringBuilder = Nothing
    Dim iPolicyId As Integer = 0
    Dim iPolicyUnitRowId As Integer = 0
    Dim sUnitType As String = String.Empty
    Dim iUnitId As Integer = 0
    'bool //bSaveParent = false;
    Dim bCoverageSplit As Boolean = False
    Dim objSplitCoverage As PolicyXCvgType = Nothing
    Dim bOut As Boolean = False
    Dim bFirst As Boolean = False
    Dim bHaveSchedule As Boolean = False
    Dim sDed As String = "0"
    Dim sLim1 As String = "0"
    Dim sLim2 As String = "0"
    Dim stmp1 As String = ""
    Dim stmp2 As String = ""
    Dim sLOB As String = ""
    Dim sDSNDetails As String() = Nothing
    Dim sPointConnectionStr As String = ""
    Dim Classification_Code As String = ""
    Dim Classification As String = ""
    Dim Coverage_Code As Integer = 0
    Dim Coverage As String = ""
    Dim CoverageState As String = ""
    Dim CoverageState_Code As String = ""
    Dim CovItemSequence As String = ""
    Dim CovSequence As String = ""
    Dim EffectiveDate As String = ""
    Dim ExpirationDate As String = ""
    Dim LegacyKey As String = ""
    Dim RiskGroup As String = ""
    Dim RiskGroup_Code As String = ""
    Dim sINSLINE As String = String.Empty
    Dim sRISKLOC As String = String.Empty
    Dim sRISKSUBLOC As String = String.Empty
    Dim sPRODUCT As String = String.Empty
    Dim sUNIT As String = String.Empty
    Dim sCOVERAGE As String = String.Empty
    Dim sCOVSEQNUM As String = String.Empty
    Dim baDedA As String = String.Empty
    Dim baDedB As String = String.Empty
    Dim sDOL As String = String.Empty
    Dim sLogSeq As String = String.Empty
    Dim sLogSeqTemp As String = String.Empty
    Dim sB5Ded As String = String.Empty
    Dim sB5Lim1 As String = String.Empty
    Dim sB5Lim2 As String = String.Empty
    Dim sB5CLim As String = String.Empty
    Dim sB5DLim As String = String.Empty
    Dim sBBLim1 As String = String.Empty
    Dim sBBLim2 As String = String.Empty
    Dim sCovALimit As String = String.Empty
    Dim teDesc As String = String.Empty
    Dim trimClass As String = String.Empty
    Dim bLoopCov As Boolean = False
    Dim bNoPMXC As Boolean = False
    Dim sCovList As String = ""
    Dim iCountPMXC As Integer = 0
    Dim iRow As Integer = 0
    Dim sLimits As String = ""
    Dim sDeductibles As String = ""
    Dim sSplitSched As String = String.Empty
    Dim iPolicyLOB As Integer = 0
    Dim sLocation As String = ""
    Dim sMasterCo As String = ""
    Dim sSymbol As String = ""
    Dim sPolicyNo As String = ""
    Dim sModule As String = ""
    Dim sMajPeril As String = ""
    Dim sSarUnit As String = ""
    Dim sSarSeqNo As String = ""
    Dim sSaSeqNo As String = ""
    Dim sClass As String = String.Empty
    Dim sSA15Lim1 As String = String.Empty
    Dim sSA15Lim2 As String = String.Empty
    Dim sSA15Ded As String = String.Empty
    Dim sCoverageDesc As String = String.Empty
    'DbConnection objConRMA = null;
    'DbConnection objConPoint = null;
    Dim m_sConnectString As String = ""
    Dim AttCount As Integer = 0
    '#End Region

    Try
        Log.Write("inside script")
        sb = New StringBuilder()

        objPolicy = DirectCast(objPolicyXCvgType.Context.Factory.GetDataModelObject("Policy", False), Policy)
        bFirst = True
        'copy parent coverage's menadatory information into split coverage. this will be used if the coverage is split
        objSplitCoverage = DirectCast(objPolicyXCvgType.Context.Factory.GetDataModelObject("PolicyXCvgType", False), PolicyXCvgType)

        m_sConnectString = objPolicyXCvgType.Context.RMDatabase.ConnectionString
        '#Region "Get Policy Unit Details from RMA"
        Log.Write("rma connection string is " & m_sConnectString)
        'Get Policy Details
        sSQL = "SELECT POLICY_X_UNIT.* FROM POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" & Convert.ToString(objPolicyXCvgType.PolicyUnitRowId)

        Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sSQL)
            If objReader.Read() Then
                iPolicyId = objReader.GetInt("POLICY_ID")
                iPolicyUnitRowId = objReader.GetInt("POLICY_UNIT_ROW_ID")
                iUnitId = objReader.GetInt("UNIT_ID")
                sUnitType = objReader.GetString("UNIT_TYPE")
            End If
        End Using
        '#End Region
        Log.Write("iPolicyId is " & iPolicyId)
        Log.Write("iPolicyUnitRowId is " & iPolicyUnitRowId)
        Log.Write("iUnitId is " & iUnitId)
        Log.Write("sUnitType is " & sUnitType)

        objPolicy.MoveTo(iPolicyId)
        iPolicyLOB = objPolicy.PolicyLOB
        If objCache Is Nothing Then
            objCache = New LocalCache(m_sConnectString)
        End If
        sLOB = objCache.GetShortCode(iPolicyLOB)
        Log.Write("sLOB is " & sLOB)
        '#Region "exit the function with out spliting coverage based on LOB validation"

        If sLOB.Contains("APV") OrElse sLOB.Contains("NSV") Then
            Log.Write("If sLOB.Contains(APV) OrElse sLOB.Contains(NSV) Then - return")
            'rupal:come again
            'Non-Homeowners policies are not split and come from Stats
            'rupal:come again

            'XMLCoverage.Classification refers to objPolicyXCvgType.CoverageText column in RMA
            'in advanced claims, wheneevr XMLCoverage.Classification refers to PMPolicyCoverage.Description. whenever description 
            'is set to empty string, then on UI, coverage type code descrition is displayed. so in RMA also, we will displaye CoverageTypeCode description from 
            'codes table if coverage text is set to empty

            'XMLCoverage.Classification_Code = "" //go again, need to set the coverage text to blank and append coverage type code description
            'XMLCoverage.Classification = ""

            'bSaveParent = true;
            Return
        End If


        If InStr(1, ":HO:FO:BP:TC:UMB:RV:CX:CPP:", sLOB) = 0 Then ' Achla:means sLob does not have given values
            Log.Write("LOB is not listed for script execution. Script will not be executed")
            Return
        End If

        '#End Region

        '#Region "get point connection"
        sPointConnectionStr = objPolicyXCvgType.Context.PointConnectionString
        Log.Write("Point Connection string is -" & sPointConnectionStr)
        'If sPointConnectionStr.Trim() <> "DSN=OMIBDAT;Uid=ISCLM9;Pwd=ISCLM9;" Then
        'Log.Write("Point Connection string is - exit" & sPointConnectionStr)
        'Return
        'End If

        If String.IsNullOrEmpty(sPointConnectionStr) Then
            Log.Write("Point Connection Details are not provided.Script will not be executed")
            Return
        End If
        'exit the function as point connection details are not provided
        '#End Region
        '#Region "PMSPSA15 , commented"
        'PMSPSA15
        'string sLocation = objPolicy.LocationCompany;
        'string sMasterCo = objPolicy.MasterCompany;
        'string sSymbol = objPolicy.PolicySymbol;
        'string sPolicyNo = objPolicy.PolicyNumber;
        'string sModule = objPolicy.Module;
        'string sMajPeril = "major peril";//Trim(rsPMSPSA15("SARMAJPERL"))//coverage type code
        'string sSarUnit = "unit num";//objPolicyXUnit.uni//pooint_unit_data.stat_unit_number (query)
        'string sSarSeqNo = objPolicyXCvgType.CvgSequenceNo;
        'string sSaSeqNo = objPolicyXCvgType.TransSequenceNo;
        'string sClass = objPolicyXCvgType.CvgClassCode; //Trim(rsPMSPSA15("SARCLASS")) //only for wc (CvgClassCode)
        'string sSA15Lim1 = "0" + objPolicyXCvgType.OccurrenceLimit;//"0" & Trim(rsPMSPSA15("LIMITOCCUR"))
        'string sSA15Lim2 = "0" + objPolicyXCvgType.Limit; //Trim(rsPMSPSA15("LIMITAGGER"))
        'string sSA15Ded = "0" + objPolicyXCvgType.SelfInsureDeduct;  //Trim(rsPMSPSA15("DEDUCTIBLE"))
        '#End Region

        '#Region "Get PMSPSA15 Table's data"

        sLocation = objPolicy.LocationCompany.Trim()
        sMasterCo = objPolicy.MasterCompany.Trim()
        sSymbol = objPolicy.PolicySymbol.Trim()
        sPolicyNo = objPolicy.PolicyNumber.Trim()
        sModule = objPolicy.[Module].Trim()
        'sMajPeril = objPolicyXCvgType.CoverageTypeCode.ToString(); //"major peril";//Trim(rsPMSPSA15("SARMAJPERL"))//coverage type code
        sSarUnit = ""
        'objPolicyXUnit.uni//pooint_unit_data.stat_unit_number (query)
        sSQL = "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA WHERE UNIT_ID=" & iUnitId & " AND UNIT_TYPE='" & sUnitType & "'"
        Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sSQL)
            If objReader.Read() Then
                sSarUnit = objReader.GetString("STAT_UNIT_NUMBER")
                Log.Write("sarunit " & sSarUnit)
            End If
        End Using
        sCoverageDesc = objCache.GetCodeDesc(objPolicyXCvgType.CoverageTypeCode).Trim()
        sSarSeqNo = objPolicyXCvgType.CvgSequenceNo.Trim()
        sSaSeqNo = objPolicyXCvgType.TransSequenceNo.Trim()
        sClass = String.Empty
        'Trim(rsPMSPSA15("SARCLASS")) //only for wc (CvgClassCode)
        sSA15Lim1 = String.Empty
        '"0" & Trim(rsPMSPSA15("LIMITOCCUR"))
        sSA15Lim2 = String.Empty
        'Trim(rsPMSPSA15("LIMITAGGER"))
        sSA15Ded = String.Empty
        'Trim(rsPMSPSA15("DEDUCTIBLE"))
        sb = New StringBuilder()
        sb.Append("SELECT * FROM PMSPSA15 WHERE ")
        sb.Append("POLICY0NUM = '" & sPolicyNo & "'")
        sb.Append(" AND SYMBOL = '" & sSymbol & "'")
        sb.Append(" AND MODULE = '" & sModule & "'")
        sb.Append(" AND MASTER0CO = '" & sMasterCo & "'")
        sb.Append(" AND LOCATION = '" & sLocation & "'")
        sb.Append(" AND SARUNIT = '" & sSarUnit & "'")
        sb.Append(" AND SARSEQNO = '" & sSarSeqNo & "'")
        sb.Append(" AND SASEQNO = '" & sSaSeqNo & "'")
        Log.Write("PMSPSA15 query is " & sb.Tostring())
        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                Log.Write("PMSPSA15 query - inside read")
                sClass = objReader.GetString("SARCLASS")
                sSA15Lim1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("LIMITOCCUR"))
                sSA15Lim2 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("LIMITAGGER"))
                sSA15Ded = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("DEDUCTIBLE"))
                'objPolicyXCvgType.CoverageTypeCode.ToString(); //"major peril";//Trim(rsPMSPSA15("SARMAJPERL"))//coverage type code
                sMajPeril = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("SARMAJPERL")).Trim()
            End If
        End Using
        Log.Write("PMSPSA15 query - outside read")
        '#End Region       


        'XMLCoverage Object
        Dim sCoverageKey As String = String.Empty
        If objPolicyXCvgType.CoverageKey = String.Empty Then
            sCoverageKey = sSarUnit & "," & objPolicyXCvgType.CvgSequenceNo & "," & objPolicyXCvgType.TransSequenceNo
            objPolicyXCvgType.CoverageKey = sCoverageKey
        Else
            sCoverageKey = objPolicyXCvgType.CoverageKey
        End If
        Classification_Code = objPolicyXCvgType.CvgClassCode 'XMLCoverage.Classification_Code
        Classification = objPolicyXCvgType.CoverageText 'objPolicyXCvgType.CvgClassCode;//XMLCoverage.Classification
        Coverage_Code = objPolicyXCvgType.CoverageTypeCode 'XMLCoverage.Coverage_Code
        Coverage = objPolicyXCvgType.CoverageText 'XMLCoverage.Coverage
        CoverageState = "state" 'XMLCoverage.CoverageState 'achla: what is the use of cov state and cov state code.//come again
        CoverageState_Code = "state_code" 'XMLCoverage.CoverageState_Code//come again
        CovItemSequence = objPolicyXCvgType.TransSequenceNo
        CovSequence = objPolicyXCvgType.CvgSequenceNo ' XMLCoverage.CovSequence
        EffectiveDate = objPolicyXCvgType.EffectiveDate 'XMLCoverage.EffectiveDate
        ExpirationDate = objPolicyXCvgType.ExpirationDate 'XMLCoverage.ExpirationDate
        LegacyKey = sCoverageKey ' XMLCoverage.LegacyKey 'achla: what is a legacy key in rmA?
        RiskGroup = "" 'XMLCoverage.RiskGroup //'achla: what is this?
        RiskGroup_Code = objPolicyXCvgType.SubLine 'XMLCoverage.RiskGroup_Code 'achla: not used anywhere later

        Log.Write("Show Coverage Keys : MajPeril - " & sMajPeril & ", Class - " & sClass)

        '#Region "Get Data From ASB1CPP Table"

        'This first read get the linkage keys from Stat to Rating
        'rupal:below feilds are available in datamodel of RMA
        sINSLINE = String.Empty
        sRISKLOC = String.Empty
        sRISKSUBLOC = String.Empty
        sPRODUCT = String.Empty
        sUNIT = String.Empty
        sCOVERAGE = String.Empty
        sCOVSEQNUM = String.Empty

        sb = New StringBuilder()
        sb.Append("SELECT * FROM ASB1CPP WHERE ")
        sb.Append("B1AACD = '" & sLocation & "'")
        sb.Append(" AND B1ABCD = '" & sMasterCo & "'")
        sb.Append(" AND B1ARTX = '" & sSymbol & "'")
        sb.Append(" AND B1ASTX = '" & sPolicyNo & "'")
        sb.Append(" AND B1ADNB = '" & sModule & "'")
        sb.Append(" AND B1J4TX = '" & sSarUnit & "'")
        sb.Append(" AND B1BSNB = '" & sSarSeqNo & "'")

        Log.Write("Read ASB1CPP: Before - sMajPeril, sSarUnit, sSarSeqNo, sSaSeqNo :" & sMajPeril & "," & sSarUnit & "," & sSarSeqNo & "," & sSaSeqNo)
        Log.Write("Read ASB1CPP: Before SSQL" & sb.ToString())

        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                sINSLINE = objReader.GetString("B1AGTX").Trim() 'Trim(CStr(rsASB1("B1AGTX")))
                sRISKLOC = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("B1BRNB")).Trim() 'Trim(CStr(rsASB1("B1BRNB")))
                sRISKSUBLOC = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("B1EGNB")).Trim() 'Trim(CStr(rsASB1("B1EGNB")))
                sPRODUCT = objReader.GetString("B1ANTX").Trim() 'Trim(CStr(rsASB1("B1ANTX")))
                sUNIT = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("B1AENB")) 'Trim(CStr(rsASB1("B1AENB"))) 'achla: checked in table, this is unit number,
                sCOVERAGE = objReader.GetString("B1AOTX").Trim()
                sCOVSEQNUM = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("B1C0NB"))
            Else
                'rupal:i think the above query checks for the proper linking of records at point side, if proper linking not found then return and do not save the coverage
                'if no linking found then the record itself becomes invalid. when parent coverage itself is invalid, no need to bother further.
                Return
            End If
        End Using

        Log.Write("Read ASB1CPP: After sCOVERAGE =" & sCOVERAGE & ", sINSLINE=" & sINSLINE & ", sPRODUCT=" & sPRODUCT & ",sCOVSEQNUM=" & sCOVSEQNUM)
        '#End Region

        '#Region "Get baDedA,baDedB from ASBACPP"
        baDedA = String.Empty
        baDedB = String.Empty
        sb = New StringBuilder()
        sb.Append("SELECT * FROM ASBACPP ")
        sb.Append("WHERE BAAACD = '" & sLocation & "'")
        sb.Append(" AND BAABCD = '" & sMasterCo & "'")
        sb.Append(" AND BAARTX = '" & sSymbol & "'")
        sb.Append(" AND BAASTX = '" & sLocation & "'")
        sb.Append(" AND BAADNB = '" & sModule & "'")

        Log.Write("Read ASBACPP: Before SSQL-" & sb.ToString())

        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                baDedA = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BAAKCD"))
                baDedB = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BAALCD"))
            Else
                baDedA = "0"
            End If
        End Using
        Log.Write("Read ASBACPP: First After, baDedA=" & baDedA & ", baDedB=" & baDedB)
        If baDedB = "0000" Then
            baDedB = baDedA
        End If

        Log.Write("Read ASBACPP: Second After, baDedA=" & baDedA & ", baDedB=" & baDedB)
        '#End Region

        '#Region "This read gets the correct Log Seq Number for Rating rows based on Date of Loss"

        'get date of loss from parent coverage object - objPolicyXCvgType
        sDOL = objPolicyXCvgType.sDateOfLoss
        sDOL = Riskmaster.Common.Conversion.ConvertRMADateToPointDate(sDOL)

        '#End Region
        sb = New StringBuilder()
        sLogSeq = String.Empty
        sLogSeqTemp = String.Empty
        sb.Append("SELECT MAX(LOGSEQNUM) FROM BASHLG0001 ")
        sb.Append("WHERE LOCATION = '" & sLocation & "' AND ")
        sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
        sb.Append("SYMBOL = '" & sSymbol & "' AND ")
        sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
        sb.Append("MODULE = '" & sModule & "' AND ")
        sb.Append("TRNSDATE = ")
        sb.Append("(SELECT MAX(TRNSDATE) FROM BASHLG0001 ")
        sb.Append("WHERE LOCATION = '" & sLocation & "' AND ")
        sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
        sb.Append("SYMBOL = '" & sSymbol & "' AND ")
        sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
        sb.Append("MODULE = '" & sModule & "' AND ")
        sb.Append("TRNSDATE <= '" & sDOL & "')")

        Log.Write("Read BASHLG0001: Before SSQL=" & sb.ToString())

        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                sLogSeq = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(0))
            Else
                Return 'bSaveParent = true;
            End If
        End Using

        Log.Write("Read BASHLG0001: After, sLogSeq= " & sLogSeq)

        sLogSeqTemp = sLogSeq
        'Start PCR168
        'Check for cancel/reinstatement
        ' RI/CN cancel out. Must use previous Sequence number before cancel to match B500,BB00,BZ00,BY00 records
        'This read gets the matching sequnce from the BASHLGB500
        sb = New StringBuilder()
        sb.Append("SELECT MAX(LOGSEQNUM) FROM BASHLGB500 ")
        sb.Append(" WHERE LOCATION = '" & sLocation & "' AND ")
        sb.Append(" MASTERCO = '" & sMasterCo & "' AND ")
        sb.Append(" SYMBOL = '" & sSymbol & "' AND ")
        sb.Append(" POLICYNO = '" & sPolicyNo & "' AND ")
        sb.Append(" MODULE = '" & sModule & "' AND ")
        sb.Append(" INSLINE = '" & sINSLINE & "' AND ")
        If sLOB <> "CPP" Then
            sb.Append("RISKLOC = '" & sRISKLOC & "' AND ")
        End If
        sb.Append(" LOGSEQNUM <= " & sLogSeqTemp)

        Log.Write("Read BASHLGB500(1) - For Sequence number: Before , Sequence=" & sLogSeq & ", sSQL = " & sb.ToString())

        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                sLogSeq = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(0))
            End If
        End Using

        Log.Write("Read BASHLGB500(1) - For Sequence number: After , Sequence=" & sLogSeq & ", sSQL = " & sb.ToString())
        'check for valid read, if not re-read without using INSLINE

        If String.IsNullOrEmpty(sLogSeq) Then
            sb = New StringBuilder()
            sb.Append("SELECT MAX(LOGSEQNUM) FROM BASHLGB500 ")
            sb.Append("WHERE LOCATION = '" & sLocation & "' AND ")
            sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
            sb.Append("SYMBOL = '" & sSymbol & "' AND ")
            sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
            sb.Append("MODULE = '" & sModule & "' AND ")
            If sLOB <> "CPP" Then
                sb.Append("RISKLOC = '" & sRISKLOC & "' AND ")
            End If
            sb.Append("LOGSEQNUM <= " & sLogSeqTemp)

            Log.Write("Read BASHLGB500(2) - For Sequence number: Before , Sequence=" & sLogSeq & ", sSQL = " & sb.ToString())
            Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
                If objReader.Read() Then
                    sLogSeq = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(0))
                End If
            End Using

            Log.Write("Read BASHLGB500(2) - For Sequence number: After , Sequence=" & sLogSeq & ", sSQL = " & sb.ToString())
        End If
        'If sLogSeq is still Null, restore it back to original sLogSeq from the BASHLG0001 read
        If String.IsNullOrEmpty(sLogSeq) Then
            sLogSeq = sLogSeqTemp
        End If
        '#End Region

        '#Region "If this is a unit level coverage, pull the data from the unit coverage table,read sB5Ded,sB5Lim1,sB5Lim2,sB5CLim,sB5DLim"
        ' IRCL0832 - 08/22/2013
        ' If sLogSeqTemp from BASHLG0001 is greater than sLogSeq from BASHLGB500 use sLogSeqTemp.
        If (CInt(sLogSeqTemp) > CInt(sLogSeq)) Then
            sLogSeq = sLogSeqTemp
        End If

        'End PCR168   

        'Possible correction for null sLogSeq - Causing Coverage F to not display correctly
        ' this may not be vaild as it will discard Coverage F which really should be on the policy
        'If sLogSeq = "" Then
        '   CleanUp
        '   Exit Function
        'End If

        'If this is a unit level coverage, pull the data from the unit coverage table

        'Now we read the rating coverages for this particular Unit
        ' IRCL0832 - 08/22/2013
        ' Updated LOGSEQNUM query to check for <= sLogSeq before it was only checking = sLogSeq.
        ' This allows you to get units added as part of a change and/or the existing unit.
        sb = New StringBuilder()
        sb.Append("SELECT * FROM BASHLGB500 ")
        sb.Append("WHERE LOGSEQNUM <= " & sLogSeq & " AND ")
        sb.Append("LOCATION = '" & sLocation & "' AND ")
        sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
        sb.Append("SYMBOL = '" & sSymbol & "' AND ")
        sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
        sb.Append("MODULE = '" & sModule & "' AND ")
        sb.Append("INSLINE = '" & sINSLINE & "' AND ")
        sb.Append("RISKLOC = " & sRISKLOC & " AND ")
        sb.Append("RISKSUBLOC = " & sRISKSUBLOC & " AND ")
        sb.Append("PRODUCT = '" & sPRODUCT & "' AND ")
        sb.Append("UNIT = " & sUNIT & " AND ")
        sb.Append("RECSTATUS = 'V'")

        Log.Write("Read BASHLGB500: Before : sSQL =" & sSQL & ", sMajPeril=" & sMajPeril & ", sINSLINE=" & sINSLINE)

        sB5Ded = String.Empty
        sB5Lim1 = String.Empty
        sB5Lim2 = String.Empty
        sB5CLim = String.Empty
        sB5DLim = String.Empty

        Dim objDSB500 As DataSet = DbFactory.GetDataSet(sPointConnectionStr, sb.ToString())
        Dim dtB500 As DataTable = Nothing
        If objDSB500 IsNot Nothing AndAlso objDSB500.Tables.Count > 0 Then
            dtB500 = objDSB500.Tables(0)
            If dtB500.Rows.Count > 0 Then
                sB5Ded = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AKCD")) '& Trim(CStr(rsB500("B5AKCD")))    
                sB5Lim1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA")) '& Trim(CStr(rsB500("B5AGVA")))                        
                sB5Lim2 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AHVA")) '& Trim(CStr(rsB500("B5AHVA")))
                sB5CLim = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5USVA3"))
                sB5DLim = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5USVA4"))
            Else
                Select Case sINSLINE
                    Case "FA"
                        Log.Write("Case FA , sMajPeril = " & sMajPeril)
                        If sMajPeril = "260" OrElse sMajPeril = "261" OrElse sMajPeril = "049" OrElse sMajPeril = "050" Then
                            sB5Ded = "0"
                            sB5Lim1 = "0"
                            sB5Lim2 = "0"
                        End If
                        Exit Select
                    Case "FE"
                        Log.Write("Case FE , sMajPeril = " & sMajPeril)
                        sB5Ded = "0"
                        sB5Lim1 = "0"
                        sB5Lim2 = "0"
                        Exit Select
                    Case "FF"
                        Log.Write("Case FF , sMajPeril = " & sMajPeril)
                        If sMajPeril = "260" OrElse sMajPeril = "261" OrElse sMajPeril = "049" OrElse sMajPeril = "050" Then
                            sB5Ded = "0"
                            sB5Lim1 = "0"
                            sB5Lim2 = "0"
                        End If
                        Exit Select
                    Case "FL"
                        Log.Write("Case FL , sMajPeril = " & sMajPeril)
                        sB5Ded = "0"
                        sB5Lim1 = "0"
                        sB5Lim2 = "0"
                        Exit Select
                    Case "FM"
                        Log.Write("Case FM , sMajPeril = " & sMajPeril)
                        sB5Ded = "0"
                        sB5Lim1 = "0"
                        sB5Lim2 = "0"
                        Exit Select
                End Select
            End If
            Log.Write("Read BASHLGB500: After : sMajPeril = " & sMajPeril)
        End If


        '#End Region

        '#Region "Read BASHLGBB00"
        sBBLim1 = String.Empty
        sBBLim2 = String.Empty
        sb = New StringBuilder()

        sb.Append("SELECT * FROM BASHLGBB00 ")
        sb.Append(" WHERE LOGSEQNUM = " & sLogSeq & " AND ")
        sb.Append("LOCATION = '" & sLocation & "' AND ")
        sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
        sb.Append("SYMBOL = '" & sSymbol & "' AND ")
        sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
        sb.Append("MODULE = '" & sModule & "' AND ")
        sb.Append("INSLINE = '" & sINSLINE & "' AND ")
        sb.Append("RECSTATUS = 'V'")

        Log.Write("Read BASHLGBB00: Before : SSQL = " & sb.ToString())

        Using objReader As DbReader = DbFactory.GetDbReader(sPointConnectionStr, sb.ToString())
            If objReader.Read() Then
                Log.Write("Read BASHLGBB00: Inside If")
                sBBLim1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BBAGVA"))
                sBBLim2 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BBAHVA"))
            Else
                Log.Write("Read BASHLGBB00: Inside Else")
                sBBLim1 = "0"
                sBBLim2 = "0"
            End If
        End Using

        Log.Write("Read BASHLGBB00: After : sMajPeril = " & sMajPeril)
        '#End Region

        '#Region "Remove Coverages"
        Log.Write("Remove Coverages: sMajPeril = " & sMajPeril & ", sINSLINE=" & sINSLINE & ", sClass=" & sClass & ", sCoverage=" & sCOVERAGE)
        'The following code is used to remove the specified coverage codes from appearing
        If sINSLINE = "FU" OrElse sINSLINE = "PU" OrElse sINSLINE = "CX" Then
            If sMajPeril <> "105" OrElse sCOVERAGE <> "1STMIL" Then
                Log.Write("Do not display this coverage at all (1) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                'do not display this coverage at all                        
                objPolicyXCvgType.SaveDefaultCoverage = False

                Return
            End If
        End If

        If sMajPeril = "599" AndAlso (sINSLINE = "CF" OrElse sINSLINE = "GL" OrElse sINSLINE = "CA" OrElse sINSLINE = "IMC" OrElse sINSLINE = "CR" OrElse sINSLINE = "GA") Then
            Log.Write("Do not display this coverage at all (2) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If (sMajPeril = "536" OrElse sMajPeril = "537") AndAlso sSymbol = "CPP" Then
            Log.Write("Remove MP 536/537 for CPP: , sMajPeril= " & sMajPeril & ", sINSLI1NE=" & sINSLINE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "005" AndAlso sClass = "9999" Then
            If sINSLINE = "FA" AndAlso (sCOVERAGE = "CREDIT" OrElse sCOVERAGE = "FEEXP" OrElse sCOVERAGE = "FIREDP" OrElse sCOVERAGE = "HO50" OrElse sCOVERAGE = "LVSCC" OrElse sCOVERAGE = "ML145" OrElse sCOVERAGE = "ML27" OrElse sCOVERAGE = "ML307" OrElse sCOVERAGE = "ML331" OrElse sCOVERAGE = "ML35" OrElse sCOVERAGE = "ML48" OrElse sCOVERAGE = "OPRECS" OrElse sCOVERAGE = "REFRIG" OrElse sCOVERAGE = "SIGNS") Then
                Log.Write("Do not display this coverage at all (3) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                objPolicyXCvgType.SaveDefaultCoverage = False
                'do not display this coverage at all
                Return
            End If
            If sINSLINE = "FE" AndAlso (sCOVERAGE = "ML307" OrElse sCOVERAGE = "ML352" OrElse sCOVERAGE = "ML361") Then
                Log.Write("Do not display this coverage at all (4): sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                objPolicyXCvgType.SaveDefaultCoverage = False
                'do not display this coverage at all
                Return
            End If
            If sINSLINE = "FF" AndAlso (sCOVERAGE = "ML27" OrElse sCOVERAGE = "ML307") Then
                Log.Write("Do not display this coverage at all (5): sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                objPolicyXCvgType.SaveDefaultCoverage = False
                'do not display this coverage at all
                Return
            End If
            If sINSLINE = "FL" AndAlso sCOVERAGE = "ML318" Then
                Log.Write("Do not display this coverage at all (6): sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                objPolicyXCvgType.SaveDefaultCoverage = False
                'do not display this coverage at all
                Return
            End If
        End If

        Select Case sMajPeril
            Case "010"
                ''Basic Policy - Liability
                Select Case sINSLINE
                    Case "FL"
                        ''E953 - 06/03/2011 - AL0170 - Per Karen King & Rick Hartschuh, do not import "AL0170 - Pers Liab w/AL 0001" as losses would be paid under Coverage L or Coverage M.
                        If sClass = "9999" AndAlso (sCOVERAGE = "AL0170" OrElse sCOVERAGE = "CUSTOM" OrElse sCOVERAGE = "FO315" OrElse sCOVERAGE = "HORSES" OrElse sCOVERAGE = "ML102" OrElse sCOVERAGE = "ML318") Then
                            Log.Write("Do not display this coverage at all (7) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                            objPolicyXCvgType.SaveDefaultCoverage = False
                            'do not display this coverage at all
                            Return
                        End If
                        ''F069 - 03/30/2012 - Remove blank liability line.
                        If sCOVERAGE = "*ALL" AndAlso sClass = "9999" Then
                            Log.Write("Do not display this coverage at all (8) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                            objPolicyXCvgType.SaveDefaultCoverage = False
                            'do not display this coverage at all
                            Return
                        End If
                        Exit Select
                End Select
                Exit Select
            Case "029"
                Select Case sINSLINE
                    Case "BP"
                        ''F068 - 05/07/2012 - Remove BPUO40 - Employment Practices Liability Extended Reporting Period.
                        If sCOVERAGE = "BPUO40" Then
                            Log.Write("Do not display this coverage at all (9) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                            objPolicyXCvgType.SaveDefaultCoverage = False
                            'do not display this coverage at all
                            Return
                        End If
                        Exit Select
                    Case "FL"
                        ''F069 - 03/30/2012 - Remove ML606 - Extended Reporting Employment Practices Liability.
                        If sCOVERAGE = "*ALL" Then
                            Log.Write("Do not display this coverage at all (10) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
                            objPolicyXCvgType.SaveDefaultCoverage = False
                            'do not display this coverage at all
                            Return
                        End If
                        Exit Select
                End Select
                ''sINSLINE
                Exit Select
            Case "539"
                Select Case sINSLINE
                    Case "FL"
                        ''F069 - Add text description for ML605 - Employment Practices Liability.
                        If sCOVERAGE = "*ALL" AndAlso sClass = "9999" Then
                            Log.Write("Default Coverage's CoverageText changed Here (1) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass)
                            objPolicyXCvgType.CvgClassCode = ""
                            'XMLCoverage.Classification_Code = ""
                            'XMLCoverage.Classification = " Employment Practices Liability"
                            objPolicyXCvgType.CoverageText = sCoverageDesc & "-Employment Practices Liability"
                        End If
                        Exit Select
                End Select
                ''sINSLINE
                Exit Select
        End Select

        If sMajPeril = "028" AndAlso sINSLINE = "BP" AndAlso sCOVERAGE = "FULLY" Then
            Log.Write("Do not display this coverage at all (11) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "599" AndAlso sINSLINE = "BP" AndAlso (sCOVERAGE = "BP06MP" OrElse sCOVERAGE = "BP07MP" OrElse sCOVERAGE = "BP10MP") Then
            Log.Write("Do not display this coverage at all (12) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "030" AndAlso sINSLINE = "BP" AndAlso sCOVERAGE = "MINPRM" Then
            Log.Write("Do not display this coverage at all (13) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "061" AndAlso sINSLINE = "BP" AndAlso sCOVERAGE = "BP0487" Then
            Log.Write("Do not display this coverage at all (14) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        If sMajPeril = "065" AndAlso sINSLINE = "BP" AndAlso (sCOVERAGE = "BP0402" OrElse sCOVERAGE = "BP0406" OrElse sCOVERAGE = "BP0407" OrElse sCOVERAGE = "BP0408" OrElse sCOVERAGE = "BPUO31" OrElse sCOVERAGE = "BP0409" OrElse sCOVERAGE = "BP0410" OrElse sCOVERAGE = "BP0411" OrElse sCOVERAGE = "BP0413" OrElse sCOVERAGE = "BP0416" OrElse sCOVERAGE = "BP0447" OrElse sCOVERAGE = "BP0448" OrElse sCOVERAGE = "BP0449" OrElse sCOVERAGE = "BP0450" OrElse sCOVERAGE = "BP0451" OrElse sCOVERAGE = "BP0452" OrElse sCOVERAGE = "BP0454" OrElse sCOVERAGE = "BP0710" OrElse sCOVERAGE = "BP0711" OrElse sCOVERAGE = "BP0712" OrElse sCOVERAGE = "BP0801" OrElse sCOVERAGE = "BP0802" OrElse sCOVERAGE = "BP0803" OrElse sCOVERAGE = "BP0804" OrElse sCOVERAGE = "BP0805" OrElse sCOVERAGE = "POOL") Then
            Log.Write("Do not display this coverage at all (15) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "029" AndAlso sINSLINE = "BP" AndAlso sCOVERAGE = "BPUO24" Then
            Log.Write("Do not display this coverage at all (16) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        ' ' Begin change for E394 - 4/23/2008
        '' Do not pull ILUO11 "Employment Practices Liability (EPLI) Extended Reporting Period" coverage into AC
        If sMajPeril = "029" AndAlso (sINSLINE = "GL" OrElse sINSLINE = "GA") AndAlso sCOVERAGE = "ILUO11" Then
            Log.Write("Do not display this coverage at all (17) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        ' ' End change for E394

        ' ' Begin change for E768 - 3/27/2009
        '' Do not pull ILUO13 "CT Employment Practices Liability (EPLI) Extended Reporting Period" coverage into AC
        If sMajPeril = "029" AndAlso (sINSLINE = "GL" OrElse sINSLINE = "GA") AndAlso sCOVERAGE = "ILUO13" Then
            Log.Write("Do not display this coverage at all (18) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        ' ' End change for E768

        '' Begin change for E788 - 5/15/2009
        '' Do not pull BPUO35 "Employment Practices Liability Extended Reporting Period - CT" coverage into AC
        If sMajPeril = "029" AndAlso (sINSLINE = "BP") AndAlso sCOVERAGE = "BPUO35" Then
            Log.Write("Do not display this coverage at all (19) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        ' ' End change for E788

        '' Begin change for E788 - 5/20/2009
        '' Add text description to IM7451 "Motor Truck Cargo Legal Liability" coverage (only item description comes across from stat record)
        If sINSLINE = "BP" AndAlso sCOVERAGE = "IM7451" Then
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.Classification = "IM7451 Motor Truck Cargo Legal Liability"
            'XMLCoverage.RiskGroup = " "
            Log.Write("Default Coverage's CoverageText changed Here (2) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass)
            objPolicyXCvgType.CvgClassCode = ""
            objPolicyXCvgType.CoverageText = sCoverageDesc & "-IM7451 Motor Truck Cargo Legal Liability"
            'bSaveParent = true;
            Return
        End If
        '' End change for E788

        '' Begin change for E768 - 3/30/2009
        '' Do not pull CAUO16 "AAA Club || Association" coverage into AC
        If sMajPeril = "100" AndAlso sINSLINE = "CA" AndAlso sCOVERAGE = "CAUO16" Then
            Log.Write("Do not display this coverage at all (20) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        '' End change for E768

        '' Begin change for F001 - 9/15/2011
        '' Do not pull ILUO21  "EPLI Extended Reporting Period - Maine" coverage into AC
        If sMajPeril = "029" AndAlso sINSLINE = "GL" AndAlso sCOVERAGE = "ILUO21" Then
            Log.Write("Do not display this coverage at all (21) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        '' Do not pull ILUO21/CGUO10  "EPLI Extended Reporting Period - Maine" coverage into AC
        If sMajPeril = "539" AndAlso sINSLINE = "GL" AndAlso sCOVERAGE = "CGUO10" Then
            Log.Write("Do not display this coverage at all (22) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        '' Do not pull ILUO21/CAUO13  "EPLI Extended Reporting Period - Maine" coverage into AC
        If sMajPeril = "539" AndAlso sINSLINE = "GA" AndAlso sCOVERAGE = "CAUO13" Then
            Log.Write("Do not display this coverage at all (23) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        '' End change for F001

        '' Begin change for E963 - 9/15/2011
        '' Do not pull EXTLIB coverage into AC
        If sMajPeril = "101" AndAlso sINSLINE = "APV" AndAlso sCOVERAGE = "EXTLIB" Then
            Log.Write("Do not display this coverage at all (24) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        ' ' Do not pull EXTLPD coverage into AC
        If sMajPeril = "110" AndAlso sINSLINE = "APV" AndAlso sCOVERAGE = "EXTLPD" Then
            Log.Write("Do not display this coverage at all (25) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        '' Do not pull EXTMED coverage into AC
        If sMajPeril = "114" AndAlso sINSLINE = "APV" AndAlso sCOVERAGE = "EXTMED" Then
            Log.Write("Do not display this coverage at all (26) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        '' End change for E963

        If sMajPeril = "529" AndAlso sINSLINE = "BP" AndAlso (sCOVERAGE = "BPUO17" OrElse sCOVERAGE = "BPUO23") Then
            Log.Write("Do not display this coverage at all (27) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If (sMajPeril = "255" OrElse sMajPeril = "256") AndAlso sINSLINE = "TA" Then
            ''OHMSI2 for TNC Policies
            Log.Write("Do not display this coverage at all (28) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "030" AndAlso sSymbol = "TNC" Then
            Log.Write("Do not display this coverage at all (29) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If (sMajPeril = "049" OrElse sMajPeril = "050") AndAlso sCOVERAGE = "OHMSI2" Then
            Log.Write("Do not display this coverage at all (30) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "010" AndAlso sINSLINE = "TL" AndAlso (sCOVERAGE = "FO315" OrElse sCOVERAGE = "HORSES" OrElse sCOVERAGE = "ML102" OrElse sCOVERAGE = "ML318" OrElse sCOVERAGE = "TNC313") Then
            Log.Write("Do not display this coverage at all (31) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "TA" AndAlso (sCOVERAGE = "ML27" OrElse sCOVERAGE = "TNC49" OrElse sCOVERAGE = "TNC145" OrElse sCOVERAGE = "TNC30") Then
            Log.Write("Do not display this coverage at all (32) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "TE" AndAlso (sCOVERAGE = "TNC350" OrElse sCOVERAGE = "TNC325" OrElse sCOVERAGE = "TNC352") Then
            Log.Write("Do not display this coverage at all (33) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "TB" AndAlso (sCOVERAGE = "ML27" OrElse sCOVERAGE = "TNC350" OrElse sCOVERAGE = "TNC325") Then
            Log.Write("Do not display this coverage at all (34) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If

        If sCOVERAGE = "GU6784" Then
            Log.Write("Do not display this coverage at all (35) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'do not display this coverage at all
            Return
        End If
        Log.Write("Remove Coverages End: ")
        '#End Region

        '#Region "Split Coverages"
        Log.Write("Split Coverages Start: ")
        If sMajPeril = "002" AndAlso sSymbol = "SHO" Then
            Log.Write("(1) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Coverage A (Dwelling)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", A"
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.Save()

            Log.Write("(2) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Coverage C (Personal Property)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", C"
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.Save()
            Return
        End If

        If sMajPeril = "018" AndAlso sSymbol = "SHO" Then
            Log.Write("(3) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Coverage E (Personal Liability)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", E"
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.Save()

            Log.Write("(4) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Coverage F (Medical Payments)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", F"
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.Save()
            Return
        End If

        '' Begin code for E783 - 5/12/2009
        '' Add text description for OM2480 coverage
        If sMajPeril = "134" Then
            Log.Write("(5) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Volunteer Wrongful Acts"
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.Save()
            Return
        End If
        '' End code for E783

        ' Begin code for MR05 0288 - 5/15/2009
        '' Prevent risk group description from displaying on coverage line
        '' Replacing the description with a space (" ") retains the risk group code number in the xml. Example: <RISK_GROUP CODE="915" />
        '' Replacing it with nothing ("") causes the node to be removed entirely.
        'XMLCoverage.RiskGroup = " "
        '' End change for MR05 0288
        '' Modified for F019 - 12/21/2011
        '' Do not split into Coverages A-D for Major Peril 002 on HMS policies.
        ' Modified for F019 (HMS) - 12/21/2011 & F191 (HMP) - 10/15/2013
        ' Do not split into Coverages A-D for Major Peril 002 on HMP or HMS policies.
        sCovALimit = String.Empty
        If sMajPeril = "002" And Not sSymbol = "HMP" And Not sSymbol = "HMS" Then
            '#Region "Split into Cov A"
            Log.Write("(6) Coverage Split : bfirst=" & bFirst & " ,sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-Coverage A (Dwelling)"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AGVA"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5AGVA")))  //'achla: AddLimit() method definition? What is the use of 1 here?
                sCovALimit = Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA"))
                ' Trim(CStr(rsB500("B5AGVA")))
                objSplitCoverage.CvgClassCode = ""
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", A"
            objSplitCoverage.Save()
            Log.Write("002 - Coverage A Saved " & objSplitCoverage.CoverageText)
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", A"
            '#End Region

            '#Region "Split into Cov B"
            Log.Write("(7) Coverage Split :,bfirst=" & bFirst & " ,sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage B (Other Structures)"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AHVA"))
                'XMLCoverage.Limit.Limit = CStr(rsB500("B5AHVA"))
                ' XMLCoverage.Deductible.Deductible = CStr(rsB500("B5AKCD"))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
            End If
            objSplitCoverage.CvgClassCode = ""
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", B"
            objSplitCoverage.Save()
            Log.Write("002 - Coverage B Saved " & objSplitCoverage.CoverageText)
            '#End Region

            '#Region "Split into Cov C"
            Log.Write("(8) Coverage Split : bFirst= " & bFirst & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & "Coverage C (Personal Property)"
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", C"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA3"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5USVA3")))
                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", C"
            objSplitCoverage.Save()
            Log.Write("002 - Coverage C Saved " & objSplitCoverage.CoverageText)
            '#End Region

            '#Region "Split into Cov D"
            Log.Write("(9) Coverage Split : bFirst= " & bFirst & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage D (Loss of Use)"
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", D"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA4"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5USVA4")))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", D"
            objSplitCoverage.Save()

            Log.Write("002 - Coverage D Saved " & objSplitCoverage.CoverageText)
            '#End Region
            'bSaveParent = false;//split
            Return
        End If

        ' Begin change for F019 (HMS) - 12/21/2011 & F191 (HMP) - 10/15/2013
        ' Split individual major perils into Coverages A-D on HMP & HMS policies.
        If (sSymbol = "HMP" Or sSymbol = "HMS") And (sMajPeril = "A02" Or sMajPeril = "B02" Or sMajPeril = "C02" Or sMajPeril = "D02" Or sMajPeril = "E02" Or sMajPeril = "F02" Or sMajPeril = "G02" Or sMajPeril = "H02" Or sMajPeril = "I02" Or sMajPeril = "J02") Then
            '#Region "Split into Cov A"
            Log.Write("(10) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", A"
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage A (Dwelling)"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AGVA"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5AGVA")))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))
                'Trim(CStr(rsB500("B5AGVA")))
                sCovALimit = Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA")).Trim()
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", A"
            objSplitCoverage.Save()
            '#End Region

            '#Region "Split into Cov B"
            Log.Write("(11) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage B (Other Structures)"
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.Limit.Limit_Code = 1
            'XMLCoverage.Deductible.Deductible_Code = 1
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", B"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AHVA"))
                'XMLCoverage.Limit.Limit = CStr(rsB500("B5AHVA"))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
                'XMLCoverage.Deductible.Deductible = CStr(rsB500("B5AKCD"))
                'Trim(CStr(rsB500("B5AGVA")))
                sCovALimit = Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA")).Trim()
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", B"
            objSplitCoverage.Save()

            '#End Region

            '#Region "Split into Cov C"
            Log.Write("(12) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage C (Personal Property)"
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", C"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA3"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5USVA3")))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", C"
            objSplitCoverage.Save()
            '#End Region

            '#Region "Split into Cov D"
            Log.Write("(13) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            'XMLCoverage.Classification_Code = ""
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", D"
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage D (Loss of Use)"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA4"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5USVA4")))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsB500("B5AKCD")))             
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AKCD"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", D"
            objSplitCoverage.Save()
            '#End Region
            'bSaveParent = false;//split  
            Return
        End If

        ' Begin change for F236 - 06/14/2013
        ' On HMS policies, for major peril 710, coverage OM1060, add coverage description and complete legacy key.
        ' For some reason these were not coming over from Point for this coverage, unable to determine why.  OM1050 is working fine.
        If sSymbol = "HMS" And sMajPeril = "710" And sCOVERAGE = "OM1060" Then
            objPolicyXCvgType.CvgClassCode = ""
            objPolicyXCvgType.CoverageText = sCoverageDesc & "-OM1060 - HO Master Series Six"
            objPolicyXCvgType.CoverageKey = objPolicyXCvgType.CoverageKey & ", OM1060, 1"
        End If
        ' End change for F236


        ' Begin change for F019 (HMS) - 12/21/2011 & F191 (HMP) - 10/15/2013
        ' On HMP & HMS policies, do not pull any major peril 002 coverages into AC.
        If (sSymbol = "HMP" Or sSymbol = "HMS") And sMajPeril = "002" Then
            Log.Write("Do not display this coverage at all (36) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false;//do not display this coverage at all
            Return
        End If
        'End change for F019
        Log.Write("Checking for Major Peril:" & sMajPeril & ", sINSLINE=" & sINSLINE & ", sSymbol= " & sSymbol & ", sCoverage = " & sCOVERAGE)

        If sMajPeril = "020" AndAlso sCOVERAGE = "AR" Then
            Log.Write("(14) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            '''/XMLCoverage.Classification_Code = ""                    
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & "AR Accounts Receivable"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "020" AndAlso sCOVERAGE = "BP0415" Then
            Log.Write("(15) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            'XMLCoverage.Classification_Code = ""
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & "BP0415 Spoilage Coverage"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "020" AndAlso sCOVERAGE = "BP0431" Then
            Log.Write("(16) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & "BP0431 Food Contamination"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "061" AndAlso sCOVERAGE = "BP0446" Then
            Log.Write("(17) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & "BP0446 Ordinance or Law Cov."
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "018" Then
            Log.Write("(18) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage E (Personal Liability)"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5CXVA")))                        
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5CXVA"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", E"
            objSplitCoverage.Save()

            Log.Write("(19) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage F (Medical Payments)"
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", F"
            If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsB500("B5CYVA")))                        
                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5CYVA"))
            End If
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", F"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "FO" Then
            Log.Write("(20) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage A (Residence)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", A"
            objSplitCoverage.Save()

            Log.Write("(21) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage C (Personal Property)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", C"
            objSplitCoverage.Save()

            Log.Write("(22) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage E (Farm Personal Property)"
            objSplitCoverage.Save()
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", E"
            'bSaveParent = false;//split
            Return
        End If

        'E987 - 6/6/2011 - For major peril 005, insurance line FE, add Coverage E, G.1, or G.2 as appropriate based on the occurrence limit value.
        If sMajPeril = "005" AndAlso sINSLINE = "FE" Then
            Select Case sSA15Lim1
                Case "00"
                    Log.Write("Default Coverage's CoverageText changed Here (3) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass & ",sSA15Lim1=" & sSA15Lim1)
                    ''CreateCovNode XMLCoverage, bFir   st
                    'XMLCoverage.Classification_Code = ""    'achla: this will not add new cov node, only update existing one..? as per comments above, looks like it is a add
                    objPolicyXCvgType.CvgClassCode = ""
                    objPolicyXCvgType.CoverageText = sCoverageDesc & "-Coverage E (Farm Personal Property)"
                    objPolicyXCvgType.CoverageKey = objPolicyXCvgType.CoverageKey & ", E"
                    Exit Select
                Case "01"
                    Log.Write("Default Coverage's CoverageText changed Here (4) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass & ",sSA15Lim1=" & sSA15Lim1)
                    ''CreateCovNode XMLCoverage, bFirst
                    'XMLCoverage.Classification_Code = ""
                    objPolicyXCvgType.CvgClassCode = ""
                    objPolicyXCvgType.CoverageText = sCoverageDesc & "-Coverage G.1 (Farm Personal Property)"
                    objPolicyXCvgType.CoverageKey = objPolicyXCvgType.CoverageKey & ", G"
                    'rupal:currenly we dont have any limits table in RMA so need to iterate for limits, I am just removing all the limits from PolicyXCvgType
                    objPolicyXCvgType.Limit = 0
                    objPolicyXCvgType.LimitCovA = 0
                    objPolicyXCvgType.LimitCovB = 0
                    objPolicyXCvgType.LimitCovC = 0
                    objPolicyXCvgType.LimitCovD = 0
                    objPolicyXCvgType.LimitCovE = 0
                    objPolicyXCvgType.LimitCovF = 0
                    'Do While Not XMLCoverage.Limit.getFirst Is Nothing
                    '    XMLCoverage.Limit.Remove  //'Remove all limits from new coverage node.
                    'Loop
                    Exit Select
                Case "02"
                    Log.Write("Default Coverage's CoverageText changed Here (5) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass & ",sSA15Lim1=" & sSA15Lim1)
                    ''CreateCovNode XMLCoverage, bFirst
                    objPolicyXCvgType.CvgClassCode = ""
                    objPolicyXCvgType.CoverageText = sCoverageDesc & "-Coverage G.2 (Farm Personal Property)"
                    objPolicyXCvgType.CoverageKey = objPolicyXCvgType.CoverageKey & ", G"
                    'rupal:currenly we dont have any limits table in RMA so need to iterate for limits, I am just removing all the limits from PolicyXCvgType
                    objPolicyXCvgType.Limit = 0
                    objPolicyXCvgType.LimitCovA = 0
                    objPolicyXCvgType.LimitCovB = 0
                    objPolicyXCvgType.LimitCovC = 0
                    objPolicyXCvgType.LimitCovD = 0
                    objPolicyXCvgType.LimitCovE = 0
                    objPolicyXCvgType.LimitCovF = 0
                    'Do While Not XMLCoverage.Limit.getFirst Is Nothing
                    '    XMLCoverage.Limit.Remove  //'Remove all limits from new coverage node.
                    'Loop
                    Exit Select
            End Select
            'bSaveParent = true;//we are just changing coverage text of the parent coverage, so it should be saved
            Return
        End If

        'E987 - 6/8/2011 - For major peril 027, insurance line FE, add coverage description when coverage code is "*ALL".
        If sMajPeril = "027" AndAlso sINSLINE = "FE" AndAlso sClass = "9999" Then
            Log.Write("Default Coverage's CoverageText changed Here (6) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass)
            objPolicyXCvgType.CvgClassCode = ""
            objPolicyXCvgType.CoverageKey = objPolicyXCvgType.CoverageKey & ", G"
            objPolicyXCvgType.CoverageText = sCoverageDesc & "-FO420 Equipment Breakdown"
        End If

        'Begin code for F019 - 12/14/2011
        'Remove generic HO Forms text description.
        If sClass = "999999" AndAlso objPolicyXCvgType.CoverageText = "Ho Forms 2,3,4,5,6 With Om0291 Attached" Then
            Log.Write("Default Coverage's CoverageText changed Here (7) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Class=" & sClass)
            objPolicyXCvgType.CvgClassCode = " "
            objPolicyXCvgType.CoverageText = sCoverageDesc & " "
        End If
        'End code for F019

        If sMajPeril = "010" AndAlso sINSLINE = "FO" Then
            Log.Write("(23) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage L (Liability)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", L"
            objSplitCoverage.Save()

            Log.Write("(24) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            'XMLCoverage.Classification_Code = ""
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage M (Medical Payments)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", M"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If (sINSLINE = "FU" OrElse sINSLINE = "PU") AndAlso sMajPeril = "105" AndAlso sCOVERAGE = "1STMIL" Then
            Log.Write("(25) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & ""
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If


        If sMajPeril = "005" AndAlso sINSLINE = "TA" Then
            If Riskmaster.Common.Conversion.CastToType(Of Double)((sB5Lim1), bOut) <> 0 Then
                Log.Write("(26) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage A (Residence)"
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", A"
                'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", A"
                objSplitCoverage.Save()
            End If

            ''Add read of ASBYCPP Here for display of MHADD Coverages
            sb = New StringBuilder()


            sb.Append("SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append(" BYABCD = '" & sMasterCo & "' AND ")
            sb.Append(" BYARTX = '" & sSymbol & "' AND ")
            sb.Append(" BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append(" BYADNB = '" & sModule & "' AND ")
            sb.Append(" BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append(" BYBRNB = " & sRISKLOC & " AND ")
            sb.Append(" BYEGNB = " & sRISKSUBLOC & " AND ")
            sb.Append(" BYAOTX = 'MHADD'")

            AttCount = 0

            Log.Write("Read ASBYCPP: Before : sSQL=" & sSQL)

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                While objReader.Read()
                    AttCount = 0
                    If objReader.GetString("BYCZST") = "N" Then
                        Log.Write("(27) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " " & objReader.GetString("BYAL36TXT")
                        'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", A" + CStr(AttCount)
                        objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , A" + CStr(AttCount)
                        objSplitCoverage.Save()
                        AttCount = AttCount + 1
                    End If
                End While
            End Using

            If Riskmaster.Common.Conversion.CastToType(Of Double)(sB5CLim, bOut) <> 0 Then
                Log.Write("(28) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage C (Personal Property)"
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , C"
                objSplitCoverage.Save()
            End If

            If Riskmaster.Common.Conversion.CastToType(Of Double)(sB5DLim, bOut) <> 0 Then
                Log.Write("(29) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage D (Additional Living Expense)"
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , D"
                objSplitCoverage.Save()
            End If
            'bSaveParent = false;//split (check with dry run)
            Return
        End If
        teDesc = String.Empty
        trimClass = String.Empty
        If sMajPeril = "005" AndAlso sINSLINE = "TE" AndAlso (sCOVERAGE <> "TNC351" AndAlso sCOVERAGE <> "ML355") Then

            teDesc = ""
            trimClass = sClass.Substring(0, 5)
            '  Right(sClass, 5)
            Select Case trimClass
                Case "68900"
                    teDesc = "Farm Products"
                    Exit Select
                Case "67900"
                    teDesc = "Growing Crops"
                    Exit Select
                Case "63900"
                    teDesc = "Farm Impl & Supplies"
                    ''achla: & in text, just like that ... 
                    Exit Select
                Case "64900"
                    teDesc = "Livestock"
                    Exit Select
                Case "62900"
                    teDesc = "Blanket FPP"
                    Exit Select
                Case Else
                    teDesc = sCOVERAGE
                    Exit Select

            End Select
            Log.Write("(30) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage E (Farm Personal Property - " & teDesc & ")"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , E"
            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", E"
            objSplitCoverage.Save()
            'bSaveParent = false; //split (check with dry run)
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "TF" AndAlso sCOVERAGE <> "ATT" Then
            Log.Write("(31) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage F (Farm Barns, Buildings, and Structures)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , F"
            objSplitCoverage.Save()
            'bSaveParent = false;//split (check with dry run)
            Return
        End If

        ''IRCL0301 - Remove OFPA - Ohio Fair Plan Assessment
        If sMajPeril = "700" AndAlso sCOVERAGE = "OFPA" Then
            Log.Write("Do not display this coverage at all (37) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false; //do not display this coverage at all
            Return
        End If

        ''IRCL0301 - Remove OFPAB - Ohio Fair Plan Assessment Building
        If sMajPeril = "700" AndAlso sCOVERAGE = "OFPAB" Then
            Log.Write("Do not display this coverage at all (38) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false; //do not display this coverage at all
            Return
        End If

        ''IRCL0301 - Remove OFPAP - Ohio Fair Plan Assessment Personal Property
        If sMajPeril = "700" AndAlso sCOVERAGE = "OFPAP" Then
            Log.Write("Do not display this coverage at all (39) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false; //do not display this coverage at all
            Return
        End If

        ''E758 - Remove FO353 - Harvested &&/|| Planting Peak Season
        If sMajPeril = "005" AndAlso sCOVERAGE = "FO353" Then
            Log.Write("Do not display this coverage at all (40) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false; //do not display this coverage at all
            Return
        End If

        If sMajPeril = "010" AndAlso sINSLINE = "TL" Then
            Log.Write("(32) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage L (Liability)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , L"
            objSplitCoverage.Save()

            Log.Write("(33) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage M (Medical Payments)"
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , M"
            objSplitCoverage.Save()

            'bSaveParent = false;//split (check with dry run)
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "FA" AndAlso sClass = "1000" Then
            If Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA")) <> "0" Then
                Log.Write("(34) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage A (Residence)"

                If dtB500 IsNot Nothing AndAlso dtB500.Rows.Count > 0 Then
                    objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AGVA"))
                    'XMLCoverage.Limit.AddLimit("Cov A", "1", Riskmaster.Common.Conversion.ConvertObjToStr(dtB500["B5AGVA")))
                    objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedA, bOut)
                    'XMLCoverage.Deductible.AddDeductible("Ded", "1", baDedA)
                    'Trim(CStr(rsB500("B5AGVA")))
                    sCovALimit = Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AGVA")).Trim()
                End If
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , A"
                objSplitCoverage.Save()
            End If

            If Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5AHVA")) <> "0" Then
                Log.Write("(35) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage B (Related Private Structures)"
                'XMLCoverage.Limit.Limit_Code = 1
                objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5AHVA"))
                'XMLCoverage.Deductible.Deductible_Code = 1
                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedA, bOut)
                'objSplitCoverage.Deductible.Deductible = baDedA
                'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", B"
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , B"
                objSplitCoverage.Save()
            End If

            If Riskmaster.Common.Conversion.ConvertObjToStr(dtB500.Rows(0)("B5USVA3")) <> "0" Then
                Log.Write("(36) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage C (Personal Property)"
                objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA3"))
                'XMLCoverage.Limit.AddLimit("Cov A", "1", Riskmaster.Common.Conversion.ConvertObjToStr(dtB500["B5USVA3")))
                'XMLCoverage.Deductible.AddDeductible("Ded", "1", baDedA)
                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , C"
                objSplitCoverage.Save()
            End If

            Log.Write("(37) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage D (Additional living costs and Loss of rent)"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.ConvertObjToDouble(dtB500.Rows(0)("B5USVA4"))
            'XMLCoverage.Limit.AddLimit("Cov A", "1", Riskmaster.Common.Conversion.ConvertObjToStr(dtB500["B5USVA4")))
            objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedA, bOut)
            'XMLCoverage.Deductible.AddDeductible("Ded", "1", baDedA)
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , D"
            objSplitCoverage.Save()
            'bSaveParent = false;//split (check with dry run)
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "FE" AndAlso sClass = "5000" Then
            Log.Write("(38) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage E (Farm Personal Property)"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(sBBLim1, bOut)
            '.AddLimit("Cov A", "1", sBBLim1)
            objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedB, bOut)
            'Deductible.AddDeductible("Ded", "1", baDedB)
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , E"
            objSplitCoverage.Save()
            'bSaveParent = false;//split (check with dry run)
            Return
        End If

        If sMajPeril = "005" AndAlso sINSLINE = "FF" AndAlso (sClass = "1000" OrElse sClass = "6000" OrElse sClass = "7000" OrElse sClass = "8000") Then
            'multiple class code possibilities
            Log.Write("(39) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage F (Farm Barns, Buildings, & Structures)"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(sB5Lim1, bOut)
            'XMLCoverage.Limit.AddLimit("Cov A", "1", sB5Lim1)
            objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedB, bOut)
            'XMLCoverage.Deductible.AddDeductible("Ded", "1", baDedB)
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , F"
            objSplitCoverage.Save()

            ''Add read of ASBYCPP Here for display of ATT Coverages
            sb = New StringBuilder()

            sb.Append("SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "'AND ")
            sb.Append("BYABCD = '" & sMasterCo & "'AND ")
            sb.Append("BYARTX = '" & sSymbol & "'AND ")
            sb.Append("BYASTX = '" & sPolicyNo & "'AND ")
            sb.Append("BYADNB = '" & sModule & "'AND ")
            sb.Append("BYAGTX = '" & sINSLINE & "'AND ")
            sb.Append("BYBRNB = " & sRISKLOC & " AND ")
            sb.Append("BYEGNB = " & sRISKSUBLOC & " AND ")
            sb.Append("BYAOTX = 'ATT'")

            Log.Write("Read ASBYCPP: Before : SQL = " & sSQL)

            AttCount = 0
            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                While objReader.Read()
                    If objReader.GetString("BYCZST") = "N" Then
                        Log.Write("(40) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " " & objReader.GetString("BYAL36TXT")
                        objSplitCoverage.Limit = objReader.GetDouble("BYAGVA")
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsASBY("BYAGVA")))
                        objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(baDedB, bOut)
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", baDedB)
                        'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", F" + CStr(AttCount)    
                        objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , F" + CStr(AttCount)
                        objSplitCoverage.Save()
                        AttCount = AttCount + 1

                    End If
                End While
            End Using
            'bSaveParent = false;//split (check with dry run)
            Return
        End If
      

        If sMajPeril = "005" AndAlso sINSLINE = "FL" AndAlso sClass = "9999" AndAlso sCOVERAGE = "ML318" Then
            Log.Write("Do not display this coverage at all (41) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false;//do not display this coverage at all
            Return
        End If

        If sMajPeril = "010" AndAlso sINSLINE = "FA" Then
            Log.Write("Do not display this coverage at all (42) : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objPolicyXCvgType.SaveDefaultCoverage = False
            'bSaveParent = false;//do not display this coverage at all
            Return
        End If

        If sMajPeril = "010" AndAlso sINSLINE = "FL" AndAlso sClass = "1000" Then
            Log.Write("(41) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage L (Personal Liability)"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(sBBLim1, bOut)
            'XMLCoverage.Limit.AddLimit("Cov A", "1", sBBLim1)
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , L"
            objSplitCoverage.Save()

            Log.Write("(42) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " Coverage M (Medical Payments)"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(sBBLim2, bOut)
            'XMLCoverage.Limit.AddLimit("Cov A", "1", sBBLim2)
            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & " , M"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If
        If sMajPeril = "040" AndAlso sINSLINE = "FA" AndAlso sClass = "9999" Then
            Log.Write("(43) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " TNC43A Inc Office By Others"
            objSplitCoverage.Limit = Riskmaster.Common.Conversion.CastToType(Of Double)(sBBLim1, bOut)
            'XMLCoverage.Limit.AddLimit("Cov A", "1", sBBLim1)
            objSplitCoverage.Save()
            Return
        End If

        '#Region "if (sMajPeril == "201" && (sINSLINE == "FM" || sINSLINE == "TM"))"
        If sMajPeril = "201" AndAlso (sINSLINE = "FM" OrElse sINSLINE = "TM") Then
            sb = New StringBuilder()
            sb.Append("SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append(" BYABCD = '" & sMasterCo & "' AND ")
            sb.Append(" BYARTX = '" & sSymbol & "' AND ")
            sb.Append(" BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append(" BYADNB = '" & sModule & "' AND ")
            sb.Append(" BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append(" BYAOTX = '" & sCOVERAGE & "' AND ")
            sb.Append(" BYBRNB = " & sRISKLOC & " AND ")
            sb.Append(" BYEGNB = " & sRISKSUBLOC)

            Log.Write("Read ASBYCPP (IMU215): Before : sSQL =" & sSQL)

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sSQL)
                If objReader.Read() Then
                    'CDebug.DebugTrace("Major Peril 201 - INSLINE FM (BY): ", sCoverage, CStr(rsASBY("BYUSVA3")), CStr(rsASBY("BYUSVA4")), CStr(rsASBY("BYAKCD")))
                    Select Case sCOVERAGE
                        Case "IMU176"
                            Log.Write("(44) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IMU176 Personal Property Cov"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", "0") 'achla: What value is set to limit here..? Use of 1..?
                                objSplitCoverage.LimitCovB = 0
                                'XMLCoverage.Limit.AddLimit("Cov A", "2", "0")//come again
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", IMU215-B" 'achla: extra - should be IMU176 here..?  
                            objSplitCoverage.Save()

                            Exit Select
                        Case "IMU215"
                            Log.Write("(45) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IMU-215 Sports Equipment Coverage"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", "0")
                                objSplitCoverage.LimitCovB = 0
                                'XMLCoverage.Limit.AddLimit("Cov A", "2", "0"), come again
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", "0")
                                objSplitCoverage.SelfInsureDeduct = 0
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", IMU215-B"
                            objSplitCoverage.Save()
                            Exit Select
                        Case "IM282"
                            Log.Write("(46) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IM-282 Wedding Presents Coverage"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", "0")
                                objSplitCoverage.LimitCovB = 0
                                'XMLCoverage.Limit.AddLimit("Cov A", "2", "0"), come again
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", "0")                        
                                objSplitCoverage.SelfInsureDeduct = 0
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", IM282-B"
                            objSplitCoverage.Save()
                            Exit Select
                        Case "IM851"
                            Log.Write("(47) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IM851 Farm Machinery Blanket"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAHVA"))
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsASBY("BYAHVA")))
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))                                               
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAHVA"))
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ",-" & CStr(AttCount)
                            objSplitCoverage.Save()
                            Exit Select
                        Case "IM860"
                            Log.Write("(48) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IM-860 Scheduled Livestock Coverage"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", "0")
                                objSplitCoverage.LimitCovB = 0
                                'XMLCoverage.Limit.AddLimit("Cov A", "2", "0"), come again
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", "0")                     
                                objSplitCoverage.SelfInsureDeduct = 0
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", IM860-B"
                            objSplitCoverage.Save()
                            Exit Select
                        Case "IM861"
                            Log.Write("(49) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " IM861 Livestock Coverage"
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                ' XMLCoverage.Limit.AddLimit("Cov A", "1", "0")
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))                                         
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ",-" & CStr(AttCount)
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ",-" & CStr(AttCount)
                            Exit Select
                            objSplitCoverage.Save()
                        Case "PTF1"
                            Log.Write("(50) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " PTF-1 Blanket: " & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BYAL36TXT")).Trim() & " " & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BYAL3VTXT")).Trim()
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAHVA"))
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsASBY("BYAHVA")))
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", PTF1-B"
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", PTF1-B"
                            objSplitCoverage.Save()
                            Exit Select
                        Case "PTF2"
                            Log.Write("(51) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " PTF-2 Blanket: " & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BYAL36TXT")).Trim() & " " & Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("BYAL3VTXT")).Trim()
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAHVA"))
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsASBY("BYAHVA")))
                                ' XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))                       
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", PTF2-B"
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", PTF2-B"
                            objSplitCoverage.Save()
                            Exit Select
                    End Select
                End If
            End Using

            ' IRCL0832 - 08/22/2013
            ' Updated LOGSEQNUM query to check for <= sLogSeq before it was only checking = sLogSeq.
            ' This allows you to get items added as part of a change and the existing items.
            sSQL = "SELECT * FROM BASHLGBZ00 "
            sb = New StringBuilder()
            sb.Append("SELECT * FROM BASHLGBZ00 ")
            sb.Append(" WHERE LOGSEQNUM <= " & sLogSeq & " AND ")
            sb.Append(" LOCATION = '" & sLocation & "' AND ")
            sb.Append(" MASTERCO = '" & sMasterCo & "' AND ")
            sb.Append(" SYMBOL = '" & sSymbol & "' AND ")
            sb.Append(" POLICYNO = '" & sPolicyNo & "' AND ")
            sb.Append(" MODULE = '" & sModule & "' AND ")
            sb.Append(" INSLINE = '" & sINSLINE & "' AND ")
            sb.Append(" RISKLOC = " & sRISKLOC & " AND ")
            sb.Append(" RISKSUBLOC = " & sRISKSUBLOC & " AND ")
            sb.Append(" PRODUCT = '" & sPRODUCT & "' AND ")
            sb.Append(" UNIT = " & sUNIT & " AND ")
            sb.Append(" COVERAGE = '" & sCOVERAGE & "' AND ")
            sb.Append(" COVSEQNUM = " & sCOVSEQNUM & " AND ")
            sb.Append(" RECSTATUS = 'V'")
            AttCount = 0
            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())

                While objReader.Read()
                    Select Case sCOVERAGE
                        Case "IMU215"
                            Log.Write("(52) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("BZPCTX").Trim() & " " & objReader.GetString("BZPDTX").Trim() & " " & objReader.GetString("BZR1TX").Trim()
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = 0
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", "0")
                                'XMLCoverage.Deductible.AddDeductible("Ded", "1", "0")
                                objSplitCoverage.SelfInsureDeduct = 0
                            End If
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey + ", IMU215-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey + ", IMU215-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.Save()
                            Exit Select
                        Case "PTF1"
                            Log.Write("(53) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("BZPCTX").Trim() & " " & objReader.GetString("BZPDTX").Trim() & " " & objReader.GetString("BZR1TX")
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BZAGVA"))
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsBZ00("BZAGVA")))
                                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD"))) 
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey + ", PTF1-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey + ", PTF1-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.Save()
                            Exit Select
                        Case "PTF2"
                            Log.Write("(54) Coverage Split : Case=" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("BZPCTX").Trim() & " " & objReader.GetString("BZPDTX").Trim() & " " & objReader.GetString("BZR1TX").Trim()
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BZAGVA"))
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsBZ00("BZAGVA")))
                                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey + ", PTF2-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey + ", PTF2-A" + ",-" + CStr(AttCount)
                            objSplitCoverage.Save()
                            Exit Select
                        Case Else
                            Log.Write("(55) Coverage Split : Case=(Default)" & sCOVERAGE & ",sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                            objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("BZPCTX").Trim() & " " & objReader.GetString("BZPDTX").Trim() & " " & objReader.GetString("BZR1TX").Trim()
                            If sINSLINE = "FM" Then
                                objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BZAGVA"))
                                'XMLCoverage.Limit.AddLimit("Cov A", "1", CStr(rsBZ00("BZAGVA")))
                                'XMLCoverage.Deductible.AddDeductible("Ded", "1", CStr(rsASBY("BYAKCD")))
                                objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAKCD"))
                            End If
                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey + ",-" + CStr(AttCount)
                            'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey + ",-" + CStr(AttCount)
                            objSplitCoverage.Save()
                            Exit Select
                    End Select
                    'end switch
                    AttCount = AttCount + 1
                    'end loop
                End While
                'end using
            End Using
            Return
        End If
        '#End Region

        If sMajPeril = "039" AndAlso sINSLINE = "RV" Then
            Log.Write("(56) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " UNSBI Un/Underinsured Boaters"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "044" AndAlso sINSLINE = "RV" Then
            Log.Write("(57) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " LIAB Watercraft Liability"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "075" AndAlso sINSLINE = "RV" Then
            Log.Write("(58) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
            objSplitCoverage.CoverageText = sCoverageDesc & "-" & " MED Watercraft Medical Payments"
            objSplitCoverage.Save()
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "200" AndAlso sINSLINE = "RV" Then
            sb = New StringBuilder()
            sb.Append("SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append(" BYABCD = '" & sMasterCo & "' AND ")
            sb.Append(" BYARTX = '" & sSymbol & "' AND ")
            sb.Append(" BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append(" BYADNB = '" & sModule & "' AND ")
            sb.Append(" BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append(" BYAOTX = '" & sCOVERAGE & "' AND ")
            sb.Append(" BYC0NB = " & sCOVSEQNUM & " AND ")
            sb.Append(" BYBRNB = " & sRISKLOC & " AND ")
            sb.Append(" BYEGNB = " & sRISKSUBLOC)

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                If objReader.Read() Then
                    Log.Write("(59) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & " PROP " & objReader.GetString("BYAKCD") & " " & objReader.GetString("BYKKTX") & " " & objReader.GetString("BYAL36TXT") & " (" & objReader.GetString("BYAL3WTXT") & ")"
                    objSplitCoverage.Save()
                Else
                    Log.Write("(60) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & " PROP " & objReader.GetString("B5ANCD") & " " & objReader.GetString("B5DCNB") & " " & objReader.GetString("B5AL36TXT") & " (" & objReader.GetString("B5DDNB") & ")"
                    objSplitCoverage.Save()
                End If
            End Using
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "201" AndAlso sINSLINE = "RV" Then
            sb = New StringBuilder()
            sb.Append("SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append("BYABCD = '" & sMasterCo & "' AND ")
            sb.Append("BYARTX = '" & sSymbol & "' AND ")
            sb.Append("BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append("BYADNB = '" & sModule & "' AND ")
            sb.Append("BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append("BYAOTX = '" & sCOVERAGE & "' AND ")
            sb.Append("BYC0NB = " & sCOVSEQNUM & " AND ")
            sb.Append("BYBRNB = " & sRISKLOC & " AND ")
            sb.Append("BYEGNB = " & sRISKSUBLOC)

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                If objReader.Read() Then
                    Log.Write("(61) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & " PROP Inland Marine, inboards"
                    objSplitCoverage.Save()
                End If
            End Using
            'bSaveParent = false;//split
            Return
        End If

        If sMajPeril = "204" AndAlso sINSLINE = "RV" AndAlso sCOVERAGE = "TRAIL" Then
            sb = New StringBuilder()
            sb.Append(" SELECT * ")
            sb.Append("FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append("BYABCD = '" & sMasterCo & "' AND ")
            sb.Append("BYARTX = '" & sSymbol & "' AND ")
            sb.Append("BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append("BYADNB = '" & sModule & "' AND ")
            sb.Append("BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append("BYAOTX = '" & sCOVERAGE & "' AND ")
            sb.Append("BYC0NB = " & sCOVSEQNUM & " AND ")
            sb.Append("BYBRNB = " & sRISKLOC & " AND ")
            sb.Append("BYEGNB = " & sRISKSUBLOC)

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                If objReader.Read() Then
                    Log.Write("(62) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & " TRAIL " & objReader.GetString("BYALCD").Trim() & " " & objReader.GetString("BYKKTX").Trim() & " " & objReader.GetString("BYAL36TXT").Trim() & " (" & objReader.GetString("BYAL3WTXT").Trim() & ")"
                    objSplitCoverage.Save()
                End If
            End Using
            'bSaveParent = false;//split;
            Return
        End If

        If sMajPeril = "097" AndAlso sINSLINE = "HO" Then
            sb = New StringBuilder()
            sb.Append(" SELECT * ")
            sb.Append(" FROM ASBYCPP ")
            sb.Append(" WHERE BYAACD = '" & sLocation & "' AND ")
            sb.Append(" BYABCD = '" & sMasterCo & "' AND ")
            sb.Append(" BYARTX = '" & sSymbol & "' AND ")
            sb.Append(" BYASTX = '" & sPolicyNo & "' AND ")
            sb.Append(" BYADNB = '" & sModule & "' AND ")
            sb.Append(" BYAGTX = '" & sINSLINE & "' AND ")
            sb.Append(" BYAOTX = 'OM0294' AND ")
            sb.Append(" BYC0NB = " & sCOVSEQNUM & " AND ")
            sb.Append(" BYBRNB = " & sRISKLOC & " AND ")
            sb.Append(" BYEGNB = " & sRISKSUBLOC)

            Dim byLimit As Double = 0
            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sSQL)
                If objReader.Read() Then
                    byLimit = Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("BYAGVA"))
                Else
                    byLimit = 0
                End If
            End Using

            Select Case sSymbol
                Case "HP"
                    If sCOVERAGE = "OM0295" Then
                        Log.Write("(63) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " OM0295 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = 1000
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", "1000")
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")
                        objSplitCoverage.Save()
                    Else
                        Log.Write("(64) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " OM0294 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = byLimit
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", byLimit)
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")      
                        objSplitCoverage.Save()
                    End If
                    Exit Select
                Case "UHP"
                    If sCOVERAGE = "OM0295" Then
                        Log.Write("(65) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " OM0295 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = 1500.0
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", "1500")
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")
                        objSplitCoverage.Save()
                    Else
                        Log.Write("(66) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " OM0294 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = byLimit
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", byLimit)
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")
                        objSplitCoverage.Save()
                    End If
                    Exit Select
                Case "OHP"
                    If sCOVERAGE = "OM0295" Then
                        Log.Write("(67) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & " OM0295 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = 1000.0
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", "1000")
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")
                        objSplitCoverage.Save()
                    Else
                        Log.Write("(68) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                        objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & "OM0294 Water Backup / Discharge"
                        objSplitCoverage.LimitCovA = byLimit
                        'XMLCoverage.Limit.AddLimit("Cov A", "1", byLimit)
                        objSplitCoverage.SelfInsureDeduct = 500.0
                        'XMLCoverage.Deductible.AddDeductible("Ded", "1", "500")
                        objSplitCoverage.Save()
                    End If
            End Select
            'bSaveParent = false;//split
            Return
        End If
        ' IRCL0832 - 08/22/2013
        ' Updated LOGSEQNUM query to check for <= sLogSeq before it was only checking = sLogSeq.
        ' This allows you to get items added as part of a change and the existing items.
        If sMajPeril = "200" AndAlso sINSLINE = "TB" Then
            sb = New StringBuilder()
            sb.Append("SELECT * FROM BASHLGBZ00 ")
            sb.Append(" WHERE LOGSEQNUM <= " & sLogSeq & " AND ")
            sb.Append("LOCATION = '" & sLocation & "' AND ")
            sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
            sb.Append("SYMBOL = '" & sSymbol & "' AND ")
            sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
            sb.Append("MODULE = '" & sModule & "' AND ")
            sb.Append("INSLINE = '" & sINSLINE & "' AND ")
            sb.Append("RISKLOC = " & sRISKLOC & " AND ")
            sb.Append("RISKSUBLOC = " & sRISKSUBLOC & " AND ")
            sb.Append("PRODUCT = '" & sPRODUCT & "' AND ")
            sb.Append("UNIT = " & sUNIT & " AND ")
            sb.Append("COVERAGE = '" & sCOVERAGE & "' AND ")
            sb.Append("COVSEQNUM = " & sCOVSEQNUM & " AND ")
            sb.Append("RECSTATUS = 'V'")

            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                If objReader.Read() Then
                    Log.Write("(69) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & " HO79 " & objReader.GetString("BZALCD").Trim() & " " & objReader.GetString("BZPCTX").Trim() & " " & objReader.GetString("BZR1TX").Trim() & " (" & objReader.GetString("BZPDTX").Trim() & ")"
                    objSplitCoverage.Save()
                End If
            End Using
            'bSaveParent = false;//split
            Return
        End If

        If (sMajPeril = "530" OrElse sMajPeril = "531") AndAlso sINSLINE = "GL" Then
            sb = New StringBuilder()
            sb.Append("SELECT * FROM ASA1REP ")
            sb.Append(" WHERE A1AGTX = '" & sINSLINE & "' AND ")
            sb.Append("A1KJTX = '" & sClass & "' AND ")
            sb.Append("A1A2DT = ( ")
            sb.Append("SELECT MAX(A1A2DT) FROM ASA1REP ")
            sb.Append("WHERE A1AGTX = '" & sINSLINE & "' AND ")
            sb.Append("A1KJTX = '" & sClass & "')")
            Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                If objReader.Read() Then
                    Log.Write("(70) Coverage Split : sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                    objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                    objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("A1KKTX")
                    objSplitCoverage.Save()
                End If
            End Using
            'bSaveParent = false;//split
            Return
        End If


        '#End Region

        '             'At this time we need to pull
        ''coverage codes for this major peril from the support table
        ''and determine how to process them
        ''Put them all in a list and submit them to the SQL statement
        sb = New StringBuilder()
        sb.Append(" SELECT * FROM PNT_MP_X_COV ")
        ''achla: this table is not coming from AS400 query, where's this table
        sb.Append(" WHERE LOB = '" & sLOB & "'")
        sb.Append(" AND INSLINE = '" & sINSLINE & "'")
        sb.Append(" AND PRODUCT = '" & sPRODUCT & "'")
        sb.Append(" AND MAJ_PERIL = '" & sMajPeril & "'")
        If sCOVERAGE <> "*ALL" Then
            sb.Append(" AND COVERAGE = '" & sCOVERAGE & "'")
        End If
        sb.Append(" ORDER BY COVERAGE")

        Log.Write("Read PNT_MP_X_COV : Before ;SQL=" & sb.ToString())
        sCovList = "'" & sCOVERAGE & "'"
        sSplitSched = "Y"
        sLimits = ""
        sDeductibles = ""
        bLoopCov = True
        bNoPMXC = False
        Log.Write("Read PNT_MP_X_COV : Before ;SQL=" & sb.ToString())
        Using objDSPMXC As DataSet = DbFactory.GetDataSet(m_sConnectString, sb.ToString())
            If objDSPMXC IsNot Nothing AndAlso objDSPMXC.Tables.Count > 0 AndAlso objDSPMXC.Tables(0).Rows.Count = 0 Then
                'no record found
                bNoPMXC = True
                If sCOVERAGE = "*ALL" Then
                    bLoopCov = False
                End If
            End If
            iCountPMXC = objDSPMXC.Tables(0).Rows.Count
            'while (iRow < iCountPMXC)
            While bLoopCov

                If Not bNoPMXC Then
                    '#Region "if PNT_MP_X_COV has data"
                    Dim iIdx As Integer = 0
                    sCovList = ""
                    Dim sCovs As String() = Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("COVERAGE")).Split(" ")
                    Dim sComma As String = ""
                    For iIdx = 0 To sCovs.Length - 1
                        '  To UBound(sCovs)
                        sCovList = sCovList & sComma & "'" & sCovs(iIdx) & "'"
                        sComma = ","
                    Next
                    Log.Write("sCovList is " & sCovList)
                    If String.IsNullOrEmpty(Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("LIMITS"))) Then
                        sLimits = ""
                    Else
                        sLimits = Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("LIMITS")).Trim()
                    End If
                    If String.IsNullOrEmpty(objDSPMXC.Tables(0).Rows(iRow)("DEDUCTIBLES").ToString()) Then
                        sDeductibles = ""
                    Else
                        sDeductibles = Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("DEDUCTIBLES")).Trim()
                    End If
                    If String.IsNullOrEmpty(Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("SCHED_ITEMS"))) Then
                        sSplitSched = ""
                    Else
                        sSplitSched = Riskmaster.Common.Conversion.ConvertObjToStr(objDSPMXC.Tables(0).Rows(iRow)("SCHED_ITEMS")).Trim()
                    End If
                End If
                Log.Write("sSplitSched is " & sSplitSched)
                ' if (!bNoPMXC)
                '#End Region

                ''Now we read all of the rating coverages for this particular display line
                sb = New StringBuilder()
                sb.Append("SELECT * FROM BASHLGBY00 ")
                sb.Append(" WHERE LOGSEQNUM = " & sLogSeq & " AND ")
                sb.Append("LOCATION = '" & sLocation & "' AND ")
                sb.Append("MASTERCO = '" & sMasterCo & "' AND ")
                sb.Append("SYMBOL = '" & sSymbol & "' AND ")
                sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
                sb.Append("MODULE = '" & sModule & "' AND ")
                sb.Append("INSLINE = '" & sINSLINE & "' AND ")
                sb.Append("RISKLOC = " & sRISKLOC & " AND ")
                sb.Append("RISKSUBLOC = " & sRISKSUBLOC & " AND ")
                sb.Append("PRODUCT = '" & sPRODUCT & "' AND ")
                sb.Append("UNIT = " & sUNIT & " AND ")
                sb.Append("COVERAGE IN (" & sCovList & ") AND ")
                sb.Append("COVSEQNUM= " & sCOVSEQNUM & " AND ")
                sb.Append("RECSTATUS = 'V'")
                Dim iBY00RowCount As Integer = 0
                Dim iBY00RowIndex As Integer = 0
                sDed = "0"
                sLim1 = "0"
                sLim2 = "0"
                Log.Write("pnt_ BASHLGBY00 query " * sb.ToString())
                Using objDSBY00 As DataSet = DbFactory.GetDataSet(m_sConnectString, sb.ToString())
                    If objDSBY00 Is Nothing Then
                        ''No coverage found, write at Maj Peril and exit
                        'bSaveParent = true;//come again
                        Return
                    End If
                    If objDSBY00 IsNot Nothing AndAlso objDSBY00.Tables.Count > 0 AndAlso objDSBY00.Tables(0).Rows.Count = 0 Then
                        ''No coverage found, write at Maj Peril and exit
                        'bSaveParent = true;//come again
                        Return
                    Else
                        iBY00RowCount = objDSBY00.Tables(0).Rows.Count
                        iBY00RowIndex = 0
                        '#Region "while (iBY00RowIndex < iBY00RowCount)"
                        sDed = "0"
                        sLim1 = "0"
                        sLim2 = "0"
                        While iBY00RowIndex < iBY00RowCount
                            Log.Write("(71) Coverage Split : iBY00RowIndex=" & iBY00RowIndex & ", sINSLine = " & sINSLINE & ",sMajPeril=" & sMajPeril & ", sCOVERAGE = " & sCOVERAGE & ",Symbol=" & sSymbol & ",Class=" & sClass)
                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)
                            If Not sCovList.Contains(",") Then
                                ''Pull coverage description from support tables only if(single coverage
                                sb = New StringBuilder()
                                sb.Append("SELECT AHAQTX FROM ASAHREP ")
                                sb.Append("WHERE AHADTX = '" & sLOB & "' AND ")
                                sb.Append("AHAGTX = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("INSLINE")) & "' AND ")
                                sb.Append("AHANTX = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("PRODUCT")) & "' AND ")
                                sb.Append("AHAOTX = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("COVERAGE")) & "'")

                                Using objReader As DbReader = DbFactory.GetDbReader(m_sConnectString, sb.ToString())
                                    If objReader.Read() Then
                                        objSplitCoverage.CoverageText = sCoverageDesc & "-" & objReader.GetString("AHAQTX")
                                    End If
                                End Using
                                objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("COVERAGE")).Trim() & ", " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("COVSEQNUM")).Trim()
                                'XMLCoverage.LegacyKey = XMLCoverage.LegacyKey & ", " & Trim(rsBY00("COVERAGE")) & ", " & Trim(rsBY00("COVSEQNUM"))
                                ''Setup Limits and deductibles
                                stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("BYAGVA")).Trim()
                                ' Trim(CStr(rsBY00("BYAGVA")))
                                stmp2 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("BYAHVA")).Trim()
                                'Trim(CStr(rsBY00("BYAHVA")))
                                Select Case sLimits
                                    Case "0"
                                        ''Set to 0
                                        sLim1 = "0"
                                        sLim2 = "0"
                                        Exit Select
                                    Case "H"
                                        ''Take highes value
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) > Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) Then
                                            sLim1 = stmp1
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) > Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) Then
                                            sLim2 = stmp2
                                        End If
                                        Exit Select
                                    Case "H1"
                                        ''Take highes value field 1
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) > Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) Then
                                            sLim1 = stmp1
                                        End If
                                        sLim2 = "0"
                                        Exit Select
                                    Case "H2"
                                        ''Take highes value field 2
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) > Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) Then
                                            sLim2 = stmp2
                                        End If
                                        sLim1 = "0"
                                        Exit Select
                                    Case "L"
                                        ' 'Take Lowest Value
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) = 0 Then
                                            sLim1 = stmp1
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) = 0 Then
                                            sLim2 = stmp2
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) <> 0 AndAlso Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) < Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) Then
                                            sLim1 = stmp1
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) <> 0 AndAlso Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) < Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) Then
                                            sLim2 = stmp2
                                        End If
                                        Exit Select
                                    Case "L1"
                                        ' 'Take Lowest Value
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) = 0 Then
                                            sLim1 = stmp1
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) <> 0 AndAlso Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) < Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut) Then
                                            sLim1 = stmp1
                                        End If
                                        sLim2 = "0"
                                        Exit Select
                                    Case "L2"
                                        ''Take Lowest Value
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) = 0 Then
                                            sLim2 = stmp2
                                        End If
                                        If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) <> 0 AndAlso Riskmaster.Common.Conversion.CastToType(Of Long)(stmp2, bOut) < Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut) Then
                                            sLim2 = stmp2
                                        End If
                                        sLim1 = "0"
                                        Exit Select
                                    Case ""
                                        ''Use from BY00
                                        sLim1 = stmp1
                                        sLim2 = stmp2
                                        Exit Select
                                    Case "U"
                                        ''Use from B500
                                        sLim1 = sB5Lim1
                                        sLim2 = sB5Lim2
                                        Exit Select
                                    Case "S"
                                        ' 'Use from SA15
                                        sLim1 = sSA15Lim1
                                        sLim2 = sSA15Lim2
                                        Exit Select
                                    Case "U1"
                                        ' 'Use 1st from B500
                                        sLim1 = sB5Lim1
                                        sLim2 = "0"
                                        Exit Select
                                    Case "U2"
                                        ''Use 2nd from B500
                                        sLim1 = "0"
                                        sLim2 = sB5Lim2
                                        Exit Select
                                    Case Else
                                        ''Replace from PNT_MP_X_COV
                                        sLim1 = sLimits
                                        sLim2 = "0"
                                        Exit Select
                                        'end switch
                                End Select
                            End If

                            stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("BYAKCD")).Trim()
                            ' Trim(CStr(rsBY00("BYAKCD")))
                            Select Case sDeductibles
                                Case "0"
                                    ' ' Set to Zero
                                    sDed = "0"
                                    Exit Select
                                Case "H"
                                    ' ' Use highes found
                                    If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) > Riskmaster.Common.Conversion.CastToType(Of Long)(sDed, bOut) Then
                                        sDed = stmp1
                                    End If
                                    Exit Select
                                Case "L"
                                    ' ' Use Lowest found
                                    If Riskmaster.Common.Conversion.CastToType(Of Long)(sDed, bOut) = 0 Then
                                        sDed = stmp1
                                    End If
                                    If Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) <> 0 AndAlso Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut) < Riskmaster.Common.Conversion.CastToType(Of Long)(sDed, bOut) Then
                                        sDed = stmp1
                                    End If
                                    Exit Select
                                Case ""
                                    ' ' Use directly from BY00
                                    sDed = stmp1
                                    Exit Select
                                Case "U"
                                    ' ' Set to B500
                                    sDed = sB5Ded
                                    Exit Select
                                Case "S"
                                    ' ' Set to SA15
                                    sDed = sSA15Ded
                                    Exit Select
                                Case "P"
                                    ' ' Treat as percentage
                                    stmp1 = stmp1.Replace("%", "")
                                    ' Replace(stmp1, "%", "")
                                    Dim itmp1 As Integer = Riskmaster.Common.Conversion.CastToType(Of Integer)(stmp1, bOut)
                                    If bOut Then
                                        sDed = stmp1
                                    End If
                                    Exit Select
                                Case Else
                                    '' Use table value
                                    sDed = sDeductibles
                                    Exit Select
                            End Select
                            'end switch
                            ''Put calculations for deductibles and limits here
                            If sMajPeril = "307" Then
                                sDed = ((Riskmaster.Common.Conversion.CastToType(Of Long)(sCovALimit, bOut)) * (Riskmaster.Common.Conversion.CastToType(Of Long)(sDed, bOut) / 100)).ToString()
                            End If

                            bHaveSchedule = False
                            'objSplitCoverage.Save();//BY00 coverage gets saved here. //come again
                            ' IRCL0832 - 08/22/2013
                            ' Updated LOGSEQNUM query to check for <= sLogSeq before it was only checking = sLogSeq.
                            ' This allows you to get items added as part of a change and the existing items.
                            If sSplitSched = "Y" Then
                                '#Region "read BASHLGBZ00"
                                sb = New StringBuilder()
                                sb.Append("SELECT * FROM BASHLGBZ00 ")
                                sb.Append(" WHERE LOGSEQNUM <= " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("LOGSEQNUM")) & " AND ")
                                sb.Append("LOCATION = '" & sLocation & "' AND ")
                                sb.Append("       MASTERCO = '" & sMasterCo & "' AND ")
                                sb.Append("SYMBOL = '" & sSymbol & "' AND ")
                                sb.Append("POLICYNO = '" & sPolicyNo & "' AND ")
                                sb.Append("MODULE = '" & sModule & "' AND ")
                                sb.Append("INSLINE = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("INSLINE")) & "' AND ")
                                sb.Append("RISKLOC = " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("RISKLOC")) & " AND ")
                                sb.Append("RISKSUBLOC = " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("RISKSUBLOC")) & " AND ")
                                sb.Append("PRODUCT = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("PRODUCT")) & "' AND ")
                                sb.Append("UNIT = " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("UNITNO")) & " AND ")
                                sb.Append("COVERAGE = '" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("COVERAGE")) & "' AND ")
                                sb.Append("COVSEQNUM = " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBY00.Tables(0).Rows(iBY00RowIndex)("COVSEQNUM")) & " AND ")
                                sb.Append("RECSTATUS = 'V'")
                                Dim iBZ00RowCount As Integer = 0
                                Dim iBZ00RowIndex As Integer = 0
                                Using objDSBZ00 As DataSet = DbFactory.GetDataSet(m_sConnectString, sb.ToString())
                                    If objDSBZ00 IsNot Nothing AndAlso objDSBZ00.Tables.Count > 0 AndAlso objDSBZ00.Tables(0).Rows.Count > 0 Then
                                        iBZ00RowCount = objDSBZ00.Tables(0).Rows.Count
                                        bFirst = True
                                        While iBZ00RowIndex < iBZ00RowCount
                                            bHaveSchedule = True
                                            objSplitCoverage = CreateCoverageNode(objPolicyXCvgType, objSplitCoverage, bFirst)

                                            If Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZPCTX")).Trim() <> "" OrElse Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZPDTX")) <> "" Then
                                                objSplitCoverage.CoverageText = sCoverageDesc & "-" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZPCTX")).Trim() & " " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZPDTX")).Trim()
                                            End If
                                            stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZAKCD")).Trim()
                                            'Trim(CStr(rsBZ00("BZAKCD")))
                                            If stmp1 = "0" Then
                                                stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BYAKCD"))
                                            End If
                                            If sDed = "0" Then
                                                stmp1 = sDed
                                            End If
                                            Dim ltemp1 As Long = Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut)
                                            If bOut Then
                                                If ltemp1 <> 0 Then
                                                    objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(stmp1, bOut)
                                                End If
                                            End If
                                            stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZAGVA"))
                                            If sLim1 = "0" Then
                                                stmp1 = sLim1
                                            End If
                                            ltemp1 = Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut)
                                            If bOut Then
                                                If ltemp1 <> 0 Then
                                                    'XMLCoverage.Limit.AddLimit("Limit 1", 1, stmp1)//come again                            
                                                    objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.CastToType(Of Double)(stmp1, bOut)
                                                End If
                                            End If
                                            stmp1 = "0" & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBZ00RowIndex)("BZAHVA"))
                                            If sLim2 = "0" Then
                                                stmp1 = sLim2
                                            End If
                                            ltemp1 = Riskmaster.Common.Conversion.CastToType(Of Long)(stmp1, bOut)
                                            If bOut Then
                                                If ltemp1 <> 0 Then
                                                    'XMLCoverage.Limit.AddLimit("Limit 2", 2, stmp1)//come again   
                                                    objSplitCoverage.LimitCovB = Riskmaster.Common.Conversion.CastToType(Of Double)(stmp1, bOut)
                                                End If
                                            End If


                                            'XMLCoverage.LegacyKey = LegacyKey & ", " & Trim(rsBZ00("COVERAGE")) & ", " & Trim(rsBZ00("ITEMSEQNUM"))
                                            objSplitCoverage.CoverageKey = objSplitCoverage.CoverageKey & ", " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBY00RowIndex)("COVERAGE")).Trim() & ", " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBY00RowIndex)("COVSEQNUM")).Trim() & ", " & Riskmaster.Common.Conversion.ConvertObjToStr(objDSBZ00.Tables(0).Rows(iBY00RowIndex)("LOGSEQNUM")).Trim()
                                            objSplitCoverage.Save()
                                            'split
                                            iBZ00RowIndex += 1
                                            'end while (iBZ00RowIndex < iBZ00RowCount)
                                        End While
                                    Else
                                        'end  if (objDSBZ00 != null && objDSBZ00.Tables.Count > 0 && objDSBZ00.Tables[0].Rows.Count == 0)
                                        'save the split coverage that was created on BY00 iteration since BZ00 have no records
                                        objSplitCoverage.Save()
                                    End If
                                    'end using objDSBZ00    
                                    '#End Region
                                End Using
                            End If
                            'end if(sSplitSched == "Y")
                            bFirst = True
                            iBY00RowIndex += 1
                            'end while (iBY00RowIndex < iBY00RowCount)
                            '#End Region
                        End While
                        'else DSBY00
                    End If
                End Using
                'end using objDSBY00
                '#Region "if(sSplitSched != "Y" || bHaveSchedule ==false )"
                If sSplitSched <> "Y" OrElse bHaveSchedule = False Then
                    Dim lDed As Long = Riskmaster.Common.Conversion.CastToType(Of Long)(sDed, bOut)
                    If bOut Then
                        If lDed <> 0 Then
                            'XMLCoverage.Deductible.AddDeductible("Ded 1", "1", CLng(sDed))
                            objSplitCoverage.SelfInsureDeduct = Riskmaster.Common.Conversion.CastToType(Of Double)(sDed, bOut)
                        End If
                    End If
                    Dim lLim1 As Long = Riskmaster.Common.Conversion.CastToType(Of Long)(sLim1, bOut)
                    If bOut Then
                        'if bout is true, sLim1 is numeric
                        If lLim1 <> 0 Then
                            'XMLCoverage.Limit.AddLimit("Cov A", "1", CLng(sLim1))                                  
                            objSplitCoverage.LimitCovA = Riskmaster.Common.Conversion.CastToType(Of Double)(sLim1, bOut)
                        End If
                    End If
                    Dim lLim2 As Long = Riskmaster.Common.Conversion.CastToType(Of Long)(sLim2, bOut)
                    If bOut Then
                        'if(IsNumeric(sLim2) )
                        If lLim2 <> 0 Then
                            'XMLCoverage.Limit.AddLimit("Cov B", "2", CLng(sLim2))                                  
                            objSplitCoverage.LimitCovB = Riskmaster.Common.Conversion.CastToType(Of Double)(sLim2, bOut)
                        End If
                    End If
                    'split
                    objSplitCoverage.Save()
                End If
                '#End Region
                If bNoPMXC OrElse iRow = iCountPMXC Then
                    bLoopCov = False
                Else
                    iRow += 1
                    If iRow = iCountPMXC Then
                        bLoopCov = False
                    End If
                End If
                bFirst = False
                'while (bLoopCov) //(iRow < iCountPMXC) 
            End While
        End Using
        'using (DataSet objDSPMXC = DbFactory.GetDataSet(m_sConnectString, sb.ToString()))
        ' ;false;//come again
        Return
        'end try
    Catch e As Exception
        'false;//come again
        'Log.Write("Exception occoured while executing the script -" & Exception.Message)
        Return
    Finally
        objPolicy = Nothing
        objPolicyXUnit = Nothing
        objCache = Nothing
        sb = Nothing
        'objDSB500 = Nothing
        'dtB500 = Nothing
    End Try
End Sub



Private Function CreateCoverageNode(objPolicyXCvgType As PolicyXCvgType, objSplitCoverage As PolicyXCvgType, bFirst As Boolean) As PolicyXCvgType
    If bFirst Then
        'if the coverage split is called for the first time, copy default coverage's value in the                
        objSplitCoverage.CvgClassCode = ""
        objSplitCoverage.CoverageTypeCode = objPolicyXCvgType.CoverageTypeCode
        objSplitCoverage.CvgSequenceNo = objPolicyXCvgType.CvgSequenceNo
        objSplitCoverage.CvgSequenceNo = objPolicyXCvgType.CvgSequenceNo
        objSplitCoverage.TransSequenceNo = objPolicyXCvgType.TransSequenceNo
        objSplitCoverage.EffectiveDate = objPolicyXCvgType.EffectiveDate
        objSplitCoverage.ExpirationDate = objPolicyXCvgType.ExpirationDate
        objSplitCoverage.PolcvgRowId = objPolicyXCvgType.PolcvgRowId
        objSplitCoverage.PolicyUnitRowId = objPolicyXCvgType.PolicyUnitRowId
        objSplitCoverage.CoverageKey = objPolicyXCvgType.CoverageKey
        objSplitCoverage.SubLine = objPolicyXCvgType.SubLine
    Else
        'create new object
        objSplitCoverage = DirectCast(objPolicyXCvgType.Context.Factory.GetDataModelObject("PolicyXCvgType", False), PolicyXCvgType)
    End If
    'get rid of all the limits and deductibles
    objSplitCoverage.Limit = 0
    objSplitCoverage.LimitCovA = 0
    objSplitCoverage.LimitCovB = 0
    objSplitCoverage.LimitCovC = 0
    objSplitCoverage.LimitCovD = 0
    objSplitCoverage.LimitCovE = 0
    objSplitCoverage.LimitCovF = 0
    objSplitCoverage.SelfInsureDeduct = 0
    objSplitCoverage.SaveDefaultCoverage = False

    If Not bFirst Then
        objSplitCoverage.CvgClassCode = ""
        objSplitCoverage.CoverageTypeCode = objPolicyXCvgType.CoverageTypeCode
        objSplitCoverage.CvgSequenceNo = objPolicyXCvgType.CvgSequenceNo
        objSplitCoverage.CvgSequenceNo = objPolicyXCvgType.CvgSequenceNo
        objSplitCoverage.TransSequenceNo = objPolicyXCvgType.TransSequenceNo
        objSplitCoverage.EffectiveDate = objPolicyXCvgType.EffectiveDate
        objSplitCoverage.ExpirationDate = objPolicyXCvgType.ExpirationDate
        objSplitCoverage.PolcvgRowId = objPolicyXCvgType.PolcvgRowId
        objSplitCoverage.PolicyUnitRowId = objPolicyXCvgType.PolicyUnitRowId
        objSplitCoverage.CoverageKey = objPolicyXCvgType.CoverageKey
        objSplitCoverage.SubLine = objPolicyXCvgType.SubLine
    End If
    objPolicyXCvgType.SaveDefaultCoverage = False
    'since the aprent coverage is split, we will not save the default coverage
    bFirst = False
    Return objSplitCoverage
End Function

'add subline in createcovnode function

'coverage text left blank for objPolicyxCvgType should be addressed