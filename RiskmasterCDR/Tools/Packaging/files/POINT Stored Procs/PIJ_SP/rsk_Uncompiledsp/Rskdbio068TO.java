// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.ProcessInfoObject;
import bphx.c2ab.util.DataConverter;

public class Rskdbio068TO extends ProcessInfoObject {
	private String lsLocation = "";
	private String lsMasterco = "";
	private String lsSymbol = "";
	private String lsPolicyNum = "";
	private String lsModule = "";
	private String lsIntType = "";
	private String lsIntLoc = "";
	private String lsIntSeq = "";
	private String lsName = "";
	private String lsAddress = "";
	private String lsCity = "";
	private String lsState = "";
	private String lsInsLine = "";
	private String lsProduct = "";
	private String lsRiskloc = "";
	private String lsSubLoc = "";
	private String lsUnitNum = "";
	private String lsCovg = "";
	private String lsCovgSeq = "";
	private String lsItemCode = "";
	private String lsItemSeq = "";
	private String lsRateState = "";

	public Rskdbio068TO(byte[] lsInputParams) {
		basicInitialization();
		setLsInputParams(lsInputParams);
	}

	public Rskdbio068TO() {
		basicInitialization();
	}

	public static int getLsInputParamsSize() {
		return 170;
	}

	public void setLsInputParams(byte[] buf) {
		int offset = 1;
		lsLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsMasterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		lsModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsIntLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsIntSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsName = DataConverter.readString(buf, offset, 31);
		offset += 31;
		lsAddress = DataConverter.readString(buf, offset, 31);
		offset += 31;
		lsCity = DataConverter.readString(buf, offset, 29);
		offset += 29;
		lsState = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsInsLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsProduct = DataConverter.readString(buf, offset, 6);
		offset += 6;
		lsRiskloc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsSubLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsUnitNum = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsCovg = DataConverter.readString(buf, offset, 6);
		offset += 6;
		lsCovgSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsItemCode = DataConverter.readString(buf, offset, 6);
		offset += 6;
		lsItemSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsRateState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		assert (offset - 1 == buf.length) : offset + "-1 != " + buf.length;
	}

	public byte[] getLsInputParams() {
		byte[] buf = new byte[getLsInputParamsSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, lsLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsMasterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, lsModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsIntLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsIntSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsName, 31);
		offset += 31;
		DataConverter.writeString(buf, offset, lsAddress, 31);
		offset += 31;
		DataConverter.writeString(buf, offset, lsCity, 29);
		offset += 29;
		DataConverter.writeString(buf, offset, lsState, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsInsLine, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, lsRiskloc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsSubLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsUnitNum, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsCovg, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, lsCovgSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsItemCode, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, lsItemSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsRateState, 2);
		offset += 2;
		assert (offset - 1 == buf.length) : offset + "-1 != " + buf.length;
		return buf;
	}

	public void basicInitialization() {}

	public String getLsInsLine() {
		return this.lsInsLine;
	}

	public String getLsLocation() {
		return this.lsLocation;
	}

	public String getLsMasterco() {
		return this.lsMasterco;
	}

	public String getLsSymbol() {
		return this.lsSymbol;
	}

	public String getLsPolicyNum() {
		return this.lsPolicyNum;
	}

	public String getLsModule() {
		return this.lsModule;
	}

	public String getLsIntType() {
		return this.lsIntType;
	}

	public String getLsIntLoc() {
		return this.lsIntLoc;
	}

	public String getLsIntSeq() {
		return this.lsIntSeq;
	}

	public String getLsProduct() {
		return this.lsProduct;
	}

	public String getLsRiskloc() {
		return this.lsRiskloc;
	}

	public String getLsSubLoc() {
		return this.lsSubLoc;
	}

	public String getLsUnitNum() {
		return this.lsUnitNum;
	}

	public String getLsCovg() {
		return this.lsCovg;
	}

	public String getLsCovgSeq() {
		return this.lsCovgSeq;
	}

	public String getLsItemCode() {
		return this.lsItemCode;
	}

	public String getLsItemSeq() {
		return this.lsItemSeq;
	}

	public String getLsName() {
		return this.lsName;
	}

	public String getLsAddress() {
		return this.lsAddress;
	}

	public String getLsCity() {
		return this.lsCity;
	}

	public String getLsState() {
		return this.lsState;
	}

	public void setLsIntType(String lsIntType) {
		this.lsIntType = lsIntType;
	}

	public void setLsIntLoc(String lsIntLoc) {
		this.lsIntLoc = lsIntLoc;
	}

	public void setLsIntSeq(String lsIntSeq) {
		this.lsIntSeq = lsIntSeq;
	}
}
