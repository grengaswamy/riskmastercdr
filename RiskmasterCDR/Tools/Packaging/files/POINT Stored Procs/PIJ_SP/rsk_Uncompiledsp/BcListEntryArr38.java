package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODecimal;

/**
 */

public class BcListEntryArr38 implements Visitable {
	private char bcTrans0stat = ' ';
	private String bcSymbol = "";
	private String bcPolicy0num = "";
	private String bcModule = "";
	private String bcMaster0co = "";
	private String bcLocation = "";
	private String bcEffdt = "";
	private String bcExpdt = "";
	private String bcRisk0state = "";
	private String bcCompany0no = "";
	private String bcAgency = "";
	private String bcLine0bus = "";
	private String bcZip0post = "";
	private String bcLastname = "";
	private String bcFirstname = "";
	private char bcMiddlename = ' ';
	private String bcAdd0line02 = "";
	private String bcAdd0line03 = "";
	private String bcAdd0line04 = "";
	private String bcState = "";
	private char bcRenewal0cd = ' ';
	private char bcIssue0code = ' ';
	private String bcFedtaxid = "";
	private char bcPay0code = ' ';
	private char bcMode0code = ' ';
	private String bcSort0name = "";
	private String bcCust0no = "";
	private String bcSpec0use0a = "";
	private String bcSpec0use0b = "";
	private String bcCanceldate = "";
	private String bcMrsseq = "";
	private String bcGroupno = "";
	private String bcPhone1 = "";
	private String bcSsn = "";
	private char bcNametype = ' ';
	private char bcNamestatus = ' ';
	private String bcStatusDesc = "";
	private ABODecimal bcTot0ag0prm = new ABODecimal(20, 2);
	private String bcRenewal0cdDesc = "";
	private String bcori0incept     = ""; 
	private String bcinsurername    = "";
	private String bcinsureraddress = "";
	private String 	bcinsurercityst  = "";
	private String bcinsurerzip     = "";
	private long bcclientseq      = 0;
	private int bcaddressseq     = 0;
	private String bcbirthdate = "";

	public String getBcori0incept() {
		return bcori0incept;
	}

	public void setBcori0incept(String bcori0incept) {
		this.bcori0incept = bcori0incept;
	}

	public String getBcinsurername() {
		return bcinsurername;
	}

	public void setBcinsurername(String bcinsurername) {
		this.bcinsurername = bcinsurername;
	}

	public String getBcinsureraddress() {
		return bcinsureraddress;
	}

	public void setBcinsureraddress(String bcinsureraddress) {
		this.bcinsureraddress = bcinsureraddress;
	}

	public String getBcinsurercityst() {
		return bcinsurercityst;
	}

	public void setBcinsurercityst(String bcinsurercityst) {
		this.bcinsurercityst = bcinsurercityst;
	}

	public String getBcinsurerzip() {
		return bcinsurerzip;
	}

	public void setBcinsurerzip(String bcinsurerzip) {
		this.bcinsurerzip = bcinsurerzip;
	}

	public long getBcclientseq() {
		return bcclientseq;
	}

	public void setBcclientseq(long bcclientseq) {
		this.bcclientseq = bcclientseq;
	}

	public int getBcaddressseq() {
		return bcaddressseq;
	}

	public void setBcaddressseq(int bcaddressseq) {
		this.bcaddressseq = bcaddressseq;
	}

	public String getBcbirthdate() {
		return bcbirthdate;
	}

	public void setBcbirthdate(String bcbirthdate) {
		this.bcbirthdate = bcbirthdate;
	}

	public void setBcTrans0stat(char bcTrans0stat) {
		this.bcTrans0stat = bcTrans0stat;
	}

	public char getBcTrans0stat() {
		return this.bcTrans0stat;
	}

	public void setBcSymbol(String bcSymbol) {
		this.bcSymbol = bcSymbol;
	}

	public String getBcSymbol() {
		return this.bcSymbol;
	}

	public void setBcPolicy0num(String bcPolicy0num) {
		this.bcPolicy0num = bcPolicy0num;
	}

	public String getBcPolicy0num() {
		return this.bcPolicy0num;
	}

	public void setBcModule(String bcModule) {
		this.bcModule = bcModule;
	}

	public String getBcModule() {
		return this.bcModule;
	}

	public void setBcMaster0co(String bcMaster0co) {
		this.bcMaster0co = bcMaster0co;
	}

	public String getBcMaster0co() {
		return this.bcMaster0co;
	}

	public void setBcLocation(String bcLocation) {
		this.bcLocation = bcLocation;
	}

	public String getBcLocation() {
		return this.bcLocation;
	}

	public void setBcEffdt(String bcEffdt) {
		this.bcEffdt = bcEffdt;
	}

	public String getBcEffdt() {
		return this.bcEffdt;
	}

	public void setBcExpdt(String bcExpdt) {
		this.bcExpdt = bcExpdt;
	}

	public String getBcExpdt() {
		return this.bcExpdt;
	}

	public void setBcRisk0state(String bcRisk0state) {
		this.bcRisk0state = bcRisk0state;
	}

	public String getBcRisk0state() {
		return this.bcRisk0state;
	}

	public void setBcCompany0no(String bcCompany0no) {
		this.bcCompany0no = bcCompany0no;
	}

	public String getBcCompany0no() {
		return this.bcCompany0no;
	}

	public void setBcAgency(String bcAgency) {
		this.bcAgency = bcAgency;
	}

	public String getBcAgency() {
		return this.bcAgency;
	}

	public void setBcLine0bus(String bcLine0bus) {
		this.bcLine0bus = bcLine0bus;
	}

	public String getBcLine0bus() {
		return this.bcLine0bus;
	}

	public void setBcZip0post(String bcZip0post) {
		this.bcZip0post = bcZip0post;
	}

	public String getBcZip0post() {
		return this.bcZip0post;
	}

	public void setBcLastname(String bcLastname) {
		this.bcLastname = bcLastname;
	}

	public String getBcLastname() {
		return this.bcLastname;
	}

	public void setBcFirstname(String bcFirstname) {
		this.bcFirstname = bcFirstname;
	}

	public String getBcFirstname() {
		return this.bcFirstname;
	}

	public void setBcMiddlename(char bcMiddlename) {
		this.bcMiddlename = bcMiddlename;
	}

	public char getBcMiddlename() {
		return this.bcMiddlename;
	}

	public void setBcAdd0line02(String bcAdd0line02) {
		this.bcAdd0line02 = bcAdd0line02;
	}

	public String getBcAdd0line02() {
		return this.bcAdd0line02;
	}

	public void setBcAdd0line03(String bcAdd0line03) {
		this.bcAdd0line03 = bcAdd0line03;
	}

	public String getBcAdd0line03() {
		return this.bcAdd0line03;
	}

	public void setBcAdd0line04(String bcAdd0line04) {
		this.bcAdd0line04 = bcAdd0line04;
	}

	public String getBcAdd0line04() {
		return this.bcAdd0line04;
	}

	public void setBcState(String bcState) {
		this.bcState = bcState;
	}

	public String getBcState() {
		return this.bcState;
	}

	public void setBcRenewal0cd(char bcRenewal0cd) {
		this.bcRenewal0cd = bcRenewal0cd;
	}

	public char getBcRenewal0cd() {
		return this.bcRenewal0cd;
	}

	public void setBcIssue0code(char bcIssue0code) {
		this.bcIssue0code = bcIssue0code;
	}

	public char getBcIssue0code() {
		return this.bcIssue0code;
	}

	public void setBcFedtaxid(String bcFedtaxid) {
		this.bcFedtaxid = bcFedtaxid;
	}

	public String getBcFedtaxid() {
		return this.bcFedtaxid;
	}

	public void setBcPay0code(char bcPay0code) {
		this.bcPay0code = bcPay0code;
	}

	public char getBcPay0code() {
		return this.bcPay0code;
	}

	public void setBcMode0code(char bcMode0code) {
		this.bcMode0code = bcMode0code;
	}

	public char getBcMode0code() {
		return this.bcMode0code;
	}

	public void setBcSort0name(String bcSort0name) {
		this.bcSort0name = bcSort0name;
	}

	public String getBcSort0name() {
		return this.bcSort0name;
	}

	public void setBcCust0no(String bcCust0no) {
		this.bcCust0no = bcCust0no;
	}

	public String getBcCust0no() {
		return this.bcCust0no;
	}

	public void setBcSpec0use0a(String bcSpec0use0a) {
		this.bcSpec0use0a = bcSpec0use0a;
	}

	public String getBcSpec0use0a() {
		return this.bcSpec0use0a;
	}

	public void setBcSpec0use0b(String bcSpec0use0b) {
		this.bcSpec0use0b = bcSpec0use0b;
	}

	public String getBcSpec0use0b() {
		return this.bcSpec0use0b;
	}

	public void setBcCanceldate(String bcCanceldate) {
		this.bcCanceldate = bcCanceldate;
	}

	public String getBcCanceldate() {
		return this.bcCanceldate;
	}

	public void setBcMrsseq(String bcMrsseq) {
		this.bcMrsseq = bcMrsseq;
	}

	public String getBcMrsseq() {
		return this.bcMrsseq;
	}

	public void setBcGroupno(String bcGroupno) {
		this.bcGroupno = bcGroupno;
	}

	public String getBcGroupno() {
		return this.bcGroupno;
	}

	public void setBcPhone1(String bcPhone1) {
		this.bcPhone1 = bcPhone1;
	}

	public String getBcPhone1() {
		return this.bcPhone1;
	}

	public void setBcSsn(String bcSsn) {
		this.bcSsn = bcSsn;
	}

	public String getBcSsn() {
		return this.bcSsn;
	}

	public void setBcNametype(char bcNametype) {
		this.bcNametype = bcNametype;
	}

	public char getBcNametype() {
		return this.bcNametype;
	}

	public void setBcNamestatus(char bcNamestatus) {
		this.bcNamestatus = bcNamestatus;
	}

	public char getBcNamestatus() {
		return this.bcNamestatus;
	}

	public void setBcStatusDesc(String bcStatusDesc) {
		this.bcStatusDesc = bcStatusDesc;
	}

	public String getBcStatusDesc() {
		return this.bcStatusDesc;
	}

	public void setBcTot0ag0prm(ABODecimal bcTot0ag0prm) {
		this.bcTot0ag0prm.assign(bcTot0ag0prm);
	}

	public ABODecimal getBcTot0ag0prm() {
		return this.bcTot0ag0prm.clone();
	}
	
	public void setBcRenewal0cdDesc(String bcRenewal0cdDesc) {
		this.bcRenewal0cdDesc = bcRenewal0cdDesc;
	}

	public String getBcRenewal0cdDesc() {
		return this.bcRenewal0cdDesc;
	}

	public void accept(Visitor visitor) {
		visitor.visitChar("BC-TRANS0STAT", bcTrans0stat);
		visitor.visitString("BC-SYMBOL", bcSymbol);
		visitor.visitString("BC-POLICY0NUM", bcPolicy0num);
		visitor.visitString("BC-MODULE", bcModule);
		visitor.visitString("BC-MASTER0CO", bcMaster0co);
		visitor.visitString("BC-LOCATION", bcLocation);
		visitor.visitString("BC-EFFDT", bcEffdt);
		visitor.visitString("BC-EXPDT", bcExpdt);
		visitor.visitString("BC-RISK0STATE", bcRisk0state);
		visitor.visitString("BC-COMPANY0NO", bcCompany0no);
		visitor.visitString("BC-AGENCY", bcAgency);
		visitor.visitString("BC-LINE0BUS", bcLine0bus);
		visitor.visitString("BC-ZIP0POST", bcZip0post);
		visitor.visitString("BC-LASTNAME", bcLastname);
		visitor.visitString("BC-FIRSTNAME", bcFirstname);
		visitor.visitChar("BC-MIDDLENAME", bcMiddlename);
		visitor.visitString("BC-ADD0LINE02", bcAdd0line02);
		visitor.visitString("BC-ADD0LINE03", bcAdd0line03);
		visitor.visitString("BC-ADD0LINE04", bcAdd0line04);
		visitor.visitString("BC-STATE", bcState);
		visitor.visitChar("BC-RENEWAL0CD", bcRenewal0cd);
		visitor.visitChar("BC-ISSUE0CODE", bcIssue0code);
		visitor.visitString("BC-FEDTAXID", bcFedtaxid);
		visitor.visitChar("BC-PAY0CODE", bcPay0code);
		visitor.visitChar("BC-MODE0CODE", bcMode0code);
		visitor.visitString("BC-SORT0NAME", bcSort0name);
		visitor.visitString("BC-CUST0NO", bcCust0no);
		visitor.visitString("BC-SPEC0USE0A", bcSpec0use0a);
		visitor.visitString("BC-SPEC0USE0B", bcSpec0use0b);
		visitor.visitString("BC-CANCELDATE", bcCanceldate);
		visitor.visitString("BC-MRSSEQ", bcMrsseq);
		visitor.visitString("BC-GROUPNO", bcGroupno);
		visitor.visitString("BC-PHONE1", bcPhone1);
		visitor.visitString("BC-SSN", bcSsn);
		visitor.visitChar("BC-NAMETYPE", bcNametype);
		visitor.visitChar("BC-NAMESTATUS", bcNamestatus);
		visitor.visitString("BC-STATUS-DESC", bcStatusDesc);
		visitor.visitDecimal("BC-TOT0AG0PRM", bcTot0ag0prm.clone());
		visitor.visitString("BC-RENEWAL0CD-DESC", bcRenewal0cdDesc);
		visitor.visitString("BC-ORI0INCEPT", bcori0incept);
		visitor.visitString("BC-INSURER-NAME", bcinsurername);
		visitor.visitString("BC-INSURER-ADDRESS", bcinsureraddress);
		visitor.visitString("BC-INSURER-CITYST", bcinsurercityst);
		visitor.visitString("BC-INSURER-ZIP", bcinsurerzip);
		visitor.visitLong("BC-CLIENT-SEQ", bcclientseq);
		visitor.visitInt("BC-ADDRESS-SEQ", bcaddressseq);
		visitor.visitString("BC-BIRTHDATE", bcbirthdate);	
		
	}

	public static int getSize() {	
		return 594;
	}

	public byte[] getData() { 
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeChar(buf, offset, bcTrans0stat);
		offset += 1;
		DataConverter.writeString(buf, offset, bcSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, bcPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcEffdt, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcExpdt, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcRisk0state, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcCompany0no, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcAgency, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcLine0bus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, bcZip0post, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, bcLastname, 60);
		offset += 60;
		DataConverter.writeString(buf, offset, bcFirstname, 40);
		offset += 40;
		DataConverter.writeChar(buf, offset, bcMiddlename);
		offset += 1;
		DataConverter.writeString(buf, offset, bcAdd0line02, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcAdd0line03, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcAdd0line04, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, bcState, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, bcRenewal0cd);
		offset += 1;
		DataConverter.writeChar(buf, offset, bcIssue0code);
		offset += 1;
		DataConverter.writeString(buf, offset, bcFedtaxid, 12);
		offset += 12;
		DataConverter.writeChar(buf, offset, bcPay0code);
		offset += 1;
		DataConverter.writeChar(buf, offset, bcMode0code);
		offset += 1;
		DataConverter.writeString(buf, offset, bcSort0name, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, bcCust0no, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, bcSpec0use0a, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, bcSpec0use0b, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcCanceldate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcMrsseq, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, bcGroupno, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, bcPhone1, 32);
		offset += 32;
		DataConverter.writeString(buf, offset, bcSsn, 11);
		offset += 11;
		DataConverter.writeChar(buf, offset, bcNametype);
		offset += 1;
		DataConverter.writeChar(buf, offset, bcNamestatus);
		offset += 1;
		DataConverter.writeString(buf, offset, bcStatusDesc, 50);
		offset += 50;
		//FSIT-152640 Resolution-55204 -Begin
		/*DataConverter.writeDecimal(buf, offset, bcTot0ag0prm.clone(), 9, 2, SignType.SIGNED_TRAILING);
		offset += 11;*/
		DataConverter.writeDecimal(buf, offset, bcTot0ag0prm.clone(), 18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		//FSIT-152640 Resolution-55204 -End
		DataConverter.writeString(buf, offset, bcRenewal0cdDesc, 45);
		offset += 45;
		DataConverter.writeString(buf, offset, bcori0incept, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcinsurername, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcinsureraddress, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcinsurercityst, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcinsurerzip, 10);
		offset += 10;
		DataConverter.writeLong(buf, offset, bcclientseq, 10);
		offset += 10;
		DataConverter.writeInt(buf, offset, bcaddressseq, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcbirthdate, 10);
		offset += 10;
		return buf;
	}

	public void initSpaces() {
		bcTrans0stat = ' ';
		bcSymbol = "";
		bcPolicy0num = "";
		bcModule = "";
		bcMaster0co = "";
		bcLocation = "";
		bcEffdt = "";
		bcExpdt = "";
		bcRisk0state = "";
		bcCompany0no = "";
		bcAgency = "";
		bcLine0bus = "";
		bcZip0post = "";
		bcLastname = "";
		bcFirstname = "";
		bcMiddlename = ' ';
		bcAdd0line02 = "";
		bcAdd0line03 = "";
		bcAdd0line04 = "";
		bcState = "";
		bcRenewal0cd = ' ';
		bcIssue0code = ' ';
		bcFedtaxid = "";
		bcPay0code = ' ';
		bcMode0code = ' ';
		bcSort0name = "";
		bcCust0no = "";
		bcSpec0use0a = "";
		bcSpec0use0b = "";
		bcCanceldate = "";
		bcMrsseq = "";
		bcGroupno = "";
		bcPhone1 = "";
		bcSsn = "";
		bcNametype = ' ';
		bcNamestatus = ' ';
		bcStatusDesc = "";
		bcTot0ag0prm.assign(ABODecimal.createNaN());
		bcRenewal0cdDesc = "";
		bcori0incept     = "";
		bcinsurername   = "";
		bcinsureraddress = "";
		bcinsurercityst = "";
		bcinsurerzip    = "";
		bcclientseq = 0;     
		bcaddressseq = 0;    
		bcbirthdate   = "";
	}
		
}