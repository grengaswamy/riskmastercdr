// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.to;

import com.csc.pt.svc.data.id.Bashlgb500ID;

import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;
import java.math.BigDecimal;

//************************************************************************************************
//* Programmer  : Ankur Gupta							Date       : 10/13/2011                  *
//* Description : This java program will contain only non key fields of BASHLGB500 file and all  *
//*				  key fields will be put in separate file BASHLGB500ID.							 *
//************************************************************************************************

public class Bashlgb500TO {
	private DBAccessStatus dBAccessStatus;
	private Bashlgb500ID id = new Bashlgb500ID();	
	private String loguser = "";
	private ABODate logdate = new ABODate();
	private ABOTime logtime = new ABOTime();
	private char b5c7st;
	private int b5andt;
	private short b5afnb;
	private int b5aldt;
	private int b5afdt;
	private String b5type0act = "";
	private String b5batx = "";
	private char b5dzst;
	private char b5d0st;
	private char b5d2st;
	private char b5d3st;
	
	private BigDecimal b5a3va = BigDecimal.ZERO;
	private BigDecimal b5brva = BigDecimal.ZERO;
	private BigDecimal b5bsva = BigDecimal.ZERO;
	private BigDecimal b5btva = BigDecimal.ZERO;
	
	private BigDecimal b5aupc = BigDecimal.ZERO;
	private String b5aqcd = "";
	private String b5arcd = "";
	private String b5ascd = "";
	private String b5eetx = "";
	private String b5pttx = "";
	private String b5kktx = "";
	private String b5kwtx = "";
	private String b5bccd = "";
	private String b5agnb = "";
	private String b5bjcd = "";
	private String b5bkcd = "";
	private char b5latx;
	private char b5lbtx;
	private String b5h1tx = "";
	private char b5hytx;
	private BigDecimal b5a9nb = BigDecimal.ZERO;
	
	private char b5o6tx;
	private String b5pqtx = "";
	private String b5prtx = "";
	private int b5fjnb;
	private int b5fknb;
	private int b5flnb;
	
	private BigDecimal b5a0va = BigDecimal.ZERO;
	private BigDecimal b5b7va = BigDecimal.ZERO;
	private BigDecimal b5azva = BigDecimal.ZERO;
	
	private char b5czst;
	private char b5c0st;
	private char b5c1st;
	private char b5c2st;
	private char b5iytx;
	private char b5iztx;
	private char b5i0tx;
	private char b5i1tx;
	private char b5i2tx;
	private char b5i3tx;
	private char b5i4tx;
	private char b5i5tx;
	private char b5usin13;
	private char b5usin14;
	private char b5usin15;
	private char b5usin16;
	private char b5usin17;
	private char b5usin18;
	private char b5usin19;
	private char b5usin20;
	private char b5usin21;
	private char b5usin22;
	private char b5usin23;
	private char b5usin24;
	private char b5usin25;
	private char b5usin26;
	private char b5usin27;
	private char b5usin28;
	private char b5usin29;
	private char b5usin30;
	private String b5akcd = "";
	private String b5alcd = "";
	private String b5amcd = "";
	private String b5ancd = "";
	private String b5uscd5 = "";
	private String b5pltx = "";
	private String b5pmtx = "";
	private String b5pntx = "";
	private String b5potx = "";
	private String b5pptx = "";
	private BigDecimal b5aepc = BigDecimal.ZERO;
	private BigDecimal b5afpc = BigDecimal.ZERO;
	private BigDecimal b5usft3 = BigDecimal.ZERO;
	private BigDecimal b5usft4 = BigDecimal.ZERO;
	private BigDecimal b5usft5 = BigDecimal.ZERO;
	private BigDecimal b5usft6 = BigDecimal.ZERO;
	private BigDecimal b5usft7 = BigDecimal.ZERO;
	private BigDecimal b5usft8 = BigDecimal.ZERO;
	private BigDecimal b5usft9 = BigDecimal.ZERO;
	private BigDecimal b5usft10 = BigDecimal.ZERO;
	private BigDecimal b5usft11 = BigDecimal.ZERO;
	private BigDecimal b5usft12 = BigDecimal.ZERO;
	private BigDecimal b5bypc = BigDecimal.ZERO;
	private BigDecimal b5bzpc = BigDecimal.ZERO;
	private BigDecimal b5b0pc = BigDecimal.ZERO;
	private BigDecimal b5b1pc = BigDecimal.ZERO;
	private BigDecimal b5b2pc = BigDecimal.ZERO;
	private BigDecimal b5b3pc = BigDecimal.ZERO;
	private BigDecimal b5b4pc = BigDecimal.ZERO;
	private BigDecimal b5b5pc = BigDecimal.ZERO;
	
	
	private BigDecimal b5agva = BigDecimal.ZERO;
	private BigDecimal b5ahva = BigDecimal.ZERO;
	private BigDecimal b5usva3 = BigDecimal.ZERO;
	private BigDecimal b5usva4 = BigDecimal.ZERO;
	private BigDecimal b5usva5 = BigDecimal.ZERO;
	
	private int b5bknb;
	private int b5blnb;
	private int b5usnb3;
	private int b5usnb4;
	private int b5usnb5;
	private int b5a5dt;
	private int b5a6dt;
	private int b5a7dt;
	private int b5a8dt;
	private int b5a9dt;
	private String b5dcnb = "";
	private String b5ddnb = "";
	
	
	private BigDecimal b5cxva = BigDecimal.ZERO;
	private BigDecimal b5cyva = BigDecimal.ZERO;
	private BigDecimal b5czva = BigDecimal.ZERO;
	private BigDecimal b5c0va = BigDecimal.ZERO;
	private BigDecimal b5c1va = BigDecimal.ZERO;
	private BigDecimal b5c2va = BigDecimal.ZERO;
	private BigDecimal b5c3va = BigDecimal.ZERO;
	private BigDecimal b5alvxval = BigDecimal.ZERO;
	private BigDecimal b5alvyval = BigDecimal.ZERO;
	private BigDecimal b5alvzval = BigDecimal.ZERO;
	private BigDecimal b5alv0val = BigDecimal.ZERO;
	private BigDecimal b5alv1val = BigDecimal.ZERO;
	private BigDecimal b5alv2val = BigDecimal.ZERO;
	private BigDecimal b5alv3val = BigDecimal.ZERO;
	private BigDecimal b5alv4val = BigDecimal.ZERO;
	private BigDecimal b5alwpval = BigDecimal.ZERO;
	
	private String b5egcd = "";
	private String b5ehcd = "";
	private String b5eicd = "";
	private String b5ejcd = "";
	private String b5ekcd = "";
	private String b5al1scde = "";
	private String b5al1tcde = "";
	private String b5al1ucde = "";
	private String b5al1vcde = "";
	private String b5al1wcde = "";
	private String b5al15cde = "";
	private int b5ibnb;
	private int b5icnb;
	private int b5idnb;
	private int b5ienb;
	private int b5ifnb;
	private int b5ignb;
	private int b5ihnb;
	private int b5aliqnbr;
	private int b5alirnbr;
	private int b5alisnbr;
	private int b5alitnbr;
	private int b5aliunbr;
	private int b5alivnbr;
	private int b5bedt;
	private int b5bfdt;
	private int b5alsedte;
	private char b5al4htxt;
	private char b5al4itxt;
	private char b5al4jtxt;
	private char b5al4ktxt;
	private char b5al4ltxt;
	private char b5al4mtxt;
	private char b5al4ntxt;
	private char b5al4otxt;
	private char b5al4ptxt;
	private char b5al4qtxt;
	private String b5al36txt = "";
	private String b5al3vtxt = "";
	private String b5al3wtxt = "";
	private String b5al3xtxt = "";
	private String b5al3ytxt = "";
	private String b5al37txt = "";
	private String b5dcvn = "";
	private String b5aavn = "";
	private int b5akdt;
	private int b5abtm;

	private char b5usin42;
	
	private char b5usin43;
	private char b5usin44;
	private char b5usin45;
	private char b5usin46;
	private char b5usin47;
	private char b5usin48;
	private char b5usin49;
	private char b5usin50;
	private char b5usin51;
	private char b5usin52;
	private char b5usin53;
	private char b5usin54;
	private char b5usin55;
	private char b5usin56;
	private char b5usin57;
	private char b5usin58;
	private char b5usin59;
	private char b5usin60;
	private char b5usin61;
	private String b5al25cde = "";
	private String b5al26cde = "";
	private String b5al27cde = "";
	private String b5al28cde = "";
	private String b5al29cde = "";
	private String b5al30cde = "";
	private String b5al31cde = "";
	private String b5al32cde = "";
	private String b5al33cde = "";
	private String b5al34cde = "";
	private String b5al35cde = "";
	private String b5al36cde = "";
	private String b5al37cde = "";
	private String b5al38cde = "";
	private String b5al39cde = "";
	private String b5al40cde = "";
	private String b5al41cde = "";
	private String b5al42cde = "";
	private String b5al43cde = "";
	private String b5al44cde = "";
	private BigDecimal b5aldjpct = BigDecimal.ZERO;
	private BigDecimal b5aldkpct = BigDecimal.ZERO;
	private BigDecimal b5aldlpct = BigDecimal.ZERO;
	private BigDecimal b5aldmpct = BigDecimal.ZERO;
	private BigDecimal b5aldnpct = BigDecimal.ZERO;
	private BigDecimal b5aldopct = BigDecimal.ZERO;
	private BigDecimal b5aldppct = BigDecimal.ZERO;
	private BigDecimal b5aldqpct = BigDecimal.ZERO;
	private BigDecimal b5aldrpct = BigDecimal.ZERO;
	private BigDecimal b5aldspct = BigDecimal.ZERO;
	private BigDecimal b5aldtpct = BigDecimal.ZERO;
	private BigDecimal b5aldupct = BigDecimal.ZERO;
	private BigDecimal b5aldvpct = BigDecimal.ZERO;
	private BigDecimal b5aldwpct = BigDecimal.ZERO;
	private BigDecimal b5aldxpct = BigDecimal.ZERO;
	private BigDecimal b5aldypct = BigDecimal.ZERO;
	private BigDecimal b5aldzpct = BigDecimal.ZERO;
	private BigDecimal b5alaapct = BigDecimal.ZERO;
	private BigDecimal b5alabpct = BigDecimal.ZERO;
	private BigDecimal b5alacpct = BigDecimal.ZERO;
	private int b5aljknbr;
	private int b5aljlnbr;
	private int b5aljmnbr;
	private int b5aljnnbr;
	private int b5aljonbr;
	private int b5aljpnbr;
	private int b5aljqnbr;
	private int b5aljrnbr;
	private int b5aljsnbr;
	private int b5aljtnbr;
	private int b5aljunbr;
	private int b5aljvnbr;
	private int b5aljwnbr;
	private int b5aljxnbr;
	private int b5aljynbr;
	private int b5aljznbr;
	private int b5alaanbr;
	private int b5alabnbr;
	private int b5alacnbr;
	private int b5aladnbr;
	private BigDecimal b5alwqval = BigDecimal.ZERO;
	private BigDecimal b5alwrval = BigDecimal.ZERO;
	private BigDecimal b5alwsval = BigDecimal.ZERO;
	private BigDecimal b5alwtval = BigDecimal.ZERO;
	private BigDecimal b5alwuval = BigDecimal.ZERO;
	private BigDecimal b5alwvval = BigDecimal.ZERO;
	private BigDecimal b5alwwval = BigDecimal.ZERO;
	private BigDecimal b5alwxval = BigDecimal.ZERO;
	private BigDecimal b5alwyval = BigDecimal.ZERO;
	private BigDecimal b5alwzval = BigDecimal.ZERO;
	private BigDecimal b5alaaval = BigDecimal.ZERO;
	private BigDecimal b5alabval = BigDecimal.ZERO;
	private BigDecimal b5alacval = BigDecimal.ZERO;
	private BigDecimal b5aladval = BigDecimal.ZERO;
	private BigDecimal b5alaeval = BigDecimal.ZERO;
	private BigDecimal b5alafval = BigDecimal.ZERO;
	private BigDecimal b5alagval = BigDecimal.ZERO;
	private BigDecimal b5alahval = BigDecimal.ZERO;
	private BigDecimal b5alaival = BigDecimal.ZERO;
	private BigDecimal b5alajval = BigDecimal.ZERO;
	private int b5alsjdte;
	private int b5alskdte;
	private int b5alsldte;
	private int b5alsmdte;
	private int b5alsndte;
	private int b5alsodte;
	private int b5alspdte;
	private int b5alsqdte;
	private int b5alsrdte;
	private int b5alssdte;
	private int b5alstdte;
	private int b5alsudte;
	private int b5alsvdte;
	private int b5alswdte;
	private int b5alsxdte;
	private int b5alsydte;
	private int b5alszdte;
	private int b5alaadte;
	private int b5alabdte;
	private int b5alacdte;
	private String b5al6txt = "";
	private String b5al7txt = "";
	private String b5al8txt = "";
	private String b5al9txt = "";
	private String b5al10txt = "";
	private String b5al11txt = "";
	private String b5al12txt = "";
	private String b5al13txt = "";
	private String b5al14txt = "";
	private String b5al15txt = "";
	private String b5al16txt = "";
	private String b5al17txt = "";
	private String b5al18txt = "";
	private String b5al19txt = "";
	private String b5al20txt = "";
	private String b5al21txt = "";
	private String b5al22txt = "";
	private String b5al23txt = "";
	private String b5al24txt = "";
	private String b5al25txt = "";
	private String b5al26txt = "";
	private String b5al27txt = "";
	private String b5al28txt = "";
	private String b5al29txt = "";
	private String b5al30txt = "";
	private String b5al31txt = "";
	private String b5al32txt = "";
	private String b5al33txt = "";
	private String b5al34txt = "";
	private String b5al35txt = "";
	private String b5al38txt = "";
	private String b5al39txt = "";
	private String b5al40txt = "";
	private String b5al41txt = "";
	private String b5al42txt = "";
	private String b5al43txt = "";
	private String b5al44txt = "";
	private String b5al45txt = "";
	private String b5al46txt = "";
	private String b5al47txt = "";

	public DBAccessStatus getDBAccessStatus() {
		return this.dBAccessStatus;
	}

	public void setDBAccessStatus(DBAccessStatus dBAccessStatus) {
		this.dBAccessStatus = dBAccessStatus;
	}

	public Bashlgb500ID getId() {
		return id;
	}
	public void setId(Bashlgb500ID id) {
		this.id = id;
	}

	public String getLoguser() {
		return this.loguser;
	}

	public void setLoguser(String loguser) {
		this.loguser = loguser;
	}

	public ABODate getLogdate() {
		return this.logdate.clone();
	}

	public void setLogdate(ABODate logdate) {
		this.logdate.assign(logdate);
	}

	public ABOTime getLogtime() {
		return this.logtime.clone();
	}

	public void setLogtime(ABOTime logtime) {
		this.logtime.assign(logtime);
	}

	public int getLogseqnum() {
		return this.id.getLogSeqNum();
	}

	public void setLogseqnum(int logseqnum) {
		this.id.setLogSeqNum(logseqnum);
	}

	public String getLocation() {
		return this.id.getLocation();
	}

	public void setLocation(String location) {
		this.id.setLocation(location);
	}

	public String getMasterco() {
		return this.id.getMasterco();
	}

	public void setMasterco(String masterco) {
		this.id.setMasterco(masterco);
	}

	public String getSymbol() {
		return this.id.getSymbol();
	}

	public void setSymbol(String symbol) {
		this.id.setSymbol(symbol);
	}

	public String getPolicyno() {
		return this.id.getPolicyno();
	}

	public void setPolicyno(String policyno) {
		this.id.setPolicyno(policyno);
		
	}

	public String getModule() {
		return this.id.getModule();
	}

	public void setModule(String module) {
		this.id.setModule(module);
	}

	public String getInsline() {
		return this.id.getInsline();
	}

	public void setInsline(String insline) {
		this.id.setInsline(insline);
	}

	public int getRiskloc() {
		return this.id.getRiskloc();
	}

	public void setRiskloc(int riskloc) {
		this.id.setRiskloc(riskloc);
	}

	public int getRisksubloc() {
		return this.id.getRisksubloc();
	}

	public void setRisksubloc(int risksubloc) {
		this.id.setRisksubloc(risksubloc);
	}

	public String getProduct() {
		return this.id.getProduct();
	}

	public void setProduct(String product) {
		this.id.setProduct(product);
	}

	public int getUnitno() {
		return this.id.getUnitno();
	}

	public void setUnitno(int unitno) {
		this.id.setUnitno(unitno);
	}

	public char getRecstatus() {
		return this.id.getRecstatus();
	}

	public void setRecstatus(char recstatus) {
		this.id.setRecstatus(recstatus);
	}

	public char getB5c7st() {
		return this.b5c7st;
	}

	public void setB5c7st(char b5c7st) {
		this.b5c7st = b5c7st;
	}

	public int getB5andt() {
		return this.b5andt;
	}

	public void setB5andt(int b5andt) {
		this.b5andt = b5andt;
	}

	public short getB5afnb() {
		return this.b5afnb;
	}

	public void setB5afnb(short b5afnb) {
		this.b5afnb = b5afnb;
	}

	public int getB5aldt() {
		return this.b5aldt;
	}

	public void setB5aldt(int b5aldt) {
		this.b5aldt = b5aldt;
	}

	public int getB5afdt() {
		return this.b5afdt;
	}

	public void setB5afdt(int b5afdt) {
		this.b5afdt = b5afdt;
	}

	public String getB5type0act() {
		return this.b5type0act;
	}

	public void setB5type0act(String b5type0act) {
		this.b5type0act = b5type0act;
	}

	public String getB5batx() {
		return this.b5batx;
	}

	public void setB5batx(String b5batx) {
		this.b5batx = b5batx;
	}

	public char getB5dzst() {
		return this.b5dzst;
	}

	public void setB5dzst(char b5dzst) {
		this.b5dzst = b5dzst;
	}

	public char getB5d0st() {
		return this.b5d0st;
	}

	public void setB5d0st(char b5d0st) {
		this.b5d0st = b5d0st;
	}

	public char getB5d2st() {
		return this.b5d2st;
	}

	public void setB5d2st(char b5d2st) {
		this.b5d2st = b5d2st;
	}

	public char getB5d3st() {
		return this.b5d3st;
	}

	public void setB5d3st(char b5d3st) {
		this.b5d3st = b5d3st;
	}

	public BigDecimal getB5a3va() {
		return this.b5a3va;
	}

	public void setB5a3va(BigDecimal b5a3va) {
		this.b5a3va = b5a3va;
	}

	public BigDecimal getB5brva() {
		return this.b5brva;
	}

	public void setB5brva(BigDecimal b5brva) {
		this.b5brva = b5brva;
	}

	public BigDecimal getB5bsva() {
		return this.b5bsva;
	}

	public void setB5bsva(BigDecimal b5bsva) {
		this.b5bsva = b5bsva;
	}

	public BigDecimal getB5btva() {
		return this.b5btva;
	}

	public void setB5btva(BigDecimal b5btva) {
		this.b5btva = b5btva;
	}

	public BigDecimal getB5aupc() {
		return this.b5aupc;
	}

	public void setB5aupc(BigDecimal b5aupc) {
		this.b5aupc = b5aupc;
	}

	public String getB5aqcd() {
		return this.b5aqcd;
	}

	public void setB5aqcd(String b5aqcd) {
		this.b5aqcd = b5aqcd;
	}

	public String getB5arcd() {
		return this.b5arcd;
	}

	public void setB5arcd(String b5arcd) {
		this.b5arcd = b5arcd;
	}

	public String getB5ascd() {
		return this.b5ascd;
	}

	public void setB5ascd(String b5ascd) {
		this.b5ascd = b5ascd;
	}

	public String getB5eetx() {
		return this.b5eetx;
	}

	public void setB5eetx(String b5eetx) {
		this.b5eetx = b5eetx;
	}

	public String getB5pttx() {
		return this.b5pttx;
	}

	public void setB5pttx(String b5pttx) {
		this.b5pttx = b5pttx;
	}

	public String getB5kktx() {
		return this.b5kktx;
	}

	public void setB5kktx(String b5kktx) {
		this.b5kktx = b5kktx;
	}

	public String getB5kwtx() {
		return this.b5kwtx;
	}

	public void setB5kwtx(String b5kwtx) {
		this.b5kwtx = b5kwtx;
	}

	public String getB5bccd() {
		return this.b5bccd;
	}

	public void setB5bccd(String b5bccd) {
		this.b5bccd = b5bccd;
	}

	public String getB5agnb() {
		return this.b5agnb;
	}

	public void setB5agnb(String b5agnb) {
		this.b5agnb = b5agnb;
	}

	public String getB5bjcd() {
		return this.b5bjcd;
	}

	public void setB5bjcd(String b5bjcd) {
		this.b5bjcd = b5bjcd;
	}

	public String getB5bkcd() {
		return this.b5bkcd;
	}

	public void setB5bkcd(String b5bkcd) {
		this.b5bkcd = b5bkcd;
	}

	public char getB5latx() {
		return this.b5latx;
	}

	public void setB5latx(char b5latx) {
		this.b5latx = b5latx;
	}

	public char getB5lbtx() {
		return this.b5lbtx;
	}

	public void setB5lbtx(char b5lbtx) {
		this.b5lbtx = b5lbtx;
	}

	public String getB5h1tx() {
		return this.b5h1tx;
	}

	public void setB5h1tx(String b5h1tx) {
		this.b5h1tx = b5h1tx;
	}

	public char getB5hytx() {
		return this.b5hytx;
	}

	public void setB5hytx(char b5hytx) {
		this.b5hytx = b5hytx;
	}

	public BigDecimal getB5a9nb() {
		return this.b5a9nb;
	}

	public void setB5a9nb(BigDecimal b5a9nb) {
		this.b5a9nb = b5a9nb;
	}

	public char getB5o6tx() {
		return this.b5o6tx;
	}

	public void setB5o6tx(char b5o6tx) {
		this.b5o6tx = b5o6tx;
	}

	public String getB5pqtx() {
		return this.b5pqtx;
	}

	public void setB5pqtx(String b5pqtx) {
		this.b5pqtx = b5pqtx;
	}

	public String getB5prtx() {
		return this.b5prtx;
	}

	public void setB5prtx(String b5prtx) {
		this.b5prtx = b5prtx;
	}

	public int getB5fjnb() {
		return this.b5fjnb;
	}

	public void setB5fjnb(int b5fjnb) {
		this.b5fjnb = b5fjnb;
	}

	public int getB5fknb() {
		return this.b5fknb;
	}

	public void setB5fknb(int b5fknb) {
		this.b5fknb = b5fknb;
	}

	public int getB5flnb() {
		return this.b5flnb;
	}

	public void setB5flnb(int b5flnb) {
		this.b5flnb = b5flnb;
	}

	public BigDecimal getB5a0va() {
		return this.b5a0va;
	}

	public void setB5a0va(BigDecimal b5a0va) {
		this.b5a0va = b5a0va;
	}

	public BigDecimal getB5b7va() {
		return this.b5b7va;
	}

	public void setB5b7va(BigDecimal b5b7va) {
		this.b5b7va = b5b7va;
	}

	public BigDecimal getB5azva() {
		return this.b5azva;
	}

	public void setB5azva(BigDecimal b5azva) {
		this.b5azva = b5azva;
	}

	public char getB5czst() {
		return this.b5czst;
	}

	public void setB5czst(char b5czst) {
		this.b5czst = b5czst;
	}

	public char getB5c0st() {
		return this.b5c0st;
	}

	public void setB5c0st(char b5c0st) {
		this.b5c0st = b5c0st;
	}

	public char getB5c1st() {
		return this.b5c1st;
	}

	public void setB5c1st(char b5c1st) {
		this.b5c1st = b5c1st;
	}

	public char getB5c2st() {
		return this.b5c2st;
	}

	public void setB5c2st(char b5c2st) {
		this.b5c2st = b5c2st;
	}

	public char getB5iytx() {
		return this.b5iytx;
	}

	public void setB5iytx(char b5iytx) {
		this.b5iytx = b5iytx;
	}

	public char getB5iztx() {
		return this.b5iztx;
	}

	public void setB5iztx(char b5iztx) {
		this.b5iztx = b5iztx;
	}

	public char getB5i0tx() {
		return this.b5i0tx;
	}

	public void setB5i0tx(char b5i0tx) {
		this.b5i0tx = b5i0tx;
	}

	public char getB5i1tx() {
		return this.b5i1tx;
	}

	public void setB5i1tx(char b5i1tx) {
		this.b5i1tx = b5i1tx;
	}

	public char getB5i2tx() {
		return this.b5i2tx;
	}

	public void setB5i2tx(char b5i2tx) {
		this.b5i2tx = b5i2tx;
	}

	public char getB5i3tx() {
		return this.b5i3tx;
	}

	public void setB5i3tx(char b5i3tx) {
		this.b5i3tx = b5i3tx;
	}

	public char getB5i4tx() {
		return this.b5i4tx;
	}

	public void setB5i4tx(char b5i4tx) {
		this.b5i4tx = b5i4tx;
	}

	public char getB5i5tx() {
		return this.b5i5tx;
	}

	public void setB5i5tx(char b5i5tx) {
		this.b5i5tx = b5i5tx;
	}

	public char getB5usin13() {
		return this.b5usin13;
	}

	public void setB5usin13(char b5usin13) {
		this.b5usin13 = b5usin13;
	}

	public char getB5usin14() {
		return this.b5usin14;
	}

	public void setB5usin14(char b5usin14) {
		this.b5usin14 = b5usin14;
	}

	public char getB5usin15() {
		return this.b5usin15;
	}

	public void setB5usin15(char b5usin15) {
		this.b5usin15 = b5usin15;
	}

	public char getB5usin16() {
		return this.b5usin16;
	}

	public void setB5usin16(char b5usin16) {
		this.b5usin16 = b5usin16;
	}

	public char getB5usin17() {
		return this.b5usin17;
	}

	public void setB5usin17(char b5usin17) {
		this.b5usin17 = b5usin17;
	}

	public char getB5usin18() {
		return this.b5usin18;
	}

	public void setB5usin18(char b5usin18) {
		this.b5usin18 = b5usin18;
	}

	public char getB5usin19() {
		return this.b5usin19;
	}

	public void setB5usin19(char b5usin19) {
		this.b5usin19 = b5usin19;
	}

	public char getB5usin20() {
		return this.b5usin20;
	}

	public void setB5usin20(char b5usin20) {
		this.b5usin20 = b5usin20;
	}

	public char getB5usin21() {
		return this.b5usin21;
	}

	public void setB5usin21(char b5usin21) {
		this.b5usin21 = b5usin21;
	}

	public char getB5usin22() {
		return this.b5usin22;
	}

	public void setB5usin22(char b5usin22) {
		this.b5usin22 = b5usin22;
	}

	public char getB5usin23() {
		return this.b5usin23;
	}

	public void setB5usin23(char b5usin23) {
		this.b5usin23 = b5usin23;
	}

	public char getB5usin24() {
		return this.b5usin24;
	}

	public void setB5usin24(char b5usin24) {
		this.b5usin24 = b5usin24;
	}

	public char getB5usin25() {
		return this.b5usin25;
	}

	public void setB5usin25(char b5usin25) {
		this.b5usin25 = b5usin25;
	}

	public char getB5usin26() {
		return this.b5usin26;
	}

	public void setB5usin26(char b5usin26) {
		this.b5usin26 = b5usin26;
	}

	public char getB5usin27() {
		return this.b5usin27;
	}

	public void setB5usin27(char b5usin27) {
		this.b5usin27 = b5usin27;
	}

	public char getB5usin28() {
		return this.b5usin28;
	}

	public void setB5usin28(char b5usin28) {
		this.b5usin28 = b5usin28;
	}

	public char getB5usin29() {
		return this.b5usin29;
	}

	public void setB5usin29(char b5usin29) {
		this.b5usin29 = b5usin29;
	}

	public char getB5usin30() {
		return this.b5usin30;
	}

	public void setB5usin30(char b5usin30) {
		this.b5usin30 = b5usin30;
	}

	public String getB5akcd() {
		return this.b5akcd;
	}

	public void setB5akcd(String b5akcd) {
		this.b5akcd = b5akcd;
	}

	public String getB5alcd() {
		return this.b5alcd;
	}

	public void setB5alcd(String b5alcd) {
		this.b5alcd = b5alcd;
	}

	public String getB5amcd() {
		return this.b5amcd;
	}

	public void setB5amcd(String b5amcd) {
		this.b5amcd = b5amcd;
	}

	public String getB5ancd() {
		return this.b5ancd;
	}

	public void setB5ancd(String b5ancd) {
		this.b5ancd = b5ancd;
	}

	public String getB5uscd5() {
		return this.b5uscd5;
	}

	public void setB5uscd5(String b5uscd5) {
		this.b5uscd5 = b5uscd5;
	}

	public String getB5pltx() {
		return this.b5pltx;
	}

	public void setB5pltx(String b5pltx) {
		this.b5pltx = b5pltx;
	}

	public String getB5pmtx() {
		return this.b5pmtx;
	}

	public void setB5pmtx(String b5pmtx) {
		this.b5pmtx = b5pmtx;
	}

	public String getB5pntx() {
		return this.b5pntx;
	}

	public void setB5pntx(String b5pntx) {
		this.b5pntx = b5pntx;
	}

	public String getB5potx() {
		return this.b5potx;
	}

	public void setB5potx(String b5potx) {
		this.b5potx = b5potx;
	}

	public String getB5pptx() {
		return this.b5pptx;
	}

	public void setB5pptx(String b5pptx) {
		this.b5pptx = b5pptx;
	}

	public BigDecimal getB5aepc() {
		return this.b5aepc;
	}

	public void setB5aepc(BigDecimal b5aepc) {
		this.b5aepc = b5aepc;
	}

	public BigDecimal getB5afpc() {
		return this.b5afpc;
	}

	public void setB5afpc(BigDecimal b5afpc) {
		this.b5afpc = b5afpc;
	}

	public BigDecimal getB5usft3() {
		return this.b5usft3;
	}

	public void setB5usft3(BigDecimal b5usft3) {
		this.b5usft3 = b5usft3;
	}

	public BigDecimal getB5usft4() {
		return this.b5usft4;
	}

	public void setB5usft4(BigDecimal b5usft4) {
		this.b5usft4 = b5usft4;
	}

	public BigDecimal getB5usft5() {
		return this.b5usft5;
	}

	public void setB5usft5(BigDecimal b5usft5) {
		this.b5usft5 = b5usft5;
	}

	public BigDecimal getB5usft6() {
		return this.b5usft6;
	}

	public void setB5usft6(BigDecimal b5usft6) {
		this.b5usft6 = b5usft6;
	}

	public BigDecimal getB5usft7() {
		return this.b5usft7;
	}

	public void setB5usft7(BigDecimal b5usft7) {
		this.b5usft7 = b5usft7;
	}

	public BigDecimal getB5usft8() {
		return this.b5usft8;
	}

	public void setB5usft8(BigDecimal b5usft8) {
		this.b5usft8 = b5usft8;
	}

	public BigDecimal getB5usft9() {
		return this.b5usft9;
	}

	public void setB5usft9(BigDecimal b5usft9) {
		this.b5usft9 = b5usft9;
	}

	public BigDecimal getB5usft10() {
		return this.b5usft10;
	}

	public void setB5usft10(BigDecimal b5usft10) {
		this.b5usft10 = b5usft10;
	}

	public BigDecimal getB5usft11() {
		return this.b5usft11;
	}

	public void setB5usft11(BigDecimal b5usft11) {
		this.b5usft11 = b5usft11;
	}

	public BigDecimal getB5usft12() {
		return this.b5usft12;
	}

	public void setB5usft12(BigDecimal b5usft12) {
		this.b5usft12 = b5usft12;
	}

	public BigDecimal getB5bypc() {
		return this.b5bypc;
	}

	public void setB5bypc(BigDecimal b5bypc) {
		this.b5bypc = b5bypc;
	}

	public BigDecimal getB5bzpc() {
		return this.b5bzpc;
	}

	public void setB5bzpc(BigDecimal b5bzpc) {
		this.b5bzpc = b5bzpc;
	}

	public BigDecimal getB5b0pc() {
		return this.b5b0pc;
	}

	public void setB5b0pc(BigDecimal b5b0pc) {
		this.b5b0pc = b5b0pc;
	}

	public BigDecimal getB5b1pc() {
		return this.b5b1pc;
	}

	public void setB5b1pc(BigDecimal b5b1pc) {
		this.b5b1pc = b5b1pc;
	}

	public BigDecimal getB5b2pc() {
		return this.b5b2pc;
	}

	public void setB5b2pc(BigDecimal b5b2pc) {
		this.b5b2pc = b5b2pc;
	}

	public BigDecimal getB5b3pc() {
		return this.b5b3pc;
	}

	public void setB5b3pc(BigDecimal b5b3pc) {
		this.b5b3pc = b5b3pc;
	}

	public BigDecimal getB5b4pc() {
		return this.b5b4pc;
	}

	public void setB5b4pc(BigDecimal b5b4pc) {
		this.b5b4pc = b5b4pc;
	}

	public BigDecimal getB5b5pc() {
		return this.b5b5pc;
	}

	public void setB5b5pc(BigDecimal b5b5pc) {
		this.b5b5pc = b5b5pc;
	}

	public BigDecimal getB5agva() {
		return this.b5agva;
	}

	public void setB5agva(BigDecimal b5agva) {
		this.b5agva = b5agva;
	}

	public BigDecimal getB5ahva() {
		return this.b5ahva;
	}

	public void setB5ahva(BigDecimal b5ahva) {
		this.b5ahva = b5ahva;
	}

	public BigDecimal getB5usva3() {
		return this.b5usva3;
	}

	public void setB5usva3(BigDecimal b5usva3) {
		this.b5usva3 = b5usva3;
	}

	public BigDecimal getB5usva4() {
		return this.b5usva4;
	}

	public void setB5usva4(BigDecimal b5usva4) {
		this.b5usva4 = b5usva4;
	}

	public BigDecimal getB5usva5() {
		return this.b5usva5;
	}

	public void setB5usva5(BigDecimal b5usva5) {
		this.b5usva5 = b5usva5;
	}

	public int getB5bknb() {
		return this.b5bknb;
	}

	public void setB5bknb(int b5bknb) {
		this.b5bknb = b5bknb;
	}

	public int getB5blnb() {
		return this.b5blnb;
	}

	public void setB5blnb(int b5blnb) {
		this.b5blnb = b5blnb;
	}

	public int getB5usnb3() {
		return this.b5usnb3;
	}

	public void setB5usnb3(int b5usnb3) {
		this.b5usnb3 = b5usnb3;
	}

	public int getB5usnb4() {
		return this.b5usnb4;
	}

	public void setB5usnb4(int b5usnb4) {
		this.b5usnb4 = b5usnb4;
	}

	public int getB5usnb5() {
		return this.b5usnb5;
	}

	public void setB5usnb5(int b5usnb5) {
		this.b5usnb5 = b5usnb5;
	}

	public int getB5a5dt() {
		return this.b5a5dt;
	}

	public void setB5a5dt(int b5a5dt) {
		this.b5a5dt = b5a5dt;
	}

	public int getB5a6dt() {
		return this.b5a6dt;
	}

	public void setB5a6dt(int b5a6dt) {
		this.b5a6dt = b5a6dt;
	}

	public int getB5a7dt() {
		return this.b5a7dt;
	}

	public void setB5a7dt(int b5a7dt) {
		this.b5a7dt = b5a7dt;
	}

	public int getB5a8dt() {
		return this.b5a8dt;
	}

	public void setB5a8dt(int b5a8dt) {
		this.b5a8dt = b5a8dt;
	}

	public int getB5a9dt() {
		return this.b5a9dt;
	}

	public void setB5a9dt(int b5a9dt) {
		this.b5a9dt = b5a9dt;
	}

	public String getB5dcnb() {
		return this.b5dcnb;
	}

	public void setB5dcnb(String b5dcnb) {
		this.b5dcnb = b5dcnb;
	}

	public String getB5ddnb() {
		return this.b5ddnb;
	}

	public void setB5ddnb(String b5ddnb) {
		this.b5ddnb = b5ddnb;
	}

	public BigDecimal getB5cxva() {
		return this.b5cxva;
	}

	public void setB5cxva(BigDecimal b5cxva) {
		this.b5cxva = b5cxva;
	}

	public BigDecimal getB5cyva() {
		return this.b5cyva;
	}

	public void setB5cyva(BigDecimal b5cyva) {
		this.b5cyva = b5cyva;
	}

	public BigDecimal getB5czva() {
		return this.b5czva;
	}

	public void setB5czva(BigDecimal b5czva) {
		this.b5czva = b5czva;
	}

	public BigDecimal getB5c0va() {
		return this.b5c0va;
	}

	public void setB5c0va(BigDecimal b5c0va) {
		this.b5c0va = b5c0va;
	}

	public BigDecimal getB5c1va() {
		return this.b5c1va;
	}

	public void setB5c1va(BigDecimal b5c1va) {
		this.b5c1va = b5c1va;
	}

	public BigDecimal getB5c2va() {
		return this.b5c2va;
	}

	public void setB5c2va(BigDecimal b5c2va) {
		this.b5c2va = b5c2va;
	}

	public BigDecimal getB5c3va() {
		return this.b5c3va;
	}

	public void setB5c3va(BigDecimal b5c3va) {
		this.b5c3va = b5c3va;
	}

	public BigDecimal getB5alvxval() {
		return this.b5alvxval;
	}

	public void setB5alvxval(BigDecimal b5alvxval) {
		this.b5alvxval = b5alvxval;
	}

	public BigDecimal getB5alvyval() {
		return this.b5alvyval;
	}

	public void setB5alvyval(BigDecimal b5alvyval) {
		this.b5alvyval = b5alvyval;
	}

	public BigDecimal getB5alvzval() {
		return this.b5alvzval;
	}

	public void setB5alvzval(BigDecimal b5alvzval) {
		this.b5alvzval = b5alvzval;
	}

	public BigDecimal getB5alv0val() {
		return this.b5alv0val;
	}

	public void setB5alv0val(BigDecimal b5alv0val) {
		this.b5alv0val = b5alv0val;
	}

	public BigDecimal getB5alv1val() {
		return this.b5alv1val;
	}

	public void setB5alv1val(BigDecimal b5alv1val) {
		this.b5alv1val = b5alv1val;
	}

	public BigDecimal getB5alv2val() {
		return this.b5alv2val;
	}

	public void setB5alv2val(BigDecimal b5alv2val) {
		this.b5alv2val = b5alv2val;
	}

	public BigDecimal getB5alv3val() {
		return this.b5alv3val;
	}

	public void setB5alv3val(BigDecimal b5alv3val) {
		this.b5alv3val = b5alv3val;
	}

	public BigDecimal getB5alv4val() {
		return this.b5alv4val;
	}

	public void setB5alv4val(BigDecimal b5alv4val) {
		this.b5alv4val = b5alv4val;
	}

	public BigDecimal getB5alwpval() {
		return this.b5alwpval;
	}

	public void setB5alwpval(BigDecimal b5alwpval) {
		this.b5alwpval = b5alwpval;
	}

	public String getB5egcd() {
		return this.b5egcd;
	}

	public void setB5egcd(String b5egcd) {
		this.b5egcd = b5egcd;
	}

	public String getB5ehcd() {
		return this.b5ehcd;
	}

	public void setB5ehcd(String b5ehcd) {
		this.b5ehcd = b5ehcd;
	}

	public String getB5eicd() {
		return this.b5eicd;
	}

	public void setB5eicd(String b5eicd) {
		this.b5eicd = b5eicd;
	}

	public String getB5ejcd() {
		return this.b5ejcd;
	}

	public void setB5ejcd(String b5ejcd) {
		this.b5ejcd = b5ejcd;
	}

	public String getB5ekcd() {
		return this.b5ekcd;
	}

	public void setB5ekcd(String b5ekcd) {
		this.b5ekcd = b5ekcd;
	}

	public String getB5al1scde() {
		return this.b5al1scde;
	}

	public void setB5al1scde(String b5al1scde) {
		this.b5al1scde = b5al1scde;
	}

	public String getB5al1tcde() {
		return this.b5al1tcde;
	}

	public void setB5al1tcde(String b5al1tcde) {
		this.b5al1tcde = b5al1tcde;
	}

	public String getB5al1ucde() {
		return this.b5al1ucde;
	}

	public void setB5al1ucde(String b5al1ucde) {
		this.b5al1ucde = b5al1ucde;
	}

	public String getB5al1vcde() {
		return this.b5al1vcde;
	}

	public void setB5al1vcde(String b5al1vcde) {
		this.b5al1vcde = b5al1vcde;
	}

	public String getB5al1wcde() {
		return this.b5al1wcde;
	}

	public void setB5al1wcde(String b5al1wcde) {
		this.b5al1wcde = b5al1wcde;
	}

	public String getB5al15cde() {
		return this.b5al15cde;
	}

	public void setB5al15cde(String b5al15cde) {
		this.b5al15cde = b5al15cde;
	}

	public int getB5ibnb() {
		return this.b5ibnb;
	}

	public void setB5ibnb(int b5ibnb) {
		this.b5ibnb = b5ibnb;
	}

	public int getB5icnb() {
		return this.b5icnb;
	}

	public void setB5icnb(int b5icnb) {
		this.b5icnb = b5icnb;
	}

	public int getB5idnb() {
		return this.b5idnb;
	}

	public void setB5idnb(int b5idnb) {
		this.b5idnb = b5idnb;
	}

	public int getB5ienb() {
		return this.b5ienb;
	}

	public void setB5ienb(int b5ienb) {
		this.b5ienb = b5ienb;
	}

	public int getB5ifnb() {
		return this.b5ifnb;
	}

	public void setB5ifnb(int b5ifnb) {
		this.b5ifnb = b5ifnb;
	}

	public int getB5ignb() {
		return this.b5ignb;
	}

	public void setB5ignb(int b5ignb) {
		this.b5ignb = b5ignb;
	}

	public int getB5ihnb() {
		return this.b5ihnb;
	}

	public void setB5ihnb(int b5ihnb) {
		this.b5ihnb = b5ihnb;
	}

	public int getB5aliqnbr() {
		return this.b5aliqnbr;
	}

	public void setB5aliqnbr(int b5aliqnbr) {
		this.b5aliqnbr = b5aliqnbr;
	}

	public int getB5alirnbr() {
		return this.b5alirnbr;
	}

	public void setB5alirnbr(int b5alirnbr) {
		this.b5alirnbr = b5alirnbr;
	}

	public int getB5alisnbr() {
		return this.b5alisnbr;
	}

	public void setB5alisnbr(int b5alisnbr) {
		this.b5alisnbr = b5alisnbr;
	}

	public int getB5alitnbr() {
		return this.b5alitnbr;
	}

	public void setB5alitnbr(int b5alitnbr) {
		this.b5alitnbr = b5alitnbr;
	}

	public int getB5aliunbr() {
		return this.b5aliunbr;
	}

	public void setB5aliunbr(int b5aliunbr) {
		this.b5aliunbr = b5aliunbr;
	}

	public int getB5alivnbr() {
		return this.b5alivnbr;
	}

	public void setB5alivnbr(int b5alivnbr) {
		this.b5alivnbr = b5alivnbr;
	}

	public int getB5bedt() {
		return this.b5bedt;
	}

	public void setB5bedt(int b5bedt) {
		this.b5bedt = b5bedt;
	}

	public int getB5bfdt() {
		return this.b5bfdt;
	}

	public void setB5bfdt(int b5bfdt) {
		this.b5bfdt = b5bfdt;
	}

	public int getB5alsedte() {
		return this.b5alsedte;
	}

	public void setB5alsedte(int b5alsedte) {
		this.b5alsedte = b5alsedte;
	}

	public char getB5al4htxt() {
		return this.b5al4htxt;
	}

	public void setB5al4htxt(char b5al4htxt) {
		this.b5al4htxt = b5al4htxt;
	}

	public char getB5al4itxt() {
		return this.b5al4itxt;
	}

	public void setB5al4itxt(char b5al4itxt) {
		this.b5al4itxt = b5al4itxt;
	}

	public char getB5al4jtxt() {
		return this.b5al4jtxt;
	}

	public void setB5al4jtxt(char b5al4jtxt) {
		this.b5al4jtxt = b5al4jtxt;
	}

	public char getB5al4ktxt() {
		return this.b5al4ktxt;
	}

	public void setB5al4ktxt(char b5al4ktxt) {
		this.b5al4ktxt = b5al4ktxt;
	}

	public char getB5al4ltxt() {
		return this.b5al4ltxt;
	}

	public void setB5al4ltxt(char b5al4ltxt) {
		this.b5al4ltxt = b5al4ltxt;
	}

	public char getB5al4mtxt() {
		return this.b5al4mtxt;
	}

	public void setB5al4mtxt(char b5al4mtxt) {
		this.b5al4mtxt = b5al4mtxt;
	}

	public char getB5al4ntxt() {
		return this.b5al4ntxt;
	}

	public void setB5al4ntxt(char b5al4ntxt) {
		this.b5al4ntxt = b5al4ntxt;
	}

	public char getB5al4otxt() {
		return this.b5al4otxt;
	}

	public void setB5al4otxt(char b5al4otxt) {
		this.b5al4otxt = b5al4otxt;
	}

	public char getB5al4ptxt() {
		return this.b5al4ptxt;
	}

	public void setB5al4ptxt(char b5al4ptxt) {
		this.b5al4ptxt = b5al4ptxt;
	}

	public char getB5al4qtxt() {
		return this.b5al4qtxt;
	}

	public void setB5al4qtxt(char b5al4qtxt) {
		this.b5al4qtxt = b5al4qtxt;
	}

	public String getB5al36txt() {
		return this.b5al36txt;
	}

	public void setB5al36txt(String b5al36txt) {
		this.b5al36txt = b5al36txt;
	}

	public String getB5al3vtxt() {
		return this.b5al3vtxt;
	}

	public void setB5al3vtxt(String b5al3vtxt) {
		this.b5al3vtxt = b5al3vtxt;
	}

	public String getB5al3wtxt() {
		return this.b5al3wtxt;
	}

	public void setB5al3wtxt(String b5al3wtxt) {
		this.b5al3wtxt = b5al3wtxt;
	}

	public String getB5al3xtxt() {
		return this.b5al3xtxt;
	}

	public void setB5al3xtxt(String b5al3xtxt) {
		this.b5al3xtxt = b5al3xtxt;
	}

	public String getB5al3ytxt() {
		return this.b5al3ytxt;
	}

	public void setB5al3ytxt(String b5al3ytxt) {
		this.b5al3ytxt = b5al3ytxt;
	}

	public String getB5al37txt() {
		return this.b5al37txt;
	}

	public void setB5al37txt(String b5al37txt) {
		this.b5al37txt = b5al37txt;
	}

	public String getB5dcvn() {
		return this.b5dcvn;
	}

	public void setB5dcvn(String b5dcvn) {
		this.b5dcvn = b5dcvn;
	}

	public String getB5aavn() {
		return this.b5aavn;
	}

	public void setB5aavn(String b5aavn) {
		this.b5aavn = b5aavn;
	}

	public int getB5akdt() {
		return this.b5akdt;
	}

	public void setB5akdt(int b5akdt) {
		this.b5akdt = b5akdt;
	}

	public int getB5abtm() {
		return this.b5abtm;
	}

	public void setB5abtm(int b5abtm) {
		this.b5abtm = b5abtm;
	}


	public char getB5usin42() {
		return this.b5usin42;
	}
	public void setB5usin42(char b5usin42) {
		this.b5usin42 = b5usin42;
	}

	public char getB5usin43() {
		return this.b5usin43;
	}

	public void setB5usin43(char b5usin43) {
		this.b5usin43 = b5usin43;
	}

	public char getB5usin44() {
		return this.b5usin44;
	}

	public void setB5usin44(char b5usin44) {
		this.b5usin44 = b5usin44;
	}

	public char getB5usin45() {
		return this.b5usin45;
	}

	public void setB5usin45(char b5usin45) {
		this.b5usin45 = b5usin45;
	}

	public char getB5usin46() {
		return this.b5usin46;
	}

	public void setB5usin46(char b5usin46) {
		this.b5usin46 = b5usin46;
	}

	public char getB5usin47() {
		return this.b5usin47;
	}

	public void setB5usin47(char b5usin47) {
		this.b5usin47 = b5usin47;
	}

	public char getB5usin48() {
		return this.b5usin48;
	}

	public void setB5usin48(char b5usin48) {
		this.b5usin48 = b5usin48;
	}

	public char getB5usin49() {
		return this.b5usin49;
	}

	public void setB5usin49(char b5usin49) {
		this.b5usin49 = b5usin49;
	}

	public char getB5usin50() {
		return this.b5usin50;
	}

	public void setB5usin50(char b5usin50) {
		this.b5usin50 = b5usin50;
	}

	public char getB5usin51() {
		return this.b5usin51;
	}

	public void setB5usin51(char b5usin51) {
		this.b5usin51 = b5usin51;
	}

	public char getB5usin52() {
		return this.b5usin52;
	}

	public void setB5usin52(char b5usin52) {
		this.b5usin52 = b5usin52;
	}

	public char getB5usin53() {
		return this.b5usin53;
	}

	public void setB5usin53(char b5usin53) {
		this.b5usin53 = b5usin53;
	}

	public char getB5usin54() {
		return this.b5usin54;
	}

	public void setB5usin54(char b5usin54) {
		this.b5usin54 = b5usin54;
	}

	public char getB5usin55() {
		return this.b5usin55;
	}

	public void setB5usin55(char b5usin55) {
		this.b5usin55 = b5usin55;
	}

	public char getB5usin56() {
		return this.b5usin56;
	}

	public void setB5usin56(char b5usin56) {
		this.b5usin56 = b5usin56;
	}

	public char getB5usin57() {
		return this.b5usin57;
	}

	public void setB5usin57(char b5usin57) {
		this.b5usin57 = b5usin57;
	}

	public char getB5usin58() {
		return this.b5usin58;
	}

	public void setB5usin58(char b5usin58) {
		this.b5usin58 = b5usin58;
	}

	public char getB5usin59() {
		return this.b5usin59;
	}

	public void setB5usin59(char b5usin59) {
		this.b5usin59 = b5usin59;
	}

	public char getB5usin60() {
		return this.b5usin60;
	}

	public void setB5usin60(char b5usin60) {
		this.b5usin60 = b5usin60;
	}

	public char getB5usin61() {
		return this.b5usin61;
	}

	public void setB5usin61(char b5usin61) {
		this.b5usin61 = b5usin61;
	}

	public String getB5al25cde() {
		return this.b5al25cde;
	}

	public void setB5al25cde(String b5al25cde) {
		this.b5al25cde = b5al25cde;
	}

	public String getB5al26cde() {
		return this.b5al26cde;
	}

	public void setB5al26cde(String b5al26cde) {
		this.b5al26cde = b5al26cde;
	}

	public String getB5al27cde() {
		return this.b5al27cde;
	}

	public void setB5al27cde(String b5al27cde) {
		this.b5al27cde = b5al27cde;
	}

	public String getB5al28cde() {
		return this.b5al28cde;
	}

	public void setB5al28cde(String b5al28cde) {
		this.b5al28cde = b5al28cde;
	}

	public String getB5al29cde() {
		return this.b5al29cde;
	}

	public void setB5al29cde(String b5al29cde) {
		this.b5al29cde = b5al29cde;
	}

	public String getB5al30cde() {
		return this.b5al30cde;
	}

	public void setB5al30cde(String b5al30cde) {
		this.b5al30cde = b5al30cde;
	}

	public String getB5al31cde() {
		return this.b5al31cde;
	}

	public void setB5al31cde(String b5al31cde) {
		this.b5al31cde = b5al31cde;
	}

	public String getB5al32cde() {
		return this.b5al32cde;
	}

	public void setB5al32cde(String b5al32cde) {
		this.b5al32cde = b5al32cde;
	}

	public String getB5al33cde() {
		return this.b5al33cde;
	}

	public void setB5al33cde(String b5al33cde) {
		this.b5al33cde = b5al33cde;
	}

	public String getB5al34cde() {
		return this.b5al34cde;
	}

	public void setB5al34cde(String b5al34cde) {
		this.b5al34cde = b5al34cde;
	}

	public String getB5al35cde() {
		return this.b5al35cde;
	}

	public void setB5al35cde(String b5al35cde) {
		this.b5al35cde = b5al35cde;
	}

	public String getB5al36cde() {
		return this.b5al36cde;
	}

	public void setB5al36cde(String b5al36cde) {
		this.b5al36cde = b5al36cde;
	}

	public String getB5al37cde() {
		return this.b5al37cde;
	}

	public void setB5al37cde(String b5al37cde) {
		this.b5al37cde = b5al37cde;
	}

	public String getB5al38cde() {
		return this.b5al38cde;
	}

	public void setB5al38cde(String b5al38cde) {
		this.b5al38cde = b5al38cde;
	}

	public String getB5al39cde() {
		return this.b5al39cde;
	}

	public void setB5al39cde(String b5al39cde) {
		this.b5al39cde = b5al39cde;
	}

	public String getB5al40cde() {
		return this.b5al40cde;
	}

	public void setB5al40cde(String b5al40cde) {
		this.b5al40cde = b5al40cde;
	}

	public String getB5al41cde() {
		return this.b5al41cde;
	}

	public void setB5al41cde(String b5al41cde) {
		this.b5al41cde = b5al41cde;
	}

	public String getB5al42cde() {
		return this.b5al42cde;
	}

	public void setB5al42cde(String b5al42cde) {
		this.b5al42cde = b5al42cde;
	}

	public String getB5al43cde() {
		return this.b5al43cde;
	}

	public void setB5al43cde(String b5al43cde) {
		this.b5al43cde = b5al43cde;
	}

	public String getB5al44cde() {
		return this.b5al44cde;
	}

	public void setB5al44cde(String b5al44cde) {
		this.b5al44cde = b5al44cde;
	}
	
	public BigDecimal getB5aldjpct() {
		return this.b5aldjpct;
	}

	public void setB5aldjpct(BigDecimal b5aldjpct) {
		this.b5aldjpct = b5aldjpct;
	}

	public BigDecimal getB5aldkpct() {
		return this.b5aldkpct;
	}

	public void setB5aldkpct(BigDecimal b5aldkpct) {
		this.b5aldkpct = b5aldkpct;
	}

	public BigDecimal getB5aldlpct() {
		return this.b5aldlpct;
	}

	public void setB5aldlpct(BigDecimal b5aldlpct) {
		this.b5aldlpct = b5aldlpct;
	}

	public BigDecimal getB5aldmpct() {
		return this.b5aldmpct;
	}

	public void setB5aldmpct(BigDecimal b5aldmpct) {
		this.b5aldmpct = b5aldmpct;
	}

	public BigDecimal getB5aldnpct() {
		return this.b5aldnpct;
	}

	public void setB5aldnpct(BigDecimal b5aldnpct) {
		this.b5aldnpct = b5aldnpct;
	}

	public BigDecimal getB5aldopct() {
		return this.b5aldopct;
	}

	public void setB5aldopct(BigDecimal b5aldopct) {
		this.b5aldopct = b5aldopct;
	}

	public BigDecimal getB5aldppct() {
		return this.b5aldppct;
	}

	public void setB5aldppct(BigDecimal b5aldppct) {
		this.b5aldppct = b5aldppct;
	}

	public BigDecimal getB5aldqpct() {
		return this.b5aldqpct;
	}

	public void setB5aldqpct(BigDecimal b5aldqpct) {
		this.b5aldqpct = b5aldqpct;
	}

	public BigDecimal getB5aldrpct() {
		return this.b5aldrpct;
	}

	public void setB5aldrpct(BigDecimal b5aldrpct) {
		this.b5aldrpct = b5aldrpct;
	}

	public BigDecimal getB5aldspct() {
		return this.b5aldspct;
	}

	public void setB5aldspct(BigDecimal b5aldspct) {
		this.b5aldspct = b5aldspct;
	}

	public BigDecimal getB5aldtpct() {
		return this.b5aldtpct;
	}

	public void setB5aldtpct(BigDecimal b5aldtpct) {
		this.b5aldtpct = b5aldtpct;
	}

	public BigDecimal getB5aldupct() {
		return this.b5aldupct;
	}

	public void setB5aldupct(BigDecimal b5aldupct) {
		this.b5aldupct = b5aldupct;
	}

	public BigDecimal getB5aldvpct() {
		return this.b5aldvpct;
	}

	public void setB5aldvpct(BigDecimal b5aldvpct) {
		this.b5aldvpct = b5aldvpct;
	}

	public BigDecimal getB5aldwpct() {
		return this.b5aldwpct;
	}

	public void setB5aldwpct(BigDecimal b5aldwpct) {
		this.b5aldwpct = b5aldwpct;
	}

	public BigDecimal getB5aldxpct() {
		return this.b5aldxpct;
	}

	public void setB5aldxpct(BigDecimal b5aldxpct) {
		this.b5aldxpct = b5aldxpct;
	}

	public BigDecimal getB5aldypct() {
		return this.b5aldypct;
	}

	public void setB5aldypct(BigDecimal b5aldypct) {
		this.b5aldypct = b5aldypct;
	}

	public BigDecimal getB5aldzpct() {
		return this.b5aldzpct;
	}

	public void setB5aldzpct(BigDecimal b5aldzpct) {
		this.b5aldzpct = b5aldzpct;
	}

	public BigDecimal getB5alaapct() {
		return this.b5alaapct;
	}

	public void setB5alaapct(BigDecimal b5alaapct) {
		this.b5alaapct = b5alaapct;
	}

	public BigDecimal getB5alabpct() {
		return this.b5alabpct;
	}

	public void setB5alabpct(BigDecimal b5alabpct) {
		this.b5alabpct = b5alabpct;
	}

	public BigDecimal getB5alacpct() {
		return this.b5alacpct;
	}

	public void setB5alacpct(BigDecimal b5alacpct) {
		this.b5alacpct = b5alacpct;
	}
	
	public int getB5aljknbr() {
		return this.b5aljknbr;
	}

	public void setB5aljknbr(int b5aljknbr) {
		this.b5aljknbr = b5aljknbr;
	}
	public int getB5aljlnbr() {
		return this.b5aljlnbr;
	}

	public void setB5aljlnbr(int b5aljlnbr) {
		this.b5aljlnbr = b5aljlnbr;
	}
	public int getB5aljmnbr() {
		return this.b5aljmnbr;
	}

	public void setB5aljmnbr(int b5aljmnbr) {
		this.b5aljmnbr = b5aljmnbr;
	}
	public int getB5aljnnbr() {
		return this.b5aljnnbr;
	}

	public void setB5aljnnbr(int b5aljnnbr) {
		this.b5aljnnbr = b5aljnnbr;
	}
	public int getB5aljonbr() {
		return this.b5aljonbr;
	}

	public void setB5aljonbr(int b5aljonbr) {
		this.b5aljonbr = b5aljonbr;
	}
	public int getB5aljpnbr() {
		return this.b5aljpnbr;
	}

	public void setB5aljpnbr(int b5aljpnbr) {
		this.b5aljpnbr = b5aljpnbr;
	}
	public int getB5aljqnbr() {
		return this.b5aljqnbr;
	}

	public void setB5aljqnbr(int b5aljqnbr) {
		this.b5aljqnbr = b5aljqnbr;
	}
	public int getB5aljrnbr() {
		return this.b5aljrnbr;
	}

	public void setB5aljrnbr(int b5aljrnbr) {
		this.b5aljrnbr = b5aljrnbr;
	}
	public int getB5aljsnbr() {
		return this.b5aljsnbr;
	}

	public void setB5aljsnbr(int b5aljsnbr) {
		this.b5aljsnbr = b5aljsnbr;
	}
	public int getB5aljtnbr() {
		return this.b5aljtnbr;
	}

	public void setB5aljtnbr(int b5aljtnbr) {
		this.b5aljtnbr = b5aljtnbr;
	}
	public int getB5aljunbr() {
		return this.b5aljunbr;
	}

	public void setB5aljunbr(int b5aljunbr) {
		this.b5aljunbr = b5aljunbr;
	}
	public int getB5aljvnbr() {
		return this.b5aljvnbr;
	}

	public void setB5aljvnbr(int b5aljvnbr) {
		this.b5aljvnbr = b5aljvnbr;
	}
	public int getB5aljwnbr() {
		return this.b5aljwnbr;
	}

	public void setB5aljwnbr(int b5aljwnbr) {
		this.b5aljwnbr = b5aljwnbr;
	}
	public int getB5aljxnbr() {
		return this.b5aljxnbr;
	}

	public void setB5aljxnbr(int b5aljxnbr) {
		this.b5aljxnbr = b5aljxnbr;
	}
	public int getB5aljynbr() {
		return this.b5aljynbr;
	}

	public void setB5aljynbr(int b5aljynbr) {
		this.b5aljynbr = b5aljynbr;
	}
	public int getB5aljznbr() {
		return this.b5aljznbr;
	}

	public void setB5aljznbr(int b5aljznbr) {
		this.b5aljznbr = b5aljznbr;
	}
	public int getB5alaanbr() {
		return this.b5alaanbr;
	}

	public void setB5alaanbr(int b5alaanbr) {
		this.b5alaanbr = b5alaanbr;
	}
	public int getB5alabnbr() {
		return this.b5alabnbr;
	}

	public void setB5alabnbr(int b5alabnbr) {
		this.b5alabnbr = b5alabnbr;
	}
	public int getB5alacnbr() {
		return this.b5alacnbr;
	}

	public void setB5alacnbr(int b5alacnbr) {
		this.b5alacnbr = b5alacnbr;
	}
	public int getB5aladnbr() {
		return this.b5aladnbr;
	}

	public void setB5aladnbr(int b5aladnbr) {
		this.b5aladnbr = b5aladnbr;
	}

	public BigDecimal getB5alwqval() {
		return this.b5alwqval;
	}

	public void setB5alwqval(BigDecimal b5alwqval) {
		this.b5alwqval = b5alwqval;
	}

	public BigDecimal getB5alwrval() {
		return this.b5alwrval;
	}

	public void setB5alwrval(BigDecimal b5alwrval) {
		this.b5alwrval = b5alwrval;
	}

	public BigDecimal getB5alwsval() {
		return this.b5alwsval;
	}

	public void setB5alwsval(BigDecimal b5alwsval) {
		this.b5alwsval = b5alwsval;
	}

	public BigDecimal getB5alwtval() {
		return this.b5alwtval;
	}

	public void setB5alwtval(BigDecimal b5alwtval) {
		this.b5alwtval = b5alwtval;
	}

	public BigDecimal getB5alwuval() {
		return this.b5alwuval;
	}

	public void setB5alwuval(BigDecimal b5alwuval) {
		this.b5alwuval = b5alwuval;
	}

	public BigDecimal getB5alwvval() {
		return this.b5alwvval;
	}

	public void setB5alwvval(BigDecimal b5alwvval) {
		this.b5alwvval = b5alwvval;
	}

	public BigDecimal getB5alwwval() {
		return this.b5alwwval;
	}

	public void setB5alwwval(BigDecimal b5alwwval) {
		this.b5alwwval = b5alwwval;
	}

	public BigDecimal getB5alwxval() {
		return this.b5alwxval;
	}

	public void setB5alwxval(BigDecimal b5alwxval) {
		this.b5alwxval = b5alwxval;
	}

	public BigDecimal getB5alwyval() {
		return this.b5alwyval;
	}

	public void setB5alwyval(BigDecimal b5alwyval) {
		this.b5alwyval = b5alwyval;
	}

	public BigDecimal getB5alwzval() {
		return this.b5alwzval;
	}

	public void setB5alwzval(BigDecimal b5alwzval) {
		this.b5alwzval = b5alwzval;
	}

	public BigDecimal getB5alaaval() {
		return this.b5alaaval;
	}

	public void setB5alaaval(BigDecimal b5alaaval) {
		this.b5alaaval = b5alaaval;
	}

	public BigDecimal getB5alabval() {
		return this.b5alabval;
	}

	public void setB5alabval(BigDecimal b5alabval) {
		this.b5alabval = b5alabval;
	}

	public BigDecimal getB5alacval() {
		return this.b5alacval;
	}

	public void setB5alacval(BigDecimal b5alacval) {
		this.b5alacval = b5alacval;
	}

	public BigDecimal getB5aladval() {
		return this.b5aladval;
	}

	public void setB5aladval(BigDecimal b5aladval) {
		this.b5aladval = b5aladval;
	}

	public BigDecimal getB5alaeval() {
		return this.b5alaeval;
	}

	public void setB5alaeval(BigDecimal b5alaeval) {
		this.b5alaeval = b5alaeval;
	}

	public BigDecimal getB5alafval() {
		return this.b5alafval;
	}

	public void setB5alafval(BigDecimal b5alafval) {
		this.b5alafval = b5alafval;
	}

	public BigDecimal getB5alagval() {
		return this.b5alagval;
	}

	public void setB5alagval(BigDecimal b5alagval) {
		this.b5alagval = b5alagval;
	}

	public BigDecimal getB5alahval() {
		return this.b5alahval;
	}

	public void setB5alahval(BigDecimal b5alahval) {
		this.b5alahval = b5alahval;
	}

	public BigDecimal getB5alaival() {
		return this.b5alaival;
	}

	public void setB5alaival(BigDecimal b5alaival) {
		this.b5alaival = b5alaival;
	}

	public BigDecimal getB5alajval() {
		return this.b5alajval;
	}

	public void setB5alajval(BigDecimal b5alajval) {
		this.b5alajval = b5alajval;
	}

	public int getB5alsjdte() {
		return this.b5alsjdte;
	}

	public void setB5alsjdte(int b5alsjdte) {
		this.b5alsjdte = b5alsjdte;
	}
	
	public int getB5alskdte() {
		return this.b5alskdte;
	}

	public void setB5alskdte(int b5alskdte) {
		this.b5alskdte = b5alskdte;
	}
	public int getB5alsldte() {
		return this.b5alsldte;
	}

	public void setB5alsldte(int b5alsldte) {
		this.b5alsldte = b5alsldte;
	}
	public int getB5alsmdte() {
		return this.b5alsmdte;
	}

	public void setB5alsmdte(int b5alsmdte) {
		this.b5alsmdte = b5alsmdte;
	}
	public int getB5alsndte() {
		return this.b5alsndte;
	}

	public void setB5alsndte(int b5alsndte) {
		this.b5alsndte = b5alsndte;
	}
	public int getB5alsodte() {
		return this.b5alsodte;
	}

	public void setB5alsodte(int b5alsodte) {
		this.b5alsodte = b5alsodte;
	}
	public int getB5alspdte() {
		return this.b5alspdte;
	}

	public void setB5alspdte(int b5alspdte) {
		this.b5alspdte = b5alspdte;
	}
	public int getB5alsqdte() {
		return this.b5alsqdte;
	}

	public void setB5alsqdte(int b5alsqdte) {
		this.b5alsqdte = b5alsqdte;
	}
	public int getB5alsrdte() {
		return this.b5alsrdte;
	}

	public void setB5alsrdte(int b5alsrdte) {
		this.b5alsrdte = b5alsrdte;
	}
	public int getB5alssdte() {
		return this.b5alssdte;
	}

	public void setB5alssdte(int b5alssdte) {
		this.b5alssdte = b5alssdte;
	}
	public int getB5alstdte() {
		return this.b5alstdte;
	}

	public void setB5alstdte(int b5alstdte) {
		this.b5alstdte = b5alstdte;
	}
	public int getB5alsudte() {
		return this.b5alsudte;
	}

	public void setB5alsudte(int b5alsudte) {
		this.b5alsudte = b5alsudte;
	}
	public int getB5alsvdte() {
		return this.b5alsvdte;
	}

	public void setB5alsvdte(int b5alsvdte) {
		this.b5alsvdte = b5alsvdte;
	}
	public int getB5alswdte() {
		return this.b5alswdte;
	}

	public void setB5alswdte(int b5alswdte) {
		this.b5alswdte = b5alswdte;
	}
	public int getB5alsxdte() {
		return this.b5alsxdte;
	}

	public void setB5alsxdte(int b5alsxdte) {
		this.b5alsxdte = b5alsxdte;
	}
	public int getB5alsydte() {
		return this.b5alsydte;
	}

	public void setB5alsydte(int b5alsydte) {
		this.b5alsydte = b5alsydte;
	}
	public int getB5alszdte() {
		return this.b5alszdte;
	}

	public void setB5alszdte(int b5alszdte) {
		this.b5alszdte = b5alszdte;
	}
	public int getB5alaadte() {
		return this.b5alaadte;
	}

	public void setB5alaadte(int b5alaadte) {
		this.b5alaadte = b5alaadte;
	}
	public int getB5alabdte() {
		return this.b5alabdte;
	}

	public void setB5alabdte(int b5alabdte) {
		this.b5alabdte = b5alabdte;
	}
	public int getB5alacdte() {
		return this.b5alacdte;
	}

	public void setB5alacdte(int b5alacdte) {
		this.b5alacdte = b5alacdte;
	}
	
	public String getB5al6txt() {
		return this.b5al6txt;
	}

	public void setB5al6txt(String b5al6txt) {
		this.b5al6txt = b5al6txt;
	}

	public String getB5al7txt() {
		return this.b5al7txt;
	}

	public void setB5al7txt(String b5al7txt) {
		this.b5al7txt = b5al7txt;
	}

	public String getB5al8txt() {
		return this.b5al8txt;
	}

	public void setB5al8txt(String b5al8txt) {
		this.b5al8txt = b5al8txt;
	}

	public String getB5al9txt() {
		return this.b5al9txt;
	}

	public void setB5al9txt(String b5al9txt) {
		this.b5al9txt = b5al9txt;
	}

	public String getB5al10txt() {
		return this.b5al10txt;
	}

	public void setB5al10txt(String b5al10txt) {
		this.b5al10txt = b5al10txt;
	}

	public String getB5al11txt() {
		return this.b5al11txt;
	}

	public void setB5al11txt(String b5al11txt) {
		this.b5al11txt = b5al11txt;
	}

	public String getB5al12txt() {
		return this.b5al12txt;
	}

	public void setB5al12txt(String b5al12txt) {
		this.b5al12txt = b5al12txt;
	}

	public String getB5al13txt() {
		return this.b5al13txt;
	}

	public void setB5al13txt(String b5al13txt) {
		this.b5al13txt = b5al13txt;
	}

	public String getB5al14txt() {
		return this.b5al14txt;
	}

	public void setB5al14txt(String b5al14txt) {
		this.b5al14txt = b5al14txt;
	}

	public String getB5al15txt() {
		return this.b5al15txt;
	}

	public void setB5al15txt(String b5al15txt) {
		this.b5al15txt = b5al15txt;
	}

	public String getB5al16txt() {
		return this.b5al16txt;
	}

	public void setB5al16txt(String b5al16txt) {
		this.b5al16txt = b5al16txt;
	}

	public String getB5al17txt() {
		return this.b5al17txt;
	}

	public void setB5al17txt(String b5al17txt) {
		this.b5al17txt = b5al17txt;
	}

	public String getB5al18txt() {
		return this.b5al18txt;
	}

	public void setB5al18txt(String b5al18txt) {
		this.b5al18txt = b5al18txt;
	}

	public String getB5al19txt() {
		return this.b5al19txt;
	}

	public void setB5al19txt(String b5al19txt) {
		this.b5al19txt = b5al19txt;
	}

	public String getB5al20txt() {
		return this.b5al20txt;
	}

	public void setB5al20txt(String b5al20txt) {
		this.b5al20txt = b5al20txt;
	}

	public String getB5al21txt() {
		return this.b5al21txt;
	}

	public void setB5al21txt(String b5al21txt) {
		this.b5al21txt = b5al21txt;
	}

	public String getB5al22txt() {
		return this.b5al22txt;
	}

	public void setB5al22txt(String b5al22txt) {
		this.b5al22txt = b5al22txt;
	}

	public String getB5al23txt() {
		return this.b5al23txt;
	}

	public void setB5al23txt(String b5al23txt) {
		this.b5al23txt = b5al23txt;
	}

	public String getB5al24txt() {
		return this.b5al24txt;
	}

	public void setB5al24txt(String b5al24txt) {
		this.b5al24txt = b5al24txt;
	}

	public String getB5al25txt() {
		return this.b5al25txt;
	}

	public void setB5al25txt(String b5al25txt) {
		this.b5al25txt = b5al25txt;
	}

	public String getB5al26txt() {
		return this.b5al26txt;
	}

	public void setB5al26txt(String b5al26txt) {
		this.b5al26txt = b5al26txt;
	}

	public String getB5al27txt() {
		return this.b5al27txt;
	}

	public void setB5al27txt(String b5al27txt) {
		this.b5al27txt = b5al27txt;
	}

	public String getB5al28txt() {
		return this.b5al28txt;
	}

	public void setB5al28txt(String b5al28txt) {
		this.b5al28txt = b5al28txt;
	}

	public String getB5al29txt() {
		return this.b5al29txt;
	}

	public void setB5al29txt(String b5al29txt) {
		this.b5al29txt = b5al29txt;
	}

	public String getB5al30txt() {
		return this.b5al30txt;
	}

	public void setB5al30txt(String b5al30txt) {
		this.b5al30txt = b5al30txt;
	}

	public String getB5al31txt() {
		return this.b5al31txt;
	}

	public void setB5al31txt(String b5al31txt) {
		this.b5al31txt = b5al31txt;
	}

	public String getB5al32txt() {
		return this.b5al32txt;
	}

	public void setB5al32txt(String b5al32txt) {
		this.b5al32txt = b5al32txt;
	}

	public String getB5al33txt() {
		return this.b5al33txt;
	}

	public void setB5al33txt(String b5al33txt) {
		this.b5al33txt = b5al33txt;
	}

	public String getB5al34txt() {
		return this.b5al34txt;
	}

	public void setB5al34txt(String b5al34txt) {
		this.b5al34txt = b5al34txt;
	}

	public String getB5al35txt() {
		return this.b5al35txt;
	}

	public void setB5al35txt(String b5al35txt) {
		this.b5al35txt = b5al35txt;
	}

	public String getB5al38txt() {
		return this.b5al38txt;
	}

	public void setB5al38txt(String b5al38txt) {
		this.b5al38txt = b5al38txt;
	}

	public String getB5al39txt() {
		return this.b5al39txt;
	}

	public void setB5al39txt(String b5al39txt) {
		this.b5al39txt = b5al39txt;
	}

	public String getB5al40txt() {
		return this.b5al40txt;
	}

	public void setB5al40txt(String b5al40txt) {
		this.b5al40txt = b5al40txt;
	}

	public String getB5al41txt() {
		return this.b5al41txt;
	}

	public void setB5al41txt(String b5al41txt) {
		this.b5al41txt = b5al41txt;
	}

	public String getB5al42txt() {
		return this.b5al42txt;
	}

	public void setB5al42txt(String b5al42txt) {
		this.b5al42txt = b5al42txt;
	}

	public String getB5al43txt() {
		return this.b5al43txt;
	}

	public void setB5al43txt(String b5al43txt) {
		this.b5al43txt = b5al43txt;
	}

	public String getB5al44txt() {
		return this.b5al44txt;
	}

	public void setB5al44txt(String b5al44txt) {
		this.b5al44txt = b5al44txt;
	}

	public String getB5al45txt() {
		return this.b5al45txt;
	}

	public void setB5al45txt(String b5al45txt) {
		this.b5al45txt = b5al45txt;
	}
	public String getB5al46txt() {
		return this.b5al46txt;
	}

	public void setB5al46txt(String b5al46txt) {
		this.b5al46txt = b5al46txt;
	}

	public String getB5al47txt() {
		return this.b5al47txt;
	}

	public void setB5al47txt(String b5al47txt) {
		this.b5al47txt = b5al47txt;
	}
	
}
