// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.sql.util.ABOSqlDB2Conversion;
import com.csc.pt.svc.data.to.Bashlgbj00TO;

public class Bashlgbj00DAO {
	public static DBAccessStatus insertRec(Bashlgbj00TO bashlgbj00TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASHLGBJ00");
			Query query = session.getNamedQuery("Bashlgbj00DAO_insertRec");
			query.setString("bashlgbj00TO_loguser", bashlgbj00TO.getLoguser());
			query.setDate("bashlgbj00TO_logdate", ABOSqlDB2Conversion.dateToSQL(bashlgbj00TO.getLogdate()));
			query.setTime("bashlgbj00TO_logtime", ABOSqlDB2Conversion.timeToSQL(bashlgbj00TO.getLogtime()));
			query.setInteger("bashlgbj00TO_logseqnum", bashlgbj00TO.getLogseqnum());
			query.setString("bashlgbj00TO_location", bashlgbj00TO.getLocation());
			query.setString("bashlgbj00TO_masterco", bashlgbj00TO.getMasterco());
			query.setString("bashlgbj00TO_symbol", bashlgbj00TO.getSymbol());
			query.setString("bashlgbj00TO_policyno", bashlgbj00TO.getPolicyno());
			query.setString("bashlgbj00TO_module", bashlgbj00TO.getModule());
			query.setString("bashlgbj00TO_insline", bashlgbj00TO.getInsline());
			query.setInteger("bashlgbj00TO_riskloc", bashlgbj00TO.getRiskloc());
			query.setInteger("bashlgbj00TO_risksubloc", bashlgbj00TO.getRisksubloc());
			query.setString("bashlgbj00TO_product", bashlgbj00TO.getProduct());
			query.setInteger("bashlgbj00TO_unitno", bashlgbj00TO.getUnitno());
			query.setCharacter("bashlgbj00TO_recstatus", bashlgbj00TO.getRecstatus());
			query.setCharacter("bashlgbj00TO_bjc7st", bashlgbj00TO.getBjc7st());
			query.setInteger("bashlgbj00TO_bjandt", bashlgbj00TO.getBjandt());
			query.setShort("bashlgbj00TO_bjafnb", bashlgbj00TO.getBjafnb());
			query.setInteger("bashlgbj00TO_bjaldt", bashlgbj00TO.getBjaldt());
			query.setInteger("bashlgbj00TO_bjafdt", bashlgbj00TO.getBjafdt());
			query.setString("bashlgbj00TO_bjtype0act", bashlgbj00TO.getBjtype0act());
			query.setString("bashlgbj00TO_bjbatx", bashlgbj00TO.getBjbatx());
			query.setCharacter("bashlgbj00TO_bjdzst", bashlgbj00TO.getBjdzst());
			query.setCharacter("bashlgbj00TO_bjd0st", bashlgbj00TO.getBjd0st());
			query.setCharacter("bashlgbj00TO_bjd2st", bashlgbj00TO.getBjd2st());
			query.setCharacter("bashlgbj00TO_bjd3st", bashlgbj00TO.getBjd3st());
			query.setBigDecimal("bashlgbj00TO_bja3va", bashlgbj00TO.getBja3va());
			query.setBigDecimal("bashlgbj00TO_bjbrva", bashlgbj00TO.getBjbrva());
			query.setBigDecimal("bashlgbj00TO_bjbsva", bashlgbj00TO.getBjbsva());
			query.setBigDecimal("bashlgbj00TO_bjbtva", bashlgbj00TO.getBjbtva());
			query.setBigDecimal("bashlgbj00TO_bjaupc", bashlgbj00TO.getBjaupc());
			query.setString("bashlgbj00TO_bjaqcd", bashlgbj00TO.getBjaqcd());
			query.setString("bashlgbj00TO_bjarcd", bashlgbj00TO.getBjarcd());
			query.setString("bashlgbj00TO_bjascd", bashlgbj00TO.getBjascd());
			query.setString("bashlgbj00TO_bjeetx", bashlgbj00TO.getBjeetx());
			query.setString("bashlgbj00TO_bjbccd", bashlgbj00TO.getBjbccd());
			query.setString("bashlgbj00TO_bjagnb", bashlgbj00TO.getBjagnb());
			query.setShort("bashlgbj00TO_bjahnb", bashlgbj00TO.getBjahnb());
			query.setString("bashlgbj00TO_bjaxtx", bashlgbj00TO.getBjaxtx());
			query.setString("bashlgbj00TO_bjaytx", bashlgbj00TO.getBjaytx());
			query.setString("bashlgbj00TO_bjaztx", bashlgbj00TO.getBjaztx());
			query.setCharacter("bashlgbj00TO_bjacst", bashlgbj00TO.getBjacst());
			query.setCharacter("bashlgbj00TO_bjadst", bashlgbj00TO.getBjadst());
			query.setBigDecimal("bashlgbj00TO_bjainb", bashlgbj00TO.getBjainb());
			query.setString("bashlgbj00TO_bjajnb", bashlgbj00TO.getBjajnb());
			query.setInteger("bashlgbj00TO_bjaknb", bashlgbj00TO.getBjaknb());
			query.setInteger("bashlgbj00TO_bjamnb", bashlgbj00TO.getBjamnb());
			query.setInteger("bashlgbj00TO_bjannb", bashlgbj00TO.getBjannb());
			query.setInteger("bashlgbj00TO_bjaonb", bashlgbj00TO.getBjaonb());
			query.setString("bashlgbj00TO_bjavcd", bashlgbj00TO.getBjavcd());
			query.setString("bashlgbj00TO_bjawcd", bashlgbj00TO.getBjawcd());
			query.setString("bashlgbj00TO_bja1tx", bashlgbj00TO.getBja1tx());
			query.setString("bashlgbj00TO_bjapnb", bashlgbj00TO.getBjapnb());
			query.setInteger("bashlgbj00TO_bjasnb", bashlgbj00TO.getBjasnb());
			query.setInteger("bashlgbj00TO_bjatnb", bashlgbj00TO.getBjatnb());
			query.setInteger("bashlgbj00TO_bjaedt", bashlgbj00TO.getBjaedt());
			query.setInteger("bashlgbj00TO_bjavnb", bashlgbj00TO.getBjavnb());
			query.setInteger("bashlgbj00TO_bjaxnb", bashlgbj00TO.getBjaxnb());
			query.setBigDecimal("bashlgbj00TO_bjaznb", bashlgbj00TO.getBjaznb());
			query.setString("bashlgbj00TO_bja2tx", bashlgbj00TO.getBja2tx());
			query.setInteger("bashlgbj00TO_bja9tx", bashlgbj00TO.getBja9tx());
			query.setInteger("bashlgbj00TO_bjbenb", bashlgbj00TO.getBjbenb());
			query.setCharacter("bashlgbj00TO_bjcxst", bashlgbj00TO.getBjcxst());
			query.setCharacter("bashlgbj00TO_bji6tx", bashlgbj00TO.getBji6tx());
			query.setCharacter("bashlgbj00TO_bjcyst", bashlgbj00TO.getBjcyst());
			query.setCharacter("bashlgbj00TO_bjc5st", bashlgbj00TO.getBjc5st());
			query.setBigDecimal("bashlgbj00TO_bjappc", bashlgbj00TO.getBjappc());
			query.setBigDecimal("bashlgbj00TO_bjaqpc", bashlgbj00TO.getBjaqpc());
			query.setBigDecimal("bashlgbj00TO_bjarpc", bashlgbj00TO.getBjarpc());
			query.setString("bashlgbj00TO_bja5cd", bashlgbj00TO.getBja5cd());
			query.setInteger("bashlgbj00TO_bjcenb", bashlgbj00TO.getBjcenb());
			query.setInteger("bashlgbj00TO_bjcfnb", bashlgbj00TO.getBjcfnb());
			query.setCharacter("bashlgbj00TO_bjdvst", bashlgbj00TO.getBjdvst());
			query.setCharacter("bashlgbj00TO_bjdwst", bashlgbj00TO.getBjdwst());
			query.setCharacter("bashlgbj00TO_bjdxst", bashlgbj00TO.getBjdxst());
			query.setCharacter("bashlgbj00TO_bjdyst", bashlgbj00TO.getBjdyst());
			query.setCharacter("bashlgbj00TO_bjczst", bashlgbj00TO.getBjczst());
			query.setCharacter("bashlgbj00TO_bjc0st", bashlgbj00TO.getBjc0st());
			query.setCharacter("bashlgbj00TO_bjc1st", bashlgbj00TO.getBjc1st());
			query.setCharacter("bashlgbj00TO_bjc2st", bashlgbj00TO.getBjc2st());
			query.setCharacter("bashlgbj00TO_bjiytx", bashlgbj00TO.getBjiytx());
			query.setCharacter("bashlgbj00TO_bjiztx", bashlgbj00TO.getBjiztx());
			query.setCharacter("bashlgbj00TO_bji0tx", bashlgbj00TO.getBji0tx());
			query.setCharacter("bashlgbj00TO_bji1tx", bashlgbj00TO.getBji1tx());
			query.setCharacter("bashlgbj00TO_bji2tx", bashlgbj00TO.getBji2tx());
			query.setCharacter("bashlgbj00TO_bji3tx", bashlgbj00TO.getBji3tx());
			query.setCharacter("bashlgbj00TO_bji4tx", bashlgbj00TO.getBji4tx());
			query.setCharacter("bashlgbj00TO_bji5tx", bashlgbj00TO.getBji5tx());
			query.setString("bashlgbj00TO_bjakcd", bashlgbj00TO.getBjakcd());
			query.setString("bashlgbj00TO_bjalcd", bashlgbj00TO.getBjalcd());
			query.setString("bashlgbj00TO_bjamcd", bashlgbj00TO.getBjamcd());
			query.setString("bashlgbj00TO_bjancd", bashlgbj00TO.getBjancd());
			query.setBigDecimal("bashlgbj00TO_bjaepc", bashlgbj00TO.getBjaepc());
			query.setBigDecimal("bashlgbj00TO_bjafpc", bashlgbj00TO.getBjafpc());
			query.setInteger("bashlgbj00TO_bjbknb", bashlgbj00TO.getBjbknb());
			query.setInteger("bashlgbj00TO_bjblnb", bashlgbj00TO.getBjblnb());
			query.setBigDecimal("bashlgbj00TO_bjagva", bashlgbj00TO.getBjagva());
			query.setBigDecimal("bashlgbj00TO_bjahva", bashlgbj00TO.getBjahva());
			query.setString("bashlgbj00TO_bjbjcd", bashlgbj00TO.getBjbjcd());
			query.setString("bashlgbj00TO_bjbkcd", bashlgbj00TO.getBjbkcd());
			query.setInteger("bashlgbj00TO_bjesnb", bashlgbj00TO.getBjesnb());

			int result = query.executeUpdate();
			status.setNumOfRows(result);

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}

	public Bashlgbj00TO readHistoryFIle(String location, String policyno, String symbol, String masterco, String module,
			int unitno, int lossdate) {
			DBAccessStatus status = new DBAccessStatus();
			Bashlgbj00TO localbashlgbj00TO = null;
			
			try
			{
			Session session = HibernateSessionFactory.current().getSession("BASHLGBJ00");
			Query query = session.getNamedQuery("Bashlgbj00TO_GETHISTORYRECORDS");
			query.setParameter("location", location);
			query.setParameter("masterco", masterco);
			query.setParameter("symbol", symbol);
			query.setParameter("policyno", policyno);			
			query.setParameter("module", module);
			query.setParameter("unitno", unitno);
			query.setParameter("lossdate", lossdate);
			query.setMaxResults(1);
			List resultList = query.list();

			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localbashlgbj00TO = (Bashlgbj00TO)(obj);
					
					status = new DBAccessStatus(DBAccessStatus.DB_OK);
					localbashlgbj00TO.setDBAccessStatus(status);
				} 
				
			}
			else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		}
		 catch (HibernateException ex) {
			status.setException(ex);
		}
		 		
		return localbashlgbj00TO;
		
	}
}
