// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;

import java.util.List;
import java.util.ListIterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.hibernate.Util;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.Primitives;

import com.csc.pt.svc.data.to.Pmspsa50TO;

/*
/****************************************************************
* PGMR: Raveesh Rao		 	DATE: 05/31/12                      *
* FSIT# 176098   RESOLUTION # 63769                             *
* Problem: No offset record generated in SA50 and RI00 after    *
* a FAC arrangement is cancel flat  					    	*
* RESOLUTION :Added new query to fetch the maximum Reinsurance  *
* Sequence from PMSPSA50 table.                                 *
*****************************************************************/
/*
 ******************************************************************
 * REF : 52721    PGMR: Risabh kumar             DATE: 02/25/13   *
 * PROJ: B61371   FSIT ISSUE NO:189367           SRC:  New        *
 *                RESOLUTION ID:72096                             *
 * ENHANCEMENT: REI - Allow retroactive treaty changes by         *
 *              performing offsets and onsets in current month.   *
 ******************************************************************
 */
public class Pmspsa50DAO {
	private ScrollableResults clcursor71;
	private ListIterator rowIter = null;
	private List rowList = null;
	private int pagesize = bphx.c2ab.util.ConfigSettings.getDefaultCursorPageSize();
	private int recnum = 1;
	private int pagenum = 0;
	private boolean cursorNoWhere = false;
	private Pmspsa50TO lastPmspsa50TO = null;

	public Pmspsa50TO fetchNext(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowList == null) {
			// cursor not opened, try opening
			if (lastPmspsa50TO == null) {
				pagenum = 0;
				status = openPmspsa50TOCsr();
			} else {
				if (lastPmspsa50TO.getDBAccessStatus().isSuccess()) {
					//last READ was successful
					Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
					status = openPmspsa50TOCsr(KeyType.GREATER_OR_EQ ,
						lastPmspsa50TO.getLocation(),
						lastPmspsa50TO.getPolicyno(),
						lastPmspsa50TO.getSymbol(),
						lastPmspsa50TO.getMasterco(),
						lastPmspsa50TO.getModule(),
						lastPmspsa50TO.getUnitno(),
						lastPmspsa50TO.getCovseq(),
						lastPmspsa50TO.getTransseq(),
						lastPmspsa50TO.getReinseq());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
								lastPmspsa50TOsave.getPolicyno(),
								lastPmspsa50TOsave.getSymbol(),
								lastPmspsa50TOsave.getMasterco(),
								lastPmspsa50TOsave.getModule(),
								lastPmspsa50TOsave.getUnitno(),
								lastPmspsa50TOsave.getCovseq(),
								lastPmspsa50TOsave.getTransseq(),
								lastPmspsa50TOsave.getReinseq())) {
							rowIter.next(); //skip record already processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					//unsuccessful READ
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localPmspsa50TO = new Pmspsa50TO();
				}
			}
		}
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (Pmspsa50TO.class.isInstance(obj)) {
				localPmspsa50TO = ((Pmspsa50TO) obj);
			} else {
				fetch(((Object[]) obj), localPmspsa50TO);
			}
			lastPmspsa50TO = localPmspsa50TO;
			recnum++;
		} else {
			if (rowIter != null && recnum >= pagesize - 1) {
				if(!cursorNoWhere) {
					//Open cursor with last key to get next page
				Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
				status = openPmspsa50TOCsr(KeyType.GREATER_OR_EQ ,
					lastPmspsa50TO.getLocation(),
					lastPmspsa50TO.getPolicyno(),
					lastPmspsa50TO.getSymbol(),
					lastPmspsa50TO.getMasterco(),
					lastPmspsa50TO.getModule(),
					lastPmspsa50TO.getUnitno(),
					lastPmspsa50TO.getCovseq(),
					lastPmspsa50TO.getTransseq(),
					lastPmspsa50TO.getReinseq());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
							lastPmspsa50TOsave.getPolicyno(),
							lastPmspsa50TOsave.getSymbol(),
							lastPmspsa50TOsave.getMasterco(),
							lastPmspsa50TOsave.getModule(),
							lastPmspsa50TOsave.getUnitno(),
							lastPmspsa50TOsave.getCovseq(),
							lastPmspsa50TOsave.getTransseq(),
							lastPmspsa50TOsave.getReinseq())) {
							rowIter.next();			// Throw away record already processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					pagenum++;
					status = openPmspsa50TOCsr();		//Open cursor to nowhere with pagenum incremented to get next page of rows
				}
				localPmspsa50TO = fetchNext(localPmspsa50TO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localPmspsa50TO = new Pmspsa50TO();
			}
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;
	}

	public Pmspsa50TO fetchPrevious(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowList == null) {
			// cursor not opened, try opening
			status = openPmspsa50TOCsr();
		}
		if (rowIter != null && rowIter.hasPrevious()) {
			localPmspsa50TO = ((Pmspsa50TO) rowIter.previous());
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localPmspsa50TO = new Pmspsa50TO();
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;
	}

	public Pmspsa50TO fetchLast(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowList == null) {
			// cursor not opened, try opening
			status = openPmspsa50TOCsr();
		}
		if (rowIter != null && (rowList.size() > 0)) {
			localPmspsa50TO = ((Pmspsa50TO) rowList.get(rowList.size() - 1));
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localPmspsa50TO = new Pmspsa50TO();
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;
	}

	public Pmspsa50TO fetchById(Pmspsa50TO pmspsa50TO, String location, String policyno, String symbol, String masterco, String module,
			String unitno, short covseq, short transseq, short reinseq) {
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		closeCsr();
		localPmspsa50TO = selectById(pmspsa50TO, location, policyno, symbol, masterco, module, unitno, covseq, transseq, reinseq);
		lastPmspsa50TO = localPmspsa50TO;
		return localPmspsa50TO;
	}

	public static Pmspsa50TO selectById(Pmspsa50TO pmspsa50TO, String location, String policyno, String symbol, String masterco, String module,
			String unitno, short covseq, short transseq, short reinseq) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		try {
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			localPmspsa50TO.getId().setLocation(location);
			localPmspsa50TO.getId().setPolicyno(policyno);
			localPmspsa50TO.getId().setSymbol(symbol);
			localPmspsa50TO.getId().setMasterco(masterco);
			localPmspsa50TO.getId().setModule(module);
			localPmspsa50TO.getId().setUnitno(unitno);
			localPmspsa50TO.getId().setCovseq(covseq);
			localPmspsa50TO.getId().setTransseq(transseq);
			localPmspsa50TO.getId().setReinseq(reinseq);
			localPmspsa50TO = Util.get(session, localPmspsa50TO, pmspsa50TO.getId());
			if (localPmspsa50TO == null) {
				localPmspsa50TO = pmspsa50TO;
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;
	}

	public static DBAccessStatus insert(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			if (Util.get(session, pmspsa50TO, pmspsa50TO.getId()) != null) {
				status.setSqlCode(DBAccessStatus.DB_DUPLICATE);
			} else {
				if (session.contains(pmspsa50TO)) {
					session.evict(pmspsa50TO);
				}
				session.save(pmspsa50TO);
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openPmspsa50TOCsr(int keyType, String location, String policyno, String symbol, String masterco, String module,
			String unitno, short covseq, short transseq, short reinseq) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			Query query = null;
			if (keyType == KeyType.GREATER) {
				query = session.getNamedQuery("Pmspsa50TO_CURSOR_GT");
			} else if (keyType == KeyType.EQUAL) {
			   query = session.getNamedQuery("Pmspsa50TO_CURSOR_EQ");
			} else {
				// default
				query = session.getNamedQuery("Pmspsa50TO_CURSOR_GTE");
			}
			if (Functions.isInvalidSqlData(location)) {
				query.setParameter("location", null);
			} else {
				query.setParameter("location", location);
			}
			if (Functions.isInvalidSqlData(policyno)) {
				query.setParameter("policyno", null);
			} else {
				query.setParameter("policyno", policyno);
			}
			if (Functions.isInvalidSqlData(symbol)) {
				query.setParameter("symbol", null);
			} else {
				query.setParameter("symbol", symbol);
			}
			if (Functions.isInvalidSqlData(masterco)) {
				query.setParameter("masterco", null);
			} else {
				query.setParameter("masterco", masterco);
			}
			if (Functions.isInvalidSqlData(module)) {
				query.setParameter("module", null);
			} else {
				query.setParameter("module", module);
			}
			if (Functions.isInvalidSqlData(unitno)) {
				query.setParameter("unitno", null);
			} else {
				query.setParameter("unitno", unitno);
			}
			if (Functions.isInvalidSqlData(covseq)) {
				query.setParameter("covseq", null);
			} else {
				query.setParameter("covseq", covseq);
			}
			if (Functions.isInvalidSqlData(transseq)) {
				query.setParameter("transseq", null);
			} else {
				query.setParameter("transseq", transseq);
			}
			if (Functions.isInvalidSqlData(reinseq)) {
				query.setParameter("reinseq", null);
			} else {
				query.setParameter("reinseq", reinseq);
			}
			closeCsr();
			query.setCacheable(true);
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = false;
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
			} else {
				if (keyType == KeyType.EQUAL) {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openPmspsa50TOCsr() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			Query query = session.getNamedQuery("Pmspsa50TO_CURSOR_NOWHERE");
			closeCsr();
			query.setCacheable(true);
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = true;
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus closeCsr() {
		DBAccessStatus status = new DBAccessStatus();
		if (rowList != null) {
			rowList.clear();
			rowList = null;
			rowIter = null;
		}
		lastPmspsa50TO = null;
		return status;
	}

	public static DBAccessStatus delete(String location, String policyno, String symbol, String masterco, String module, String unitno, short covseq,
			short transseq, short reinseq) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			Pmspsa50TO pmspsa50TO = new Pmspsa50TO();
			// set data for where clause
			pmspsa50TO.getId().setLocation(location);
			pmspsa50TO.getId().setPolicyno(policyno);
			pmspsa50TO.getId().setSymbol(symbol);
			pmspsa50TO.getId().setMasterco(masterco);
			pmspsa50TO.getId().setModule(module);
			pmspsa50TO.getId().setUnitno(unitno);
			pmspsa50TO.getId().setCovseq(covseq);
			pmspsa50TO.getId().setTransseq(transseq);
			pmspsa50TO.getId().setReinseq(reinseq);
			pmspsa50TO = Util.get(session, pmspsa50TO, pmspsa50TO.getId());
			if (pmspsa50TO == null) {
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			} else {
				session.delete(pmspsa50TO);
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public void fetch(Object[] colData, Pmspsa50TO pmspsa50TO) {
		pmspsa50TO.getId().setSymbol(Primitives.getString(colData[(1) - 1]));
		pmspsa50TO.getId().setPolicyno(Primitives.getString(colData[(2) - 1]));
		pmspsa50TO.getId().setModule(Primitives.getString(colData[(3) - 1]));
		pmspsa50TO.getId().setMasterco(Primitives.getString(colData[(4) - 1]));
		pmspsa50TO.getId().setLocation(Primitives.getString(colData[(5) - 1]));
		pmspsa50TO.getId().setUnitno(Primitives.getString(colData[(6) - 1]));
		pmspsa50TO.getId().setCovseq(Primitives.getShort(colData[(7) - 1]));
		pmspsa50TO.getId().setTransseq(Primitives.getShort(colData[(8) - 1]));
		pmspsa50TO.getId().setReinseq(Primitives.getShort(colData[(9) - 1]));
		pmspsa50TO.setPricont(Primitives.getString(colData[(10) - 1]));
		pmspsa50TO.setRecont(Primitives.getString(colData[(11) - 1]));
		pmspsa50TO.setReeffdte(Primitives.getString(colData[(12) - 1]));
		pmspsa50TO.setOriginal(Primitives.getBigDecimal(colData[(13) - 1]));
		pmspsa50TO.setRepremium(Primitives.getBigDecimal(colData[(14) - 1]));
		pmspsa50TO.setCessoccur(Primitives.getLong(colData[(15) - 1]));
		pmspsa50TO.setReprempart(Primitives.getBigDecimal(colData[(16) - 1]));
		pmspsa50TO.setRecontcomm(Primitives.getBigDecimal(colData[(17) - 1]));
		pmspsa50TO.setReco(Primitives.getString(colData[(18) - 1]));
		pmspsa50TO.setRecobrok(Primitives.getString(colData[(19) - 1]));
		pmspsa50TO.setRegenind(Primitives.getChar(colData[(20) - 1]));
		pmspsa50TO.setReinstatus(Primitives.getChar(colData[(21) - 1]));
		pmspsa50TO.setCesseffdt(Primitives.getInt(colData[(22) - 1]));
		pmspsa50TO.setCessexpdt(Primitives.getInt(colData[(23) - 1]));
		pmspsa50TO.setCessentdt(Primitives.getInt(colData[(24) - 1]));
		pmspsa50TO.setAcctgdte(Primitives.getInt(colData[(25) - 1]));
	}

	public boolean isRecordEqual(String location, String policyno, String symbol, String masterco, String module, String unitno, short covseq,
			short transseq, short reinseq) {
		Pmspsa50TO pmspsa50TO = new Pmspsa50TO();
		if (rowIter != null && rowIter.hasNext()) {
			pmspsa50TO = ((Pmspsa50TO) rowIter.next());
			rowIter.previous();
		}
		if ((Functions.isInvalidSqlData(location) || location.compareTo(pmspsa50TO.getId().getLocation()) == 0)
			&& (Functions.isInvalidSqlData(policyno) || policyno.compareTo(pmspsa50TO.getId().getPolicyno()) == 0)
			&& (Functions.isInvalidSqlData(symbol) || symbol.compareTo(pmspsa50TO.getId().getSymbol()) == 0)
			&& (Functions.isInvalidSqlData(masterco) || masterco.compareTo(pmspsa50TO.getId().getMasterco()) == 0)
			&& (Functions.isInvalidSqlData(module) || module.compareTo(pmspsa50TO.getId().getModule()) == 0)
			&& (Functions.isInvalidSqlData(unitno) || unitno.compareTo(pmspsa50TO.getId().getUnitno()) == 0)
			&& (Functions.isInvalidSqlData(covseq) || covseq == pmspsa50TO.getId().getCovseq())
			&& (Functions.isInvalidSqlData(transseq) || transseq == pmspsa50TO.getId().getTransseq())
			&& (Functions.isInvalidSqlData(reinseq) || reinseq == pmspsa50TO.getId().getReinseq())) {
			return true;
		}
		return false;
	}

	public DBAccessStatus closeClcursor71() {
		DBAccessStatus status = new DBAccessStatus();
		if (clcursor71 != null) {
			clcursor71.close();
		}
		return status;
	}
	public static short fetchMaxReinsuranceSequence(String location, String policyno, String symbol, String masterco, String module,
			String unitno, short covseq, short transseq) {
			short localMaxReinSeq=0;
			DBAccessStatus status = new DBAccessStatus();
			List sa50List ;
		try{
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			Query query = session.getNamedQuery("Pmspsa50TO_SELECTMAXREINSEQ");
			query.setParameter("location", location);
			query.setParameter("policyno", policyno);
			query.setParameter("symbol", symbol);
			query.setParameter("masterco", masterco);
			query.setParameter("module", module);
			query.setParameter("unitno", unitno);
			query.setParameter("covseq", covseq);
			query.setParameter("transseq", transseq);

			query.setMaxResults(1);
			sa50List = query.list();

			if(sa50List.isEmpty()) {
				status = new DBAccessStatus(DBAccessStatus.DB_EOF);

			} else {
				status = new DBAccessStatus(DBAccessStatus.DB_OK);
				localMaxReinSeq = (Short) sa50List.get(0);
			}

		}
		catch (HibernateException ex)
		{
			status.setException(ex);
		}
		return localMaxReinSeq;
	}

	public void fetchRecords(Object[] colData, Pmspsa50TO pmspsa50TO) {
		pmspsa50TO.getId().setUnitno(Primitives.getString(colData[(1) - 1]));
		pmspsa50TO.getId().setCovseq(Primitives.getShort(colData[(2) - 1]));
		pmspsa50TO.getId().setTransseq(Primitives.getShort(colData[(3) - 1]));
		pmspsa50TO.getId().setReinseq(Primitives.getShort(colData[(4) - 1]));
		pmspsa50TO.setOriginal(Primitives.getBigDecimal(colData[(5) - 1]));
		pmspsa50TO.setRepremium(Primitives.getBigDecimal(colData[(6) - 1]));
		pmspsa50TO.setReprempart(Primitives.getBigDecimal(colData[(7) - 1]));
		pmspsa50TO.setRecontcomm(Primitives.getBigDecimal(colData[(8) - 1]));
	}

	public DBAccessStatus openPmspsa50TOPosCsr(String retroPolLoc,
			String retroPolMoc, String retroPolSym, String retroPolNum,
			String retroPolMod, String retroContract, String retroContractEffDte) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession(
					"PMSPSA50");
			Query query = session
					.getNamedQuery("Pmspsa50DAO_openPmspsa50TOPosCsr");
			query.setString("location", retroPolLoc);
			query.setString("module", retroPolMod);
			query.setString("masterco", retroPolMoc);
			query.setString("policyno", retroPolNum);
			query.setString("symbol", retroPolSym);
			query.setString("recont", retroContract);
			query.setString("reeffdte", retroContractEffDte);

			query.setCacheable(true);
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = true;
			if (pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
			}
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}

	public Pmspsa50TO fetchNextSa50PosCrs(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowList == null) {
			if (lastPmspsa50TO == null) {
				pagenum = 0;
				status = openPmspsa50TOCsr();
			} else {
				if (lastPmspsa50TO.getDBAccessStatus().isSuccess()) {
					Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
					status = openPmspsa50TOPosCsr(lastPmspsa50TO.getLocation(),
							lastPmspsa50TO.getMasterco(),
							lastPmspsa50TO.getSymbol(),
							lastPmspsa50TO.getPolicyno(),
							lastPmspsa50TO.getModule(),
							lastPmspsa50TO.getRecont(),
							lastPmspsa50TO.getReeffdte());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
								lastPmspsa50TOsave.getPolicyno(),
								lastPmspsa50TOsave.getSymbol(),
								lastPmspsa50TOsave.getMasterco(),
								lastPmspsa50TOsave.getModule(),
								lastPmspsa50TOsave.getUnitno(),
								lastPmspsa50TOsave.getCovseq(),
								lastPmspsa50TOsave.getTransseq(),
								lastPmspsa50TOsave.getReinseq())) {
							rowIter.next(); // skip record already processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					// unsuccessful READ
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localPmspsa50TO = new Pmspsa50TO();
				}
			}
		}
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (Pmspsa50TO.class.isInstance(obj)) {
				localPmspsa50TO = ((Pmspsa50TO) obj);
			} else {
				fetch(((Object[]) obj), localPmspsa50TO);
			}
			lastPmspsa50TO = localPmspsa50TO;
			recnum++;
		} else {
			if (rowIter != null && recnum >= pagesize - 1) {
				if (!cursorNoWhere) {
					Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
					status = openPmspsa50TOPosCsr(lastPmspsa50TO.getLocation(),
							lastPmspsa50TO.getMasterco(),
							lastPmspsa50TO.getSymbol(),
							lastPmspsa50TO.getPolicyno(),
							lastPmspsa50TO.getModule(),
							lastPmspsa50TO.getRecont(),
							lastPmspsa50TO.getReeffdte());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
								lastPmspsa50TOsave.getPolicyno(),
								lastPmspsa50TOsave.getSymbol(),
								lastPmspsa50TOsave.getMasterco(),
								lastPmspsa50TOsave.getModule(),
								lastPmspsa50TOsave.getUnitno(),
								lastPmspsa50TOsave.getCovseq(),
								lastPmspsa50TOsave.getTransseq(),
								lastPmspsa50TOsave.getReinseq())) {
							rowIter.next(); // Throw away record already
											// processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					pagenum++;
					status = openPmspsa50TOCsr(); // Open cursor to nowhere with
													// pagenum incremented to
													// get next page of rows
				}
				localPmspsa50TO = fetchNextSa50PosCrs(localPmspsa50TO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localPmspsa50TO = new Pmspsa50TO();
			}
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;

	}

	public Pmspsa50TO fetchNextSa50PosCursor(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowList == null) {
			if (lastPmspsa50TO == null) {
				pagenum = 0;
				status = openPmspsa50TOCsr();
			} else {
				if (lastPmspsa50TO.getDBAccessStatus().isSuccess()) {
					Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
					status = openPmspsa50TOPosCsr(lastPmspsa50TO.getLocation(),
							lastPmspsa50TO.getMasterco(),
							lastPmspsa50TO.getSymbol(),
							lastPmspsa50TO.getPolicyno(),
							lastPmspsa50TO.getModule(),
							lastPmspsa50TO.getRecont(),
							lastPmspsa50TO.getReeffdte());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
								lastPmspsa50TOsave.getPolicyno(),
								lastPmspsa50TOsave.getSymbol(),
								lastPmspsa50TOsave.getMasterco(),
								lastPmspsa50TOsave.getModule(),
								lastPmspsa50TOsave.getUnitno(),
								lastPmspsa50TOsave.getCovseq(),
								lastPmspsa50TOsave.getTransseq(),
								lastPmspsa50TOsave.getReinseq())) {
							rowIter.next(); // skip record already processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localPmspsa50TO = new Pmspsa50TO();
				}
			}
		}
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (Pmspsa50TO.class.isInstance(obj)) {
				localPmspsa50TO = ((Pmspsa50TO) obj);
			} else {
				fetchRecords(((Object[]) obj), localPmspsa50TO);
			}
			lastPmspsa50TO = localPmspsa50TO;
			recnum++;
		} else {
			if (rowIter != null && recnum >= pagesize - 1) {
				if (!cursorNoWhere) {
					Pmspsa50TO lastPmspsa50TOsave = lastPmspsa50TO;
					status = openPmspsa50TOPosCsr(lastPmspsa50TO.getLocation(),
							lastPmspsa50TO.getMasterco(),
							lastPmspsa50TO.getSymbol(),
							lastPmspsa50TO.getPolicyno(),
							lastPmspsa50TO.getModule(),
							lastPmspsa50TO.getRecont(),
							lastPmspsa50TO.getReeffdte());
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastPmspsa50TOsave.getLocation(),
								lastPmspsa50TOsave.getPolicyno(),
								lastPmspsa50TOsave.getSymbol(),
								lastPmspsa50TOsave.getMasterco(),
								lastPmspsa50TOsave.getModule(),
								lastPmspsa50TOsave.getUnitno(),
								lastPmspsa50TOsave.getCovseq(),
								lastPmspsa50TOsave.getTransseq(),
								lastPmspsa50TOsave.getReinseq())) {
							rowIter.next(); // Throw away record already
											// processed
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localPmspsa50TO = new Pmspsa50TO();
						localPmspsa50TO.setDBAccessStatus(status);
						return localPmspsa50TO;
					}
				} else {
					pagenum++;
					status = openPmspsa50TOCsr(); // Open cursor to nowhere with
													// pagenum incremented to
													// get next page of rows
				}
				localPmspsa50TO = fetchNextSa50PosCursor(localPmspsa50TO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localPmspsa50TO = new Pmspsa50TO();
			}
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;

	}

	public DBAccessStatus openClcursor72(String symbol, String policyno,
			String module, String masterco, String location) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession(
					"PMSPSA50");
			Query query = session.getNamedQuery("Pmspsa50DAO_openClcursor72");
			query.setString("location", location);
			query.setString("module", module);
			query.setString("masterco", masterco);
			query.setString("policyno", policyno);
			query.setString("symbol", symbol);
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				status.setSqlCode(DBAccessStatus.DB_OK);
			}else{
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}

	public Pmspsa50TO fetchNextPmspsa50(Pmspsa50TO pmspsa50TO) {
		DBAccessStatus status = new DBAccessStatus();
		Pmspsa50TO localPmspsa50TO = pmspsa50TO;
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (Pmspsa50TO.class.isInstance(obj)) {
				localPmspsa50TO = ((Pmspsa50TO) obj);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localPmspsa50TO = new Pmspsa50TO();
			localPmspsa50TO.setDBAccessStatus(status);
		}
		localPmspsa50TO.setDBAccessStatus(status);
		return localPmspsa50TO;
	}

	public DBAccessStatus closePmspsa50() {
		DBAccessStatus status = new DBAccessStatus();
		if (rowList != null) {
			rowList.clear();
			rowList = null;
			rowIter = null;
		}
		return status;
	}
	
	public boolean checkReinsRecord(String location, String policyno, String symbol, String masterco, String module,
			String unitno, short covseq, short transseq) {
			boolean isReinsRecordFound = false;
			DBAccessStatus status = new DBAccessStatus();
			List sa50List ;
		try{
			Session session = HibernateSessionFactory.current().getSession("PMSPSA50");
			Query query = session.getNamedQuery("Pmspsa50TO_CHECKREINSRCD");
			query.setParameter("location", location);
			query.setParameter("policyno", policyno);
			query.setParameter("symbol", symbol);
			query.setParameter("masterco", masterco);
			query.setParameter("module", module);
			query.setParameter("unitno", unitno);
			query.setParameter("covseq", covseq);
			query.setParameter("transseq", transseq);

			query.setMaxResults(1);
			sa50List = query.list();

			if(sa50List.isEmpty()) {
				status = new DBAccessStatus(DBAccessStatus.DB_EOF);
				isReinsRecordFound = false;

			} else {
				status = new DBAccessStatus(DBAccessStatus.DB_OK);
					isReinsRecordFound = true;
			}

		}
		catch (HibernateException ex)
		{
			status.setException(ex);
		}
		return isReinsRecordFound;
	}
	
}
