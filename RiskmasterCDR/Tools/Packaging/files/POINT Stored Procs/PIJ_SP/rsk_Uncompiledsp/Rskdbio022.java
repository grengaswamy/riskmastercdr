package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DataExtConverter;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.c2ab.util.SignType;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.common.Baspprenst;
import com.csc.pt.svc.ctrl.to.Pmsp0200RecTO;
import com.csc.pt.svc.data.dao.Asbjcpl2DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.to.Asbjcpl2TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.rsk.BcListEntryArr9;
import com.csc.pt.svc.db.to.Asbjcpl2RecTO;
import com.csc.pt.svc.rsk.dao.Asb1cppDAO;
import com.csc.pt.svc.rsk.to.Rskdbio022TO;
import com.csc.pt.svc.isl.Cobolsqlca;

/**

*/

public class Rskdbio022 {
	private static final String PROGRAM_ID = "RSKDBIO022";
	public Asbjcpl2RecTO asbjcpl2RecTO = new Asbjcpl2RecTO();
	public Pmsp0200RecTO pmsp0200RecTO = new Pmsp0200RecTO();
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private IABODynamicArray<BcListEntryArr9> bcListEntryArr9 = ABOCollectionCreator.getReferenceFactory(BcListEntryArr9.class).createDynamicArray(1);
	private int numOfEntrys;
	private int numOfEntrysForSql;
	private String ws1FileStatus = "";
	public static final String WS_GOOD_RETURN = "00";
	private int wsCount;
	private short wsWorkC;
	private short wsWorkYy;
	private short wsWorkMm;
	private short wsWorkDd;
	//FSIT: 152640, Resolution: 55204 - Begin
	//private ABODecimal wsEditMaskCurr = new ABODecimal(11, 2);
	private ABODecimal wsEditMaskCurr = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private short wsEditMaskMm;
	private char rskdbio022filler1 = '/';
	private short wsEditMaskDd;
	private char rskdbio022filler2 = '/';
	private short wsEditMaskYy;
	private Rskdbio022TO rskdbio022TO = new Rskdbio022TO();
	private Asbjcpl2DAO asbjcpl2DAO = new Asbjcpl2DAO();
	private Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	private Asbjcpl2TO asbjcpl2TO = new Asbjcpl2TO();

	private Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	//FSIT # 119900 Res # 47654 Start
	private int wsRenewMod;
	private String wsLocationCo;
	private String wsMasterCo;
	private String wsPolSymbol;
	private String wsPolicyNum;
	private String wsPolModule;
	private String wsInsuranceLine; 
	private int wsRiskLocNo; 
	private int wsRiskLocSubNo ;
	private String wsProduct; 
	private String wsRateState;
	private int wsUnitNo; 
	private String wsCoverage; 
	private int wsCoverageSequence ; 
	private int wsDriverId ; 
	private String wsFileName; 
	private char wsBatype0Act; 
	
	//FSIT # 119900 Res # 47654 End

	public Rskdbio022() {
		bcListEntryArr9.assign((1) - 1, new BcListEntryArr9());
	}

	public void mainSubroutine() {
		programRskdbio022();
		exit();
	}

	public void programRskdbio022() {
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		asbjcpl2RecTO.initSpaces();
		asbjcpl2RecTO.fbjcpemRec.setBjabcd(rskdbio022TO.getInMaster0co());
		asbjcpl2RecTO.fbjcpemRec.setBjaacd(rskdbio022TO.getInLocation());
		asbjcpl2RecTO.fbjcpemRec.setBjartx(rskdbio022TO.getInSymbol());
		asbjcpl2RecTO.fbjcpemRec.setBjastx(rskdbio022TO.getInPolicy0num());
		asbjcpl2RecTO.fbjcpemRec.setBjadnb(rskdbio022TO.getInModule());
		//CSC-Manual Start
		asbjcpl2RecTO.fbjcpemRec.setBjaenb(0 % 100000);
		/*cobolsqlca.setDBAccessStatus(asbjcpl2DAO.openAsbjcpl2TOCsr(KeyType.GREATER_OR_EQ, asbjcpl2RecTO.fbjcpemRec.getBjaacd(),
			asbjcpl2RecTO.fbjcpemRec.getBjabcd(), asbjcpl2RecTO.fbjcpemRec.getBjartx(), asbjcpl2RecTO.fbjcpemRec.getBjastx(),
			asbjcpl2RecTO.fbjcpemRec.getBjadnb(), asbjcpl2RecTO.fbjcpemRec.getBjaenb(), asbjcpl2RecTO.fbjcpemRec.getBjc6st()));*/
		cobolsqlca.setDBAccessStatus(asbjcpl2DAO.openAsbjcpl2TOCsrTree(asbjcpl2RecTO.fbjcpemRec.getBjaacd(),
				asbjcpl2RecTO.fbjcpemRec.getBjabcd(), asbjcpl2RecTO.fbjcpemRec.getBjartx(), asbjcpl2RecTO.fbjcpemRec.getBjastx(),
				asbjcpl2RecTO.fbjcpemRec.getBjadnb(), asbjcpl2RecTO.fbjcpemRec.getBjaenb()));
		//CSC-Manual End
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		numOfEntrys = 0 % 100000;
		initBcListTableSpaces();
		rng2000ReadAsbjcpl2();
		cobolsqlca.setDBAccessStatus(asbjcpl2DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		cobolsqlca.setDBAccessStatus(pmsp0200DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		//
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(bcListEntryArr9, numOfEntrysForSql);
	}

	public void exit() {
		throw new ReturnException();
	}

	public String readAsbjcpl2() {
		asbjcpl2TO = asbjcpl2DAO.fetchNext(asbjcpl2TO);
		asbjcpl2RecTO.setData(asbjcpl2TO.getData());
		//CSC-Manual Start
		//asbjcpl2RecTO.setData(asbjcpl2RecTO.getData());
		//CSC-Manual End
		ws1FileStatus = Functions.subString(asbjcpl2TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (asbjcpl2TO.getDBAccessStatus().isEOF()) {
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			return "";
		}
		if (rskdbio022TO.getInMaster0co().compareTo(asbjcpl2RecTO.fbjcpemRec.getBjabcd()) != 0
			|| rskdbio022TO.getInLocation().compareTo(asbjcpl2RecTO.fbjcpemRec.getBjaacd()) != 0
			|| rskdbio022TO.getInSymbol().compareTo(asbjcpl2RecTO.fbjcpemRec.getBjartx()) != 0
			|| rskdbio022TO.getInPolicy0num().compareTo(asbjcpl2RecTO.fbjcpemRec.getBjastx()) != 0
			|| rskdbio022TO.getInModule().compareTo(asbjcpl2RecTO.fbjcpemRec.getBjadnb()) != 0) {
			return "";
		} else {
			if (asbjcpl2RecTO.fbjcpemRec.getBjbatx().compareTo("OSE") == 0) {
				return "2000-READ-ASBJCPL2";
			}
		}
		rng5000ProcessEntry();
		// DISPLAY BC-UNIT-NUMBER(WS-COUNT) " "
		// BC-STATUS(WS-COUNT) " "
		// BC-LAST-CHANGE-DATE(WS-COUNT) " "
		// BC-RATE-PREMIUM(WS-COUNT) " "
		// BC-MAKE-YEAR(WS-COUNT) " "
		// BC-MAKE-DESCRIPTION(WS-COUNT) " "
		// BC-PRODUCT(WS-COUNT) " "
		// BC-SERIAL(WS-COUNT).
		// DISPLAY BC-UNIT-NUMBER(WS-COUNT) " "
		// BC-STATUS(WS-COUNT) " "
		// BC-LAST-CHANGE-DATE(WS-COUNT) " "
		// BC-RATE-PREMIUM(WS-COUNT) " "
		// BC-MAKE-YEAR(WS-COUNT) " "
		// BC-MAKE-DESCRIPTION(WS-COUNT) " "
		// BC-PRODUCT(WS-COUNT) " "
		// BC-SERIAL(WS-COUNT).
		return "2000-READ-ASBJCPL2";
	}

	public void exitAsbjcpl2() {
	// exit
	}

	public void processEntry() {
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		getBcListEntryArr9(wsCount).setBcUnitNumber(ABOSystem.convertUsingString(asbjcpl2RecTO.fbjcpemRec.getBjaenb(), "S99999"));
		getBcListEntryArr9(wsCount).setBcProduct(asbjcpl2RecTO.fbjcpemRec.getBjantx());
		getBcListEntryArr9(wsCount).setBcSerial(asbjcpl2RecTO.fbjcpemRec.getBjaytx());
		setWsWorkDate(DataExtConverter.intToBytes(asbjcpl2RecTO.fbjcpemRec.getBjaldt(), 7, SignType.SIGNED_TRAILING));
		wsEditMaskYy = wsWorkYy;
		wsEditMaskMm = wsWorkMm;
		wsEditMaskDd = wsWorkDd;
		getBcListEntryArr9(wsCount).setBcLastChangeDate(getWsEditMaskDateAsStr());
		wsEditMaskCurr.assign(asbjcpl2RecTO.fbjcpemRec.getBja3va());
		getBcListEntryArr9(wsCount).setBcRatePremium(getWsEditMaskCurrFormatted());
		getBcListEntryArr9(wsCount).setBcMakeYear(ABOSystem.convertUsingString(asbjcpl2RecTO.fbjcpemRec.getBjahnb(), "S9999"));
		getBcListEntryArr9(wsCount).setBcMakeDescription(asbjcpl2RecTO.fbjcpemRec.getBjaxtx());
		getBcListEntryArr9(wsCount).setBcInsLine(asbjcpl2RecTO.fbjcpemRec.getBjagtx());
		getBcListEntryArr9(wsCount).setBcRiskLoc(ABOSystem.convertUsingString(asbjcpl2RecTO.fbjcpemRec.getBjbrnb(), "S99999"));
		getBcListEntryArr9(wsCount).setBcRiskSubLoc(ABOSystem.convertUsingString(asbjcpl2RecTO.fbjcpemRec.getBjegnb(), "S99999"));
		getBcListEntryArr9(wsCount).setBcRateState(asbjcpl2RecTO.fbjcpemRec.getBjbccd());
		if (asbjcpl2RecTO.fbjcpemRec.getBjc6st() == 'V') {
		//FSIT # 119900 Res # 47654 Start
			InitializeWsPreRenewalRec();
			wsUnitNo = asbjcpl2RecTO.fbjcpemRec.getBjaenb();
			wsFileName = "ASBJCPL2";
			CallPgmChkNxtRecord();
			if(wsBatype0Act == 'C'){		
				getBcListEntryArr9(wsCount).setBcStatus("Verified-R");		
			}else{
				getBcListEntryArr9(wsCount).setBcStatus("Verified");				
			}
		//FSIT # 119900 Res # 47654 End
		} else {
			if (asbjcpl2RecTO.fbjcpemRec.getBjc6st() == 'P') {
				if (asbjcpl2RecTO.fbjcpemRec.getBjc7st() == 'Y') {
					getBcListEntryArr9(wsCount).setBcStatus("Drop Pending");
				} else {
					getBcListEntryArr9(wsCount).setBcStatus("Pending");
				}
			} else {
				if (asbjcpl2RecTO.fbjcpemRec.getBjc6st() == 'D') {
					getBcListEntryArr9(wsCount).setBcStatus("Drop");
				} else {
					if (asbjcpl2RecTO.fbjcpemRec.getBjc6st() == 'H') {
						getBcListEntryArr9(wsCount).setBcStatus("History");
					}
				}
			}
		}
		rng6000ReadPmsp0200IssueCode();
		//RSK - START
		readAsb1cpp();
		//RSK - END
	}
	
	//RSK - Start
	public void readAsb1cpp(){
		getBcListEntryArr9(wsCount).setBcStatUnit(Integer.toString(asbjcpl2RecTO.fbjcpemRec.getBjaenb()));
		String unit = Asb1cppDAO.fetchUnit(asbjcpl2RecTO.fbjcpemRec.getBjaacd(), asbjcpl2RecTO.fbjcpemRec.getBjabcd(), asbjcpl2RecTO.fbjcpemRec.getBjartx(), 
				asbjcpl2RecTO.fbjcpemRec.getBjastx(), asbjcpl2RecTO.fbjcpemRec.getBjadnb(), asbjcpl2RecTO.fbjcpemRec.getBjagtx(), 
				asbjcpl2RecTO.fbjcpemRec.getBjbrnb(), asbjcpl2RecTO.fbjcpemRec.getBjegnb(), asbjcpl2RecTO.fbjcpemRec.getBjantx(), asbjcpl2RecTO.fbjcpemRec.getBjaenb());
		if (unit != null){
			getBcListEntryArr9(wsCount).setBcStatUnit(unit);
		}
	}
	//RSK - End

	public void readPmsp0200IssueCode() {
		pmsp0200RecTO.rec02fmtRec.setId02("02");
		pmsp0200RecTO.rec02fmtRec.setLocation(rskdbio022TO.getInLocation());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(rskdbio022TO.getInPolicy0num());
		pmsp0200RecTO.rec02fmtRec.setSymbol(rskdbio022TO.getInSymbol());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(rskdbio022TO.getInMaster0co());
		pmsp0200RecTO.rec02fmtRec.setModule(rskdbio022TO.getInModule());
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(' ');
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(pmsp0200DAO.openPmsp0200TOCsr(KeyType.EQUAL, pmsp0200RecTO.rec02fmtRec.getId02(),				
			pmsp0200RecTO.rec02fmtRec.getLocation(), pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(),
			pmsp0200RecTO.rec02fmtRec.getMaster0co(), pmsp0200RecTO.rec02fmtRec.getModule(), pmsp0200RecTO.rec02fmtRec.getTrans0stat()));*/
		cobolsqlca.setDBAccessStatus(pmsp0200DAO.openPmsp0200TOCsrModule(pmsp0200RecTO.rec02fmtRec.getId02(),				
				pmsp0200RecTO.rec02fmtRec.getLocation(), pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(),
				pmsp0200RecTO.rec02fmtRec.getMaster0co(), pmsp0200RecTO.rec02fmtRec.getModule()));
		//CSC-Manual End	
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		pmsp0200TO = pmsp0200DAO.fetchNext(pmsp0200TO);
		pmsp0200RecTO.setData(pmsp0200TO.getData());
		ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
			// continue
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			getBcListEntryArr9(wsCount).setBcIssueCode(pmsp0200RecTO.rec02fmtRec.getIssue0code());
		}
	}

	public void rng2000ReadAsbjcpl2() {
		String retcode = "";
		boolean goto2000ReadAsbjcpl2 = false;
		do {
			goto2000ReadAsbjcpl2 = false;
			retcode = readAsbjcpl2();
		} while (retcode.compareTo("2000-READ-ASBJCPL2") == 0);
		exitAsbjcpl2();
	}

	public void rng5000ProcessEntry() {
		processEntry();
	}

	public void rng6000ReadPmsp0200IssueCode() {
		readPmsp0200IssueCode();
	}

	public SPResultSetTO runSP(Rskdbio022TO rskdbio022TO) {
		this.rskdbio022TO = rskdbio022TO;
		run();
		return resultSetTO;
	}

	public SPResultSetTO runSP(byte[] dynamic_inputParameters) {
		rskdbio022TO.setInputParameters(dynamic_inputParameters);
		return runSP(rskdbio022TO);
	}

	public Rskdbio022TO run(Rskdbio022TO rskdbio022TO) {
		this.rskdbio022TO = rskdbio022TO;
		run();
		return this.rskdbio022TO;
	}

	public void run(byte[] dynamic_inputParameters) {
		if (dynamic_inputParameters != null) {
			rskdbio022TO.setInputParameters(dynamic_inputParameters);
		}
		run(rskdbio022TO);
		if (dynamic_inputParameters != null) {
			DataConverter.arrayCopy(rskdbio022TO.getInputParameters(), rskdbio022TO.getInputParametersSize(), dynamic_inputParameters, 1);
		}
	}

	public BcListEntryArr9 getBcListEntryArr9(int index) {
		if (index > bcListEntryArr9.size()) {
			// for an element beyond current array limits, all intermediate
			// items will be added with default elements;
			bcListEntryArr9.ensureCapacity(index);
			for (int i = bcListEntryArr9.size() + 1; i <= index; i++) {
				bcListEntryArr9.append(new BcListEntryArr9());
				// assign default value
			}
		}
		return bcListEntryArr9.get((index) - 1);
	}

	public static Rskdbio022 getInstance() {
		Rskdbio022 iRskdbio022 = ((Rskdbio022) CalledProgList.getInst("RSKDBIO022"));
		if (iRskdbio022 == null) {
			// create an instance if needed
			iRskdbio022 = new Rskdbio022();
			CalledProgList.addInst("RSKDBIO022", ((Object) iRskdbio022));
		}
		return iRskdbio022;
	}

	public void run() {
		try {
			mainSubroutine();
		} catch (ReturnException re) {
			// normal termination of the program
		}
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	public int getBcListTableSize() {
		return 55500;
	}

	public byte[] getBcListTable() {
		byte[] buf = new byte[getBcListTableSize()];
		int offset = 1;
		for (int idx = 1; idx <= bcListEntryArr9.size(); idx++) {
			DataConverter.arrayCopy(bcListEntryArr9.get((idx) - 1).getData(), 1, BcListEntryArr9.getSize(), buf, offset);
			offset += BcListEntryArr9.getSize();
		}
		if (bcListEntryArr9.size() < 500) {
			byte[] rowBuf = DataConverter.fillEndMarker(BcListEntryArr9.getSize());
			for (int idx = bcListEntryArr9.size() + 1; idx <= 500; idx++) {
				DataConverter.arrayCopy(rowBuf, BcListEntryArr9.getSize(), buf, offset);
				offset += BcListEntryArr9.getSize();
			}
		}
		return buf;
	}

	public void initBcListTableSpaces() {
		for (int idx = 1; idx <= bcListEntryArr9.size(); idx++) {
			if (bcListEntryArr9.get((idx) - 1) != null) {
				bcListEntryArr9.get((idx) - 1).initSpaces();
			}
		}
	}

	public int getWsWorkDateSize() {
		return 7;
	}

	public byte[] getWsWorkDate() {
		byte[] buf = new byte[getWsWorkDateSize()];
		int offset = 1;
		DataConverter.writeShort(buf, offset, wsWorkC, 1);
		offset += 1;
		DataConverter.writeShort(buf, offset, wsWorkYy, 2);
		offset += 2;
		DataConverter.writeShort(buf, offset, wsWorkMm, 2);
		offset += 2;
		DataConverter.writeShort(buf, offset, wsWorkDd, 2);
		return buf;
	}

	public void setWsWorkDate(byte[] buf) {
		int offset = 1;
		wsWorkC = DataConverter.readShort(buf, offset, 1);
		offset += 1;
		wsWorkYy = DataConverter.readShort(buf, offset, 2);
		offset += 2;
		wsWorkMm = DataConverter.readShort(buf, offset, 2);
		offset += 2;
		wsWorkDd = DataConverter.readShort(buf, offset, 2);
	}

	public int getWsEditMaskDateSize() {
		return 8;
	}

	public byte[] getWsEditMaskDate() {
		byte[] buf = new byte[getWsEditMaskDateSize()];
		int offset = 1;
		DataConverter.writeShort(buf, offset, wsEditMaskMm, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, rskdbio022filler1);
		offset += 1;
		DataConverter.writeShort(buf, offset, wsEditMaskDd, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, rskdbio022filler2);
		offset += 1;
		DataConverter.writeShort(buf, offset, wsEditMaskYy, 2);
		return buf;
	}

	public String getWsEditMaskDateAsStr() {
		return DataConverter.readString(getWsEditMaskDate(), 1, getWsEditMaskDate().length);
	}

	public String getWsEditMaskCurrFormatted() {
		//FSIT: 152640, Resolution: 55204 - Begin
		//return ABOSystem.convertUsingString(wsEditMaskCurr.clone(), "-ZZZ,ZZZ,ZZ9.99");
		return ABOSystem.convertUsingString(wsEditMaskCurr.clone(), "-ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
		// FSIT: 152640 Resolution : 55204- End
	}
	
	//FSIT # 119900 Res # 47654 Start
	/*method CallPgmChkNxtRecord is used to call program BASPPRENST which is reading next pending record
	from the file passed to this program as parameter and returns activity type into wsBatype0Act               */
	public void CallPgmChkNxtRecord(){
		Baspprenst baspprenst;
		wsRenewMod=0;
		wsLocationCo=rskdbio022TO.getInLocation();
		wsMasterCo=rskdbio022TO.getInMaster0co();
		wsPolSymbol=rskdbio022TO.getInSymbol();
		wsPolicyNum =rskdbio022TO.getInPolicy0num();
		if(Functions.isNumber(rskdbio022TO.getInModule())){
			wsRenewMod= Integer.valueOf(rskdbio022TO.getInModule()).intValue();
		}
		wsRenewMod= wsRenewMod+1;
		wsPolModule = new Integer(wsRenewMod).toString();
		baspprenst = Baspprenst.getInstance();
		setWsPreRenewalrec(baspprenst.run(getWsPreRenewalrec()));		                            
	}
	
	public void InitializeWsPreRenewalRec(){
		wsLocationCo = "";
		wsMasterCo = "";
		wsPolSymbol = "";
		wsPolicyNum = "";
		wsPolModule = "";
		wsInsuranceLine = ""; 
		wsRiskLocNo = 0; 
		wsRiskLocSubNo = 0;
		wsProduct = ""; 
		wsRateState = "";
		wsUnitNo = 0; 
		wsCoverage = ""; 
		wsCoverageSequence = 0 ; 
		wsDriverId =0; 
		wsFileName = ""; 
		wsBatype0Act = ' ';
	}
	
	public void setWsPreRenewalrec(byte[] buf){
		int offset = 1;
		wsLocationCo  = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsMasterCo = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsPolSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		wsPolModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsInsuranceLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsRiskLocNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsRiskLocSubNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsProduct  = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsRateState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsUnitNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsCoverage = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsCoverageSequence = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsDriverId = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsFileName = DataConverter.readString(buf, offset, 10);
		offset += 10;
		wsBatype0Act = DataConverter.readChar(buf, offset);		
	}
	
	public byte[] getWsPreRenewalrec() {
		byte[] buf = new byte[50];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsLocationCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsMasterCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsPolSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, wsPolModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsInsuranceLine, 3);
		offset += 3;
		DataConverter.writeInt(buf, offset, wsRiskLocNo, 1);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsRiskLocSubNo, 1);
		offset += 1;
		DataConverter.writeString(buf, offset, wsProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsRateState, 2);
		offset += 2;
		DataConverter.writeInt(buf, offset, wsUnitNo, 1);
		offset += 1;
		DataConverter.writeString(buf, offset, wsCoverage, 6);
		offset += 6;
		DataConverter.writeInt(buf, offset, wsCoverageSequence, 1);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsDriverId, 1);
		offset += 1;
		DataConverter.writeString(buf, offset, wsFileName, 10);
		offset += 10;
		DataConverter.writeChar(buf, offset, wsBatype0Act);
		return buf;
	}
	//FSIT # 119900 Res # 47654 End
}
