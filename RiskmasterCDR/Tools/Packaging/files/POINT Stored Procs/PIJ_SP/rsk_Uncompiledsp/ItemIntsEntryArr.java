// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;

public class ItemIntsEntryArr implements Visitable {
	private String piIntType = "";
	private String piIntLocation = "";
	private String piIntSeq = "";
	private String piName = "";
	private String piAddress = "";
	private String piCity = "";
	private String piState = "";
	private char piIssueCode = ' ';
	private long piClientSeq      = 0;
	private int piAddressSeq     = 0;
	private String piNumOfRecords = "";

	public void setPiIntType(String piIntType) {
		this.piIntType = piIntType;
	}

	public String getPiIntType() {
		return this.piIntType;
	}

	public void setPiIntLocation(String piIntLocation) {
		this.piIntLocation = piIntLocation;
	}

	public String getPiIntLocation() {
		return this.piIntLocation;
	}

	public void setPiIntSeq(String piIntSeq) {
		this.piIntSeq = piIntSeq;
	}

	public String getPiIntSeq() {
		return this.piIntSeq;
	}

	public void setPiName(String piName) {
		this.piName = piName;
	}

	public String getPiName() {
		return this.piName;
	}

	public void setPiAddress(String piAddress) {
		this.piAddress = piAddress;
	}

	public String getPiAddress() {
		return this.piAddress;
	}

	public void setPiCity(String piCity) {
		this.piCity = piCity;
	}

	public String getPiCity() {
		return this.piCity;
	}

	public void setPiState(String piState) {
		this.piState = piState;
	}

	public String getPiState() {
		return this.piState;
	}

	public void setPiIssueCode(char piIssueCode) {
		this.piIssueCode = piIssueCode;
	}

	public char getPiIssueCode() {
		return this.piIssueCode;
	}
	
	public void setPiClientSeq(long piClientSeq)
	{
		this.piClientSeq = piClientSeq;
	}
	
	public long getPiClientSeq()
	{
		return this.piClientSeq;
	}
	
	public void setPiAddressSeq(int piAddressSeq)
	{
		this.piAddressSeq = piAddressSeq;
	}
	
	public int getPiAddressSeq()
	{
		return this.piAddressSeq;
	}

	public void setPiNumOfRecords(String piNumOfRecords) {
		this.piNumOfRecords = piNumOfRecords;
	}

	public String getPiNumOfRecords() {
		return this.piNumOfRecords;
	}

	public void accept(Visitor visitor) {
		visitor.visitString("PI-INT-TYPE", piIntType);
		visitor.visitString("PI-INT-LOCATION", piIntLocation);
		visitor.visitString("PI-INT-SEQ", piIntSeq);
		visitor.visitString("PI-NAME", piName);
		visitor.visitString("PI-ADDRESS", piAddress);
		visitor.visitString("PI-CITY", piCity);
		visitor.visitString("PI-STATE", piState);
		visitor.visitChar("PI-ISSUE-CODE", piIssueCode);
		visitor.visitLong("PI-CLIENT-SEQ", piClientSeq);
		visitor.visitInt("PI-ADDRESS-SEQ", piAddressSeq);
		visitor.visitString("PI-NUM-OF-RECORDS", piNumOfRecords);
	}

	public static int getSize() {
		return 120;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, piIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, piIntLocation, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, piIntSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, piName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, piAddress, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, piCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, piState, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, piIssueCode);
		offset += 1;
		DataConverter.writeLong(buf, offset, piClientSeq, 10);
		offset +=10;
		DataConverter.writeInt(buf, offset, piAddressSeq, 2);
		offset +=2;
		DataConverter.writeString(buf, offset, piNumOfRecords, 5);
		offset += 5;
		assert (offset - 1 == buf.length) : offset + "-1 != " + buf.length;
		return buf;
	}
}
