package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;

public class UnitEntryArr implements Visitable {
	private String unInsLineDesc = "";
	private String unRateState = "";
	private String unProduct = "";
	private String unUnitNumber = "";
	private String unUnitDesc = "";
	private String unClassCode = "";
	private String unRatePremium = "";
	private String unChgEffDate = "";
	private String unRecStatusDesc = "";
	private char unIssueCode = ' ';
	private String unNumOfRecords = "";
	private String unRiskLocation = "";
	private String unSubLocation = "";
	private String unUnitYear = "";
	private String unUnitVin = "";
	private char unSwUnit = ' ';
	private char unSwLocation = ' ';
	private char unSwSubLocation = ' ';
	private String unTotalPremium = "";
	private String unStatUnit = "";
	private String unSumdesc = "";

	public String getUnStatUnit() {
		return unStatUnit;
	}

	public void setUnStatUnit(String unStatUnit) {
		this.unStatUnit = unStatUnit;
	}

	public String getUnSumdesc() {
		return unSumdesc;
	}

	public void setUnSumdesc(String unSumdesc) {
		this.unSumdesc = unSumdesc;
	}

	public void setUnInsLineDesc(String unInsLineDesc) {
		this.unInsLineDesc = unInsLineDesc;
	}

	public String getUnInsLineDesc() {
		return this.unInsLineDesc;
	}

	public void setUnRateState(String unRateState) {
		this.unRateState = unRateState;
	}

	public String getUnRateState() {
		return this.unRateState;
	}

	public void setUnProduct(String unProduct) {
		this.unProduct = unProduct;
	}

	public String getUnProduct() {
		return this.unProduct;
	}

	public void setUnUnitNumber(String unUnitNumber) {
		this.unUnitNumber = unUnitNumber;
	}

	public String getUnUnitNumber() {
		return this.unUnitNumber;
	}

	public void setUnUnitDesc(String unUnitDesc) {
		this.unUnitDesc = unUnitDesc;
	}

	public String getUnUnitDesc() {
		return this.unUnitDesc;
	}

	public void setUnClassCode(String unClassCode) {
		this.unClassCode = unClassCode;
	}

	public String getUnClassCode() {
		return this.unClassCode;
	}

	public void setUnRatePremium(String unRatePremium) {
		this.unRatePremium = unRatePremium;
	}

	public String getUnRatePremium() {
		return this.unRatePremium;
	}

	public void setUnChgEffDate(String unChgEffDate) {
		this.unChgEffDate = unChgEffDate;
	}

	public String getUnChgEffDate() {
		return this.unChgEffDate;
	}

	public void setUnRecStatusDesc(String unRecStatusDesc) {
		this.unRecStatusDesc = unRecStatusDesc;
	}

	public String getUnRecStatusDesc() {
		return this.unRecStatusDesc;
	}

	public void setUnIssueCode(char unIssueCode) {
		this.unIssueCode = unIssueCode;
	}

	public char getUnIssueCode() {
		return this.unIssueCode;
	}

	public void setUnNumOfRecords(String unNumOfRecords) {
		this.unNumOfRecords = unNumOfRecords;
	}

	public String getUnNumOfRecords() {
		return this.unNumOfRecords;
	}

	public void setUnRiskLocation(String unRiskLocation) {
		this.unRiskLocation = unRiskLocation;
	}

	public String getUnRiskLocation() {
		return this.unRiskLocation;
	}

	public void setUnSubLocation(String unSubLocation) {
		this.unSubLocation = unSubLocation;
	}

	public String getUnSubLocation() {
		return this.unSubLocation;
	}

	public void setUnUnitYear(String unUnitYear) {
		this.unUnitYear = unUnitYear;
	}

	public String getUnUnitYear() {
		return this.unUnitYear;
	}

	public void setUnUnitVin(String unUnitVin) {
		this.unUnitVin = unUnitVin;
	}

	public String getUnUnitVin() {
		return this.unUnitVin;
	}

	public void setUnSwUnit(char unSwUnit) {
		this.unSwUnit = unSwUnit;
	}

	public char getUnSwUnit() {
		return this.unSwUnit;
	}

	public void setUnSwLocation(char unSwLocation) {
		this.unSwLocation = unSwLocation;
	}

	public char getUnSwLocation() {
		return this.unSwLocation;
	}

	public void setUnSwSubLocation(char unSwSubLocation) {
		this.unSwSubLocation = unSwSubLocation;
	}

	public char getUnSwSubLocation() {
		return this.unSwSubLocation;
	}

	public void setUnTotalPremium(String unTotalPremium) {
		this.unTotalPremium = unTotalPremium;
	}

	public String getUnTotalPremium() {
		return this.unTotalPremium;
	}

	public void accept(Visitor visitor) {
		visitor.visitString("UN-INS-LINE-DESC", unInsLineDesc);
		visitor.visitString("UN-RATE-STATE", unRateState);
		visitor.visitString("UN-PRODUCT", unProduct);
		visitor.visitString("UN-UNIT-NUMBER", unUnitNumber);
		visitor.visitString("UN-UNIT-DESC", unUnitDesc);
		visitor.visitString("UN-CLASS-CODE", unClassCode);
		visitor.visitString("UN-RATE-PREMIUM", unRatePremium);
		visitor.visitString("UN-CHG-EFF-DATE", unChgEffDate);
		visitor.visitString("UN-REC-STATUS-DESC", unRecStatusDesc);
		visitor.visitChar("UN-ISSUE-CODE", unIssueCode);
		visitor.visitString("UN-NUM-OF-RECORDS", unNumOfRecords);
		visitor.visitString("UN-RISK-LOCATION", unRiskLocation);
		visitor.visitString("UN-SUB-LOCATION", unSubLocation);
		visitor.visitString("UN-UNIT-YEAR", unUnitYear);
		visitor.visitString("UN-UNIT-VIN", unUnitVin);
		visitor.visitChar("UN-SW-UNIT", unSwUnit);
		visitor.visitChar("UN-SW-LOCATION", unSwLocation);
		visitor.visitChar("UN-SW-SUB-LOCATION", unSwSubLocation);
		visitor.visitString("UN-TOTAL-PREMIUM", unTotalPremium);
		visitor.visitString("UN-STAT-UNIT", unStatUnit);
		visitor.visitString("UN-SUMDESC", unSumdesc);
	}

	public static int getSize() {
		return 274;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, unInsLineDesc, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, unRateState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, unProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, unUnitNumber, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, unUnitDesc, 60);
		offset += 60;
		DataConverter.writeString(buf, offset, unClassCode, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, unRatePremium, 12);
		offset += 12;
		DataConverter.writeString(buf, offset, unChgEffDate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, unRecStatusDesc, 20);
		offset += 20;
		DataConverter.writeChar(buf, offset, unIssueCode);
		offset += 1;
		DataConverter.writeString(buf, offset, unNumOfRecords, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, unRiskLocation, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, unSubLocation, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, unUnitYear, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, unUnitVin, 30);
		offset += 30;
		DataConverter.writeChar(buf, offset, unSwUnit);
		offset += 1;
		DataConverter.writeChar(buf, offset, unSwLocation);
		offset += 1;
		DataConverter.writeChar(buf, offset, unSwSubLocation);
		offset += 1;
		DataConverter.writeString(buf, offset, unTotalPremium, 16);
		offset += 16;
		DataConverter.writeString(buf, offset, unStatUnit, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, unSumdesc, 50);
		offset += 50;
		return buf;
	}
}