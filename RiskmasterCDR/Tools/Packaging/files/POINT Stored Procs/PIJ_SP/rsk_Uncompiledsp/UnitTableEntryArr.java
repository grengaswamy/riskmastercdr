// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.SignType;
import java.math.BigDecimal;

public class UnitTableEntryArr implements Visitable {
	private String policyno = "";
	private String symbol   = "";
	private String lob           = "";
	private String unitno        = "";
	private String covseq       = "";
	private String transseq      = "";
	private String subline       = "";
	private String sublindesc    = "";
	private String classnum      = "";
	private String majperil      = "";
	private String majperildesc1 = "";
	private String coveffdte     = "";
	private String covexpdte     = "";
	private String insline       = "";
	private String transdte      = "";
	private String entrydte      = "";
	private String acctgdte   = "";
	private String retrodte   = "";
	private String extenddte  = "";
	private String state      = "";
	private String asline     = "";
	private String prodline   = "";
	private String exposure   = "";
	private String limitoccur = "";
	private String limitagger = "";
	private String deductible = "";
	private char covstatus  = ' ';
	private BigDecimal totalorig  = BigDecimal.ZERO;
	private BigDecimal original   = BigDecimal.ZERO;
	private BigDecimal premium    = BigDecimal.ZERO;
	private BigDecimal totalprem  = BigDecimal.ZERO;
	private String product    = "";
	private String classdesc  = "";
	private BigDecimal limitCovA  = BigDecimal.ZERO;
	private BigDecimal limitCovB  = BigDecimal.ZERO;
	private BigDecimal limitCovC  = BigDecimal.ZERO;
	private BigDecimal limitCovD  = BigDecimal.ZERO;
	private BigDecimal limitCovE  = BigDecimal.ZERO;
	private BigDecimal limitCovF  = BigDecimal.ZERO;
	private long wc07DedAmt = 0;
	private long wc07DedAggr = 0;
	private char reinsind = ' ';



	@Override
	public void accept(Visitor visitor) {
		visitor.visitString("POLICYNO", policyno);
		visitor.visitString("SYMBOL", symbol);
		visitor.visitString("LOB", lob);
		visitor.visitString("UNITNO", unitno);
		visitor.visitString("COVSEQ", covseq);
		visitor.visitString("TRANSSEQ", transseq);
		visitor.visitString("SUBLINE", subline);
		visitor.visitString("SUBLINDESC", sublindesc);
		visitor.visitString("CLASSNUM", classnum);
		visitor.visitString("MAJPERIL", majperil);
		visitor.visitString("MAJPERILDESC1", majperildesc1);
		visitor.visitString("COVEFFDTE", coveffdte);
		visitor.visitString("COVEXPDTE", covexpdte);
		visitor.visitString("INSLINE", insline);
		visitor.visitString("TRANSDTE", transdte);
		visitor.visitString("ENTRYDTE", entrydte);
		visitor.visitString("ACCTGDTE", acctgdte);
		visitor.visitString("RETRODTE", retrodte);
		visitor.visitString("EXTENDDTE", extenddte);
		visitor.visitString("STATE", state);
		visitor.visitString("ASLINE", asline);
		visitor.visitString("PRODLINE", prodline);
		visitor.visitString("EXPOSURE", exposure);
		visitor.visitString("LIMITOCCUR", limitoccur);
		visitor.visitString("LIMITAGGER", limitagger);
		visitor.visitString("DEDUCTIBLE", deductible);
		visitor.visitChar("COVSTATUS", covstatus);
		visitor.visitBigDecimal("TOTALORIG", totalorig);
		visitor.visitBigDecimal("ORIGINAL", original);
		visitor.visitBigDecimal("PREMIUM", premium);
		visitor.visitBigDecimal("TOTALPREM", totalprem);
		visitor.visitString("PRODUCT", product);
		visitor.visitString("CLASSDESC", classdesc);
		visitor.visitBigDecimal("LIMIT-COVA", limitCovA);
		visitor.visitBigDecimal("LIMIT-COVB", limitCovB);
		visitor.visitBigDecimal("LIMIT-COVC", limitCovC);
		visitor.visitBigDecimal("LIMIT-COVD", limitCovD);
		visitor.visitBigDecimal("LIMIT-COVE", limitCovE);
		visitor.visitBigDecimal("LIMIT-COVF", limitCovF);
		visitor.visitLong("WC07-DED-AMT", wc07DedAmt);
		visitor.visitLong("WC07-DED-AGG", wc07DedAggr);
		visitor.visitChar("REINSIND", reinsind);

	}

	public static int getSize() {
		 return 591;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, policyno, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, symbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lob, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, unitno, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, covseq, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, transseq, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, subline, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, sublindesc, 40);
		offset += 40;
		DataConverter.writeString(buf, offset, classnum, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, majperil, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, majperildesc1, 49);
		offset += 49;
		DataConverter.writeString(buf, offset, coveffdte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, covexpdte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, insline, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, transdte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, entrydte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, acctgdte, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, retrodte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, extenddte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, state, 2);
		offset += 2;

		DataConverter.writeString(buf, offset, asline, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, prodline, 4);
		offset += 4;

		DataConverter.writeString(buf, offset, exposure, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, limitoccur, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, limitagger, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, deductible, 7);
		offset += 7;
		DataConverter.writeChar(buf, offset, covstatus);
		offset += 1;
		DataConverter.writeBigDecimal(buf, offset, totalorig,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, original,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, premium,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, totalprem,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeString(buf, offset, product, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, classdesc, 60);
		offset += 60;

		DataConverter.writeBigDecimal(buf, offset, limitCovA,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, limitCovB,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, limitCovC,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, limitCovD,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, limitCovE,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeBigDecimal(buf, offset, limitCovF,18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeLong(buf, offset, wc07DedAmt, 20, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeLong(buf, offset, wc07DedAggr, 20, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeChar(buf, offset, reinsind);
		offset += 1;
		return buf;
	}

	public void initSpaces() {
		policyno  = "";
		symbol   = "";
		lob      = "";
		unitno       = "";
		covseq       = "";
		transseq     = "";
		subline      = "";
		sublindesc   = "";
		classnum     = "";
		majperil     = "";
		majperildesc1 = "";
		coveffdte    = "";
		covexpdte    = "";
		insline      = "";
		transdte     = "";
		entrydte     = "";
		acctgdte  = "";
		retrodte  = "";
		extenddte = "";
		state     = "";
		exposure  = "";
		limitoccur= "";
		limitagger= "";
		deductible= "";
		covstatus = ' ';
		totalorig = BigDecimal.ZERO;
		original  = BigDecimal.ZERO;
		premium   = BigDecimal.ZERO;
		totalprem = BigDecimal.ZERO;
		product   = "";
		classdesc = "";
		reinsind = ' ';

	}

	public String getPolicyno() {
		return policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getUnitno() {
		return unitno;
	}

	public void setUnitno(String unitno) {
		this.unitno = unitno;
	}

	public String getCovseq() {
		return covseq;
	}

	public void setCovseq(String covseq) {
		this.covseq = covseq;
	}

	public String getTransseq() {
		return transseq;
	}

	public void setTransseq(String transseq) {
		this.transseq = transseq;
	}

	public String getSubline() {
		return subline;
	}

	public void setSubline(String subline) {
		this.subline = subline;
	}

	public String getSublindesc() {
		return sublindesc;
	}

	public void setSublindesc(String sublindesc) {
		this.sublindesc = sublindesc;
	}

	public String getClassnum() {
		return classnum;
	}

	public void setClassnum(String classnum) {
		this.classnum = classnum;
	}

	public String getMajperil() {
		return majperil;
	}

	public void setMajperil(String majperil) {
		this.majperil = majperil;
	}

	public String getMajperildesc1() {
		return majperildesc1;
	}

	public void setMajperildesc1(String majperildesc1) {
		this.majperildesc1 = majperildesc1;
	}

	public String getCoveffdte() {
		return coveffdte;
	}

	public void setCoveffdte(String coveffdte) {
		this.coveffdte = coveffdte;
	}

	public String getCovexpdte() {
		return covexpdte;
	}

	public void setCovexpdte(String covexpdte) {
		this.covexpdte = covexpdte;
	}

	public String getInsline() {
		return insline;
	}

	public void setInsline(String insline) {
		this.insline = insline;
	}

	public String getTransdte() {
		return transdte;
	}

	public void setTransdte(String transdte) {
		this.transdte = transdte;
	}

	public String getEntrydte() {
		return entrydte;
	}

	public void setEntrydte(String entrydte) {
		this.entrydte = entrydte;
	}

	public String getAcctgdte() {
		return acctgdte;
	}

	public void setAcctgdte(String acctgdte) {
		this.acctgdte = acctgdte;
	}

	public String getRetrodte() {
		return retrodte;
	}

	public void setRetrodte(String retrodte) {
		this.retrodte = retrodte;
	}

	public String getExtenddte() {
		return extenddte;
	}

	public void setExtenddte(String extenddte) {
		this.extenddte = extenddte;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExposure() {
		return exposure;
	}

	public void setExposure(String exposure) {
		this.exposure = exposure;
	}

	public String getLimitoccur() {
		return limitoccur;
	}

	public void setLimitoccur(String limitoccur) {
		this.limitoccur = limitoccur;
	}

	public String getLimitagger() {
		return limitagger;
	}

	public void setLimitagger(String limitagger) {
		this.limitagger = limitagger;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public char getCovstatus() {
		return covstatus;
	}

	public void setCovstatus(char covstatus) {
		this.covstatus = covstatus;
	}

	public BigDecimal getTotalorig() {
		return totalorig;
	}

	public void setTotalorig(BigDecimal totalorig) {
		this.totalorig = totalorig;
	}

	public BigDecimal getOriginal() {
		return original;
	}

	public void setOriginal(BigDecimal original) {
		this.original = original;
	}

	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	public BigDecimal getTotalprem() {
		return totalprem;
	}

	public void setTotalprem(BigDecimal totalprem) {
		this.totalprem = totalprem;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getClassdesc() {
		return classdesc;
	}

	public void setClassdesc(String classdesc) {
		this.classdesc = classdesc;
	}

	public String getAsline() {
		return asline;
	}

	public void setAsline(String asline) {
		this.asline = asline;
	}

	public String getProdline() {
		return prodline;
	}

	public void setProdline(String prodline) {
		this.prodline = prodline;
	}

	public BigDecimal getLimitCovA() {
		return limitCovA;
	}

	public void setLimitCovA(BigDecimal limitCovA) {
		this.limitCovA = limitCovA;
	}

	public BigDecimal getLimitCovB() {
		return limitCovB;
	}

	public void setLimitCovB(BigDecimal limitCovB) {
		this.limitCovB = limitCovB;
	}

	public BigDecimal getLimitCovC() {
		return limitCovC;
	}

	public void setLimitCovC(BigDecimal limitCovC) {
		this.limitCovC = limitCovC;
	}

	public BigDecimal getLimitCovD() {
		return limitCovD;
	}

	public void setLimitCovD(BigDecimal limitCovD) {
		this.limitCovD = limitCovD;
	}

	public BigDecimal getLimitCovE() {
		return limitCovE;
	}

	public void setLimitCovE(BigDecimal limitCovE) {
		this.limitCovE = limitCovE;
	}

	public BigDecimal getLimitCovF() {
		return limitCovF;
	}

	public void setLimitCovF(BigDecimal limitCovF) {
		this.limitCovF = limitCovF;
	}

	public long getWc07DedAmt() {
		return wc07DedAmt;
	}

	public void setWc07DedAmt(long wc07DedAmt) {
		this.wc07DedAmt = wc07DedAmt;
	}

	public long getWc07DedAggr() {
		return wc07DedAggr;
	}

	public void setWc07DedAggr(long wc07DedAggr) {
		this.wc07DedAggr = wc07DedAggr;
	}

	public char getReinsind() {
		return reinsind;
	}

	public void setReinsind(char reinsind) {
		this.reinsind = reinsind;
	}
}
