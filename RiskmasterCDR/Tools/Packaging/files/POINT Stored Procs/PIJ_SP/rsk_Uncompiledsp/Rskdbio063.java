package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.AddressableData;
import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.sql.TypeCast;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicCallException;
import bphx.c2ab.util.DynamicSqlDAO;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;

//FSIT # 119900 Res # 47654 Start
import com.csc.pt.svc.common.Baspprenst;
//FSIT # 119900 Res # 47654 End
//FSIT#164226 Resolution#59925 starts
import com.csc.pt.svc.data.dao.Asb5cpl0DAO;
//FSIT#164226 Resolution#59925 ends
import com.csc.pt.svc.data.dao.Bassys0500DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.to.Bassys0500TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.db.InslDescRecRec;
import com.csc.pt.svc.db.LocRecRec2;
import com.csc.pt.svc.db.LsWhereClause7;
import com.csc.pt.svc.db.SublocRecRec2;
import com.csc.pt.svc.rsk.UnitEntryArr;
import com.csc.pt.svc.db.UnitRecordRec;
import com.csc.pt.svc.db.Ws1SelectStatement7;
import com.csc.pt.svc.rsk.dao.Asb1cppDAO;
import com.csc.pt.svc.rsk.dao.Bassys0400DAO;
import com.csc.pt.svc.rsk.to.Rskdbio063TO;
import com.csc.pt.svc.isl.Cobolsqlca;

/**
****************************************************************
* This is the Stored procedure to return all the data for the  *
* Unit List grid corresponding to the following parameters     *
* passed from the server-side:                                 *
* 1. LOC                                                       *
* 2. MCO                                                       *
* 3. Symbol                                                    *
* 4. Policy                                                    *
* 5. Module Number                                             *
* 6. Line of Business                                          *
* 7. Insurance Line                                            *
* 8. Product                                                   *
* 9. State                                                     *
* The following fields are set when the user enters data in the*
* search fields:                                               *
* 10.Unit Number                                               *
* 11.Location Number                                           *
* 12.Sub Location Number                                       *
* 13.Unit Description                                          *
****************************************************************
* REF : 41964      PGMR: Dhiraj Pandey        DATE: 08/24/2006  *
* PROJ: BAS1906    FSIT ISSUE NO: 87784       SRC : BNRRINOBJ   *
*                  RESOLUTION ID: 41964                         *
* PROBLEM   : Out of sequence records are shown on the tree.    *
* RESOLUTION: If the building record has drop record indicator  *
*             value as 'X', it means its a future record for    *
*             out of sequence endorsement. These records should *
*             not be shown on Tree.                             *
*****************************************************************
* REF : 45607      PGMR: Deep V. Narula       DATE: 03/26/2008  *
* PROJ: BAS2575    FSIT ISSUE NO: 110450      SRC : BNRSINOBJ   *
*                  RESOLUTION ID: 45607                         *
* PROBLEM: POL - Total Premium on grid screens, insurance line  *
*          list, unit list, coverage list do not total correctly*
* RESOLUTION: The length of the total premium field passed to   *
*          the server side was 1 byte short.As a result the last*
*          digit of the total premium was getting truncated.    *
*          Corrected it by increasing the length of the field.  *
*****************************************************************
* REF : 47654      PGMR: Brij M Garg          DATE: 01/05/2009  * 
* PROJ: B119900    FSIT ISSUE NO: 119900      SRC : BNRR00OBJ   * 
*                  RESOLUTION ID: 47654                         * 
* PROBLEM: POL - User will be able to see the status of any pre-* 
* renewal changes while inquiring the policy.                   * 
* RESOLUTION: New program BASPPRENST is called from this program* 
* which is used to read next record from corresponding files    * 
* and return WS-BATYPE0ACT (Activity Type). If activity type is * 
* 'C' then record status is changed to 'Verified-R'. BASPPRENST * 
* will retrieve the pre-renewal status for a pending policy rcd.* 
***************************************************************** 
******************************************************************
* REF : 47285      PGMR: Mohammad Ashar       DATE: 08/06/2009  *
* PROJ: B112328    FSIT ISSUE NO: 112328      SRC : BNRR00OBJ   *
*                  RESOLUTION ID: 47285                         *
* PROBLEM   : POL - Global issue with the inconsistency of the  *
*             Search functions of POINT IN's result grids       *
* RESOLUTION: The functionality for search on the basis of      *
*             unit,location and building is added.              *
*****************************************************************
****************************************************************** 
* REF : 51114      PGMR: Gaurav Sateeja      DATE: 01/12/2010   * 
* PROJ: B135591    FSIT ISSUE NO: 135591     SRC : BNRQP6OBJ    * 
*                  RESOLUTION ID: 51114                         * 
* PROBLEM   : POL - Deleted Units appearing & wrong total       * 
*                   premium on Auto Coverage list               * 
* RESOLUTION: Changed the condition such that the deleted units * 
*             are not considered while calculating the total    * 
*             premium.                                          * 
***************************************************************** 
* PGMR: Dharmendra K Dhanesh       		DATE: 05/24/2012	    *
* FSIT: 176472                			RESOLUTION ID: 63564    *
* Issue : INT - POL - On the Location/Building/Unit List screen,*
* 			 location is not getting search using the LOC.		*
* Resolution: Added the Location/Building/Unit condition to test*
* 			 the exact match.									*
*****************************************************************
*/

public class Rskdbio063 {
	private static final String PROGRAM_ID = "RSKDBIO063";
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private Ws1SelectStatement7 ws1SelectStatement7 = new Ws1SelectStatement7();
	private int numOfEntrys;
	private int numOfRec;
	private int numOfEntrysForSql;
	private IABODynamicArray<UnitEntryArr> unitEntryArr = ABOCollectionCreator.getReferenceFactory(UnitEntryArr.class).createDynamicArray(1);
	private UnitRecordRec unitRecordRec = new UnitRecordRec();
	private InslDescRecRec inslDescRecRec = new InslDescRecRec();
	private LocRecRec2 locRecRec2 = new LocRecRec2();
	private SublocRecRec2 sublocRecRec2 = new SublocRecRec2();
	private char wsTrans0stat = ' ';
	private String wsLocation = "";
	private String wsMasterco = "";
	private String wsSymbol = "";
	private String wsPolicyNum = "";
	private String wsModule = "";
	private char wsIssue0code = ' ';
	private int wsUnitLimit;
	private String wsInsLine = "";
	private String wsLineOfBus = "";
	private String wsProduct = "";
	private String wsState = "";
	//FSIT: 152640, Resolution: 55204 - Begin
	/*private ABODecimal wsPaymentAmount = new ABODecimal(11, 2);
	private ABODecimal wsTotal = new ABODecimal(14, 2);
	private ABODecimal wsTotalAmount = new ABODecimal(15, 2);*/
	
	private ABODecimal wsPaymentAmount = new ABODecimal(20, 2);
	private ABODecimal wsTotal = new ABODecimal(20, 2);
	private ABODecimal wsTotalAmount = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private int wsNumber;
	private String wsId02 = "02";
	private String wsDescription = "";
	//FSIT# 112328 RESL# 47285 Start
	private String wsUnitNum = "";
	private String wsRiskLoc = "";
	private String wsSubLoc = "";
	//FSIT# 112328 RESL# 47285 End
	private String wsProductS = "";
	private String wsStateS = "";
	private String wsVinS = "";
	private short wsCheckNum;
	private short sqlIoOk;
	private short sqlNoRecord = ((short) 100);
	private short sqlInvalidToken = ((short) -104);
	private short sqlObjectNotFound = ((short) -201);
	private short sqlCursorAlreadyOpen = ((short) -502);
	private short sqlRecordExists = ((short) -803);
	private short sqlObjectLock = ((short) -913);
	private short sqlTableLock = ((short) -7958);
	private short sqlTooManyHostVariable = ((short) 326);
	private int ws1StrPos;
	private String wsReturnCode = "";
	public static final String GOOD_RETURN_FROM_IO = "0000000";
	private char wsControlSw = ' ';
	private Rskdbio063TO rskdbio063TO = new Rskdbio063TO();
	private LsWhereClause7 lsWhereClause7 = new LsWhereClause7();
	private Pmsp0200TO pmsp0200to = new Pmsp0200TO();
	private Bassys0500TO bassys0500to = new Bassys0500TO();
	private DynamicSqlDAO sqlCursor_DAO = new DynamicSqlDAO();
	//FSIT # 119900 Res # 47654 Start
	private int wsRenewMod;
	private String wsLocationCo = "";
	private String wsMasterCo = "";
	private String wsPolSymbol = "";
	private String wsPolicyNum1 = "";
	private String wsPolModule = "";
	private String wsInsuranceLine = ""; 
	private int wsRiskLocNo; 
	private int wsRiskLocSubNo ;
	private String wsProd = ""; 
	private String wsRateState1 = "";
	private int wsUnitNo; 
	private String wsCoverage = ""; 
	private int wsCoverageSequence ; 
	private int wsDriverId ; 
	private char wsBatype0Act = ' ';
	private String wsFileName ="";
	//FSIT # 119900 Res # 47654 End
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	//FSIT#164226 Resolution#59925 starts
	private int count;
	//FSIT#164226 Resolution#59925 ends
	public Rskdbio063() {
		unitEntryArr.assign((1) - 1, new UnitEntryArr());
	}

	public void mainSubroutine() {
		mainParagraph();
		exitProgram();
	}

	public void mainParagraph() {
		// statement is ignored: DECLARECURSOR[IDeclareCursorStatement]
		// @source=RSKDBIO063.cbl:line=166
		/*
		 * DECLARE SQL_CURSOR CURSOR FOR SQL_STATEMENT
		 */
		//
		//
		init();
		getIssueCode();
		getUnitLimit();
		loadUnits();
		passUnits();
	}

	public void exitProgram() {
		throw new ReturnException();
	}

	public void init() {
		numOfEntrys = 0 % 100000;
		wsLocation = rskdbio063TO.getLsLocation();
		wsMasterco = rskdbio063TO.getLsMasterco();
		wsSymbol = rskdbio063TO.getLsSymbol();
		wsPolicyNum = rskdbio063TO.getLsPolicyNum();
		wsModule = rskdbio063TO.getLsModule();
		wsInsLine = rskdbio063TO.getLsInsLine();
		wsProduct = rskdbio063TO.getLsProduct();
		wsState = rskdbio063TO.getLsState();
		wsLineOfBus = rskdbio063TO.getLsLineOfBus();
	}

	public void getIssueCode() {
		pmsp0200to = Pmsp0200DAO.selectById62(pmsp0200to, wsId02, wsSymbol, wsPolicyNum, wsModule, wsMasterco, wsLocation);
		cobolsqlca.setDBAccessStatus(pmsp0200to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsIssue0code = pmsp0200to.getIssue0code();
		}
	}

	public void getUnitLimit() {
		readBassys0500();
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsInsLine = "*AL";
			readBassys0500();
		}
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsLineOfBus = "*AL";
			wsInsLine = rskdbio063TO.getLsInsLine();
			readBassys0500();
		}
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsLineOfBus = "*AL";
			wsInsLine = "*AL";
			readBassys0500();
		}
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsUnitLimit = 100;
		}
	}

	public void readBassys0500() {
		bassys0500to = Bassys0500DAO.selectByLob(bassys0500to, wsLineOfBus, wsInsLine);
		cobolsqlca.setDBAccessStatus(bassys0500to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsUnitLimit = bassys0500to.getLimit5();
		}
	}

	public void loadUnits() {
		//
		ws1SelectStatement7.setWs1SelectStatement("");
		unitRecordRec.initUnitRecord();
		buildSelectStatement();
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.prepWithOverrideCheck(ws1SelectStatement7.getWs1SelectStatement()));
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.open());
		//FSIT#164226 Resolution#59925 starts
		count=Asb5cpl0DAO.selectPendingCount(rskdbio063TO.getLsLocation(),rskdbio063TO.getLsMasterco(),
				rskdbio063TO.getLsSymbol(),rskdbio063TO.getLsPolicyNum(),rskdbio063TO.getLsModule());
		//FSIT#164226 Resolution#59925 ends
		//
		//
		fetchRecord();
		//
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsTotal.assign(0);
		if (rskdbio063TO.getLsInsLine().compareTo("") != 0) {
			loadInsLineDesc();
		}
		getUnitEntryArr(numOfEntrys).setUnIssueCode(wsIssue0code);
		numOfEntrys = (1 + numOfEntrys) % 100000;
		if (!(unitRecordRec.getB5antx().compareTo("") == 0 && unitRecordRec.getB5aenb() == 0 && unitRecordRec.getB5dcnb().compareTo("") == 0
			&& unitRecordRec.getB5pttx().compareTo("") == 0 && unitRecordRec.getB5ancd().compareTo("") == 0
			&& unitRecordRec.getB5ddnb().compareTo("") == 0 && unitRecordRec.getB5a3va().compareTo(0) == 0 && unitRecordRec.getB5aldt() == 0
			&& unitRecordRec.getB5brnb() == 0 && unitRecordRec.getB5egnb() == 0 && unitRecordRec.getB5c6st() == ' ' && unitRecordRec.getB5c7st() == ' ')) {
			cobolsqlca.setSqlCode(((short) (0 % 1000000000l)));
			numOfEntrys = numOfEntrys;
			while (!(cobolsqlca.getSqlCode() != sqlIoOk || numOfEntrys > wsUnitLimit + 1)) {
				loadRecords();
				numOfEntrys = numOfEntrys + 1;
			}
			numOfEntrys = (numOfEntrys - 1) % 100000;
			getUnitEntryArr(2).setUnNumOfRecords(ABOSystem.convertUsingString(numOfEntrys, "S99999"));
		} else {
			numOfEntrys = (numOfEntrys - 1) % 100000;
		}
		wsTotalAmount.assign(wsTotal.clone());
		getUnitEntryArr(2).setUnTotalPremium(getWsTotalAmountFormatted());
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.close());
	}

	public void buildSelectStatement() {
		// ---------------------------------------------------------*
		// Set the address of LS-WHERE-CLAUSE to the address of *
		// WS1-SELECT-STATEMENT. This will overlay LS-WHERE-CLAUSE *
		// onto WS1-SELECT-STATEMENT, this is the same as *
		// redefining WS1-SELECT-STATEMENT at run time. *
		// ---------------------------------------------------------*
		lsWhereClause7.setAddressableData(((AddressableData) ws1SelectStatement7));
		lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, "SELECT", new String[] { " B5BCCD,", " B5ANTX,",
			" B5AENB,", " B5DCNB,", " B5PTTX,", " B5A3VA,", " B5ALDT,", " B5BRNB,", " B5EGNB,", " B5ANCD,", " B5DDNB,", " B5C6ST,", " B5C7ST",
			" FROM ASB5CPL0 WHERE (", "\u0000" }));
		// -------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// -------------------------------------------------------*
		// -------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// -------------------------------------------------------*
		setAddress();
		//
		// Location
		//
		if (rskdbio063TO.getLsLocation().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " B5AACD = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsLocation(), 2), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Master Company
		//
		if (rskdbio063TO.getLsMasterco().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5ABCD = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsMasterco(), 2), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Policy Symbol
		//
		if (rskdbio063TO.getLsSymbol().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5ARTX = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsSymbol(), 3), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Policy Number
		//
		if (rskdbio063TO.getLsPolicyNum().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5ASTX = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsPolicyNum(), 7), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Policy Module
		//
		if (rskdbio063TO.getLsModule().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5ADNB = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsModule(), 2), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Reason Amended field should not be 'OSE' ( =Out of Sequence)
		// Future records in out of sequence are stored with reason amen-
		// ded value as 'OSE'. These records should not be shown.
		//
		lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5BATX <> \"OSE\"", "\u0000"));
		setAddress();
		//
		// Insurance Line
		//
		if (rskdbio063TO.getLsInsLine().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5AGTX = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsInsLine(), 3), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Product
		//
		if (rskdbio063TO.getLsProduct().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5ANTX = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsProduct(), 6), "\"", "\u0000" }));
			setAddress();
		}
		//
		// State
		//
		if (rskdbio063TO.getLsState().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5BCCD = \"", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsState(), 2), "\"", "\u0000" }));
			setAddress();
		}
		//
		// Unit Number
		//
		if (rskdbio063TO.getLsUnitNum().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5AENB >= ", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsUnitNum(), 5), "\u0000" }));
			setAddress();
		}
		//
		// Location Number
		//
		if (rskdbio063TO.getLsRiskLoc().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5BRNB >= ", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsRiskLoc(), 5), "\u0000" }));
			setAddress();
		}
		//
		// Sub Location Number
		//
		if (rskdbio063TO.getLsSubLoc().compareTo("") != 0) {
			lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, " AND B5EGNB >= ", new String[] {
				Functions.padBlanks(rskdbio063TO.getLsSubLoc(), 5), "\u0000" }));
			setAddress();
		}
		//
		// --------------------------*
		// Close the WHERE clause. *
		// Add the ORDER BY clause. *
		// --------------------------*
		//
		lsWhereClause7.setLsWhereClause(Functions.concat(lsWhereClause7.getLsWhereClause(), 200, ") ORDER BY", new String[] { " B5AENB,", " B5BRNB,",
			" B5EGNB,", " B5C6ST" }));
	}

	public void loadInsLineDesc() {
		// * Build Description for Insurance Line for the first entry **
		// * of the table - UNIT-TABLE **
		if (numOfEntrys == 1) {
			inslDescRecRec.initInslDescRecSpaces();
			inslDescRecRec.setAfadtx(rskdbio063TO.getLsLineOfBus());
			inslDescRecRec.setAfagtx(rskdbio063TO.getLsInsLine());
			try {
				call1();
			} catch (DynamicCallException ex) {
				if (ex.getPgmName().compareTo("BASASAF101") != 0) {
					throw ex;
				} else {
					wsReturnCode = "FS00";
				}
			}
			if (wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0 && inslDescRecRec.getAfahtx().compareTo("") != 0) {
				getUnitEntryArr(numOfEntrys).setUnInsLineDesc(inslDescRecRec.getAfahtx());
			}
		} else {
			getUnitEntryArr(numOfEntrys).setUnInsLineDesc("");
		}
	}

	public void loadRecords() {
		//
		// Building the Description for Units
		//
		wsDescription = "";
		if (unitRecordRec.getB5dcnb().compareTo("") == 0 && unitRecordRec.getB5aenb() == 0
			&& (unitRecordRec.getB5brnb() != 0 || unitRecordRec.getB5egnb() != 0)) {
			getRisklLoc();
		} else {
			if (unitRecordRec.getB5aenb() == 0) {
				if (unitRecordRec.getB5dcnb().compareTo("") != 0) {
					wsDescription =
						Functions.concat(wsDescription, 61, Functions.trimAfter(unitRecordRec.getB5dcnb(), "  "), new String[] {
							Functions.trimAfter(" ", "  "), Functions.trimAfter(unitRecordRec.getB5ddnb(), "  ") });
				} else {
					wsDescription = Functions.concat(wsDescription, 61, Functions.trimAfter(unitRecordRec.getB5ddnb(), "  "));
				}
			} else {
				if (unitRecordRec.getB5dcnb().compareTo("") != 0) {
					wsDescription = Functions.concat(wsDescription, 61, Functions.trimAfter(unitRecordRec.getB5dcnb(), "  "));
				} else {
					wsDescription = "";
				}
			}
		}
		if (rskdbio063TO.getLsUnitDesc().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(rskdbio063TO.getLsUnitDesc(), 62, "%");
			if (Functions.subString(rskdbio063TO.getLsUnitDesc(), 1, ws1StrPos - 1).compareTo(
				ABOSystem.Upper(Functions.subString(wsDescription, 1, ws1StrPos - 1))) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getUnitEntryArr(numOfEntrys).setUnUnitDesc(Functions.subString(wsDescription, 1, 60));
		//
		// Product Search
		//
		wsProductS = unitRecordRec.getB5antx();
		if (rskdbio063TO.getLsProductS().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(rskdbio063TO.getLsProductS(), 7, "%");
			if (ABOSystem.Upper(Functions.subString(wsProductS, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(rskdbio063TO.getLsProductS(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getUnitEntryArr(numOfEntrys).setUnProduct(Functions.subString(wsProductS, 1, 6));
		//
		// State Search
		//
		wsStateS = unitRecordRec.getB5bccd();
		if (rskdbio063TO.getLsStateS().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(rskdbio063TO.getLsStateS(), 3, "%");
			if (ABOSystem.Upper(Functions.subString(wsStateS, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(rskdbio063TO.getLsStateS(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getUnitEntryArr(numOfEntrys).setUnRateState(Functions.subString(wsStateS, 1, 2));

		//FSIT# 112328 RESL# 47285 Start
		wsUnitNum = Integer.toString(unitRecordRec.getB5aenb());
		if (rskdbio063TO.getLsUnitNum().compareTo("") != 0) {
		//FSIT# 176472 RESL# 63564 Start
			//if(wsUnitNum.compareTo(rskdbio063TO.getLsUnitNum()) != 0){
			if(wsUnitNum.compareTo(String.valueOf(Integer.parseInt(rskdbio063TO.getLsUnitNum()))) != 0){
		//FSIT# 176472 RESL# 63564 End
				numOfEntrys=numOfEntrys-1;
				fetchRecord();
				return;
			}
		}
		wsRiskLoc = Integer.toString(unitRecordRec.getB5brnb());
		if (rskdbio063TO.getLsRiskLoc().compareTo("") != 0) {
		//FSIT# 176472 RESL# 63564 Start
			//if(wsRiskLoc.compareTo(rskdbio063TO.getLsRiskLoc()) != 0){
			if(wsRiskLoc.compareTo(String.valueOf(Integer.parseInt(rskdbio063TO.getLsRiskLoc()))) != 0){
		//FSIT# 176472 RESL# 63564 End
				numOfEntrys=numOfEntrys-1;
				fetchRecord();
				return;
			}
		}
		wsSubLoc = Integer.toString(unitRecordRec.getB5egnb());
		if (rskdbio063TO.getLsSubLoc().compareTo("") != 0) {
		//FSIT# 176472 RESL# 63564 Start
			//if(wsSubLoc.compareTo(rskdbio063TO.getLsSubLoc()) != 0){
			if(wsSubLoc.compareTo(String.valueOf(Integer.parseInt(rskdbio063TO.getLsSubLoc()))) != 0){
		//FSIT# 176472 RESL# 63564 End
				numOfEntrys=numOfEntrys-1;
				fetchRecord();
				return;
			}
		}
		//FSIT# 112328 RESL# 47285 End

		//
		// VIN Number applicable in case of Units only
		//
		if (unitRecordRec.getB5brnb() == 0 && unitRecordRec.getB5egnb() == 0) {
			wsVinS = unitRecordRec.getB5ddnb();
		} else {
			wsVinS = "";
		}
		if (rskdbio063TO.getLsVinS().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(rskdbio063TO.getLsVinS(), 31, "%");
			if (ABOSystem.Upper(Functions.subString(wsVinS, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(rskdbio063TO.getLsVinS(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getUnitEntryArr(numOfEntrys).setUnUnitVin(Functions.subString(wsVinS, 1, 30));
		// * For subsequent entries of the table move spaces to Ins-Line **
		// * description and the switches.
		getUnitEntryArr(numOfEntrys).setUnInsLineDesc("");
		getUnitEntryArr(numOfEntrys).setUnSwUnit(' ');
		getUnitEntryArr(numOfEntrys).setUnSwLocation(' ');
		getUnitEntryArr(numOfEntrys).setUnSwSubLocation(' ');
		//
		getUnitEntryArr(numOfEntrys).setUnUnitYear(unitRecordRec.getB5ancd());
		wsNumber = unitRecordRec.getB5aenb();
		getUnitEntryArr(numOfEntrys).setUnUnitNumber(getWsNumberFormatted());
		//RSK - Start
		getUnitEntryArr(numOfEntrys).setUnStatUnit(getWsNumberFormatted());
		//RSK - End
		wsNumber = unitRecordRec.getB5brnb();
		getUnitEntryArr(numOfEntrys).setUnRiskLocation(getWsNumberFormatted());
		wsNumber = unitRecordRec.getB5egnb();
		getUnitEntryArr(numOfEntrys).setUnSubLocation(getWsNumberFormatted());
		getUnitEntryArr(numOfEntrys).setUnClassCode(unitRecordRec.getB5pttx());
		wsPaymentAmount.assign(unitRecordRec.getB5a3va());
		//FSIT# 135591 RESL# 51114 Start
		//FSIT# 135591 RESL# 51114 End
		//FSIT#164226 Resolution#59925 starts
		if(count==0){
			
			//if (!(unitRecordRec.getB5c6st() == 'P' && unitRecordRec.getB5c7st() == 'Y')) {
			if (!(unitRecordRec.getB5c6st() == 'P') && !(unitRecordRec.getB5c7st() == 'Y')) {
			//FSIT# 135591 RESL# 51114 End	
				//wsTotal.assign((wsTotal.clone().add(unitRecordRec.getB5a3va())).createConverted(14, 2));
				//FSIT: 152640, Resolution: 55204 - Begin
				//wsTotal.assign((wsTotal.clone().add(unitRecordRec.getB5a3va())).createConverted(14, 2));
				wsTotal.assign((wsTotal.clone().add(unitRecordRec.getB5a3va())).createConverted(20, 2));
				//FSIT: 152640, Resolution: 55204 - End
			}
		}else{
			if (unitRecordRec.getB5c6st() == 'P' && !(unitRecordRec.getB5c6st() == 'Y')) {
				wsTotal.assign((wsTotal.clone().add(unitRecordRec.getB5a3va())).createConverted(20, 2));
			}
			//FSIT#164226 Resolution#59925 ends
		}
		
		getUnitEntryArr(numOfEntrys).setUnRatePremium(getWsPaymentAmountFormatted());
		if (unitRecordRec.getB5aldt() == 0) {
			getUnitEntryArr(numOfEntrys).setUnChgEffDate("");
		} else {
			getUnitEntryArr(numOfEntrys).setUnChgEffDate(ABOSystem.convertUsingString(unitRecordRec.getB5aldt(), "S9999999"));
		}
		//
		// * To set the switches that will determine whether to show the
		// * Unit, Location or Sub-Location on the Unit List Grid.
		//
		if (unitRecordRec.getB5aenb() != 0) {
			getUnitEntryArr(1).setUnSwUnit('Y');
		}
		if (unitRecordRec.getB5brnb() != 0) {
			getUnitEntryArr(1).setUnSwLocation('Y');
		}
		if (unitRecordRec.getB5egnb() != 0) {
			getUnitEntryArr(1).setUnSwSubLocation('Y');
		}
		//
		// ***** Build Description for Record-Status ******
		if (unitRecordRec.getB5c6st() == 'H') {
			getUnitEntryArr(numOfEntrys).setUnRecStatusDesc("History");
		} else {
			if (unitRecordRec.getB5c6st() == 'V') {
				//FSIT # 119900 Res # 47654 Start
				InitializeWsPreRenewalRec();
				wsInsuranceLine = rskdbio063TO.getLsInsLine();
				wsProd = unitRecordRec.getB5antx();
				wsRiskLocNo = unitRecordRec.getB5brnb();
				wsRiskLocSubNo = unitRecordRec.getB5egnb();
				wsUnitNo = unitRecordRec.getB5aenb();
				wsFileName ="ASB5CPP";				
				CallPgmChkNxtRecord();
			   if(wsBatype0Act == 'C'){
				   getUnitEntryArr(numOfEntrys).setUnRecStatusDesc("Verified-R");

				}else{
					getUnitEntryArr(numOfEntrys).setUnRecStatusDesc("Verified");
				}
				//FSIT # 119900 Res # 47654 End
				
			} else {
				if (unitRecordRec.getB5c7st() == 'Y') {
					getUnitEntryArr(numOfEntrys).setUnRecStatusDesc("Drop Pending");
				} else {
					getUnitEntryArr(numOfEntrys).setUnRecStatusDesc("Pending");
				}
			}
		}
		//
		getUnitEntryArr(numOfEntrys).setUnIssueCode(wsIssue0code);
		//
		//
		//RSK - Start
		fetchAsb1cpp();
		fetchBassys0400();
		//RSK - End
		fetchRecord();
	}
	
	//RSK - Start
	public void fetchAsb1cpp(){
		String unit = Asb1cppDAO.fetchUnit(unitRecordRec.getB5aacd(), unitRecordRec.getB5abcd(), unitRecordRec.getB5artx(), 
				unitRecordRec.getB5astx(), unitRecordRec.getB5adnb(), unitRecordRec.getB5agtx(), 
				unitRecordRec.getB5brnb(), unitRecordRec.getB5egnb(), unitRecordRec.getB5antx(), unitRecordRec.getB5aenb());
		if (unit != null){
			getUnitEntryArr(numOfEntrys).setUnStatUnit(unit);
		}
	}
	
	public void fetchBassys0400(){
		String desc;
		desc = Bassys0400DAO.fetchDesc(rskdbio063TO.getLsLineOfBus(), rskdbio063TO.getLsInsLine());
		if(desc == null){
			desc = Bassys0400DAO.fetchDesc("*AL", rskdbio063TO.getLsInsLine());
		}
		if(desc == null){
			desc = Bassys0400DAO.fetchDesc("*AL", "*AL");
		}
		if(desc != null){
			getUnitEntryArr(numOfEntrys).setUnSumdesc(desc);
		}
	}
	//RSK - End

	public void getRisklLoc() {
		//
		locRecRec2.initLocRecSpaces();
		locRecRec2.setBuaacd(rskdbio063TO.getLsLocation());
		locRecRec2.setBuabcd(rskdbio063TO.getLsMasterco());
		locRecRec2.setBuartx(rskdbio063TO.getLsSymbol());
		locRecRec2.setBuastx(rskdbio063TO.getLsPolicyNum());
		locRecRec2.setBuadnb(rskdbio063TO.getLsModule());
		locRecRec2.setBubrnb(unitRecordRec.getB5brnb());
		wsControlSw = 'Y';
		try {
			call2();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASASBU101") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		sublocRecRec2.initSublocRecSpaces();
		//CSC-Manual Start
		if(unitRecordRec.getB5egnb() != 0) {
		//CSC-Manual End	
			sublocRecRec2.setBvaacd(rskdbio063TO.getLsLocation());
			sublocRecRec2.setBvabcd(rskdbio063TO.getLsMasterco());
			sublocRecRec2.setBvartx(rskdbio063TO.getLsSymbol());
			sublocRecRec2.setBvastx(rskdbio063TO.getLsPolicyNum());
			sublocRecRec2.setBvadnb(rskdbio063TO.getLsModule());
			sublocRecRec2.setBvbrnb(unitRecordRec.getB5brnb());
			sublocRecRec2.setBvegnb(unitRecordRec.getB5egnb());
			wsControlSw = 'Y';
			try {
				call3();
			} catch (DynamicCallException ex) {
				if (ex.getPgmName().compareTo("BASASBV101") != 0) {
					throw ex;
				} else {
					wsReturnCode = "FS00";
				}
			}
		//CSC-Manual Start	
		}
		//CSC-Manual End
		//
		wsDescription = "";
		if (locRecRec2.getBueftx().compareTo("") != 0) {
			wsDescription =
				Functions.concat(wsDescription, 61, Functions.trimAfter(locRecRec2.getBueftx(), "  "), new String[] { " ",
					Functions.trimAfter(sublocRecRec2.getBveftx(), "  ") });
		} else {
			wsDescription = sublocRecRec2.getBveftx();
		}
	}

	public void passUnits() {
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(unitEntryArr, numOfEntrysForSql);
	}

	public void fetchRecord() {
		Object[] resultSet;
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.fetchNext());
		if (cobolsqlca.getSqlCode() == 0) {
			resultSet = cobolsqlca.getResultSet();
			unitRecordRec.setB5bccd(TypeCast.getAsStr(resultSet[(1) - 1], 2));
			unitRecordRec.setB5antx(TypeCast.getAsStr(resultSet[(2) - 1], 6));
			unitRecordRec.setB5aenb(TypeCast.getAsInt(resultSet[(3) - 1], 5));
			unitRecordRec.setB5dcnb(TypeCast.getAsStr(resultSet[(4) - 1], 30));
			unitRecordRec.setB5pttx(TypeCast.getAsStr(resultSet[(5) - 1], 6));
			//FSIT: 152640, Resolution: 55204 - Begin
			//unitRecordRec.setB5a3va(new ABODecimal(TypeCast.getAsDecimal(resultSet[(6) - 1], 11, 2), 11, 2));
			unitRecordRec.setB5a3va(new ABODecimal(TypeCast.getAsDecimal(resultSet[(6) - 1], 20, 2), 20, 2));
			//FSIT: 152640, Resolution: 55204 - End
			unitRecordRec.setB5aldt(TypeCast.getAsInt(resultSet[(7) - 1], 7));
			unitRecordRec.setB5brnb(TypeCast.getAsInt(resultSet[(8) - 1], 5));
			unitRecordRec.setB5egnb(TypeCast.getAsInt(resultSet[(9) - 1], 5));
			unitRecordRec.setB5ancd(TypeCast.getAsStr(resultSet[(10) - 1], 6));
			unitRecordRec.setB5ddnb(TypeCast.getAsStr(resultSet[(11) - 1], 30));
			unitRecordRec.setB5c6st(TypeCast.getAsChar(resultSet[(12) - 1]));
			unitRecordRec.setB5c7st(TypeCast.getAsChar(resultSet[(13) - 1]));
		}
		//
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			return;
		}
	}

	public void setAddress() {
		ws1StrPos = 1;
		// -------------------------------------------------------------*
		// Calculate the WS1-STR-POS by getting the number of characters*
		// before the X'00'. *
		// -------------------------------------------------------------*
		ws1StrPos = ws1StrPos + Functions.countCharsBefore(ws1SelectStatement7.getWs1SelectStatement(), 10000, "\u0000");
		// -------------------------------------------------------------*
		// The STRING statement can not have a referential modified *
		// field as the INTO fields. So we use a field that has been *
		// defined in linkage named LS-WHERE-CLAUSE. We set the *
		// address of LS-WHERE-CLAUSE to the address of the part of *
		// field that we want to add the where condition to. *
		// -------------------------------------------------------------*
		lsWhereClause7.setAddressableData(((AddressableData) ws1SelectStatement7), ws1StrPos);
		lsWhereClause7.setLsWhereClause("");
	}

	public SPResultSetTO runSP(Rskdbio063TO rskdbio063TO) {
		this.rskdbio063TO = rskdbio063TO;
		run();
		return resultSetTO;
	}

	public SPResultSetTO runSP(byte[] dynamic_lsInputParams) {
		rskdbio063TO.setLsInputParams(dynamic_lsInputParams);
		return runSP(rskdbio063TO);
	}

	public Rskdbio063TO run(Rskdbio063TO rskdbio063TO) {
		this.rskdbio063TO = rskdbio063TO;
		run();
		return this.rskdbio063TO;
	}

	public void run(byte[] dynamic_lsInputParams) {
		if (dynamic_lsInputParams != null) {
			rskdbio063TO.setLsInputParams(dynamic_lsInputParams);
		}
		run(rskdbio063TO);
		if (dynamic_lsInputParams != null) {
			DataConverter.arrayCopy(rskdbio063TO.getLsInputParams(), rskdbio063TO.getLsInputParamsSize(), dynamic_lsInputParams, 1);
		}
	}

	public void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[inslDescRecRec.getInslDescRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(inslDescRecRec.getInslDescRec(), inslDescRecRec.getInslDescRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASASAF101", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		inslDescRecRec.setInslDescRec(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public void call2() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbucpl1RecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(getAsbucpl1Rec(), getAsbucpl1RecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASASBU101", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbucpl1Rec(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public void call3() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbvcpl1RecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(getAsbvcpl1Rec(), getAsbvcpl1RecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASASBV101", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbvcpl1Rec(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public UnitEntryArr getUnitEntryArr(int index) {
		if (index > unitEntryArr.size()) {
			// for an element beyond current array limits, all intermediate
			// items will be added with default elements;
			unitEntryArr.ensureCapacity(index);
			for (int i = unitEntryArr.size() + 1; i <= index; i++) {
				unitEntryArr.append(new UnitEntryArr());
				// assign default value
			}
		}
		return unitEntryArr.get((index) - 1);
	}

	public static Rskdbio063 getInstance() {
		Rskdbio063 iRskdbio063 = ((Rskdbio063) CalledProgList.getInst("RSKDBIO063"));
		if (iRskdbio063 == null) {
			// create an instance if needed
			iRskdbio063 = new Rskdbio063();
			CalledProgList.addInst("RSKDBIO063", ((Object) iRskdbio063));
		}
		return iRskdbio063;
	}

	public void run() {
		try {
			mainSubroutine();
		} catch (ReturnException re) {
			// normal termination of the program
		}
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	public String getWsTotalAmountFormatted() {
		//FSIT: 152640, Resolution: 55204 - Begin
		//return ABOSystem.convertUsingString(wsTotalAmount.clone(), "ZZZZZZZZZZZZ9.99");
		return ABOSystem.convertUsingString(wsTotalAmount.clone(), "ZZZZZZZZZZZZZZZZZ9.99");
		//FSIT: 152640, Resolution: 55204 - End
	}

	public String getWsNumberFormatted() {
		return ABOSystem.convertUsingString(wsNumber, "ZZZZ9");
	}

	public String getWsPaymentAmountFormatted() {
		//FSIT: 152640, Resolution: 55204 - Begin
		//return ABOSystem.convertUsingString(wsPaymentAmount.clone(), "ZZZZZZZZ9.99");
		return ABOSystem.convertUsingString(wsPaymentAmount.clone(), "ZZZZZZZZZZZZZZZZZ9.99");
		//FSIT: 152640, Resolution: 55204 - End
	}

	public int getAsbucpl1RecSize() {
		return 148;
	}

	public byte[] getAsbucpl1Rec() {
		byte[] buf = new byte[getAsbucpl1RecSize()];
		int offset = 1;
		DataConverter.arrayCopy(locRecRec2.getLocRec(), 1, locRecRec2.getLocRecSize(), buf, offset);
		return buf;
	}

	public void setAsbucpl1Rec(byte[] buf) {
		int offset = 1;
		byte[] locRec_buf = DataConverter.allocNcopy(buf, offset, locRecRec2.getLocRecSize());
		locRecRec2.setLocRec(locRec_buf);
	}

	public int getAsbvcpl1RecSize() {
		return 153;
	}

	public byte[] getAsbvcpl1Rec() {
		byte[] buf = new byte[getAsbvcpl1RecSize()];
		int offset = 1;
		DataConverter.arrayCopy(sublocRecRec2.getSublocRec(), 1, sublocRecRec2.getSublocRecSize(), buf, offset);
		return buf;
	}

	public void setAsbvcpl1Rec(byte[] buf) {
		int offset = 1;
		byte[] sublocRec_buf = DataConverter.allocNcopy(buf, offset, sublocRecRec2.getSublocRecSize());
		sublocRecRec2.setSublocRec(sublocRec_buf);
	}
	//FSIT # 119900 Res # 47654 Start
	/*method CallPgmChkNxtRecord is used to call program BASPPRENST which is reading next pending record
	from the file passed to this program as parameter and returns activity type into wsBatype0Act               */
	public void CallPgmChkNxtRecord(){
		Baspprenst baspprenst;
		wsRenewMod=0;
		wsLocationCo=rskdbio063TO.getLsLocation();
		wsMasterCo=rskdbio063TO.getLsMasterco();
		wsPolSymbol=rskdbio063TO.getLsSymbol();
		wsPolicyNum1 =rskdbio063TO.getLsPolicyNum();
		if(Functions.isNumber(rskdbio063TO.getLsModule())){
			wsRenewMod= Integer.valueOf(rskdbio063TO.getLsModule()).intValue();
		}
		wsRenewMod= wsRenewMod+1;
		wsPolModule = new Integer(wsRenewMod).toString();
		baspprenst = Baspprenst.getInstance();
		setWsPreRenewalrec(baspprenst.run(getWsPreRenewalrec()));		                            
	}
	
	public void InitializeWsPreRenewalRec(){
		wsLocationCo = "";
		wsMasterCo = "";
		wsPolSymbol = "";
		wsPolicyNum1 = "";
		wsPolModule = "";
		wsInsuranceLine = ""; 
		wsRiskLocNo = 0; 
		wsRiskLocSubNo = 0;
		wsProd = ""; 
		wsRateState1 = "";
		wsUnitNo = 0; 
		wsCoverage = ""; 
		wsCoverageSequence = 0 ; 
		wsDriverId =0; 
		wsFileName = ""; 
		wsBatype0Act = ' ';
	}
	
	public void setWsPreRenewalrec(byte[] buf){
		int offset = 1;
		wsLocationCo  = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsMasterCo = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsPolSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsPolicyNum1 = DataConverter.readString(buf, offset, 7);
		offset += 7;
		wsPolModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsInsuranceLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsRiskLocNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsRiskLocSubNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsProd  = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsRateState1 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsUnitNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsCoverage = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsCoverageSequence = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsDriverId = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsFileName = DataConverter.readString(buf, offset, 10);
		offset += 10;
		wsBatype0Act = DataConverter.readChar(buf, offset);		
	}
	
	public byte[] getWsPreRenewalrec() {
		byte[] buf = new byte[69];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsLocationCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsMasterCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsPolSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsPolicyNum1, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, wsPolModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsInsuranceLine, 3);
		offset += 3;
		DataConverter.writeInt(buf, offset, wsRiskLocNo, 5);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsRiskLocSubNo, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsProd, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsRateState1, 2);
		offset += 2;
		DataConverter.writeInt(buf, offset, wsUnitNo, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsCoverage, 6);
		offset += 6;
		DataConverter.writeInt(buf, offset, wsCoverageSequence, 5);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsDriverId, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsFileName, 10);
		offset += 10;
		DataConverter.writeChar(buf, offset, wsBatype0Act);
		return buf;
	}
	//FSIT # 119900 Res # 47654 End
}
