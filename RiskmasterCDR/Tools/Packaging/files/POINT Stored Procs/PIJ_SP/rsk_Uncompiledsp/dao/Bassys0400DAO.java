package com.csc.pt.svc.rsk.dao;

import java.io.Serializable;
import java.util.List;
import java.util.ListIterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.ClassValidator;
import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.hibernate.Util;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.Primitives;

import com.csc.pt.svc.data.to.Asb1cppTO;
import com.csc.pt.svc.data.to.Bassys0400TO;

public class Bassys0400DAO {
	private ScrollableResults tab5cursor;
	private ScrollableResults tab4cursor;
	private ScrollableResults tab3cursor;
	private ScrollableResults tab2cursor;
	private ScrollableResults tab1cursor;
	private ScrollableResults tab0cursor;
	//private ScrollableResults resultSet;
	private ListIterator rowIter = null;
	private List rowList = null;
	private static ClassValidator validator = new ClassValidator(Bassys0400TO.getTOClass());
	//BPHX changes start - fix 157944
	private int pagesize = bphx.c2ab.util.ConfigSettings.getDefaultCursorPageSize();
	private int recnum = 1;
	private int pagenum = 0;
	private boolean cursorNoWhere = false;
	// BPHX changes start - fix 157870
	//private Bassys0400TO lastBassys0400TO = new Bassys0400TO();
	private Bassys0400TO lastBassys0400TO = null;
	// BPHX changes end - fix 157870
	//BPHX changes end - fix 157944

	public Bassys0400TO fetchNext(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO localBassys0400TO = bassys0400TO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			// BPHX changes start - fix 157870
			if (lastBassys0400TO == null) {
			// BPHX changes end - fix 157870
				//BPHX changes Start - 157944
				pagenum = 0;
				//BPHX changes End - 157944
				status = openBassys0400TOCsr();
			// BPHX changes start - fix 157870
			} else {
				if (lastBassys0400TO.getDBAccessStatus().isSuccess()) {
					//last READ was successful
					Bassys0400TO lastBassys0400TOsave = lastBassys0400TO;
					status = openBassys0400TOCsr(KeyType.GREATER_OR_EQ , 
						lastBassys0400TO.getFilename(), 
						lastBassys0400TO.getKeyfld1(), 
						lastBassys0400TO.getKeyfld2(), 
						lastBassys0400TO.getKeyfld3(), 
						lastBassys0400TO.getKeyfld4(), 
						lastBassys0400TO.getKeyfld5());
					//BPHX changes start - fix 158659
					if (rowList != null && rowList.size() > 0) {
					//BPHX changes end - fix 158659
						if (isRecordEqual(lastBassys0400TOsave.getFilename(), 
								lastBassys0400TOsave.getKeyfld1(), 
								lastBassys0400TOsave.getKeyfld2(), 
								lastBassys0400TOsave.getKeyfld3(), 
								lastBassys0400TOsave.getKeyfld4(), 
								lastBassys0400TOsave.getKeyfld5())) {
							rowIter.next(); //skip record already processed
						}
					//BPHX changes start - fix 158659
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localBassys0400TO = new Bassys0400TO();
						localBassys0400TO.setDBAccessStatus(status);
						return localBassys0400TO;
					}
					//BPHX changes end - fix 158659
				} else {
					//unsuccessful READ
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localBassys0400TO = new Bassys0400TO();
				}
			}
			// BPHX changes end - fix 157870
		}
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (Bassys0400TO.class.isInstance(obj)) {
				localBassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localBassys0400TO);
			}*/
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (Bassys0400TO.class.isInstance(obj)) {
				localBassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) obj), localBassys0400TO);
			}
		//BPHX changes start - fix 157944
//		} else {
//			status.setSqlCode(DBAccessStatus.DB_EOF);
//			localBassys0400TO = new Bassys0400TO();
//		}
			lastBassys0400TO = localBassys0400TO;
			recnum++;
		} else {
			//if (resultSet != null && recnum >= pagesize - 1) {
			if (rowIter != null && recnum >= pagesize - 1) {
				if(!cursorNoWhere) {
					//Open cursor with last key to get next page
				//BPHX changes start - fix 158659
				Bassys0400TO lastBassys0400TOsave = lastBassys0400TO;
				//BPHX changes end - fix 158659
				status = openBassys0400TOCsr(KeyType.GREATER_OR_EQ , 
					lastBassys0400TO.getFilename(), 
					lastBassys0400TO.getKeyfld1(), 
					lastBassys0400TO.getKeyfld2(), 
					lastBassys0400TO.getKeyfld3(), 
					lastBassys0400TO.getKeyfld4(), 
					lastBassys0400TO.getKeyfld5());
					//BPHX changes start - fix 158659
					if (rowList != null && rowList.size() > 0) {
						if (isRecordEqual(lastBassys0400TOsave.getFilename(), 
							lastBassys0400TOsave.getKeyfld1(), 
							lastBassys0400TOsave.getKeyfld2(), 
							lastBassys0400TOsave.getKeyfld3(), 
							lastBassys0400TOsave.getKeyfld4(), 
							lastBassys0400TOsave.getKeyfld5())) {
					//BPHX changes end - fix 158659		
							//resultSet.next();
							rowIter.next();			// Throw away record already processed
					//BPHX changes start - fix 158659
						}
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localBassys0400TO = new Bassys0400TO();
						localBassys0400TO.setDBAccessStatus(status);
						return localBassys0400TO;
					}
					//BPHX changes end - fix 158659
				} else {
					pagenum++;
					status = openBassys0400TOCsr();		//Open cursor to nowhere with pagenum incremented to get next page of rows
				}
				localBassys0400TO = fetchNext(localBassys0400TO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localBassys0400TO = new Bassys0400TO();
			}
		}
		//BPHX changes end - fix 157944
		localBassys0400TO.setDBAccessStatus(status);
		return localBassys0400TO;
	}

	public Bassys0400TO fetchPrevious(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO localBassys0400TO = bassys0400TO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openBassys0400TOCsr();
		}
		/*if (resultSet != null && resultSet.previous()) {
			Object obj = resultSet.get(0);
			if (Bassys0400TO.class.isInstance(obj)) {
				localBassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localBassys0400TO);
			}*/
		if (rowIter != null && rowIter.hasPrevious()) {
			localBassys0400TO = ((Bassys0400TO) rowIter.previous());
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localBassys0400TO = new Bassys0400TO();
		}
		localBassys0400TO.setDBAccessStatus(status);
		return localBassys0400TO;
	}

	public Bassys0400TO fetchFirst(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO localBassys0400TO = bassys0400TO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openBassys0400TOCsr();
		}
		/*if (resultSet != null && resultSet.first()) {
			Object obj = resultSet.get(0);
			if (Bassys0400TO.class.isInstance(obj)) {
				localBassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localBassys0400TO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localBassys0400TO = ((Bassys0400TO) rowList.get(0));
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localBassys0400TO = new Bassys0400TO();
		}
		localBassys0400TO.setDBAccessStatus(status);
		return localBassys0400TO;
	}

	public Bassys0400TO fetchLast(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO localBassys0400TO = bassys0400TO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openBassys0400TOCsr();
		}
		/*if (resultSet != null && resultSet.last()) {
			Object obj = resultSet.get(0);
			if (Bassys0400TO.class.isInstance(obj)) {
				localBassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localBassys0400TO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localBassys0400TO = ((Bassys0400TO) rowList.get(rowList.size() - 1));
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localBassys0400TO = new Bassys0400TO();
		}
		localBassys0400TO.setDBAccessStatus(status);
		return localBassys0400TO;
	}

	// BPHX changes start - fix 157870
	public Bassys0400TO fetchById(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4,
			String keyfld5) {
		Bassys0400TO localBassys0400TO = bassys0400TO;
		closeCsr();
		localBassys0400TO = selectById(bassys0400TO, filename, keyfld1, keyfld2, keyfld3, keyfld4, keyfld5);
		lastBassys0400TO = localBassys0400TO;
		return localBassys0400TO;
	}
	// BPHX changes end - fix 157870

	public static Bassys0400TO selectById(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4,
			String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO localBassys0400TO = bassys0400TO;
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			localBassys0400TO.getId().setFilename(filename);
			localBassys0400TO.getId().setKeyfld1(keyfld1);
			localBassys0400TO.getId().setKeyfld2(keyfld2);
			localBassys0400TO.getId().setKeyfld3(keyfld3);
			localBassys0400TO.getId().setKeyfld4(keyfld4);
			localBassys0400TO.getId().setKeyfld5(keyfld5);
			localBassys0400TO = ((Bassys0400TO) Util.get(session, ((Object) localBassys0400TO), ((Serializable) bassys0400TO.getId())));
			if (localBassys0400TO == null) {
				localBassys0400TO = bassys0400TO;
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		localBassys0400TO.setDBAccessStatus(status);
		return localBassys0400TO;
	}

	public static DBAccessStatus update(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4,
			String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			InvalidValue[] errors = validator.getInvalidValues(((Object) bassys0400TO));
			if (errors.length > 0) {
				throw (new InvalidStateException(errors));
			}
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			if (bassys0400TO.getId() == null) {
				Query query = session.getNamedQuery("Bassys0400TO_UPDATE");
				query.setParameter("sumdesc", bassys0400TO.getSumdesc());
				query.setParameter("filename", filename);
				query.setParameter("keyfld1", keyfld1);
				query.setParameter("keyfld2", keyfld2);
				query.setParameter("keyfld3", keyfld3);
				query.setParameter("keyfld4", keyfld4);
				query.setParameter("keyfld5", keyfld5);
				query.executeUpdate();
				selectById(bassys0400TO, filename, keyfld1, keyfld2, keyfld3, keyfld4, keyfld5);
			} else {
				Bassys0400TO localBassys0400TO = ((Bassys0400TO) Util.get(session, ((Object) bassys0400TO), ((Serializable) bassys0400TO.getId())));
				if (localBassys0400TO == null) {
					status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
				} else {
					session.merge(bassys0400TO);
					session.flush();
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public static DBAccessStatus insert(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			if (Util.get(session, ((Object) bassys0400TO), ((Serializable) bassys0400TO.getId())) != null) {
				status.setSqlCode(DBAccessStatus.DB_DUPLICATE);
			} else {
				if (session.contains(((Object) bassys0400TO))) {
					session.evict(((Object) bassys0400TO));
				}
				InvalidValue[] errors = validator.getInvalidValues(((Object) bassys0400TO));
				if (errors.length > 0) {
					throw (new InvalidStateException(errors));
				}
				session.save(((Object) bassys0400TO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openBassys0400TOCsr(int keyType, String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4,
			String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = null;
			if (keyType == KeyType.GREATER) {
				query = session.getNamedQuery("Bassys0400TO_CURSOR_GT");
			} else if (keyType == KeyType.EQUAL) { 
			   query = session.getNamedQuery("Bassys0400TO_CURSOR_EQ"); 
			} else {
				// default
				query = session.getNamedQuery("Bassys0400TO_CURSOR_GTE");
			}
			if (Functions.isInvalidSqlData(filename)) {
				query.setParameter("filename", ((Object) null));
			} else {
				query.setParameter("filename", filename);
			}
			if (Functions.isInvalidSqlData(keyfld1)) {
				query.setParameter("keyfld1", ((Object) null));
			} else {
				query.setParameter("keyfld1", keyfld1);
			}
			if (Functions.isInvalidSqlData(keyfld2)) {
				query.setParameter("keyfld2", ((Object) null));
			} else {
				query.setParameter("keyfld2", keyfld2);
			}
			if (Functions.isInvalidSqlData(keyfld3)) {
				query.setParameter("keyfld3", ((Object) null));
			} else {
				query.setParameter("keyfld3", keyfld3);
			}
			if (Functions.isInvalidSqlData(keyfld4)) {
				query.setParameter("keyfld4", ((Object) null));
			} else {
				query.setParameter("keyfld4", keyfld4);
			}
			if (Functions.isInvalidSqlData(keyfld5)) {
				query.setParameter("keyfld5", ((Object) null));
			} else {
				query.setParameter("keyfld5", keyfld5);
			}
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = false;
			//BPHX changes end - fix 157944
			//resultSet = query.scroll();
			rowList = query.list();
			/*if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();*/
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				// reset
				/*if (keyType == KeyType.EQUAL) {
					// record with given key value found ?
					if (!isRecordEqual(filename, keyfld1, keyfld2, keyfld3, keyfld4, keyfld5)) {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						//resultSet.close();
						rowList.clear();
						//BPHX changes start - fix 157789
						//resultSet = null;
						//BPHX changes end - fix 157789
					}
				}*/
			} else {
				if (keyType == KeyType.EQUAL) {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openBassys0400TOCsr() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400TO_CURSOR_NOWHERE");
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = true;
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			//BPHX changes end - fix 157944
			//resultSet = query.scroll();
			rowList = query.list();
			/*if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();*/
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				// reset
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus closeCsr() {
		DBAccessStatus status = new DBAccessStatus();
		/*if (resultSet != null) {
			resultSet.close();
			resultSet = null;*/
		if (rowList != null) {
			rowList.clear();
			rowList = null;
			rowIter = null;
		}
		// BPHX changes start - fix 157870
		lastBassys0400TO = null;
		// BPHX changes end - fix 157870
		return status;
	}

	public static DBAccessStatus delete(String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4, String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Bassys0400TO bassys0400TO = new Bassys0400TO();
			// set data for where clause
			bassys0400TO.getId().setFilename(filename);
			bassys0400TO.getId().setKeyfld1(keyfld1);
			bassys0400TO.getId().setKeyfld2(keyfld2);
			bassys0400TO.getId().setKeyfld3(keyfld3);
			bassys0400TO.getId().setKeyfld4(keyfld4);
			bassys0400TO.getId().setKeyfld5(keyfld5);
			bassys0400TO = ((Bassys0400TO) Util.get(session, ((Object) bassys0400TO), ((Serializable) bassys0400TO.getId())));
			if (bassys0400TO == null) {
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			} else {
				session.delete(((Object) bassys0400TO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public void fetch(Object[] colData, Bassys0400TO bassys0400TO) {
		bassys0400TO.getId().setFilename(Primitives.getString(colData[(1) - 1]));
		bassys0400TO.getId().setKeyfld1(Primitives.getString(colData[(2) - 1]));
		bassys0400TO.getId().setKeyfld2(Primitives.getString(colData[(3) - 1]));
		bassys0400TO.getId().setKeyfld3(Primitives.getString(colData[(4) - 1]));
		bassys0400TO.getId().setKeyfld4(Primitives.getString(colData[(5) - 1]));
		bassys0400TO.getId().setKeyfld5(Primitives.getString(colData[(6) - 1]));
		bassys0400TO.setSumdesc(Primitives.getString(colData[(7) - 1]));
	}

	public boolean isRecordEqual(String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4, String keyfld5) {
		Bassys0400TO bassys0400TO = new Bassys0400TO();
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (Bassys0400TO.class.isInstance(obj)) {
				bassys0400TO = ((Bassys0400TO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), bassys0400TO);
			}
			resultSet.previous();*/
		if (rowIter != null && rowIter.hasNext()) {
			bassys0400TO = ((Bassys0400TO) rowIter.next());
			rowIter.previous();
			// reset
		}
		if ((Functions.isInvalidSqlData(filename) || filename.compareTo(bassys0400TO.getId().getFilename()) == 0)
			&& (Functions.isInvalidSqlData(keyfld1) || keyfld1.compareTo(bassys0400TO.getId().getKeyfld1()) == 0)
			&& (Functions.isInvalidSqlData(keyfld2) || keyfld2.compareTo(bassys0400TO.getId().getKeyfld2()) == 0)
			&& (Functions.isInvalidSqlData(keyfld3) || keyfld3.compareTo(bassys0400TO.getId().getKeyfld3()) == 0)
			&& (Functions.isInvalidSqlData(keyfld4) || keyfld4.compareTo(bassys0400TO.getId().getKeyfld4()) == 0)
			&& (Functions.isInvalidSqlData(keyfld5) || keyfld5.compareTo(bassys0400TO.getId().getKeyfld5()) == 0)) {
			return true;
		}
		return false;
	}

	public DBAccessStatus deleteAll() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			//BPHX changes start - fix 157118
			session.flush();
			session.clear();
			//BPHX changes end - fix 157118
			Query query = session.getNamedQuery("Bassys0400TO_DELETE_ALL");
			int result = query.executeUpdate();
			status.setNumOfRows(result);
			//BPHX changes start - fix 157118
			//session.flush();
			//BPHX changes end - fix 157118
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public static Bassys0400TO selectByFilename(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3,
			String keyfld4, String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_selectByFilename");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld3", keyfld3);
			query.setString("keyfld4", keyfld4);
			query.setString("keyfld5", keyfld5);

			List resultList = query.list();
			if (resultList != null && resultList.size() > 0) {
				Object rowResult = resultList.get(0);

				if (rowResult != null) {
					bassys0400TO.setSumdesc((String) rowResult);
				} else {
					bassys0400TO.setSumdesc(null);
				}
				if (resultList.size() > 1) {
					status.setSqlCode(DBAccessStatus.DB_MULTIPLE_RECORDS);
				}
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (org.hibernate.HibernateException ex) {
			status.setException(ex);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public static Bassys0400TO selectByFilename2(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_selectByFilename2");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld3", keyfld3);

			List resultList = query.list();
			if (resultList != null && resultList.size() > 0) {
				Object rowResult = resultList.get(0);

				if (rowResult != null) {
					bassys0400TO.setSumdesc((String) rowResult);
				} else {
					bassys0400TO.setSumdesc(null);
				}
				if (resultList.size() > 1) {
					status.setSqlCode(DBAccessStatus.DB_MULTIPLE_RECORDS);
				}
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (org.hibernate.HibernateException ex) {
			status.setException(ex);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus openTab0cursor(String filename) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab0cursor");
			query.setString("filename", filename);

			tab0cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab0cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab0cursor != null && tab0cursor.next()) {
			Object rowResult = tab0cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab0cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab0cursor != null) {
			tab0cursor.close();
		}
		return status;
	}
	public DBAccessStatus openTab1cursor(String filename, String keyfld1) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab1cursor");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);

			tab1cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab1cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab1cursor != null && tab1cursor.next()) {
			Object rowResult = tab1cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab1cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab1cursor != null) {
			tab1cursor.close();
		}
		return status;
	}
	public DBAccessStatus openTab2cursor(String filename, String keyfld1, String keyfld2) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab2cursor");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);

			tab2cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab2cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab2cursor != null && tab2cursor.next()) {
			Object rowResult = tab2cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab2cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab2cursor != null) {
			tab2cursor.close();
		}
		return status;
	}
	public DBAccessStatus openTab3cursor(String filename, String keyfld1, String keyfld2, String keyfld3) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab3cursor");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld3", keyfld3);

			tab3cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab3cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab3cursor != null && tab3cursor.next()) {
			Object rowResult = tab3cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab3cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab3cursor != null) {
			tab3cursor.close();
		}
		return status;
	}
	public DBAccessStatus openTab4cursor(String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab4cursor");
			query.setString("filename", filename);
			query.setString("keyfld3", keyfld3);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld4", keyfld4);

			tab4cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab4cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab4cursor != null && tab4cursor.next()) {
			Object rowResult = tab4cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab4cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab4cursor != null) {
			tab4cursor.close();
		}
		return status;
	}
	public DBAccessStatus openTab5cursor(String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4, String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400DAO_openTab5cursor");
			query.setString("filename", filename);
			query.setString("keyfld3", keyfld3);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld4", keyfld4);
			query.setString("keyfld5", keyfld5);

			tab5cursor = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Bassys0400TO fetchTab5cursor(Bassys0400TO bassys0400TO) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOResult = bassys0400TO;
		if (tab5cursor != null && tab5cursor.next()) {
			Object rowResult = tab5cursor.get(0);

			if (rowResult != null) {
				bassys0400TO.setSumdesc((String) rowResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		bassys0400TOResult.setDBAccessStatus(status);
		return bassys0400TOResult;
	}
	public DBAccessStatus closeTab5cursor() {
		DBAccessStatus status = new DBAccessStatus();
		if (tab5cursor != null) {
			tab5cursor.close();
		}
		return status;
	}
	
	//CSC-Manual Start	
	public static Bassys0400TO selectById1(Bassys0400TO bassys0400TO, String filename, String keyfld1, String keyfld2, String keyfld3, String keyfld4,
			String keyfld5) {
		DBAccessStatus status = new DBAccessStatus();
		Bassys0400TO bassys0400TOTOResult = bassys0400TO;
		try {
			Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
			Query query = session.getNamedQuery("Bassys0400TO_SELECTBYID");
			query.setString("filename", filename);
			query.setString("keyfld1", keyfld1);
			query.setString("keyfld2", keyfld2);
			query.setString("keyfld3", keyfld3);
			query.setString("keyfld4", keyfld4);
			query.setString("keyfld5", keyfld5);

			query.setCacheable(true);
			query.setMaxResults(1);
			List resultList = query.list();
			
			if (resultList != null && resultList.size() > 0) {
				Object rowResult = resultList.get(0);
				if (rowResult != null) {
					bassys0400TOTOResult = (Bassys0400TO) rowResult;
				} else {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
				if (resultList.size() > 1) {
					status.setSqlCode(DBAccessStatus.DB_MULTIPLE_RECORDS);
				}
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (org.hibernate.HibernateException ex) {
			status.setException(ex);
		}
		bassys0400TOTOResult.setDBAccessStatus(status);
		return bassys0400TOTOResult;			
	}
	//CSC-Manual End
	
	//RSK - START
	public static String fetchDesc(String lob, String insLine){
		String desc = null;
		try {
		Session session = HibernateSessionFactory.current().getSession("BASSYS0400");
		Criteria criteria=session.createCriteria(Bassys0400TO.class).setMaxResults(1);
		criteria.add(Restrictions.eq("id.filename",(String) "ASB5CPP" ));
		criteria.add(Restrictions.eq("id.keyfld1",lob));
		criteria.add(Restrictions.eq("id.keyfld2",insLine ));
		List<Bassys0400TO> list=criteria.list();			
		if (list.size()>0) {
			desc=list.get(0).getSumdesc();
		}else{
			desc = null;
		}
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}		
		return desc;
	}
	//RSK - End
	
}/* @lineinfo:generated-code */
