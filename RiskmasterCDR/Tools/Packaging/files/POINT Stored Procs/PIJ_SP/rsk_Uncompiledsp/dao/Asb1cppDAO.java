package com.csc.pt.svc.rsk.dao;

import java.io.Serializable;
import java.util.ListIterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.ClassValidator;
import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;

import bphx.c2ab.hibernate.ABOTypes;
import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.hibernate.Util;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.Primitives;
import bphx.sdf.datatype.ABODecimal;

import com.csc.pt.svc.data.to.Asb1cppTO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
//FSIT# 183574 , RES # 67897  - Start
import java.util.concurrent.atomic.AtomicInteger;
import bphx.c2ab.util.ISession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
// FSIT# 183574 , RES # 67897  - End
/*
* ** ********************************************************************************************
* PGMR: Sanjeev Sharma        				DATE: 05/23/2012                                    *	
* FSIT: 176105     							RESOLUTION: 63502                                   * 
* IC - COMMON - Premium of cov with manual premium must stay as after exp date is shorten at EN.* 							
*************************************************************************************************
* PGMR:Steve		     	DATE:  11/08/2012                  	*
* FSIT ISSUE NO#183574      RESOLUTION ID# 67897                *
* Resolution: INT - Performance issue with Billing Indicator ON *
* and issue any policy.                    						*              
*****************************************************************
* Programmer   : Nikhil Verma        Date : 12/19/2012	 	             *
* FSIT Issue # : 161121			      Resolution Number : 58707     			   *
* Description  : Conversion of the Base FSIT # 161121 into POINT IN J              *
************************************************************************************
**/
public class Asb1cppDAO {
	private ScrollableResults clcursor2;
	//FSIT:161121 RESO:58707 Start
	private ScrollableResults clcursor3;
	//FSIT:161121 RESO:58707 End
//	CSC-Changes start - 155024
//	private ScrollableResults resultSet;
	private ListIterator rowIter = null;
	private List rowList = null;
//CSC-Changes end - 155024
	private static ClassValidator validator = new ClassValidator(Asb1cppTO.getTOClass());
	//BPHX changes start - fix 157944
	//BPHX changes start - fix 158448
	//private int pagesize = Integer.MAX_VALUE;
	private int pagesize = bphx.c2ab.util.ConfigSettings.getDefaultCursorPageSizeForFilesWoUniqKey();
	private boolean cursorFirstTime = true;
	private Asb1cppTO firstAsb1cppTO = new Asb1cppTO();
	//BPHX changes end - fix 158448
	private int recnum = 1;
	private int pagenum = 0;
	private boolean cursorNoWhere = false;
	// BPHX changes start - fix 157870
	//private Asb1cppTO lastAsb1cppTO = new Asb1cppTO();
	private Asb1cppTO lastAsb1cppTO = null;
	// BPHX changes end - fix 157870
	//BPHX changes end - fix 157944
//FSIT:183574 Resolution:67897 Start
	// INSERT_COUNTER is initialized using the String constructor because it serves as a key
	// in an IdentityHashMap
	private static final String INSERT_COUNTER = new String("Asb1cppDAO.insert.insertCounter");
	private static final Log log = LogFactory.getLog(Asb1cppDAO.class);
//FSIT:183574 Resolution:67897 End

	public Asb1cppTO fetchNext(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
//		CSC-Changes start - 155024
		// if (resultSet == null) {
		if (rowList == null) {
			//CSC-Changes end - 155024
			// cursor not opened, try opening
			// BPHX changes start - fix 157870
			if (lastAsb1cppTO == null) {
			// BPHX changes end - fix 157870
				//BPHX changes Start - 157944
				pagenum = 0;
				//BPHX changes End - 157944
				status = openAsb1cppTOCsr();
			// BPHX changes start - fix 157870
			} else {
				if (lastAsb1cppTO.getDBAccessStatus().isSuccess()) {
					//last READ was successful
					Asb1cppTO lastAsb1cppTOsave = lastAsb1cppTO;
					status = openAsb1cppTOCsr(KeyType.GREATER_OR_EQ , 
						lastAsb1cppTO.getB1aacd(), 
						lastAsb1cppTO.getB1abcd(), 
						lastAsb1cppTO.getB1artx(), 
						lastAsb1cppTO.getB1astx(), 
						lastAsb1cppTO.getB1adnb(), 
						lastAsb1cppTO.getB1agtx(), 
						lastAsb1cppTO.getB1brnb(), 
						lastAsb1cppTO.getB1egnb(), 
						lastAsb1cppTO.getB1antx(), 
						lastAsb1cppTO.getB1aenb(), 
						lastAsb1cppTO.getB1aotx(), 
						lastAsb1cppTO.getB1c0nb(), 
						lastAsb1cppTO.getB1pbtx(), 
						lastAsb1cppTO.getB1item(), 
						lastAsb1cppTO.getB1azcd(), 
						lastAsb1cppTO.getB1fgtx(), 
						lastAsb1cppTO.getB1a1cd(), 
						lastAsb1cppTO.getB1transeq(), 
						lastAsb1cppTO.getB1j4tx(), 
						lastAsb1cppTO.getB1bsnb(), 
						lastAsb1cppTO.getB1cinb(), 
						lastAsb1cppTO.getB1cbtx(), 
						lastAsb1cppTO.getB1czst(), 
						lastAsb1cppTO.getB1akcd(), 
						lastAsb1cppTO.getB1aepc(), 
						lastAsb1cppTO.getB1agva(), 
						lastAsb1cppTO.getB1alrind(), 
						lastAsb1cppTO.getB1pstx(), 
						lastAsb1cppTO.getB1alrus1(), 
						lastAsb1cppTO.getB1alrus2(), 
						lastAsb1cppTO.getB1alrus3());
					//BPHX changes start - fix 158659
					if (rowList != null && rowList.size() > 0) {
					//BPHX changes end - fix 158659
						if (isRecordEqual(lastAsb1cppTOsave.getB1aacd(), 
								lastAsb1cppTOsave.getB1abcd(), 
								lastAsb1cppTOsave.getB1artx(), 
								lastAsb1cppTOsave.getB1astx(), 
								lastAsb1cppTOsave.getB1adnb(), 
								lastAsb1cppTOsave.getB1agtx(), 
								lastAsb1cppTOsave.getB1brnb(), 
								lastAsb1cppTOsave.getB1egnb(), 
								lastAsb1cppTOsave.getB1antx(), 
								lastAsb1cppTOsave.getB1aenb(), 
								lastAsb1cppTOsave.getB1aotx(), 
								lastAsb1cppTOsave.getB1c0nb(), 
								lastAsb1cppTOsave.getB1pbtx(), 
								lastAsb1cppTOsave.getB1item(), 
								lastAsb1cppTOsave.getB1azcd(), 
								lastAsb1cppTOsave.getB1fgtx(), 
								lastAsb1cppTOsave.getB1a1cd(), 
								lastAsb1cppTOsave.getB1transeq(), 
								lastAsb1cppTOsave.getB1j4tx(), 
								lastAsb1cppTOsave.getB1bsnb(), 
								lastAsb1cppTOsave.getB1cinb(), 
								lastAsb1cppTOsave.getB1cbtx(), 
								lastAsb1cppTOsave.getB1czst(), 
								lastAsb1cppTOsave.getB1akcd(), 
								lastAsb1cppTOsave.getB1aepc(), 
								lastAsb1cppTOsave.getB1agva(), 
								lastAsb1cppTOsave.getB1alrind(), 
								lastAsb1cppTOsave.getB1pstx(), 
								lastAsb1cppTOsave.getB1alrus1(), 
								lastAsb1cppTOsave.getB1alrus2(), 
								lastAsb1cppTOsave.getB1alrus3())) {
							rowIter.next(); //skip record already processed
						}
					//BPHX changes start - fix 158659
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localAsb1cppTO = new Asb1cppTO();
						localAsb1cppTO.setDBAccessStatus(status);
						return localAsb1cppTO;
					}
					//BPHX changes end - fix 158659
				} else {
					//unsuccessful READ
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localAsb1cppTO = new Asb1cppTO();
				}
			}
			// BPHX changes end - fix 157870
		}
//		CSC-Changes start - 155024
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				localAsb1cppTO = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsb1cppTO);
			}*/
		if (rowIter != null && rowIter.hasNext()) {
			localAsb1cppTO = ((Asb1cppTO) rowIter.next());
			//CSC-Changes end - 155024
		//BPHX changes start - fix 157944
//		} else {
//			status.setSqlCode(DBAccessStatus.DB_EOF);
//			localAsb1cppTO = new Asb1cppTO();
//		}
			//BPHX changes start - fix 158448
			//lastAsb1cppTO = localAsb1cppTO;
			if(cursorFirstTime) {
				firstAsb1cppTO = localAsb1cppTO;
				cursorFirstTime = false;
			}
			//BPHX changes end - fix 158448
			recnum++;
		} else {
//			CSC-Changes start - 155024
//			if (resultSet != null && recnum >= pagesize - 1) {
			if (rowIter != null && recnum >= pagesize - 1) {
//CSC-Changes start - 155024
				if(!cursorNoWhere) {
					//BPHX changes start - fix 158448
					//Open cursor with first key to get next page
					pagenum++;
					//BPHX changes end - fix 158448
				status = openAsb1cppTOCsr(KeyType.GREATER_OR_EQ , 
					firstAsb1cppTO.getB1aacd(), 
					firstAsb1cppTO.getB1abcd(), 
					firstAsb1cppTO.getB1artx(), 
					firstAsb1cppTO.getB1astx(), 
					firstAsb1cppTO.getB1adnb(), 
					firstAsb1cppTO.getB1agtx(), 
					firstAsb1cppTO.getB1brnb(), 
					firstAsb1cppTO.getB1egnb(), 
					firstAsb1cppTO.getB1antx(), 
					firstAsb1cppTO.getB1aenb(), 
					firstAsb1cppTO.getB1aotx(), 
					firstAsb1cppTO.getB1c0nb(), 
					firstAsb1cppTO.getB1pbtx(), 
					firstAsb1cppTO.getB1item(), 
					firstAsb1cppTO.getB1azcd(), 
					firstAsb1cppTO.getB1fgtx(), 
					firstAsb1cppTO.getB1a1cd(), 
					firstAsb1cppTO.getB1transeq(), 
					firstAsb1cppTO.getB1j4tx(), 
					firstAsb1cppTO.getB1bsnb(), 
					firstAsb1cppTO.getB1cinb(), 
					firstAsb1cppTO.getB1cbtx(), 
					firstAsb1cppTO.getB1czst(), 
					firstAsb1cppTO.getB1akcd(), 
					firstAsb1cppTO.getB1aepc(), 
					firstAsb1cppTO.getB1agva(), 
					firstAsb1cppTO.getB1alrind(), 
					firstAsb1cppTO.getB1pstx(), 
					firstAsb1cppTO.getB1alrus1(), 
					firstAsb1cppTO.getB1alrus2(), 
					firstAsb1cppTO.getB1alrus3());
//				CSC-Changes start - 155024
//				resultSet.next();			// Throw away record already processed
				//BPHX changes start - fix 158448
				//rowIter.next();			// Throw away record already processed
				//BPHX changes end - fix 158448
//CSC-Changes end - 155024
				} else {
					pagenum++;
					status = openAsb1cppTOCsr();		//Open cursor to nowhere with pagenum incremented to get next page of rows
				}
				localAsb1cppTO = fetchNext(localAsb1cppTO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localAsb1cppTO = new Asb1cppTO();
			}
		}
		//BPHX changes end - fix 157944
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}

	public Asb1cppTO fetchPrevious(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
//		CSC-Changes start - 155024
		// if (resultSet == null) {
		if (rowList == null) {
			//CSC-Changes end - 155024
			// cursor not opened, try opening
			status = openAsb1cppTOCsr();
		}
//		CSC-Changes start - 155024
		/*if (resultSet != null && resultSet.previous()) {
			Object obj = resultSet.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				localAsb1cppTO = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsb1cppTO);
			}*/
		if (rowIter != null && rowIter.hasPrevious()) {
			localAsb1cppTO = ((Asb1cppTO) rowIter.previous());
			//CSC-Changes end - 155024
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsb1cppTO = new Asb1cppTO();
		}
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}

	public Asb1cppTO fetchFirst(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
//		CSC-Changes start - 155024
		// if (resultSet == null) {
		if (rowList == null) {
			//CSC-Changes end - 155024
			// cursor not opened, try opening
			status = openAsb1cppTOCsr();
		}
//		CSC-Changes start - 155024
		/*if (resultSet != null && resultSet.first()) {
			Object obj = resultSet.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				localAsb1cppTO = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsb1cppTO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localAsb1cppTO = ((Asb1cppTO) rowList.get(0));
			//CSC-Changes end - 155024
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsb1cppTO = new Asb1cppTO();
		}
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}

	public Asb1cppTO fetchLast(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
//		CSC-Changes start - 155024
		// if (resultSet == null) {
		if (rowList == null) {
			//CSC-Changes end - 155024
			// cursor not opened, try opening
			status = openAsb1cppTOCsr();
		}
//		CSC-Changes start - 155024
		/*if (resultSet != null && resultSet.last()) {
			Object obj = resultSet.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				localAsb1cppTO = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsb1cppTO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localAsb1cppTO = ((Asb1cppTO) rowList.get(rowList.size() - 1));
			//CSC-Changes end - 155024
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsb1cppTO = new Asb1cppTO();
		}
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}

	// BPHX changes start - fix 157870
	public Asb1cppTO fetchById(Asb1cppTO asb1cppTO, String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb, String b1agtx,
			int b1brnb, int b1egnb, String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd, String b1fgtx,
			String b1a1cd, int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd, ABODecimal b1aepc,
			ABODecimal b1agva, char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		Asb1cppTO localAsb1cppTO = asb1cppTO;
		closeCsr();
		localAsb1cppTO = selectById(asb1cppTO, b1aacd, b1abcd, b1artx, b1astx, b1adnb, b1agtx, b1brnb, b1egnb, b1antx, b1aenb, b1aotx, b1c0nb, b1pbtx, b1item, b1azcd, b1fgtx, b1a1cd, b1transeq, b1j4tx, b1bsnb, b1cinb, b1cbtx, b1czst, b1akcd, b1aepc, b1agva, b1alrind, b1pstx, b1alrus1, b1alrus2, b1alrus3);
		lastAsb1cppTO = localAsb1cppTO;
		return localAsb1cppTO;
	}
	// BPHX changes end - fix 157870

	public static Asb1cppTO selectById(Asb1cppTO asb1cppTO, String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb, String b1agtx,
			int b1brnb, int b1egnb, String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd, String b1fgtx,
			String b1a1cd, int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd, ABODecimal b1aepc,
			ABODecimal b1agva, char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			localAsb1cppTO.getId().setB1aacd(b1aacd);
			localAsb1cppTO.getId().setB1abcd(b1abcd);
			localAsb1cppTO.getId().setB1artx(b1artx);
			localAsb1cppTO.getId().setB1astx(b1astx);
			localAsb1cppTO.getId().setB1adnb(b1adnb);
			localAsb1cppTO.getId().setB1agtx(b1agtx);
			localAsb1cppTO.getId().setB1brnb(b1brnb);
			localAsb1cppTO.getId().setB1egnb(b1egnb);
			localAsb1cppTO.getId().setB1antx(b1antx);
			localAsb1cppTO.getId().setB1aenb(b1aenb);
			localAsb1cppTO.getId().setB1aotx(b1aotx);
			localAsb1cppTO.getId().setB1c0nb(b1c0nb);
			localAsb1cppTO.getId().setB1pbtx(b1pbtx);
			localAsb1cppTO.getId().setB1item(b1item);
			localAsb1cppTO.getId().setB1azcd(b1azcd);
			localAsb1cppTO.getId().setB1fgtx(b1fgtx);
			localAsb1cppTO.getId().setB1a1cd(b1a1cd);
			localAsb1cppTO.getId().setB1transeq(b1transeq);
			localAsb1cppTO.getId().setB1j4tx(b1j4tx);
			localAsb1cppTO.getId().setB1bsnb(b1bsnb);
			localAsb1cppTO.getId().setB1cinb(b1cinb);
			localAsb1cppTO.getId().setB1cbtx(b1cbtx);
			localAsb1cppTO.getId().setB1czst(b1czst);
			localAsb1cppTO.getId().setB1akcd(b1akcd);
			localAsb1cppTO.getId().setB1aepc(b1aepc);
			localAsb1cppTO.getId().setB1agva(b1agva);
			localAsb1cppTO.getId().setB1alrind(b1alrind);
			localAsb1cppTO.getId().setB1pstx(b1pstx);
			localAsb1cppTO.getId().setB1alrus1(b1alrus1);
			localAsb1cppTO.getId().setB1alrus2(b1alrus2);
			localAsb1cppTO.getId().setB1alrus3(b1alrus3);
			localAsb1cppTO = ((Asb1cppTO) Util.get(session, ((Object) localAsb1cppTO), ((Serializable) asb1cppTO.getId())));
			if (localAsb1cppTO == null) {
				localAsb1cppTO = asb1cppTO;
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}

	public static DBAccessStatus update(Asb1cppTO asb1cppTO, String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb,
			String b1agtx, int b1brnb, int b1egnb, String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd,
			String b1fgtx, String b1a1cd, int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd,
			ABODecimal b1aepc, ABODecimal b1agva, char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			InvalidValue[] errors = validator.getInvalidValues(((Object) asb1cppTO));
			if (errors.length > 0) {
				throw (new InvalidStateException(errors));
			}
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			if (asb1cppTO.getId() == null) {
				Query query = session.getNamedQuery("Asb1cppTO_UPDATE");
				query.setParameter("b1aacd", b1aacd);
				query.setParameter("b1abcd", b1abcd);
				query.setParameter("b1artx", b1artx);
				query.setParameter("b1astx", b1astx);
				query.setParameter("b1adnb", b1adnb);
				query.setParameter("b1agtx", b1agtx);
				query.setParameter("b1brnb", b1brnb);
				query.setParameter("b1egnb", b1egnb);
				query.setParameter("b1antx", b1antx);
				query.setParameter("b1aenb", b1aenb);
				query.setParameter("b1aotx", b1aotx);
				query.setParameter("b1c0nb", b1c0nb);
				query.setParameter("b1pbtx", b1pbtx);
				query.setParameter("b1item", b1item);
				query.setParameter("b1azcd", b1azcd);
				query.setParameter("b1fgtx", b1fgtx);
				query.setParameter("b1a1cd", b1a1cd);
				query.setParameter("b1transeq", b1transeq);
				query.setParameter("b1j4tx", b1j4tx);
				query.setParameter("b1bsnb", b1bsnb);
				query.setParameter("b1cinb", b1cinb);
				query.setParameter("b1cbtx", b1cbtx);
				query.setParameter("b1czst", b1czst);
				query.setParameter("b1akcd", b1akcd);
				query.setParameter("b1aepc", b1aepc, ABOTypes.DecimalType());
				query.setParameter("b1agva", b1agva, ABOTypes.DecimalType());
				query.setParameter("b1alrind", b1alrind);
				query.setParameter("b1pstx", b1pstx);
				query.setParameter("b1alrus1", b1alrus1);
				query.setParameter("b1alrus2", b1alrus2);
				query.setParameter("b1alrus3", b1alrus3);
				query.executeUpdate();
				selectById(asb1cppTO, b1aacd, b1abcd, b1artx, b1astx, b1adnb, b1agtx, b1brnb, b1egnb, b1antx, b1aenb, b1aotx, b1c0nb, b1pbtx, b1item,
					b1azcd, b1fgtx, b1a1cd, b1transeq, b1j4tx, b1bsnb, b1cinb, b1cbtx, b1czst, b1akcd, b1aepc, b1agva, b1alrind, b1pstx, b1alrus1,
					b1alrus2, b1alrus3);
			} else {
				Asb1cppTO localAsb1cppTO = ((Asb1cppTO) Util.get(session, ((Object) asb1cppTO), ((Serializable) asb1cppTO.getId())));
				if (localAsb1cppTO == null) {
					status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
				} else {
					session.merge(asb1cppTO);
					session.flush();
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}

//FSIT #148243  Stat Which will read all data Frorm ASB1CPP Files
	public static Asb1cppTO select(Asb1cppTO asb1cppTo, String b1artx, String b1astx, String b1adnb, String b1abcd,String b1aacd)
	{
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO pmspwc02TOResult = asb1cppTo ;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Read_ALl_Record");
			
			query.setString("b1artx", b1artx);
			query.setString("b1astx", b1astx);
			query.setString("b1adnb", b1adnb);
			query.setString("b1abcd", b1abcd);
			query.setString("b1aacd", b1aacd);
			
			
			
			//query.setMaxResults(1);
			List resultList = query.list();
			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					pmspwc02TOResult = (Asb1cppTO) obj;
				} else {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
				if (resultList.size() > 1) {
					status.setSqlCode(DBAccessStatus.DB_MULTIPLE_RECORDS);
				}
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (org.hibernate.HibernateException ex) {
			status.setException(ex);
		}
		pmspwc02TOResult.setDBAccessStatus(status);
		return pmspwc02TOResult;
	}
	        
//FSIT #148243  ENds Which will read all data from ASB1CPP Files	
	public static DBAccessStatus insert(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			if (Util.get(session, ((Object) asb1cppTO), ((Serializable) asb1cppTO.getId())) != null) {
				status.setSqlCode(DBAccessStatus.DB_DUPLICATE);
			} else {
				if (session.contains(((Object) asb1cppTO))) {
					session.evict(((Object) asb1cppTO));
				}
				InvalidValue[] errors = validator.getInvalidValues(((Object) asb1cppTO));
				if (errors.length > 0) {
					throw (new InvalidStateException(errors));
				}
				session.save(((Object) asb1cppTO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}
//FSIT:183574 Resolution:67897 Start
	public static DBAccessStatus batchInsert(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		AtomicInteger insertCounter;
		int batchSize = new Integer(HibernateSessionFactory.current().getConfiguration().getProperty("hibernate.jdbc.batch_size"));
		
		if(ISession.current().getDynamicSessionDataValue(INSERT_COUNTER) == null) {
			log.debug(INSERT_COUNTER + " is null. Creating new instance and putting in ISession DynamicSessionData");
			//System.out.println(INSERT_COUNTER + " is null. Creating new instance and putting in ISession DynamicSessionData");
			insertCounter = new AtomicInteger(0);
			ISession.current().putDynamicSessionDataValue(INSERT_COUNTER, insertCounter);
		} else {
			insertCounter = (AtomicInteger) ISession.current().getDynamicSessionDataValue(INSERT_COUNTER);
		}
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
				session.save(((Object) asb1cppTO));
				insertCounter.incrementAndGet();
				if(insertCounter.get() == batchSize) {
					log.debug("Asb1cppDAO.insert() batch threshold hit. Flushing Hibernate session.");
					//System.out.println("Asb1cppDAO.insert() batch threshold hit. Flushing Hibernate session.");
					insertCounter.set(0);
					session.flush();
				}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}
//FSIT:183574 Resolution:67897 End

	public DBAccessStatus openAsb1cppTOCsr(int keyType, String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb, String b1agtx,
			int b1brnb, int b1egnb, String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd, String b1fgtx,
			String b1a1cd, int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd, ABODecimal b1aepc,
			ABODecimal b1agva, char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = null;
			if (keyType == KeyType.GREATER) {
				query = session.getNamedQuery("Asb1cppTO_CURSOR_GT");
			} else if (keyType == KeyType.EQUAL) { 
			   query = session.getNamedQuery("Asb1cppTO_CURSOR_EQ"); 
			} else {
				// default
				query = session.getNamedQuery("Asb1cppTO_CURSOR_GTE");
			}
			if (Functions.isInvalidSqlData(b1aacd)) {
				query.setParameter("b1aacd", ((Object) null));
			} else {
				query.setParameter("b1aacd", b1aacd);
			}
			if (Functions.isInvalidSqlData(b1abcd)) {
				query.setParameter("b1abcd", ((Object) null));
			} else {
				query.setParameter("b1abcd", b1abcd);
			}
			if (Functions.isInvalidSqlData(b1artx)) {
				query.setParameter("b1artx", ((Object) null));
			} else {
				query.setParameter("b1artx", b1artx);
			}
			if (Functions.isInvalidSqlData(b1astx)) {
				query.setParameter("b1astx", ((Object) null));
			} else {
				query.setParameter("b1astx", b1astx);
			}
			if (Functions.isInvalidSqlData(b1adnb)) {
				query.setParameter("b1adnb", ((Object) null));
			} else {
				query.setParameter("b1adnb", b1adnb);
			}
			if (Functions.isInvalidSqlData(b1agtx)) {
				query.setParameter("b1agtx", ((Object) null));
			} else {
				query.setParameter("b1agtx", b1agtx);
			}
			if (Functions.isInvalidSqlData(b1brnb)) {
				query.setParameter("b1brnb", ((Object) null));
			} else {
				query.setParameter("b1brnb", b1brnb);
			}
			if (Functions.isInvalidSqlData(b1egnb)) {
				query.setParameter("b1egnb", ((Object) null));
			} else {
				query.setParameter("b1egnb", b1egnb);
			}
			if (Functions.isInvalidSqlData(b1antx)) {
				query.setParameter("b1antx", ((Object) null));
			} else {
				query.setParameter("b1antx", b1antx);
			}
			if (Functions.isInvalidSqlData(b1aenb)) {
				query.setParameter("b1aenb", ((Object) null));
			} else {
				query.setParameter("b1aenb", b1aenb);
			}
			if (Functions.isInvalidSqlData(b1aotx)) {
				query.setParameter("b1aotx", ((Object) null));
			} else {
				query.setParameter("b1aotx", b1aotx);
			}
			if (Functions.isInvalidSqlData(b1c0nb)) {
				query.setParameter("b1c0nb", ((Object) null));
			} else {
				query.setParameter("b1c0nb", b1c0nb);
			}
			if (Functions.isInvalidSqlData(b1pbtx)) {
				query.setParameter("b1pbtx", ((Object) null));
			} else {
				query.setParameter("b1pbtx", b1pbtx);
			}
			if (Functions.isInvalidSqlData(b1item)) {
				query.setParameter("b1item", ((Object) null));
			} else {
				query.setParameter("b1item", b1item);
			}
			if (Functions.isInvalidSqlData(b1azcd)) {
				query.setParameter("b1azcd", ((Object) null));
			} else {
				query.setParameter("b1azcd", b1azcd);
			}
			if (Functions.isInvalidSqlData(b1fgtx)) {
				query.setParameter("b1fgtx", ((Object) null));
			} else {
				query.setParameter("b1fgtx", b1fgtx);
			}
			if (Functions.isInvalidSqlData(b1a1cd)) {
				query.setParameter("b1a1cd", ((Object) null));
			} else {
				query.setParameter("b1a1cd", b1a1cd);
			}
			if (Functions.isInvalidSqlData(b1transeq)) {
				query.setParameter("b1transeq", ((Object) null));
			} else {
				query.setParameter("b1transeq", b1transeq);
			}
			if (Functions.isInvalidSqlData(b1j4tx)) {
				query.setParameter("b1j4tx", ((Object) null));
			} else {
				query.setParameter("b1j4tx", b1j4tx);
			}
			if (Functions.isInvalidSqlData(b1bsnb)) {
				query.setParameter("b1bsnb", ((Object) null));
			} else {
				query.setParameter("b1bsnb", b1bsnb);
			}
			if (Functions.isInvalidSqlData(b1cinb)) {
				query.setParameter("b1cinb", ((Object) null));
			} else {
				query.setParameter("b1cinb", b1cinb);
			}
			if (Functions.isInvalidSqlData(b1cbtx)) {
				query.setParameter("b1cbtx", ((Object) null));
			} else {
				query.setParameter("b1cbtx", b1cbtx);
			}
			if (Functions.isInvalidSqlData(b1czst)) {
				query.setParameter("b1czst", ((Object) null));
			} else {
				query.setParameter("b1czst", b1czst);
			}
			if (Functions.isInvalidSqlData(b1akcd)) {
				query.setParameter("b1akcd", ((Object) null));
			} else {
				query.setParameter("b1akcd", b1akcd);
			}
			query.setParameter("b1aepc", b1aepc, ABOTypes.DecimalType());
			query.setParameter("b1agva", b1agva, ABOTypes.DecimalType());
			if (Functions.isInvalidSqlData(b1alrind)) {
				query.setParameter("b1alrind", ((Object) null));
			} else {
				query.setParameter("b1alrind", b1alrind);
			}
			if (Functions.isInvalidSqlData(b1pstx)) {
				query.setParameter("b1pstx", ((Object) null));
			} else {
				query.setParameter("b1pstx", b1pstx);
			}
			if (Functions.isInvalidSqlData(b1alrus1)) {
				query.setParameter("b1alrus1", ((Object) null));
			} else {
				query.setParameter("b1alrus1", b1alrus1);
			}
			if (Functions.isInvalidSqlData(b1alrus2)) {
				query.setParameter("b1alrus2", ((Object) null));
			} else {
				query.setParameter("b1alrus2", b1alrus2);
			}
			if (Functions.isInvalidSqlData(b1alrus3)) {
				query.setParameter("b1alrus3", ((Object) null));
			} else {
				query.setParameter("b1alrus3", b1alrus3);
			}
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = false;
			//BPHX changes start - fix 158448
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			//BPHX changes end - fix 158448
			//BPHX changes end - fix 157944
//			CSC-Changes start - 155024
			/*resultSet = query.scroll();
			if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();
				// reset
				if (keyType == KeyType.EQUAL) {
					// record with given key value found ?
					if (!isRecordEqual(b1aacd, b1abcd, b1artx, b1astx, b1adnb, b1agtx, b1brnb, b1egnb, b1antx, b1aenb, b1aotx, b1c0nb, b1pbtx,
						b1item, b1azcd, b1fgtx, b1a1cd, b1transeq, b1j4tx, b1bsnb, b1cinb, b1cbtx, b1czst, b1akcd, b1aepc, b1agva, b1alrind, b1pstx,
						b1alrus1, b1alrus2, b1alrus3)) {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						resultSet.close();
						//BPHX changes start - fix 157789
						resultSet = null;
						//BPHX changes start - fix 157789
					}
				}
			} else {
				if (keyType == KeyType.EQUAL) {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
			}*/
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				/*if (keyType == KeyType.EQUAL) {
					// record with given key value found ?
					if (!isRecordEqual(b1aacd, b1abcd, b1artx, b1astx, b1adnb, b1agtx, b1brnb, b1egnb, b1antx, b1aenb, b1aotx, b1c0nb, b1pbtx,
							b1item, b1azcd, b1fgtx, b1a1cd, b1transeq, b1j4tx, b1bsnb, b1cinb, b1cbtx, b1czst, b1akcd, b1aepc, b1agva, b1alrind, b1pstx,
							b1alrus1, b1alrus2, b1alrus3)) {
							status.setSqlCode(DBAccessStatus.DB_EOF);
							rowList.clear();
					}
				}*/
			} else {
				if (keyType == KeyType.EQUAL) {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
			}
			//CSC-Changes end - 155024
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openAsb1cppTOCsr() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Asb1cppTO_CURSOR_NOWHERE");
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = true;
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			//BPHX changes end - fix 157944
//			CSC-Changes start - 155024
			/*resultSet = query.scroll();
			if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();
				// reset
			}*/
			rowList = query.list();
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
			}
			//CSC-Changes end - 155024
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus closeCsr() {
		DBAccessStatus status = new DBAccessStatus();
//		CSC-Changes start - 155024
		/*if (resultSet != null) {
			resultSet.close();
			resultSet = null;
		}*/
		if (rowList != null) {
			rowList.clear();
			rowList = null;
			rowIter = null;
		}
		//CSC-Changes end - 155024
		// BPHX changes start - fix 157870
		lastAsb1cppTO = null;
		// BPHX changes end - fix 157870
		return status;
	}

	public static DBAccessStatus delete(String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb, String b1agtx, int b1brnb,
			int b1egnb, String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd, String b1fgtx, String b1a1cd,
			int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd, ABODecimal b1aepc, ABODecimal b1agva,
			char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Asb1cppTO asb1cppTO = new Asb1cppTO();
			// set data for where clause
			asb1cppTO.getId().setB1aacd(b1aacd);
			asb1cppTO.getId().setB1abcd(b1abcd);
			asb1cppTO.getId().setB1artx(b1artx);
			asb1cppTO.getId().setB1astx(b1astx);
			asb1cppTO.getId().setB1adnb(b1adnb);
			asb1cppTO.getId().setB1agtx(b1agtx);
			asb1cppTO.getId().setB1brnb(b1brnb);
			asb1cppTO.getId().setB1egnb(b1egnb);
			asb1cppTO.getId().setB1antx(b1antx);
			asb1cppTO.getId().setB1aenb(b1aenb);
			asb1cppTO.getId().setB1aotx(b1aotx);
			asb1cppTO.getId().setB1c0nb(b1c0nb);
			asb1cppTO.getId().setB1pbtx(b1pbtx);
			asb1cppTO.getId().setB1item(b1item);
			asb1cppTO.getId().setB1azcd(b1azcd);
			asb1cppTO.getId().setB1fgtx(b1fgtx);
			asb1cppTO.getId().setB1a1cd(b1a1cd);
			asb1cppTO.getId().setB1transeq(b1transeq);
			asb1cppTO.getId().setB1j4tx(b1j4tx);
			asb1cppTO.getId().setB1bsnb(b1bsnb);
			asb1cppTO.getId().setB1cinb(b1cinb);
			asb1cppTO.getId().setB1cbtx(b1cbtx);
			asb1cppTO.getId().setB1czst(b1czst);
			asb1cppTO.getId().setB1akcd(b1akcd);
			asb1cppTO.getId().setB1aepc(b1aepc);
			asb1cppTO.getId().setB1agva(b1agva);
			asb1cppTO.getId().setB1alrind(b1alrind);
			asb1cppTO.getId().setB1pstx(b1pstx);
			asb1cppTO.getId().setB1alrus1(b1alrus1);
			asb1cppTO.getId().setB1alrus2(b1alrus2);
			asb1cppTO.getId().setB1alrus3(b1alrus3);
			asb1cppTO = ((Asb1cppTO) Util.get(session, ((Object) asb1cppTO), ((Serializable) asb1cppTO.getId())));
			if (asb1cppTO == null) {
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			} else {
				session.delete(((Object) asb1cppTO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public void fetch(Object[] colData, Asb1cppTO asb1cppTO) {
		asb1cppTO.getId().setB1aacd(Primitives.getString(colData[(1) - 1]));
		asb1cppTO.getId().setB1abcd(Primitives.getString(colData[(2) - 1]));
		asb1cppTO.getId().setB1artx(Primitives.getString(colData[(3) - 1]));
		asb1cppTO.getId().setB1astx(Primitives.getString(colData[(4) - 1]));
		asb1cppTO.getId().setB1adnb(Primitives.getString(colData[(5) - 1]));
		asb1cppTO.getId().setB1agtx(Primitives.getString(colData[(6) - 1]));
		asb1cppTO.getId().setB1brnb(Primitives.getInt(colData[(7) - 1]));
		asb1cppTO.getId().setB1egnb(Primitives.getInt(colData[(8) - 1]));
		asb1cppTO.getId().setB1antx(Primitives.getString(colData[(9) - 1]));
		asb1cppTO.getId().setB1aenb(Primitives.getInt(colData[(10) - 1]));
		asb1cppTO.getId().setB1aotx(Primitives.getString(colData[(11) - 1]));
		asb1cppTO.getId().setB1c0nb(Primitives.getInt(colData[(12) - 1]));
		asb1cppTO.getId().setB1pbtx(Primitives.getString(colData[(13) - 1]));
		asb1cppTO.getId().setB1item(Primitives.getInt(colData[(14) - 1]));
		asb1cppTO.getId().setB1azcd(Primitives.getString(colData[(15) - 1]));
		asb1cppTO.getId().setB1fgtx(Primitives.getString(colData[(16) - 1]));
		asb1cppTO.getId().setB1a1cd(Primitives.getString(colData[(17) - 1]));
		asb1cppTO.getId().setB1transeq(Primitives.getInt(colData[(18) - 1]));
		asb1cppTO.getId().setB1j4tx(Primitives.getString(colData[(19) - 1]));
		asb1cppTO.getId().setB1bsnb(Primitives.getShort(colData[(20) - 1]));
		asb1cppTO.getId().setB1cinb(Primitives.getShort(colData[(21) - 1]));
		asb1cppTO.getId().setB1cbtx(Primitives.getChar(colData[(22) - 1]));
		asb1cppTO.getId().setB1czst(Primitives.getChar(colData[(23) - 1]));
		asb1cppTO.getId().setB1akcd(Primitives.getString(colData[(24) - 1]));
		asb1cppTO.getId().setB1aepc(new ABODecimal(Primitives.getDecimal(colData[(25) - 1]), 8, 5));
		//FSIT: 152640, Resolution: 55204 - Begin
		//asb1cppTO.getId().setB1agva(new ABODecimal(Primitives.getDecimal(colData[(26) - 1]), 11, 2));
		asb1cppTO.getId().setB1agva(new ABODecimal(Primitives.getDecimal(colData[(26) - 1]), 20, 2));
		//FSIT: 152640, Resolution: 55204 - End
		asb1cppTO.getId().setB1alrind(Primitives.getChar(colData[(27) - 1]));
		asb1cppTO.getId().setB1pstx(Primitives.getString(colData[(28) - 1]));
		asb1cppTO.getId().setB1alrus1(Primitives.getChar(colData[(29) - 1]));
		asb1cppTO.getId().setB1alrus2(Primitives.getChar(colData[(30) - 1]));
		asb1cppTO.getId().setB1alrus3(Primitives.getChar(colData[(31) - 1]));
	}

	public boolean isRecordEqual(String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb, String b1agtx, int b1brnb, int b1egnb,
			String b1antx, int b1aenb, String b1aotx, int b1c0nb, String b1pbtx, int b1item, String b1azcd, String b1fgtx, String b1a1cd,
			int b1transeq, String b1j4tx, short b1bsnb, short b1cinb, char b1cbtx, char b1czst, String b1akcd, ABODecimal b1aepc, ABODecimal b1agva,
			char b1alrind, String b1pstx, char b1alrus1, char b1alrus2, char b1alrus3) {
		Asb1cppTO asb1cppTO = new Asb1cppTO();
//		CSC-Changes start - 155024
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				asb1cppTO = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), asb1cppTO);
			}
			resultSet.previous();
			// reset
		}*/
		if (rowIter != null && rowIter.hasNext()) {
			asb1cppTO = ((Asb1cppTO) rowIter.next());
			rowIter.previous();
		}
		//CSC-Changes end - 155024
		if ((Functions.isInvalidSqlData(b1aacd) || b1aacd.compareTo(asb1cppTO.getId().getB1aacd()) == 0)
			&& (Functions.isInvalidSqlData(b1abcd) || b1abcd.compareTo(asb1cppTO.getId().getB1abcd()) == 0)
			&& (Functions.isInvalidSqlData(b1artx) || b1artx.compareTo(asb1cppTO.getId().getB1artx()) == 0)
			&& (Functions.isInvalidSqlData(b1astx) || b1astx.compareTo(asb1cppTO.getId().getB1astx()) == 0)
			&& (Functions.isInvalidSqlData(b1adnb) || b1adnb.compareTo(asb1cppTO.getId().getB1adnb()) == 0)
			&& (Functions.isInvalidSqlData(b1agtx) || b1agtx.compareTo(asb1cppTO.getId().getB1agtx()) == 0)
			&& (Functions.isInvalidSqlData(b1brnb) || b1brnb == asb1cppTO.getId().getB1brnb())
			&& (Functions.isInvalidSqlData(b1egnb) || b1egnb == asb1cppTO.getId().getB1egnb())
			&& (Functions.isInvalidSqlData(b1antx) || b1antx.compareTo(asb1cppTO.getId().getB1antx()) == 0)
			&& (Functions.isInvalidSqlData(b1aenb) || b1aenb == asb1cppTO.getId().getB1aenb())
			&& (Functions.isInvalidSqlData(b1aotx) || b1aotx.compareTo(asb1cppTO.getId().getB1aotx()) == 0)
			&& (Functions.isInvalidSqlData(b1c0nb) || b1c0nb == asb1cppTO.getId().getB1c0nb())
			&& (Functions.isInvalidSqlData(b1pbtx) || b1pbtx.compareTo(asb1cppTO.getId().getB1pbtx()) == 0)
			&& (Functions.isInvalidSqlData(b1item) || b1item == asb1cppTO.getId().getB1item())
			&& (Functions.isInvalidSqlData(b1azcd) || b1azcd.compareTo(asb1cppTO.getId().getB1azcd()) == 0)
			&& (Functions.isInvalidSqlData(b1fgtx) || b1fgtx.compareTo(asb1cppTO.getId().getB1fgtx()) == 0)
			&& (Functions.isInvalidSqlData(b1a1cd) || b1a1cd.compareTo(asb1cppTO.getId().getB1a1cd()) == 0)
			&& (Functions.isInvalidSqlData(b1transeq) || b1transeq == asb1cppTO.getId().getB1transeq())
			&& (Functions.isInvalidSqlData(b1j4tx) || b1j4tx.compareTo(asb1cppTO.getId().getB1j4tx()) == 0)
			&& (Functions.isInvalidSqlData(b1bsnb) || b1bsnb == asb1cppTO.getId().getB1bsnb())
			&& (Functions.isInvalidSqlData(b1cinb) || b1cinb == asb1cppTO.getId().getB1cinb())
			&& (Functions.isInvalidSqlData(b1cbtx) || b1cbtx == asb1cppTO.getId().getB1cbtx())
			&& (Functions.isInvalidSqlData(b1czst) || b1czst == asb1cppTO.getId().getB1czst())
			&& (Functions.isInvalidSqlData(b1akcd) || b1akcd.compareTo(asb1cppTO.getId().getB1akcd()) == 0)
			&& (Functions.isInvalidSqlData(b1aepc) || b1aepc.compareTo(asb1cppTO.getId().getB1aepc()) == 0)
			&& (Functions.isInvalidSqlData(b1agva) || b1agva.compareTo(asb1cppTO.getId().getB1agva()) == 0)
			&& (Functions.isInvalidSqlData(b1alrind) || b1alrind == asb1cppTO.getId().getB1alrind())
			&& (Functions.isInvalidSqlData(b1pstx) || b1pstx.compareTo(asb1cppTO.getId().getB1pstx()) == 0)
			&& (Functions.isInvalidSqlData(b1alrus1) || b1alrus1 == asb1cppTO.getId().getB1alrus1())
			&& (Functions.isInvalidSqlData(b1alrus2) || b1alrus2 == asb1cppTO.getId().getB1alrus2())
			&& (Functions.isInvalidSqlData(b1alrus3) || b1alrus3 == asb1cppTO.getId().getB1alrus3())) {
			return true;
		}
		return false;
	}

	public DBAccessStatus deleteAll() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			//BPHX changes start - fix 157118
			session.flush();
			session.clear();
			//BPHX changes end - fix 157118
			Query query = session.getNamedQuery("Asb1cppTO_DELETE_ALL");
			int result = query.executeUpdate();
			status.setNumOfRows(result);
			//BPHX changes start - fix 157118
			//session.flush();
			//BPHX changes end - fix 157118
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}
	public DBAccessStatus closeClcursor2() {
		DBAccessStatus status = new DBAccessStatus();
		if (clcursor2 != null) {
			clcursor2.close();
		}
		return status;
	}
	public DBAccessStatus openClcursor2(String b1artx, String b1astx, String b1adnb, String b1abcd, String b1aacd) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Asb1cppDAO_openClcursor2");
			query.setString("b1adnb", b1adnb);
			query.setString("b1aacd", b1aacd);
			query.setString("b1abcd", b1abcd);
			query.setString("b1artx", b1artx);
			query.setString("b1astx", b1astx);

			clcursor2 = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Asb1cppTO fetchClcursor2(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO asb1cppTOResult = asb1cppTO;
		if (clcursor2 != null && clcursor2.next()) {
			Object obj = clcursor2.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				asb1cppTOResult = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) clcursor2.get()), asb1cppTOResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		asb1cppTOResult.setDBAccessStatus(status);
		return asb1cppTOResult;
	}
	public static DBAccessStatus deleteByB1aacdRec(String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Asb1cppDAO_deleteByB1aacdRec");
			query.setString("b1aacd", b1aacd);
			query.setString("b1abcd", b1abcd);
			query.setString("b1artx", b1artx);
			query.setString("b1astx", b1astx);
			query.setString("b1adnb", b1adnb);

			int result = query.executeUpdate();
			status.setNumOfRows(result);
			session.flush();

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}
	
	//FSIT#176105 RESL#63502 Start
	public static Asb1cppTO selectBySeq(Asb1cppTO asb1cppTO, String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb,
			String b1azcd, String b1fgtx,String b1a1cd, String b1j4tx, short b1bsnb, short b1cinb) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO localAsb1cppTO = asb1cppTO;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Asb1cppDAO_SelectBySEQ");
			
			query.setString("b1aacd",b1aacd);
			query.setString("b1abcd",b1abcd);
			query.setString("b1artx",b1artx);
			query.setString("b1astx",b1astx);
			query.setString("b1adnb",b1adnb);
		
			
			query.setString("b1azcd",b1azcd);
			query.setString("b1fgtx",b1fgtx);
			query.setString("b1a1cd",b1a1cd);
			
			query.setString("b1j4tx",b1j4tx);
			query.setShort("b1bsnb",b1bsnb);
			query.setShort("b1cinb",b1cinb);
			List resultList = query.list();
			
		
			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localAsb1cppTO = (Asb1cppTO) obj;
				} else {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
				if (resultList.size() > 1) {
					status.setSqlCode(DBAccessStatus.DB_MULTIPLE_RECORDS);
				}
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		localAsb1cppTO.setDBAccessStatus(status);
		return localAsb1cppTO;
	}
	//FSIT:161121 RESO:58707 Start
	public DBAccessStatus openClcursor3(String b1aacd, String b1abcd, String b1artx, String b1astx, String b1adnb) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");
			Query query = session.getNamedQuery("Asb1cppDAO_openClcursor3");
			query.setString("b1adnb", b1adnb);
			query.setString("b1aacd", b1aacd);
			query.setString("b1abcd", b1abcd);
			query.setString("b1artx", b1artx);
			query.setString("b1astx", b1astx);

			clcursor3 = query.scroll();
			status = new DBAccessStatus(0);
		} catch (HibernateException he) {
			status = new DBAccessStatus(he);
		}
		return status;
	}
	public Asb1cppTO fetchClcursor3(Asb1cppTO asb1cppTO) {
		DBAccessStatus status = new DBAccessStatus();
		Asb1cppTO asb1cppTOResult = asb1cppTO;
		if (clcursor3 != null && clcursor3.next()) {
			Object obj = clcursor3.get(0);
			if (Asb1cppTO.class.isInstance(obj)) {
				asb1cppTOResult = ((Asb1cppTO) obj);
			} else {
				fetch(((Object[]) clcursor3.get()), asb1cppTOResult);
			}
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		asb1cppTOResult.setDBAccessStatus(status);
		return asb1cppTOResult;
	}
	
	public DBAccessStatus closeClcursor3() {
		DBAccessStatus status = new DBAccessStatus();
		if (clcursor3 != null) {
			clcursor3.close();
		}
		return status;
	}
	//FSIT:161121 RESO:58707 End
	//RSK - START
	public static String fetchUnit( String b1aacd,
			 String b1abcd,
			 String b1artx,
			 String b1astx,
			 String b1adnb,
			 String b1agtx,
			 int b1brnb,
			 int b1egnb,
			 String b1antx,
			 int b1aenb){
		
		String unit = null;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");			
			Criteria criteria=session.createCriteria(Asb1cppTO.class).setMaxResults(1);
			criteria.add(Restrictions.eq("id.b1aacd",b1aacd ));
			criteria.add(Restrictions.eq("id.b1abcd",b1abcd));
			criteria.add(Restrictions.eq("id.b1artx",b1artx ));
			criteria.add(Restrictions.eq("id.b1astx",b1astx ));	
			criteria.add(Restrictions.eq("id.b1adnb",b1adnb ));
			criteria.add(Restrictions.eq("id.b1agtx",b1agtx ));
			criteria.add(Restrictions.eq("id.b1brnb",b1brnb ));
			criteria.add(Restrictions.eq("id.b1egnb",b1egnb ));
			criteria.add(Restrictions.eq("id.b1antx",b1antx ));
			criteria.add(Restrictions.eq("id.b1aenb",b1aenb ));
			List<Asb1cppTO> list=criteria.list();			
			if (list.size()>0) {
				unit=list.get(0).getB1j4tx();
			}else{
				unit = null;
			}
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}		
		
		return unit;
		
	}
	
	public static String fetchUnit1( String b1aacd,
			 String b1abcd,
			 String b1artx,
			 String b1astx,
			 String b1adnb,
			 int b1brnb,
			 int b1egnb,
			 int b1aenb){
		
		String unit = null;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASB1CPP");			
			Criteria criteria=session.createCriteria(Asb1cppTO.class).setMaxResults(1);
			criteria.add(Restrictions.eq("id.b1aacd",b1aacd ));
			criteria.add(Restrictions.eq("id.b1abcd",b1abcd));
			criteria.add(Restrictions.eq("id.b1artx",b1artx ));
			criteria.add(Restrictions.eq("id.b1astx",b1astx ));	
			criteria.add(Restrictions.eq("id.b1adnb",b1adnb ));
			criteria.add(Restrictions.eq("id.b1brnb",b1brnb ));
			criteria.add(Restrictions.eq("id.b1egnb",b1egnb ));
			criteria.add(Restrictions.eq("id.b1aenb",b1aenb ));
			List<Asb1cppTO> list=criteria.list();			
			if (list.size()>0) {
				unit=list.get(0).getB1j4tx();
			}else{
				unit = null;
			}
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}		
		
		return unit;
		
	}
	
	//RSK - END

}