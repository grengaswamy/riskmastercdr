// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.dao;


import java.util.List;
import java.util.ListIterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.Primitives;

import com.csc.pt.svc.rsk.to.Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO;

	public class Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO {
		private List rowList = null;
		private ListIterator rowIter = null;
		private List rowList1 = null;
		private int pagesize = bphx.c2ab.util.ConfigSettings.getDefaultCursorPageSize();
				
		public DBAccessStatus openTPASearchCursor(boolean isWildCardSearchOn, String wsMco,String wsPco,String wsAgen ,String wsSegment,String wsBusUnit
				, String wsLoc, String wsSymb,String wsPolNo,String wsMod,String wsLastNam, String wsFirstName, String wsLob, String wsCustNo,String wsZip,
				String phone1,String fein, String groupNo, String wsCity,String wsState,String sortName, String wsMailing,
				char var1,char var2, String var3,char var4, short maxNumRecords, String wildSearchFlag) {
			DBAccessStatus status = new DBAccessStatus();
			try {
				Session session = HibernateSessionFactory.current().getSession("PMSP0200BASCLT1400BASCLT0100BASCLT0300BASPPROFIT");
				Query query  = null;
				query = session.getNamedQuery("Pmsp0200Basclt1400Basclt0100Basclt0300Baspprofit_INS_CURSOR");
					query.setParameter("wsLoc", wsLoc+"%");
					if(isWildCardSearchOn){
						if( "".equals(wsSymb))
						{
							wsSymb = wsSymb + "%";
						}
						query.setParameter("wsSymb", wsSymb);
					} else {
						query.setParameter("wsSymb", wsSymb+"%");
					}
					query.setParameter("wsPolNo", wsPolNo+"%");
					query.setParameter("wsMod", wsMod+"%");
					if(isWildCardSearchOn){
						if( "".equals(wsLastNam))
						{
							wsLastNam = wsLastNam + "%";
						}
						query.setParameter("wsLastNam", wsLastNam);
					} else {
						query.setParameter("wsLastNam", wsLastNam+"%");
					}
					if(isWildCardSearchOn){
						if( "".equals(wsFirstName))
						{
							wsFirstName = wsFirstName + "%";
						}
						query.setParameter("wsFirstName", wsFirstName);
					} else {
						query.setParameter("wsFirstName", wsFirstName+"%");
					}
					if(isWildCardSearchOn){
						if( "".equals(wsLob))
						{
							wsLob = wsLob + "%";
						}
						query.setParameter("wsLob", wsLob);
					} else {
						query.setParameter("wsLob", wsLob+"%");
					}
					query.setParameter("wsCustNo", wsCustNo+"%");
					query.setParameter("wsPhone1", phone1+"%");
					query.setParameter("wsFein", fein+"%");
					query.setParameter("wsGroupNo", groupNo+"%");
					if(isWildCardSearchOn){
						if( "".equals(wsCity))
						{
							wsCity = wsCity + "%";
						}
						query.setParameter("wsCity", wsCity);
					} else {
						query.setParameter("wsCity", wsCity+"%");
					}
					if(isWildCardSearchOn){
						if( "".equals(wsState))
						{
							wsState = wsState + "%";
						}
						query.setParameter("wsState", wsState);
					} else {
						query.setParameter("wsState", wsState+"%");
					}
					query.setParameter("wsZip", wsZip+"%");
					query.setParameter("wsSortName", sortName+"%");
					query.setParameter("wsMailing", wsMailing+"%");
					query.setParameter("var1", var1);
					query.setParameter("var2", var2);
					query.setParameter("var3", var3);
					query.setParameter("var4", var4);
					query.setParameter("wsMco", wsMco+"%");
					query.setParameter("wsPco", wsPco+"%");
					query.setParameter("wsAgen", wsAgen+"%");
					query.setParameter("wsSegment", wsSegment+"%");
					query.setParameter("wsBusUnit", wsBusUnit+"%");
				closeCsr();
				query.setCacheable(true);
				if(wildSearchFlag!=null && wildSearchFlag.trim().equalsIgnoreCase("ON")){
					query.setMaxResults(maxNumRecords+1); //To check weather search criteria satisfies more than maxNumRecords
				}
				else{
				query.setMaxResults(pagesize);
				}
				rowList = query.list();
				if (rowList != null && rowList.size() > 0) {
					rowIter = rowList.listIterator();
				} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
				} }
			catch (HibernateException ex) {
					status.setException(ex);
				}
				return status;
			}

		
    	public Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO fetchTPASearchCursor(Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO,boolean wsSubIdFlag) {
			DBAccessStatus status = new DBAccessStatus();
			Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResult = pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO;
			if (rowIter != null && rowIter.hasNext()) {
				Object obj[] = ((Object[]) rowIter.next());
				fetchTPA(obj, pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResult,wsSubIdFlag);

			} else {
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			}
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResult.setDBAccessStatus(status);
			return pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResult;
		}
		
		
		
		public void fetchTPA(Object[] colData, Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult, boolean wsSubIdFlag) {
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setTrans0Stat(Primitives.getChar(colData[(1) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setEffYr(Primitives.getString(colData[(2) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setEffMo(Primitives.getString(colData[(3) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setEffDa(Primitives.getString(colData[(4) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setExpYr(Primitives.getString(colData[(5) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setExpMo(Primitives.getString(colData[(6) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setExpDa(Primitives.getString(colData[(7) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setRisk0state(Primitives.getString(colData[(8) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFillr1(Primitives.getString(colData[(9) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setRpt0agt0nr(Primitives.getChar(colData[(10) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFillr2(Primitives.getString(colData[(11) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFillr3(Primitives.getChar(colData[(12) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFillr4(Primitives.getString(colData[(13) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setTot0ag0prm(Primitives.getBigDecimal(colData[(14) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setLine0bus(Primitives.getString(colData[(15) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setIssue0code(Primitives.getChar(colData[(16) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setPay0code(Primitives.getChar(colData[(17) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setMode0code(Primitives.getChar(colData[(18) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setSort0name(Primitives.getString(colData[(19) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setRenewal0cd(Primitives.getChar(colData[(20) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setCust0no(Primitives.getString(colData[(21) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setSpec0use0a(Primitives.getString(colData[(22) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setSpec0use0b(Primitives.getString(colData[(23) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setZip0post(Primitives.getString(colData[(24) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setAdd0line01(Primitives.getString(colData[(25) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setAdd0line03(Primitives.getString(colData[(26) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setAdd0line04(Primitives.getString(colData[(27) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setType0act(Primitives.getString(colData[(28) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setCanceldate(Primitives.getString(colData[(29) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setMrsseq(Primitives.getString(colData[(30) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setLocation(Primitives.getString(colData[(31) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setMasterCo(Primitives.getString(colData[(32) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setSymbol(Primitives.getString(colData[(33) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setPolicy0Num(Primitives.getString(colData[(34) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setModule(Primitives.getString(colData[(35) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setCltseqnum(Primitives.getLong(colData[(36) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setAddrseqnum(Primitives.getShort(colData[(37) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setGroupno(Primitives.getString(colData[(38) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setNametype(Primitives.getChar(colData[(39) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setNamestatus(Primitives.getChar(colData[(40) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setClientname(Primitives.getString(colData[(41) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFirstname(Primitives.getString(colData[(42) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setMidname(Primitives.getString(colData[(43) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setDba(Primitives.getString(colData[(44) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setPhone1(Primitives.getString(colData[(45) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setFedtaxid(Primitives.getString(colData[(46) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setSsn(Primitives.getString(colData[(47) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setAddrln1(Primitives.getString(colData[(48) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setCity(Primitives.getString(colData[(49) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setState(Primitives.getString(colData[(50) - 1]));
			pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTOResultResult.setZipcode(Primitives.getString(colData[(51) - 1]));
			
		}
	
		public DBAccessStatus closeCsr() {
			DBAccessStatus status = new DBAccessStatus();
			if (rowList != null) {
				rowList.clear();
				rowList = null;
				rowIter = null;
			}
			return status;
		}

	}



