package com.csc.pt.svc.rsk.dao;

import java.io.Serializable;
import java.util.List;
import java.util.ListIterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.validator.ClassValidator;
import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.hibernate.Util;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.Primitives;

import com.csc.pt.svc.data.to.AsafrepTO;

public class AsafrepDAO {
	//private ScrollableResults resultSet;
	private ListIterator rowIter = null;
	private List rowList = null;
	private static ClassValidator validator = new ClassValidator(AsafrepTO.getTOClass());
	//BPHX changes start - fix 157944
	//BPHX changes start - fix 158448
	//private int pagesize = Integer.MAX_VALUE;
	private int pagesize = bphx.c2ab.util.ConfigSettings.getDefaultCursorPageSizeForFilesWoUniqKey();
	private boolean cursorFirstTime = true;
	private AsafrepTO firstAsafrepTO = new AsafrepTO();
	//BPHX changes end - fix 158448
	private int recnum = 1;
	private int pagenum = 0;
	private boolean cursorNoWhere = false;
	// BPHX changes start - fix 157870
	//private AsafrepTO lastAsafrepTO = new AsafrepTO();
	private AsafrepTO lastAsafrepTO = null;
	// BPHX changes end - fix 157870
	//BPHX changes end - fix 157944

	public AsafrepTO fetchNext(AsafrepTO asafrepTO) {
		DBAccessStatus status = new DBAccessStatus();
		AsafrepTO localAsafrepTO = asafrepTO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			// BPHX changes start - fix 157870
			if (lastAsafrepTO == null) {
			// BPHX changes end - fix 157870
				//BPHX changes Start - 157944
				pagenum = 0;
				//BPHX changes End - 157944
				status = openAsafrepTOCsr();
			// BPHX changes start - fix 157870
			} else {
				if (lastAsafrepTO.getDBAccessStatus().isSuccess()) {
					//last READ was successful
					AsafrepTO lastAsafrepTOsave = lastAsafrepTO;
					status = openAsafrepTOCsr(KeyType.GREATER_OR_EQ , 
						lastAsafrepTO.getAfadtx(), 
						lastAsafrepTO.getAfagtx(), 
						lastAsafrepTO.getAfahtx(), 
						lastAsafrepTO.getAfaavn(), 
						lastAsafrepTO.getAfakdt(), 
						lastAsafrepTO.getAfabtm());
					//BPHX changes start - fix 158659
					if (rowList != null && rowList.size() > 0) {
					//BPHX changes end - fix 158659
						if (isRecordEqual(lastAsafrepTOsave.getAfadtx(), 
								lastAsafrepTOsave.getAfagtx(), 
								lastAsafrepTOsave.getAfahtx(), 
								lastAsafrepTOsave.getAfaavn(), 
								lastAsafrepTOsave.getAfakdt(), 
								lastAsafrepTOsave.getAfabtm())) {
							rowIter.next(); //skip record already processed
						}
					//BPHX changes start - fix 158659
					} else {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						localAsafrepTO = new AsafrepTO();
						localAsafrepTO.setDBAccessStatus(status);
						return localAsafrepTO;
					}
					//BPHX changes end - fix 158659
				} else {
					//unsuccessful READ
					status.setSqlCode(DBAccessStatus.DB_EOF);
					localAsafrepTO = new AsafrepTO();
				}
			}
			// BPHX changes end - fix 157870
		}
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (AsafrepTO.class.isInstance(obj)) {
				localAsafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsafrepTO);
			}*/
		if (rowIter != null && rowIter.hasNext()) {
			Object obj = rowIter.next();
			if (AsafrepTO.class.isInstance(obj)) {
				localAsafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) obj), localAsafrepTO);
			}
		//BPHX changes start - fix 157944
//		} else {
//			status.setSqlCode(DBAccessStatus.DB_EOF);
//			localAsafrepTO = new AsafrepTO();
//		}
			//BPHX changes start - fix 158448
			//lastAsafrepTO = localAsafrepTO;
			if(cursorFirstTime) {
				firstAsafrepTO = localAsafrepTO;
				cursorFirstTime = false;
			}
			//BPHX changes end - fix 158448
			recnum++;
		} else {
			//if (resultSet != null && recnum >= pagesize - 1) {
			if (rowIter != null && recnum >= pagesize - 1) {
				if(!cursorNoWhere) {
					//BPHX changes start - fix 158448
					//Open cursor with first key to get next page
					pagenum++;
					//BPHX changes end - fix 158448
				status = openAsafrepTOCsr(KeyType.GREATER_OR_EQ , 
					firstAsafrepTO.getAfadtx(), 
					firstAsafrepTO.getAfagtx(), 
					firstAsafrepTO.getAfahtx(), 
					firstAsafrepTO.getAfaavn(), 
					firstAsafrepTO.getAfakdt(), 
					firstAsafrepTO.getAfabtm());
					//resultSet.next();
					//BPHX changes start - fix 158448
					//rowIter.next();			// Throw away record already processed
					//BPHX changes end - fix 158448
				} else {
					pagenum++;
					status = openAsafrepTOCsr();		//Open cursor to nowhere with pagenum incremented to get next page of rows
				}
				localAsafrepTO = fetchNext(localAsafrepTO);
			} else {
				status.setSqlCode(DBAccessStatus.DB_EOF);
				localAsafrepTO = new AsafrepTO();
			}
		}
		//BPHX changes end - fix 157944
		localAsafrepTO.setDBAccessStatus(status);
		return localAsafrepTO;
	}

	public AsafrepTO fetchPrevious(AsafrepTO asafrepTO) {
		DBAccessStatus status = new DBAccessStatus();
		AsafrepTO localAsafrepTO = asafrepTO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openAsafrepTOCsr();
		}
		/*if (resultSet != null && resultSet.previous()) {
			Object obj = resultSet.get(0);
			if (AsafrepTO.class.isInstance(obj)) {
				localAsafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsafrepTO);
			}*/
		if (rowIter != null && rowIter.hasPrevious()) {
			localAsafrepTO = ((AsafrepTO) rowIter.previous());
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsafrepTO = new AsafrepTO();
		}
		localAsafrepTO.setDBAccessStatus(status);
		return localAsafrepTO;
	}

	public AsafrepTO fetchFirst(AsafrepTO asafrepTO) {
		DBAccessStatus status = new DBAccessStatus();
		AsafrepTO localAsafrepTO = asafrepTO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openAsafrepTOCsr();
		}
		/*if (resultSet != null && resultSet.first()) {
			Object obj = resultSet.get(0);
			if (AsafrepTO.class.isInstance(obj)) {
				localAsafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsafrepTO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localAsafrepTO = ((AsafrepTO) rowList.get(0));
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsafrepTO = new AsafrepTO();
		}
		localAsafrepTO.setDBAccessStatus(status);
		return localAsafrepTO;
	}

	public AsafrepTO fetchLast(AsafrepTO asafrepTO) {
		DBAccessStatus status = new DBAccessStatus();
		AsafrepTO localAsafrepTO = asafrepTO;
		//if (resultSet == null) {
		if (rowList == null) {
			// cursor not opened, try opening
			status = openAsafrepTOCsr();
		}
		/*if (resultSet != null && resultSet.last()) {
			Object obj = resultSet.get(0);
			if (AsafrepTO.class.isInstance(obj)) {
				localAsafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), localAsafrepTO);
			}*/
		if (rowIter != null && (rowList.size() > 0)) {
			localAsafrepTO = ((AsafrepTO) rowList.get(rowList.size() - 1));
		} else {
			status.setSqlCode(DBAccessStatus.DB_EOF);
			localAsafrepTO = new AsafrepTO();
		}
		localAsafrepTO.setDBAccessStatus(status);
		return localAsafrepTO;
	}

	// BPHX changes start - fix 157870
	public AsafrepTO fetchById(AsafrepTO asafrepTO, String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		AsafrepTO localAsafrepTO = asafrepTO;
		closeCsr();
		localAsafrepTO = selectById(asafrepTO, afadtx, afagtx, afahtx, afaavn, afakdt, afabtm);
		lastAsafrepTO = localAsafrepTO;
		return localAsafrepTO;
	}
	// BPHX changes end - fix 157870

	public static AsafrepTO selectById(AsafrepTO asafrepTO, String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		DBAccessStatus status = new DBAccessStatus();
		AsafrepTO localAsafrepTO = asafrepTO;
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			localAsafrepTO.getId().setAfadtx(afadtx);
			localAsafrepTO.getId().setAfagtx(afagtx);
			localAsafrepTO.getId().setAfahtx(afahtx);
			localAsafrepTO.getId().setAfaavn(afaavn);
			localAsafrepTO.getId().setAfakdt(afakdt);
			localAsafrepTO.getId().setAfabtm(afabtm);
			localAsafrepTO = ((AsafrepTO) Util.get(session, ((Object) localAsafrepTO), ((Serializable) asafrepTO.getId())));
			if (localAsafrepTO == null) {
				localAsafrepTO = asafrepTO;
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		localAsafrepTO.setDBAccessStatus(status);
		return localAsafrepTO;
	}

	public static DBAccessStatus update(AsafrepTO asafrepTO, String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			InvalidValue[] errors = validator.getInvalidValues(((Object) asafrepTO));
			if (errors.length > 0) {
				throw (new InvalidStateException(errors));
			}
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			if (asafrepTO.getId() == null) {
				Query query = session.getNamedQuery("AsafrepTO_UPDATE");
				query.setParameter("afadtx", afadtx);
				query.setParameter("afagtx", afagtx);
				query.setParameter("afahtx", afahtx);
				query.setParameter("afaavn", afaavn);
				query.setParameter("afakdt", afakdt);
				query.setParameter("afabtm", afabtm);
				query.executeUpdate();
				selectById(asafrepTO, afadtx, afagtx, afahtx, afaavn, afakdt, afabtm);
			} else {
				AsafrepTO localAsafrepTO = ((AsafrepTO) Util.get(session, ((Object) asafrepTO), ((Serializable) asafrepTO.getId())));
				if (localAsafrepTO == null) {
					status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
				} else {
					session.merge(asafrepTO);
					session.flush();
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public static DBAccessStatus insert(AsafrepTO asafrepTO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			if (Util.get(session, ((Object) asafrepTO), ((Serializable) asafrepTO.getId())) != null) {
				status.setSqlCode(DBAccessStatus.DB_DUPLICATE);
			} else {
				if (session.contains(((Object) asafrepTO))) {
					session.evict(((Object) asafrepTO));
				}
				InvalidValue[] errors = validator.getInvalidValues(((Object) asafrepTO));
				if (errors.length > 0) {
					throw (new InvalidStateException(errors));
				}
				session.save(((Object) asafrepTO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		} catch (InvalidStateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openAsafrepTOCsr(int keyType, String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			Query query = null;
			if (keyType == KeyType.GREATER) {
				query = session.getNamedQuery("AsafrepTO_CURSOR_GT");
			} else if (keyType == KeyType.EQUAL) { 
			   query = session.getNamedQuery("AsafrepTO_CURSOR_EQ"); 
			} else {
				// default
				query = session.getNamedQuery("AsafrepTO_CURSOR_GTE");
			}
			if (Functions.isInvalidSqlData(afadtx)) {
				query.setParameter("afadtx", ((Object) null));
			} else {
				query.setParameter("afadtx", afadtx);
			}
			if (Functions.isInvalidSqlData(afagtx)) {
				query.setParameter("afagtx", ((Object) null));
			} else {
				query.setParameter("afagtx", afagtx);
			}
			if (Functions.isInvalidSqlData(afahtx)) {
				query.setParameter("afahtx", ((Object) null));
			} else {
				query.setParameter("afahtx", afahtx);
			}
			if (Functions.isInvalidSqlData(afaavn)) {
				query.setParameter("afaavn", ((Object) null));
			} else {
				query.setParameter("afaavn", afaavn);
			}
			if (Functions.isInvalidSqlData(afakdt)) {
				query.setParameter("afakdt", ((Object) null));
			} else {
				query.setParameter("afakdt", afakdt);
			}
			if (Functions.isInvalidSqlData(afabtm)) {
				query.setParameter("afabtm", ((Object) null));
			} else {
				query.setParameter("afabtm", afabtm);
			}
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = false;
			//BPHX changes start - fix 158448
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			//BPHX changes end - fix 158448
			//BPHX changes end - fix 157944
			//resultSet = query.scroll();
			rowList = query.list();
			/*if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();*/
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				// reset
				/*if (keyType == KeyType.EQUAL) {
					// record with given key value found ?
					if (!isRecordEqual(afadtx, afagtx, afahtx, afaavn, afakdt, afabtm)) {
						status.setSqlCode(DBAccessStatus.DB_EOF);
						//resultSet.close();
						rowList.clear();
						//BPHX changes start - fix 157789
						//resultSet = null;
						//BPHX changes end - fix 157789
					}
				}*/
			} else {
				if (keyType == KeyType.EQUAL) {
					status.setSqlCode(DBAccessStatus.DB_EOF);
				}
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus openAsafrepTOCsr() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			Query query = session.getNamedQuery("AsafrepTO_CURSOR_NOWHERE");
			closeCsr();
			query.setCacheable(true);
			//BPHX changes start - fix 157944
			query.setMaxResults(pagesize);
			recnum = 0;
			cursorNoWhere = true;
			if(pagenum > 0)
				query.setFirstResult(pagenum * pagesize);
			//BPHX changes end - fix 157944
			//resultSet = query.scroll();
			rowList = query.list();
			/*if (resultSet != null && resultSet.next()) {
				resultSet.beforeFirst();*/
			if (rowList != null && rowList.size() > 0) {
				rowIter = rowList.listIterator();
				// reset
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public DBAccessStatus closeCsr() {
		DBAccessStatus status = new DBAccessStatus();
		/*if (resultSet != null) {
			resultSet.close();
			resultSet = null;*/
		if (rowList != null) {
			rowList.clear();
			rowList = null;
			rowIter = null;
		}
		// BPHX changes start - fix 157870
		lastAsafrepTO = null;
		// BPHX changes end - fix 157870
		return status;
	}

	public static DBAccessStatus delete(String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			AsafrepTO asafrepTO = new AsafrepTO();
			// set data for where clause
			asafrepTO.getId().setAfadtx(afadtx);
			asafrepTO.getId().setAfagtx(afagtx);
			asafrepTO.getId().setAfahtx(afahtx);
			asafrepTO.getId().setAfaavn(afaavn);
			asafrepTO.getId().setAfakdt(afakdt);
			asafrepTO.getId().setAfabtm(afabtm);
			asafrepTO = ((AsafrepTO) Util.get(session, ((Object) asafrepTO), ((Serializable) asafrepTO.getId())));
			if (asafrepTO == null) {
				status.setSqlCode(DBAccessStatus.DB_REC_NOTFOUND);
			} else {
				session.delete(((Object) asafrepTO));
				session.flush();
			}
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}

	public void fetch(Object[] colData, AsafrepTO asafrepTO) {
		asafrepTO.getId().setAfadtx(Primitives.getString(colData[(1) - 1]));
		asafrepTO.getId().setAfagtx(Primitives.getString(colData[(2) - 1]));
		asafrepTO.getId().setAfahtx(Primitives.getString(colData[(3) - 1]));
		asafrepTO.getId().setAfaavn(Primitives.getString(colData[(4) - 1]));
		asafrepTO.getId().setAfakdt(Primitives.getInt(colData[(5) - 1]));
		asafrepTO.getId().setAfabtm(Primitives.getInt(colData[(6) - 1]));
	}

	public boolean isRecordEqual(String afadtx, String afagtx, String afahtx, String afaavn, int afakdt, int afabtm) {
		AsafrepTO asafrepTO = new AsafrepTO();
		/*if (resultSet != null && resultSet.next()) {
			Object obj = resultSet.get(0);
			if (AsafrepTO.class.isInstance(obj)) {
				asafrepTO = ((AsafrepTO) obj);
			} else {
				fetch(((Object[]) resultSet.get()), asafrepTO);
			}
			resultSet.previous();*/
		if (rowIter != null && rowIter.hasNext()) {
			asafrepTO = ((AsafrepTO) rowIter.next());
			rowIter.previous();
			// reset
		}
		if ((Functions.isInvalidSqlData(afadtx) || afadtx.compareTo(asafrepTO.getId().getAfadtx()) == 0)
			&& (Functions.isInvalidSqlData(afagtx) || afagtx.compareTo(asafrepTO.getId().getAfagtx()) == 0)
			&& (Functions.isInvalidSqlData(afahtx) || afahtx.compareTo(asafrepTO.getId().getAfahtx()) == 0)
			&& (Functions.isInvalidSqlData(afaavn) || afaavn.compareTo(asafrepTO.getId().getAfaavn()) == 0)
			&& (Functions.isInvalidSqlData(afakdt) || afakdt == asafrepTO.getId().getAfakdt())
			&& (Functions.isInvalidSqlData(afabtm) || afabtm == asafrepTO.getId().getAfabtm())) {
			return true;
		}
		return false;
	}

	public DBAccessStatus deleteAll() {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("ASAFREP");
			//BPHX changes start - fix 157118
			session.flush();
			session.clear();
			//BPHX changes end - fix 157118
			Query query = session.getNamedQuery("AsafrepTO_DELETE_ALL");
			int result = query.executeUpdate();
			status.setNumOfRows(result);
			//BPHX changes start - fix 157118
			//session.flush();
			//BPHX changes end - fix 157118
		} catch (HibernateException ex) {
			status.setException(ex);
		}
		return status;
	}
	
	//RSK - Start
	public static String selectInsLine(){
		String wsInsLine = null;
		return wsInsLine;
	}
}