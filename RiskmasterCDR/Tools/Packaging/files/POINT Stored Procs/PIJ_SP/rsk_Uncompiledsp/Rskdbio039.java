package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicCallException;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import com.csc.pt.svc.common.Ws03RtrvMsgWorkArea;
import com.csc.pt.svc.common.Ws04ErrorDetailArea;
import com.csc.pt.svc.common.WsAddlIntsCsrSw;
import com.csc.pt.svc.common.WsFeinCsrSw;
import com.csc.pt.svc.common.WsInsCsrSw;
import com.csc.pt.svc.ctrl.Rec00fmtRec3;
import com.csc.pt.svc.ctrl.to.Pmsl0004RecTO;
import com.csc.pt.svc.ctrl.to.Pmsp0200RecTO;
import com.csc.pt.svc.ctrl.to.Pmsp1200RecTO;
import com.csc.pt.svc.ctrl.to.Pmspwc04RecTO;
import com.csc.pt.svc.custom.common.Cpyerrhrtn;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Pmsp0200Pmsp1200DAO;
import com.csc.pt.svc.data.dao.Pmsp0200Pmspwc04DAO;
import com.csc.pt.svc.data.dao.Pmspap00DAO;
import com.csc.pt.svc.data.dao.Pmspmm80DAO;
import com.csc.pt.svc.data.dao.Pmspwc04DAO;
import com.csc.pt.svc.data.to.Pmsp0200Pmsp1200TO;
import com.csc.pt.svc.data.to.Pmsp0200Pmspwc04TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.data.to.Pmspmm80TO;
import com.csc.pt.svc.db.BcListEntryArr37;
import com.csc.pt.svc.db.FeinIndicator;
import com.csc.pt.svc.rsk.to.Rskdbio039TO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.pay.ErrorListRecord;

/**
*/

public class Rskdbio039 {
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private Cpyerrhrtn cpyerrhrtn = new Cpyerrhrtn();
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	private Rskdbio039TO rskdbio039TO = new Rskdbio039TO();
	private Pmspap00DAO pmspap00dao =  new Pmspap00DAO();
	private Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	private Pmsp0200Pmsp1200DAO pmsp0200Pmsp1200DAO = new Pmsp0200Pmsp1200DAO();
	private Pmsp0200Pmspwc04DAO pmsp0200Pmspwc04DAO = new Pmsp0200Pmspwc04DAO();
	private Pmsl0004RecTO pmsl0004RecTO = new Pmsl0004RecTO();
	private Pmsp0200RecTO pmsp0200RecTO = new Pmsp0200RecTO();
	private Pmsp1200RecTO pmsp1200RecTO = new Pmsp1200RecTO();
	private Pmspwc04RecTO pmspwc04RecTO = new Pmspwc04RecTO();
	private Rec00fmtRec3 rec00fmtRec3 = new Rec00fmtRec3();
	private Ws03RtrvMsgWorkArea ws03RtrvMsgWorkArea = new Ws03RtrvMsgWorkArea();
	private Ws04ErrorDetailArea ws04ErrorDetailArea = new Ws04ErrorDetailArea();
	private ErrorListRecord errorListRecord = new ErrorListRecord();
	private Pmsp0200Pmsp1200TO pmsp0200Pmsp1200TO =  new Pmsp0200Pmsp1200TO();
	private Pmsp0200Pmspwc04TO pmsp0200Pmspwc04TO =  new Pmsp0200Pmspwc04TO();
	private Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	private Pmspwc04DAO pmspwc04dao =  new Pmspwc04DAO();
	private char holdBcTrans0stat = ' ';
	private String holdBcSymbol = "";
	private String holdBcPolicy0num = "";
	private String holdBcModule = "";
	private String holdBcMaster0co = "";
	private String holdBcLocation = "";
	private String holdBcEffdt = "";
	private String holdBcExpdt = "";
	private String holdBcRisk0state = "";
	private String holdBcCompany0no = "";
	private String holdBcAgency = "";
	private String holdBcLine0bus = "";
	private String holdBcZip0post = "";
	private String holdBcAdd0line01 = "";
	private String holdBcAdd0line02 = "";
	private String holdBcAdd0line03 = "";
	private String holdBcAdd0line04 = "";
	private String holdBcState = "";
	private char holdBcRenewal0cd = ' ';
	private char holdBcIssue0code = ' ';
	private char holdBcPay0code = ' ';
	private char holdBcMode0code = ' ';
	private String holdBcSort0name = "";
	private String holdBcCust0no = "";
	private String holdBcSpec0use0a = "";
	private String holdBcSpec0use0b = "";
	private String holdBcCanceldate = "";
	private String holdBcMrsseq = "";
	private String holdBcStatusDesc = "";
	//FSIT: 152640, Resolution: 55204 - Begin
	/*private ABODecimal holdBcTot0ag0prm = new ABODecimal(11, 2);*/
	private ABODecimal holdBcTot0ag0prm = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private String holdBcType0act = "";
	private int wsCount;
	private int numOfEntrys;
	private int numOfEntrysForSql;
	private String wsReturnCode = "";
	public static final String GOOD_RETURN_FROM_IO = "0000000";
	private char wsControlSw = ' ';
	private String userId = "";
	private String wsCmpny = "";
	private String wsAgency = "";
	private String ws1Agency = "";
	private String wsEffdt = "";
	private String wsExpdt = "";
	private int wsCounter = 0;
	private String wsMasterCo = "";
	private String wsLocation = "";
	private String wsSymbol = "";
	private String wsFein = "";
	private String wsSSN = "";
	private String wsModule = "";
	private String wsPolicyNo = "";
	private String wsReadFile = "";
	private char wsData = ' ';
	public boolean wsResultSetSw = false;
	private WsInsCsrSw wsInsCsrSw = new WsInsCsrSw();
	private WsFeinCsrSw wsFeinCsrSw = new WsFeinCsrSw();
	private WsAddlIntsCsrSw wsAddlIntsCsrSw= new WsAddlIntsCsrSw();
	private FeinIndicator feinIndicator = new FeinIndicator();
	public boolean ws0200Flag = false;
	public boolean ws1200Flag = false;
	public boolean wsWc04Flag = false;
	public boolean wsPopArray = false;
	private char var1='Q';
	private char var2='0';
	//FSIT #136957 RESL #51611 Start
	private String var3="03";
	//FSIT #136957 RESL #51611 End
	//FSIT#178549 RESL#64623 - Start
	private char var4='B';
	//FSIT#178549 RESL#64623 - End
	private String wsMco = "";
	private String wsLoc = "";
	private String wsSymb = "";
	private String wsPolNo = "";
	private String wsMod = "";
	private String wsAgen = "";
	private String wsLastNam = "";
	private String wsDba = "";
	private String wsDbaName = "";
	private String wsCustNo = "";
	private String wsSortName = "";
	private String wsCity = "";
	private String wsState = "";
	private String wsZip = "";
	private String wsPhone = "";
	private String wsGroupNo = "";
	private String wsLob = "";
	private String wsAdIntTyp = "";
	private String wsAdIntNam = "";
	private String wsLoanNo = "";
	private String wsLegalAdd = "";
	private String wsLaCity = "";
	private String wsLaState = "";
	private String wsLaZip = "";
	private String wsMailing = "";
	private String holdPreviousType = "";
	private IABODynamicArray<BcListEntryArr37> bcListEntryArr37 = ABOCollectionCreator.getReferenceFactory(BcListEntryArr37.class).createDynamicArray(1);
	private short sqlIoOk;
	private short sqlNoRecord = ((short) 100);
	private short sqlInvalidToken = ((short) -104);
	private short sqlObjectNotFound = ((short) -201);
	private short sqlCursorAlreadyOpen = ((short) -502);
	private short sqlRecordExists = ((short) -803);
	private short sqlObjectLock = ((short) -913);
	private short sqlTableLock = ((short) -7958);
	private short sqlTooManyHostVariable = ((short) 326);
	private String noRecordFound="";
	private String recordFound="";
	//FSIT# 117377 RESL# 49190 Start
	private String wsMaxModule="";
	private short ind1;
	//FSIT# 117377 RESL# 49190 End

public Rskdbio039() {
		bcListEntryArr37.assign((1) - 1, new BcListEntryArr37());
}
public void mainSubroutine(){
	initialization();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred() || !wsResultSetSw){
		return;
	}
	prepareSearchVariable();
	openSearchCursor();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	processSearchCursor();
	overPara();
}

public void initialization(){
	if(rskdbio039TO.getInMaxrecords()<1){
		numOfEntrys = 0;
		wsResultSetSw = false;
	}else{
		wsResultSetSw = true;
	}
	if(rskdbio039TO.getInMaxrecords()>100){
		rskdbio039TO.setInMaxrecords((short)100);
	}
	userId = rskdbio039TO.getInUserid();
	retrieveMm80UserData();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	ws03RtrvMsgWorkArea.init();
	errorListRecord.initSpaces();
	ws04ErrorDetailArea.init();
	numOfEntrys = 0;
	ws04ErrorDetailArea.setWs04HoldProgName("RSKDBIO039");
	if(!("").equals(rskdbio039TO.getInSymbol()) || !("").equals(rskdbio039TO.getInPolicy0num()) || !("").equals(rskdbio039TO.getInModule()) ||
	   !("").equals(rskdbio039TO.getInName()) ||!("").equals(rskdbio039TO.getInState()) ||!("").equals(rskdbio039TO.getInLine0bus()) ||
	   !("").equals(rskdbio039TO.getInCust0no()) ||!("").equals(rskdbio039TO.getInAdd0Line04()) ||!("").equals(rskdbio039TO.getInZip0post()) ||
	   !("").equals(rskdbio039TO.getInSort0name()) ||!("").equals(rskdbio039TO.getInAgency()) ||!("").equals(rskdbio039TO.getInDbaName()) ||
	   !("").equals(rskdbio039TO.getInMailIngAdd())){
		ws0200Flag = true;
	}else{
		ws0200Flag = false;
	}

	if(!("").equals(rskdbio039TO.getInAddnlIntType()) ||!("").equals(rskdbio039TO.getInAddnlIntName()) ||!("").equals(rskdbio039TO.getInLegalAdd()) ||!("").equals(rskdbio039TO.getInloanNum())||
	   !("").equals(rskdbio039TO.getInLaCity()) ||!("").equals(rskdbio039TO.getInLaState()) ||!("").equals(rskdbio039TO.getInLaZip())){
		ws1200Flag = true;
	}else{
		ws1200Flag = false;
	}

	if(rskdbio039TO.getInFein()!="" ){
		wsWc04Flag = true;
	}else{
		wsWc04Flag = false;
	}

	if(ws0200Flag && !ws1200Flag && !wsWc04Flag)
		wsReadFile = "INS-INFO";

	if(ws1200Flag)
		wsReadFile = "ADDN-INT";

	if(wsWc04Flag && !ws1200Flag){
		if(rskdbio039TO.getInFeinIndSite()=='Y' &&rskdbio039TO.getInFeinIndInt()=='N'){
			wsReadFile = "FEIN-INFO";
		}
		if(rskdbio039TO.getInFeinIndSite()=='N' &&rskdbio039TO.getInFeinIndInt()=='Y'){
			wsReadFile = "ADDN-INT";
		}
		if(rskdbio039TO.getInFeinIndSite()=='Y' &&rskdbio039TO.getInFeinIndInt()=='Y'){
			wsReadFile = "ADDN-INT";
		}
	}
	if(!ws0200Flag && !ws1200Flag && !wsWc04Flag){
		numOfEntrys =0;
		wsResultSetSw  =true;
	}
}

public void retrieveMm80UserData(){
	Pmspmm80TO pmspmm80TO= new Pmspmm80TO();
	pmspmm80TO = Pmspmm80DAO.selectByUsrprf(pmspmm80TO, userId);
	cobolsqlca.setDBAccessStatus(pmspmm80TO.getDBAccessStatus());
	if (!cobolsqlca.isEOF()) {
		wsCmpny= pmspmm80TO.getCompany0no();
		wsAgency = pmspmm80TO.getAgnmnbr();
	}
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
		//continue
	}else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		ws04ErrorDetailArea.setWs04ErrParaName("1100-RETRIEVE-MM80-USER-DATA");
		ws04ErrorDetailArea.setWs04ErrErrorTyp('S');
		ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(userId);
		ws04ErrorDetailArea.setWs04ErrFileName("PMSPMM80");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
}

public void prepareSearchVariable() {
	wsMco = rskdbio039TO.getInMaster0co();
	Functions.replaceAll(wsMco, " ", "%");
	wsLoc = rskdbio039TO.getInLocation();
	Functions.replaceAll(wsLoc, " ", "%");
	wsSymb = rskdbio039TO.getInSymbol();
	Functions.replaceAll(wsSymb, " ", "%");
	wsPolNo = rskdbio039TO.getInPolicy0num();
	Functions.replaceAll(wsPolNo, " ", "%");
	//FSIT# 117377 RESL# 49190 Start
	//If the GO button is clicked on and the Module is left blank.
	if(rskdbio039TO.getInModule().equalsIgnoreCase("XX")){
		getMaxModule();
	}
	//FSIT# 117377 RESL# 49190 End
	wsMod = rskdbio039TO.getInModule();
	Functions.replaceAll(wsMod, " ", "%");
	wsLastNam = rskdbio039TO.getInName().toUpperCase();
	Functions.replaceAll(wsLastNam, " ", "%");
	wsCustNo = rskdbio039TO.getInCust0no();
	Functions.replaceAll(wsCustNo, " ", "%");
	wsSortName = rskdbio039TO.getInSort0name().toUpperCase();
	Functions.replaceAll(wsSortName, " ", "%");
	wsDbaName  = rskdbio039TO.getInDbaName().toUpperCase();
	Functions.replaceAll(wsDbaName, " ", "%");
	wsCity =rskdbio039TO.getInAdd0Line04().toUpperCase();
	Functions.replaceAll(wsCity, " ", "%");
	wsZip = rskdbio039TO.getInZip0post();
	Functions.replaceAll(wsZip, " ", "%");
	wsState= rskdbio039TO.getInState().toUpperCase();
	Functions.replaceAll(wsState, " ", "%");
	wsAgen = rskdbio039TO.getInAgency();
	Functions.replaceAll(wsAgen, " ", "%");
	wsLob = rskdbio039TO.getInLine0bus();
	Functions.replaceAll(wsLob, " ", "%");
	wsAdIntTyp = rskdbio039TO.getInAddnlIntType().toUpperCase();
	Functions.replaceAll(wsAdIntTyp, " ", "%");
	wsAdIntNam= rskdbio039TO.getInAddnlIntName().toUpperCase();
	Functions.replaceAll(wsAdIntNam, " ", "%");
	wsLoanNo=  rskdbio039TO.getInloanNum();
	Functions.replaceAll(wsLoanNo, " ", "%");
	wsLegalAdd= rskdbio039TO.getInLegalAdd().toUpperCase();
	Functions.replaceAll(wsLegalAdd, " ", "%");
	wsLaCity =rskdbio039TO.getInLaCity().toUpperCase();
	Functions.replaceAll(wsLaCity, " ", "%");
	wsLaState = rskdbio039TO.getInLaState().toUpperCase();
	Functions.replaceAll(wsLaState, " ", "%");
	wsLaZip = rskdbio039TO.getInLaZip();
	Functions.replaceAll(wsLaZip, " ", "%");
	wsFein = rskdbio039TO.getInFein();
	Functions.replaceAll(wsFein, " ", "%");
	wsSSN = rskdbio039TO.getInFein();
	Functions.replaceAll(wsSSN, " ", "%");
	wsMailing = rskdbio039TO.getInMailIngAdd();
	Functions.replaceAll(wsMailing, " ", "%");
	if(rskdbio039TO.getInLegalAdd()=="" || rskdbio039TO.getInLaCity() =="" || rskdbio039TO.getInLaState() ==""){
		wsAdIntTyp ="LA";
		Functions.replaceAll(wsAdIntTyp, " ", "%");
	}
}
public void openSearchCursor(){
	if(wsReadFile.equalsIgnoreCase("INS-INFO")){
		openPolInsuredInfoCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closePolInsuredCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
				openPolInsuredInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
			}else{
				return;
			}
		}
	}
	if(wsReadFile.equalsIgnoreCase("ADDN-INT")){
		openAddlIntsInfoCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closeAddlIntsInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
				openAddlIntsInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
			}else{
				return;
			}
		}
	}
	if(wsReadFile.equalsIgnoreCase("FEIN-INFO")){
		openFeinInfoCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closeFeinInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
				openFeinInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
					return;
			}else{
				return;
			}
		}
	}

}

public void openPolInsuredInfoCsr(){
	wsInsCsrSw.setStartOfCursor();
	pmsp0200RecTO.rec02fmtRec.setLocation(wsLoc);
	pmsp0200RecTO.rec02fmtRec.setMaster0co(wsMco);
	pmsp0200RecTO.rec02fmtRec.setSymbol(wsSymb);
	pmsp0200RecTO.rec02fmtRec.setPolicy0num(wsPolNo);
	pmsp0200RecTO.rec02fmtRec.setModule(wsMod);
	cobolsqlca.setDBAccessStatus(pmsp0200DAO.openPmsp0200TOCsr5(wsMco, wsLoc, wsSymb, wsPolNo,wsMod,
			wsLastNam, wsDbaName, wsLob, wsCustNo, wsZip, wsSortName, wsMailing, wsCity, wsState, wsAgen, var1, var2, var3));
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
			// continue
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("3100-OPEN-POL-INSURED-INFO-CSR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("INS_CURSOR");
			r000PrepErrorListPara();
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}

public void r000PrepErrorListPara() {
	ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(
			Functions.concat(
			ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000,
			Functions.trimAfter(wsSymb, "%"), new String[] { "; ",
			Functions.trimAfter(wsPolNo, "%"), "; ",
			Functions.trimAfter(wsMod, "%"), "; ",
			Functions.trimAfter(wsLastNam, "%"), "; ",
			Functions.trimAfter(wsLob, "%"), "; ",
			Functions.trimAfter(wsCustNo, "%"), "; ",
			Functions.trimAfter(wsCity, "%"), "; ",
			Functions.trimAfter(wsState, "%"), "; ",
			Functions.trimAfter(wsZip, "%"), "; ",
			Functions.trimAfter(wsSortName, "%"), "; ",
			Functions.trimAfter(wsMailing, "%"), "; ",
			Functions.trimAfter(wsAgen, "%"), "; ",
			Functions.trimAfter(String.valueOf(var1), "%"), "; ",
			Functions.trimAfter(String.valueOf(var2), "%"), "; "
			}));
}

public void closePolInsuredCsr(){
	cobolsqlca.setDBAccessStatus(pmsp0200DAO.closeCsr5());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
		// continue
	} else {
		ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		ws04ErrorDetailArea.setWs04ErrParaName("3200-CLOSE-POL-INSURED-CSR");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

public void openAddlIntsInfoCsr(){
	wsAddlIntsCsrSw.setValue((short)0);
	//FSIT# 136957 RESL# 51611 Start
	/*cobolsqlca.setDBAccessStatus(pmsp0200Pmsp1200DAO.openPmsp0200Pmsp1200TOCsr(wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
										wsDbaName, wsLob, wsCustNo, wsZip, wsSortName, wsMailing, wsAdIntTyp, wsAdIntNam, wsLoanNo,
										wsSSN, wsCity, wsState, wsAgen, wsLaCity, wsLaState, wsLaZip, var1, var2));*/
	cobolsqlca.setDBAccessStatus(pmsp0200Pmsp1200DAO.openPmsp0200Pmsp1200TOCsr(wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
			wsDbaName, wsLob, wsCustNo, wsZip, wsSortName, wsMailing, wsAdIntTyp, wsAdIntNam, wsLoanNo,
			wsSSN, wsCity, wsState, wsAgen, wsLaCity, wsLaState, wsLaZip, var1, var2, var3));

	//FSIT# 136957 RESL# 51611 End
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
			// continue
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("3300-OPEN-ADDL-INTS-INFO-CSR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("ADDIN_CURSOR");
			ws04ErrorDetailArea.setWs04ErrSqlDtastgDta("Search Join Cursor");
			r000PrepErrorListPara();
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}

public void closeAddlIntsInfoCsr(){
	cobolsqlca.setDBAccessStatus(pmsp0200Pmsp1200DAO.closeCsr());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
		// continue
	} else {
		ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		ws04ErrorDetailArea.setWs04ErrParaName("3400-CLOSE-ADDL-INTS-INFO-CSR");
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

public void openFeinInfoCsr(){
	wsFeinCsrSw.setStartOfCursor();
	//FSIT# 136957 RESL# 51611 Start
		/*cobolsqlca.setDBAccessStatus(pmsp0200Pmspwc04DAO.openFeinCursor(wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
				wsDbaName, wsLob, wsCustNo, wsZip, wsSortName, wsMailing, wsCity, wsState, wsAgen, wsFein, var1, var2));*/
	cobolsqlca.setDBAccessStatus(pmsp0200Pmspwc04DAO.openFeinCursor(wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
			wsDbaName, wsLob, wsCustNo, wsZip, wsSortName, wsMailing, wsCity, wsState, wsAgen, wsFein, var1, var2, var3));
	//FSIT# 136957 RESL# 51611 End
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
			// continue
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("3500-OPEN-FEIN-INFO-CSR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("FEIN_CURSOR");
			r000PrepErrorListPara();
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
}

public void closeFeinInfoCsr(){
	cobolsqlca.setDBAccessStatus(pmsp0200Pmspwc04DAO.closeCsr());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
		// continue
	} else {
		ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		ws04ErrorDetailArea.setWs04ErrParaName("3600-CLOSE-FEIN-INFO-CSR");
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

public void processSearchCursor(){
	if(wsReadFile.equalsIgnoreCase("INS-INFO")){
		while(!(wsInsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
		processPolInsuredCsr();
		}
	}

	if(wsReadFile.equalsIgnoreCase("ADDN-INT")){
		while(!(wsAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
		processAddlIntsInfcsr();
		}
		if(wsWc04Flag && !ws1200Flag && rskdbio039TO.getInFeinIndSite()=='Y' && rskdbio039TO.getInFeinIndInt() == 'Y'){
			wsReadFile = "FEIN-INFO";
			openSearchCursor();
		}

	}
	if(wsReadFile.equalsIgnoreCase("FEIN-INFO")){

		while(!(wsFeinCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
			processFeinInfCsr();
		}
	}

}


public void processPolInsuredCsr(){
	fetchPolInsuredCsr();
	if(wsInsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
	{
		 return;
	}
		movePolInsAiData();
		if(wsPopArray == true) {
			//continue
		} else {
			return;
		}
		processEntry();
		checkDuplicatePara();
}

public void fetchPolInsuredCsr(){
	pmsp0200TO = pmsp0200DAO.fetchAddIntCltCursor(pmsp0200TO);
	if(pmsp0200TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200TO.getTrans0stat());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200TO.getMaster0co());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200TO.getPolicy0num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200TO.getModule());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200TO.getEff0yr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200TO.getEff0mo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200TO.getEff0da());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200TO.getExp0yr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200TO.getExp0mo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200TO.getExp0da());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200TO.getMrsseq());
		if(wsInsCsrSw.isStartOfCursor()) {
			wsInsCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200TO.getDBAccessStatus().isRecNotFound()) {
			if(wsInsCsrSw.isStartOfCursor()) {
				wsInsCsrSw.setEndOfCursor();
			} else if(wsInsCsrSw.isFetchingCursor()) {
				wsInsCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("5100-FETCH-POL-INSURED-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("INS_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(pmsp0200Pmsp1200TO.getDBAccessStatus());
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}

public void processAddlIntsInfcsr() {
	fetchAddlIntCsr();
	if(wsAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	movePolInsAiData();
	if(wsPopArray == true){
		//continue
	} else {
		return;
	}
	processEntry();
	checkDuplicatePara();
}

public void fetchAddlIntCsr(){
	pmsp0200Pmsp1200TO = pmsp0200Pmsp1200DAO.fetchPmsp0200Pmsp1200(pmsp0200Pmsp1200TO);
	if(pmsp0200Pmsp1200TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Pmsp1200TO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Pmsp1200TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Pmsp1200TO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Pmsp1200TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Pmsp1200TO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Pmsp1200TO.getModule());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Pmsp1200TO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Pmsp1200TO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Pmsp1200TO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Pmsp1200TO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Pmsp1200TO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Pmsp1200TO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Pmsp1200TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Pmsp1200TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Pmsp1200TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Pmsp1200TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Pmsp1200TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Pmsp1200TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Pmsp1200TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Pmsp1200TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Pmsp1200TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Pmsp1200TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Pmsp1200TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Pmsp1200TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Pmsp1200TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Pmsp1200TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Pmsp1200TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Pmsp1200TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Pmsp1200TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Pmsp1200TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Pmsp1200TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Pmsp1200TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Pmsp1200TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Pmsp1200TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Pmsp1200TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Pmsp1200TO.getMrsseq());
		pmsp1200RecTO.rec12fmtRec.setLoannum(pmsp0200Pmsp1200TO.getLoannum());
		pmsp1200RecTO.rec12fmtRec.setUse0code(pmsp0200Pmsp1200TO.getUse0code());
		pmsp1200RecTO.rec12fmtRec.setDzip0code(pmsp0200Pmsp1200TO.getDzip0code());
		pmsp1200RecTO.rec12fmtRec.setDesc0line1(pmsp0200Pmsp1200TO.getDesc0line1());
		pmsp1200RecTO.rec12fmtRec.setDesc0line2(pmsp0200Pmsp1200TO.getDesc0line2());
		pmsp1200RecTO.rec12fmtRec.setDesc0line3(pmsp0200Pmsp1200TO.getDesc0line3());
		pmsp1200RecTO.rec12fmtRec.setDesc0line4(pmsp0200Pmsp1200TO.getDesc0line4());
		pmsp1200RecTO.rec12fmtRec.setSsn(pmsp0200Pmsp1200TO.getSsn());
		if(wsAddlIntsCsrSw.isStartOfCursor()) {
			wsAddlIntsCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200Pmsp1200TO.getDBAccessStatus().isRecNotFound()) {
			if(wsAddlIntsCsrSw.isStartOfCursor()) {
				wsAddlIntsCsrSw.setEndOfCursor();
			} else if(wsAddlIntsCsrSw.isFetchingCursor()) {
				wsAddlIntsCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("6100-FETCH-ADDL-INT-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("ADDINT_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(pmsp0200Pmsp1200TO.getDBAccessStatus());
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}

public void processFeinInfCsr(){
	fetchPolFeinCsr();
	if(wsFeinCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	movePolInsAiData();
	if(wsPopArray == true){
		//continue
	} else {
		return;
	}
	 processEntry();
	 checkDuplicatePara();
}


public void fetchPolFeinCsr(){
	pmsp0200Pmspwc04TO = pmsp0200Pmspwc04DAO.fetchPmsp0200Pmspwc04(pmsp0200Pmspwc04TO);
	if(pmsp0200Pmspwc04TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Pmspwc04TO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Pmspwc04TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Pmspwc04TO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Pmspwc04TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Pmspwc04TO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Pmspwc04TO.getModule());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Pmspwc04TO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Pmspwc04TO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Pmspwc04TO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Pmspwc04TO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Pmspwc04TO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Pmspwc04TO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Pmspwc04TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Pmspwc04TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Pmspwc04TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Pmspwc04TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Pmspwc04TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Pmspwc04TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Pmspwc04TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Pmspwc04TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Pmspwc04TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Pmspwc04TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Pmspwc04TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Pmspwc04TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Pmspwc04TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Pmspwc04TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Pmspwc04TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Pmspwc04TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Pmspwc04TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Pmspwc04TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Pmspwc04TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Pmspwc04TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Pmspwc04TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Pmspwc04TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Pmspwc04TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Pmspwc04TO.getMrsseq());
		pmspwc04RecTO.wc04Rec.setFein(pmsp0200Pmspwc04TO.getFein());
		if(wsFeinCsrSw.isStartOfCursor()) {
			wsFeinCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200Pmspwc04TO.getDBAccessStatus().isRecNotFound()) {
			if(wsFeinCsrSw.isStartOfCursor()) {
				wsFeinCsrSw.setEndOfCursor();
			} else if(wsFeinCsrSw.isFetchingCursor()) {
				wsFeinCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			ws04ErrorDetailArea.setWs04ErrParaName("7100-FETCH-POL-FEIN-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("FEIN_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(pmsp0200Pmsp1200TO.getDBAccessStatus().getSqlCode()));
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}


public void overPara(){
	numOfEntrysForSql = numOfEntrys;
	resultSetTO.addResultSet(bcListEntryArr37, numOfEntrysForSql);
}
//FSIT# 117377 RESL# 49190 Start
public void getMaxModule(){
	Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	//FSIT#178549 RESL#64623 Start
	//pmsp0200TO = pmsp0200DAO.selectMaxMasterco(pmsp0200TO, wsMco, wsLoc, wsSymb, wsPolicyNo, var1, var2, var3);
	pmsp0200TO = pmsp0200DAO.selectMaxMasterco(pmsp0200TO, wsMco, wsLoc, wsSymb, wsPolicyNo, var1, var2, var3, var4);
	//FSIT#178549 RESL#64623 End
	cobolsqlca.setDBAccessStatus(pmsp0200TO.getDBAccessStatus());
	if (!cobolsqlca.isEOF() || cobolsqlca.getSqlCode() == sqlIoOk ) {
		wsMaxModule = pmsp0200TO.getModule();
	}
	else
	{
		gatherErrorEventData3700E();
	}
}

public void gatherErrorEventData3700E(){
	ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
	ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
	ws04ErrorDetailArea.setWs04ErrParaName("3700-GET-MAX-MODULE");
	ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
	ws04ErrorDetailArea.setWs04ErrFileName("PMSP0200");
	r000PrepErrorListPara();
	cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
	cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
	cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
	cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
	ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
	ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
	errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
}
//FSIT# 117377 RESL# 49190 End
public void movePolInsAiData(){
	wsEffdt = Functions.concat(wsEffdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0yr(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0mo(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0da()));

	wsExpdt = Functions.concat(wsExpdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0yr(), " "),
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0mo(), " "),
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0da(), " "));

	ws1Agency = Functions.concat(ws1Agency, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr1(), " "),
			Functions.trimAfter(Character.toString(pmsp0200RecTO.rec02fmtRec.getRpt0agt0nr()), " "),
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr2(), " "));

	wsDba = Functions.concat(wsDba, 1000,
			Functions.trimAfter(Character.toString(pmsp0200RecTO.rec02fmtRec.getFillr3()), " "),
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr4(), " "));

	if(!wsCmpny.equalsIgnoreCase("")){
		if(wsCmpny.equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getMaster0co())){
			//continue
		}else{
			wsPopArray =false;
		}
	}

	if(!wsAgency.equalsIgnoreCase("")){
		if(wsAgency.equalsIgnoreCase(ws1Agency)){
			//continue
		}else{
			wsPopArray =false;
		}
	}

	if(!rskdbio039TO.getInSymbol().equalsIgnoreCase("")){
		if(rskdbio039TO.getInSymbol().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getSymbol())){
			//continue
		}else{
			wsPopArray =false;
		}
	}

	if(!rskdbio039TO.getInLine0bus().equalsIgnoreCase("")){
		if(rskdbio039TO.getInLine0bus().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getLine0bus())){
			//continue
		}else{
			wsPopArray =false;
		}
	}
	
	//RSK - Start
	
	if(!rskdbio039TO.getInLossdte().equals("") && !rskdbio039TO.getInLossdte().equals("0000000")){
		if(rskdbio039TO.getInLossdte().compareTo(wsEffdt) < 0 || rskdbio039TO.getInLossdte().compareTo(wsExpdt) > 0 ){
			wsPopArray = false;
			return;
		}
	}
	//RSK - End
	
	//FSIT# 200328 Resolution# 79580 - Start
	if (!pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("") && !pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("0000000")) {
		if(rskdbio039TO.getInLossdte().compareTo(pmsp0200RecTO.rec02fmtRec.getCanceldate()) > 0 ){
			wsPopArray = false;
			return;
		}
	}
	//FSIT# 200328 Resolution# 79580 - End

	/*===============================================================*
	* 1.Check if SSN is entered. If YES read PMSPWC04 to get        *
	*   FEIN/SSN information.                                       *
	* 2.Check for FEIN-INDICATOR and read the file accordingly.
	* 3.This function will work even if all values related to       *
	*   PMSP0200,PMSP1200 & PMSPWC04 are entered.                   *
	* 4.Move Key values from PMSP0200 to PMSPWC04 key fields.       *
	* 5.If SSN not found discard the policy and move to next.       *
	*===============================================================*/
	if(!(rskdbio039TO.getInFein().equalsIgnoreCase("")) && ws1200Flag == true){
		if(rskdbio039TO.getInFeinIndSite() ==  'N' && rskdbio039TO.getInFeinIndInt() == 'Y'){
			if(!rskdbio039TO.getInFein().equalsIgnoreCase(pmsp1200RecTO.rec12fmtRec.getSsn())){
				wsPopArray =false;
			}
		}
		if(rskdbio039TO.getInFeinIndSite() ==  'Y' && rskdbio039TO.getInFeinIndInt() == 'N'){
			wsFein = rskdbio039TO.getInFein();
			wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
			wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
			wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
			wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
			wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
			/*===============================================================*
			* Read PMSPWC04 and check if record exist,on FAIL move 'N'      *
			* to FEIN-INDICATOR.
			*===============================================================*/
			readPmspwc04Para();
			if(feinIndicator.isFeinNotFound() && ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
				wsPopArray =false;
			}
		}


	 /*===============================================================*
	 * If both SITE and CLIENT boxes are checked then first look in  *
	 * client file,if not found then look in PMSPWC04(SITE).         *
	 *===============================================================*/
		if(rskdbio039TO.getInFeinIndSite() ==  'Y' && rskdbio039TO.getInFeinIndInt() == 'N'){
			if(!rskdbio039TO.getInFein().equalsIgnoreCase(pmsp1200RecTO.rec12fmtRec.getSsn())){
				feinIndicator.setValue('N');
			}else{
				feinIndicator.setValue('Y');
			}
			if(feinIndicator.isFeinNotFound()){
				wsFein = rskdbio039TO.getInFein();
				wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
				wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
				wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
				wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
				wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
				/*===============================================================*
				* Read PMSPWC04 and check if record exist,on FAIL move 'N'      *
				* to FEIN-INDICATOR.
				*===============================================================*/
				readPmspwc04Para();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
					wsPopArray =false;
				}
			}
		}
	}
		 /*===============================================================*
		 * If all conditions were true or SSN was not entered then turn  *
		 * on WS-POP-ARRAY for processing of data in next subroutine.    *
		 *===============================================================*/
		wsPopArray = true;
		holdBcTrans0stat = pmsp0200RecTO.rec02fmtRec.getTrans0stat();
		holdBcSymbol = pmsp0200RecTO.rec02fmtRec.getSymbol();
		holdBcPolicy0num = pmsp0200RecTO.rec02fmtRec.getPolicy0num();
		holdBcModule = pmsp0200RecTO.rec02fmtRec.getModule();
		holdBcMaster0co = pmsp0200RecTO.rec02fmtRec.getMaster0co();
		holdBcLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
		holdBcEffdt = pmsp0200RecTO.rec02fmtRec.getEff0da();
		holdBcExpdt = pmsp0200RecTO.rec02fmtRec.getExp0da();
		holdBcRisk0state = pmsp0200RecTO.rec02fmtRec.getRisk0state();
		holdBcCompany0no = pmsp0200RecTO.rec02fmtRec.getCompany0no();
		holdBcAgency = ws1Agency;
		holdBcLine0bus = pmsp0200RecTO.rec02fmtRec.getLine0bus();
		holdBcZip0post = pmsp0200RecTO.rec02fmtRec.getZip0post();
		holdBcAdd0line01 = pmsp0200RecTO.rec02fmtRec.getAdd0line01();
		holdBcAdd0line02 = wsDba;
		holdBcAdd0line03 = pmsp0200RecTO.rec02fmtRec.getAdd0line03();
		holdBcAdd0line04 = pmsp0200RecTO.rec02fmtRec.getAdd0line04();
		holdBcState = Functions.subString(pmsp0200RecTO.rec02fmtRec.getAdd0line04(),29,2);
		holdBcRenewal0cd = pmsp0200RecTO.rec02fmtRec.getRenewal0cd();
		holdBcIssue0code = pmsp0200RecTO.rec02fmtRec.getIssue0code();
		holdBcPay0code = pmsp0200RecTO.rec02fmtRec.getPay0code();
		holdBcMode0code = pmsp0200RecTO.rec02fmtRec.getMode0code();
		holdBcSort0name = pmsp0200RecTO.rec02fmtRec.getSort0name();
		holdBcCust0no = pmsp0200RecTO.rec02fmtRec.getCust0no();
		holdBcSpec0use0a =pmsp0200RecTO.rec02fmtRec.getSpec0use0a();
		holdBcSpec0use0b =pmsp0200RecTO.rec02fmtRec.getSpec0use0b();
		if(holdBcRenewal0cd =='9'){
			holdBcCanceldate = "";
		}else{
			holdBcCanceldate = pmsp0200RecTO.rec02fmtRec.getCanceldate();
		}
		holdBcMrsseq = pmsp0200RecTO.rec02fmtRec.getMrsseq();
		holdBcTot0ag0prm = pmsp0200RecTO.rec02fmtRec.getTot0ag0prm();
		holdBcType0act = pmsp0200RecTO.rec02fmtRec.getType0act();
		if(numOfEntrys == rskdbio039TO.getInMaxrecords()){
			wsInsCsrSw.setValue((short)2);
			wsAddlIntsCsrSw.setValue((short)2);
		}
}

public void readPmspwc04Para(){
	cobolsqlca.setDBAccessStatus(pmspwc04dao.openEscheatCursor(wsLocation, wsSymbol, wsPolicyNo,wsMasterCo,wsModule,wsFein));
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
			feinIndicator.setFeinFound();
		} else {
			if (cobolsqlca.getSqlCode() == sqlNoRecord) {
				feinIndicator.setFeinNotFound();
			} else {
				ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
				ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
				ws04ErrorDetailArea.setWs04ErrParaName("7777-CHECK-FOR-RECORD");
				ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
				ws04ErrorDetailArea.setWs04ErrFileName("FEIN_CURSOR");
				ws04ErrorDetailArea.setWs04ErrFileName("PMSPWC04");
				ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(
						Functions.concat(
								ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000,
								Functions.trimAfter(wsSymb, "%"), new String[] { "; ",
								Functions.trimAfter(wsPolNo, "%"), "; ",
								Functions.trimAfter(wsMod, "%"), "; "}));
				cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
				cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
				cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
				cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
				ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
				ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
				errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
			 }
		}
}

public void processEntry(){
	numOfEntrys = (1 + numOfEntrys) % 100000;
	wsCount = numOfEntrys;
	getBcListEntryArr37(wsCount).setBcTrans0stat(holdBcTrans0stat);
	getBcListEntryArr37(wsCount).setBcSymbol(holdBcSymbol);
	getBcListEntryArr37(wsCount).setBcPolicy0num(holdBcPolicy0num);
	getBcListEntryArr37(wsCount).setBcModule(holdBcModule);
	getBcListEntryArr37(wsCount).setBcMaster0co(holdBcMaster0co);
	getBcListEntryArr37(wsCount).setBcLocation(holdBcLocation);
	getBcListEntryArr37(wsCount).setBcEffdt(holdBcEffdt);
	getBcListEntryArr37(wsCount).setBcExpdt(holdBcExpdt);
	getBcListEntryArr37(wsCount).setBcRisk0state(holdBcRisk0state);
	getBcListEntryArr37(wsCount).setBcCompany0no(holdBcCompany0no);
	getBcListEntryArr37(wsCount).setBcAgency(holdBcAgency);
	getBcListEntryArr37(wsCount).setBcLine0bus(holdBcLine0bus);
	getBcListEntryArr37(wsCount).setBcRenewal0cd(holdBcRenewal0cd);
	getBcListEntryArr37(wsCount).setBcIssue0code(holdBcIssue0code);
	getBcListEntryArr37(wsCount).setBcPay0code(holdBcPay0code);
	getBcListEntryArr37(wsCount).setBcMode0code(holdBcMode0code);
	getBcListEntryArr37(wsCount).setBcSort0name(holdBcSort0name);
	getBcListEntryArr37(wsCount).setBcCust0no(holdBcCust0no);
	getBcListEntryArr37(wsCount).setBcSpec0use0a(holdBcSpec0use0a);
	getBcListEntryArr37(wsCount).setBcSpec0use0b(holdBcSpec0use0b);
	getBcListEntryArr37(wsCount).setBcCanceldate(holdBcCanceldate);
	getBcListEntryArr37(wsCount).setBcMrsseq(holdBcMrsseq);
	getBcListEntryArr37(wsCount).setBcTot0ag0prm(holdBcTot0ag0prm.clone());
	getBcListEntryArr37(wsCount).setBcAdd0line01(holdBcAdd0line01);
	getBcListEntryArr37(wsCount).setBcAdd0line02(holdBcAdd0line02);
	getBcListEntryArr37(wsCount).setBcAdd0line03(holdBcAdd0line03);
	getBcListEntryArr37(wsCount).setBcAdd0line04(holdBcAdd0line04);
	getBcListEntryArr37(wsCount).setBcState(holdBcState);
	getBcListEntryArr37(wsCount).setBcZip0post(holdBcZip0post);
	if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '0') {
		getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Alright For Agent To Renew");
	} else {
		if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '1') {
			getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Computer Renew");
		} else {
			if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '2') {
				getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Alright For Home Office To renew");
			} else {
				if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '3') {
					getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Already Renewed");
				} else {
					if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '4') {
						getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
					} else {
						if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '7') {
							getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Non-Renewal With A Notice");
						} else {
							if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '8') {
								getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Non-Renewal Without A Notice");
							} else {
								if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '9') {
									getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("Cancelled Policy");
								} else {
									getBcListEntryArr37(wsCount).setBcRenewal0cdDesc("");
								}
							}
						}
					}
				}
			}
		}
	}
	/*===============================================================*
	* Check the activity on policy from file PMSL0004.              *
	* Call program BASL000401 to get the activity.                  *
	*===============================================================*/
	rec00fmtRec3.setSymbol(holdBcSymbol);
	rec00fmtRec3.setPolicy0num(holdBcPolicy0num);
	rec00fmtRec3.setModule(holdBcModule);
	rec00fmtRec3.setMaster0co(holdBcMaster0co);
	rec00fmtRec3.setLocation(holdBcLocation);
	wsControlSw = 'Y';
	try {
		call1();
	} catch (DynamicCallException ex) {
		if (ex.getPgmName().compareTo("BASL000401") != 0) {
			throw ex;
		} else {
			wsReturnCode = "FS00";
		}
	}
	/*===============================================================*
	* If no record was returned then the policy is verified so      *
	* set the status on the activity record to a 'V'                *
	*===============================================================*/
	// If no record was returned then the policy is verified so
	// set the status on the activity record to a 'V'
	if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
		// MOVE 'V' TO TRANS0STAT OF REC00FMT
		rec00fmtRec3.setTrans0stat(holdBcTrans0stat);
		rec00fmtRec3.setType0act(holdBcType0act);
	}
	getBcListEntryArr37(wsCount).setBcStatusDesc("Verified");
	if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr37(wsCount).setBcStatusDesc("NB Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr37(wsCount).setBcStatusDesc("NB Pending");
			}
		}
		return;
	}
	if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr37(wsCount).setBcStatusDesc("EN Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr37(wsCount).setBcStatusDesc("EN Pending");
			}
		}
		return;
	}
	getBcListEntryArr37(wsCount).setBcTrans0stat(rec00fmtRec3.getTrans0stat());
	if (getBcListEntryArr37(wsCount).getBcRenewal0cd() == '9' && getBcListEntryArr37(wsCount).getBcCanceldate().compareTo("") != 0) {
		if (getBcListEntryArr37(wsCount).getBcTrans0stat() == 'V') {
			getBcListEntryArr37(wsCount).setBcTrans0stat('C');
			getBcListEntryArr37(wsCount).setBcStatusDesc("Cancelled");
			return;
		}
	}
	if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr37(wsCount).setBcTrans0stat('1');
			getBcListEntryArr37(wsCount).setBcStatusDesc("CN Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr37(wsCount).setBcStatusDesc("CN Pending");
				getBcListEntryArr37(wsCount).setBcTrans0stat('X');
			}
		}
		return;
	}
	//
	if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr37(wsCount).setBcStatusDesc("RN Error");
			getBcListEntryArr37(wsCount).setBcTrans0stat('2');
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr37(wsCount).setBcStatusDesc("RN Pending");
				getBcListEntryArr37(wsCount).setBcTrans0stat('W');
			}
		}
		return;
	}
	//
	if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr37(wsCount).setBcStatusDesc("RI Error");
			getBcListEntryArr37(wsCount).setBcTrans0stat('3');
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr37(wsCount).setBcStatusDesc("RI Pending");
				getBcListEntryArr37(wsCount).setBcTrans0stat('I');
			}
		}
		return;
	}

}

public void checkDuplicatePara(){
	if(numOfEntrys>1){
		holdPreviousType = getBcListEntryArr37(wsCount).getBcStatusDesc();
		wsCounter = 1;
		holdPreviousType = "";
		while(wsCounter >= numOfEntrys){
			processDuplicatePara();
			wsCounter = (short) (wsCounter + 1 );
		}
	}
}

public void processDuplicatePara(){
	if(getBcListEntryArr37(wsCounter).getBcSymbol().equalsIgnoreCase(getBcListEntryArr37(numOfEntrys).getBcSymbol())
			&& getBcListEntryArr37(wsCounter).getBcPolicy0num().equalsIgnoreCase(getBcListEntryArr37(numOfEntrys).getBcPolicy0num())
			&& getBcListEntryArr37(wsCounter).getBcModule().equalsIgnoreCase(getBcListEntryArr37(numOfEntrys).getBcModule())
			&& getBcListEntryArr37(wsCounter).getBcMaster0co().equalsIgnoreCase(getBcListEntryArr37(numOfEntrys).getBcMaster0co())
			&& getBcListEntryArr37(wsCounter).getBcLocation().equalsIgnoreCase(getBcListEntryArr37(numOfEntrys).getBcLocation())){
		wsCount = wsCount-1;
		numOfEntrys = numOfEntrys -1;
	}
}

public static Rskdbio039 getInstance() {
	Rskdbio039 iRskdbio039 = ((Rskdbio039) CalledProgList.getInst("RSKDBIO039"));
	if (iRskdbio039 == null) {
		iRskdbio039 = new Rskdbio039();
		CalledProgList.addInst("RSKDBIO039", ((Object) iRskdbio039));
	}
	return iRskdbio039;
}

public void run() {
	try {
		mainSubroutine();
	} catch (ReturnException re) {
		// normal termination of the program
	}
}

public static void main(String[] args) {
	getInstance().run();
}

public void exit() {
	throw new ReturnException();
}

public SPResultSetTO runSP(Rskdbio039TO rskdbio039TO) {
	this.rskdbio039TO = rskdbio039TO;
	run();
	return resultSetTO;
}

public SPResultSetTO runSP(byte[] dynamic_inputParameters) {
	rskdbio039TO.setInputParameters(dynamic_inputParameters);
	return runSP(rskdbio039TO);
}

public Rskdbio039TO run(Rskdbio039TO rskdbio039TO) {
	this.rskdbio039TO = rskdbio039TO;
	run();
	return this.rskdbio039TO;
}

public void run(byte[] dynamic_inputParameters) {
	if (dynamic_inputParameters != null) {
		rskdbio039TO.setInputParameters(dynamic_inputParameters);
	}
	run(rskdbio039TO);
	if (dynamic_inputParameters != null) {
		DataConverter.arrayCopy(rskdbio039TO.getInputParameters(), rskdbio039TO.getInputParametersSize(), dynamic_inputParameters, 1);
	}
}

public void call1() {
	byte[] arr_p0 = new byte[7];
	byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
	byte[] arr_p2 = new byte[1];
	DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
	DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
	DataConverter.writeChar(arr_p2, 1, wsControlSw);
	DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
	wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
	rec00fmtRec3.setRec00fmt(arr_p1);
	wsControlSw = DataConverter.readChar(arr_p2, 1);
}

public BcListEntryArr37 getBcListEntryArr37(int index) {
	if (index > bcListEntryArr37.size()) {
		// for an element beyond current array limits, all intermediate
		// items will be added with default elements;
		bcListEntryArr37.ensureCapacity(index);
		for (int i = bcListEntryArr37.size() + 1; i <= index; i++) {
			bcListEntryArr37.append(new BcListEntryArr37());
			// assign default value
		}
	}
	return bcListEntryArr37.get((index) - 1);
}

public void initBcListTableSpaces() {
	for (int idx = 1; idx <= bcListEntryArr37.size(); idx++) {
		if (bcListEntryArr37.get((idx) - 1) != null) {
			bcListEntryArr37.get((idx) - 1).initSpaces();
		}
	}
}
}
