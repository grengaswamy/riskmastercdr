// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.sql.util.ABOSqlDB2Conversion;

import com.csc.pt.svc.data.to.Bashlgbv00TO;

public class Bashlgbv00DAO {
	public static DBAccessStatus insertRec(Bashlgbv00TO bashlgbv00TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASHLGBV00");
			Query query = session.getNamedQuery("Bashlgbv00DAO_insertRec");
			query.setString("bashlgbv00TO_loguser", bashlgbv00TO.getLoguser());
			query.setDate("bashlgbv00TO_logdate", ABOSqlDB2Conversion.dateToSQL(bashlgbv00TO.getLogdate()));
			query.setTime("bashlgbv00TO_logtime", ABOSqlDB2Conversion.timeToSQL(bashlgbv00TO.getLogtime()));
			query.setInteger("bashlgbv00TO_logseqnum", bashlgbv00TO.getLogseqnum());
			query.setString("bashlgbv00TO_location", bashlgbv00TO.getLocation());
			query.setString("bashlgbv00TO_masterco", bashlgbv00TO.getMasterco());
			query.setString("bashlgbv00TO_symbol", bashlgbv00TO.getSymbol());
			query.setString("bashlgbv00TO_policyno", bashlgbv00TO.getPolicyno());
			query.setString("bashlgbv00TO_module", bashlgbv00TO.getModule());
			query.setInteger("bashlgbv00TO_riskloc", bashlgbv00TO.getRiskloc());
			query.setInteger("bashlgbv00TO_risksubloc", bashlgbv00TO.getRisksubloc());
			query.setCharacter("bashlgbv00TO_recstatus", bashlgbv00TO.getRecstatus());
			query.setCharacter("bashlgbv00TO_bvc7st", bashlgbv00TO.getBvc7st());
			query.setInteger("bashlgbv00TO_bvandt", bashlgbv00TO.getBvandt());
			query.setString("bashlgbv00TO_bvtype0act", bashlgbv00TO.getBvtype0act());
			query.setInteger("bashlgbv00TO_bvafdt", bashlgbv00TO.getBvafdt());
			query.setInteger("bashlgbv00TO_bvaldt", bashlgbv00TO.getBvaldt());
			query.setString("bashlgbv00TO_bveftx", bashlgbv00TO.getBveftx());
			query.setString("bashlgbv00TO_bvegtx", bashlgbv00TO.getBvegtx());
			query.setString("bashlgbv00TO_bveitx", bashlgbv00TO.getBveitx());
			query.setString("bashlgbv00TO_bvejtx", bashlgbv00TO.getBvejtx());
			query.setString("bashlgbv00TO_bvapnb", bashlgbv00TO.getBvapnb());

			int result = query.executeUpdate();
			status.setNumOfRows(result);

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}

	public Bashlgbv00TO readHistoryFIle(String location, String policyno, String symbol, String masterco, String module,
			int riskloc, int risksubloc, int lossdate) {
			DBAccessStatus status = new DBAccessStatus();
			Bashlgbv00TO localbashlgbv00TO = null;
		try{
			Session session = HibernateSessionFactory.current().getSession("BASHLGBV00");
			Query query = session.getNamedQuery("Bashlgbv00TO_GETHISTORYRECORDS");
			query.setParameter("location", location);
			query.setParameter("masterco", masterco);
			query.setParameter("symbol", symbol);
			query.setParameter("policyno", policyno);			
			query.setParameter("module", module);
			query.setParameter("riskloc", riskloc);
			query.setParameter("risksubloc",risksubloc);
			query.setParameter("lossdate", lossdate);
			query.setMaxResults(1);
			List resultList = query.list();

			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localbashlgbv00TO = (Bashlgbv00TO)(obj);
					
					status = new DBAccessStatus(DBAccessStatus.DB_OK);
					localbashlgbv00TO.setDBAccessStatus(status);
				}
				
			}
			else status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		 catch (HibernateException ex) {
			status.setException(ex);
		}
		 
		
		return localbashlgbv00TO;
		
	}
}

