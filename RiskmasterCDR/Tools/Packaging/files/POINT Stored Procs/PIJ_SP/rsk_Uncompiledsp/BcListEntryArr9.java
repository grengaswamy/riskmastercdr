package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;

public class BcListEntryArr9 implements Visitable {
	private String bcUnitNumber = "";
	private String bcStatus = "";
	private String bcLastChangeDate = "";
	private String bcRatePremium = "";
	private String bcMakeYear = "";
	private String bcMakeDescription = "";
	private String bcProduct = "";
	private String bcSerial = "";
	private String bcInsLine = "";
	private String bcRiskLoc = "";
	private String bcRiskSubLoc = "";
	private String bcRateState = "";
	private char bcIssueCode = ' ';
	private String bcStatUnit = "";

	public void setBcUnitNumber(String bcUnitNumber) {
		this.bcUnitNumber = bcUnitNumber;
	}

	public String getBcUnitNumber() {
		return this.bcUnitNumber;
	}

	public void setBcStatus(String bcStatus) {
		this.bcStatus = bcStatus;
	}

	public String getBcStatus() {
		return this.bcStatus;
	}

	public void setBcLastChangeDate(String bcLastChangeDate) {
		this.bcLastChangeDate = bcLastChangeDate;
	}

	public String getBcLastChangeDate() {
		return this.bcLastChangeDate;
	}

	public void setBcRatePremium(String bcRatePremium) {
		this.bcRatePremium = bcRatePremium;
	}

	public String getBcRatePremium() {
		return this.bcRatePremium;
	}

	public void setBcMakeYear(String bcMakeYear) {
		this.bcMakeYear = bcMakeYear;
	}

	public String getBcMakeYear() {
		return this.bcMakeYear;
	}

	public void setBcMakeDescription(String bcMakeDescription) {
		this.bcMakeDescription = bcMakeDescription;
	}

	public String getBcMakeDescription() {
		return this.bcMakeDescription;
	}

	public void setBcProduct(String bcProduct) {
		this.bcProduct = bcProduct;
	}

	public String getBcProduct() {
		return this.bcProduct;
	}

	public void setBcSerial(String bcSerial) {
		this.bcSerial = bcSerial;
	}

	public String getBcSerial() {
		return this.bcSerial;
	}

	public void setBcInsLine(String bcInsLine) {
		this.bcInsLine = bcInsLine;
	}

	public String getBcInsLine() {
		return this.bcInsLine;
	}

	public void setBcRiskLoc(String bcRiskLoc) {
		this.bcRiskLoc = bcRiskLoc;
	}

	public String getBcRiskLoc() {
		return this.bcRiskLoc;
	}

	public void setBcRiskSubLoc(String bcRiskSubLoc) {
		this.bcRiskSubLoc = bcRiskSubLoc;
	}

	public String getBcRiskSubLoc() {
		return this.bcRiskSubLoc;
	}

	public void setBcRateState(String bcRateState) {
		this.bcRateState = bcRateState;
	}

	public String getBcRateState() {
		return this.bcRateState;
	}

	public void setBcIssueCode(char bcIssueCode) {
		this.bcIssueCode = bcIssueCode;
	}

	public char getBcIssueCode() {
		return this.bcIssueCode;
	}

	public void accept(Visitor visitor) {
		visitor.visitString("BC-UNIT-NUMBER", bcUnitNumber);
		visitor.visitString("BC-STATUS", bcStatus);
		visitor.visitString("BC-LAST-CHANGE-DATE", bcLastChangeDate);
		visitor.visitString("BC-RATE-PREMIUM", bcRatePremium);
		visitor.visitString("BC-MAKE-YEAR", bcMakeYear);
		visitor.visitString("BC-MAKE-DESCRIPTION", bcMakeDescription);
		visitor.visitString("BC-PRODUCT", bcProduct);
		visitor.visitString("BC-SERIAL", bcSerial);
		visitor.visitString("BC-INS-LINE", bcInsLine);
		visitor.visitString("BC-RISK-LOC", bcRiskLoc);
		visitor.visitString("BC-RISK-SUB-LOC", bcRiskSubLoc);
		visitor.visitString("BC-RATE-STATE", bcRateState);
		visitor.visitChar("BC-ISSUE-CODE", bcIssueCode);
		visitor.visitString("BC-STAT-UNIT", bcStatUnit);
	}

	public static int getSize() {
		return 116;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, bcUnitNumber, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcStatus, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcLastChangeDate, 8);
		offset += 8;
		DataConverter.writeString(buf, offset, bcRatePremium, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcMakeYear, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, bcMakeDescription, 25);
		offset += 25;
		DataConverter.writeString(buf, offset, bcProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, bcSerial, 17);
		offset += 17;
		DataConverter.writeString(buf, offset, bcInsLine, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, bcRiskLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcRiskSubLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcRateState, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, bcIssueCode);
		offset += 1;
		DataConverter.writeString(buf, offset, bcStatUnit, 5);
		offset += 5;
		return buf;
	}

	public void initSpaces() {
		bcUnitNumber = "";
		bcStatus = "";
		bcLastChangeDate = "";
		bcRatePremium = "";
		bcMakeYear = "";
		bcMakeDescription = "";
		bcProduct = "";
		bcSerial = "";
		bcInsLine = "";
		bcRiskLoc = "";
		bcRiskSubLoc = "";
		bcRateState = "";
		bcIssueCode = ' ';
		bcStatUnit = "";
	}

	public String getBcStatUnit() {
		return bcStatUnit;
	}

	public void setBcStatUnit(String bcStatUnit) {
		this.bcStatUnit = bcStatUnit;
	}
}