// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.to;

import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;
import com.csc.pt.svc.data.id.Bashlgbv00ID;

public class Bashlgbv00TO {
	private DBAccessStatus dBAccessStatus;
	private String loguser = "";
	private ABODate logdate = new ABODate();
	private ABOTime logtime = new ABOTime();
	private char bvc7st;
	private int bvandt;
	private String bvtype0act = "";
	private int bvafdt;
	private int bvaldt;
	private String bveftx = "";
	private String bvegtx = "";
	private String bveitx = "";
	private String bvejtx = "";
	private String bvapnb = "";
	private Bashlgbv00ID id = new Bashlgbv00ID();
	public DBAccessStatus getDBAccessStatus() {
		return this.dBAccessStatus;
	}

	public void setDBAccessStatus(DBAccessStatus dBAccessStatus) {
		this.dBAccessStatus = dBAccessStatus;
	}

	public String getLoguser() {
		return this.loguser;
	}

	public void setLoguser(String loguser) {
		this.loguser = loguser;
	}

	public ABODate getLogdate() {
		return this.logdate.clone();
	}

	public void setLogdate(ABODate logdate) {
		this.logdate.assign(logdate);
	}

	public ABOTime getLogtime() {
		return this.logtime.clone();
	}

	public void setLogtime(ABOTime logtime) {
		this.logtime.assign(logtime);
	}

	public int getLogseqnum() {
		return this.id.getLogseqnum();
	}

	public void setLogseqnum(int logseqnum) {
		this.id.setLogseqnum(logseqnum);
	}

	public String getLocation() {
		return this.id.getLocation();
	}

	public void setLocation(String location) {
		this.id.setLocation(location);
	}

	public String getMasterco() {
		return this.id.getMasterco();
	}

	public void setMasterco(String masterco) {
		this.id.setMasterco(masterco);
	}

	public String getSymbol() {
		return this.id.getSymbol();
	}

	public void setSymbol(String symbol) {
		this.id.setSymbol(symbol);
	}

	public String getPolicyno() {
		return this.id.getPolicyno();
	}

	public void setPolicyno(String policyno) {
		this.id.setPolicyno(policyno);
	}

	public String getModule() {
		return this.id.getModule();
	}

	public void setModule(String module) {
		this.id.setModule(module);
	}

	public int getRiskloc() {
		return this.id.getRiskloc();
	}

	public void setRiskloc(int riskloc) {
		this.id.setRiskloc(riskloc);
	}

	public int getRisksubloc() {
		return this.id.getRisksubloc();
	}

	public void setRisksubloc(int risksubloc) {
		this.id.setRisksubloc(risksubloc);
	}


	public char getRecstatus() {
		return this.id.getRecstatus();
	}

	public void setRecstatus(char recstatus) {
		this.id.setRecstatus(recstatus);
	}

	public char getBvc7st() {
		return this.bvc7st;
	}

	public void setBvc7st(char bvc7st) {
		this.bvc7st = bvc7st;
	}

	public int getBvandt() {
		return this.bvandt;
	}

	public void setBvandt(int bvandt) {
		this.bvandt = bvandt;
	}

	public String getBvtype0act() {
		return this.bvtype0act;
	}

	public void setBvtype0act(String bvtype0act) {
		this.bvtype0act = bvtype0act;
	}

	public int getBvafdt() {
		return this.bvafdt;
	}

	public void setBvafdt(int bvafdt) {
		this.bvafdt = bvafdt;
	}

	public int getBvaldt() {
		return this.bvaldt;
	}

	public void setBvaldt(int bvaldt) {
		this.bvaldt = bvaldt;
	}

	public String getBveftx() {
		return this.bveftx;
	}

	public void setBveftx(String bveftx) {
		this.bveftx = bveftx;
	}

	public String getBvegtx() {
		return this.bvegtx;
	}

	public void setBvegtx(String bvegtx) {
		this.bvegtx = bvegtx;
	}

	public String getBveitx() {
		return this.bveitx;
	}

	public void setBveitx(String bveitx) {
		this.bveitx = bveitx;
	}

	public String getBvejtx() {
		return this.bvejtx;
	}

	public void setBvejtx(String bvejtx) {
		this.bvejtx = bvejtx;
	}

	public String getBvapnb() {
		return this.bvapnb;
	}

	public void setBvapnb(String bvapnb) {
		this.bvapnb = bvapnb;
	}
	public Bashlgbv00ID getId() {
		return this.id;
	}

	public void setId(Bashlgbv00ID id) {
		this.id = id;
	}
}
