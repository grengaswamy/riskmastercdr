package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;

public class UnitTableEntryArr1 implements Visitable {
	private String statUnitNo = "";
	private String riskLoc = "";
	private String riskSubLoc = "";
	private char transStatus = 'V';
	private ABODecimal premium   = new ABODecimal(20, 2);
	private String unitName = "";
	private String insuranceLine = "";
	private String product = "";
	private String unitNumber = "";
	private String lob   = "";           
	private String vin   = "";       
	private String vehiclemodel   = ""; 
	private int vehiclemakeyear = 0;
	private String propertyaddress = "";
	private String sitecity  = "";      
	private String sitestate  = "";     
	private String sitezip   = "";
	private String sumdesc   = ""; 
	private String ratestate= "";
	private char typeStat = ' ';
	private char locVehSw = ' ';
	

	public void setStatUnitNo(String statUnitNo) {
		this.statUnitNo = statUnitNo;
	}

	public String getStatUnitNo() {
		return this.statUnitNo;
	}


	public String getRiskLoc() {
		return riskLoc;
	}

	public void setRiskLoc(String riskLoc) {
		this.riskLoc = riskLoc;
	}

	public String getRiskSubLoc() {
		return riskSubLoc;
	}

	public void setRiskSubLoc(String riskSubLoc) {
		this.riskSubLoc = riskSubLoc;
	}

	public char getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(char transStatus) {
		this.transStatus = transStatus;
	}

	public ABODecimal getPremium() {
		return premium;
	}

	public void setPremium(ABODecimal premium) {
		this.premium = premium;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getVehiclemodel() {
		return vehiclemodel;
	}

	public void setVehiclemodel(String vehiclemodel) {
		this.vehiclemodel = vehiclemodel;
	}

	public int getVehiclemakeyear() {
		return vehiclemakeyear;
	}

	public void setVehiclemakeyear(int vehiclemakeyear) {
		this.vehiclemakeyear = vehiclemakeyear;
	}

	public String getPropertyaddress() {
		return propertyaddress;
	}

	public void setPropertyaddress(String propertyaddress) {
		this.propertyaddress = propertyaddress;
	}

	public String getSitecity() {
		return sitecity;
	}

	public void setSitecity(String sitecity) {
		this.sitecity = sitecity;
	}

	public String getSitestate() {
		return sitestate;
	}

	public void setSitestate(String sitestate) {
		this.sitestate = sitestate;
	}

	public String getSitezip() {
		return sitezip;
	}

	public void setSitezip(String sitezip) {
		this.sitezip = sitezip;
	}

	public String getSumdesc() {
		return sumdesc;
	}

	public void setSumdesc(String sumdesc) {
		this.sumdesc = sumdesc;
	}

	public String getRatestate() {
		return ratestate;
	}

	public void setRatestate(String ratestate) {
		this.ratestate = ratestate;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitName() {
		return this.unitName;
	}

	public void setInsuranceLine(String insuranceLine) {
		this.insuranceLine = insuranceLine;
	}

	public String getInsuranceLine() {
		return this.insuranceLine;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProduct() {
		return this.product;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getUnitNumber() {
		return this.unitNumber;
	}

	public void accept(Visitor visitor) {
		visitor.visitString("STAT-UNIT-NO", statUnitNo);
		visitor.visitString("RISK-LOC", riskLoc);
		visitor.visitString("RISK-SUBLOC", riskSubLoc);
		visitor.visitChar("TRANS-STATUS", transStatus);
		visitor.visitDecimal("PREMIUM", premium);
		visitor.visitString("UNIT-NAME", unitName);
		visitor.visitString("INSURANCE-LINE", insuranceLine);
		visitor.visitString("PRODUCT", product);
		visitor.visitString("UNIT-NUMBER", unitNumber); 
		visitor.visitString("LOB", lob);
		visitor.visitString("VIN", vin);
		visitor.visitString("VEHICLE-MODEL", vehiclemodel);
		visitor.visitInt("VEHICLE-MAKE-YEAR", vehiclemakeyear);
		visitor.visitString("PROPERTY-ADDRESS", propertyaddress);
		visitor.visitString("SITE-CITY", sitecity);
		visitor.visitString("SITE-STATE", sitestate);
		visitor.visitString("SITE-ZIP", sitezip);
		visitor.visitString("SUMDESC", sumdesc);
		visitor.visitString("RATE-STATE", ratestate);
		visitor.visitChar("TYPE-STAT", typeStat);
		visitor.visitChar("LOC-VEH-SW", locVehSw);
	}

	public static int getSize() {
		return 318;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, statUnitNo, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, riskLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, riskSubLoc, 5);
		offset += 5;
		DataConverter.writeChar(buf, offset, transStatus);
		offset += 1;
		DataConverter.writeDecimal(buf, offset, premium.clone(),18, 2, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeString(buf, offset, unitName, 100);
		offset += 100;
		DataConverter.writeString(buf, offset, insuranceLine, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, product, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, lob, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, vin, 17);
		offset += 17;
		DataConverter.writeString(buf, offset, vehiclemodel, 25);
		offset += 25;
		DataConverter.writeInt(buf, offset, vehiclemakeyear, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, propertyaddress, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, sitecity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, sitestate, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, sitezip, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, sumdesc, 50);
		offset += 50;
		DataConverter.writeString(buf, offset, ratestate, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, typeStat);
		offset += 1;
		DataConverter.writeChar(buf, offset, locVehSw);
		offset += 1;
		return buf;
	}

	public String getAsStr() {
		return DataConverter.readString(getData(), 1, getData().length);
	}

	public void setUnitNameFromReference(String unitName, int startIndex, int length) {
		String pre = "", post = "";
		String temp = Functions.padBlanks(this.unitName, 100);
		if (startIndex > 1) {
			pre = temp.substring((1) - 1, startIndex - 1);
		}
		if ((startIndex + length) <= ABOSystem.StrLen(temp)) {
			post = Functions.trimAfter(temp.substring((startIndex + length) - 1));
		}
		this.unitName = (new StringBuffer(256).append(pre).append(Functions.resizeTo(unitName, length)).append(post).toString());
	}

	public char getTypeStat() {
		return typeStat;
	}

	public void setTypeStat(char typeStat) {
		this.typeStat = typeStat;
	}

	public char getLocVehSw() {
		return locVehSw;
	}

	public void setLocVehSw(char locVehSw) {
		this.locVehSw = locVehSw;
	}
}