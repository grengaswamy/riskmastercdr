package com.csc.pt.svc.rsk;

import bphx.c2ab.data.Visitable;
import bphx.c2ab.data.Visitor;
import bphx.c2ab.util.DataConverter;

public class BcListEntryArr23 implements Visitable {
	private String bcUnitNumber = "";
	private String bcStatus = "";
	private String bcRatePremium = "";
	private String bcRiskLocationNumber = "";
	private String bcRiskSublocationNumber = "";
	private String bcProduct = "";
	private String bcCoverageLimit1 = "";
	private String bcCoveragePremium1 = "";
	private String bcCoverageLimit2 = "";
	private String bcCoveragePremium2 = "";
	private String bcCoverageLimit3 = "";
	private String bcCoveragePremium3 = "";
	private String bcCoverageLimit4 = "";
	private String bcCoveragePremium4 = "";
	private String bcCoverageLimit5 = "";
	private String bcCoveragePremium5 = "";
	private String bcCoverageLimit6 = "";
	private String bcCoveragePremium6 = "";
	private String bcTarget = "";
	private String bcRqcode = "";
	private String bcLocAdd = "";
	private String bcLocCity = "";
	private String bcRateState = "";
	private String bcYearOfConstr = "";
	private String bcTypeOfConstr = "";
	private String bcLastChangeDate = "";
	private String bcInsLine = "";
	private char bcIssueCode = ' ';
	private String bcStatUnit = "";

	public String getBcStatUnit() {
		return bcStatUnit;
	}

	public void setBcStatUnit(String bcStatUnit) {
		this.bcStatUnit = bcStatUnit;
	}

	public void setBcUnitNumber(String bcUnitNumber) {
		this.bcUnitNumber = bcUnitNumber;
	}

	public String getBcUnitNumber() {
		return this.bcUnitNumber;
	}

	public void setBcStatus(String bcStatus) {
		this.bcStatus = bcStatus;
	}

	public String getBcStatus() {
		return this.bcStatus;
	}

	public void setBcRatePremium(String bcRatePremium) {
		this.bcRatePremium = bcRatePremium;
	}

	public String getBcRatePremium() {
		return this.bcRatePremium;
	}

	public void setBcRiskLocationNumber(String bcRiskLocationNumber) {
		this.bcRiskLocationNumber = bcRiskLocationNumber;
	}

	public String getBcRiskLocationNumber() {
		return this.bcRiskLocationNumber;
	}

	public void setBcRiskSublocationNumber(String bcRiskSublocationNumber) {
		this.bcRiskSublocationNumber = bcRiskSublocationNumber;
	}

	public String getBcRiskSublocationNumber() {
		return this.bcRiskSublocationNumber;
	}

	public void setBcProduct(String bcProduct) {
		this.bcProduct = bcProduct;
	}

	public String getBcProduct() {
		return this.bcProduct;
	}

	public void setBcCoverageLimit1(String bcCoverageLimit1) {
		this.bcCoverageLimit1 = bcCoverageLimit1;
	}

	public String getBcCoverageLimit1() {
		return this.bcCoverageLimit1;
	}

	public void setBcCoveragePremium1(String bcCoveragePremium1) {
		this.bcCoveragePremium1 = bcCoveragePremium1;
	}

	public String getBcCoveragePremium1() {
		return this.bcCoveragePremium1;
	}

	public void setBcCoverageLimit2(String bcCoverageLimit2) {
		this.bcCoverageLimit2 = bcCoverageLimit2;
	}

	public String getBcCoverageLimit2() {
		return this.bcCoverageLimit2;
	}

	public void setBcCoveragePremium2(String bcCoveragePremium2) {
		this.bcCoveragePremium2 = bcCoveragePremium2;
	}

	public String getBcCoveragePremium2() {
		return this.bcCoveragePremium2;
	}

	public void setBcCoverageLimit3(String bcCoverageLimit3) {
		this.bcCoverageLimit3 = bcCoverageLimit3;
	}

	public String getBcCoverageLimit3() {
		return this.bcCoverageLimit3;
	}

	public void setBcCoveragePremium3(String bcCoveragePremium3) {
		this.bcCoveragePremium3 = bcCoveragePremium3;
	}

	public String getBcCoveragePremium3() {
		return this.bcCoveragePremium3;
	}

	public void setBcCoverageLimit4(String bcCoverageLimit4) {
		this.bcCoverageLimit4 = bcCoverageLimit4;
	}

	public String getBcCoverageLimit4() {
		return this.bcCoverageLimit4;
	}

	public void setBcCoveragePremium4(String bcCoveragePremium4) {
		this.bcCoveragePremium4 = bcCoveragePremium4;
	}

	public String getBcCoveragePremium4() {
		return this.bcCoveragePremium4;
	}

	public void setBcCoverageLimit5(String bcCoverageLimit5) {
		this.bcCoverageLimit5 = bcCoverageLimit5;
	}

	public String getBcCoverageLimit5() {
		return this.bcCoverageLimit5;
	}

	public void setBcCoveragePremium5(String bcCoveragePremium5) {
		this.bcCoveragePremium5 = bcCoveragePremium5;
	}

	public String getBcCoveragePremium5() {
		return this.bcCoveragePremium5;
	}

	public void setBcCoverageLimit6(String bcCoverageLimit6) {
		this.bcCoverageLimit6 = bcCoverageLimit6;
	}

	public String getBcCoverageLimit6() {
		return this.bcCoverageLimit6;
	}

	public void setBcCoveragePremium6(String bcCoveragePremium6) {
		this.bcCoveragePremium6 = bcCoveragePremium6;
	}

	public String getBcCoveragePremium6() {
		return this.bcCoveragePremium6;
	}

	public void setBcTarget(String bcTarget) {
		this.bcTarget = bcTarget;
	}

	public String getBcTarget() {
		return this.bcTarget;
	}

	public void setBcRqcode(String bcRqcode) {
		this.bcRqcode = bcRqcode;
	}

	public String getBcRqcode() {
		return this.bcRqcode;
	}

	public void setBcLocAdd(String bcLocAdd) {
		this.bcLocAdd = bcLocAdd;
	}

	public String getBcLocAdd() {
		return this.bcLocAdd;
	}

	public void setBcLocCity(String bcLocCity) {
		this.bcLocCity = bcLocCity;
	}

	public String getBcLocCity() {
		return this.bcLocCity;
	}

	public void setBcRateState(String bcRateState) {
		this.bcRateState = bcRateState;
	}

	public String getBcRateState() {
		return this.bcRateState;
	}

	public void setBcYearOfConstr(String bcYearOfConstr) {
		this.bcYearOfConstr = bcYearOfConstr;
	}

	public String getBcYearOfConstr() {
		return this.bcYearOfConstr;
	}

	public void setBcTypeOfConstr(String bcTypeOfConstr) {
		this.bcTypeOfConstr = bcTypeOfConstr;
	}

	public String getBcTypeOfConstr() {
		return this.bcTypeOfConstr;
	}

	public void setBcLastChangeDate(String bcLastChangeDate) {
		this.bcLastChangeDate = bcLastChangeDate;
	}

	public String getBcLastChangeDate() {
		return this.bcLastChangeDate;
	}

	public void setBcInsLine(String bcInsLine) {
		this.bcInsLine = bcInsLine;
	}

	public String getBcInsLine() {
		return this.bcInsLine;
	}

	public void setBcIssueCode(char bcIssueCode) {
		this.bcIssueCode = bcIssueCode;
	}

	public char getBcIssueCode() {
		return this.bcIssueCode;
	}

	public void accept(Visitor visitor) {
		visitor.visitString("BC-UNIT-NUMBER", bcUnitNumber);
		visitor.visitString("BC-STATUS", bcStatus);
		visitor.visitString("BC-RATE-PREMIUM", bcRatePremium);
		visitor.visitString("BC-RISK-LOCATION-NUMBER", bcRiskLocationNumber);
		visitor.visitString("BC-RISK-SUBLOCATION-NUMBER", bcRiskSublocationNumber);
		visitor.visitString("BC-PRODUCT", bcProduct);
		visitor.visitString("BC-COVERAGE-LIMIT1", bcCoverageLimit1);
		visitor.visitString("BC-COVERAGE-PREMIUM1", bcCoveragePremium1);
		visitor.visitString("BC-COVERAGE-LIMIT2", bcCoverageLimit2);
		visitor.visitString("BC-COVERAGE-PREMIUM2", bcCoveragePremium2);
		visitor.visitString("BC-COVERAGE-LIMIT3", bcCoverageLimit3);
		visitor.visitString("BC-COVERAGE-PREMIUM3", bcCoveragePremium3);
		visitor.visitString("BC-COVERAGE-LIMIT4", bcCoverageLimit4);
		visitor.visitString("BC-COVERAGE-PREMIUM4", bcCoveragePremium4);
		visitor.visitString("BC-COVERAGE-LIMIT5", bcCoverageLimit5);
		visitor.visitString("BC-COVERAGE-PREMIUM5", bcCoveragePremium5);
		visitor.visitString("BC-COVERAGE-LIMIT6", bcCoverageLimit6);
		visitor.visitString("BC-COVERAGE-PREMIUM6", bcCoveragePremium6);
		visitor.visitString("BC-TARGET", bcTarget);
		visitor.visitString("BC-RQCODE", bcRqcode);
		visitor.visitString("BC-LOC-ADD", bcLocAdd);
		visitor.visitString("BC-LOC-CITY", bcLocCity);
		visitor.visitString("BC-RATE-STATE", bcRateState);
		visitor.visitString("BC-YEAR-OF-CONSTR", bcYearOfConstr);
		visitor.visitString("BC-TYPE-OF-CONSTR", bcTypeOfConstr);
		visitor.visitString("BC-LAST-CHANGE-DATE", bcLastChangeDate);
		visitor.visitString("BC-INS-LINE", bcInsLine);
		visitor.visitChar("BC-ISSUE-CODE", bcIssueCode);
		visitor.visitString("BC-STAT-UNIT", bcStatUnit);
	}

	public static int getSize() {
		return 386;
	}

	public byte[] getData() {
		byte[] buf = new byte[getSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, bcUnitNumber, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcStatus, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcRatePremium, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcRiskLocationNumber, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcRiskSublocationNumber, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, bcProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, bcCoverageLimit1, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium1, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoverageLimit2, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium2, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoverageLimit3, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium3, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoverageLimit4, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium4, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoverageLimit5, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium5, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoverageLimit6, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcCoveragePremium6, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcTarget, 35);
		offset += 35;
		DataConverter.writeString(buf, offset, bcRqcode, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, bcLocAdd, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, bcLocCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, bcRateState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcYearOfConstr, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, bcTypeOfConstr, 25);
		offset += 25;
		DataConverter.writeString(buf, offset, bcLastChangeDate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcInsLine, 3);
		offset += 3;
		DataConverter.writeChar(buf, offset, bcIssueCode);
		offset += 1;
		DataConverter.writeString(buf, offset, bcStatUnit, 5);
		offset += 5;
		return buf;
	}

	public void initSpaces() {
		bcUnitNumber = "";
		bcStatus = "";
		bcRatePremium = "";
		bcRiskLocationNumber = "";
		bcRiskSublocationNumber = "";
		bcProduct = "";
		bcCoverageLimit1 = "";
		bcCoveragePremium1 = "";
		bcCoverageLimit2 = "";
		bcCoveragePremium2 = "";
		bcCoverageLimit3 = "";
		bcCoveragePremium3 = "";
		bcCoverageLimit4 = "";
		bcCoveragePremium4 = "";
		bcCoverageLimit5 = "";
		bcCoveragePremium5 = "";
		bcCoverageLimit6 = "";
		bcCoveragePremium6 = "";
		bcTarget = "";
		bcRqcode = "";
		bcLocAdd = "";
		bcLocCity = "";
		bcRateState = "";
		bcYearOfConstr = "";
		bcTypeOfConstr = "";
		bcLastChangeDate = "";
		bcInsLine = "";
		bcIssueCode = ' ';
		bcStatUnit = "";
	}
}