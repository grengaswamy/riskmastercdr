// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import com.csc.pt.svc.util.to.DateprTO;
import com.csc.pt.svc.util.Datepr;
import com.csc.pt.svc.util.to.Bascgn03TO;
import com.csc.pt.svc.util.Bascgn03;
import com.csc.pt.svc.common.to.Baswc07ei1TO;
import com.csc.pt.svc.nav.to.Basasbq102TO;
import com.csc.pt.svc.nav.to.Pmsbwc04TO;
import com.csc.pt.svc.nav.to.Baswc01m01TO;

import bphx.c2ab.util.ProcessInfo;
import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DataExtConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.IProcessInfo;
import bphx.c2ab.util.SignType;
import bphx.c2ab.util.Types;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.common.Baswc07ei1;
import com.csc.pt.svc.ctrl.DatePrParms;
import com.csc.pt.svc.ctrl.to.Pmsp0200RecTO;
import com.csc.pt.svc.data.dao.Asbbcpl0DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Pmspsa05DAO;
import com.csc.pt.svc.data.dao.Pmspsa15DAO;
import com.csc.pt.svc.data.dao.Pmspsa35DAO;
import com.csc.pt.svc.data.dao.Pmspwc07DAO;
import com.csc.pt.svc.data.dao.Tbcl007DAO;
import com.csc.pt.svc.data.dao.Tbpp004DAO;
import com.csc.pt.svc.data.dao.Tbpp005DAO;
import com.csc.pt.svc.data.dao.Tbtc22DAO;
import com.csc.pt.svc.data.dao.Tbtl29DAO;
import com.csc.pt.svc.data.dao.TbtransDAO;
import com.csc.pt.svc.data.dao.Tbwc30DAO;
import com.csc.pt.svc.data.to.Asbbcpl0TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.data.to.Pmspsa05TO;
import com.csc.pt.svc.data.to.Pmspsa15TO;
import com.csc.pt.svc.data.to.Pmspsa35TO;
import com.csc.pt.svc.data.to.Pmspwc07TO;
import com.csc.pt.svc.data.to.Tbcl007TO;
import com.csc.pt.svc.data.to.Tbpp004TO;
import com.csc.pt.svc.data.to.Tbpp005TO;
import com.csc.pt.svc.data.to.Tbtc22TO;
import com.csc.pt.svc.data.to.Tbtl29TO;
import com.csc.pt.svc.data.to.TbtransTO;
import com.csc.pt.svc.data.to.Tbts01TO;
import com.csc.pt.svc.data.to.Tbwc30TO;
import com.csc.pt.svc.rsk.dao.Asb1cppDAO;
import com.csc.pt.svc.data.dao.Pmspsa50DAO;
import com.csc.pt.svc.rsk.to.Basclcovl1TO;
import com.csc.pt.svc.db.DescApplicableInd;
import com.csc.pt.svc.db.EstimatingCnPrfBefFaInd;
import com.csc.pt.svc.db.ProcessedAudValsSbSplitSw;
import com.csc.pt.svc.db.ProcessedEstValsSbSplitSw;
import com.csc.pt.svc.db.ProcessedValsSbSplitSw;
import com.csc.pt.svc.db.RateSp1ByPolEffInd;
import com.csc.pt.svc.db.SplitArdBy3MonRuleInd;
import com.csc.pt.svc.db.SplitEnteredClassesInd;
import com.csc.pt.svc.db.SplitEstCalcsInd;
import com.csc.pt.svc.db.SplitToFind;
import com.csc.pt.svc.db.Sw4;
import com.csc.pt.svc.db.WclobfmtRec;
import com.csc.pt.svc.rsk.UnitTableEntryArr;
import com.csc.pt.svc.db.VerOnlyInd;
import com.csc.pt.svc.db.Wc02OutRec;
import com.csc.pt.svc.db.WsFileStatus21;
import com.csc.pt.svc.db.to.Pmspsa35RecTO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.nav.Basasbq102;
import com.csc.pt.svc.nav.Baswc01m01;
import com.csc.pt.svc.nav.Pmsbwc04;
import com.csc.pt.svc.pas.alr.Pmsbwc03;
import com.csc.pt.svc.pas.alr.Sa15Rec2;
import com.csc.pt.svc.pas.alr.to.Pmsbwc03TO;

import java.math.BigDecimal;

import bphx.c2ab.data.Initialize;

import com.csc.pt.svc.data.to.AsbqcppTO;

/**
*/

public class Basclcovl1 extends ProcessInfo<Basclcovl1TO>  {

	protected Pmspsa05TO pmspsa05RecTO2 = Initialize.initSpaces(new Pmspsa05TO());
	protected Pmsp0200RecTO pmsp0200RecTO = new Pmsp0200RecTO();

	protected Sa15Rec2 pmspsa15RecTO = new Sa15Rec2();
	protected Pmspsa35RecTO pmspsa35RecTO = new Pmspsa35RecTO();
	protected Tbpp005TO tbpp005RecTO = Initialize.initSpaces(new Tbpp005TO());
	protected Tbpp004TO tbpp004RecTO = Initialize.initSpaces(new Tbpp004TO());
	protected Tbcl007TO tbcl007RecTO = Initialize.initSpaces(new Tbcl007TO());
	protected TbtransTO tbtransRecTO = Initialize.initSpaces(new TbtransTO());
	protected Tbtl29TO tbtl29RecTO = Initialize.initSpaces(new Tbtl29TO());
	protected Tbtc22TO tbtc22RecTO = Initialize.initSpaces(new Tbtc22TO());
	protected WclobfmtRec tbwc30RecTO = new WclobfmtRec();
	protected String wsSymbolWc07e = "";
	protected String wsPolicynoWc07e = "";
	protected String wsModuleWc07e = "";
	protected String wsMastercoWc07e = "";
	protected String wsLocationWc07e = "";
	protected String wsStateWc07e = "";
	protected String wsAnvdateWc07e = "";
	protected String wsNewState = "";
	protected String wsOldState = "";
	protected WsFileStatus21 wsFileStatus21 = new WsFileStatus21();
	protected String holdFileStatus = "";
	protected String basclcovl1filler1 = "";
	protected String wcDescSeq = "";
	protected String basclcovl1filler2 = "";
	protected String basclcovl1filler3 = "";
	protected String w1Majperil = "";
	protected String basclcovl1filler4 = "MAJOR PERIL:";
	protected String wsMajperil = "";
	protected String basclcovl1filler5 = " SUBLINE:";
	protected String wsSubline = "";
	protected String basclcovl1filler6 = " CLASS NUMBER:";
	protected String wsClassnum = "";
	protected String wsCoverage = "";
	protected char basclcovl1filler7 = '-';
	protected String wsCovseq = "";
	protected String msgText1 = "";
	protected String msgText2 = "";
	protected BigDecimal errSeverity = BigDecimal.ZERO;
	protected short ws1Sa15CntrlCovseq;
	protected int ws1Sa15CntrlCoveff;
	protected int ws1Sa15CntrlCovexp;
	protected short ws1Sa15CntrlCovseqFirm;
	protected int ws1Sa15CntrlCoveffFirm;
	protected int ws1Sa15CntrlCovexpFirm;
	protected int ws1CovexpDate;
	protected int ws1Sa15CntrlRetroFirm;
	protected int ws1Sa15CntrlExtendFirm;
	protected short wsCovCount;
	protected short wsValidCovCount;
	protected short wsErrorCount;
	protected Sw4 sw4 = new Sw4();
	protected char screenErrorsSw = ' ';
	protected static final char SCREEN_ERRORS = 'Y';
	protected char invalidKeySw = ' ';
	protected static final char INVALID_KEY = 'Y';
	protected char excludedMajPerSw = ' ';
	protected static final char EXCLUDED_MAJ_PER = 'Y';
	protected char notExcludedMajPerSw = ' ';
	protected static final char NOT_EXCLUDED_MAJ_PER = 'Y';
	protected char flatCancelSw = ' ';
	protected static final char FLAT_CANCEL = 'Y';
	protected char pendingCancelSw = ' ';
	protected static final char PENDING_CANCEL = 'Y';
	protected char selfDestructSw = ' ';
	protected static final char SELF_DESTRUCT_POLICY = 'Y';
	protected char cl007RecordSw = ' ';
	protected char noCovRcdsSw = 'Y';
	protected static final char NO_COV_RCDS = 'Y';
	protected char wsEndOfLoopSw = 'N';
	protected static final char END_OF_LOOP = 'Y';
	protected boolean workersCompPolicySw = false;
	protected boolean wsMoreThan500RcdSw = false;
	protected String yy = "";
	protected String mm = "";
	protected String dd = "";
	protected String yy2 = "";
	protected String mm2 = "";
	protected String dd2 = "";
	protected String msgId = "";
	protected String msgText = "";
	protected String msgData = "";
	protected String msgL2Text = "";
	protected String wsDisplayDateMm = "";
	protected char slash1 = '/';
	protected String wsDisplayDateDd = "";
	protected char slash2 = '/';
	protected String wsDisplayDateYy = "";
	protected short wsIdx = ((short) 1);
	protected short wsIndex = ((short) 1);
	protected short wsCtr;
	protected short wsCount;
	protected short wsCtr2;
	protected short wsCount2;
	protected short wsCounter;
	protected String wsCl007Loc = "";
	protected String wsCl007Mco = "";
	protected String wsCl007Lob = "";
	protected String wsCl007Mp = "";
	protected char wsCl007Lossdte = ' ';
	protected char wsCl007Rptdte = ' ';
	protected char wsCl007Clmmadedte = ' ';
	protected char wsCl007Dolsoftedt = ' ';
	protected char wsCl007Rptsoftedt = ' ';
	protected char wsCl007Clmsoftedt = ' ';
	protected char wsCl007Dolcoveff = ' ';
	protected char wsCl007Dolcovexp = ' ';
	protected char wsCl007Dolretro = ' ';
	protected char wsCl007Dolextend = ' ';
	protected char wsCl007Rptcoveff = ' ';
	protected char wsCl007Rptcovexp = ' ';
	protected char wsCl007Rptretro = ' ';
	protected char wsCl007Rptextend = ' ';
	protected char wsCl007Clmcoveff = ' ';
	protected char wsCl007Clmcovexp = ' ';
	protected char wsCl007Clmretro = ' ';
	protected char wsCl007Clmextend = ' ';
	protected String location = "";
	protected String masterco = "";
	protected String symbol = "";
	protected String policyno = "";
	protected String module = "";
	protected VerOnlyInd verOnlyInd = new VerOnlyInd();
	protected Wc02OutRec wc02OutRec = new Wc02OutRec();
	protected String location2 = "";
	protected String masterco2 = "";
	protected String policyco = "";
	protected String riskState = "";
	protected String wc02Anvdate = "";
	protected String bcEffdate = "";
	protected String bcExpdate = "";
	protected String actualAnvdate = "";
	protected String splitAnvdate = "";
	protected ProcessedEstValsSbSplitSw processedEstValsSbSplitSw = new ProcessedEstValsSbSplitSw();
	protected ProcessedAudValsSbSplitSw processedAudValsSbSplitSw = new ProcessedAudValsSbSplitSw();
	protected char prEExpInd = ' ';
	protected static final char PRORATE_EXP_RATE_CHGS = 'Y';
	protected SplitEstCalcsInd splitEstCalcsInd = new SplitEstCalcsInd();
	protected RateSp1ByPolEffInd rateSp1ByPolEffInd = new RateSp1ByPolEffInd();
	protected SplitArdBy3MonRuleInd splitArdBy3MonRuleInd = new SplitArdBy3MonRuleInd();
	protected SplitEnteredClassesInd splitEnteredClassesInd = new SplitEnteredClassesInd();
	protected EstimatingCnPrfBefFaInd estimatingCnPrfBefFaInd = new EstimatingCnPrfBefFaInd();
	protected String rateState = "";
	protected String classDds = "";
	protected String descSeq = "";
	protected String bcEffdate2 = "";
	protected String actualAnvdate2 = "";
	protected String splitAnvdate2 = "";
	protected ProcessedValsSbSplitSw processedValsSbSplitSw = new ProcessedValsSbSplitSw();
	protected RateSp1ByPolEffInd rateSp1ByPolEffInd2 = new RateSp1ByPolEffInd();
	protected SplitToFind splitToFind = new SplitToFind();
	protected String descFound = "";
	protected DescApplicableInd descApplicableInd = new DescApplicableInd();
	protected DatePrParms datePrParms = new DatePrParms();
	protected Cobolsqlca cobolsqlca = new Cobolsqlca();
	protected short sqlIoOk;
	protected short sqlNoRecord = ((short) 100);
	protected short sqlObjectNotFound = ((short) -204);
	protected short sqlCursorAlreadyOpen = ((short) -502);
	protected short sqlRecordExist = ((short) -803);
	protected short sqlObjectLock = ((short) -913);
	protected short sqlTableLock = ((short) -7958);
	protected String location3 = "";
	protected String masterco3 = "";
	protected String symbol2 = "";
	protected String policyno2 = "";
	protected String module2 = "";
	protected String lossdte = "";
	protected String errorMsg = "";
	protected String errorField = "";
	protected String wsStateCode = "";
	protected String wsStateNumb = "";
	protected short numOfEntriesForSql;
	protected short wsMaxUnit = ((short) 0500);
	protected IABODynamicArray<UnitTableEntryArr> unitTableEntryArr =
		ABOCollectionCreator.getReferenceFactory(UnitTableEntryArr.class).createDynamicArray(1);

	protected Pmspsa05DAO pmspsa05DAO = new Pmspsa05DAO();
	protected Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();

	protected Pmspsa15DAO pmspsa15DAO = new Pmspsa15DAO();
	protected Pmspsa35DAO pmspsa35DAO = new Pmspsa35DAO();
	protected Tbpp005DAO tbpp005DAO = new Tbpp005DAO();
	protected Tbpp004DAO tbpp004DAO = new Tbpp004DAO();
	protected Tbcl007DAO tbcl007DAO = new Tbcl007DAO();
	protected TbtransDAO tbtransDAO = new TbtransDAO();
	protected Tbtl29DAO tbtl29DAO = new Tbtl29DAO();
	protected Tbtc22DAO tbtc22DAO = new Tbtc22DAO();
	protected Tbwc30DAO tbwc30DAO = new Tbwc30DAO();
	protected Pmspsa15TO pmspsa15TO = new Pmspsa15TO();

	protected Pmspsa35TO pmspsa35TO = new Pmspsa35TO();
	protected Pmspsa05TO pmspsa05TO = new Pmspsa05TO();
	protected Pmsp0200TO pmsp0200TO = new Pmsp0200TO();

	protected Tbpp005TO tbpp005TO = new Tbpp005TO();
	protected Tbpp004TO tbpp004TO = new Tbpp004TO();
	protected Tbcl007TO tbcl007TO = new Tbcl007TO();
	protected TbtransTO tbtransTO = new TbtransTO();
	protected Tbtc22TO tbtc22TO = new Tbtc22TO();
	protected Tbwc30TO tbwc30TO = new Tbwc30TO();
	protected Tbtl29TO tbtl29TO = new Tbtl29TO();
	
	protected Asbbcpl0DAO asbbcpl0DAO = new Asbbcpl0DAO();
	protected Asbbcpl0TO asbbcpl0TO = new Asbbcpl0TO();
	protected Pmspsa50DAO pmspsa50DAO = new Pmspsa50DAO();

	protected Tbts01TO tbts01to2 = new Tbts01TO();
	protected String holdUnit = "";
	protected char asb1cpl4CallFunction = ' ';
	protected String asbqcpl2ReturnCode = "";
	protected AsbqcppTO fbocpevRec = Initialize.initSpaces(new AsbqcppTO());
	protected char asbqcpl2CallFunction = ' ';
	protected String insline = "";
	protected boolean wsClaimMade = false;
	protected boolean endOfCoverageSw = false;
	protected boolean checkReinsRecord = false;
	protected short holdCovSeq;
	protected String holdUnitNo = "";
	
	protected char flag = 'N';

	protected Basclcovl1 () {
		this.data = new Basclcovl1TO();

		unitTableEntryArr.assign((1) - 1, new UnitTableEntryArr());
	}
	@Override
	protected void mainSubroutine () {
		main1();
	}

	protected void main1() {
		housekeeping();
 		processRecords();
		rng0300CloseFiles();
	}

	protected void housekeeping() {
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
	}

	protected void processRecords() {
		pmsp0200RecTO.rec02fmtRec.setLocation(this.data.getLocation());
		pmsp0200RecTO.rec02fmtRec.setModule(this.data.getModule());
		pmsp0200RecTO.rec02fmtRec.setSymbol(this.data.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(this.data.getMasterco());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(this.data.getPolicyno());
		pmsp0200RecTO.rec02fmtRec.setId02("02");
		pmsp0200RecTO.rec02fmtRec.setTrans0stat('P');
		pmsp0200TO =
			Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO.rec02fmtRec.getId02(), pmsp0200RecTO.rec02fmtRec.getLocation(),
				pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(), pmsp0200RecTO.rec02fmtRec.getMaster0co(),
				pmsp0200RecTO.rec02fmtRec.getModule(), pmsp0200RecTO.rec02fmtRec.getTrans0stat());
		if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
			pmsp0200RecTO.setData(pmsp0200TO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
			pmsp0200RecTO.rec02fmtRec.setTrans0stat('V');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO.rec02fmtRec.getId02(), pmsp0200RecTO.rec02fmtRec.getLocation(),
					pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(), pmsp0200RecTO.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO.rec02fmtRec.getModule(), pmsp0200RecTO.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO.setData(pmsp0200TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				holdFileStatus = wsFileStatus21.getValue();
			}
		}
		yy = pmsp0200RecTO.rec02fmtRec.getEff0yr();
		mm = pmsp0200RecTO.rec02fmtRec.getEff0mo();
		dd = pmsp0200RecTO.rec02fmtRec.getEff0da();
		yy2 = pmsp0200RecTO.rec02fmtRec.getExp0yr();
		mm2 = pmsp0200RecTO.rec02fmtRec.getExp0mo();
		dd2 = pmsp0200RecTO.rec02fmtRec.getExp0da();
	
		readTbwc30();
		getPwc02Info();
		
		if (!workersCompPolicySw) {
			readASBBCPL0();
		}
		
		//RSK - Start
		getUnit();
		//RSK - End
		while (!(wsIndex > 9999  || wsEndOfLoopSw == END_OF_LOOP)) {
			loadCoverages();
		}
		wsIndex = ((short) ((wsIndex - 1) % 10000));
		numOfEntriesForSql = wsIndex;
		resultSetTO.addResultSet(unitTableEntryArr, numOfEntriesForSql);

	}

	protected void readASBBCPL0()
	{
		
		insline = this.data.getInslineDesc().replaceFirst("^0+", "");
		asbbcpl0TO.setBbaacd(this.data.getLocation());
		asbbcpl0TO.setBbabcd(this.data.getMasterco());
		asbbcpl0TO.setBbartx(this.data.getSymbol());
		asbbcpl0TO.setBbastx(this.data.getPolicyno());
		asbbcpl0TO.setBbadnb(this.data.getModule());
		asbbcpl0TO.setBbagtx(insline);
		asbbcpl0TO.setBbc6st('V');
		asbbcpl0TO =
			Asbbcpl0DAO.selectById(asbbcpl0TO, asbbcpl0TO.getBbaacd(), asbbcpl0TO.getBbabcd(), asbbcpl0TO
				.getBbartx(), asbbcpl0TO.getBbastx(), asbbcpl0TO.getBbadnb(), asbbcpl0TO.getBbagtx(),
				asbbcpl0TO.getBbc6st());
		if (asbbcpl0TO.getDBAccessStatus().isSuccess()) {
			if (asbbcpl0TO.getBbi4tx() == 'Y')
			{
				wsClaimMade = true;
			}
			else
			{
				wsClaimMade = false;
			}
		}
		
	}
	
	//RSK - Start
	protected void getUnit(){
		Asb1cppDAO.fetchUnit1(this.data.getLocation(), this.data.getMasterco(), this.data.getSymbol(), this.data.getPolicyno(), this.data.getModule(), Integer.parseInt(this.data.getLocno()), Integer.parseInt(this.data.getSublocno()), Integer.parseInt(this.data.getUnitno()));
	}
	//RSK - End

	protected void closeFiles() {
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca.setDBAccessStatus(pmspsa05DAO.closeCsr());
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca.setDBAccessStatus(pmspsa15DAO.closePmspsa15TOCsr2());
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
	}

	protected void loadCoverages() {
		pmspsa05RecTO2.setLocation(this.data.getLocation());
		pmspsa05RecTO2.setMasterco(this.data.getMasterco());
		pmspsa05RecTO2.setModule(this.data.getModule());
		pmspsa05RecTO2.setPolicyno(this.data.getPolicyno());
		pmspsa05RecTO2.setSymbol(this.data.getSymbol());
		if (this.data.getUnitno().compareTo("") != 0){
			pmspsa05RecTO2.setUnitno(Functions.subString(this.data.getUnitno(), 1, 5));
		}else{
			pmspsa05RecTO2.setUnitno("00000");
		}
		cobolsqlca.setDBAccessStatus(pmspsa05DAO.openPmspsa05TOCsr(KeyType.GREATER_OR_EQ, pmspsa05RecTO2.getLocation(),
			pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(),
			pmspsa05RecTO2.getModule(), pmspsa05RecTO2.getUnitno()));
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (cobolsqlca.isInvalidKey()) {
			wsEndOfLoopSw = END_OF_LOOP;
			return;
		}
		pmspsa05TO = pmspsa05DAO.fetchNext(pmspsa05TO);
		pmspsa05RecTO2.setData(pmspsa05TO.getData());
		wsFileStatus21.setValue(Functions.subString(pmspsa05TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmspsa05TO.getDBAccessStatus().isEOF() || pmspsa05RecTO2.getSymbol().compareTo(this.data.getSymbol()) != 0
				|| pmspsa05RecTO2.getPolicyno().compareTo(this.data.getPolicyno()) != 0
				|| pmspsa05RecTO2.getModule().compareTo(this.data.getModule()) != 0
				|| pmspsa05RecTO2.getLocation().compareTo(this.data.getLocation()) != 0
				|| pmspsa05RecTO2.getMasterco().compareTo(this.data.getMasterco()) != 0) {
			wsEndOfLoopSw = END_OF_LOOP;
			return;
		}else{
			if (holdUnit.compareTo(pmspsa05RecTO2.getUnitno()) != 0){
				holdUnit = pmspsa05RecTO2.getUnitno();
				this.data.setUnitno(pmspsa05RecTO2.getUnitno());
			}else{
				wsEndOfLoopSw = END_OF_LOOP;
				return;
			}
		}
		loadCov();
		return;
	}

	protected void getPwc02Info() {
		IProcessInfo<Pmsbwc03TO> pmsbwc03;
		Pmsbwc03TO pmsbwc03to;
		// If WC continue....
		if (workersCompPolicySw) {
		} else {
			return;
		}
		location = this.data.getLocation();
		masterco = this.data.getMasterco();
		module = this.data.getModule();
		policyno = this.data.getPolicyno();
		symbol = this.data.getSymbol();
		verOnlyInd.setGetCurrentPOrV();
		pmsbwc03 = DynamicCall.getInfoProcessor(Pmsbwc03.class, false);
		pmsbwc03to = new Pmsbwc03TO(getGetPwc02Parms(), getWc02OutputValues());
		pmsbwc03to = pmsbwc03.run(pmsbwc03to);
		setGetPwc02Parms(pmsbwc03to.getLnkIo());
		setWc02OutputValues(pmsbwc03to.getOutputValues());
		CalledProgList.deleteInst(Pmsbwc03.class);
	}

	protected void getArdValues() {
		IProcessInfo<Baswc01m01TO> baswc01m01;
		// If WC continue....
		if (workersCompPolicySw) {
		} else {
			return;
		}
		location2 = this.data.getLocation();
		masterco2 = this.data.getMasterco();
		policyco = pmsp0200RecTO.rec02fmtRec.getCompany0no();
		riskState = pmsp0200RecTO.rec02fmtRec.getRisk0state();
		wc02Anvdate = wsAnvdateWc07e;
		bcEffdate = getW1HoldBcEffAsStr();
		bcExpdate = getW1HoldBcExpAsStr();
		baswc01m01 = DynamicCall.getInfoProcessor(Baswc01m01.class);
		Baswc01m01TO callingBaswc01m01TO = Baswc01m01.getToFromByteArray(getGetArdParms());
		callingBaswc01m01TO = baswc01m01.run(callingBaswc01m01TO);
		setGetArdParms(Baswc01m01.setByteArrayFromTo(callingBaswc01m01TO, getGetArdParms()));

		CalledProgList.deleteInst(Baswc01m01.class);
	}

	protected void loadCov() {
		pmspsa15RecTO.initPmspsa15RecordLowValues();
		pmspsa15RecTO.setEntrydte(pmspsa05RecTO2.getEntrydte());
		pmspsa15RecTO.setLob(pmspsa05RecTO2.getLob());
		pmspsa15RecTO.setLocation(pmspsa05RecTO2.getLocation());
		pmspsa15RecTO.setMasterco(pmspsa05RecTO2.getMasterco());
		pmspsa15RecTO.setModule(pmspsa05RecTO2.getModule());
		pmspsa15RecTO.setPolicyno(pmspsa05RecTO2.getPolicyno());
		pmspsa15RecTO.setSymbol(pmspsa05RecTO2.getSymbol());
		pmspsa15RecTO.setTypeact(pmspsa05RecTO2.getTypeact());
		pmspsa15RecTO.setUnitno(pmspsa05RecTO2.getUnitno());
		pmspsa15RecTO.setCovseq(((short) (Types.LOW_SHORT_VAL % 10000)));
		pmspsa15RecTO.setTransseq(((short) 9999));
		cobolsqlca.setDBAccessStatus(pmspsa15DAO.openPmspsa15TOCsr2(pmspsa15RecTO.getLocation(),
			pmspsa15RecTO.getPolicyno(), pmspsa15RecTO.getSymbol(), pmspsa15RecTO.getMasterco(), pmspsa15RecTO
				.getModule(), pmspsa15RecTO.getUnitno(), pmspsa15RecTO.getCovseq(), pmspsa15RecTO.getTransseq()));
		wsFileStatus21.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (cobolsqlca.isInvalidKey()) {
			sw4.setEndOfFile();
		}

		ws1Sa15CntrlCovseq = ((short) (0 % 10000));
		ws1Sa15CntrlCoveff = 0 % 10000000;
		ws1Sa15CntrlCovexp = 0 % 10000000;
		ws1Sa15CntrlCovseqFirm = ((short) (0 % 10000));
		ws1Sa15CntrlCoveffFirm = 0 % 10000000;
		ws1Sa15CntrlCovexpFirm = 0 % 10000000;
		if (sw4.isNotEndOfFile()) {
			while (!sw4.isEndOfFile()) {
				fillCovPage();
			}
		}
	}


	protected void checkRecordPmspsa50() {
		checkReinsRecord =
			pmspsa50DAO.checkReinsRecord(pmspsa15RecTO.getLocation(), pmspsa15RecTO.getPolicyno(), pmspsa15RecTO.getSymbol(), pmspsa15RecTO.getMasterco(), pmspsa15RecTO.getModule(),
					pmspsa15RecTO.getUnitno(), pmspsa15RecTO.getCovseq(), pmspsa15RecTO.getTransseq());
	}

	
	protected void clearErrsIfValidCov() {
	}

	protected void fillCovPage() {
		pmspsa15TO = pmspsa15DAO.fetchNextPmspsa15TOCsr2(pmspsa15TO);
		pmspsa15RecTO.setSa15(pmspsa15TO.getData());
		wsFileStatus21.setValue(Functions.subString(pmspsa15TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmspsa15TO.getDBAccessStatus().isEOF()) {
			sw4.setEndOfFile();
			return;
		}
		
		if (pmspsa15RecTO.getReinsind() != 'Y') {
			checkRecordPmspsa50();
		}
		else {
			checkReinsRecord = true;
		}

		if (!wsFileStatus21.isGoodRead() || pmspsa15RecTO.getSymbol().compareTo(this.data.getSymbol()) != 0
			|| pmspsa15RecTO.getPolicyno().compareTo(this.data.getPolicyno()) != 0
			|| pmspsa15RecTO.getModule().compareTo(this.data.getModule()) != 0
			|| pmspsa15RecTO.getLocation().compareTo(this.data.getLocation()) != 0
			|| pmspsa15RecTO.getMasterco().compareTo(this.data.getMasterco()) != 0
			|| pmspsa15RecTO.getUnitno().compareTo(this.data.getUnitno()) != 0) 
		{
			sw4.setEndOfFile();
			return;
		}

		 if (endOfCoverageSw) {     
	            if ((pmspsa15RecTO.getCovseq()==holdCovSeq) && (pmspsa15RecTO.getUnitno().equals(holdUnitNo)))     
	            {     
	                return;     
	            }     
	            else     
	            {
	                endOfCoverageSw = false;
	            }
	     }     
	             
	        if ((((this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getTransdte(), "S9999999")) ==0 ) ||     
	                (this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getTransdte(), "S9999999")) > 0 ) )    
	                && (pmspsa15RecTO.getReason().compareTo("DC"))==0)   )  
	     {     
	            endOfCoverageSw = true;     
	            holdCovSeq = pmspsa15RecTO.getCovseq();     
	            holdUnitNo = pmspsa15RecTO.getUnitno();     
	            return;     
	     } 
	     
	       
	     if(!wsClaimMade && this.data.getLossDte()!= "") {
	    	 if ((Integer.toString(pmspsa15RecTO.getCoveffdte()).compareTo(this.data.getLossDte()) > 0)
	    	 || (this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getCovexpdte(), "S9999999")) > 0))
	    	 {
	    		 return;
	    	 }
	     }
		
	     if(wsClaimMade) {
	    	 if(pmspsa15RecTO.getRetrodte() != 0 && pmspsa15RecTO.getExtenddte() ==0 )
	    	 {
	    		 if ((Integer.toString(pmspsa15RecTO.getRetrodte()).compareTo(this.data.getLossDte()) > 0)
	    		 || (this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getCovexpdte(), "S9999999")) > 0))
	    		 {
	    		  		 return;
	    		 }
	    	 }
	    	 else if (pmspsa15RecTO.getRetrodte() == 0 && pmspsa15RecTO.getExtenddte() !=0)
	    	 {
	    		 if ((Integer.toString(pmspsa15RecTO.getCoveffdte()).compareTo(this.data.getLossDte()) > 0)
	    		 || (this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getExtenddte(), "S9999999")) >  0))
	    		 {
	    		    		 return;
	    		 }
	    	 }
	    	 else
	    	 {
	    		 if ((Integer.toString(pmspsa15RecTO.getRetrodte()).compareTo(this.data.getLossDte()) > 0)
	    		 || (this.data.getLossDte().compareTo(ABOSystem.convertUsingString(pmspsa15RecTO.getExtenddte(), "S9999999")) > 0))
	    		 {
	    	    		  		 return;
	    		 }
	    	 }
	     }
	  
		if (pmspsa15RecTO.getCovstatus() != 'V') {
			return;
		}
		pendingCancelSw = 'N';


		if (sw4.isEndOfFile()) {
			return	; 	
		}
		if (pmsp0200RecTO.rec02fmtRec.getRenewal0cd() == '9') {
			if ((ABOSystem.convertUsingString(pmspsa15RecTO.getCoveffdte(), "S9999999"))
				.compareTo(pmsp0200RecTO.rec02fmtRec.getCanceldate()) == 0) {
				if (workersCompPolicySw && pmspsa15RecTO.getTransseq() == 1 || !workersCompPolicySw) {
					flatCancelSw = 'Y';
					noCovRcdsSw = NO_COV_RCDS;
					sw4.setEndOfFile();
					return;
				} 
			}
		}
		// See notes on this code at end of routine...
		if (workersCompPolicySw && pmspsa15RecTO.getMajperil().compareTo("032") == 0
			&& (pmspsa15RecTO.getTrans().compareTo("15") == 0 || pmspsa15RecTO.getTrans().compareTo("29") == 0)) {
			pmspsa15RecTO.setCovexpdte(DataExtConverter.bytesToInt(getW1HoldBcExp(), 7, SignType.SIGNED_TRAILING));
		}
		if (pmspsa15RecTO.getCovseq() == ws1Sa15CntrlCovseqFirm) {
			noCovRcdsSw = 'N';
			return;
		}
		ws1Sa15CntrlCoveff = 0 % 10000000;
		ws1Sa15CntrlCovexp = 0 % 10000000;
		// IS THIS A NEW COVERAGE SEQUENCE?
		if (pmspsa15RecTO.getCovseq() != ws1Sa15CntrlCovseq) {
			ws1Sa15CntrlCovseq = pmspsa15RecTO.getCovseq();
			ws1Sa15CntrlCovseqFirm = ((short) (0 % 10000));
			ws1Sa15CntrlCoveffFirm = 0 % 10000000;
			ws1Sa15CntrlRetroFirm = 0 % 10000000;
			ws1Sa15CntrlExtendFirm = 0 % 10000000;
			ws1Sa15CntrlCovexpFirm = 0 % 10000000;
		}
		if (ws1Sa15CntrlCoveff == 0) {
			if (pmspsa15RecTO.getRetrodte() != 0) {
				ws1Sa15CntrlCoveff = pmspsa15RecTO.getRetrodte();
				if (ws1Sa15CntrlRetroFirm == 0) {
					ws1Sa15CntrlRetroFirm = pmspsa15RecTO.getRetrodte();
				}
			} else {
				ws1Sa15CntrlCoveff = pmspsa15RecTO.getCoveffdte();
			}
		}
		if (ws1Sa15CntrlCovexp == 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
				if (ws1Sa15CntrlExtendFirm == 0) {
					ws1Sa15CntrlExtendFirm = pmspsa15RecTO.getExtenddte();
				}
			} else {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getCovexpdte();
			}
		}
		// *** THEY WILL ALSO SKIP THE "CLAIMS MADE" TABLE EDITS
		// *** BY THE EARLY "GO TO 1400-EXIT." STATEMENTS.
		// *** (20 AND 21 = FLAT)
		if (pmspsa15RecTO.getTrans().compareTo("20") == 0 || pmspsa15RecTO.getTrans().compareTo("21") == 0) {
			ws1Sa15CntrlCovexp = ws1Sa15CntrlCoveff;
			ws1Sa15CntrlCoveffFirm = ws1Sa15CntrlCoveff;
			ws1Sa15CntrlCovexpFirm = ws1Sa15CntrlCoveff;
			return;
		}
		// *** (23, 25 AND 29 = NON-FLAT)
		if (pmspsa15RecTO.getTrans().compareTo("23") == 0 || pmspsa15RecTO.getTrans().compareTo("25") == 0
			|| pmspsa15RecTO.getTrans().compareTo("29") == 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
				ws1Sa15CntrlCovexpFirm = pmspsa15RecTO.getExtenddte();
			} else {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getTransdte();
				ws1Sa15CntrlCovexpFirm = pmspsa15RecTO.getTransdte();
			}
			return;
		}
		// *** SUBSEQUENT COVERAGE DATES; DO NOT ALLOW ATTACHMENT TO ITEM.
		if (pmspsa15RecTO.getTrans().compareTo("22") == 0 && pmspsa15RecTO.getReason().compareTo("DC") == 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
				ws1Sa15CntrlCovexpFirm = pmspsa15RecTO.getExtenddte();
			} else {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getTransdte();
				if (ws1Sa15CntrlCovexpFirm == 0) {
					ws1Sa15CntrlCovexpFirm = pmspsa15RecTO.getTransdte();
				}
			}
			return;
		}
		if (pmspsa15RecTO.getTrans().compareTo("14") == 0 || pmspsa15RecTO.getTrans().compareTo("24") == 0) {
			return;
		}
		if (pmspsa15RecTO.getTrans().compareTo("12") == 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
			} else {
				if (pmspsa15RecTO.getRetrodte() != 0) {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getRetrodte();
				} else {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getTransdte();
				}
			}
		}
		// ACTUAL PERIOD COVERED; BUT
		if (pmspsa15RecTO.getTrans().compareTo("15") == 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
			} else {
				if (pmspsa15RecTO.getRetrodte() != 0) {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getRetrodte();
				} else {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getTransdte();
				}
			}
			if (pmspsa15RecTO.getTransdte() == pmspsa15RecTO.getCoveffdte()) {
				if (pmspsa15RecTO.getExtenddte() != 0) {
					ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
				} else {
					if (pmspsa15RecTO.getRetrodte() != 0) {
						ws1Sa15CntrlCoveff = pmspsa15RecTO.getRetrodte();
					} else {
						ws1Sa15CntrlCoveff = pmspsa15RecTO.getCoveffdte();
					}
				}
			}
		}
		if (pmspsa15RecTO.getTrans().compareTo("22") == 0 && pmspsa15RecTO.getReason().compareTo("DC") != 0) {
			if (pmspsa15RecTO.getExtenddte() != 0) {
				ws1Sa15CntrlCovexp = pmspsa15RecTO.getExtenddte();
			} else {
				if (pmspsa15RecTO.getRetrodte() != 0) {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getRetrodte();
				} else {
					ws1Sa15CntrlCoveff = pmspsa15RecTO.getTransdte();
				}
			}
		}
		// ***> IF FLAT CANCEL DO NOT ALLOW CLAIM TO BE ATTACHED ****
		if (ws1Sa15CntrlCoveff == ws1Sa15CntrlCovexp && pmspsa15RecTO.getReason().compareTo("DC") == 0) {
			ws1Sa15CntrlCovseqFirm = ws1Sa15CntrlCovseq;
			return;
		}
		// ***************************************************************
		if (ws1Sa15CntrlCoveffFirm != 0) {
			ws1Sa15CntrlCoveff = ws1Sa15CntrlCoveffFirm;
		}
		if (ws1Sa15CntrlCovexpFirm != 0) {
			ws1Sa15CntrlCovexp = ws1Sa15CntrlCovexpFirm;
		}
		specialArdAdjToExp();
		if (ws1Sa15CntrlRetroFirm != 0) {
			ws1Sa15CntrlCoveff = ws1Sa15CntrlRetroFirm;
		}
		if (ws1Sa15CntrlExtendFirm != 0) {
			ws1Sa15CntrlCovexp = ws1Sa15CntrlExtendFirm;
		}

		checkExcludeMajorPeril();
		if (excludedMajPerSw == EXCLUDED_MAJ_PER) {
			return;
		}
		// ***************************************************************
		// * **
		// ***************************************************************
		// **> IF RETRO DATE HAS SOMETHING IN IT
		// **> THEN GET EDITS FROM TABLE(TBCL007).
		if (pmspsa15RecTO.getRetrodte() != 0 || pmspsa15RecTO.getExtenddte() != 0 || ws1Sa15CntrlRetroFirm != 0
			|| ws1Sa15CntrlExtendFirm != 0) {
			wsSubline = pmspsa15RecTO.getSubline();
			wsClassnum = pmspsa15RecTO.getClassnum();
			wsMajperil = pmspsa15RecTO.getMajperil();
			screenErrorsSw = ' ';
			initWsCl007RecordSpaces();
			getEditDateTable();
			if (screenErrorsSw == SCREEN_ERRORS) {
				return;
			}
		}
		// ***************************************************************
		// ***************************************************************
		noCovRcdsSw = 'N';
		wsCovCount = ((short) ((1 + wsCovCount) % 1000));
		ws1Sa15CntrlCovseqFirm = pmspsa15RecTO.getCovseq();

		getUnitTableEntryArr(wsIndex).setPolicyno(pmspsa15RecTO.getPolicyno());
		getUnitTableEntryArr(wsIndex).setSymbol(pmspsa15RecTO.getSymbol());
		getUnitTableEntryArr(wsIndex).setLob(pmspsa15RecTO.getLob());
		getUnitTableEntryArr(wsIndex).setCovseq(Short.toString(pmspsa15RecTO.getCovseq()));
		getUnitTableEntryArr(wsIndex).setEntrydte(Integer.toString(pmspsa15RecTO.getEntrydte()));
		getUnitTableEntryArr(wsIndex).setAcctgdte(Integer.toString(pmspsa15RecTO.getAcctgdte()));
		getUnitTableEntryArr(wsIndex).setRetrodte(Integer.toString(pmspsa15RecTO.getRetrodte()));
		getUnitTableEntryArr(wsIndex).setExtenddte(Integer.toString(pmspsa15RecTO.getExtenddte()));
		getUnitTableEntryArr(wsIndex).setState(pmspsa15RecTO.getState());
		getUnitTableEntryArr(wsIndex).setAsline(pmspsa15RecTO.getAsline());
		getUnitTableEntryArr(wsIndex).setProdline(pmspsa15RecTO.getProdline());
		getUnitTableEntryArr(wsIndex).setExposure(ABOSystem.convertUsingString(pmspsa15RecTO.getExposure(),"S999999999999999999"));
		getUnitTableEntryArr(wsIndex).setLimitoccur(ABOSystem.convertUsingString(pmspsa15RecTO.getLimitoccur(), "S999999999999999999"));
		getUnitTableEntryArr(wsIndex).setLimitagger(ABOSystem.convertUsingString(pmspsa15RecTO.getLimitagger(), "S999999999999999999"));
		getUnitTableEntryArr(wsIndex).setDeductible(ABOSystem.convertUsingString(pmspsa15RecTO.getDeductible(), "S999999999999999999"));
		getUnitTableEntryArr(wsIndex).setCovstatus(pmspsa15RecTO.getCovstatus());
		getUnitTableEntryArr(wsIndex).setTotalorig(pmspsa15RecTO.getTotalorig());
		getUnitTableEntryArr(wsIndex).setOriginal(pmspsa15RecTO.getOriginal());
		getUnitTableEntryArr(wsIndex).setPremium(pmspsa15RecTO.getPremium());
		getUnitTableEntryArr(wsIndex).setTotalprem(pmspsa15RecTO.getTotalprem());
		getUnitTableEntryArr(wsIndex).setClassnum(pmspsa15RecTO.getClassnum());
		getUnitTableEntryArr(wsIndex).setCoveffdte(ABOSystem.convertUsingString(pmspsa15RecTO.getCoveffdte(), "S9999999"));
		getUnitTableEntryArr(wsIndex).setCovexpdte(ABOSystem.convertUsingString(pmspsa15RecTO.getCovexpdte(), "S9999999"));
		getUnitTableEntryArr(wsIndex).setCovseq(ABOSystem.convertUsingString(pmspsa15RecTO.getCovseq(), "S9999"));
		getUnitTableEntryArr(wsIndex).setMajperil(pmspsa15RecTO.getMajperil());
		getUnitTableEntryArr(wsIndex).setSubline(pmspsa15RecTO.getSubline());
		getUnitTableEntryArr(wsIndex).setTransdte(ABOSystem.convertUsingString(pmspsa15RecTO.getTransdte(), "S9999999"));
		getUnitTableEntryArr(wsIndex).setTransseq(ABOSystem.convertUsingString(pmspsa15RecTO.getTransseq(), "S9999"));
		getUnitTableEntryArr(wsIndex).setUnitno(pmspsa15RecTO.getUnitno());

		if (pmspsa15RecTO.getReinsind() == 'Y') {
		getUnitTableEntryArr(wsIndex).setReinsind(pmspsa15RecTO.getReinsind());
		}
		else if (checkReinsRecord) {
			getUnitTableEntryArr(wsIndex).setReinsind('Y');
		}
		else {
			getUnitTableEntryArr(wsIndex).setReinsind(flag);
		}
	
		tbpp004RecTO.setSubline(pmspsa15RecTO.getSubline());
		tbpp005RecTO.setClassnum(pmspsa15RecTO.getClassnum());
		tbpp005RecTO.setLob(pmspsa15RecTO.getLob());
		tbpp005RecTO.setSubline(pmspsa15RecTO.getSubline());


		formatDates();
		getPersPropertyUnit();
		if (workersCompPolicySw){
			getWc07Deductible();
		}
		tbtl29RecTO.setTl29loc(pmspsa15RecTO.getLocation());
		tbtl29RecTO.setTl29mco(pmspsa15RecTO.getMasterco());
		tbtl29RecTO.setTl29majper(pmspsa15RecTO.getMajperil());
		invalidKeySw = ' ';
		tbtl29TO =
			Tbtl29DAO.selectById(tbtl29TO, tbtl29RecTO.getTl29loc(), tbtl29RecTO.getTl29mco(), tbtl29RecTO
				.getTl29majper());
		if (tbtl29TO.getDBAccessStatus().isSuccess()) {
			tbtl29RecTO.setData(tbtl29TO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbtl29TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbtl29TO.getDBAccessStatus().isInvalidKey()) {
			invalidKeySw = 'Y';
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbtl29RecTO.setTl29loc("99");
			tbtl29TO =
				Tbtl29DAO.selectById(tbtl29TO, tbtl29RecTO.getTl29loc(), tbtl29RecTO.getTl29mco(), tbtl29RecTO
					.getTl29majper());
			if (tbtl29TO.getDBAccessStatus().isSuccess()) {
				tbtl29RecTO.setData(tbtl29TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbtl29TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbtl29TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbtl29RecTO.setTl29mco("99");
			tbtl29TO =
				Tbtl29DAO.selectById(tbtl29TO, tbtl29RecTO.getTl29loc(), tbtl29RecTO.getTl29mco(), tbtl29RecTO
					.getTl29majper());
			if (tbtl29TO.getDBAccessStatus().isSuccess()) {
				tbtl29RecTO.setData(tbtl29TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbtl29TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbtl29TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
		}
		if (invalidKeySw == INVALID_KEY) {
			getUnitTableEntryArr(wsIndex).setMajperildesc1("N/F");
		} else {
			getUnitTableEntryArr(wsIndex).setMajperildesc1(tbtl29RecTO.getTl29desc1());
		}
		tbtransRecTO.setTranactin(pmspsa15RecTO.getTrans());
		invalidKeySw = ' ';
		tbtransTO = TbtransDAO.selectById(tbtransTO, tbtransRecTO.getTranactin());
		if (tbtransTO.getDBAccessStatus().isSuccess()) {
			tbtransRecTO.setData(tbtransTO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbtransTO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbtransTO.getDBAccessStatus().isInvalidKey()) {
			invalidKeySw = 'Y';
		}

		invalidKeySw = ' ';
		if (workersCompPolicySw) {
			readSa35();
			getDescInfo();
			getUnitTableEntryArr(wsIndex).setClassdesc(descFound);
		} else {
			tbpp005TO =
				Tbpp005DAO.selectById(tbpp005TO, tbpp005RecTO.getLob(), tbpp005RecTO.getSubline(), tbpp005RecTO
					.getClassnum());
			if (tbpp005TO.getDBAccessStatus().isSuccess()) {
				tbpp005RecTO.setData(tbpp005TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbpp005TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbpp005TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
			if (invalidKeySw == INVALID_KEY) {
				getUnitTableEntryArr(wsIndex).setClassdesc("CLASS DESC NOT DEFINED");
			} else {
				getUnitTableEntryArr(wsIndex).setClassdesc(tbpp005RecTO.getClassdesc());
				getUnitTableEntryArr(wsIndex).setClassnum(tbpp005RecTO.getClassnum());
				getUnitTableEntryArr(wsIndex).setSubline(tbpp005RecTO.getSubline());
			}
		}
		invalidKeySw = ' ';
		tbpp004TO = Tbpp004DAO.selectById(tbpp004TO, tbpp004RecTO.getSubline());
		if (tbpp004TO.getDBAccessStatus().isSuccess()) {
			tbpp004RecTO.setData(tbpp004TO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbpp004TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbpp004TO.getDBAccessStatus().isInvalidKey()) {
			invalidKeySw = 'Y';
		}
		if (invalidKeySw == INVALID_KEY) {
			getUnitTableEntryArr(wsIndex).setSublindesc("SUBLINE DESC NOT DEFINED");
		} else {
			getUnitTableEntryArr(wsIndex).setSublindesc(tbpp004RecTO.getSublindesc());
			getUnitTableEntryArr(wsIndex).setSubline(tbpp004RecTO.getSubline());
		}
		wsIndex = ((short) ((1 + wsIndex) % 10000));
		ws1Sa15CntrlCovseqFirm = pmspsa15RecTO.getCovseq();
	}

	protected void getPersPropertyUnit(){
		asbqcpl2ReturnCode = "";
		initAsbqcpl2LinkRecSpaces();
		fbocpevRec.setBqaacd(pmspsa05RecTO2.getLocation());
		fbocpevRec.setBqabcd(pmspsa05RecTO2.getMasterco());
		fbocpevRec.setBqartx(pmspsa05RecTO2.getSymbol());
		fbocpevRec.setBqastx(pmspsa05RecTO2.getPolicyno());
		fbocpevRec.setBqadnb(pmspsa05RecTO2.getModule());
		if (Functions.isNumber(this.data.getUnitno())){
			fbocpevRec.setBqaenb(ABOSystem.convertUsingInt(this.data.getUnitno(), "D"));
		}else{
			fbocpevRec.setBqaenb(0);
		}
		asbqcpl2CallFunction = 'Y';
		call7();
		if (!Functions.isAllZeros(asbqcpl2ReturnCode, 7)) {
			return;
		}
		readPersPropertyUnit();
	}

	protected void call7() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbqcpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbqcpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsbqcpl2LinkRec(), getAsbqcpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbqcpl2CallFunction);
		IProcessInfo<Basasbq102TO> basasbq102 = DynamicCall.getInfoProcessor(Basasbq102.class);
		Basasbq102TO callingBasasbq102TO = Basasbq102.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbq102TO = basasbq102.run(callingBasasbq102TO);
		Basasbq102.setByteArrayFromTo(callingBasasbq102TO, arr_p0, arr_p1, arr_p2);
		asbqcpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbqcpl2LinkRec(arr_p1);
		asbqcpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected int getAsbqcpl2LinkRecSize() {
		return 687;
	}

	protected byte[] getAsbqcpl2LinkRec() {
		byte[] buf = new byte[getAsbqcpl2LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fbocpevRec.getData(), 1, AsbqcppTO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsbqcpl2LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fbocpev_buf = DataConverter.allocNcopy(buf, offset, AsbqcppTO.getSize());
		fbocpevRec.setData(fbocpev_buf);
	}

	protected void initAsbqcpl2LinkRecSpaces() {
		Initialize.initSpaces(fbocpevRec);
	}

	protected void readPersPropertyUnit() {
		getUnitTableEntryArr(wsIndex).setLimitCovA(fbocpevRec.getBqa6va());
		getUnitTableEntryArr(wsIndex).setLimitCovB(fbocpevRec.getBqa8va());
		getUnitTableEntryArr(wsIndex).setLimitCovC(fbocpevRec.getBqbava());
		getUnitTableEntryArr(wsIndex).setLimitCovD(fbocpevRec.getBqbcva());
		getUnitTableEntryArr(wsIndex).setLimitCovE(fbocpevRec.getBqbeva());
		getUnitTableEntryArr(wsIndex).setLimitCovF(fbocpevRec.getBqbgva());
	}

	protected void getWc07Deductible(){
		Pmspwc07TO pmspwc07TO = new Pmspwc07TO();
		pmspwc07TO = Pmspwc07DAO.selectById(pmspwc07TO, this.data.getLocation(), this.data.getPolicyno(), this.data.getSymbol(), this.data.getMasterco(), this.data.getModule(), pmspsa15TO.getSarstate(), 'V');
		if (pmspwc07TO.getDBAccessStatus().isSuccess()) {
			getUnitTableEntryArr(wsIndex).setWc07DedAggr(pmspwc07TO.getWc07DedAggr());
			getUnitTableEntryArr(wsIndex).setWc07DedAmt(pmspwc07TO.getWc07DedAmt());
		}
	}

	protected void readTbwc30() {
		tbwc30RecTO.setLob(pmsp0200RecTO.rec02fmtRec.getLine0bus());
		tbwc30TO = Tbwc30DAO.selectById(tbwc30TO, tbwc30RecTO.getLob());
		if (tbwc30TO.getDBAccessStatus().isSuccess()) {
			tbwc30RecTO.setWclobfmt(tbwc30TO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbwc30TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbwc30TO.getDBAccessStatus().isInvalidKey()) {
			workersCompPolicySw = false;
		}
		if (wsFileStatus21.isGoodRead()) {
			workersCompPolicySw = true;
		} else {
			workersCompPolicySw = false;
		}
	}

	protected void readSa35() {
		// In case value NOT found...
		wcDescSeq = "01";
		// Try to find value...
		if (pmspsa15RecTO.isPmspsa15RecordEmpty()) {
			return;
		} else {
			pmspsa35RecTO.sa35Rec.setCovseq(pmspsa15RecTO.getCovseq());
			pmspsa35RecTO.sa35Rec.setLocation(pmspsa15RecTO.getLocation());
			pmspsa35RecTO.sa35Rec.setMasterco(pmspsa15RecTO.getMasterco());
			pmspsa35RecTO.sa35Rec.setModule(pmspsa15RecTO.getModule());
			pmspsa35RecTO.sa35Rec.setPolicyno(pmspsa15RecTO.getPolicyno());
			pmspsa35RecTO.sa35Rec.setSymbol(pmspsa15RecTO.getSymbol());
			pmspsa35RecTO.sa35Rec.setTrans(pmspsa15RecTO.getTrans());
			pmspsa35RecTO.sa35Rec.setTransseq(pmspsa15RecTO.getTransseq());
			pmspsa35RecTO.sa35Rec.setTypeact(pmspsa15RecTO.getTypeact());
			pmspsa35RecTO.sa35Rec.setTypebureau(pmspsa15RecTO.getTypebureau());
			pmspsa35RecTO.sa35Rec.setUnitno(pmspsa15RecTO.getUnitno());
			pmspsa35TO =
				Pmspsa35DAO.selectById(pmspsa35TO, pmspsa35RecTO.sa35Rec.getLocation(), pmspsa35RecTO.sa35Rec.getPolicyno(), pmspsa35RecTO.sa35Rec
					.getSymbol(), pmspsa35RecTO.sa35Rec.getMasterco(), pmspsa35RecTO.sa35Rec.getModule(), pmspsa35RecTO.sa35Rec.getUnitno(),
					pmspsa35RecTO.sa35Rec.getCovseq(), pmspsa35RecTO.sa35Rec.getTransseq());
			if (pmspsa35TO.getDBAccessStatus().isSuccess()) {
				pmspsa35RecTO.setData(pmspsa35TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(pmspsa35TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (pmspsa35TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
			if (wsFileStatus21.isGoodRead()) {
				setHoldBureau34(DataExtConverter.strToBytes(pmspsa35RecTO.sa35Rec.getBureau34(), 112));
			}
		}
	}

	protected void getDescInfo() {
		IProcessInfo<Pmsbwc04TO> pmsbwc04;
		wsNewState = pmspsa15RecTO.getState();
		if(wsNewState.compareTo(wsOldState)== 0){
		}else{
			getAnvdate();
		}
		getArdValues();
		actualAnvdate2 = actualAnvdate;
		rateSp1ByPolEffInd2.setValue(rateSp1ByPolEffInd.getValue());
		splitAnvdate2 = splitAnvdate;
		rateState = pmspsa15RecTO.getState();
		classDds = pmspsa15RecTO.getClassnum();
		descSeq = wcDescSeq;
		bcEffdate2 = getW1HoldBcEffAsStr();
		// Use second split since there is -no- split distinction
		// on the screen.
		splitToFind.setValue('2');
		pmsbwc04 = DynamicCall.getInfoProcessor(Pmsbwc04.class);
		Pmsbwc04TO callingPmsbwc04TO = Pmsbwc04.getToFromByteArray(getGetDescParms());
		callingPmsbwc04TO = pmsbwc04.run(callingPmsbwc04TO);
		setGetDescParms(Pmsbwc04.setByteArrayFromTo(callingPmsbwc04TO, getGetDescParms()));

		CalledProgList.deleteInst(Pmsbwc04.class);
	}
	/**
	 * * Under this paragraph, program BASWC07EI1 is being called to get
     * the anniversary rate date for the given rate state.
	 */
	protected void getAnvdate(){
		initWsBaspwc07eVars();
		wsLocationWc07e =  pmspsa15RecTO.getLocation();
		wsMastercoWc07e = pmspsa15RecTO.getMasterco();
		wsSymbolWc07e =  pmspsa15RecTO.getSymbol();
		wsPolicynoWc07e =  pmspsa15RecTO.getPolicyno();
		wsModuleWc07e =  pmspsa15RecTO.getModule();
		wsStateWc07e = pmspsa15RecTO.getState();
		wsOldState = pmspsa15RecTO.getState();
		wsAnvdateWc07e = "";
		IProcessInfo<Baswc07ei1TO> baswc07ei1 = DynamicCall.getInfoProcessor(Baswc07ei1.class);
		Baswc07ei1TO callingBaswc07ei1TO = Baswc07ei1.getToFromByteArray(getWsBaspwc07eVars());
		callingBaswc07ei1TO = baswc07ei1.run(callingBaswc07ei1TO);
		setWsBaspwc07eVars(Baswc07ei1.setByteArrayFromTo(callingBaswc07ei1TO, getWsBaspwc07eVars()));

	}
	protected void initWsBaspwc07eVars(){
		wsSymbolWc07e = "";
		wsPolicynoWc07e = "";
		wsModuleWc07e = "";
	    wsMastercoWc07e = "";
		wsLocationWc07e = "";
		wsStateWc07e = "";
		wsAnvdateWc07e = "";
	}
	protected byte[] getWsBaspwc07eVars(){
		byte[] buf = new byte[25];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsSymbolWc07e, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, 	wsPolicynoWc07e, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, wsModuleWc07e, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsMastercoWc07e, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsLocationWc07e, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsStateWc07e, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsAnvdateWc07e, 7);
		return buf;
	}

	protected void setWsBaspwc07eVars(byte[] buf) {
		int offset = 1;
		wsSymbolWc07e = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsPolicynoWc07e = DataConverter.readString(buf, offset, 7);
		offset += 7;
		wsModuleWc07e = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsMastercoWc07e = DataConverter.readString(buf, offset,2);
		offset += 2;
		wsLocationWc07e = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsStateWc07e = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsAnvdateWc07e = DataConverter.readString(buf, offset, 7);
	}
	protected void specialArdAdjToExp() {
		if (workersCompPolicySw) {
		} else {
			return;
		}
		if ((ABOSystem.convertUsingString(ws1Sa15CntrlCovexp, "S9999999")).compareTo(getW1HoldBcExpAsStr()) == 0) {
		} else {
			datePrParms.date1.setDate1Cymd(DataExtConverter.intToBytes(ws1Sa15CntrlCovexp, 7, SignType.SIGNED_TRAILING));
			datePrParms.date2.setDate2Cymd(DataExtConverter.intToBytes(ws1Sa15CntrlCovexp, 7, SignType.SIGNED_TRAILING));
			datePrParms.setDateDays(BigDecimal.valueOf(1));
			datePrParms.date1Fmt.setValue("*YMD");
			datePrParms.date2Fmt.setValue("*YMD");
			datePrParms.dateOption.setValue('3');
			call2();
			ws1Sa15CntrlCovexp = DataExtConverter.bytesToInt(datePrParms.date2.getDate2Cymd(), 7, SignType.SIGNED_TRAILING);
		}
	}

	protected void formatDates() {
		datePrParms.date1.setDate1Ymd(Functions.subString(ABOSystem.convertUsingString(pmspsa15RecTO.getCoveffdte(), "S9999999"), 2, 6));
		datePrParms.date1.setDate1Cy('0');
		datePrParms.date2.setDate2("");
		datePrParms.setDateDays(BigDecimal.ZERO);
		datePrParms.date1Fmt.setValue("*YMD");
		datePrParms.date2Fmt.setValue("*MDY");
		datePrParms.dateOption.setValue('2');
		call3();
		wsDisplayDateMm = Functions.subString(datePrParms.date2.getDate2Mdy(), 1, 2);
		wsDisplayDateDd = Functions.subString(datePrParms.date2.getDate2Mdy(), 3, 2);
		wsDisplayDateYy = Functions.subString(datePrParms.date2.getDate2Mdy(), 5, 2);
		getUnitTableEntryArr(wsIndex).setCoveffdte(getWsDisplayDateAsStr());
		datePrParms.date1.setDate1Ymd(Functions.subString(ABOSystem.convertUsingString(pmspsa15RecTO.getCovexpdte(), "S9999999"), 2, 6));
		datePrParms.date1.setDate1Cy('0');
		datePrParms.date2.setDate2("");
		datePrParms.setDateDays(BigDecimal.ZERO);
		datePrParms.date1Fmt.setValue("*YMD");
		datePrParms.date2Fmt.setValue("*MDY");
		datePrParms.dateOption.setValue('2');
		call4();
		wsDisplayDateMm = Functions.subString(datePrParms.date2.getDate2Mdy(), 1, 2);
		wsDisplayDateDd = Functions.subString(datePrParms.date2.getDate2Mdy(), 3, 2);
		wsDisplayDateYy = Functions.subString(datePrParms.date2.getDate2Mdy(), 5, 2);
		getUnitTableEntryArr(wsIndex).setCovexpdte(getWsDisplayDateAsStr());
		datePrParms.date1.setDate1Ymd(Functions.subString(ABOSystem.convertUsingString(pmspsa15RecTO.getTransdte(), "S9999999"), 2, 6));
		datePrParms.date1.setDate1Cy('0');
		datePrParms.date2.setDate2("");
		datePrParms.setDateDays(BigDecimal.ZERO);
		datePrParms.date1Fmt.setValue("*YMD");
		datePrParms.date2Fmt.setValue("*MDY");
		datePrParms.dateOption.setValue('2');
		call5();
		wsDisplayDateMm = Functions.subString(datePrParms.date2.getDate2Mdy(), 1, 2);
		wsDisplayDateDd = Functions.subString(datePrParms.date2.getDate2Mdy(), 3, 2);
		wsDisplayDateYy = Functions.subString(datePrParms.date2.getDate2Mdy(), 5, 2);
		getUnitTableEntryArr(wsIndex).setTransdte(getWsDisplayDateAsStr());
	}

	protected void checkExcludeMajorPeril() {
		invalidKeySw = ' ';
		excludedMajPerSw = ' ';
		tbtc22RecTO.setLocation(pmspsa15RecTO.getLocation());
		tbtc22RecTO.setMajperil(pmspsa15RecTO.getMajperil());
		tbtc22RecTO.setMasterco(pmspsa15RecTO.getMasterco());
		tbtc22RecTO.setState(pmspsa15RecTO.getState());
		tbtc22TO =
			Tbtc22DAO.selectById(tbtc22TO, tbtc22RecTO.getLocation(), tbtc22RecTO.getMasterco(), tbtc22RecTO.getState(),
				tbtc22RecTO.getMajperil());
		if (tbtc22TO.getDBAccessStatus().isSuccess()) {
			tbtc22RecTO.setData(tbtc22TO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbtc22TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbtc22TO.getDBAccessStatus().isInvalidKey()) {
			invalidKeySw = 'Y';
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbtc22RecTO.setLocation("99");
			tbtc22TO =
				Tbtc22DAO.selectById(tbtc22TO, tbtc22RecTO.getLocation(), tbtc22RecTO.getMasterco(), tbtc22RecTO.getState(),
					tbtc22RecTO.getMajperil());
			if (tbtc22TO.getDBAccessStatus().isSuccess()) {
				tbtc22RecTO.setData(tbtc22TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbtc22TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbtc22TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbtc22RecTO.setMasterco("99");
			tbtc22TO =
				Tbtc22DAO.selectById(tbtc22TO, tbtc22RecTO.getLocation(), tbtc22RecTO.getMasterco(), tbtc22RecTO.getState(),
					tbtc22RecTO.getMajperil());
			if (tbtc22TO.getDBAccessStatus().isSuccess()) {
				tbtc22RecTO.setData(tbtc22TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbtc22TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbtc22TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbtc22RecTO.setState("99");
			tbtc22TO =
				Tbtc22DAO.selectById(tbtc22TO, tbtc22RecTO.getLocation(), tbtc22RecTO.getMasterco(), tbtc22RecTO.getState(),
					tbtc22RecTO.getMajperil());
			if (tbtc22TO.getDBAccessStatus().isSuccess()) {
				tbtc22RecTO.setData(tbtc22TO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbtc22TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbtc22TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
		}
		if (wsFileStatus21.isGoodRead()) {
			screenErrorsSw = 'Y';
			excludedMajPerSw = 'Y';
		}
	}

	protected void getEditDateTable() {
		tbcl007RecTO.setLob(pmspsa15RecTO.getLob());
		tbcl007RecTO.setLocation(pmspsa15RecTO.getLocation());
		tbcl007RecTO.setMajperil(pmspsa15RecTO.getMajperil());
		tbcl007RecTO.setMasterco(pmspsa15RecTO.getMasterco());
		invalidKeySw = ' ';
		tbcl007TO =
			Tbcl007DAO.selectById(tbcl007TO, tbcl007RecTO.getLocation(), tbcl007RecTO.getMasterco(),
				tbcl007RecTO.getLob(), tbcl007RecTO.getMajperil());
		if (tbcl007TO.getDBAccessStatus().isSuccess()) {
			tbcl007RecTO.setData(tbcl007TO.getData());
			setWsCl007Record(tbcl007RecTO.getData());
		}
		wsFileStatus21.setValue(Functions.subString(tbcl007TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbcl007TO.getDBAccessStatus().isInvalidKey()) {
			invalidKeySw = 'Y';
		}
		if (invalidKeySw == INVALID_KEY) {
			invalidKeySw = ' ';
			tbcl007RecTO.setLocation("99");
			tbcl007TO =
				Tbcl007DAO.selectById(tbcl007TO, tbcl007RecTO.getLocation(), tbcl007RecTO.getMasterco(), tbcl007RecTO
					.getLob(), tbcl007RecTO.getMajperil());
			if (tbcl007TO.getDBAccessStatus().isSuccess()) {
				tbcl007RecTO.setData(tbcl007TO.getData());
				setWsCl007Record(tbcl007RecTO.getData());
			}
			wsFileStatus21.setValue(Functions.subString(tbcl007TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbcl007TO.getDBAccessStatus().isInvalidKey()) {
				invalidKeySw = 'Y';
			}
			if (invalidKeySw == INVALID_KEY) {
				invalidKeySw = ' ';
				tbcl007RecTO.setMasterco("99");
				tbcl007TO =
					Tbcl007DAO.selectById(tbcl007TO, tbcl007RecTO.getLocation(), tbcl007RecTO.getMasterco(), tbcl007RecTO
						.getLob(), tbcl007RecTO.getMajperil());
				if (tbcl007TO.getDBAccessStatus().isSuccess()) {
					tbcl007RecTO.setData(tbcl007TO.getData());
					setWsCl007Record(tbcl007RecTO.getData());
				}
				wsFileStatus21.setValue(Functions.subString(tbcl007TO.getDBAccessStatus().getFileIOCode(), 1, 2));
				if (tbcl007TO.getDBAccessStatus().isInvalidKey()) {
					invalidKeySw = 'Y';
				}
				if (invalidKeySw == INVALID_KEY) {
					invalidKeySw = ' ';
					tbcl007RecTO.setLob("999");
					tbcl007TO =
						Tbcl007DAO.selectById(tbcl007TO, tbcl007RecTO.getLocation(), tbcl007RecTO.getMasterco(),
							tbcl007RecTO.getLob(), tbcl007RecTO.getMajperil());
					if (tbcl007TO.getDBAccessStatus().isSuccess()) {
						tbcl007RecTO.setData(tbcl007TO.getData());
						setWsCl007Record(tbcl007RecTO.getData());
					}
					wsFileStatus21.setValue(Functions.subString(tbcl007TO.getDBAccessStatus().getFileIOCode(), 1, 2));
					if (tbcl007TO.getDBAccessStatus().isInvalidKey()) {
						invalidKeySw = 'Y';
					}
					if (invalidKeySw == INVALID_KEY) {
						invalidKeySw = ' ';
						tbcl007RecTO.setMajperil("999");
						tbcl007TO =
							Tbcl007DAO.selectById(tbcl007TO, tbcl007RecTO.getLocation(), tbcl007RecTO.getMasterco(),
								tbcl007RecTO.getLob(), tbcl007RecTO.getMajperil());
						if (tbcl007TO.getDBAccessStatus().isSuccess()) {
							tbcl007RecTO.setData(tbcl007TO.getData());
							setWsCl007Record(tbcl007RecTO.getData());
						}
						wsFileStatus21.setValue(Functions.subString(tbcl007TO.getDBAccessStatus().getFileIOCode(), 1, 2));
						if (tbcl007TO.getDBAccessStatus().isInvalidKey()) {
							invalidKeySw = 'Y';
						}
					}
				}
			}
		}
		if (invalidKeySw == INVALID_KEY) {
			screenErrorsSw = 'Y';
			msgId = "CL00900";
			wsCl007Loc = pmspsa15RecTO.getLocation();
			wsCl007Mco = pmspsa15RecTO.getMasterco();
			wsCl007Lob = pmspsa15RecTO.getLob();
			wsCl007Mp = pmspsa15RecTO.getMajperil();
			callMsgControlProgram();
			initWsCl007RecordSpaces();
		} else {
			if (ws1Sa15CntrlCovexpFirm == 0) {
				ws1CovexpDate = pmspsa15RecTO.getCovexpdte();
			} else {
				ws1CovexpDate = ws1Sa15CntrlCovexpFirm;
			}
		}
	}


	protected void callMsgControlProgram() {
		call1();
		CalledProgList.deleteInst(Bascgn03.class);
		wsIndex = ((short) ((1 + wsIndex) % 10000));
	}

	protected void rng0300CloseFiles() {
		closeFiles();
	}

	public static Basclcovl1TO getToFromByteArray(byte[] dynamic_claimData) {
		Basclcovl1TO localBasclcovl1TO = new Basclcovl1TO();

		if (dynamic_claimData != null) {
			localBasclcovl1TO.setClaimData(dynamic_claimData);
	}

		return localBasclcovl1TO;
	}
	public static void setByteArrayFromTo(Basclcovl1TO localBasclcovl1TO, byte[] dynamic_claimData){

		if (dynamic_claimData != null) {
			DataConverter.arrayCopy(localBasclcovl1TO.getClaimData(), localBasclcovl1TO.getClaimDataSize(), dynamic_claimData, 1);
		}

	}

	protected void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[80];
		byte[] arr_p2 = new byte[3000];
		byte[] arr_p3 = new byte[2];
		DataConverter.writeString(arr_p0, 1, msgId, 7);
		DataConverter.writeString(arr_p1, 1, msgText, 80);
		DataConverter.writeString(arr_p2, 1, msgL2Text, 3000);
		DataConverter.writePackedBigDecimal(arr_p3, 1, errSeverity, 2, 0, SignType.NO_SIGN);
		IProcessInfo<Bascgn03TO> bascgn03 = DynamicCall.getInfoProcessor(Bascgn03.class);
		Bascgn03TO callingBascgn03TO = Bascgn03.getToFromByteArray((arr_p0), (arr_p1), (arr_p2), (arr_p3));
		callingBascgn03TO = bascgn03.run(callingBascgn03TO);
		Bascgn03.setByteArrayFromTo(callingBascgn03TO, (arr_p0), (arr_p1), (arr_p2), (arr_p3));
		msgId = DataConverter.readString(arr_p0, 1, 7);
		msgText = DataConverter.readString(arr_p1, 1, 80);
		msgL2Text = DataConverter.readString(arr_p2, 1, 3000);
		errSeverity = DataConverter.readPackedBigDecimal(arr_p3, 1, 2, 0, SignType.NO_SIGN);
	}

	protected void call2() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[4];
		byte[] arr_p2 = new byte[7];
		byte[] arr_p3 = new byte[4];
		byte[] arr_p4 = new byte[5];
		byte[] arr_p5 = new byte[1];
		DataConverter.writeString(arr_p0, 1, datePrParms.date1.getDate1(), 7);
		DataConverter.writeString(arr_p1, 1, datePrParms.date1Fmt.getValue(), 4);
		DataConverter.writeString(arr_p2, 1, datePrParms.date2.getDate2(), 7);
		DataConverter.writeString(arr_p3, 1, datePrParms.date2Fmt.getValue(), 4);
		DataConverter.writePackedBigDecimal(arr_p4, 1, datePrParms.getDateDays(), 5, 0, SignType.SIGNED_TRAILING);
		DataConverter.writeChar(arr_p5, 1, datePrParms.dateOption.getValue());
		IProcessInfo<DateprTO> datepr = DynamicCall.getInfoProcessor(Datepr.class);
		DateprTO callingDateprTO = Datepr.getToFromByteArray((arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		callingDateprTO = datepr.run(callingDateprTO);
		Datepr.setByteArrayFromTo(callingDateprTO, (arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		datePrParms.date1.setDate1(DataConverter.readString(arr_p0, 1, 7));
		datePrParms.date1Fmt.setValue(DataConverter.readString(arr_p1, 1, 4));
		datePrParms.date2.setDate2(DataConverter.readString(arr_p2, 1, 7));
		datePrParms.date2Fmt.setValue(DataConverter.readString(arr_p3, 1, 4));
		datePrParms.setDateDays(DataConverter.readPackedBigDecimal(arr_p4, 1, 5, 0, SignType.SIGNED_TRAILING));
		datePrParms.dateOption.setValue(DataConverter.readChar(arr_p5, 1));
	}

	protected void call3() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[4];
		byte[] arr_p2 = new byte[7];
		byte[] arr_p3 = new byte[4];
		byte[] arr_p4 = new byte[5];
		byte[] arr_p5 = new byte[1];
		DataConverter.writeString(arr_p0, 1, datePrParms.date1.getDate1(), 7);
		DataConverter.writeString(arr_p1, 1, datePrParms.date1Fmt.getValue(), 4);
		DataConverter.writeString(arr_p2, 1, datePrParms.date2.getDate2(), 7);
		DataConverter.writeString(arr_p3, 1, datePrParms.date2Fmt.getValue(), 4);
		DataConverter.writePackedBigDecimal(arr_p4, 1, datePrParms.getDateDays(), 5, 0, SignType.SIGNED_TRAILING);
		DataConverter.writeChar(arr_p5, 1, datePrParms.dateOption.getValue());
		IProcessInfo<DateprTO> datepr = DynamicCall.getInfoProcessor(Datepr.class);
		DateprTO callingDateprTO = Datepr.getToFromByteArray((arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		callingDateprTO = datepr.run(callingDateprTO);
		Datepr.setByteArrayFromTo(callingDateprTO, (arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		datePrParms.date1.setDate1(DataConverter.readString(arr_p0, 1, 7));
		datePrParms.date1Fmt.setValue(DataConverter.readString(arr_p1, 1, 4));
		datePrParms.date2.setDate2(DataConverter.readString(arr_p2, 1, 7));
		datePrParms.date2Fmt.setValue(DataConverter.readString(arr_p3, 1, 4));
		datePrParms.setDateDays(DataConverter.readPackedBigDecimal(arr_p4, 1, 5, 0, SignType.SIGNED_TRAILING));
		datePrParms.dateOption.setValue(DataConverter.readChar(arr_p5, 1));
	}

	protected void call4() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[4];
		byte[] arr_p2 = new byte[7];
		byte[] arr_p3 = new byte[4];
		byte[] arr_p4 = new byte[5];
		byte[] arr_p5 = new byte[1];
		DataConverter.writeString(arr_p0, 1, datePrParms.date1.getDate1(), 7);
		DataConverter.writeString(arr_p1, 1, datePrParms.date1Fmt.getValue(), 4);
		DataConverter.writeString(arr_p2, 1, datePrParms.date2.getDate2(), 7);
		DataConverter.writeString(arr_p3, 1, datePrParms.date2Fmt.getValue(), 4);
		DataConverter.writePackedBigDecimal(arr_p4, 1, datePrParms.getDateDays(), 5, 0, SignType.SIGNED_TRAILING);
		DataConverter.writeChar(arr_p5, 1, datePrParms.dateOption.getValue());
		IProcessInfo<DateprTO> datepr = DynamicCall.getInfoProcessor(Datepr.class);
		DateprTO callingDateprTO = Datepr.getToFromByteArray((arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		callingDateprTO = datepr.run(callingDateprTO);
		Datepr.setByteArrayFromTo(callingDateprTO, (arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		datePrParms.date1.setDate1(DataConverter.readString(arr_p0, 1, 7));
		datePrParms.date1Fmt.setValue(DataConverter.readString(arr_p1, 1, 4));
		datePrParms.date2.setDate2(DataConverter.readString(arr_p2, 1, 7));
		datePrParms.date2Fmt.setValue(DataConverter.readString(arr_p3, 1, 4));
		datePrParms.setDateDays(DataConverter.readPackedBigDecimal(arr_p4, 1, 5, 0, SignType.SIGNED_TRAILING));
		datePrParms.dateOption.setValue(DataConverter.readChar(arr_p5, 1));
	}

	protected void call5() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[4];
		byte[] arr_p2 = new byte[7];
		byte[] arr_p3 = new byte[4];
		byte[] arr_p4 = new byte[5];
		byte[] arr_p5 = new byte[1];
		DataConverter.writeString(arr_p0, 1, datePrParms.date1.getDate1(), 7);
		DataConverter.writeString(arr_p1, 1, datePrParms.date1Fmt.getValue(), 4);
		DataConverter.writeString(arr_p2, 1, datePrParms.date2.getDate2(), 7);
		DataConverter.writeString(arr_p3, 1, datePrParms.date2Fmt.getValue(), 4);
		DataConverter.writePackedBigDecimal(arr_p4, 1, datePrParms.getDateDays(), 5, 0, SignType.SIGNED_TRAILING);
		DataConverter.writeChar(arr_p5, 1, datePrParms.dateOption.getValue());
		IProcessInfo<DateprTO> datepr = DynamicCall.getInfoProcessor(Datepr.class);
		DateprTO callingDateprTO = Datepr.getToFromByteArray((arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		callingDateprTO = datepr.run(callingDateprTO);
		Datepr.setByteArrayFromTo(callingDateprTO, (arr_p0), (arr_p1), (arr_p2), (arr_p3), (arr_p4), (arr_p5));
		datePrParms.date1.setDate1(DataConverter.readString(arr_p0, 1, 7));
		datePrParms.date1Fmt.setValue(DataConverter.readString(arr_p1, 1, 4));
		datePrParms.date2.setDate2(DataConverter.readString(arr_p2, 1, 7));
		datePrParms.date2Fmt.setValue(DataConverter.readString(arr_p3, 1, 4));
		datePrParms.setDateDays(DataConverter.readPackedBigDecimal(arr_p4, 1, 5, 0, SignType.SIGNED_TRAILING));
		datePrParms.dateOption.setValue(DataConverter.readChar(arr_p5, 1));
	}

	protected UnitTableEntryArr getUnitTableEntryArr(int index) {
		if (index > unitTableEntryArr.size()) {
			// for an element beyond current array limits, all intermediate
			unitTableEntryArr.ensureCapacity(index);
			for (int i = unitTableEntryArr.size() + 1; i <= index; i++) {
				unitTableEntryArr.append(new UnitTableEntryArr());
				// assign default value
			}
		}
		return unitTableEntryArr.get((index) - 1);
	}

	protected int getInputValuesSize() {
		return 17;
	}

	protected byte[] getInputValues() {
		byte[] buf = new byte[getInputValuesSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, symbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, policyno, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, module, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, verOnlyInd.getValue());
		return buf;
	}

	protected int getW1HoldBcEffSize() {
		return 7;
	}

	protected byte[] getW1HoldBcEff() {
		byte[] buf = new byte[getW1HoldBcEffSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, yy, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, mm, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, dd, 2);
		return buf;
	}

	protected String getW1HoldBcEffAsStr() {
		return DataConverter.readString(getW1HoldBcEff(), 1, getW1HoldBcEff().length);
	}

	protected int getW1HoldBcExpSize() {
		return 7;
	}

	protected byte[] getW1HoldBcExp() {
		byte[] buf = new byte[getW1HoldBcExpSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, yy2, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, mm2, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, dd2, 2);
		return buf;
	}

	protected String getW1HoldBcExpAsStr() {
		return DataConverter.readString(getW1HoldBcExp(), 1, getW1HoldBcExp().length);
	}

	protected int getWsCl007RecordSize() {
		return 28;
	}

	protected byte[] getWsCl007Record() {
		byte[] buf = new byte[getWsCl007RecordSize()];
		int offset = 1;
		DataConverter.arrayCopy(getWsCl007Key(), 1, getWsCl007KeySize(), buf, offset);
		offset += getWsCl007KeySize();
		DataConverter.arrayCopy(getWsCl007Data(), 1, getWsCl007DataSize(), buf, offset);
		return buf;
	}

	protected int getWsCl007KeySize() {
		return 10;
	}

	protected byte[] getWsCl007Key() {
		byte[] buf = new byte[getWsCl007KeySize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsCl007Loc, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsCl007Mco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsCl007Lob, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsCl007Mp, 3);
		return buf;
	}

	protected int getWsCl007DataSize() {
		return 18;
	}

	protected byte[] getWsCl007Data() {
		byte[] buf = new byte[getWsCl007DataSize()];
		int offset = 1;
		DataConverter.writeChar(buf, offset, wsCl007Lossdte);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptdte);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmmadedte);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Dolsoftedt);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptsoftedt);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmsoftedt);
		offset += 1;
		DataConverter.arrayCopy(getWsCl007Dol(), 1, getWsCl007DolSize(), buf, offset);
		offset += getWsCl007DolSize();
		DataConverter.arrayCopy(getWsCl007Rpt(), 1, getWsCl007RptSize(), buf, offset);
		offset += getWsCl007RptSize();
		DataConverter.arrayCopy(getWsCl007Clm(), 1, getWsCl007ClmSize(), buf, offset);
		return buf;
	}

	protected int getWsCl007DolSize() {
		return 4;
	}

	protected byte[] getWsCl007Dol() {
		byte[] buf = new byte[getWsCl007DolSize()];
		int offset = 1;
		DataConverter.writeChar(buf, offset, wsCl007Dolcoveff);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Dolcovexp);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Dolretro);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Dolextend);
		return buf;
	}

	protected int getWsCl007RptSize() {
		return 4;
	}

	protected byte[] getWsCl007Rpt() {
		byte[] buf = new byte[getWsCl007RptSize()];
		int offset = 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptcoveff);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptcovexp);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptretro);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Rptextend);
		return buf;
	}

	protected int getWsCl007ClmSize() {
		return 4;
	}

	protected byte[] getWsCl007Clm() {
		byte[] buf = new byte[getWsCl007ClmSize()];
		int offset = 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmcoveff);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmcovexp);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmretro);
		offset += 1;
		DataConverter.writeChar(buf, offset, wsCl007Clmextend);
		return buf;
	}

	protected void initWsCl007RecordSpaces() {
		initWsCl007KeySpaces();
		initWsCl007DataSpaces();
	}

	protected void initWsCl007KeySpaces() {
		wsCl007Loc = "";
		wsCl007Mco = "";
		wsCl007Lob = "";
		wsCl007Mp = "";
	}

	protected void initWsCl007DataSpaces() {
		wsCl007Lossdte = ' ';
		wsCl007Rptdte = ' ';
		wsCl007Clmmadedte = ' ';
		wsCl007Dolsoftedt = ' ';
		wsCl007Rptsoftedt = ' ';
		wsCl007Clmsoftedt = ' ';
		initWsCl007DolSpaces();
		initWsCl007RptSpaces();
		initWsCl007ClmSpaces();
	}

	protected void initWsCl007DolSpaces() {
		wsCl007Dolcoveff = ' ';
		wsCl007Dolcovexp = ' ';
		wsCl007Dolretro = ' ';
		wsCl007Dolextend = ' ';
	}

	protected void initWsCl007RptSpaces() {
		wsCl007Rptcoveff = ' ';
		wsCl007Rptcovexp = ' ';
		wsCl007Rptretro = ' ';
		wsCl007Rptextend = ' ';
	}

	protected void initWsCl007ClmSpaces() {
		wsCl007Clmcoveff = ' ';
		wsCl007Clmcovexp = ' ';
		wsCl007Clmretro = ' ';
		wsCl007Clmextend = ' ';
	}

	protected int getHoldBureau34Size() {
		return 112;
	}

	protected byte[] getHoldBureau34() {
		byte[] buf = new byte[getHoldBureau34Size()];
		int offset = 1;
		DataConverter.writeString(buf, offset, basclcovl1filler1, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wcDescSeq, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, basclcovl1filler2, 31);
		offset += 31;
		DataConverter.writeString(buf, offset, basclcovl1filler3, 76);
		return buf;
	}

	protected void setHoldBureau34(byte[] buf) {
		int offset = 1;
		basclcovl1filler1 = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wcDescSeq = DataConverter.readString(buf, offset, 2);
		offset += 2;
		basclcovl1filler2 = DataConverter.readString(buf, offset, 31);
		offset += 31;
		basclcovl1filler3 = DataConverter.readString(buf, offset, 76);
	}

	protected int getInputValuesSize2() {
		return 34;
	}

	protected byte[] getInputValues2() {
		byte[] buf = new byte[getInputValuesSize2()];
		int offset = 1;
		DataConverter.writeString(buf, offset, rateState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, classDds, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, descSeq, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, bcEffdate2, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, actualAnvdate2, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, splitAnvdate2, 7);
		offset += 7;
		DataConverter.writeChar(buf, offset, processedValsSbSplitSw.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, rateSp1ByPolEffInd2.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, splitToFind.getValue());
		return buf;
	}

	protected int getOutputValuesSize() {
		return 22;
	}

	protected byte[] getOutputValues() {
		byte[] buf = new byte[getOutputValuesSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, actualAnvdate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, splitAnvdate, 7);
		offset += 7;
		DataConverter.writeChar(buf, offset, processedEstValsSbSplitSw.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, processedAudValsSbSplitSw.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, prEExpInd);
		offset += 1;
		DataConverter.writeChar(buf, offset, splitEstCalcsInd.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, rateSp1ByPolEffInd.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, splitArdBy3MonRuleInd.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, splitEnteredClassesInd.getValue());
		offset += 1;
		DataConverter.writeChar(buf, offset, estimatingCnPrfBefFaInd.getValue());
		return buf;
	}

	protected int getWsDisplayDateSize() {
		return 8;
	}

	protected byte[] getWsDisplayDate() {
		byte[] buf = new byte[getWsDisplayDateSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsDisplayDateMm, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, slash1);
		offset += 1;
		DataConverter.writeString(buf, offset, wsDisplayDateDd, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, slash2);
		offset += 1;
		DataConverter.writeString(buf, offset, wsDisplayDateYy, 2);
		return buf;
	}

	protected String getWsDisplayDateAsStr() {
		return DataConverter.readString(getWsDisplayDate(), 1, getWsDisplayDate().length);
	}

	protected void setWsCl007Record(byte[] buf) {
		int offset = 1;
		byte[] wsCl007Key_buf = DataConverter.allocNcopy(buf, offset, getWsCl007KeySize());
		setWsCl007Key(wsCl007Key_buf);
		offset += getWsCl007KeySize();
		byte[] wsCl007Data_buf = DataConverter.allocNcopy(buf, offset, getWsCl007DataSize());
		setWsCl007Data(wsCl007Data_buf);
	}

	protected void setWsCl007Key(byte[] buf) {
		int offset = 1;
		wsCl007Loc = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsCl007Mco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsCl007Lob = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsCl007Mp = DataConverter.readString(buf, offset, 3);
	}

	protected void setWsCl007Data(byte[] buf) {
		int offset = 1;
		wsCl007Lossdte = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Rptdte = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Clmmadedte = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Dolsoftedt = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Rptsoftedt = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Clmsoftedt = DataConverter.readChar(buf, offset);
		offset += 1;
		byte[] wsCl007Dol_buf = DataConverter.allocNcopy(buf, offset, getWsCl007DolSize());
		setWsCl007Dol(wsCl007Dol_buf);
		offset += getWsCl007DolSize();
		byte[] wsCl007Rpt_buf = DataConverter.allocNcopy(buf, offset, getWsCl007RptSize());
		setWsCl007Rpt(wsCl007Rpt_buf);
		offset += getWsCl007RptSize();
		byte[] wsCl007Clm_buf = DataConverter.allocNcopy(buf, offset, getWsCl007ClmSize());
		setWsCl007Clm(wsCl007Clm_buf);
	}

	protected void setWsCl007Dol(byte[] buf) {
		int offset = 1;
		wsCl007Dolcoveff = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Dolcovexp = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Dolretro = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Dolextend = DataConverter.readChar(buf, offset);
	}

	protected void setWsCl007Rpt(byte[] buf) {
		int offset = 1;
		wsCl007Rptcoveff = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Rptcovexp = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Rptretro = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Rptextend = DataConverter.readChar(buf, offset);
	}

	protected void setWsCl007Clm(byte[] buf) {
		int offset = 1;
		wsCl007Clmcoveff = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Clmcovexp = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Clmretro = DataConverter.readChar(buf, offset);
		offset += 1;
		wsCl007Clmextend = DataConverter.readChar(buf, offset);
	}

	protected String getWsCl007DolAsStr() {
		return DataConverter.readString(getWsCl007Dol(), 1, getWsCl007Dol().length);
	}

	protected String getWsCl007KeyAsStr() {
		return DataConverter.readString(getWsCl007Key(), 1, getWsCl007Key().length);
	}

	protected int getWsMsgdtaSize() {
		return 50;
	}

	protected byte[] getWsMsgdta() {
		byte[] buf = new byte[getWsMsgdtaSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, basclcovl1filler4, 13);
		offset += 13;
		DataConverter.writeString(buf, offset, wsMajperil, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, basclcovl1filler5, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, wsSubline, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, basclcovl1filler6, 15);
		offset += 15;
		DataConverter.writeString(buf, offset, wsClassnum, 6);
		return buf;
	}

	protected String getWsMsgdtaAsStr() {
		return DataConverter.readString(getWsMsgdta(), 1, getWsMsgdta().length);
	}

	protected String getWsCl007RptAsStr() {
		return DataConverter.readString(getWsCl007Rpt(), 1, getWsCl007Rpt().length);
	}

	protected String getWsCl007ClmAsStr() {
		return DataConverter.readString(getWsCl007Clm(), 1, getWsCl007Clm().length);
	}

	protected int getPoleditLinkSize() {
		return 153;
	}

	protected byte[] getPoleditLink() {
		byte[] buf = new byte[getPoleditLinkSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location3, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco3, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, symbol2, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, policyno2, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, module2, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lossdte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, errorMsg, 80);
		offset += 80;
		DataConverter.writeString(buf, offset, errorField, 50);
		return buf;
	}

	protected int getGetPwc02ParmsSize() {
		return 17;
	}

	protected byte[] getGetPwc02Parms() {
		byte[] buf = new byte[getGetPwc02ParmsSize()];
		int offset = 1;
		DataConverter.arrayCopy(getInputValues(), 1, getInputValuesSize(), buf, offset);
		return buf;
	}

	protected int getWc02OutputValuesSize() {
		return 515;
	}

	protected byte[] getWc02OutputValues() {
		byte[] buf = new byte[getWc02OutputValuesSize()];
		int offset = 1;
		DataConverter.arrayCopy(wc02OutRec.getWc02Out(), 1, Wc02OutRec.getWc02OutSize(), buf, offset);
		return buf;
	}

	protected void setGetPwc02Parms(byte[] buf) {
		int offset = 1;
		byte[] inputValues_buf = DataConverter.allocNcopy(buf, offset, getInputValuesSize());
		setInputValues(inputValues_buf);
	}

	protected void setInputValues(byte[] buf) {
		int offset = 1;
		location = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		symbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		policyno = DataConverter.readString(buf, offset, 7);
		offset += 7;
		module = DataConverter.readString(buf, offset, 2);
		offset += 2;
		verOnlyInd.setValue(DataConverter.readChar(buf, offset));
	}

	protected void setWc02OutputValues(byte[] buf) {
		int offset = 1;
		byte[] wc02Out_buf = DataConverter.allocNcopy(buf, offset, Wc02OutRec.getWc02OutSize());
		wc02OutRec.setWc02Out(wc02Out_buf);
	}

	protected int getGetArdParmsSize() {
		return 51;
	}

	protected byte[] getGetArdParms() {
		byte[] buf = new byte[getGetArdParmsSize()];
		int offset = 1;
		DataConverter.arrayCopy(getInputValues3(), 1, getInputValuesSize3(), buf, offset);
		offset += getInputValuesSize3();
		DataConverter.arrayCopy(getOutputValues(), 1, getOutputValuesSize(), buf, offset);
		return buf;
	}

	protected int getInputValuesSize3() {
		return 29;
	}

	protected byte[] getInputValues3() {
		byte[] buf = new byte[getInputValuesSize3()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location2, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco2, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, policyco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, riskState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wc02Anvdate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcEffdate, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, bcExpdate, 7);
		return buf;
	}

	protected void setGetArdParms(byte[] buf) {
		int offset = 1;
		byte[] inputValues2_buf = DataConverter.allocNcopy(buf, offset, getInputValuesSize3());
		setInputValues2(inputValues2_buf);
		offset += getInputValuesSize3();
		byte[] outputValues_buf = DataConverter.allocNcopy(buf, offset, getOutputValuesSize());
		setOutputValues(outputValues_buf);
	}

	protected void setInputValues2(byte[] buf) {
		int offset = 1;
		location2 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco2 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		policyco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		riskState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wc02Anvdate = DataConverter.readString(buf, offset, 7);
		offset += 7;
		bcEffdate = DataConverter.readString(buf, offset, 7);
		offset += 7;
		bcExpdate = DataConverter.readString(buf, offset, 7);
	}

	protected void setOutputValues(byte[] buf) {
		int offset = 1;
		actualAnvdate = DataConverter.readString(buf, offset, 7);
		offset += 7;
		splitAnvdate = DataConverter.readString(buf, offset, 7);
		offset += 7;
		processedEstValsSbSplitSw.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		processedAudValsSbSplitSw.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		prEExpInd = DataConverter.readChar(buf, offset);
		offset += 1;
		splitEstCalcsInd.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		rateSp1ByPolEffInd.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		splitArdBy3MonRuleInd.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		splitEnteredClassesInd.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		estimatingCnPrfBefFaInd.setValue(DataConverter.readChar(buf, offset));
	}

	protected int getGetDescParmsSize() {
		return 95;
	}

	protected byte[] getGetDescParms() {
		byte[] buf = new byte[getGetDescParmsSize()];
		int offset = 1;
		DataConverter.arrayCopy(getInputValues2(), 1, getInputValuesSize2(), buf, offset);
		offset += getInputValuesSize2();
		DataConverter.arrayCopy(getOutputValues2(), 1, getOutputValuesSize2(), buf, offset);
		return buf;
	}

	protected int getOutputValuesSize2() {
		return 61;
	}

	protected byte[] getOutputValues2() {
		byte[] buf = new byte[getOutputValuesSize2()];
		int offset = 1;
		DataConverter.writeString(buf, offset, descFound, 60);
		offset += 60;
		DataConverter.writeChar(buf, offset, descApplicableInd.getValue());
		return buf;
	}

	protected void setGetDescParms(byte[] buf) {
		int offset = 1;
		byte[] inputValues3_buf = DataConverter.allocNcopy(buf, offset, getInputValuesSize2());
		setInputValues3(inputValues3_buf);
		offset += getInputValuesSize2();
		byte[] outputValues2_buf = DataConverter.allocNcopy(buf, offset, getOutputValuesSize2());
		setOutputValues2(outputValues2_buf);
	}

	protected void setInputValues3(byte[] buf) {
		int offset = 1;
		rateState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		classDds = DataConverter.readString(buf, offset, 6);
		offset += 6;
		descSeq = DataConverter.readString(buf, offset, 2);
		offset += 2;
		bcEffdate2 = DataConverter.readString(buf, offset, 7);
		offset += 7;
		actualAnvdate2 = DataConverter.readString(buf, offset, 7);
		offset += 7;
		splitAnvdate2 = DataConverter.readString(buf, offset, 7);
		offset += 7;
		processedValsSbSplitSw.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		rateSp1ByPolEffInd2.setValue(DataConverter.readChar(buf, offset));
		offset += 1;
		splitToFind.setValue(DataConverter.readChar(buf, offset));
	}

	protected void setOutputValues2(byte[] buf) {
		int offset = 1;
		descFound = DataConverter.readString(buf, offset, 60);
		offset += 60;
		descApplicableInd.setValue(DataConverter.readChar(buf, offset));
	}

	protected void setPoleditLink(byte[] buf) {
		int offset = 1;
		location3 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco3 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		symbol2 = DataConverter.readString(buf, offset, 3);
		offset += 3;
		policyno2 = DataConverter.readString(buf, offset, 7);
		offset += 7;
		module2 = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lossdte = DataConverter.readString(buf, offset, 7);
		offset += 7;
		errorMsg = DataConverter.readString(buf, offset, 80);
		offset += 80;
		errorField = DataConverter.readString(buf, offset, 50);
	}
}
