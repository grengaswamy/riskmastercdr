package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicCallException;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.c2ab.util.Types;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.ctrl.Rec00fmtRec3;
import com.csc.pt.svc.data.dao.Basclt0100DAO;
import com.csc.pt.svc.data.dao.Basclt0102DAO;
import com.csc.pt.svc.data.dao.Basclt0103DAO;
import com.csc.pt.svc.data.dao.Basclt0104DAO;
import com.csc.pt.svc.data.dao.Basclt0105DAO;
import com.csc.pt.svc.data.dao.Basclt0106DAO;
import com.csc.pt.svc.data.dao.Basclt0300DAO;
import com.csc.pt.svc.data.dao.Basclt0311DAO;
import com.csc.pt.svc.data.dao.Basclt0312DAO;
import com.csc.pt.svc.data.dao.Basclt0313DAO;
import com.csc.pt.svc.data.dao.Basclt1400DAO;
import com.csc.pt.svc.data.dao.Basclt1402DAO;
import com.csc.pt.svc.data.dao.Basclt1502DAO;
import com.csc.pt.svc.data.dao.Pmsj0201DAO;
import com.csc.pt.svc.data.dao.Pmsj1201DAO;
import com.csc.pt.svc.data.dao.Pmsj1202DAO;
import com.csc.pt.svc.data.dao.Pmsjwc04DAO;
import com.csc.pt.svc.data.dao.Pmsl0252DAO;
import com.csc.pt.svc.data.dao.Pmsl0253DAO;
import com.csc.pt.svc.data.dao.Pmsl0254DAO;
import com.csc.pt.svc.data.dao.Pmsl0255DAO;
import com.csc.pt.svc.data.dao.Pmsl0256DAO;
import com.csc.pt.svc.data.dao.Pmsl0257DAO;
import com.csc.pt.svc.data.dao.Pmsl0258DAO;
import com.csc.pt.svc.data.dao.Pmsl0259DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Pmspmm80DAO;
import com.csc.pt.svc.data.to.Basclt0100TO;
import com.csc.pt.svc.data.to.Basclt0102TO;
import com.csc.pt.svc.data.to.Basclt0103TO;
import com.csc.pt.svc.data.to.Basclt0104TO;
import com.csc.pt.svc.data.to.Basclt0105TO;
import com.csc.pt.svc.data.to.Basclt0106TO;
import com.csc.pt.svc.data.to.Basclt0300TO;
import com.csc.pt.svc.data.to.Basclt0311TO;
import com.csc.pt.svc.data.to.Basclt0312TO;
import com.csc.pt.svc.data.to.Basclt0313TO;
import com.csc.pt.svc.data.to.Basclt1400TO;
import com.csc.pt.svc.data.to.Basclt1402TO;
import com.csc.pt.svc.data.to.Basclt1502TO;
import com.csc.pt.svc.data.to.Pmsj0201TO;
import com.csc.pt.svc.data.to.Pmsj1201TO;
import com.csc.pt.svc.data.to.Pmsj1202TO;
import com.csc.pt.svc.data.to.Pmsjwc04TO;
import com.csc.pt.svc.data.to.Pmsl0252TO;
import com.csc.pt.svc.data.to.Pmsl0253TO;
import com.csc.pt.svc.data.to.Pmsl0254TO;
import com.csc.pt.svc.data.to.Pmsl0255TO;
import com.csc.pt.svc.data.to.Pmsl0256TO;
import com.csc.pt.svc.data.to.Pmsl0257TO;
import com.csc.pt.svc.data.to.Pmsl0258TO;
import com.csc.pt.svc.data.to.Pmsl0259TO;
import com.csc.pt.svc.data.to.Pmspmm80TO;
import com.csc.pt.svc.db.to.Basclt0100RecTO;
import com.csc.pt.svc.db.to.Basclt0102RecTO;
import com.csc.pt.svc.db.to.Basclt0103RecTO;
import com.csc.pt.svc.db.to.Basclt0104RecTO;
import com.csc.pt.svc.db.to.Basclt0105RecTO;
import com.csc.pt.svc.db.to.Basclt0106RecTO;
import com.csc.pt.svc.db.to.Basclt0300RecTO;
import com.csc.pt.svc.db.to.Basclt0311RecTO;
import com.csc.pt.svc.db.to.Basclt0312RecTO;
import com.csc.pt.svc.db.to.Basclt0313RecTO;
import com.csc.pt.svc.db.to.Basclt1400RecTO;
import com.csc.pt.svc.db.to.Basclt1402RecTO;
import com.csc.pt.svc.db.to.Basclt1502RecTO;
import com.csc.pt.svc.rsk.to.Rskdbio021TO;
import com.csc.pt.svc.db.to.Pmsj0201RecTO;
import com.csc.pt.svc.db.to.Pmsj1201RecTO;
import com.csc.pt.svc.db.to.Pmsj1202RecTO;
import com.csc.pt.svc.db.to.Pmsjwc04RecTO;
import com.csc.pt.svc.db.to.Pmsl0252RecTO;
import com.csc.pt.svc.db.to.Pmsl0253RecTO;
import com.csc.pt.svc.db.to.Pmsl0254RecTO;
import com.csc.pt.svc.db.to.Pmsl0255RecTO;
import com.csc.pt.svc.db.to.Pmsl0256RecTO;
import com.csc.pt.svc.db.to.Pmsl0257RecTO;
import com.csc.pt.svc.db.to.Pmsl0258RecTO;
import com.csc.pt.svc.db.to.Pmsl0259RecTO;
import com.csc.pt.svc.db.to.Pmsp0200RecTO2;
import com.csc.pt.svc.db.to.Pmspmm80RecTO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.pas.wc.LocalDataArea;
import com.csc.pt.svc.db.AddnlIntIndicator;
import com.csc.pt.svc.db.AddnlNameIndicator;
import com.csc.pt.svc.db.AddnlTypeIndicator;
import com.csc.pt.svc.db.BcListEntryArr8;
import com.csc.pt.svc.db.QwcAspList;
import com.csc.pt.svc.db.QwcCurList;
import com.csc.pt.svc.db.QwcGrpList;
import com.csc.pt.svc.db.QwcJobi0100;
import com.csc.pt.svc.db.QwcJobi0150;
import com.csc.pt.svc.db.QwcJobi0200;
import com.csc.pt.svc.db.QwcJobi0300;
import com.csc.pt.svc.db.QwcJobi0400;
import com.csc.pt.svc.db.QwcJobi0500;
import com.csc.pt.svc.db.QwcJobi0600;
import com.csc.pt.svc.db.QwcJobi0700;
import com.csc.pt.svc.db.QwcJobi0750;
import com.csc.pt.svc.db.QwcJobi0800;
import com.csc.pt.svc.db.QwcJobi0900;
import com.csc.pt.svc.db.QwcJobi1000;
import com.csc.pt.svc.db.QwcLibList;
import com.csc.pt.svc.db.QwcLibList2;
import com.csc.pt.svc.db.QwcSigList;
import com.csc.pt.svc.db.QwcTzoneList;
import com.csc.pt.svc.db.SsnIndicator;
import com.csc.pt.svc.db.WsProcessControlSw3;


public class Rskdbio021 {
	private static final String PROGRAM_ID = "RSKDBIO021";
	public Pmsp0200RecTO2 pmsp0200RecTO2 = new Pmsp0200RecTO2();
	public Pmsl0259RecTO pmsl0259RecTO = new Pmsl0259RecTO();
	public Pmsl0252RecTO pmsl0252RecTO = new Pmsl0252RecTO();
	public Pmsl0255RecTO pmsl0255RecTO = new Pmsl0255RecTO();
	public Pmsl0256RecTO pmsl0256RecTO = new Pmsl0256RecTO();
	public Pmsl0257RecTO pmsl0257RecTO = new Pmsl0257RecTO();
	public Pmsl0258RecTO pmsl0258RecTO = new Pmsl0258RecTO();
	public Pmsl0253RecTO pmsl0253RecTO = new Pmsl0253RecTO();
	public Pmsl0254RecTO pmsl0254RecTO = new Pmsl0254RecTO();
	public Pmspmm80RecTO pmspmm80RecTO = new Pmspmm80RecTO();
	public Basclt1400RecTO basclt1400RecTO = new Basclt1400RecTO();
	public Basclt1402RecTO basclt1402RecTO = new Basclt1402RecTO();
	public Basclt0100RecTO basclt0100RecTO = new Basclt0100RecTO();
	public Basclt0103RecTO basclt0103RecTO = new Basclt0103RecTO();
	public Basclt0102RecTO basclt0102RecTO = new Basclt0102RecTO();
	public Basclt0106RecTO basclt0106RecTO = new Basclt0106RecTO();
	public Basclt0104RecTO basclt0104RecTO = new Basclt0104RecTO();
	public Basclt0105RecTO basclt0105RecTO = new Basclt0105RecTO();
	public Basclt0300RecTO basclt0300RecTO = new Basclt0300RecTO();
	public Basclt0311RecTO basclt0311RecTO = new Basclt0311RecTO();
	public Basclt0312RecTO basclt0312RecTO = new Basclt0312RecTO();
	public Basclt0313RecTO basclt0313RecTO = new Basclt0313RecTO();
	public Pmsjwc04RecTO pmsjwc04RecTO = new Pmsjwc04RecTO();
	public Pmsj0201RecTO pmsj0201RecTO = new Pmsj0201RecTO();
	public Basclt1502RecTO basclt1502RecTO = new Basclt1502RecTO();
	public Pmsj1201RecTO pmsj1201RecTO = new Pmsj1201RecTO();
	public Pmsj1202RecTO pmsj1202RecTO = new Pmsj1202RecTO();
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private char endFileRead = ' ';
	public static final char END_READ_REACHED = 'Y';
	private char endCrossRead = ' ';
	public static final char END_FILE_REACHED = 'Y';
	private char endParaIndic = ' ';
	public static final char PARA_END = 'Y';
	private SsnIndicator ssnIndicator = new SsnIndicator();
	private String wsTmpMaster0co = "";
	private String wsTmpModule = "";
	private AddnlIntIndicator addnlIntIndicator = new AddnlIntIndicator();
	private AddnlTypeIndicator addnlTypeIndicator = new AddnlTypeIndicator();
	private AddnlNameIndicator addnlNameIndicator = new AddnlNameIndicator();
	private String ws1FileStatus = "";
	public static final String WS_GOOD_RETURN = "00";
	private short wsLnLength;
	private short wsFnLength;
	private short wsCtLength;
	private short wsGnLength;
	private short wsZcLength;
	private char wsErrorSw = 'Y';
	public static final char WS_ERROR_SET = 'Y';
	private short wsAttrNum;
	private int wsCount;
	private int numOfEntrys;
	private int numOfEntrysForSql;
	private String wsReturnCode = "";
	public static final String GOOD_RETURN_FROM_IO = "0000000";
	private short wsAddnlIntNameLen;
	private char wsControlSw = ' ';
	private WsProcessControlSw3 wsProcessControlSw3 = new WsProcessControlSw3();
	private String holdPreviousType = "";
	private short wsCounter = Types.INVALID_SHORT_VAL;
	private Rec00fmtRec3 rec00fmtRec3 = new Rec00fmtRec3();
	private LocalDataArea localDataArea = new LocalDataArea();
	private int jobiReceiverLength = 200;
	private String jobiFormatName = "";
	private String jobiJobName = "*";
	private String jobiJobNameInt = "";
	private int rskdbio021filler3;
	private String qusrjobi = "QUSRJOBI";
	private QwcJobi0100 qwcJobi0100 = new QwcJobi0100();
	private QwcJobi0150 qwcJobi0150 = new QwcJobi0150();
	private QwcJobi0200 qwcJobi0200 = new QwcJobi0200();	
	private QwcJobi0300 qwcJobi0300 = new QwcJobi0300();
	private QwcAspList qwcAspList = new QwcAspList();
	private QwcJobi0400 qwcJobi0400 = new QwcJobi0400();
	private QwcJobi0500 qwcJobi0500 = new QwcJobi0500();
	private QwcGrpList qwcGrpList = new QwcGrpList();
	private QwcTzoneList qwcTzoneList = new QwcTzoneList();
	private QwcJobi0600 qwcJobi0600 = new QwcJobi0600();
	private QwcLibList qwcLibList = new QwcLibList();
	private QwcJobi0700 qwcJobi0700 = new QwcJobi0700();
	private QwcLibList2 qwcLibList2 = new QwcLibList2();
	private QwcJobi0750 qwcJobi0750 = new QwcJobi0750();
	private QwcSigList qwcSigList = new QwcSigList();
	private QwcJobi0800 qwcJobi0800 = new QwcJobi0800();
	private QwcCurList qwcCurList = new QwcCurList();
	private QwcJobi0900 qwcJobi0900 = new QwcJobi0900();
	private QwcJobi1000 qwcJobi1000 = new QwcJobi1000();
	private BcListEntryArr8[] bcListEntryArr8 = ABOCollectionCreator.getReferenceFactory(BcListEntryArr8.class).createArray(101);
	private char holdBcTrans0stat = ' ';
	private String holdBcSymbol = "";
	private String holdBcPolicy0num = "";
	private String holdBcModule = "";
	private String holdBcMaster0co = "";
	private String holdBcLocation = "";
	private String holdBcEffdt = "";
	private String holdBcExpdt = "";
	private String holdBcRisk0state = "";
	private String holdBcCompany0no = "";
	private String holdBcAgency = "";
	private String holdBcLine0bus = "";
	private String holdBcZipcode = "";
	private String holdBcLastname = "";
	private String holdBcFirstname = "";
	private char holdBcMiddlename = ' ';
	private String holdBcAdd0line02 = "";
	private String holdBcAddrln1 = "";
	private String holdBcCity = "";
	private String holdBcState = "";
	private char holdBcRenewal0cd = ' ';
	private char holdBcIssue0code = ' ';
	private String holdBcFedtaxid = "";
	private char holdBcPay0code = ' ';
	private char holdBcMode0code = ' ';
	private String holdBcSort0name = "";
	private String holdBcCust0no = "";
	private String holdBcSpec0use0a = "";
	private String holdBcSpec0use0b = "";
	private String holdBcCanceldate = "";
	private String holdBcMrsseq = "";
	private String holdBcGroupno = "";
	private String holdBcPhone1 = "";
	private String holdBcSsn = "";
	private char holdBcNametype = ' ';
	private char holdBcNamestatus = ' ';
	private String holdBcStatusDesc = "";
	//FSIT: 152640, Resolution: 55204 - Begin
	//private ABODecimal holdBcTot0ag0prm = new ABODecimal(11, 2);
	
	private ABODecimal holdBcTot0ag0prm = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private String holdBcType0act = "";
	private Rskdbio021TO rskdbio021TO = new Rskdbio021TO();
	private Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	private Pmsl0259DAO pmsl0259DAO = new Pmsl0259DAO();
	private Pmsl0252DAO pmsl0252DAO = new Pmsl0252DAO();
	private Pmsl0255DAO pmsl0255DAO = new Pmsl0255DAO();
	private Pmsl0256DAO pmsl0256DAO = new Pmsl0256DAO();
	private Pmsl0257DAO pmsl0257DAO = new Pmsl0257DAO();
	private Pmsl0258DAO pmsl0258DAO = new Pmsl0258DAO();
	private Pmsl0253DAO pmsl0253DAO = new Pmsl0253DAO();
	private Pmsl0254DAO pmsl0254DAO = new Pmsl0254DAO();
	private Pmspmm80DAO pmspmm80DAO = new Pmspmm80DAO();
	private Basclt1400DAO basclt1400DAO = new Basclt1400DAO();
	private Basclt1402DAO basclt1402DAO = new Basclt1402DAO();
	private Basclt0100DAO basclt0100DAO = new Basclt0100DAO();
	private Basclt0103DAO basclt0103DAO = new Basclt0103DAO();
	private Basclt0102DAO basclt0102DAO = new Basclt0102DAO();
	private Basclt0106DAO basclt0106DAO = new Basclt0106DAO();
	private Basclt0104DAO basclt0104DAO = new Basclt0104DAO();
	private Basclt0105DAO basclt0105DAO = new Basclt0105DAO();
	private Basclt0300DAO basclt0300DAO = new Basclt0300DAO();
	private Basclt0311DAO basclt0311DAO = new Basclt0311DAO();
	private Basclt0312DAO basclt0312DAO = new Basclt0312DAO();
	private Basclt0313DAO basclt0313DAO = new Basclt0313DAO();
	private Pmsjwc04DAO pmsjwc04DAO = new Pmsjwc04DAO();
	private Pmsj0201DAO pmsj0201DAO = new Pmsj0201DAO();
	private Basclt1502DAO basclt1502DAO = new Basclt1502DAO();
	private Pmsj1201DAO pmsj1201DAO = new Pmsj1201DAO();
	private Pmsj1202DAO pmsj1202DAO = new Pmsj1202DAO();
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	private Pmspmm80TO pmspmm80TO = new Pmspmm80TO();

	private Pmsl0259TO pmsl0259TO = new Pmsl0259TO();
	private Basclt1400TO basclt1400TO = new Basclt1400TO();
	private Basclt0100TO basclt0100TO = new Basclt0100TO();
	private Basclt0300TO basclt0300TO = new Basclt0300TO();
	private Pmsj1201TO pmsj1201TO = new Pmsj1201TO();
	private Pmsjwc04TO pmsjwc04TO = new Pmsjwc04TO();
	private Pmsj1202TO pmsj1202TO = new Pmsj1202TO();
	private Basclt1502TO basclt1502TO = new Basclt1502TO();
	private Pmsl0255TO pmsl0255TO = new Pmsl0255TO();
	private Pmsl0256TO pmsl0256TO = new Pmsl0256TO();
	private Pmsl0257TO pmsl0257TO = new Pmsl0257TO();
	private Pmsl0258TO pmsl0258TO = new Pmsl0258TO();
	private Pmsl0252TO pmsl0252TO = new Pmsl0252TO();
	private Basclt0103TO basclt0103TO = new Basclt0103TO();
	private Basclt1402TO basclt1402TO = new Basclt1402TO();
	private Pmsj0201TO pmsj0201TO = new Pmsj0201TO();
	private Basclt0102TO basclt0102TO = new Basclt0102TO();
	private Basclt0106TO basclt0106TO = new Basclt0106TO();
	private Pmsl0253TO pmsl0253TO = new Pmsl0253TO();
	private Pmsl0254TO pmsl0254TO = new Pmsl0254TO();
	private Basclt0104TO basclt0104TO = new Basclt0104TO();
	private Basclt0105TO basclt0105TO = new Basclt0105TO();
	private Basclt0311TO basclt0311TO = new Basclt0311TO();
	private Basclt0312TO basclt0312TO = new Basclt0312TO();
	private Basclt0313TO basclt0313TO = new Basclt0313TO();

	public Rskdbio021() {
		for (int idx = 1; idx <= 101; idx++) {
			bcListEntryArr8[(idx) - 1] = new BcListEntryArr8();
		}
	}

	public void mainSubroutine() {
		main1();
		exitProgram();
		exit();
	}

	public void main1() {
		// * If MAXRECORDS is less than one, just return an empty resultset
		if (rskdbio021TO.getInMaxrecords() < 1) {
			numOfEntrysForSql = 0;
			resultSetTO.addResultSet(bcListEntryArr8, numOfEntrysForSql);
			exit();
		}
		// THIS SECTION OF THE CODE TO DETERMINE THE LOGICAL FILE
		// TO BE USED FOR THE SEARCH PROCESS USING THE HIGH ORDER KEY
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0) {
			// MOVE "16" TO WS-PROCESS-CONTROL-SW
			wsProcessControlSw3.setValue("11");
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0) {
			wsProcessControlSw3.setValue("9");
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0) {
			wsProcessControlSw3.setValue("5");
		}
		if (rskdbio021TO.getInState().compareTo("") != 0) {
			wsProcessControlSw3.setValue("15");
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0) {
			wsProcessControlSw3.setValue("16");
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0) {
			wsProcessControlSw3.setValue("4");
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0) {
			wsProcessControlSw3.setValue("3");
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0) {
			wsProcessControlSw3.setValue("13");
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0) {
			wsProcessControlSw3.setValue("14");
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0) {
			wsProcessControlSw3.setValue("10");
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0) {
			wsProcessControlSw3.setValue("8");
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0) {
			wsProcessControlSw3.setValue("6");
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0) {
			wsProcessControlSw3.setValue("12");
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			wsProcessControlSw3.setValue("7");
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0) {
			wsProcessControlSw3.setValue("18");
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0) {
			wsProcessControlSw3.setValue("17");
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0) {
			if (rskdbio021TO.getInPolicy0num().compareTo("") != 0) {
				if (rskdbio021TO.getInModule().compareTo("") != 0) {
					wsProcessControlSw3.setValue("1");
				} else {
					wsProcessControlSw3.setValue("2");
				}
			}
		}
		//
		// PERFORM 9900-GET-USER-INFO THRU 9900-EXIT.
		//
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// MOVE CURR-USRPRF-NAME OF QWC-JOBI0600
		pmspmm80RecTO.pmm80Rec.setUsrprf(rskdbio021TO.getInUserid());
		pmspmm80TO = Pmspmm80DAO.selectById(pmspmm80TO, pmspmm80RecTO.pmm80Rec.getUsrprf());
		if (pmspmm80TO.getDBAccessStatus().isSuccess()) {
			pmspmm80RecTO.setData(pmspmm80TO.getData());
		}
		ws1FileStatus = Functions.subString(pmspmm80TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			pmspmm80RecTO.pmm80Rec.initPmm80Spaces();
		}
		if (!Functions.isNumber(rskdbio021TO.getInMaxrecords()) || rskdbio021TO.getInMaxrecords() > 100) {
			rskdbio021TO.setInMaxrecords(((short) 100));
		}
		// DETERMINE THE LENGTH OF LAST-NAME & FIRST-NAME
		if (rskdbio021TO.getInLastname().compareTo("") != 0) {
			wsLnLength = ((short) 60);
			while (!(Functions.subString(rskdbio021TO.getInLastname(), wsLnLength, 1).compareTo("") != 0)) {
				exit1();
				wsLnLength = ((short) (wsLnLength + -1));
			}
		} else {
			wsLnLength = ((short) (60 % 100));
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0) {
			wsFnLength = ((short) 40);
			while (!(Functions.subString(rskdbio021TO.getInFirstname(), wsFnLength, 1).compareTo("") != 0)) {
				exit1();
				wsFnLength = ((short) (wsFnLength + -1));
			}
		} else {
			wsFnLength = ((short) (40 % 100));
		}
		// DETERMINE THE LENGTH OF City & Group Number
		if (rskdbio021TO.getInCity().compareTo("") != 0) {
			wsCtLength = ((short) 28);
			while (!(Functions.subString(rskdbio021TO.getInCity(), wsCtLength, 1).compareTo("") != 0)) {
				exit1();
				wsCtLength = ((short) (wsCtLength + -1));
			}
		} else {
			wsCtLength = ((short) (28 % 100));
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0) {
			wsGnLength = ((short) 10);
			while (!(Functions.subString(rskdbio021TO.getInGroupno(), wsGnLength, 1).compareTo("") != 0)) {
				exit1();
				wsGnLength = ((short) (wsGnLength + -1));
			}
		} else {
			wsGnLength = ((short) (10 % 100));
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0) {
			wsZcLength = ((short) 10);
			while (!(Functions.subString(rskdbio021TO.getInZip0post(), wsZcLength, 1).compareTo("") != 0)) {
				exit1();
				wsZcLength = ((short) (wsZcLength + -1));
			}
		} else {
			wsZcLength = ((short) (10 % 100));
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0) {
			wsAddnlIntNameLen = ((short) 30);
			while (!(Functions.subString(rskdbio021TO.getInAddnlIntName(), wsAddnlIntNameLen, 1).compareTo("") != 0)) {
				exit1();
				wsAddnlIntNameLen = ((short) (wsAddnlIntNameLen + -1));
			}
		}
		//
		numOfEntrys = 0 % 100000;
		initBcListTableSpaces();
		//
		if (wsProcessControlSw3.isPolicyFullKey()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
			pmsl0259RecTO.rec02fmtRec2.setSymbol(rskdbio021TO.getInSymbol());
			pmsl0259RecTO.rec02fmtRec2.setModule(rskdbio021TO.getInModule());
			pmsl0259RecTO.rec02fmtRec2.setPolicy0num(rskdbio021TO.getInPolicy0num());
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.openPmsl0259TOCsr(KeyType.GREATER_OR_EQ, pmsl0259RecTO.rec02fmtRec2.getLocation(),
				pmsl0259RecTO.rec02fmtRec2.getPolicy0num(), pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(),
				pmsl0259RecTO.rec02fmtRec2.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0259();
			}
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt1502DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isPolicySymbol()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0255RecTO.rec02fmtRec4.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0255RecTO.rec02fmtRec4.setLocation(rskdbio021TO.getInLocation());
			pmsl0255RecTO.rec02fmtRec4.setSymbol(rskdbio021TO.getInSymbol());
			pmsl0255RecTO.rec02fmtRec4.setPolicy0num("");
			pmsl0255RecTO.rec02fmtRec4.setModule(Types.HIGH_STR_VAL);
			cobolsqlca.setDBAccessStatus(pmsl0255DAO.openPmsl0255TOCsr(KeyType.GREATER_OR_EQ, pmsl0255RecTO.rec02fmtRec4.getLocation(),
				pmsl0255RecTO.rec02fmtRec4.getMaster0co(), pmsl0255RecTO.rec02fmtRec4.getSymbol(), pmsl0255RecTO.rec02fmtRec4.getPolicy0num(),
				pmsl0255RecTO.rec02fmtRec4.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0255();
			}
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			cobolsqlca.setDBAccessStatus(pmsl0255DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isPolicyNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0256RecTO.rec02fmtRec5.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0256RecTO.rec02fmtRec5.setLocation(rskdbio021TO.getInLocation());
			pmsl0256RecTO.rec02fmtRec5.setPolicy0num(rskdbio021TO.getInPolicy0num());
			pmsl0256RecTO.rec02fmtRec5.setSymbol("");
			pmsl0256RecTO.rec02fmtRec5.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0256DAO.openPmsl0256TOCsr(KeyType.GREATER_OR_EQ, pmsl0256RecTO.rec02fmtRec5.getLocation(),
				pmsl0256RecTO.rec02fmtRec5.getMaster0co(), pmsl0256RecTO.rec02fmtRec5.getPolicy0num(), pmsl0256RecTO.rec02fmtRec5.getSymbol(),
				pmsl0256RecTO.rec02fmtRec5.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0256();
			}
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			cobolsqlca.setDBAccessStatus(pmsl0256DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isPolicyModule()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0257RecTO.rec02fmtRec6.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0257RecTO.rec02fmtRec6.setLocation(rskdbio021TO.getInLocation());
			pmsl0257RecTO.rec02fmtRec6.setModule(rskdbio021TO.getInModule());
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(pmsl0257DAO.openPmsl0257TOCsr(KeyType.GREATER_OR_EQ, pmsl0257RecTO.rec02fmtRec6.getLocation(),
				pmsl0257RecTO.rec02fmtRec6.getMaster0co(), pmsl0257RecTO.rec02fmtRec6.getModule(), pmsl0257RecTO.rec02fmtRec6.getSymbol(),
				pmsl0257RecTO.rec02fmtRec6.getPolicy0num()));*/
			cobolsqlca.setDBAccessStatus(pmsl0257DAO.openPmsl0257TOCsrWithoutCase(pmsl0257RecTO.rec02fmtRec6.getLocation(),
					pmsl0257RecTO.rec02fmtRec6.getMaster0co(), pmsl0257RecTO.rec02fmtRec6.getModule()));
			//CSC-Manual End
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0257();
			}
			cobolsqlca.setDBAccessStatus(pmsl0257DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
		}
		if (wsProcessControlSw3.isPolicySymbolNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0258RecTO.rec02fmtRec7.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0258RecTO.rec02fmtRec7.setLocation(rskdbio021TO.getInLocation());
			pmsl0258RecTO.rec02fmtRec7.setSymbol(rskdbio021TO.getInSymbol());
			pmsl0258RecTO.rec02fmtRec7.setPolicy0num(rskdbio021TO.getInPolicy0num());
			pmsl0258RecTO.rec02fmtRec7.setModule(Types.HIGH_STR_VAL);
			cobolsqlca.setDBAccessStatus(pmsl0258DAO.openPmsl0258TOCsr(KeyType.GREATER_OR_EQ, pmsl0258RecTO.rec02fmtRec7.getLocation(),
				pmsl0258RecTO.rec02fmtRec7.getMaster0co(), pmsl0258RecTO.rec02fmtRec7.getSymbol(), pmsl0258RecTO.rec02fmtRec7.getPolicy0num(),
				pmsl0258RecTO.rec02fmtRec7.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0258();
			}
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt1502DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0258DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
		}
		if (wsProcessControlSw3.isCustomerNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0252RecTO.rec02fmtRec3.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0252RecTO.rec02fmtRec3.setLocation(rskdbio021TO.getInLocation());
			pmsl0252RecTO.rec02fmtRec3.setCust0no(rskdbio021TO.getInCust0no());
			pmsl0252RecTO.rec02fmtRec3.setSymbol("");
			pmsl0252RecTO.rec02fmtRec3.setPolicy0num("");
			pmsl0252RecTO.rec02fmtRec3.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0252DAO.openPmsl0252TOCsr(KeyType.GREATER_OR_EQ, pmsl0252RecTO.rec02fmtRec3.getMaster0co(),
				pmsl0252RecTO.rec02fmtRec3.getLocation(), pmsl0252RecTO.rec02fmtRec3.getCust0no(), pmsl0252RecTO.rec02fmtRec3.getSymbol(),
				pmsl0252RecTO.rec02fmtRec3.getPolicy0num(), pmsl0252RecTO.rec02fmtRec3.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsl0252();
			}
			cobolsqlca.setDBAccessStatus(pmsl0252DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
		}
		if (wsProcessControlSw3.isCustomerSsn()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// MOVE IN-SSN TO SSN OF BASCLT0103-REC
			//
			// START BASCLT0103-FILE
			// KEY NOT LESS THAN EXTERNALLY-DESCRIBED-KEY
			// INVALID KEY GO TO 0000-EXIT-PROGRAM
			// END-START
			//
			// PERFORM 2000-READ-BASCLT0103 THRU
			// 2000-EXIT-BASCLT0103 UNTIL END-READ-REACHED
			if (rskdbio021TO.getInSsnCltInd() == 'Y' && rskdbio021TO.getInSsnSiteInd() == 'N') {
				processCltSsn();
			}
			if (rskdbio021TO.getInSsnCltInd() == 'N' && rskdbio021TO.getInSsnSiteInd() == 'Y') {
				processSiteSsn();
			}
			if (rskdbio021TO.getInSsnCltInd() == 'Y' && rskdbio021TO.getInSsnSiteInd() == 'Y') {
				processCltSsn();
				processSiteSsn();
			}
			cobolsqlca.setDBAccessStatus(basclt0103DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj0201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw3.isAddnlIntName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio021TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(rskdbio021TO.getInAddnlIntName());
			pmsj1202RecTO.rcdj1202Rec.setSymbol("");
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num("");
			pmsj1202RecTO.rcdj1202Rec.setModule("");
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				//CSC-Manual Start
				/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
				//CSC-Manual End
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1202();
			}
			endFileRead = ' ';
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio021TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setSymbol("");
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num("");
			pmsj1202RecTO.rcdj1202Rec.setModule("");
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(Functions.subString(ABOSystem.Lower(rskdbio021TO.getInAddnlIntName()), 1, 30));
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				//CSC-Manual Start
				/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
				//CSC-Manual End
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1202();
			}
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			endCrossRead = ' ';
			basclt1502RecTO.clt1502recRec.setLongname(rskdbio021TO.getInAddnlIntName());
			cobolsqlca.setDBAccessStatus(basclt1502DAO.openBasclt1502TOCsr(KeyType.GREATER_OR_EQ, basclt1502RecTO.clt1502recRec.getLongname()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(basclt1502DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endCrossRead == END_FILE_REACHED)) {
				readBasclt1502();
			}
			cobolsqlca.setDBAccessStatus(basclt1502DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		//
		if (wsProcessControlSw3.isAddnlIntType()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsj1201RecTO.rcdj1201Rec.setLocation(rskdbio021TO.getInLocation());
			pmsj1201RecTO.rcdj1201Rec.setUse0code(rskdbio021TO.getInAddnlIntType());
			pmsj1201RecTO.rcdj1201Rec.setSymbol("");
			pmsj1201RecTO.rcdj1201Rec.setPolicy0num("");
			pmsj1201RecTO.rcdj1201Rec.setModule("");
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.openPmsj1201TOCsr(KeyType.GREATER_OR_EQ, pmsj1201RecTO.rcdj1201Rec.getLocation(),
				pmsj1201RecTO.rcdj1201Rec.getMaster0co(), pmsj1201RecTO.rcdj1201Rec.getUse0code(), pmsj1201RecTO.rcdj1201Rec.getSymbol(),
				pmsj1201RecTO.rcdj1201Rec.getPolicy0num(), pmsj1201RecTO.rcdj1201Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				//CSC-Manual Start
				/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
				//CSC-Manual End
				cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1201();
			}
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
		}
		//
		//
		if (wsProcessControlSw3.isCustomerLastName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0102RecTO.clt0101recRec2.setClientname(rskdbio021TO.getInLastname());
			cobolsqlca.setDBAccessStatus(basclt0102DAO.openBasclt0102TOCsr(KeyType.GREATER_OR_EQ, basclt0102RecTO.clt0101recRec2.getClientname()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0102();
			}
			cobolsqlca.setDBAccessStatus(basclt0102DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isCustomerFirstName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0106RecTO.clt0101recRec3.setFirstname(rskdbio021TO.getInFirstname());
			cobolsqlca.setDBAccessStatus(basclt0106DAO.openBasclt0106TOCsr(KeyType.GREATER_OR_EQ, basclt0106RecTO.clt0101recRec3.getFirstname()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0106();
			}
			cobolsqlca.setDBAccessStatus(basclt0106DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw3.isAgentNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0253RecTO.rec02fmtRec9.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0253RecTO.rec02fmtRec9.setLocation(rskdbio021TO.getInLocation());
			pmsl0253RecTO.setRec02fmtAgencyFromString(Functions.subString(rskdbio021TO.getInAgency(), 1, 0));
			pmsl0253RecTO.rec02fmtRec9.setSymbol("");
			pmsl0253RecTO.rec02fmtRec9.setPolicy0num("");
			pmsl0253RecTO.rec02fmtRec9.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0253DAO.openPmsl0253TOCsr(KeyType.GREATER_OR_EQ, pmsl0253RecTO.rec02fmtRec9.getMaster0co(),
				pmsl0253RecTO.rec02fmtRec9.getLocation(), pmsl0253RecTO.rec02fmtRec9.getFillr1(), pmsl0253RecTO.rec02fmtRec9.getRpt0agt0nr(),
				pmsl0253RecTO.rec02fmtRec9.getFillr2(), pmsl0253RecTO.rec02fmtRec9.getSymbol(), pmsl0253RecTO.rec02fmtRec9.getPolicy0num(),
				pmsl0253RecTO.rec02fmtRec9.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				rng2000ReadPmsl0253();
			}
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			cobolsqlca.setDBAccessStatus(pmsl0253DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isLineOfBusiness()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0254RecTO.rec02fmtRec10.setMaster0co(rskdbio021TO.getInMaster0co());
			pmsl0254RecTO.rec02fmtRec10.setLocation(rskdbio021TO.getInLocation());
			pmsl0254RecTO.rec02fmtRec10.setLine0bus(rskdbio021TO.getInLine0bus());
			pmsl0254RecTO.rec02fmtRec10.setSymbol("");
			pmsl0254RecTO.rec02fmtRec10.setPolicy0num("");
			pmsl0254RecTO.rec02fmtRec10.setModule("");
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(pmsl0254DAO.openPmsl0254TOCsr(KeyType.GREATER_OR_EQ, pmsl0254RecTO.rec02fmtRec10.getMaster0co(),
				pmsl0254RecTO.rec02fmtRec10.getLocation(), pmsl0254RecTO.rec02fmtRec10.getLine0bus(), pmsl0254RecTO.rec02fmtRec10.getSymbol(),
				pmsl0254RecTO.rec02fmtRec10.getPolicy0num(), pmsl0254RecTO.rec02fmtRec10.getModule()));*/
			cobolsqlca.setDBAccessStatus(pmsl0254DAO.openPmsl0254TOCsrRecordsWithoutCase(pmsl0254RecTO.rec02fmtRec10.getMaster0co(),
					pmsl0254RecTO.rec02fmtRec10.getLocation(), pmsl0254RecTO.rec02fmtRec10.getLine0bus()));
			//CSC-Manual End
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				rng2000ReadPmsl0254();
			}
			//CSC-Manual Start
			/*cobolsqlca.setDBAccessStatus(basclt1400DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);*/
			//CSC-Manual End
			cobolsqlca.setDBAccessStatus(pmsl0254DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isPhoneNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0104RecTO.clt0101recRec4.setPhone1(rskdbio021TO.getInPhone1());
			cobolsqlca.setDBAccessStatus(basclt0104DAO.openBasclt0104TOCsr(KeyType.GREATER_OR_EQ, basclt0104RecTO.clt0101recRec4.getPhone1()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0104();
			}
			cobolsqlca.setDBAccessStatus(basclt0104DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isGroupNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0105RecTO.clt0101recRec5.setGroupno(rskdbio021TO.getInGroupno());
			cobolsqlca.setDBAccessStatus(basclt0105DAO.openBasclt0105TOCsr(KeyType.GREATER_OR_EQ, basclt0105RecTO.clt0101recRec5.getGroupno()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				rng2000ReadBasclt0105();
			}
			cobolsqlca.setDBAccessStatus(basclt0105DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0300DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isPostalCode()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0311RecTO.clt0300recRec2.setZipcode(rskdbio021TO.getInZip0post());
			cobolsqlca.setDBAccessStatus(basclt0311DAO.openBasclt0311TOCsr(KeyType.GREATER_OR_EQ, basclt0311RecTO.clt0300recRec2.getZipcode()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0311();
			}
			cobolsqlca.setDBAccessStatus(basclt0311DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isStateAddress()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0312RecTO.clt0300recRec3.setState(rskdbio021TO.getInState());
			cobolsqlca.setDBAccessStatus(basclt0312DAO.openBasclt0312TOCsr(KeyType.GREATER_OR_EQ, basclt0312RecTO.clt0300recRec3.getState()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0312();
			}
			cobolsqlca.setDBAccessStatus(basclt0312DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw3.isCityAddress()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// OPEN INPUT BASCLT1401-FILE
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			basclt0313RecTO.clt0300recRec4.setCity(rskdbio021TO.getInCity());
			cobolsqlca.setDBAccessStatus(basclt0313DAO.openBasclt0313TOCsr(KeyType.GREATER_OR_EQ, basclt0313RecTO.clt0300recRec4.getCity()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readBasclt0313();
			}
			cobolsqlca.setDBAccessStatus(basclt0313DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// CLOSE BASCLT1401-FILE
			cobolsqlca.setDBAccessStatus(basclt1402DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(basclt0100DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
	}

	public void exitProgram() {
		// close. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		//
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(bcListEntryArr8, numOfEntrysForSql);
	}

	public void exit() {
		throw new ReturnException();
	}

	public void processCltSsn() {
		basclt0103RecTO.clt0101recRec.setSsn(rskdbio021TO.getInSsn());
		cobolsqlca.setDBAccessStatus(basclt0103DAO.openBasclt0103TOCsr(KeyType.GREATER_OR_EQ, basclt0103RecTO.clt0101recRec.getSsn()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		while (!(endFileRead == END_READ_REACHED)) {
			readBasclt0103();
		}
	}

	public void processSiteSsn() {
		endFileRead = ' ';
		pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
		pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
		pmsjwc04RecTO.rcdjwc04Rec.setMasterco(rskdbio021TO.getInMaster0co());
		pmsjwc04RecTO.rcdjwc04Rec.setSymbol("");
		pmsjwc04RecTO.rcdjwc04Rec.setPolicyno("");
		pmsjwc04RecTO.rcdjwc04Rec.setModule("");
		cobolsqlca.setDBAccessStatus(pmsjwc04DAO.openPmsjwc04TOCsr(KeyType.GREATER_OR_EQ, pmsjwc04RecTO.rcdjwc04Rec.getLocation(),
			pmsjwc04RecTO.rcdjwc04Rec.getMasterco(), pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(),
			pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(), pmsjwc04RecTO.rcdjwc04Rec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		while (!(endFileRead == END_READ_REACHED)) {
			readPmsjwc04();
		}
	}

	public void readPmsjwc04() {
		pmsjwc04TO = pmsjwc04DAO.fetchNext(pmsjwc04TO);
		pmsjwc04RecTO.setData(pmsjwc04TO.getData());
		pmsjwc04RecTO.setData(pmsjwc04RecTO.getData());
		ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsjwc04TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsjwc04RecTO.getRcdjwc04AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getCust0no().compareTo(rskdbio021TO.getInCust0no()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0 && rskdbio021TO.getInMaster0co().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getMasterco()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (pmsjwc04RecTO.rcdjwc04Rec.getFein().compareTo(rskdbio021TO.getInSsn()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsjwc04RecTO.rcdjwc04Rec.getPolicyno().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		pmsj0201RecTO.pmsrecRec.setLocation(pmsjwc04RecTO.rcdjwc04Rec.getLocation());
		pmsj0201RecTO.pmsrecRec.setMaster0co(pmsjwc04RecTO.rcdjwc04Rec.getMasterco());
		pmsj0201RecTO.pmsrecRec.setSymbol(pmsjwc04RecTO.rcdjwc04Rec.getSymbol());
		pmsj0201RecTO.pmsrecRec.setPolicy0num(pmsjwc04RecTO.rcdjwc04Rec.getPolicyno());
		pmsj0201RecTO.pmsrecRec.setModule(pmsjwc04RecTO.rcdjwc04Rec.getModule());
		cobolsqlca.setDBAccessStatus(pmsj0201DAO.openPmsj0201TOCsr(KeyType.GREATER_OR_EQ, pmsj0201RecTO.pmsrecRec.getMaster0co(),
			pmsj0201RecTO.pmsrecRec.getLocation(), pmsj0201RecTO.pmsrecRec.getSymbol(), pmsj0201RecTO.pmsrecRec.getPolicy0num(),
			pmsj0201RecTO.pmsrecRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crossReadPmsj0201();
		}
	}

	public void crossReadPmsj0201() {
		pmsj0201TO = pmsj0201DAO.fetchNext(pmsj0201TO);
		pmsj0201RecTO.setData(pmsj0201TO.getData());
		ws1FileStatus = Functions.subString(pmsj0201TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj0201TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (pmsj0201RecTO.pmsrecRec.getLocation().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getLocation()) != 0
			|| pmsj0201RecTO.pmsrecRec.getMaster0co().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getMasterco()) != 0
			|| pmsj0201RecTO.pmsrecRec.getSymbol().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getSymbol()) != 0
			|| pmsj0201RecTO.pmsrecRec.getPolicy0num().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getPolicyno()) != 0
			|| pmsj0201RecTO.pmsrecRec.getModule().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getModule()) != 0) {
			endCrossRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj0201RecTO.pmsrecRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj0201RecTO.pmsrecRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsj0201RecTO.getRcdj0201AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsj0201RecTO.pmsrecRec.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(pmsj0201RecTO.pmsrecRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj0201RecTO.pmsrecRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj0201RecTO.pmsrecRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(pmsj0201RecTO.pmsrecRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsj0201RecTO.pmsrecRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj0201RecTO.pmsrecRec.getTrans0stat();
		holdBcSymbol = pmsj0201RecTO.pmsrecRec.getSymbol();
		holdBcPolicy0num = pmsj0201RecTO.pmsrecRec.getPolicy0num();
		holdBcModule = pmsj0201RecTO.pmsrecRec.getModule();
		holdBcMaster0co = pmsj0201RecTO.pmsrecRec.getMaster0co();
		holdBcLocation = pmsj0201RecTO.pmsrecRec.getLocation();
		holdBcEffdt = pmsj0201RecTO.getRcdj0201EffdtAsStr();
		holdBcExpdt = pmsj0201RecTO.getRcdj0201ExpdtAsStr();
		holdBcRisk0state = pmsj0201RecTO.pmsrecRec.getRisk0state();
		holdBcCompany0no = pmsj0201RecTO.pmsrecRec.getCompany0no();
		holdBcAgency = pmsj0201RecTO.getRcdj0201AgencyAsStr();
		holdBcLine0bus = pmsj0201RecTO.pmsrecRec.getLine0bus();
		holdBcRenewal0cd = pmsj0201RecTO.pmsrecRec.getRenewal0cd();
		holdBcIssue0code = pmsj0201RecTO.pmsrecRec.getIssue0code();
		holdBcPay0code = pmsj0201RecTO.pmsrecRec.getPay0code();
		holdBcMode0code = pmsj0201RecTO.pmsrecRec.getMode0code();
		holdBcSort0name = pmsj0201RecTO.pmsrecRec.getSort0name();
		holdBcCust0no = pmsj0201RecTO.pmsrecRec.getCust0no();
		holdBcSpec0use0a = pmsj0201RecTO.pmsrecRec.getSpec0use0a();
		holdBcSpec0use0b = pmsj0201RecTO.pmsrecRec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj0201RecTO.pmsrecRec.getCanceldate();
		}
		holdBcMrsseq = pmsj0201RecTO.pmsrecRec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj0201RecTO.pmsrecRec.getTot0ag0prm());
		holdBcType0act = pmsj0201RecTO.pmsrecRec.getType0act();
		holdBcLastname = pmsj0201RecTO.pmsrecRec.getClientname();
		holdBcFirstname = pmsj0201RecTO.pmsrecRec.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(pmsj0201RecTO.pmsrecRec.getMidname(), 1, 1), 1);
		holdBcFedtaxid = pmsj0201RecTO.pmsrecRec.getFedtaxid();
		holdBcGroupno = pmsj0201RecTO.pmsrecRec.getGroupno();
		holdBcPhone1 = pmsj0201RecTO.pmsrecRec.getPhone1();
		holdBcSsn = pmsjwc04RecTO.rcdjwc04Rec.getFein();
		holdBcNametype = pmsj0201RecTO.pmsrecRec.getNametype();
		holdBcNamestatus = pmsj0201RecTO.pmsrecRec.getNamestatus();
		holdBcAdd0line02 = Functions.subString(pmsj0201RecTO.pmsrecRec.getDba(), 1, 30);
		holdBcAddrln1 = Functions.subString(pmsj0201RecTO.pmsrecRec.getAddrln1(), 1, 30);
		holdBcCity = Functions.subString(pmsj0201RecTO.pmsrecRec.getCity(), 1, 28);
		holdBcState = pmsj0201RecTO.pmsrecRec.getState();
		holdBcZipcode = Functions.subString(pmsj0201RecTO.pmsrecRec.getZipcode(), 1, 10);
		processEntry3();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsj1201() {
		pmsj1201TO = pmsj1201DAO.fetchNext(pmsj1201TO);
		pmsj1201RecTO.setData(pmsj1201TO.getData());
		pmsj1201RecTO.setData(pmsj1201RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1201TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1201TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsj1201RecTO.rcdj1201Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsj1201RecTO.getRcdj1201AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsj1201RecTO.rcdj1201Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsj1201RecTO.rcdj1201Rec.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsj1201RecTO.rcdj1201Rec.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsj1201RecTO.rcdj1201Rec.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsj1201RecTO.rcdj1201Rec.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) != 0) {
			return;
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0
			&& pmsj1201RecTO.rcdj1201Rec.getUse0code().compareTo(rskdbio021TO.getInAddnlIntType()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsj1201RecTO.rcdj1201Rec.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsj1201RecTO.getRcdj1201AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsj1201RecTO.rcdj1201Rec.getLine0bus()) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsj1201RecTO.rcdj1201Rec.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsj1201RecTO.rcdj1201Rec.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsj1201RecTO.rcdj1201Rec.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsj1201RecTO.rcdj1201Rec.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsj1201RecTO.rcdj1201Rec.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsj1201RecTO.rcdj1201Rec.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsj1201RecTO.rcdj1201Rec.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsj1201RecTO.rcdj1201Rec.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsj1201RecTO.rcdj1201Rec.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsj1201RecTO.rcdj1201Rec.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'N' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
					ssnIndicator.setValue('N');
				} else {
					ssnIndicator.setValue('Y');
				}
				if (ssnIndicator.isSsnNotFound()) {
					pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
					pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
					pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
					pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
					pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
					pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
					// BPHX changes start - fix 157870
					/*pmsjwc04TO =
						Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
					pmsjwc04TO =
						pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
						pmsjwc04RecTO.setData(pmsjwc04TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
						ssnIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						ssnIndicator.setValue('Y');
					} else {
						ssnIndicator.setValue('N');
					}
				}
				if (ssnIndicator.isSsnNotFound()) {
					return;
				}
			}
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj1201RecTO.rcdj1201Rec.getTrans0stat();
		holdBcSymbol = pmsj1201RecTO.rcdj1201Rec.getSymbol();
		holdBcPolicy0num = pmsj1201RecTO.rcdj1201Rec.getPolicy0num();
		holdBcModule = pmsj1201RecTO.rcdj1201Rec.getModule();
		holdBcMaster0co = pmsj1201RecTO.rcdj1201Rec.getMaster0co();
		holdBcLocation = pmsj1201RecTO.rcdj1201Rec.getLocation();
		holdBcEffdt = pmsj1201RecTO.getRcdj1201EffdtAsStr();
		holdBcExpdt = pmsj1201RecTO.getRcdj1201ExpdtAsStr();
		holdBcRisk0state = pmsj1201RecTO.rcdj1201Rec.getRisk0state();
		holdBcCompany0no = pmsj1201RecTO.rcdj1201Rec.getCompany0no();
		holdBcAgency = pmsj1201RecTO.getRcdj1201AgencyAsStr();
		holdBcLine0bus = pmsj1201RecTO.rcdj1201Rec.getLine0bus();
		holdBcRenewal0cd = pmsj1201RecTO.rcdj1201Rec.getRenewal0cd();
		holdBcIssue0code = pmsj1201RecTO.rcdj1201Rec.getIssue0code();
		holdBcPay0code = pmsj1201RecTO.rcdj1201Rec.getPay0code();
		holdBcMode0code = pmsj1201RecTO.rcdj1201Rec.getMode0code();
		holdBcSort0name = pmsj1201RecTO.rcdj1201Rec.getSort0name();
		holdBcCust0no = pmsj1201RecTO.rcdj1201Rec.getCust0no();
		holdBcSpec0use0a = pmsj1201RecTO.rcdj1201Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsj1201RecTO.rcdj1201Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj1201RecTO.rcdj1201Rec.getCanceldate();
		}
		holdBcMrsseq = pmsj1201RecTO.rcdj1201Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj1201RecTO.rcdj1201Rec.getTot0ag0prm());
		holdBcType0act = pmsj1201RecTO.rcdj1201Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsj1202() {
		pmsj1202TO = pmsj1202DAO.fetchNext(pmsj1202TO);
		pmsj1202RecTO.setData(pmsj1202TO.getData());
		pmsj1202RecTO.setData(pmsj1202RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1202TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1202TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsj1202RecTO.rcdj1202Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsj1202RecTO.getRcdj1202AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsj1202RecTO.rcdj1202Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsj1202RecTO.rcdj1202Rec.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsj1202RecTO.rcdj1202Rec.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsj1202RecTO.rcdj1202Rec.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsj1202RecTO.rcdj1202Rec.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0
			&& pmsj1202RecTO.rcdj1202Rec.getUse0code().compareTo(rskdbio021TO.getInAddnlIntType()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsj1202RecTO.rcdj1202Rec.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsj1202RecTO.getRcdj1202AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsj1202RecTO.rcdj1202Rec.getLine0bus()) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsj1202RecTO.rcdj1202Rec.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsj1202RecTO.rcdj1202Rec.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsj1202RecTO.rcdj1202Rec.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsj1202RecTO.rcdj1202Rec.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsj1202RecTO.rcdj1202Rec.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsj1202RecTO.rcdj1202Rec.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsj1202RecTO.rcdj1202Rec.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsj1202RecTO.rcdj1202Rec.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsj1202RecTO.rcdj1202Rec.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsj1202RecTO.rcdj1202Rec.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'N' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
					ssnIndicator.setValue('N');
				} else {
					ssnIndicator.setValue('Y');
				}
				if (ssnIndicator.isSsnNotFound()) {
					pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
					pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
					pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
					pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
					pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
					pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
					// BPHX changes start - fix 157870
					/*pmsjwc04TO =
						Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
					pmsjwc04TO =
						pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
						pmsjwc04RecTO.setData(pmsjwc04TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
						ssnIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						ssnIndicator.setValue('Y');
					} else {
						ssnIndicator.setValue('N');
					}
				}
				if (ssnIndicator.isSsnNotFound()) {
					return;
				}
			}
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj1202RecTO.rcdj1202Rec.getTrans0stat();
		holdBcSymbol = pmsj1202RecTO.rcdj1202Rec.getSymbol();
		holdBcPolicy0num = pmsj1202RecTO.rcdj1202Rec.getPolicy0num();
		holdBcModule = pmsj1202RecTO.rcdj1202Rec.getModule();
		holdBcMaster0co = pmsj1202RecTO.rcdj1202Rec.getMaster0co();
		holdBcLocation = pmsj1202RecTO.rcdj1202Rec.getLocation();
		holdBcEffdt = pmsj1202RecTO.getRcdj1202EffdtAsStr();
		holdBcExpdt = pmsj1202RecTO.getRcdj1202ExpdtAsStr();
		holdBcRisk0state = pmsj1202RecTO.rcdj1202Rec.getRisk0state();
		holdBcCompany0no = pmsj1202RecTO.rcdj1202Rec.getCompany0no();
		holdBcAgency = pmsj1202RecTO.getRcdj1202AgencyAsStr();
		holdBcLine0bus = pmsj1202RecTO.rcdj1202Rec.getLine0bus();
		holdBcRenewal0cd = pmsj1202RecTO.rcdj1202Rec.getRenewal0cd();
		holdBcIssue0code = pmsj1202RecTO.rcdj1202Rec.getIssue0code();
		holdBcPay0code = pmsj1202RecTO.rcdj1202Rec.getPay0code();
		holdBcMode0code = pmsj1202RecTO.rcdj1202Rec.getMode0code();
		holdBcSort0name = pmsj1202RecTO.rcdj1202Rec.getSort0name();
		holdBcCust0no = pmsj1202RecTO.rcdj1202Rec.getCust0no();
		holdBcSpec0use0a = pmsj1202RecTO.rcdj1202Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsj1202RecTO.rcdj1202Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj1202RecTO.rcdj1202Rec.getCanceldate();
		}
		holdBcMrsseq = pmsj1202RecTO.rcdj1202Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj1202RecTO.rcdj1202Rec.getTot0ag0prm());
		holdBcType0act = pmsj1202RecTO.rcdj1202Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readBasclt1502() {
		basclt1502TO = basclt1502DAO.fetchNext(basclt1502TO);
		basclt1502RecTO.setData(basclt1502TO.getData());
		basclt1502RecTO.setData(basclt1502RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1502TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1502TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = END_FILE_REACHED;
			return;
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getLongname(), 1, wsAddnlIntNameLen))) != 0) {
			endCrossRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0
			&& basclt1502RecTO.clt1502recRec.getUse0code().compareTo(rskdbio021TO.getInAddnlIntType()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(basclt1502RecTO.clt1502recRec.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(basclt1502RecTO.getClt1502recAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0
			&& rskdbio021TO.getInLine0bus().compareTo(basclt1502RecTO.clt1502recRec.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1502RecTO.clt1502recRec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(basclt1502RecTO.clt1502recRec.getLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(basclt1502RecTO.clt1502recRec.getSsn(), 1, 9));
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(basclt1502RecTO.clt1502recRec.getSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(basclt1502RecTO.clt1502recRec.getPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(basclt1502RecTO.clt1502recRec.getModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'N' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt1502RecTO.clt1502recRec.getSsn()) != 0) {
					return;
				}
			}
			if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'Y') {
				if (rskdbio021TO.getInSsn().compareTo(basclt1502RecTO.clt1502recRec.getSsn()) != 0) {
					ssnIndicator.setValue('N');
				} else {
					ssnIndicator.setValue('Y');
				}
				if (ssnIndicator.isSsnNotFound()) {
					pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1502RecTO.clt1502recRec.getMaster0co());
					pmsjwc04RecTO.rcdjwc04Rec.setLocation(basclt1502RecTO.clt1502recRec.getLocation());
					pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(basclt1502RecTO.clt1502recRec.getSsn(), 1, 9));
					pmsjwc04RecTO.rcdjwc04Rec.setSymbol(basclt1502RecTO.clt1502recRec.getSymbol());
					pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(basclt1502RecTO.clt1502recRec.getPolicy0num());
					pmsjwc04RecTO.rcdjwc04Rec.setModule(basclt1502RecTO.clt1502recRec.getModule());
					// BPHX changes start - fix 157870
					/*pmsjwc04TO =
						Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
					pmsjwc04TO =
						pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
							pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
							pmsjwc04RecTO.rcdjwc04Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
						pmsjwc04RecTO.setData(pmsjwc04TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
						ssnIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						ssnIndicator.setValue('Y');
					} else {
						ssnIndicator.setValue('N');
					}
				}
				if (ssnIndicator.isSsnNotFound()) {
					return;
				}
			}
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt1502RecTO.clt1502recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt1502RecTO.clt1502recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt1502RecTO.clt1502recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = basclt1502RecTO.clt1502recRec.getTrans0stat();
		holdBcSymbol = basclt1502RecTO.clt1502recRec.getSymbol();
		holdBcPolicy0num = basclt1502RecTO.clt1502recRec.getPolicy0num();
		holdBcModule = basclt1502RecTO.clt1502recRec.getModule();
		holdBcMaster0co = basclt1502RecTO.clt1502recRec.getMaster0co();
		holdBcLocation = basclt1502RecTO.clt1502recRec.getLocation();
		holdBcEffdt = basclt1502RecTO.getClt1502recEffdtAsStr();
		holdBcExpdt = basclt1502RecTO.getClt1502recExpdtAsStr();
		holdBcRisk0state = basclt1502RecTO.clt1502recRec.getRisk0state();
		holdBcCompany0no = basclt1502RecTO.clt1502recRec.getCompany0no();
		holdBcAgency = basclt1502RecTO.getClt1502recAgencyAsStr();
		holdBcLine0bus = basclt1502RecTO.clt1502recRec.getLine0bus();
		holdBcRenewal0cd = basclt1502RecTO.clt1502recRec.getRenewal0cd();
		holdBcIssue0code = basclt1502RecTO.clt1502recRec.getIssue0code();
		holdBcPay0code = basclt1502RecTO.clt1502recRec.getPay0code();
		holdBcMode0code = basclt1502RecTO.clt1502recRec.getMode0code();
		holdBcSort0name = basclt1502RecTO.clt1502recRec.getSort0name();
		holdBcCust0no = basclt1502RecTO.clt1502recRec.getCust0no();
		holdBcSpec0use0a = basclt1502RecTO.clt1502recRec.getSpec0use0a();
		holdBcSpec0use0b = basclt1502RecTO.clt1502recRec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = basclt1502RecTO.clt1502recRec.getCanceldate();
		}
		holdBcMrsseq = basclt1502RecTO.clt1502recRec.getMrsseq();
		holdBcTot0ag0prm.assign(basclt1502RecTO.clt1502recRec.getTot0ag0prm());
		holdBcType0act = basclt1502RecTO.clt1502recRec.getType0act();
		holdBcLastname = basclt1502RecTO.clt1502recRec.getClientname();
		holdBcFirstname = basclt1502RecTO.clt1502recRec.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt1502RecTO.clt1502recRec.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt1502RecTO.clt1502recRec.getFedtaxid();
		holdBcGroupno = basclt1502RecTO.clt1502recRec.getGroupno();
		holdBcPhone1 = basclt1502RecTO.clt1502recRec.getPhone1();
		holdBcSsn = basclt1502RecTO.clt1502recRec.getSsn();
		holdBcNametype = basclt1502RecTO.clt1502recRec.getNametype();
		holdBcNamestatus = basclt1502RecTO.clt1502recRec.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt1502RecTO.clt1502recRec.getDba(), 1, 30);
		holdBcAddrln1 = Functions.subString(basclt1502RecTO.clt1502recRec.getAddrln1(), 1, 30);
		holdBcCity = Functions.subString(basclt1502RecTO.clt1502recRec.getCity(), 1, 28);
		holdBcState = basclt1502RecTO.clt1502recRec.getState();
		holdBcZipcode = Functions.subString(basclt1502RecTO.clt1502recRec.getZipcode(), 1, 10);
		processEntry3();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0259() {
		pmsl0259TO = pmsl0259DAO.fetchNext(pmsl0259TO);
		pmsl0259RecTO.setData(pmsl0259TO.getData());
		pmsl0259RecTO.setData(pmsl0259RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (pmsl0259RecTO.rec02fmtRec2.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0
			|| pmsl0259RecTO.rec02fmtRec2.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0
			|| pmsl0259RecTO.rec02fmtRec2.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0259RecTO.rec02fmtRec2.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0259RecTO.rec02fmtRec2.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0259RecTO.rec02fmtRec2.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0259RecTO.rec02fmtRec2.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
			basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0259RecTO.rec02fmtRec2.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0259RecTO.rec02fmtRec2.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsl0259RecTO.rec02fmtRec2.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0259RecTO.rec02fmtRec2.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0259RecTO.rec02fmtRec2.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		// READ X REF FILE TO GET CLIENT NAME
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		// IF IN-SSN NOT EQUAL SPACES
		// AND IN-SSN NOT EQUAL SSN OF CLT0100REC
		// GO TO 2000-EXIT-PMSL0259
		// END-IF.
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			chkSsn();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0) {
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
			wsTmpMaster0co = pmsl0259RecTO.rec02fmtRec2.getMaster0co();
			chkIntType();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") == 0 && rskdbio021TO.getInAddnlIntName().compareTo("") != 0) {
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
			wsTmpMaster0co = pmsl0259RecTO.rec02fmtRec2.getMaster0co();
			chkIntName();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0259RecTO.rec02fmtRec2.getTrans0stat();
		holdBcSymbol = pmsl0259RecTO.rec02fmtRec2.getSymbol();
		holdBcPolicy0num = pmsl0259RecTO.rec02fmtRec2.getPolicy0num();
		holdBcModule = pmsl0259RecTO.rec02fmtRec2.getModule();
		holdBcMaster0co = pmsl0259RecTO.rec02fmtRec2.getMaster0co();
		holdBcLocation = pmsl0259RecTO.rec02fmtRec2.getLocation();
		holdBcEffdt = pmsl0259RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0259RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0259RecTO.rec02fmtRec2.getRisk0state();
		holdBcCompany0no = pmsl0259RecTO.rec02fmtRec2.getCompany0no();
		holdBcAgency = pmsl0259RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0259RecTO.rec02fmtRec2.getLine0bus();
		holdBcRenewal0cd = pmsl0259RecTO.rec02fmtRec2.getRenewal0cd();
		holdBcIssue0code = pmsl0259RecTO.rec02fmtRec2.getIssue0code();
		holdBcPay0code = pmsl0259RecTO.rec02fmtRec2.getPay0code();
		holdBcMode0code = pmsl0259RecTO.rec02fmtRec2.getMode0code();
		holdBcSort0name = pmsl0259RecTO.rec02fmtRec2.getSort0name();
		holdBcCust0no = pmsl0259RecTO.rec02fmtRec2.getCust0no();
		holdBcSpec0use0a = pmsl0259RecTO.rec02fmtRec2.getSpec0use0a();
		holdBcSpec0use0b = pmsl0259RecTO.rec02fmtRec2.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0259RecTO.rec02fmtRec2.getCanceldate();
		}
		holdBcMrsseq = pmsl0259RecTO.rec02fmtRec2.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0259RecTO.rec02fmtRec2.getTot0ag0prm());
		holdBcType0act = pmsl0259RecTO.rec02fmtRec2.getType0act();
		
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0259() {
	// exit
	}

	public void chkSsn() {
		if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'N') {
			pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
			pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
			pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
			pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
			pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
			if (rskdbio021TO.getInModule().compareTo("") != 0) {
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
			} else {
				pmsjwc04RecTO.rcdjwc04Rec.setModule(basclt1400RecTO.clt1400recRec.getModule());
			}
			// BPHX changes start - fix 157870
			/*pmsjwc04TO =
				Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
					pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
					pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
			pmsjwc04TO =
				pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
					pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
					pmsjwc04RecTO.rcdjwc04Rec.getModule());
			// BPHX changes end - fix 157870
			if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
				pmsjwc04RecTO.setData(pmsjwc04TO.getData());
			}
			ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
				endParaIndic = PARA_END;
				return;
			}
			if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
				endParaIndic = PARA_END;
				return;
			}
		}
		if (rskdbio021TO.getInSsnSiteInd() == 'N' && rskdbio021TO.getInSsnCltInd() == 'Y') {
			if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
				endParaIndic = PARA_END;
				return;
			}
		}
		if (rskdbio021TO.getInSsnSiteInd() == 'Y' && rskdbio021TO.getInSsnCltInd() == 'Y') {
			if (rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
				ssnIndicator.setValue('N');
			} else {
				ssnIndicator.setValue('Y');
			}
			if (ssnIndicator.isSsnNotFound()) {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(basclt1400RecTO.clt1400recRec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio021TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(Functions.subString(rskdbio021TO.getInSsn(), 1, 9));
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio021TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio021TO.getInPolicy0num());
				if (rskdbio021TO.getInModule().compareTo("") != 0) {
					pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio021TO.getInModule());
				} else {
					pmsjwc04RecTO.rcdjwc04Rec.setModule(basclt1400RecTO.clt1400recRec.getModule());
				}
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					ssnIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					ssnIndicator.setValue('Y');
				} else {
					ssnIndicator.setValue('N');
				}
			}
			if (ssnIndicator.isSsnNotFound()) {
				endParaIndic = PARA_END;
				return;
			}
		}
	}

	public void exitChkSsn() {
	// exit
	}

	public void chkIntType() {
		addnlTypeIndicator.setValue('N');
		pmsj1201RecTO.rcdj1201Rec.setLocation(rskdbio021TO.getInLocation());
		pmsj1201RecTO.rcdj1201Rec.setUse0code(rskdbio021TO.getInAddnlIntType());
		pmsj1201RecTO.rcdj1201Rec.setSymbol(rskdbio021TO.getInSymbol());
		pmsj1201RecTO.rcdj1201Rec.setPolicy0num(rskdbio021TO.getInPolicy0num());
		if (rskdbio021TO.getInModule().compareTo("") != 0) {
			pmsj1201RecTO.rcdj1201Rec.setModule(rskdbio021TO.getInModule());
			wsTmpModule = rskdbio021TO.getInModule();
		} else {
			pmsj1201RecTO.rcdj1201Rec.setModule(basclt1400RecTO.clt1400recRec.getModule());
			wsTmpModule = basclt1400RecTO.clt1400recRec.getModule();
		}
		cobolsqlca.setDBAccessStatus(pmsj1201DAO.openPmsj1201TOCsr(KeyType.GREATER_OR_EQ, pmsj1201RecTO.rcdj1201Rec.getLocation(),
			pmsj1201RecTO.rcdj1201Rec.getMaster0co(), pmsj1201RecTO.rcdj1201Rec.getUse0code(), pmsj1201RecTO.rcdj1201Rec.getSymbol(),
			pmsj1201RecTO.rcdj1201Rec.getPolicy0num(), pmsj1201RecTO.rcdj1201Rec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			endParaIndic = PARA_END;
			return;
		}
		while (!(endCrossRead == END_FILE_REACHED || addnlNameIndicator.isAddnlNameFound())) {
			readPmsj1201Int();
		}
		if (addnlTypeIndicator.isAddnlTypeNotFound()) {
			endParaIndic = PARA_END;
			return;
		}
		if (addnlNameIndicator.isAddnlNameFound() && addnlTypeIndicator.isAddnlTypeFound()) {
			addnlIntIndicator.setValue('Y');
		} else {
			addnlIntIndicator.setValue('N');
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0 && addnlNameIndicator.isAddnlNameNotFound() && addnlIntIndicator.isAddnlIntNotFound()) {
			basclt1502RecTO.clt1502recRec.setLongname(rskdbio021TO.getInAddnlIntName());
			cobolsqlca.setDBAccessStatus(basclt1502DAO.openBasclt1502TOCsr(KeyType.GREATER_OR_EQ, basclt1502RecTO.clt1502recRec.getLongname()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				endParaIndic = PARA_END;
				return;
			}
			endCrossRead = ' ';
			while (!(endCrossRead == END_FILE_REACHED || addnlNameIndicator.isAddnlNameFound())) {
				readBasclt1502Int();
			}
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0 && addnlNameIndicator.isAddnlNameNotFound()) {
			endParaIndic = PARA_END;
			return;
		}
	}

	public void exitChkIntType() {
	// exit
	}

	public void chkIntName() {
		endCrossRead = ' ';
		pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio021TO.getInLocation());
		pmsj1202RecTO.rcdj1202Rec.setDesc0line1(rskdbio021TO.getInAddnlIntName());
		pmsj1202RecTO.rcdj1202Rec.setSymbol(rskdbio021TO.getInSymbol());
		pmsj1202RecTO.rcdj1202Rec.setPolicy0num(rskdbio021TO.getInPolicy0num());
		if (rskdbio021TO.getInModule().compareTo("") != 0) {
			pmsj1202RecTO.rcdj1202Rec.setModule(rskdbio021TO.getInModule());
			wsTmpModule = rskdbio021TO.getInModule();
		} else {
			pmsj1202RecTO.rcdj1202Rec.setModule(basclt1400RecTO.clt1400recRec.getModule());
			wsTmpModule = basclt1400RecTO.clt1400recRec.getModule();
		}
		cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
			pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
			pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			endParaIndic = PARA_END;
			return;
		}
		while (!(endCrossRead == END_FILE_REACHED || addnlNameIndicator.isAddnlNameFound())) {
			readPmsj1202Int();
		}
		if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0 && addnlNameIndicator.isAddnlNameNotFound()) {
			basclt1502RecTO.clt1502recRec.setLongname(rskdbio021TO.getInAddnlIntName());
			cobolsqlca.setDBAccessStatus(basclt1502DAO.openBasclt1502TOCsr(KeyType.GREATER_OR_EQ, basclt1502RecTO.clt1502recRec.getLongname()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				endParaIndic = PARA_END;
				return;
			}
			endCrossRead = ' ';
			while (!(endCrossRead == END_FILE_REACHED || addnlNameIndicator.isAddnlNameFound())) {
				readBasclt1502Int();
			}
		}
		if (addnlNameIndicator.isAddnlNameNotFound()) {
			endParaIndic = PARA_END;
			return;
		}
	}

	public void exitChkIntName() {
	// exit
	}

	public void readPmsj1201Int() {
		pmsj1201TO = pmsj1201DAO.fetchNext(pmsj1201TO);
		pmsj1201RecTO.setData(pmsj1201TO.getData());
		pmsj1201RecTO.setData(pmsj1201RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1201TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1201TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			addnlNameIndicator.setValue('N');
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (wsTmpMaster0co.compareTo(pmsj1201RecTO.rcdj1201Rec.getMaster0co()) == 0
			&& rskdbio021TO.getInLocation().compareTo(pmsj1201RecTO.rcdj1201Rec.getLocation()) == 0
			&& rskdbio021TO.getInAddnlIntType().compareTo(pmsj1201RecTO.rcdj1201Rec.getUse0code()) == 0
			&& rskdbio021TO.getInSymbol().compareTo(pmsj1201RecTO.rcdj1201Rec.getSymbol()) == 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsj1201RecTO.rcdj1201Rec.getPolicy0num()) == 0
			&& wsTmpModule.compareTo(pmsj1201RecTO.rcdj1201Rec.getModule()) == 0) {
			addnlTypeIndicator.setValue('Y');
			if (rskdbio021TO.getInAddnlIntName().compareTo("") != 0
				&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
					ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) == 0) {
				addnlNameIndicator.setValue('Y');
			} else {
				addnlNameIndicator.setValue('N');
			}
		}
	}

	public void exitReadPmsj1201Int() {
	// exit
	}

	public void readPmsj1202Int() {
		pmsj1202TO = pmsj1202DAO.fetchNext(pmsj1202TO);
		pmsj1202RecTO.setData(pmsj1202TO.getData());
		pmsj1202RecTO.setData(pmsj1202RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1202TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1202TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			addnlNameIndicator.setValue('N');
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (wsTmpMaster0co.compareTo(pmsj1202RecTO.rcdj1202Rec.getMaster0co()) == 0
			&& rskdbio021TO.getInLocation().compareTo(pmsj1202RecTO.rcdj1202Rec.getLocation()) == 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) == 0
			&& rskdbio021TO.getInSymbol().compareTo(pmsj1202RecTO.rcdj1202Rec.getSymbol()) == 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsj1202RecTO.rcdj1202Rec.getPolicy0num()) == 0
			&& wsTmpModule.compareTo(pmsj1202RecTO.rcdj1202Rec.getModule()) == 0) {
			addnlNameIndicator.setValue('Y');
		}
	}

	public void exitReadPmsj1202Int() {
	// exit
	}

	public void readBasclt1502Int() {
		basclt1502TO = basclt1502DAO.fetchNext(basclt1502TO);
		basclt1502RecTO.setData(basclt1502TO.getData());
		basclt1502RecTO.setData(basclt1502RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1502TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1502TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			addnlNameIndicator.setValue('N');
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (wsTmpMaster0co.compareTo(basclt1502RecTO.clt1502recRec.getMaster0co()) == 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1502RecTO.clt1502recRec.getLocation()) == 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt1502RecTO.clt1502recRec.getLongname(), 1, wsAddnlIntNameLen))) == 0
			&& rskdbio021TO.getInSymbol().compareTo(basclt1502RecTO.clt1502recRec.getSymbol()) == 0
			&& rskdbio021TO.getInPolicy0num().compareTo(basclt1502RecTO.clt1502recRec.getPolicy0num()) == 0
			&& wsTmpModule.compareTo(basclt1502RecTO.clt1502recRec.getModule()) == 0) {
			if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0) {
				if (rskdbio021TO.getInAddnlIntType().compareTo(basclt1502RecTO.clt1502recRec.getUse0code()) == 0) {
					addnlNameIndicator.setValue('Y');
				}
			} else {
				addnlNameIndicator.setValue('Y');
			}
		}
	}

	public void exitReadBasclt1502Int() {
	// exit
	}

	public void readPmsl0255() {
		pmsl0255TO = pmsl0255DAO.fetchNext(pmsl0255TO);
		pmsl0255RecTO.setData(pmsl0255TO.getData());
		pmsl0255RecTO.setData(pmsl0255RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0255TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0255TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0255RecTO.rec02fmtRec4.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0255RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0255RecTO.rec02fmtRec4.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0255RecTO.rec02fmtRec4.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0255RecTO.rec02fmtRec4.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0255RecTO.rec02fmtRec4.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0255RecTO.rec02fmtRec4.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0255RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0255RecTO.rec02fmtRec4.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0255RecTO.rec02fmtRec4.getCust0no())) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0255RecTO.rec02fmtRec4.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0255RecTO.rec02fmtRec4.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0255RecTO.rec02fmtRec4.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0255RecTO.rec02fmtRec4.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0255RecTO.rec02fmtRec4.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0255RecTO.rec02fmtRec4.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0255RecTO.rec02fmtRec4.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsl0255RecTO.rec02fmtRec4.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0255RecTO.rec02fmtRec4.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0255RecTO.rec02fmtRec4.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0255RecTO.rec02fmtRec4.getTrans0stat();
		holdBcSymbol = pmsl0255RecTO.rec02fmtRec4.getSymbol();
		holdBcPolicy0num = pmsl0255RecTO.rec02fmtRec4.getPolicy0num();
		holdBcModule = pmsl0255RecTO.rec02fmtRec4.getModule();
		holdBcMaster0co = pmsl0255RecTO.rec02fmtRec4.getMaster0co();
		holdBcLocation = pmsl0255RecTO.rec02fmtRec4.getLocation();
		holdBcEffdt = pmsl0255RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0255RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0255RecTO.rec02fmtRec4.getRisk0state();
		holdBcCompany0no = pmsl0255RecTO.rec02fmtRec4.getCompany0no();
		holdBcAgency = pmsl0255RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0255RecTO.rec02fmtRec4.getLine0bus();
		holdBcRenewal0cd = pmsl0255RecTO.rec02fmtRec4.getRenewal0cd();
		holdBcIssue0code = pmsl0255RecTO.rec02fmtRec4.getIssue0code();
		holdBcPay0code = pmsl0255RecTO.rec02fmtRec4.getPay0code();
		holdBcMode0code = pmsl0255RecTO.rec02fmtRec4.getMode0code();
		holdBcSort0name = pmsl0255RecTO.rec02fmtRec4.getSort0name();
		holdBcCust0no = pmsl0255RecTO.rec02fmtRec4.getCust0no();
		holdBcSpec0use0a = pmsl0255RecTO.rec02fmtRec4.getSpec0use0a();
		holdBcSpec0use0b = pmsl0255RecTO.rec02fmtRec4.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0255RecTO.rec02fmtRec4.getCanceldate();
		}
		holdBcMrsseq = pmsl0255RecTO.rec02fmtRec4.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0255RecTO.rec02fmtRec4.getTot0ag0prm());
		holdBcType0act = pmsl0255RecTO.rec02fmtRec4.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0255RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0255RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0255() {
	// exit
	}

	public void readPmsl0256() {
		pmsl0256TO = pmsl0256DAO.fetchNext(pmsl0256TO);
		pmsl0256RecTO.setData(pmsl0256TO.getData());
		pmsl0256RecTO.setData(pmsl0256RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0256TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0256TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0256RecTO.rec02fmtRec5.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0256RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0256RecTO.rec02fmtRec5.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0256RecTO.rec02fmtRec5.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0256RecTO.rec02fmtRec5.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0256RecTO.rec02fmtRec5.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0256RecTO.rec02fmtRec5.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0256RecTO.rec02fmtRec5.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0256RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0256RecTO.rec02fmtRec5.getCust0no())) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0256RecTO.rec02fmtRec5.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0256RecTO.rec02fmtRec5.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0256RecTO.rec02fmtRec5.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0256RecTO.rec02fmtRec5.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0256RecTO.rec02fmtRec5.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0256RecTO.rec02fmtRec5.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0
			|| pmsl0256RecTO.rec02fmtRec5.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0256RecTO.rec02fmtRec5.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0256RecTO.rec02fmtRec5.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0256RecTO.rec02fmtRec5.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0256RecTO.rec02fmtRec5.getTrans0stat();
		holdBcSymbol = pmsl0256RecTO.rec02fmtRec5.getSymbol();
		holdBcPolicy0num = pmsl0256RecTO.rec02fmtRec5.getPolicy0num();
		holdBcModule = pmsl0256RecTO.rec02fmtRec5.getModule();
		holdBcMaster0co = pmsl0256RecTO.rec02fmtRec5.getMaster0co();
		holdBcLocation = pmsl0256RecTO.rec02fmtRec5.getLocation();
		holdBcEffdt = pmsl0256RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0256RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0256RecTO.rec02fmtRec5.getRisk0state();
		holdBcCompany0no = pmsl0256RecTO.rec02fmtRec5.getCompany0no();
		holdBcAgency = pmsl0256RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0256RecTO.rec02fmtRec5.getLine0bus();
		holdBcRenewal0cd = pmsl0256RecTO.rec02fmtRec5.getRenewal0cd();
		holdBcIssue0code = pmsl0256RecTO.rec02fmtRec5.getIssue0code();
		holdBcPay0code = pmsl0256RecTO.rec02fmtRec5.getPay0code();
		holdBcMode0code = pmsl0256RecTO.rec02fmtRec5.getMode0code();
		holdBcSort0name = pmsl0256RecTO.rec02fmtRec5.getSort0name();
		holdBcCust0no = pmsl0256RecTO.rec02fmtRec5.getCust0no();
		holdBcSpec0use0a = pmsl0256RecTO.rec02fmtRec5.getSpec0use0a();
		holdBcSpec0use0b = pmsl0256RecTO.rec02fmtRec5.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0256RecTO.rec02fmtRec5.getCanceldate();
		}
		holdBcMrsseq = pmsl0256RecTO.rec02fmtRec5.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0256RecTO.rec02fmtRec5.getTot0ag0prm());
		holdBcType0act = pmsl0256RecTO.rec02fmtRec5.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0256RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0256RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0256() {
	// exit
	}

	public void readPmsl0257() {
		pmsl0257TO = pmsl0257DAO.fetchNext(pmsl0257TO);
		pmsl0257RecTO.setData(pmsl0257TO.getData());
		pmsl0257RecTO.setData(pmsl0257RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0257TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0257TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0257RecTO.rec02fmtRec6.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0257RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0257RecTO.rec02fmtRec6.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0257RecTO.rec02fmtRec6.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0257RecTO.rec02fmtRec6.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0257RecTO.rec02fmtRec6.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0257RecTO.rec02fmtRec6.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0257RecTO.rec02fmtRec6.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0257RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0257RecTO.rec02fmtRec6.getCust0no())) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0257RecTO.rec02fmtRec6.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0257RecTO.rec02fmtRec6.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0257RecTO.rec02fmtRec6.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0257RecTO.rec02fmtRec6.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0257RecTO.rec02fmtRec6.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0257RecTO.rec02fmtRec6.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0
			|| pmsl0257RecTO.rec02fmtRec6.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0257RecTO.rec02fmtRec6.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0257RecTO.rec02fmtRec6.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0257RecTO.rec02fmtRec6.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0257RecTO.rec02fmtRec6.getTrans0stat();
		holdBcSymbol = pmsl0257RecTO.rec02fmtRec6.getSymbol();
		holdBcPolicy0num = pmsl0257RecTO.rec02fmtRec6.getPolicy0num();
		holdBcModule = pmsl0257RecTO.rec02fmtRec6.getModule();
		holdBcMaster0co = pmsl0257RecTO.rec02fmtRec6.getMaster0co();
		holdBcLocation = pmsl0257RecTO.rec02fmtRec6.getLocation();
		holdBcEffdt = pmsl0257RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0257RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0257RecTO.rec02fmtRec6.getRisk0state();
		holdBcCompany0no = pmsl0257RecTO.rec02fmtRec6.getCompany0no();
		holdBcAgency = pmsl0257RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0257RecTO.rec02fmtRec6.getLine0bus();
		holdBcRenewal0cd = pmsl0257RecTO.rec02fmtRec6.getRenewal0cd();
		holdBcIssue0code = pmsl0257RecTO.rec02fmtRec6.getIssue0code();
		holdBcPay0code = pmsl0257RecTO.rec02fmtRec6.getPay0code();
		holdBcMode0code = pmsl0257RecTO.rec02fmtRec6.getMode0code();
		holdBcSort0name = pmsl0257RecTO.rec02fmtRec6.getSort0name();
		holdBcCust0no = pmsl0257RecTO.rec02fmtRec6.getCust0no();
		holdBcSpec0use0a = pmsl0257RecTO.rec02fmtRec6.getSpec0use0a();
		holdBcSpec0use0b = pmsl0257RecTO.rec02fmtRec6.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0257RecTO.rec02fmtRec6.getCanceldate();
		}
		holdBcMrsseq = pmsl0257RecTO.rec02fmtRec6.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0257RecTO.rec02fmtRec6.getTot0ag0prm());
		holdBcType0act = pmsl0257RecTO.rec02fmtRec6.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0257RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0257RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0257() {
	// exit
	}

	public void readPmsl0258() {
		pmsl0258TO = pmsl0258DAO.fetchNext(pmsl0258TO);
		pmsl0258RecTO.setData(pmsl0258TO.getData());
		pmsl0258RecTO.setData(pmsl0258RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0258TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0258TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0258RecTO.rec02fmtRec7.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0258RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0258RecTO.rec02fmtRec7.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0
			|| pmsl0258RecTO.rec02fmtRec7.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0258RecTO.rec02fmtRec7.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0258RecTO.rec02fmtRec7.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0258RecTO.rec02fmtRec7.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0258RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0258RecTO.rec02fmtRec7.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0258RecTO.rec02fmtRec7.getCust0no())) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0258RecTO.rec02fmtRec7.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0258RecTO.rec02fmtRec7.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0258RecTO.rec02fmtRec7.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0258RecTO.rec02fmtRec7.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0258RecTO.rec02fmtRec7.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0258RecTO.rec02fmtRec7.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0258RecTO.rec02fmtRec7.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsl0258RecTO.rec02fmtRec7.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0258RecTO.rec02fmtRec7.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0258RecTO.rec02fmtRec7.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		// IF IN-SSN NOT EQUAL SPACES
		// AND IN-SSN NOT EQUAL SSN OF CLT0100REC
		// GO TO 2000-EXIT-PMSL0258
		// END-IF.
		if (rskdbio021TO.getInSsn().compareTo("") != 0) {
			chkSsn();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") != 0) {
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(pmsl0258RecTO.rec02fmtRec7.getMaster0co());
			wsTmpMaster0co = pmsl0258RecTO.rec02fmtRec7.getMaster0co();
			chkIntType();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		if (rskdbio021TO.getInAddnlIntType().compareTo("") == 0 && rskdbio021TO.getInAddnlIntName().compareTo("") != 0) {
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0258RecTO.rec02fmtRec7.getMaster0co());
			wsTmpMaster0co = pmsl0258RecTO.rec02fmtRec7.getMaster0co();
			chkIntName();
			if (endParaIndic == PARA_END) {
				return;
			}
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0258RecTO.rec02fmtRec7.getTrans0stat();
		holdBcSymbol = pmsl0258RecTO.rec02fmtRec7.getSymbol();
		holdBcPolicy0num = pmsl0258RecTO.rec02fmtRec7.getPolicy0num();
		holdBcModule = pmsl0258RecTO.rec02fmtRec7.getModule();
		holdBcMaster0co = pmsl0258RecTO.rec02fmtRec7.getMaster0co();
		holdBcLocation = pmsl0258RecTO.rec02fmtRec7.getLocation();
		holdBcEffdt = pmsl0258RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0258RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0258RecTO.rec02fmtRec7.getRisk0state();
		holdBcCompany0no = pmsl0258RecTO.rec02fmtRec7.getCompany0no();
		holdBcAgency = pmsl0258RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0258RecTO.rec02fmtRec7.getLine0bus();
		holdBcRenewal0cd = pmsl0258RecTO.rec02fmtRec7.getRenewal0cd();
		holdBcIssue0code = pmsl0258RecTO.rec02fmtRec7.getIssue0code();
		holdBcPay0code = pmsl0258RecTO.rec02fmtRec7.getPay0code();
		holdBcMode0code = pmsl0258RecTO.rec02fmtRec7.getMode0code();
		holdBcSort0name = pmsl0258RecTO.rec02fmtRec7.getSort0name();
		holdBcCust0no = pmsl0258RecTO.rec02fmtRec7.getCust0no();
		holdBcSpec0use0a = pmsl0258RecTO.rec02fmtRec7.getSpec0use0a();
		holdBcSpec0use0b = pmsl0258RecTO.rec02fmtRec7.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0258RecTO.rec02fmtRec7.getCanceldate();
		}
		holdBcMrsseq = pmsl0258RecTO.rec02fmtRec7.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0258RecTO.rec02fmtRec7.getTot0ag0prm());
		holdBcType0act = pmsl0258RecTO.rec02fmtRec7.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0258RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0258RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0258() {
	// exit
	}

	public void readPmsl0252() {
		pmsl0252TO = pmsl0252DAO.fetchNext(pmsl0252TO);
		pmsl0252RecTO.setData(pmsl0252TO.getData());
		pmsl0252RecTO.setData(pmsl0252RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0252TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0252TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0252RecTO.rec02fmtRec3.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0252RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0252RecTO.rec02fmtRec3.getCust0no().compareTo(rskdbio021TO.getInCust0no()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsl0252RecTO.rec02fmtRec3.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0252RecTO.rec02fmtRec3.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0252RecTO.rec02fmtRec3.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0252RecTO.rec02fmtRec3.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0252RecTO.rec02fmtRec3.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0252RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0252RecTO.rec02fmtRec3.getLine0bus()) != 0) {
			return;
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0252RecTO.rec02fmtRec3.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0252RecTO.rec02fmtRec3.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0252RecTO.rec02fmtRec3.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0252RecTO.rec02fmtRec3.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0252RecTO.rec02fmtRec3.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmsl0252RecTO.rec02fmtRec3.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return;
		}
		if (pmsl0252RecTO.rec02fmtRec3.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return;
		}
		if (pmsl0252RecTO.rec02fmtRec3.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0252RecTO.rec02fmtRec3.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0252RecTO.rec02fmtRec3.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return;
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0252RecTO.rec02fmtRec3.getTrans0stat();
		holdBcSymbol = pmsl0252RecTO.rec02fmtRec3.getSymbol();
		holdBcPolicy0num = pmsl0252RecTO.rec02fmtRec3.getPolicy0num();
		holdBcModule = pmsl0252RecTO.rec02fmtRec3.getModule();
		holdBcMaster0co = pmsl0252RecTO.rec02fmtRec3.getMaster0co();
		holdBcLocation = pmsl0252RecTO.rec02fmtRec3.getLocation();
		holdBcEffdt = pmsl0252RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0252RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0252RecTO.rec02fmtRec3.getRisk0state();
		holdBcCompany0no = pmsl0252RecTO.rec02fmtRec3.getCompany0no();
		holdBcAgency = pmsl0252RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0252RecTO.rec02fmtRec3.getLine0bus();
		holdBcRenewal0cd = pmsl0252RecTO.rec02fmtRec3.getRenewal0cd();
		holdBcIssue0code = pmsl0252RecTO.rec02fmtRec3.getIssue0code();
		holdBcPay0code = pmsl0252RecTO.rec02fmtRec3.getPay0code();
		holdBcMode0code = pmsl0252RecTO.rec02fmtRec3.getMode0code();
		holdBcSort0name = pmsl0252RecTO.rec02fmtRec3.getSort0name();
		holdBcCust0no = pmsl0252RecTO.rec02fmtRec3.getCust0no();
		holdBcSpec0use0a = pmsl0252RecTO.rec02fmtRec3.getSpec0use0a();
		holdBcSpec0use0b = pmsl0252RecTO.rec02fmtRec3.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0252RecTO.rec02fmtRec3.getCanceldate();
		}
		holdBcMrsseq = pmsl0252RecTO.rec02fmtRec3.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0252RecTO.rec02fmtRec3.getTot0ag0prm());
		holdBcType0act = pmsl0252RecTO.rec02fmtRec3.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0252RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0252RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void exitPmsl0252() {
	// exit
	}

	public void readBasclt0103() {
		basclt0103TO = basclt0103DAO.fetchNext(basclt0103TO);
		basclt0103RecTO.setData(basclt0103TO.getData());
		basclt0103RecTO.setData(basclt0103RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0103TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0103TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (basclt0103RecTO.clt0101recRec.getSsn().compareTo(rskdbio021TO.getInSsn()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0103RecTO.clt0101recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0103RecTO.clt0101recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0103RecTO.clt0101recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0103RecTO.clt0101recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0103-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ZERO TO ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0103RecTO.clt0101recRec.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(((short) (0 % 100)));
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100SsnStart();
		}
	}

	public void exitBasclt0103() {
	// exit
	}

	public void crefer0100SsnStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0103RecTO.clt0101recRec.getCltseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IF IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC OR
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC
		// GO TO 3000-CREFER-0100-SSN-EXIT
		// END-IF.
		if (rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0259RecTO.rec02fmtRec2.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF BASCLT0300-REC.
		// MOVE ADDRSEQNUM OF CLT1401REC TO ADDRSEQNUM OF
		// BASCLT0300-REC.
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1402RecTO.clt1402recRec.getAddrseqnum());
		basclt0300TO =
			Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		if (basclt0300TO.getDBAccessStatus().isSuccess()) {
			basclt0300RecTO.setData(basclt0300TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		// IF CLTSEQNUM OF CLT1401REC NOT EQUAL CLTSEQNUM OF
		// BASCLT0300-REC OR
		// ADDRSEQNUM OF CLT1401REC NOT EQUAL ADDRSEQNUM OF
		// BASCLT0300-REC
		// GO TO 3000-CREFER-0100-SSN-EXIT
		// END-IF.
		if (basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1402RecTO.clt1402recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		//
		holdBcLastname = basclt0103RecTO.clt0101recRec.getClientname();
		holdBcFirstname = basclt0103RecTO.clt0101recRec.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt0103RecTO.clt0101recRec.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt0103RecTO.clt0101recRec.getFedtaxid();
		holdBcGroupno = basclt0103RecTO.clt0101recRec.getGroupno();
		holdBcPhone1 = basclt0103RecTO.clt0101recRec.getPhone1();
		holdBcSsn = basclt0103RecTO.clt0101recRec.getSsn();
		holdBcNametype = basclt0103RecTO.clt0101recRec.getNametype();
		holdBcNamestatus = basclt0103RecTO.clt0101recRec.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt0103RecTO.clt0101recRec.getDba(), 1, 30);
		//
		//
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry1();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void crefer0100SsnExit() {
	// exit
	}

	public void readBasclt0102() {
		basclt0102TO = basclt0102DAO.fetchNext(basclt0102TO);
		basclt0102RecTO.setData(basclt0102TO.getData());
		basclt0102RecTO.setData(basclt0102RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0102TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0102TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0102RecTO.clt0101recRec2.getClientname(), 1, wsLnLength))) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0102RecTO.clt0101recRec2.getSsn()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0102RecTO.clt0101recRec2.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0102RecTO.clt0101recRec2.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0102RecTO.clt0101recRec2.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0102-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ZERO TO ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0102RecTO.clt0101recRec2.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(((short) (0 % 100)));
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100LnameStart();
		}
	}

	public void exitBasclt0102() {
	// exit
	}

	public void crefer0100LnameStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0102RecTO.clt0101recRec2.getCltseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IF IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC OR
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC
		// GO TO 3000-CREFER-0100-LNAME-EXIT
		// END-IF.
		if (rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0259RecTO.rec02fmtRec2.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF BASCLT0300-REC.
		// MOVE ADDRSEQNUM OF CLT1401REC TO ADDRSEQNUM OF
		// BASCLT0300-REC.
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1402RecTO.clt1402recRec.getAddrseqnum());
		basclt0300TO =
			Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		if (basclt0300TO.getDBAccessStatus().isSuccess()) {
			basclt0300RecTO.setData(basclt0300TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		holdBcLastname = basclt0102RecTO.clt0101recRec2.getClientname();
		holdBcFirstname = basclt0102RecTO.clt0101recRec2.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt0102RecTO.clt0101recRec2.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt0102RecTO.clt0101recRec2.getFedtaxid();
		holdBcGroupno = basclt0102RecTO.clt0101recRec2.getGroupno();
		holdBcPhone1 = basclt0102RecTO.clt0101recRec2.getPhone1();
		holdBcSsn = basclt0102RecTO.clt0101recRec2.getSsn();
		holdBcNametype = basclt0102RecTO.clt0101recRec2.getNametype();
		holdBcNamestatus = basclt0102RecTO.clt0101recRec2.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt0102RecTO.clt0101recRec2.getDba(), 1, 30);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry1();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void crefer0100LnameExit() {
	// exit
	}

	public void readBasclt0106() {
		basclt0106TO = basclt0106DAO.fetchNext(basclt0106TO);
		basclt0106RecTO.setData(basclt0106TO.getData());
		basclt0106RecTO.setData(basclt0106RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0106TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0106TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0106RecTO.clt0101recRec3.getFirstname(), 1, wsFnLength))) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0106RecTO.clt0101recRec3.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0106RecTO.clt0101recRec3.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0106-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ZERO TO ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0106RecTO.clt0101recRec3.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(((short) (0 % 100)));
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100FnameStart();
		}
	}

	public void exitBasclt0106() {
	// exit
	}

	public void crefer0100FnameStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0106RecTO.clt0101recRec3.getCltseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IF IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC OR
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC
		// GO TO 3000-CREFER-0100-FNAME-EXIT
		// END-IF.
		if (rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF BASCLT0300-REC.
		// MOVE ADDRSEQNUM OF CLT1401REC TO ADDRSEQNUM OF
		// BASCLT0300-REC.
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1402RecTO.clt1402recRec.getAddrseqnum());
		basclt0300TO =
			Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		if (basclt0300TO.getDBAccessStatus().isSuccess()) {
			basclt0300RecTO.setData(basclt0300TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		holdBcLastname = basclt0106RecTO.clt0101recRec3.getClientname();
		holdBcFirstname = basclt0106RecTO.clt0101recRec3.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt0106RecTO.clt0101recRec3.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt0106RecTO.clt0101recRec3.getFedtaxid();
		holdBcGroupno = basclt0106RecTO.clt0101recRec3.getGroupno();
		holdBcPhone1 = basclt0106RecTO.clt0101recRec3.getPhone1();
		holdBcSsn = basclt0106RecTO.clt0101recRec3.getSsn();
		holdBcNametype = basclt0106RecTO.clt0101recRec3.getNametype();
		holdBcNamestatus = basclt0106RecTO.clt0101recRec3.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt0106RecTO.clt0101recRec3.getDba(), 1, 30);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry1();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void crefer0100FnameExit() {
	// exit
	}

	public String readPmsl0253() {
		pmsl0253TO = pmsl0253DAO.fetchNext(pmsl0253TO);
		pmsl0253RecTO.setData(pmsl0253TO.getData());
		pmsl0253RecTO.setData(pmsl0253RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0253TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0253TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0253RecTO.rec02fmtRec9.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0253RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0253RecTO.rec02fmtRec9.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0253RecTO.rec02fmtRec9.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0253RecTO.getRec02fmtAgencyAsStr()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			} else {
				return "2000-READ-PMSL0253";
			}
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0253RecTO.rec02fmtRec9.getLine0bus()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0253RecTO.rec02fmtRec9.getCust0no())) != 0) {
			return "";
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsl0253RecTO.rec02fmtRec9.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0253RecTO.rec02fmtRec9.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0253RecTO.rec02fmtRec9.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return "";
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0253RecTO.rec02fmtRec9.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0253RecTO.rec02fmtRec9.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0253RecTO.rec02fmtRec9.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0253RecTO.rec02fmtRec9.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0253RecTO.rec02fmtRec9.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		// READ X REF FILE
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (pmsl0253RecTO.rec02fmtRec9.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return "";
		}
		if (pmsl0253RecTO.rec02fmtRec9.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return "";
		}
		if (pmsl0253RecTO.rec02fmtRec9.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0253RecTO.rec02fmtRec9.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0253RecTO.rec02fmtRec9.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return "";
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		// READ X REF FILE TO GET CLIENT NAME
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0100RecTO.clt0100recRec.getCltseqnum()) {
			return "";
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return "";
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return "";
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return "";
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0253RecTO.rec02fmtRec9.getTrans0stat();
		holdBcSymbol = pmsl0253RecTO.rec02fmtRec9.getSymbol();
		holdBcPolicy0num = pmsl0253RecTO.rec02fmtRec9.getPolicy0num();
		holdBcModule = pmsl0253RecTO.rec02fmtRec9.getModule();
		holdBcMaster0co = pmsl0253RecTO.rec02fmtRec9.getMaster0co();
		holdBcLocation = pmsl0253RecTO.rec02fmtRec9.getLocation();
		holdBcEffdt = pmsl0253RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0253RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0253RecTO.rec02fmtRec9.getRisk0state();
		holdBcCompany0no = pmsl0253RecTO.rec02fmtRec9.getCompany0no();
		holdBcAgency = pmsl0253RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0253RecTO.rec02fmtRec9.getLine0bus();
		holdBcRenewal0cd = pmsl0253RecTO.rec02fmtRec9.getRenewal0cd();
		holdBcIssue0code = pmsl0253RecTO.rec02fmtRec9.getIssue0code();
		holdBcPay0code = pmsl0253RecTO.rec02fmtRec9.getPay0code();
		holdBcMode0code = pmsl0253RecTO.rec02fmtRec9.getMode0code();
		holdBcSort0name = pmsl0253RecTO.rec02fmtRec9.getSort0name();
		holdBcCust0no = pmsl0253RecTO.rec02fmtRec9.getCust0no();
		holdBcSpec0use0a = pmsl0253RecTO.rec02fmtRec9.getSpec0use0a();
		holdBcSpec0use0b = pmsl0253RecTO.rec02fmtRec9.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0253RecTO.rec02fmtRec9.getCanceldate();
		}
		holdBcMrsseq = pmsl0253RecTO.rec02fmtRec9.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0253RecTO.rec02fmtRec9.getTot0ag0prm());
		holdBcType0act = pmsl0253RecTO.rec02fmtRec9.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0253RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0253RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0253() {
	// exit
	}

	public String readPmsl0254() {
		pmsl0254TO = pmsl0254DAO.fetchNext(pmsl0254TO);
		pmsl0254RecTO.setData(pmsl0254TO.getData());
		pmsl0254RecTO.setData(pmsl0254RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0254TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0254TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0254RecTO.rec02fmtRec10.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0254RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0254RecTO.rec02fmtRec10.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0254RecTO.rec02fmtRec10.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0254RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0254RecTO.rec02fmtRec10.getLine0bus()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			} else {
				return "2000-READ-PMSL0254";
			}
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0254RecTO.rec02fmtRec10.getCust0no())) != 0) {
			return "";
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && pmsl0254RecTO.rec02fmtRec10.getSymbol().compareTo(rskdbio021TO.getInSymbol()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0254RecTO.rec02fmtRec10.getPolicy0num().compareTo(rskdbio021TO.getInPolicy0num()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0254RecTO.rec02fmtRec10.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return "";
		}
		basclt1400RecTO.clt1400recRec.setMaster0co(pmsl0254RecTO.rec02fmtRec10.getMaster0co());
		basclt1400RecTO.clt1400recRec.setLocation(pmsl0254RecTO.rec02fmtRec10.getLocation());
		basclt1400RecTO.clt1400recRec.setSymbol(pmsl0254RecTO.rec02fmtRec10.getSymbol());
		basclt1400RecTO.clt1400recRec.setPolicy0num(pmsl0254RecTO.rec02fmtRec10.getPolicy0num());
		basclt1400RecTO.clt1400recRec.setModule(pmsl0254RecTO.rec02fmtRec10.getModule());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt1400DAO.openBasclt1400TOCsr(KeyType.GREATER_OR_EQ, basclt1400RecTO.clt1400recRec.getLocation(),
			basclt1400RecTO.clt1400recRec.getMaster0co(), basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(),
			basclt1400RecTO.clt1400recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return "";
		}
		basclt1400TO = basclt1400DAO.fetchNext(basclt1400TO);*/
		basclt1400TO = Basclt1400DAO.selectById(basclt1400TO, basclt1400RecTO.clt1400recRec.getLocation(), basclt1400RecTO.clt1400recRec.getMaster0co(),
				basclt1400RecTO.clt1400recRec.getSymbol(), basclt1400RecTO.clt1400recRec.getPolicy0num(), basclt1400RecTO.clt1400recRec.getModule());
		//CSC-Manual End
		basclt1400RecTO.setData(basclt1400TO.getData());
		basclt1400RecTO.clt1400recRec.setClt1400rec(basclt1400RecTO.getData());
		ws1FileStatus = Functions.subString(basclt1400TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1400TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (pmsl0254RecTO.rec02fmtRec10.getMaster0co().compareTo(basclt1400RecTO.clt1400recRec.getMaster0co()) != 0) {
			return "";
		}
		if (pmsl0254RecTO.rec02fmtRec10.getLocation().compareTo(basclt1400RecTO.clt1400recRec.getLocation()) != 0) {
			return "";
		}
		if (pmsl0254RecTO.rec02fmtRec10.getSymbol().compareTo(basclt1400RecTO.clt1400recRec.getSymbol()) != 0
			|| pmsl0254RecTO.rec02fmtRec10.getPolicy0num().compareTo(basclt1400RecTO.clt1400recRec.getPolicy0num()) != 0
			|| pmsl0254RecTO.rec02fmtRec10.getModule().compareTo(basclt1400RecTO.clt1400recRec.getModule()) != 0) {
			return "";
		}
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0100DAO.openBasclt0100TOCsr(KeyType.GREATER_OR_EQ, basclt0100RecTO.clt0100recRec.getCltseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}*/
		//CSC-Manual End
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return "";
		}
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1400RecTO.clt1400recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1400RecTO.clt1400recRec.getAddrseqnum());
		//CSC-Manual Start
		/*cobolsqlca.setDBAccessStatus(basclt0300DAO.openBasclt0300TOCsr(KeyType.GREATER_OR_EQ, basclt0300RecTO.clt0300recRec.getCltseqnum(),
			basclt0300RecTO.clt0300recRec.getAddrseqnum()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		basclt0300TO = basclt0300DAO.fetchNext(basclt0300TO);*/
		basclt0300TO = Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		//CSC-Manual End
		basclt0300RecTO.setData(basclt0300TO.getData());
		basclt0300RecTO.setData(basclt0300RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return "";
		}
		if (basclt1400RecTO.clt1400recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1400RecTO.clt1400recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return "";
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return "";
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0254RecTO.rec02fmtRec10.getTrans0stat();
		holdBcSymbol = pmsl0254RecTO.rec02fmtRec10.getSymbol();
		holdBcPolicy0num = pmsl0254RecTO.rec02fmtRec10.getPolicy0num();
		holdBcModule = pmsl0254RecTO.rec02fmtRec10.getModule();
		holdBcMaster0co = pmsl0254RecTO.rec02fmtRec10.getMaster0co();
		holdBcLocation = pmsl0254RecTO.rec02fmtRec10.getLocation();
		holdBcEffdt = pmsl0254RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0254RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0254RecTO.rec02fmtRec10.getRisk0state();
		holdBcCompany0no = pmsl0254RecTO.rec02fmtRec10.getCompany0no();
		holdBcAgency = pmsl0254RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0254RecTO.rec02fmtRec10.getLine0bus();
		holdBcRenewal0cd = pmsl0254RecTO.rec02fmtRec10.getRenewal0cd();
		holdBcIssue0code = pmsl0254RecTO.rec02fmtRec10.getIssue0code();
		holdBcPay0code = pmsl0254RecTO.rec02fmtRec10.getPay0code();
		holdBcMode0code = pmsl0254RecTO.rec02fmtRec10.getMode0code();
		holdBcSort0name = pmsl0254RecTO.rec02fmtRec10.getSort0name();
		holdBcCust0no = pmsl0254RecTO.rec02fmtRec10.getCust0no();
		holdBcSpec0use0a = pmsl0254RecTO.rec02fmtRec10.getSpec0use0a();
		holdBcSpec0use0b = pmsl0254RecTO.rec02fmtRec10.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0254RecTO.rec02fmtRec10.getCanceldate();
		}
		holdBcMrsseq = pmsl0254RecTO.rec02fmtRec10.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0254RecTO.rec02fmtRec10.getTot0ag0prm());
		holdBcType0act = pmsl0254RecTO.rec02fmtRec10.getType0act();
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0254RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0254RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0254() {
	// exit
	}

	public void readBasclt0104() {
		basclt0104TO = basclt0104DAO.fetchNext(basclt0104TO);
		basclt0104RecTO.setData(basclt0104TO.getData());
		basclt0104RecTO.setData(basclt0104RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0104TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0104TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0104RecTO.clt0101recRec4.getPhone1()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0104RecTO.clt0101recRec4.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0104RecTO.clt0101recRec4.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0104RecTO.clt0101recRec4.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0104RecTO.clt0101recRec4.getSsn()) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0104-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ZERO TO ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0104RecTO.clt0101recRec4.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(((short) (0 % 100)));
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100PhoneStart();
		}
	}

	public void exitBasclt0104() {
	// exit
	}

	public void crefer0100PhoneStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0104RecTO.clt0101recRec4.getCltseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC)
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC)
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo("") != 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		} else {
			// B0750A MOVE MASTER0CO OF CLT1401REC TO MASTER0CO OF PMSL0259-REC
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(basclt1402RecTO.clt1402recRec.getMaster0co());
		}
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && pmsl0259RecTO.rec02fmtRec2.getModule().compareTo(rskdbio021TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF BASCLT0300-REC.
		// MOVE ADDRSEQNUM OF CLT1401REC TO ADDRSEQNUM OF
		// BASCLT0300-REC.
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1402RecTO.clt1402recRec.getAddrseqnum());
		basclt0300TO =
			Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		if (basclt0300TO.getDBAccessStatus().isSuccess()) {
			basclt0300RecTO.setData(basclt0300TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		// IF CLTSEQNUM OF CLT1401REC NOT EQUAL CLTSEQNUM OF
		// BASCLT0300-REC OR
		// ADDRSEQNUM OF CLT1401REC NOT EQUAL ADDRSEQNUM OF
		// BASCLT0300-REC
		// GO TO 3000-CREFER-0100-PHONE-END
		// END-IF.
		if (basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0300RecTO.clt0300recRec.getCltseqnum()
			|| basclt1402RecTO.clt1402recRec.getAddrseqnum() != basclt0300RecTO.clt0300recRec.getAddrseqnum()) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		//
		holdBcLastname = basclt0104RecTO.clt0101recRec4.getClientname();
		holdBcFirstname = basclt0104RecTO.clt0101recRec4.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt0104RecTO.clt0101recRec4.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt0104RecTO.clt0101recRec4.getFedtaxid();
		holdBcGroupno = basclt0104RecTO.clt0101recRec4.getGroupno();
		holdBcPhone1 = basclt0104RecTO.clt0101recRec4.getPhone1();
		holdBcSsn = basclt0104RecTO.clt0101recRec4.getSsn();
		holdBcNametype = basclt0104RecTO.clt0101recRec4.getNametype();
		holdBcNamestatus = basclt0104RecTO.clt0101recRec4.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt0104RecTO.clt0101recRec4.getDba(), 1, 30);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry1();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void crefer0100PhoneEnd() {
	// exit
	}

	public String readBasclt0105() {
		basclt0105TO = basclt0105DAO.fetchNext(basclt0105TO);
		basclt0105RecTO.setData(basclt0105TO.getData());
		basclt0105RecTO.setData(basclt0105RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0105TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0105TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0105RecTO.clt0101recRec5.getPhone1()) != 0) {
			return "";
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0105RecTO.clt0101recRec5.getGroupno(), 1, wsGnLength))) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return "";
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0105RecTO.clt0101recRec5.getClientname(), 1, wsLnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0105RecTO.clt0101recRec5.getFirstname(), 1, wsFnLength))) != 0) {
			return "";
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0105RecTO.clt0101recRec5.getSsn()) != 0) {
			return "2000-EXIT-PMSL0259";
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0105-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ZERO TO ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0105RecTO.clt0101recRec5.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(((short) (0 % 100)));
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100GroupStart();
		}
		return "";
	}

	public void exitBasclt0105() {
	// exit
	}

	public void crefer0100GroupStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0105RecTO.clt0101recRec5.getCltseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC)
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC)
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo("") != 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		} else {
			// B0750A MOVE MASTER0CO OF CLT1401REC TO MASTER0CO OF PMSL0259-REC
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(basclt1402RecTO.clt1402recRec.getMaster0co());
		}
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0259RecTO.rec02fmtRec2.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF BASCLT0300-REC.
		// MOVE ADDRSEQNUM OF CLT1401REC TO ADDRSEQNUM OF
		// BASCLT0300-REC.
		basclt0300RecTO.clt0300recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0300RecTO.clt0300recRec.setAddrseqnum(basclt1402RecTO.clt1402recRec.getAddrseqnum());
		basclt0300TO =
			Basclt0300DAO.selectById(basclt0300TO, basclt0300RecTO.clt0300recRec.getCltseqnum(), basclt0300RecTO.clt0300recRec.getAddrseqnum());
		if (basclt0300TO.getDBAccessStatus().isSuccess()) {
			basclt0300RecTO.setData(basclt0300TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0300TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0300TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0300RecTO.clt0300recRec.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		holdBcLastname = basclt0105RecTO.clt0101recRec5.getClientname();
		holdBcFirstname = basclt0105RecTO.clt0101recRec5.getFirstname();
		holdBcMiddlename = Functions.charAt(Functions.subString(basclt0105RecTO.clt0101recRec5.getMidname(), 1, 1), 1);
		holdBcFedtaxid = basclt0105RecTO.clt0101recRec5.getFedtaxid();
		holdBcGroupno = basclt0105RecTO.clt0101recRec5.getGroupno();
		holdBcPhone1 = basclt0105RecTO.clt0101recRec5.getPhone1();
		holdBcSsn = basclt0105RecTO.clt0101recRec5.getSsn();
		holdBcNametype = basclt0105RecTO.clt0101recRec5.getNametype();
		holdBcNamestatus = basclt0105RecTO.clt0101recRec5.getNamestatus();
		holdBcAdd0line02 = Functions.subString(basclt0105RecTO.clt0101recRec5.getDba(), 1, 30);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry1();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void readBasclt0311() {
		basclt0311TO = basclt0311DAO.fetchNext(basclt0311TO);
		basclt0311RecTO.setData(basclt0311TO.getData());
		basclt0311RecTO.setData(basclt0311RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0311TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0311TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0311RecTO.clt0300recRec2.getZipcode(), 1, wsZcLength)) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0311RecTO.clt0300recRec2.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0311RecTO.clt0300recRec2.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0311-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ADDRSEQNUM OF BASCLT0311-REC TO
		// ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0311RecTO.clt0300recRec2.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(basclt0311RecTO.clt0300recRec2.getAddrseqnum());
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0100PostalStart();
		}
	}

	public void crefer0100PostalStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		// ADDRSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0311RecTO.clt0300recRec2.getCltseqnum()
			|| basclt1402RecTO.clt1402recRec.getAddrseqnum() != basclt0311RecTO.clt0300recRec2.getAddrseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC)
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC)
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo("") != 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		} else {
			// B0750A MOVE MASTER0CO OF CLT1401REC TO MASTER0CO OF PMSL0259-REC
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(basclt1402RecTO.clt1402recRec.getMaster0co());
		}
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0259RecTO.rec02fmtRec2.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF CLT0100REC.
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		holdBcAddrln1 = Functions.subString(basclt0311RecTO.clt0300recRec2.getAddrln1(), 1, 30);
		holdBcCity = Functions.subString(basclt0311RecTO.clt0300recRec2.getCity(), 1, 28);
		holdBcState = basclt0311RecTO.clt0300recRec2.getState();
		holdBcZipcode = Functions.subString(basclt0311RecTO.clt0300recRec2.getZipcode(), 1, 10);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry2();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void readBasclt0312() {
		basclt0312TO = basclt0312DAO.fetchNext(basclt0312TO);
		basclt0312RecTO.setData(basclt0312TO.getData());
		basclt0312RecTO.setData(basclt0312RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0312TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0312TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0312RecTO.clt0300recRec3.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0312RecTO.clt0300recRec3.getState()) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0312RecTO.clt0300recRec3.getCity(), 1, wsCtLength))) != 0) {
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0312-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ADDRSEQNUM OF BASCLT0312-REC TO
		// ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0312RecTO.clt0300recRec3.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(basclt0312RecTO.clt0300recRec3.getAddrseqnum());
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0300StateStart();
		}
	}

	public void crefer0300StateStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		// ADDRSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0312RecTO.clt0300recRec3.getCltseqnum()
			|| basclt1402RecTO.clt1402recRec.getAddrseqnum() != basclt0312RecTO.clt0300recRec3.getAddrseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC)
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC)
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo("") != 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		} else {
			// B0750A MOVE MASTER0CO OF CLT1401REC TO MASTER0CO OF PMSL0259-REC
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(basclt1402RecTO.clt1402recRec.getMaster0co());
		}
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0259RecTO.rec02fmtRec2.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF CLT0100REC.
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		holdBcAddrln1 = Functions.subString(basclt0312RecTO.clt0300recRec3.getAddrln1(), 1, 30);
		holdBcCity = Functions.subString(basclt0312RecTO.clt0300recRec3.getCity(), 1, 28);
		holdBcState = basclt0312RecTO.clt0300recRec3.getState();
		holdBcZipcode = Functions.subString(basclt0312RecTO.clt0300recRec3.getZipcode(), 1, 10);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry2();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void readBasclt0313() {
		basclt0313TO = basclt0313DAO.fetchNext(basclt0313TO);
		basclt0313RecTO.setData(basclt0313TO.getData());
		basclt0313RecTO.setData(basclt0313RecTO.getData());
		ws1FileStatus = Functions.subString(basclt0313TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0313TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (rskdbio021TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio021TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(basclt0313RecTO.clt0300recRec4.getZipcode(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio021TO.getInState().compareTo("") != 0 && rskdbio021TO.getInState().compareTo(basclt0313RecTO.clt0300recRec4.getState()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCity().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInCity(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0313RecTO.clt0300recRec4.getCity(), 1, wsCtLength))) != 0) {
			if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		// MOVE SPACES TO CLT1401REC
		// MOVE CLTSEQNUM OF BASCLT0313-REC TO CLTSEQNUM OF CLT1401REC
		// MOVE ADDRSEQNUM OF BASCLT0313-REC TO
		// ADDRSEQNUM OF CLT1401REC
		// START BASCLT1401-FILE
		basclt1402RecTO.clt1402recRec.initClt1402recSpaces();
		basclt1402RecTO.clt1402recRec.setCltseqnum(basclt0313RecTO.clt0300recRec4.getCltseqnum());
		basclt1402RecTO.clt1402recRec.setAddrseqnum(basclt0313RecTO.clt0300recRec4.getAddrseqnum());
		cobolsqlca.setDBAccessStatus(basclt1402DAO.openBasclt1402TOCsr(KeyType.GREATER_OR_EQ, basclt1402RecTO.clt1402recRec.getCltseqnum(),
			basclt1402RecTO.clt1402recRec.getAddrseqnum(), basclt1402RecTO.clt1402recRec.getLocation(), basclt1402RecTO.clt1402recRec.getMaster0co(),
			basclt1402RecTO.clt1402recRec.getSymbol(), basclt1402RecTO.clt1402recRec.getPolicy0num(), basclt1402RecTO.clt1402recRec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		endCrossRead = ' ';
		while (!(endCrossRead == END_FILE_REACHED)) {
			crefer0300CityStart();
		}
	}

	public void crefer0300CityStart() {
		// READ BASCLT1401-FILE NEXT RECORD
		basclt1402TO = basclt1402DAO.fetchNext(basclt1402TO);
		basclt1402RecTO.setData(basclt1402TO.getData());
		ws1FileStatus = Functions.subString(basclt1402TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt1402TO.getDBAccessStatus().isInvalidKey()) {
			endCrossRead = 'Y';
			return;
		}
		// CLTSEQNUM OF CLT1401REC NOT EQUAL
		// ADDRSEQNUM OF CLT1401REC NOT EQUAL
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)
			|| basclt1402RecTO.clt1402recRec.getCltseqnum() != basclt0313RecTO.clt0300recRec4.getCltseqnum()
			|| basclt1402RecTO.clt1402recRec.getAddrseqnum() != basclt0313RecTO.clt0300recRec4.getAddrseqnum()) {
			endCrossRead = 'Y';
			return;
		}
		// IN-MASTER0CO NOT EQUAL MASTER0CO OF CLT1401REC)
		// IN-LOCATION NOT EQUAL LOCATION OF CLT1401REC)
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(basclt1402RecTO.clt1402recRec.getMaster0co()) != 0
			|| rskdbio021TO.getInLocation().compareTo("") != 0
			&& rskdbio021TO.getInLocation().compareTo(basclt1402RecTO.clt1402recRec.getLocation()) != 0) {
			return;
		}
		pmsl0259RecTO.rec02fmtRec2.setLocation(rskdbio021TO.getInLocation());
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0) {
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(rskdbio021TO.getInMaster0co());
		} else {
			// B0750A MOVE MASTER0CO OF CLT1401REC TO MASTER0CO OF PMSL0259-REC
			pmsl0259RecTO.rec02fmtRec2.setMaster0co(basclt1402RecTO.clt1402recRec.getMaster0co());
		}
		// MOVE SYMBOL OF CLT1401REC TO SYMBOL OF PMSL0259-REC.
		// MOVE POLICY0NUM OF CLT1401REC TO POLICY0NUM OF PMSL0259-REC.
		// MOVE MODULE OF CLT1401REC TO MODULE OF PMSL0259-REC.
		pmsl0259RecTO.rec02fmtRec2.setSymbol(basclt1402RecTO.clt1402recRec.getSymbol());
		pmsl0259RecTO.rec02fmtRec2.setPolicy0num(basclt1402RecTO.clt1402recRec.getPolicy0num());
		pmsl0259RecTO.rec02fmtRec2.setModule(basclt1402RecTO.clt1402recRec.getModule());
		pmsl0259RecTO.rec02fmtRec2.setTrans0stat(' ');
		// BPHX changes start - fix 157870
		/*pmsl0259TO =
			Pmsl0259DAO.selectById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());*/
		pmsl0259TO =
			pmsl0259DAO.fetchById(pmsl0259TO, pmsl0259RecTO.rec02fmtRec2.getLocation(), pmsl0259RecTO.rec02fmtRec2.getPolicy0num(),
				pmsl0259RecTO.rec02fmtRec2.getSymbol(), pmsl0259RecTO.rec02fmtRec2.getMaster0co(), pmsl0259RecTO.rec02fmtRec2.getModule());
		// BPHX changes end - fix 157870
		if (pmsl0259TO.getDBAccessStatus().isSuccess()) {
			pmsl0259RecTO.setData(pmsl0259TO.getData());
		}
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio021TO.getInModule().compareTo("") != 0 && rskdbio021TO.getInModule().compareTo(pmsl0259RecTO.rec02fmtRec2.getModule()) != 0) {
			return;
		}
		if (rskdbio021TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio021TO.getInPolicy0num().compareTo(pmsl0259RecTO.rec02fmtRec2.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio021TO.getInMaster0co().compareTo("") != 0
			&& rskdbio021TO.getInMaster0co().compareTo(pmsl0259RecTO.rec02fmtRec2.getMaster0co()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLocation().compareTo("") != 0 && rskdbio021TO.getInLocation().compareTo(pmsl0259RecTO.rec02fmtRec2.getLocation()) != 0) {
			return;
		}
		if (rskdbio021TO.getInSymbol().compareTo("") != 0 && rskdbio021TO.getInSymbol().compareTo(pmsl0259RecTO.rec02fmtRec2.getSymbol()) != 0) {
			return;
		}
		if (rskdbio021TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio021TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio021TO.getInAgency().compareTo("") != 0 && rskdbio021TO.getInAgency().compareTo(pmsl0259RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio021TO.getInLine0bus().compareTo("") != 0 && rskdbio021TO.getInLine0bus().compareTo(pmsl0259RecTO.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		// MOVE CLTSEQNUM OF CLT1401REC TO CLTSEQNUM OF CLT0100REC.
		basclt0100RecTO.clt0100recRec.setCltseqnum(basclt1402RecTO.clt1402recRec.getCltseqnum());
		basclt0100TO = Basclt0100DAO.selectById(basclt0100TO, basclt0100RecTO.clt0100recRec.getCltseqnum());
		if (basclt0100TO.getDBAccessStatus().isSuccess()) {
			basclt0100RecTO.setData(basclt0100TO.getData());
		}
		ws1FileStatus = Functions.subString(basclt0100TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (basclt0100TO.getDBAccessStatus().isInvalidKey()) {
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			return;
		}
		if (rskdbio021TO.getInPhone1().compareTo("") != 0 && rskdbio021TO.getInPhone1().compareTo(basclt0100RecTO.clt0100recRec.getPhone1()) != 0) {
			return;
		}
		if (rskdbio021TO.getInGroupno().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInGroupno(), 1, wsGnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getGroupno(), 1, wsGnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInLastname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInLastname(), 1, wsLnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getClientname(), 1, wsLnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInFirstname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio021TO.getInFirstname(), 1, wsFnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(basclt0100RecTO.clt0100recRec.getFirstname(), 1, wsFnLength))) != 0) {
			return;
		}
		if (rskdbio021TO.getInSsn().compareTo("") != 0 && rskdbio021TO.getInSsn().compareTo(basclt0100RecTO.clt0100recRec.getSsn()) != 0) {
			return;
		}
		holdBcAddrln1 = Functions.subString(basclt0313RecTO.clt0300recRec4.getAddrln1(), 1, 30);
		holdBcCity = Functions.subString(basclt0313RecTO.clt0300recRec4.getCity(), 1, 28);
		holdBcState = basclt0313RecTO.clt0300recRec4.getState();
		holdBcZipcode = Functions.subString(basclt0313RecTO.clt0300recRec4.getZipcode(), 1, 10);
		//RSK - START
		
		if(!rskdbio021TO.getInLossdte().equals("") && !rskdbio021TO.getInLossdte().equals("0000000")){
			if(rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio021TO.getInLossdte().compareTo(pmsl0259RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry2();
		conditionCheck();
		if (numOfEntrys == rskdbio021TO.getInMaxrecords()) {
			endCrossRead = 'Y';
			endFileRead = 'Y';
			return;
		}
	}

	public void processEntry() {
		//
		// Move the data to the table
		//
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(holdBcTrans0stat);
		bcListEntryArr8[(wsCount) - 1].setBcSymbol(holdBcSymbol);
		bcListEntryArr8[(wsCount) - 1].setBcPolicy0num(holdBcPolicy0num);
		bcListEntryArr8[(wsCount) - 1].setBcModule(holdBcModule);
		bcListEntryArr8[(wsCount) - 1].setBcMaster0co(holdBcMaster0co);
		bcListEntryArr8[(wsCount) - 1].setBcLocation(holdBcLocation);
		bcListEntryArr8[(wsCount) - 1].setBcEffdt(holdBcEffdt);
		bcListEntryArr8[(wsCount) - 1].setBcExpdt(holdBcExpdt);
		bcListEntryArr8[(wsCount) - 1].setBcRisk0state(holdBcRisk0state);
		bcListEntryArr8[(wsCount) - 1].setBcCompany0no(holdBcCompany0no);
		bcListEntryArr8[(wsCount) - 1].setBcAgency(holdBcAgency);
		bcListEntryArr8[(wsCount) - 1].setBcLine0bus(holdBcLine0bus);
		bcListEntryArr8[(wsCount) - 1].setBcRenewal0cd(holdBcRenewal0cd);
		bcListEntryArr8[(wsCount) - 1].setBcIssue0code(holdBcIssue0code);
		bcListEntryArr8[(wsCount) - 1].setBcPay0code(holdBcPay0code);
		bcListEntryArr8[(wsCount) - 1].setBcMode0code(holdBcMode0code);
		bcListEntryArr8[(wsCount) - 1].setBcSort0name(holdBcSort0name);
		bcListEntryArr8[(wsCount) - 1].setBcCust0no(holdBcCust0no);
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0a(holdBcSpec0use0a);
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0b(holdBcSpec0use0b);
		bcListEntryArr8[(wsCount) - 1].setBcCanceldate(holdBcCanceldate);
		bcListEntryArr8[(wsCount) - 1].setBcMrsseq(holdBcMrsseq);
		bcListEntryArr8[(wsCount) - 1].setBcTot0ag0prm(holdBcTot0ag0prm.clone());
		//
		bcListEntryArr8[(wsCount) - 1].setBcLastname(basclt0100RecTO.clt0100recRec.getClientname());
		bcListEntryArr8[(wsCount) - 1].setBcFirstname(basclt0100RecTO.clt0100recRec.getFirstname());
		bcListEntryArr8[(wsCount) - 1].setBcMiddlename(Functions.charAt(Functions.subString(basclt0100RecTO.clt0100recRec.getMidname(), 1, 1), 1));
		bcListEntryArr8[(wsCount) - 1].setBcFedtaxid(basclt0100RecTO.clt0100recRec.getFedtaxid());
		bcListEntryArr8[(wsCount) - 1].setBcGroupno(basclt0100RecTO.clt0100recRec.getGroupno());
		bcListEntryArr8[(wsCount) - 1].setBcPhone1(basclt0100RecTO.clt0100recRec.getPhone1());
		bcListEntryArr8[(wsCount) - 1].setBcSsn(basclt0100RecTO.clt0100recRec.getSsn());
		bcListEntryArr8[(wsCount) - 1].setBcNametype(basclt0100RecTO.clt0100recRec.getNametype());
		bcListEntryArr8[(wsCount) - 1].setBcNamestatus(basclt0100RecTO.clt0100recRec.getNamestatus());
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line02(Functions.subString(basclt0100RecTO.clt0100recRec.getDba(), 1, 30));
		//
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line03(Functions.subString(basclt0300RecTO.clt0300recRec.getAddrln1(), 1, 30));
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line04(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, 28));
		bcListEntryArr8[(wsCount) - 1].setBcState(basclt0300RecTO.clt0300recRec.getState());
		bcListEntryArr8[(wsCount) - 1].setBcZip0post(Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, 10));
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '0') {
			bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Agent To Renew");
		} else {
			if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '1') {
				bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Computer Renew");
			} else {
				if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '2') {
					bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Home Office To renew");
				} else {
					if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '3') {
						bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Already Renewed");
					} else {
						if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '4') {
							bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
						} else {
							if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '7') {
								bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal With A Notice");
							} else {
								if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '8') {
									bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal Without A Notice");
								} else {
									if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9') {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Cancelled Policy");
									} else {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("");
									}
								}
							}
						}
					}
				}
			}
		}
		// --------------*
		// READ PMSL0004 *
		// --------------*
		rec00fmtRec3.setSymbol(holdBcSymbol);
		rec00fmtRec3.setPolicy0num(holdBcPolicy0num);
		rec00fmtRec3.setModule(holdBcModule);
		rec00fmtRec3.setMaster0co(holdBcMaster0co);
		rec00fmtRec3.setLocation(holdBcLocation);
		wsControlSw = 'Y';
		try {
			call1();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASL000401") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		// If no record was returned then the policy is verified so
		// set the status on the activity record to a 'V'
		if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
			// MOVE 'V' TO TRANS0STAT OF REC00FMT
			rec00fmtRec3.setTrans0stat(holdBcTrans0stat);
			rec00fmtRec3.setType0act(holdBcType0act);
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Verified");
		//
		//
		if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Pending");
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Pending");
				}
			}
			return;
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(rec00fmtRec3.getTrans0stat());
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9' && bcListEntryArr8[(wsCount) - 1].getBcCanceldate().compareTo("") != 0) {
			if (bcListEntryArr8[(wsCount) - 1].getBcTrans0stat() == 'V') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('C');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Cancelled");
				return;
			}
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('1');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('X');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('2');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('W');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('3');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('I');
				}
			}
			return;
		}
	}

	public void processEntry1() {
		//
		// Move the data to the table
		//
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(pmsl0259RecTO.rec02fmtRec2.getTrans0stat());
		bcListEntryArr8[(wsCount) - 1].setBcSymbol(pmsl0259RecTO.rec02fmtRec2.getSymbol());
		bcListEntryArr8[(wsCount) - 1].setBcPolicy0num(pmsl0259RecTO.rec02fmtRec2.getPolicy0num());
		bcListEntryArr8[(wsCount) - 1].setBcModule(pmsl0259RecTO.rec02fmtRec2.getModule());
		bcListEntryArr8[(wsCount) - 1].setBcMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
		bcListEntryArr8[(wsCount) - 1].setBcLocation(pmsl0259RecTO.rec02fmtRec2.getLocation());
		bcListEntryArr8[(wsCount) - 1].setBcEffdt(pmsl0259RecTO.getRec02fmtEffdtAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcExpdt(pmsl0259RecTO.getRec02fmtExpdtAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcRisk0state(pmsl0259RecTO.rec02fmtRec2.getRisk0state());
		bcListEntryArr8[(wsCount) - 1].setBcCompany0no(pmsl0259RecTO.rec02fmtRec2.getCompany0no());
		bcListEntryArr8[(wsCount) - 1].setBcAgency(pmsl0259RecTO.getRec02fmtAgencyAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcLine0bus(pmsl0259RecTO.rec02fmtRec2.getLine0bus());
		bcListEntryArr8[(wsCount) - 1].setBcRenewal0cd(pmsl0259RecTO.rec02fmtRec2.getRenewal0cd());
		bcListEntryArr8[(wsCount) - 1].setBcIssue0code(pmsl0259RecTO.rec02fmtRec2.getIssue0code());
		bcListEntryArr8[(wsCount) - 1].setBcPay0code(pmsl0259RecTO.rec02fmtRec2.getPay0code());
		bcListEntryArr8[(wsCount) - 1].setBcMode0code(pmsl0259RecTO.rec02fmtRec2.getMode0code());
		bcListEntryArr8[(wsCount) - 1].setBcSort0name(pmsl0259RecTO.rec02fmtRec2.getSort0name());
		bcListEntryArr8[(wsCount) - 1].setBcCust0no(pmsl0259RecTO.rec02fmtRec2.getCust0no());
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0a(pmsl0259RecTO.rec02fmtRec2.getSpec0use0a());
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0b(pmsl0259RecTO.rec02fmtRec2.getSpec0use0b());
		if (holdBcRenewal0cd != '9') {
			bcListEntryArr8[(wsCount) - 1].setBcCanceldate("");
		} else {
			bcListEntryArr8[(wsCount) - 1].setBcCanceldate(pmsl0259RecTO.rec02fmtRec2.getCanceldate());
		}
		bcListEntryArr8[(wsCount) - 1].setBcMrsseq(pmsl0259RecTO.rec02fmtRec2.getMrsseq());
		bcListEntryArr8[(wsCount) - 1].setBcTot0ag0prm(pmsl0259RecTO.rec02fmtRec2.getTot0ag0prm());
		bcListEntryArr8[(wsCount) - 1].setBcLastname(holdBcLastname);
		bcListEntryArr8[(wsCount) - 1].setBcFirstname(holdBcFirstname);
		bcListEntryArr8[(wsCount) - 1].setBcMiddlename(holdBcMiddlename);
		bcListEntryArr8[(wsCount) - 1].setBcFedtaxid(holdBcFedtaxid);
		bcListEntryArr8[(wsCount) - 1].setBcGroupno(holdBcGroupno);
		bcListEntryArr8[(wsCount) - 1].setBcPhone1(holdBcPhone1);
		bcListEntryArr8[(wsCount) - 1].setBcSsn(holdBcSsn);
		bcListEntryArr8[(wsCount) - 1].setBcNametype(holdBcNametype);
		bcListEntryArr8[(wsCount) - 1].setBcNamestatus(holdBcNamestatus);
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line02(holdBcAdd0line02);
		//
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line03(Functions.subString(basclt0300RecTO.clt0300recRec.getAddrln1(), 1, 30));
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line04(Functions.subString(basclt0300RecTO.clt0300recRec.getCity(), 1, 28));
		bcListEntryArr8[(wsCount) - 1].setBcState(basclt0300RecTO.clt0300recRec.getState());
		bcListEntryArr8[(wsCount) - 1].setBcZip0post(Functions.subString(basclt0300RecTO.clt0300recRec.getZipcode(), 1, 10));
		// --------------*
		// READ PMSL0004 *
		// --------------*
		rec00fmtRec3.setSymbol(pmsl0259RecTO.rec02fmtRec2.getSymbol());
		rec00fmtRec3.setPolicy0num(pmsl0259RecTO.rec02fmtRec2.getPolicy0num());
		rec00fmtRec3.setModule(pmsl0259RecTO.rec02fmtRec2.getModule());
		rec00fmtRec3.setMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
		rec00fmtRec3.setLocation(pmsl0259RecTO.rec02fmtRec2.getLocation());
		wsControlSw = 'Y';
		try {
			call2();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASL000401") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		// If no record was returned then the policy is verified so
		// set the status on the activity record to a 'V'
		if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
			// MOVE 'V' TO TRANS0STAT OF REC00FMT
			rec00fmtRec3.setTrans0stat(pmsl0259RecTO.rec02fmtRec2.getTrans0stat());
			rec00fmtRec3.setType0act(pmsl0259RecTO.rec02fmtRec2.getType0act());
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Verified");
		//
		//
		if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Pending");
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Pending");
				}
			}
			return;
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(rec00fmtRec3.getTrans0stat());
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9' && bcListEntryArr8[(wsCount) - 1].getBcCanceldate().compareTo("") != 0) {
			if (bcListEntryArr8[(wsCount) - 1].getBcTrans0stat() == 'V') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('C');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Cancelled");
				return;
			}
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('1');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('X');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('2');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('W');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('3');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('I');
				}
			}
			return;
		}
	}

	public void processEntry2() {
		//
		// Move the data to the table
		//
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(pmsl0259RecTO.rec02fmtRec2.getTrans0stat());
		bcListEntryArr8[(wsCount) - 1].setBcSymbol(pmsl0259RecTO.rec02fmtRec2.getSymbol());
		bcListEntryArr8[(wsCount) - 1].setBcPolicy0num(pmsl0259RecTO.rec02fmtRec2.getPolicy0num());
		bcListEntryArr8[(wsCount) - 1].setBcModule(pmsl0259RecTO.rec02fmtRec2.getModule());
		bcListEntryArr8[(wsCount) - 1].setBcMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
		bcListEntryArr8[(wsCount) - 1].setBcLocation(pmsl0259RecTO.rec02fmtRec2.getLocation());
		bcListEntryArr8[(wsCount) - 1].setBcEffdt(pmsl0259RecTO.getRec02fmtEffdtAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcExpdt(pmsl0259RecTO.getRec02fmtExpdtAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcRisk0state(pmsl0259RecTO.rec02fmtRec2.getRisk0state());
		bcListEntryArr8[(wsCount) - 1].setBcCompany0no(pmsl0259RecTO.rec02fmtRec2.getCompany0no());
		bcListEntryArr8[(wsCount) - 1].setBcAgency(pmsl0259RecTO.getRec02fmtAgencyAsStr());
		bcListEntryArr8[(wsCount) - 1].setBcLine0bus(pmsl0259RecTO.rec02fmtRec2.getLine0bus());
		bcListEntryArr8[(wsCount) - 1].setBcRenewal0cd(pmsl0259RecTO.rec02fmtRec2.getRenewal0cd());
		bcListEntryArr8[(wsCount) - 1].setBcIssue0code(pmsl0259RecTO.rec02fmtRec2.getIssue0code());
		bcListEntryArr8[(wsCount) - 1].setBcPay0code(pmsl0259RecTO.rec02fmtRec2.getPay0code());
		bcListEntryArr8[(wsCount) - 1].setBcMode0code(pmsl0259RecTO.rec02fmtRec2.getMode0code());
		bcListEntryArr8[(wsCount) - 1].setBcSort0name(pmsl0259RecTO.rec02fmtRec2.getSort0name());
		bcListEntryArr8[(wsCount) - 1].setBcCust0no(pmsl0259RecTO.rec02fmtRec2.getCust0no());
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0a(pmsl0259RecTO.rec02fmtRec2.getSpec0use0a());
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0b(pmsl0259RecTO.rec02fmtRec2.getSpec0use0b());
		if (holdBcRenewal0cd != '9') {
			bcListEntryArr8[(wsCount) - 1].setBcCanceldate("");
		} else {
			bcListEntryArr8[(wsCount) - 1].setBcCanceldate(pmsl0259RecTO.rec02fmtRec2.getCanceldate());
		}
		bcListEntryArr8[(wsCount) - 1].setBcMrsseq(pmsl0259RecTO.rec02fmtRec2.getMrsseq());
		bcListEntryArr8[(wsCount) - 1].setBcTot0ag0prm(pmsl0259RecTO.rec02fmtRec2.getTot0ag0prm());
		//
		bcListEntryArr8[(wsCount) - 1].setBcLastname(basclt0100RecTO.clt0100recRec.getClientname());
		bcListEntryArr8[(wsCount) - 1].setBcFirstname(basclt0100RecTO.clt0100recRec.getFirstname());
		bcListEntryArr8[(wsCount) - 1].setBcMiddlename(Functions.charAt(Functions.subString(basclt0100RecTO.clt0100recRec.getMidname(), 1, 1), 1));
		bcListEntryArr8[(wsCount) - 1].setBcFedtaxid(basclt0100RecTO.clt0100recRec.getFedtaxid());
		bcListEntryArr8[(wsCount) - 1].setBcGroupno(basclt0100RecTO.clt0100recRec.getGroupno());
		bcListEntryArr8[(wsCount) - 1].setBcPhone1(basclt0100RecTO.clt0100recRec.getPhone1());
		bcListEntryArr8[(wsCount) - 1].setBcSsn(basclt0100RecTO.clt0100recRec.getSsn());
		bcListEntryArr8[(wsCount) - 1].setBcNametype(basclt0100RecTO.clt0100recRec.getNametype());
		bcListEntryArr8[(wsCount) - 1].setBcNamestatus(basclt0100RecTO.clt0100recRec.getNamestatus());
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line02(Functions.subString(basclt0100RecTO.clt0100recRec.getDba(), 1, 30));
		//
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line03(holdBcAddrln1);
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line04(holdBcCity);
		bcListEntryArr8[(wsCount) - 1].setBcState(holdBcState);
		bcListEntryArr8[(wsCount) - 1].setBcZip0post(holdBcZipcode);
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '0') {
			bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Agent To Renew");
		} else {
			if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '1') {
				bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Computer Renew");
			} else {
				if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '2') {
					bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Home Office To renew");
				} else {
					if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '3') {
						bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Already Renewed");
					} else {
						if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '4') {
							bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
						} else {
							if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '7') {
								bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal With A Notice");
							} else {
								if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '8') {
									bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal Without A Notice");
								} else {
									if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9') {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Cancelled Policy");
									} else {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("");
									}
								}
							}
						}
					}
				}
			}
		}
		// --------------*
		// READ PMSL0004 *
		// --------------*
		rec00fmtRec3.setSymbol(pmsl0259RecTO.rec02fmtRec2.getSymbol());
		rec00fmtRec3.setPolicy0num(pmsl0259RecTO.rec02fmtRec2.getPolicy0num());
		rec00fmtRec3.setModule(pmsl0259RecTO.rec02fmtRec2.getModule());
		rec00fmtRec3.setMaster0co(pmsl0259RecTO.rec02fmtRec2.getMaster0co());
		rec00fmtRec3.setLocation(pmsl0259RecTO.rec02fmtRec2.getLocation());
		wsControlSw = 'Y';
		try {
			call3();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASL000401") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		// If no record was returned then the policy is verified so
		// set the status on the activity record to a 'V'
		if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
			// MOVE 'V' TO TRANS0STAT OF REC00FMT
			rec00fmtRec3.setTrans0stat(pmsl0259RecTO.rec02fmtRec2.getTrans0stat());
			rec00fmtRec3.setType0act(pmsl0259RecTO.rec02fmtRec2.getType0act());
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Verified");
		//
		//
		if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Pending");
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Pending");
				}
			}
			return;
		}
		//
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(rec00fmtRec3.getTrans0stat());
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9' && bcListEntryArr8[(wsCount) - 1].getBcCanceldate().compareTo("") != 0) {
			if (bcListEntryArr8[(wsCount) - 1].getBcTrans0stat() == 'V') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('C');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Cancelled");
				return;
			}
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('1');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('X');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('2');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('W');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('3');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('I');
				}
			}
			return;
		}
	}

	public void processEntry3() {
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(holdBcTrans0stat);
		bcListEntryArr8[(wsCount) - 1].setBcSymbol(holdBcSymbol);
		bcListEntryArr8[(wsCount) - 1].setBcPolicy0num(holdBcPolicy0num);
		bcListEntryArr8[(wsCount) - 1].setBcModule(holdBcModule);
		bcListEntryArr8[(wsCount) - 1].setBcMaster0co(holdBcMaster0co);
		bcListEntryArr8[(wsCount) - 1].setBcLocation(holdBcLocation);
		bcListEntryArr8[(wsCount) - 1].setBcEffdt(holdBcEffdt);
		bcListEntryArr8[(wsCount) - 1].setBcExpdt(holdBcExpdt);
		bcListEntryArr8[(wsCount) - 1].setBcRisk0state(holdBcRisk0state);
		bcListEntryArr8[(wsCount) - 1].setBcCompany0no(holdBcCompany0no);
		bcListEntryArr8[(wsCount) - 1].setBcAgency(holdBcAgency);
		bcListEntryArr8[(wsCount) - 1].setBcLine0bus(holdBcLine0bus);
		bcListEntryArr8[(wsCount) - 1].setBcRenewal0cd(holdBcRenewal0cd);
		bcListEntryArr8[(wsCount) - 1].setBcIssue0code(holdBcIssue0code);
		bcListEntryArr8[(wsCount) - 1].setBcPay0code(holdBcPay0code);
		bcListEntryArr8[(wsCount) - 1].setBcMode0code(holdBcMode0code);
		bcListEntryArr8[(wsCount) - 1].setBcSort0name(holdBcSort0name);
		bcListEntryArr8[(wsCount) - 1].setBcCust0no(holdBcCust0no);
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0a(holdBcSpec0use0a);
		bcListEntryArr8[(wsCount) - 1].setBcSpec0use0b(holdBcSpec0use0b);
		bcListEntryArr8[(wsCount) - 1].setBcCanceldate(holdBcCanceldate);
		bcListEntryArr8[(wsCount) - 1].setBcMrsseq(holdBcMrsseq);
		bcListEntryArr8[(wsCount) - 1].setBcTot0ag0prm(holdBcTot0ag0prm.clone());
		bcListEntryArr8[(wsCount) - 1].setBcLastname(holdBcLastname);
		bcListEntryArr8[(wsCount) - 1].setBcFirstname(holdBcFirstname);
		bcListEntryArr8[(wsCount) - 1].setBcMiddlename(holdBcMiddlename);
		bcListEntryArr8[(wsCount) - 1].setBcFedtaxid(holdBcFedtaxid);
		bcListEntryArr8[(wsCount) - 1].setBcGroupno(holdBcGroupno);
		bcListEntryArr8[(wsCount) - 1].setBcPhone1(holdBcPhone1);
		bcListEntryArr8[(wsCount) - 1].setBcSsn(holdBcSsn);
		bcListEntryArr8[(wsCount) - 1].setBcNametype(holdBcNametype);
		bcListEntryArr8[(wsCount) - 1].setBcNamestatus(holdBcNamestatus);
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line02(holdBcAdd0line02);
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line03(holdBcAddrln1);
		bcListEntryArr8[(wsCount) - 1].setBcAdd0line04(holdBcCity);
		bcListEntryArr8[(wsCount) - 1].setBcState(holdBcState);
		bcListEntryArr8[(wsCount) - 1].setBcZip0post(holdBcZipcode);
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '0') {
			bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Agent To Renew");
		} else {
			if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '1') {
				bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Computer Renew");
			} else {
				if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '2') {
					bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Home Office To renew");
				} else {
					if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '3') {
						bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Already Renewed");
					} else {
						if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '4') {
							bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
						} else {
							if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '7') {
								bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal With A Notice");
							} else {
								if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '8') {
									bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal Without A Notice");
								} else {
									if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9') {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("Cancelled Policy");
									} else {
										bcListEntryArr8[(wsCount) - 1].setBcRenewal0cdDesc("");
									}
								}
							}
						}
					}
				}
			}
		}
		// --------------*
		// READ PMSL0004 *
		// --------------*
		initPmsl0004Rec();
		wsReturnCode = "";
		wsControlSw = ' ';
		rec00fmtRec3.setSymbol(holdBcSymbol);
		rec00fmtRec3.setPolicy0num(holdBcPolicy0num);
		rec00fmtRec3.setModule(holdBcModule);
		rec00fmtRec3.setMaster0co(holdBcMaster0co);
		rec00fmtRec3.setLocation(holdBcLocation);
		wsControlSw = 'Y';
		try {
			call4();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASL000401") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		// If no record was returned then the policy is verified so
		// set the status on the activity record to a 'V'
		if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
			rec00fmtRec3.setTrans0stat(holdBcTrans0stat);
			rec00fmtRec3.setType0act(holdBcType0act);
		}
		bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Verified");
		if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("NB Pending");
				}
			}
			return;
		}
		if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("EN Pending");
				}
			}
			return;
		}
		bcListEntryArr8[(wsCount) - 1].setBcTrans0stat(rec00fmtRec3.getTrans0stat());
		if (bcListEntryArr8[(wsCount) - 1].getBcRenewal0cd() == '9' && bcListEntryArr8[(wsCount) - 1].getBcCanceldate().compareTo("") != 0) {
			if (bcListEntryArr8[(wsCount) - 1].getBcTrans0stat() == 'V') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('C');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("Cancelled");
				return;
			}
		}
		if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('1');
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("CN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('X');
				}
			}
			return;
		}
		if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('2');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RN Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('W');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Error");
				bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('3');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr8[(wsCount) - 1].setBcStatusDesc("RI Pending");
					bcListEntryArr8[(wsCount) - 1].setBcTrans0stat('I');
				}
			}
			return;
		}
	}

	public void conditionCheck() {
		// B0782 END-IF
		if (numOfEntrys > 1) {
			holdPreviousType = Functions.subString(bcListEntryArr8[(wsCount) - 1].getBcStatusDesc(), 1, 10);
			wsCounter = ((short) 1);
			// B0782 IF HOLD-PREVIOUS-TYPE = 'CN Pending'
			holdPreviousType = "";
			wsCounter = ((short) 1);
			while (!(wsCounter >= numOfEntrys)) {
				checkPending();
				wsCounter = ((short) (wsCounter + 1));
			}
		}
	}

	public void checkPending() {
		if (bcListEntryArr8[(wsCounter) - 1].getBcSymbol().compareTo(bcListEntryArr8[(numOfEntrys) - 1].getBcSymbol()) == 0
			&& bcListEntryArr8[(wsCounter) - 1].getBcPolicy0num().compareTo(bcListEntryArr8[(numOfEntrys) - 1].getBcPolicy0num()) == 0
			&& bcListEntryArr8[(wsCounter) - 1].getBcModule().compareTo(bcListEntryArr8[(numOfEntrys) - 1].getBcModule()) == 0
			&& bcListEntryArr8[(wsCounter) - 1].getBcMaster0co().compareTo(bcListEntryArr8[(numOfEntrys) - 1].getBcMaster0co()) == 0
			&& bcListEntryArr8[(wsCounter) - 1].getBcLocation().compareTo(bcListEntryArr8[(numOfEntrys) - 1].getBcLocation()) == 0) {
			wsCount = (wsCount - 1) % 100000;
			numOfEntrys = (numOfEntrys - 1) % 100000;
		}
	}

	public void exit1() {
	// exit
	}

	public void rng2000ReadPmsl0253() {
		String retcode = "";
		boolean goto2000ReadPmsl0253 = false;
		do {
			goto2000ReadPmsl0253 = false;
			retcode = readPmsl0253();
		} while (retcode.compareTo("2000-READ-PMSL0253") == 0);
		exitPmsl0253();
	}

	public void rng2000ReadPmsl0254() {
		boolean goto2000ReadPmsl0254 = false;
		String retcode = "";
		do {
			goto2000ReadPmsl0254 = false;
			retcode = readPmsl0254();
		} while (retcode.compareTo("2000-READ-PMSL0254") == 0);
		exitPmsl0254();
	}

	public void rng2000ReadBasclt0105() {
		boolean goto2000ReadPmsl0254 = false;
		boolean goto2000ExitPmsl0259 = false;
		String retcode = "";
		boolean goto2000ReadPmsl0253 = false;
		boolean goto2000ReadBasclt0105 = false;
		do {
			if (goto2000ExitPmsl0259) {
				goto2000ExitPmsl0259 = false;
				exitPmsl0259();
				chkSsn();
				exitChkSsn();
				chkIntType();
				exitChkIntType();
				chkIntName();
				exitChkIntName();
				readPmsj1201Int();
				exitReadPmsj1201Int();
				readPmsj1202Int();
				exitReadPmsj1202Int();
				readBasclt1502Int();
				exitReadBasclt1502Int();
				readPmsl0255();
				exitPmsl0255();
				readPmsl0256();
				exitPmsl0256();
				readPmsl0257();
				exitPmsl0257();
				readPmsl0258();
				exitPmsl0258();
				readPmsl0252();
				exitPmsl0252();
				readBasclt0103();
				exitBasclt0103();
				crefer0100SsnStart();
				crefer0100SsnExit();
				readBasclt0102();
				exitBasclt0102();
				crefer0100LnameStart();
				crefer0100LnameExit();
				readBasclt0106();
				exitBasclt0106();
				crefer0100FnameStart();
				crefer0100FnameExit();
				do {
					goto2000ReadPmsl0253 = false;
					retcode = readPmsl0253();
				} while (retcode.compareTo("2000-READ-PMSL0253") == 0);
				exitPmsl0253();
				do {
					goto2000ReadPmsl0254 = false;
					retcode = readPmsl0254();
				} while (retcode.compareTo("2000-READ-PMSL0254") == 0);
				exitPmsl0254();
				readBasclt0104();
				exitBasclt0104();
				crefer0100PhoneStart();
				crefer0100PhoneEnd();
			}
			goto2000ReadBasclt0105 = false;
			retcode = readBasclt0105();
			goto2000ExitPmsl0259 = retcode.compareTo("2000-EXIT-PMSL0259") == 0;
		} while (goto2000ExitPmsl0259);
		exitBasclt0105();
	}

	public SPResultSetTO runSP(Rskdbio021TO rskdbio021TO) {
		this.rskdbio021TO = rskdbio021TO;
		run();
		return resultSetTO;
	}

	public SPResultSetTO runSP(byte[] dynamic_inputParameters) {
		rskdbio021TO.setInputParameters(dynamic_inputParameters);
		return runSP(rskdbio021TO);
	}

	public Rskdbio021TO run(Rskdbio021TO rskdbio021TO) {
		this.rskdbio021TO = rskdbio021TO;
		run();
		return this.rskdbio021TO;
	}

	public void run(byte[] dynamic_inputParameters) {
		if (dynamic_inputParameters != null) {
			rskdbio021TO.setInputParameters(dynamic_inputParameters);
		}
		run(rskdbio021TO);
		if (dynamic_inputParameters != null) {
			DataConverter.arrayCopy(rskdbio021TO.getInputParameters(), rskdbio021TO.getInputParametersSize(), dynamic_inputParameters, 1);
		}
	}

	public void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		rec00fmtRec3.setRec00fmt(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public void call2() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		rec00fmtRec3.setRec00fmt(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public void call3() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		rec00fmtRec3.setRec00fmt(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public void call4() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		rec00fmtRec3.setRec00fmt(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public static Rskdbio021 getInstance() {
		Rskdbio021 iRskdbio021 = ((Rskdbio021) CalledProgList.getInst("RSKDBIO021"));
		if (iRskdbio021 == null) {
			// create an instance if needed
			iRskdbio021 = new Rskdbio021();
			CalledProgList.addInst("RSKDBIO021", ((Object) iRskdbio021));
		}
		return iRskdbio021;
	}

	public void run() {
		try {
			mainSubroutine();
		} catch (ReturnException re) {
			// normal termination of the program
		}
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	public int getBcListTableSize() {
		return 46258;
	}

	public byte[] getBcListTable() {
		byte[] buf = new byte[getBcListTableSize()];
		int offset = 1;
		for (int idx = 1; idx <= bcListEntryArr8.length; idx++) {
			DataConverter.arrayCopy(bcListEntryArr8[(idx) - 1].getData(), 1, bcListEntryArr8[(idx) - 1].getSize(), buf, offset);
			offset += bcListEntryArr8[(idx) - 1].getSize();
		}
		return buf;
	}

	public void initBcListTableSpaces() {
		for (int idx = 1; idx <= bcListEntryArr8.length; idx++) {
			bcListEntryArr8[(idx) - 1].initSpaces();
		}
	}

	public int getPmsl0004RecSize() {
		return 128;
	}

	public byte[] getPmsl0004Rec() {
		byte[] buf = new byte[getPmsl0004RecSize()];
		int offset = 1;
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), 1, rec00fmtRec3.getRec00fmtSize(), buf, offset);
		return buf;
	}

	public void initPmsl0004Rec() {
		rec00fmtRec3.initPmsl0004Record();
	}
}