package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Rskdbio028TO {
	private String inUserid = "";
	private String inSymbol = "";
	private String inPolicy0num = "";
	private String inModule = "";
	private String inName = "";
	private String inState = "";
	private String inLine0bus = "";
	private String inCust0no = "";
	private String inAdd0line04 = "";
	private String inZip0post = "";
	private String inSort0name = "";
	private String inLocation = "";
	private String inMaster0co = "";
	private String inAgency = "";
	private String inAddnlIntType = "";
	private String inAddnlIntName = "";
	private String inDbaname = "";
	private String inFein = "";
	private char inFeinIndSite = ' ';
	private char inFeinIndInt = ' ';
	private String inLossdte = "";
	private short inMaxrecords;

	public Rskdbio028TO(byte[] inputParameters) {
		basicInitialization();
		setInputParameters(inputParameters);
	}

	public Rskdbio028TO() {
		basicInitialization();
	}

	public int getInputParametersSize() {
		return 203;
	}

	public void setInputParameters(byte[] buf) {
		int offset = 1;
		inUserid = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolicy0num = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLine0bus = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inCust0no = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inAdd0line04 = DataConverter.readString(buf, offset, 28);
		offset += 28;
		inZip0post = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSort0name = DataConverter.readString(buf, offset, 4);
		offset += 4;
		inLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inMaster0co = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAgency = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inAddnlIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAddnlIntName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inDbaname = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inFein = DataConverter.readString(buf, offset, 9);
		offset += 9;
		inFeinIndSite = DataConverter.readChar(buf, offset);
		offset += 1;
		inFeinIndInt = DataConverter.readChar(buf, offset);
		offset += 1;
		inLossdte = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inMaxrecords = DataConverter.readShort(buf, offset, 3);
	}

	public byte[] getInputParameters() {
		byte[] buf = new byte[getInputParametersSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, inUserid, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, inState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inLine0bus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inCust0no, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inAdd0line04, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, inZip0post, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inSort0name, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, inLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inAgency, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inAddnlIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inAddnlIntName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, inDbaname, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, inFein, 9);
		offset += 9;
		DataConverter.writeChar(buf, offset, inFeinIndSite);
		offset += 1;
		DataConverter.writeChar(buf, offset, inFeinIndInt);
		offset += 1;
		DataConverter.writeString(buf, offset, inLossdte, 7);
		offset += 7;
		DataConverter.writeShort(buf, offset, inMaxrecords, 3);
		return buf;
	}

	public void basicInitialization() {}

	public short getInMaxrecords() {
		return this.inMaxrecords;
	}

	public String getInState() {
		return this.inState;
	}

	public String getInAdd0line04() {
		return this.inAdd0line04;
	}

	public String getInLine0bus() {
		return this.inLine0bus;
	}

	public String getInAgency() {
		return this.inAgency;
	}

	public String getInZip0post() {
		return this.inZip0post;
	}

	public String getInModule() {
		return this.inModule;
	}

	public String getInSymbol() {
		return this.inSymbol;
	}

	public String getInPolicy0num() {
		return this.inPolicy0num;
	}

	public String getInCust0no() {
		return this.inCust0no;
	}

	public String getInSort0name() {
		return this.inSort0name;
	}

	public String getInName() {
		return this.inName;
	}

	public String getInFein() {
		return this.inFein;
	}

	public String getInDbaname() {
		return this.inDbaname;
	}

	public String getInAddnlIntName() {
		return this.inAddnlIntName;
	}

	public String getInAddnlIntType() {
		return this.inAddnlIntType;
	}

	public String getInUserid() {
		return this.inUserid;
	}

	public void setInMaxrecords(short inMaxrecords) {
		this.inMaxrecords = inMaxrecords;
	}

	public String getInMaster0co() {
		return this.inMaster0co;
	}

	public String getInLocation() {
		return this.inLocation;
	}

	public char getInFeinIndSite() {
		return this.inFeinIndSite;
	}

	public char getInFeinIndInt() {
		return this.inFeinIndInt;
	}

	public String getInLossdte() {
		return inLossdte;
	}

	public void setInLossdte(String inLossdte) {
		this.inLossdte = inLossdte;
	}
}