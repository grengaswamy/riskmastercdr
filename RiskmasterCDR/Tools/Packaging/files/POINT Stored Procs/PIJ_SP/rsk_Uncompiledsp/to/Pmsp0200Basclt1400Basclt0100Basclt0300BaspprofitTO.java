// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DBAccessStatus;
import java.math.BigDecimal;


public class Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO {
	private DBAccessStatus dBAccessStatus;
	private char trans0Stat = ' ';
	private String effYr = "";
	private String effMo = "";
	private String effDa = "";
	private String expYr = "";
	private String expMo = "";	
	private String expDa = "";
	private String risk0state = "";
	private String company0no = "";
	private String fillr1 = "";
	private char rpt0agt0nr;
	private String fillr2 = "";
	private char fillr3 = ' ';
	private String fillr4 = "";
	private BigDecimal tot0ag0prm = BigDecimal.ZERO;
	private String line0bus = "";
	private char issue0code = ' ';
	private char pay0code = ' ';
	private char mode0code = ' ';
	private String sort0name = "";
	private char renewal0cd = ' ';
	private String cust0no = "";
	private String spec0use0a = "";
	private String spec0use0b = "";
	private String zip0post = "";
	private String add0line01 = "";
	private String add0line03 = "";
	private String add0line04 = "";
	private String type0act = "";
	private String canceldate = "";
	private String mrsseq = "";	
	private String location = "";
	private String masterCo = "";
	private String symbol = "";
	private String policy0Num = "";
	private String module = "";
	private long cltseqnum;
	private short addrseqnum;
	private String groupno = "";
	private char nametype;
	private char namestatus;
	private String clientname = "";
	private String firstname = "";
	private String midname = "";
	private String dba = "";
	private String phone1 = "";
	private String fedtaxid = "";
	private String ssn = "";
	private String addrln1 = "";
	private String city = "";
	private String state = "";
	private String zipcode = "";
	private String submissionId = "";
	
	public String getSubmissionId() {
		return submissionId;
	}
	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}
	public DBAccessStatus getDBAccessStatus() {
		return dBAccessStatus;
	}
	public void setDBAccessStatus(DBAccessStatus accessStatus) {
		dBAccessStatus = accessStatus;
	}
	public char getTrans0Stat() {
		return trans0Stat;
	}
	public void setTrans0Stat(char trans0Stat) {
		this.trans0Stat = trans0Stat;
	}
	public String getEffYr() {
		return effYr;
	}
	public void setEffYr(String effYr) {
		this.effYr = effYr;
	}
	public String getEffMo() {
		return effMo;
	}
	public void setEffMo(String effMo) {
		this.effMo = effMo;
	}
	public String getEffDa() {
		return effDa;
	}
	public void setEffDa(String effDa) {
		this.effDa = effDa;
	}
	public String getExpYr() {
		return expYr;
	}
	public void setExpYr(String expYr) {
		this.expYr = expYr;
	}
	public String getExpMo() {
		return expMo;
	}
	public void setExpMo(String expMo) {
		this.expMo = expMo;
	}
	public String getExpDa() {
		return expDa;
	}
	public void setExpDa(String expDa) {
		this.expDa = expDa;
	}
	public String getRisk0state() {
		return risk0state;
	}
	public void setRisk0state(String risk0state) {
		this.risk0state = risk0state;
	}
	public String getCompany0no() {
		return company0no;
	}
	public void setCompany0no(String company0no) {
		this.company0no = company0no;
	}
	public String getFillr1() {
		return fillr1;
	}
	public void setFillr1(String fillr1) {
		this.fillr1 = fillr1;
	}
	public char getRpt0agt0nr() {
		return rpt0agt0nr;
	}
	public void setRpt0agt0nr(char rpt0agt0nr) {
		this.rpt0agt0nr = rpt0agt0nr;
	}
	public String getFillr2() {
		return fillr2;
	}
	public void setFillr2(String fillr2) {
		this.fillr2 = fillr2;
	}
	public char getFillr3() {
		return fillr3;
	}
	public void setFillr3(char fillr3) {
		this.fillr3 = fillr3;
	}
	public String getFillr4() {
		return fillr4;
	}
	public void setFillr4(String fillr4) {
		this.fillr4 = fillr4;
	}
	public BigDecimal getTot0ag0prm() {
		return tot0ag0prm;
	}
	public void setTot0ag0prm(BigDecimal tot0ag0prm) {
		this.tot0ag0prm = tot0ag0prm;
	}
	public String getLine0bus() {
		return line0bus;
	}
	public void setLine0bus(String line0bus) {
		this.line0bus = line0bus;
	}
	public char getIssue0code() {
		return issue0code;
	}
	public void setIssue0code(char issue0code) {
		this.issue0code = issue0code;
	}
	public char getPay0code() {
		return pay0code;
	}
	public void setPay0code(char pay0code) {
		this.pay0code = pay0code;
	}
	public char getMode0code() {
		return mode0code;
	}
	public void setMode0code(char mode0code) {
		this.mode0code = mode0code;
	}
	public String getSort0name() {
		return sort0name;
	}
	public void setSort0name(String sort0name) {
		this.sort0name = sort0name;
	}
	public char getRenewal0cd() {
		return renewal0cd;
	}
	public void setRenewal0cd(char renewal0cd) {
		this.renewal0cd = renewal0cd;
	}
	public String getCust0no() {
		return cust0no;
	}
	public void setCust0no(String cust0no) {
		this.cust0no = cust0no;
	}
	public String getSpec0use0a() {
		return spec0use0a;
	}
	public void setSpec0use0a(String spec0use0a) {
		this.spec0use0a = spec0use0a;
	}
	public String getSpec0use0b() {
		return spec0use0b;
	}
	public void setSpec0use0b(String spec0use0b) {
		this.spec0use0b = spec0use0b;
	}
	public String getZip0post() {
		return zip0post;
	}
	public void setZip0post(String zip0post) {
		this.zip0post = zip0post;
	}
	public String getAdd0line01() {
		return add0line01;
	}
	public void setAdd0line01(String add0line01) {
		this.add0line01 = add0line01;
	}
	public String getAdd0line03() {
		return add0line03;
	}
	public void setAdd0line03(String add0line03) {
		this.add0line03 = add0line03;
	}
	public String getAdd0line04() {
		return add0line04;
	}
	public void setAdd0line04(String add0line04) {
		this.add0line04 = add0line04;
	}
	public String getType0act() {
		return type0act;
	}
	public void setType0act(String type0act) {
		this.type0act = type0act;
	}
	public String getCanceldate() {
		return canceldate;
	}
	public void setCanceldate(String canceldate) {
		this.canceldate = canceldate;
	}
	public String getMrsseq() {
		return mrsseq;
	}
	public void setMrsseq(String mrsseq) {
		this.mrsseq = mrsseq;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMasterCo() {
		return masterCo;
	}
	public void setMasterCo(String masterCo) {
		this.masterCo = masterCo;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getPolicy0Num() {
		return policy0Num;
	}
	public void setPolicy0Num(String policy0Num) {
		this.policy0Num = policy0Num;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public long getCltseqnum() {
		return cltseqnum;
	}
	public void setCltseqnum(long cltseqnum) {
		this.cltseqnum = cltseqnum;
	}
	public short getAddrseqnum() {
		return addrseqnum;
	}
	public void setAddrseqnum(short addrseqnum) {
		this.addrseqnum = addrseqnum;
	}
	public String getGroupno() {
		return groupno;
	}
	public void setGroupno(String groupno) {
		this.groupno = groupno;
	}
	public char getNametype() {
		return nametype;
	}
	public void setNametype(char nametype) {
		this.nametype = nametype;
	}
	public char getNamestatus() {
		return namestatus;
	}
	public void setNamestatus(char namestatus) {
		this.namestatus = namestatus;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMidname() {
		return midname;
	}
	public void setMidname(String midname) {
		this.midname = midname;
	}
	public String getDba() {
		return dba;
	}
	public void setDba(String dba) {
		this.dba = dba;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getFedtaxid() {
		return fedtaxid;
	}
	public void setFedtaxid(String fedtaxid) {
		this.fedtaxid = fedtaxid;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getAddrln1() {
		return addrln1;
	}
	public void setAddrln1(String addrln1) {
		this.addrln1 = addrln1;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}	
	
	}
