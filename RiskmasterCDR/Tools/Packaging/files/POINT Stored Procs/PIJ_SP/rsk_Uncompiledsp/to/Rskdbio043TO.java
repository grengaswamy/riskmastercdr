package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Rskdbio043TO {
	private String inLocation = "";
	private String inMaster0co = "";
	private String inSymbol = "";
	private String inPolicy0num = "";
	private String inModule = "";
	private String inLob = "";
	private String inPolco = "";
	private String inAgentNumber = "";
	private String inState = "";
	private String inTransmode = "";

	public Rskdbio043TO(byte[] inputParameters) {
		basicInitialization();
		setInputParameters(inputParameters);
	}

	public Rskdbio043TO() {
		basicInitialization();
	}

	public int getInputParametersSize() {
		return 32;
	}

	public void setInputParameters(byte[] buf) {
		int offset = 1;
		inLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inMaster0co = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolicy0num = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLob = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAgentNumber = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inTransmode = DataConverter.readString(buf, offset, 2);
	}

	public byte[] getInputParameters() {
		byte[] buf = new byte[getInputParametersSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, inLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inLob, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inPolco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inAgentNumber, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inTransmode, 2);
		return buf;
	}

	public void basicInitialization() {}

	public String getInMaster0co() {
		return this.inMaster0co;
	}

	public String getInLocation() {
		return this.inLocation;
	}

	public String getInSymbol() {
		return this.inSymbol;
	}

	public String getInPolicy0num() {
		return this.inPolicy0num;
	}

	public String getInModule() {
		return this.inModule;
	}

	public String getInPolco() {
		return this.inPolco;
	}

	public String getInState() {
		return this.inState;
	}

	public String getInLob() {
		return this.inLob;
	}
}