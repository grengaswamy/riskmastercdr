package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;


public class Rskdbio039TO {
	private String inUserid = "";
	private String inSymbol = "";
	private String inPolicy0num = "";
	private String inModule = "";
	private String inName = "";
	private String inState = "";
	private String inLine0bus = "";
	private String inCust0no = "";
	private String inAdd0Line04 = "";
	private String inZip0post = "";
	private String inSort0name = "";
	private String inLocation = "";
	private String inMaster0co = "";
	private String inAgency = "";
	private String inAddnlIntType = "";
	private String inAddnlIntName = "";
	private String inDbaName = "";
	private String inFein = "";
	private char inFeinIndSite = ' ';
	private char inFeinIndInt = ' ';
	private String inLegalAdd = "";
	private String inLaCity = "";
	private String inLaState = "";
	private String inLaZip = "";
	private String inMailIngAdd = "";
	private String inloanNum = "";
	private String inLossdte = "";
	private short inMaxrecords;

	public void setInMaxrecords(short inMaxrecords) {
		this.inMaxrecords = inMaxrecords;
	}

	public Rskdbio039TO(byte[] inputParameters) {
		basicInitialization();
		setInputParameters(inputParameters);
	}

	public Rskdbio039TO() {
		basicInitialization();
	}

	public int getInputParametersSize() {
		return 328;
	}

	public void setInputParameters(byte[] buf) {
		int offset = 1;
		inUserid = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolicy0num = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLine0bus = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inCust0no = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inAdd0Line04 = DataConverter.readString(buf, offset, 28);
		offset += 28;
		inZip0post = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSort0name = DataConverter.readString(buf, offset, 4);
		offset += 4;
		inLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inMaster0co = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAgency = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inAddnlIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAddnlIntName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inDbaName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inFein = DataConverter.readString(buf, offset, 9);
		offset += 9;
		inFeinIndSite = DataConverter.readChar(buf, offset);
		offset += 1;
		inFeinIndInt = DataConverter.readChar(buf, offset);
		offset += 1;
		inLegalAdd = DataConverter.readString(buf, offset,30);
		offset += 30;
		inLaCity = DataConverter.readString(buf, offset,28);
		offset += 28;
		inLaState = DataConverter.readString(buf, offset,2);
		offset += 2;
		inLaZip = DataConverter.readString(buf, offset,10);
		offset += 10;
		inMailIngAdd = DataConverter.readString(buf, offset,30);
		offset += 30;
		inloanNum = DataConverter.readString(buf, offset,25);
		offset += 25;
		inLossdte = DataConverter.readString(buf, offset,7);
		offset += 7;
		inMaxrecords = DataConverter.readShort(buf, offset, 3);
	}

	public byte[] getInputParameters() {
		byte[] buf = new byte[getInputParametersSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset,inUserid, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset,inPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset,inModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset,inState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inLine0bus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset,inCust0no, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inAdd0Line04, 28);
		offset += 28;
		DataConverter.writeString(buf, offset,inZip0post, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inSort0name, 4);
		offset += 4;
		DataConverter.writeString(buf, offset,inLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inAgency, 7);
		offset += 7;
		DataConverter.writeString(buf, offset,inAddnlIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inAddnlIntName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset,inDbaName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset,inFein, 9);
		offset += 9;
		DataConverter.writeChar(buf, offset,inFeinIndSite);
		offset += 1;
		DataConverter.writeChar(buf, offset,inFeinIndInt);
		offset += 1;
		DataConverter.writeString(buf, offset,inLegalAdd,30);
		offset += 30;
		DataConverter.writeString(buf, offset,inLaCity,28);
		offset += 28;
		DataConverter.writeString(buf, offset,inLaState,2);
		offset += 2;
		DataConverter.writeString(buf, offset,inLaZip,10);
		offset += 10;
		DataConverter.writeString(buf, offset,inMailIngAdd,30);
		offset += 30;
		DataConverter.writeString(buf, offset,inloanNum,25);
		offset += 25;
		DataConverter.writeString(buf, offset,inLossdte,7);
		offset += 7;
		DataConverter.writeShort(buf, offset,inMaxrecords, 3);		
		return buf;
	}

	public void basicInitialization() {}

	public String getInUserid() {
		return inUserid;
	}

	public String getInSymbol() {
		return inSymbol;
	}

	public String getInPolicy0num() {
		return inPolicy0num;
	}

	public String getInModule() {
		return inModule;
	}

	public String getInName() {
		return inName;
	}

	public String getInState() {
		return inState;
	}

	public String getInLine0bus() {
		return inLine0bus;
	}

	public String getInCust0no() {
		return inCust0no;
	}

	public String getInAdd0Line04() {
		return inAdd0Line04;
	}

	public String getInZip0post() {
		return inZip0post;
	}

	public String getInSort0name() {
		return inSort0name;
	}

	public String getInLocation() {
		return inLocation;
	}

	public String getInMaster0co() {
		return inMaster0co;
	}

	public String getInAgency() {
		return inAgency;
	}

	public String getInAddnlIntType() {
		return inAddnlIntType;
	}

	public String getInAddnlIntName() {
		return inAddnlIntName;
	}

	public String getInDbaName() {
		return inDbaName;
	}

	public String getInFein() {
		return inFein;
	}

	public char getInFeinIndSite() {
		return inFeinIndSite;
	}

	public char getInFeinIndInt() {
		return inFeinIndInt;
	}

	public String getInLegalAdd() {
		return inLegalAdd;
	}

	public String getInLaCity() {
		return inLaCity;
	}

	public String getInLaState() {
		return inLaState;
	}

	public String getInLaZip() {
		return inLaZip;
	}

	public String getInMailIngAdd() {
		return inMailIngAdd;
	}

	public String getInloanNum() {
		return inloanNum;
	}

	public short getInMaxrecords() {
		return inMaxrecords;
	}

	public String getInLossdte() {
		return inLossdte;
	}

	public void setInLossdte(String inLossdte) {
		this.inLossdte = inLossdte;
	}


}
