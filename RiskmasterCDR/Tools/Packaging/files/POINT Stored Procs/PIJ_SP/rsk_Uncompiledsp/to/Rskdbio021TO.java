package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Rskdbio021TO {
	private String inUserid = "";
	private String inSymbol = "";
	private String inPolicy0num = "";
	private String inModule = "";
	private String inLastname = "";
	private char inLnBwith = ' ';
	private String inFirstname = "";
	private char inFnBwith = ' ';
	private String inState = "";
	private String inLine0bus = "";
	private String inCust0no = "";
	private String inCity = "";
	private String inZip0post = "";
	private String inSort0name = "";
	private String inLocation = "";
	private String inMaster0co = "";
	private String inAgency = "";
	private String inPhone1 = "";
	private String inSsn = "";
	private String inGroupno = "";
	private String inAddnlIntType = "";
	private String inAddnlIntName = "";
	private char inSsnSiteInd = ' ';
	private char inSsnCltInd = ' ';
	private String inLossdte = "";
	private short inMaxrecords;

	public Rskdbio021TO(byte[] inputParameters) {
		basicInitialization();
		setInputParameters(inputParameters);
	}

	public Rskdbio021TO() {
		basicInitialization();
	}

	public int getInputParametersSize() {
		return 289;
	}

	public void setInputParameters(byte[] buf) {
		int offset = 1;
		inUserid = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolicy0num = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLastname = DataConverter.readString(buf, offset, 60);
		offset += 60;
		inLnBwith = DataConverter.readChar(buf, offset);
		offset += 1;
		inFirstname = DataConverter.readString(buf, offset, 40);
		offset += 40;
		inFnBwith = DataConverter.readChar(buf, offset);
		offset += 1;
		inState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLine0bus = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inCust0no = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		inZip0post = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSort0name = DataConverter.readString(buf, offset, 4);
		offset += 4;
		inLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inMaster0co = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAgency = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inPhone1 = DataConverter.readString(buf, offset, 32);
		offset += 32;
		inSsn = DataConverter.readString(buf, offset, 11);
		offset += 11;
		inGroupno = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inAddnlIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAddnlIntName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inSsnSiteInd = DataConverter.readChar(buf, offset);
		offset += 1;
		inSsnCltInd = DataConverter.readChar(buf, offset);
		offset += 1;
		inLossdte = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inMaxrecords = DataConverter.readShort(buf, offset, 3);
	}

	public byte[] getInputParameters() {
		byte[] buf = new byte[getInputParametersSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, inUserid, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inLastname, 60);
		offset += 60;
		DataConverter.writeChar(buf, offset, inLnBwith);
		offset += 1;
		DataConverter.writeString(buf, offset, inFirstname, 40);
		offset += 40;
		DataConverter.writeChar(buf, offset, inFnBwith);
		offset += 1;
		DataConverter.writeString(buf, offset, inState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inLine0bus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, inCust0no, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, inZip0post, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inSort0name, 4);
		offset += 4;
		DataConverter.writeString(buf, offset, inLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inAgency, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inPhone1, 32);
		offset += 32;
		DataConverter.writeString(buf, offset, inSsn, 11);
		offset += 11;
		DataConverter.writeString(buf, offset, inGroupno, 10);
		offset += 10;
		DataConverter.writeString(buf, offset, inAddnlIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, inAddnlIntName, 30);
		offset += 30;
		DataConverter.writeChar(buf, offset, inSsnSiteInd);
		offset += 1;
		DataConverter.writeChar(buf, offset, inSsnCltInd);
		offset += 1;
		DataConverter.writeString(buf, offset, inLossdte, 7);
		offset += 7;
		DataConverter.writeShort(buf, offset, inMaxrecords, 3);
		return buf;
	}

	public void basicInitialization() {}

	public short getInMaxrecords() {
		return this.inMaxrecords;
	}

	public String getInLine0bus() {
		return this.inLine0bus;
	}

	public String getInFirstname() {
		return this.inFirstname;
	}

	public String getInModule() {
		return this.inModule;
	}

	public String getInState() {
		return this.inState;
	}

	public String getInCity() {
		return this.inCity;
	}

	public String getInPolicy0num() {
		return this.inPolicy0num;
	}

	public String getInSymbol() {
		return this.inSymbol;
	}

	public String getInGroupno() {
		return this.inGroupno;
	}

	public String getInZip0post() {
		return this.inZip0post;
	}

	public String getInAgency() {
		return this.inAgency;
	}

	public String getInLastname() {
		return this.inLastname;
	}

	public String getInCust0no() {
		return this.inCust0no;
	}

	public String getInPhone1() {
		return this.inPhone1;
	}

	public String getInSsn() {
		return this.inSsn;
	}

	public String getInAddnlIntType() {
		return this.inAddnlIntType;
	}

	public String getInAddnlIntName() {
		return this.inAddnlIntName;
	}

	public String getInUserid() {
		return this.inUserid;
	}

	public void setInMaxrecords(short inMaxrecords) {
		this.inMaxrecords = inMaxrecords;
	}

	public String getInMaster0co() {
		return this.inMaster0co;
	}

	public String getInLocation() {
		return this.inLocation;
	}

	public char getInSsnCltInd() {
		return this.inSsnCltInd;
	}

	public char getInSsnSiteInd() {
		return this.inSsnSiteInd;
	}

	public String getInLossdte() {
		return inLossdte;
	}

	public void setInLossdte(String inLossdte) {
		this.inLossdte = inLossdte;
	}
}