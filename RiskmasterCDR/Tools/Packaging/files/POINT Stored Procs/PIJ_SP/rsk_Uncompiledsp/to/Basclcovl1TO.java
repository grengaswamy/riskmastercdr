package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Basclcovl1TO {
	
	private String location = "";
	private String masterco = "";
	private String symbol = "";
	private String policyno = "";
	private String module = "";
	private String unitno = "";
	private String locno = "";
	private String sublocno = "";
	private String lossDte = "";
	private String inslineDesc = "";
	private String lnkProduct = "";
	//FSIT# 189295 Resolution# 71716 - Start
	private String lnkSwitch = "";
	//FSIT# 189295 Resolution# 71716 - End
	
		public Basclcovl1TO(byte[] claimData) {
		basicInitialization();
		setClaimData(claimData);
	}

	public Basclcovl1TO() {
		basicInitialization();
	}

	public int getClaimDataSize() {
		//FSIT# 189295 Resolution# 71716 - Start
		//return 74;
		return 75;
		//FSIT# 189295 Resolution# 71716 - End
	}

	public void setClaimData(byte[] buf) {
		int offset = 1;
		location = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		symbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		policyno = DataConverter.readString(buf, offset, 7);
		offset += 7;
		module = DataConverter.readString(buf, offset, 2);
		offset += 2;
		unitno = DataConverter.readString(buf, offset, 5);
		offset += 5;
		locno = DataConverter.readString(buf, offset, 5);
		offset += 5;
		sublocno = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lossDte = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inslineDesc = DataConverter.readString(buf, offset, 30);
		offset += 30;
		lnkProduct = DataConverter.readString(buf, offset, 6);
		offset += 6;
		//FSIT# 189295 Resolution# 71716 - Start
		lnkSwitch = DataConverter.readString(buf, offset, 1);
		offset += 1;
		//FSIT# 189295 Resolution# 71716 - End
	}

	

	
	public byte[] getClaimData() {
		byte[] buf = new byte[getClaimDataSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, symbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, policyno, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, module, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, unitno, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, locno, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, sublocno, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lossDte, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, inslineDesc, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, lnkProduct, 6);
		offset += 6;
		//FSIT# 189295 Resolution# 71716 - Start
		DataConverter.writeString(buf, offset, lnkSwitch, 1);
		offset += 1;
		//FSIT# 189295 Resolution# 71716 - End
		return buf;
	}

	public void basicInitialization() {}


	public String getLocation() {
		return this.location;
	}

	public String getModule() {
		return this.module;
	}

	public String getSymbol() {
		return this.symbol;
	}

	public String getMasterco() {
		return this.masterco;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public String getUnitno() {
		return unitno;
	}

	public void setUnitno(String unitno) {
		this.unitno = unitno;
	}

	public String getLocno() {
		return locno;
	}

	public void setLocno(String locno) {
		this.locno = locno;
	}

	public String getSublocno() {
		return sublocno;
	}

	public void setSublocno(String sublocno) {
		this.sublocno = sublocno;
	}

	public String getLossDte() {
		return lossDte;
	}

	public void setLossDte(String lossDte) {
		this.lossDte = lossDte;
	}

	public String getInslineDesc() {
		return inslineDesc;
	}

	public void setInslineDesc(String inslineDesc) {
		this.inslineDesc = inslineDesc;
	}

	public String getLnkProduct() {
		return lnkProduct;
	}

	public void setLnkProduct(String lnkProduct) {
		this.lnkProduct = lnkProduct;
	}
	
	//FSIT# 189295 Resolution# 71716 - Start
	public String getLnkSwitch() {
		return lnkSwitch;
	}

	public void setLnkSwitch(String lnkSwitch) {
		this.lnkSwitch = lnkSwitch;
	}
	//FSIT# 189295 Resolution# 71716 - End


}
