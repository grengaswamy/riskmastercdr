package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Rskclunit1TO {
	private String location = "";
	private String masterco = "";
	private String symbol = "";
	private String policyno = "";
	private String module = "";

	public Rskclunit1TO(byte[] claimData) {
		basicInitialization();
		setClaimData(claimData);
	}

	public Rskclunit1TO() {
		basicInitialization();
	}

	public int getClaimDataSize() {
		return 16;
	}

	public void setClaimData(byte[] buf) {
		int offset = 1;
		location = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		symbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		policyno = DataConverter.readString(buf, offset, 7);
		offset += 7;
		module = DataConverter.readString(buf, offset, 2);
	}

	public byte[] getClaimData() {
		byte[] buf = new byte[getClaimDataSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, symbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, policyno, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, module, 2);
		return buf;
	}

	public void basicInitialization() {}


	public String getSymbol() {
		return this.symbol;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public String getModule() {
		return this.module;
	}

	public String getLocation() {
		return this.location;
	}

	public String getMasterco() {
		return this.masterco;
	}
}