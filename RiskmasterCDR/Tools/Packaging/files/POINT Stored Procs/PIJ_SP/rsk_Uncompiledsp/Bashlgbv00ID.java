// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.id;

import java.io.Serializable;

import bphx.c2ab.hibernate.NotNaN;

public class Bashlgbv00ID implements Serializable {
	private static final long serialVersionUID = 6371996070597176261L;
	private String location = "";
	private String masterco = "";
	private String symbol = "";
	private String policyno = "";
	private String module = "";
	@NotNaN
	private int logseqnum;
	private int riskloc;
	private int risksubloc;
	private char recstatus;

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMasterco() {
		return this.masterco;
	}

	public void setMasterco(String masterco) {
		this.masterco = masterco;
	}

	public String getSymbol() {
		return this.symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public int getLogseqnum() {
		return this.logseqnum;
	}

	public void setLogseqnum(int logseqnum) {
		this.logseqnum = logseqnum;
	}

	public int getRiskloc() {
		return riskloc;
	}

	public void setRiskloc(int riskloc) {
		this.riskloc = riskloc;
	}

	public int getRisksubloc() {
		return risksubloc;
	}

	public void setRisksubloc(int risksubloc) {
		this.risksubloc = risksubloc;
	}
	
	public char getRecstatus() {
		return recstatus;
	}

	public void setRecstatus(char recstatus) {
		this.recstatus = recstatus;
	}
}
