// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.to;

import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;
import java.math.BigDecimal;
import com.csc.pt.svc.data.id.Bashlgbj00ID;

public class Bashlgbj00TO {
	private DBAccessStatus dBAccessStatus;
	private String loguser = "";
	private ABODate logdate = new ABODate();
	private ABOTime logtime = new ABOTime();
	private char bjc7st;
	private int bjandt;
	private short bjafnb;
	private int bjaldt;
	private int bjafdt;
	private String bjtype0act = "";
	private String bjbatx = "";
	private char bjdzst;
	private char bjd0st;
	private char bjd2st;
	private char bjd3st;
	
	private BigDecimal bja3va = BigDecimal.ZERO;
	private BigDecimal bjbrva = BigDecimal.ZERO;
	private BigDecimal bjbsva = BigDecimal.ZERO;
	private BigDecimal bjbtva = BigDecimal.ZERO;
	private BigDecimal bjaupc = BigDecimal.ZERO;
	private String bjaqcd = "";
	private String bjarcd = "";
	private String bjascd = "";
	private String bjeetx = "";
	private String bjbccd = "";
	private String bjagnb = "";
	private short bjahnb;
	private String bjaxtx = "";
	private String bjaytx = "";
	private String bjaztx = "";
	private char bjacst;
	private char bjadst;
	private BigDecimal bjainb = BigDecimal.ZERO;
	private String bjajnb = "";
	private int bjaknb;
	private int bjamnb;
	private int bjannb;
	private int bjaonb;
	private String bjavcd = "";
	private String bjawcd = "";
	private String bja1tx = "";
	private String bjapnb = "";
	private int bjasnb;
	private int bjatnb;
	private int bjaedt;
	private int bjavnb;
	private int bjaxnb;
	private BigDecimal bjaznb = BigDecimal.ZERO;
	private String bja2tx = "";
	private int bja9tx;
	private int bjbenb;
	private char bjcxst;
	private char bji6tx;
	private char bjcyst;
	private char bjc5st;
	private BigDecimal bjappc = BigDecimal.ZERO;
	private BigDecimal bjaqpc = BigDecimal.ZERO;
	private BigDecimal bjarpc = BigDecimal.ZERO;
	private String bja5cd = "";
	private int bjcenb;
	private int bjcfnb;
	private char bjdvst;
	private char bjdwst;
	private char bjdxst;
	private char bjdyst;
	private char bjczst;
	private char bjc0st;
	private char bjc1st;
	private char bjc2st;
	private char bjiytx;
	private char bjiztx;
	private char bji0tx;
	private char bji1tx;
	private char bji2tx;
	private char bji3tx;
	private char bji4tx;
	private char bji5tx;
	private String bjakcd = "";
	private String bjalcd = "";
	private String bjamcd = "";
	private String bjancd = "";
	private BigDecimal bjaepc = BigDecimal.ZERO;
	private BigDecimal bjafpc = BigDecimal.ZERO;
	private int bjbknb;
	private int bjblnb;
	private BigDecimal bjagva = BigDecimal.ZERO;
	private BigDecimal bjahva = BigDecimal.ZERO;
	private String bjbjcd = "";
	private String bjbkcd = "";
	private int bjesnb;
	private Bashlgbj00ID id = new Bashlgbj00ID();
	

	public DBAccessStatus getDBAccessStatus() {
		return this.dBAccessStatus;
	}

	public void setDBAccessStatus(DBAccessStatus dBAccessStatus) {
		this.dBAccessStatus = dBAccessStatus;
	}

	public String getLoguser() {
		return this.loguser;
	}

	public void setLoguser(String loguser) {
		this.loguser = loguser;
	}

	public ABODate getLogdate() {
		return this.logdate.clone();
	}

	public void setLogdate(ABODate logdate) {
		this.logdate.assign(logdate);
	}

	public ABOTime getLogtime() {
		return this.logtime.clone();
	}

	public void setLogtime(ABOTime logtime) {
		this.logtime.assign(logtime);
	}

	public Bashlgbj00ID getId() {
		return this.id;
	}

	public void setId(Bashlgbj00ID id) {
		this.id = id;
	}


	public int getLogseqnum() {
		return this.id.getLogseqnum();
	}

	public void setLogseqnum(int logseqnum) {
		this.id.setLogseqnum(logseqnum);
	}

	public String getLocation() {
		return this.id.getLocation();
	}

	public void setLocation(String location) {
		this.id.setLocation(location);
	}

	public String getMasterco() {
		return this.id.getMasterco();
	}

	public void setMasterco(String masterco) {
		this.id.setMasterco(masterco);
	}

	public String getSymbol() {
		return this.id.getSymbol();
	}

	public void setSymbol(String symbol) {
		this.id.setSymbol(symbol);
	}

	public String getPolicyno() {
		return this.id.getPolicyno();
	}

	public void setPolicyno(String policyno) {
		this.id.setPolicyno(policyno);
	}

	public String getModule() {
		return this.id.getModule();
	}

	public void setModule(String module) {
		this.id.setModule(module);
	}

	public String getInsline() {
		return this.id.getInsline();
	}

	public void setInsline(String insline) {
		this.id.setInsline(insline);
	}

	public int getRiskloc() {
		return this.id.getRiskloc();
	}

	public void setRiskloc(int riskloc) {
		this.id.setRiskloc(riskloc);
	}

	public int getRisksubloc() {
		return this.id.getRisksubloc();
	}

	public void setRisksubloc(int risksubloc) {
		this.id.setRisksubloc(risksubloc);
	}

	public String getProduct() {
		return this.id.getProduct();
	}

	public void setProduct(String product) {
		this.id.setProduct(product);
	}

	public int getUnitno() {
		return this.id.getUnitNo();
	}

	public void setUnitno(int unitNo) {
		this.id.setUnitNo(unitNo);
	}

	public char getRecstatus() {
		return this.id.getRecstatus();
	}

	public void setRecstatus(char recstatus) {
		this.id.setRecstatus(recstatus);
	}

	public char getBjc7st() {
		return this.bjc7st;
	}

	public void setBjc7st(char bjc7st) {
		this.bjc7st = bjc7st;
	}

	public int getBjandt() {
		return this.bjandt;
	}

	public void setBjandt(int bjandt) {
		this.bjandt = bjandt;
	}

	public short getBjafnb() {
		return this.bjafnb;
	}

	public void setBjafnb(short bjafnb) {
		this.bjafnb = bjafnb;
	}

	public int getBjaldt() {
		return this.bjaldt;
	}

	public void setBjaldt(int bjaldt) {
		this.bjaldt = bjaldt;
	}

	public int getBjafdt() {
		return this.bjafdt;
	}

	public void setBjafdt(int bjafdt) {
		this.bjafdt = bjafdt;
	}

	public String getBjtype0act() {
		return this.bjtype0act;
	}

	public void setBjtype0act(String bjtype0act) {
		this.bjtype0act = bjtype0act;
	}

	public String getBjbatx() {
		return this.bjbatx;
	}

	public void setBjbatx(String bjbatx) {
		this.bjbatx = bjbatx;
	}

	public char getBjdzst() {
		return this.bjdzst;
	}

	public void setBjdzst(char bjdzst) {
		this.bjdzst = bjdzst;
	}

	public char getBjd0st() {
		return this.bjd0st;
	}

	public void setBjd0st(char bjd0st) {
		this.bjd0st = bjd0st;
	}

	public char getBjd2st() {
		return this.bjd2st;
	}

	public void setBjd2st(char bjd2st) {
		this.bjd2st = bjd2st;
	}

	public char getBjd3st() {
		return this.bjd3st;
	}

	public void setBjd3st(char bjd3st) {
		this.bjd3st = bjd3st;
	}

	public BigDecimal getBja3va() {
		return this.bja3va;
	}

	public void setBja3va(BigDecimal bja3va) {
		this.bja3va = bja3va;
	}

	public BigDecimal getBjbrva() {
		return this.bjbrva;
	}

	public void setBjbrva(BigDecimal bjbrva) {
		this.bjbrva = bjbrva;
	}

	public BigDecimal getBjbsva() {
		return this.bjbsva;
	}

	public void setBjbsva(BigDecimal bjbsva) {
		this.bjbsva = bjbsva;
	}

	public BigDecimal getBjbtva() {
		return this.bjbtva;
	}

	public void setBjbtva(BigDecimal bjbtva) {
		this.bjbtva = bjbtva;
	}

	public BigDecimal getBjaupc() {
		return this.bjaupc;
	}

	public void setBjaupc(BigDecimal bjaupc) {
		this.bjaupc = bjaupc;
	}

	public String getBjaqcd() {
		return this.bjaqcd;
	}

	public void setBjaqcd(String bjaqcd) {
		this.bjaqcd = bjaqcd;
	}

	public String getBjarcd() {
		return this.bjarcd;
	}

	public void setBjarcd(String bjarcd) {
		this.bjarcd = bjarcd;
	}

	public String getBjascd() {
		return this.bjascd;
	}

	public void setBjascd(String bjascd) {
		this.bjascd = bjascd;
	}

	public String getBjeetx() {
		return this.bjeetx;
	}

	public void setBjeetx(String bjeetx) {
		this.bjeetx = bjeetx;
	}

	public String getBjbccd() {
		return this.bjbccd;
	}

	public void setBjbccd(String bjbccd) {
		this.bjbccd = bjbccd;
	}

	public String getBjagnb() {
		return this.bjagnb;
	}

	public void setBjagnb(String bjagnb) {
		this.bjagnb = bjagnb;
	}

	public short getBjahnb() {
		return this.bjahnb;
	}

	public void setBjahnb(short bjahnb) {
		this.bjahnb = bjahnb;
	}

	public String getBjaxtx() {
		return this.bjaxtx;
	}

	public void setBjaxtx(String bjaxtx) {
		this.bjaxtx = bjaxtx;
	}

	public String getBjaytx() {
		return this.bjaytx;
	}

	public void setBjaytx(String bjaytx) {
		this.bjaytx = bjaytx;
	}

	public String getBjaztx() {
		return this.bjaztx;
	}

	public void setBjaztx(String bjaztx) {
		this.bjaztx = bjaztx;
	}

	public char getBjacst() {
		return this.bjacst;
	}

	public void setBjacst(char bjacst) {
		this.bjacst = bjacst;
	}

	public char getBjadst() {
		return this.bjadst;
	}

	public void setBjadst(char bjadst) {
		this.bjadst = bjadst;
	}

	public BigDecimal getBjainb() {
		return this.bjainb;
	}

	public void setBjainb(BigDecimal bjainb) {
		this.bjainb = bjainb;
	}

	public String getBjajnb() {
		return this.bjajnb;
	}

	public void setBjajnb(String bjajnb) {
		this.bjajnb = bjajnb;
	}

	public int getBjaknb() {
		return this.bjaknb;
	}

	public void setBjaknb(int bjaknb) {
		this.bjaknb = bjaknb;
	}

	public int getBjamnb() {
		return this.bjamnb;
	}

	public void setBjamnb(int bjamnb) {
		this.bjamnb = bjamnb;
	}

	public int getBjannb() {
		return this.bjannb;
	}

	public void setBjannb(int bjannb) {
		this.bjannb = bjannb;
	}

	public int getBjaonb() {
		return this.bjaonb;
	}

	public void setBjaonb(int bjaonb) {
		this.bjaonb = bjaonb;
	}

	public String getBjavcd() {
		return this.bjavcd;
	}

	public void setBjavcd(String bjavcd) {
		this.bjavcd = bjavcd;
	}

	public String getBjawcd() {
		return this.bjawcd;
	}

	public void setBjawcd(String bjawcd) {
		this.bjawcd = bjawcd;
	}

	public String getBja1tx() {
		return this.bja1tx;
	}

	public void setBja1tx(String bja1tx) {
		this.bja1tx = bja1tx;
	}

	public String getBjapnb() {
		return this.bjapnb;
	}

	public void setBjapnb(String bjapnb) {
		this.bjapnb = bjapnb;
	}

	public int getBjasnb() {
		return this.bjasnb;
	}

	public void setBjasnb(int bjasnb) {
		this.bjasnb = bjasnb;
	}

	public int getBjatnb() {
		return this.bjatnb;
	}

	public void setBjatnb(int bjatnb) {
		this.bjatnb = bjatnb;
	}

	public int getBjaedt() {
		return this.bjaedt;
	}

	public void setBjaedt(int bjaedt) {
		this.bjaedt = bjaedt;
	}

	public int getBjavnb() {
		return this.bjavnb;
	}

	public void setBjavnb(int bjavnb) {
		this.bjavnb = bjavnb;
	}

	public int getBjaxnb() {
		return this.bjaxnb;
	}

	public void setBjaxnb(int bjaxnb) {
		this.bjaxnb = bjaxnb;
	}

	public BigDecimal getBjaznb() {
		return this.bjaznb;
	}

	public void setBjaznb(BigDecimal bjaznb) {
		this.bjaznb = bjaznb;
	}

	public String getBja2tx() {
		return this.bja2tx;
	}

	public void setBja2tx(String bja2tx) {
		this.bja2tx = bja2tx;
	}

	public int getBja9tx() {
		return this.bja9tx;
	}

	public void setBja9tx(int bja9tx) {
		this.bja9tx = bja9tx;
	}

	public int getBjbenb() {
		return this.bjbenb;
	}

	public void setBjbenb(int bjbenb) {
		this.bjbenb = bjbenb;
	}

	public char getBjcxst() {
		return this.bjcxst;
	}

	public void setBjcxst(char bjcxst) {
		this.bjcxst = bjcxst;
	}

	public char getBji6tx() {
		return this.bji6tx;
	}

	public void setBji6tx(char bji6tx) {
		this.bji6tx = bji6tx;
	}

	public char getBjcyst() {
		return this.bjcyst;
	}

	public void setBjcyst(char bjcyst) {
		this.bjcyst = bjcyst;
	}

	public char getBjc5st() {
		return this.bjc5st;
	}

	public void setBjc5st(char bjc5st) {
		this.bjc5st = bjc5st;
	}

	public BigDecimal getBjappc() {
		return this.bjappc;
	}

	public void setBjappc(BigDecimal bjappc) {
		this.bjappc = bjappc;
	}

	public BigDecimal getBjaqpc() {
		return this.bjaqpc;
	}

	public void setBjaqpc(BigDecimal bjaqpc) {
		this.bjaqpc = bjaqpc;
	}

	public BigDecimal getBjarpc() {
		return this.bjarpc;
	}

	public void setBjarpc(BigDecimal bjarpc) {
		this.bjarpc = bjarpc;
	}

	public String getBja5cd() {
		return this.bja5cd;
	}

	public void setBja5cd(String bja5cd) {
		this.bja5cd = bja5cd;
	}

	public int getBjcenb() {
		return this.bjcenb;
	}

	public void setBjcenb(int bjcenb) {
		this.bjcenb = bjcenb;
	}

	public int getBjcfnb() {
		return this.bjcfnb;
	}

	public void setBjcfnb(int bjcfnb) {
		this.bjcfnb = bjcfnb;
	}

	public char getBjdvst() {
		return this.bjdvst;
	}

	public void setBjdvst(char bjdvst) {
		this.bjdvst = bjdvst;
	}

	public char getBjdwst() {
		return this.bjdwst;
	}

	public void setBjdwst(char bjdwst) {
		this.bjdwst = bjdwst;
	}

	public char getBjdxst() {
		return this.bjdxst;
	}

	public void setBjdxst(char bjdxst) {
		this.bjdxst = bjdxst;
	}

	public char getBjdyst() {
		return this.bjdyst;
	}

	public void setBjdyst(char bjdyst) {
		this.bjdyst = bjdyst;
	}

	public char getBjczst() {
		return this.bjczst;
	}

	public void setBjczst(char bjczst) {
		this.bjczst = bjczst;
	}

	public char getBjc0st() {
		return this.bjc0st;
	}

	public void setBjc0st(char bjc0st) {
		this.bjc0st = bjc0st;
	}

	public char getBjc1st() {
		return this.bjc1st;
	}

	public void setBjc1st(char bjc1st) {
		this.bjc1st = bjc1st;
	}

	public char getBjc2st() {
		return this.bjc2st;
	}

	public void setBjc2st(char bjc2st) {
		this.bjc2st = bjc2st;
	}

	public char getBjiytx() {
		return this.bjiytx;
	}

	public void setBjiytx(char bjiytx) {
		this.bjiytx = bjiytx;
	}

	public char getBjiztx() {
		return this.bjiztx;
	}

	public void setBjiztx(char bjiztx) {
		this.bjiztx = bjiztx;
	}

	public char getBji0tx() {
		return this.bji0tx;
	}

	public void setBji0tx(char bji0tx) {
		this.bji0tx = bji0tx;
	}

	public char getBji1tx() {
		return this.bji1tx;
	}

	public void setBji1tx(char bji1tx) {
		this.bji1tx = bji1tx;
	}

	public char getBji2tx() {
		return this.bji2tx;
	}

	public void setBji2tx(char bji2tx) {
		this.bji2tx = bji2tx;
	}

	public char getBji3tx() {
		return this.bji3tx;
	}

	public void setBji3tx(char bji3tx) {
		this.bji3tx = bji3tx;
	}

	public char getBji4tx() {
		return this.bji4tx;
	}

	public void setBji4tx(char bji4tx) {
		this.bji4tx = bji4tx;
	}

	public char getBji5tx() {
		return this.bji5tx;
	}

	public void setBji5tx(char bji5tx) {
		this.bji5tx = bji5tx;
	}

	public String getBjakcd() {
		return this.bjakcd;
	}

	public void setBjakcd(String bjakcd) {
		this.bjakcd = bjakcd;
	}

	public String getBjalcd() {
		return this.bjalcd;
	}

	public void setBjalcd(String bjalcd) {
		this.bjalcd = bjalcd;
	}

	public String getBjamcd() {
		return this.bjamcd;
	}

	public void setBjamcd(String bjamcd) {
		this.bjamcd = bjamcd;
	}

	public String getBjancd() {
		return this.bjancd;
	}

	public void setBjancd(String bjancd) {
		this.bjancd = bjancd;
	}

	public BigDecimal getBjaepc() {
		return this.bjaepc;
	}

	public void setBjaepc(BigDecimal bjaepc) {
		this.bjaepc = bjaepc;
	}

	public BigDecimal getBjafpc() {
		return this.bjafpc;
	}

	public void setBjafpc(BigDecimal bjafpc) {
		this.bjafpc = bjafpc;
	}

	public int getBjbknb() {
		return this.bjbknb;
	}

	public void setBjbknb(int bjbknb) {
		this.bjbknb = bjbknb;
	}

	public int getBjblnb() {
		return this.bjblnb;
	}

	public void setBjblnb(int bjblnb) {
		this.bjblnb = bjblnb;
	}

	public BigDecimal getBjagva() {
		return this.bjagva;
	}

	public void setBjagva(BigDecimal bjagva) {
		this.bjagva = bjagva;
	}

	public BigDecimal getBjahva() {
		return this.bjahva;
	}

	public void setBjahva(BigDecimal bjahva) {
		this.bjahva = bjahva;
	}

	public String getBjbjcd() {
		return this.bjbjcd;
	}

	public void setBjbjcd(String bjbjcd) {
		this.bjbjcd = bjbjcd;
	}

	public String getBjbkcd() {
		return this.bjbkcd;
	}

	public void setBjbkcd(String bjbkcd) {
		this.bjbkcd = bjbkcd;
	}

	public int getBjesnb() {
		return this.bjesnb;
	}

	public void setBjesnb(int bjesnb) {
		this.bjesnb = bjesnb;
	}
}
