'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 2007
'
' NAME: VersioningFunctions
'
' AUTHOR: Computer Sciences Corporation , CSC
' DATE  : 11/7/2008
'
' COMMENT: Contains functions used for versioning builds and packages
'
'==========================================================================
Function GetVersionInfo(dblProductVersion)
	Dim arrVersionInfo
	Dim intMajorBuildNumber, intMinorBuildNumber, intRevisionNumber
	
	arrVersionInfo = Split(dblProductVersion, ".")
	
	intMajorBuildNumber = arrVersionInfo(0)
	intMinorBuildNumber = arrVersionInfo(1)
	intRevisionNumber = arrVersionInfo(2)
	
	GetVersionInfo = arrVersionInfo
End Function

Function GetFinalVersion(intMajorNumber, intMinorNumber, intRevisionNumber)
	Dim arrVersionInfo(2)
	
	arrVersionInfo(0) = intMajorNumber
	arrVersionInfo(1) = intMinorNumber
	arrVersionInfo(2) = intRevisionNumber
	
	GetFinalVersion = Join(arrVersionInfo, ".")
End Function

Function GetMajorBuildNumber(dblProductVersion)
	Dim arrVersionInfo
	Dim intMajorBuildNumber
	
	arrVersionInfo = GetVersionInfo(dblProductVersion)
		
	intMajorBuildNumber = arrVersionInfo(0)
	
	GetMajorBuildNumber = intMajorBuildNumber
End Function

Function GetMinorBuildNumber(dblProductVersion)
	Dim arrVersionInfo
	Dim intMinorBuildNumber
	
	arrVersionInfo = GetVersionInfo(dblProductVersion)
		
	intMinorBuildNumber = arrVersionInfo(1)
	
	GetMinorBuildNumber = intMinorBuildNumber
End Function

Function GetRevisionNumber(dblProductVersion)
	Dim arrVersionInfo
	Dim intRevisionNumber
	
	arrVersionInfo = GetVersionInfo(dblProductVersion)
		
	intRevisionNumber = arrVersionInfo(2)
	
	GetRevisionNumber = intRevisionNumber
End Function

Function IncrementMajorBuildNumber(dblProductVersion)
	Dim intMajorBuildNumber
	
	intMajorBuildNumber = GetMajorBuildNumber(dblProductVersion)
	
	IncrementMajorBuildNumber = CInt(intMajorBuildNumber) + 1
End Function

Function IncrementMinorBuildNumber(dblProductVersion)
	Dim intMinorBuildNumber
	
	intMinorBuildNumber = GetMinorBuildNumber(dblProductVersion) 
	
	IncrementMinorBuildNumber = CInt(intMinorBuildNumber) + 1
End Function

Function IncrementRevisionNumber(dblProductVersion)
	Dim intRevisionNumber
	
	intRevisionNumber = GetRevisionNumber(dblProductVersion)

	IncrementRevisionNumber = dblProductVersion + 1
End Function

