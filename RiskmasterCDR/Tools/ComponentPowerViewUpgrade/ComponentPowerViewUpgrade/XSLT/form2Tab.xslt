<!--<?xml version='1.0'?>-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="ISO-8859-1" />
<xsl:template match="/">

<html xmlns="http://www.w3.org/1999/xhtml" ><head runat="server"><title><xsl:apply-templates select="form"/></title>
	<link rel="stylesheet" href="RMNet.css" type="text/css" />
	<script language="JavaScript" SRC="form.js"></script>
	<script language="JavaScript" SRC="drift.js"></script>
	<xsl:if test="form/@includefilename"><script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="form/@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script></xsl:if>	
</head> 
	 <body class="10pt">
		<xsl:attribute name="onload"><xsl:choose><xsl:when test="form/@onload"><xsl:value-of select="form/@onload"/></xsl:when><xsl:otherwise>pageLoaded();</xsl:otherwise></xsl:choose></xsl:attribute>
		<form name="frmData" runat="server">
		<xsl:attribute name="action"><xsl:choose><xsl:when test="form/@postpage"><xsl:value-of select="form/@postpage"/></xsl:when><xsl:otherwise>forms.asp</xsl:otherwise></xsl:choose></xsl:attribute>
		<input type="hidden" name="sysCmd" />
		<input type="hidden" name="sysCmdConfirmSave" value="0"/>
		<input type="hidden" name="hTabName" />		
		<xsl:if test="form/toolbar">
			<div id="toolbardrift" name="toolbardrift" class="toolbardrift">
				<table border="0" CLASS="toolbar" CELLPADDING="0" CELLSPACING="0"><tr>
				<xsl:apply-templates select="form/toolbar/button" />
				</tr></table>
			</div>
			<br />
		</xsl:if>
		<div class="msgheader" id="formtitle"><xsl:value-of select="form/@title"/> <xsl:value-of select="form/@subtitle"/></div>
		<DIV class="errtextheader"><xsl:value-of select="form/demo/@title"/></DIV>
		<table border="0">
		<tr><td>
		<xsl:if test="form/errors">
			<br />
			<div class="errtextheader">Following errors were reported:</div>
			<ul>
				<xsl:apply-templates select="form/errors/error" />	
			</ul>
		</xsl:if>
		<xsl:if test="form/alerts">
			<br />
			<div class="errtextheader">Following alerts were reported:</div>
			<ul>
				<xsl:apply-templates select="form/alerts/error" />	
			</ul>
		</xsl:if>
		<xsl:if test="form/MESSAGES">
			<br />
			<center>
			<div class="messagetextheader" id="NotificationMessage" name="NotificationMessage">
				<table width="100%">
					<tbody>
						<tr>
							<td align="right" nowrap="true"><a href="javascript:CloseDiv();" onmouseover="window.status='Close'; return true;" onmouseout="window.status=''; return true;">Close this message [X]</a></td>
						</tr>
						<tr>
							<th>Please Note</th>
						</tr>
						<tr>
							<td>
								<ul>
									<xsl:for-each select="form/MESSAGES/MESSAGE">
										<li>
											<xsl:value-of select="text()"/>
										</li>
									</xsl:for-each>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</center>
		</xsl:if>
		<xsl:if test="form/dupes">
			<br />
			<xsl:apply-templates select="form/dupes" />	
		</xsl:if>
		<xsl:apply-templates select="form/control" />
		<table border="0" cellspacing="0" celpadding="0" >
			<tr>				
				<xsl:for-each select="form/group">
					<xsl:choose>
						<xsl:when test="@selected[.='1']">
							<td class="Selected" nowrap="true"><xsl:attribute name="name">TABS<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">TABS<xsl:value-of select="@name"/></xsl:attribute><a class="Selected" HREF="#" onClick="tabChange(this.name);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">LINKTABS<xsl:value-of select="@name"/></xsl:attribute><span style="text-decoration:none"><xsl:value-of select="@title"/></span></a></td>
							<td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&#160;&#160;</td>
						</xsl:when>
						<xsl:otherwise>
							<td class="NotSelected" nowrap="true"><xsl:attribute name="name">TABS<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">TABS<xsl:value-of select="@name"/></xsl:attribute><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">LINKTABS<xsl:value-of select="@name"/></xsl:attribute><span style="text-decoration:none"><xsl:value-of select="@title"/></span></a></td>
							<td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&#160;&#160;</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<td valign="top" nowrap="true">
					<xsl:if test="form/@goto[.='1']">
						<script language="JavaScript" SRC="goto.js"></script>
						<strong>Record Id: </strong><input type="text" size="2" name="txtgoto" OnKeyDown="txtgotoOnKeyDown();" onfocus="txtGotFocus(this);"><xsl:if test="form/@recordid[.!='0']"><xsl:attribute name="value"><xsl:value-of select="form/@recordid"/></xsl:attribute></xsl:if></input>
						<input type="button" class="button" name="btnGoTo" value=" Go "><xsl:attribute name="onClick">GoTo();</xsl:attribute></input>
					</xsl:if>
				</td>							
			</tr>
		</table>
		<div style="position:relative;left:0;top:0;width:870px;height:350px;overflow:auto" class="singletopborder">
		<table border="0" cellspacing="0" celpadding="0" width="95%" height="95%" >
			<tr>
				<td valign="top">					
					<xsl:for-each select="form/group">
						<xsl:choose>
							<xsl:when test="@selected[.='1']">
								<table border="0"><xsl:attribute name="name">FORMTAB<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">FORMTAB<xsl:value-of select="@name"/></xsl:attribute>
									<xsl:for-each select=".//displaycolumn">
										<tr><xsl:apply-templates select="./control | ./section/control"/></tr>
									</xsl:for-each>
								</table>
							</xsl:when>
							<xsl:otherwise>
								<table border="0" style="display:none;"><xsl:attribute name="name">FORMTAB<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id">FORMTAB<xsl:value-of select="@name"/></xsl:attribute>
									<xsl:for-each select=".//displaycolumn">
										<tr><xsl:apply-templates select="./control | ./section/control"/></tr>
									</xsl:for-each>
								</table>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					
				</td>
				<td valign="top">
					<xsl:if test="/form/@image"><img border="0" align="right"><xsl:attribute name="src">img/<xsl:value-of select="/form/@image"/></xsl:attribute></img></xsl:if>
				</td>
			</tr>
		</table>
		</div>
		
		<table>
			<!-- Buttons Area -->			
			<tr>
			<td>
			<xsl:for-each select="form/button">
				<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
					<xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
				</input>
			</xsl:for-each>
			<xsl:for-each select="form/buttonscript">
				<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>				
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
			</xsl:for-each>
			</td>
			</tr>			
		</table>
		<xsl:apply-templates select="//internal"/>
		<input type="hidden" name="formname"><xsl:attribute name="value"><xsl:for-each select="form"><xsl:value-of select="@name"/></xsl:for-each></xsl:attribute></input>
		<input type="hidden" name="sys_required"><xsl:attribute name="value"><xsl:for-each select=".//control"><xsl:if test="@required[.='yes']"><xsl:value-of select="@name"/><xsl:if test="@type[.='code']">_cid</xsl:if><xsl:if test="@type[.='codelist']">_lst</xsl:if><xsl:if test="@type[.='entitylist']">_lst</xsl:if><xsl:if test="@type[.='orglist']">_lst</xsl:if><xsl:if test="@type[.='orgh']">_cid</xsl:if>|</xsl:if></xsl:for-each></xsl:attribute></input>
		<input type="hidden" name="sys_focusfields"><xsl:attribute name="value"><xsl:for-each select=".//control"><xsl:if test="@firstfield[.='1']"><xsl:value-of select="@name"/>|</xsl:if></xsl:for-each></xsl:attribute></input>
	</td>
	</tr></table></form>
	</body>
</html>
</xsl:template>

<!--<xsl:template "button">
	<input type="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
		<xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
	</input>
</xsl:template>-->

<xsl:template match="control">
<xsl:choose>
	<xsl:when test="@type[.='message']"><TR><xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute><td  colspan="5"><b><FONT><xsl:attribute name="color"><xsl:value-of select="@color"/></xsl:attribute><xsl:value-of select="@title"/></FONT></b></td></TR><TR><td colspan="5">&#160;</td></TR>
	</xsl:when>
	<!--PJS MITS #11475 04/08/2008 start-->
	<xsl:when test="@type[.='space']">
		<xsl:value-of select="."/>
	</xsl:when>
	<!--PJS MITS #11475 04/08/2008 end-->
	<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute></input></xsl:when>
	<xsl:otherwise>
		<xsl:choose>
			<xsl:when test="@type[.='controlgroup']">
				<!-- no td between controls in controlgroup -->
				<td colspan="2"><xsl:if test="@listitem"><li></li></xsl:if>
					<xsl:for-each select="control">
					<xsl:choose>
						<xsl:when test="@required[.='yes']"><b><u><xsl:value-of select="@title"/></u></b></xsl:when>
						<xsl:otherwise><xsl:if test="@type[.!='hidden']"><xsl:value-of select="@title"/></xsl:if></xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute></input></xsl:when>
						<xsl:when test="@type[.='text']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if></input>
						<xsl:if test="@href"><a class="LightBold" href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
						<xsl:if test="@button"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input></xsl:if>
						<xsl:if test="@lookupbutton"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@lookupbutton"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input></xsl:if>
						</xsl:when>
						<xsl:when test="@type[.='numeric']"><input type="text" size="30" onblur="numLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
							<xsl:attribute name="onChange">	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
							</input>
						</xsl:when>
						<xsl:when test="@type[.='currency']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="currencyLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
						</xsl:when>
						<xsl:when test="@type[.='combobox']"><select size="1"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="onChange">
							<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when>
							<xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose>
							</xsl:attribute>
							<xsl:apply-templates select="option">
								<!-- <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template> -->
					    </xsl:apply-templates>
							</select>
						</xsl:when>
						<xsl:when test="@type[.='radio']">
							<input type="radio"><xsl:attribute name="onClick"><xsl:value-of select="@onclick"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
						</xsl:when>
						<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
						</xsl:when>
						<xsl:when test="@type[.='textlabel']">
							<xsl:choose>
								<xsl:when test="@color[.!='']">
								<font><xsl:attribute name="color"><xsl:value-of select="@color"/></xsl:attribute><xsl:value-of select="text()"/></font>
								</xsl:when>
								<xsl:when test="@bgcolor">
								<xsl:attribute name="background-color"><xsl:value-of select="@bgcolor"/></xsl:attribute>
								</xsl:when>								
							<xsl:otherwise>
								<xsl:value-of select="text()"/>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						
						<xsl:when test="@type[.='buttonscript']">
							<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>				
							<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@buttontitle"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
						</xsl:when>
						
					</xsl:choose>
					</xsl:for-each>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='javascript']">
				<script language="JavaScript"><xsl:value-of select="text()"/></script>
			</xsl:when>
			<xsl:when test="@type[.='radio']">
				<td colspan="2"><xsl:if test="@listitem"><li></li></xsl:if>
				<input type="radio"><xsl:attribute name="onClick"><xsl:value-of select="@onclick"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input><xsl:value-of select="@title"/>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='button']">
				<td colspan="2">
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
				</td>
			</xsl:when>

	<!--AP - MITS 5229 - Add/Edit Org Hierarchy-->
			<xsl:when test="@type[.='divbutton']">
				<td colspan="2"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute><xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<!--End-->			
			
			<xsl:when test="@type[.='buttonscript']">
				<td colspan="2">
				<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>				
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='href']">
				<td colspan="3">
				<a href="#"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@title"/></a>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='textlabel']">
				<td colspan="2">
				<xsl:choose>
					<xsl:when test="@color[.!='']">
					<font><xsl:attribute name="color"><xsl:value-of select="@color"/></xsl:attribute><xsl:value-of select="text()"/></font>
					</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="text()"/>
				</xsl:otherwise>
				</xsl:choose>
					
				</td>
			</xsl:when>
			
			<!--Sanket Added for supp grid -start-->
			<xsl:when test="@type[.='suppgrid']">
                 <input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_suppgrid</xsl:attribute></input>
                 
				 <td>
					<xsl:value-of select="@Value"/>&#160;&#160;&#160;
				</td>
				
				<td>
					<xsl:element name="table" >
					<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
		
				<tr>
					<xsl:attribute name="class" >grayline2</xsl:attribute>
					<xsl:for-each select="fields/field">
			   <th>
							<font size="2pt"><xsl:value-of select="title"/></font>
			   </th>
			</xsl:for-each>					
			</tr>

			<xsl:for-each select="rows/row">
			<tr>
				<xsl:attribute name="class" >grayline</xsl:attribute>
				<xsl:for-each select="cell">
			<td>
				<xsl:value-of select="text()"/>
			</td>
			</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:element>
</td>
<!--Sanket Added for supp grid -end-->
</xsl:when>
			
			
			
			<xsl:when test="@type[.='subtable']">
				<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_rows</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@rows"/></xsl:attribute></input>
				<td colspan="2">
				<div color = "red" style="position:relative;left:0;top:0;width:100px;height:50px;overflow:auto" class="singletopborder">
				<table>
					<tr class="colheader" bgcolor = "red" ><td align="center" bgcolor = "blue" ><xsl:attribute name="colspan"><xsl:value-of select="@colspan"/></xsl:attribute><xsl:value-of select="@title"/></td></tr>
					<xsl:for-each select="row">
						<tr bgcolor = "red" ><xsl:attribute name="class"><xsl:value-of select="@rowclass"/></xsl:attribute>
							<xsl:for-each select="control">
								<td>
								<xsl:choose>
									<xsl:when test="@required[.='yes']"><b><u><xsl:value-of select="@title"/></u></b></xsl:when>
									<xsl:otherwise><xsl:if test="@type[.!='hidden']"><xsl:value-of select="@title"/></xsl:if></xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute></input></xsl:when>
									<xsl:when test="@type[.='text']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if></input>
									<xsl:if test="@href"><a class="LightBold" href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
									<xsl:if test="@button"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input></xsl:if>
									<xsl:if test="@lookupbutton"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@lookupbutton"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input></xsl:if>
									</xsl:when>
									<xsl:when test="@type[.='numeric']"><input type="text" size="30" onblur="numLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
										<xsl:attribute name="onChange">	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
										</input>
									</xsl:when>
									<xsl:when test="@type[.='currency']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="currencyLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
									</xsl:when>
									<xsl:when test="@type[.='combobox']"><select size="1"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
										<xsl:attribute name="onChange">
										<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when>
										<xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose>
										</xsl:attribute>
										<xsl:apply-templates select="option">
										<!-- 	<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template> -->
								    </xsl:apply-templates>
										</select>
									</xsl:when>
									<xsl:when test="@type[.='radio']">
										<input type="radio"><xsl:attribute name="onClick"><xsl:value-of select="@onclick"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
									</xsl:when>
									<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
									</xsl:when>
									<xsl:when test="@type[.='textlabel']">
										<xsl:choose>
											<xsl:when test="@color[.!='']">
											<font><xsl:attribute name="color"><xsl:value-of select="@color"/></xsl:attribute><xsl:value-of select="text()"/></font>
											</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="text()"/>
										</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="@type[.='buttonscript']">
										<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>				
										<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@buttontitle"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
									</xsl:when>
								</xsl:choose>
							</td>
							</xsl:for-each>
						<xsl:if test="@input"><td><input><xsl:attribute name="type"><xsl:value-of select="@inputtype"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@input"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@rowid"/></xsl:attribute></input></td></xsl:if>
						</tr>
					</xsl:for-each>
					<tr><td align="left"><xsl:attribute name="colspan"><xsl:value-of select="@colspan"/></xsl:attribute>
						<input type="button" class="button" value=" Add "><xsl:attribute name="name">btnAdd<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">Add<xsl:value-of select="@name"/>();</xsl:attribute></input>
						<input type="button" class="button" value="Delete"><xsl:attribute name="name">btnDelete<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">Delete<xsl:value-of select="@name"/>();</xsl:attribute></input>
					</td></tr>
				</table>
				</div>
				</td>
				<script language="JavaScript"><xsl:value-of select="@onload"/></script>
			</xsl:when>
			<xsl:when test="@type[.='subtable1']" >
				<!--<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>-->
				<input type="hidden">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>_rows</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="count(row)"/></xsl:attribute>
							<xsl:attribute name="DisplayColumns"><xsl:for-each select="./header/column"><xsl:if test="@hidden[.='false']"><xsl:value-of select="@name"/>/</xsl:if></xsl:for-each></xsl:attribute>
				</input>
				<td colspan="6" valign="top">
					<div class = "divscroll">
					<!--div style="position:relative;left:0;top:0;width:700px;height:100px;overflow:auto" class = "divscroll"-->
						<xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
						<xsl:element name="table">
								<xsl:attribute name="id"><xsl:value-of select="@name"/>_table</xsl:attribute>
								<!--xsl:attribute name="border">1</xsl:attribute-->
								<!-- Table Header -->
								<xsl:if test="@title">
									<tr class="ctrlgroup">
										<!-- Leave room for selection check boxes -->
										<!--xsl:if test="@rowselect">
											<td><xsl:text> </xsl:text></td>
										</xsl:if-->
										<xsl:element name="td">
											<xsl:attribute name="colspan">
												<xsl:value-of select="count(header/column)  + 1"/>
											</xsl:attribute>
											<xsl:value-of select="@title"/>
										</xsl:element>
									</tr>
								</xsl:if>
								<!-- Column Headers -->
								<tr class="colheader3">
									<!-- Leave room for selection check boxes -->
									<xsl:if test="@rowselect">
										<td></td>
									</xsl:if>
					
									<xsl:for-each select="./header/column">
										<xsl:if test="@hidden != 'true'">
											<td>
												<xsl:value-of select="@title"/>
												<!--xsl:variable name="display-columns" select="concat($display-columns, '@name', ',' )" /-->
											</td>
										</xsl:if>
									</xsl:for-each>
					
									<!-- Single Column Header for All hidden columns -->
									<xsl:if test="./header/column[@hidden = 'true']">
										<xsl:element name="td">
											<xsl:attribute name="colspan">
												<xsl:value-of select="count(./header/column[@hidden = 'true'])"/>
											</xsl:attribute>
										</xsl:element>
									</xsl:if>
								</tr>
					
								<!-- Render Value Cells -->
								<xsl:for-each select="row">
									<xsl:variable select="position()" name="row-position"/>
									<tr>
										<!-- Leave room for selection check boxes -->
										<xsl:choose>
													<xsl:when  test="position() mod 2 = 0">
														<xsl:attribute name="class" >grayline2</xsl:attribute>
													</xsl:when>
													<xsl:otherwise>
														<xsl:attribute name="class" >grayline</xsl:attribute>
													</xsl:otherwise>
											</xsl:choose>
										<xsl:if test="../@rowselect">
											<td>
												<xsl:choose>
													<xsl:when test="@selectable!='false'">
														<xsl:element name="input">
															<xsl:attribute name="type">checkbox</xsl:attribute>
															<xsl:attribute name="name"><xsl:value-of select="../@name"/>_chk_<xsl:value-of select="position()"/></xsl:attribute>
															<xsl:attribute name="id"><xsl:value-of select="../@name"/>_chk_<xsl:value-of select="position()"/></xsl:attribute>
															<xsl:attribute name="onclick">onClickCheckBox(<xsl:value-of select="position()"/>, '<xsl:value-of select="../@name"/>')</xsl:attribute>
														</xsl:element>
													</xsl:when>
													<xsl:otherwise>
															&#160;
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</xsl:if>
					
										<xsl:for-each select="cell">
											<xsl:variable select="position()" name="cell-position"/>
											<xsl:if test="../../header/column[position()=$cell-position]/@hidden != 'true'">
												<xsl:element name="td">
													<!--xsl:attribute name="name">
														<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute-->
					
													<xsl:choose>
														<xsl:when test="@onclick">
															<xsl:element name="a">
																<xsl:attribute name="href">
																	<xsl:value-of select="@onclick"/>
																</xsl:attribute>
																<xsl:value-of select="text()"/>
															</xsl:element>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="text()"/>
														</xsl:otherwise>
													</xsl:choose>
					
													<xsl:element name="input">
														<xsl:attribute name="type">hidden</xsl:attribute>
														<xsl:attribute name="id">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="name">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="value">
															<xsl:value-of select="@idvalue"/>
														</xsl:attribute>
														<xsl:attribute name="fieldCaption">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
													</xsl:element>
												</xsl:element>
											</xsl:if>
										</xsl:for-each>
										<td>
											<xsl:for-each select="cell">
												<xsl:variable select="position()" name="cell-position"/>
												<xsl:if test="../../header/column[position()=$cell-position]/@hidden = 'true'">
													<xsl:element name="input">
														<xsl:attribute name="type">hidden</xsl:attribute>
														<xsl:attribute name="id">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="name">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="value">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
														<xsl:attribute name="fieldCaption">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
													</xsl:element>
												</xsl:if>
											</xsl:for-each>
										</td>
									</tr>
									</xsl:for-each>
								</xsl:element>
						</div>
						<table>
							<tr>
								<xsl:apply-templates select="./control | ./section/control"/>
							</tr>
						</table>


					</td>
		
					


				
				<!--td></td-->
				<!--script language="JavaScript"><xsl:value-of select="@onload"/></script-->
   
			   </xsl:when>
		<xsl:otherwise>
			<td nowrap="true">
				<!--pmahli MITS 7857 12/05/2006 columnid added to column-->
				<xsl:attribute name="id"><xsl:value-of select="@name"/>_ctlcol</xsl:attribute>
				<xsl:choose>
					<xsl:when test="@required[.='yes']"><b><u><xsl:value-of select="@title"/></u></b></xsl:when>
					<xsl:otherwise><xsl:if test="@type[.!='hidden']"><xsl:value-of select="@title"/></xsl:if></xsl:otherwise>
				</xsl:choose>
			</td>
			<td nowrap="true">
				<xsl:choose>
					<xsl:when test="@type[.='text']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if></input>
					<xsl:if test="@href"><a class="LightBold" href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
					<xsl:if test="@button"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input></xsl:if>
					<xsl:if test="@lookupbutton"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@lookupbutton"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input></xsl:if>
					</xsl:when>
					<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='textml']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:value-of select="text()"/></xsl:element>
					<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute><xsl:attribute name="onClick">EditMemo('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='code']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="@codeid"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='codewithdetail']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="button" class="button" value="Detail"><xsl:attribute name="name"><xsl:value-of select="@name"/>detailbtn</xsl:attribute><xsl:attribute name="onClick">selectCodeWithDetail('<xsl:value-of select="@name"/>',<xsl:value-of select="@detailtype"/>)</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="@codeid"/></xsl:attribute><xsl:attribute name="oldvalue"><xsl:value-of select="@codeid"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='freecode']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:value-of select="text()"/></xsl:element><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
					</xsl:when>
					<!--ngupta20 MITS 6812 01/17/2007 Added the onFocus event for date control-->
					<xsl:when test="@type[.='date']"><input type="text" size="30" onblur="dateLostFocus(this.name);" onFocus="dateFocus(this.name);" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectDate('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='datebuttonscript']">
						<input type="text" size="30" onblur="dateLostFocus(this.name);"><xsl:attribute name="onchange"><xsl:value-of select="@onchangefunctionname"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
						<script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>				
						<input type="button" class="button"><xsl:attribute name="name">btn<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value">...</xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@onclickfunctionname"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='time']"><input type="text" size="30" onblur="timeLostFocus(this.name);" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='orgh']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('orgh','<xsl:value-of select="@name"/>','<xsl:value-of select="@title"/>')</xsl:attribute></input>
					<xsl:if test="@instructions"><input type="button" class="button" value="Instr"><xsl:attribute name="name"><xsl:value-of select="@name"/>btnInstructions</xsl:attribute><xsl:attribute name="onClick">ClaimInstructions()</xsl:attribute></input></xsl:if>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="@codeid"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='label']"><input type="text" size="30"  onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='readonly']">
						<xsl:choose>
							<xsl:when test="@readonlymemo[.='1']">
								<xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onkeypress">return eatKeystrokes(event);</xsl:attribute><xsl:value-of select="text()"/></xsl:element>
							</xsl:when>
							<xsl:otherwise>
								<input type="text" size="30"  onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
								<xsl:choose>
								<xsl:when test="@linktobutton[.='1']">
									<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
								</xsl:when>
								<xsl:otherwise>
								<xsl:if test="@lookupbutton[.='1']">
									<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@fieldmark"/>',11)</xsl:attribute></input>
								</xsl:if>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="@type[.='memo']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:if test="@readonly"><xsl:attribute name="readonly"><xsl:value-of select="@readonly"/></xsl:attribute></xsl:if><xsl:value-of select="text()"/></xsl:element>
					<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute><xsl:attribute name="onClick">EditMemo('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='checkbox']"><input type="checkbox" value="1" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
						<xsl:if test="@onclick"><xsl:attribute name="onclick"><xsl:value-of select="@onclick"/></xsl:attribute></xsl:if>
						<xsl:if test="text()[.='1']"><xsl:attribute name="checked">true</xsl:attribute></xsl:if></input>
					</xsl:when>
					<xsl:when test="@type[.='zip']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='phone']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='ssn']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='taxid']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="taxidLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='numeric']"><input type="text" size="30" onblur="numLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
						<xsl:attribute name="onChange">	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
						</input>
					</xsl:when>
					<xsl:when test="@type[.='currency']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="currencyLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
					<xsl:if test="@lookupbutton"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@lookupbutton"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input></xsl:if>
					</xsl:when>
					<xsl:when test="@type[.='entitylookup']">
						<!--pmahli MITS 7857 12/05/2006 -start ClaimantName filed made readonly-->
						<xsl:choose>
							<xsl:when test="@name[.='clm_lastname']">
								<input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" onkeydown="eatKeystrokes();">
									<xsl:attribute name="name">
										<xsl:value-of select="@name"/>
									</xsl:attribute>
									<xsl:attribute name="value">
										<xsl:value-of select="text()"/>
									</xsl:attribute>
									<xsl:attribute name="cancelledvalue">
										<xsl:value-of select="text()"/>
									</xsl:attribute>
								</input>
							</xsl:when>
							<xsl:otherwise>
								<input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
									<xsl:attribute name="name">
										<xsl:value-of select="@name"/>
									</xsl:attribute>
									<xsl:attribute name="value">
										<xsl:value-of select="text()"/>
									</xsl:attribute>
									<xsl:attribute name="cancelledvalue">
										<xsl:value-of select="text()"/>
									</xsl:attribute>
								</input>
						      </xsl:otherwise>
						</xsl:choose>
						<!--pmahli MITS 7857 12/05/2006 - End-->
						<xsl:if test="@creatable">
							<input type="hidden">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</input>
						</xsl:if>
						<!--pmahli test-->
							<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='entitylookupwithLink']">
						<input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
							<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
							<xsl:attribute name="LinkTitle"><xsl:value-of select="@LinkTitle"/></xsl:attribute>
							<xsl:attribute name="article1"><xsl:value-of select="@article1"/></xsl:attribute>
							<xsl:attribute name="article2"><xsl:value-of select="@article2"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="required"><xsl:value-of select="@required"/></xsl:attribute>
						</input>
						<xsl:if test="@creatable">
							<input type="hidden">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</input>
						</xsl:if>
						<input type="button" class="button" value="...">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute>
							<xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1)</xsl:attribute>
						</input>
						<img src="img\spacer.gif" width="10" height="1"></img>
						<a href="#"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@LinkTitle"/></a>
					</xsl:when>
					<xsl:when test="@type[.='employeelookup']"><input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','employees',3,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='eidlookup']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@name"/>',2)</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="codeid"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='vehiclelookup']">
						<input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
							<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
						</input>
						<xsl:if test="@creatable">
							<input type="hidden">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</input>
						</xsl:if>
						<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@fieldmark"/>',10)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='claimnumberlookup']"><input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','claim',1,'<xsl:value-of select="@name"/>',6)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='eventnumberlookup']"><input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@name"/>',7)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='eventlookup']"><input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if>					<xsl:choose>
						<xsl:when test="@linktobutton[.='1']">
							<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
						</xsl:when>
						<xsl:otherwise>
						<xsl:if test="@lookupbutton[.='1']">
							<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@fieldmark"/>',11)</xsl:attribute></input>
						</xsl:if>
						</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="@type[.='vehiclenumberlookup']"><input type="text" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></input><xsl:if test="@creatable"><input type="hidden" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute></input></xsl:if><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@name"/>',8)</xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='policynumberlookup']"><input type="text" size="30" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','policy',6,'<xsl:value-of select="@name"/>',9)</xsl:attribute></input>
					</xsl:when>
							
					<xsl:when test="@type[.='policylookup' or .='planlookup']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupClaimPolicy('<xsl:value-of select="@name"/>','<xsl:value-of select="@lookup"/>')</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
						<xsl:if test="@linktobutton[.='1']">
							<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
						</xsl:if>
					</xsl:when>
					<xsl:when test="@type[.='codelist']"><select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
						<xsl:apply-templates select="option">
							<!-- <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template> -->
				 	</xsl:apply-templates>
						</select> <input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
						<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
						<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
				 		<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each></xsl:attribute>
				 	</input>
					</xsl:when>
					<xsl:when test="@type[.='entitylist']"><select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
						<xsl:apply-templates select="option">
							<!-- <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template> -->
				 	</xsl:apply-templates>
						</select> <input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@name"/>',3)</xsl:attribute></input>
						<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
						<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
				 		<!--<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="not(last())">,</xsl:if></xsl:for-each></xsl:attribute>-->
						<!--AP MITS 8870 -->
						<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each></xsl:attribute>
				 	</input>
					</xsl:when>
					<xsl:when test="@type[.='policysearch']">
						<input type="text" size="30" onblur="eidLostFocus(this.name);" onchange="setDataChanged(true);">
							<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
						</input>
						<input type="button" class="button" value="...">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute>
							<xsl:attribute name="onClick">searchPolicies(6,'<xsl:value-of select="@name"/>')</xsl:attribute></input>
						<input type="hidden">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
						</input>
					</xsl:when>
					<xsl:when test="@type[.='combobox']"><select size="1"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
						<xsl:attribute name="onChange">
						<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when>
						<xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose>
						</xsl:attribute>
						<xsl:if test="@includefilename"><script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script></xsl:if>
						<xsl:apply-templates select="option">
							<!-- <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template> -->
					</xsl:apply-templates>
						</select>
					</xsl:when>
					<xsl:when test="@type[.='accountlist']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectAccountList('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
					</xsl:when>
					<xsl:when test="@type[.='labelonly']">&#160;
					</xsl:when>
					<xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
					</xsl:otherwise>
				</xsl:choose>
			</td>	
			<td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td>	
		</xsl:otherwise></xsl:choose>
	</xsl:otherwise>
</xsl:choose>

	<xsl:if test="@GroupAssoc[.!='']">
		<input type="hidden">
			<xsl:attribute name="name"><xsl:value-of select="@name"/>_GroupAssoc</xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="@name"/>_GroupAssoc</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@GroupAssoc"/></xsl:attribute>
		</input>
	</xsl:if>

</xsl:template>

<xsl:template match="internal">
	<xsl:choose>
		<xsl:when test="@type[.='hidden']">
			<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="form">
	<xsl:value-of select="@title"/>
</xsl:template>

<xsl:template match="toolbar/button">
	<td align="center" valign="middle" HEIGHT="32" WIDTH="28">
	<xsl:choose>
		<xsl:when test="@type[.='movefirst']"><a class="LightBold" href="#" onClick="Navigate(1)"><img src="img/first.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='img/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/first.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='moveprevious']"><a class="LightBold" href="#" onClick="Navigate(2)"><img src="img/prev.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='img/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/prev.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='movenext']"><a class="LightBold" href="#" onClick="Navigate(3)"><img src="img/next.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='img/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/next.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='movelast']"><a class="LightBold" href="#" onClick="Navigate(4)"><img src="img/last.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='img/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/last.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='save']"><a class="LightBold" href="#" onClick="Navigate(5)"><img src="img/save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/save.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='saveandsend']"><a class="LightBold" href="#" onClick="SaveAndSend()"><img src="img/saveandsend.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/saveandsend2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/saveandsend.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='delete']"><a class="LightBold" href="#" onClick="DeleteRecord()"><img src="img/delete.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/delete.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='new']"><a class="LightBold" href="#" onClick="Navigate('')"><img src="img/new.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/new.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='froi']"><a class="LightBold" href="#" onClick="tlbButton('form=froi;id=%currentid%')"><img src="img/froi.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/froi2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/froi.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='attach']"><a class="LightBold" href="#" onClick="attach()"><img src="img/attach.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/attach.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='esumm']"><a class="LightBold" href="#" onClick="tlbButton('form=esumm;%currentidname%=%currentid%')"><img src="img/esumm.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/esumm2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/esumm.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='search']"><a class="LightBold" href="#" onClick="doSearch()"><img src="img/search.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/search2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/search.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='lookup']"><a class="LightBold" href="#" onClick="doLookup()"><img src="img/lookup.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/lookup.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='summary']"><a class="LightBold" href="#" onClick="showSummary()"><img src="img/esumm.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/esumm2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/esumm.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='supplemental']"><a class="LightBold" href="#" onClick="showSupp()"><img src="img/supp.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/supp2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/supp.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='jurisdictionals']"><a class="LightBold" href="#" onClick="showJuris()"><img src="img/juris.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/juris2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/juris.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='quickdiary']"><a class="LightBold" href="#" onClick="QuickDiary()"><img src="img/quickdiary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/quickdiary2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/quickdiary.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='filtereddiary']"><a class="LightBold" href="#" onClick="FilteredDiary()"><img src="img/filtereddiary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/filtereddiary2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/filtereddiary.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='diary']"><a href="#" onClick="Diary()"><img src="img/diary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/diary.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='comments']"><a class="LightBold" href="#" onClick="Comments()"><img src="img/comments.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/comments2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/comments.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='mailmerge']"><a class="LightBold" href="#" onClick="MailMerge()"><img src="img/mailmerge.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/mailmerge2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/mailmerge.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='recordsummary']"><a class="LightBold" href="#" onClick="recordSummary()"><img src="img/recordsummary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/recordsummary.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='commentsummary']"><a class="LightBold" href="#" onClick="CommentSummary()"><img src="img/commentsummary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/commentsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/commentsummary.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='printadjustertext']"><a class="LightBold" href="#" onClick="PrintAdjusterText()"><img src="img/print.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/print2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/print.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='eventexplorer']"><a class="LightBold" href="#" onClick="EventExplorer()"><img src="img/eventexplorer.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/eventexplorer2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/eventexplorer.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='deleteoh']"><a class="LightBold" href="#" onClick="DeleteOrgHierarchy()"><img src="img/delete.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/delete.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='saveoh']"><a class="LightBold" href="#" onClick="submitReport()"><img src="img/save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/save.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:when test="@type[.='saveohadd']"><a class="LightBold" href="#" onClick="addSubmitReport()"><img src="img/save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/save.gif';this.style.zoom='100%'"><xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute></img></a></xsl:when>
		<xsl:otherwise>Unknown Toolbar Button: <xsl:value-of select="@type"/></xsl:otherwise>
	</xsl:choose>
	</td>
</xsl:template>

<xsl:template match="error">
<li class="errtext">
	<xsl:if test="@number[.='10000']"><img src="img/locked.gif" width="12" height="15" title="Security Message" border="0" />&#160;&#160;</xsl:if>
	<xsl:value-of select="text()"/>
</li>
</xsl:template>

<xsl:template match="dupes">
	<script language="JavaScript">setDataChanged(true);
	function DupeCallback(arg)
	{
		m_Wnd.close(); 
		if(arg==1){
			document.frmData.dupeoverride.value=1; 
			Navigate(5); 
		}
		return false; 
	}
	document.DupeCallback=DupeCallback;
	m_sDupes = new String("");
	m_sDupes=m_sDupes+('<html><head><title>Possible duplicate claim...</title><link rel="stylesheet" href="RMNet.css" type="text/css" />');
	m_sDupes=m_sDupes+('</head><body class="10pt">');
	m_sDupes=m_sDupes+('<div class="errtextheader">Claim was not saved, possible duplicate claim...</div>');
	m_sDupes=m_sDupes+('<table width="100%"><tr><td>');
	<xsl:choose>
	<xsl:when test="@formname[.='claimgc' or .='claimva']">
		m_sDupes=m_sDupes+('<table width="100%">');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><b><u>Date of Event</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@eventdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><b><u>Claim Type</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimtype"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><b><u>Date of Claim</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><b><u>Department</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@department"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('</table>');
		m_sDupes=m_sDupes+('<table width="100%"><tr class="ctrlgroup"><td class="colHeader">Event Number</td><td class="colHeader">Claim Number</td><td class="colHeader">Claim Status</td><td class="colHeader">Claimant</td><td class="colHeader">Event Description</td></tr>');
		<xsl:for-each select="claim">
				m_sDupes=m_sDupes+('<tr class="rowlight">');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=event&amp;syscmd=0&amp;psid=3000&amp;sys_formidname=eventid&amp;eventid=<xsl:value-of select="@eventid"/></xsl:attribute>');
					m_sDupes=m_sDupes+('<xsl:value-of select="@eventnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=<xsl:value-of select="//dupes/@formname"/>&amp;syscmd=0&amp;sys_formidname=claimid&amp;claimid=<xsl:value-of select="@claimid"/></xsl:attribute> ');
					m_sDupes=m_sDupes+('<xsl:value-of select="@claimnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimstatus"/></td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimant"/></td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@eventdescription"/></td>');
				m_sDupes=m_sDupes+('</tr>');
		</xsl:for-each>
		m_sDupes=m_sDupes+('</table><table><tr>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Save (Ignore Duplication)" onClick="window.opener.document.DupeCallback(1); window.close(); return true;" /></td>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Cancel" onClick="window.close();" /></td>');
		m_sDupes=m_sDupes+('</tr></table>');
	</xsl:when>
	<xsl:when test="@formname[.='claimwc']">
		m_sDupes=m_sDupes+('<table width="100%">');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><b><u>Date of Event</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@eventdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><b><u>Department</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@department"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><b><u>Jurisdiction</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@juris"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><b><u>Employee Name</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@employee"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><b><u>Claim Type</u></b></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimtype"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><b><u>Employee SSN</u></b></td><td ><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@taxid"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('</table>');
		m_sDupes=m_sDupes+('<table width="100%"><tr class="ctrlgroup"><td class="colHeader">Claim Date</td><td class="colHeader">Claim Number</td><td class="colHeader">Claim Status</td><td class="colHeader">Event Number</td><td class="colHeader">Event Description</td></tr>');
		<xsl:for-each select="claim">
				m_sDupes=m_sDupes+('<tr class="rowlight">');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimdate"/></td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=<xsl:value-of select="//dupes/@formname"/>&amp;syscmd=0&amp;sys_formidname=claimid&amp;claimid=<xsl:value-of select="@claimid"/></xsl:attribute> ');
					m_sDupes=m_sDupes+('<xsl:value-of select="@claimnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimstatus"/></td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=event&amp;syscmd=0&amp;psid=3000&amp;sys_formidname=eventid&amp;eventid=<xsl:value-of select="@eventid"/></xsl:attribute>');
					m_sDupes=m_sDupes+('<xsl:value-of select="@eventnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@eventdescription"/></td>');
				m_sDupes=m_sDupes+('</tr>');
		</xsl:for-each>
		m_sDupes=m_sDupes+('</table><table><tr>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Save (Ignore Duplication)" onClick="window.opener.document.DupeCallback(1); window.close(); return true;" /></td>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Cancel" onClick="window.close();" /></td>');
		m_sDupes=m_sDupes+('</tr></table>');
	</xsl:when>
	</xsl:choose>
	m_sDupes=m_sDupes+('</td></tr></table></body></html>');
	</script>
</xsl:template>
<xsl:template match="option"><xsl:copy-of select="."/></xsl:template>
</xsl:stylesheet>