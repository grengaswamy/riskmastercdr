﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Xml.Xsl;
using System.IO;
using System.Xml.XPath;
using Riskmaster.Common;
namespace ComponentPowerViewUpgrade.Classes
{
    public class PowerViewUpgrade
    {
        private  string XslFileTab = @"form2Tab.xslt";
        private  string XslFileTopDown = @"form2.xslt";
        private  string XslFileTabReadOnly = @"form2Tabreadonly.xslt";
        private  string XslFileTopDownReadOnly = @"form2readonly.xslt";
        private string sXslBasePath = "";

        public PowerViewUpgrade()
        {
            XmlNode objPVXmlPath = null;
            objPVXmlPath = RMConfigurator.NamedNode("PowerViewXSL/XSLLocator");

            if (objPVXmlPath != null)
            {
                sXslBasePath = objPVXmlPath.InnerXml;
                //To Do :: once the path is fixed this condition could be removed
                if (File.Exists(sXslBasePath + "/" + XslFileTab))
                {
                    XslFileTab = sXslBasePath + "/" + XslFileTab;
                }

                if (File.Exists(sXslBasePath + "/" + XslFileTopDown))
                {
                    XslFileTopDown = sXslBasePath + "/" + XslFileTopDown;
                }

                if (File.Exists(sXslBasePath + "/" + XslFileTabReadOnly))
                {
                    XslFileTabReadOnly = sXslBasePath + "/" + XslFileTabReadOnly;
                }

                if (File.Exists(sXslBasePath + "/" + XslFileTopDownReadOnly))
                {
                    XslFileTopDownReadOnly = sXslBasePath + "/" + XslFileTopDownReadOnly;
                }
            }

        }

        public bool UpgradeXmlToAspx(bool bReadOnly,bool bTopDown,XmlDocument viewXml, string pageName, out StringBuilder convertedAspx)
        {
            XslCompiledTransform xsl = new XslCompiledTransform();
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            string[] aspxPageName = new string[2];
            string sHtml = "";
            try
            { 
                XmlDocument objDom = viewXml;
                XmlNode objNode = objDom.SelectSingleNode("//form");
                if (objNode.Attributes["formtype"] != null)
                {
                    if (objNode.Attributes["formtype"].Value.ToLower() == "gridpopup")
                    {
                        if (!bTopDown && !bReadOnly)
                        {
                            xsl.Load(XslFileTab);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (!bTopDown && bReadOnly)
                        {
                            xsl.Load(XslFileTabReadOnly);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (bTopDown && !bReadOnly)
                        {
                            xsl.Load(XslFileTopDown);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (bTopDown && bReadOnly)
                        {
                            xsl.Load(XslFileTopDownReadOnly);
                            xslArg.AddParam("divClass", "", "full");
                        }
                    }
                }
                // load xslt to do transformation
                else if (pageName.IndexOf("sentinel") > -1 && !bReadOnly)
                {
                    xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "sentinel");
                }
                else if (pageName.IndexOf("sentinel") > -1 && bReadOnly)
                {
                    xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "sentinel");
                }
                // load xslt to do transformation
                
                // load xslt to do transformation
                else if (pageName.IndexOf("admintracking") > -1 && !bReadOnly)
                {
                    xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "full");
                }
                else if (pageName.IndexOf("admintracking") > -1 && bReadOnly)
                {
                    xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "full");
                }
                // load xslt to do transformation
                else if (bTopDown && !bReadOnly)
                {
                    xsl.Load(XslFileTopDown);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && !bReadOnly)
                {
                    xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "half");

                }
                else if (bTopDown && bReadOnly)
                {
                    xsl.Load(XslFileTopDownReadOnly);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && bReadOnly)
                {
                    xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "half");

                }

                // get transformed results
                xsl.Transform(viewXml, xslArg, sw);
                aspxPageName = pageName.Split('.');
                convertedAspx = new StringBuilder("");
                convertedAspx.Append("<%@ Page Language=\"C#\" AutoEventWireup=\"true\" CodeBehind=\"");
                if (pageName.IndexOf("admintracking") > -1)
                {
                    convertedAspx.Append("admintracking");
                }
                else
                {
                    convertedAspx.Append(aspxPageName[0]);
                }
                convertedAspx.Append(".aspx.cs");
                //convertedAspx.Append("\" Inherits=\"Power_Editor.WebForm1\" %>");
                if (pageName.IndexOf("admintracking") > -1)
                {
                    convertedAspx.Append("\"  Inherits=\"Riskmaster.UI.FDM.Admintracking"  + "\" ValidateRequest=\"false\" %>");
                }
                else
                {
                convertedAspx.Append("\"  Inherits=\"Riskmaster.UI.FDM." + aspxPageName[0].Substring(0 , 1).ToUpper() + aspxPageName[0].Substring(1) + "\" ValidateRequest=\"false\" %>");
                }
                //convertedAspx.Append("<%@ Register assembly=\"AjaxControlToolkit\" namespace=\"AjaxControlToolkit\" tagprefix=\"cc1\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" tagname=\"CodeLookUp\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" tagname=\"UserControlDataGrid\" tagprefix=\"dg\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" tagname=\"MultiCode\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" tagname=\"PleaseWaitDialog\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/SystemUsers.ascx\" tagname=\"SystemUsers\" tagprefix=\"cul\" %>");
                convertedAspx.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                sHtml = sw.ToString().Replace("&amp;", "&");
                convertedAspx.Append(sHtml);
                // free up the memory of objects that are not used anymore
                sw.Close();
                return true;
                
            }

            catch (FileLoadException e)
            {
                throw new FileLoadException("A File Load exception has occured in upgrading the screen " + pageName + e.Message, e);
                
            }

            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("File not found during upgradation of the screen " + pageName + e.Message, e);
                
            }
            catch (IOException e)
            {
                throw new IOException("An IO exception has occured in upgrading the screen " + pageName + e.Message, e);
                
            }
            catch (Exception e)
            {
                throw new Exception("An exception has occured in upgrading the screen " + pageName + e.Message, e);
                
            }
            finally
            {
                if (xsl != null)
                {
                    xsl = null;
                }
                if (sw != null)
                {
                    sw.Dispose();
                }
            }
        }
        /// <summary>
        /// Function for converting any xml tag in form of Aspx controls
        /// created by- Parijat, for dynamic Controls.
        /// </summary>
        /// <param name="bReadOnly">If the converted should be in form of read only.</param>
        /// <param name="bTopDown">If it has to be in form of top down layout.</param>
        /// <param name="viewXml">Xml document which needs to be converted.</param>
        /// <param name="divName">Any name Of div tag which is being converted</param>
        /// <param name="convertedAspx">The converted result in the form of ASPX controls.</param>
        /// <returns>bool</returns>
           public bool UpgradeXmlTagToAspxForm(bool bReadOnly,bool bTopDown,XmlDocument viewXml, string divName, out StringBuilder convertedAspx)
        {
            XslCompiledTransform xsl = new XslCompiledTransform();
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            string[] aspxPageName = new string[2];
            string sHtml = "";
            try
            {
                // load xslt to do transformation
                if (bTopDown && !bReadOnly)
                {
                    xsl.Load(XslFileTopDown);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && !bReadOnly)
                {
                    xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "half");

                }
                else if (bTopDown && bReadOnly)
                {
                    xsl.Load(XslFileTopDownReadOnly);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && bReadOnly)
                {
                    xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "half");

                }

                // get transformed results
                xsl.Transform(viewXml, xslArg, sw);
                convertedAspx = new StringBuilder("");
                sHtml = sw.ToString().Replace("&amp;", "&");
                convertedAspx.Append(sHtml);
                // free up the memory of objects that are not used anymore
                sw.Close();
                return true;

            }

            catch (FileLoadException e)
            {
                throw new FileLoadException("A File Load exception has occured in upgrading the division " + divName + e.Message, e);

            }

            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("File not found during upgradation of the division " + divName + e.Message, e);

            }
            catch (IOException e)
            {
                throw new IOException("An IO exception has occured in upgrading the division " + divName + e.Message, e);

            }
            catch (Exception e)
            {
                throw new Exception("An exception has occured in upgrading the division " + divName + e.Message, e);

            }
            finally
            {
                if (xsl != null)
                {
                    xsl = null;
                }
                if (sw != null)
                {
                    sw.Dispose();
                }
            }
        }
    }
}
