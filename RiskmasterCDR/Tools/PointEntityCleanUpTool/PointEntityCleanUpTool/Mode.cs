﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using PointEntityCleanUpTool.AuthenticationService;
using PointEntityCleanUpTool.PolicyInterfaceService;
using Riskmaster.Application.PolicySystemInterface;
using Riskmaster.Security.RMApp;
using System.Data.SqlClient;

namespace PointEntityCleanUpTool
{
    public partial class FrmFixRedundantEntities : Form
    {
        #region "Page Variables"
        const string s_Agents = "AGENTS";
        const string s_Drivers = "DRIVERS";
        const string s_Driver = "DRIVER";
        const string s_PolicyInterest = "POLICY INTEREST";
        const string s_UnitInterest = "UNIT INTEREST";
        const string s_PolicyInsured = "POLICY INSURED";

        const string s_PolicyID = "POLICYID";
        const string s_EntityID = "ENTITYID";
        const string s_ClientSeqNum = "CLIENTSEQNUM";
        const string s_AddrSeqNum = "ADDRSEQNUM";
        const string s_LocationCompany = "LOCATIONCOMPANY";
        const string s_MasterCompany = "MASTERCOMPANY";
        const string s_Module = "MODULE";
        const string s_InsLine = "INSLINE";
        const string s_RiskLoc = "RISKLOC";
        const string s_RiskSubLoc = "RISKSUBLOC";
        const string s_Product = "PRODUCT";
        const string s_DriverID = "DRIVERID";
        const string s_RecordStatus = "RECORDSTATUS";
        const string s_ErrorMsg = "ERRORMSG";
        const string s_ErrorQry = "ERRORQRY";
        const string s_Address1 = "ADDRESS1";
        const string s_Address2 = "ADDRESS2";
        const string s_City = "CITY";
        const string s_PostalCode = "POSTALCODE";
        const string s_StateProvCd = "STATEPROVCD";
        const string s_UpdateMode = "UPDATE";
        const string s_DeleteMode = "DELETE";
        const string s_EntityTable = "ENTITY";
        const string s_PolicyXEntityTable = "POLICY_X_ENTITY";
        const string s_PolicyXInsuredTable = "POLICY_X_INSURED";
        const string s_EntityXAddressTable = "ENTITY_X_ADDRESS";
        const string s_DriverTable = "DRIVER";
        const string s_ShortName = "SHORTNAME";
        const string s_TaxID = "TAXID";
        const string s_BirthDate = "BIRTHDATE";
        const string s_NameType = "NAMETYPE";
        const string s_PolicyNumber = "POLICYNUMBER";
        const string s_FullName = "FULLNAME";
        const string s_FullAddress = "FULLADDRESS";

        StringBuilder sb_Query;
        string s_Session = string.Empty;
        string s_AcordRequest = string.Empty;
        string s_AcordResponse = string.Empty;

        LocalCache obj_Cache = null;
        PolicySystemInterface obj_PolicyInterface = null;
        PolicyInterfaceServiceClient obj_Pol = null;
        PolicyEnquiry obj_Request = null;
        PolicyEnquiry obj_Response = null; 
        Dictionary<string, string> obj_OutputColumns = null;
        Dictionary<string, string> obj_dictParams = null;
        private int m_iClientId = 0;
        #endregion

        #region "Page Events"
        public FrmFixRedundantEntities()
        {
            InitializeComponent();
        }
        private void FrmFixRedundantEntities_Load(object sender, EventArgs e)
        {
            HomeLoad();
            GetPolicySystem();
            SetControlAttributes();

            lblWarning.Text = string.Concat("Please ensure you have taken backup of ", CommonCache.ss_MainDatabaseName, " database...");
        }
        private void cbPolicySystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonCache.si_PolicySystemID = Convert.ToInt32(cbPolicySystem.SelectedValue.ToString().Split('|')[0]);
            CommonCache.si_ClientFileFlag = Convert.ToInt32(cbPolicySystem.SelectedValue.ToString().Split('|')[1]);
            SetControlAttributes();
        }
        private void txtFromPolicyID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "\\d+") && !string.Equals(e.KeyChar.ToString(), "\b"))
                e.Handled = true;
        }
        private void txtToPolicyID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "\\d+") && !string.Equals(e.KeyChar.ToString(), "\b"))
                e.Handled = true;
        }
        private void FrmFixRedundantEntities_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose(); 
            this.Close();
        }
        private void btnFixEntities_Click(object sender, EventArgs e)
        {
            bool bIsNext = false;

            if (int.Equals(cbPolicySystem.SelectedIndex, -1))
                MessageBox.Show("Please select any Policy System...");
            else if (string.IsNullOrEmpty(txtFromPolicyID.Text.Trim()))
                MessageBox.Show("Please enter From Policy ID value...");
            else if (string.IsNullOrEmpty(txtToPolicyID.Text.Trim()))
                MessageBox.Show("Please enter To Policy ID value...");
            else
            {
                SetEntityTypeSelection(false, false, false, false, string.Empty, string.Empty);
                List<string> lstItems = clbCustom.CheckedItems.Cast<string>().ToList();
                if (lstItems.Count > 0)
                {
                    bIsNext = true;
                    foreach (object item in lstItems)
                    {
                        switch (item.ToString().ToUpper())
                        {
                            case s_PolicyInterest:
                                CommonCache.sb_IsPolicyInterestSelected = true;
                                break;
                            case s_UnitInterest:
                                CommonCache.sb_IsUnitInterestSelected = true;
                                break;
                            case s_PolicyInsured:
                                CommonCache.sb_IsPolicyInsuredSelected = true;
                                break;
                            case s_Driver:
                                CommonCache.sb_IsDriverSelected = true;
                                break;
                        }
                    }
                }
                else
                    MessageBox.Show("Please select any entity to proceed...");
            }
            if (bIsNext)
            {
                try
                {
                    sb_Query = new StringBuilder();
                    obj_PolicyInterface = new PolicySystemInterface(CommonCache.ss_DSNConnectionString, m_iClientId);
                    obj_Cache = new LocalCache(CommonCache.ss_DSNConnectionString, m_iClientId);
                    obj_Pol = new PolicyInterfaceServiceClient();
                    obj_OutputColumns = new Dictionary<string, string>();
                    obj_dictParams = new Dictionary<string, string>();

                    CommonCache.si_PolicySystemID = Convert.ToInt32(cbPolicySystem.SelectedValue.ToString().Split('|')[0]);
                    CommonCache.WriteLogFile(this.GetType().Name, "btnFixEntities_Click()", string.Concat("Logging Start at --> ", DateTime.Now), 2);

                    //Will update Entity --> Client_Seq_Num and Entity_X_Address --> Address_Seq_Num in RMA Table from POINT.
                    if (CommonCache.sb_IsPolicyInsuredSelected)
                        FixInsuredEntities();
                    if (CommonCache.sb_IsDriverSelected || CommonCache.sb_IsPolicyInterestSelected || CommonCache.sb_IsUnitInterestSelected)
                        FixNonInsuredEntities();
                    //End Updataion of Entity --> Client_Seq_Num and Entity_X_Address --> Address_Seq_Num in RMA Table from POINT.

                    CommonCache.WriteLogFile(this.GetType().Name, "btnFixEntities_Click()", string.Concat("Entity Cleanup Start at --> ", DateTime.Now), 2);
                    CleanEntityTable();
                    if (CommonCache.sb_IsDriverSelected)
                    {
                        CommonCache.WriteLogFile(this.GetType().Name, "btnFixEntities_Click()", string.Concat("Driver Cleanup Start at --> ", DateTime.Now), 2);
                        CleanDriverTable();
                    }
                }
                catch (Exception ex)
                {
                    CommonCache.WriteLogFile(this.GetType().Name, "btnFixEntities_Click()", ex.Message, 4);
                    lblError.Text = ex.Message;
                    lblError.Update();
                }
                finally
                {
                    sb_Query = null;
                    obj_PolicyInterface = null;
                    obj_Cache = null;
                    obj_Pol = null;
                    obj_OutputColumns = null;
                    obj_dictParams = null;
                }
            }
        }
        #endregion

        #region "Class Methods"

        #region "Updation of Entity --> Client_Seq_Num and Entity_X_Address --> Address_Seq_Num in RMA Table from POINT."
        #region "Database Select Queries for service request"
        private void FixInsuredEntities()
        {
            lblInfo1.Text = "Processing Start for Insured Entity Types...";
            lblInfo1.Update();
            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();

                sb_Query.Append(" SELECT E.ENTITY_ID, E.CLIENT_SEQ_NUM, P.POLICY_ID, P.POLICY_NUMBER, ");
                sb_Query.Append(" PDDX.ACORD_XML,  ");
                sb_Query.Append(" P.POLICY_NUMBER, P.POLICY_SYMBOL, P.MODULE, P.LOCATION_COMPANY, P.MASTER_COMPANY, C.CODE_ID AS LOB, E.* ");
                sb_Query.Append(" FROM POLICY_X_INSURED PXI ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = PXI.POLICY_ID ");
                sb_Query.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = PXI.INSURED_EID ");
                sb_Query.Append(" INNER JOIN CODES C ON C.CODE_ID = P.POLICY_LOB_CODE ");
                sb_Query.Append(" LEFT OUTER JOIN PS_DOWNLOAD_DATA_XML PDDX ON PDDX.TABLE_ROW_ID = P.POLICY_ID AND PDDX.TABLE_ID = {0} ");
                sb_Query.Append(" WHERE P.POLICY_SYSTEM_ID = {1} AND E.DELETED_FLAG <> -1 ");
                if (!string.IsNullOrEmpty(txtFromPolicyID.Text.ToString().Trim()) && !string.IsNullOrEmpty(txtToPolicyID.Text.ToString().Trim()))
                    sb_Query.Append(" AND P.POLICY_ID BETWEEN {2} AND {3} ");
                sb_Query.Append(" ORDER BY PXI.POLICY_ID, PXI.INSURED_EID ");

                sb_Query.Replace("{0}", "~TABLEID~");
                sb_Query.Replace("{1}", "~POLICYSYSTEMID~");
                sb_Query.Replace("{2}", "~FROMPID~");
                sb_Query.Replace("{3}", "~TOPID~");


                obj_dictParams.Add("TABLEID", obj_Cache.GetTableId("POLICY").ToString());
                obj_dictParams.Add("POLICYSYSTEMID", CommonCache.si_PolicySystemID.ToString());
                obj_dictParams.Add("FROMPID", txtFromPolicyID.Text.ToString().Trim());
                obj_dictParams.Add("TOPID", txtToPolicyID.Text.ToString().Trim());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    string sFullName = string.Empty;
                    bool bFieldExists = CommonCache.ColumnExists(ds.Tables[0], "ADDRESS_SEQ_NUM");

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sFullName = string.Concat(ds.Tables[0].Rows[i]["LAST_NAME"].ToString().Trim(), " ", ds.Tables[0].Rows[i]["FIRST_NAME"].ToString().Trim()).Trim();
                        lblInfo2.Text = string.Concat("For Policy_Number --> ", ds.Tables[0].Rows[i]["POLICY_NUMBER"].ToString(), " \n Entity --> ", sFullName);
                        lblInfo2.Update();
                        this.Update();

                        obj_OutputColumns.Clear();
                        obj_OutputColumns.Add(s_EntityID, ds.Tables[0].Rows[i]["ENTITY_ID"].ToString());
                        obj_OutputColumns.Add(s_PolicyID, ds.Tables[0].Rows[i]["POLICY_ID"].ToString());
                        obj_OutputColumns.Add(s_PolicyNumber, ds.Tables[0].Rows[i]["POLICY_NUMBER"].ToString());
                        obj_OutputColumns.Add(s_FullName, sFullName);

                        FixPolicyInsuredEntity(ds.Tables[0].Rows[i], bFieldExists);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixInsuredEntities()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                lblInfo1.Text = "Insured Completed...";
                lblInfo1.Update();

                lblInfo2.Text = "...";
                lblInfo2.Update();
            }
        }
        private void FixNonInsuredEntities()
        {
            lblInfo1.Text = "Processing Start for Non Insured Entity Types...";
            lblInfo1.Update();

            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();

                sb_Query.Append(" SELECT P.POLICY_ID,E.ENTITY_ID, G.SYSTEM_TABLE_NAME, ");
                sb_Query.Append(string.Concat(" CASE WHEN PXE.POLICY_UNIT_ROW_ID > 0 THEN '", s_UnitInterest, "'"));
                sb_Query.Append(string.Concat(" ELSE CASE WHEN G.SYSTEM_TABLE_NAME <> 'DRIVERS' THEN '", s_PolicyInterest, "'"));
                sb_Query.Append(string.Concat(" ELSE '", s_Driver, "'", " END END AS E_TYPE, "));
                sb_Query.Append(" PXE.POLICY_UNIT_ROW_ID, PDDX.ACORD_XML, PPDDX.ACORD_XML POLICY_ACORD_XML, P.POLICY_NUMBER, P.POLICY_SYMBOL, ");
                sb_Query.Append(" P.MODULE, P.LOCATION_COMPANY, P.MASTER_COMPANY, C.SHORT_CODE AS LOB, PUD.UNIT_NUMBER, PUD.UNIT_RISK_LOC, E.LAST_NAME, E.FIRST_NAME ");
                sb_Query.Append(" FROM POLICY_X_ENTITY PXE ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = PXE.POLICY_ID ");
                sb_Query.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = PXE.ENTITY_ID ");
                sb_Query.Append(" INNER JOIN GLOSSARY G ON G.TABLE_ID = PXE.TYPE_CODE ");
                sb_Query.Append(" INNER JOIN CODES C ON C.CODE_ID = P.POLICY_LOB_CODE ");
                sb_Query.Append(" LEFT OUTER JOIN PS_DOWNLOAD_DATA_XML PDDX ON PDDX.TABLE_ROW_ID = PXE.POLICYENTITY_ROWID AND PDDX.TABLE_ID = {0} ");
                sb_Query.Append(" LEFT OUTER JOIN PS_DOWNLOAD_DATA_XML PPDDX ON PPDDX.TABLE_ROW_ID = P.POLICY_ID AND PPDDX.TABLE_ID = {1} ");
                sb_Query.Append(" LEFT OUTER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = PXE.POLICY_UNIT_ROW_ID ");
                sb_Query.Append(" LEFT OUTER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID = PXU.UNIT_ID AND PUD.UNIT_TYPE = PXU.UNIT_TYPE ");
                sb_Query.Append(" WHERE P.POLICY_SYSTEM_ID = {2} ");
                sb_Query.Append(" AND UPPER(G.SYSTEM_TABLE_NAME) <> {3}");
                sb_Query.Append(" AND PDDX.ACORD_XML IS NOT NULL AND E.DELETED_FLAG <> -1 ");
                if (!CommonCache.sb_IsUnitInterestSelected)
                    sb_Query.Append(" AND PXE.POLICY_UNIT_ROW_ID = 0 ");
                if (!CommonCache.sb_IsPolicyInterestSelected)
                    sb_Query.Append(" AND (PXE.POLICY_UNIT_ROW_ID <> 0 OR UPPER(G.SYSTEM_TABLE_NAME) = {4})");
                if (!CommonCache.sb_IsDriverSelected)
                    sb_Query.Append(" AND UPPER(G.SYSTEM_TABLE_NAME) <> {5}");

                if (!string.IsNullOrEmpty(txtFromPolicyID.Text.ToString().Trim()) && !string.IsNullOrEmpty(txtToPolicyID.Text.ToString().Trim()))
                    sb_Query.Append(" AND P.POLICY_ID BETWEEN {6} AND {7} ");

                sb_Query.Append(" ORDER BY P.POLICY_ID, E_TYPE ");

                sb_Query.Replace("{0}", "~POLICYXENTITYTABLEID~");
                sb_Query.Replace("{1}", "~POLICYTABLEID~");
                sb_Query.Replace("{2}", "~POLICYSYSTEMID~");
                sb_Query.Replace("{3}", "~AGENT~");
                sb_Query.Replace("{4}", "~DRIVERLIST2~");
                sb_Query.Replace("{5}", "~DRIVERLIST3~");
                sb_Query.Replace("{6}", "~FROMPOLICYID~");
                sb_Query.Replace("{7}", "~TOPOLICYID~");

                obj_dictParams.Add("POLICYXENTITYTABLEID", obj_Cache.GetTableId("POLICY_X_ENTITY").ToString());
                obj_dictParams.Add("POLICYTABLEID", obj_Cache.GetTableId("POLICY").ToString());
                obj_dictParams.Add("POLICYSYSTEMID", CommonCache.si_PolicySystemID.ToString());
                obj_dictParams.Add("AGENT", s_Agents);
                obj_dictParams.Add("DRIVERLIST2", s_Drivers);
                obj_dictParams.Add("DRIVERLIST3", s_Drivers);
                obj_dictParams.Add("FROMPOLICYID", txtFromPolicyID.Text.ToString().Trim());
                obj_dictParams.Add("TOPOLICYID", txtToPolicyID.Text.ToString().Trim());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    string sFullName = string.Empty;
                    string sSelectedVal = string.Empty;

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sFullName = string.Concat(ds.Tables[0].Rows[i]["LAST_NAME"].ToString().Trim(), " ", ds.Tables[0].Rows[i]["FIRST_NAME"].ToString().Trim()).Trim();
                        sSelectedVal = string.Concat(ds.Tables[0].Rows[i]["E_TYPE"].ToString(), ds.Tables[0].Rows[i]["POLICY_ID"].ToString());
                        lblInfo2.Text = string.Concat("For Policy_Number --> ", ds.Tables[0].Rows[i]["POLICY_NUMBER"].ToString(), " \n Entity --> ", sFullName);
                        lblInfo2.Update();
                        this.Update();

                        if (!string.Equals(sSelectedVal, CommonCache.ss_LastSelectedVal))
                            SetEntityTypeSelection(false, false, false, false, string.Empty, sSelectedVal);

                        if (ds.Tables[0].Rows[i]["E_TYPE"] != null && !DBNull.Value.Equals(ds.Tables[0].Rows[i]["E_TYPE"]))
                        {
                            obj_Request = new PolicyEnquiry();
                            obj_Response = new PolicyEnquiry();

                            obj_OutputColumns.Clear();
                            obj_OutputColumns.Add(s_EntityID, ds.Tables[0].Rows[i]["ENTITY_ID"].ToString());
                            obj_OutputColumns.Add(s_PolicyID, ds.Tables[0].Rows[i]["POLICY_ID"].ToString());
                            obj_OutputColumns.Add(s_PolicyNumber, ds.Tables[0].Rows[i]["POLICY_NUMBER"].ToString());
                            obj_OutputColumns.Add(s_FullName, sFullName);

                            if (string.Equals(ds.Tables[0].Rows[i]["E_TYPE"].ToString(), s_UnitInterest))
                                FixUnitInterestEntity(ds.Tables[0].Rows[i]);
                            else if (string.Equals(ds.Tables[0].Rows[i]["E_TYPE"].ToString(), s_PolicyInterest))
                                FixPolicyInterestEntity(ds.Tables[0].Rows[i]);
                            else if (string.Equals(ds.Tables[0].Rows[i]["E_TYPE"].ToString(), s_Driver))
                                FixDriverEntity(ds.Tables[0].Rows[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixNonInsuredEntities()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                lblInfo1.Text = "Non Insured Completed....";
                lblInfo1.Update();

                lblInfo2.Text = "...";
                lblInfo2.Update();

                obj_Request = null;
                obj_Response = null;
            }
        }
        #endregion

        #region "Request --> Response from service"
        private void FixUnitInterestEntity(DataRow dr)
        {
            try
            {
                obj_Request.Token = CommonCache.ss_Session;
                obj_Request.PolicyIdentfier = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicyNumber = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicySymbol = dr["POLICY_SYMBOL"].ToString();
                obj_Request.LOB = dr["LOB"].ToString();
                obj_Request.MasterCompany = dr["MASTER_COMPANY"].ToString();
                obj_Request.Module = dr["MODULE"].ToString();
                obj_Request.Location = dr["LOCATION_COMPANY"].ToString();
                obj_Request.PolLossDt = "";
                obj_Request.PolicySystemId = CommonCache.si_PolicySystemID;
                obj_Request.UnitNumber = dr["UNIT_NUMBER"].ToString();
                obj_Request.UnitRiskLocation = dr["UNIT_RISK_LOC"].ToString();


                if (!CommonCache.sb_IsPolicyInterestSelected)
                {
                    //Service Call
                    obj_Response = obj_Pol.GetUnitInterestListResult(obj_Request);
                    //End Service Call
                    if (obj_Response != null && !string.IsNullOrEmpty(obj_Response.ResponseAcordXML))
                        SetEntityTypeSelection(false, false, false, true, obj_Response.ResponseAcordXML, string.Empty);
                }

                if (!string.IsNullOrEmpty(CommonCache.ss_OutputXML))
                {
                    GetOutputDictionary(CommonCache.ss_OutputXML, dr["ACORD_XML"].ToString(), string.Empty, Convert.ToInt32(dr["POLICY_ID"]));
                    //**************Update Entity Table******************************************
                    if (UpdateEntityTable())
                        WirteLog(true, s_EntityTable, s_UpdateMode, "FixUnitInterestEntity()");

                    //**************Update Entity_X_Address Table********************************
                    if (UpdateEntityXAddressTable(true))
                        WirteLog(true, s_EntityXAddressTable, s_UpdateMode, "FixUnitInterestEntity()");
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixUnitInterestEntity()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                obj_Request = null;
                obj_Response = null;
            }
        }
        private void FixPolicyInterestEntity(DataRow dr)
        {
            try
            {
                obj_Request.Token = CommonCache.ss_Session;
                obj_Request.PolicyIdentfier = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicyNumber = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicySymbol = dr["POLICY_SYMBOL"].ToString();
                obj_Request.LOB = dr["LOB"].ToString();
                obj_Request.MasterCompany = dr["MASTER_COMPANY"].ToString();
                obj_Request.Module = dr["MODULE"].ToString();
                obj_Request.Location = dr["LOCATION_COMPANY"].ToString();
                obj_Request.PolLossDt = "";
                obj_Request.PolicySystemId = CommonCache.si_PolicySystemID;

                if (!CommonCache.sb_IsPolicyInterestSelected)
                {
                    //Service Call
                    obj_Response = obj_Pol.GetPolicyInterestList(obj_Request);
                    //End Service Call
                    if (obj_Response != null && !string.IsNullOrEmpty(obj_Response.PolicyInterestListAcordXML))
                        SetEntityTypeSelection(false, false, true, false, obj_Response.PolicyInterestListAcordXML, string.Empty);
                }

                if (!string.IsNullOrEmpty(CommonCache.ss_OutputXML))
                {
                    GetOutputDictionary(CommonCache.ss_OutputXML, dr["ACORD_XML"].ToString(), string.Empty, Convert.ToInt32(dr["POLICY_ID"]));
                    //**************Update Entity Table******************************************
                    if (UpdateEntityTable())
                        WirteLog(true, s_EntityTable, s_UpdateMode, "FixPolicyInterestEntity()");

                    //**************Update Entity_X_Address Table********************************
                    if (UpdateEntityXAddressTable(true))
                        WirteLog(true, s_EntityXAddressTable, s_UpdateMode, "FixPolicyInterestEntity()");
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixPolicyInterestEntity()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                obj_Request = null;
                obj_Response = null;
            }
        }
        private void FixDriverEntity(DataRow dr)
        {
            try
            {
                obj_OutputColumns.Add(s_LocationCompany, dr["LOCATION_COMPANY"].ToString());
                obj_OutputColumns.Add(s_Module, dr["MODULE"].ToString());
                obj_OutputColumns.Add(s_MasterCompany, dr["MASTER_COMPANY"].ToString());

                obj_Request.Token = CommonCache.ss_Session;
                obj_Request.PolicyIdentfier = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicyNumber = dr["POLICY_NUMBER"].ToString().Trim();
                obj_Request.PolicySymbol = dr["POLICY_SYMBOL"].ToString();
                obj_Request.LOB = dr["LOB"].ToString();
                obj_Request.MasterCompany = dr["MASTER_COMPANY"].ToString();
                obj_Request.Module = dr["MODULE"].ToString();
                obj_Request.Location = dr["LOCATION_COMPANY"].ToString();
                obj_Request.PolicySystemId = CommonCache.si_PolicySystemID;

                if (!CommonCache.sb_IsDriverSelected)
                {
                    //Service Call
                    obj_Response = obj_Pol.GetPolicyDriverList(obj_Request);
                    //End Service Call
                    if (obj_Response != null && !string.IsNullOrEmpty(obj_Response.DriverListAcordXML))
                        SetEntityTypeSelection(false, true, false, false, obj_Response.DriverListAcordXML, string.Empty);
                }

                if (!string.IsNullOrEmpty(CommonCache.ss_OutputXML))
                {
                    GetOutputDictionary(CommonCache.ss_OutputXML, dr["ACORD_XML"].ToString(), dr["POLICY_ACORD_XML"].ToString(), Convert.ToInt32(dr["POLICY_ID"]));
                    //**************Update Driver Table******************************************
                    if (UpdateDriverTable())
                        WirteLog(true, s_DriverTable, s_UpdateMode, "FixDriverEntity()");
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixDriverEntity()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                obj_Request = null;
                obj_Response = null;
            }
        }

        private void FixPolicyInsuredEntity(DataRow drRow, bool bColumnExists)
        {
            int iPolicyID = 0;
            string sEntityXML = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sCity = string.Empty;
            string sStateProvCd = string.Empty;
            string sPostalCode = string.Empty;
            PolicySearch obj_Search_Request = null;
            PolicySearch obj_Search_Response = null;
            try
            {
                obj_Search_Request = new PolicySearch();
                obj_Search_Request.objSearchFilters = new SearchFilters();
                obj_Search_Response = new PolicySearch();

                sEntityXML = drRow["ACORD_XML"].ToString().Trim();
                iPolicyID = Convert.ToInt32(drRow["POLICY_ID"]);

                //***********Setting Values from Stored Accord XML******************************************************
                sAddr1 = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1", false, sEntityXML);
                try
                {
                    sAddr2 = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2", false, sEntityXML);
                }
                catch(Exception ex) { }

                sCity = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipal/GeneralPartyInfo/Addr/City", false, sEntityXML);
                sStateProvCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd", false, sEntityXML);
                sPostalCode = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode", false, sEntityXML);

                //************Preparing Dictionary to run the process***************************************************
                obj_OutputColumns.Add(s_Address1, sAddr1.Trim());
                obj_OutputColumns.Add(s_Address2, sAddr2.Trim());
                obj_OutputColumns.Add(s_City, sCity.Trim());
                obj_OutputColumns.Add(s_StateProvCd, sStateProvCd.Trim());
                obj_OutputColumns.Add(s_PostalCode, sPostalCode.Trim());

                if (!string.IsNullOrEmpty(drRow["CLIENT_SEQ_NUM"].ToString()) && bColumnExists && !string.IsNullOrEmpty(drRow["ADDRESS_SEQ_NUM"].ToString()))
                {
                    obj_OutputColumns.Add(s_AddrSeqNum, drRow["ADDRESS_SEQ_NUM"].ToString());
                    //**************Update Entity_X_Address Table********************************
                    if (UpdateEntityXAddressTable(false))
                        WirteLog(true, s_EntityXAddressTable, s_UpdateMode, "FixPolicyInsuredEntity()");
                }
                else
                {
                    obj_Search_Request.Token = CommonCache.ss_Session;
                    obj_Search_Request.objSearchFilters.PolicyNumber = drRow["POLICY_NUMBER"].ToString().Trim();
                    obj_Search_Request.objSearchFilters.PolicySymbol = drRow["POLICY_SYMBOL"].ToString();
                    obj_Search_Request.objSearchFilters.LOB = Convert.ToInt32(drRow["LOB"]);
                    obj_Search_Request.objSearchFilters.MasterCompany = drRow["MASTER_COMPANY"].ToString();
                    obj_Search_Request.objSearchFilters.Module = drRow["MODULE"].ToString();
                    obj_Search_Request.objSearchFilters.LocationCompany = drRow["LOCATION_COMPANY"].ToString();
                    obj_Search_Request.objSearchFilters.PolicySystemId = CommonCache.si_PolicySystemID;
                    //Service Call
                    obj_Search_Response = obj_Pol.GetPolicySearchResult(obj_Search_Request);
                    //End Service Call
                    if (obj_Search_Response != null && !string.IsNullOrEmpty(obj_Search_Response.ResponseXML))
                    {
                        GetOutputDictionary(obj_Search_Response.ResponseXML, sEntityXML, string.Empty, iPolicyID);
                        //**************Update Entity Table******************************************
                        if (UpdateEntityTable())
                            WirteLog(true, s_EntityTable, s_UpdateMode, "FixPolicyInterestEntity()");

                        //**************Update Entity_X_Address Table********************************
                        if (UpdateEntityXAddressTable(false))
                            WirteLog(true, s_EntityXAddressTable, s_UpdateMode, "FixPolicyInterestEntity()");
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "FixPolicyInsuredEntity()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                obj_Search_Request = null;
                obj_Search_Response = null;
            }
        }

        private void GetOutputDictionary(string pResponseXML, string sEntityXML, string sPolicyXML, int iPolicyID)
        {
            IEnumerable<XElement> oResponseElements = null;
            XElement oResponseDocElement;
            //**************************For Policy Insured***********************************************************************
            string sPICity, sPIStateProvCd, sPIShortName, sPITaxID, sPIBirthDate, sPINameType;
            string sOPICity, sOPIStateProvCd, sOPIShortName, sOPITaxID, sOPIBirthDate, sOPINameType, sOPIClientSeqNum, sOPIAddressSeqNum;

            //**************************For Policy/Unit Interest*****************************************************************
            string sILCscLocation, sILCscSeqNum, sILCommercialName, sILAddr2, sILCity, sILStateProvCd, sILInsuredOrPrincipalRoleCd;
            string sILAddr1, sILPostalCode;
            string sOCscLocation, sOCscSeqNum, sOCommercialName, sOAddr1, sOCity, sOStateProvCd, sOInsuredOrPrincipalRoleCd, sOCscClientSeqNum, sOCscAddressSeqNum;

            //**************************For Driver*******************************************************************************
            string sDDriverID, sDDriverType, sDLicenseDt, sDLicensePermitNumber, sDStateProvCd, sDBirthDt, sDCommercialName, sDDriverStatus;
            string sDFirstName, sDMiddleName, sDLastName, sDSuffix;
            string sDPolCompany;
            string sODInsLine;
            string sODRiskLocNum, sODRiskSubLocNum, sODDriverProduct, sODRecordStatus;
            string sODDriverID, sODDriverType, sODLicenseDt, sODLicensePermitNumber, sODStateProvCd, sODBirthDt, sODCommercialName, sODDriverStatus;
            try
            {
                //**************************For Policy Insured***********************************************************************
                sPICity = string.Empty; sPIStateProvCd = string.Empty; sPIShortName = string.Empty; sPITaxID = string.Empty; sPIBirthDate = string.Empty; sPINameType = string.Empty;

                //**************************For Policy/Unit Interest*****************************************************************
                sILCscLocation = string.Empty; sILCscSeqNum = string.Empty; sILCommercialName = string.Empty; sILAddr2 = string.Empty; sILCity = string.Empty;
                sILStateProvCd = string.Empty; sILInsuredOrPrincipalRoleCd = string.Empty; sILAddr1 = string.Empty; sILPostalCode = string.Empty;

                //**************************For Driver*******************************************************************************
                sDDriverID = string.Empty; sDDriverType = string.Empty; sDLicenseDt = string.Empty; sDLicensePermitNumber = string.Empty; sDStateProvCd = string.Empty; sDBirthDt = string.Empty; sDCommercialName = string.Empty; sDDriverStatus = string.Empty;
                sDPolCompany = string.Empty;
                sODDriverID = string.Empty; sODDriverType = string.Empty; sODLicenseDt = string.Empty; sODLicensePermitNumber = string.Empty; sODStateProvCd = string.Empty; sODBirthDt = string.Empty; sODCommercialName = string.Empty; sODDriverStatus = string.Empty;

                oResponseDocElement = XElement.Parse(pResponseXML);

                #region "Policy Insured"
                if (CommonCache.sb_IsPolicyInsuredSelected)
                {
                    //Do Nothing
                }
                #endregion

                #region "Drivers"
                else if (CommonCache.sb_IsDriverSelected)
                {
                    //****************************Driver*********************************************************
                    sDDriverType = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverType", true, sEntityXML);
                    sDLicensePermitNumber = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "License/LicensePermitNumber", false, sEntityXML);
                    sDStateProvCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "License/StateProvCd", false, sEntityXML);
                    sDBirthDt = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonInfo/BirthDt", false, sEntityXML);
                    sDPolCompany = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_PolCompany", true, sPolicyXML);

                    sDLastName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonName/Surname", false, sEntityXML);
                    sDFirstName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonName/GivenName", false, sEntityXML);
                    sDMiddleName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonName/OtherGivenName", false, sEntityXML);
                    sDSuffix = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonName/NameSuffix", false, sEntityXML);
                    if (!string.IsNullOrEmpty(sDFirstName))
                        sDCommercialName = sDFirstName;
                    if (!string.IsNullOrEmpty(sDMiddleName))
                        sDCommercialName = string.Concat(sDCommercialName, " ", sDMiddleName);
                    if (!string.IsNullOrEmpty(sDLastName))
                        sDCommercialName = string.Concat(sDCommercialName, " ", sDLastName);
                    if (!string.IsNullOrEmpty(sDSuffix))
                        sDCommercialName = string.Concat(sDCommercialName, " ", sDSuffix);

                    //****************************Response XML***************************************************
                    oResponseElements = oResponseDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_DriverListRs/DriverInfo");
                }
                #endregion

                #region "Policy / Unit Interest List"
                else
                {
                    //****************************Policy Interest XML********************************************
                    sILCscLocation = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_Location", true, sEntityXML);
                    sILCscSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_SequenceNum", true, sEntityXML);
                    sILCommercialName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "CommlName/CommercialName", false, sEntityXML);
                    sILAddr2 = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/Addr2", false, sEntityXML);
                    sILCity = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/City", false, sEntityXML);
                    sILStateProvCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/StateProvCd", false, sEntityXML);
                    sILInsuredOrPrincipalRoleCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd", false, sEntityXML);
                    sILAddr1 = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/Addr1", false, sEntityXML);
                    sILPostalCode = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/PostalCode", false, sEntityXML);

                    //****************************Response XML***************************************************
                    if (CommonCache.sb_IsPolicyInterestSelected)
                        oResponseElements = oResponseDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInterestListRs/InsuredOrPrincipal");
                    else if (CommonCache.sb_IsUnitInterestSelected)
                        oResponseElements = oResponseDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitInterestListRs/InsuredOrPrincipal");
                }
                #endregion

                #region "Policy Insured"
                if (CommonCache.sb_IsPolicyInsuredSelected)
                {
                    sOPIClientSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "ClientSeqNo", false, oResponseDocElement.ToString());
                    sOPIAddressSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "AddressSeqNo", false, oResponseDocElement.ToString());
                    obj_OutputColumns.Add(s_ClientSeqNum, sOPIClientSeqNum);
                    obj_OutputColumns.Add(s_AddrSeqNum, sOPIAddressSeqNum);
                }
                #endregion

                #region "Non Insured"
                else
                {
                    foreach (XElement oElement in oResponseElements)
                    {
                        #region "Drivers"
                        if (CommonCache.sb_IsDriverSelected)
                        {
                            sODDriverID = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverId", true, oElement.ToString());
                            sODDriverType = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverType", true, oElement.ToString());
                            sODLicensePermitNumber = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "License/LicensePermitNumber", false, oElement.ToString());
                            sODStateProvCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "License/StateProvCd", false, oElement.ToString());
                            sODBirthDt = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "PersonInfo/BirthDt", false, oElement.ToString());
                            sODCommercialName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "CommlName/CommercialName", false, oElement.ToString());

                            sODRiskLocNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_RiskLoc", true, oElement.ToString());
                            sODRiskSubLocNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_RiskSubLoc", true, oElement.ToString());
                            sODDriverProduct = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverProduct", true, oElement.ToString());
                            sODRecordStatus = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverStatus", false, oElement.ToString());

                            if (string.Equals(sDLicenseDt, "0"))
                                sDLicenseDt = sODLicenseDt;

                            if (string.Equals(sDDriverType, sODDriverType) &&
                                string.Equals(sDLicensePermitNumber, sODLicensePermitNumber) &&
                                string.Equals(sDStateProvCd, sODStateProvCd) &&
                                string.Equals(sDBirthDt, sODBirthDt) &&
                                string.Equals(sDCommercialName, sODCommercialName))
                            {
                                obj_Request.UnitStatus = sODRecordStatus;
                                obj_Request.UnitNumber = sODDriverID;
                                obj_Request.Product = sODDriverProduct;
                                obj_Request.UnitState = sODStateProvCd;
                                obj_Request.PolCompany = sDPolCompany;
                                obj_Request.InterestTypeCode = sODDriverType;
                                obj_Request.UnitRiskLocation = sODRiskLocNum;
                                obj_Request.UnitSubRiskLocation = sODRiskSubLocNum;
                                //Service Call
                                obj_Response = obj_Pol.GetPolicyDriverDetailResult(obj_Request);
                                //End Service Call
                                sODInsLine = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_InsLine", true, obj_Response.ResponseAcordXML);
                                sODRiskLocNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_RiskLocationNumber", true, obj_Response.ResponseAcordXML);
                                sODRiskSubLocNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_RiskSubLocationNumber", true, obj_Response.ResponseAcordXML);
                                sODDriverProduct = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_Product", true, obj_Response.ResponseAcordXML);
                                sODDriverID = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverId", true, obj_Response.ResponseAcordXML);
                                sODRecordStatus = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_DriverStatus", false, obj_Response.ResponseAcordXML);

                                obj_OutputColumns.Add(s_InsLine, sODInsLine);
                                obj_OutputColumns.Add(s_RiskLoc, sODRiskLocNum);
                                obj_OutputColumns.Add(s_RiskSubLoc, sODRiskSubLocNum);
                                obj_OutputColumns.Add(s_Product, sODDriverProduct);
                                obj_OutputColumns.Add(s_DriverID, sODDriverID);
                                obj_OutputColumns.Add(s_RecordStatus, sODRecordStatus);

                                break;
                            }
                        }
                        #endregion

                        #region "Policy / Unit Interest List"
                        else
                        {
                            sOCscLocation = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_Location", true, oElement.ToString());
                            sOCscSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_SeqNum", true, oElement.ToString());
                            sOCommercialName = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "CommlName/CommercialName", false, oElement.ToString());
                            sOAddr1 = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/Addr1", false, oElement.ToString());
                            sOCity = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/City", false, oElement.ToString());
                            sOStateProvCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "Addr/StateProvCd", false, oElement.ToString());
                            sOInsuredOrPrincipalRoleCd = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd", false, oElement.ToString());
                            sOCscClientSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_ClientSeqNum", true, oElement.ToString());
                            sOCscAddressSeqNum = obj_PolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", iPolicyID, iPolicyID, "com.csc_AddressSeqNum", true, oElement.ToString());

                            if (string.Equals(sILCscLocation, sOCscLocation) &&
                                string.Equals(sILCscSeqNum, sOCscSeqNum) &&
                                string.Equals(sILCommercialName, sOCommercialName) &&
                                string.Equals(sILAddr2, sOAddr1) &&
                                string.Equals(sILCity, sOCity) &&
                                string.Equals(sILStateProvCd, sOStateProvCd) &&
                                string.Equals(sILInsuredOrPrincipalRoleCd, sOInsuredOrPrincipalRoleCd))
                            {
                                obj_OutputColumns.Add(s_ClientSeqNum, sOCscClientSeqNum);
                                obj_OutputColumns.Add(s_AddrSeqNum, sOCscAddressSeqNum);
                                obj_OutputColumns.Add(s_Address1, sILAddr1);
                                obj_OutputColumns.Add(s_Address2, sILAddr2);
                                obj_OutputColumns.Add(s_City, sILCity);
                                obj_OutputColumns.Add(s_PostalCode, sILPostalCode);
                                obj_OutputColumns.Add(s_StateProvCd, sILStateProvCd);

                                break;
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "GetOutputDictionary()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                oResponseElements = null;
                oResponseDocElement = null;
            }
        }
        #endregion

        #region "Update Missing Columns"
        private bool UpdateEntityTable()
        {        
            string sEntityID = string.Empty;
            string sClientSeqNum = string.Empty;
            bool bReturn = false;

            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();
                sEntityID = obj_OutputColumns[s_EntityID];
                if (obj_OutputColumns.ContainsKey(s_ClientSeqNum))
                {
                    sClientSeqNum = obj_OutputColumns[s_ClientSeqNum];

                    sb_Query.Append(" UPDATE ENTITY SET CLIENT_SEQ_NUM = {0} ");
                    sb_Query.Append(" WHERE ENTITY_ID = {1}");

                    sb_Query.Replace("{0}", "~CLIENTSEQNUM~");
                    sb_Query.Replace("{1}", "~ENTITYID~");

                    obj_dictParams.Add("CLIENTSEQNUM", string.IsNullOrEmpty(sClientSeqNum) ? "NULL" : sClientSeqNum);
                    obj_dictParams.Add("ENTITYID", sEntityID);
                    DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                obj_OutputColumns.Add(s_ErrorQry, sb_Query.ToString());
                obj_OutputColumns.Add(s_ErrorMsg, ex.Message);
                lblError.Text = ex.Message;
                lblError.Update();
                bReturn = false;
            }
            finally
            {
            }

            return bReturn;
        }
        private bool UpdateEntityXAddressTable(bool isPolicyXEntity)
        {
            string sEntityID = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sCity = string.Empty;
            string sPostalCode = string.Empty;
            string sSearchString = string.Empty;
            string sStateDesc = string.Empty;
            string sStateCode = string.Empty;
            int iStateID = 0;
            StringBuilder sbTempWhere = new StringBuilder();
            int iNewAddrSeqNum = 0;
            int iAddressID = 0;
            int iRowID = 0;
            bool bReturn = false;

            try
            {
                sEntityID = obj_OutputColumns[s_EntityID];
                if (obj_OutputColumns.ContainsKey(s_AddrSeqNum))
                {
                    iNewAddrSeqNum = Convert.ToInt32(obj_OutputColumns[s_AddrSeqNum]);
                    if (obj_OutputColumns.ContainsKey(s_Address1) && !string.IsNullOrEmpty(obj_OutputColumns[s_Address1]))
                    {
                        if (obj_OutputColumns[s_Address1].Length > CommonCache.GetDBColumnLength(CommonCache.cst_EntityXAddress, CommonCache.csc_Addr1))        //Need to Truncate
                            sAddr1 = obj_OutputColumns[s_Address1].Substring(0, CommonCache.GetDBColumnLength(CommonCache.cst_EntityXAddress, CommonCache.csc_Addr1));
                        else
                            sAddr1 = obj_OutputColumns[s_Address1];
                    }
                    if (obj_OutputColumns.ContainsKey(s_Address2) && !string.IsNullOrEmpty(obj_OutputColumns[s_Address2]))
                    {
                        if (obj_OutputColumns[s_Address2].Length > CommonCache.GetDBColumnLength(CommonCache.cst_EntityXAddress, CommonCache.csc_Addr2))        //Need to Truncate
                            sAddr2 = obj_OutputColumns[s_Address2].Substring(0, CommonCache.GetDBColumnLength(CommonCache.cst_EntityXAddress, CommonCache.csc_Addr2));
                        else
                            sAddr2 = obj_OutputColumns[s_Address2];
                    }
                    if (obj_OutputColumns.ContainsKey(s_City) && !string.IsNullOrEmpty(obj_OutputColumns[s_City]))
                        sCity = obj_OutputColumns[s_City];
                    if (obj_OutputColumns.ContainsKey(s_PostalCode) && !string.IsNullOrEmpty(obj_OutputColumns[s_PostalCode]))
                        sPostalCode = obj_OutputColumns[s_PostalCode];
                    if (obj_OutputColumns.ContainsKey(s_StateProvCd) && !string.IsNullOrEmpty(obj_OutputColumns[s_StateProvCd]))
                        iStateID = obj_PolicyInterface.GetRMXCodeIdFromPSMappedCode(obj_OutputColumns[s_StateProvCd], "STATES", "PointEntityCleanUpTool", CommonCache.si_PolicySystemID.ToString());

                    //To Remove : String Query for testing
                    //string sWhere;
                    //StringBuilder sb_Query1 = new StringBuilder();
                    //sWhere = string.Concat(" ENTITY_ID = ", sEntityID);
                    //sWhere = string.Concat(sWhere, " AND LTRIM(RTRIM(ISNULL(ADDR1, ''))) = '", sAddr1, "'");
                    //if (!string.IsNullOrEmpty(sAddr2))
                    //    sWhere = string.Concat(sWhere, " AND LTRIM(RTRIM(ISNULL(ADDR2, ''))) = '", sAddr2, "'");
                    //sWhere = string.Concat(sWhere, " AND ISNULL(CITY, '') = '", sCity, "'");
                    //sWhere = string.Concat(sWhere, " AND ISNULL(STATE_ID, '') = '", iStateID, "'");
                    //sWhere = string.Concat(sWhere, " AND ISNULL(ZIP_CODE, '') = '", sPostalCode, "'");
                    //sWhere = string.Concat(sWhere, " AND (ISNULL(ADDRESS_SEQ_NUM, '') = '' Or ADDRESS_SEQ_NUM = ", iNewAddrSeqNum, ")");

                    //sb_Query1.Append(" SELECT ADDRESS_ID, ADDRESS_SEQ_NUM ");
                    //sb_Query1.Append(" FROM ENTITY_X_ADDRESSES ");
                    //sb_Query1.Append(string.Concat(" WHERE ", sWhere));
                    //End To Remove

                    //**Need to write for Oracle
                    sb_Query.Clear();
                    obj_dictParams.Clear();
                    //Ashish Ahuja - Address Master
                    // sbTempWhere.Append(" WHERE ENTITY_ID = {0} ");
                    //if (!string.IsNullOrEmpty(sAddr1))
                    //    sbTempWhere.Append(" AND LTRIM(RTRIM(ISNULL(ADDR1, ''))) = {1} ");
                    //else
                    //    sbTempWhere.Append(" AND ISNULL(ADDR1, '') = '' ");
                    //if (!string.IsNullOrEmpty(sAddr2))
                    //    sbTempWhere.Append(" AND LTRIM(RTRIM(ISNULL(ADDR2, ''))) = {2} ");
                    //if(!string.IsNullOrEmpty(sCity))
                    //    sbTempWhere.Append(" AND ISNULL(CITY, '') = {3} ");
                    //else
                    //    sbTempWhere.Append(" AND ISNULL(CITY, '') = '' ");
                    //sbTempWhere.Append(" AND ISNULL(STATE_ID, '') = {4} ");
                    //if(!string.IsNullOrEmpty(sPostalCode))
                    //    sbTempWhere.Append(" AND ISNULL(ZIP_CODE, '') = {5} ");
                    //else
                    //    sbTempWhere.Append(" AND ISNULL(ZIP_CODE, '') = '' ");
                    //sbTempWhere.Append(" AND (ISNULL(ADDRESS_SEQ_NUM, '') = '' Or ADDRESS_SEQ_NUM = {6}) ");

                    //sbTempWhere.Replace("{0}", "~ENTITYID~");
                    //if (!string.IsNullOrEmpty(sAddr1))
                    //    sbTempWhere.Replace("{1}", "~ADDR1~");
                    //if (!string.IsNullOrEmpty(sAddr2))
                    //    sbTempWhere.Replace("{2}", "~ADDR2~");
                    //if (!string.IsNullOrEmpty(sCity)) 
                    //    sbTempWhere.Replace("{3}", "~CITY~");
                    //sbTempWhere.Replace("{4}", "~STATEID~");
                    //if (!string.IsNullOrEmpty(sPostalCode)) 
                    //    sbTempWhere.Replace("{5}", "~ZIPCODE~");
                    //sbTempWhere.Replace("{6}", "~ADDRESSSEQNUM~");

                    //obj_dictParams.Add("ENTITYID", sEntityID);
                    //if(!string.IsNullOrEmpty(sAddr1))
                    //    obj_dictParams.Add("ADDR1", sAddr1);                    
                    //if (!string.IsNullOrEmpty(sAddr2))
                    //    obj_dictParams.Add("ADDR2", sAddr2);
                    //if (!string.IsNullOrEmpty(sCity)) 
                    //    obj_dictParams.Add("CITY", sCity);
                    //obj_dictParams.Add("STATEID", iStateID.ToString());
                    //if (!string.IsNullOrEmpty(sPostalCode)) 
                    //    obj_dictParams.Add("ZIPCODE", sPostalCode);
                    //obj_dictParams.Add("ADDRESSSEQNUM", iNewAddrSeqNum.ToString());

                    //sb_Query.Append(" SELECT ADDRESS_ID, ADDRESS_SEQ_NUM ");
                    //sb_Query.Append(string.Concat(" FROM ENTITY_X_ADDRESSES ", sbTempWhere.ToString()));
                    sbTempWhere.Append(" ON EA.ENTITY_ID = {0} AND A.ADDRESS_ID = EA.ADDRESS_ID ");
                    if (!string.IsNullOrEmpty(sAddr1))
                        sbTempWhere.Append(" AND LTRIM(RTRIM(ISNULL(A.ADDR1, ''))) = {1} ");
                    else
                        sbTempWhere.Append(" AND ISNULL(A.ADDR1, '') = '' ");
                    if (!string.IsNullOrEmpty(sAddr2))
                        sbTempWhere.Append(" AND LTRIM(RTRIM(ISNULL(A.ADDR2, ''))) = {2} ");
                    if(!string.IsNullOrEmpty(sCity))
                        sbTempWhere.Append(" AND ISNULL(A.CITY, '') = {3} ");
                    else
                        sbTempWhere.Append(" AND ISNULL(A.CITY, '') = '' ");
                    sbTempWhere.Append(" AND ISNULL(A.STATE_ID, '') = {4} ");
                    if(!string.IsNullOrEmpty(sPostalCode))
                        sbTempWhere.Append(" AND ISNULL(A.ZIP_CODE, '') = {5} ");
                    else
                        sbTempWhere.Append(" AND ISNULL(A.ZIP_CODE, '') = '' ");
                    sbTempWhere.Append(" AND (ISNULL(A.ADDRESS_SEQ_NUM, '') = '' Or A.ADDRESS_SEQ_NUM = {6}) ");

                    sbTempWhere.Replace("{0}", "~ENTITYID~");
                    if (!string.IsNullOrEmpty(sAddr1))
                        sbTempWhere.Replace("{1}", "~ADDR1~");
                    if (!string.IsNullOrEmpty(sAddr2))
                        sbTempWhere.Replace("{2}", "~ADDR2~");
                    if (!string.IsNullOrEmpty(sCity)) 
                        sbTempWhere.Replace("{3}", "~CITY~");
                    sbTempWhere.Replace("{4}", "~STATEID~");
                    if (!string.IsNullOrEmpty(sPostalCode)) 
                        sbTempWhere.Replace("{5}", "~ZIPCODE~");
                    sbTempWhere.Replace("{6}", "~ADDRESSSEQNUM~");

                    obj_dictParams.Add("ENTITYID", sEntityID);
                    if(!string.IsNullOrEmpty(sAddr1))
                        obj_dictParams.Add("ADDR1", sAddr1);                    
                    if (!string.IsNullOrEmpty(sAddr2))
                        obj_dictParams.Add("ADDR2", sAddr2);
                    if (!string.IsNullOrEmpty(sCity)) 
                        obj_dictParams.Add("CITY", sCity);
                    obj_dictParams.Add("STATEID", iStateID.ToString());
                    if (!string.IsNullOrEmpty(sPostalCode)) 
                        obj_dictParams.Add("ZIPCODE", sPostalCode);
                    obj_dictParams.Add("ADDRESSSEQNUM", iNewAddrSeqNum.ToString());

                    sb_Query.Append(" SELECT A.ADDRESS_ID, A.ADDRESS_SEQ_NUM ");
                    sb_Query.Append(string.Concat(" FROM ADDRESS A INNER JOIN ENTITY_X_ADDRESSES EA ", sbTempWhere.ToString()));

                    using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            iAddressID = Convert.ToInt32(ds.Tables[0].Rows[0]["ADDRESS_ID"]);

                            sb_Query.Clear();
                            //Ashish Ahuja Address Master
                            //sb_Query.Append(string.Concat(" UPDATE ENTITY_X_ADDRESSES SET ADDRESS_SEQ_NUM = {7} ", sbTempWhere.ToString()));
                            sb_Query.Append(string.Concat(" UPDATE A SET A.ADDRESS_SEQ_NUM = 1  FROM ADDRESS AS A INNER JOIN ENTITY_X_ADDRESSES EA ", sbTempWhere.ToString()));
                            sb_Query.Replace("{7}", "~NEWADDRESSSEQNUM~");
                            obj_dictParams.Add("NEWADDRESSSEQNUM", iNewAddrSeqNum.ToString());

                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);                            
                        }
                        else
                        {
                            //Ashish Ahuja - Address Master
                            //iAddressID = Utilities.GetNextUID(CommonCache.ss_DSNConnectionString, "ENTITY_X_ADDRESSES", m_iClientId);
                            iAddressID = Utilities.GetNextUID(CommonCache.ss_DSNConnectionString, "ADDRESS", m_iClientId);
                            iRowID = Utilities.GetNextUID(CommonCache.ss_DSNConnectionString, "ENTITY_X_ADDRESSES", m_iClientId);
                            sb_Query.Clear();
                            obj_dictParams.Clear();
                            //Ashish Ahuja - Address Master
                            sb_Query.Append(" INSERT INTO ENTITY_X_ADDRESSES(ENTITY_ID, ADDRESS_ID,ROW_ID) ");
                            sb_Query.Append(" VALUES ({0}, {1}, {2})");

                            sb_Query.Replace("{0}", "~ENTITYID~");
                            sb_Query.Replace("{1}", "~ADDRESSID~");
                            sb_Query.Replace("{2}", "~ROWID~");
                            obj_dictParams.Add("ENTITYID", sEntityID);
                            obj_dictParams.Add("ADDRESSID", iAddressID.ToString());
                            obj_dictParams.Add("ROWID", iRowID.ToString());
                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);

                            sb_Query.Clear();
                            obj_dictParams.Clear();

                            sb_Query.Append(" INSERT INTO ADDRESS(ADDRESS_ID, ADDR1, ADDR2, CITY, STATE_ID, ZIP_CODE, ADDRESS_SEQ_NUM,SEARCH_STRING,ADDED_BY_USER,UPDATED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD) ");
                            sb_Query.Append(" VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7},'" + CommonCache.ss_UName + "','" + CommonCache.ss_UName + "','" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "')");

                            sb_Query.Replace("{0}", "~ADDRESSID~");
                            sb_Query.Replace("{1}", "~ADDR1~");
                            sb_Query.Replace("{2}", "~ADDR2~");
                            sb_Query.Replace("{3}", "~CITY~");
                            sb_Query.Replace("{4}", "~STATEID~");
                            sb_Query.Replace("{5}", "~ZIPCODE~");
                            sb_Query.Replace("{6}", "~ADDRESSSEQNUM~");
                            sb_Query.Replace("{7}", "~SEARCHSTRING~");

                            obj_dictParams.Add("ADDRESSID", iAddressID.ToString());
                            obj_dictParams.Add("ADDR1", sAddr1);
                            obj_dictParams.Add("ADDR2", sAddr2);
                            obj_dictParams.Add("CITY", sCity);
                            obj_dictParams.Add("STATEID", iStateID.ToString());
                            obj_dictParams.Add("ZIPCODE", sPostalCode);
                            obj_dictParams.Add("ADDRESSSEQNUM", iNewAddrSeqNum.ToString());
                            LocalCache objCache = null;
                            objCache = new LocalCache(CommonCache.ss_DSNConnectionString, m_iClientId);
                            objCache.GetStateInfo(iStateID, ref sStateCode, ref sStateDesc);
                           
                            sSearchString = (sAddr1 + sAddr2 + sCity + sStateDesc + sPostalCode + iNewAddrSeqNum).ToLower().Replace(" ", "");
                            obj_dictParams.Add("SEARCHSTRING", sSearchString);
                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                        }
                    }

                    //****************Update Policy_X_Entity & Policy_X_Insured Table's Address_ID******************************
                    sb_Query.Clear();
                    obj_dictParams.Clear();
                    if (isPolicyXEntity)
                        sb_Query.Append(" UPDATE POLICY_X_ENTITY SET ADDRESS_ID = {0} WHERE POLICY_ID = {1} AND ENTITY_ID = {2} ");
                    else
                        sb_Query.Append(" UPDATE POLICY_X_INSURED SET ADDRESS_ID = {0} WHERE POLICY_ID = {1} AND INSURED_EID = {2} ");

                    sb_Query.Replace("{0}", "~ADDRESSID~");
                    sb_Query.Replace("{1}", "~POLICYID~");
                    sb_Query.Replace("{2}", "~ENTITYID~");

                    obj_dictParams.Add("ADDRESSID", iAddressID.ToString());
                    obj_dictParams.Add("POLICYID", obj_OutputColumns[s_PolicyID]);
                    obj_dictParams.Add("ENTITYID", sEntityID);
                    DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                    //**********************************************************************************************************
                }
            }
            catch (Exception ex)
            {
                obj_OutputColumns.Add(s_ErrorQry, sb_Query.ToString());
                obj_OutputColumns.Add(s_ErrorMsg, ex.Message);
                lblError.Text = ex.Message;
                lblError.Update();
                bReturn = false;
            }
            finally
            {
                sbTempWhere = null;
            }

            return bReturn;
        }
        private bool UpdateDriverTable()
        {
            string sEntityID = string.Empty;
            string sInsLine = string.Empty;
            string sRiskLocation = string.Empty;
            string sRiskSubLocation = string.Empty;
            string sProduct = string.Empty;
            string sDriverStatus = string.Empty;
            int iDriverID = 0;
            bool bReturn = false;

            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();
                sEntityID = obj_OutputColumns[s_EntityID];
                if (obj_OutputColumns.ContainsKey(s_InsLine) && !string.IsNullOrEmpty(obj_OutputColumns[s_InsLine]))
                    sInsLine = obj_OutputColumns[s_InsLine];
                if (obj_OutputColumns.ContainsKey(s_RiskLoc) && !string.IsNullOrEmpty(obj_OutputColumns[s_RiskLoc]))
                    sRiskLocation = obj_OutputColumns[s_RiskLoc];
                if (obj_OutputColumns.ContainsKey(s_RiskSubLoc) && !string.IsNullOrEmpty(obj_OutputColumns[s_RiskSubLoc]))
                    sRiskSubLocation = obj_OutputColumns[s_RiskSubLoc];
                if (obj_OutputColumns.ContainsKey(s_Product) && !string.IsNullOrEmpty(obj_OutputColumns[s_Product]))
                    sProduct = obj_OutputColumns[s_Product];
                if (obj_OutputColumns.ContainsKey(s_RecordStatus) && !string.IsNullOrEmpty(obj_OutputColumns[s_RecordStatus]))
                    sDriverStatus = obj_OutputColumns[s_RecordStatus];
                iDriverID = Convert.ToInt32(obj_OutputColumns[s_DriverID]);

                sb_Query.Append(" UPDATE DRIVER SET PS_INS_LINE = {0}, PS_RISK_LOC = {1}, PS_RISK_SUB_LOC = {2}, PS_PRODUCT = {3}, ");
                sb_Query.Append(" PS_DRIVER_ID = {4}, PS_RECORD_STATUS = {5} WHERE DRIVER_EID = {6} ");

                sb_Query.Replace("{0}", "~PSINSLINE~");
                sb_Query.Replace("{1}", "~PSRISKLOC~");
                sb_Query.Replace("{2}", "~PSRISKSUBLOC~");
                sb_Query.Replace("{3}", "~PSPRODUCT~");
                sb_Query.Replace("{4}", "~PSDRIVERID~");
                sb_Query.Replace("{5}", "~PSRECORDSTATUS~");
                sb_Query.Replace("{6}", "~DRIVEREID~");

                obj_dictParams.Add("PSINSLINE", string.IsNullOrEmpty(sInsLine) ? "NULL" : sInsLine);
                obj_dictParams.Add("PSRISKLOC", string.IsNullOrEmpty(sRiskLocation) ? "NULL" : sRiskLocation);
                obj_dictParams.Add("PSRISKSUBLOC", string.IsNullOrEmpty(sRiskSubLocation) ? "NULL" : sRiskSubLocation);
                obj_dictParams.Add("PSPRODUCT", string.IsNullOrEmpty(sProduct) ? "NULL" : sProduct);
                obj_dictParams.Add("PSDRIVERID", int.Equals(iDriverID, 0) ? "NULL" : iDriverID.ToString());
                obj_dictParams.Add("PSRECORDSTATUS", string.IsNullOrEmpty(sDriverStatus) ? "NULL" : sDriverStatus);
                obj_dictParams.Add("DRIVEREID", sEntityID);
                DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);

                bReturn = true;
            }
            catch (Exception ex)
            {
                obj_OutputColumns.Add(s_ErrorQry, sb_Query.ToString());
                obj_OutputColumns.Add(s_ErrorMsg, ex.Message);
                lblError.Text = ex.Message;
                lblError.Update();
                bReturn = false;
            }
            finally
            {
            }

            return bReturn;
        }
        #endregion
        #endregion

        #region "Cleanup Start"
        private void CleanDriverTable()
        {
            obj_dictParams.Clear();
            sb_Query.Clear();
            lblInfo1.Text = "Driver Cleanup Start...";
            lblInfo1.Update();
            try
            {
                sb_Query.Append(" SELECT D.DRIVER_EID AS ENTITY_ID, P.LOCATION_COMPANY, P.MASTER_COMPANY, P.POLICY_SYMBOL, P.POLICY_NUMBER, P.MODULE, ");
                sb_Query.Append(" D.PS_INS_LINE, D.PS_RISK_LOC, D.PS_RISK_SUB_LOC, D.PS_PRODUCT, D.PS_DRIVER_ID, D.PS_RECORD_STATUS, E.DELETED_FLAG ");
                sb_Query.Append(" FROM DRIVER D ");
                sb_Query.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = D.DRIVER_EID ");
                sb_Query.Append(" INNER JOIN POLICY_X_ENTITY PXE ON PXE.ENTITY_ID = D.DRIVER_EID ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = PXE.POLICY_ID ");
                sb_Query.Append(" WHERE P.POLICY_SYSTEM_ID = {0} ");
                sb_Query.Append(" GROUP BY D.DRIVER_EID, P.LOCATION_COMPANY, P.MASTER_COMPANY, P.POLICY_SYMBOL, P.POLICY_NUMBER, P.MODULE, ");
                sb_Query.Append(" D.PS_INS_LINE, D.PS_RISK_LOC, D.PS_RISK_SUB_LOC, D.PS_PRODUCT, D.PS_DRIVER_ID, D.PS_RECORD_STATUS, E.DELETED_FLAG ");
                sb_Query.Append(" ORDER BY P.LOCATION_COMPANY, P.MASTER_COMPANY, P.POLICY_SYMBOL, P.POLICY_NUMBER, P.MODULE, ");
                sb_Query.Append(" D.PS_INS_LINE, D.PS_RISK_LOC, D.PS_RISK_SUB_LOC, D.PS_PRODUCT, D.PS_DRIVER_ID, D.PS_RECORD_STATUS ");

                sb_Query.Replace("{0}", "~POLICYSYSTEMID~");

                obj_dictParams.Add("POLICYSYSTEMID", CommonCache.si_PolicySystemID.ToString());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    DataRow drGoodRow = null;
                    DataTable dt = ds.Tables[0];
                    int iBadEntityID = 0;
                    int iGoodEntityID = 0;
                    string sGoodEntity = string.Empty;
                    string sAddedString = string.Empty;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        sAddedString = string.Concat(dt.Rows[i]["LOCATION_COMPANY"].ToString(), "|", dt.Rows[i]["MASTER_COMPANY"].ToString(), "|", dt.Rows[i]["POLICY_SYMBOL"].ToString(), "|", dt.Rows[i]["POLICY_NUMBER"].ToString(), "|", dt.Rows[i]["MODULE"].ToString(), "|", dt.Rows[i]["PS_INS_LINE"].ToString(), "|", dt.Rows[i]["PS_RISK_LOC"].ToString(), "|", dt.Rows[i]["PS_RISK_SUB_LOC"].ToString(), "|", dt.Rows[i]["PS_PRODUCT"].ToString(), "|", dt.Rows[i]["PS_DRIVER_ID"].ToString(), "|", dt.Rows[i]["PS_RECORD_STATUS"].ToString());
                        if (!string.Equals(sGoodEntity, sAddedString) || int.Equals(iGoodEntityID, Convert.ToInt32(dt.Rows[i]["ENTITY_ID"])))
                        {
                            iGoodEntityID = Convert.ToInt32(dt.Rows[i]["ENTITY_ID"]);
                            sGoodEntity = sAddedString;
                            drGoodRow = dt.Rows[i];

                            if (int.Equals(Convert.ToInt32(dt.Rows[i]["DELETED_FLAG"]), -1))                //Good Entity is soft Deleted
                            {
                                //****************Remove Soft Deletition*******************************************************************
                                sb_Query.Clear();
                                obj_dictParams.Clear();

                                sb_Query.Append(" UPDATE ENTITY SET DELETED_FLAG = 0 WHERE ENTITY_ID = {0} ");
                                sb_Query.Replace("{0}", "~ENTITYID~");
                                obj_dictParams.Add("ENTITYID", drGoodRow["ENTITY_ID"].ToString());
                                DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                                //**********************************************************************************************************
                            }
                        }
                        else
                        {
                            iBadEntityID = Convert.ToInt32(dt.Rows[i]["ENTITY_ID"]);
                            //****************Set Entity References*********************************************************************
                            SetEntityReferences(drGoodRow, dt.Rows[i]);
                            //**********************************************************************************************************

                            //****************Soft Deletition of duplicate Entity_ID****************************************************
                            sb_Query.Clear();
                            obj_dictParams.Clear();

                            sb_Query.Append(" UPDATE ENTITY SET DELETED_FLAG = -1 WHERE ENTITY_ID = {0} ");
                            sb_Query.Replace("{0}", "~ENTITYID~");
                            obj_dictParams.Add("ENTITYID", iBadEntityID.ToString());
                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                            //**********************************************************************************************************
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "CleanDriverTable()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                lblInfo1.Text = "Driver Cleanup Completed...";
                lblInfo1.Update();

                lblInfo2.Text = "...";
                lblInfo2.Update();
            }
        }
        private void CleanEntityTable()
        {
            obj_dictParams.Clear();
            sb_Query.Clear();
            lblInfo1.Text = "Cleanup Start...";
            lblInfo1.Update();
            try
            {
                sb_Query.Append(" SELECT TBL.ENTITY_ID, TBL.ENTITY_TABLE_ID, TBL.CLIENT_SEQ_NUM, TBL.DELETED_FLAG, MIN(EXA.ADDRESS_SEQ_NUM) AS ADDRESS_SEQ_NUM ");
                sb_Query.Append(" FROM ( ");
                sb_Query.Append(" SELECT E.ENTITY_ID, E.ENTITY_TABLE_ID, E.CLIENT_SEQ_NUM, PXE.POLICY_ID, pxe.ADDRESS_ID, 0 AS ISINSURED, E.DELETED_FLAG ");
                sb_Query.Append(" FROM POLICY_X_ENTITY PXE ");
                sb_Query.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = PXE.ENTITY_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT E.ENTITY_ID, E.ENTITY_TABLE_ID, E.CLIENT_SEQ_NUM, PXI.POLICY_ID, pxi.ADDRESS_ID, -1 AS ISINSURED, E.DELETED_FLAG   ");
                sb_Query.Append(" FROM POLICY_X_INSURED PXI ");
                sb_Query.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = PXI.INSURED_EID ");
                sb_Query.Append(" ) TBL ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = TBL.POLICY_ID ");
                sb_Query.Append(" INNER JOIN ENTITY_X_ADDRESSES EXA ON EXA.ADDRESS_ID = TBL.ADDRESS_ID ");
                sb_Query.Append(" WHERE P.POLICY_SYSTEM_ID = {0} ");
                sb_Query.Append(" AND (TBL.CLIENT_SEQ_NUM IS NOT NULL OR LTRIM(RTRIM(TBL.CLIENT_SEQ_NUM)) <> '') ");
                sb_Query.Append(" AND CAST(TBL.CLIENT_SEQ_NUM AS INT) > 0 ");
                sb_Query.Append(" AND TBL.ADDRESS_ID IS NOT NULL ");
                sb_Query.Append(" GROUP BY TBL.ENTITY_ID, TBL.ENTITY_TABLE_ID, TBL.CLIENT_SEQ_NUM, TBL.DELETED_FLAG ");
                sb_Query.Append(" ORDER BY TBL.ENTITY_TABLE_ID, CLIENT_SEQ_NUM, TBL.ENTITY_ID ");

                sb_Query.Replace("{0}", "~POLICYSYSTEMID~");

                obj_dictParams.Add("POLICYSYSTEMID", CommonCache.si_PolicySystemID.ToString());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    DataRow drGoodRow = null;
                    int iBadEntityID = 0;
                    int iGoodEntityID = 0;
                    string sGoodEntity = string.Empty;
                    string sAddedString = string.Empty;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sAddedString = string.Concat(ds.Tables[0].Rows[i]["ENTITY_TABLE_ID"].ToString(), "|", ds.Tables[0].Rows[i]["CLIENT_SEQ_NUM"].ToString());
                        if (!string.Equals(sGoodEntity, sAddedString) || int.Equals(iGoodEntityID, Convert.ToInt32(ds.Tables[0].Rows[i]["ENTITY_ID"])))
                        {
                            iGoodEntityID = Convert.ToInt32(ds.Tables[0].Rows[i]["ENTITY_ID"]);
                            sGoodEntity = sAddedString;
                            drGoodRow = ds.Tables[0].Rows[i];

                            if (int.Equals(Convert.ToInt32(ds.Tables[0].Rows[i]["DELETED_FLAG"]), -1))                //Good Entity is soft Deleted
                            {
                                //****************Remove Soft Deletition*******************************************************************
                                sb_Query.Clear();
                                obj_dictParams.Clear();

                                sb_Query.Append(" UPDATE ENTITY SET DELETED_FLAG = 0 WHERE ENTITY_ID = {0} ");
                                sb_Query.Replace("{0}", "~ENTITYID~");
                                obj_dictParams.Add("ENTITYID", drGoodRow["ENTITY_ID"].ToString());
                                DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                                //**********************************************************************************************************
                            }

                        }
                        else
                        {
                            iBadEntityID = Convert.ToInt32(ds.Tables[0].Rows[i]["ENTITY_ID"]);
                            //****************Set Entity References*********************************************************************
                            SetEntityReferences(drGoodRow, ds.Tables[0].Rows[i]);
                            //**********************************************************************************************************

                            //****************Soft Deletition of duplicate Entity_ID****************************************************
                            sb_Query.Clear();
                            obj_dictParams.Clear();

                            sb_Query.Append(" UPDATE ENTITY SET DELETED_FLAG = -1 WHERE ENTITY_ID = {0} ");
                            sb_Query.Replace("{0}", "~ENTITYID~");
                            obj_dictParams.Add("ENTITYID", iBadEntityID.ToString());
                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                            //**********************************************************************************************************
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "CleanEntityTable()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                lblInfo1.Text = "Cleanup Completed...";
                lblInfo1.Update();

                lblInfo2.Text = "...";
                lblInfo2.Update();
            }
        }

        private void SetEntityReferences(DataRow dtGoodRow, DataRow dtBadRow)
        {
            int iGoodEntityID = Convert.ToInt32(dtGoodRow["ENTITY_ID"]);
            int iBadEntityID = Convert.ToInt32(dtBadRow["ENTITY_ID"]);

            SetPendingReferences(iGoodEntityID, iBadEntityID);
            SetPolicyXReferences(dtGoodRow, dtBadRow);
        }

        private void SetPolicyXReferences(DataRow dtGoodRow, DataRow dtBadRow)
        {
            int iGoodEntityID = 0;
            int iGoodAddressSeqNum = 0;

            int iBadEntityID = 0;
            int iBadAddressID = 0;
            bool IsInsured = false;
            int iBadAddressSeqNum = 0;
            sb_Query.Clear();
            obj_dictParams.Clear();
            lblInfo1.Text = "Updating References for Policy Tables...";
            lblInfo1.Update();
            try
            {
                iGoodEntityID = Convert.ToInt32(dtGoodRow["ENTITY_ID"]);
                iGoodAddressSeqNum = Convert.ToInt32(dtGoodRow["ADDRESS_SEQ_NUM"]);

                iBadEntityID = Convert.ToInt32(dtBadRow["ENTITY_ID"]);
                //Ashish Ahuja Address Master 
                sb_Query.Append(" SELECT '' POLICYENTITY_ROWID, PXI.INSURED_EID ENTITY_ID, PXI.POLICY_ID, PXI.ADDRESS_ID, A.ADDRESS_SEQ_NUM, -1 AS ISINSURED ");
                sb_Query.Append(" FROM POLICY_X_INSURED PXI ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = PXI.POLICY_ID ");
                sb_Query.Append(" INNER JOIN ENTITY_X_ADDRESSES EXA ON EXA.ADDRESS_ID = PXI.ADDRESS_ID ");
                sb_Query.Append(" INNER JOIN ADDRESS A ON A.ADDRESS_ID = PXI.ADDRESS_ID ");
                sb_Query.Append(" WHERE PXI.INSURED_EID = {0} ");
                sb_Query.Append(" AND P.POLICY_SYSTEM_ID = {1} ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT PXE.POLICYENTITY_ROWID, PXE.ENTITY_ID, PXE.POLICY_ID, PXE.ADDRESS_ID, A.ADDRESS_SEQ_NUM, 0 AS ISINSURED ");
                sb_Query.Append(" FROM POLICY_X_ENTITY PXE ");
                sb_Query.Append(" INNER JOIN POLICY P ON P.POLICY_ID = PXE.POLICY_ID ");
                sb_Query.Append(" INNER JOIN ENTITY_X_ADDRESSES EXA ON EXA.ADDRESS_ID = PXE.ADDRESS_ID ");
                sb_Query.Append(" INNER JOIN ADDRESS A ON A.ADDRESS_ID = PXE.ADDRESS_ID ");
                sb_Query.Append(" WHERE PXE.ENTITY_ID = {0} ");
                sb_Query.Append(" AND P.POLICY_SYSTEM_ID = {1} ");

                sb_Query.Replace("{0}", "~ENTITYID~");
                sb_Query.Replace("{1}", "~POLICYSYSTEMID~");

                obj_dictParams.Add("ENTITYID", iBadEntityID.ToString());
                obj_dictParams.Add("POLICYSYSTEMID", CommonCache.si_PolicySystemID.ToString());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    int iPolicyId = 0;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sb_Query.Clear();
                        obj_dictParams.Clear();

                        iPolicyId = Convert.ToInt32(ds.Tables[0].Rows[i]["POLICY_ID"]);
                        iBadAddressID = Convert.ToInt32(ds.Tables[0].Rows[i]["ADDRESS_ID"]);
                        iBadAddressSeqNum = Convert.ToInt32(ds.Tables[0].Rows[i]["ADDRESS_SEQ_NUM"]);
                        IsInsured = int.Equals(Convert.ToInt32(ds.Tables[0].Rows[i]["ISINSURED"]), -1) ? true : false;

                        if (IsInsured)         //For Insured
                            sb_Query.Append(" UPDATE POLICY_X_INSURED SET INSURED_EID = {0} WHERE INSURED_EID = {1} AND ADDRESS_ID = {2} AND POLICY_ID = {3} ");
                        else
                            sb_Query.Append(" UPDATE POLICY_X_ENTITY SET ENTITY_ID = {0}  WHERE ENTITY_ID = {1} AND ADDRESS_ID = {2}  AND POLICY_ID = {3} ");

                        sb_Query.Replace("{0}", "~GOODENTITYID~");
                        sb_Query.Replace("{1}", "~BADENTITYID~");
                        sb_Query.Replace("{2}", "~BADADDRESSID~");
                        sb_Query.Replace("{3}", "~POLICYID~");

                        obj_dictParams.Add("GOODENTITYID", iGoodEntityID.ToString());
                        obj_dictParams.Add("BADENTITYID", iBadEntityID.ToString());
                        obj_dictParams.Add("BADADDRESSID", iBadAddressID.ToString());
                        obj_dictParams.Add("POLICYID", iPolicyId.ToString());
                        DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                        //Start Logging
                        if (IsInsured)         //For Insured
                            WirteUpdateLog("POLICY_X_INSURED", "INSURED_EID", string.Concat("POLICY_ID/ADDRESS_ID --> ", iPolicyId, "/", iBadAddressID), iBadEntityID.ToString(), iGoodEntityID.ToString(), "SetPolicyXReferences");
                        else
                            WirteUpdateLog("POLICY_X_ENTITY", "ENTITY_ID", string.Concat("POLICYENTITY_ROWID --> ", ds.Tables[0].Rows[i]["POLICYENTITY_ROWID"].ToString()), iBadEntityID.ToString(), iGoodEntityID.ToString(), "SetPolicyXReferences");
                        //End Logging

                        if (!int.Equals(iGoodAddressSeqNum, iBadAddressSeqNum))
                        {
                            sb_Query.Clear();
                            obj_dictParams.Clear();
                            sb_Query.Append(" UPDATE ENTITY_X_ADDRESSES SET ENTITY_ID = {0} WHERE ENTITY_ID = {1} AND ADDRESS_ID = {2} ");

                            sb_Query.Replace("{0}", "~GOODENTITYID~");
                            sb_Query.Replace("{1}", "~BADENTITYID~");
                            sb_Query.Replace("{2}", "~BADADDRESSID~");

                            obj_dictParams.Add("GOODENTITYID", iGoodEntityID.ToString());
                            obj_dictParams.Add("BADENTITYID", iBadEntityID.ToString());
                            obj_dictParams.Add("BADADDRESSID", iBadAddressID.ToString());
                            DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);

                            //Start Logging
                            WirteUpdateLog("ENTITY_X_ADDRESSES", "ENTITY_ID", string.Concat("ADDRESS_ID --> ", iBadAddressID.ToString()), iBadEntityID.ToString(), iGoodEntityID.ToString(), "SetPolicyXReferences");
                            //End Loggin
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "SetPolicyXReferences()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                lblInfo1.Text = "Updation of Policy Tables are Completed...";
                lblInfo1.Update();
            }
        }

        //Ankit : Start for Test
        private void SetPendingReferences(int iGoodEntityID, int iBadEntityID)
        {
            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();

                sb_Query.Append(" SELECT TBL.PRIMARYKEY, TBL.TABLENAME, TBL.COLUMNNAME, TBL.WHERECOLUMNNAME ");
                sb_Query.Append(" FROM ( ");
                sb_Query.Append(" SELECT FUNDS.CLAIM_ID, FUNDS.CLAIMANT_EID AS ENTITY_ID , FUNDS.TRANS_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS' AS TABLENAME, 'CLAIMANT_EID' AS COLUMNNAME, 'TRANS_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT FUNDS.CLAIM_ID, FUNDS.PAYEE_EID AS ENTITY_ID, FUNDS.TRANS_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS' AS TABLENAME, 'PAYEE_EID' AS COLUMNNAME, 'TRANS_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT FUNDS_AUTO.CLAIM_ID, FUNDS_AUTO.CLAIMANT_EID AS ENTITY_ID, FUNDS_AUTO.AUTO_TRANS_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS_AUTO' AS TABLENAME, 'CLAIMANT_EID' AS COLUMNNAME, 'AUTO_TRANS_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS_AUTO ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS_AUTO.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT FUNDS_AUTO.CLAIM_ID, FUNDS_AUTO.PAYEE_EID AS ENTITY_ID, FUNDS_AUTO.AUTO_TRANS_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS_AUTO' AS TABLENAME, 'PAYEE_EID' AS COLUMNNAME, 'AUTO_TRANS_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS_AUTO ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS_AUTO.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT FUNDS_AUTO.CLAIM_ID, FUNDS_AUTO_X_PAYEE.PAYEE_EID AS ENTITY_ID, FUNDS_AUTO_X_PAYEE.PAYEE_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS_AUTO_X_PAYEE' AS TABLENAME, 'PAYEE_EID' AS COLUMNNAME, 'PAYEE_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS_AUTO_X_PAYEE ");
                sb_Query.Append(" INNER JOIN FUNDS_AUTO ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_X_PAYEE.FUNDS_AUTO_TRANS_ID ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS_AUTO.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT FUNDS.CLAIM_ID, FUNDS_X_PAYEE.PAYEE_EID AS ENTITY_ID, FUNDS_X_PAYEE.PAYEE_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'FUNDS_X_PAYEE' AS TABLENAME, 'PAYEE_EID' AS COLUMNNAME, 'PAYEE_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM FUNDS_X_PAYEE ");
                sb_Query.Append(" INNER JOIN FUNDS ON FUNDS.TRANS_ID = FUNDS_X_PAYEE.FUNDS_TRANS_ID ");
                sb_Query.Append(" INNER JOIN CLAIM ON FUNDS.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, CLAIMANT.CLAIMANT_EID AS ENTITY_ID, CLAIMANT.CLAIMANT_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'CLAIMANT' AS TABLENAME, 'CLAIMANT_EID' AS COLUMNNAME, 'CLAIMANT_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM CLAIMANT ");
                sb_Query.Append(" INNER JOIN CLAIM ON CLAIMANT.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, CLAIMANT.ASSIGNADJ_EID AS ENTITY_ID, CLAIMANT.CLAIMANT_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'CLAIMANT' AS TABLENAME, 'ASSIGNADJ_EID' AS COLUMNNAME, 'CLAIMANT_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM CLAIMANT ");
                sb_Query.Append(" INNER JOIN CLAIM ON CLAIMANT.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, PERSON_INVOLVED.PI_EID AS ENTITY_ID, PERSON_INVOLVED.PI_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'PERSON_INVOLVED' AS TABLENAME, 'PI_EID' AS COLUMNNAME, 'PI_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM PERSON_INVOLVED ");
                sb_Query.Append(" INNER JOIN EVENT ON PERSON_INVOLVED.EVENT_ID = EVENT.EVENT_ID ");
                sb_Query.Append(" INNER JOIN CLAIM ON CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, CLAIM_ADJUSTER.ADJUSTER_EID AS ENTITY_ID, CLAIM_ADJUSTER.ADJ_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'CLAIM_ADJUSTER' AS TABLENAME, 'ADJUSTER_EID' AS COLUMNNAME, 'ADJ_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM CLAIM_ADJUSTER ");
                sb_Query.Append(" INNER JOIN CLAIM ON CLAIM_ADJUSTER.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID AS ENTITY_ID, RC_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'RESERVE_CURRENT' AS TABLENAME, 'CLAIMANT_EID' AS COLUMNNAME, 'RC_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM RESERVE_CURRENT ");
                sb_Query.Append(" INNER JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, RESERVE_CURRENT.ASSIGNADJ_EID AS ENTITY_ID, RC_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'RESERVE_CURRENT' AS TABLENAME, 'ASSIGNADJ_EID' AS COLUMNNAME, 'RC_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM RESERVE_CURRENT ");
                sb_Query.Append(" INNER JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, RESERVE_HISTORY.CLAIMANT_EID AS ENTITY_ID, RSV_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'RESERVE_HISTORY' AS TABLENAME, 'CLAIMANT_EID' AS COLUMNNAME, 'RSV_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM  RESERVE_HISTORY ");
                sb_Query.Append(" INNER JOIN CLAIM ON RESERVE_HISTORY.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" UNION ");
                sb_Query.Append(" SELECT CLAIM.CLAIM_ID, RESERVE_HISTORY.ASSIGNADJ_EID AS ENTITY_ID, RSV_ROW_ID AS PRIMARYKEY, ");
                sb_Query.Append(" 'RESERVE_HISTORY' AS TABLENAME, 'ASSIGNADJ_EID' AS COLUMNNAME, 'RSV_ROW_ID' AS WHERECOLUMNNAME ");
                sb_Query.Append(" FROM  RESERVE_HISTORY ");
                sb_Query.Append(" INNER JOIN CLAIM ON RESERVE_HISTORY.CLAIM_ID = CLAIM.CLAIM_ID ");
                sb_Query.Append(" ) AS TBL ");
                sb_Query.Append(" INNER JOIN CLAIM_X_POLICY ON Tbl.CLAIM_ID=CLAIM_X_POLICY.CLAIM_ID ");
                sb_Query.Append(" INNER JOIN POLICY ON CLAIM_X_POLICY.POLICY_ID=POLICY.POLICY_ID ");
                sb_Query.Append(" WHERE POLICY.POLICY_SYSTEM_ID = {0} ");
                sb_Query.Append(" AND TBL.ENTITY_ID = {1} ");
                sb_Query.Append(" GROUP BY TBL.PRIMARYKEY, TBL.TABLENAME, TBL.COLUMNNAME, TBL.WHERECOLUMNNAME ");
                sb_Query.Append(" ORDER BY TBL.TABLENAME ");

                sb_Query.Replace("{0}", "~PolicySystemId~");
                sb_Query.Replace("{1}", "~BadEntityId~");

                obj_dictParams.Add("PolicySystemId", CommonCache.si_PolicySystemID.ToString());
                obj_dictParams.Add("BadEntityId", iBadEntityID.ToString());

                using (DataSet ds = DbFactory.ExecuteDataSet(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams, m_iClientId))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        UpdatePendingReferences(dr, iGoodEntityID, iBadEntityID);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "SetPendingReferences()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
        }

        private void UpdatePendingReferences(DataRow dr, int iGoodEntityID, int iBadEntityID)
        {
            string sTableName = string.Empty;
            string sColumnName = string.Empty;
            string sWhereColumnname = string.Empty;
            int iPrimaryID = 0;
            try
            {
                sb_Query.Clear();
                obj_dictParams.Clear();

                sTableName = dr["TABLENAME"].ToString().Trim();
                sColumnName = dr["COLUMNNAME"].ToString().Trim();
                sWhereColumnname = dr["WHERECOLUMNNAME"].ToString().Trim();
                iPrimaryID = Convert.ToInt32(dr["PRIMARYKEY"]);

                lblInfo2.Text = string.Concat("Table --> ", sTableName, " \n Column --> ", sColumnName);
                lblInfo2.Update();
                this.Update();

                sb_Query.AppendFormat(string.Concat("UPDATE ", sTableName, " SET ", sColumnName, " = {0} WHERE ", sWhereColumnname, " = {1}"), "~GOODENTITYID~", "~PRIMARYID~ ");
                obj_dictParams.Add("GOODENTITYID", iGoodEntityID.ToString());
                obj_dictParams.Add("PRIMARYID", iPrimaryID.ToString());
                DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                WirteUpdateLog(sTableName, sColumnName, string.Concat(sWhereColumnname, " --> ", iPrimaryID.ToString()), iBadEntityID.ToString(), iGoodEntityID.ToString(), "SetFundsReferences");
            }
            catch (SqlException ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "SetPendingReferences()", string.Concat(ex.Message, " Error Query - ", sb_Query.ToString().Replace("~GOODENTITYID~", obj_dictParams["GOODENTITYID"]).Replace("~PRIMARYID~", obj_dictParams["PRIMARYID"])), 4);
                lblError.Text = ex.Message;
                lblError.Update();
                if (int.Equals(ex.Number, 2627) || int.Equals(ex.Number, 2601))            //2627 - For Primary Key Constraint Exception; 2601 - For Unique Key Constraint
                {
                    if (string.Equals(sTableName.ToUpper(), "CLAIMANT") || string.Equals(sTableName.ToUpper(), "PERSON_INVOLVED") || string.Equals(sTableName.ToUpper(), "CLAIM_ADJUSTER"))
                    {
                        sb_Query.Clear();
                        obj_dictParams.Clear();
                        sb_Query.AppendFormat(string.Concat("DELETE FROM ", sTableName, " WHERE ", sWhereColumnname, " = {0}"), "~PRIMARYID~ ");
                        obj_dictParams.Add("PRIMARYID", iPrimaryID.ToString());
                        DbFactory.ExecuteNonQuery(CommonCache.ss_DSNConnectionString, sb_Query.ToString(), obj_dictParams);
                        CommonCache.WriteLogFile(this.GetType().Name, "UpdatePendingReferences()", string.Concat("Deleted Query - ", sb_Query.ToString().Replace("~PRIMARYID~", obj_dictParams["PRIMARYID"]), " For Bad Entity Id - ", iBadEntityID.ToString()), 2);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCache.WriteLogFile(this.GetType().Name, "SetPendingReferences()", string.Concat(ex.Message, " Error Query - ", sb_Query.ToString()), 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
        }
        //Ankit End

        #endregion

        #region "Helper Functions"
        private void WirteLog(bool isSuccess, string sTableName, string sMode, string sMethodName)
        {
            sb_Query = new StringBuilder();
            if (!isSuccess)
                sb_Query.Append(string.Concat("Not "));

            sb_Query.Append(string.Concat("Able to ", sMode," ", sTableName, " Table for Policy_ID --> ", obj_OutputColumns[s_PolicyID]));
            sb_Query.Append(string.Concat(", Entity_Id --> ", obj_OutputColumns[s_EntityID]));
            if (obj_OutputColumns.ContainsKey(s_PolicyNumber))
                sb_Query.Append(string.Concat(", Policy_Number --> ", obj_OutputColumns[s_PolicyNumber]));
            if (obj_OutputColumns.ContainsKey(s_FullName))
                sb_Query.Append(string.Concat(", Full_Name --> ", obj_OutputColumns[s_FullName]));
            if (obj_OutputColumns.ContainsKey(s_FullAddress))
                sb_Query.Append(string.Concat(", Full_Address --> ", obj_OutputColumns[s_FullAddress]));
            if (obj_OutputColumns.ContainsKey(s_ClientSeqNum))
                sb_Query.Append(string.Concat(", Client_Seq_Num --> ", obj_OutputColumns[s_ClientSeqNum]));
            if (obj_OutputColumns.ContainsKey(s_InsLine))
                sb_Query.Append(string.Concat(", PS_INS_LINE --> ", obj_OutputColumns[s_InsLine]));
            if (obj_OutputColumns.ContainsKey(s_RiskLoc))
                sb_Query.Append(string.Concat(", PS_RISK_LOC --> ", obj_OutputColumns[s_RiskLoc]));
            if (obj_OutputColumns.ContainsKey(s_RiskSubLoc))
                sb_Query.Append(string.Concat(", PS_RISK_SUB_LOC --> ", obj_OutputColumns[s_RiskSubLoc]));
            if (obj_OutputColumns.ContainsKey(s_Product))
                sb_Query.Append(string.Concat(", PS_PRODUCT --> ", obj_OutputColumns[s_Product]));
            if (obj_OutputColumns.ContainsKey(s_DriverID))
                sb_Query.Append(string.Concat(", PS_DRIVER_ID --> ", obj_OutputColumns[s_DriverID]));
            if (obj_OutputColumns.ContainsKey(s_RecordStatus))
                sb_Query.Append(string.Concat(", PS_RECORD_STATUS --> ", obj_OutputColumns[s_RecordStatus]));
            if (obj_OutputColumns.ContainsKey(s_ErrorQry))
                sb_Query.Append(string.Concat(", Error_Qry --> ", obj_OutputColumns[s_ErrorQry]));
            if (obj_OutputColumns.ContainsKey(s_ErrorMsg))
                sb_Query.Append(string.Concat(", Error_Msg --> ", obj_OutputColumns[s_ErrorMsg]));


            //Log.Write(sb_Query.ToString());
            CommonCache.WriteLogFile(this.GetType().Name, sMethodName, sb_Query.ToString(), (isSuccess ? 2 : 1));
        }
        private void WirteUpdateLog(string p_sTableName, string p_sColumnName, string p_sPrimaryKey, string p_BadEntityId, string p_GoodEntityId, string p_sMethodName)
        {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("Update Entity Reference For Table : ");
            sbMessage.Append(p_sTableName);
            sbMessage.Append(", Primary Key : ");
            sbMessage.Append(p_sPrimaryKey);
            sbMessage.Append(",Column Name : ");
            sbMessage.Append(p_sColumnName);
            sbMessage.Append(", Old Entity Id : ");
            sbMessage.Append(p_BadEntityId);
            sbMessage.Append(", New Entity Id : ");
            sbMessage.Append(p_GoodEntityId);

            CommonCache.WriteLogFile(this.GetType().Name, p_sMethodName, sbMessage.ToString(), 2);
        } 
        private void GetPolicySystem()
        {
            try
            {
                sb_Query = new StringBuilder();
                sb_Query.Append("SELECT (SELECT MAX(POLICY_ID) FROM POLICY) MAX_POLICY_ID, POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME, ");
                sb_Query.Append(" (CAST(POLICY_SYSTEM_ID AS VARCHAR(50)) + '|' + CAST(CLIENTFILE_FLAG AS VARCHAR(1))) COMBINED_ID, ");          //Need to write for orcale
                sb_Query.Append(" DEFAULT_POL_FLAG,POLICY_SYSTEM_CODE,CLIENTFILE_FLAG, LOC_COMPNY,MASTER_COMPANY ");
                sb_Query.Append(" FROM POLICY_X_WEB ");
                sb_Query.Append(" ORDER BY DEFAULT_POL_FLAG,POLICY_SYSTEM_NAME");

                using (DbReader reader = DbFactory.ExecuteReader(CommonCache.ss_DSNConnectionString, String.Format(sb_Query.ToString())))
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                        cbPolicySystem.SelectedIndexChanged -= new EventHandler(cbPolicySystem_SelectedIndexChanged);
                        cbPolicySystem.DataSource = dt;
                        cbPolicySystem.ValueMember = "COMBINED_ID";
                        cbPolicySystem.DisplayMember = "POLICY_SYSTEM_NAME";
                        cbPolicySystem.SelectedIndexChanged += new EventHandler(cbPolicySystem_SelectedIndexChanged);
                        
                        //***************Set Default Controls******************************
                        CommonCache.si_ClientFileFlag = Convert.ToInt32(dt.Rows[0]["CLIENTFILE_FLAG"].ToString());
                        lblMaxPolicyIDResult.Text = dt.Rows[0]["MAX_POLICY_ID"].ToString();
                        txtFromPolicyID.Text = "0";
                        txtToPolicyID.Text = dt.Rows[0]["MAX_POLICY_ID"].ToString();
                    }
                    else
                        MessageBox.Show("Not able to Fetch DSNs...");
                }
            }
            catch (Exception ex)
            {
                //Log.Write(string.Concat("REF_Error in GetPolicySystem() for Mode.cs - ", ex.Message));
                CommonCache.WriteLogFile(this.GetType().Name, "GetPolicySystem()", ex.Message, 4);
                lblError.Text = ex.Message;
                lblError.Update();
            }
            finally
            {
                sb_Query = null;
            }
        }
        private void SetEntityTypeSelection(bool isPolicyInsuredSelected, bool isDriverSelected, bool isPolicyInterestSelected, bool isUnitInterestSelected, string sOutputXML, string sLastSelectedVal)
        {
            CommonCache.sb_IsPolicyInsuredSelected = isPolicyInsuredSelected;
            CommonCache.sb_IsDriverSelected = isDriverSelected;
            CommonCache.sb_IsPolicyInterestSelected = isPolicyInterestSelected;
            CommonCache.sb_IsUnitInterestSelected = isUnitInterestSelected;
            CommonCache.ss_OutputXML = sOutputXML;
            if(!string.IsNullOrEmpty(sLastSelectedVal))
                CommonCache.ss_LastSelectedVal = sLastSelectedVal;
        }
        private void SetControlAttributes()
        {
            if (int.Equals(CommonCache.si_ClientFileFlag, 0))
            {
                clbCustom.Items.Clear();
                clbCustom.Items.Add(s_Driver, true);

                SetEntityTypeSelection(false, true, false, false, string.Empty, string.Empty);
            }
            else
            {
                clbCustom.Items.Clear();
                clbCustom.Items.Add(s_PolicyInsured, true);
                clbCustom.Items.Add(s_PolicyInterest, true);
                clbCustom.Items.Add(s_UnitInterest, true);
                clbCustom.Items.Add(s_Driver, true);
                SetEntityTypeSelection(true, true, true, true, string.Empty, string.Empty);
            }
        }
        private void HomeLoad()
        {
            Boolean isValidLogin = false;
            UserLogin oUserLogin = null;
            Riskmaster.Security.Login m_objLogin = null;
            AuthenticationServiceClient obj_Auth = null;
            try
            {
                obj_Auth = new AuthenticationServiceClient();
                m_objLogin = new Riskmaster.Security.Login(m_iClientId);

                isValidLogin = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref oUserLogin, m_iClientId);
                
                if (isValidLogin)
                {
                    if (oUserLogin.objRiskmasterDatabase.DataSourceName == null || oUserLogin.objRiskmasterDatabase.DataSourceName == "")
                    {
                        MessageBox.Show("User is not authenticated for given data source name.", "Login Failed");
                        this.Dispose();
                        this.Close();
                        //Home_Load(sender, e);
                    }
                    else
                    {
                        CommonCache.ss_UName = oUserLogin.LoginName;
                        CommonCache.ss_DsnName = oUserLogin.objRiskmasterDatabase.DataSourceName;
                        CommonCache.ss_DSNConnectionString = oUserLogin.objRiskmasterDatabase.ConnectionString;
                        CommonCache.ss_Session = obj_Auth.GetUserSessionID(CommonCache.ss_UName, CommonCache.ss_DsnName);
                        
                        using (DbConnection objConn = DbFactory.GetDbConnection(CommonCache.ss_DSNConnectionString))
                        {
                            objConn.Open();
                            CommonCache.ss_MainDatabaseName = objConn.Database;
                            objConn.Close();
                            objConn.Dispose();
                        }

                        MessageBox.Show(string.Concat("Please take ", CommonCache.ss_MainDatabaseName, " Database Backup..."));
                    }
                }
                if (!isValidLogin)
                {
                    // for invalid Login
                    if (m_objLogin != null) m_objLogin.Dispose();
                    MessageBox.Show("Invalid Login", "Login Failed");
                    this.Dispose();
                    this.Close();
                    //Home_Load(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                this.Dispose();
                this.Close();
            }
            finally
            {
                oUserLogin = null;
                obj_Auth = null;
            }
        }       
        #endregion        
       
        #endregion
    }
}
