﻿namespace PointEntityCleanUpTool
{
    partial class FrmFixRedundantEntities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFixRedundantEntities));
            this.clbCustom = new System.Windows.Forms.CheckedListBox();
            this.btnFixEntities = new System.Windows.Forms.Button();
            this.lblEntityTypes = new System.Windows.Forms.Label();
            this.cbPolicySystem = new System.Windows.Forms.ComboBox();
            this.lblPolicySystem = new System.Windows.Forms.Label();
            this.lblInfo1 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblMaxPolicyID = new System.Windows.Forms.Label();
            this.lblMaxPolicyIDResult = new System.Windows.Forms.Label();
            this.lblFromPolicyID = new System.Windows.Forms.Label();
            this.lblToPolicyID = new System.Windows.Forms.Label();
            this.txtToPolicyID = new System.Windows.Forms.TextBox();
            this.txtFromPolicyID = new System.Windows.Forms.TextBox();
            this.lblWarning = new System.Windows.Forms.Label();
            this.lblInfo2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // clbCustom
            // 
            this.clbCustom.CheckOnClick = true;
            this.clbCustom.FormattingEnabled = true;
            this.clbCustom.Location = new System.Drawing.Point(163, 163);
            this.clbCustom.Name = "clbCustom";
            this.clbCustom.Size = new System.Drawing.Size(120, 64);
            this.clbCustom.TabIndex = 3;
            // 
            // btnFixEntities
            // 
            this.btnFixEntities.Location = new System.Drawing.Point(99, 241);
            this.btnFixEntities.Name = "btnFixEntities";
            this.btnFixEntities.Size = new System.Drawing.Size(163, 23);
            this.btnFixEntities.TabIndex = 4;
            this.btnFixEntities.Text = "Run Process";
            this.btnFixEntities.UseVisualStyleBackColor = true;
            this.btnFixEntities.Click += new System.EventHandler(this.btnFixEntities_Click);
            // 
            // lblEntityTypes
            // 
            this.lblEntityTypes.AutoSize = true;
            this.lblEntityTypes.Location = new System.Drawing.Point(68, 161);
            this.lblEntityTypes.Name = "lblEntityTypes";
            this.lblEntityTypes.Size = new System.Drawing.Size(65, 13);
            this.lblEntityTypes.TabIndex = 4;
            this.lblEntityTypes.Text = "Entity Types";
            // 
            // cbPolicySystem
            // 
            this.cbPolicySystem.FormattingEnabled = true;
            this.cbPolicySystem.Location = new System.Drawing.Point(162, 53);
            this.cbPolicySystem.Name = "cbPolicySystem";
            this.cbPolicySystem.Size = new System.Drawing.Size(217, 21);
            this.cbPolicySystem.TabIndex = 0;
            this.cbPolicySystem.SelectedIndexChanged += new System.EventHandler(this.cbPolicySystem_SelectedIndexChanged);
            // 
            // lblPolicySystem
            // 
            this.lblPolicySystem.AutoSize = true;
            this.lblPolicySystem.Location = new System.Drawing.Point(63, 53);
            this.lblPolicySystem.Name = "lblPolicySystem";
            this.lblPolicySystem.Size = new System.Drawing.Size(72, 13);
            this.lblPolicySystem.TabIndex = 6;
            this.lblPolicySystem.Text = "Policy System";
            // 
            // lblInfo1
            // 
            this.lblInfo1.AutoSize = true;
            this.lblInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo1.ForeColor = System.Drawing.Color.Green;
            this.lblInfo1.Location = new System.Drawing.Point(12, 2);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(19, 13);
            this.lblInfo1.TabIndex = 7;
            this.lblInfo1.Text = "...";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(12, 295);
            this.lblError.MaximumSize = new System.Drawing.Size(400, 295);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(16, 13);
            this.lblError.TabIndex = 9;
            this.lblError.Text = "...";
            // 
            // lblMaxPolicyID
            // 
            this.lblMaxPolicyID.AutoSize = true;
            this.lblMaxPolicyID.Location = new System.Drawing.Point(63, 89);
            this.lblMaxPolicyID.Name = "lblMaxPolicyID";
            this.lblMaxPolicyID.Size = new System.Drawing.Size(72, 13);
            this.lblMaxPolicyID.TabIndex = 10;
            this.lblMaxPolicyID.Text = "Max Policy ID";
            // 
            // lblMaxPolicyIDResult
            // 
            this.lblMaxPolicyIDResult.AutoSize = true;
            this.lblMaxPolicyIDResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxPolicyIDResult.Location = new System.Drawing.Point(159, 90);
            this.lblMaxPolicyIDResult.Name = "lblMaxPolicyIDResult";
            this.lblMaxPolicyIDResult.Size = new System.Drawing.Size(19, 20);
            this.lblMaxPolicyIDResult.TabIndex = 11;
            this.lblMaxPolicyIDResult.Text = "0";
            // 
            // lblFromPolicyID
            // 
            this.lblFromPolicyID.AutoSize = true;
            this.lblFromPolicyID.Location = new System.Drawing.Point(63, 125);
            this.lblFromPolicyID.Name = "lblFromPolicyID";
            this.lblFromPolicyID.Size = new System.Drawing.Size(75, 13);
            this.lblFromPolicyID.TabIndex = 12;
            this.lblFromPolicyID.Text = "From Policy ID";
            // 
            // lblToPolicyID
            // 
            this.lblToPolicyID.AutoSize = true;
            this.lblToPolicyID.Location = new System.Drawing.Point(232, 127);
            this.lblToPolicyID.Name = "lblToPolicyID";
            this.lblToPolicyID.Size = new System.Drawing.Size(65, 13);
            this.lblToPolicyID.TabIndex = 13;
            this.lblToPolicyID.Text = "To Policy ID";
            // 
            // txtToPolicyID
            // 
            this.txtToPolicyID.Location = new System.Drawing.Point(316, 127);
            this.txtToPolicyID.Name = "txtToPolicyID";
            this.txtToPolicyID.Size = new System.Drawing.Size(49, 20);
            this.txtToPolicyID.TabIndex = 2;
            this.txtToPolicyID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToPolicyID_KeyPress);
            // 
            // txtFromPolicyID
            // 
            this.txtFromPolicyID.Location = new System.Drawing.Point(162, 127);
            this.txtFromPolicyID.Name = "txtFromPolicyID";
            this.txtFromPolicyID.Size = new System.Drawing.Size(49, 20);
            this.txtFromPolicyID.TabIndex = 1;
            this.txtFromPolicyID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFromPolicyID_KeyPress);
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(12, 276);
            this.lblWarning.MaximumSize = new System.Drawing.Size(400, 295);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(16, 13);
            this.lblWarning.TabIndex = 17;
            this.lblWarning.Text = "...";
            // 
            // lblInfo2
            // 
            this.lblInfo2.AutoSize = true;
            this.lblInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo2.ForeColor = System.Drawing.Color.Green;
            this.lblInfo2.Location = new System.Drawing.Point(12, 19);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(16, 13);
            this.lblInfo2.TabIndex = 18;
            this.lblInfo2.Text = "...";
            // 
            // FrmFixRedundantEntities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 329);
            this.Controls.Add(this.lblInfo2);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.txtFromPolicyID);
            this.Controls.Add(this.txtToPolicyID);
            this.Controls.Add(this.lblToPolicyID);
            this.Controls.Add(this.lblFromPolicyID);
            this.Controls.Add(this.lblMaxPolicyIDResult);
            this.Controls.Add(this.lblMaxPolicyID);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblInfo1);
            this.Controls.Add(this.lblPolicySystem);
            this.Controls.Add(this.cbPolicySystem);
            this.Controls.Add(this.lblEntityTypes);
            this.Controls.Add(this.btnFixEntities);
            this.Controls.Add(this.clbCustom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 380);
            this.MinimizeBox = false;
            this.Name = "FrmFixRedundantEntities";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fix Redundant Entities";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFixRedundantEntities_FormClosing);
            this.Load += new System.EventHandler(this.FrmFixRedundantEntities_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbCustom;
        private System.Windows.Forms.Button btnFixEntities;
        private System.Windows.Forms.Label lblEntityTypes;
        private System.Windows.Forms.ComboBox cbPolicySystem;
        private System.Windows.Forms.Label lblPolicySystem;
        private System.Windows.Forms.Label lblInfo1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblMaxPolicyID;
        private System.Windows.Forms.Label lblMaxPolicyIDResult;
        private System.Windows.Forms.Label lblFromPolicyID;
        private System.Windows.Forms.Label lblToPolicyID;
        private System.Windows.Forms.TextBox txtToPolicyID;
        private System.Windows.Forms.TextBox txtFromPolicyID;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Label lblInfo2;
    }
}