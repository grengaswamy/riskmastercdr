VERSION 5.00
Begin VB.Form frmLicenseLogin 
   Caption         =   "LSS (RISKMASTER X ) Database Upgrade"
   ClientHeight    =   4335
   ClientLeft      =   5370
   ClientTop       =   3180
   ClientWidth     =   4905
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   4905
   Begin VB.CheckBox chkMultiDB 
      Caption         =   "Update Multiple Databases"
      Height          =   345
      Left            =   180
      TabIndex        =   4
      Top             =   4410
      Width           =   2265
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   455
      Left            =   2985
      TabIndex        =   2
      Top             =   3795
      Width           =   1810
   End
   Begin VB.CommandButton btnUpgrade 
      Caption         =   "Upgrade"
      Default         =   -1  'True
      Height          =   455
      Left            =   135
      TabIndex        =   1
      Top             =   3795
      Width           =   1810
   End
   Begin VB.ListBox List1 
      Height          =   3375
      ItemData        =   "license-login.frx":0000
      Left            =   90
      List            =   "license-login.frx":0002
      TabIndex        =   0
      Top             =   285
      Width           =   4740
   End
   Begin VB.Label Label1 
      Caption         =   "Pick Database to Upgrade:"
      Height          =   225
      Left            =   165
      TabIndex        =   3
      Top             =   15
      Width           =   3615
   End
End
Attribute VB_Name = "frmLicenseLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public boolOK As Boolean

Private Sub btnCancel_Click()
    boolOK = False
    bMultiDB = False
    Me.Hide
End Sub

Private Sub btnUpgrade_Click()
On Error GoTo hError

    Dim db As Integer
    Dim rs As Integer
    Dim sTmp As String
    Dim lTmp As Long
    Dim crypt As New DTGCrypt
    Dim iPos As Integer
    Dim iPos2 As Integer
    Dim sTmp2 As String
    Dim sDate As String
    Dim sDSN As String
    Dim lLicNum As Long
    Dim byteArr() As Byte
    Dim i As Integer
    
    If List1.ListIndex = -1 Then
        MsgBox "Please select a database to upgrade first.", vbInformation, "Upgrade"
        Exit Sub
    End If
    
    lTmp = List1.ItemData(List1.ListIndex)
    lItem = List1.ListIndex

    db = DB_OpenDatabase(hEnv, objLogin.SecurityDSN, 0)

    rs = DB_CreateRecordset(db, "SELECT * FROM DATA_SOURCE_TABLE WHERE DSNID = " & lTmp, DB_FORWARD_ONLY, 0)
    DB_CloseRecordset rs, DB_CLOSE
    
    sODBCName = vDB_GetData(rs, "DSN") & ""   ' used in error logging
    
    ' Now, form a connection string so we can connect and do rest of upgrade
    If vDB_GetData(rs, "CONNECTION_STRING") & "" = "" Then
        g_sConnectString = "DSN=" & vDB_GetData(rs, "DSN") & ";UID=" & _
            crypt.DecryptString(vDB_GetData(rs, "RM_USERID") & "", "6378b87457a5ecac8674e9bac12e7cd9") & _
            ";PWD=" & crypt.DecryptString(vDB_GetData(rs, "RM_PASSWORD") & "", "6378b87457a5ecac8674e9bac12e7cd9")
    Else
        g_sConnectString = vDB_GetData(rs, "CONNECTION_STRING") & ";UID=" & _
            crypt.DecryptString(vDB_GetData(rs, "RM_USERID") & "", "6378b87457a5ecac8674e9bac12e7cd9") & _
            ";PWD=" & crypt.DecryptString(vDB_GetData(rs, "RM_PASSWORD") & "", "6378b87457a5ecac8674e9bac12e7cd9")
    End If

    g_bOrgSecOn = (iAnyVarToInt(vDB_GetData(rs, "ORGSEC_FLAG")) <> 0)
    
    DB_CloseRecordset rs, DB_DROP
    DB_CloseDatabase db
    
    If bMultiDB Then
        If Not (chkMultiDB = 1) Then bMultiDB = False
    End If
    
    boolOK = True
    Me.Hide

    Exit Sub

hError:
    Dim EResult As Integer
    EResult = iGeneralError(Err, Error$, "frmLicLog.btnUpgrade")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select


End Sub

Private Sub Form_Load()
On Error GoTo hError

    Dim db As Integer
    Dim rs As Integer
    Dim sTmp As String
    Dim lTmp As Long
    
    db = DB_OpenDatabase(hEnv, objLogin.SecurityDSN, 0)
    If bMultiDB Then chkMultiDB = 1
    
    rs = DB_CreateRecordset(db, "SELECT * FROM DATA_SOURCE_TABLE ORDER BY DSN", DB_FORWARD_ONLY, 0)
    While Not DB_EOF(rs)
        List1.AddItem vDB_GetData(rs, "DSN")
        List1.ItemData(List1.NewIndex) = lAnyVarToLong(vDB_GetData(rs, "DSNID"))
    
        bMultiDB = True
        DB_MoveNext rs
        If Not DB_EOF(rs) Then bMultiDB = True
    Wend
    If bMultiDB Then   ' chkMultiDB.Top = 4410   was 4110
        Height = 5265   ' was 4965
    Else
        Height = 4740   ' was 4440
    End If
    
    DB_CloseRecordset rs, DB_DROP
    
hExit:
    DB_CloseDatabase db
    Exit Sub
hError:
    Dim EResult As Integer
    EResult = iGeneralError(Err, Error$, "Display RM/World Databases")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Resume hExit
    End Select

End Sub
