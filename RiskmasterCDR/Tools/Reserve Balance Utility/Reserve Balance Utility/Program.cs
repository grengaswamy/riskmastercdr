﻿using System;
using System.Configuration;
using System.Data;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;

namespace ReserveBalanceUtility
{
    /// <summary>
    /// Author : Deb
    /// Date : 12/23/2013
    /// </summary>
    public class Program
    {
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static string m_sLoginPwd = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string sJobID = string.Empty;
        static string sMode = string.Empty;
        static string sClaimId = string.Empty;
        static DbConnection m_objDbConnection = null;
        static DbConnection m_objSecDbConnection = null;
        static DbCommand m_objSecDbCommand = null;
        static DbCommand m_objCmd = null;
        static eDatabaseType m_sDatabaseType = 0;
        private static string sClaimNumber;
        private static string resBalOption;
        private static string sFromDate;
        private static string sToDate;
        static int iClientId = 0;//Add & Change by kuladeep for Cloud.
        static string sTempClientId = string.Empty;

        /// <summary>
        /// Start
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            UserLogin oUserLogin = null;
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            bool blnSuccess = false;

            try
            {
                int argCount = (args == null) ? 0 : args.Length;
                if (argCount > 0) //Entry from TaskManager
                {
                    GetParameters(args);
                }
                //Add & Change by kuladeep for Cloud----Start
                if (args != null && args.Length > 0 && (string.IsNullOrEmpty(sTempClientId)==false))
                {
                    
                    iClientId = Convert.ToInt32(sTempClientId);
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by kuladeep for Cloud----End

                if (argCount > 0) //Entry from TaskManager
                {
                    //GetParameters(args);  //Change by kuladeep for Cloud
                    oUserLogin = new UserLogin(m_sLoginName, m_sDataSource,iClientId);//Add & Change by kuladeep for Cloud.
                    if (oUserLogin.DatabaseId <= 0)
                    {
                        throw new Exception("Authentication failure.");
                    }
                    Initalize(oUserLogin);
                }
                else //Entry for direct run
                {
                    Login objLogin = new Login(iClientId);//Add & Change by kuladeep for Cloud.
                    bool bCon = false;
                    bCon = objLogin.RegisterApplication(0, 0, ref oUserLogin, iClientId);//Add & Change by kuladeep for Cloud.
                    if (bCon)
                    {
                        Initalize(oUserLogin);
                    }
                    else
                    {
                        return;
                    }
                    GetConfigOptions();
                }
                OpenDbConnection();     
                ProcessReserveBalanceUtility(iClientId);//psharma206
            }
            catch (Exception exc)
            {
                smsg = exc.Message;
                FormatAndDisplayExcMsg(smsg);
                Log.Write(exc.Message, "CommonWebServiceLog", iClientId);//psharma206
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();

                }
                if (m_objSecDbConnection != null)
                {
                    m_objSecDbConnection.Close();
                    m_objDbConnection.Dispose();
                }
                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objSecDbCommand != null)
                    m_objSecDbCommand = null;

            }
        }

        /// <summary>
        /// Gets the configuration options.
        /// </summary>
        /// <exception cref="System.Exception">
        /// Invalid arguments provided.
        /// or
        /// Invalid arguments provided.
        /// </exception>
        private static void GetConfigOptions()
        {
            resBalOption = ConfigurationManager.AppSettings["Mode"];
            switch (resBalOption)
            {
                case "2":
                    sClaimNumber = ConfigurationManager.AppSettings["ClaimNumber"];

                    if (string.IsNullOrEmpty(sClaimNumber))
                    {
                        throw new Exception("Invalid arguments provided.");
                    }
                    break;
                case "3":
                    sFromDate = ConfigurationManager.AppSettings["ClaimFromDate"];
                    sToDate = ConfigurationManager.AppSettings["ClaimToDate"];
                    if (string.IsNullOrEmpty(sFromDate) || string.IsNullOrEmpty(sToDate))
                    {
                        throw new Exception("Invalid arguments provided.");
                    }
                    break;
            }
        }

        /// <summary>
        /// OpenDbConnection
        /// </summary>
        private static void OpenDbConnection()
        {
            m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
            m_objDbConnection.Open();
            m_sDatabaseType = m_objDbConnection.DatabaseType;
        }

        /// <summary>
        /// Processes the reserve balance utility.
        /// </summary>
        private static void ProcessReserveBalanceUtility(int p_iClientId)//psharma206
        {
            string sSQL = string.Empty;
            string serr_msg = string.Empty;
            DbParameter objParamout = null;
            DbParameter objParam = null;
            try
            {
                m_objCmd = m_objDbConnection.CreateCommand();
                sSQL = "USP_RESERVE_BALANCE_UTILITY";
                m_objCmd.CommandText = sSQL;
                m_objCmd.CommandType = CommandType.StoredProcedure;
                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.Int32;
                objParam.Size = 100;
                objParam.ParameterName = "@psnglclaims";
                objParam.Value = resBalOption.Equals("2") ? 1:0;
                m_objCmd.Parameters.Add(objParam);

                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.String;
                objParam.Size = 100;
                objParam.ParameterName = "@pClaimNumber";
                objParam.Value = sClaimNumber;
                m_objCmd.Parameters.Add(objParam);
                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.String;
                objParam.Size = 100;
                objParam.ParameterName = "@psFrom";
                objParam.Value = Conversion.GetDate(sFromDate);
                m_objCmd.Parameters.Add(objParam);
                            
                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.String;
                objParam.Size = 100;
                objParam.ParameterName = "@psTo";
                objParam.Value = Conversion.GetDate(sToDate);
                m_objCmd.Parameters.Add(objParam);

                objParamout = m_objCmd.CreateParameter();
                objParamout.Direction = System.Data.ParameterDirection.Output;
                objParamout.DbType = DbType.String;
                objParamout.Size = 4000;
                objParamout.ParameterName = "@p_sqlerrm";
                m_objCmd.Parameters.Add(objParamout);
                m_objCmd.CommandText = sSQL;

                Console.WriteLine("0 ^*^*^ Started procedure RESERVE_BALANCE_UTILITY ");
                m_objCmd.ExecuteNonQuery();
                serr_msg = objParamout.Value.ToString();
                string[] sOut = serr_msg.Split('|');
                foreach (string s in sOut)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        DateTime sDatetime = Conversion.ToDate(s.Split(':')[0]);
                        Console.WriteLine("0 ^*^*^ [" + sDatetime + "] " + s.Split(':')[1]);
                    }
                }
                Console.WriteLine("0 ^*^*^ Completed procedure RESERVE_BALANCE_UTILITY ");
            }
            catch (Exception ex)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", "Error occured in RESERVE_BALANCE_UTILITY");
                serr_msg = ex.Message;
                Log.Write(serr_msg, "CommonWebServiceLog", p_iClientId);//psharma206
            }
        }
        /// <summary>
        /// Format Exception Message returned from Stored Procedure.
        /// </summary>
        /// <param name="smsg">Exception Message</param>
        private static void FormatAndDisplayExcMsg(string smsg)
        {
            string[] sMsgArray = smsg.Split('\n');

            int iLengthofArray = sMsgArray.Length;
            for (int i = 0; i < iLengthofArray; i++)
            {
                sMsgArray[i] = sMsgArray[i].Replace("'", "");
                sMsgArray[i] = sMsgArray[i].Replace("-", " ");
                Console.WriteLine("1001 ^*^*^ {0} ", sMsgArray[i]);
            }
        }

        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            try
            {
                m_sDSNID = p_oUserLogin.DatabaseId.ToString();
                m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
                m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
                m_sDatabaseType = DbFactory.GetDatabaseType(m_sDbConnstring);
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
            }
        }

        /// <summary>
        /// GetParameters
        /// </summary>
        /// <param name="p_args">The p_args.</param>
        /// <exception cref="System.Exception">
        /// Invalid arguments provided.
        /// or
        /// Invalid arguments provided.
        /// or
        /// Invalid arguments provided.
        /// </exception>
        private static void GetParameters(string[] p_args)
        {
            if (Array.FindIndex(p_args, x => x.Equals("-ds")).Equals(-1) ||
                Array.FindIndex(p_args, x => x.Equals("-ru")).Equals(-1) ||
                Array.FindIndex(p_args, x => x.Equals("-rp")).Equals(-1) ||
                Array.FindIndex(p_args, x => x.Equals("-op")).Equals(-1) )
            {
                throw new Exception("Invalid arguments provided.");
            }
            m_sDataSource = p_args[Array.FindIndex(p_args, x => x.Equals("-ds")) + 1];
            m_sLoginName = p_args[Array.FindIndex(p_args, x => x.Equals("-ru")) + 1];
            m_sLoginPwd = p_args[Array.FindIndex(p_args, x => x.Equals("-rp")) + 1];
            resBalOption = p_args[Array.FindIndex(p_args, x => x.Equals("-op")) + 1];

            //Check for ClientId---If ClientId is not available in that case it will be it will be 0 for base.
            if(Array.FindIndex(p_args, x => x.Equals("-ci"))>0)
            {
                sTempClientId = p_args[Array.FindIndex(p_args, x => x.Equals("-ci")) + 1];
            }

            switch (resBalOption)
            {
                case "2":
                    if (Array.FindIndex(p_args, x => x.Equals("-cn")).Equals(-1))
                    {
                        throw new Exception("Invalid arguments provided.");
                    }
                    sClaimNumber = p_args[Array.FindIndex(p_args, x => x.Equals("-cn")) + 1];
                    break;
                case "3":
                    if (Array.FindIndex(p_args, x => x.Equals("-fd")).Equals(-1) || Array.FindIndex(p_args, x => x.Equals("-td")).Equals(-1))
                    {
                        throw new Exception("Invalid arguments provided.");
                    }

                    sFromDate = p_args[Array.FindIndex(p_args, x => x.Equals("-fd")) + 1];
                    sToDate = p_args[Array.FindIndex(p_args, x => x.Equals("-td")) + 1];
                    break;
            }
        }
    }
}

