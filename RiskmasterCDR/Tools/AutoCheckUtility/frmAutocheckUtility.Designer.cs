﻿namespace AutoCheckUtility
{
    partial class frmAutocheckUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutocheckUtility));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribCurrentPayment = new DevComponents.DotNetBar.RibbonPanel();
            this.cmdUndoCurrentPayment = new System.Windows.Forms.Button();
            this.cmdLoadAutocheckPayments = new System.Windows.Forms.Button();
            this.cmdCorrectCurrentPayment = new System.Windows.Forms.Button();
            this.cmdEndApp = new System.Windows.Forms.Button();
            this.lstAutoChecks = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.ribStartDate = new DevComponents.DotNetBar.RibbonPanel();
            this.cmdUndo = new System.Windows.Forms.Button();
            this.cmdLoad = new System.Windows.Forms.Button();
            this.cmdCorrect = new System.Windows.Forms.Button();
            this.cmdClose = new System.Windows.Forms.Button();
            this.lstAutocheckData = new System.Windows.Forms.ListView();
            this.AutobatchID = new System.Windows.Forms.ColumnHeader();
            this.PayeeName = new System.Windows.Forms.ColumnHeader();
            this.AutocheckAmount = new System.Windows.Forms.ColumnHeader();
            this.StartDate = new System.Windows.Forms.ColumnHeader();
            this.CorrectedStartDate = new System.Windows.Forms.ColumnHeader();
            this.TotalPayments = new System.Windows.Forms.ColumnHeader();
            this.CurrentPayment = new System.Windows.Forms.ColumnHeader();
            this.lblLastName = new System.Windows.Forms.Label();
            this.ribbonTabCurrentPayment = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabStartDate = new DevComponents.DotNetBar.RibbonTabItem();
            this.office2007StartButton1 = new DevComponents.DotNetBar.Office2007StartButton();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.mnuLoadAutochecks = new DevComponents.DotNetBar.ButtonItem();
            this.mnuCorrect = new DevComponents.DotNetBar.ButtonItem();
            this.munUndo = new DevComponents.DotNetBar.ButtonItem();
            this.mnuClose = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer1 = new DevComponents.DotNetBar.GalleryContainer();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.btnGenerateChecksum = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonControl1.SuspendLayout();
            this.ribCurrentPayment.SuspendLayout();
            this.ribStartDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribCurrentPayment);
            this.ribbonControl1.Controls.Add(this.ribStartDate);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ribbonTabCurrentPayment,
            this.ribbonTabStartDate});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Office2007ColorTable = DevComponents.DotNetBar.Rendering.eOffice2007ColorScheme.VistaGlass;
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.office2007StartButton1,
            this.btnGenerateChecksum,
            this.qatCustomizeItem1});
            this.ribbonControl1.Size = new System.Drawing.Size(872, 511);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 0;
            this.ribbonControl1.Text = "ribbonSMS";
            // 
            // ribCurrentPayment
            // 
            this.ribCurrentPayment.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribCurrentPayment.Controls.Add(this.cmdUndoCurrentPayment);
            this.ribCurrentPayment.Controls.Add(this.cmdLoadAutocheckPayments);
            this.ribCurrentPayment.Controls.Add(this.cmdCorrectCurrentPayment);
            this.ribCurrentPayment.Controls.Add(this.cmdEndApp);
            this.ribCurrentPayment.Controls.Add(this.lstAutoChecks);
            this.ribCurrentPayment.Controls.Add(this.label1);
            this.ribCurrentPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribCurrentPayment.Location = new System.Drawing.Point(0, 55);
            this.ribCurrentPayment.Name = "ribCurrentPayment";
            this.ribCurrentPayment.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribCurrentPayment.Size = new System.Drawing.Size(872, 454);
            this.ribCurrentPayment.TabIndex = 6;
            // 
            // cmdUndoCurrentPayment
            // 
            this.cmdUndoCurrentPayment.Location = new System.Drawing.Point(518, 257);
            this.cmdUndoCurrentPayment.Name = "cmdUndoCurrentPayment";
            this.cmdUndoCurrentPayment.Size = new System.Drawing.Size(75, 23);
            this.cmdUndoCurrentPayment.TabIndex = 34;
            this.cmdUndoCurrentPayment.Text = "Undo";
            this.cmdUndoCurrentPayment.UseVisualStyleBackColor = true;
            this.cmdUndoCurrentPayment.Click += new System.EventHandler(this.cmdUndoCurrentPayment_Click);
            // 
            // cmdLoadAutocheckPayments
            // 
            this.cmdLoadAutocheckPayments.Location = new System.Drawing.Point(36, 257);
            this.cmdLoadAutocheckPayments.Name = "cmdLoadAutocheckPayments";
            this.cmdLoadAutocheckPayments.Size = new System.Drawing.Size(134, 23);
            this.cmdLoadAutocheckPayments.TabIndex = 33;
            this.cmdLoadAutocheckPayments.Text = "Load Autocheck Data";
            this.cmdLoadAutocheckPayments.UseVisualStyleBackColor = true;
            this.cmdLoadAutocheckPayments.Click += new System.EventHandler(this.cmdLoadAutocheckPayments_Click);
            // 
            // cmdCorrectCurrentPayment
            // 
            this.cmdCorrectCurrentPayment.Location = new System.Drawing.Point(617, 257);
            this.cmdCorrectCurrentPayment.Name = "cmdCorrectCurrentPayment";
            this.cmdCorrectCurrentPayment.Size = new System.Drawing.Size(138, 23);
            this.cmdCorrectCurrentPayment.TabIndex = 32;
            this.cmdCorrectCurrentPayment.Text = "Correct Current Payments";
            this.cmdCorrectCurrentPayment.UseVisualStyleBackColor = true;
            this.cmdCorrectCurrentPayment.Click += new System.EventHandler(this.cmdCorrectCurrentPayment_Click);
            // 
            // cmdEndApp
            // 
            this.cmdEndApp.Location = new System.Drawing.Point(761, 257);
            this.cmdEndApp.Name = "cmdEndApp";
            this.cmdEndApp.Size = new System.Drawing.Size(75, 23);
            this.cmdEndApp.TabIndex = 31;
            this.cmdEndApp.Text = "Close";
            this.cmdEndApp.UseVisualStyleBackColor = true;
            this.cmdEndApp.Click += new System.EventHandler(this.cmdEndApp_Click);
            // 
            // lstAutoChecks
            // 
            this.lstAutoChecks.AutoArrange = false;
            this.lstAutoChecks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lstAutoChecks.GridLines = true;
            this.lstAutoChecks.Location = new System.Drawing.Point(36, 46);
            this.lstAutoChecks.MultiSelect = false;
            this.lstAutoChecks.Name = "lstAutoChecks";
            this.lstAutoChecks.ShowGroups = false;
            this.lstAutoChecks.Size = new System.Drawing.Size(800, 197);
            this.lstAutoChecks.TabIndex = 30;
            this.lstAutoChecks.UseCompatibleStateImageBehavior = false;
            this.lstAutoChecks.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Autobatch ID";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "PayeeName";
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Autocheck Amount";
            this.columnHeader3.Width = 110;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Total Payments";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Current Payment";
            this.columnHeader5.Width = 110;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Actual Current Payment";
            this.columnHeader6.Width = 90;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Payments To Date";
            this.columnHeader7.Width = 95;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Actual Payments To Date";
            this.columnHeader8.Width = 95;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(33, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Auto Check Records with incorrect Payment Numbers:";
            // 
            // ribStartDate
            // 
            this.ribStartDate.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribStartDate.Controls.Add(this.cmdUndo);
            this.ribStartDate.Controls.Add(this.cmdLoad);
            this.ribStartDate.Controls.Add(this.cmdCorrect);
            this.ribStartDate.Controls.Add(this.cmdClose);
            this.ribStartDate.Controls.Add(this.lstAutocheckData);
            this.ribStartDate.Controls.Add(this.lblLastName);
            this.ribStartDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribStartDate.Location = new System.Drawing.Point(0, 55);
            this.ribStartDate.Name = "ribStartDate";
            this.ribStartDate.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribStartDate.Size = new System.Drawing.Size(872, 454);
            this.ribStartDate.TabIndex = 5;
            this.ribStartDate.Visible = false;
            // 
            // cmdUndo
            // 
            this.cmdUndo.Location = new System.Drawing.Point(559, 258);
            this.cmdUndo.Name = "cmdUndo";
            this.cmdUndo.Size = new System.Drawing.Size(75, 23);
            this.cmdUndo.TabIndex = 40;
            this.cmdUndo.Text = "Undo";
            this.cmdUndo.UseVisualStyleBackColor = true;
            this.cmdUndo.Click += new System.EventHandler(this.cmdUndo_Click);
            // 
            // cmdLoad
            // 
            this.cmdLoad.Location = new System.Drawing.Point(36, 258);
            this.cmdLoad.Name = "cmdLoad";
            this.cmdLoad.Size = new System.Drawing.Size(134, 23);
            this.cmdLoad.TabIndex = 39;
            this.cmdLoad.Text = "Load Autocheck Data";
            this.cmdLoad.UseVisualStyleBackColor = true;
            this.cmdLoad.Click += new System.EventHandler(this.cmdLoad_Click);
            // 
            // cmdCorrect
            // 
            this.cmdCorrect.Location = new System.Drawing.Point(650, 258);
            this.cmdCorrect.Name = "cmdCorrect";
            this.cmdCorrect.Size = new System.Drawing.Size(105, 23);
            this.cmdCorrect.TabIndex = 38;
            this.cmdCorrect.Text = "Correct Start Dates";
            this.cmdCorrect.UseVisualStyleBackColor = true;
            this.cmdCorrect.Click += new System.EventHandler(this.cmdCorrect_Click);
            // 
            // cmdClose
            // 
            this.cmdClose.Location = new System.Drawing.Point(761, 258);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(75, 23);
            this.cmdClose.TabIndex = 37;
            this.cmdClose.Text = "Close";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // lstAutocheckData
            // 
            this.lstAutocheckData.AutoArrange = false;
            this.lstAutocheckData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.AutobatchID,
            this.PayeeName,
            this.AutocheckAmount,
            this.StartDate,
            this.CorrectedStartDate,
            this.TotalPayments,
            this.CurrentPayment});
            this.lstAutocheckData.GridLines = true;
            this.lstAutocheckData.Location = new System.Drawing.Point(36, 46);
            this.lstAutocheckData.MultiSelect = false;
            this.lstAutocheckData.Name = "lstAutocheckData";
            this.lstAutocheckData.ShowGroups = false;
            this.lstAutocheckData.Size = new System.Drawing.Size(800, 197);
            this.lstAutocheckData.TabIndex = 36;
            this.lstAutocheckData.UseCompatibleStateImageBehavior = false;
            this.lstAutocheckData.View = System.Windows.Forms.View.Details;
            // 
            // AutobatchID
            // 
            this.AutobatchID.Text = "Autobatch ID";
            this.AutobatchID.Width = 80;
            // 
            // PayeeName
            // 
            this.PayeeName.Text = "PayeeName";
            this.PayeeName.Width = 200;
            // 
            // AutocheckAmount
            // 
            this.AutocheckAmount.Text = "Autocheck Amount";
            this.AutocheckAmount.Width = 110;
            // 
            // StartDate
            // 
            this.StartDate.Text = "Start Date";
            this.StartDate.Width = 110;
            // 
            // CorrectedStartDate
            // 
            this.CorrectedStartDate.Text = "Corrected Start Date";
            this.CorrectedStartDate.Width = 110;
            // 
            // TotalPayments
            // 
            this.TotalPayments.Text = "Total Payments";
            this.TotalPayments.Width = 90;
            // 
            // CurrentPayment
            // 
            this.CurrentPayment.Text = "Current Payment";
            this.CurrentPayment.Width = 95;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.BackColor = System.Drawing.Color.Transparent;
            this.lblLastName.Location = new System.Drawing.Point(33, 29);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(227, 13);
            this.lblLastName.TabIndex = 35;
            this.lblLastName.Text = "Auto Check Records with incorrect start dates:";
            // 
            // ribbonTabCurrentPayment
            // 
            this.ribbonTabCurrentPayment.Checked = true;
            this.ribbonTabCurrentPayment.ImagePaddingHorizontal = 8;
            this.ribbonTabCurrentPayment.Name = "ribbonTabCurrentPayment";
            this.ribbonTabCurrentPayment.Panel = this.ribCurrentPayment;
            this.ribbonTabCurrentPayment.Text = "Autocheck Current Payment Utility";
            this.ribbonTabCurrentPayment.Click += new System.EventHandler(this.ribbonTabCurrentPayment_Click);
            // 
            // ribbonTabStartDate
            // 
            this.ribbonTabStartDate.ImagePaddingHorizontal = 8;
            this.ribbonTabStartDate.Name = "ribbonTabStartDate";
            this.ribbonTabStartDate.Panel = this.ribStartDate;
            this.ribbonTabStartDate.Text = "Autocheck Start Date Utility";
            this.ribbonTabStartDate.Click += new System.EventHandler(this.ribbonTabStartDate_Click);
            // 
            // office2007StartButton1
            // 
            this.office2007StartButton1.AutoExpandOnClick = true;
            this.office2007StartButton1.CanCustomize = false;
            this.office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.office2007StartButton1.Image = ((System.Drawing.Image)(resources.GetObject("office2007StartButton1.Image")));
            this.office2007StartButton1.ImagePaddingHorizontal = 2;
            this.office2007StartButton1.ImagePaddingVertical = 2;
            this.office2007StartButton1.Name = "office2007StartButton1";
            this.office2007StartButton1.ShowSubItems = false;
            this.office2007StartButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.office2007StartButton1.Text = "&File";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer4,
            this.buttonItem1});
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.galleryContainer1});
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.mnuLoadAutochecks,
            this.mnuCorrect,
            this.munUndo,
            this.mnuClose});
            // 
            // mnuLoadAutochecks
            // 
            this.mnuLoadAutochecks.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mnuLoadAutochecks.Image = ((System.Drawing.Image)(resources.GetObject("mnuLoadAutochecks.Image")));
            this.mnuLoadAutochecks.ImagePaddingHorizontal = 8;
            this.mnuLoadAutochecks.Name = "mnuLoadAutochecks";
            this.mnuLoadAutochecks.SubItemsExpandWidth = 24;
            this.mnuLoadAutochecks.Text = "&Load Autochecks";
            this.mnuLoadAutochecks.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // mnuCorrect
            // 
            this.mnuCorrect.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mnuCorrect.Image = ((System.Drawing.Image)(resources.GetObject("mnuCorrect.Image")));
            this.mnuCorrect.ImagePaddingHorizontal = 8;
            this.mnuCorrect.Name = "mnuCorrect";
            this.mnuCorrect.SubItemsExpandWidth = 24;
            this.mnuCorrect.Text = "&Correct Autochecks";
            this.mnuCorrect.Click += new System.EventHandler(this.mnuCorrect_Click);
            // 
            // munUndo
            // 
            this.munUndo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.munUndo.Image = ((System.Drawing.Image)(resources.GetObject("munUndo.Image")));
            this.munUndo.ImagePaddingHorizontal = 8;
            this.munUndo.Name = "munUndo";
            this.munUndo.SubItemsExpandWidth = 24;
            this.munUndo.Text = "&Undo";
            this.munUndo.Click += new System.EventHandler(this.munUndo_Click);
            // 
            // mnuClose
            // 
            this.mnuClose.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mnuClose.Image = ((System.Drawing.Image)(resources.GetObject("mnuClose.Image")));
            this.mnuClose.ImagePaddingHorizontal = 8;
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.SubItemsExpandWidth = 24;
            this.mnuClose.Text = "&Close";
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            // 
            // galleryContainer1
            // 
            // 
            // 
            // 
            this.galleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer1.EnableGalleryPopup = false;
            this.galleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer1.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer1.MultiLine = false;
            this.galleryContainer1.Name = "galleryContainer1";
            this.galleryContainer1.PopupUsesStandardScrollbars = false;
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem13});
            // 
            // buttonItem13
            // 
            this.buttonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem13.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem13.Image")));
            this.buttonItem13.ImagePaddingHorizontal = 8;
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.SubItemsExpandWidth = 24;
            this.buttonItem13.Text = "E&xit";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ImagePaddingHorizontal = 8;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "buttonItem1";
            // 
            // btnGenerateChecksum
            // 
            this.btnGenerateChecksum.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGenerateChecksum.Icon")));
            this.btnGenerateChecksum.ImagePaddingHorizontal = 8;
            this.btnGenerateChecksum.KeyTips = "GENERATE SCRIPT";
            this.btnGenerateChecksum.Name = "btnGenerateChecksum";
            this.btnGenerateChecksum.Tag = "Generate Script";
            this.btnGenerateChecksum.Text = "&Generate Script";
            this.btnGenerateChecksum.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // frmAutocheckUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 352);
            this.Controls.Add(this.ribbonControl1);
            this.MaximizeBox = false;
            this.Name = "frmAutocheckUtility";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Autocheck Utility";
            this.Load += new System.EventHandler(this.frmAutoCheckStartDateUtility_Load);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribCurrentPayment.ResumeLayout(false);
            this.ribCurrentPayment.PerformLayout();
            this.ribStartDate.ResumeLayout(false);
            this.ribStartDate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.Office2007StartButton office2007StartButton1;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ButtonItem btnGenerateChecksum;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonPanel ribStartDate;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabStartDate;
        private DevComponents.DotNetBar.ButtonItem mnuCorrect;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        public DevComponents.DotNetBar.ButtonItem mnuLoadAutochecks;
        private DevComponents.DotNetBar.ButtonItem munUndo;
        private DevComponents.DotNetBar.ButtonItem mnuClose;
        private DevComponents.DotNetBar.RibbonPanel ribCurrentPayment;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabCurrentPayment;
        private System.Windows.Forms.Button cmdUndoCurrentPayment;
        private System.Windows.Forms.Button cmdLoadAutocheckPayments;
        private System.Windows.Forms.Button cmdCorrectCurrentPayment;
        private System.Windows.Forms.Button cmdEndApp;
        private System.Windows.Forms.ListView lstAutoChecks;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdUndo;
        private System.Windows.Forms.Button cmdLoad;
        private System.Windows.Forms.Button cmdCorrect;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.ListView lstAutocheckData;
        private System.Windows.Forms.ColumnHeader AutobatchID;
        private System.Windows.Forms.ColumnHeader PayeeName;
        private System.Windows.Forms.ColumnHeader AutocheckAmount;
        private System.Windows.Forms.ColumnHeader StartDate;
        private System.Windows.Forms.ColumnHeader CorrectedStartDate;
        private System.Windows.Forms.ColumnHeader TotalPayments;
        private System.Windows.Forms.ColumnHeader CurrentPayment;
        private System.Windows.Forms.Label lblLastName;


    }
}

