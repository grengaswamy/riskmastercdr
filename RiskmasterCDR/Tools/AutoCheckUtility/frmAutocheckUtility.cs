﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Collections;
using Microsoft.Win32;
//using Riskmaster.Tools.ChecksumManager;

namespace AutoCheckUtility
{

    /// <summary>
    /// 
    /// 
    /// 
    /// 
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    public partial class frmAutocheckUtility : Form
    {

        string sCurrentPaymentAutoBatchIDs = string.Empty;
        string sStartDateAutoBatchIDs = string.Empty;

        #region // PUBLIC PROPERTIES
        enum DATABASE_DRIVERS
        {
            SQL_Server = 1,
            Oracle = 4
        }


        internal int DBTYPE
        {
            get;
            set;
        }//property: STATUS

        #endregion // PUBLIC PROPERTIES

        public frmAutocheckUtility()
        {
            InitializeComponent();
        }
        private void frmAutoCheckStartDateUtility_Load(object sender, EventArgs e)
        {
            this.DBTYPE = (int)DATABASE_DRIVERS.SQL_Server; //Default to SQL Server
            //cmbODBCDrivers.DataSource = GetODBCDrivers();
        }
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (ribStartDate.Visible == true)
            {
                subLoadAutoCheckStartDatePayments();
            }
            else
            {
                subLoadAutoCheckPayments();
            }
        }
        private void subLoadAutoCheckStartDatePayments()
        {
            bool bRecordsFound = false;
            string sAutoBatchID = string.Empty;
            string sPayeeName = string.Empty;
            string sAutocheckAmount = string.Empty;
            string sStartDate = string.Empty;
            string sCalculatedStartDate = string.Empty;
            string sTotalPayments = string.Empty;
            string sCurrentPayment = string.Empty;
            string sThisAutoCheckID = string.Empty;
            string sLastAutoCheckID = string.Empty;
            string sPayInterval = string.Empty;
            string sPrintDate = string.Empty;
            int iCurrentPayment = 0;


            lstAutocheckData.Items.Clear();
            sStartDateAutoBatchIDs = string.Empty;

            try
            {
                // Read appSettings
                //string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                string connectString = ConfigurationManager.ConnectionStrings["AutoCheckDatabase"].ConnectionString;

                SqlConnection conAutochecks = new SqlConnection(connectString);
                //SqlConnection conAutochecks = new SqlConnection("Data Source=(local);Initial Catalog=RMXR5;User Id=sa;Password=riskmaster;");
                conAutochecks.Open();
                SqlCommand comAutochecks = conAutochecks.CreateCommand();
                comAutochecks.CommandText = "SELECT FAB.AUTO_BATCH_ID, FAB.START_DATE, FAB.TOTAL_PAYMENTS, FAB.CURRENT_PAYMENT, FAB.PAYMENT_INTERVAL, FA.AMOUNT, FA.PRINT_DATE, FA.PAY_NUMBER, FA.LAST_NAME, FA.FIRST_NAME FROM FUNDS_AUTO_BATCH FAB, FUNDS_AUTO FA WHERE FA.AUTO_BATCH_ID = FAB.AUTO_BATCH_ID AND THIRD_PARTY_FLAG = 0 AND PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING) ORDER BY FAB.AUTO_BATCH_ID, FA.PAY_NUMBER";
                sLastAutoCheckID = "0";
                SqlDataReader rdr = comAutochecks.ExecuteReader();

                while (rdr.Read())
                {
                    sThisAutoCheckID = rdr["AUTO_BATCH_ID"].ToString();
                    if (sLastAutoCheckID != sThisAutoCheckID)
                    {
                        sAutoBatchID = rdr["AUTO_BATCH_ID"].ToString();
                        if (rdr["FIRST_NAME"].ToString() == string.Empty)
                        {
                            sPayeeName = rdr["LAST_NAME"].ToString();
                        }
                        else
                        {
                            sPayeeName = rdr["LAST_NAME"].ToString() + ", " + rdr["FIRST_NAME"].ToString();
                        }
                        sAutocheckAmount = rdr["AMOUNT"].ToString();

                        sTotalPayments = rdr["TOTAL_PAYMENTS"].ToString();
                        sCurrentPayment = rdr["CURRENT_PAYMENT"].ToString();
                        iCurrentPayment = Convert.ToInt32(sCurrentPayment);
                        sStartDate = rdr["START_DATE"].ToString();
                        sPayInterval = rdr["PAYMENT_INTERVAL"].ToString();
                        sPrintDate = rdr["PRINT_DATE"].ToString();

                        if (sStartDate.IndexOf('/') == -1)
                        {
                            sStartDate = sStartDate.Substring(4, 2) + "/" + sStartDate.Substring(6, 2) + "/" + sStartDate.Substring(0, 4);
                        }
                        else
                        {
                            sStartDate = sStartDate.Substring(6, 4) + sStartDate.Substring(0, 2) + sStartDate.Substring(3, 2);
                        }
                        if (sPrintDate.IndexOf('/') == -1)
                        {
                            sPrintDate = sPrintDate.Substring(4, 2) + "/" + sPrintDate.Substring(6, 2) + "/" + sPrintDate.Substring(0, 4);
                        }
                        else
                        {
                            sPrintDate = sPrintDate.Substring(6, 4) + sPrintDate.Substring(0, 2) + sPrintDate.Substring(3, 2);
                        }


                        sCalculatedStartDate = sGetCalculatedStartDate(sStartDate, sPayInterval, sPrintDate, iCurrentPayment);

                        if (sCalculatedStartDate != sStartDate)
                        {
                            string[] row = { sPayeeName, sAutocheckAmount, sStartDate, sCalculatedStartDate, sTotalPayments, sCurrentPayment };
                            lstAutocheckData.Items.Add(sAutoBatchID.ToString()).SubItems.AddRange(row);
                            if (sStartDateAutoBatchIDs == string.Empty)
                            {
                                sStartDateAutoBatchIDs = sAutoBatchID.ToString();
                            }
                            else
                            {
                                sStartDateAutoBatchIDs = sStartDateAutoBatchIDs + ", " + sAutoBatchID.ToString();
                            }
                            bRecordsFound = true;
                        }
                    }
                    sLastAutoCheckID = sThisAutoCheckID;
                }
                rdr.Close();
                conAutochecks.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
                bRecordsFound = true; //We prevent showing the message if we had an error
            }
            if (!bRecordsFound)
            {
                MessageBox.Show("There are no Auto Check records in need of correction.", "Autocheck Current Payment Utility");
            }
        }
        private void cmdLoad_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            subLoadAutoCheckStartDatePayments();
            Cursor = System.Windows.Forms.Cursors.Default;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            subEndApp();
        }

        private void mnuClose_Click(object sender, EventArgs e)
        {
            subEndApp();
        }

        private void subEndApp()
        {
            Application.Exit();
        }
        private void cmdUndo_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            subUndoStartDates();
            Cursor = System.Windows.Forms.Cursors.Default;
        }

        private void munUndo_Click(object sender, EventArgs e)
        {
            if (ribStartDate.Visible == true)
            {
                subUndoStartDates();
            }
            else
            {
                subUndoCurrentPayment();
            }
        }

        private void cmdCorrect_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            if (sStartDateAutoBatchIDs != string.Empty)
            {
                if (bPaymentNumbersCorrect())
                {
                    subCorrectStartDates();
                }
                else
                {
                    DialogResult Result;
                    Result = MessageBox.Show("It appears that the Current Payment Numbers are incorrect.  Do you want to ignore the incorrect Payment Numbers and correct the Start Dates anyway?", "Autocheck Start Date Utility", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    if (Result == DialogResult.Yes)
                    {
                        subCorrectStartDates();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Load Autocheck for correction.", "Autocheck Start Date Utility");
            }
            Cursor = System.Windows.Forms.Cursors.Default;
        }
        private bool bPaymentNumbersCorrect()
        {
            bool bPaymentNumbersCorrect = true;
            string sAutoBatchID = string.Empty;
            string sSQL = string.Empty;
            string sTemp = string.Empty;
            string sName = string.Empty;
            string sAmount = string.Empty;
            string sTotalPayments = string.Empty;
            string sCurrentPayments = string.Empty;
            string sPaymentsToDate = string.Empty;
            string sCheckCount = string.Empty;

            try
            {
                lstAutoChecks.Items.Clear();
                sCurrentPaymentAutoBatchIDs = string.Empty;

                sSQL = "SELECT AUTO_BATCH_ID, CURRENT_PAYMENT, PAYMENTS_TO_DATE, TOTAL_PAYMENTS FROM FUNDS_AUTO_BATCH ORDER BY AUTO_BATCH_ID";

                // Read appSettings
                string connectString = ConfigurationSettings.AppSettings["ConnectionString"];

                SqlConnection conAutochecks = new SqlConnection(connectString);
                conAutochecks.Open();
                SqlCommand comAutochecks = conAutochecks.CreateCommand();
                comAutochecks.CommandText = sSQL;
                SqlDataReader rdr = comAutochecks.ExecuteReader();
                while ((rdr.Read()) && (bPaymentNumbersCorrect))
                {
                    sSQL = "SELECT AUTO_BATCH_ID, END_PAY_NUMBER, COUNT(AUTO_BATCH_ID) CHECKCOUNT FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + Convert.ToInt32(rdr["AUTO_BATCH_ID"].ToString()) + " AND THIRD_PARTY_FLAG = 0 AND PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING) GROUP BY AUTO_BATCH_ID, END_PAY_NUMBER";
                    SqlConnection conAutochecks1 = new SqlConnection(connectString);
                    conAutochecks1.Open();
                    SqlCommand comAutochecks1 = conAutochecks1.CreateCommand();
                    comAutochecks1.CommandText = sSQL;
                    SqlDataReader rdr1 = comAutochecks1.ExecuteReader();
                    if (rdr1.Read())
                    {
                        if (Convert.ToInt32(rdr["PAYMENTS_TO_DATE"].ToString()) != ((Convert.ToInt32(rdr["TOTAL_PAYMENTS"].ToString())) - (Convert.ToInt32(rdr1["CHECKCOUNT"].ToString()))))
                        {
                            bPaymentNumbersCorrect = false;
                        }
                    }
                    conAutochecks1.Close();
                }
                conAutochecks.Close();

                return bPaymentNumbersCorrect;
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
            return bPaymentNumbersCorrect;
        }
        private void subCorrectStartDates()
        {
            DateTime dtDate = new DateTime();
            dtDate = System.DateTime.Today;
            string sDate = dtDate.ToString("MM/dd/yyyy");
            string sDate2 = sDate;
            string sCalculatedStartDate = string.Empty;
            string sStartDate = string.Empty;
            string sPayInterval = string.Empty;
            string sPrintDate = string.Empty;
            string sCurrentPayment = string.Empty;
            string sAutoBatchID = string.Empty;
            string sUndoFileLog = string.Empty;
            string sLogFile = string.Empty;
            string sSQL = string.Empty;
            int iThisAutoBatchID = 0;
            int iLastAutoBatchID = 0;
            int iCurrentPayment = 0;
            int iAutoBatchID = 0;
            int iCount = 0;

            try
            {
                if (sStartDateAutoBatchIDs != string.Empty)
                {
                    // Read appSettings
                    string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                    if (sDate.IndexOf('/') == -1)
                    {
                        sDate = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                    }
                    else
                    {
                        sDate = sDate.Substring(6, 4) + sDate.Substring(0, 2) + sDate.Substring(3, 2);
                    }

                    sLogFile = Application.StartupPath + "\\AutoCheckStartDateLog\\";
                    sUndoFileLog = Application.StartupPath + "\\AutoCheckStartDateUnDoLog\\";
                    string sDatabase = ConfigurationSettings.AppSettings["Database"];

                    // Call function for creating a directory
                    MakeDirectoryIfExists(sLogFile);
                    MakeDirectoryIfExists(sUndoFileLog);

                    sLogFile = Application.StartupPath + "\\AutoCheckStartDateLog\\" + sDate + "-AutoCheckLog.log";
                    sUndoFileLog = Application.StartupPath + "\\AutoCheckStartDateUnDoLog\\" + sDatabase + "-" + sDate + "-UndoLog.log";

                    //Pass the filepath and filename to the StreamWriter Constructor
                    StreamWriter swWriteLog = new StreamWriter(sLogFile);
                    StreamWriter swWriteUnDoLog = new StreamWriter(sUndoFileLog);

                    SqlConnection conAutochecksSD = new SqlConnection(connectString);
                    //SqlConnection conAutochecks = new SqlConnection("Data Source=(local);Initial Catalog=RMXR5;User Id=sa;Password=riskmaster;");
                    conAutochecksSD.Open();
                    SqlCommand comAutochecks = conAutochecksSD.CreateCommand();
                    comAutochecks.CommandText = "SELECT FAB.AUTO_BATCH_ID, FAB.START_DATE, FAB.TOTAL_PAYMENTS, FAB.CURRENT_PAYMENT, FAB.PAYMENT_INTERVAL, FA.AMOUNT, FA.PRINT_DATE, FA.PAY_NUMBER, FA.LAST_NAME, FA.FIRST_NAME FROM FUNDS_AUTO_BATCH FAB, FUNDS_AUTO FA WHERE FA.AUTO_BATCH_ID = FAB.AUTO_BATCH_ID AND THIRD_PARTY_FLAG = 0 AND PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING) AND FAB.AUTO_BATCH_ID IN (" + sStartDateAutoBatchIDs + ") ORDER BY FAB.AUTO_BATCH_ID, FA.PAY_NUMBER";
                    SqlDataReader rdr = comAutochecks.ExecuteReader();
                    iLastAutoBatchID = 0;
                    while (rdr.Read())
                    {
                        iThisAutoBatchID = Convert.ToInt32(rdr["AUTO_BATCH_ID"].ToString());
                        if (iThisAutoBatchID != iLastAutoBatchID)
                        {
                            sStartDate = rdr["START_DATE"].ToString();
                            sPayInterval = rdr["PAYMENT_INTERVAL"].ToString();
                            sPrintDate = rdr["PRINT_DATE"].ToString();
                            sCurrentPayment = rdr["CURRENT_PAYMENT"].ToString();
                            iCurrentPayment = Convert.ToInt32(sCurrentPayment);
                            sAutoBatchID = rdr["AUTO_BATCH_ID"].ToString();
                            iAutoBatchID = iThisAutoBatchID;

                            if (sStartDate.IndexOf('/') == -1)
                            {
                                sStartDate = sStartDate.Substring(4, 2) + "/" + sStartDate.Substring(6, 2) + "/" + sStartDate.Substring(0, 4);
                            }
                            else
                            {
                                sStartDate = sStartDate.Substring(6, 4) + sStartDate.Substring(0, 2) + sStartDate.Substring(3, 2);
                            }
                            if (sPrintDate.IndexOf('/') == -1)
                            {
                                sPrintDate = sPrintDate.Substring(4, 2) + "/" + sPrintDate.Substring(6, 2) + "/" + sPrintDate.Substring(0, 4);
                            }
                            else
                            {
                                sPrintDate = sPrintDate.Substring(6, 4) + sPrintDate.Substring(0, 2) + sPrintDate.Substring(3, 2);
                            }

                            swWriteLog.WriteLine("Auto Check Start Date Utility run on: " + sDate2);
                            swWriteLog.WriteLine("");

                            sCalculatedStartDate = sGetCalculatedStartDate(sStartDate, sPayInterval, sPrintDate, iCurrentPayment);

                            if (sCalculatedStartDate != sStartDate)
                            {
                                if (sStartDate.IndexOf('/') == -1)
                                {
                                    sStartDate = sStartDate.Substring(4, 2) + "/" + sStartDate.Substring(6, 2) + "/" + sStartDate.Substring(0, 4);
                                }
                                else
                                {
                                    sStartDate = sStartDate.Substring(6, 4) + sStartDate.Substring(0, 2) + sStartDate.Substring(3, 2);
                                }
                                if (sCalculatedStartDate.IndexOf('/') == -1)
                                {
                                    sCalculatedStartDate = sCalculatedStartDate.Substring(4, 2) + "/" + sCalculatedStartDate.Substring(6, 2) + "/" + sCalculatedStartDate.Substring(0, 4);
                                }
                                else
                                {
                                    sCalculatedStartDate = sCalculatedStartDate.Substring(6, 4) + sCalculatedStartDate.Substring(0, 2) + sCalculatedStartDate.Substring(3, 2);
                                }
                                SqlConnection conAutochecks2 = new SqlConnection(connectString);
                                conAutochecks2.Open();
                                swWriteLog.WriteLine("Batch ID = " + iAutoBatchID + ", Original Start Date = " + sStartDate + ", New Start Date = " + sCalculatedStartDate);
                                sSQL = "UPDATE FUNDS_AUTO_BATCH SET START_DATE = '" + sCalculatedStartDate + "' WHERE AUTO_BATCH_ID = " + iAutoBatchID;
                                swWriteUnDoLog.WriteLine("UPDATE FUNDS_AUTO_BATCH SET START_DATE = '" + sStartDate + "' WHERE AUTO_BATCH_ID = " + iAutoBatchID);
                                SqlCommand comAutochecks2 = conAutochecks2.CreateCommand();
                                comAutochecks2.CommandText = sSQL;
                                comAutochecks2.ExecuteNonQuery();
                                iCount = iCount + 1;
                                conAutochecks2.Close();
                            }
                        }
                        iLastAutoBatchID = iThisAutoBatchID;
                    }

                    if (iCount > 0)
                    {
                        MessageBox.Show("Finished correcting Auto Check Start Dates. " + iCount + " Auto Check records corrected.", "Autocheck Start Date Utility");
                    }
                    lstAutocheckData.Items.Clear();
                    sStartDateAutoBatchIDs = string.Empty;
                    conAutochecksSD.Close();
                    swWriteLog.Close();
                    swWriteUnDoLog.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
        }
        private void subUndoStartDates()
        {
            string sUndoFileLog = string.Empty;
            string sLine = string.Empty;
            string sSQL = string.Empty;


            try
            {
                OpenFileDialog dlgOpenFile = new OpenFileDialog();
                dlgOpenFile.Filter = "Log Files (*.log) |*.log";
                sUndoFileLog = Application.StartupPath + "\\AutoCheckStartDateUnDoLog\\";
                dlgOpenFile.InitialDirectory = sUndoFileLog;
                dlgOpenFile.Title = "Select a Log File To Process";
                dlgOpenFile.ShowDialog();
                sUndoFileLog = dlgOpenFile.FileName;

                string sDatabase = ConfigurationSettings.AppSettings["Database"];
                if (sUndoFileLog.IndexOf(sDatabase) == -1)
                {
                    MessageBox.Show("This Undo File was not created for this Database.  Please select another Undo file.", "Undo File Validation");
                }
                else
                {
                    StreamReader objReader = new StreamReader(sUndoFileLog);

                    ArrayList arrText = new ArrayList();
                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                            arrText.Add(sLine);
                    }
                    objReader.Close();

                    foreach (string sOutput in arrText)
                    {
                        //sSQL = sLine.Replace("'""'", "");
                        string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                        sSQL = sOutput;
                        SqlConnection conAutochecks = new SqlConnection(connectString);
                        conAutochecks.Open();
                        SqlCommand comAutochecks = conAutochecks.CreateCommand();
                        comAutochecks.CommandText = sSQL;
                        comAutochecks.ExecuteNonQuery();

                    }
                    MessageBox.Show("The Undo Process is complete.", "Undo Auto Check Current Payment Corrections");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
        }
        private void mnuCorrect_Click(object sender, EventArgs e)
        {
            if (ribStartDate.Visible == true)
            {
                cmdCorrect_Click(sender, e);
            }
            else
            {
                subCorrectCurrentPayment();
            }
        }

        private string sGetCalculatedStartDate(string sCurStartDate, string sPayInterval, string sPrintDate, int iNumOfPayments)
        {
            DateTime dtThisStartDate = Convert.ToDateTime(sCurStartDate);
            DateTime dtThisPrintDate = Convert.ToDateTime(sPrintDate);
            DateTime dtCalculatedStartDate = new DateTime();
            string sPayIntShortCode = string.Empty;
            string sCalulatedDate = string.Empty;


            try
            {
                sPayIntShortCode = sGetPayIntervalShortCode(sPayInterval);
                iNumOfPayments = (iNumOfPayments - 1);

                switch (sPayIntShortCode)
                {

                    case "PD":

                        dtCalculatedStartDate = dtThisPrintDate.AddDays(-iNumOfPayments);
                        break;

                    case "PW":

                        dtCalculatedStartDate = dtThisPrintDate.AddDays(-(iNumOfPayments * 7));
                        break;

                    case "PB":

                        dtCalculatedStartDate = dtThisPrintDate.AddDays(-(iNumOfPayments * 14));
                        break;

                    case "PM":

                        dtCalculatedStartDate = dtThisPrintDate.AddMonths(-iNumOfPayments);
                        break;

                    case "PQ":

                        dtCalculatedStartDate = dtThisPrintDate.AddMonths(-(iNumOfPayments * 3));
                        break;

                    case "PY":

                        dtCalculatedStartDate = dtThisPrintDate.AddYears(-iNumOfPayments);
                        break;

                }
                sCalulatedDate = dtCalculatedStartDate.ToString("MM/dd/yyyy");
                return sCalulatedDate;
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
            return sCalulatedDate;
        }
        private string sGetPayIntervalShortCode(string sThisPayInterval)
        {
            string sShortCode = string.Empty;
            string connectString = ConfigurationSettings.AppSettings["ConnectionString"];

            try
            {
                SqlConnection conAutochecks = new SqlConnection(connectString);
                conAutochecks.Open();
                SqlCommand comAutochecks = conAutochecks.CreateCommand();
                comAutochecks.CommandText = "SELECT SHORT_CODE FROM CODES WHERE CODE_ID = " + Convert.ToInt32(sThisPayInterval);
                SqlDataReader rdr = comAutochecks.ExecuteReader();
                if (rdr.Read())
                {
                    sShortCode = rdr["SHORT_CODE"].ToString();
                }
                conAutochecks.Close();
                return sShortCode;
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
            return sShortCode;
        }

        private void cmdLoadAutocheckPayments_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            subLoadAutoCheckPayments();
            Cursor = System.Windows.Forms.Cursors.Default;

        }

        private void MakeDirectoryIfExists(string NewDirectory)
        {
            try
            {
                // Check if directory exists
                if (!Directory.Exists(NewDirectory))
                {
                    // Create the directory.
                    Directory.CreateDirectory(NewDirectory);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
        }
        private void cmdCorrectCurrentPayment_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            subCorrectCurrentPayment();
            Cursor = System.Windows.Forms.Cursors.Default;
        }

        private void cmdUndoCurrentPayment_Click(object sender, EventArgs e)
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;
            subUndoCurrentPayment();
            Cursor = System.Windows.Forms.Cursors.Default;
        }

        private void cmdEndApp_Click(object sender, EventArgs e)
        {
            subEndApp();
        }
        private void subUndoCurrentPayment()
        {
            string sUndoFileLog = string.Empty;
            string sLine = string.Empty;
            string sSQL = string.Empty;

            try
            {
                OpenFileDialog dlgOpenFile = new OpenFileDialog();
                dlgOpenFile.Filter = "Log Files (*.log) |*.log";
                sUndoFileLog = Application.StartupPath + "\\AutoCheckCurrentPmtUnDoLog\\";
                dlgOpenFile.InitialDirectory = sUndoFileLog;
                dlgOpenFile.Title = "Select a Log File To Process";
                dlgOpenFile.ShowDialog();
                sUndoFileLog = dlgOpenFile.FileName;

                string sDatabase = ConfigurationSettings.AppSettings["Database"];
                if (sUndoFileLog.IndexOf(sDatabase) == -1)
                {
                    MessageBox.Show("This Undo File was not created for this Database.  Please select another Undo file.", "Undo File Validation");
                }
                else
                {
                    StreamReader objReader = new StreamReader(sUndoFileLog);

                    ArrayList arrText = new ArrayList();
                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                            arrText.Add(sLine);
                    }
                    objReader.Close();

                    foreach (string sOutput in arrText)
                    {
                        //sSQL = sLine.Replace("'""'", "");
                        string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                        sSQL = sOutput;
                        SqlConnection conAutochecks = new SqlConnection(connectString);
                        conAutochecks.Open();
                        SqlCommand comAutochecks = conAutochecks.CreateCommand();
                        comAutochecks.CommandText = sSQL;
                        comAutochecks.ExecuteNonQuery();
                        conAutochecks.Close();

                    }
                    MessageBox.Show("The Undo Process is complete.", "Undo Auto Check Current Payment Corrections");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
        }
        private void subLoadAutoCheckPayments()
        {
            bool bRecordsFound = false;
            string sAutoBatchID = string.Empty;
            string sSQL = string.Empty;
            string sTemp = string.Empty;
            string sName = string.Empty;
            string sAmount = string.Empty;
            string sTotalPayments = string.Empty;
            string sCurrentPayments = string.Empty;
            string sPaymentsToDate = string.Empty;
            int iTotalPayments = 0;
            string sCheckCount = string.Empty;
            int iCheckCount = 0;
            int iNumOfChecks = 0;

            try
            {
                lstAutoChecks.Items.Clear();
                sCurrentPaymentAutoBatchIDs = string.Empty;

                sSQL = "SELECT AUTO_BATCH_ID, CURRENT_PAYMENT, PAYMENTS_TO_DATE, TOTAL_PAYMENTS FROM FUNDS_AUTO_BATCH ORDER BY AUTO_BATCH_ID";

                // Read appSettings
                //string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                string connectString = ConfigurationManager.ConnectionStrings["AutoCheckDatabase"].ConnectionString;

                SqlConnection conAutochecks = new SqlConnection(connectString);
                //SqlConnection conAutochecks = new SqlConnection("Data Source=(local);Initial Catalog=RMXR5;User Id=sa;Password=riskmaster;");
                conAutochecks.Open();
                SqlCommand comAutochecks = conAutochecks.CreateCommand();
                comAutochecks.CommandText = sSQL;
                SqlDataReader rdr = comAutochecks.ExecuteReader();
                while (rdr.Read())
                {
                    sSQL = "SELECT AUTO_BATCH_ID, END_PAY_NUMBER, COUNT(AUTO_BATCH_ID) CHECKCOUNT FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + Convert.ToInt32(rdr["AUTO_BATCH_ID"].ToString()) + " AND THIRD_PARTY_FLAG = 0 AND PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING) GROUP BY AUTO_BATCH_ID, END_PAY_NUMBER";
                    SqlConnection conAutochecks1 = new SqlConnection(connectString);
                    conAutochecks1.Open();
                    SqlCommand comAutochecks1 = conAutochecks1.CreateCommand();
                    comAutochecks1.CommandText = sSQL;
                    SqlDataReader rdr1 = comAutochecks1.ExecuteReader();
                    if (rdr1.Read())
                    {
                        if (Convert.ToInt32(rdr["PAYMENTS_TO_DATE"].ToString()) != ((Convert.ToInt32(rdr["TOTAL_PAYMENTS"].ToString())) - (Convert.ToInt32(rdr1["CHECKCOUNT"].ToString()))))
                        {
                            sSQL = "SELECT FIRST_NAME, LAST_NAME, AMOUNT FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + Convert.ToInt32(rdr1["AUTO_BATCH_ID"].ToString());
                            SqlConnection conAutochecks2 = new SqlConnection(connectString);
                            conAutochecks2.Open();
                            SqlCommand comAutochecks2 = conAutochecks2.CreateCommand();
                            comAutochecks2.CommandText = sSQL;
                            SqlDataReader rdr2 = comAutochecks2.ExecuteReader();
                            if (rdr2.Read())
                            {
                                sTemp = Convert.ToString(rdr2["FIRST_NAME"]);
                                if (sTemp.Trim() != string.Empty)
                                {
                                    sName = Convert.ToString(rdr2["LAST_NAME"]);
                                    sName = sName.Trim() + ", " + sTemp.Trim();
                                }
                                else
                                {
                                    sName = Convert.ToString(rdr2["LAST_NAME"]);
                                    sName = sName.Trim();
                                }
                                sAutoBatchID = Convert.ToString(rdr1["AUTO_BATCH_ID"]);
                                sAmount = Convert.ToString(rdr2["AMOUNT"]);
                                sTotalPayments = Convert.ToString(rdr["TOTAL_PAYMENTS"]);
                                sCurrentPayments = Convert.ToString(rdr["CURRENT_PAYMENT"]);
                                iTotalPayments = Convert.ToInt32(sTotalPayments);
                                sCheckCount = Convert.ToString(rdr1["CHECKCOUNT"]);
                                iCheckCount = Convert.ToInt32(sCheckCount);
                                iNumOfChecks = ((iTotalPayments - iCheckCount) + 1);
                                sPaymentsToDate = Convert.ToString(rdr["PAYMENTS_TO_DATE"]);

                                bRecordsFound = true;

                                string[] row = { sName, sAmount, sTotalPayments, sCurrentPayments, iNumOfChecks.ToString(), sPaymentsToDate, sCheckCount };
                                lstAutoChecks.Items.Add(sAutoBatchID.ToString()).SubItems.AddRange(row);
                                if (sCurrentPaymentAutoBatchIDs == string.Empty)
                                {
                                    sCurrentPaymentAutoBatchIDs = sAutoBatchID.ToString();
                                }
                                else
                                {
                                    sCurrentPaymentAutoBatchIDs = sCurrentPaymentAutoBatchIDs + ", " + sAutoBatchID.ToString();
                                }
                            }
                            conAutochecks2.Close();
                        }
                    }
                    conAutochecks1.Close();
                }
                conAutochecks.Close();

                if (!bRecordsFound)
                {
                    MessageBox.Show("There are no Auto Check records in need of correction.", "Autocheck Current Payment Utility");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }

        }
        private void subCorrectCurrentPayment()
        {
            DateTime dtDate = new DateTime();
            dtDate = System.DateTime.Today;
            string sDate = dtDate.ToString("MM/dd/yyyy");
            string sDate2 = sDate;
            string sLogFile = string.Empty;
            string sUndoFileLog = string.Empty;
            string sSQL = string.Empty;
            int iTotalPayments = 0;
            int iOriginalCurrentPmt = 0;
            int iOriginalPmtToDate = 0;
            int iPaymentsMade = 0;
            int iRemainingPayments = 0;
            int iCheckCount = 0;
            int iAutoBatchID = 0;
            int iCount = 0;

            try
            {
                if (sCurrentPaymentAutoBatchIDs != string.Empty)
                {
                    string sDatabase = ConfigurationSettings.AppSettings["Database"];
                    if (sDate.IndexOf('/') == -1)
                    {
                        sDate = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                    }
                    else
                    {
                        sDate = sDate.Substring(6, 4) + sDate.Substring(0, 2) + sDate.Substring(3, 2);
                    }

                    sLogFile = Application.StartupPath + "\\AutoCheckCurrentPmtLog\\";
                    sUndoFileLog = Application.StartupPath + "\\AutoCheckCurrentPmtUnDoLog\\";


                    // Call function for creating a directory
                    MakeDirectoryIfExists(sLogFile);
                    MakeDirectoryIfExists(sUndoFileLog);

                    sLogFile = Application.StartupPath + "\\AutoCheckCurrentPmtLog\\" + sDate + "-AutoCheckLog.log";
                    sUndoFileLog = Application.StartupPath + "\\AutoCheckCurrentPmtUnDoLog\\" + sDatabase + "-" + sDate + "-UndoLog.log";

                    //Pass the filepath and filename to the StreamWriter Constructor
                    StreamWriter swWriteLog = new StreamWriter(sLogFile);
                    StreamWriter swWriteUnDoLog = new StreamWriter(sUndoFileLog);

                    swWriteLog.WriteLine("Auto Check Current Payment Utility run on: " + sDate2);
                    swWriteLog.WriteLine("");

                    string connectString = ConfigurationSettings.AppSettings["ConnectionString"];
                    sSQL = "SELECT * FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID IN (" + sCurrentPaymentAutoBatchIDs + ")";
                    SqlConnection conAutochecks = new SqlConnection(connectString);
                    conAutochecks.Open();
                    SqlCommand comAutochecks = conAutochecks.CreateCommand();
                    comAutochecks.CommandText = sSQL;
                    SqlDataReader rdr = comAutochecks.ExecuteReader();
                    while (rdr.Read())
                    {
                        iAutoBatchID = Convert.ToInt32(rdr["AUTO_BATCH_ID"]);
                        sSQL = "SELECT AUTO_BATCH_ID,  COUNT(AUTO_BATCH_ID) CHECKCOUNT FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + iAutoBatchID + " AND THIRD_PARTY_FLAG = 0 GROUP BY AUTO_BATCH_ID, END_PAY_NUMBER";
                        string connectString2 = ConfigurationSettings.AppSettings["ConnectionString"];
                        SqlConnection conAutochecks2 = new SqlConnection(connectString);
                        conAutochecks2.Open();
                        SqlCommand comAutochecks2 = conAutochecks2.CreateCommand();
                        comAutochecks2.CommandText = sSQL;
                        SqlDataReader rdr2 = comAutochecks2.ExecuteReader();
                        if (rdr2.Read())
                        {
                            iCheckCount = Convert.ToInt32(rdr2["CHECKCOUNT"]);
                            iTotalPayments = Convert.ToInt32(rdr["TOTAL_PAYMENTS"]);
                            iOriginalCurrentPmt = Convert.ToInt32(rdr["CURRENT_PAYMENT"]);
                            iOriginalPmtToDate = Convert.ToInt32(rdr["PAYMENTS_TO_DATE"]);
                            iPaymentsMade = iTotalPayments - iCheckCount;
                            iRemainingPayments = iTotalPayments - iPaymentsMade;

                            if (iOriginalCurrentPmt != (iCheckCount + 1))
                            {
                                SqlConnection conAutochecks3 = new SqlConnection(connectString);
                                conAutochecks3.Open();
                                swWriteLog.WriteLine("Batch ID = " + iAutoBatchID + ", Original Current Payment = " + iOriginalCurrentPmt + ", New Current Payment = " + (iPaymentsMade + 1) + " Original Payments To Date = " + iOriginalPmtToDate + " New Payments To Date = " + iPaymentsMade);
                                sSQL = "UPDATE FUNDS_AUTO_BATCH SET CURRENT_PAYMENT = " + (iPaymentsMade + 1) + ", PAYMENTS_TO_DATE = " + iPaymentsMade + " WHERE AUTO_BATCH_ID = " + iAutoBatchID;
                                swWriteUnDoLog.WriteLine("UPDATE FUNDS_AUTO_BATCH SET CURRENT_PAYMENT = " + iOriginalCurrentPmt + ", PAYMENTS_TO_DATE = " + iOriginalPmtToDate + " WHERE AUTO_BATCH_ID = " + iAutoBatchID);
                                SqlCommand comAutochecks3 = conAutochecks3.CreateCommand();
                                comAutochecks3.CommandText = sSQL;
                                comAutochecks3.ExecuteNonQuery();
                                iCount = iCount + 1;
                            }
                        }
                    }
                    swWriteLog.Close();
                    swWriteUnDoLog.Close();

                    if (iCount > 0)
                    {
                        MessageBox.Show("Finished correcting Auto Check records. " + iCount + " Auto Check records corrected.", "Autocheck Current Payment Utility");
                    }
                    lstAutoChecks.Items.Clear();
                    sCurrentPaymentAutoBatchIDs = string.Empty;
                }
                else
                {
                    MessageBox.Show("Please Load Autocheck for correction.", "Autocheck Current Payment Utility");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An Error has occured.  The error is: " + e.Message + " The Source is: " + e.Source + " The StackTrace is: " + e.StackTrace);
            }
        }

        private void ribbonTabCurrentPayment_Click(object sender, EventArgs e)
        {
            subClearForm();
        }
        private void subClearForm()
        {
            try
            {
                lstAutocheckData.Items.Clear();
                lstAutoChecks.Items.Clear();
                sStartDateAutoBatchIDs = string.Empty;
                sCurrentPaymentAutoBatchIDs = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An Error has occured.  The error is: " + ex.Message + " The Source is: " + ex.Source + " The StackTrace is: " + ex.StackTrace);
            }
        }

        private void ribbonTabStartDate_Click(object sender, EventArgs e)
        {
            subClearForm();
        }

    }

}

