/**************************************************************************
-- -Summary: Transact-SQL script that synchronizes database objects.
-- -Action:  Execute this script on 20.198.58.52.WMMIC_MCM_new

**************************************************************************/
BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET QUOTED_IDENTIFIER ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET ANSI_WARNINGS ON
	SET ARITHABORT ON
	SET NUMERIC_ROUNDABORT OFF
	SET CONCAT_NULL_YIELDS_NULL ON
	SET XACT_ABORT ON
COMMIT TRANSACTION
GO

IF EXISTS (select * from tempdb..sysobjects where id = OBJECT_ID('tempdb..#ErrorLog')) 
	DROP TABLE #ErrorLog 
CREATE TABLE #ErrorLog 
( pkid [int] IDENTITY(1,1) NOT NULL, errno [int] NOT NULL, errdescr [varchar](100) NULL )
GO

IF @@TRANCOUNT=0 BEGIN TRANSACTION
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE [dbo].[NDX_RMXEVNT] DROP CONSTRAINT [PK_NDX_RMXEVNT]
GO

IF @@ERROR<>0 
Begin
	IF @@TRANCOUNT>0 ROLLBACK TRANSACTION
	INSERT INTO #ErrorLog (errno,errdescr) values(@@ERROR,'Failed to drop primary key dbo.NDX_RMXEVNT.PK_NDX_RMXEVNT')
END
GO
IF @@TRANCOUNT=0 BEGIN TRANSACTION
GO
ALTER TABLE [dbo].[NDX_RMXEVNT]
	ALTER COLUMN [NDX_EVNTNBR] [char](22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 
Begin
	IF @@TRANCOUNT>0 ROLLBACK TRANSACTION
	INSERT INTO #ErrorLog (errno,errdescr) values(@@ERROR,'Failed to alter column dbo.NDX_RMXEVNT.NDX_EVNTNBR')
END
GO
IF @@TRANCOUNT=0 BEGIN TRANSACTION
GO
ALTER TABLE [dbo].[NDX_RMXEVNT]
	ALTER COLUMN [NDX_CLMNBR] [char](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 
Begin
	IF @@TRANCOUNT>0 ROLLBACK TRANSACTION
	INSERT INTO #ErrorLog (errno,errdescr) values(@@ERROR,'Failed to alter column dbo.NDX_RMXEVNT.NDX_CLMNBR')
END
GO
IF EXISTS (Select * from #ErrorLog)
BEGIN
	IF @@TRANCOUNT>0 ROLLBACK TRANSACTION
END
ELSE
BEGIN
	IF @@TRANCOUNT>0 COMMIT TRANSACTION
END
IF EXISTS (Select * from #ErrorLog)
BEGIN

	
	Print 'Database synchronization script failed'
	GOTO QuitWithErrors
END
ELSE
BEGIN
	update ndx_field_def
	set NFD_ATTR = 'LEN=22'
	where nfd_id = 'evntnbr'

	update ndx_field_def
	set NFD_ATTR = 'LEN=25'
	where nfd_id = 'clmnbr'


	Print 'Database synchronization completed successfully'
END



QuitWithErrors:


