﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using EventLogMonitorConfiguration;

namespace EventLogMonitorService
{
    public class EventLogMonitor
    {
        private EventLog m_EventLog = null;

        private string m_sLogName = string.Empty;
        private string m_sSource = string.Empty;
        private string m_sLevel = string.Empty;
        private string m_sDescription = string.Empty;
        private string m_sCommand = string.Empty;

        public EventLogMonitor(EventMonitorSectionType emType)
        {
            m_sLogName = emType.LogName;
            m_sSource = emType.Source;
            m_sLevel = emType.Level;
            m_sDescription = emType.Description;
            m_sCommand = emType.Command;

            m_EventLog = new EventLog(m_sLogName);
            m_EventLog.EntryWritten += new EntryWrittenEventHandler(EventLog_OnEntryWrite);
            m_EventLog.EnableRaisingEvents = true;

        }

        private void EventLog_OnEntryWrite(object source, EntryWrittenEventArgs e)
        {
            try
            {
                string sLogName = ((EventLog)source).LogDisplayName;
                if (IsSearchedEvent(sLogName, e.Entry))
                {
                    // create the ProcessStartInfo using "cmd" as the program to be run,
                    // and "/c " as the parameters.
                    // Incidentally, /c tells cmd that we want it to execute the command that follows,
                    // and then exit.
                    ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + m_sCommand);

                    // The following commands are needed to redirect the standard output.
                    // This means that it will be redirected to the Process.StandardOutput StreamReader.
                    procStartInfo.RedirectStandardOutput = true;
                    procStartInfo.UseShellExecute = false;
                    procStartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;

                    // Do not create the black window.
                    procStartInfo.CreateNoWindow = true;
                    // Now we create a process, assign its ProcessStartInfo and start it
                    System.Diagnostics.Process proc = new Process();
                    proc.StartInfo = procStartInfo;
                    proc.Start();
                    // Get the output into a string
                    string result = proc.StandardOutput.ReadToEnd();

                }
            }
            catch (Exception ex)
            {
                string sError = ex.Message;
            }
        }

        private bool IsSearchedEvent(string sLogName, EventLogEntry oEntry)
        {
            bool bSearchedEvent = false;
            string sSource = oEntry.Source;
            string sEventType = oEntry.EntryType.ToString();
            string sDetail = oEntry.Message;

            if (string.Compare(sSource, m_sSource, true) == 0 &&
                string.Compare(sLogName, m_sLogName, true) == 0 &&
                string.Compare(sEventType, m_sLevel, true) == 0)
            {
                if (sDetail.LastIndexOf(m_sDescription) >= 0)
                {
                    bSearchedEvent = true;
                }
            }

            return bSearchedEvent;
        }
    }
}
