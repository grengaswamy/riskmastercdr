﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using EventLogMonitorConfiguration;

namespace EventLogMonitorService
{
    public partial class EventLogMonitorService : ServiceBase
    {
        private List<EventLogMonitor> m_EventLogMonitors = new List<EventLogMonitor>();

        public EventLogMonitorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Help debug the OnStart
            string sDebugOnStart = ConfigurationManager.AppSettings["debugOnStart"];
            string sDebugOnStartTime = ConfigurationManager.AppSettings["debugOnStartTime"];
            int iDebugOnStartTime = 25000;
            bool bParse = int.TryParse(sDebugOnStartTime, out iDebugOnStartTime);
            if (sDebugOnStart == "true")
            {
                System.Threading.Thread.Sleep(iDebugOnStartTime);
            }

            EventMonitorSection oSection = ConfigurationManager.GetSection("eventMonitorGroup/eventMonitorSection") as EventMonitorSection;
            foreach (EventMonitorSectionType emType in oSection.MonitoredEvents)
            {
                EventLogMonitor oMonitor = new EventLogMonitor(emType);
                m_EventLogMonitors.Add(oMonitor);
            }
        }

        protected override void OnStop()
        {
            foreach (EventLogMonitor oMonitor in m_EventLogMonitors)
            {

            }
        }
    }
}
