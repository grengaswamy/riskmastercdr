﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using EventLogMonitorConfiguration;

namespace EventGenerator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string sSource = string.Empty;
            string sLog = string.Empty;
            string sLevel = string.Empty;
            string sDescription = string.Empty;

            sSource = txtSource.Text;
            sLog = txtLogName.Text;
            sLevel = cboLevel.Text;
            sDescription = txtDescription.Text;
            EventLogEntryType eType = GetEventType(sLevel);

            //Create Event Source if it doesn't exist
            if(!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }

            EventLog.WriteEntry(sSource, sDescription, eType);

        }

        private EventLogEntryType GetEventType(string sLevel)
        {
            EventLogEntryType eType = EventLogEntryType.Error;
            switch (sLevel)
            {
                case "Erro":
                    eType = EventLogEntryType.Error;
                    break;
                case "FailureAudit":
                    eType = EventLogEntryType.FailureAudit;
                    break;
                case "Information":
                    eType = EventLogEntryType.Information;
                    break;
                case "SuccessAudit":
                    eType = EventLogEntryType.SuccessAudit;
                    break;
                case "Warning":
                    eType = EventLogEntryType.Warning;
                    break;
            }

            return eType;

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            EventMonitorSection oSection = ConfigurationManager.GetSection("eventMonitorGroup/eventMonitorSection") as EventMonitorSection;

            string sLogName = string.Empty;
            string sSource = string.Empty;
            string sLevel = string.Empty;
            string sDescription = string.Empty;

            foreach (EventMonitorSectionType oEventType in oSection.MonitoredEvents)
            {
                sLogName = oEventType.LogName;
                sSource = oEventType.Source;
                sLevel = oEventType.Level;
                sDescription = oEventType.Description;
            }

            txtLogName.Text = sLogName;
            txtSource.Text = sSource;
            txtDescription.Text = sDescription;
            cboLevel.Text = sLevel;
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
