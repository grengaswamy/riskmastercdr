﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace EventLogMonitorConfiguration
{
    public class EventMonitorSectionTypeCollection : ConfigurationElementCollection
    {
        public EventMonitorSectionType this[int index]
        {
            get
            {
                return base.BaseGet(index) as EventMonitorSectionType;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new EventMonitorSectionType();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((EventMonitorSectionType)element).Name;
        }

    }
}
