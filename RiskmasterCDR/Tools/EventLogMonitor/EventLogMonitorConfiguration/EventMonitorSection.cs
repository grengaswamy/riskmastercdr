﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace EventLogMonitorConfiguration
{
    public class EventMonitorSection : ConfigurationSection
    {
        [ConfigurationProperty("monitoredEvents")]
        public EventMonitorSectionTypeCollection MonitoredEvents
        {
            get
            {
                return this["monitoredEvents"] as EventMonitorSectionTypeCollection;
            }
        }
    }
}
