﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace EventLogMonitorConfiguration
{
    public class EventMonitorSectionType : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("logName", IsRequired = true)]
        public string LogName
        {
            get
            {
                return this["logName"] as string;
            }
        }

        [ConfigurationProperty("source", IsRequired = true)]
        public string Source
        {
            get
            {
                return this["source"] as string;
            }
        }

        [ConfigurationProperty("level", IsRequired = true)]
        public string Level
        {
            get
            {
                return this["level"] as string;
            }
        }

        [ConfigurationProperty("description", IsRequired = true)]
        public string Description
        {
            get
            {
                return this["description"] as string;
            }
        }

        [ConfigurationProperty("command", IsRequired = false)]
        public string Command
        {
            get
            {
                return this["command"] as string;
            }
        }

        [ConfigurationProperty("email", IsRequired = false)]
        public string Email
        {
            get
            {
                return this["email"] as string;
            }
        }
    }
}
