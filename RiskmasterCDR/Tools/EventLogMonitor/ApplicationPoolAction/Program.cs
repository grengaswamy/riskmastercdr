﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.DirectoryServices;
using System.Diagnostics;

namespace ApplicationPoolAction
{
    class Program
    {
        static void Main(string[] args)
        {
            //Get the appPool name and action
            if (args.Length < 1)
            {
                LogMessage("Required parameter is missing. Usage: ApplicationPoolAction.exe appPoolName");
                return;
            }

            string appPoolId = args[0];
            string server = "localhost";

            string act = "Recylce";
            if (args.Length > 1)
            {
                act = args[1];
            }

            string appPoolPath = "IIS://localhost/W3SVC/AppPools/" + appPoolId;
            try
            {
                DirectoryEntry appPoolEntry = new DirectoryEntry(appPoolPath);
                appPoolEntry.Invoke("Recycle");
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message + " StackTrace: " + ex.StackTrace.ToString());
            }

            /*
            ConnectionOptions co = new ConnectionOptions();
            co.Username = @"xhu-PC\xhu";
            co.Password = "riskmaster";
            co.Impersonation = ImpersonationLevel.Impersonate;
            co.Authentication = AuthenticationLevel.PacketPrivacy;
            string objPath = "IISApplicationPool.Name='W3SVC/AppPools/" + appPoolId + "'";

            ManagementScope scope = new ManagementScope(@"\\" + server + @"\root\MicrosoftIISV2", co);
            using (ManagementObject mc = new ManagementObject(objPath))
            {
                mc.Scope = scope;
                switch (act)
                {
                    case "Start":
                        mc.InvokeMethod("Start", null, null);
                        break;
                    case "Stop":
                        mc.InvokeMethod("Stop", null, null);
                        break;
                    case "Recycle":
                        mc.InvokeMethod("Recycle", null, null);
                        break;

                }

            }
             */
        }

        //Log any message to the Application Event Log
        private static void LogMessage(string sMessage)
        {
            string sSource = "AppPoolAction";
            string sLog = "Application";
            EventLogEntryType eType = EventLogEntryType.Error;

            try
            {
                //Create Event Source if it doesn't exist
                if (!EventLog.SourceExists(sSource))
                {
                    EventLog.CreateEventSource(sSource, sLog);
                }

                EventLog.WriteEntry(sSource, sMessage, eType);
            }
            catch (Exception ex)
            {
                //Do nothing here.
            }
        }
    }
}
