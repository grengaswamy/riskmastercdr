<%@ Page language="c#" Codebehind="CreateNewResource.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.CreateNewResource" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreateNewResource</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="AspTestingTool.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:image id="Image1" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 9px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Width="479px" Height="44px"></asp:image><INPUT id="hidFormAction" style="Z-INDEX: 106; LEFT: 533px; WIDTH: 105px; POSITION: absolute; TOP: 12px; HEIGHT: 25px"
				type="hidden" size="12" name="hidFormAction"><INPUT id="btnAddResource" style="Z-INDEX: 105; LEFT: 28px; WIDTH: 174px; POSITION: absolute; TOP: 186px; HEIGHT: 42px"
				onclick="AddResource('','');" type="button" value="Add Image Resources" name="btnAddResource">
			<asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 24px; POSITION: absolute; TOP: 61px" runat="server"
				Width="479px" Height="33px" Font-Size="X-Large">Add Images to Resource File</asp:label>
			<asp:hyperlink id="lnkMain" style="Z-INDEX: 102; LEFT: 527px; POSITION: absolute; TOP: 58px" runat="server"
				Width="128px" Height="26px" NavigateUrl="Main.aspx">Return to Main</asp:hyperlink>
			<asp:label id="Label1" style="Z-INDEX: 104; LEFT: 23px; POSITION: absolute; TOP: 128px" runat="server"
				Width="161px" Height="16px">1.  Select culture</asp:label>
			<asp:Table id="tblResources" style="Z-INDEX: 107; LEFT: 232px; POSITION: absolute; TOP: 153px"
				runat="server" Height="194px" Width="512px"></asp:Table>
			<asp:Button id="btnCompile" style="Z-INDEX: 108; LEFT: 33px; POSITION: absolute; TOP: 240px"
				runat="server" Height="42px" Width="170px" Text="Compile"></asp:Button>
			<asp:DropDownList id="selCultures" style="Z-INDEX: 109; LEFT: 28px; POSITION: absolute; TOP: 154px"
				runat="server" Height="26px" Width="171px"></asp:DropDownList>
		</form>
	</body>
</HTML>
