if exists (select * from dbo.sysobjects where id = object_id(N'[RESOURCE_DICTIONARY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RESOURCE_DICTIONARY]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RESOURCE_DICTIONARY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [RESOURCE_DICTIONARY] (
	[RESOURCE_DICTIONARY_ID] [int] NOT NULL ,
	[GROUP_ID] [int] NOT NULL ,
	[NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VALUE] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DEFAULT_VALUE] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCRIPTION] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TYPE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
	CONSTRAINT [PK_RESOURCE_DICTIONARY] PRIMARY KEY  NONCLUSTERED 
	(
		[RESOURCE_DICTIONARY_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
END

GO

if exists (select * from dbo.sysindexes where name = N'IX_RESOURCE_DICTIONARY_GROUP_ID' and id = object_id(N'[dbo].[RESOURCE_DICTIONARY]'))
drop index [dbo].[RESOURCE_DICTIONARY].[IX_RESOURCE_DICTIONARY_GROUP_ID]
GO

 CREATE  CLUSTERED  INDEX [IX_RESOURCE_DICTIONARY_GROUP_ID] ON [dbo].[RESOURCE_DICTIONARY]([GROUP_ID]) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[RESOURCE_DICTIONARY_GROUPS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RESOURCE_DICTIONARY_GROUPS]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RESOURCE_DICTIONARY_GROUPS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [RESOURCE_DICTIONARY_GROUPS] (
	[GROUP_ID] [int] NOT NULL ,
	[NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCRIPTION] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TYPE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CULTURE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_RESOURCE_DICTIONARY_GROUPS_GROUP_ID] PRIMARY KEY  CLUSTERED 
	(
		[GROUP_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
END

GO