//resource identifier is: [JavaScript file name without extension].[function name if inside function/variable name if module-level variable].[short descriptor]
//token is: <~[resource identifier]"[text]"~>

var ExistRecAlert=<~Form.js.ExistRecAlert"Existing record alert"~>;
var DataHasChanged=<~Form.js.DataHasChanged"Data Has Changed alert"~>;
var DupeError=<~Form.js.DupeError"Duplication Error"~>;

function pageLoaded()
{
	alert(EXISTREC_ALERT);
}
function btnPush()
{
	var s = StringFormat(<~Form.js.btnPush.VarSelect"The first var {0}, the second var {1}, the third var {2}"~>,["one","two","three"]);
	alert(s);
	var s2=<~Form.js.btnPush.VarTwo"This is variable number 2"~>;
	alert(s2);	
}

//ape C# String.Format()
function StringFormat(s,arr)
{
	for(i=0;i<arr.length;i++)
	{
		s=s.replace('\{'+i+'\}',arr[i]);
	}
	return s;
}