using System;
using System.Web.UI.HtmlControls;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Xml;
using System.Globalization;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for Functions.
	/// </summary>
	public class Functions
	{
		private string m_connectionString=String.Empty;
		public Functions(){}
		public Functions(string connectionString)
		{
			m_connectionString=connectionString;
		}
		/// <summary>
		/// Read directory structure under "master" and insert into Resource_Dictionary_Groups:
		/// subfolders under "img" are groups
		/// individual files in "js" are groups
		/// individual files in "resx" are groups (one Global.resx file for each assembly referenced by the main asp.net application, named AssemblyName.Global.resx)
		/// individual files in "xml" are groups
		/// </summary>
		public void insertResources(string sCulture)
		{
			//remove any old records
			DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString);
			DbCommand DbCmd;
			StringBuilder sbSQL = new StringBuilder("DELETE FROM RESOURCE_DICTIONARY WHERE GROUP_ID IN (SELECT GROUP_ID FROM RESOURCE_DICTIONARY_GROUPS WHERE CULTURE = '"+sCulture+"')");
			sbSQL.Append(" ;DELETE FROM RESOURCE_DICTIONARY_GROUPS WHERE CULTURE='"+sCulture+"'");

			try
			{
				DbConn.Open();			
				DbCmd  = DbConn.CreateCommand();
				DbCmd.CommandText=sbSQL.ToString();
				DbCmd.ExecuteNonQuery();
			}
			catch(Exception e)
			{
				DbConn.Dispose();
				throw new Exception("Data access error",e);
			}
			int GroupId=1;
			int ResourceId=1;

			string sMasterFolder = HttpContext.Current.Server.MapPath("").ToString()+"\\master";
			string sCultureFolder=sMasterFolder.Replace("\\master","\\bin\\"+sCulture);
			#region create directory structure
			//raise error if the master folder does not exist
			if(!Directory.Exists(sMasterFolder))
			{
				throw new Exception(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),sMasterFolder));
				
			}
			if(!Directory.Exists(sMasterFolder+"\\img"))
			{
				throw new Exception(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),sMasterFolder+"\\img"));
				
			}
			//create the culture folder and 'img' subfolder if it does not exist
			if(!Directory.Exists(sCultureFolder))
			{
				try
				{
					Directory.CreateDirectory(sCultureFolder);
				}
				catch(Exception e)
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCreateFolderError"),sCultureFolder),e);
				}
			}
			if(!Directory.Exists(sCultureFolder+"\\img"))
			{
				try
				{
					Directory.CreateDirectory(sCultureFolder+"\\img");
				}
				catch(Exception e)
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCreateFolderError"),sCultureFolder+"\\img"),e);
				}
			}
			//directory that holds default images, so user can restore default
			if(!Directory.Exists(sCultureFolder+"\\img\\default"))
			{
				try
				{
					Directory.CreateDirectory(sCultureFolder+"\\img\\default");
				}
				catch(Exception e)
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCreateFolderError"),sCultureFolder+"\\img\\default"),e);
				}
			}
			#endregion

			sbSQL = new StringBuilder("");
			DbReader rdr = null;
			//get new unique id starting point.  obviously this not work with multiple users
			//TODO need to lock writing to these tables
			try
			{
				string sSQL="Select IsNull(Max(Group_Id),0) From Resource_Dictionary_Groups";
				rdr = DbConn.ExecuteReader(sSQL);
				if(rdr.Read())
					GroupId=Convert.ToInt32(rdr.GetValue(0).ToString())+1;
				rdr.Close();

				sSQL="Select IsNull(Max(Resource_Dictionary_Id),0) From Resource_Dictionary";
				rdr=DbConn.ExecuteReader(sSQL);
				if(rdr.Read())
					ResourceId=Convert.ToInt32(rdr.GetValue(0).ToString())+1;
				rdr.Close();
			}
			catch(Exception e)
			{
				rdr.Dispose();
				DbConn.Dispose();
				throw new Exception("Data access error",e);
			}

			//begin the transaction
			DbTransaction DbTrans=DbCmd.Connection.BeginTransaction();
			DbCmd.Transaction=DbTrans;

			//read the disk and add each group of resources
			DbParameter p;
			try
			{
				#region add images
				string[] arrFolders = Directory.GetDirectories(sMasterFolder + "\\img");
				string[] arrFiles;
				string sFile=String.Empty;
				string sName=String.Empty;
				string sValue=String.Empty;
				string sDefaultValue=String.Empty;
				string sFolder=String.Empty;
				string s=String.Empty;
				foreach (string folder in arrFolders) 
				{
					//add the folder name to resource_dictionary_groups
					sFolder = folder.ToString();
					sName=sFolder.Substring(sFolder.LastIndexOf("\\")+1);

					DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY_GROUPS (GROUP_ID,NAME,TYPE,CULTURE) VALUES(~GROUP_ID~,~NAME~,'Images','"+sCulture+"')";

					p = DbCmd.CreateParameter();
					p.ParameterName="GROUP_ID";
					p.Value=GroupId;
					DbCmd.Parameters.Add(p);

					p = DbCmd.CreateParameter();
					p.ParameterName="NAME";
					p.Value=sName.ToString();
					DbCmd.Parameters.Add(p);

					DbCmd.ExecuteNonQuery();
					DbCmd.Parameters.Clear();

					//copy each image in the folder to the culture folder and the culture's default folder,
					//then add each to RESOURCE_DICTIONARY
					arrFiles=Directory.GetFiles(sFolder);
					foreach (string file in arrFiles)
					{
						sFile=file.ToString();
						sName=sFile.Substring(sFile.LastIndexOf("\\")+1);
						sValue=sCultureFolder+"\\img\\"+sName;
						sDefaultValue=sCultureFolder+"\\img\\default\\"+sName;
						try
						{
							System.IO.File.Copy(sFile,sValue,true);
							System.IO.File.Copy(sFile,sDefaultValue,true);
						}
						catch(Exception e)
						{
							throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCopyFileError"),sFile,sValue),e);
						}

						DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~)";

						p = DbCmd.CreateParameter();
						p.ParameterName="RESOURCE_DICTIONARY_ID";
						p.Value=ResourceId;
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="GROUP_ID";
						p.Value=GroupId;
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="NAME";
						p.Value=sName.ToString();
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="VALUE";
						p.Value=sValue.ToString();
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="DEFAULT_VALUE";
						p.Value=sDefaultValue.ToString();
						DbCmd.Parameters.Add(p);

						DbCmd.ExecuteNonQuery();
						ResourceId=ResourceId+1;
						DbCmd.Parameters.Clear();
					}
					GroupId=GroupId+1;
				}
				#endregion

				#region add javascript files
				/*
				s = sMasterFolder + "\\js";
				if(!Directory.Exists(s))
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),s));
				
				}
				arrFiles = Directory.GetFiles(s);
				foreach (string file in arrFiles) 
				{
					s = file.ToString();
					s=s.Substring(s.LastIndexOf("\\")+1);
					if (file.EndsWith(".js"))
					{
						DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY_GROUPS (GROUP_ID,NAME,TYPE,CULTURE) VALUES(~GROUP_ID~,~NAME~,'JavaScript','"+m_culture.ToString()+"')";

						p = DbCmd.CreateParameter();
						p.ParameterName="GROUP_ID";
						p.Value=GroupId;
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="NAME";
						p.Value=s.ToString();
						DbCmd.Parameters.Add(p);

						DbCmd.ExecuteNonQuery();
						DbCmd.Parameters.Clear();
						//now add each token in the javascript file to RESOURCE_DICTIONARY
						Regex reg = new Regex("<~(.|\n)+?~>",RegexOptions.IgnoreCase|RegexOptions.Compiled);
						Match m;
						string sTag;
						string sText;
						string sIdentifier;
						StreamReader sr = new StreamReader(file.ToString());
						s=sr.ReadToEnd();
						for (m = reg.Match(s.ToString()); m.Success; m = m.NextMatch()) 
						{
							sTag=m.Value;
							sIdentifier=sTag.Substring(2,sTag.IndexOf("\"")-2);
							sText=sTag.Remove(0,sTag.IndexOf("\""));
							sText=sText.Replace("~>","");

							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=sIdentifier.ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=sText.ToString();
							DbCmd.Parameters.Add(p);

							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						GroupId=GroupId+1;
					}
				}
				*/
				#endregion

				#region add *.resx files
				//Global.resx is the name for every default/fallback resource file for every RM assembly.
				//files in "master\resx" folder must be renamed "AssemblyName.Global.resx"
				XmlDocument xmldoc = new XmlDocument();
				XmlNodeList list;

				s = sMasterFolder + "\\resx";
				if(!Directory.Exists(s))
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),s));
				
				}
				arrFiles = Directory.GetFiles(s);
				foreach (string file in arrFiles) 
				{
					s = file.ToString();
					s=s.Substring(s.LastIndexOf("\\")+1);
					if (file.EndsWith(".resx"))
					{
						DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY_GROUPS (GROUP_ID,NAME,TYPE,CULTURE) VALUES(~GROUP_ID~,~NAME~,'Resources','"+sCulture+"')";

						p = DbCmd.CreateParameter();
						p.ParameterName="GROUP_ID";
						p.Value=GroupId;
						DbCmd.Parameters.Add(p);

						p = DbCmd.CreateParameter();
						p.ParameterName="NAME";
						p.Value=s.ToString();
						DbCmd.Parameters.Add(p);

						DbCmd.ExecuteNonQuery();
						DbCmd.Parameters.Clear();
						//now read each resource from the resx file into resource_dictionary
						xmldoc.Load(file.ToString());
						list = xmldoc.SelectNodes("//data");
						foreach(System.Xml.XmlNode elem in list)
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,DESCRIPTION) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~DESCRIPTION~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=elem.Attributes["name"].InnerText.ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=elem.SelectSingleNode("value").InnerText.ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=elem.SelectSingleNode("value").InnerText.ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DESCRIPTION";
							if(elem.SelectSingleNode("comment")!=null)
								p.Value=elem.SelectSingleNode("comment").InnerText.ToString();
							else
								p.Value=String.Empty;
							DbCmd.Parameters.Add(p);
						
							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						GroupId=GroupId+1;
					}
				}
				#endregion

				#region add page definition files
				s = sMasterFolder + "\\xml";
				if(!Directory.Exists(s))
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),s));
				
				}
				arrFiles = Directory.GetFiles(s);
				foreach (string file in arrFiles) 
				{
					s = file.ToString();
					s=s.Substring(s.LastIndexOf("\\")+1);
					if (file.EndsWith(".xml"))
					{
						DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY_GROUPS (GROUP_ID,NAME,TYPE,CULTURE) VALUES(~GROUP_ID~,~NAME~,'Page','"+sCulture+"')";
						p = DbCmd.CreateParameter();
						p.ParameterName="GROUP_ID";
						p.Value=GroupId;
						DbCmd.Parameters.Add(p);
						p = DbCmd.CreateParameter();
						p.ParameterName="NAME";
						p.Value=s.ToString();
						DbCmd.Parameters.Add(p);
						DbCmd.ExecuteNonQuery();
						DbCmd.Parameters.Clear();
						//now read each resource from the xml file into resource_dictionary
						xmldoc.Load(file.ToString());
						if(xmldoc.DocumentElement.GetAttribute("title")!="")
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,TYPE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~TYPE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value="title";
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=xmldoc.DocumentElement.GetAttribute("title");
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=xmldoc.DocumentElement.GetAttribute("title");
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="TYPE";
							p.Value="Page Title";
							DbCmd.Parameters.Add(p);

							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						//all controls
						list = xmldoc.SelectNodes("//control");
						foreach(System.Xml.XmlElement elem in list)
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,TYPE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~TYPE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=elem.GetAttribute("name").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="TYPE";
							p.Value="Input";
							DbCmd.Parameters.Add(p);
						
							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}

						list = xmldoc.SelectNodes("form/toolbar/button");
						foreach(System.Xml.XmlElement elem in list)
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,TYPE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~TYPE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=elem.GetAttribute("type").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="TYPE";
							p.Value="Toolbar button hover";
							DbCmd.Parameters.Add(p);
						
							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						list = xmldoc.SelectNodes("form/button");
						foreach(System.Xml.XmlElement elem in list)
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,TYPE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~TYPE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=elem.GetAttribute("name").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="TYPE";
							p.Value="Button caption";
							DbCmd.Parameters.Add(p);
						
							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						//all groups
						list = xmldoc.SelectNodes("//group");
						foreach(System.Xml.XmlElement elem in list)
						{
							DbCmd.CommandText = "INSERT INTO RESOURCE_DICTIONARY (RESOURCE_DICTIONARY_ID,GROUP_ID,NAME,VALUE,DEFAULT_VALUE,TYPE) VALUES(~RESOURCE_DICTIONARY_ID~,~GROUP_ID~,~NAME~,~VALUE~,~DEFAULT_VALUE~,~TYPE~)";

							p = DbCmd.CreateParameter();
							p.ParameterName="RESOURCE_DICTIONARY_ID";
							p.Value=ResourceId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="GROUP_ID";
							p.Value=GroupId;
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="NAME";
							p.Value=elem.GetAttribute("name").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="DEFAULT_VALUE";
							p.Value=elem.GetAttribute("title").ToString();
							DbCmd.Parameters.Add(p);

							p = DbCmd.CreateParameter();
							p.ParameterName="TYPE";
							p.Value="Group title";
							DbCmd.Parameters.Add(p);
						
							DbCmd.ExecuteNonQuery();
							ResourceId=ResourceId+1;
							DbCmd.Parameters.Clear();
						}
						GroupId=GroupId+1;
					}
				}
				#endregion

				DbCmd.Transaction.Commit();
			}
			catch(Exception e)
			{
				DbCmd.Transaction.Rollback();
				throw new Exception("Unable to commit transaction, error message: "+e.Message.ToString(),e);
			}
			finally
			{
				DbCmd.Transaction.Dispose();
				DbConn.Close();
				DbConn.Dispose();
			}

		}
		public void getResourceSets(DataGrid dg,string sCulture)
		{
			string sSQL = "SELECT GROUP_ID,NAME,DESCRIPTION FROM RESOURCE_DICTIONARY_GROUPS WHERE CULTURE='"+sCulture+"'";
			using (DbReader rdr= DbFactory.GetDbReader(m_connectionString,sSQL))
				{
					dg.DataSource=rdr;
					dg.DataBind();
					rdr.Dispose();
				}

			/*
			 *this does not work, columns must be defined on the aspx page (?MS bug?)
			EditCommandColumn ed = new EditCommandColumn();
			ed.EditText="Edit";
			ed.CancelText="Cancel";
			ed.UpdateText="Update";
			ed.ButtonType=ButtonColumnType.LinkButton;
			dg.Columns.Add(ed);

			BoundColumn col = new BoundColumn();
			col.DataField="GROUP_ID";
			col.HeaderText="Group Id";
			col.ReadOnly=true;
			dg.Columns.Add(col);

			col = new BoundColumn();
			col.DataField="NAME";
			col.HeaderText="Name";
			col.ReadOnly=true;
			dg.Columns.Add(col);

			col = new BoundColumn();
			col.DataField="DESCRIPTION";
			col.HeaderText="Description";
			dg.Columns.Add(col);
			*/

		}
		/// <summary>
		/// Each Resource_Dictionary.Group_Id represents a logical set of resources (e.g. a collection of images, a page definition file, etc.)
		/// Function fills dropdown list with names from Resource_Dictionary_Groups for the desired culture
		/// </summary>
		public void getResourceSets(System.Web.UI.WebControls.DropDownList selResourceSets,string sCulture)
		{
			//note: if add list items to a listitemcollection and databind the listitemcollection to dropdownlist,
			//the "value" gets lost and the value instead becomes the "text" property (? MS bug?)
			string sSQL = "SELECT GROUP_ID,DESCRIPTION FROM RESOURCE_DICTIONARY_GROUPS WHERE CULTURE='"+sCulture+"'";
			using(DbReader rdr = DbFactory.GetDbReader(m_connectionString,sSQL))
			{
				bool b=false;
				ListItem li;
				//add a blank item
				selResourceSets.Items.Add(new ListItem("","0"));
				while(rdr.Read()) 
				{
					li = new ListItem();
					li.Text=rdr.GetValue(1).ToString();
					li.Value=rdr.GetValue(0).ToString();
					selResourceSets.Items.Add(li);
					//lic.Add(new ListItem(rdr.GetValue(1).ToString(),rdr.GetValue(0).ToString()));
					b=true;
				}
				if(!b)
					selResourceSets.Items.Add(new ListItem("Zero resource sets are in database for the selected culture.","0"));
			}


		}
		/// <summary>
		/// Get all culture subfolders that have been created on this machine and load into dropdown list.
		/// </summary>
		/// <param name="selCultures"></param>
		public static void getCultures(System.Web.UI.WebControls.DropDownList selCultures)
		{
			//fetch all subfolders under bin, put in resource picker dropdown
			string sBinFolder = HttpContext.Current.Server.MapPath("").ToString()+"\\bin";
			if(!Directory.Exists(sBinFolder))
			{
				throw new Exception("The folder "+sBinFolder+" does not exist, unable to access resources.");
			}
			string[] arrFolders = Directory.GetDirectories(sBinFolder);
			ListItemCollection lic = new ListItemCollection();

			string s = "";
			foreach (string folder in arrFolders) 
			{
				s = folder.ToString();
				lic.Add(new ListItem(s.Substring(s.LastIndexOf("\\")+1)));
			}
			selCultures.DataSource=lic;
			selCultures.DataBind();
		}

		/// <summary>
		/// Return list of all possible cultures from the .Net list
		/// </summary>
		/// <param name="selAllCultures"></param>
		public static void getAllCultures(System.Web.UI.WebControls.DropDownList selAllCultures)
		{
			ListItem li;
			foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
			{
				li=new ListItem(ci.DisplayName.ToString(),ci.Name.ToString());
				selAllCultures.Items.Add(li);
			}
			selAllCultures.DataBind();
		}
	}
}
