<%@ Page language="c#" Codebehind="ReplaceImage.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.ReplaceImage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReplaceImage</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="AspTestingTool.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Label id="Label1" style="Z-INDEX: 101; LEFT: 266px; POSITION: absolute; TOP: 26px" runat="server"
				Width="128px" Height="29px" Font-Bold="True" Font-Size="Medium">Replace Image</asp:Label>
			<asp:Image id="imgDefault" style="Z-INDEX: 109; LEFT: 426px; POSITION: absolute; TOP: 68px"
				runat="server" Width="96px" Height="72px" Visible="False"></asp:Image>
			<asp:Label id="lblDefault" style="Z-INDEX: 108; LEFT: 287px; POSITION: absolute; TOP: 66px"
				runat="server" Width="121px" Height="27px" Visible="False">Default image</asp:Label>
			<asp:Image id="Image1" style="Z-INDEX: 104; LEFT: 167px; POSITION: absolute; TOP: 65px" runat="server"
				Width="96px" Height="72px"></asp:Image><INPUT id="txtFile" style="Z-INDEX: 102; LEFT: 22px; WIDTH: 487px; POSITION: absolute; TOP: 186px; HEIGHT: 25px"
				type="file" size="62" name="txtFile" runat="server">
			<asp:Image id="imgCurrent" style="Z-INDEX: 103; LEFT: 167px; POSITION: absolute; TOP: 65px"
				runat="server" Width="96px" Height="72px"></asp:Image>
			<asp:Label id="lblCurrent" style="Z-INDEX: 105; LEFT: 26px; POSITION: absolute; TOP: 70px"
				runat="server" Width="121px" Height="27px">Current image</asp:Label>
			<asp:Label id="lblFile" style="Z-INDEX: 106; LEFT: 26px; POSITION: absolute; TOP: 152px" runat="server"
				Width="160px" Height="27px">Select replacement</asp:Label>
			<asp:Button id="btnSubmit" style="Z-INDEX: 107; LEFT: 22px; POSITION: absolute; TOP: 232px"
				runat="server" Width="95px" Height="33px" Text="Replace"></asp:Button><INPUT runat="server" id="hidAction" style="Z-INDEX: 110; LEFT: 462px; WIDTH: 52px; POSITION: absolute; TOP: 9px; HEIGHT: 27px"
				type="hidden" size="3"><INPUT runat="server" id="hidId" style="Z-INDEX: 111; LEFT: 534px; WIDTH: 44px; POSITION: absolute; TOP: 9px; HEIGHT: 26px"
				type="hidden" size="2"> <INPUT style="Z-INDEX: 112; LEFT: 149px; WIDTH: 73px; POSITION: absolute; TOP: 229px; HEIGHT: 39px"
				type="button" value="Button" id="btnReplace" name="btnReplace" onclick="submitChangeImage()">
		</form>
	</body>
</HTML>
