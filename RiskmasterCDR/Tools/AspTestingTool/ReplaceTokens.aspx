<%@ Page language="c#" Codebehind="ReplaceTokens.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.ReplaceTokens" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReplaceTokens</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Image id="Image1" style="Z-INDEX: 101; LEFT: 18px; POSITION: absolute; TOP: 11px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Height="44px" Width="479px"></asp:Image>
			<asp:Label id="lblTitle" style="Z-INDEX: 102; LEFT: 21px; POSITION: absolute; TOP: 61px" runat="server"
				Height="33px" Width="331px" Font-Size="X-Large">Generate JavaScript Files </asp:Label>
			<p></p>
			<BR>
			&nbsp;
			<asp:Label id="Label1" style="Z-INDEX: 103; LEFT: 27px; POSITION: absolute; TOP: 122px" runat="server"
				Height="16px" Width="161px">1.  Select culture</asp:Label>
			<asp:Button id="btnReplaceTokens" style="Z-INDEX: 104; LEFT: 29px; POSITION: absolute; TOP: 191px"
				runat="server" Width="111px" Height="26px" Text="Generate .JS"></asp:Button>
			<asp:Label id="lblResults" style="Z-INDEX: 105; LEFT: 151px; POSITION: absolute; TOP: 192px"
				runat="server" Width="556px" Height="162px">Results</asp:Label>
			<asp:HyperLink id="lnkMain" style="Z-INDEX: 106; LEFT: 384px; POSITION: absolute; TOP: 71px" runat="server"
				Width="106px" Height="26px" NavigateUrl="Main.aspx">Return to Main</asp:HyperLink>
			<asp:DropDownList id="selCultures" style="Z-INDEX: 107; LEFT: 31px; POSITION: absolute; TOP: 149px"
				runat="server" Width="164px" Height="25px"></asp:DropDownList>
		</form>
	</body>
</HTML>
