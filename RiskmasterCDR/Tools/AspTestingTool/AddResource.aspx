<%@ Page language="c#" Codebehind="AddResource.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.AddResouce" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AddResouce</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
		function AddResource()
		{
			var sValue='';
			for(var i=0;i<document.Form1.rdoAllOrOne.length;i++)
			{
				if(document.Form1.rdoAllOrOne[i].checked)
				{
					sValue=document.Form1.rdoAllOrOne[i].value;
					break;
				}
			}
			window.opener.AddResource(document.Form1.txtKey.value,document.Form1.txtFile.value,sValue);
			window.close();
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<INPUT id="txtKey" title="Key" style="Z-INDEX: 100; LEFT: 97px; WIDTH: 212px; POSITION: absolute; TOP: 64px; HEIGHT: 27px"
				type="text" size="30" name="txtKey">
			<DIV title="Add a Resource" style="DISPLAY: inline; FONT-SIZE: larger; Z-INDEX: 101; LEFT: 24px; WIDTH: 449px; FONT-FAMILY: Arial; POSITION: absolute; TOP: 10px; HEIGHT: 37px"
				align="center" ms_positioning="FlowLayout">Add a Resource</DIV>
			<DIV style="DISPLAY: inline; Z-INDEX: 102; LEFT: 31px; WIDTH: 56px; POSITION: absolute; TOP: 64px; HEIGHT: 27px"
				ms_positioning="FlowLayout">
				<P>Key</P>
			</DIV>
			<DIV style="DISPLAY: inline; Z-INDEX: 103; LEFT: 33px; WIDTH: 56px; POSITION: absolute; TOP: 105px; HEIGHT: 24px"
				ms_positioning="FlowLayout">Source</DIV>
			<INPUT id="txtFile" style="Z-INDEX: 104; LEFT: 98px; WIDTH: 487px; POSITION: absolute; TOP: 103px; HEIGHT: 25px"
				type="file" size="62" name="txtFile"> <INPUT id="btnAdd" style="Z-INDEX: 105; LEFT: 34px; WIDTH: 70px; POSITION: absolute; TOP: 225px; HEIGHT: 31px"
				type="button" value="Add" name="btnAdd" onclick="AddResource();"><INPUT id="btnCancel" style="Z-INDEX: 106; LEFT: 115px; WIDTH: 76px; POSITION: absolute; TOP: 224px; HEIGHT: 32px"
				type="button" value="Cancel" name="btnCancel" onclick="window.close();"> <INPUT style="Z-INDEX: 107; LEFT: 155px; WIDTH: 35px; POSITION: absolute; TOP: 136px; HEIGHT: 25px"
				type="radio" CHECKED value="one" name="rdoAllOrOne">
			<DIV id="lblOne" title="Just this file" style="DISPLAY: inline; Z-INDEX: 110; LEFT: 39px; WIDTH: 91px; POSITION: absolute; TOP: 138px; HEIGHT: 25px"
				ms_positioning="FlowLayout">
				<P>Just this file</P>
			</DIV>
			<INPUT style="Z-INDEX: 111; LEFT: 156px; WIDTH: 35px; POSITION: absolute; TOP: 170px; HEIGHT: 25px"
				type="radio" value="all" name="rdoAllOrOne">
			<DIV id="Div1" title="Just this file" style="DISPLAY: inline; Z-INDEX: 112; LEFT: 38px; WIDTH: 112px; POSITION: absolute; TOP: 172px; HEIGHT: 26px"
				ms_positioning="FlowLayout">
				<P>All files in folder</P>
			</DIV>
		</form>
	</body>
</HTML>
