<%@ Page language="c#" Codebehind="Main.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.Main" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Main</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Image id="Image1" style="Z-INDEX: 100; LEFT: 27px; POSITION: absolute; TOP: 16px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Height="44px" Width="479px"></asp:Image>
			<asp:Label id="Label7" style="Z-INDEX: 114; LEFT: 45px; POSITION: absolute; TOP: 467px" runat="server"
				Width="408px" Height="37px">View all environment variables.</asp:Label>
			<asp:Label id="Label6" style="Z-INDEX: 113; LEFT: 47px; POSITION: absolute; TOP: 421px" runat="server"
				Width="408px" Height="37px">Add images to a resource dll (we will probably not do this).</asp:Label>
			<asp:Label id="Label5" style="Z-INDEX: 112; LEFT: 32px; POSITION: absolute; TOP: 340px" runat="server"
				Width="408px" Height="37px" Font-Bold="True">Other (not needed for resource creation):</asp:Label>
			<asp:Label id="Label4" style="Z-INDEX: 111; LEFT: 52px; POSITION: absolute; TOP: 377px" runat="server"
				Width="408px" Height="37px">Read all strings for any of this project's resource dlls.</asp:Label>
			<asp:Label id="Label3" style="Z-INDEX: 110; LEFT: 28px; POSITION: absolute; TOP: 271px" runat="server"
				Width="408px" Height="37px">3.  Create javascript files (template .js files with tokens are in the "master" folder; tokens are replaced with strings from the culture's resource dll, compiled in step 2.)</asp:Label>
			<asp:Label id="Label2" style="Z-INDEX: 109; LEFT: 31px; POSITION: absolute; TOP: 186px" runat="server"
				Width="408px" Height="37px">2.  Edit the string resources (actually editing Resource_Dictionary table) or replace image resources (image files get replaced).  When done editing strings, compile a new resource dll for a culture from the Resource_Dictionary table.</asp:Label>
			<asp:HyperLink id="lnkEditResources" style="Z-INDEX: 107; LEFT: 460px; POSITION: absolute; TOP: 195px"
				runat="server" Width="189px" Height="36px" NavigateUrl="EditResources.aspx">Edit And Compile Resources</asp:HyperLink>
			<asp:HyperLink id="lnkMngResources" style="Z-INDEX: 106; LEFT: 461px; POSITION: absolute; TOP: 113px"
				runat="server" Width="189px" Height="36px" NavigateUrl="ManageResourceDictionary.aspx">Create Resource Set</asp:HyperLink>
			<asp:HyperLink id="lnkEnvVars" style="Z-INDEX: 105; LEFT: 469px; POSITION: absolute; TOP: 475px"
				runat="server" Width="189px" Height="36px" NavigateUrl="Env.aspx">Environment Variables</asp:HyperLink>
			<asp:HyperLink id="lnkReadResources" style="Z-INDEX: 103; LEFT: 461px; POSITION: absolute; TOP: 364px"
				runat="server" Width="184px" Height="30px" NavigateUrl="ResourceReader.aspx">Read Resources</asp:HyperLink>
			<asp:Label id="lblTitle" style="Z-INDEX: 101; LEFT: 25px; POSITION: absolute; TOP: 66px" runat="server"
				Height="33px" Width="630px" Font-Size="X-Large">3 Steps to create all resources for a culture.</asp:Label>
			<asp:HyperLink id="lnkReplaceTokens" style="Z-INDEX: 102; LEFT: 464px; POSITION: absolute; TOP: 276px"
				runat="server" Width="187px" Height="30px" NavigateUrl="ReplaceTokens.aspx">Generate JavaScript Files</asp:HyperLink>
			<asp:HyperLink id="lnkCreateEditResource" style="Z-INDEX: 104; LEFT: 472px; POSITION: absolute; TOP: 426px"
				runat="server" Width="189px" Height="36px" NavigateUrl="CreateNewResource.aspx">Add Images</asp:HyperLink>
			<asp:Label id="Label1" style="Z-INDEX: 108; LEFT: 30px; POSITION: absolute; TOP: 111px" runat="server"
				Width="408px" Height="37px">1.  Create a set of resources for a culture.  This copies all resources from the "master" folder into a culture folder, and creates a new resources set in Resource_Dictionary tables.</asp:Label>
		</form>
	</body>
</HTML>
