using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Riskmaster.Db;
using Riskmaster.Common;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for ReplaceImage.
	/// </summary>
	public class ReplaceImage : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Image imgCurrent;
		protected System.Web.UI.WebControls.Label lblCurrent;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Label lblDefault;
		protected System.Web.UI.WebControls.Image imgDefault;
		protected System.Web.UI.WebControls.Label lblFile;
		protected System.Web.UI.HtmlControls.HtmlInputFile txtFile;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidAction;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidId;
		protected System.Web.UI.WebControls.Label Label1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				string sAction=Request["action"];
				string sId=Request["id"].ToString();

				this.hidAction.Value=sAction.ToString();
				this.hidId.Value=sId.ToString();
				//store these 2 items in view state,can use them again on submit
				//this.ViewState["action"]=sAction;
				//this.ViewState["id"]=sId;

				string sValue=String.Empty;
				string sDefaultValue=String.Empty;

				//fetch path to the image from db and set the image control url
				string sSQL="Select Value,Default_Value From Resource_Dictionary Where Resource_Dictionary_Id="+sId;
				using(DbReader rdr = DbFactory.GetDbReader(Application["WebFarmSession"].ToString(),sSQL))
				{
					if(rdr.Read())
					{
						sValue=rdr.GetValue(0).ToString();
						sDefaultValue=rdr.GetValue(1).ToString();
					}
					this.imgCurrent.ImageUrl="file:///"+sValue;
				}

				switch(sAction.ToLower())
				{
					case "default":    //over-write the existing image with the default
						this.txtFile.Visible=false;
						this.lblFile.Visible=false;
						imgDefault.Visible=true;
						imgDefault.ImageUrl="file:///"+sDefaultValue;
						lblDefault.Visible=true;
						break;
					case "change":	  //over-write the image with file the user chooses with file picker
						this.imgDefault.Visible=false;
						this.lblDefault.Visible=false;
						break;
					default:
						break;
				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			string sAction=this.ViewState["action"].ToString();
			string sId=this.ViewState["id"].ToString();
			string sNewImageFile=String.Empty;
			string sValue=String.Empty;
			string sDefaultValue=String.Empty;

			string sSQL="Select Value,Default_Value From Resource_Dictionary Where Resource_Dictionary_Id="+sId;
			using(DbReader rdr = DbFactory.GetDbReader(Application["WebFarmSession"].ToString(),sSQL))
			{
				if(rdr.Read())
				{
					sValue=rdr.GetValue(0).ToString();
					sDefaultValue=rdr.GetValue(1).ToString();
				}
			}

			switch(sAction.ToLower())
			{
				case "change":	  //over-write the image with file the user chooses with file picker
					sNewImageFile=txtFile.Value.ToString();
					break;
				case "default":  //over-write the existing image with the default
					sNewImageFile=sDefaultValue;
					break;
				default:
					break;
			}

			//copy the new over the old
			try
			{
				System.IO.File.Copy(sNewImageFile,sValue,true);
			}
			catch(Exception ex)
			{
				throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCopyFileError"),sNewImageFile,sValue),ex);
			}

		}
	}
}
