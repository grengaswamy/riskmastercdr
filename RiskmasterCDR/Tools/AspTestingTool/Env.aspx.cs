using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for Env.
	/// </summary>
	public class Env : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Table tblResources;
		protected System.Web.UI.WebControls.Image Image1;
		TableRow tRow;
		TableCell tCell;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			IDictionary    environmentVariables = Environment.GetEnvironmentVariables();
			foreach (DictionaryEntry de in environmentVariables)
			{
				tRow = new TableRow();
				tblResources.Rows.Add(tRow);

				tCell = new TableCell();
				tCell.Text = de.Key.ToString();
				tRow.Cells.Add(tCell);

				tCell = new TableCell();
				tCell.Text = de.Value.ToString();
				tRow.Cells.Add(tCell);

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
