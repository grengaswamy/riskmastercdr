using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Riskmaster.Db;
using Riskmaster.Common;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for ManageResourceDictionary.
	/// </summary>
	public class ManageResourceDictionary : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Button btnRegenerateGroups;
		protected System.Web.UI.WebControls.DataGrid dg;
		protected System.Web.UI.WebControls.DropDownList selCultures;
		private Functions objFunctions;
		protected System.Web.UI.WebControls.Label Label1;
		private string m_connectionString;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			//selResourceSets.Items.Clear();
			this.m_connectionString=this.Application["WebFarmSession"].ToString();
			objFunctions = new Functions(this.m_connectionString.ToString());
			if(!Page.IsPostBack)
			{
				Functions.getAllCultures(this.selCultures);
				string sCulture=this.selCultures.SelectedValue;
				objFunctions.getResourceSets(this.dg,sCulture);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.selCultures.SelectedIndexChanged += new System.EventHandler(this.selCultures_SelectedIndexChanged);
			this.btnRegenerateGroups.Click += new System.EventHandler(this.btnRegenerateGroups_Click);
			this.dg.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand);
			this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
			this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// Read all subfolders from under master folder, dump all into Resource_Dictionary_Groups and Resource_Dictionary
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnRegenerateGroups_Click(object sender, System.EventArgs e)
		{
			string sCulture=this.selCultures.SelectedValue;
			objFunctions.insertResources(sCulture);
			objFunctions.getResourceSets(this.dg,sCulture);
		}

		//post-backs from datagrid when user selects "Edit", "Update", or "Cancel"
		private void dg_EditCommand(object sender, DataGridCommandEventArgs e)
		{
			//each editable cell in this row is rendered as a text input
			dg.EditItemIndex=e.Item.ItemIndex;
			string sCulture=this.selCultures.SelectedValue;
			objFunctions.getResourceSets(this.dg,sCulture);
		}
		private void dg_CancelCommand(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex=-1;
			string sCulture=this.selCultures.SelectedValue;
			objFunctions.getResourceSets(this.dg,sCulture);
		}
		private void dg_UpdateCommand(object sender, DataGridCommandEventArgs e)
		{
			TextBox txtDesc = (TextBox)e.Item.Cells[3].Controls[0];
 
			// Retrieve the updated values.
			int groupID = Convert.ToInt32(e.Item.Cells[1].Text.ToString());
			string desc = txtDesc.Text;

			//update database
			string sSQL="UPDATE RESOURCE_DICTIONARY_GROUPS SET DESCRIPTION='"+desc.ToString()+"' WHERE GROUP_ID=" + groupID;
			using(DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString))
			{
				DbConn.Open();			
				DbCommand DbCmd  = DbConn.CreateCommand();
				DbCmd.CommandText=sSQL.ToString();
				DbCmd.ExecuteNonQuery();
			}

			//tell datagrid editing is over
			dg.EditItemIndex=-1;
			string sCulture=this.selCultures.SelectedValue;
			objFunctions.getResourceSets(this.dg,sCulture);
		}

		private void selCultures_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string sCulture=this.selCultures.SelectedValue;
			objFunctions.getResourceSets(this.dg,sCulture);
		}
	}
}
