var m_Wnd;
document.AddResource=AddResource;
document.replaceImage=replaceImage;

function AddResource(sKey,sValue,sAllOrOne)
{
	if(m_Wnd!=null)
		m_Wnd.close();

	if(sValue!='')
	{
		document.Form1.hidFormAction.value="addresource"+"|"+sKey+"|"+sValue+"|"+sAllOrOne;
		document.Form1.submit();
	}
	else
	{
		var s = "addResource.aspx";
		m_Wnd=window.open(s,'codeWnd',
			'width=700,height=350'+',top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');
	}
	
	return false;

}

//EditResources.aspx submits itself to replace the desired image
function replaceImage(action,id,file)
{
	document.Form1.hidFormAction.value="action="+action+"&id="+id+"&file="+file;
	document.Form1.submit();
}
//EditResources.aspx, user chooses to replace or set default value for an image
function changeImage(ctl,action)
{
	//popup for user to select a new image
	var s = "ReplaceImage.aspx?action="+action+"&id="+ctl.title;
	m_Wnd=window.open(s,'codeWnd',
		'width=700,height=350'+',top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');

}
//ReplaceImage.aspx calls back EditResources.aspx to replace an image
function submitChangeImage()
{
	var sFile='';
	if(eval(document.Form1.txtFile))
		sFile=document.Form1.txtFile.value;
	window.opener.replaceImage(document.Form1.hidAction.value,document.Form1.hidId.value,sFile);
	window.close();
}