using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Riskmaster.Common;
using System.Resources;
using System.Text;
using System.IO;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for ResourceReader.
	/// </summary>
	public class ResourceReader : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Button btnReadResources;
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.Table tblResources;
		protected System.Web.UI.WebControls.Button btnReadResx;
		protected System.Web.UI.WebControls.DropDownList selCultures;
		TableRow tRow;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.HtmlControls.HtmlInputFile fileResx;
		TableCell tCell;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
				Functions.getCultures(this.selCultures);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnReadResources.Click += new System.EventHandler(this.btnReadResources_Click);
			this.btnReadResx.Click += new System.EventHandler(this.btnReadResx_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		/// <summary>
		/// Read the resources from the resource dll selected by user and load into table
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnReadResources_Click(object sender, System.EventArgs e)
		{
			StringBuilder sResults=new StringBuilder(String.Empty);
			string sLang=this.selCultures.SelectedValue.ToString();
			CultureInfo ci = new CultureInfo(sLang,false);

			string sType=String.Empty;
			//both of next 2 lines work, but use Riskmaster.Globalization
			//ResourceManager rm = new ResourceManager("AspTestingTool.Global",typeof(ResourceReader).Assembly);
			//ResourceManager rm = (ResourceManager)Application["ResourceManager"];
			ResourceManager rm = Globalization.ResourceManager;
			ResourceSet rs = rm.GetResourceSet(ci,true,true);
			IDictionaryEnumerator en = rs.GetEnumerator();
			enumDictItems(en);
			tblResources.Visible=true;
			//rs.Dispose();  NO, do not dispose.  throws runtime error "resource set is closed" if used more than once
			//probably because Globalization.ResourceManager is static
			
		}

		private void btnReadResx_Click(object sender, System.EventArgs e)
		{

			string sType=String.Empty;
			string sLang=this.selCultures.SelectedValue;
			string sResx=this.fileResx.Value.ToString();
			ResXResourceReader rsxr=null;
			//string sResx=Server.MapPath("").ToString()+"\\master\\resx\\Global."+sLang+".resx";
			if(sResx.Length!=0 && File.Exists(sResx))
			{
				try
				{
					rsxr = new ResXResourceReader(sResx);
					IDictionaryEnumerator en = rsxr.GetEnumerator();
					enumDictItems(en);
					tblResources.Visible=true;
				}
				catch(Exception ex)
				{
					throw new Exception("Error reading resource file",ex);
				}
				finally
				{
					if(rsxr!=null)
						rsxr.Close();
				}
			}

		}
		private void enumDictItems(IDictionaryEnumerator en)
		{
			string sType;
			while (en.MoveNext())
			{
				sType=en.Value.GetType().ToString();

				tRow = new TableRow();
				tblResources.Rows.Add(tRow);

				tCell = new TableCell();
				tCell.Text = en.Key.ToString();
				tRow.Cells.Add(tCell);

				tCell = new TableCell();
				tCell.Text = sType;
				tRow.Cells.Add(tCell);

				tCell = new TableCell();
				switch(sType.ToLower())
				{
					case "system.string":
						tCell.Text = en.Value.ToString();
						break;
					default:
						break;
				}
				tRow.Cells.Add(tCell);
				
			}
		}
	}
}
