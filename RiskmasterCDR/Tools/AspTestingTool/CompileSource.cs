using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Collections;

namespace AspTestingTool
{
	/// <summary>
	/// Note:  this will NOT work from Asp.net unless machine.config is changed (in .Net framework "CONFIG" folder, as of today is C:\WINNT\Microsoft.NET\Framework\v1.1.4322\CONFIG
	/// processmodel element in machine.config must change from processModel  userName="machine" to userName="SYSTEM"
	/// warning--need to investigate if this opens any security vulnerabilities
	/// see MS KB 317012 INFO: Process and Request Identity in ASP.NET
	/// </summary>
	public class CompileSource
	{
		public static string GetCompilerPath()
		{
			string baseDir = System.Environment.GetEnvironmentVariable("SystemRoot").ToString() + "\\Microsoft.NET\\Framework";
			DirectoryInfo dirInfo = new DirectoryInfo(baseDir);
			ArrayList folders = new ArrayList();

			//get all folders in framework directory that contain compiler
			foreach (DirectoryInfo subDir in dirInfo.GetDirectories())
			{
				//if this folder contains the compiler, add it to the collection
				if(subDir.GetFiles("al.exe").Length!=0)
					folders.Add(subDir.FullName.ToString());
			}

			if(folders.Count==0)
				return "";

			//get the latest version
			folders.Sort();
			return folders[folders.Count-1].ToString();
		}

		/// <summary>
		/// Execute a .Net framework compiler with instructions sCommandLine and output to sPath
		/// Note:  this will NOT work from Asp.net unless machine.config is changed (in .Net framework "CONFIG" folder, as of today is C:\WINNT\Microsoft.NET\Framework\v1.1.4322\CONFIG
		/// processmodel element in machine.config must change from processModel  userName="machine" to userName="SYSTEM"
		/// warning--need to investigate if this opens any security vulnerabilities
		/// see MS KB 317012 INFO: Process and Request Identity in ASP.NET
		/// </summary>
		/// <param name="sCommand">the phrase "al.exe" and all arguments</param>
		/// <param name="sPath">the folder into which the dll is to be created</param>
		public static void DoCompile(string sCommand,string sPath)
		{
			// create a batch file to do the build...
			StreamWriter batchFile = File.CreateText(sPath+"\\build.bat");
			string compilerPath = GetCompilerPath();
			batchFile.WriteLine("set path=%path%;{0}", compilerPath);
			batchFile.WriteLine(sCommand);
			batchFile.Close();

			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = "cmd.exe";
			psi.Arguments = "/c " + sPath + "\\build.bat";
			psi.CreateNoWindow=true;
			psi.WindowStyle = ProcessWindowStyle.Hidden;
			psi.UseShellExecute=true;
			psi.WorkingDirectory=sPath;

			Process proc = Process.Start(psi);
			proc.WaitForExit();

		}
	}
}
