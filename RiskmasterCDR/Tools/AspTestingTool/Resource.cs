using System;
using System.Web;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.IO;

namespace ResourceCodeBehind
{
	///<summary>
	///Code-behind for *.ashx page.
	///Directive in *.ashx page that points to this code-behind page must contain "class=[namespace].[class]"
	///Example of directive: @ WebHandler Language="C#" class="ResourceCodeBehind.Resource" 
	///</summary>
	public class Resource : IHttpHandler
	{
		public Resource()
		{
			//
			// no constructor needed
			//
		}
		/// <summary>
		/// This runs when the *.ashx page is called.
		/// </summary>
		/// <example>(inside a script tag)script language="javascript" src="Global.ashx?res=form.js&amp;res2=form2.js">/script</example>
		public void ProcessRequest (System.Web.HttpContext context)
		{
			//note: next line will NOT fetch the culture off of the current thread as defined in page load of the aspx page that causes this to run.  
			//the culture here is ALWAYS en-US, irregardless of browser setttings or the culture set in page load event (this could be a MS bug)
			//string sLang=(string)Thread.CurrentThread.CurrentUICulture.ToString();
			string sLang = context.Request.UserLanguages[0].ToString();
			string sFile;
			string sPath;
			string sKey;
			string sRoot=context.Server.MapPath("").ToString();

			System.Collections.Specialized.NameValueCollection coll=context.Request.QueryString;	
			String[] collKeys = coll.AllKeys;

			for(int i=0;i<collKeys.Length;i++)
			{	
				//each key in querystring will indicate a file that should be returned
				sFile=(string)coll.Get(collKeys[i]);
				sKey=collKeys[i];
				switch(sKey.Substring(0,3).ToLower())
				{
					case "fil":
						sPath = sRoot+"\\bin\\" + sLang + "\\" + sFile;
						break;
					case "img":
						sPath = sRoot+"\\bin\\" + sLang + "\\img\\" + sFile;
						break;
					default:
						sPath = sRoot+"\\bin\\" + sLang + "\\" + sFile;
						break;
				}			
				if(File.Exists(sPath.ToString()))
					context.Response.WriteFile(sPath.ToString());
			}		
		}

		public bool IsReusable
		{
			//true means can be pooled by Asp.Net runtime
			get { return true; }
		}
	}

}
