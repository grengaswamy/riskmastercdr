<%@ Page language="c#" Codebehind="ManageResourceDictionary.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.ManageResourceDictionary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ManageResourceDictionary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:image id="Image1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Height="44px" Width="479px"></asp:image>
			<asp:DropDownList id="selCultures" style="Z-INDEX: 106; LEFT: 38px; POSITION: absolute; TOP: 156px"
				runat="server" Width="172px" Height="24px" AutoPostBack="True"></asp:DropDownList><asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 41px; POSITION: absolute; TOP: 63px" runat="server"
				Height="33px" Width="535px" Font-Size="X-Large">Create Resource Set </asp:label><asp:hyperlink id="lnkMain" style="Z-INDEX: 102; LEFT: 572px; POSITION: absolute; TOP: 70px" runat="server"
				Height="26px" Width="106px" NavigateUrl="Main.aspx">Return to Main</asp:hyperlink><asp:button id="btnRegenerateGroups" style="Z-INDEX: 104; LEFT: 218px; POSITION: absolute; TOP: 152px"
				runat="server" Width="83px" Text="Generate"></asp:button><asp:datagrid id="dg" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 184px" runat="server"
				Height="85px" Width="653px" AutoGenerateColumns="False">
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
					<asp:BoundColumn DataField="GROUP_ID" ReadOnly="True" HeaderText="Group Id"></asp:BoundColumn>
					<asp:BoundColumn DataField="NAME" ReadOnly="True" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="DESCRIPTION" HeaderText="Description"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			<asp:Label id="Label1" style="Z-INDEX: 107; LEFT: 38px; POSITION: absolute; TOP: 109px" runat="server"
				Width="644px">Select a culture, then press 'Generate' to create a new resource set for that culture from the 'master' resource set.</asp:Label></form>
	</body>
</HTML>
