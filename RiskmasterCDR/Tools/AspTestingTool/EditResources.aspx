<%@ Page language="c#" Codebehind="EditResources.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.EditResources" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EditResources</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="AspTestingTool.js"></script>
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:image id="Image1" style="Z-INDEX: 103; LEFT: 13px; POSITION: absolute; TOP: 11px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Height="44px" Width="479px"></asp:image><asp:label id="Label1" style="Z-INDEX: 104; LEFT: 27px; POSITION: absolute; TOP: 115px" runat="server"
				Height="16px" Width="161px">1.  Select  culture</asp:label><asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 29px; POSITION: absolute; TOP: 66px" runat="server"
				Height="33px" Width="471px" Font-Size="X-Large">Edit And Compile Resources </asp:label><asp:hyperlink id="lnkMain" style="Z-INDEX: 102; LEFT: 508px; POSITION: absolute; TOP: 70px" runat="server"
				Height="26px" Width="106px" NavigateUrl="Main.aspx">Return to Main</asp:hyperlink><asp:dropdownlist id="selResources" style="Z-INDEX: 105; LEFT: 227px; POSITION: absolute; TOP: 145px"
				runat="server" Height="31px" Width="197px" AutoPostBack="True"></asp:dropdownlist><asp:label id="lblSelResource" style="Z-INDEX: 106; LEFT: 231px; POSITION: absolute; TOP: 117px"
				runat="server" Height="23px" Width="192px">2.  Select Resource Group</asp:label><asp:dropdownlist id="selCultures" style="Z-INDEX: 107; LEFT: 32px; POSITION: absolute; TOP: 143px"
				runat="server" Height="21px" Width="154px" AutoPostBack="True"></asp:dropdownlist><INPUT id="hidFormAction" style="Z-INDEX: 108; LEFT: 515px; WIDTH: 58px; POSITION: absolute; TOP: 10px; HEIGHT: 24px"
				type="hidden" size="4" runat="server">
			<asp:panel id="pnlText" style="Z-INDEX: 109; LEFT: 25px; POSITION: relative; TOP: 170px" runat="server"
				Height="142px" Width="761px" Visible="False">
				<asp:datagrid id="dgText" runat="server" Width="762px" Height="311px" Visible="False" AutoGenerateColumns="False">
					<Columns>
						<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
						<asp:BoundColumn Visible="False" DataField="RESOURCE_DICTIONARY_ID" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="NAME" ReadOnly="True" HeaderText="Item"></asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="TYPE" ReadOnly="True" HeaderText="Type"></asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="DEFAULT_VALUE" ReadOnly="True" HeaderText="Original Value"></asp:BoundColumn>
						<asp:BoundColumn DataField="VALUE" HeaderText="Value"></asp:BoundColumn>
					</Columns>
				</asp:datagrid>
			</asp:panel>
			<asp:Button id="btnCompile" style="Z-INDEX: 111; LEFT: 448px; POSITION: absolute; TOP: 145px"
				runat="server" Width="116px" Height="27px" Visible="False" Text="Compile New DLL"></asp:Button><asp:panel id="pnlImages" style="Z-INDEX: 110; LEFT: 25px; POSITION: relative; TOP: 170px"
				runat="server" Height="63px" Width="765px" Visible="False">
				<asp:datagrid id="dgImages" runat="server" Width="762px" Height="311px" Visible="False" AutoGenerateColumns="False">
					<Columns>
						<asp:TemplateColumn>
							<ItemTemplate>
								<input type="button" runat="server"  onclick="changeImage(this,'change')" title='<%# DataBinder.Eval(Container.DataItem, "Resource_Dictionary_Id") %>' value="Change" ID="Button1" NAME="Button1"/>
								<br />
								<input type="button" runat="server" onclick="changeImage(this,'default')" title='<%# DataBinder.Eval(Container.DataItem, "Resource_Dictionary_Id") %>' value="Restore Default" ID="Button2" NAME="Button2"/>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn Visible="False" DataField="RESOURCE_DICTIONARY_ID" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
						<asp:BoundColumn DataField="DESCRIPTION" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
						<asp:BoundColumn DataField="DEFAULT_VALUE" ReadOnly="True" HeaderText="Original Value"></asp:BoundColumn>
						<asp:BoundColumn DataField="VALUE" HeaderText="Value"></asp:BoundColumn>
						<asp:TemplateColumn HeaderText="Value">
							<ItemTemplate>
								<asp:Image Width="96" Height="72" ImageUrl='<%# "getimage.ashx?img=" + DataBinder.Eval(Container.DataItem, "VALUE") %>' Runat="server" ID="Image2" NAME="Image2"/>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid>
			</asp:panel></form>
	</body>
</HTML>
