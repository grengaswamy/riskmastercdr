using System;
using System.Web;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace GetImageCodeBehind
{
	///<summary>
	///Code-behind for *.ashx page.
	///Directive in *.ashx page that points to this code-behind page must contain "class=[namespace].[class]"
	///Example of directive: @ WebHandler Language="C#" class="GetImageCodeBehind.GetImage" 
	///</summary>
	public class GetImage : IHttpHandler
	{
		public GetImage()
		{
			//
			// no constructor needed
			//
		}
		/// <summary>
		/// This runs when the *.ashx page is called.
		/// Returns a thumbnail of the desired image file(s); send complete path to image file(s) in querystring.
		/// </summary>
		/// <example>src="GetImage.ashx?img=D:\Temp\img.jpg"></example>
		public void ProcessRequest (System.Web.HttpContext context)
		{
			string sFile;
			Trace.Write("QueryString="+context.Request.QueryString,"GetImage.ProcessRequest()");
			System.Collections.Specialized.NameValueCollection coll=context.Request.QueryString;	
			String[] collKeys = coll.AllKeys;
			Image image=null;
			Image thumbImage=null;
			Bitmap bitmap=null;

			for(int i=0;i<collKeys.Length;i++)
			{	
				//each key in querystring will indicate an image that should be returned
				sFile=(string)coll.Get(collKeys[i]);	
				Trace.Write("Image file="+sFile,"GetImage.ProcessRequest()");
				if(File.Exists(sFile.ToString()))
				{
					try
					{
						image=Image.FromFile(sFile);
						thumbImage = image.GetThumbnailImage(96,72,null,IntPtr.Zero);
						bitmap = new Bitmap(thumbImage, 96,72);
						ImageFormat imgFormat;
						switch(sFile.Substring(sFile.LastIndexOf(".")+1))
						{
							case "jpg":
							case "jpeg":
							case "jpe":
								imgFormat=ImageFormat.Jpeg;
								break;
							case "gif":
								imgFormat=ImageFormat.Gif;
								break;
							case "bmp":
								imgFormat=ImageFormat.Bmp;
								break;
							case "ico":
								imgFormat=ImageFormat.Icon;
								break;
							default:
								imgFormat=ImageFormat.Jpeg;
								break;
						}
						bitmap.Save(context.Response.OutputStream,imgFormat);
					}
					catch(Exception ex)
					{
						throw new Exception("Error creating thumbnail image",ex);
					}
					finally
					{
						//note: if fail to call Dispose() will get error "another process is using" if try over-write the image file
						image.Dispose();
						thumbImage.Dispose();
						bitmap.Dispose();
					}
				}
			}		
		}

		public bool IsReusable
		{
			//true means can be pooled by Asp.Net runtime
			get { return true; }
		}
	}

}
