using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.RiskmasterSettings
{
    /// <summary>
    /// Provides access to the Riskmaster Security DSN Connection String
    /// information which is stored in the Riskmaster.config file
    /// </summary>
    public class RMSecurityDSN: IDataSourceConnection
    {
        #region Private Class Member variables
        private bool m_blnIsODBCDSN = false;
        private string m_strUID;
        private string m_strDSN;
        private string m_strConnString;
        private string m_strDriver;
        private string m_strSERVER;
        private string m_strPWD;
        private string m_strDATABASE;
        private RMConfigurator m_objRMConfigurator;
        #endregion

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private RMSecurityDSN()
        {
        } // constructor

        /// <summary>
        /// OVERLOADED: Class constructor
        /// to initialize the Riskmaster.config file path
        /// with the specified file path
        /// </summary>
        /// <param name="strRiskmasterConfigPath">string containing the 
        /// absolute file path to the Riskmaster.config file</param>
        public RMSecurityDSN(string strRiskmasterConfigPath)
        {
            m_objRMConfigurator = new RMConfigurator(strRiskmasterConfigPath);

            //Initialize all class member variables
            InitializeValues();
        } // constructor

        /// <summary>
        /// Initializes all class member variables
        /// </summary>
        private void InitializeValues()
        {
            //DTG Security32 is always a System DSN
            m_blnIsODBCDSN = true;
            m_strUID = string.Empty;
            m_strDSN = string.Empty;
            m_strConnString = string.Empty;
            m_strDriver = string.Empty;
            m_strSERVER = string.Empty;
            m_strPWD = string.Empty;
            m_strDATABASE = string.Empty;
        } // method: InitializeValues



        #region IDataSourceConnection Members
            #region ConnectionString Properties

            /// <summary>
            /// Gets and sets the entirety of a
            /// database connection string
            /// </summary>
            public string ConnectionString
            {
                get { return m_strConnString; }
                set { m_strConnString = value; }
            }//property ConnectionString

            /// <summary>
            /// Gets and sets the Driver element of a 
            /// database connection string
            /// </summary>
            public string Driver
            {
                get { return m_strDriver; }
                set { m_strDriver = value; }
            } // property Driver

            /// <summary>
            /// Gets and sets whether or not the 
            /// specified connection string is an
            /// ODBC DSN Connection string
            /// </summary>
            public bool IsODBCDSN
            {
                get { return m_blnIsODBCDSN; }
                set { m_blnIsODBCDSN = value; }
            }

            /// <summary>
            /// Gets and sets the DSN element
            /// of a database connection string
            /// </summary>
            public string DSN
            {
                get { return m_strDSN; }
                set { m_strDSN = value; }
            } // property DSN

            /// <summary>
            /// Gets and sets the Server element
            /// of a database connection string
            /// </summary>
            public string SERVER
            {
                get { return m_strSERVER; }
                set { m_strSERVER = value; }
            } // property SERVER

            /// <summary>
            /// Gets and sets the Database element
            /// of a database connection string
            /// </summary>
            public string DATABASE
            {
                get { return m_strDATABASE; }
                set { m_strDATABASE = value; }
            } // property DATABASE

            /// <summary>
            /// Gets and sets the UID element (User ID)
            /// of a database connection string
            /// </summary>
            public string UID
            {
                get { return m_strUID; }
                set { m_strUID = value; }
            } // property UID

            /// <summary>
            /// Gets and sets the PWD (password) element
            /// of a database connection string
            /// </summary>
            public string PWD
            {
                get { return m_strPWD; }
                set { m_strPWD = value; }
            } // property PWD 
            #endregion 
        #endregion

        /// <summary>
        /// Retrieves the Riskmaster.config Security Settings
        /// and populates the individual connection string elements
        /// for the class
        /// </summary>
        public void GetRMSecuritySettings()
        {
            try
            {
                //If the Security Settings have been enabled within the Riskmaster.config file
                if (m_objRMConfigurator.IsSecuritySettingsEnabled)
                {
                    NameValueCollection nvSecSettings = new NameValueCollection();

                    //Retrieve the NameValue Collection
                    nvSecSettings = m_objRMConfigurator.GetSecuritySettings();

                    //If the security credentials are available in an encrypted form
                    if (nvSecSettings[SecurityDSNElements.ENCDSN] != null)
                    {
                        //Populate each of the individual elements in the Security Settings
                        //Decrypt each of the individual elements that are available in an encrypted form
                        this.DSN = DecryptCredentials(nvSecSettings[SecurityDSNElements.ENCDSN]);
                        this.DATABASE = DecryptCredentials(nvSecSettings[SecurityDSNElements.ENCDATABASE]);
                        this.UID = DecryptCredentials(nvSecSettings[SecurityDSNElements.ENCUID]);
                        this.PWD = DecryptCredentials(nvSecSettings[SecurityDSNElements.ENCPWD]);
                    } // if
                    //If the security credentials are available in clear text
                    else if (nvSecSettings[SecurityDSNElements.DSN] != null)
                    {
                        //Populate each of the individual elements in the Security Settings
                        this.DSN = nvSecSettings[SecurityDSNElements.DSN];
                        this.DATABASE = nvSecSettings[SecurityDSNElements.DATABASE];
                        this.UID = nvSecSettings[SecurityDSNElements.UID];
                        this.PWD = nvSecSettings[SecurityDSNElements.PWD];
                    } // else if



                    

                    //Clean up
                    nvSecSettings = null;
                } // if
            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message);
            }
        } // method: GetRMSecuritySettings

        
        /// <summary>
        /// Decrypts the specified encrypted credentials and
        /// returns the decrypted string value
        /// </summary>
        /// <returns>string containing the decrypted credentials</returns>
        private string DecryptCredentials(string strEncryptedCredential)
        {
            string strDecryptedCredential = string.Empty;

            //Decrypt the specified encrypted credential
            //and assign it to the variable
            strDecryptedCredential = RMCryptography.DecryptString(strEncryptedCredential);

            //return the decrypted string credentials
            return strDecryptedCredential;
        } // method: DecryptCredentials


    }

    /// <summary>
    /// Stores the names of the Xml Nodes
    /// that are provided in the Riskmaster.config file
    /// for storing Riskmaster Security DSN information
    /// </summary>
    struct SecurityDSNElements
    {
        public static string DSN = "DataSourceName";
        public static string DATABASE = "DbDatabase";
        public static string UID = "DbUserId";
        public static string PWD = "DbPassword";
        public static string ENCDSN = "EncDataSourceName";
        public static string ENCDATABASE = "EncDbDatabase";
        public static string ENCUID = "EncDbUserId";
        public static string ENCPWD = "EncDbPassword";
    } // struct

}
