using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.RiskmasterSettings
{
    internal class RegExManager
    {
        internal static bool IsMatch(string strSearchString, string strRegExpr)
        {
            //return a boolean indicating whether or not the specified
            //search string matches based on the Regular Expression pattern
            return Regex.IsMatch(strSearchString, strRegExpr);
        }//method: IsMatch()

        internal static string[] MatchGroups(string strSearchString, string strRegExpr)
        {
            //Get the Match for the string
            Match regexMatch = Regex.Match(strSearchString, strRegExpr, RegexOptions.IgnoreCase);


            //Get the group matches
            GroupCollection regexGroups = regexMatch.Groups;

            //Declare a string Array
            object[] arrMatchGroups = new object[regexGroups.Count];

            //Copy all of the elements of the Match Groups to the specified Array
            regexGroups.CopyTo(arrMatchGroups, 0);            

            //Clean up
            regexGroups = null;
            regexMatch = null;

            List<string> arrGroupNames = new List<string>();

            foreach (object objGroupName in arrMatchGroups)
            {
                arrGroupNames.Add(objGroupName.ToString());
            }//foreach

            return arrGroupNames.ToArray();
        }//method: MatchGroups()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strSearchString"></param>
        /// <param name="strRegExpr"></param>
        /// <param name="strGroupName"></param>
        /// <returns></returns>
        internal static string MatchGroups(string strSearchString, string strRegExpr, string strGroupName)
        {
            string strMatchValue = string.Empty;

            //Get the Match for the string
            Match regExMatch = Regex.Match(strSearchString, strRegExpr, RegexOptions.IgnoreCase);

            //Get the matching value for the specified group name
            strMatchValue = regExMatch.Groups[strGroupName].Value;

            return strMatchValue;
        } // method: MatchGroups

    }
}
