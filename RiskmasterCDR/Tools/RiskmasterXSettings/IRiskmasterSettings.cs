using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public interface IRiskmasterSettings
    {
        string SettingType
        {
            get;
            set;
        }

        string ReadSetting(System.Collections.Specialized.NameValueCollection nvCollSettings);

        void WriteSetting(System.Collections.Specialized.NameValueCollection nvCollSettings, string strSettingValue);
    }
}
