using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    interface ISecuritySettings
    {
        string SecurityUID
        {
            get;
            set;
        }

        string SecurityPwd
        {
            get;
            set;
        }
    }
}
