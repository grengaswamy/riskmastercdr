using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    interface ISMServerSettings
    {
        string SMServerUID
        {
            get;
            set;
        }

        string SMServerPwd
        {
            get;
            set;
        }
    }
}
