using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Riskmaster.Application.RiskmasterSettings
{
    public interface IRMConfigurator
    {
        /// <summary>
        /// Gets or sets the absolute file path to the Riskmaster.config file
        /// </summary>
        string RiskmasterConfigPath
        {
            get;
            set;
        }

        /// <summary>
        /// Retrieves the value of a specified Xml Node from Riskmaster.config
        /// by providing an XPath Query to the node
        /// </summary>
        /// <remarks>By default all nodes only have a single instance, therefore, the // to select a single node is not required in the beginning of the XPath Query</remarks>
        /// <returns>string indicating the value of the specified Xml Node</returns>
        string Value(string strXmlNode);

        /// <summary>
        /// Sets the value of a specified Xml Node from Riskmaster.config by providing an XPath Query to the node and the appropriate value for the node
        /// </summary>
        void Value(string strXmlNode, string strXmlNodeValue);

        /// <summary>
        /// Retrieves the value of a specified Xml Attribute from Riskmaster.config by providing an XPath Query to the node and the name of the attribute
        /// </summary>
        string AttributeValue(string strXmlNode, string strXmlAttributeName);

        /// <summary>
        /// Sets the value of a specified Xml Attribute from Riskmaster.config by providing an XPath Query to the node, the name of the attribute and the value for the attribute
        /// </summary>
        void AttributeValue(string strXmlNode, string strXmlAttributeName, string strXmlAttributeValue);

        /// <summary>
        /// Retrieves an instance of the XmlNode given the XPath query to the specified Xml Node Name
        /// </summary>
        /// <returns>XmlNode</returns>
        XmlNode NamedNode(string strXmlNode);
    }
}
