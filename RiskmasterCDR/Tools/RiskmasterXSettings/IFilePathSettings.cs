using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public interface IFilePathSettings
    {
        string GlobalASAPath
        {
            get;
            set;
        }

        string RiskmasterConfigPath
        {
            get;
            set;
        }
    }
}
