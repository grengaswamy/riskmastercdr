using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class RiskmasterApplicationSettings
    {
    }

    public class RMConfigurationSettings
    {
        const string RISKMASTER_CONFIG_APP_SETTING = "RiskmasterConfigPath";
        const string SORTMASTER_DIRECTORY = "WIZARDSM";
        const string GLOBAL_ASA_FILE = "Global.asa";
        public string m_strRMConfigPath = string.Empty;
        public string m_strGlobalASAPath = string.Empty;
        
        /// <summary>
        /// Class constructor to initialize all values
        /// </summary>
        public RMConfigurationSettings()
        {

            m_strRMConfigPath = ConfigurationManager.AppSettings[RISKMASTER_CONFIG_APP_SETTING];
            FileInfo objFileInfo = new FileInfo(m_strRMConfigPath);
            DirectoryInfo objDirInfo = objFileInfo.Directory;
            DirectoryInfo objParentDirInfo = objDirInfo.Parent;
            string strParentDir = objParentDirInfo.FullName;

            m_strGlobalASAPath = strParentDir + "\\" + SORTMASTER_DIRECTORY + "\\" + GLOBAL_ASA_FILE;
        }//class constructor

        public string GlobalASAPath
        {
            get { return m_strGlobalASAPath; }
            set { m_strGlobalASAPath = value; }
        }

        public string RMConfigurationPath
        {
            get { return m_strRMConfigPath; }
            set { m_strRMConfigPath = value; }
        }
        
    }//class

    struct RiskmasterRegistryKeys
    {
        public static string SMServerKey = @"Software\DTG\SORTMASTER Server";
        public static string SMServerDSNValue = "DSN";
        public static string DTGSecurity32UserKey = @"Software\DTG\RISKMASTER\Security\EncDBUserID";
        public static string DTGSecurity32PasswordKey = @"Software\DTG\RISKMASTER\Security\EncDBPassword";
    }//struct

    struct GlobalASASppSettings
    {
        public static string DTGSecurityAppSetting = "Application(APP_SECURITYDSN)";
	    public static string WebFarmSessionAppSetting = "Application(APP_SESSIONDSN)";
	    public static string SMServerAppSetting = "Application(APP_SMDSN)";
        public static string SMTPServerAppSetting = "Application(APP_SMTPSERVER)";
    }//struct

    struct RiskmasterConfigSettings
    {
        public static string RMSecurity = "SecuritySettings";
        public static string BIS = "BIS"; //structure is <BIS><BISURL>
        public static string SMTPServer = "SMTPServer";
        public static string AppServer = "AppServer";
        public static string SMNetServer = "SMNetServer";
        public static string RMSession = "SessionDataSource";
        public static string PasswordPolicy = "PasswordPolicy";
        public static string Pabblo = "Pabblo";
        public static string IPAddress = "IP";  //Format is <TrustedIPPool><IP>
    }//struct

    public struct RMRegularExpressions
    {
        public static string strURL = @"^http://.+$";
        /// <summary>
        /// Allow any characters to precede the Application Name
        /// </summary>
        /// <remarks>
        /// The most common character to precede the Application Name will be 
        /// a tab character i.e. \t
        /// </remarks>
        public static string strApplication = @"^.+Application\((.+\))\s+=\s+.+$";
        public static string strApplicationConnString = @"^.+Application\((?<APPLICATION_NAME>.+)\)\s+=\s+(?<CONNECTION_STRING>.+)$";
        public static string strApplicationDSN = @"^.+Application\((?<APPLICATION_NAME>.+)\)\s+=\s+""DSN=(?<DSN_VALUE>.+);UID=(?<UID_VALUE>.+);PWD=(?<PWD_VALUE>.+);""$";
    }//struct RMRegularExpressions
}
