using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    interface ISMTPServerSettings
    {
        string SMTPServer
        {
            get;
            set;
        }
    }
}
