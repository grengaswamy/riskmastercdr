using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class RiskmasterConfigConfiguration : RiskmasterSettings, IRiskmasterSettings
    {

        private string m_strRiskmasterConfigPath;

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private RiskmasterConfigConfiguration()
        {
        }//class constructor

        /// <summary>
        /// OVERLOADED: Class constructor
        /// Assigns the Riskmaster.config path based on the specified path
        /// </summary>
        public RiskmasterConfigConfiguration(string strRMConfigPath)
        {
            m_strRiskmasterConfigPath = strRMConfigPath;
        } // constructor


        /// <summary>
        /// Gets or sets the absolute file path
        /// to the Riskmaster.config file
        /// </summary>
        public string RiskmasterConfigPath
        {
            get { return m_strRiskmasterConfigPath; }
            set { m_strRiskmasterConfigPath = value; }
        }
	
	
        #region IRiskmasterSettings Members

        public string SettingType
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Reads the specified setting from the Riskmaster.config file
        /// and returns the value
        /// </summary>
        /// <param name="nvCollSettings">NameValueCollection specifying the XPath query
        /// to the specified XmlNode containing the required Riskmaster.config setting</param>
        /// <returns>string containing the value for the specified setting</returns>
        public string ReadSetting(System.Collections.Specialized.NameValueCollection nvCollSettings)
        {
            string strRMConfigValue = string.Empty;

            //Retrieve the Generic String Collection
            List<string> arrRMConfigSettings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            string strRMConfigSetting = GetListString(arrRMConfigSettings);

            try
            {
                RMConfigurator objRMConfig = new RMConfigurator(m_strRiskmasterConfigPath);

                strRMConfigValue = objRMConfig.Value(strRMConfigSetting);

                //Clean up
                objRMConfig = null;
                
            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message, ex.InnerException);
            }

            return strRMConfigValue;
        }

        /// <summary>
        /// Reads and replaces the specified setting within Riskmaster.config 
        /// </summary>
        /// <param name="nvCollSettings">NameValueCollection specifying the XPath query
        /// to the specified XmlNode containing the required Riskmaster.config setting</param>
        /// <param name="strSettingValue">string containing the new setting value</param>
        public void WriteSetting(System.Collections.Specialized.NameValueCollection nvCollSettings, string strSettingValue)
        {
            string strRMConfigValue = string.Empty;

            //Retrieve the Generic String Collection
            List<string> arrRMConfigSettings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            string strRMConfigSetting = GetListString(arrRMConfigSettings);

            try
            {
                RMConfigurator objRMConfig = new RMConfigurator(m_strRiskmasterConfigPath);

                //Set the value for the specific node
                objRMConfig.Value(strRMConfigSetting, strSettingValue);

                //Clean up
                objRMConfig = null;

            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message, ex.InnerException);
            }
        }//method: WriteSetting()

        public void WriteSetting(System.Collections.Specialized.NameValueCollection nvCollSettings, System.Data.DataSet dstSettingValue)
        {
            string strRMConfigValue = string.Empty;

            //Retrieve the Generic String Collection
            List<string> arrRMConfigSettings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            string strRMConfigSetting = GetListString(arrRMConfigSettings);

            try
            {
                RMConfigurator objRMConfig = new RMConfigurator(m_strRiskmasterConfigPath);

                //Set the value for the specific node
                objRMConfig.Value(strRMConfigSetting, dstSettingValue);

                //Clean up
                objRMConfig = null;

            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message, ex.InnerException);
            }
        }//method: WriteSetting

        #endregion
    }
}
