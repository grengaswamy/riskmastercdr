using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    /// <summary>
    /// Provides a set of commonly needed regular expressions
    /// that can be used to parse various Riskmaster Setings
    /// </summary>
    /// <remarks>
    /// Currenly provides access to the following Regular Expression Patterns
    /// 1.  URLs
    /// 2.  Application Settings
    /// 3.  Database Connection Strings
    /// </remarks>
    public struct RMRegularExpressions
    {
        public static string strURL = @"^http://.+$";
        /// <summary>
        /// Allow any characters to precede the Application Name
        /// </summary>
        /// <remarks>
        /// The most common character to precede the Application Name will be 
        /// a tab character i.e. \t
        /// </remarks>
        public static string strApplication = @"Application\((.+\))\s+=\s+.+";
        /// <summary>
        /// Allow any characters to precede the Application Name
        /// and capture the relevant database connection string information
        /// </summary>
        public static string strApplicationConnString = @"Application\(.+\)\s=\s+""(?<CONNECTION_STRING>.*)""";

        /// <summary>
        /// Provides a means of parsing out a SQL Server connection string into its respective elements
        /// </summary>
        /// <remarks>This connection string is ODBC-compliant ONLY</remarks>
        public static string strSQLServerConnString = @"Driver=(?<DRIVER>.*);Server=(?<SERVER>.*);Database=(?<DATABASE>.*);UID=(?<UID>.*);PWD=(?<PWD>.*);";

        /// <summary>
        /// Provides a means of parsing out an Oracle connection string into its respective elements
        /// </summary>
        /// <remarks>This connection string is ODBC-compliant ONLY</remarks>
        public static string strOracleConnString = @"Driver=(?<DRIVER>.*);Server=(?<SERVER>.*);DBQ=(?<DATABASE>.*);UID=(?<UID>.*);PWD=(?<PWD>.*);";

        /// <summary>
        /// Allow any characters to precede the Application Name
        /// and capture the relevant database connection string information
        /// for relevant DSNs
        /// </summary>
        public static string strApplicationDSN = @"^.+Application\((?<APPLICATION_NAME>.+)\)\s+=\s+""DSN=(?<DSN_VALUE>.+);UID=(?<UID_VALUE>.+);PWD=(?<PWD_VALUE>.*);""$";

        
    }//struct RMRegularExpressions
}
