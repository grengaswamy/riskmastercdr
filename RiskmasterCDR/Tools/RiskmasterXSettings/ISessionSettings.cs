using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    interface ISessionSettings
    {
        string SessionUID
        {
            get;
            set;
        }

        string SessionPwd
        {
            get;
            set;
        }
    }
}
