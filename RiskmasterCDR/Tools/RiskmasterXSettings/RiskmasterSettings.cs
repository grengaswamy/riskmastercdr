using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class RiskmasterSettings
    {
        protected System.Collections.Generic.List<string> GetNVCollectionValues(System.Collections.Specialized.NameValueCollection nvColl)
        {
            List<string> arrStrings = new List<string>();

            //Retrieve the enumerator for the NameValueCollection
            IEnumerator collEnumerator = nvColl.GetEnumerator();

            //Loop through the NameValueCollection and retrieve the strings
            foreach (string strNVCollKey in nvColl.AllKeys)
            {
                arrStrings.Add(nvColl[strNVCollKey]);
            }//foreach

            return arrStrings;
        }

        protected virtual string GetListString(System.Collections.Generic.List<string> arrStrings)
        {
            string strValue = string.Empty;

            if (arrStrings.Count == 1)
            {
                //Only retrieve the first value in the ArrayList
                strValue = arrStrings[0];
            }//if

            return strValue;
        }

        protected virtual void GetListString(System.Collections.Generic.List<string> arrStrings, ref string strValue1, ref string strValue2)
        {
            //if there are only 2 elements in the Array
            if (arrStrings.Count == 2)
            {
                //Populate the string references with the values from the String Array
                strValue1 = arrStrings[0];
                strValue2 = arrStrings[1];
            }//if
        }
    }
}
