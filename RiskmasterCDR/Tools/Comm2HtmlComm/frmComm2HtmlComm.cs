using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Common.Win32;
using Riskmaster.Db;
using Riskmaster.Common;
using Oracle.DataAccess;

namespace Riskmaster.Tools.Comm2HtmlComm
{
    /**************************************************************
	 * $File		: frmComm2HtmlComm.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/18/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: 
	**************************************************************/
    /*
    *	The tool can either be run directly or can be executed via a bacth file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.Comm2HtmlComm.exe uid pwd DSN
    *		
    */
    public partial class frmComm2HtmlComm : Form
    {
        #region Declarations

        private Login m_objLogin = new Login();        
        private Riskmaster.Db.DbConnection m_objDbConnection ;

        private static long m_CommentsCount = 0;
        private static long m_ProcessedCommentsCount = 0;
        //private static long m_EnhcNotesCount = 0;
        //private static long m_EnhcNotesCountInDataset = 0;


        //private static Hashtable m_UidCache = new Hashtable(150);
        //private static Hashtable m_GroupidCache = new Hashtable(150);
        private static string[] m_LOBinfo = new string[20]; //assuming max 10 LOB division 
        private static int m_LOBCount ;    
        public static  StreamWriter m_sw1;
        //public static m_sHTMLTempText ="";
        public static StringBuilder m_sbHtmlComment =  new StringBuilder();

        //private int m_iNoteId = -1;

        // saving the "Now" time which will be appear as the created datetime & activity datetime in ehnc notes
        private string m_nowDateTime = Conversion.ToDbDate(System.DateTime.Now);
        private string m_nowTime = Conversion.ToDbTime(System.DateTime.Now);

        private static DbWriter m_EventHtmlCommentsdbWriter;
        private static DbWriter m_ClaimHtmlCommentsdbWriter;

        private int m_strDisplay = 0; 
            //1 = event is displayed
            //2 = claim is displayed
        

        // regular expression pattern to extract comments
        // it has been modified to accept dates in both mm/dd/yy or dd/mm/yy format ..... 
        //private string m_pattern = @"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?";
        
        //using the compiled regex now.....
        Regex RexDtStamp = new Regex(@"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        //Regex RexLineBreaks = new Regex(@"<br/>", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        char[] m_Breaks = new char[] { '\t', '\n', ' ' };

        // below is the complete pattern which will match for 
        // with some relaxation 
        // in dates(mm/dd/yy); in place of slash, the user can have : or - or . 
        // in time ; user can skip the seconds part, can have it with AM/Pm text or whcihout it.
        // however it months value will have to be less than 12 
        // dates value will have to be les than 31 etc
        //private string m_pattern = @"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\(([a-zA-Z0-9]{1,12})\))?";

        public frmComm2HtmlComm()
        {
            InitializeComponent();
        }

        #endregion

        #region debugging only Code

        //Loading comments attached to claims
        private void btnLoadCommentsClaim_Click(object sender, EventArgs e)
        {
            string sqlClaims = "SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL ";
            writeLog("Loading comments attached to claims.");
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlClaims);
                AppGlobals.m_dsComments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsComments);
                //dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
                //dataGridView1.Refresh();
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("comments attached to claims Load operation completed.");

            m_strDisplay = 2;
            //btnExtractNotes.Enabled = true;
            txtNoClaimsComments.Text = AppGlobals.m_dsComments.Tables[0].Rows.Count.ToString();
        }

        //Loading comments attached to events
        private void btnLoadCommentsEvent_Click(object sender, EventArgs e)
        {
            string sqlEvents = "SELECT  EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL ";
            writeLog("Loading comments attached to events");
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlEvents);
                AppGlobals.m_dsComments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsComments);
                //dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
                //dataGridView1.Refresh();
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("comments attached to events Load operation completed.");
            m_strDisplay = 1;
//            btnExtractNotes.Enabled = true;
            txtNoEventComments.Text = AppGlobals.m_dsComments.Tables[0].Rows.Count.ToString();

        }

        private void btnBindCommentGrid_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
            dataGridView1.Refresh();
        }

        private void btnBindEnhcNotesGrid_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = AppGlobals.m_dsComments.Tables[0];
            dataGridView2.Refresh();
        }

        //reading note_types  
        private void button4_Click(object sender, EventArgs e)
        {                           
            string sqlNotes = "SELECT C.CODE_ID,CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID=CT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE') ORDER BY 2";
            DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlNotes);            
            DataSet ds2 = new DataSet();            
            ObjDbDataAdapter.Fill(ds2);            
            grdNoteType.DataSource = ds2.Tables[0];
            grdNoteType.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowLoc=Convert.ToInt32(e.RowIndex.ToString());
            int ColumnLoc = Convert.ToInt32(e.ColumnIndex.ToString());
            string celltext = dataGridView1.Rows[rowLoc].Cells[ColumnLoc].Value.ToString();
            MessageBox.Show(celltext,"cell text");
        }

        private void dataGridView2_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int rowLoc = Convert.ToInt32(e.RowIndex.ToString());
            int ColumnLoc = Convert.ToInt32(e.ColumnIndex.ToString());
            string celltext = dataGridView2.Rows[rowLoc].Cells[ColumnLoc].Value.ToString();
            MessageBox.Show(celltext, "cell text");
        }

        // delete new saved enhanced c2n 
        private void btnDeleteC2NenchNotes_Click(object sender, EventArgs e)
        {
            //string sqlDelNotes = string.Format("DELETE FROM CLAIM_PRG_NOTE WHERE NOTE_TYPE_CODE IN (SELECT CODE_ID FROM CODES WHERE UPPER(SHORT_CODE) = '{0}')", m_sEnhcNotesShortCode);
            //DbCommand objDbCommand;
            ////MessageBox.Show(m_objDbConnection.State.ToString());
            //m_objDbConnection.Open();
            //objDbCommand = m_objDbConnection.CreateCommand();            
            //objDbCommand.CommandText = sqlDelNotes;
            //try
            //{
            //    objDbCommand.ExecuteNonQuery();
            //}
            //catch (Exception p_oException)
            //{
            //    writeLog(string.Format("ERROR:[C2N notes deletion error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
            //    throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            //}
            //finally
            //{
            //    m_objDbConnection.Close();
            //}
        }

        // this button wil noe NOT be used
        // Extracting Notes from comments/event dataset
        //updates the RTF box wih the provided text

        #endregion

        //Form Load 
        private void frmComm2HtmlComm_Load(object sender, EventArgs e)
        {
            if (AppGlobals.bSilentMode)
            {
                this.Hide();                
            }
            try
            {
                m_sw1 = new StreamWriter("Comm2HtmlComm.log", true);
                //if (m_sEnhcNotesShortCode == "" || m_sEnhcNotesCodeDesc == "" || m_sEnhcNotesShortCode == null || m_sEnhcNotesCodeDesc == null)
                //{
                //    writeLog("ShortCode for new Enhc notes missing in config file");
                //    MessageBox.Show("ShortCode for new Enhc notes missing in config file");
                //    Application.Exit();
                //}
            }
            catch (Exception exp)
            {
                //writeLog(string.Format("ERROR:[Log file Open error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            if (AppGlobals.bSilentMode)
            {             
                writeLog("Running in silent mode; DSN: " + AppGlobals.sDSN);
            }
            writeLog("-----------------------------");
            writeLog("Form Loaded... authenticating");
            writeLog("-----------------------------");
           
            Boolean bCon = false;
            try
            {
                if (!AppGlobals.bSilentMode)
                {
                    bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref AppGlobals.Userlogin);    
                }                
                if (!AppGlobals.bSilentMode && !bCon)
                { 
                    // for invalid Login
                    if (m_objLogin!=null) m_objLogin.Dispose();
                    writeLog("Invalid Logon. terminating");
                    Application.Exit();
                }
                else
                {
                    writeLog("Authentication successful");
                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                    m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);

                    // enable the follwoing line when running in debug mode.
                    // this will enable the developer to view the extracted notes before they can be saved to db
                    //this.Size = new Size(870, 621);
                    this.FormBorderStyle = FormBorderStyle.Fixed3D;

                    //setting status messages                    
                    //txtNewEnhcCode.Text = m_sEnhcNotesShortCode;
                    //txtnewEnhcCodeDesc.Text = m_sEnhcNotesCodeDesc;
                    
                    setStatus("Click on the 'Start' button to start migrating Comments to HTML Comments ...");
                    
                    loadLOBinfo(); 
                    // if application is called thru the command line then directly proceed to saving the Ench Notes. 
                    if (AppGlobals.bSilentMode)
                    {
                        btnStart_Click(null, null);                                                
                    }
                    
                }
                //MessageBox.Show(getNoteTypeCodeId().ToString()   );
                if (AppGlobals.bSilentMode)
                {
                    this.Hide();
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                if (!AppGlobals.bSilentMode)
                {
                    writeLog(string.Format("ERROR:[form Load error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", ex.Message.ToString(), ex.InnerException.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }
                       //MessageBox.Show(ex.Message, "C2n error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Application.Exit();
            }
        }

        // appends text to the RTF box
        private void setStatus(string p_status)
        {
            //lblStatus.Text=p_status;
            //rtfStatus.AppendText(p_status + "\n");
        }

        //updates the progress bar text box (its just above the progress bar)
        private void setProgressStatus(string p_status)
        {
            txtProgressStatusMessage.Text=p_status;            
        }

        //returns the Code Type of 'Comments2EnhNotes' if presents otherwise creates one
        //private int getNoteTypeCodeId()
        //{
        //    int iNodeTypeCodeId = -1, iCodeIdFromCodesTable=-1, iCodeIdFromCodesTextTable=-1, iNotesGlossaryTableId=-1;
        //    // checking to see if the 'C2N' code type already exits, in which case directly retunr its Code_id
        //    string sqlNotes = string.Format("SELECT C.CODE_ID FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID=CT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE') AND UPPER(CT.CODE_DESC) = '{0}' ", m_sEnhcNotesCodeDesc.ToUpper());
        //    DbReader objDbReader; 
        //    //objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
        //    //objDbReader.Read();
        //    //m_objDbConnection.ExecuteScalar
        //    DbCommand objDbCommand;
        //    m_objDbConnection.Open();
        //    objDbCommand = m_objDbConnection.CreateCommand();
        //    objDbCommand.CommandText = sqlNotes;
        //    object objobject = objDbCommand.ExecuteScalar();
                  
        //    //if (!objDbReader.IsDBNull(0))
        //    iNodeTypeCodeId = Convert.ToInt32(objobject);            

        //    // if the Code type already exits then returning the same.  
        //    if (!(iNodeTypeCodeId == -1 || iNodeTypeCodeId == 0)) return iNodeTypeCodeId;

        //    //getting Table id for enhanced note to be put in codes table
        //    sqlNotes = "SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE'";
            
        //    objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iNotesGlossaryTableId = objDbReader.GetInt32(0);

        //    // TO DO : encapsulate the following block within a transaction
            
        //    // getting new code_id from codes table
        //    sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES ";
            
        //    objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iCodeIdFromCodesTable = objDbReader.GetInt32(0);

        //    // getting new code_id from codes_text table
        //    sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES_TEXT ";

        //    objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iCodeIdFromCodesTextTable = objDbReader.GetInt32(0);

        //    // normally the two tables (Codes & Codes_text would be in sync)... just in case
        //    // taking the maximum of the two for our Comments2EnhNotes codes type
        //    iNodeTypeCodeId = (iCodeIdFromCodesTable > iCodeIdFromCodesTextTable) ? iCodeIdFromCodesTable : iCodeIdFromCodesTextTable;

        //    //saving our new Codes into the table
        //    DbWriter objWriter;
            
        //    // Saving into the codes table
        //    objWriter = DbFactory.GetDbWriter(m_objDbConnection);
        //    objWriter.Tables.Add("CODES");
        //    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
        //    objWriter.Fields.Add("TABLE_ID", iNotesGlossaryTableId.ToString());
        //    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
        //    objWriter.Fields.Add("DELETED_FLAG", "0");
        //    objWriter.Execute();

        //    // Saving into the codes_text table
        //    //sqlNotes = "insert into codes_text (code_id,short_code,code_desc) values (9999, 'C2N','Comm2EnhNotes')";
                    
        //    // Saving into the codes table
        //    objWriter = DbFactory.GetDbWriter(m_objDbConnection);
        //    objWriter.Tables.Add("CODES_TEXT");
        //    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
        //    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
        //    objWriter.Fields.Add("CODE_DESC", m_sEnhcNotesCodeDesc);
        //    objWriter.Execute();

        //    return iNodeTypeCodeId;
        //}

        //getting Uid for login id
        //private int getUid(string p_loginId)
        //{
        //    string sqlQuery,strResult=string.Empty;
        //    int iUid = -1;
        //    if (m_UidCache.ContainsKey(p_loginId))
        //        return  Convert.ToInt32(m_UidCache[p_loginId]);

        //    writeLog("Retriving uid for login "+ p_loginId);
        //    sqlQuery = string.Format("SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}' AND DSNID='{1}'", p_loginId, AppGlobals.Userlogin.DatabaseId);
        //    //sqlQuery = string.Format("SELECT * FROM USER_DETAILS_TABLE UDT LEFT JOIN DATA_SOURCE_TABLE DST ON UDT.DSNID=DST.DSNID WHERE UPPER(UDT.LOGIN_NAME)='{0}' AND UPPER(UDT.CONNECTION_STRING)='{1}''", p_loginId.ToUpper(), AppGlobals.ConnectionString.ToUpper());
        //    //SELECT * FROM USER_DETAILS_TABLE udt left Join DATA_SOURCE_TABLE dst on udt.dsnid=dst.dsnid WHERE upper(udt.LOGIN_NAME)='{0}' and upper(udt.connection_string)='{1}'
        //    strResult = GetSingleValue_Sql(sqlQuery, m_objLogin.SecurityDsn);
        //    if (strResult==string.Empty)
        //    {
        //        m_UidCache.Add(p_loginId, -1);
        //        return iUid;
        //    }
        //    iUid = Convert.ToInt32(strResult);
        //    m_UidCache.Add(p_loginId,iUid);
        
        //    return iUid;
        //}

        //getting GroupId (of the group from sms) for the uid 
        //private int getGroupId(string p_uId)
        //{
        //    string sqlQuery,strResult=string.Empty;
        //    int iGroupId=-1;
        //    if (m_GroupidCache.ContainsKey(p_uId))
        //        return Convert.ToInt32(m_GroupidCache[p_uId]);

        //    writeLog("Retriving groupid for uid " + p_uId);
        //    sqlQuery = string.Format("SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID={0}", p_uId);
        //    strResult = GetSingleValue_Sql(sqlQuery, AppGlobals.ConnectionString);
        //    if (strResult == string.Empty)
        //    {
        //        m_GroupidCache.Add(p_uId, -1);
        //        return iGroupId;
        //    }
        //    iGroupId = Convert.ToInt32(strResult);
        //    m_GroupidCache.Add(p_uId, iGroupId);

        //    return iGroupId;
        //}

        //closing all connections & disposing 
                
        private void frmComm2HtmlComm_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects" );
            if (m_objLogin!=null)   m_objLogin.Dispose();
            if (AppGlobals.m_dsComments!=null) AppGlobals.m_dsComments.Dispose();
            //if (AppGlobals.m_dsEnhNotes!=null) AppGlobals.m_dsEnhNotes.Dispose();
            if (m_objDbConnection!=null) m_objDbConnection.Dispose();
            if (m_sw1 != null)
            {                
                m_sw1.Close();
                m_sw1.Dispose();
            }

            
            
            
            //Todo: additiona dispose here for new collections
            m_sbHtmlComment.Length = 0;
            

            //if (m_EventHtmlCommentsdbWriter!=null)
            //{
            //    m_EventHtmlCommentsdbWriter.
            //}
            
        }

        public static string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);            
            try
            {                
                m_sw1.WriteLine(sMessage);                
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            
        }

        public void loadLOBinfo()
         {
             int i = 0;
             string sqlNotes = "SELECT C.CODE_ID,CT.CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID IN ( SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) LIKE '%LINE_OF_BUSINESS%' ) ";
             DbReader objDbReader;
             m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);
             objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
             while (objDbReader.Read())             
             if (!objDbReader.IsDBNull(0))
             {
                 m_LOBinfo[i] =   Convert.ToString(objDbReader.GetInt32(0));
                 m_LOBinfo[i+1] = objDbReader.GetString(1);
                 i += 2;
             }
             // m_LOBCount would have twice the number of LOB the actualy number of LOB's
             // their  Code & code_desc are stored in consecutive values.
             m_LOBCount = i;

         }

        private void btnStart_Click(object sender, EventArgs e)
        {            
            string sqlLoadComment = string.Empty;
            //DbReader objDbReader;            
            StringBuilder sbSqlLOBGroup = new StringBuilder();
            //m_EnhcNotesCountInDataset = 0;        

            btnStart.Enabled = false;

            //preparing the dbWriter 
            m_EventHtmlCommentsdbWriter = DbFactory.GetDbWriter(m_objDbConnection);
            m_EventHtmlCommentsdbWriter.Tables.Add("EVENT");
            m_EventHtmlCommentsdbWriter.Fields.Add("HTMLCOMMENTS", "");
            //m_EventHtmlCommentsdbWriter.Fields.Add("EVENT_ID",0 );            
            
            m_ClaimHtmlCommentsdbWriter = DbFactory.GetDbWriter(m_objDbConnection);
            m_ClaimHtmlCommentsdbWriter.Tables.Add("CLAIM");
            m_ClaimHtmlCommentsdbWriter.Fields.Add("HTMLCOMMENTS", "");
            //m_ClaimHtmlCommentsdbWriter.Fields.Add("CLAIM_ID", 0);
       
            //loading comments attached to events
            sqlLoadComment = "SELECT  EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL";
            txtProcessingNow.Text = "Events";            
            setProgressStatus(string.Format("Progress for phase 1 of {0}", m_LOBCount/2+1));            
            loadComments(sqlLoadComment, txtProcessingNow.Text);

            //button1_Click(null, null);
            // extract enhc notes here....
            m_strDisplay = 1;
            
            ExtractHtmlComments();
            m_strDisplay = 2;

            //#region debug                        
            //    sqlLoadComment = "SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE CLAIM_ID='536049'";
            //    txtProcessingNow.Text = "My_ claim";
            //    setProgressStatus(string.Format("Progress for phase 1 of {0}", m_LOBCount / 2 + 1));
            //    loadComments(sqlLoadComment, txtProcessingNow.Text);
            //    ExtractHtmlComments();
            //    m_strDisplay = 2;
            //#endregion



            int i =0;
            for (i = 0; i < m_LOBCount / 2; i++)
            {
                txtProcessingNow.Text = string.Format("Claims ({0})", m_LOBinfo[i * 2 + 1]);
                setProgressStatus(string.Format("Progress for phase {0} of {1}", Convert.ToString(i + 1), m_LOBCount / 2 + 1));
                sqlLoadComment = string.Format("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL  AND LINE_OF_BUS_CODE = {0}", m_LOBinfo[i * 2]);
                loadComments(sqlLoadComment, txtProcessingNow.Text);
                //m_strDisplay = 2;
                                
                ExtractHtmlComments();

                // adding up Lob Codes for 
                sbSqlLOBGroup.Append(m_LOBinfo[i * 2].ToString());
                sbSqlLOBGroup.Append(",");

            }

            // for claims which might now be associated with any LOB
            if (m_LOBCount > 1)
            {
                txtProcessingNow.Text = "Claims (unassociated)";
                setProgressStatus(string.Format("Progress for phase {0} of {0}", m_LOBCount / 2 + 1));
                sqlLoadComment = string.Format("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL  AND LINE_OF_BUS_CODE NOT IN({0})", sbSqlLOBGroup.ToString().Substring(0, sbSqlLOBGroup.ToString().Length - 1));
                loadComments(sqlLoadComment, txtProcessingNow.Text);
                ExtractHtmlComments();

                // extract enhc notes here....            
            }
            prgBrNotesSave.Value = 100;
            if (m_objDbConnection!=null)
            {
                m_objDbConnection.Close();
            }
            btnStart.Enabled = false;

            // Updating Glossary table
            //writeLog("Updating Glossary table with NextUniqueValue");
            //UpdateGlossary();

            setStatus("Comments to HTML comments migration completed.");
            //writeLog("Comments to Enhanced notes migration completed.");
            if (!AppGlobals.bSilentMode)
            {
                MessageBox.Show("Comments to HTML comments migration completed.");
            }
            

            // ("comments to Enhanced notes migration completed.");
        }

        private void loadComments(string p_sqlLoadComment, string p_ProcessingNow)
        {
            writeLog(string.Format("Loading comments for: {0}", p_ProcessingNow));
            setStatus(string.Format("Loading comments for: {0}", p_ProcessingNow));
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, p_sqlLoadComment);
                AppGlobals.m_dsComments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsComments);                
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Load Comments] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            //writeLog("Comments load operation completed.");            
            writeLog(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            setStatus(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            m_CommentsCount += AppGlobals.m_dsComments.Tables[0].Rows.Count;
            txtCommentCount.Text = m_CommentsCount.ToString();
            writeLog(string.Format("{0} comments loaded so far",txtCommentCount.Text ));            

            //btnExtractNotes.Enabled = true;
            //txtNoEventComments.Text = AppGlobals.m_dsComments.Tables[0].Rows.Count.ToString();
        }

        // Extracting Notes from comments/event dataset
        private void ExtractHtmlComments()
        {
            MatchCollection MxComment, MxHTMLComment;
            string CommentLine = string.Empty, HTMLCommentLine = string.Empty;
            string EventId = string.Empty, EventNumber = string.Empty;
            string ClaimId = string.Empty, ClaimNumber = string.Empty;
            string Comment = string.Empty, HTMLComment = string.Empty;
            string DateTimeStamp = string.Empty, strEnteredBy = string.Empty, strTime = string.Empty, sDateTimeTemp=string.Empty;
            string sHtmlDateTimeTemp = string.Empty, HtmlDateTimeStamp = string.Empty, strHtmlTime = string.Empty;
            DateTime dtDateTimeTemp, dtHtmlDateTimeTemp;
            bool bDateTimeParse = false;
            bool bIsCommentDTPresent = false;
            bool bHtmlDateTimeParse = false;            
            string strUid = string.Empty, strGroupID = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            string sCommentTemp=string.Empty;

            int matchCount = 0, index = 0, length = 0, HTMLindex = 0, HTMLlength = 0, HTMLmatchCount = 0;
            int i = 0, j = 0,k = 0 ;
                //, iUid=0, iGroupID=0;
            //int iYear=0, iMonth=0, iDay=0;
            int startLoc = 0, endLoc = 0, startHTMLLoc = 0, endHTMLLoc = 0;
            //MessageBox.Show("extracting notes");
            writeLog("Extracting HTMLComments from comments from Events");
           
            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            //todo
            
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                m_sbHtmlComment.Length = 0;

                CommentLine = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();

                //CommentLine = dataGridView1.Rows[i].Cells["COMMENTS"].Value.ToString();
                HTMLCommentLine = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();

                EventId = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_ID"].ToString();
                EventNumber = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_NUMBER"].ToString();
                if (m_strDisplay == 2)
                {
                    ClaimId = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_ID"].ToString();
                    ClaimNumber = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_NUMBER"].ToString();
                }
                else
                {
                    ClaimId = "";
                    ClaimNumber = "";
                }

                // Assuming that the "Htmlcomments" column contains the the same text 
                // as comments columns with just some extra HTML markup.  
                //MxComment = Regex.Matches(CommentLine, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                //MxHTMLComment = Regex.Matches(HTMLCommentLine, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                MxComment = RexDtStamp.Matches(CommentLine);//,  RegexOptions.IgnoreCase | RegexOptions.Compiled);
                MxHTMLComment = RexDtStamp.Matches(HTMLCommentLine);//, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                
                matchCount = MxComment.Count;
                HTMLmatchCount = MxHTMLComment.Count;

                // If in case a portion of text within the datetime stamp is some formatting attached
                // the same would otherwise fail the regex pattern matching. 
                // ignoring the markup in this case.

                try
                {
                    if (MxComment.Count > 0)
                    {                    
                        for (j = 0; j < MxComment.Count; j++)
                        {
                            #region iterating thru comments & inserting Br in between & coping to HTMLcomments...

                            sDateTimeTemp = string.Format(@"{0}/{1}/{2} {3} {4}", MxComment[j].Groups[1].ToString(), MxComment[j].Groups[2].ToString(), MxComment[j].Groups[3].ToString(), MxComment[j].Groups[5].ToString(), MxComment[j].Groups[6].ToString());

                            index = MxComment[j].Index;
                            length = MxComment[j].Length;

                            //this extracted Uid cotains the enclosing backets as well so removing the backets.
                            strEnteredBy = MxComment[j].Groups[7].ToString();
                            if (strEnteredBy.Length > 2)
                                strEnteredBy = strEnteredBy.Substring(1, strEnteredBy.Length - 2);

                            if (j == 0 & index > 3)
                            {
                                // for the scenario where there is some text added before the first date-time stamp                                    
                                Comment = CommentLine.Substring(0, index).Trim();
                                //sCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                                if (Comment.Length > 0)
                                    AppendHtmlComment("", "", Comment);
                            }
                            startLoc = index + length;

                            if (j != matchCount - 1)
                            {
                                endLoc = MxComment[j + 1].Index - startLoc;
                            }
                            else
                            {
                                endLoc = CommentLine.Length - startLoc;
                            }

                            Comment = CommentLine.Substring(startLoc, endLoc).Trim();

                            //sCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                            if (Comment.Length > 0)
                                AppendHtmlComment(sDateTimeTemp, strEnteredBy, Comment);

                            #endregion
                        }

                        if (m_strDisplay==1)
                        {
                            UpdateHtmlCell(EventId);
                        }
                        else
                        {
                            UpdateHtmlCell(ClaimId);
                        }
                        
                    }                    
                    else
                    {
                        // scenario where dateTimeStamps are disabled.                       
                        Comment = CommentLine;
                        if (sCommentTemp.Length > 0)
                        {
                            AppendHtmlComment("", "", Comment);

                            if (m_strDisplay == 1)
                            {
                                UpdateHtmlCell(EventId);
                            }
                            else
                            {
                                UpdateHtmlCell(ClaimId);
                            }
                        
                        }
                    }

                    //if (i % 50 == 0)
                    //{
                    //    //writing to log progress afetr every 50 comments have been processed.                    
                    //    setStatus("Saving HtmlComments....");
                    //}
                    if (i % 10 == 0)
                    {
                        //writing the notes count to log file
                        if (m_strDisplay == 1)
                        {
                            writeLog(string.Format("{0} comments extracted so far. Current Event no:{1}", txtProcessedCommentsCount.Text, EventId));
                        }
                        else
                        {
                            writeLog(string.Format("{0} comments extracted so far. Current Event no:{1}", txtProcessedCommentsCount.Text, ClaimId));                            
                        }                        
                    }
                    // saving the extracted enhc Notes here.                
                    //Thread.Sleep(100);
                    Application.DoEvents();
                }
                catch (Exception exp)
                {
                    writeLog(string.Format("ERROR:[Error extracting HtmlComment ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                    throw new Exception(exp.Message.ToString(), exp.InnerException);

                }
                //saveNotes();
                //saveHtmlComments();

                prgBrNotesSave.Value = (i+1)*100/AppGlobals.m_dsComments.Tables[0].Rows.Count;
                m_ProcessedCommentsCount++;
                txtProcessedCommentsCount.Text = m_ProcessedCommentsCount.ToString();
                //AppGlobals.m_dsEnhNotes.Tables[0].Clear();
                                

            }
            //a small value might be left out at the end of the progress bar, so setting it to 100 as we have already processed all the comments
            prgBrNotesSave.Value = 100;
        }

        private static void AppendHtmlComment(string p_DateTimeStamp,string p_strEnteredBy ,string p_HTMLComment )
        {
            if (p_strEnteredBy.Trim().Length>0 )
	        {
                if (p_DateTimeStamp.Trim().Length>0)
                {
                    m_sbHtmlComment.Append(p_DateTimeStamp);    
                }                
                m_sbHtmlComment.Append(" ( ");
                m_sbHtmlComment.Append(p_strEnteredBy);
                m_sbHtmlComment.Append(" ) ");
        	    m_sbHtmlComment.Append(p_HTMLComment);	 
                m_sbHtmlComment.Append(" <br/><br/> ");
	        }
            else
            {
                if (p_DateTimeStamp.Trim().Length > 0)
                {
                    m_sbHtmlComment.Append(p_DateTimeStamp);
                }
                m_sbHtmlComment.Append("  ");                
        	    m_sbHtmlComment.Append(p_HTMLComment);	 
                m_sbHtmlComment.Append(" <br/><br/> ");
            }            
        }
        
        //debug code //saving HtmlComments into DB
        private void saveHtmlComments()
        {
            //int iRowCount = 0, iEnhNotesCount, iPercentCompleted;
            ////bool bDoSkip = false; // to be removed after dbupgrade patch
            ////iEnhNotesCount = AppGlobals.m_dsEnhNotes.Tables[0].Rows.Count;
            ////string strEnteredByName = string.Empty;
            //int iUid = -1, iGroupID = -1, i = 0;
            ////string strUid = string.Empty, strGroupID = string.Empty;

            ////writeLog("Starting Enhc Notes save operation.");

            ////DbReader objDbReader;

            ////Building the HtmlComments part
            //StringBuilder strHtmlComment = new StringBuilder() ;
            //for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
            //{
            //    strHtmlComment.Append(AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount]["DATE"].ToString());
            //    strHtmlComment.Append(string.Format(" {0} ", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount]["ENTERED_BY_NAME"].ToString()));
            //    strHtmlComment.Append(AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount]["HTML_TEXT"].ToString());                

            //    //ENTERED_BY_NAME
                
            //    //additional checks in the statement below
            //    if (iRowCount < iEnhNotesCount - 1)
            //    {
            //        strHtmlComment.Append("<br/><br/>");    
            //    }
                
            //}
            //MessageBox.Show(strHtmlComment.ToString());

            ////Updating into Db
            //int iNoteId = 0, iCodeId=0;
            //DbReader objDbReader;
            ////DbWriter objDbWriter;
            //DbCommand objCmd = null;
            //string strSql=string.Empty;
            ////try
            //{
            //    //updating next_unique_id for enhanced Notes  
            //    objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, "SELECT MAX(CL_PROG_NOTE_ID) +1 FROM CLAIM_PRG_NOTE");
            //    objDbReader.Read();
            //    iNoteId = objDbReader.GetInt32(0);
            //    strSql = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CLAIM_PRG_NOTE')", iNoteId);
            //    m_objDbConnection.Open();
            //    objCmd = m_objDbConnection.CreateCommand();
            //    objCmd.CommandText = strSql;
            //    objCmd.ExecuteNonQuery();
            //}
                


            //for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
            //{
            //    //iRowCount = p_rowNum;                

            //    //adding values for each enh notes row.           
            //    m_EventHtmlCommentsdbWriter.Fields["CL_PROG_NOTE_ID"].Value = m_iNoteId.ToString();
            //    m_iNoteId++;
            //    m_EventHtmlCommentsdbWriter.Fields["EVENT_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString();

            //    /*    
            //        bDoSkip = false;
            //        if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Length > 3900)
            //        {
            //            MessageBox.Show(string.Format("[Text]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the text\nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
            //            AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3] = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Substring(0, 3900);
            //            bDoSkip = true;
            //        }
            //     */
            //    m_EventHtmlCommentsdbWriter.Fields["NOTE_MEMO"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString();

            //    //if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Length>3900)
            //    //{
            //    //    MessageBox.Show(string.Format("[HTMLtext]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the HTMLtext \nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
            //    //    AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4]=AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Substring(0,3900);
            //    //    bDoSkip = true;
            //    //}

            //    m_EventHtmlCommentsdbWriter.Fields["NOTE_MEMO_CARETECH"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString();
            //    m_EventHtmlCommentsdbWriter.Fields["CLAIM_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][5].ToString();
            //    m_EventHtmlCommentsdbWriter.Fields["DATE_ENTERED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
            //    m_EventHtmlCommentsdbWriter.Fields["DATE_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
            //    //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();

            //    // the above value can be used for saving the extracted date into the db instead of datetime.Now
            //    // basically we are indeed extracting the datetime values from the comments and keeping them in dataset
            //    // but eventually we are saving the datetime.now values for 
            //    // both the "date created" & "activity date" fields on the frontend

            //    // in case where the extracted datetime is to be used and additional check will also need to be 
            //    // employed to make the length of digits for each of the month & day fixed.
            //    // for e.g if the number of digits in day decrease below 2 (i.e. when date is before the 10th)
            //    // there should be a preceding '0' padded to it.


            //    m_EventHtmlCommentsdbWriter.Fields["TIME_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
            //    //System.DateTime.Now.TimeOfDay.ToString().Replace(":", "");
            //    // AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
            //    strEnteredByName = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();
            //    m_EventHtmlCommentsdbWriter.Fields["ENTERED_BY_NAME"].Value = strEnteredByName;

            //    //iUid = getUid(strEnteredByName);

            //    iUid = getUid(strEnteredByName);
            //    if (iUid == -1)
            //    {
            //        strUid = string.Empty;
            //        strGroupID = string.Empty;
            //    }
            //    else
            //    {
            //        strUid = iUid.ToString();
            //        iGroupID = getGroupId(iUid.ToString());
            //        if (iGroupID != -1) strGroupID = iGroupID.ToString();
            //    }
            //    m_EventHtmlCommentsdbWriter.Fields["ENTERED_BY"].Value = strUid;
            //    m_EventHtmlCommentsdbWriter.Fields["USER_TYPE_CODE"].Value = strGroupID;
            //    //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();
            //    //m_EventHtmlCommentsdbWriter.Fields["note_type_code"].Value = m_NoteTypeCodeId;                

            //    try
            //    {
            //        if (!bDoSkip) m_EventHtmlCommentsdbWriter.Execute();
            //    }
            //    catch (Exception exp)
            //    {
            //        writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
            //        throw new Exception(exp.Message.ToString(), exp.InnerException);
            //    }

            //    //changing status & progress bar info
            //    //iPercentCompleted = Convert.ToInt32(((Convert.ToSingle(iRowCount) * 100.0) / Convert.ToSingle(iEnhNotesCount)));
            //    //setStatus(iPercentCompleted.ToString() + " % save completed");                
            //    //prgBrNotesSave.Value = iPercentCompleted;

            //    //disposing
            //    //objDbReader.Close();

            //    //updating the counter
            //    m_EnhcNotesCount++;
            //    txtNotesCount.Text = m_EnhcNotesCount.ToString();
            //}

        }

        //saving individual Notes into DB
        //private void saveNotes()
        //{
        //    int iRowCount = 0,  iEnhNotesCount, iPercentCompleted;
        //    //bool bDoSkip = false; // to be removed after dbupgrade patch
        //    iEnhNotesCount = AppGlobals.m_dsEnhNotes.Tables[0].Rows.Count;
        //    string strEnteredByName=string.Empty;
        //    int iUid = -1, iGroupID = -1;
        //    string strUid = string.Empty, strGroupID = string.Empty;

        //    //writeLog("Starting Enhc Notes save operation.");

        //    //DbReader objDbReader;

        //    for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
        //    {
        //        //iRowCount = p_rowNum;                

        //        //adding values for each enh notes row.           
        //        m_EventHtmlCommentsdbWriter.Fields["CL_PROG_NOTE_ID"].Value = m_iNoteId.ToString();
        //        m_iNoteId++;
        //        m_EventHtmlCommentsdbWriter.Fields["EVENT_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString();

        //    /*    
        //        bDoSkip = false;
        //        if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Length > 3900)
        //        {
        //            MessageBox.Show(string.Format("[Text]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the text\nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
        //            AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3] = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Substring(0, 3900);
        //            bDoSkip = true;
        //        }
        //     */
        //        m_EventHtmlCommentsdbWriter.Fields["NOTE_MEMO"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString();

        //        //if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Length>3900)
        //        //{
        //        //    MessageBox.Show(string.Format("[HTMLtext]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the HTMLtext \nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
        //        //    AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4]=AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Substring(0,3900);
        //        //    bDoSkip = true;
        //        //}

        //        m_EventHtmlCommentsdbWriter.Fields["NOTE_MEMO_CARETECH"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString();
        //        m_EventHtmlCommentsdbWriter.Fields["CLAIM_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][5].ToString();
        //        m_EventHtmlCommentsdbWriter.Fields["DATE_ENTERED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
        //        m_EventHtmlCommentsdbWriter.Fields["DATE_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
        //        //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();

        //        // the above value can be used for saving the extracted date into the db instead of datetime.Now
        //        // basically we are indeed extracting the datetime values from the comments and keeping them in dataset
        //        // but eventually we are saving the datetime.now values for 
        //        // both the "date created" & "activity date" fields on the frontend

        //        // in case where the extracted datetime is to be used and additional check will also need to be 
        //        // employed to make the length of digits for each of the month & day fixed.
        //        // for e.g if the number of digits in day decrease below 2 (i.e. when date is before the 10th)
        //        // there should be a preceding '0' padded to it.


        //        m_EventHtmlCommentsdbWriter.Fields["TIME_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
        //        //System.DateTime.Now.TimeOfDay.ToString().Replace(":", "");
        //        // AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
        //        strEnteredByName = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();                
        //        m_EventHtmlCommentsdbWriter.Fields["ENTERED_BY_NAME"].Value = strEnteredByName;
                
        //        //iUid = getUid(strEnteredByName);

        //        iUid = getUid(strEnteredByName);
        //        if (iUid == -1)
        //        {
        //            strUid = string.Empty;
        //            strGroupID = string.Empty;
        //        }
        //        else
        //        {
        //            strUid = iUid.ToString();
        //            iGroupID = getGroupId(iUid.ToString());
        //            if (iGroupID != -1) strGroupID = iGroupID.ToString();
        //        }
        //        m_EventHtmlCommentsdbWriter.Fields["ENTERED_BY"].Value = strUid;
        //        m_EventHtmlCommentsdbWriter.Fields["USER_TYPE_CODE"].Value = strGroupID; 
        //        //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();
        //        //m_EventHtmlCommentsdbWriter.Fields["note_type_code"].Value = m_NoteTypeCodeId;                

        //        try
        //        {
        //            //if (!bDoSkip) 
        //                m_EventHtmlCommentsdbWriter.Execute();
        //        }
        //        catch (Exception exp)
        //        {
        //            writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
        //            throw new Exception(exp.Message.ToString(), exp.InnerException);
        //        }

        //        //changing status & progress bar info
        //        //iPercentCompleted = Convert.ToInt32(((Convert.ToSingle(iRowCount) * 100.0) / Convert.ToSingle(iEnhNotesCount)));
        //        //setStatus(iPercentCompleted.ToString() + " % save completed");                
        //        //prgBrNotesSave.Value = iPercentCompleted;

        //        //disposing
        //        //objDbReader.Close();
                
        //        //updating the counter
        //        m_EnhcNotesCount++;
        //        txtNotesCount.Text = m_EnhcNotesCount.ToString();
        //    }
            
        //}

        //update NextUniqueValue in Glossary Table
        //private void UpdateGlossary()
        //{
        //    int iNoteId = 0, iCodeId=0;
        //    DbReader objDbReader;
        //    //DbWriter objDbWriter;
        //    DbCommand objCmd = null;
        //    string strSql=string.Empty;
        //    try
        //    {
        //        //updating next_unique_id for enhanced Notes  
        //        objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, "SELECT MAX(CL_PROG_NOTE_ID) +1 FROM CLAIM_PRG_NOTE");
        //        objDbReader.Read();
        //        iNoteId = objDbReader.GetInt32(0);                
        //        strSql = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CLAIM_PRG_NOTE')", iNoteId);                
        //        m_objDbConnection.Open();
        //        objCmd = m_objDbConnection.CreateCommand();
        //        objCmd.CommandText = strSql;
        //        objCmd.ExecuteNonQuery();
                
                
        //        //updating next_unique_id for Code
        //        objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, "SELECT MAX(CODE_ID) +1 FROM CODES");
        //        objDbReader.Read();
        //        iCodeId = objDbReader.GetInt32(0);
        //        strSql = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CODES')", iCodeId);
        //        //m_objDbConnection.Open();
        //        objCmd = m_objDbConnection.CreateCommand();
        //        objCmd.CommandText = strSql;
        //        objCmd.ExecuteNonQuery();

                
        //        //next_unique_id is not used for the Codes_text..   

        //        if (objDbReader!=null)
        //        {
        //            objDbReader.Dispose();
        //        }

                
        //    }
        //    catch (Exception exp)
        //    {
        //        writeLog(string.Format("ERROR:[Error updating glossary table ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
        //        throw new Exception(exp.Message.ToString(), exp.InnerException);
        //    } 
          
        //    //dispose objects
        
        //}

        private void button1_Click(object sender, EventArgs e)
        {            
            dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
            dataGridView1.Refresh();
        }

        private void UpdateHtmlCell(string p_RecordId )
        {
            if (m_strDisplay == 1)
            {
                // for events
                m_EventHtmlCommentsdbWriter.Where.Clear();
                try
                {
                    m_EventHtmlCommentsdbWriter.Fields["HTMLCOMMENTS"].Value = m_sbHtmlComment.ToString();
                    //m_EventHtmlCommentsdbWriter.Fields["EVENT_ID"].Value = p_RecordId;
                    m_EventHtmlCommentsdbWriter.Where.Add("EVENT_ID=" + p_RecordId.ToString());                    
                    m_EventHtmlCommentsdbWriter.Execute();
                }
                catch (Exception exp)
                {
                    writeLog(string.Format("ERROR:[Error writing HtmlComment ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                    throw new Exception(exp.Message.ToString(), exp.InnerException);
                }
            }
            else if (m_strDisplay == 2)
            {
                // for claims
                m_ClaimHtmlCommentsdbWriter.Where.Clear();
                try
                {
                    m_ClaimHtmlCommentsdbWriter.Fields["HTMLCOMMENTS"].Value = m_sbHtmlComment.ToString();
                    m_ClaimHtmlCommentsdbWriter.Where.Add("CLAIM_ID=" + p_RecordId.ToString());
                    m_ClaimHtmlCommentsdbWriter.Execute();
                }
                catch (Exception exp)
                {
                    writeLog(string.Format("ERROR:[Error writing HtmlComment ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                    throw new Exception(exp.Message.ToString(), exp.InnerException);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
 
    }
}