﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using System.Reflection;
using System.Text;
using System.IO;
using Riskmaster.Application.MailMerge;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.MediaViewWrapper;
using System.Configuration;


namespace Riskmaster.Tools.DocToDocx
{
    class WordMerge_DocToDocx
    {
        private class rmTemplate
        {
            public rmTemplate(int formId, string docFormFileName, string docWorkingFilePath)
            {
                Form_Id = formId;
                Doc_Form_File_Name = docFormFileName;
                Doc_Working_File_Path = docWorkingFilePath;
                Docx_Form_File_Name = Doc_Form_File_Name.Replace(".doc", ".docx");
                Docx_Working_File_Path = Doc_Working_File_Path.Replace(".doc", ".docx");
            }
            public int Form_Id;
            public string Doc_Form_File_Name;
            public string Docx_Form_File_Name;
            public string Doc_Working_File_Path;
            public string Docx_Working_File_Path;
            public bool Converted = false;
            public bool Stored = false;
        }
        static string m_sUserID = string.Empty;
        static string m_sPassword = string.Empty;
        static string m_sPathSahred = string.Empty;//nkaranam2 - To Support Shared Path
        static string m_sDSN = string.Empty;
        static string m_DocPath = "";
        static string m_WorkPath = AppDomain.CurrentDomain.BaseDirectory + "work\\";
        static string m_sConnectionString = "";
        static UserLogin m_objUserLogin = null;
        //static bool m_bDbDocType = false;
        static long m_iDBStorageType = 0;
        static string m_SecDsn = string.Empty;
        static string _logPath = Environment.CurrentDirectory + "\\WordMerge_DocToDocx.log";
        static List<rmTemplate> _templates = new List<rmTemplate>();
        static int _warningCount = 0;
        static int _errorCount = 0;
        private static string m_UserDataPath = RMConfigurator.UserDataPath;
        private static UserLogin m_userLogin = null;
        private static SysSettings objSettings = null;
        private static bool blnSuccess = false;

        private static int m_iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess); //sonali for jira -RMACLOUD 2944

        static void log(string msg, Exception ex)
        {
            _errorCount++;
            System.IO.File.AppendAllText(_logPath, Environment.NewLine + "Error: " + msg + Environment.NewLine + ex.ToString());
            Console.WriteLine("Error: " + msg + Environment.NewLine + ex.ToString());
        }
        private enum MessageType { Info, Warning, Error };

        static void log(string msg)
        {
            log(msg, MessageType.Info);
        }
        static void log(string msg, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Error:
                    _errorCount++;
                    break;
                case MessageType.Warning:
                    _warningCount++;
                    break;
            }
            System.IO.File.AppendAllText(_logPath, Environment.NewLine + messageType.ToString() + ": " + msg);
            Console.WriteLine(messageType.ToString() + ": " + msg);
        }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (!CheckParams(args))
            {
                EndApp();
                return;
            }

            if (!UserLogin())
            {
                EndApp();
                return;
            }

            if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
            {
                if (m_iDBStorageType == 0 && !System.IO.Directory.Exists(m_DocPath))
                {//file server
                    Console.WriteLine(String.Format("Invalid Document Path {0} - No templates processed", m_DocPath));
                    EndApp();
                    return;
                }
                else if (m_iDBStorageType == 1)
                {//database
                    string errMsg = string.Empty;

                    using (DbConnection cn = DbFactory.GetDbConnection(m_DocPath))
                    {
                        try
                        {
                            cn.Open();
                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            errMsg = ex.ToString();
                        }
                        if (errMsg.Length > 0)
                        {
                            Console.WriteLine("Error opening connection to document database " + m_DocPath + ", no templates processed. " + Environment.NewLine + errMsg);
                            EndApp();
                            return;
                        }
                    }
                }
            }
            if (File.Exists(_logPath))
            {
                string logFile = _logPath;
                int i = 0;
                while (File.Exists(logFile))
                {
                    i++;
                    logFile = _logPath + i.ToString();
                }
                File.Copy(_logPath, logFile);
                File.Delete(_logPath);
            }

            if (!GetMergeTemplates())
            {
                EndApp();
                return;
            }

            if (!ConvertDocToDocx())
            {
                EndApp();
                return;
            }

            //put converted merge templates in RM docx storage, replacing the docs
            if (!StoreConvertedTemplatesInRiskmaster())
            {
                EndApp();
                return;
            }

            if (!UpdateRMMergeTable())
            {
                EndApp();
                return;
            }

            EndApp();
        }


        static bool GetMergeTemplatesAcrosoft()
        {
            string sAppExcpXml = string.Empty;
            Acrosoft objAcrosoft = null;
            //List<String> lststrDocFiles = new System.Collections.Generic.List<string>();
            //List<String> lststrHDRFiles = new System.Collections.Generic.List<string>();
            //List<int> lstDocFileIDs = new System.Collections.Generic.List<int>();
            using (DbConnection cn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                cn.Open();
                using (DbReader rdr = cn.ExecuteReader("SELECT FORM_ID, FORM_FILE_NAME FROM MERGE_FORM"))
                {
                    while (rdr.Read())
                    {
                        string formFileName = rdr.GetString("FORM_FILE_NAME");
                        int formID = rdr.GetInt("FORM_ID");
                        if (formFileName.EndsWith(".doc"))
                        {
                            //lststrDocFiles.Add(formFileName);
                            //lststrHDRFiles.Add(formFileName.Replace(formFileName.Substring(formFileName.Length - 4, 4),".HDR"));
                            //lstDocFileIDs.Add(rdr.GetInt("FORM_ID"));

                            try
                            {
                                int iRetCd = 0;
                                sAppExcpXml = "";
                                //Fetching  doc from Acrosoft and putting it in the Working path
                                objAcrosoft = new Acrosoft(m_iClientId);//sonali for jira -RMACLOUD 2944
                                Boolean bSuccess = false;
                                string sTempAcrosoftUserId = m_userLogin.LoginName;
                                string sTempAcrosoftPassword = m_userLogin.Password;
                                if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                                {
                                    if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                                    {
                                        sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                                        sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                                    }
                                    else
                                    {
                                        sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                        sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                                    }
                                }

                                iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                             formFileName, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName,
                                             m_WorkPath + formFileName, out sAppExcpXml);
                                if (File.Exists(m_WorkPath + formFileName))
                                {
                                    CreateHeaderFile(formID, m_WorkPath, formFileName);
                                    _templates.Add(new rmTemplate(formID, formFileName, m_WorkPath + "\\" + formFileName));
                                }
                                else
                                {
                                    log("unable to pull from Acrosoft " + formFileName + " into " + m_WorkPath + formFileName);
                                    continue;
                                }
                            }
                            catch (Exception ex)
                            {
                                log("unable to pull from Acrosoft " + formFileName + " into " + m_WorkPath + formFileName, ex);
                                continue;
                            }
                            log("successfully pulled from Acrosoft " + formFileName + " into " + m_WorkPath + formFileName);
                            //foreach (String strDocFile in lststrDocFiles)
                            //{
                            //    iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            //             strDocFile, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName,
                            //             m_WorkPath + strDocFile, out sAppExcpXml);
                            //    if (iRetCd == 0)
                            //    {
                            //        _templates.Add(new rmTemplate(lstDocFileIDs.ElementAt(2), strDocFile, m_WorkPath + "\\" + strDocFile));
                            //    }
                            //}
                            //foreach (String strHDRFile in lststrHDRFiles)
                            //{
                            //    iRetCd = objAcrosoft.FetchAndWriteGeneralFolderDocument(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            //                strHDRFile, "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName,
                            //                m_WorkPath + strHDRFile, out sAppExcpXml);
                            //}          


                        }
                    }
                }
            }

            return true;
        }
        static bool GetMergeTemplatesMediaView()
        {
            MediaView objMediaView = null;
            using (DbConnection cn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                cn.Open();
                using (DbReader rdr = cn.ExecuteReader("SELECT FORM_ID, FORM_FILE_NAME FROM MERGE_FORM"))
                {
                    while (rdr.Read())
                    {
                        string formFileName = rdr.GetString("FORM_FILE_NAME");
                        int formID = rdr.GetInt("FORM_ID");
                        if (formFileName.EndsWith(".doc"))
                        {
                            try
                            {
                                objMediaView = new MediaView();

                                string sFormFileName = formFileName;
                                if (objMediaView.MMGetDocumentByName("MailMergeTemplates", "Misc", ref sFormFileName, ref m_WorkPath))
                                {
                                    _templates.Add(new rmTemplate(formID, formFileName, m_WorkPath + "\\" + formFileName));
                                    log("successfully pulled from Media View " + formFileName + " into " + m_WorkPath + formFileName);
                                    CreateHeaderFile(formID, m_WorkPath, formFileName);
                                }
                                else
                                    log("unable to pull from Media View " + formFileName + " into " + m_WorkPath + formFileName);

                            }
                            catch (Exception ex)
                            {
                                log("unable to pull from Media View " + formFileName + " into " + m_WorkPath + formFileName, ex);
                                continue;
                            }
                        }
                    }
                }
            }

            return true;

        }

        static bool GetMergeTemplates()
        {
            if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
            {
                string sTrash = "";

                FileStorageManager objFileManager = null;

                if (m_iDBStorageType ==1)
                {
                    objFileManager = new FileStorageManager(m_iClientId);//sonali for jira -RMACLOUD 2944
                    objFileManager.FileStorageType = StorageType.DatabaseStorage;
                    objFileManager.DestinationStoragePath = m_DocPath;
                }

                int counter = 0;

                using (DbConnection cn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    cn.Open();

                    //Clear out and ensure working folder exists.
                    if (System.IO.Directory.Exists(m_WorkPath))
                        System.IO.Directory.Delete(m_WorkPath, true);
                    System.IO.Directory.CreateDirectory(m_WorkPath);

                    using (DbReader rdr = cn.ExecuteReader("SELECT FORM_ID, FORM_FILE_NAME FROM MERGE_FORM"))
                    {
                        while (rdr.Read())
                        {
                            string formFileName = rdr.GetString("FORM_FILE_NAME");
                            int formId = rdr.GetInt("FORM_ID");
                            if (formFileName.EndsWith(".doc"))
                            {
                                counter++;
                                if (m_iDBStorageType == 1) //Pull from DocStorage
                                {
                                    try
                                    {
                                        objFileManager.RetrieveFile(formFileName, m_WorkPath + formFileName, m_userLogin.LoginName, out sTrash);//nkaranam2 - Including the parameter as a new parameter added to this method in 14.1
                                        CreateHeaderFile(formId, m_WorkPath, formFileName);
                                        _templates.Add(new rmTemplate(formId, formFileName, m_WorkPath + formFileName));
                                    }
                                    catch (Exception ex)
                                    {
                                        log("unable to pull from database " + formFileName + " into " + m_WorkPath + formFileName, ex);
                                        continue;
                                    }
                                    log("successfully pulled from database " + formFileName + " into " + m_WorkPath + formFileName);
                                }
                                else  //Pull from DocPath
                                {
                                    //nkaranam2 - To Support Shared Path
                                    if (!string.IsNullOrEmpty(m_sPathSahred))
                                    {
                                        try
                                        {
                                            if (File.Exists(m_sPathSahred + "\\" + formFileName))
                                            {
                                                System.IO.File.Copy(m_sPathSahred + "\\" + formFileName, m_WorkPath + formFileName, true);
                                                CreateHeaderFile(formId, m_WorkPath, formFileName);
                                                _templates.Add(new rmTemplate(formId, formFileName, m_WorkPath + "\\" + formFileName));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            log("unable to copy " + m_sPathSahred + "\\" + formFileName + " into " + m_WorkPath + formFileName, ex);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            System.IO.File.Copy(m_DocPath + "\\" + formFileName, m_WorkPath + formFileName, true);
                                            CreateHeaderFile(formId, m_WorkPath, formFileName);
                                            _templates.Add(new rmTemplate(formId, formFileName, m_WorkPath + "\\" + formFileName));
                                        }
                                        catch (Exception ex)
                                        {
                                            log("unable to copy " + m_DocPath + "\\" + formFileName + " into " + m_WorkPath + formFileName, ex);
                                            continue;
                                        }
                                        log("successfully copied " + m_DocPath + "\\" + formFileName + " into " + m_WorkPath + formFileName);
                                    }
                                }
                            }
                        }
                    }
                }
                if (counter == 0)
                {
                    log("zero merge templates with '*.doc' extension exist in your document storage, there is nothing to process.");
                    return false;
                }
                else if (counter > _templates.Count)
                {
                    log(counter.ToString() + " merge templates with '*.doc' extension exist in your document storage but only " + _templates.Count + " will be processed.", MessageType.Warning);
                }
                else
                {
                    log(counter.ToString() + " merge templates with '*.doc' extension exist in your document storage and " + _templates.Count + " will be processed.");
                }
            }
            else if (objSettings.UseAcrosoftInterface == true)
            {
                GetMergeTemplatesAcrosoft();
            }
            else if (objSettings.UseMediaViewInterface)
            {
                GetMergeTemplatesMediaView();
            }
            return true;
        }
        static void CreateHeaderFile(long iFormID, string sWorkPath, string docFileName)
        {
            IDictionaryEnumerator objEnum = null;
            TemplateField objField = null;
            string sLine = string.Empty;
            string sHeaderFileName = docFileName.Replace(".doc", ".HDR");
            Template oTemplate = new Template(m_objUserLogin, m_iDBStorageType, m_DocPath, m_iClientId);

            oTemplate.SecureDSN = SecurityDatabase.Dsn;
            oTemplate.Load(iFormID);
            objEnum = oTemplate.Fields.GetEnumerator();
            while (objEnum.MoveNext())
            {
                objField = (TemplateField)objEnum.Value;
                if (sLine != "")
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
                sLine = sLine + Convert.ToChar(34).ToString() + objField.WordFieldName + Convert.ToChar(34).ToString();

                //For single field, it need to end with ascii 9
                if (oTemplate.Fields.Count == 1)
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
            }

            //write the header file to the work folder
            sHeaderFileName = sWorkPath + sHeaderFileName;
            StreamWriter objWriter = new StreamWriter(sHeaderFileName, false);
            objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
            objWriter.Flush();
            objWriter.Close();
        }

        static bool ConvertDocToDocx()
        {

            Type tApp = null;
            Object objApp = null;
            const int wdFormatXMLDocument = 12;
            try
            {
                // Use reflection to create instance of Word...avoids version compatibility issue
                tApp = System.Type.GetTypeFromProgID("Word.Application");
                objApp = Activator.CreateInstance(tApp);
                foreach (rmTemplate template in _templates)
                {
                    log("converting " + template.Doc_Working_File_Path + " to " + template.Docx_Working_File_Path);
                    object oDocs = tApp.InvokeMember("Documents", BindingFlags.GetProperty, null, objApp, null);
                    object aDoc = oDocs.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, oDocs, new object[] { template.Doc_Working_File_Path });
                    aDoc.GetType().InvokeMember("Activate", BindingFlags.InvokeMethod, null, aDoc, null);
                    object oActiveDoc = tApp.InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, objApp, null);
                    //nkaranam2 - To Associate with the DataSource
                    object oMailMerge = oActiveDoc.GetType().InvokeMember("MailMerge", BindingFlags.GetProperty, null, oActiveDoc, null);
                    oMailMerge.GetType().InvokeMember("OpenDataSource", BindingFlags.InvokeMethod, null, oMailMerge, new object[] { template.Doc_Working_File_Path.Replace(".doc", ".HDR") });
                    //nkaranam2 - To Associate with the DataSource
                    oActiveDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, oActiveDoc, new object[] { template.Docx_Working_File_Path, wdFormatXMLDocument });
                    aDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, aDoc, new object[] { false, null, null });
                    log("successfully converted " + template.Doc_Working_File_Path + " to " + template.Docx_Working_File_Path);
                    template.Converted = true;
                }
            }
            finally
            {
                //Closing the application
                if (objApp != null)
                    objApp.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, objApp, new object[] { false, null, null });
            }
            return true;

        }

        static bool StoreConvertedTemplatesInRiskmaster()
        {
            if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
            {
                FileStorageManager objFileManager = null;
                if (m_iDBStorageType == 1)
                {
                    objFileManager = new FileStorageManager(m_iClientId);//sonali for jira -RMACLOUD 2944
                    objFileManager.FileStorageType = StorageType.DatabaseStorage;
                    objFileManager.DestinationStoragePath = m_DocPath;
                }

                foreach (rmTemplate template in _templates)
                {
                    //get the header file name
                    string sHeaderFileName = template.Docx_Form_File_Name;
                    sHeaderFileName = sHeaderFileName.Replace(sHeaderFileName.Substring(sHeaderFileName.Length - 5, 5), ".HDR");
                    string sHeaderWorkingFilePath = template.Docx_Working_File_Path;
                    sHeaderWorkingFilePath = sHeaderWorkingFilePath.Replace(sHeaderWorkingFilePath.Substring(sHeaderWorkingFilePath.Length - 5, 5), ".HDR");

                    if (template.Converted)
                    {
                        if (m_iDBStorageType == 1) //Push into DocStorage
                        {
                            try
                            {
                                objFileManager.StoreFile(template.Docx_Working_File_Path, template.Docx_Form_File_Name);
                                objFileManager.StoreFile(sHeaderWorkingFilePath, sHeaderFileName);
                                template.Stored = true;
                            }
                            catch (Exception ex)
                            {
                                log("while placing in datatabase document storage: " + template.Docx_Working_File_Path, ex);
                                continue;
                            }
                            //delete the doc
                            try
                            {
                                objFileManager.DeleteFile(template.Doc_Form_File_Name);
                                log("successfully deleted doc file from document storage: " + template.Doc_Form_File_Name);
                            }
                            catch (Exception ex)
                            {
                                log("template was successfully placed in doc storage, but unable to delete doc: " + template.Doc_Form_File_Name, ex);
                            }
                        }
                        else  //Push into DocPath
                        {
                            if (!string.IsNullOrEmpty(m_sPathSahred))
                            {
                                try
                                {
                                    File.Copy(template.Docx_Working_File_Path, m_sPathSahred + "\\" + template.Docx_Form_File_Name, true);
                                    File.Copy(sHeaderWorkingFilePath, m_sPathSahred + "\\" + sHeaderFileName, true);
                                    template.Stored = true;
                                }
                                catch (Exception ex)
                                {
                                    log("while copying into document storage: " + template.Docx_Working_File_Path + " to " + m_sPathSahred + "\\" + template.Docx_Form_File_Name, ex);
                                }
                                //delete the doc
                                try
                                {
                                    File.Delete(m_sPathSahred + "\\" + template.Doc_Form_File_Name);
                                    log("successfully deleted doc file from document storage: " + m_sPathSahred + "\\" + template.Doc_Form_File_Name);
                                }
                                catch (Exception ex)
                                {
                                    log("Warning: template was successfully placed in doc storage, but unable to delete doc: " + m_sPathSahred + "\\" + template.Doc_Form_File_Name, ex);
                                }

                            }
                            else
                            {
                                try
                                {
                                    File.Copy(template.Docx_Working_File_Path, m_DocPath + "\\" + template.Docx_Form_File_Name, true);
                                    File.Copy(sHeaderWorkingFilePath, m_DocPath + "\\" + sHeaderFileName, true);
                                    template.Stored = true;
                                }
                                catch (Exception ex)
                                {
                                    log("while copying into document storage: " + template.Docx_Working_File_Path + " to " + m_DocPath + "\\" + template.Docx_Form_File_Name, ex);
                                    continue;
                                }
                                //delete the doc
                                try
                                {
                                    File.Delete(m_DocPath + "\\" + template.Doc_Form_File_Name);
                                    log("successfully deleted doc file from document storage: " + m_DocPath + "\\" + template.Doc_Form_File_Name);
                                }
                                catch (Exception ex)
                                {
                                    log("Warning: template was successfully placed in doc storage, but unable to delete doc: " + m_DocPath + "\\" + template.Doc_Form_File_Name, ex);
                                }
                            }
                        }
                    }
                }
            }
            else if (objSettings.UseAcrosoftInterface == true)
            {
                StoreConvertedTemplatesInAcrosoft();
            }
            else if (objSettings.UseMediaViewInterface)
            {
                StoreConvertedTemplatesInMediaView();
            }
            return true;
        }
        static bool StoreConvertedTemplatesInAcrosoft()
        {
            Acrosoft objAcrosoft = null;
            RMConfigurator objConfig = null;
            foreach (rmTemplate template in _templates)
            {
                //objFileStorageManager.StoreFile(Path.GetTempPath() +this.FormFileName,this.FormFileName);
                string sDocTitle = template.Docx_Form_File_Name;
                string sAppExcpXml = "";
                objConfig = new RMConfigurator();
                objAcrosoft = new Acrosoft(m_iClientId);//sonali for jira -RMACLOUD 2944

                //rsolanki2 :  start updates for MCM mits 19200 
                Boolean bTempSuccess = false;
                string sTempAcrosoftUserId = m_userLogin.LoginName;
                string sTempAcrosoftPassword = m_userLogin.Password;

                if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bTempSuccess))
                {
                    if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                        && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                        sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                    }
                    else
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                        sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                    }
                }

                //deleting old template
                //gagnihotri R5 MCM changes
                //objAcrosoft.DeleteFile(m_userLogin.LoginName , m_userLogin.Password ,this.FormFileName , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , objConfig.GetValue("GeneralFolderFriendlyName"), out sAppExcpXml);
                //objAcrosoft.StoreObjectBuffer(m_userLogin.LoginName , m_userLogin.Password , Path.GetTempPath() + this.FormFileName , sDocTitle , "" , "Templates" , objConfig.GetValue("AcrosoftGeneralStorageTypeKey") , "" , "" , m_userLogin.LoginName , out sAppExcpXml);
                try
                {
                    objAcrosoft.CreateGeneralFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftGeneralStorageTypeKey, "Templates", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);

                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        template.Docx_Working_File_Path, sDocTitle, "", "Templates", AcrosoftSection.AcrosoftGeneralStorageTypeKey, "", "",
                        sTempAcrosoftUserId, out sAppExcpXml);
                    template.Stored = true;

                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword,
                      template.Doc_Form_File_Name, "Templates",
                      AcrosoftSection.AcrosoftGeneralStorageTypeKey, AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                    //gagnihotri Creating the Templates folder, if not there already
                    log("successfully deleted doc file from document storage: " + template.Doc_Form_File_Name);
                }
                catch (Exception ex)
                {
                    log("while placing in Acrosoft document storage: " + template.Docx_Working_File_Path, ex);
                }
            }
            return true;

        }

        static bool StoreConvertedTemplatesInMediaView()
        {
            MediaView objMediaView = null;
            foreach (rmTemplate template in _templates)
            {
                try
                {
                    objMediaView = new MediaView();
                    importList oList = new importList("MailMergeTemplates", "Misc", template.Docx_Form_File_Name);
                    objMediaView.MMUploadDocument(template.Docx_Form_File_Name, m_WorkPath, oList);
                    template.Stored = true;
                }
                catch (Exception ex)
                {
                    log("while placing in Media View document storage: " + template.Docx_Working_File_Path, ex);
                }
                try
                {
                    objMediaView.MMDeleteDocumentByName("MailMergeTemplates", "Misc", template.Doc_Working_File_Path);
                    log("successfully deleted doc file from document storage: " + template.Doc_Form_File_Name);
                }
                catch (Exception ex)
                {
                    log("while Deleting in Media View document storage: " + template.Doc_Form_File_Name, ex);
                }
            }
            return true;
        }

        static bool UpdateRMMergeTable()
        {
            string sql = "";
            try
            {
                using (DbConnection cnn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    cnn.Open();
                    foreach (rmTemplate template in _templates)
                    {
                        if (template.Stored)
                        {
                            sql = "UPDATE MERGE_FORM SET FORM_FILE_NAME = '" + template.Docx_Form_File_Name + "' WHERE FORM_ID = " + template.Form_Id.ToString();
                            int i = cnn.ExecuteNonQuery(sql);
                            if (i == 1)
                                log("successfully updated MERGE_FORM record for FORM_ID " + template.Form_Id.ToString());
                            else
                            {
                                log("update statement " + sql + " updated " + i.ToString() + " records, it should have updated 1 record", MessageType.Warning);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log("updating MERGE_FORM table while executing sql statement: " + sql, ex);
                return false;
            }

            return true;
        }



        /// <summary>
        /// Login to the system
        /// </summary>
        static bool UserLogin()
        {
            frmLogin objForm = new frmLogin(m_iClientId);
            bool bLoginSuccess = false;
            string sErrorMessage = string.Empty;
            try
            {
                while (!bLoginSuccess)
                {
                    objForm.ShowDialog();
                    if (!objForm.LoginSuccess)
                    {
                        objForm.Dispose();
                        objForm = null;
                        return false;
                    }

                    m_sUserID = objForm.UserID;
                    m_sPassword = objForm.Password;
                    m_sDSN = objForm.DSN;
                    m_userLogin = new Security.UserLogin(m_sUserID, m_sDSN, m_iClientId);//sonali JIRA RMACLOUD-2944
                    if (!string.IsNullOrEmpty(objForm.sPathShared))
                        m_sPathSahred = objForm.sPathShared;
                    try
                    {
                        m_SecDsn = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);//sonali for jira -RMACLOUD 2944
                        Login objLogin = new Login(m_SecDsn, m_iClientId);//sonali JIRA RMACLOUD-2944
                        bool bLogin = objLogin.AuthenticateUser(m_sDSN, m_sUserID, m_sPassword, out m_objUserLogin, m_iClientId);//sonali JIRA RMACLOUD-2944
                        if (bLogin)
                        {
                            m_sConnectionString = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
                            m_DocPath = m_objUserLogin.objRiskmasterDatabase.GlobalDocPath;
                            m_iDBStorageType = m_objUserLogin.objRiskmasterDatabase.DocPathType;
                            bLoginSuccess = true;
                        }
                        else
                        {
                            sErrorMessage = "Login failed. Please try again.";
                            objForm.LoginMessage = sErrorMessage;
                            Console.WriteLine(sErrorMessage);
                        }
                        objSettings = new SysSettings(m_sConnectionString, m_iClientId);//sonali JIRA RMACLOUD-2944
                        if (m_iDBStorageType == 0 && objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
                        {
                            m_DocPath = objForm.m_sPathShared;
                        }
                    }
                    catch (OrgSecAccessViolationException ex)
                    {
                        sErrorMessage = "BES is enabled. Please use a BES admin group member account to upgrade PowerViews";
                        objForm.LoginMessage = ex.Message;
                        Console.WriteLine(sErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        sErrorMessage = ex.Message + " Please try again.";
                        objForm.LoginMessage = sErrorMessage;
                        Console.WriteLine(sErrorMessage);
                    }
                }
                objForm.Dispose();
                objForm = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            };
            return true;
        }


        static void EndApp()
        {
            string msg = "Completed";
            if (_errorCount > 0)
                msg += " with " + _errorCount.ToString() + " Errors";
            if (_warningCount > 0)
                msg += " with " + _warningCount.ToString() + " Warnings";
            msg += ", please read log file at " + _logPath;
            msg += Environment.NewLine + "Press any key to close...";
            Console.WriteLine(msg);

            Console.ReadLine();
        }

        static bool CheckParams(string[] args)
        {
            if (args.Length > 0)
            {
                Console.WriteLine("WordMerge_DocToDocx.exe does not use any command line parameters.");
                Console.WriteLine("Please execute this application again without command line arguments.");
                Console.WriteLine("You will select the target Riskmaster database using the standard Riskmaster Security UI.");
                return false;
            }
            return true;
        }

    }
}
