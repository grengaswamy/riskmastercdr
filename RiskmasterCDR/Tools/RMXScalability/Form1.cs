﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Scripting;
using Riskmaster.Settings;
using Riskmaster.Models;
using Riskmaster.Application.CodesList;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.Common;
using System.Resources;
using System.Threading;
using Riskmaster.Common;


namespace RMXScalability
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Author : Rupal Kotak
        /// creation Date : 12 April 2011
        /// Description : Generated Random Data for WC claim / Employee and saves in database
        /// </summary>

        #region Variable Declaration
        //DataModelFactory DBHelper.oDataModelFactory = null;
        //CodesListManager DBHelper.oCodeListManager = null;

        List<int> ilstEventTypeCode = new List<int>();
        List<int> ilstClaimStatusCode =  new List<int>();
        List<int> ilstClaimTypeCodeWC = new List<int>();
        List<int> ilstClaimTypeCodeGC = new List<int>();
        List<int> ilstClaimTypeCodePC = new List<int>();
        List<int> ilstClaimTypeCodeVA = new List<int>();
        List<int> ilstClaimTypeCodeDI = new List<int>();
        List<int> ilstDepartmentCode = new List<int>();
        List<int> ilstStateRowID = new List<int>();
        List<int> ilstPolicyLOB_WC = new List<int>();
        List<int> ilstPolicyLOB_GC = new List<int>();
        List<int> ilstPropertyIdCode = new List<int>();

        List<string> slstEmpName = new List<string>();
        
        public int iWCLOBCode = 0;
        public int iGCLOBCode = 0;
        public int iPCLOBCode = 0;
        public int iVALOBCode = 0;
        public int iNonOccLOBCode = 0;
        public int iEventStatusCodeOpen = 0;
        public int iClaimStatusCodeOpenWC = 0;
        public int iClaimStatusCodeOpenGC = 0;
        public int iClaimStatusCodeOpenPC = 0;
        public int iClaimStatusCodeOpenVA = 0;
        public int iClaimStatusCodeOpenDI = 0;
        public int iEmployeeEntityTableId = 0;
        public int iClaimantEntityTableId = 0;
        public int iEntityApprovalStatusCode = 0;
        public int iPlanStatusCode = 0; //for "In Effect"
        public int iTotalCount = 0;

        public int iEmpRowCount = 0;
        public int iEventRowCount = 0;
        public int iWCRowCount = 0;
        public int iGCRowCount = 0;
        public int iPCRowCount = 0;
        public int iVARowCount = 0;
        public int iNonOccRowCount = 0;
        public int iDeptCount = 20;
        
        //int iWCClaimPerEmployee = 0;
        //int iGCClaimPerClaimant = 0;
        int iUseSupportiveChildEntitiesForHowManyRecords = 0;
        DateTime dtFromDate;
        DateTime dtToDate;

        bool allowSpace = false;        
        
        CodeListPrm objCodeListPrm = new CodeListPrm();
        Random objRandom = new Random();
        
        string sNumbers = "0123456789";        
        
        bool bConverted = false;
        
        int iNoOfClaimCreated = 0;
        int iNoOfEmpCreated = 0;
        int iNoOfEventCreated = 0;

        AppHelper objHelper = new AppHelper();
        string sAlphabets = "abcdefghijklmnopqrstuvwxyz";
        int iEmpNameLength = 4;
        int iProcessID = 0;
        int iBaseCurrCode = 0;

        List<int> ilstProcessIDs = new List<int>();

        /// <summary>
        /// struct to hold together required parameters to populate a code List
        /// </summary>
        public struct CodeListPrm
        {
            public string sFormName;
            public string sTableName;
            public string sLOBCode;            
        }

        string sExceptionKey = "Form1.Error";
        string sEmpTableName = "Employee";
        string sWCClaimTableName = "Claim";
        string sGCClaimTableName = "Claim";
        string sEventTableName = "Event";
        string sClaimantTableName = "OTHER_PEOPLE";
        bool bSuccess = false;
        #endregion        

        public Form1()
        {
            InitializeComponent();            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                lblStatusBar.Text = string.Empty;
                if (!ValidateForm())
                {
                    EnableButtons(false);
                    btnClose.Enabled = true;
                    return;
                }

                //must be called after InitializeComponent
                InitializeApp();

                //Get Values that will remail Constant for each Employee/WC Claim
                GetConstantIDs();

                //Populate code list or any kind of list from which we will get random records
                //CreateCodeListEntities();//moved to btnGenerate_Click

                EnableButtons(true);
                //enable view report button only after succesful execution
                btnViewReport.Enabled = false;

                TimeSpan objTimeSpan = new TimeSpan(30, 0, 0, 0);
                dtpFromDate.Value = DateTime.Now.Subtract(objTimeSpan);
            }
            catch (RMAppException ex)
            {
                EnableButtons(false);
                btnClose.Enabled = true;
                objHelper.SaveExceptionToDisk(ex);
                MessageBox.Show(ex.Message, "Error");
            }
            catch (Exception ex)
            {
                EnableButtons(false);
                btnClose.Enabled = true;
                objHelper.SaveExceptionToDisk(ex);
                MessageBox.Show(ex.Message, "Error");
            }
        }
      
        #region Validation       

        private bool ValidateForm()
        {
            try
            {
                //validate App Seetings Keys and their values
                if (!objHelper.ValidateAppConfigSetting())
                    return false;               
                if (!objHelper.ValidateWCClaimPerEmployee())
                    return false;
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "ValidateForm"), ex);
            }
            return true;
        }
        #endregion

        #region Initialize Application            
        
        /// <summary>
        /// This function initializes user credentials and authentication and gets datamodel factory. must be called before any function called on the page
        /// </summary>
        /// 
        private void InitializeApp()
        {
            try
            {
                GetConfigSettings();                                                     
                //GetDataModelFactory();                     
                //GetCodeListManager();                
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "InitializeApp"), ex);
            }
        }

        private void GetConfigSettings()
        {
            try
            {
               // sUserName = objHelper.GetConfigSetting("UserID");
               // sPassword = objHelper.GetConfigSetting("Password");
               // sDataSourceName = objHelper.GetConfigSetting("DataSourceName");                
                //sEmployeeNameFilePath = objHelper.GetExePath() + "\\EmployeeName.xml";
                //iWCClaimPerEmployee = Conversion.CastToType<int>(objHelper.GetConfigSetting("WCClaimPerEmployee"), out bConverted);
                iUseSupportiveChildEntitiesForHowManyRecords = Conversion.CastToType<int>(objHelper.GetConfigSetting("UseSupportiveChildEntitiesForHowManyRecords"), out bConverted);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetConfigSettings"), ex);
            }
        }        

        /// <summary>
        /// Get DataModel Factory
        /// </summary>
       /*
        private void GetDataModelFactory()
        {
            try
            {
                DBHelper.oDataModelFactory = DBHelper.oDataModelFactory;
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetDataModelFactory"), ex);
                
            }
        }
        */
        /*
        /// <summary>
        /// Get CodeListManager
        /// </summary>
        private void GetCodeListManager()
        {
            try
            {
                DBHelper.oCodeListManager = DBHelper.oCodeListManager;               
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetCodeListManager"), ex);
            }
        }       */

        #endregion

        #region Private Functions      

        /// <summary>
        /// This function gets all constant Feilds that do need randomization.
        /// </summary>
        private void GetConstantIDs()
        {
            try
            {
                //Get WC,GC Lob Code
                GetLOBCode();

                #region Get EVENT_Status Code for "Open"
                objCodeListPrm = new CodeListPrm();
                objCodeListPrm.sTableName = "EVENT_STATUS";
                objCodeListPrm.sFormName = "EVENT";
                objCodeListPrm.sLOBCode = string.Empty;
                iEventStatusCodeOpen = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                #endregion                               

                #region Get EntityApproval Code for "Approved"
                objCodeListPrm = new CodeListPrm();
                objCodeListPrm.sTableName = "ENTITY_APPRV_REJ";
                objCodeListPrm.sFormName = string.Empty;
                objCodeListPrm.sLOBCode = string.Empty;
                iEntityApprovalStatusCode = GetStatusCodeID(objCodeListPrm, "A"); //"A" for Approved
                #endregion

                //get employee entity table id
                iEmployeeEntityTableId = objHelper.GetTableID(sEmpTableName); //constant for all employee 

                //get claimant entity table id
                iClaimantEntityTableId = objHelper.GetTableID(sClaimantTableName); //constant for all employee 

                //get basecurrency code
                iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(DBHelper.sConnectionString);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetConstantIDs"), ex);
            }
        }

        /// <summary>
        /// Get WC LOB Code
        /// </summary>
        private void GetLOBCode()
        {
            try
            {

                CodeListType objCodeListType = DBHelper.oCodeListManager.GetCodesNew(0, "LINE_OF_BUSINESS", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, string.Empty, string.Empty);
                List<CodeType> lstCodeType = objCodeListType.Codes;
                if (lstCodeType.Count > 0)
                {
                    iWCLOBCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "WC"
                                       select a.Id).FirstOrDefault();
                    iGCLOBCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "GC"
                                       select a.Id).FirstOrDefault();
                    iPCLOBCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "PC"
                                       select a.Id).FirstOrDefault();
                    iVALOBCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "VA"
                                       select a.Id).FirstOrDefault();
                    iNonOccLOBCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "DI"
                                       select a.Id).FirstOrDefault();
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("WCLOBCode.Missing"));
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetWCLOBCode"), ex);
            }
        }

        /// <summary>
        /// generic function To get StatusCode ID. it first populates the code list based first parameter and then gets status id
        /// </summary>
        /// <param name="objCodeListPrm">Struct which containts all values required to populate code list</param>
        /// <param name="ShortCode">Get CodeID for this ShortCode from the CodeList</param>
        /// <returns></returns>
        private int GetStatusCodeID(CodeListPrm objCodeListPrm, string ShortCode)
        {
            int iStatusCodeID = 0;
            try
            {
                CodeListType objCodeListType = DBHelper.oCodeListManager.GetCodesNew(0, objCodeListPrm.sTableName, objCodeListPrm.sLOBCode, string.Empty, objCodeListPrm.sFormName, string.Empty, string.Empty, string.Empty, 0, 0, string.Empty, string.Empty);
                List<CodeType> lstCodeType = objCodeListType.Codes;
                if (lstCodeType.Count > 0)
                {
                    if (ShortCode != string.Empty)
                    {
                        iStatusCodeID = (int)(from a in lstCodeType
                                              where a.ShortCode.ToUpper() == ShortCode.ToUpper()
                                              select a.Id).First();
                    }
                    else
                    {
                        iStatusCodeID = (int)(from a in lstCodeType                                              
                                              select a.Id).First();
                    }
                }
                else
                {
                    string ResourceKey = objCodeListPrm.sTableName + ".Missing";
                    throw new RMAppException(Globalization.GetString(ResourceKey));
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetStatusCodeID"), ex);
            }
            return iStatusCodeID;
        }

        /// <summary>
        /// this fucntions populates different codes list from which we will pick values randomly.
        /// </summary>
        private void CreateCodeListEntities()
        {
            try
            {
                #region Get EVENT_Type Code List
                objCodeListPrm = new CodeListPrm();
                objCodeListPrm.sTableName = "EVENT_TYPE";
                objCodeListPrm.sFormName = "EVENT";
                objCodeListPrm.sLOBCode = string.Empty;
                GetCodeList(objCodeListPrm, ref ilstEventTypeCode);
                #endregion

                /*
                #region Get POLICY_LOB Code List
                GetPolicyLOBList();
                #endregion
                */

                #region Get CLAIM_TYPE Code List, Get claim Status COde for Open
                objCodeListPrm = new CodeListPrm();
                objCodeListPrm.sTableName = "CLAIM_TYPE";
                if (chkWC.Checked)
                {
                    objCodeListPrm.sFormName = "claimwc";
                    objCodeListPrm.sLOBCode = iWCLOBCode.ToString();
                    GetCodeList(objCodeListPrm, ref ilstClaimTypeCodeWC);

                    //first try to get claim status code for short code "O" (Open)
                    try
                    {                        
                        objCodeListPrm.sTableName = "CLAIM_STATUS";
                        iClaimStatusCodeOpenWC = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                    }
                    catch
                    {
                    }
                    if (iClaimStatusCodeOpenWC == 0)
                    {
                        iClaimStatusCodeOpenWC = GetStatusCodeID(objCodeListPrm, string.Empty); //get any status code for claim since "Open" code does not exists
                    }
                }
                if (chkGC.Checked)
                {
                    objCodeListPrm.sFormName = "claimgc";
                    objCodeListPrm.sLOBCode = iGCLOBCode.ToString();
                    GetCodeList(objCodeListPrm, ref ilstClaimTypeCodeGC);

                    //first try to get claim status code for short code "O" (Open)
                    try
                    {                        
                        objCodeListPrm.sTableName = "CLAIM_STATUS";                     
                        iClaimStatusCodeOpenGC = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                    }
                    catch
                    {
                    }
                    if (iClaimStatusCodeOpenGC == 0)
                    {
                        iClaimStatusCodeOpenGC = GetStatusCodeID(objCodeListPrm, string.Empty); //get any status code for claim since "Open" code does not exists
                    }
                }
                if (chkPC.Checked)
                {
                    objCodeListPrm.sFormName = "claimpc";
                    objCodeListPrm.sLOBCode = iPCLOBCode.ToString();
                    GetCodeList(objCodeListPrm, ref ilstClaimTypeCodePC);
                    //first try to get claim status code for short code "O" (Open)
                    try
                    {
                        objCodeListPrm.sTableName = "CLAIM_STATUS";
                        iClaimStatusCodeOpenPC = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                    }
                    catch
                    {
                    }
                    if (iClaimStatusCodeOpenPC == 0)
                    {
                        iClaimStatusCodeOpenPC = GetStatusCodeID(objCodeListPrm, string.Empty); //get any status code for claim since "Open" code does not exists
                    }
                }
                if (chkVA.Checked)
                {
                    objCodeListPrm.sFormName = "claimva";
                    objCodeListPrm.sLOBCode = iVALOBCode.ToString();
                    GetCodeList(objCodeListPrm, ref ilstClaimTypeCodeVA);
                    //first try to get claim status code for short code "O" (Open)
                    try
                    {                        
                        objCodeListPrm.sTableName = "CLAIM_STATUS";                        
                        iClaimStatusCodeOpenVA = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                    }
                    catch
                    {
                    }
                    if (iClaimStatusCodeOpenVA == 0)
                    {
                        iClaimStatusCodeOpenVA = GetStatusCodeID(objCodeListPrm, string.Empty); //get any status code for claim since "Open" code does not exists
                    }
                }
                if (chkNonOccc.Checked)
                {
                    //get Claim Type
                    objCodeListPrm.sFormName = "claimdi";
                    objCodeListPrm.sLOBCode = iNonOccLOBCode.ToString();
                    GetCodeList(objCodeListPrm, ref ilstClaimTypeCodeDI);

                    //get Claim status code for "Open"
                    //first try to get claim status code for short code "O" (Open)
                    try
                    {                        
                        objCodeListPrm.sTableName = "CLAIM_STATUS";                        
                        iClaimStatusCodeOpenDI = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                    }
                    catch
                    {
                    }
                    if (iClaimStatusCodeOpenDI == 0)
                    {
                        iClaimStatusCodeOpenDI = GetStatusCodeID(objCodeListPrm, string.Empty); //get any status code for claim since "Open" code does not exists
                    }

                    //get plan status code for "In Effect"
                    GetPlanStatusCode();
                }
                #endregion

                #region Get State Code List

                objCodeListPrm = new CodeListPrm();
                objCodeListPrm.sTableName = "states";
                objCodeListPrm.sFormName = "claimwc"; //irrelavant for states code list
                objCodeListPrm.sLOBCode = iWCLOBCode.ToString();//irrelavant for states code list
                GetCodeList(objCodeListPrm, ref ilstStateRowID);              

                #endregion

                #region Get DepartmentCode List
                //This will fetch only selected no of records for department. The setting for no of rows is defined in config file.
                GetDepartmentList();
                #endregion

                /*
                #region Get Property Unit List
                //This will fetch only selected no of records for department. The setting for no of rows is defined in config file.
                GetPropertyIdList();
                #endregion
                */
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "CreateCodeListEntities"), ex);
            }
           
        }       

        /// <summary>
        /// Generic function to populate all kinf of CodeList
        /// </summary>
        /// <param name="objCodeListPrm">Struct which containts all values required to populate code list</param>
        /// <param name="ilstCodeList">List which will hold the code list</param>
        private void GetCodeList(CodeListPrm objCodeListPrm, ref List<int> ilstCodeList)
        {
            try
            {
                CodeListType objCodeListType = DBHelper.oCodeListManager.GetCodesNew(0, objCodeListPrm.sTableName, objCodeListPrm.sLOBCode, string.Empty, objCodeListPrm.sFormName, string.Empty, string.Empty, string.Empty, 0, 0, string.Empty, string.Empty);
                List<CodeType> lstCodeType = objCodeListType.Codes;
                if (lstCodeType.Count > 0)
                {
                    ilstCodeList = (from a in lstCodeType
                                    select a.Id).ToList();
                }
                else
                {
                    string ResourceKey = objCodeListPrm.sTableName + ".Missing";
                    throw new RMAppException(Globalization.GetString(ResourceKey));
                }

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetCodeList"), ex);
            }  
        }

        private void GetDepartmentList()
        {
            try
            {
                int iDeptTableID = objHelper.GetTableID("Department");
                string sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE DELETED_FLAG <> -1 AND ENTITY_TABLE_ID=" + iDeptTableID;
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //get No Of Dept count from config file
                        /*
                        ilstDepartmentCode = (from a in ds.Tables[0].AsEnumerable()
                                              select a.Field<int>("ENTITY_ID")).ToList();
                        ilstDepartmentCode = ilstDepartmentCode.Take(20).ToList();*/
                        DataTable dt = ds.Tables[0];
                        if (iDeptCount > dt.Rows.Count)
                            iDeptCount = dt.Rows.Count;

                        for (int i = 0; i < iDeptCount; i++)
                        {
                            ilstDepartmentCode.Add(Conversion.CastToType<int>(dt.Rows[i][0].ToString(), out bConverted));
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("Department.Missing"));
                    }
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("Department.Missing"));
                }
                

        
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetDepartmentList"), ex);
            }
        }
        /*
        public void GetPropertyIdList()
        {
            try
            {
                string sSQL = "SELECT PROPERTY_ID FROM PROPERTY_UNIT";
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (iPropertyount > dt.Rows.Count)
                            iPropertyount = dt.Rows.Count;

                        for (int i = 0; i < iPropertyount; i++)
                        {
                            ilstPropertyIdCode.Add(Conversion.CastToType<int>(dt.Rows[i][0].ToString(), out bConverted));
                        }
                    }
                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        CreatePropertyUnit();//create one property unit in case no property unit exists in database
                        GetPropertyIdList();
                    }
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("Property.Missing"));
                }

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetDepartmentList"), ex);
            }
        }*/

        private void GetPolicyLOBList()
        {
            try
            {
                int iPolicyTableId = objHelper.GetTableID("POLICY_CLAIM_LOB");
                //get policy lob codes for WC LOB
                string sSQL = "SELECT CODE_ID FROM CODES WHERE DELETED_FLAG <> -1 AND TABLE_ID=" + iPolicyTableId +" AND LINE_OF_BUS_CODE = "+iWCLOBCode ;
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ilstPolicyLOB_WC.Add(Conversion.CastToType<int>(ds.Tables[0].Rows[i][0].ToString(), out bConverted));
                        }
                    }                   
                }                

                //get policy lob codes for GC LOB
                sSQL = "SELECT CODE_ID FROM CODES WHERE DELETED_FLAG <> -1 AND TABLE_ID=" + iPolicyTableId + " AND LINE_OF_BUS_CODE = " + iGCLOBCode;
                ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ilstPolicyLOB_GC.Add(Conversion.CastToType<int>(ds.Tables[0].Rows[i][0].ToString(), out bConverted));
                        }
                    }                   
                }               

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetPolicyLOBList"), ex);
            }
        }

        private void GetPlanStatusCode()
        {
            try
            {

                CodeListType objCodeListType = DBHelper.oCodeListManager.GetCodesNew(0, "PLAN_STATUS", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, string.Empty, string.Empty);
                List<CodeType> lstCodeType = objCodeListType.Codes;
                if (lstCodeType.Count > 0)
                {
                    iPlanStatusCode = (int)(from a in lstCodeType
                                       where a.ShortCode == "I"
                                       select a.Id).FirstOrDefault();                    
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("GetPlanStatusCode.Missing"));
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetPlanStatusCode"), ex);
            }
        }

        /*
        /// <summary>
        /// this function populates EmpName from xmldocument. the names are populated as lastname+separator(||*||)+firstname
        /// </summary>
        private void PopulateEmpNameList()
        {
            try
            {
                XDocument xDoc = null;
                try
                {
                    xDoc = XDocument.Load(sEmployeeNameFilePath);
                }
                catch (Exception ex)
                {
                    throw new RMAppException(Globalization.GetString("EmployeeName.InvalidFile") + "." + ex.Message);
                }
                var linq = (from a in xDoc.Descendants("EmployeeName")
                            select (string)a.Element("LastName") + chEmpNameSeparator + (string)a.Element("FirstName"));

                if (linq.Count() > 0)
                    slstEmpName = linq.ToList();
                else
                {
                    throw new RMAppException(Globalization.GetString("EmployeeName.Missing"));
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "PopulateEmpNameList"), ex);
            } 
        }*/


        private string GetEmployeeNameInitials(string FirstName, string LastName)
        {
            string sInitials = string.Empty;
            if (FirstName != string.Empty)
                sInitials = FirstName[0].ToString();
            if (LastName != string.Empty)
                sInitials = sInitials + LastName[0].ToString();
            return sInitials;
        }

        #endregion

        #region Save Random Data

        public void SaveEmployee()
        {
            Employee objEmployee = null;                
            try
            {
                
                //save Process Detail
                SaveProcessDetails(sEmpTableName, iEmpRowCount, 0);
                GetRandomObject(); //create new instances of random class. same instance will be used through out for loop

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();
                iNoOfEmpCreated = 0;
                for (int i = 0; i < iEmpRowCount; i++)
                {
                    try
                    {                        
                        objEmployee = (Employee)(DBHelper.oDataModelFactory.GetDataModelObject("Employee", false));
                        //save employee
                        objEmployee = CreateEmployee();
                        //save process row detail
                        SaveProcessRowDetails(objEmployee.EmployeeNumber, string.Empty, string.Empty, objEmployee.EmployeeNumber, 0, 0);
                        pBar.PerformStep();                       
                        
                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }                    
                    finally
                    {
                        if (objEmployee != null)
                            objEmployee = null;
                    }
                }

                List<string> lstMsg = new List<string>();
                //lstMsg.Add("Employee Batch completed. To run the batch again, close the form and run again.\n");
                //lstMsg.Add("No of employee created : " + iNoOfEmpCreated.ToString());
                //lblStatusBar.Text = lstMsg[0] + lstMsg[1];

                lstMsg.Add(" No of employee created : " + iNoOfEmpCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0];

                UpdateProcessDetails(iProcessID, iNoOfEmpCreated);
                //MessageBox.Show("No Of Employee Created - " + iNoOfEmpCreated.ToString(), "Report"); 
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveEmployee"), ex);
            }
            finally
            {
                if (objEmployee != null)
                    objEmployee = null;
            }
        }

        private Employee CreateEmployee()
        {
            
            try
            {
                #region Get Random EmployeeName
                
                string sFirstName = GetRandomEmployeeName();
                string sLastName = GetRandomEmployeeName();               
                #endregion

                Employee objEmployee = (Employee)(DBHelper.oDataModelFactory.GetDataModelObject("Employee", false));
                objEmployee.EmployeeEntity.EntityTableId = iEmployeeEntityTableId;
                objEmployee.EmployeeEntity.LastName = sLastName;
                objEmployee.EmployeeEntity.FirstName = sFirstName;
                objEmployee.EmployeeEntity.TaxId = GenerateRandomTaxId();
                objEmployee.EmployeeEntity.EntityApprovalStatusCode = iEntityApprovalStatusCode;
                objEmployee.EmployeeEntity.AddedByUser = DBHelper.sUserName;
                objEmployee.EmployeeEntity.DttmRcdAdded = System.DateTime.Now.ToString();
                objEmployee.EmployeeEntity.Abbreviation = GetEmployeeNameInitials(objEmployee.EmployeeEntity.FirstName, objEmployee.EmployeeEntity.LastName);
                objEmployee.EmployeeEid = 0;
                objEmployee.DeptAssignedEid = GetRandomCode(ilstDepartmentCode);
                objEmployee.Save();
                iNoOfEmpCreated++;
                return objEmployee;
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "CreateEmployee"), ex);
            }          
        }

        private PiOther CreatePiOther()
        {
            try
            {
                #region Get Random Entity Name

                string sFirstName = GetRandomEmployeeName();
                string sLastName = GetRandomEmployeeName();

                #endregion
                Entity objEntity = (Entity)(DBHelper.oDataModelFactory.GetDataModelObject("Entity", false));
                objEntity.EntityTableId = iClaimantEntityTableId;
                objEntity.LastName = sLastName;
                objEntity.FirstName = sFirstName;
                objEntity.TaxId = GenerateRandomTaxId();
                objEntity.EntityApprovalStatusCode = iEntityApprovalStatusCode;
                objEntity.AddedByUser = DBHelper.sUserName;
                objEntity.DttmRcdAdded = System.DateTime.Now.ToString();
                objEntity.Abbreviation = GetEmployeeNameInitials(objEntity.FirstName, objEntity.LastName);
                objEntity.Save();

                #region create PiOther

                PiOther objPiOther = (PiOther)(DBHelper.oDataModelFactory.GetDataModelObject("PiOther", false));
                objPiOther.PiEid = objEntity.EntityId;
                return objPiOther;

                #endregion
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "CreatePiOther"), ex);
            }
        }

        public PropertyUnit CreatePropertyUnit()
        {
            try
            {
                using (PropertyUnit objPropertyUnit = (PropertyUnit)(DBHelper.oDataModelFactory.GetDataModelObject("PropertyUnit", false)))
                {
                    objPropertyUnit.Pin = GetRandomEmployeeName();
                    objPropertyUnit.Addr1 = GetRandomEmployeeName();
                    objPropertyUnit.City = GetRandomEmployeeName();
                    objPropertyUnit.StateId = GetRandomCode(ilstStateRowID);
                    objPropertyUnit.ZipCode = GetRandomNumber(5);
                    objPropertyUnit.Save();
                    return objPropertyUnit;
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "CreatePropertyUnit"), ex);
            }
        }
        public void SaveEvent()
        {
            Event objEvent = null;
            iNoOfEventCreated = 0;
            try
            {
                pBar.Step = 1;
                pBar.PerformStep();
                //save Process Detail
                SaveProcessDetails(sEventTableName, iEventRowCount, 0);
                GetRandomObject(); //create new instances of random class. same instance will be used through out for loop

                for (int i = 0; i < iEventRowCount; i++)
                {
                    try
                    {
                        objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false));
                        string sDateReported = GetRandomDate();
                        objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                        objEvent.DateOfEvent = sDateReported;
                        objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                        objEvent.EventStatusCode = iEventStatusCodeOpen;
                        objEvent.DeptEid = GetRandomCode(ilstDepartmentCode);
                        objEvent.AddedByUser = DBHelper.sUserName;
                        objEvent.DateReported = sDateReported;
                        objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                        objEvent.PiList.Add(CreatePiOther());
                        objEvent.Save();
                        iNoOfEventCreated++;
                        //save process row detail
                        //SaveProcessRowDetails(objEvent.EventId, objEvent.EventNumber);
                        SaveProcessRowDetails(objEvent.EventNumber, objEvent.EventNumber, string.Empty, string.Empty, 0, 0);
                        pBar.PerformStep();

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    finally
                    {
                        if (objEvent != null)
                            objEvent = null;
                    }
                }
                
                List<string> lstMsg = new List<string>();
                //lstMsg.Add("Batch completed. To run the batch again, close the form and run again.\n");
                //lstMsg.Add("No of event created : " + iNoOfEventCreated.ToString());
                //lblStatusBar.Text = lstMsg[0] + lstMsg[1];

                lstMsg.Add(" No of event created : " + iNoOfEventCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0];

                UpdateProcessDetails(iProcessID, iNoOfEventCreated);                
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveEvent"), ex);
            }
            finally
            {
                if (objEvent != null)
                    objEvent = null;
            }
        }

        public void SaveWC()
        {
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            iNoOfClaimCreated = 0;
            try
            {                
                GetRandomObject();//create new instances of random class. same instance will be used through out for loop
                
                int iEmpUsedCount = 0;
                int iUseEmpForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iPiRowID = 0;
                int iEventId = 0;                
                string sEventNumber = String.Empty;
                string sDateReported = String.Empty;

                SaveProcessDetails(sWCClaimTableName, iWCRowCount, iWCLOBCode);

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();
                iNoOfEmpCreated = 0;
                if (iWCRowCount < iUseSupportiveChildEntitiesForHowManyRecords)
                    iUseEmpForHowManyClaim = iWCRowCount;
                for (int i = 0; i < iWCRowCount; i++)
                {
                    try
                    {
                        #region create Employee and PiEmployee

                        if (iEmpUsedCount == iUseEmpForHowManyClaim || iEmpUsedCount == 0)
                        {
                            /*
                             use the same employee to create multiple WC claim as per the value defined in iWCClaimPerEmployee.
                             This will prevent from creating new employee for each claim
                             */
                            objEmployee = (Employee)(DBHelper.oDataModelFactory.GetDataModelObject("Employee", false));
                            objEmployee = CreateEmployee();
                            //once the iEmpUsedcount reaches to iWCClaimPerEmployee, then reset it with zero so that a new employeee can be created in next loop
                            iEmpUsedCount = 0;
                        }

                        objPiEmployee = (PiEmployee)(DBHelper.oDataModelFactory.GetDataModelObject("PiEmployee", false));
                        objEmployee.CopyToPiEmployee(objPiEmployee);
                        objPiEmployee.PiEid = objEmployee.EmployeeEid;

                        #endregion

                        #region Create Event and Person Involved

                        using (Event objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false)))
                        {
                            sDateReported = GetRandomDate();
                            objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                            objEvent.DateOfEvent = sDateReported;
                            objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                            objEvent.EventStatusCode = iEventStatusCodeOpen;
                            objEvent.DeptEid = objEmployee.DeptAssignedEid;
                            objEvent.AddedByUser = DBHelper.sUserName;
                            objEvent.DateReported = sDateReported;
                            objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                            objEvent.PiList.Add(objPiEmployee);
                            objEvent.Save();
                            iEmpUsedCount++;
                            using (PersonInvolved objPersonInvolved = objEvent.PiList.GetPiByEID(objEmployee.EmployeeEid))
                            {
                                iPiRowID = objPersonInvolved.PiRowId;
                            }
                            iEventId = objEvent.EventId;
                            sEventNumber = objEvent.EventNumber;
                            sDateReported = objEvent.DateReported;
                        }
                        #endregion

                        #region Create Claim

                        using (Claim objClaim = (Claim)(DBHelper.oDataModelFactory.GetDataModelObject("Claim", false)))
                        {
                            objClaim.EventId = iEventId;
                            objClaim.EventNumber = sEventNumber;
                            objClaim.ClaimId = 0;
                            objClaim.ClaimNumber = string.Empty;
                            objClaim.ClaimStatusCode = iClaimStatusCodeOpenWC; //constant                            
                            objClaim.ClaimTypeCode = GetRandomCode(ilstClaimTypeCodeWC);
                            objClaim.FilingStateId = GetRandomCode(ilstStateRowID);
                            objClaim.AddedByUser = DBHelper.sUserName;
                            objClaim.DateOfClaim = sDateReported;
                            objClaim.TimeOfClaim = System.DateTime.Now.ToShortTimeString();
                            //objClaim.Comments = "";//rupal:commented for r8                            
                            objClaim.LineOfBusCode = iWCLOBCode; //constan;
                            objClaim.CurrencyType = iBaseCurrCode;//added for r8
                            if (ilstPolicyLOB_WC.Count > 0)
                                objClaim.PolicyLOBCode = GetRandomCode(ilstPolicyLOB_WC);
                            objClaim.HookPrimaryPiEmployeeByPiRowId(iPiRowID);
                            #region Create Claimnat
                            using (Claimant objClaimant = (Claimant)(DBHelper.oDataModelFactory.GetDataModelObject("Claimant", false)))
                            {
                                objClaimant.ClaimantEid = objEmployee.EmployeeEid;
                                objClaimant.PrimaryClmntFlag = true;
                                objClaim.ClaimantList.Add(objClaimant);
                            }
                            #endregion
                            
                            objClaim.Save();
                            iNoOfClaimCreated++;
                            //SaveProcessRowDetails(objClaim.ClaimId, objClaim.ClaimNumber);
                            SaveProcessRowDetails(objClaim.ClaimNumber, sEventNumber, objClaim.ClaimNumber, objEmployee.EmployeeNumber, objClaim.PrimaryClaimant.ClaimantRowId, iPiRowID);
                        }
                        pBar.PerformStep();
                        #endregion

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }                    
                }
                
                List<string> lstMsg = new List<string>();
                //lstMsg.Add("Batch completed. To run the batch again, close the form and run again.\n");
                //lstMsg.Add("No of claim created : " + iNoOfClaimCreated.ToString() + "\n");
                //lstMsg.Add("No of employee created : " + iNoOfEmpCreated.ToString() + "\n");
                //lblStatusBar.Text = lstMsg[0] + lstMsg[1] + lstMsg[2];

                lstMsg.Add(" No of WC claim created : " + iNoOfClaimCreated.ToString() + ".");
                lstMsg.Add(" No of employee created for WC : " + iNoOfEmpCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0] + lstMsg[1];
                UpdateProcessDetails(iProcessID, iNoOfClaimCreated);

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveWC"), ex);
            }
            finally
            {
                if (objEmployee != null)
                    objEmployee = null;
                if (objPiEmployee != null)
                    objPiEmployee = null;
            }
        }

        public void SaveGC()
        {
            PiOther objPiOther = null;
            iNoOfClaimCreated = 0;
            try
            {
                
                GetRandomObject();//create new instances of random class. same instance will be used through out for loop               
                int iUseClaimantForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iClaimantUsedCount = 0;
                int iEventId = 0;
                string sEventNumber = String.Empty;
                string sDateReported = String.Empty;

                SaveProcessDetails(sGCClaimTableName, iGCRowCount, iGCLOBCode);

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();

                if (iGCRowCount < iUseSupportiveChildEntitiesForHowManyRecords)
                    iUseClaimantForHowManyClaim = iGCRowCount;
                for (int i = 0; i < iGCRowCount; i++)
                {
                    try
                    {
                        #region create Other People Entity for claimant

                        if (iClaimantUsedCount == iUseClaimantForHowManyClaim || iClaimantUsedCount == 0)
                        {
                            /*
                             use the same claimant to create multiple GC claim as per the value defined in iGCClaimPerClaimant.
                             This will prevent from creating new claimant for each claim
                             */
                            objPiOther = (PiOther)(DBHelper.oDataModelFactory.GetDataModelObject("PiOther", false));
                            objPiOther = CreatePiOther();

                            //once the iClaimantUsedCount reaches to iGCClaimPerClaimant, then reset it with zero so that a new claimant can be created in next loop
                            iClaimantUsedCount = 0;
                        }

                        #endregion

                        #region Create Event and Person Involved

                        using (Event objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false)))
                        {
                            sDateReported = GetRandomDate();
                            objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                            objEvent.DateOfEvent = sDateReported;
                            objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                            objEvent.EventStatusCode = iEventStatusCodeOpen;
                            objEvent.DeptEid = GetRandomCode(ilstDepartmentCode);
                            objEvent.AddedByUser = DBHelper.sUserName;
                            objEvent.DateReported = sDateReported;
                            objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                            objEvent.PiList.Add(objPiOther);
                            objEvent.Save();

                            iEventId = objEvent.EventId;
                            sEventNumber = objEvent.EventNumber;
                            sDateReported = objEvent.DateReported;
                        }
                        #endregion

                        #region Create Claim

                        using (Claim objClaim = (Claim)(DBHelper.oDataModelFactory.GetDataModelObject("Claim", false)))
                        {
                            objClaim.EventId = iEventId;
                            objClaim.EventNumber = sEventNumber;
                            objClaim.ClaimId = 0;
                            objClaim.ClaimNumber = string.Empty;
                            objClaim.ClaimStatusCode = iClaimStatusCodeOpenGC; //constant                            
                            objClaim.ClaimTypeCode = GetRandomCode(ilstClaimTypeCodeGC);
                            objClaim.FilingStateId = GetRandomCode(ilstStateRowID);
                            objClaim.AddedByUser = DBHelper.sUserName;
                            objClaim.DateOfClaim = sDateReported;
                            objClaim.TimeOfClaim = System.DateTime.Now.ToShortTimeString();
                            //objClaim.Comments = "";//rupal:commented for r8                            
                            objClaim.LineOfBusCode = iGCLOBCode; //constan; 
                            objClaim.CurrencyType = iBaseCurrCode;
                            if (ilstPolicyLOB_GC.Count > 0)
                                objClaim.PolicyLOBCode = GetRandomCode(ilstPolicyLOB_GC);
                            #region Create Claimnat
                            using (Claimant objClaimant = (Claimant)(DBHelper.oDataModelFactory.GetDataModelObject("Claimant", false)))
                            {
                                objClaimant.ClaimantEid = objPiOther.PiEid;
                                objClaimant.PrimaryClmntFlag = true;
                                objClaim.ClaimantList.Add(objClaimant);
                                iClaimantUsedCount++;
                            }
                            #endregion

                            objClaim.Save();
                            iNoOfClaimCreated++;
                            //SaveProcessRowDetails(objClaim.ClaimId, objClaim.ClaimNumber);
                            SaveProcessRowDetails(objClaim.ClaimNumber, sEventNumber, objClaim.ClaimNumber, string.Empty, objClaim.PrimaryClaimant.ClaimantRowId, objPiOther.PiRowId);
                        }
                        pBar.PerformStep();
                        #endregion

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }                    
                }
                
                List<string> lstMsg = new List<string>();
                //lstMsg.Add("Batch completed. To run the batch again, close the form and run again.\n");
                //lstMsg.Add("No of claim created : " + iNoOfClaimCreated.ToString() + "\n");
                //lblStatusBar.Text = lstMsg[0] + lstMsg[1];
                
                lstMsg.Add(" No of General Claim created : " + iNoOfClaimCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0];
                UpdateProcessDetails(iProcessID, iNoOfClaimCreated);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveGC"), ex);
            }
            finally
            {
                if (objPiOther != null)
                    objPiOther = null;
            }
        }

        public void SavePC()
        {
            PiOther objPiOther = null;
            PropertyUnit objPropertyUnit = null;
            iNoOfClaimCreated = 0;
            try
            {                
                GetRandomObject();//create new instances of random class. same instance will be used through out for loop               
                int iUseClaimantForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iUsePropertyForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iClaimantUsedCount = 0;
                int iPropertyUsedCount = 0;
                int iEventId = 0;
                string sEventNumber = String.Empty;
                string sDateReported = String.Empty;

                SaveProcessDetails(sGCClaimTableName, iPCRowCount, iPCLOBCode);

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();
                if (iPCRowCount < iUseSupportiveChildEntitiesForHowManyRecords)
                {
                    iUseClaimantForHowManyClaim = iPCRowCount;
                    iUsePropertyForHowManyClaim = iPCRowCount;
                }
                for (int i = 0; i < iPCRowCount; i++)
                {
                    try
                    {
                       
                        #region create Other People Entity for claimant

                        if (iClaimantUsedCount == iUseClaimantForHowManyClaim || iClaimantUsedCount == 0)
                        {
                            /*
                             use the same claimant to create multiple GC claim as per the value defined in iGCClaimPerClaimant.
                             This will prevent from creating new claimant for each claim
                             */
                            objPiOther = (PiOther)(DBHelper.oDataModelFactory.GetDataModelObject("PiOther", false));
                            objPiOther = CreatePiOther();

                            //once the iClaimantUsedCount reaches to iGCClaimPerClaimant, then reset it with zero so that a new claimant can be created in next loop
                            iClaimantUsedCount = 0;
                        }

                        #endregion

                        #region Create Event and Person Involved

                        using (Event objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false)))
                        {
                            sDateReported = GetRandomDate();
                            objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                            objEvent.DateOfEvent = sDateReported;
                            objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                            objEvent.EventStatusCode = iEventStatusCodeOpen;
                            objEvent.DeptEid = GetRandomCode(ilstDepartmentCode);
                            objEvent.AddedByUser = DBHelper.sUserName;
                            objEvent.DateReported = sDateReported;
                            objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                            objEvent.PiList.Add(objPiOther);
                            objEvent.Save();

                            iEventId = objEvent.EventId;
                            sEventNumber = objEvent.EventNumber;
                            sDateReported = objEvent.DateReported;
                        }
                        #endregion

                        #region Create Claim

                        using (Claim objClaim = (Claim)(DBHelper.oDataModelFactory.GetDataModelObject("Claim", false)))
                        {
                            objClaim.EventId = iEventId;
                            objClaim.EventNumber = sEventNumber;
                            objClaim.ClaimId = 0;
                            objClaim.ClaimNumber = string.Empty;
                            objClaim.ClaimStatusCode = iClaimStatusCodeOpenPC; //constant                            
                            objClaim.ClaimTypeCode = GetRandomCode(ilstClaimTypeCodePC);
                            objClaim.FilingStateId = GetRandomCode(ilstStateRowID);
                            objClaim.AddedByUser = DBHelper.sUserName;
                            objClaim.DateOfClaim = sDateReported;
                            objClaim.TimeOfClaim = System.DateTime.Now.ToShortTimeString();
                            //objClaim.Comments = "";//rupal:commented for r8                            
                            objClaim.LineOfBusCode = iPCLOBCode; //constan; 
                            objClaim.CurrencyType = iBaseCurrCode;
                            
                            #region Create Claimnat
                            using (Claimant objClaimant = (Claimant)(DBHelper.oDataModelFactory.GetDataModelObject("Claimant", false)))
                            {
                                objClaimant.ClaimantEid = objPiOther.PiEid;
                                objClaimant.PrimaryClmntFlag = true;
                                objClaim.ClaimantList.Add(objClaimant);
                                iClaimantUsedCount++;
                            }
                            #endregion

                            #region Create Property Info
                            if (iPropertyUsedCount == iUsePropertyForHowManyClaim || iPropertyUsedCount == 0)
                            {
                                //using (PropertyUnit objPropertyUnit = (PropertyUnit)(DBHelper.oDataModelFactory.GetDataModelObject("PropertyUnit", false)))
                                objPropertyUnit = (PropertyUnit)(DBHelper.oDataModelFactory.GetDataModelObject("PropertyUnit", false));
                                objPropertyUnit = CreatePropertyUnit();
                                //objPropertyUnit.MoveTo(GetRandomCode(ilstPropertyIdCode));
                                iPropertyUsedCount = 0;
                            }
                                using (ClaimXProperty objClaimXProperty = (ClaimXProperty)(DBHelper.oDataModelFactory.GetDataModelObject("ClaimXProperty", false)))
                                {
                                    objClaimXProperty.PropertyID = objPropertyUnit.PropertyId;
                                    objClaimXProperty.Addr1 = objPropertyUnit.Addr1;
                                    objClaimXProperty.City = objPropertyUnit.City;
                                    objClaimXProperty.StateId = objPropertyUnit.StateId;
                                    objClaimXProperty.ZipCode = objPropertyUnit.ZipCode;
                                    objClaimXProperty.Save();
                                    objClaim.PropRowId = objClaimXProperty.PropRowID;
                                iPropertyUsedCount++;
                                }

                            
                            #endregion

                            objClaim.Save();
                            iNoOfClaimCreated++;
                            //SaveProcessRowDetails(objClaim.ClaimId, objClaim.ClaimNumber);
                            SaveProcessRowDetails(objClaim.ClaimNumber, sEventNumber, objClaim.ClaimNumber, string.Empty, objClaim.PrimaryClaimant.ClaimantRowId, objPiOther.PiRowId);
                        }
                        pBar.PerformStep();
                        #endregion

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }                   
                }
                
                List<string> lstMsg = new List<string>();
                //lstMsg.Add("Batch completed. To run the batch again, close the form and run again.\n");
                //lstMsg.Add("No of claim created : " + iNoOfClaimCreated.ToString() + "\n");
                //lblStatusBar.Text = lstMsg[0] + lstMsg[1];

                lstMsg.Add(" No of Property Claim created : " + iNoOfClaimCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0];
                UpdateProcessDetails(iProcessID, iNoOfClaimCreated);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveGC"), ex);
            }
            finally
            {
                if (objPiOther != null)
                    objPiOther = null;
                if (objPropertyUnit != null)
                    objPropertyUnit = null;
            }
        }

        public void SaveVA()
        {
            iNoOfClaimCreated = 0;
            PiOther objPiOther = null;
            try
            {
                //int iRowCount = Conversion.CastToType<int>(txtVA.Text.Trim(), out bSuccess);
                GetRandomObject();//create new instances of random class. same instance will be used through out for loop               
                int iUseClaimantForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iClaimantUsedCount = 0;
                int iEventId = 0;
                string sEventNumber = String.Empty;
                string sDateReported = String.Empty;
                iUseClaimantForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                SaveProcessDetails(sGCClaimTableName, iVARowCount, iVALOBCode);

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();
                if (iVARowCount < iUseSupportiveChildEntitiesForHowManyRecords)
                    iUseClaimantForHowManyClaim = iGCRowCount;
                for (int i = 0; i < iVARowCount; i++)
                {
                    try
                    {                       
                        #region create Other People Entity for claimant

                        if (iClaimantUsedCount == iUseClaimantForHowManyClaim || iClaimantUsedCount == 0)
                        {
                            /*
                             use the same claimant to create multiple GC claim as per the value defined in iGCClaimPerClaimant.
                             This will prevent from creating new claimant for each claim
                             */
                            objPiOther = (PiOther)(DBHelper.oDataModelFactory.GetDataModelObject("PiOther", false));
                            objPiOther = CreatePiOther();

                            //once the iClaimantUsedCount reaches to iGCClaimPerClaimant, then reset it with zero so that a new claimant can be created in next loop
                            iClaimantUsedCount = 0;
                        }

                        #endregion

                        #region Create Event and Person Involved

                        using (Event objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false)))
                        {
                            sDateReported = GetRandomDate();
                            objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                            objEvent.DateOfEvent = sDateReported;
                            objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                            objEvent.EventStatusCode = iEventStatusCodeOpen;
                            objEvent.DeptEid = GetRandomCode(ilstDepartmentCode);
                            objEvent.AddedByUser = DBHelper.sUserName;
                            objEvent.DateReported = sDateReported;
                            objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                            objEvent.PiList.Add(objPiOther);
                            objEvent.Save();

                            iEventId = objEvent.EventId;
                            sEventNumber = objEvent.EventNumber;
                            sDateReported = objEvent.DateReported;
                        }
                        #endregion

                        #region Create Claim

                        using (Claim objClaim = (Claim)(DBHelper.oDataModelFactory.GetDataModelObject("Claim", false)))
                        {
                            objClaim.EventId = iEventId;
                            objClaim.EventNumber = sEventNumber;
                            objClaim.ClaimId = 0;
                            objClaim.ClaimNumber = string.Empty;
                            objClaim.ClaimStatusCode = iClaimStatusCodeOpenVA; //constant                            
                            objClaim.ClaimTypeCode = GetRandomCode(ilstClaimTypeCodeVA);
                            objClaim.FilingStateId = GetRandomCode(ilstStateRowID);
                            objClaim.AddedByUser = DBHelper.sUserName;
                            objClaim.DateOfClaim = sDateReported;
                            objClaim.TimeOfClaim = System.DateTime.Now.ToShortTimeString();
                            //objClaim.Comments = "";//rupal:commented for r8                            
                            objClaim.LineOfBusCode = iVALOBCode; //constan; 
                            objClaim.CurrencyType = iBaseCurrCode;

                            #region Create Claimnat
                            using (Claimant objClaimant = (Claimant)(DBHelper.oDataModelFactory.GetDataModelObject("Claimant", false)))
                            {
                                objClaimant.ClaimantEid = objPiOther.PiEid;
                                objClaimant.PrimaryClmntFlag = true;
                                objClaim.ClaimantList.Add(objClaimant);
                                iClaimantUsedCount++;
                            }
                            #endregion                          

                            objClaim.Save();
                            iNoOfClaimCreated++;
                            //SaveProcessRowDetails(objClaim.ClaimId, objClaim.ClaimNumber);
                            SaveProcessRowDetails(objClaim.ClaimNumber, sEventNumber, objClaim.ClaimNumber, string.Empty, objClaim.PrimaryClaimant.ClaimantRowId, objPiOther.PiRowId);
                        }
                        pBar.PerformStep();
                        #endregion

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    }

                
                List<string> lstMsg = new List<string>();
                lstMsg.Add(" No of Vehical Accident Claim created : " + iNoOfClaimCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0];
                UpdateProcessDetails(iProcessID, iNoOfClaimCreated);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveGC"), ex);
            }           
            finally
            {
                if (objPiOther != null)
                    objPiOther = null;
            }
        }

        public void SaveNonOcc()
        {
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            DisabilityPlan objDisabilityPlan = null;
            iNoOfClaimCreated = 0;
            try
            {
                //int iRowCount = Conversion.CastToType<int>(txtNonOcc.Text.Trim(), out bSuccess);
                GetRandomObject();//create new instances of random class. same instance will be used through out for loop

                int iEmpUsedCount = 0;
                Hashtable hsDeptIDPlanID = new Hashtable();
                int iUseEmpForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iUsePlanForHowManyClaim = iUseSupportiveChildEntitiesForHowManyRecords;
                int iPiRowID = 0;
                int iEventId = 0;
                string sEventNumber = String.Empty;
                string sDateReported = String.Empty;
                int iDeptEID = 0;

                SaveProcessDetails(sWCClaimTableName, iNonOccRowCount, iNonOccLOBCode);

                //progress bar setting
                //pBar.Visible = true;
                //pBar.Minimum = 1;
                //pBar.Maximum = iRowCount;
                pBar.Step = 1;
                pBar.PerformStep();
                iNoOfEmpCreated = 0;
                if (iNonOccRowCount < iUseSupportiveChildEntitiesForHowManyRecords)
                {
                    iUseEmpForHowManyClaim = iNonOccRowCount;
                    iUsePlanForHowManyClaim = iNonOccRowCount;
                }
                for (int i = 0; i < iNonOccRowCount; i++)
                {
                    try
                    {
                        #region create Employee and PiEmployee

                        if (iEmpUsedCount == iUseEmpForHowManyClaim || iEmpUsedCount == 0)
                        {
                            /*
                             use the same employee to create multiple WC claim as per the value defined in iWCClaimPerEmployee.
                             This will prevent from creating new employee for each claim
                             */
                            objEmployee = (Employee)(DBHelper.oDataModelFactory.GetDataModelObject("Employee", false));
                            objEmployee = CreateEmployee();
                            //once the iEmpUsedcount reaches to iWCClaimPerEmployee, then reset it with zero so that a new employeee can be created in next loop
                            iEmpUsedCount = 0;
                        }

                        objPiEmployee = (PiEmployee)(DBHelper.oDataModelFactory.GetDataModelObject("PiEmployee", false));
                        objEmployee.CopyToPiEmployee(objPiEmployee);
                        objPiEmployee.PiEid = objEmployee.EmployeeEid;

                        #endregion

                        #region Create Event and Person Involved

                        using (Event objEvent = (Event)(DBHelper.oDataModelFactory.GetDataModelObject("Event", false)))
                        {
                            sDateReported = GetRandomDate();
                            objEvent.EventTypeCode = GetRandomCode(ilstEventTypeCode); //random
                            objEvent.DateOfEvent = sDateReported;
                            objEvent.TimeOfEvent = System.DateTime.Now.ToShortTimeString();
                            objEvent.EventStatusCode = iEventStatusCodeOpen;
                            objEvent.DeptEid = objEmployee.DeptAssignedEid;
                            objEvent.AddedByUser = DBHelper.sUserName;
                            objEvent.DateReported = sDateReported;
                            objEvent.TimeReported = System.DateTime.Now.ToShortTimeString();
                            objEvent.PiList.Add(objPiEmployee);
                            objEvent.Save();
                            iEmpUsedCount++;
                            using (PersonInvolved objPersonInvolved = objEvent.PiList.GetPiByEID(objEmployee.EmployeeEid))
                            {
                                iPiRowID = objPersonInvolved.PiRowId;
                            }
                            iEventId = objEvent.EventId;
                            sEventNumber = objEvent.EventNumber;
                            sDateReported = objEvent.DateReported;
                            iDeptEID=objEvent.DeptEid;
                        }
                        #endregion

                        #region Create Claim

                        using (Claim objClaim = (Claim)(DBHelper.oDataModelFactory.GetDataModelObject("Claim", false)))
                        {
                            objClaim.EventId = iEventId;
                            objClaim.EventNumber = sEventNumber;
                            objClaim.ClaimId = 0;
                            objClaim.ClaimNumber = string.Empty;
                            objClaim.ClaimStatusCode = iClaimStatusCodeOpenDI; //constant                            
                            objClaim.ClaimTypeCode = GetRandomCode(ilstClaimTypeCodeDI);
                            objClaim.FilingStateId = GetRandomCode(ilstStateRowID);
                            objClaim.AddedByUser = DBHelper.sUserName;
                            objClaim.DateOfClaim = sDateReported;
                            objClaim.TimeOfClaim = System.DateTime.Now.ToShortTimeString();                                                    
                            objClaim.LineOfBusCode = iNonOccLOBCode; //constan;
                            objClaim.CurrencyType = iBaseCurrCode;//added for r8    
                            objClaim.DisabilFromDate = GetRandomDate();
                            objClaim.HookPrimaryPiEmployeeByPiRowId(iPiRowID);
                            
                            #region Create Claimnat
                            using (Claimant objClaimant = (Claimant)(DBHelper.oDataModelFactory.GetDataModelObject("Claimant", false)))
                            {
                                objClaimant.ClaimantEid = objEmployee.EmployeeEid;
                                objClaimant.PrimaryClmntFlag = true;
                                objClaim.ClaimantList.Add(objClaimant);
                            }
                            #endregion

                            #region Attatch Plan
                            if (hsDeptIDPlanID[iDeptEID] != null)
                            {
                                objClaim.PlanId = (int)hsDeptIDPlanID[iDeptEID];
                            }
                            else                            
                            {
                                objDisabilityPlan = (DisabilityPlan)(DBHelper.oDataModelFactory.GetDataModelObject("DisabilityPlan", false));
                                objDisabilityPlan.PlanNumber = GetRandomEmployeeName();//get any randon name for plan
                                objDisabilityPlan.PlanName = objDisabilityPlan.PlanNumber;
                                objDisabilityPlan.PlanStatusCode = iPlanStatusCode;
                                objDisabilityPlan.EffectiveDate = GetRandomDate();
                                objDisabilityPlan.ExpirationDate = GetRandomDate();
                                objDisabilityPlan.InsuredList.Add(iDeptEID);
                                objDisabilityPlan.Save();
                                hsDeptIDPlanID.Add(iDeptEID, objDisabilityPlan.PlanId);
                                objClaim.PlanId = objDisabilityPlan.PlanId;
                            }
                            #endregion

                            objClaim.Save();
                            iNoOfClaimCreated++;
                            //SaveProcessRowDetails(objClaim.ClaimId, objClaim.ClaimNumber);
                            SaveProcessRowDetails(objClaim.ClaimNumber, sEventNumber, objClaim.ClaimNumber, objEmployee.EmployeeNumber, objClaim.PrimaryClaimant.ClaimantRowId, iPiRowID);
                        }
                        pBar.PerformStep();
                        #endregion

                    }
                    catch (RMAppException ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }
                    catch (Exception ex)
                    {
                        //we don't want to come out from this for loop. if this loop failed to complete, we will move to next loop
                        objHelper.SaveExceptionToDisk(ex);
                    }                   
                }
                
                List<string> lstMsg = new List<string>();
                lstMsg.Add(" No of Non-Occ claim created : " + iNoOfClaimCreated.ToString() + ".");
                lstMsg.Add(" No of employee created for Non-Occ Claims: " + iNoOfEmpCreated.ToString() + "\n");
                lblStatusBar.Text = lblStatusBar.Text + lstMsg[0] + lstMsg[1];
                UpdateProcessDetails(iProcessID, iNoOfClaimCreated);

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveWC"), ex);
            }
            finally
            {
                if (objEmployee != null)
                    objEmployee = null;
                if (objPiEmployee != null)
                    objPiEmployee = null;
                if (objDisabilityPlan != null)
                    objDisabilityPlan = null;
            }
        }

        public void SaveProcessDetails(string sTableName,int iRowCount,int iLOBCode)
        {
            DbWriter odbWriter=null;
            try
            {

                int RowCount = 0;
                iProcessID = GetNextUniqueID("BatchDetails");
                odbWriter = DbFactory.GetDbWriter(DBHelper.sConnectionString);
                odbWriter.Tables.Add("BatchDetails");
                odbWriter.Fields.Add("BatchID", iProcessID);
                odbWriter.Fields.Add("TableID", objHelper.GetTableID(sTableName));
                odbWriter.Fields.Add("BatchStartDate", System.DateTime.Now.ToShortDateString());
                odbWriter.Fields.Add("BatchStartTime", System.DateTime.Now.ToShortTimeString());
                odbWriter.Fields.Add("BatchEndDate", System.DateTime.Now.ToShortDateString());
                odbWriter.Fields.Add("BatchEndTime", System.DateTime.Now.ToShortTimeString());
                odbWriter.Fields.Add("AddedByUser", DBHelper.sUserName);
                odbWriter.Fields.Add("ConnectionString", DBHelper.sConnectionString);
                odbWriter.Fields.Add("LOB", iLOBCode);
                odbWriter.Fields.Add("Remarks", "Radom Data Generator EXE");
                odbWriter.Fields.Add("NoRowsEntered", iRowCount);
                odbWriter.Fields.Add("NoRowsCreated", -1);

                RowCount = odbWriter.Execute();
                ilstProcessIDs.Add(iProcessID);
                if (RowCount == 0)
                {
                    throw new RMAppException(Globalization.GetString("ProcessDetails.Save.Error"));
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveProcessDetails"), ex);
            }
            finally
            {
                odbWriter = null;
            }
        }       
        /*
        public void SaveProcessRowDetails(int iRowID, string sRowNumber)
        {
            DbWriter odbWriter = null;
            try
            {
                odbWriter = DbFactory.GetDbWriter(DBHelper.sConnectionString);
                odbWriter.Tables.Add("ProcessRowDetails");
                odbWriter.Fields.Add("ProcessRowID", GetNextUniqueID("ProcessRowDetails"));
                odbWriter.Fields.Add("ProcessID", iProcessID);
                odbWriter.Fields.Add("PRowID", iRowID);
                odbWriter.Fields.Add("PRowNumber", sRowNumber);
                odbWriter.Execute();
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveProcessRowDetails"), ex);
            }
            finally
            {
                odbWriter = null;
            }
        }
        */
        public void SaveProcessRowDetails(string sProcessRowNumber, string sEventNumber, string sClaimNumber, string sEmpNumber, int iClaimantRowId, int iPiRowId)
        {
            DbWriter odbWriter = null;
            try
            {
                odbWriter = DbFactory.GetDbWriter(DBHelper.sConnectionString);
                odbWriter.Tables.Add("BatchRowDetails");
                odbWriter.Fields.Add("BatchRowID", GetNextUniqueID("BatchRowDetails"));
                odbWriter.Fields.Add("BatchID", iProcessID);
                odbWriter.Fields.Add("BatchRowNumber", sProcessRowNumber);
                odbWriter.Fields.Add("EventNumber", sClaimNumber);
                odbWriter.Fields.Add("ClaimNumber", sClaimNumber);
                odbWriter.Fields.Add("EmpNumber", sEmpNumber);
                odbWriter.Fields.Add("ClaimantRowId", iClaimantRowId);
                odbWriter.Fields.Add("PiRowId", iPiRowId);
                odbWriter.Execute();
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "SaveProcessRowDetails"), ex);
            }
            finally
            {
                odbWriter = null;
            }
        }

        public int  GetNextUniqueID(string sTableName)
        {
            try
            {
                return Utilities.GetNextUID(DBHelper.sConnectionString, sTableName);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(objHelper.GetErrorMessage(ex, sExceptionKey, "GetNextUniqueID"), ex);
            } 
        }

        public void UpdateProcessDetails(int iProcessID, int iNoOfRowsCreated)
        {
            DbWriter odbWriter = null;
            try
            {
                //using (odbWriter = DbFactory.GetDbWriter(DBHelper.sConnectionString))
                //{
                int RowCount = 0;              
                odbWriter = DbFactory.GetDbWriter(DBHelper.sConnectionString);
                odbWriter.Tables.Add("BatchDetails");                
                odbWriter.Fields.Add("NoRowsCreated", iNoOfRowsCreated);
                odbWriter.Fields.Add("BatchEndTime", System.DateTime.Now.ToShortTimeString());
                odbWriter.Where.Add("BatchID = " + iProcessID.ToString());

                RowCount = odbWriter.Execute();
                if (RowCount == 0)
                {
                    throw new RMAppException(Globalization.GetString("UpdateProcessDetails.Error"));
                }
                //}
            }
            catch (RMAppException ex)
            {
                objHelper.SaveExceptionToDisk(ex);
            }
            catch (Exception ex)
            {
                objHelper.SaveExceptionToDisk(ex);
            }
            finally
            {
                odbWriter = null;
            }
        }       

        #endregion

        #region Random Data Generator Functions

        private void GetRandomObject()
        {            
            objRandom = new Random();
        }

        /// <summary>
        /// generic function to get random code for list of Integers(CodeIDs)
        /// </summary>
        /// <param name="objList">Integer List of CodeIds</param>
        /// <returns></returns>
        private int GetRandomCode(List<int> objList)
        {
            try
            {
                int iCode = 0;
                if (objList != null)
                {
                    //Random objRandom = new Random();
                    iCode = objList[objRandom.Next(0, objList.Count - 1)];
                }
                return iCode;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("form1.GetRandomCode.Error") + "." + ex.Message);
            }
        }

        /// <summary>
        /// this function returns employee name from XML file
        /// </summary>
        /// <returns>returns the name in this format : lastname+|+firstname. i.e john|paul</returns>
        private string GetRandomEmployeeNameFromXMLFile()
        {
            try
            {
                return slstEmpName[objRandom.Next(0, slstEmpName.Count - 1)];
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("form1.GetRandomEmployeeNameFromXMLFile.Error") + "." + ex.Message);
            }
        }

        /// <summary>
        /// This function will generate any random employee name from alphabets. 
        /// </summary>
        /// <returns>return only name which can be used as firstname or lastname</returns>
        private string GetRandomEmployeeName()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < iEmpNameLength; i++)
                {
                    sb.Append(sAlphabets.Substring(objRandom.Next(0, sAlphabets.Length - 1), 1));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("form1.GetRandomEmployeeName.Error") + "." + ex.Message);
            }
        }

        private string GetRandomNumber(int iLength)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < iLength; i++)
                {
                    sb.Append(sNumbers.Substring(objRandom.Next(0, iLength - 1), 1));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("form1.GetRandomNumber.Error") + "." + ex.Message);
            }
        }
        private string GenerateRandomTaxId()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < 3; i++)
                {
                    sb.Append(sNumbers.Substring(objRandom.Next(0, sNumbers.Length - 1), 1));
                }
                sb.Append("-");
                for (int i = 0; i < 2; i++)
                {
                    sb.Append(sNumbers.Substring(objRandom.Next(0, sNumbers.Length - 1), 1));
                }
                sb.Append("-");
                for (int i = 0; i < 4; i++)
                {
                    sb.Append(sNumbers.Substring(objRandom.Next(0, sNumbers.Length - 1), 1));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("form1.GenerateRandomTaxId.Error") + "." + ex.Message);
            }
        }

        private string GetRandomDate()
        {            
            return dtFromDate.AddDays(objRandom.Next(GetRangeDays())).ToString();
        }

        private int GetRangeDays()
        {
            DateTime dtFromDate = Convert.ToDateTime(dtpFromDate.Text);
            DateTime dtToDate = Convert.ToDateTime(dtpToDate.Text);
            int range = ((TimeSpan)(dtToDate - dtFromDate)).Days;
            return range;
        }
       
        #endregion       

        #region Form Events
        /*
        private void btnGenerate_Click(object sender, EventArgs e)
        {           
            try
            {
                #region validation
                if (rdbEmp.Checked == false && rdbWC.Checked == false && rdbGC.Checked == false && rdbEvent.Checked == false)
                {
                    MessageBox.Show("Please select any option.", "Input missing");
                    return;
                }
                if (txtRowCount.Text == "")
                {
                    MessageBox.Show("Please enter no of records.", "Input missing");
                    txtRowCount.Focus();
                    return;
                }
                #endregion
                
                if (MessageBox.Show("Are you sure you want to generate records?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Populate code list or any kind of list from which we will get random records
                    CreateCodeListEntities();
                    bool bSuccess = false;
                    iRowCount = Conversion.CastToType<int>(txtRowCount.Text.Trim(), out bSuccess);
                    dtFromDate = Convert.ToDateTime(dtpFromDate.Text);
                    if (rdbEmp.Checked == true)
                    {
                        EnableButtons(false);
                        SaveEmployee();
                        EnableButtons(true);
                        btnGenerate.Enabled = false;
                        //enable view report button only after succesful execution
                        btnViewReport.Enabled = true;      
                        //grpBoxButton.Enabled = true;
                    }
                    else if (rdbWC.Checked == true)
                    {
                        #region Get Claim_Status Code for "Open"
                        objCodeListPrm = new CodeListPrm();
                        objCodeListPrm.sTableName = "CLAIM_STATUS";
                        objCodeListPrm.sFormName = "claimwc";
                        objCodeListPrm.sLOBCode = iWCLOBCode.ToString();
                        iClaimStatusCodeOpen = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                        #endregion
                        EnableButtons(false);            
                        SaveWC();
                        EnableButtons(true);
                        //enable view report button only after succesful execution
                        btnViewReport.Enabled = true;      
                        btnGenerate.Enabled = false;
                    }
                    else if (rdbGC.Checked == true)
                    {
                        #region Get Claim_Status Code for "Open"
                        objCodeListPrm = new CodeListPrm();
                        objCodeListPrm.sTableName = "CLAIM_STATUS";
                        objCodeListPrm.sFormName = "claimgc";
                        objCodeListPrm.sLOBCode = iGCLOBCode.ToString();
                        iClaimStatusCodeOpen = GetStatusCodeID(objCodeListPrm, "O"); //"O" for Open
                        #endregion
                        EnableButtons(false);
                        SaveGC();
                        EnableButtons(true);
                        //enable view report button only after succesful execution
                        btnViewReport.Enabled = true;
                        btnGenerate.Enabled = false;
                    }
                    else if (rdbEvent.Checked == true)
                    {
                        EnableButtons(false);
                        SaveEvent();
                        EnableButtons(true);
                        //enable view report button only after succesful execution
                        btnViewReport.Enabled = true;
                        btnGenerate.Enabled = false;
                    }
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
        }
        */

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                #region validation
                if (chkEmp.Checked == false && chkEvent.Checked == false && chkWC.Checked == false && chkGC.Checked == false && chkVA.Checked == false && chkPC.Checked == false && chkNonOccc.Checked == false)
                {
                    MessageBox.Show("Please select any option.", "Input missing");
                    return;
                }
                if (chkEmp.Checked && txtEmp.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Employee.", "Input missing");
                    txtEmp.Focus();
                    return;
                }
                else if (chkEmp.Checked)
                {
                    iEmpRowCount = Conversion.CastToType<int>(txtEmp.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iEmpRowCount;
                }
                if (chkEvent.Checked && txtEvent.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Event.", "Input missing");
                    txtEvent.Focus();
                    return;
                }
                else if (chkEvent.Checked)
                {
                    iEventRowCount = Conversion.CastToType<int>(txtEvent.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iEventRowCount;
                }
                if (chkWC.Checked && txtWC.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Worker's Compensation Claim.", "Input missing");
                    txtWC.Focus();
                    return;
                }
                else  if (chkWC.Checked)
                {
                    iWCRowCount = Conversion.CastToType<int>(txtWC.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iWCRowCount;
                }
                if (chkGC.Checked && txtGC.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for General Claim.", "Input missing");
                    txtGC.Focus();
                    return;
                }
                else if (chkGC.Checked)
                {
                    iGCRowCount = Conversion.CastToType<int>(txtGC.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iGCRowCount;
                }
                if (chkVA.Checked && txtVA.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Vehicle Accident Claim.", "Input missing");
                    txtVA.Focus();
                    return;
                }
                else if (chkVA.Checked)
                {
                    iVARowCount = Conversion.CastToType<int>(txtVA.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iVARowCount;
                }
                if (chkPC.Checked && txtPC.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Peroperty Claim.", "Input missing");
                    txtPC.Focus();
                    return;
                }
                else if (chkPC.Checked)
                {
                    iPCRowCount = Conversion.CastToType<int>(txtPC.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iPCRowCount;
                }
                if (chkNonOccc.Checked && txtNonOcc.Text == string.Empty)
                {
                    MessageBox.Show("Please enter no of records for Non-Occ Claim.", "Input missing");
                    txtNonOcc.Focus();
                    return;
                }
                else if (chkNonOccc.Checked)
                {
                    iNonOccRowCount = Conversion.CastToType<int>(txtNonOcc.Text.Trim(), out bSuccess);
                    iTotalCount = iTotalCount + iNonOccRowCount;
                }
                #endregion

                if (MessageBox.Show("Are you sure you want to generate records?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //progress bar setting
                    pBar.Visible = true;
                    pBar.Minimum = 1;
                    pBar.Maximum = iTotalCount;
                    pBar.Step = 1;
                    pBar.PerformStep();
                    //Populate code list or any kind of list from which we will get random records
                    CreateCodeListEntities();                    
                    
                    dtFromDate = Convert.ToDateTime(dtpFromDate.Text);
                    dtToDate = Convert.ToDateTime(dtpToDate.Text);
                    EnableButtons(false);
                    if (chkEvent.Checked == true)
                    {
                        SaveEvent();
                    }
                    if (chkEmp.Checked == true)
                    {   
                        SaveEmployee();
                    }
                    if (chkWC.Checked == true)
                    {                      
                        SaveWC();
                    }
                    if (chkGC.Checked == true)
                    {  
                        SaveGC();                                               
                    }
                    if (chkPC.Checked == true)
                    {
                        SavePC();                                                                    
                    }
                    if (chkVA.Checked == true)
                    {                     
                        SaveVA();                        
                    }
                    if (chkNonOccc.Checked == true)
                    {                     
                        SaveNonOcc();                                                
                    }                    
                    
                    EnableButtons(true);
                    btnGenerate.Enabled = false;
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                //BatchRowDetails objBatchRowDetails = new BatchRowDetails(iProcessID);
                //objBatchRowDetails.ShowDialog();
                BatchList objBatchList = new BatchList(ilstProcessIDs);
                objBatchList.ShowDialog();
                //objBatchRowDetails.Show();
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }

        }    

        private void txtRowCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region Valiate Numeric
            NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            string groupSeparator = numberFormatInfo.NumberGroupSeparator;
            string negativeSign = numberFormatInfo.NegativeSign;

            string keyInput = e.KeyChar.ToString();

            if (Char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }           
            else if (this.allowSpace && e.KeyChar == ' ')
            {

            }
            else
            {
                // Consume this invalid key and beep
                e.Handled = true;
            }
            #endregion
        }                      

        private void EnableButtons(bool enable)
        {
            btnGenerate.Enabled = enable;
            btnViewReport.Enabled = enable;
            btnClose.Enabled = enable;
        }

        private void txtEvent_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtEmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtWC_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtGC_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtPC_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtVA_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void txtNonOcc_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateNumeric(e);
        }

        private void ValidateNumeric(KeyPressEventArgs e)
        {
            #region Valiate Numeric
            NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            string groupSeparator = numberFormatInfo.NumberGroupSeparator;
            string negativeSign = numberFormatInfo.NegativeSign;

            string keyInput = e.KeyChar.ToString();

            if (Char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else if (this.allowSpace && e.KeyChar == ' ')
            {

            }
            else
            {
                // Consume this invalid key and beep
                e.Handled = true;
            }
            #endregion
        }

        #endregion              

       
    }
}
