/*
Created By : Rupal Kotak
Created On: 14 Apr 2011
Desc : Adds a new table named [ProcessRowDetails] in database

*/

use <Database Name>
GO


CREATE TABLE BatchRowDetails
(
	BatchRowId int NOT NULL,
	BatchId int NOT NULL,
	BatchRowNumber varchar(50) NULL,	
	EventNumber varchar(50) NULL,	
	ClaimNumber varchar(50) NULL,	
	EmpNumber varchar(50) NULL,
	ClaimantRowId int NULL,
	PiRowId int NULL
) 

go

