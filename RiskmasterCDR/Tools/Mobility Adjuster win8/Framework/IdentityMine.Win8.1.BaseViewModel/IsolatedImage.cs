﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Media.Imaging;

namespace IdentityMine.ViewModel
{

    public class IsolatedImage : ResultItem
    {
        #region Fields

        private bool isLoading;
        private BitmapImage bitmapValue;
        protected bool isToBeCached;
        private bool clearPreviewImage;

        public event EventHandler ImageDownloaded;


        #endregion

        #region Constructor

        public IsolatedImage(string url, bool IsAtRootDirectory = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            Url = url;

            this.clearPreviewImage = clearPreviewImage;

            if (string.IsNullOrEmpty(url))
                return;
            PrepareImageFile(IsAtRootDirectory);
        }

        public IsolatedImage(string url, Uri previewUri, bool clearPreviewImage, bool isToBeCached = false)
            : this(url, previewUri, ImageSize.Thumbnail, false, isToBeCached, clearPreviewImage)
        {
        }

        public IsolatedImage(string url, Uri previewUri, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            PreviewUri = previewUri;
            this.clearPreviewImage = clearPreviewImage;
            LoadUrl(url, imgSize, IsAtRootDirectory, istobeCahced);
        }

        public IsolatedImage(string url, BitmapImage previewImage, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            PreviewUri = previewImage;
            this.clearPreviewImage = clearPreviewImage;
            LoadUrl(url, imgSize, IsAtRootDirectory, istobeCahced);
        }

        #endregion

        #region Properties

        public object PreviewUri { get; set; }

        public string Url { get; set; }

        public double PixelWidth
        {
            get
            {
                if (Value != null)
                    return Value.PixelWidth;
                return 0.0;
            }
        }
        public double PixelHeight
        {
            get
            {
                if (Value != null)
                    return Value.PixelHeight;
                return 0.0;
            }
        }

        /// <summary>
        /// indicates the loaded ImageSource for the image that is backed in isolated storage.
        /// </summary>
        public BitmapImage Value
        {
            get
            {
                return bitmapValue;
            }
            set
            {
                if (bitmapValue != value)
                {
                    bitmapValue = value;
                    RaisePropertyChanged("Value");
                }
            }
        }

        public string OriginalUrl { get; set; }

        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged("IsLoading");
                }

            }
        }

        public bool IsInMemory { get; set; }

        #endregion

        #region Methods

        public void LoadUrl(string url, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false)
        {
            isToBeCached = istobeCahced;
            OriginalUrl = url;

            if (string.IsNullOrEmpty(url))
                return;

            if (url.ToLower().EndsWith(".gif"))
            {
                url = "";
                return;
            }

            IsInMemory = false;

            Url = ProcessImageUrlForSize(url, imgSize);
            PrepareImageFile(IsAtRootDirectory);
        }

        #region Image Download

        public override async void LoadIsoImages()
        {
            if (!string.IsNullOrEmpty(Url))
            {
                if (IsInMemory || LoadImageFromIsolated())
                {
                    IsLoading = false;
                }
                else
                {
                    HttpClient client = new HttpClient();
                    var bytes = await client.GetByteArrayAsync(Url);
                    IsLoading = true;

                    if (bytes != null)
                    {
                       await WriteAndSetSource(bytes);         
                    }

                    IsLoading = false;
                    if (ImageDownloaded != null)
                        ImageDownloaded(this, null);

                }

                Debug.WriteLine("Image load called : " + Url);
            }
        }

        public async Task WriteAndSetSource(byte[]  bytes)
        {
            IsLoading = false;
            try
            {
                InMemoryRandomAccessStream randomAccessStream = new InMemoryRandomAccessStream();
                var dataWriter = new DataWriter(randomAccessStream);
                dataWriter.WriteBytes(bytes);
                await dataWriter.StoreAsync();
                randomAccessStream.Seek(0);

                MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal , () =>
                    {
                        if (Value == null)
                        {
                            Value = new BitmapImage();
                        }
                        //Value.SetSource(randomAccessStream);
                       Value.SetSourceAsync(randomAccessStream);
                    });

                IsInMemory = true;
                if (clearPreviewImage)
                {
                    PreviewUri = null;
                    RaisePropertyChanged("PreviewUri");
                }
            }
            catch
            {
                Debug.WriteLine("Stream is not a supported Image");
            }

            //if (isToBeCached)
            //{
            //    byte[] content = null;

            //  //  lock (MainVMBase.InstanceBase.IsolatedStorage)
            //    {
            // //       using (Stream isoStream = MainVMBase.InstanceBase.IsolatedStorage.OpenFile(CacheFileName, FileMode.Create))
            //        {
            //            //Save the image file stream rather than BitmapImage to Isolated Storage.
            //            content = new byte[stream.Length];
            //            stream.Position = 0;
            //            stream.Read(content, 0, content.Length);

            //            if (stream.Length != 0)
            //            {
            //               // isoStream.WriteAsync(content, 0, content.Length, null, null);
            //            }
            //        }
            //    }
            //}

        }

        public async void LoadIsoImageAsync()
        {
            await ThreadPool.RunAsync((s) => LoadIsoImages(), WorkItemPriority.Low);
        }

        #endregion

        protected virtual string ProcessImageUrlForSize(string url, ImageSize imgSize)
        {
            return url;
        }

        private void PrepareImageFile(bool IsAtRootDirectory)
        {
            string directory = "";

            if (!IsAtRootDirectory)
                directory = "/" + MainVMBase.InstanceBase.AppInstance.AppName + "/";

            //Finding the filename part of the URL - And assuming that will be unique for each file on the server
            Id = directory + FindFileNameFromUrl(Url);
        }

        private string FindFileNameFromUrl(string Url)
        {
            int index = Url.LastIndexOf('/');
            string url = Url.Substring(index + 1);

            return url.Replace(".", "").Replace("?", "") + ".img";
        }

        string _fileName;

        /// <summary>
        /// Set this property in case there is custom cache filenaming convention to be used. This defaults to Id, but sometimes Id(Which is filename of the Url) might not uniquily identify each file.
        /// </summary>
        public string CacheFileName
        {
            get
            {
                if (string.IsNullOrEmpty(_fileName))
                    return Id;
                else
                    return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        public override void UnLoad()
        {
            IsInMemory = false;
            Value = null;
        }

        public bool LoadImageFromIsolated()
        {

#if NETFX_CORE
            return false;
#else

            lock (MainVMBase.InstanceBase.IsolatedStorage)
            {

                if (MainVMBase.InstanceBase.IsolatedStorage.FileExists(CacheFileName))
                {
                    byte[] content = null;

                    using (Stream imageFileStream = MainVMBase.InstanceBase.IsolatedStorage.OpenFile(CacheFileName, FileMode.Open))
                    {
                        try
                        {
                            MemoryStream stream = new MemoryStream();
                            content = new byte[imageFileStream.Length];
                            imageFileStream.Read(content, 0, content.Length);

                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<byte[]>((s) =>
                            {
                                if (Value == null)
                                {
                                    Value = new BitmapImage();
                                    //   Value.CreateOptions = BitmapCreateOptions.BackgroundCreation;
                                }

                                Value.SetSource(new MemoryStream(s));

                                RaisePropertyChanged("PixelWidth");
                                RaisePropertyChanged("PixelHeight");

                                IsInMemory = true;

                                if (clearPreviewImage)
                                {
                                    PreviewUri = null;
                                    RaisePropertyChanged("PreviewUri");
                                }
#if DEBUG
                                Debug.WriteLine("Image loaded from Isolated : " + CacheFileName);

#endif


                            }), content);


                            if (ImageDownloaded != null)
                                ImageDownloaded(this, null);
                        }
                        catch
                        {
                            Debug.WriteLine("Error loading Isolated Image :" + CacheFileName);
                        }
                    }


                    IsLoading = false;

                    return true;

                }
            }

            return false;
#endif
        }

        #endregion
    }

    public enum ImageSize
    {
        Original,
        PhoneSize,
        Medium,
        Small,
        Thumbnail
    }

}