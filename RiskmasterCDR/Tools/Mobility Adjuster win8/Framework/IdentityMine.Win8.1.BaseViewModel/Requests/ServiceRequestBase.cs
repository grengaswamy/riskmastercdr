﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Web.Http.Headers;

namespace IdentityMine.ViewModel
{
    public abstract class ServiceRequestBase : ViewModelBase, IServiceRequest, IDisposable 
    {
        #region Fields

        private ResultItem result;
        private bool isLoaded;
        private bool isBusy;
        private bool useCache;
        private bool isNetworkError;
        private HttpClient httpClient;
        private CancellationTokenSource cancelTokenSource;

        //protected bool hasValue; //Changing temperorly for IMDb update, but we should change it back to private
        public virtual bool HasValue { get; set; }

        private bool clearResult;

        protected string classId;
        protected static string deviceId;

        public event EventHandler DownloadFinished;
        public event EventHandler DownloadError;
        public event EventHandler NoConnectionOrCache;


        #endregion

        #region Constructors

        static ServiceRequestBase()
        {
#if WINDOWS_PHONE
            deviceId = MainVMBase.InstanceBase.DeviceID;
#endif
        }

        public ServiceRequestBase()
        {
        }

        public ServiceRequestBase(string id, bool clearResult = false)
            : this(id, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBase(string title, string id, bool clearResult = false)
            : this(title, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBase(string title, string id, bool isCacheEnabled, TimeSpan cacheLifetime, bool clearResult = false)
        {
            Title = title;
            Id = id;
            IsCacheEnabled = isCacheEnabled;
            this.clearResult = clearResult;
            if (isCacheEnabled)
                CacheLifetime = cacheLifetime;
            MainVMBase.InstanceBase.ServiceFactory.AddService(this);
        }

        #endregion

        #region Properties

        public string Id { get; set; }

        public string Title { get; set; }

        public string Request { get; set; }

        public string PostData { get; set; }

        public bool ReturnJson { get; set; }

        public string Image { get; set; }

        public string Summary { get; set; }

        public DateTime RequestedDateTime { get; set; }

        public WebExceptionStatus ServiceException { get; set; }

        public string CacheFolder { get; set; }

        public bool ViewOffline { get; set; }

        public ContentType PostContentType;

        protected bool IsCacheEnabled
        {
            get
            {
                return useCache;
            }
            set
            {
                useCache = value;
            }
        }

        public bool IsNetworkError
        {
            get
            {
                return isNetworkError;
            }
            set
            {
                isNetworkError = value;
                RaisePropertyChanged("IsNetworkError");
            }
        }

        public bool IsPinnedToTile { get; set; }

        protected virtual string FileName
        {
            get
            {
                return Id + ".data";
            }
        }

        public bool IsBusy
        {
            get
            {
                return isBusy;
            }

            set
            {
                isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsLoaded
        {
            get
            {
                return isLoaded;
            }

            protected set
            {
                isLoaded = value;
                RaisePropertyChanged("IsLoaded");
            }
        }

        public bool ClearResult
        {
            get
            {
                return clearResult;
            }

            set
            {
                clearResult = value;
            }
        }

        public string ClassId
        {
            get
            {
                if (string.IsNullOrEmpty(classId))
                    classId = GetType().Name;

                return classId;
            }
        }

        public Dictionary<string, string> Headers { get; set; }

        public TimeSpan CacheLifetime { get; set; } // discard the cache in case an entry found in the cache.

        public ResultItem Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
                RaisePropertyChanged("Result");
            }
        }

        #endregion

        #region Methods

        public virtual async Task ExecuteAsync()
        {
            if (clearResult)
            {
                HasValue = false;
                Result.IsLoaded = false;
            }

            IsBusy = true;
            IsLoaded = false;
            IsNetworkError = !MainVMBase.InstanceBase.IsNetworkOnline;

            await OnLoad();
        }

        /// <summary>
        /// Call this function to cancel the network call in case of Back navigation immediatly
        /// </summary>
        protected virtual async Task OnUnload()
        {
        }

        public async Task Unload()
        {
            await OnUnload();
        }

        protected async virtual Task OnLoad()
        {
            Result.ServiceId = Id;
            RequestedDateTime = DateTime.Now;
            Debug.WriteLine("Service Load : " + Id);

            PrepareRequestString();

            //Assume that when Request=null we dont need this Load() to be executed at all
            if (string.IsNullOrEmpty(Request) || Request.Contains("{0}"))
            {
                if (Result != null) Result.IsLoaded = true;

                OnFinishedLoading();

                return;
            }

            if (HasValue) //In Memory
            {
                OnFinishedLoading();

            }
            else if (IsCacheEnabled) //Available in Isolated Storage
            {
                if (MainVMBase.InstanceBase.IsCacheExpired(FileName) || clearResult) //After reading check if the result is expired. So remove the entry from storage
                {
                    HasValue = false;
                    await CallWebServiceAsync();
                    var file = MainVMBase.InstanceBase.IsolatedStorage.GetFileAsync(FileName).AsTask();
                    //if(file.Status!=TaskStatus.Faulted)
                    //    await file.Result.DeleteAsync(); 
                }
                else
                {
                    string result = await ReadFromFile();
                    await ProcessResult(result);
                }
            }
            else
            {
                await CallWebServiceAsync();
            }
        }

        public void Reload() { }

        private async Task CallWebServiceAsync()
        {
            if (IsNetworkError)
            {
                 //should have a notification or event that the network is down and there is no cache!
                if (NoConnectionOrCache != null)
                {
                    NoConnectionOrCache(this, null);
                }
                OnFinishedLoading();

                return;
            }

            cancelTokenSource = new CancellationTokenSource();
            HttpBaseProtocolFilter httpFilter = new HttpBaseProtocolFilter();  //Just a reference not used now
            httpClient = new HttpClient(httpFilter);
            HttpRequestMessage requestMessage = new HttpRequestMessage();
            if (ReturnJson)
                httpClient.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));

            //Set Custom Header
            if (Headers != null && Headers.Count > 0)
            {
                foreach (var header in Headers)
                {
                    httpClient.DefaultRequestHeaders[header.Key] = header.Value;
                }
            }

            if (!String.IsNullOrEmpty(PostData))
            {
                requestMessage.Method = HttpMethod.Post;
                if (PostContentType == ContentType.XML)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/atom+xml");
                else if (PostContentType == ContentType.FormData)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/x-www-form-urlencoded");
                else if (PostContentType == ContentType.JSON)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/json");
            }

            HttpResponseMessage responseMessage = null;
            try
            {
                if (!String.IsNullOrEmpty(PostData))
                {
                    responseMessage = await httpClient.PostAsync(new Uri(Request), new HttpStringContent(PostData)).AsTask(cancelTokenSource.Token);
                }
                else
                {
                    responseMessage = await httpClient.GetAsync(new Uri(Request)).AsTask(cancelTokenSource.Token);
                }

                string json = await responseMessage.Content.ReadAsStringAsync();
                await ProcessResult(json);
                await WriteToFile(json);
            }
            catch (WebException we)
            {
                ServiceException = we.Status;
                Debug.WriteLine(we.Status);
                if (we.Response == null)
                {
                    if (this.DownloadError != null)
                        this.DownloadError(this, null);
                    return;
                }


                //Not needed now - Will check
                if (!string.IsNullOrEmpty(we.Response.ContentType))
                {
                    Debug.WriteLine("Need to revisit here");
                    using (Stream stream = we.Response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string json = reader.ReadToEnd();
                            ProcessResult(json);
                            WriteToFile(json);
                        }
                    }
                    return;
                }
            }
            catch (SecurityException se)
            {
                string statusString = se.Message;
                if (statusString == "")
                    statusString = se.InnerException.Message;
                Debug.WriteLine(statusString);
            }

            MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                IsNetworkError = true;
                OnFinishedLoading();
            });
        }

        public void Cancel()
        {
            cancelTokenSource.Cancel();
            cancelTokenSource.Dispose();

            // Re-create the CancellationTokenSource.
            cancelTokenSource = new CancellationTokenSource();
        }

        protected async virtual Task ProcessResult(string result)
        {
            Debug.WriteLine("Service Call Completed -WebClient : " + Id);
            Result.ServiceId = Id;

            await Task.Run(() => Result.SetResult(result));
            Debug.WriteLine("Data Procesessing Completed  : " + Id);

            HasValue = true;
            OnFinishedLoading();
        }

        protected async Task WriteToFile(string jsonString)
        {
            if (!IsCacheEnabled) return;

            StorageFolder folder = MainVMBase.InstanceBase.IsolatedStorage;
            if (!string.IsNullOrEmpty(CacheFolder))
            {
                folder = await folder.CreateFolderAsync(CacheFolder, CreationCollisionOption.OpenIfExists);
            }

            // Declare a new StreamWriter.
            StreamWriter writer = null;
            using (writer = new StreamWriter(await folder.OpenStreamForWriteAsync(FileName, CreationCollisionOption.ReplaceExisting)))
            {
                writer.Write(jsonString);
                writer.Dispose();
            }

            SetCacheExpiry(jsonString);
        }

        protected void SetCacheExpiry(string jsonString)
        {
            DateTime time = DateTime.Now.Add(CacheLifetime).ToUniversalTime();
            if (MainVMBase.InstanceBase.LocalSettings.Values.ContainsKey(FileName))
                MainVMBase.InstanceBase.LocalSettings.Values[FileName] = time.ToString();
            else
                MainVMBase.InstanceBase.LocalSettings.Values.Add(FileName, time.ToString());
        }


        protected async Task<string> ReadFromFile()
        {
            StorageFolder folder = MainVMBase.InstanceBase.IsolatedStorage;
            if (!string.IsNullOrEmpty(CacheFolder))
            {
                folder = await folder.CreateFolderAsync(CacheFolder, CreationCollisionOption.OpenIfExists);
            }

            using (Stream isoStream = await folder.OpenStreamForReadAsync(FileName))
            {
                StreamReader reader = new StreamReader(isoStream);

                // Read a line from the file and add it to sb.
                string sb = reader.ReadToEnd();
                // Close the reader.
                reader.Dispose();
                return sb;
            }
        }

        protected void OnFinishedLoading()
        {
            IsLoaded = true;
            IsBusy = false;

            if (DownloadFinished != null)
                DownloadFinished(this, null);

            if (Result != null)
            {
                Result.DownloadFinished();
            }
        }

        public void Dispose()
        {
            if (httpClient != null)
            {
                httpClient.Dispose();
                httpClient = null;
            }

            if (cancelTokenSource != null)
            {
                cancelTokenSource.Dispose();
                cancelTokenSource = null;
            }
        }

        #endregion

        #region Virtual Functions & Overrides

        protected abstract void PrepareRequestString();

        /// <summary>
        /// Override this for Tombstoning, so that end class can specify the extra state-fields to which it reactivates to
        /// </summary>
        /// <returns></returns>
        public virtual StateInfo WriteToState()
        {
            StateInfo stateInfo = new StateInfo();
            stateInfo["Type"] = GetType().ToString();
            stateInfo["Id"] = Id;
            stateInfo["Title"] = Title;
            return stateInfo;
        }

        /// <summary>
        /// Re-Activates an Instance of ServiceRequest from its basic properties, Derived instanced instances will override for more properties if needed.
        /// </summary>
        /// <param name="stateInfo"></param>
        public virtual Task ReadFromState(StateInfo stateInfo)
        {
            Id = stateInfo["Id"];
            Title = stateInfo["Title"];
            return Task.FromResult(false);
        }

        #endregion

        public event EventHandler NetworkErrorChanged;
    }
}
