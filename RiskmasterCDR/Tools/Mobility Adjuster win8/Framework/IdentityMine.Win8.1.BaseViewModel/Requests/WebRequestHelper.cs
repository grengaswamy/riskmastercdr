﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Web.Http.Headers;

#if !NETFX_CORE
using IdentityMine.BaseViewModel.Requests.GZipWebRequest;
using IdentityMine.BaseViewModel.Helpers;
#endif


namespace IdentityMine.ViewModel
{
    public class WebRequestHelper : IDisposable
    {

        #region Fields

        private string postData = string.Empty;
        private Dictionary<string, string> headers;
        private HttpClient httpClient;
        private CancellationTokenSource cancelTokenSource;

        #endregion

        public string Request { get; set; }

        public string PostData
        {
            get
            {
                return postData;
            }
            set
            {
                postData = value;
            }
        }

        public RequestMethod Method { get; set; }

        public bool AcceptHeaderRequired { get; set; }

        public ContentType PostContentType;

        public Dictionary<string, object> Parameters { get; set; }

        public string Error { get; set; }

        public Dictionary<string, string> Headers
        {
            get
            {
                if (headers == null)
                    headers = new Dictionary<string, string>();
                return headers;
            }
            set
            {
                headers = value;
            }
        }

#if !NETFX_CORE
        public bool ShouldUseGzip { get; set; }
#endif
        public delegate void DownloadFinishedEventHandler(object sender, DownloadFinishedEventArgs de);
        public event DownloadFinishedEventHandler DownloadFinished;

        public WebRequestHelper()
        {
            Method = RequestMethod.GET;
        }

        public WebRequestHelper(RequestMethod method)
        {
            Method = method;
            if (method == RequestMethod.POST)
            {
                PostContentType = ContentType.FormData;
            }
        }

        public WebRequestHelper(RequestMethod method, ContentType postContentType)
        {
            Method = method;
            PostContentType = postContentType;
        }

        private void PrepareRequest()
        {
            Error = string.Empty;
            cancelTokenSource = new CancellationTokenSource();
            HttpBaseProtocolFilter httpFilter = new HttpBaseProtocolFilter();  //Just a reference not used now
            httpClient = new HttpClient(httpFilter);
            HttpRequestMessage requestMessage = new HttpRequestMessage();
            if (AcceptHeaderRequired)
                httpClient.DefaultRequestHeaders.Accept.Add(new HttpMediaTypeWithQualityHeaderValue("application/json"));

            //Set Custom Header
            if (Headers != null && Headers.Count > 0)
            {
                foreach (var header in Headers)
                {
                    httpClient.DefaultRequestHeaders[header.Key] = header.Value;
                }
            }

            if (Method != RequestMethod.GET)
            {
                switch (Method)
                {
                    case RequestMethod.POST:
                        requestMessage.Method = HttpMethod.Post;
                        break;
                    case RequestMethod.PUT:
                        requestMessage.Method = HttpMethod.Put;
                        break;
                    case RequestMethod.DELETE:
                        requestMessage.Method = HttpMethod.Delete;
                        break;
                    case RequestMethod.PATCH:
                        requestMessage.Method = HttpMethod.Patch;
                        break;
                }
                if (PostContentType == ContentType.XML)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/atom+xml");

                else if (PostContentType == ContentType.JSON)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/json");
                else if (PostContentType == ContentType.ApplicationXML)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/xml");
                else if (PostContentType == ContentType.ApplicationSOAPXML)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/soap+xml");
                else if (PostContentType == ContentType.PSV)
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("text/psv");
                else
                    requestMessage.Content.Headers.ContentType = new HttpMediaTypeHeaderValue("application/x-www-form-urlencoded");
            }
        }

        public virtual void CallWebService()
        {
            CallWebServiceAsync();
        }

        public virtual async Task<string> CallWebServiceAsync()
        {
            PrepareRequest();
            string result = string.Empty;
            HttpResponseMessage responseMessage = null;
            try
            {
                if (!String.IsNullOrEmpty(PostData))
                {
                    responseMessage = await httpClient.PostAsync(new Uri(Request), new HttpStringContent(PostData)).AsTask(cancelTokenSource.Token);
                }
                else
                {
                    responseMessage = await httpClient.GetAsync(new Uri(Request)).AsTask(cancelTokenSource.Token);
                }

                result = await responseMessage.Content.ReadAsStringAsync();
            }
            catch (WebException we)
            {

                Error = we.Message;
                if (Error == string.Empty)
                    Error = we.InnerException.Message; ;
            }


#if NETFX_CORE
            MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (DownloadFinished != null)
                    DownloadFinished(this, new DownloadFinishedEventArgs() { Result = result, Error = Error });
            });
#else
            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
            {
                if (DownloadFinished != null)
                    DownloadFinished(this, new DownloadFinishedEventArgs() { Result = result, Error = Error, Headers = headers });
            }), "");
#endif

            return result;
        }

        public void Dispose()
        {
            Error = string.Empty;
            if (httpClient != null)
            {
                httpClient.Dispose();
                httpClient = null;
            }

            if (cancelTokenSource != null)
            {
                cancelTokenSource.Dispose();
                cancelTokenSource = null;
            }
        }
    }

    public enum RequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE,
        PATCH
    }

    public enum ContentType
    {
        XML,
        FormData,
        JSON,
        ApplicationXML,
        ApplicationSOAPXML,
        PSV
    }

    public class DownloadFinishedEventArgs : EventArgs
    {
        public string Result { get; set; }
        public string Error { get; set; }
        public string Headers { get; set; }
    }
}
