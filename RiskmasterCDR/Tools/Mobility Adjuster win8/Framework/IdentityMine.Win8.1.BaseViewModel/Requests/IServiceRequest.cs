﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace IdentityMine.ViewModel
{
    public interface IServiceRequest
    {
        event EventHandler DownloadFinished;

        event EventHandler DownloadError;

        event EventHandler NetworkErrorChanged;

        string Id { get; set; }

        string Title { get; set; }

        string ClassId { get; }

        bool IsNetworkError { get; set; }

        bool IsPinnedToTile { get; set; }

        DateTime RequestedDateTime { get; set; }

        string Request { get; set; }

        ResultItem Result { get; set; }

        WebExceptionStatus ServiceException { get; set; }

        Task ExecuteAsync();

        StateInfo WriteToState();

        Task ReadFromState(StateInfo stateInfo);

        Task Unload();
    }
}
