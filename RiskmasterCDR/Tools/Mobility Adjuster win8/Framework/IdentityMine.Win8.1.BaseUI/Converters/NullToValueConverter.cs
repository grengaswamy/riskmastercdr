﻿using System;
using Windows.UI.Xaml.Data;

namespace IdentityMine.Converters
{
    public class NullToValueConverter : IValueConverter
    {
        public object ValueIfNull { get; set; }

        public object ValueIfNotNull { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            object result = ValueIfNotNull;
            if (value == null)
            {
                result = ValueIfNull;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new InvalidOperationException("The NullToValueConverter is only valid for OneWay bindings.");
        }
    }

}
