﻿using System;
using System.Collections.Generic;

namespace IdentityMine.BaseUI
{
    public class DataToViewCollection : List<DataToViewMapping>
    {

    }

    public class DataToViewMapping
    {
        public DataToViewMapping()
        {
            
        }

        public string ResultID
        {
            get;
            set;
        }

        public Type PageType
        {
            get;
            set;
        }

    }
}
