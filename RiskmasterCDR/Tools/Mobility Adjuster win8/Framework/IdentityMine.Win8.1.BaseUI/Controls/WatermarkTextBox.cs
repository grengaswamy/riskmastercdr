﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace IdentityMine.Controls
{
    public sealed class WatermarkTextBox : TextBox
    {
        #region Constructor

        public WatermarkTextBox()
        {

            this.DefaultStyleKey = typeof(WatermarkTextBox);
            this.GotFocus += WatermarkTextBox_GotFocus;
            this.LostFocus += WatermarkTextBox_LostFocus;
            this.TextChanged += WatermarkTextBox_TextChanged;

            Binding b = new Binding();
            b.RelativeSource = new RelativeSource();
            b.RelativeSource.Mode = RelativeSourceMode.Self;
            b.Path = new PropertyPath("Text");
            this.SetBinding(WatermarkTextBox.DummyTextProperty, b);
        
        }
        
        #endregion

        #region Properties

        public static DependencyProperty WatermarkProperty = DependencyProperty.Register("Watermark", typeof(object), typeof(WatermarkTextBox), new PropertyMetadata(null));
        public object Watermark
        {
            get { return (object)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        public static DependencyProperty WatermarkTemplateProperty = DependencyProperty.Register("WatermarkTemplate", typeof(DataTemplate), typeof(WatermarkTextBox), new PropertyMetadata(null));
        public DataTemplate WatermarkTemplate
        {
            get { return (DataTemplate)GetValue(WatermarkTemplateProperty); }
            set { SetValue(WatermarkTemplateProperty, value); }
        }

        //The TextChanged event does not fire when the Text is set via a binding. As a result the watermark does not work as expected. This dummy property is used to fix the issue.
        public static readonly DependencyProperty DummyTextProperty = DependencyProperty.Register("DummyText", typeof(object), typeof(WatermarkTextBox), new PropertyMetadata(null, OnDummyTextChanged));
        public object DummyText
        {
            get { return (object)GetValue(DummyTextProperty); }
            set { SetValue(DummyTextProperty, value); }
        }
        
        private static void OnDummyTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var text2 = d as WatermarkTextBox;
            if (text2.FocusState == Windows.UI.Xaml.FocusState.Unfocused)
                text2.GoToWatermarkVisualState(false);
            else if (text2.FocusState == Windows.UI.Xaml.FocusState.Pointer)
                text2.GoToWatermarkVisualState(true);

        }

        #endregion

        #region Base Class Overrides

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //we need to set the initial state of the watermark
            GoToWatermarkVisualState(false);
        }

        #endregion

        #region Event Handlers
        
        void WatermarkTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            GoToWatermarkVisualState();
        }

        void WatermarkTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            GoToWatermarkVisualState(false);
        }

        void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            GoToWatermarkVisualState(this.FocusState != Windows.UI.Xaml.FocusState.Unfocused);
        }

        #endregion

        #region Methods

        public void GoToWatermarkVisualState(bool hasFocus = true)
        {
            //if our text is empty and our control doesn't have focus then show the watermark
            //otherwise the control eirther has text or has focus which in either case we need to hide the watermark
            if (String.IsNullOrEmpty(Text) && !hasFocus)
                GoToVisualState("WatermarkVisible"); //TODO: create constants for our magic strings
            else
                GoToVisualState("WatermarkCollapsed");
        }

        private void GoToVisualState(string stateName, bool useTransitions = true)
        {
            VisualStateManager.GoToState(this, stateName, useTransitions);
        }

        #endregion
    }
}