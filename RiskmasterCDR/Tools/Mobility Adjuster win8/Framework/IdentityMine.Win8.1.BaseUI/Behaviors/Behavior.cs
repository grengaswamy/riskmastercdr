﻿using Windows.UI.Xaml;

namespace IdentityMine.Behaviors
{
    public abstract class Behavior : DependencyObject
    {
        private FrameworkElement associatedObject;
        public FrameworkElement AssociatedObject
        {
            get
            {
                return associatedObject;
            }
            set
            {
                if (associatedObject != null)
                {
                    OnDetaching();
                }
                associatedObject = value;
                if (associatedObject != null)
                {
                    OnAttached();
                }
            }
        }

        protected virtual void OnAttached()
        {
            AssociatedObject.Unloaded += AssociatedObjectUnloaded;
        }

        protected virtual void OnDetaching()
        {
            AssociatedObject.Unloaded -= AssociatedObjectUnloaded;
        }

        void AssociatedObjectUnloaded(object sender, RoutedEventArgs e)
        {
            OnDetaching();
        }
    }

    public abstract class Behavior<T> : Behavior
    where T : FrameworkElement
    {
        protected Behavior()
        {
            
        }

        public T AssociatedObject
        {
            get
            {
                return (T)base.AssociatedObject;
            }
            set
            {
                base.AssociatedObject = value;
            }
        }
    }

}
