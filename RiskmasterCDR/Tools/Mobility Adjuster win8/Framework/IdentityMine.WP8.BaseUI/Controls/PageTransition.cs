﻿using IdentityMine.ViewModel;
using Microsoft.Phone.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
//using IdentityMine.WindowsPhone.Navigation;

namespace IdentityMine.BaseUI
{
    public static class PageTransition
    {

        public static bool IsTransitonDisabled { get; set; }

        public static bool IsActivePageEmpty { get; set; }

        #region dependency properties

        #region SupportsPageTransition (Attached DependencyProperty)

        public static readonly DependencyProperty SupportsPageTransitionProperty =
            DependencyProperty.RegisterAttached("SupportsPageTransition", typeof(bool), typeof(PageTransition),
            new PropertyMetadata(false, new PropertyChangedCallback(OnSupportsPageTransitionChanged)));

        public static void SetSupportsPageTransition(DependencyObject o, bool value)
        {
            o.SetValue(SupportsPageTransitionProperty, value);
        }

        public static bool GetSupportsPageTransition(DependencyObject o)
        {
            return (bool)o.GetValue(SupportsPageTransitionProperty);
        }

        private static void OnSupportsPageTransitionChanged(DependencyObject o,
            DependencyPropertyChangedEventArgs e)
        {
            bool supportsTransitions = (bool)e.NewValue;
            PhoneApplicationPage attachedPage = o as PhoneApplicationPage;

            if (attachedPage != null)
            {
                if (supportsTransitions)
                {
                    bool unwireHandler = false;

                    RoutedEventHandler loadedHandler = null;

                    loadedHandler = delegate(object sender, RoutedEventArgs rea)
                    {
                        TransitionSupportItem supportItem = new TransitionSupportItem(attachedPage);
                        _items.Add(supportItem);

                        if (!_navigationHandlerAttached)
                        {
                            attachedPage.NavigationService.Navigating += new NavigatingCancelEventHandler(HandleNavigationServiceNavigating);

                            _navigationHandlerAttached = true;
                        }

                        if (unwireHandler) attachedPage.Loaded -= loadedHandler;
                    };

                    if (attachedPage.NavigationService == null)
                    {
                        unwireHandler = true;
                        attachedPage.Loaded += loadedHandler;
                    }
                    else
                    {
                        loadedHandler(attachedPage, new RoutedEventArgs());
                    }
                }
                else
                {
                    //remove the TransitionSupportItem for this page
                    TransitionSupportItem supportItem = GetTransitionSupportItemForPage(attachedPage);

                    if (supportItem != null)
                    {
                        supportItem.DisconnectHandlers();
                        _items.Remove(supportItem);
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("SupportsPageTransition can only be set on PhoneApplicationPage objects.");
            }
        }

        #endregion

        #region AwayPageTransitionName (Attached DependencyProperty)

        public static readonly DependencyProperty AwayPageTransitionNameProperty =
            DependencyProperty.RegisterAttached("AwayPageTransitionName", typeof(string), typeof(PageTransition),
            new PropertyMetadata(string.Empty));

        public static void SetAwayPageTransitionName(DependencyObject o, string value)
        {
            o.SetValue(AwayPageTransitionNameProperty, value);
        }

        public static string GetAwayPageTransitionName(DependencyObject o)
        {
            if (o == null) return "";

            return (string)o.GetValue(AwayPageTransitionNameProperty);
        }

        #endregion

        #region ManageOpacityDuringTransitionIn (Attached DependencyProperty)

        public static readonly DependencyProperty ManageOpacityDuringTransitionInProperty =
            DependencyProperty.RegisterAttached("ManageOpacityDuringTransitionIn", typeof(bool), typeof(PageTransition),
            new PropertyMetadata(false));

        public static void SetManageOpacityDuringTransitionIn(DependencyObject o, bool value)
        {
            o.SetValue(ManageOpacityDuringTransitionInProperty, value);
        }

        public static bool GetManageOpacityDuringTransitionIn(DependencyObject o)
        {
            return (bool)o.GetValue(ManageOpacityDuringTransitionInProperty);
        }

        #endregion

        #endregion

        #region methods

        public static void SetActivePageAwayPageTransitionName(string transitionName)
        {
            PhoneApplicationPage page = GetActivePage();

            if (page != null)
            {
                SetAwayPageTransitionName(page, transitionName);
            }
        }

        public static void CancelCurrentPendingTransition()
        {
            _cancelledTransition = _currentTransition;
        }

        private static void HandleNavigationServiceNavigating(object sender, NavigatingCancelEventArgs e)
        {
            //if someone else already cancelled the navigation we don't want to do anything
            IsActivePageEmpty = false;
            if ((e.Uri.IsAbsoluteUri && e.Uri.Host == "external") || e.Cancel || PageTransition.IsTransitonDisabled) return;

            if (e.Uri.OriginalString.Contains("Microsoft.Phone.Controls.Toolkit")) return;

            if (e.NavigationMode == NavigationMode.Back)
            {
                PhoneApplicationPage activePage = PageTransition.GetActivePage();
                if (activePage == null)
                    IsActivePageEmpty = true;
                return;
            }
                

            //Debug.WriteLine("navigating to: " + e.Uri.ToString());

            if (!_navigationInitiatedByAnimationCompletion)
            {
                //find the trans support item for the active page
                PhoneApplicationPage activePage = PageTransition.GetActivePage();
                TransitionSupportItem supportItem = GetTransitionSupportItemForPage(activePage);

                if (supportItem != null)
                {
                    supportItem.HandleNavigationServiceNavigating(sender, e);
                }
                else
                {
                    //if the active page does not support transitions attach a navigated handler to see if
                    //the navigated to page does support transitions, in which case we will set it to 
                    //a generic transition state name for the navigation type
                    _lastNavMode = e.NavigationMode;

                    //BUGBUG: need to better handle getting the active page. in some scenarios it is null
                    if(activePage != null)
                        activePage.NavigationService.Navigated += new NavigatedEventHandler(OnNoTransitionPageNavigated);
                    
                }
            }
        }

        private static bool RaiseShouldHandleNavigation(Uri toUri)
        {
            EventHandler<ShouldHandleNavigationEventArgs> handler = ShouldHandleNavigation;

            if (handler != null)
            {
                ShouldHandleNavigationEventArgs args = new ShouldHandleNavigationEventArgs() { HandleNavigation = true, ToUri = toUri };

                handler(null, args);

                return args.HandleNavigation;
            }

            return true;
        }

        private static void OnNoTransitionPageNavigated(object sender, NavigationEventArgs e)
        {
            ((System.Windows.Navigation.NavigationService)sender).Navigated -= new NavigatedEventHandler(OnNoTransitionPageNavigated);

            PhoneApplicationPage navigatedToPage = e.Content as PhoneApplicationPage;

            if (navigatedToPage != null && GetSupportsPageTransition(navigatedToPage))
            {
                string stateName = GetBaseNavigatedToStateName();

                AlertPageTransitionInComplete(navigatedToPage, stateName);
                VisualStateManager.GoToState(navigatedToPage, stateName, true);
            }
        }

        private static void Navigate(PhoneApplicationPage page, Uri uri)
        {
          //  Debug.WriteLine("delayed navigation from: " + ((App)App.Current).RootFrame.CurrentSource.ToString() + " to: " + uri.ToString());

            page.NavigationService.Navigate(uri);
            _navigationInitiatedByAnimationCompletion = false;
        }

        private static void GoBack(PhoneApplicationPage page)
        {
            if (page.NavigationService.CanGoBack)
            {
                page.NavigationService.GoBack();
                _navigationInitiatedByAnimationCompletion = false;
            }
        }

        private static void GoForward(PhoneApplicationPage page)
        {
            if (page.NavigationService.CanGoForward)
            {
                page.NavigationService.GoForward();
                _navigationInitiatedByAnimationCompletion = false;
            }
        }

        private static PhoneApplicationPage GetActivePage()
        {
            PhoneApplicationFrame frame = Application.Current.RootVisual as PhoneApplicationFrame;

            if (frame != null)
            {
                return frame.FindChild<PhoneApplicationPage>();
            }

            return null;
        }

        private static TransitionSupportItem GetTransitionSupportItemForPage(PhoneApplicationPage page)
        {
            foreach (TransitionSupportItem supportItem in _items)
            {
                if (supportItem.Page == page)
                {
                    return supportItem;
                }
            }

            return null;
        }

        private static string GetBaseNavigatingAwayStateNameAndSetLastNavMode(NavigationMode navMode)
        {
            string stateName = string.Empty;

            switch (navMode)
            {
                case NavigationMode.Back:
                    _lastNavMode = NavigationMode.Back;
                    stateName = "NavigatingAwayBack";
                    break;
                case NavigationMode.Forward:
                case NavigationMode.New:
                    _lastNavMode = NavigationMode.Forward;
                    stateName = "NavigatingAwayForward";
                    break;
            }
            return stateName;
        }

        private static string GetBaseNavigatedToStateName()
        {
            return "NavigatedTo" + Enum.GetName(typeof(NavigationMode), _lastNavMode);
        }

        public static void AlertPageTransitionOutStarting(UIElement page, string transitionName)
        {
            IPageTransitionEvents eventsSink = page as IPageTransitionEvents;

            if (eventsSink != null)
            {
                eventsSink.PageTransitionOutStarting(transitionName);
            }
        }

        public static void AlertPageTransitionInStarting(UIElement page, string transitionName)
        {
            IPageTransitionEvents eventsSink = page as IPageTransitionEvents;

            if (eventsSink != null)
            {
                eventsSink.PageTransitionInStarting(transitionName);
            }
        }

        public static void AlertPageTransitionInComplete(UIElement page, string transitionName)
        {
            IPageTransitionEvents eventsSink = page as IPageTransitionEvents;

            if (eventsSink != null)
            {
                eventsSink.PageTransitionInCompleted(transitionName);
            }
        }

        #endregion

        #region properties

        public static FrameworkElement NavigationSource
        {
            get { return PageTransition._navigationSource; }
            set { PageTransition._navigationSource = value; }
        }

        #endregion

        #region events

        public static EventHandler<ShouldHandleNavigationEventArgs> ShouldHandleNavigation;

        #endregion

        #region private types

        private class TransitionSupportItem
        {
            public TransitionSupportItem(PhoneApplicationPage page)
            {
                page.BackKeyPress += new EventHandler<System.ComponentModel.CancelEventArgs>(HandleBackKeyPress);

                _page = new WeakReference(page);
            }

            public void HandleBackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
            {

                //if we're at the end of the back stack just return, the phone will probably exit the app
                if (Page != null && !Page.NavigationService.CanGoBack || PageTransition.IsTransitonDisabled) return;

                //if someone else already cancelled the navigation we don't want to do anything
                if (e.Cancel) return;

                e.Cancel = CommonNavBackKeyHandler(NavigationMode.Back, null);
            }

            public void HandleNavigationServiceNavigating(object sender, NavigatingCancelEventArgs e)
            {
                if (PageTransition.IsTransitonDisabled) return;

                e.Cancel = CommonNavBackKeyHandler(e.NavigationMode, e.Uri);

                Debug.WriteLine(string.Format("----------Navigating to {0}: {1}", e.Uri, e.Cancel));
            }

            private bool CommonNavBackKeyHandler(NavigationMode navMode, Uri uri)
            {
                bool shouldCancel = false;
                PhoneApplicationPage page = _page.Target as PhoneApplicationPage;

                if (!_navigationInitiatedByAnimationCompletion && page != null)
                {
                    shouldCancel = true;
                    _currentTransition = Guid.NewGuid();

                    //store some values for use when the state animation is completed
                    _storyboardCompletedNavMode = navMode;
                    _storyboardCompletedUri = uri;

                    string stateName = GetBaseNavigatingAwayStateNameAndSetLastNavMode(navMode);

                    if (!string.IsNullOrEmpty(stateName))
                    {
                        //fix BeginInvoke this to allow other navigating handlers to cancel the transition
                        MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() =>
                        {
                            if (_cancelledTransition == _currentTransition)
                            {
                                //don't do anything the transition was cancelled
                                return;
                            }

                            //append the configured navigation name
                            string transitionName = GetAwayPageTransitionName(page);
                            stateName += transitionName;

                            if (AttachStateStoryboardCompletionHandler(page, stateName, new EventHandler(OnNavigateAwayStoryboardCompleted)))
                            {
                                AlertPageTransitionOutStarting(this.Page, transitionName);
                                VisualStateManager.GoToState(page, stateName, true);
                            }
                            else
                            {
                                //there was no visual state defined for this transition combination so just let it navigate
                                shouldCancel = false;
                                //wire up handler so that we still notify the navigated to page that the transition is complete
                                page.NavigationService.Navigated += new NavigatedEventHandler(HandleNavigationServiceNavigated);

                                Debug.WriteLine("PageTransition from animation: No visual state found for name '" + stateName + "'.  Navigating normally.");
                            }
                        });
                    }
                }

                return shouldCancel;
            }

            private bool AttachStateStoryboardCompletionHandler(FrameworkElement element, string stateName, EventHandler handler)
            {
                if (element != null)
                {
                    //for now just traverse down and grab the storyboard from the VSM.  this may need some caching for perf later
                    IList stateGroups = GetVisualStateGroupsRecursive(element);

                    if (stateGroups != null)
                    {
                        int stateGroupCount = stateGroups.Count;

                        //loop through the groups
                        for (int i = 0; i < stateGroupCount; i++)
                        {
                            VisualStateGroup vsGroup = stateGroups[i] as VisualStateGroup;

                            //loop through the states
                            int stateCount = vsGroup.States.Count;

                            for (int j = 0; j < stateCount; j++)
                            {
                                VisualState vState = vsGroup.States[j] as VisualState;

                                if (vState.Name == stateName)
                                {
                                    vState.Storyboard.Completed += handler;

                                    return true;
                                }
                            }
                        }
                    }
                }

                return false;
            }

            private void OnNavigateAwayStoryboardCompleted(object sender, EventArgs e)
            {
                ProcessNavigation();

                Storyboard sb = sender as Storyboard;

                if (sb != null)
                {
                    sb.Completed -= new EventHandler(OnNavigateAwayStoryboardCompleted);
                }
            }

            private void OnNavigateToStoryboardCompleted(object sender, EventArgs e)
            {
                if (this.Page == null)
                {
                    //the page was released just return
                    //TODO: the management of release pages should be handled at a more central location
                    _items.Remove(this);
                    this.DisconnectHandlers();
                    return;
                }

                AlertPageTransitionInComplete(PageTransition.GetActivePage(), GetAwayPageTransitionName(this.Page));

                Storyboard sb = sender as Storyboard;

                if (sb != null)
                {
                    sb.Completed -= new EventHandler(OnNavigateToStoryboardCompleted);
                }
            }

            private void ProcessNavigation()
            {

                PhoneApplicationPage page = _page.Target as PhoneApplicationPage;

                if (page != null)
                {
                    _navigationInitiatedByAnimationCompletion = true;
                    page.NavigationService.Navigated += new NavigatedEventHandler(HandleNavigationServiceNavigated);

                    switch (_storyboardCompletedNavMode)
                    {
                        case NavigationMode.New:
                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() => Navigate(page, _storyboardCompletedUri));
                            break;
                        case NavigationMode.Forward:
                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() => GoForward(page));
                            break;
                        case NavigationMode.Back:
                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() => GoBack(page));
                            break;
                    }
                }
            }

            public void DisconnectHandlers()
            {
                PhoneApplicationPage page = this.Page;

                if (page != null)
                {
                    page.BackKeyPress -= new EventHandler<System.ComponentModel.CancelEventArgs>(HandleBackKeyPress);
                }
            }

            private IList GetVisualStateGroupsRecursive(FrameworkElement element)
            {
                if (element == null) return null;

                IList stateGroups = VisualStateManager.GetVisualStateGroups(element);

                if (stateGroups == null || stateGroups.Count == 0)
                {
                    int childCount = VisualTreeHelper.GetChildrenCount(element);

                    for (int i = 0; i < childCount; i++)
                    {
                        FrameworkElement child = VisualTreeHelper.GetChild(element, i) as FrameworkElement;

                        IList childStateGroups = GetVisualStateGroupsRecursive(child);

                        if (childStateGroups != null)
                        {
                            return childStateGroups;
                        }
                    }
                }

                return stateGroups;
            }

            public void HandleNavigationServiceNavigated(object sender, NavigationEventArgs e)
            {
                PhoneApplicationPage navigatedToPage = e.Content as PhoneApplicationPage;
                PhoneApplicationPage page = _page.Target as PhoneApplicationPage;

                if (page != null)
                {
                    //disconnect the handler
                    page.NavigationService.Navigated -= new NavigatedEventHandler(HandleNavigationServiceNavigated);
                }

                if (navigatedToPage != null && GetSupportsPageTransition(navigatedToPage))
                {
                    string stateName = GetBaseNavigatedToStateName();

                    if (page != null)
                    {
                        stateName += GetAwayPageTransitionName(page);
                    }

                    if (AttachStateStoryboardCompletionHandler(navigatedToPage, stateName, new EventHandler(OnNavigateToStoryboardCompleted)))
                    {
                        //hacking around the load time issues with the visual states
                        UIElement child = navigatedToPage.FindChildWithPropertyValue<FrameworkElement>(PageTransition.ManageOpacityDuringTransitionInProperty, true); // VisualTreeHelper.GetChild(navigatedToPage, 0) as UIElement;
                        if (child != null)
                        {
                            child.Opacity = 0.0;
                        }

                        RoutedEventHandler loadedHandler = null;

                        loadedHandler = (RoutedEventHandler)delegate(object s1, RoutedEventArgs e1)
                        {
                            AlertPageTransitionInStarting(navigatedToPage, stateName);

                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() =>
                            {
                                IServiceRequest sr = ((IServiceRequest)navigatedToPage.DataContext);

                                if (sr.Result == null || sr.Result.IsLoaded)
                                {
                                    VisualStateManager.GoToState(navigatedToPage, stateName, true);

                                    if (child != null)
                                    {
                                        child.Opacity = 1.0;
                                    }
                                }
                                else
                                {
                                    EventHandler transitionOnLoaded = null;

                                    transitionOnLoaded = (EventHandler)delegate(object s2, EventArgs e2s)
                                    {
                                        VisualStateManager.GoToState(navigatedToPage, stateName, true);

                                        if (child != null)
                                        {
                                            child.Opacity = 1.0;
                                        }

                                        sr.DownloadFinished -= transitionOnLoaded;
                                    };

                                    sr.DownloadFinished += new EventHandler(transitionOnLoaded);
                                }
                            });

                            navigatedToPage.Loaded -= loadedHandler;
                        };

                        navigatedToPage.Loaded += loadedHandler;
                    }
                    else
                    {
                        Debug.WriteLine("PageTranstion to animation: No visual state found for name '" + stateName + "'.");
                        AlertPageTransitionInComplete(PageTransition.GetActivePage(), stateName);
                    }
                }
            }

            public PhoneApplicationPage Page
            {
                get
                {
                    return _page.Target as PhoneApplicationPage;
                }
            }

            private WeakReference _page;
            private NavigationMode _storyboardCompletedNavMode;
            private Uri _storyboardCompletedUri;
        }

        #endregion

        #region fields

        private static bool _navigationInitiatedByAnimationCompletion;
        private static NavigationMode _lastNavMode;
        private static List<TransitionSupportItem> _items = new List<TransitionSupportItem>();
        private static FrameworkElement _navigationSource;
        private static bool _navigationHandlerAttached;
        private static Guid _currentTransition;
        private static Guid _cancelledTransition;

        #endregion
    }

    public class ShouldHandleNavigationEventArgs : EventArgs
    {
        public bool HandleNavigation { get; set; }
        public Uri ToUri { get; set; }
    }
}
