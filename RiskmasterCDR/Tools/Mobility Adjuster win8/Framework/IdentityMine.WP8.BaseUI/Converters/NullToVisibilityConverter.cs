﻿using System;
using System.Windows;
using System.Windows.Data;

namespace IdentityMine.BaseUI.Converters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //for inverse parameter, if value is null, return visible else collapsed.

            if (parameter == null)
            {
                return (value == null) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "Inverse")
            {
                return (value == null) ? Visibility.Visible : Visibility.Collapsed;
            }
            else if ((string)parameter == "String")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                if (value == null)
                {
                    return Visibility.Collapsed;
                }
                return String.IsNullOrWhiteSpace((string)value) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "Int")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((int)value) == 0) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "IntInverse")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((int)value) == 0) ? Visibility.Visible : Visibility.Collapsed;
            }
            else if ((string)parameter == "Double")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((double)value) == 0.0) ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
