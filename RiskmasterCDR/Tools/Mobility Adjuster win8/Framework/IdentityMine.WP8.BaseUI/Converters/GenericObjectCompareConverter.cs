﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IdentityMine.BaseUI.Converters
{
        /// <summary>
        /// A Generic Converter converts an object value to Bool based on whether it is null or not. 
        /// If a paramter is passed along with the converter it checks the equality with the value and return boolean
        /// </summary>
        public class GenericObjectCompareConverter : IValueConverter
        {

            #region IValueConverter Members

            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (value == null)
                    return false;

                if (parameter != null)
                {
                    if (value.ToString().Equals(parameter))
                        return true;

                    return false;
                }

                if (value is bool)
                {
                    if ((bool)value) return true;
                    else return false;
                }

                if (value is string)
                {
                    return !string.IsNullOrEmpty(value.ToString());
                }

                if (value != null)
                    return true;

                return false;
            }



            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (value == null)
                    return false;

                if (parameter != null)
                {
                    if (value.ToString().Equals(parameter))
                        return true;

                    return false;
                }

                if (value is bool)
                {
                    if ((bool)value) return true;
                    else return false;
                }

                if (value is string)
                {
                    return !string.IsNullOrEmpty(value.ToString());
                }


                if (value != null)
                    return true;

                return false;
            }

            #endregion
    }
}
