﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Navigation;
using System.Collections;
using IdentityMine.ViewModel;


#if WINDOWS_PHONE
using Microsoft.Phone.Shell;
using Microsoft.Phone.Controls;
#endif

namespace IdentityMine.BaseUI
{
    public class BaseUserControl : UserControl
    {
        #region Fields
        
        int scrollItemcount;
        int scrollItemoffset;
        //bool isScrolling;
        protected ScrollViewer scrollViewer;
        protected bool isLoaded;
        bool hasImage;

        #endregion

        #region Constructor

        public BaseUserControl()
        {
            scrollItemcount = 10;
            scrollItemoffset = -2;
            this.Loaded += new RoutedEventHandler(ThisControl_Loaded);
            SupportedOrientation = SupportedPageOrientation.Portrait;
        }

        #endregion

        #region Properties

        #if WINDOWS_PHONE
        
        /// <summary>
        /// Set this property statically for any page to get the orientation
        /// Default is Portrait
        /// </summary>
        protected SupportedPageOrientation SupportedOrientation { get; set; }
        

        public ApplicationBar ApplicationBar
        {
            get { return (ApplicationBar)GetValue(ApplicationBarProperty); }
            set { SetValue(ApplicationBarProperty, value); }
        }



        // Using a DependencyProperty as the backing store for ApplicationBar.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ApplicationBarProperty =
            DependencyProperty.Register("ApplicationBar", typeof(ApplicationBar), typeof(BaseUserControl), new PropertyMetadata(null));




        #endif

        public IDictionary<string, object> State { get; set; }

        /// <summary>
        /// Number of Images gets loaded at a batch. This will be based on the scroll position. Default to 10
        /// </summary>
        protected virtual int ScrollItemCount
        {
            get
            {
                return scrollItemcount;
            }
        }

        /// <summary>
        /// Number of previous items get loaded, Default is -2 , which means it downloads items from [Index-2] to [Index -  2 + ScrollitemCount]
        /// </summary>
        protected virtual int ScrollItemOffset
        {
            get
            {
                return scrollItemoffset;
            }
        }

        #endregion

        #region Methods

        #region Control Loaded

        void ThisControl_Loaded(object sender, RoutedEventArgs e)
        {
            OnControlLoaded(sender, e);
        }
        
        protected virtual void OnControlLoaded(object sender, RoutedEventArgs e)
        {
        #if WINDOWS_PHONE

        #else
            OnResultUserControlLoaded(null, false);
        #endif
        }

        #endregion

        #region ServiceResult Loaded
        //Just to know the control loaded from Back action or not?  To reset Scrolls and UI
        internal void ResultAndControlLoaded(FrameworkElement frameworkElement, bool isBack)
        {

#if WINDOWS_PHONE      
            if (!MainVMBase.IsAppInstancePreserved)
            {
                OnResultUserControlLoaded(frameworkElement, isBack); 
            }

#else
            OnResultUserControlLoaded(frameworkElement, isBack);
#endif

        }

        //Once the Data got in to the UI this will get called. More Like a DataContext Changed.. BasePage is the PhoneApplicationPage this Control is loaded into.
        protected virtual void OnResultUserControlLoaded(FrameworkElement basePage, bool isBack = false)
        {

            if (scrollViewer != null && hasImage)
            {
                if (isBack)
                {
                    if (State!= null && State.ContainsKey("scroll"))
                        scrollViewer.ScrollToVerticalOffset(double.Parse(State["scroll"].ToString()));
                }
                else
                {
                    if(!MainVMBase.IsAppInstancePreserved)
                        scrollViewer.ScrollToVerticalOffset(0);
                }


                double ratio = scrollViewer.VerticalOffset / (scrollViewer.ScrollableHeight + scrollViewer.ViewportHeight);

                if (double.IsNaN(ratio) || double.IsInfinity(ratio))
                    ratio = 0.0;
                if (scrollViewer.DataContext is IResults)
                    (scrollViewer.DataContext as IResults).LoadIsoImages(Convert.ToInt32((scrollViewer.DataContext as IResults).Count * ratio) + ScrollItemOffset, ScrollItemCount); //Loads up the required images to the View
            }
            else
            {

                if (DataContext is IResults)
                {
                    (DataContext as IResults).LoadIsoImages(0, ScrollItemCount);
                }
            }


            if (!isBack && DataContext is ResultItem)
            {
                (DataContext as ResultItem).LoadIsoImages();
            }

            PhoneApplicationPage _page = basePage as PhoneApplicationPage;

            if (_page != null && _page.SupportedOrientations != SupportedOrientation)
                _page.SupportedOrientations = SupportedOrientation;
        }

        #endregion

        protected virtual void OnBackNavigation(CancelEventArgs cancelEvent)
        {
            if (((!cancelEvent.Cancel)) && this.DataContext is IResults)
            {
                (this.DataContext as ResultItem).UnLoad(); //JJNEW
            }
           // (this.DataContext as ResultItem).UnLoad(); //RGBug Need think how to unload single page images
        }

        //More like a UnLoad event
        protected virtual void OnNavigatedFrom(NavigationEventArgs args, bool isBack)
        {
            if (isBack)
            {
                MainVMBase.InstanceBase.CurrentService.Unload();

                if (State != null)
                    State.Clear();
            }

            if (scrollViewer != null)
            {
                if (isBack)
                    scrollViewer.ScrollToVerticalOffset(0);
                else
                    State.AddDictionaryValue("scroll", scrollViewer.VerticalOffset);

            }

        }

        internal void BackNavigated(CancelEventArgs e)
        {

            OnBackNavigation(e);
        }

        internal void NavigatedFrom(NavigationEventArgs e, bool isBack)
        {
            OnNavigatedFrom(e, isBack);
        }

        public void RegisterForScroll(ScrollViewer scroll, bool hasImage = true)
        {
            scrollViewer = scroll;            
            this.hasImage = hasImage;

            AttachScroll();
        }

        /// <summary>
        /// isbottom tells that whether it reaches the bottom or top 
        /// </summary>
        /// <param name="isBottom"></param>
        protected virtual void OnVerticalScrollEnded(bool isBottom)
        {

        }

        internal void AttachScroll()
        {

            if (hasImage && scrollViewer != null && VisualTreeHelper.GetChildrenCount(scrollViewer) != 0)
            {

                if (scrollViewer.DataContext is IResults)
                {
                    // Visual States are always on the first child of the control template 
                    FrameworkElement element = VisualTreeHelper.GetChild(scrollViewer, 0) as FrameworkElement;
                    if (element != null)
                    {

                        //VisualStateGroup group = FindVisualState(element, "ScrollStates");
                        //if (group != null)
                        //{
                        //    group.CurrentStateChanging += (s, args) =>
                        //    {
                        //        if (args.NewState.Name == "NotScrolling")
                        //        {
                        //            if (isScrolling)
                        //            {
                        //                double ratio = scrollViewer.VerticalOffset / (scrollViewer.ScrollableHeight + scrollViewer.ViewportHeight);

                        //                if (!double.IsNaN(ratio))
                        //                    (scrollViewer.DataContext as IResults).LoadIsoImages(Convert.ToInt32((scrollViewer.DataContext as IResults).Count * ratio) + ScrollItemOffset, ScrollItemCount);

                        //                isScrolling = false;
                        //            }

                        //        }
                        //        else
                        //        {
                        //            isScrolling = true;
                        //        }
                        //    };
                        //}

                        VisualStateGroup vgroup = FindVisualState(element, "VerticalCompression");
                        //VisualStateGroup hgroup = FindVisualState(element, "HorizontalCompression");
                        if (vgroup != null)
                        {
                            vgroup.CurrentStateChanging += new EventHandler<VisualStateChangedEventArgs>(vgroup_CurrentStateChanging);
                        }
                        //if (hgroup != null)
                        //{
                        //    hgroup.CurrentStateChanging += new EventHandler<VisualStateChangedEventArgs>(hgroup_CurrentStateChanging);
                        //}

                    }

                }
            }
        }

        /*
                private void hgroup_CurrentStateChanging(object sender, VisualStateChangedEventArgs e)
                {
                    if (e.NewState.Name == "CompressionLeft")
                    {
                        OnVerticalScrollEnded(false);
                    }

                    if (e.NewState.Name == "CompressionRight")
                    {
                        OnVerticalScrollEnded(true);
                    }
                    if (e.NewState.Name == "NoHorizontalCompression")
                    {
             
                    }
        
                }
         */


        private void vgroup_CurrentStateChanging(object sender, VisualStateChangedEventArgs e)
        {
            if (e.NewState.Name == "CompressionTop")
            {
                OnVerticalScrollEnded(false);
            }

            if (e.NewState.Name == "CompressionBottom")
            {
                OnVerticalScrollEnded(true);
            }
            if (e.NewState.Name == "NoVerticalCompression")
            {
             
            }
        }

        #endregion

        #region VisualTreeHelpers

       protected VisualStateGroup FindVisualState(FrameworkElement element, string name)
        {
            if (element == null)
                return null;

            IList groups = VisualStateManager.GetVisualStateGroups(element);
            foreach (VisualStateGroup group in groups)
                if (group.Name == name)
                    return group;

            return null;
        }

        T FindSimpleVisualChild<T>(DependencyObject element) where T : class
        {
            while (element != null)
            {

                if (element is T)
                    return element as T;

                element = VisualTreeHelper.GetChild(element, 0);
            }

            return null;
        }

        #endregion
    }
}
