﻿using IdentityMine.BaseUI.Helpers;
using System;

namespace IdentityMine.BaseUI.Behaviors
{
    public class DataTrigger : BindingTriggerBase
    {
        protected override void OnBindingValueChanged()
        {
            base.OnBindingValueChanged();

            if (AreValuesEqual(BindingValue, Value))
            {
                this.InvokeActions(null);
            }
        }

        public object Value
        {
            get;
            set;
        }

        private bool AreValuesEqual(object triggerValue, object setValue)
        {
            if (triggerValue == null || setValue == null)
            {
                return triggerValue == setValue;
            }

            Type type = setValue.GetType();
            IComparable comparable = setValue as IComparable;

            if (comparable != null)
            {
                try
                {
                    return comparable.CompareTo(Convert.ChangeType(triggerValue, type, null)) == 0;
                }
                catch (InvalidCastException)
                {
                    var typeConverter = ConverterHelper.GetTypeConverter(type);

                    if (typeConverter != null
                        && typeConverter.CanConvertFrom(triggerValue.GetType()))
                    {
                        var toValue = typeConverter.ConvertFrom(triggerValue);
                        return toValue.Equals(setValue);
                    }
                }
            }
            return triggerValue == setValue;
        }
    }
}