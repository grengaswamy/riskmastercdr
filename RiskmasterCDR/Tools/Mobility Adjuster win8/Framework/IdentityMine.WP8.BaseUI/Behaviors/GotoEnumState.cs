﻿using Microsoft.Expression.Interactivity.Core;
using System.Windows;

namespace IdentityMine.BaseUI.Behaviors
{
    public class GotoEnumState : BindingBehaviorBase<FrameworkElement>
    {
        public string Prefix
        {
            get;
            set;
        }

        protected override void OnBindingValueChanged()
        {
            if (BindingValue != null)
            {
                string state = string.IsNullOrEmpty(Prefix) ? BindingValue.ToString() : string.Format("{0}{1}", Prefix, BindingValue.ToString());

                ExtendedVisualStateManager.GoToElementState(AssociatedObject, state, true);
            }
        }
    }

}
