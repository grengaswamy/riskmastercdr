﻿using System.Windows;
using System.Windows.Navigation;



#if WINDOWS_PHONE

#endif

namespace IdentityMine.BaseUI
{
    public class BaseItemUserControl : BaseUserControl
    {
        public void CallControlLoaded(object sender , RoutedEventArgs e)
        {
            OnControlLoaded(sender, e);
        }

        public void CallResultUserControlLoaded(FrameworkElement serviceResult, bool isBack = false)
        {
            OnResultUserControlLoaded(serviceResult, isBack);
        }

        public void CallNavigatedFrom(NavigationEventArgs args, bool isBack)
        {
            OnNavigatedFrom(args, isBack);
        }

    }
}
