﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace IdentityMine.Controls
{
    public sealed class AutoAlignContentControl : LayoutAwareControl 
    {
        Style defaultBackButtonStyle;
        Button BackButton;

        public AutoAlignContentControl()
        {
            this.DefaultStyleKey = typeof(AutoAlignContentControl);
            this.Loaded += AutoAlignContentControl_Loaded;
        }

        #region Properties

        public event RoutedEventHandler BackButtonClicked;

        public bool HasSeperateSnappedView
        {
            get { return (bool)GetValue(HasSeperateSnappedViewProperty); }
            set { SetValue(HasSeperateSnappedViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasSeperateSnappedView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasSeperateSnappedViewProperty =
            DependencyProperty.Register("HasSeperateSnappedView", typeof(bool), typeof(AutoAlignContentControl), new PropertyMetadata(true));

        public bool HasSeperateSnappedViewHeader
        {
            get { return (bool)GetValue(HasSeperateSnappedViewHeaderProperty); }
            set { SetValue(HasSeperateSnappedViewHeaderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasSeperateSnappedViewHeader.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasSeperateSnappedViewHeaderProperty =
            DependencyProperty.Register("HasSeperateSnappedViewHeader", typeof(bool), typeof(AutoAlignContentControl), new PropertyMetadata(true));

        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(AutoAlignContentControl), new PropertyMetadata(null));

        public object Header
        {
            get { return (object)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Header.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(object), typeof(AutoAlignContentControl), new PropertyMetadata(null));

        public object SnappedViewHeader
        {
            get { return (object)GetValue(SnappedViewHeaderProperty); }
            set { SetValue(SnappedViewHeaderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SnapViewHeader.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SnappedViewHeaderProperty =
            DependencyProperty.Register("SnappedViewHeader", typeof(object), typeof(AutoAlignContentControl), new PropertyMetadata(null));

        public object SnappedViewContent
        {
            get { return (object)GetValue(SnappedViewContentProperty); }
            set { SetValue(SnappedViewContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SnapViewContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SnappedViewContentProperty =
            DependencyProperty.Register("SnappedViewContent", typeof(object), typeof(AutoAlignContentControl), new PropertyMetadata(null));

        public bool IsScrollEnabled
        {
            get { return (bool)GetValue(IsScrollEnabledProperty); }
            set { SetValue(IsScrollEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsScrollEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsScrollEnabledProperty =
            DependencyProperty.Register("IsScrollEnabled", typeof(bool), typeof(AutoAlignContentControl), new PropertyMetadata(true, new PropertyChangedCallback(OnIsScrollEnabled)));

        public bool ShowBackButton
        {
            get { return (bool)GetValue(ShowBackButtonProperty); }
            set { SetValue(ShowBackButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowBackButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowBackButtonProperty =
            DependencyProperty.Register("ShowBackButton", typeof(bool), typeof(AutoAlignContentControl), new PropertyMetadata(false));

        public bool ShowBackButtonAlways
        {
            get { return (bool)GetValue(ShowBackButtonAlwaysProperty); }
            set { SetValue(ShowBackButtonAlwaysProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowBackButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowBackButtonAlwaysProperty =
            DependencyProperty.Register("ShowBackAlwaysButton", typeof(bool), typeof(AutoAlignContentControl), new PropertyMetadata(false));


        public Thickness ScrollableContentMargin
        {
            get { return (Thickness)GetValue(ScrollableContentMarginProperty); }
            set { SetValue(ScrollableContentMarginProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScrollableContentMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScrollableContentMarginProperty =
            DependencyProperty.Register("ScrollableContentMargin", typeof(Thickness), typeof(AutoAlignContentControl), new PropertyMetadata(new Thickness(120, 0, 40, 46)));

        public Thickness BackButtonMargin
        {
            get { return (Thickness)GetValue(BackButtonMarginProperty); }
            set { SetValue(BackButtonMarginProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScrollableContentMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackButtonMarginProperty =
            DependencyProperty.Register("BackButtonMargin", typeof(Thickness), typeof(AutoAlignContentControl), new PropertyMetadata(new Thickness(0, 0, 0, 0)));


        public VerticalAlignment BackButtonVerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(BackButtonVerticalAlignmentProperty); }
            set { SetValue(BackButtonVerticalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackButtonVerticalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackButtonVerticalAlignmentProperty =
            DependencyProperty.Register("BackButtonVerticalAlignment", typeof(VerticalAlignment), typeof(AutoAlignContentControl), new PropertyMetadata(VerticalAlignment.Bottom));


        public Visibility ContentVisibility
        {
            get { return (Visibility)GetValue(ContentVisibilityProperty); }
            set { SetValue(ContentVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContentVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentVisibilityProperty =
            DependencyProperty.Register("ContentVisibility", typeof(Visibility), typeof(AutoAlignContentControl), new PropertyMetadata(Visibility.Visible));

        #endregion

        #region Methods

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            BackButton = this.GetTemplateChild("BackButton") as Button;
           
        }

        protected override string DetermineVisualState(Windows.UI.ViewManagement.ApplicationViewState viewState)
        {
            string state = base.DetermineVisualState(viewState);
            if (state == "Snapped")
            {
                if (HasSeperateSnappedView && HasSeperateSnappedViewHeader && state == "Snapped")
                {
                    state = "SeperateSnapped";
                }
                else if (HasSeperateSnappedViewHeader)
                {
                    state = "SeperateHeaderSnapped";
                }
                else if (HasSeperateSnappedView)
                {
                    state = "SeperateContentSnapped";
                }
            }
            return state;
        }

        private static void OnIsScrollEnabled(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            AutoAlignContentControl control = sender as AutoAlignContentControl;
            if (control != null)
            {
                control.UpdateScrollBarVisibility();
            }
        }

        private void UpdateScrollBarVisibility()
        {
          
        }

        public void ScrollToPosition(double position)
        {
          
        }

        void AutoAlignContentControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!ShowBackButtonAlways)
            {
                BackButton.Visibility = (((Frame)Window.Current.Content).CanGoBack) ? Visibility.Visible : Visibility.Collapsed;
            }
            BackButton.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (BackButtonClicked == null)
            {
                if (Window.Current.Content is Frame)
                {
                    if (((Frame)Window.Current.Content).CanGoBack)
                        ((Frame)Window.Current.Content).GoBack();
                }
            }
            else
            {
                this.BackButtonClicked(sender, e);
            }
        }


        public void UpdateBackButtonStyle(string styleName)
        {
            Style style = Application.Current.Resources["styleName"] as Style;
            if (style != null && style.TargetType == BackButton.GetType())
            {
                defaultBackButtonStyle = BackButton.Style;
                BackButton.Style = style;
            }
        }

        public void ShowDefaultButtonStyle()
        {
            BackButton.Style = defaultBackButtonStyle;
        }

        #endregion
    }
}
