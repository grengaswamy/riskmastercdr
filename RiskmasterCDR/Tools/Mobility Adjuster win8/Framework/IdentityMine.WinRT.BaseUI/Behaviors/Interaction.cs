﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Windows.ApplicationModel;
using Windows.UI.Xaml;

namespace IdentityMine.Behaviors
{
    /// <summary>
    /// Attached dependency property storing 'behaviors'
    /// </summary>
    public static class Interaction
    {
        public static readonly DependencyProperty BehaviorsProperty =
           DependencyProperty.RegisterAttached("Behaviors",
           typeof(ObservableCollection<Behavior>),
           typeof(Interaction),
           new PropertyMetadata(
             DesignMode.DesignModeEnabled ? new ObservableCollection<Behavior>() : null,
           BehaviorsChanged));


        /// <summary>
        /// Called when Property is retrieved
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static ObservableCollection<Behavior> GetBehaviors(DependencyObject obj)
        {
            var associatedObject = obj as FrameworkElement;
            var behaviors = obj.GetValue(BehaviorsProperty) as ObservableCollection<Behavior>;
            if (behaviors == null)
            {
                behaviors = new ObservableCollection<Behavior>();
                SetBehaviors(obj, behaviors);
            }

            return behaviors;
        }

        /// <summary>
        /// Called when Property is retrieved
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetBehaviors(
           DependencyObject obj,
           ObservableCollection<Behavior> value)
        {
            obj.SetValue(BehaviorsProperty, value);
        }

        /// <summary>
        /// Called when the property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private static void BehaviorsChanged(
         object sender,
         DependencyPropertyChangedEventArgs args)
        {
            var associatedObject = sender as FrameworkElement;
            if (associatedObject != null)
            {
                var oldList = args.OldValue as ObservableCollection<Behavior>;
                if (oldList != null)
                {
                    foreach (var behavior in oldList)
                    {
                        behavior.AssociatedObject = null;
                    }
                }

                var newList = args.NewValue as ObservableCollection<Behavior>;
                if (newList != null)
                {
                    foreach (var behavior in newList)
                    {
                        behavior.AssociatedObject = sender as FrameworkElement;
                    }
                    newList.CollectionChanged += (collectionSender, collectionArgs) =>
                    {
                        switch (collectionArgs.Action)
                        {
                            case NotifyCollectionChangedAction.Add:
                                {
                                    foreach (Behavior behavior in collectionArgs.NewItems)
                                    {
                                        behavior.AssociatedObject = associatedObject;
                                    }
                                    break;
                                }
                            case NotifyCollectionChangedAction.Reset:
                            case NotifyCollectionChangedAction.Remove:
                                {
                                    foreach (Behavior behavior in collectionArgs.NewItems)
                                    {
                                        behavior.AssociatedObject = null;
                                    }
                                    break;
                                }
                        }
                    };
                }
            }
        }
    }

}
