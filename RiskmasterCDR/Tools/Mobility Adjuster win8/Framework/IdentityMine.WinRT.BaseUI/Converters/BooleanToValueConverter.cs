﻿using System;
using Windows.UI.Xaml.Data;

namespace IdentityMine.Converters
{    
    public sealed class BooleanToValueConverter : IValueConverter
    {

        #region properties

        public object TrueValue { get; set; }

        public object FalseValue { get; set; }

        #endregion

        #region IValueConverter implementation

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            object result = FalseValue;
            if (value is bool && (bool)value)
            {
                result = TrueValue;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            bool result = false;
            if (value == null && TrueValue == null)
            {
                result = true;
            }
            else if (value != null && value.Equals(TrueValue))
            {
                result = true;
            }
            return result;
        }

        #endregion
    }
}
