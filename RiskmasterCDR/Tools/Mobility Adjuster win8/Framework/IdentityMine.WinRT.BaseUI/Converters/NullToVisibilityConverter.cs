﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace IdentityMine.Converters
{
    /// <summary>
    /// Value converter that translates true to false and vice versa.
    /// </summary>
    public class NullToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            //for inverse parameter, if value is null, return visible else collapsed.
            if (parameter == null)
            {
                return (value == null) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "Inverse")
            {
                return (value == null) ? Visibility.Visible : Visibility.Collapsed;
            }
            else if ((string)parameter == "String")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return value == null || String.IsNullOrEmpty(((string)value).Trim()) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "Int")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((int)value) == 0) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if ((string)parameter == "IntInverse")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((int)value) == 0) ? Visibility.Visible : Visibility.Collapsed;
            }
            else if ((string)parameter == "Double")
            {
                //for inverse parameter, if value is null, return collapsed else visible.
                return (((double)value) == 0.0) ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }

        #endregion
    }
}
