﻿#if !NETFX_CORE
using Microsoft.Phone.Tasks;
using Microsoft.Phone.Info;
#endif

using System;
using System.IO;
using System.Threading.Tasks;


#if WP7       

#else
using Windows.Storage;
using System.Windows;

#endif

namespace IdentityMine.BaseViewModel.Helpers
{
    public static class Logger
    {
        private const string FileName = "app.log";

        public static string EmailId { get; set; }
        public static string EmailSubject { get; set; }
        public static string EmailMessageBoxQuestion { get; set; }

        public static bool ShouldWriteFile { get; set; }
        public static bool IsEnabled { get; set; }

        public enum Level
        {
            Verbose,
            Debug,
            Info,
            Warn,
            Error,
            Fatal,
        }

        public static Level LogLevel { get; set; }

        static Logger()
        {
#if DEBUG
            LogLevel = Level.Debug;
            //if(System.Diagnostics.Debugger.IsAttached)
            //{
            //   LogLevel = Level.Verbose;
            //}
#else
            LogLevel = Level.Debug;
#endif
            ShouldWriteFile = true;
        }

        public static async Task Verbose(string format, params object[] parameters)
        {
            await Log(Level.Verbose, format, parameters);
        }

        public static async Task Debug(string format, params object[] parameters)
        {
            await Log(Level.Debug, format, parameters);
        }

        public static async Task Info(string format, params object[] parameters)
        {
            await Log(Level.Info, format, parameters);
        }

        public static async Task Warn(string format, params object[] parameters)
        {
            await Log(Level.Warn, format, parameters);
        }

        public static async Task Error(string format, params object[] parameters)
        {
            await Log(Level.Error, format, parameters);
        }

        public static async Task Fatal(string format, params object[] parameters)
        {
            await Log(Level.Fatal, format, parameters);
        }

        public static async Task Log(Level level, string format, object[] parameters)
        {
            if (!IsEnabled)
            {
                return;
            }

            try
            {
                if (level < LogLevel)
                {
                    return;
                }

#if WP7
                int threadId = 0; //PORTBUG
#else
                int threadId = Environment.CurrentManagedThreadId;
#endif

                var msg = parameters == null || parameters.Length < 1 ? format : String.Format(format, parameters);
                var logMsg = String.Format("{0:yyyy'-'MM'-'dd HH':'mm':'ss'.'ffff} {1} [{2}] {3}\r\n", DateTime.Now, LevelToString(level)
                    , threadId, msg);

                System.Diagnostics.Debug.WriteLine(logMsg);
                if (ShouldWriteFile)
                {
                    await WriteToFile(logMsg);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("FAIL writing log file: {0}. \r\nError {1}", FileName, e);
            }
        }

        private static async Task WriteToFile(string logMsg)
        {
#if WP7    
             //PORTBUG
#else
            try
            {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                StorageFile storageFile = await localFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);

                Stream writeStream = await storageFile.OpenStreamForWriteAsync();
                using (StreamWriter writer = new StreamWriter(writeStream))
                {
                    await writer.WriteAsync(logMsg);
                }
            }
            catch (Exception)
            {
            }
#endif
        }

        public static async Task CheckForPreviousException()
        {
#if WP7    
             //PORTBUG
#else
            try
            {
                string contents = null;
                StorageFile storageFile = null;

                // Get a reference to the file in the Local Folder 
                var localFolder = ApplicationData.Current.LocalFolder;

                //Using try catch to check if a file exists or not as there is no inbuilt function yet
                try
                {
                    storageFile = await localFolder.GetFileAsync(FileName);
                }
                catch (FileNotFoundException)
                {
                }

                if (storageFile != null)
                {
                    // Open it and read the contents 
                    Stream readStream = await storageFile.OpenStreamForReadAsync();
                    using (StreamReader reader = new StreamReader(readStream))
                    {
                        contents = await reader.ReadToEndAsync();
                    }

                    //delete the file
                    await storageFile.DeleteAsync();
                }
                if (contents != null)
                {
#if NETFX_CORE

                    //send email

#else
                   if (MessageBox.Show(EmailMessageBoxQuestion ?? String.Empty, EmailSubject, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        var moreInfo = string.Format("Device: {0} | {1} \nOS Version : {2}", DeviceStatus.DeviceManufacturer, DeviceStatus.DeviceName, Environment.OSVersion.Version.ToString());
                        //send email
                        EmailComposeTask email = new EmailComposeTask();
                        email.To = EmailId;
                        email.Subject = EmailSubject;
                        email.Body = String.Format("{0} \n\n{1}", moreInfo, contents);
                        email.Show();
                    }
#endif
                }

            }
            catch (Exception)
            {
            }
#endif
        }

        private static string LevelToString(Level level)
        {
            switch (level)
            {
                case Level.Verbose: return "VERBOSE";
                case Level.Debug: return "DEBUG";
                case Level.Info: return "INFO ";
                case Level.Warn: return "WARN ";
                case Level.Error: return "ERROR";
                case Level.Fatal: return "FATAL";
                default: throw new ArgumentOutOfRangeException("level");
            }
        }
    }
}
