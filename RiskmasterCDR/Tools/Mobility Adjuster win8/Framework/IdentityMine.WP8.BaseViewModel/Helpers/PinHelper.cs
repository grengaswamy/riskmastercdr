﻿using IdentityMine.ViewModel;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace IdentityMine.BaseViewModel.Helpers
{
    public class PinHelper
    {
        private int asyncCallCount;

        public PinHelper()
        {
            asyncCallCount = 0;
        }

        #region Methods

        public void PintoTile(ShellTile shellTile, CustomTileData pinTileData, string newUrl, string url, bool supportsWideTile = true)
        {
            if (!string.IsNullOrEmpty(pinTileData.SmallBackgroundImageUrl))
                DownloadAndUpdateTile(shellTile, "SmallBackgroundImage", pinTileData.SmallBackgroundImageUrl, pinTileData, url, newUrl);
            if (!string.IsNullOrEmpty(pinTileData.BackgroundImageUrl))
                DownloadAndUpdateTile(shellTile, "BackgroundImage", pinTileData.BackgroundImageUrl, pinTileData, url, newUrl);
            if (!string.IsNullOrEmpty(pinTileData.BackBackgroundImageUrl))
                DownloadAndUpdateTile(shellTile, "BackBackgroundImage", pinTileData.BackBackgroundImageUrl, pinTileData, url, newUrl);
            if (!string.IsNullOrEmpty(pinTileData.WideBackgroundImageUrl))
                DownloadAndUpdateTile(shellTile, "WideBackgroundImage", pinTileData.WideBackgroundImageUrl, pinTileData, url, newUrl);
            if (!string.IsNullOrEmpty(pinTileData.WideBackBackgroundImageUrl))
                DownloadAndUpdateTile(shellTile, "WideBackBackgroundImage", pinTileData.WideBackBackgroundImageUrl, pinTileData, url, newUrl);

            if (asyncCallCount == 0)
                UpdateShellTile(shellTile, pinTileData, newUrl, url, supportsWideTile);
        }

        private void DownloadAndUpdateTile(ShellTile shellTile, string propertyName, string imageurl, CustomTileData pinTileData, string url, string newUrl, bool supportsWideTile = true)
        {
            asyncCallCount++;
            WebClient client = new WebClient();
            client.OpenReadCompleted += (s, e) =>
            {
                try
                {
#if !WP7
                    pinTileData.GetType().GetProperty(propertyName).SetValue(pinTileData, GetIsoFilenameFromStream(e.Result));
#else
                    pinTileData.GetType().GetProperty(propertyName).SetValue(pinTileData, GetIsoFilenameFromStream(e.Result), null);
#endif
                }
                catch { }
                --asyncCallCount;
                if (asyncCallCount == 0)
                    UpdateShellTile(shellTile, pinTileData, newUrl, url, supportsWideTile);
            };
            client.OpenReadAsync(new Uri(imageurl));
        }

        private Uri GetIsoFilenameFromStream(Stream stream)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.CreateOptions = BitmapCreateOptions.None;
            bitmapImage.SetSource(stream);
            WriteableBitmap writeableBitmap = new WriteableBitmap(bitmapImage);
            const string folderName = "Shared/ShellContent/";
            var filename = string.Format("{0}{1}.jpg", folderName, Guid.NewGuid());
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                // First check if folder exists and create if not
                if (!store.DirectoryExists(folderName))
                {
                    store.CreateDirectory(folderName);
                }

                // Write image
                using (var isoFileStream = new IsolatedStorageFileStream(filename, FileMode.Create, FileAccess.Write, store))
                {
                    writeableBitmap.SaveJpeg(isoFileStream, 173, 173, 0, 100);
                }
            }
            return new Uri("isostore:" + filename);
        }

        public void UpdateShellTile(ShellTile shellTile, CustomTileData pinTileData, string newUrl, string url, bool supportsWideTile = true)
        {
            Dictionary<string, string> urlmapper = MainVMBase.InstanceBase.UrlMapper;
            if (shellTile == null)
            {
                urlmapper.Add(newUrl, url);
                MainVMBase.InstanceBase.UrlMapper = urlmapper;
#if WP7
                ShellTile.Create(new Uri(newUrl, UriKind.Relative), pinTileData);
#else
                ShellTile.Create(new Uri(newUrl, UriKind.Relative), pinTileData, supportsWideTile);
#endif

            }
            else
            {
                shellTile.Update(pinTileData);
            }
        }

#if WP7

        /// <summary>
        /// Create fliptile using reflecter for OS 7.8
        /// </summary>
        /// <param name="newUrl"></param>
        /// <param name="url"></param>
        public void CreateFlipTile(string newUrl, string url, CustomStandardTileData pinTileData)
        {
            Dictionary<string, string> urlmapper = MainVMBase.InstanceBase.UrlMapper;
            urlmapper.Add(newUrl, url);
            MainVMBase.InstanceBase.UrlMapper = urlmapper;

            // Get the new FlipTileData type.
            Type flipTileDataType = Type.GetType("Microsoft.Phone.Shell.FlipTileData, Microsoft.Phone");

            // Get the ShellTile type so we can call the new version of "Create" that takes the new Tile templates.
            Type shellTileType = Type.GetType("Microsoft.Phone.Shell.ShellTile, Microsoft.Phone");

            // Get the constructor for the new FlipTileData class and assign it to our variable to hold the Tile properties.
            var CreateTileData = flipTileDataType.GetConstructor(new Type[] { }).Invoke(null);

            // Set the properties. 
            SetProperty(CreateTileData, "SmallBackgroundImage", pinTileData.SmallBackgroundImage);
            SetProperty(CreateTileData, "BackgroundImage", pinTileData.BackgroundImage);
            SetProperty(CreateTileData, "WideBackgroundImage", pinTileData.WideBackgroundImage);

            // Invoke the new version of ShellTile.Create.
            shellTileType.GetMethod("Create", new Type[] { typeof(Uri), typeof(ShellTileData), typeof(bool) }).Invoke(null, new Object[] { new Uri(newUrl, UriKind.Relative), CreateTileData, true });
        }

        public static void UpdateFlipTile(ShellTile shellTile, CustomStandardTileData pinTileData)
        {
            // Get the new FlipTileData type.
            Type flipTileDataType = Type.GetType("Microsoft.Phone.Shell.FlipTileData, Microsoft.Phone");

            // Get the ShellTile type so we can call the new version of "Update" that takes the new Tile templates.
            Type shellTileType = Type.GetType("Microsoft.Phone.Shell.ShellTile, Microsoft.Phone");

            // Get the constructor for the new FlipTileData class and assign it to our variable to hold the Tile properties.
            var UpdateTileData = flipTileDataType.GetConstructor(new Type[] { }).Invoke(null);

            // Set the properties. 
            if (pinTileData.SmallBackgroundImage != null)
                SetProperty(UpdateTileData, "SmallBackgroundImage", pinTileData.SmallBackgroundImage);
            if (pinTileData.BackgroundImage != null)
                SetProperty(UpdateTileData, "BackgroundImage", pinTileData.BackgroundImage);
            if (pinTileData.BackBackgroundImage != null)
                SetProperty(UpdateTileData, "BackBackgroundImage", pinTileData.BackBackgroundImage);
            if (pinTileData.WideBackgroundImage != null)
                SetProperty(UpdateTileData, "WideBackgroundImage", pinTileData.WideBackgroundImage);
            if (pinTileData.WideBackBackgroundImage != null)
                SetProperty(UpdateTileData, "WideBackBackgroundImage", pinTileData.WideBackBackgroundImage);

            // Invoke the new version of ShellTile.Update.
            shellTileType.GetMethod("Update").Invoke(shellTile, new Object[] { UpdateTileData });

        }

        private static void SetProperty(object instance, string name, object value)
        {
            var setMethod = instance.GetType().GetProperty(name).GetSetMethod();
            setMethod.Invoke(instance, new object[] { value });
        }

#endif

        #endregion
    }
}
