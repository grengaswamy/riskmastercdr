﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace IdentityMine.ViewModel
{
    public abstract class ServiceRequestFactory
    {
        private static Dictionary<string, IServiceRequest> services = new Dictionary<string,IServiceRequest>();

        public IServiceRequest GetService(ResultItem s)
        {
            if (!string.IsNullOrEmpty(s.Id) && services.ContainsKey(s.Id))
            {
                return services[s.Id];
            }
            else
            {
                IServiceRequest sr = s.CreateServiceRequest();
                //Any Service requests like Video lauching doesnt need to be stored in Service Stack.
                if(sr != null)
                    AddService(sr);
                return sr;
            }
        }

        public IServiceRequest GetService(string id)
        {
            if (services.ContainsKey(id))
                return services[id];
            return null;
        }

        public void AddService(IServiceRequest sr)
        {
            if (services == null)
                Debug.WriteLine("Services is null");
            else
                Debug.WriteLine("Services is not null");
            Debug.WriteLine("sr.Id = " + sr.Id.ToString());
            Debug.WriteLine("services = " + services.ToString());
            if(!services.ContainsKey(sr.Id))
                services.Add(sr.Id, sr);
        }

        public async Task<IServiceRequest> SetServiceByClassName(StateInfo stateInfo)
        {
            IServiceRequest sr = await GetServiceByClassName(stateInfo);
            AddService(sr);
            return sr;
        }

        public void RemoveService(string id)
        {
            if (services.ContainsKey(id))
                services.Remove(id);
        }

        protected abstract Task<IServiceRequest> GetServiceByClassName(StateInfo sessionString);
    }
}
