﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Security;
using System.Text;
using IdentityMine.BaseViewModel.Requests.GZipWebRequest;
using System.Globalization;
using System.Threading.Tasks;

#if WP7
using System.Threading;
#else

#endif

namespace IdentityMine.ViewModel
{
    public abstract class ServiceRequestBase : ViewModelBase, IServiceRequest
    {
        #region Fields

        private ResultItem result;
        private bool isLoaded;
        private bool isBusy;
        private bool useCache;
        private bool isNetworkError;

        //protected bool hasValue; //Changing temperorly for IMDb update, but we should change it back to private
        public virtual bool HasValue { get; set; }

        private bool clearResult;

        protected string classId;
        protected static string deviceId;

        public event EventHandler DownloadFinished;
        public event EventHandler DownloadError;
        public event EventHandler NetworkErrorChanged;

        #endregion

        #region Constructors

        static ServiceRequestBase()
        {
#if WINDOWS_PHONE
            deviceId = MainVMBase.InstanceBase.DeviceID;
#endif
        }

        public ServiceRequestBase()
        {
        }

        public ServiceRequestBase(string id, bool clearResult = false)
            : this(id, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBase(string title, string id, bool clearResult = false)
            : this(title, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBase(string title, string id, bool isCacheEnabled, TimeSpan cacheLifetime, bool clearResult = false)
        {
            Title = title;
            Id = id;
            IsCacheEnabled = isCacheEnabled;
            this.clearResult = clearResult;
            this.Headers = new Dictionary<string, string>();
            if (isCacheEnabled)
                CacheLifetime = cacheLifetime;
            if (!this.IsCancel)
                MainVMBase.InstanceBase.ServiceFactory.AddService(this);
        }

        #endregion

        #region Properties

        public string Id { get; set; }

        public string Title { get; set; }

        public string Request { get; set; }

        public string PostData { get; set; }

        public ContentType PostContentType;

        public bool ReturnJson { get; set; }

        public string Image { get; set; }

        public string Summary { get; set; }

        public DateTime RequestedDateTime { get; set; }

        public WebExceptionStatus ServiceException { get; set; }

        public bool ShouldUseGzip { get; set; }

        public bool ViewOffline { get; set; }

        protected bool IsCacheEnabled
        {
            get
            {
                return useCache;
            }
            set
            {
                useCache = value;
            }
        }

        public bool IsNetworkError
        {
            get
            {
                return isNetworkError;
            }
            set
            {
                if (isNetworkError != value)
                {
                    isNetworkError = value;
                    if (NetworkErrorChanged != null)
                        this.NetworkErrorChanged(this, null);
                }

                //if (value)
                //{
                //    MainVMBase.InstanceBase.HandleError(MainVMBase.InstanceBase.GetStringResource("NetworkFailureMessageText"), null, new Action(() =>
                //    {
                //        this.Execute();
                //    }), true);
                //}
                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                {
                    RaisePropertyChanged("IsNetworkError");
                    MainVMBase.InstanceBase.IsInternetAvailable = !isNetworkError;
                }), "");
            }
        }

        public bool IsPinnedToTile { get; set; }

        protected virtual string FileName
        {
            get
            {
                return Id + ".data";
            }
        }

        public bool IsBusy
        {
            get
            {
                return isBusy;
            }

            set
            {
                isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsLoaded
        {
            get
            {
                return isLoaded;
            }

            protected set
            {
                isLoaded = value;
                RaisePropertyChanged("IsLoaded");
            }
        }

        public bool ClearResult
        {
            get
            {
                return clearResult;
            }

            set
            {
                clearResult = value;
            }
        }

        public string ClassId
        {
            get
            {
                if (string.IsNullOrEmpty(classId))
                    classId = GetType().Name;

                return classId;
            }
        }

        public Dictionary<string, string> Headers { get; set; }

        /// <summary>
        /// In case of some error check you want cancel the Service Navigation, use this Property
        /// </summary>
        public bool IsCancel { get; set; }

        public TimeSpan CacheLifetime { get; set; } // discard the cache in case an entry found in the cache.

        public ResultItem Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
                RaisePropertyChanged("Result");
            }
        }

        /// <summary>
        /// Gets whether result is loaded from cache
        /// </summary>
        public bool IsLoadedFromCache { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Trigger Async call - The major hub of processing requests
        /// </summary>
        public virtual void Execute(bool isAsync = true)
        {
            //RNBug Check later
            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
            {
                IsBusy = true;
            }), "");

            IsLoaded = false;
            if (!ViewOffline)
                IsNetworkError = !MainVMBase.InstanceBase.IsNetworkOnline;

            if (isAsync)
            {
#if WP7
                ThreadPool.QueueUserWorkItem((s) => OnLoad());
#else
                System.Threading.Tasks.Task.Run(() => { OnLoad(); });
#endif
            }
            else
                OnLoad();
        }

        /// <summary>
        /// Call this function to cancel the network call in case of Back navigation immediatly
        /// </summary>
        protected virtual void OnUnload()
        {
        }

        public void Unload()
        {
            OnUnload();
        }

        protected virtual void OnLoad()
        {
            IsLoadedFromCache = false;
            if (clearResult)
            {
                HasValue = false;
                Result.IsLoaded = false;
            }

            Result.ServiceId = Id;
            RequestedDateTime = DateTime.Now;
            Debug.WriteLine("Service Load : " + Id);

            PrepareRequestString();

            //Assume that when Request=null we dont need this Load() to be executed at all
            if (string.IsNullOrEmpty(Request) || Request.Contains("{0}"))
            {
                if (Result != null) Result.IsLoaded = true;
                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                {
                    OnFinishedLoading();

                }), "");
                return;
            }

            if (HasValue) //In Memory
            {
                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                {
                    OnFinishedLoading();
                }), "");

            }
            else if (IsCacheEnabled && MainVMBase.InstanceBase.IsolatedStorage != null && MainVMBase.InstanceBase.IsolatedStorage.FileExists(FileName)) //Available in Isolated Storage
            {

                // ELBUG: Not sure if Lakeview needs a cache expiration from our end. The system automatically deletes cached files while the app isn't running.
#if WINDOWS_PHONE
                if (MainVMBase.InstanceBase.IsCacheExpired(FileName) || clearResult) //After reading check if the result is expired. So remove the entry from storage
                {
                    HasValue = false;
                    CallWebService();
                    MainVMBase.InstanceBase.IsolatedStorage.DeleteFile(FileName);
                }
                else
#endif
                {
                    string result = ReadFromFile(); //Read result from IsolatedStorage.
                    IsLoadedFromCache = true;
                    ProcessResult(result);
                }
            }
            else
            {
                CallWebService();
            }
        }

        private void CallWebService()
        {

            if (IsNetworkError)
            {
                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                {
                    OnFinishedLoading();

                }), "");
                return;
            }

            var request = (HttpWebRequest)WebRequestCreator.Create(Request, ShouldUseGzip);
            if (ReturnJson)
                request.Accept = "application/json";

            //Set Custom Header
            if (Headers != null && Headers.Count > 0)
            {
                foreach (var header in Headers)
                {
                    request.Headers[header.Key] = header.Value;
                }
            }

            if (MainVMBase.InstanceBase.Cookie != null)
                request.CookieContainer = MainVMBase.InstanceBase.Cookie;
            if (String.IsNullOrEmpty(PostData))
                request.BeginGetResponse(new AsyncCallback(ReadCallback), request);
            else
            {
                request.Method = "POST";
                if (PostContentType == ContentType.XML)
                    request.ContentType = "application/atom+xml";
                else if (PostContentType == ContentType.FormData)
                    request.ContentType = "application/x-www-form-urlencoded";
                else if (PostContentType == ContentType.JSON)
                    request.ContentType = "application/json";
                request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
            }
        }

        void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest webRequest = (HttpWebRequest)asynchronousResult.AsyncState;
            // End the stream request operation            
            Stream postStream = webRequest.EndGetRequestStream(asynchronousResult);

            byte[] byteArray = Encoding.UTF8.GetBytes(PostData);
            // Add the post data to the web request            
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            // Start the web request            
            webRequest.BeginGetResponse(new AsyncCallback(ReadCallback), webRequest);
        }

        private void ReadCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = asynchronousResult.AsyncState as HttpWebRequest;

            WebResponse response = null;
            try
            {
                response = request.EndGetResponse(asynchronousResult);
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string json = reader.ReadToEnd();

                        ProcessResult(json);

                        WriteToFile(json);
                    }
                }
                
                return;
            }
            catch (WebException we)
            {
                ServiceException = we.Status;
                Debug.WriteLine(we.Status);
                if (we.Response == null)
                {
                    if (this.DownloadError != null)
                        this.DownloadError(this, null);
                    return;
                }

                if (!string.IsNullOrEmpty(we.Response.ContentType))
                {

                    using (Stream stream = we.Response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string json = reader.ReadToEnd();

                            ProcessResult(json);

                            WriteToFile(json);
                        }
                    }
                    
                    return;
                }
            }
            catch (SecurityException se)
            {
                string statusString = se.Message;
                if (statusString == "")
                    statusString = se.InnerException.Message;
                Debug.WriteLine(statusString);
            }

            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
            {
                if (!ViewOffline)
                    IsNetworkError = true;
                OnFinishedLoading();
            }), "");
        }

        protected void ProcessResult(string result)
        {
            Debug.WriteLine("Service Call Completed -WebClient : " + Id);
            Result.ServiceId = Id;

            //Check to see any header values 
            bool networkError = MainVMBase.InstanceBase.CheckForFaultResult(result, Id);
            if (networkError)
            {
                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(() =>
                {
                    MainVMBase.InstanceBase.IsNetworkOnline = false;
                    IsNetworkError = true;
                    // Added for proper Fault injection flow
                    IsLoaded = true;
                    IsBusy = false;
                    if (Result != null)
                    {
                        Result.DownloadFinished();
                    }
                });
                return;
            }

            Result.SetResult(result);
            Debug.WriteLine("Data Procesessing Completed  : " + Id);

            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
            {
                HasValue = true;
                OnFinishedLoading();
            }), "");

            if (!string.IsNullOrEmpty(Result.Error))
            {
                MainVMBase.InstanceBase.HandleError(Result.Error);
            }
        }

        protected void WriteToFile(string jsonString)
        {
            if (!IsCacheEnabled) return;

            // Declare a new StreamWriter.
            StreamWriter writer = null;

            lock (MainVMBase.InstanceBase.IsolatedStorage)
            {
                using (writer = new StreamWriter(MainVMBase.InstanceBase.IsolatedStorage.OpenFile(FileName, FileMode.Create)))
                {
                    writer.Write(jsonString);

                    writer.Close();
                    // writer.Dispose();
                }
            }

            SetCacheExpiry(jsonString);

        }

        protected void SetCacheExpiry(string jsonString)
        {
            DateTime time = DateTime.Now.Add(CacheLifetime).ToUniversalTime();

            if (IsolatedStorageSettings.ApplicationSettings.Contains(FileName))
                IsolatedStorageSettings.ApplicationSettings[FileName] = time.ToString(CultureInfo.InvariantCulture);
            else
                IsolatedStorageSettings.ApplicationSettings.Add(FileName, time.ToString(CultureInfo.InvariantCulture));
        }


        protected string ReadFromFile()
        {
            lock (MainVMBase.InstanceBase.IsolatedStorage)
            {
                using (Stream isoStream = MainVMBase.InstanceBase.IsolatedStorage.OpenFile(FileName, FileMode.Open))
                {
                    StreamReader reader = new StreamReader(isoStream);

                    // Read a line from the file and add it to sb.
                    string sb = reader.ReadToEnd();
                    // Close the reader.
                    reader.Close();
                    reader.Dispose();
                    return sb;
                }
            }
        }

        protected void OnFinishedLoading()
        {
            IsLoaded = true;
            IsBusy = false;

            if (DownloadFinished != null)
                DownloadFinished(this, null);

            if (Result != null)
            {
                Result.DownloadFinished();
            }
        }

        /// <summary>
        /// Resets network error silently. This is used to bypass the setter of IsNetworkError and prevent related events from raising.
        /// </summary>
        protected void DoSilentNetworkErrorReset()
        {
            isNetworkError = false;
        }

        #endregion

        #region Virtual Functions & Overrides

        protected abstract void PrepareRequestString();

        /// <summary>
        /// Override this for Tombstoning, so that end class can specify the extra state-fields to which it reactivates to
        /// </summary>
        /// <returns></returns>
        public virtual StateInfo WriteToState()
        {
            StateInfo stateInfo = new StateInfo();
            stateInfo["Type"] = GetType().ToString();
            stateInfo["Id"] = Id;
            stateInfo["Title"] = Title;
            return stateInfo;
        }

        /// <summary>
        /// Re-Activates an Instance of ServiceRequest from its basic properties, Derived instanced instances will override for more properties if needed.
        /// </summary>
        /// <param name="stateInfo"></param>
        public virtual Task ReadFromState(StateInfo stateInfo)
        {
            Id = stateInfo["Id"];
            Title = stateInfo["Title"];
#if WP7
            return TaskEx.FromResult(false);
#else
            return Task.FromResult(false);
#endif
        }
        #endregion
    }
}
