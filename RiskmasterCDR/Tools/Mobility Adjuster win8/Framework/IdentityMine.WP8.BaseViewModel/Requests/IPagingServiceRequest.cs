﻿
namespace IdentityMine.ViewModel
{
    public interface IPagingServiceRequest 
    {
        int Offset { get; set; }
        int AvailableRecordCount { get; set; }
    }
}
