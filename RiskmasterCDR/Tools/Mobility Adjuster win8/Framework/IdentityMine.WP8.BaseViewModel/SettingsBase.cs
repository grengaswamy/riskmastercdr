﻿


#if NETFX_CORE

#else

using System; 

#endif


using System.Runtime.Serialization;


namespace IdentityMine.ViewModel
{

    [DataContract]
    public class SettingsBase : ResultItem
    {
        #region Fields 
       
        #endregion

        #region Constructor

        public SettingsBase()
        {

#if WINDOWS_PHONE
        
            DeviceID = Guid.NewGuid().ToString();
#endif
        }

        #endregion

#if WINDOWS_PHONE

        #region Properties

        [DataMember]
        public string DeviceID { get; set; }

        #endregion

        #region Methods

        #endregion

#endif

    }

}
