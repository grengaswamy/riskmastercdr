﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;

#if WP7
using System.Threading;
using System.Windows;
#else
using System.Threading.Tasks;
using System.Windows;
#endif

namespace IdentityMine.ViewModel
{
    public class IsolatedImage : ResultItem
    {
        #region Fields

        private bool isLoading;
        private BitmapImage bitmapValue;
        protected bool isToBeCached;
        private bool clearPreviewImage;


        string _fileName;
        public event EventHandler ImageDownloaded;


        #endregion

        #region Constructor

        public IsolatedImage(string url, bool IsAtRootDirectory = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            Url = url;
            this.clearPreviewImage = clearPreviewImage;

            if (string.IsNullOrEmpty(url))
                return;
            PrepareImageFile(IsAtRootDirectory);
        }

        public IsolatedImage(string url, Uri previewUri, bool clearPreviewImage, bool isToBeCached = false)
            : this(url, previewUri, ImageSize.Thumbnail, false, isToBeCached, clearPreviewImage)
        {
        }

        public IsolatedImage(string url, Uri previewUri, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            PreviewUri = previewUri;
            this.clearPreviewImage = clearPreviewImage;
            LoadUrl(url, imgSize, IsAtRootDirectory, istobeCahced);
        }

        public IsolatedImage(string url, BitmapImage previewImage, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false, bool clearPreviewImage = false)
        {
            OriginalUrl = url;
            PreviewUri = previewImage;
            this.clearPreviewImage = clearPreviewImage;
            LoadUrl(url, imgSize, IsAtRootDirectory, istobeCahced);
        }

        #endregion

        #region Properties

        public object PreviewUri { get; set; }

        public Dictionary<string, string> Headers { get; set; }

        public string Url { get; set; }

        /// <summary>
        // To save significant application memory, set the DecodePixelWidth or   
        // DecodePixelHeight of the BitmapImage value of the image source to the desired  
        // height or width of the rendered image. If you don't do this, the application will  
        // cache the image as though it were rendered as its normal size rather then just  
        // the size that is displayed. 
        // Note: In order to preserve aspect ratio, set DecodePixelWidth 
        // or DecodePixelHeight but not both.
        /// </summary>
        public Size RestrcitedDecodeSize { get; set; }

        /// <summary>
        /// indicates the loaded ImageSource for the image that is backed in isolated storage.
        /// </summary>
        public BitmapImage Value
        {
            get
            {
                return bitmapValue;
            }
            set
            {
                if (bitmapValue != value)
                {
                    bitmapValue = value;
                    RaisePropertyChanged("Value");
                }
            }
        }

        public string OriginalUrl { get; set; }

        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;

                    MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                    {
                        RaisePropertyChanged("IsLoading");
                    }), "");
                }

            }
        }

        public new bool IsLoaded { get; set; }


        /// <summary>
        /// Set this property in case there is custom cache filenaming convention to be used. This defaults to Id, but sometimes Id(Which is filename of the Url) might not uniquily identify each file.
        /// </summary>
        public string CacheFileName
        {
            get
            {
                if (string.IsNullOrEmpty(_fileName))
                    return Id;
                else
                    return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        #endregion

        #region Methods

        public virtual void LoadUrl(string url, ImageSize imgSize = ImageSize.Thumbnail, bool IsAtRootDirectory = false, bool istobeCahced = false)
        {
            isToBeCached = istobeCahced;
            OriginalUrl = url;

            if (string.IsNullOrEmpty(url))
                return;

#if WINDOWS_PHONE

            if (url.ToLower().EndsWith(".gif"))
            {
                url = "";
                return;
            }

#endif

            IsLoaded = false;

            Url = ProcessImageUrlForSize(url, imgSize);
            PrepareImageFile(IsAtRootDirectory);
        }

        #region Image Download


        public void LoadIsoImageAsync()
        {
#if WP7
       ThreadPool.QueueUserWorkItem((s) => LoadIsoImages());
#else
            Task.Run(() => { LoadIsoImages(); });
#endif

        }

        public override void LoadIsoImages()
        {
            if (!string.IsNullOrEmpty(Url))
            {
                if (IsLoaded || LoadImageFromIsolated())
                {
                    IsLoading = false;
                }
                else
                {
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(Url);
                    //Set Custom Header
                    if ((this.Headers != null) && (Headers.Count > 0))
                    {
                        foreach (var header in Headers)
                        {
                            request.Headers[header.Key] = header.Value;
                        }
                    }
                    request.BeginGetResponse(new AsyncCallback(ReadCallback), request);
                    IsLoading = true;
                }

                // Debug.WriteLine("Image load called : " + Id);
            }
        }


        #endregion

        private void ReadCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = asynchronousResult.AsyncState as HttpWebRequest;
            WebResponse response = null;
            try
            {
                response = request.EndGetResponse(asynchronousResult);
            }
            catch (WebException we)
            {
                Debug.WriteLine(we.Status);
            }
            catch (SecurityException se)
            {
                string statusString = se.Message;
                if (statusString == "")
                    statusString = se.InnerException.Message;
                Debug.WriteLine(statusString);
            }

            // Write the stream to Image as well as persisit on to the IsolatedStorage 
            if (response != null)
                WriteAndSetSource(response.GetResponseStream());

            IsLoading = false;


            if (ImageDownloaded != null)
                ImageDownloaded(this, null);
        }

        protected virtual string ProcessImageUrlForSize(string url, ImageSize imgSize)
        {
            return url;
        }

        protected void PrepareImageFile(bool IsAtRootDirectory)
        {
            string directory = "";

            if (!IsAtRootDirectory)
                directory = "/" + MainVMBase.InstanceBase.AppInstance.AppName + "/";

            //Finding the filename part of the URL - And assuming that will be unique for each file on the server

            string fileName = FindFileNameFromUrl(Url);
            Id = directory + fileName;

            //Note: Id is used as CacheFileName by default.'IsolatedStorage.FileExists(CacheFileName)' throws 'System.IO.PathTooLongException' if  the CacheFileName has more than 160 characters .
            //(but MSDN says the fully qualified file name must be less than 260 characters). 
            if (Id.Length > 160)
            {
                CacheFileName = directory + fileName.Remove(0, Id.Length - 160); //Restrict the CacheFileName length to 160
            }
        }

        private string FindFileNameFromUrl(string url)
        {
            string pattern = "[\\~#%&*{}./:<>?|\"-]";
            Regex regEx = new Regex(pattern);

            return regEx.Replace(url, "") + ".img";
        }

        public override void UnLoad()
        {
            IsLoaded = false;
            Value = null;
        }

        public bool LoadImageFromIsolated()
        {
            lock (MainVMBase.InstanceBase.IsolatedStorage)
            {

                if (MainVMBase.InstanceBase.IsolatedStorage.FileExists(CacheFileName))
                {
                    byte[] content = null;

                    using (Stream imageFileStream = MainVMBase.InstanceBase.IsolatedStorage.OpenFile(CacheFileName, FileMode.Open))
                    {
                        try
                        {
                            MemoryStream stream = new MemoryStream();
                            content = new byte[imageFileStream.Length];
                            imageFileStream.Read(content, 0, content.Length);

                            MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<byte[]>((s) =>
                            {
                                if (Value == null)
                                {

                                    Value = new BitmapImage();

#if !WP7
                                    // To save significant application memory, set the DecodePixelWidth or   
                                    // DecodePixelHeight of the BitmapImage value of the image source to the desired  
                                    // height or width of the rendered image. If you don't do this, the application will  
                                    // cache the image as though it were rendered as its normal size rather then just  
                                    // the size that is displayed. 
                                    // Note: In order to preserve aspect ratio, set DecodePixelWidth 
                                    // or DecodePixelHeight but not both.
                                    if (RestrcitedDecodeSize.Height > 0.0)
                                    {
                                        Value.DecodePixelHeight = (int)RestrcitedDecodeSize.Height;

                                        if (RestrcitedDecodeSize.Width > 0.0)
                                        {
                                            Value.DecodePixelWidth = (int)RestrcitedDecodeSize.Width;

                                        }
                                    }
#endif

                                }

                                Value.SetSource(new MemoryStream(s));

                                IsLoaded = true;
                                RaisePropertyChanged("IsLoaded");

                                if (clearPreviewImage)
                                {
                                    PreviewUri = null;
                                    RaisePropertyChanged("PreviewUri");
                                }
#if DEBUG
                                Debug.WriteLine("Image loaded from Isolated : " + CacheFileName);

#endif
                            }), content);

                            if (ImageDownloaded != null)
                                ImageDownloaded(this, null);
                        }
                        catch
                        {
                            Debug.WriteLine("Error loading Isolated Image :" + CacheFileName);
                        }
                    }

                    IsLoading = false;

                    return true;

                }
            }

            return false;

        }

        public async virtual void WriteAndSetSource(Stream stream)
        {
            IsLoading = false;

            if (stream.Length != 0)
            {

                MainVMBase.InstanceBase.Dispatcher.BeginInvoke(new Action<String>((s) =>
                {
                    if (Value == null)
                    {
                        Value = new BitmapImage();
#if !WP7
                        // To save significant application memory, set the DecodePixelWidth or   
                        // DecodePixelHeight of the BitmapImage value of the image source to the desired  
                        // height or width of the rendered image. If you don't do this, the application will  
                        // cache the image as though it were rendered as its normal size rather then just  
                        // the size that is displayed. 
                        // Note: In order to preserve aspect ratio, set DecodePixelWidth 
                        // or DecodePixelHeight but not both.
                        if (RestrcitedDecodeSize.Height > 0.0)
                        {
                            Value.DecodePixelHeight = (int)RestrcitedDecodeSize.Height;

                            if (RestrcitedDecodeSize.Width > 0.0)
                            {
                                Value.DecodePixelWidth = (int)RestrcitedDecodeSize.Width;

                            }
                        }
#endif
                    }
                    try
                    {
                        Value.SetSource(stream);

                        IsLoaded = true;
                        RaisePropertyChanged("IsLoaded");

                        if (clearPreviewImage)
                        {
                            PreviewUri = null;
                            RaisePropertyChanged("PreviewUri");
                        }
                    }
                    catch
                    {
                        Debug.WriteLine("Stream is not a supported Image" + Id);
                    }

                }), "");


            }

            if (isToBeCached)
            {
                byte[] content = null;

                // lock (MainVMBase.InstanceBase.IsolatedStorage)  //JJBUG
                try
                {
                    using (Stream isoStream = MainVMBase.InstanceBase.IsolatedStorage.OpenFile(CacheFileName, FileMode.Create))
                    {
                        //Save the image file stream rather than BitmapImage to Isolated Storage.
                        content = new byte[stream.Length];
                        stream.Position = 0;
                        stream.Read(content, 0, content.Length);

                        if (stream.Length != 0)
                        {
                            //isoStream.BeginWrite(content, 0, content.Length, null, null)

                            await isoStream.WriteAsync(content, 0, content.Length);
                        }
                    }
                }
                catch
                {
                    Debug.WriteLine("Error reading from Isolated Storage : " + Id);
                }
            }
        }

        #endregion

    }


    public enum ImageSize
    {
        Original,
        PhoneSize,
        Medium,
        Small,
        Thumbnail
    }
}
