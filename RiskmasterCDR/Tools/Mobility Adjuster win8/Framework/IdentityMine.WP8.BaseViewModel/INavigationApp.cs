﻿using System.Threading.Tasks;


#if !NETFX_CORE

using System.Windows.Navigation;
using System;

#endif

namespace IdentityMine.ViewModel
{
    public interface INavigationApp
    {
        void OnThemeChanged(string theme);

        Task OnLanguageChanged(string lang);

        string AppName { get; }

        string GetResourceString(string key);

#if NETFX_CORE   
         void Navigate(IServiceRequest request);
#else
        NavigationService CurrentNavigation { get; set; }

        SessionType OldSessionType {get; set;}

        void ClearBackStackNavigation();

        void Navigate(Uri navUri);
#endif

    }

    public enum SessionType
    {
        None,
        Home,
        DeepLink
    }
}
