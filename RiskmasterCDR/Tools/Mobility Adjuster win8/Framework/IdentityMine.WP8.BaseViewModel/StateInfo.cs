﻿using System.Linq;


#if NETFX_CORE

#else

using System.Windows.Controls;

#endif

using System.Collections.Generic;

namespace IdentityMine.ViewModel
{
    /// <summary>
    /// Class which represents a StateInfo object
    /// </summary>
   
    public class StateInfo :ResultItem
    {
        #region Fields

        private Dictionary<string, string> stateCollection;
        private IsolatedImage isolatedImage;

        #endregion

        #region Constructor

        public StateInfo()
        {
            stateCollection = new Dictionary<string, string>();
        }

        public StateInfo(string stateString)
        {
            stateCollection = new Dictionary<string, string>();

            string[] stateInfoCollection = stateString.Split('|');
            foreach (string stateInfo in stateInfoCollection)
            {
                string[] keyvals = stateInfo.Split('~');
                this[keyvals[0]] = keyvals[1];
            }
        }

        #endregion

        #region Properties

        public new string Id
        {
            get
            {
                return this["Id"];
            }
        }

        public string Title
        {
            get
            {
                return this["Title"];
            }
        }

        public string this[string key]
        {
            get
            {
                if (stateCollection.ContainsKey(key))
                    return stateCollection[key];
                else
                    return null;
            }
            set
            {
                if (stateCollection.ContainsKey(key))
                    stateCollection[key] = value;
                else
                {
                    stateCollection.Add(key, value);
                }
            }
        }

        public int Length
        {
            get
            {
                return stateCollection.Count;
            }
        }

        public IsolatedImage Image
        {
            get
            {
                if (isolatedImage == null)
                    isolatedImage = new IsolatedImage(null, MainVMBase.InstanceBase.GetPlaceHolder(this), ImageSize.Thumbnail);

                isolatedImage.LoadUrl(this["Image"]);

                return isolatedImage;
            }
        }

        #endregion

        #region Methods

        public override void LoadIsoImages()
        {
            if (Image != null)
                Image.LoadIsoImages();
        }

        public override string ToString()
        {
            return stateCollection.Select(i => i.Key + "~" + i.Value).Aggregate((i, j) => i + "|" + j); //Isnt it awesome to serialize a dictionary with one line LINQ!!! :) 	     
        }

        #endregion
    }

}
