﻿using System;



#if NETFX_CORE
using System.Reflection;
using System.Windows.Input;
using Windows.UI.StartScreen;
using System.Threading.Tasks;
#else

using System.Windows.Input;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;
#endif

using System.Runtime.Serialization;
using System.Linq;
using System.Collections.Generic;
using IdentityMine.BaseViewModel.Helpers;



namespace IdentityMine.ViewModel
{
    [DataContract]
    public class ResultItem : ViewModelBase, IResult
    {
        #region Fields

        public static string QueryParamSeparator = "?";

        ICommand invokeServiceCommand;
        string classId;

        public event EventHandler ResultLoaded;
        private bool isPinnedToTile;
        private bool supportsWideTile = true;

        #endregion

        #region Constructor

        static ResultItem()
        {

        }

        public ResultItem()
        {
        }
        #endregion

        #region Properties

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Id { get; set; }

        public string ServiceId { get; set; }

        public bool IsLoaded { get; set; }

        public bool CanLoadImage { get; set; }

        public ICommand InvokeService
        {
            get
            {
                if (invokeServiceCommand == null)
                {
                    invokeServiceCommand = new RelayCommand<object>(new Action<object>(OnServiceRequested));
                }

                return invokeServiceCommand;
            }
        }

        public string Error
        {
            get;
            set;
        }

        public bool SupportsWideTile
        {
            get
            {
                return supportsWideTile;
            }
            set
            {
                supportsWideTile = value;
            }
        }

#if NETFX_CORE

        public virtual SecondaryTile PinTileData
        {
            get
            {
                return new SecondaryTile
                {
                    TileId =ServiceId,
                    DisplayName ="Secondary Tile"
                };
            }
        }

        public virtual void UpdateTile()
        {

        }

        public bool IsPinnedToTile
        {
            get
            {
                try
                {
                    return SecondaryTile.Exists(ServiceId);
                }
                catch
                {
                    return false;
                }
            }
        }

        public async Task<bool> PinTile()
        {
            if (IsPinnedToTile) return true;

            return await PinTileData.RequestCreateAsync();
        }


        public async Task<bool> UnPinTile()
        {
            if (!IsPinnedToTile) return true;
            var tiles = await SecondaryTile.FindAllForPackageAsync();
            var tile = tiles.FirstOrDefault(s => s.TileId == ServiceId);
            if (tile==null) return true;

            return !(await tile.RequestDeleteAsync());
        }
#else

        public virtual CustomTileData PinTileData
        {
            get
            {
                return new CustomTileData
                {
                    BackgroundImage = new Uri("Images/SecondaryTileFrontIcon.jpg", UriKind.Relative),
                    Title = "Secondary Tile",
                    Count = null,
                    BackTitle = "Back of Tile",
                    BackContent = "You can put some data here......",
                    //BackBackgroundImage = new Uri("Images/SecondaryTileFrontIcon.jpg", UriKind.Relative)   
                };

            }
        }


        public bool IsPinnedToTile
        {
            get
            {
                if (String.IsNullOrEmpty(ServiceId))
                {
                    isPinnedToTile = false;
                }
                else
                {
                    IServiceRequest serviceRequest = MainVMBase.InstanceBase.ServiceFactory.GetService(ServiceId);
                    if (serviceRequest == null)
                    {
                        isPinnedToTile = false;
                        return isPinnedToTile;
                    }
                    StateInfo stateInfo = serviceRequest.WriteToState();
                    string url = MainVMBase.PhonePage + QueryParamSeparator + "id=" + ServiceId + "&StateInfo=" + stateInfo.ToString();

#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[PIN] Current URL: {0}", url);
#endif

                    Dictionary<string, string> urlmapper = MainVMBase.InstanceBase.UrlMapper;
                    if (urlmapper.ContainsValue(url))
                    {
                        string newUrl = urlmapper.Where(s => s.Value == url).First().Key;

                        isPinnedToTile = (ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString() == newUrl) != null);
                        if (!isPinnedToTile)
                        {
                            urlmapper.Remove(newUrl);
                            MainVMBase.InstanceBase.UrlMapper = urlmapper;
                        }
                    }
                    else
                        isPinnedToTile = false;
                }

                #region DEBUG
                System.Diagnostics.Debug.WriteLine("[PIN] Is Pinned: {0}", isPinnedToTile);
                #endregion

                return isPinnedToTile;
            }
            set
            {
                IServiceRequest serviceRequest = MainVMBase.InstanceBase.ServiceFactory.GetService(ServiceId);
                StateInfo stateInfo = serviceRequest.WriteToState();
                string url = MainVMBase.PhonePage + QueryParamSeparator + "id=" + ServiceId + "&StateInfo=" + stateInfo.ToString();
                Dictionary<string, string> urlmapper = MainVMBase.InstanceBase.UrlMapper;

                //Change to fix pin-unpin-pin-back issue with pinned tiles
                string currentUrl = MainVMBase.InstanceBase.AppInstance.CurrentNavigation.CurrentSource.OriginalString;
                var uriPart = "/redirect" + QueryParamSeparator + "guid";
                string newUrl = (urlmapper.ContainsValue(url)) ? urlmapper.Where(s => s.Value == url).First().Key : (currentUrl.Contains(uriPart)) ? currentUrl : uriPart + "=" + Guid.NewGuid().ToString();

                var shellTile = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString() == newUrl);
                serviceRequest.IsPinnedToTile = value;
                if (value)
                {
                    PinHelper pinHelper = new PinHelper();

#if WP7
                    if (MainVMBase.InstanceBase.IsVersion78) //For OS 7.8 and Above
                    {
                        pinHelper.CreateFlipTile(newUrl, url, PinTileData);
                    }
                    else
                    {
                        pinHelper.PintoTile(shellTile, PinTileData, newUrl, url);
                    }
#else
                    pinHelper.PintoTile(shellTile, PinTileData, newUrl, url, this.SupportsWideTile);
#endif
                }
                else
                {
                    if (MainVMBase.InstanceBase.VolatileUrlMapper == null)
                    {
                        MainVMBase.InstanceBase.VolatileUrlMapper = new Dictionary<string, string>();
                        MainVMBase.InstanceBase.VolatileUrlMapper.Add(newUrl, url);
                    }
                    urlmapper.Remove(newUrl);
                    MainVMBase.InstanceBase.UrlMapper = urlmapper;
                    IsolatedStorageSettings.ApplicationSettings.Save();

                    if (MainVMBase.InstanceBase.IsolatedStorage.FileExists(Id))
                    {
                        MainVMBase.InstanceBase.IsolatedStorage.CopyFile(Id, Id + ".tmpfile", true);
                    }

                    if (shellTile != null)
                        shellTile.Delete();
                }

                isPinnedToTile = value;
            }
        }
#endif

        #endregion

        #region Methods

        public virtual void SetResult(string result)
        {
            IsLoaded = true;

#if NETFX_CORE
            RaisePropertyChanged(string.Empty);
#else
            RaisePropertyChanged(null);
#endif
        }

        public virtual IServiceRequest CreateServiceRequest()
        {
            return null;
        }

        protected virtual string GetStringResource(string key)
        {
            if (string.IsNullOrEmpty(key)) return "";

            return MainVMBase.InstanceBase.GetStringResource(key);
        }

        protected virtual void OnServiceRequested(object param)
        {
            MainVMBase.InstanceBase.Navigate(this);
        }

        public virtual void LoadIsoImages()
        {
            CanLoadImage = true;
        }

        /// <summary>
        /// Override this method in the ViewModel class to delete all heavy memory allocations, 
        /// For example IsolatedImage.Value =null ( Bitmap holds a lot of memory)
        /// </summary>
        public virtual void UnLoad()
        {

        }

        /// <summary>
        /// Binary Serialization
        /// </summary>
        /// <param name="writer"></param>
        public virtual void BinaryWrite(System.IO.BinaryWriter writer)
        {
#if NETFX_CORE
            writer.Write(Id ?? string.Empty);
            writer.Write(DisplayName ?? string.Empty);
            writer.Write(ClassID ?? string.Empty); 
#else
            writer.WriteString(Id);
            writer.WriteString(DisplayName);
            writer.WriteString(ClassID);
#endif
        }

        public virtual void BinaryRead(System.IO.BinaryReader reader)
        {
            Id = reader.ReadString();
            DisplayName = reader.ReadString();
            ClassID = reader.ReadString();
        }

        public virtual string ClassID
        {
            get
            {
                if (string.IsNullOrEmpty(classId))
                    classId = GetType().Name;

                return classId;
            }

            set
            {
                classId = value;
            }
        }

        public void DownloadFinished()
        {
            IsLoaded = true;
#if NETFX_CORE          
            RaisePropertyChanged(string.Empty);
#else
            RaisePropertyChanged(null);

#endif

            if (ResultLoaded != null)
                ResultLoaded(this, null);
        }

        public new void RaisePropertyChanged(string propertyName)
        {
            base.RaisePropertyChanged(propertyName);
        }

        #endregion

    }
}
