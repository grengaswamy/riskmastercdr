using System.Linq;
using System.Collections.ObjectModel;

#if NETFX_CORE

#else


#endif

#if WP7
    using System.Threading;
#else
using System.Threading.Tasks;
using Windows.System.Threading;
#endif




namespace IdentityMine.ViewModel
{

    public class ResultItems<T> : ResultItem, IResults where T : ResultItem
    {

        #region Constructor
        
        public ResultItems()
        {
            Collection = new ObservableCollection<T>();
        }

        public ResultItems(string itemId, string displayName)
        {
            DisplayName = displayName;
            Id = itemId;
            Collection = new ObservableCollection<T>();
        }

        #endregion

        #region Properties

        public ObservableCollection<T> Collection { get; set; }

        public string GroupName { get; set; } //Used to store some header information

        public virtual int Count { get { return Collection.Count; } }

        #endregion

        #region Methods

        public void LoadIsoImages(int startIndex, int count)
        {
#if WP7
            ThreadPool.QueueUserWorkItem((s) => OnLoadImages(startIndex, count));     
#else
           ThreadPool.RunAsync((s) => OnLoadImages(startIndex, count), WorkItemPriority.Low );
#endif
        }

        public override async void UnLoad()
        {
#if !WP7
            await System.Threading.Tasks.Task.Delay(300);
#endif
            foreach (ResultItem item in Collection)
            {
                item.UnLoad();
            }

            Collection.Clear();
        }

        public void UnLoadImages()
        {
            foreach (ResultItem item in Collection)
            {
                item.UnLoad();
            }
        }

#if NETFX_CORE
        protected virtual async void OnLoadImages(int startIndex, int count)
#else
        protected virtual void OnLoadImages(int startIndex, int count)
#endif
        {
            if (startIndex < 0) startIndex = 0;

            for (int i = startIndex; i < startIndex + count; i++)
            {
#if NETFX_CORE
                await Task.Delay(50);
#endif
                ResultItem item = Collection.ElementAtOrDefault(i);

                if (item != null)
                {
                    item.LoadIsoImages();
                }
               
            }
        }

        #endregion

    }
}
