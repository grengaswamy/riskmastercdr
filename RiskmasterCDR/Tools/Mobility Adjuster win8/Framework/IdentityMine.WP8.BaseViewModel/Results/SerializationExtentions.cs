﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IdentityMine.ViewModel
{
    public static class BinarySerializerExtensions
    {
        public static void WriteList<T>(this BinaryWriter writer, List<T> list) where T : ResultItem
        {
            if (list != null)
            {
                writer.Write(list.Count);
                foreach (T item in list)
                {
                    item.BinaryWrite(writer);
                }
            }
            else
            {
                writer.Write(0);
            }
        }

        public static void WriteList(this BinaryWriter writer, List<string> list)
        {
            if (list != null)
            {
                writer.Write(list.Count);
                foreach (string item in list)
                {
                    writer.Write(item);
                }
            }
            else
            {
                writer.Write(0);
            }
        }

        public static void Write<T>(this BinaryWriter writer, T value) where T : ResultItem
        {
            if (value != null)
            {
                writer.Write(true);
                value.BinaryWrite(writer);
            }
            else
            {
                writer.Write(false);
            }
        }

        public static void Write(this BinaryWriter writer, DateTime value)
        {
            writer.Write(value.Ticks);
        }

        public static void WriteString(this BinaryWriter writer, string value)
        {
            writer.Write(value ?? string.Empty);
        }

        public static T ReadGeneric<T>(this BinaryReader reader) where T : ResultItem, new()
        {
            if (reader.ReadBoolean())
            {
                T result = new T();
                result.BinaryRead(reader);
                return result;
            }
            return default(T);
        }

        public static List<string> ReadList(this BinaryReader reader)
        {
            int count = reader.ReadInt32(); //9
            if (count > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < count; i++)
                {
                    list.Add(reader.ReadString());
                }
                return list;
            }

            return null;
        }

        public static List<T> ReadList<T>(this BinaryReader reader) where T : ResultItem, new()
        {
            int count = reader.ReadInt32();
            if (count > 0)
            {
                List<T> list = new List<T>();
                for (int i = 0; i < count; i++)
                {
                    T item = new T();
                    item.BinaryRead(reader);
                    list.Add(item);
                }
                return list;
            }

            return null;
        }

        public static DateTime ReadDateTime(this BinaryReader reader)
        {
            var int64 = reader.ReadInt64();
            return new DateTime(int64);
        }
    }
}
