﻿using System;

namespace IdentityMine.ViewModel
{
    public interface IResult
    {
        event EventHandler ResultLoaded;

        string Id { get; set; }

        string DisplayName { get;  set; }

        bool IsLoaded { get; set; }

        void SetResult(string result);

        string ClassID { get; }
    }
}
