﻿using Microsoft.Phone.Shell;

namespace IdentityMine.ViewModel
{
    public class CustomFlipTileData : FlipTileData 
    {
        public string SmallBackgroundImageUrl { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string BackBackgroundImageUrl { get; set; }
        public string WideBackgroundImageUrl { get; set; }
        public string WideBackBackgroundImageUrl { get; set; }

    }
}
