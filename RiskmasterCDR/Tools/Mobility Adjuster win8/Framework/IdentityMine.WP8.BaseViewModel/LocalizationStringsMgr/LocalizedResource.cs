﻿using System.Collections.Generic;

namespace IdentityMine.ViewModel
{
    /// <summary>
    /// Class which represents a Resource Strings used for a specific language
    /// </summary>
   
    public class LocalizedResource
    {
        public LocalizedResource(string language)
        {
            Sessions = new Dictionary<string, string>();
            Lang = language;
        }


        public string Lang { get; set; }

        public string this[string key]
        {
            get
            {
                return Sessions[key];
            }
            set
            {
                if (Sessions.ContainsKey(key))
                    Sessions[key] = value;
                else
                {
                    Sessions.Add(key, value);
                }
            }
        }


        public Dictionary<string, string> Sessions{get; set;}
    }


}
