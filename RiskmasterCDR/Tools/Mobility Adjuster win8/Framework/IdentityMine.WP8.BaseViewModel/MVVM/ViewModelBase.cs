﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace IdentityMine.ViewModel
{
    [DataContract]
#if NETFX_CORE
    [Windows.Foundation.Metadata.WebHostHidden]
#endif    
    public abstract class ViewModelBase : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
#if NETFX_CORE
                if (MainVMBase.InstanceBase.Dispatcher.HasThreadAccess)
                    handler(this, new PropertyChangedEventArgs(propertyName));
                else
                    MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        handler(this, new PropertyChangedEventArgs(propertyName));
                    });                
#else
                handler(this, new PropertyChangedEventArgs(propertyName));
#endif
            }
        }
    }
}
