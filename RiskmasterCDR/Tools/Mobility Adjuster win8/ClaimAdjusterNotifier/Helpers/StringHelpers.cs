﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ClaimAdjuster.Notifier
{
    public static class StringHelpers
    {
        public static ToastData DeserializeCustom(string result)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ToastData));
            XmlReader reader = XmlReader.Create(new StringReader(result));
            ToastData deserializeObject = (ToastData)(serializer.Deserialize(reader));
            return deserializeObject;
        }
    }
}
