﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.UI.Core;


namespace ClaimsAdjuster.ViewModel
{
    public class MediaCollection : ResultItems<MediaViewModel>
    {
        #region Fields

        private ICommand deletePhotoCommand;
        private string title;

        #endregion

        #region Properties

        public ICommand DeletePhotoCommand
        {
            get
            {
                if (deletePhotoCommand == null)
                    deletePhotoCommand = new RelayCommand(OnDeletePhotos);

                return deletePhotoCommand;
            }
        }

        public string Title
        {
            get { return title; }
            set 
            { 
                title = value;
                RaisePropertyChanged("Title");
            }
        }

        public string AppBarText
        {
            get
            {
                return string.Format("{0} {1}", MainViewModel.Instance.GetStringResource("DeleteText"), Title);
            }
        }

        public bool HasAnySelectedItem
        {
            get
            {
                return Collection.Any(s => s.IsSelected == true);
            }
        }

        public bool HasMediaItems
        {
            get
            {
                return !MainViewModel.Instance.IsAppBusy && Count == 0;
            }
        }

        #endregion

        #region Methods

        public async Task LoadData(string parentId, ParentType parentType, MediaCaptureType mediaCaptureType)
        {

            var photos = await MainViewModel.Instance.CscRepository.GetAttachments(parentId, parentType, mediaCaptureType);
            List<MediaViewModel> photoVMs = new List<MediaViewModel>();

            foreach (var photo in photos)
            {
                MediaViewModel cscPhoto = new MediaViewModel();
                cscPhoto.MediaElement = photo;
                    await cscPhoto.GetThumbNailImage();
                photoVMs.Add(cscPhoto);
            }
            await AddToCollection(photoVMs);
        }

        public async Task AddToCollection(List<MediaViewModel> photos)
        {
            await MainViewModel.Instance.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Collection.Clear();
                foreach (var photo in photos)
                {
                    Collection.Add(photo);
                }
                RaiseCountProperties();
            });           

        }

        public void RaiseCountProperties()
        {
            RaisePropertyChanged("Count");
            RaisePropertyChanged("HasMediaItems");
        }

        private async void OnDeletePhotos()
        {

            var photosToDelete = Collection.Where(photo => photo.IsSelected == true).ToList();
            if (photosToDelete == null) return;
            foreach (var photo in photosToDelete)
            {
                try
                {
                    Uri filepath = new Uri(photo.FileLocation);
                    var file = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(filepath);
                    await file.DeleteAsync();
                    Collection.Remove(photo);
                    RaiseCountProperties();
                    await MainViewModel.Instance.CscRepository.DeleteAsync<Attachment>(photo.MediaElement);
                    MainViewModel.Instance.RemoveAttachment(photo.MediaElement);
                }
                catch (Exception ex)
                {
                    MessageHelper.Show(MainViewModel.Instance.GetStringResource("DeleteFailedText"));
                }
            }


        }
        #endregion
    }
}
