﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
namespace ClaimsAdjuster.ViewModel
{
    public class ClaimCollection : ResultItems<ClaimViewModel>
    {
        #region Feilds

        private string selectedLossDate;
        private string selectedPolicyType;
        private string selectedSortingField;
        private Code openClaimStatusCode;
        private Code selectedClaimStatus;
        private string selectedAssignedTo;
        private ObservableCollection<string> dateOfLossList;
        private ObservableCollection<string> policyTypeList;
        private ObservableCollection<string> assignedToList;
        private ObservableCollection<string> sortingFieldList;
        private List<Code> claimStatusList;

        #endregion

        #region Propertes

        public List<Claim> AllClaim { get; set; }

        public ObservableCollection<string> DateOfLossList
        {
            get
            {
                if (dateOfLossList == null)
                    dateOfLossList = new ObservableCollection<string>() { MainViewModel.Instance.GetStringResource("AllDatesText") };
                return dateOfLossList;
            }
        }

        public string SelectedLossDate
        {
            get
            {
                return selectedLossDate;
            }
            set
            {
                selectedLossDate = value;
                RefreshData();
            }
        }

        public ObservableCollection<string> PolicyTypeList
        {
            get
            {
                if (policyTypeList == null)
                    policyTypeList = new ObservableCollection<string>() { MainViewModel.Instance.GetStringResource("AllTypesText") };
                return policyTypeList;
            }
        }

        public string SelectedPolicyType
        {
            get
            {
                return selectedPolicyType;
            }
            set
            {
                selectedPolicyType = value;
                RefreshData();
            }
        }

        public ObservableCollection<string> SortingFieldList
        {
            get
            {
                if (sortingFieldList == null)
                {
                    sortingFieldList = new ObservableCollection<string>()
                    {   
                        MainViewModel.Instance.GetStringResource("ClaimNumberText"),
                        MainViewModel.Instance.GetStringResource("ClaimantNameText"),
                        MainViewModel.Instance.GetStringResource("EventText"),
                    };
                }
                return sortingFieldList;

            }
        }

        public string SelectedSortingField
        {
            get
            {
                return selectedSortingField;
            }
            set
            {
                selectedSortingField = value;
                RefreshData();
            }
        }

        public List<Code> ClaimStatusList
        {
            get
            {
                return claimStatusList;
            }
            set
            {
                claimStatusList = value;
                RaisePropertyChanged("ClaimStatusList");
            }
        }

        public Code SelectedClaimStatus
        {
            get { return selectedClaimStatus; }
            set
            {
                if (value != null)
                {
                    selectedClaimStatus = value;
                    RefreshData();
                }
            }
        }

        public ObservableCollection<string> AssignedToList
        {
            get
            {
                if (assignedToList == null)
                    assignedToList = new ObservableCollection<string>() { MainViewModel.Instance.CurrentUserInfo.UserName };
                return assignedToList;
            }
        }

        public string SelectedAssignedTo
        {
            get
            {
                return selectedAssignedTo;
            }
            set
            {
                selectedAssignedTo = value;
                RefreshData();
            }
        }

        public bool IsNoRecordTextVisible
        {
            get
            {
                return !MainViewModel.Instance.IsAppBusy && (this.Collection == null || Collection.Count == 0);
            }
        }
        #endregion

        #region Constructor

        public ClaimCollection()
        {
        }

        #endregion

        #region Methods

        public async Task InitializeFilterValues()
        {
            selectedLossDate = MainViewModel.Instance.GetStringResource("AllDatesText");

            selectedPolicyType = MainViewModel.Instance.GetStringResource("AllTypesText");

            //selectedAssignedTo = MainViewModel.Instance.CurrentUserInfo.UserName;

            selectedSortingField = MainViewModel.Instance.GetStringResource("ClaimNumberText");

            List<Code> claimStatusList = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.CLAIM_STATUS);
            selectedClaimStatus = openClaimStatusCode = claimStatusList.FirstOrDefault(code => code.ShortCode.Equals("O"));
            selectedClaimStatus = new Code()
            {
                Id = -1,
                CodeId = string.Empty,
                Description = MainViewModel.Instance.GetStringResource("AllText"),
                ShortCode = MainViewModel.Instance.GetStringResource("AllText"),
            };
            claimStatusList.Insert(0, selectedClaimStatus);
            ClaimStatusList = claimStatusList;

            RaisePropertyChanged("SelectedLossDate");
            RaisePropertyChanged("SelectedPolicyType");
            RaisePropertyChanged("SelectedSortingField");
            RaisePropertyChanged("SelectedClaimStatus");
        }

        public async Task LoadData(List<Claim> claimItems)
        {
            await InitializeFilterValues();
            AllClaim = claimItems;
            foreach (var claim in claimItems)
            {
                if (claim.LossDate != DateTime.MinValue && !DateOfLossList.Contains(claim.LossDate.ToString("MM/dd/yyyy")))
                    DateOfLossList.Add(claim.LossDate.ToString("MM/dd/yyyy"));

                if (claim.PolicyName != string.Empty && !PolicyTypeList.Contains(claim.PolicyName))
                    PolicyTypeList.Add(claim.PolicyName);
            }
            AddToCollection(claimItems.OrderBy(claim => claim.ClaimNumber).ToList());
        }

        public async Task AddToCollection(List<Claim> claimItems)
        {
            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
           {
               Collection.Clear();
               foreach (Claim item in claimItems)
               {
                   ClaimViewModel cscClaimVM = new ClaimViewModel();
                   cscClaimVM.CscClaim = item;
                   cscClaimVM.DisplayName = string.Format("{0} {1}", item.ClaimantFirstName, item.ClaimantLasttName);
                   Collection.Add(cscClaimVM);
               }
               RaisePropertyChanged("Count");
               RaisePropertyChanged("IsNoRecordTextVisible");


           });
        }

        private void RefreshData()
        {
            if (selectedLossDate == null || selectedPolicyType == null || selectedClaimStatus == null || selectedSortingField == null)
                return;

            IAsyncAction asyncAction = MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
         {
             var filteredClaims = AllClaim.Where(claim =>
                  (selectedLossDate.Equals(MainViewModel.Instance.GetStringResource("AllDatesText")) || claim.LossDate.ToString("MM/dd/yyyy").Equals(selectedLossDate))
                  &&
                  (selectedPolicyType.Equals(MainViewModel.Instance.GetStringResource("AllTypesText")) || claim.PolicyName.Equals(selectedPolicyType))
                  &&
                   (selectedClaimStatus.Description.Equals(MainViewModel.Instance.GetStringResource("AllText")) || (claim.ClaimStatus != null && claim.ClaimStatus.Equals(selectedClaimStatus.Description)))
                  ).ToList();
             if (selectedSortingField == MainViewModel.Instance.GetStringResource("ClaimNumberText"))
                 filteredClaims = filteredClaims.OrderBy(claim => claim.ClaimNumber).ToList();
             else if (selectedSortingField == MainViewModel.Instance.GetStringResource("ClaimantNameText"))
                 filteredClaims = filteredClaims.OrderBy(claim => claim.ClaimantFirstName).ToList();
             else if (selectedSortingField == MainViewModel.Instance.GetStringResource("EventText"))
                 filteredClaims = filteredClaims.OrderBy(claim => claim.EventNumber).ToList();
             AddToCollection(filteredClaims);
         });
        }

        /// <summary>
        /// Gets ServerClaims and Adds to the DB Gets list to collection
        /// </summary>
        public async Task SetServerClaims(string result)
        {
            var offlineClaims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            var claimsToInsert = ParsingHelper.ParseClaimList(offlineClaims, result);
            if (claimsToInsert != null)
            {
                var sortedclaims = claimsToInsert.OrderByDescending(claim => claim.LossDate).ToList();
                await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Claim>(offlineClaims);
                await MainViewModel.Instance.CscRepository.InsertItemsAsync(claimsToInsert);
                foreach (var topclaim in sortedclaims.Take(10))
                {
                    MainViewModel.Instance.GetClaimDetails(topclaim);
                }
            }
            List<Claim> claimItems = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            AddToCollection(claimItems.OrderByDescending(claim => claim.LossDate).ToList());
        }

        public override async void UnLoad()
        {
            if (AllClaim != null && AllClaim.Count > 0)
                AllClaim.Clear();
            base.UnLoad();
        }

        #endregion
    }
}
