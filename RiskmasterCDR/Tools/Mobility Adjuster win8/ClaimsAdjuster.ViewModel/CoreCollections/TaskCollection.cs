﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.ObjectModel;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
namespace ClaimsAdjuster.ViewModel
{
    public class TaskCollection : ResultItems<TaskViewModel>
    {
        #region Feilds

        private Code selectedTaskType;
        private string selectedSortingField;
        private ObservableCollection<string> sortingFieldList;
        private List<Code> taskTypeList;
        private List<string> claimsNumbers;
        private List<string> eventsNumbers;
        private string selectedClaimsNumbers;
        private string selectedEventsNumbers;

        #endregion

        #region Propertes

        public List<ClaimAdjusterTask> AllTasks { get; set; }

        public List<Code> TaskTypeList
        {
            get
            {
                return taskTypeList;
            }
            set
            {
                taskTypeList = value;
                RaisePropertyChanged("TaskTypeList");
            }
        }

        public Code SelectedTaskType
        {
            get
            {
                return selectedTaskType;
            }
            set
            {
                if (value != null)
                {
                    selectedTaskType = value;
                    RefreshData();
                }
            }
        }

        public ObservableCollection<string> SortingFieldList
        {
            get
            {
                if (sortingFieldList == null)
                {
                    sortingFieldList = new ObservableCollection<string>()            
                    {
                        MainViewModel.Instance.GetStringResource("DueDateText"),
                        MainViewModel.Instance.GetStringResource("TaskTypeText"),
                        MainViewModel.Instance.GetStringResource("ClaimText"),
                        MainViewModel.Instance.GetStringResource("EventText"),
                    };
                }
                return sortingFieldList;
            }
        }

        public string SelectedSortingField
        {
            get
            {
                return selectedSortingField;
            }
            set
            {
                selectedSortingField = value;
             
                RefreshData();
            }
        }

        public bool IsNoRecordTextVisible
        {
            get
            {
                return !MainViewModel.Instance.IsAppBusy && (this.Collection == null || Collection.Count == 0);
            }
        }

        public List<string> ClaimNumbers
        {
            get
            {
                return claimsNumbers;
            }
            set
            {
                claimsNumbers = value;
                RaisePropertyChanged("ClaimNumbers");
            }
        }

        public string SelectedClaimNumber
        {
            get
            {
                return selectedClaimsNumbers;
            }
            set
            {
                if (value != null)
                {
                    selectedClaimsNumbers = value;
                    RefreshData();
                }
            }
        }

        public List<string> EventNumbers
        {
            get
            {
                return eventsNumbers;
            }
            set
            {
                eventsNumbers = value;
                RaisePropertyChanged("EventNumbers");
            }
        }

        public string SelectedEventNumber
        {
            get
            {
                return selectedEventsNumbers;
            }
            set
            {
                if (value != null)
                {
                    selectedEventsNumbers = value;
                    RefreshData();
                }
            }
        }
        #endregion

        #region Constructor

        public TaskCollection()
        {
        }

        #endregion

        #region Methods

        public void InitializeFilterValues()
        {
            selectedSortingField = MainViewModel.Instance.GetStringResource("DueDateText");
            selectedClaimsNumbers = MainViewModel.Instance.GetStringResource("AllClaimsText");
            selectedEventsNumbers = MainViewModel.Instance.GetStringResource("AllEventsText");
            selectedTaskType = new Code()
            {
                Id = -1,
                CodeId = string.Empty,
                Description = MainViewModel.Instance.GetStringResource("AllTypesText"),
                ShortCode = MainViewModel.Instance.GetStringResource("AllTypesText"),
            };

            RaisePropertyChanged("SelectedSortingField");
            RaisePropertyChanged("SelectedClaimNumber");
            RaisePropertyChanged("SelectedEventNumber");
            RaisePropertyChanged("SelectedTaskType");

        }

        public async Task LoadData(List<ClaimAdjusterTask> tasktems, int claimViewid = 0)//mbahl3 Mits 36145 
        {
            InitializeFilterValues();
            AllTasks = tasktems;
            var taskTypes = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.WPA_ACTIVITIES);
            //TaskTypeList=taskTypeCodes.Select(dbCode => dbCode.Description).ToList();
            taskTypes.Insert(0, selectedTaskType);
            TaskTypeList = taskTypes;
            List<Claim> claims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            List<Event> events = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
            List<string> claimNumbers = claims.Select(s => s.ClaimNumber).ToList();
            claimNumbers.Insert(0, MainViewModel.Instance.GetStringResource("AllClaimsText"));
            ClaimNumbers = claimNumbers;
            List<string> eventNumbers = events.Select(s => s.EventNumber).ToList();
            eventNumbers.Insert(0, MainViewModel.Instance.GetStringResource("AllEventsText"));
            EventNumbers = eventNumbers;
            AddToCollection(tasktems.OrderBy(task => task.TaskDateTime).ToList(), claimViewid); //mbahl3 Mits 36145
        }



        public async Task AddToCollection(List<ClaimAdjusterTask> taskItems, int claimViewid = 0) //mbahl3 Mits 36145
        {
            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
             {
                 Collection.Clear();
                 foreach (ClaimAdjusterTask item in taskItems)
                 {
                     TaskViewModel cscTaskViewModel = new TaskViewModel();
                     cscTaskViewModel.CscTask = item;
                     cscTaskViewModel.ClaimViewId = claimViewid; //mbahl3 Mits 36145
                     Collection.Add(cscTaskViewModel);
                 }
                 RaisePropertyChanged("Count");
                 RaisePropertyChanged("IsNoRecordTextVisible");
             });
        }

        public async void RefreshData()
        {

            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {

                MainViewModel.Instance.SetAppBusy(Constants.State_Filtering, true);
                
                RefreshDataSet();

            });

        }

        public void RefreshDataSet()
        {
            List<CustomFilter> strbQuery = new List<CustomFilter>();
            
            var filteredTasks = AllTasks;
            SortUtility<ClaimAdjusterTask> sortUtil = new SortUtility<ClaimAdjusterTask>();

            if (selectedTaskType != null && !selectedTaskType.Description.Equals(MainViewModel.Instance.GetStringResource("AllTypesText")))
            {
                //strbQuery.Append(string.Format("TaskType.Contains({0})", SelectedTaskType.Description));
                //filteredTasks = filteredTasks.Where(tasks => tasks.TaskType.Contains(SelectedTaskType.Description)).ToList();
                strbQuery.Add(new CustomFilter("TaskType", SelectedTaskType.Description,WhereOperationType.Contains,false));
            }

            if (SelectedClaimNumber != MainViewModel.Instance.GetStringResource("AllClaimsText"))
            {
                //if (strbQuery.Length >0)
                //    strbQuery.Append(" && ");
                //strbQuery.Append(string.Format("ClaimNumber!=null && ClaimNumber.Equals({0})", SelectedClaimNumber));
                //filteredTasks = filteredTasks.Where(tasks => tasks.ClaimNumber != null && tasks.ClaimNumber.Equals(SelectedClaimNumber)).ToList();
               
                strbQuery.Add(new CustomFilter("ClaimNumber", SelectedClaimNumber, WhereOperationType.Contains,true));
            
            }
            if (SelectedEventNumber != MainViewModel.Instance.GetStringResource("AllEventsText"))
            {
                //if (strbQuery.Length >0)
                //    strbQuery.Append(" && ");
                //strbQuery.Append(string.Format("ClaimNumber!=null && ClaimNumber.Equals({0})", SelectedClaimNumber));
               // filteredTasks = filteredTasks.Where(tasks => tasks.EventNumber != null && tasks.EventNumber.Equals(SelectedEventNumber)).ToList();
               
                strbQuery.Add(new CustomFilter("EventNumber", SelectedEventNumber, WhereOperationType.Equals,true));
               

            }
            if (strbQuery.Count>0)
                filteredTasks= sortUtil.GetFilteredList(filteredTasks, strbQuery);
            if (selectedSortingField == MainViewModel.Instance.GetStringResource("DueDateText"))
                filteredTasks = filteredTasks.OrderBy(task => task.TaskDateTime).ToList();
            else if (selectedSortingField == MainViewModel.Instance.GetStringResource("TaskTypeText"))
                filteredTasks = filteredTasks.OrderBy(task => task.TaskType.Substring(task.TaskType.IndexOf(" "))).ToList();
            else if (selectedSortingField == MainViewModel.Instance.GetStringResource("ClaimText"))
                filteredTasks = sortUtil.GetSortedListMultiple(filteredTasks, "TaskParentType", "ClaimNumber", true);
            else if (selectedSortingField == MainViewModel.Instance.GetStringResource("EventText"))
                filteredTasks = sortUtil.GetSortedListMultiple(filteredTasks, "TaskParentType", "EventNumber", false, true);
            AddToCollection(filteredTasks);

            MainViewModel.Instance.SetAppBusy(Constants.State_Filtering, false);

            RaisePropertyChanged("IsNoRecordTextVisible");
        }
        /// <summary>
        /// Gets ServerTasks and Adds to the DB Gets list to collection
        /// </summary>
        public async Task SetServerTasks(string tasksResult)
        {
            var offlineTasks = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
            var tasksToInsert = ParsingHelper.ParseTaskList(offlineTasks, tasksResult);
            if (tasksToInsert != null)
            {
                offlineTasks = offlineTasks.Where(task => task.Status != (int)DataStatus.Modified && task.Status != (int)DataStatus.SyncFail && task.Status != (int)DataStatus.ClosedOffline).ToList();
                await MainViewModel.Instance.CscRepository.DeleteItemListAsync<ClaimAdjusterTask>(offlineTasks);
                await MainViewModel.Instance.CscRepository.InsertItemsAsync(tasksToInsert);
            }
            List<ClaimAdjusterTask> taskItems = await MainViewModel.Instance.CscRepository.GetTasksByFilteringStatusAsync((int)DataStatus.ClosedOffline);
            AddToCollection(MainViewModel.Instance.ReArrangeTasks(taskItems));

        }

        public override async void UnLoad()
        {
            if (AllTasks != null && AllTasks.Count > 0)
                AllTasks.Clear();
            base.UnLoad();
        }

        #endregion
    }
}
