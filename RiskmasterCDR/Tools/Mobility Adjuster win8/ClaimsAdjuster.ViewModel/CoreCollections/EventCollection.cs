﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class EventCollection : ResultItems<EventViewModel>
    {
        #region Fields

        private List<CustomKeyValuePair<int, string>> sortingList;
        public int selectedSortOption;
        private List<Event> allEvents;
        private List<string> eventsNumbers;
        private string selectedEventsNumbers;
        private string selectedSortingField;
        private ObservableCollection<string> sortingFieldList;

        #endregion

        #region Properties

        public List<CustomKeyValuePair<int, string>> SortingList
        {
            get
            {
                if (sortingList == null)
                {
                    sortingList = new List<CustomKeyValuePair<int, string>>();
                    sortingList.Add(new CustomKeyValuePair<int, string>() { Key = 1, Value = MainViewModel.Instance.GetStringResource("LatestEventText") });
                    sortingList.Add(new CustomKeyValuePair<int, string>() { Key = 2, Value = MainViewModel.Instance.GetStringResource("EventNameText") });
                    sortingList.Add(new CustomKeyValuePair<int, string>() { Key = 3, Value = MainViewModel.Instance.GetStringResource("EventNumberText") });
                }
                return sortingList;
            }
        }

        public int SelectedSortOption
        {
            get
            {
                return selectedSortOption;
            }
            set
            {
                selectedSortOption = value;
                RefreshData();
            }
        }

        public ObservableCollection<string> SortingFieldList
        {
            get
            {
                if (sortingFieldList == null)
                {
                    sortingFieldList = new ObservableCollection<string>()            
                    {
                        MainViewModel.Instance.GetStringResource("LatestEventText"),
                        MainViewModel.Instance.GetStringResource("EventNameText"),
                        MainViewModel.Instance.GetStringResource("EventNumberText"),
                    };
                }
                return sortingFieldList;
            }
        }

        public string SelectedSortingField
        {
            get
            {
                return selectedSortingField;
            }
            set
            {
                if (value != null)
                {
                    selectedSortingField = value;
                    RefreshData();
                }
            }
        }

        public List<string> EventNumbers
        {
            get
            {
                return eventsNumbers;
            }
            set
            {
                eventsNumbers = value;
                RaisePropertyChanged("EventNumbers");
            }
        }

        public string SelectedEventNumber
        {
            get
            {
                return selectedEventsNumbers;
            }
            set
            {
                if (value != null)
                {
                    selectedEventsNumbers = value;
                    RefreshData();
                }
            }
        }

        public bool HasEvents
        {
            get
            {
                return !MainViewModel.Instance.IsAppBusy && Count == 0;
            }
        }

        #endregion

        #region Methods

        public async Task AddToCollection(List<Event> eventItems)
        {
            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
             {
                 Collection.Clear();
                 foreach (Event item in eventItems)
                 {
                     EventViewModel cscEventVM = new EventViewModel();
                     cscEventVM.CscEvent = item;
                     Collection.Add(cscEventVM);
                 }
                 RaisePropertyChanged("Count");
                 RaisePropertyChanged("HasEvents");
             });
        }

        public async Task SetServerEvents(string eventsResult)
        {
            List<Event> events = await ParsingHelper.ParseEventList(eventsResult);
            if (events != null)
            {
                await MainViewModel.Instance.CscRepository.DeleteAllAsync<Event>();
                await MainViewModel.Instance.CscRepository.InsertItemsAsync(events);
            }
            events = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
            AddToCollection(events.OrderBy(s => s.EventDate).ToList());
        }

        /// <summary>
        /// Creates filter values and add's items to Collection
        /// </summary>
        public void LoadData(List<Event> events)
        {
            //InitializeFilterValues();
            allEvents = events;
            List<string> eventNumbers = events.Select(s => s.EventNumber).ToList();
            eventNumbers.Insert(0, MainViewModel.Instance.GetStringResource("AllEventsText"));
            EventNumbers = eventNumbers;
            AddToCollection(events);
        }

        public void InitializeFilterValues()
        {
            //selectedSortOption = SortingList.FirstOrDefault().Key;
            selectedSortingField = MainViewModel.Instance.GetStringResource("LatestEventText");
            selectedEventsNumbers = MainViewModel.Instance.GetStringResource("AllEventsText");

            RaisePropertyChanged("SelectedSortingField");
            RaisePropertyChanged("SelectedEventNumber");
        }

        /// <summary>
        /// Sorts the event items
        /// </summary>
        private void RefreshData()
        {
            List<CustomFilter> strbQuery = new List<CustomFilter>();

            var filteredEvents = allEvents;
            SortUtility<Event> sortUtil = new SortUtility<Event>();

            if (SelectedEventNumber != MainViewModel.Instance.GetStringResource("AllEventsText"))
                strbQuery.Add(new CustomFilter("EventNumber", SelectedEventNumber, WhereOperationType.Equals, true));

            if (strbQuery.Count > 0)
                filteredEvents = sortUtil.GetFilteredList(filteredEvents, strbQuery);

            if (selectedSortingField == MainViewModel.Instance.GetStringResource("LatestEventText"))
                filteredEvents = filteredEvents.OrderByDescending(events => events.EventDate).ToList();
            else if (selectedSortingField == MainViewModel.Instance.GetStringResource("EventNameText"))
                filteredEvents = filteredEvents.OrderBy(events => events.EventName).ToList();
            else if (selectedSortingField == MainViewModel.Instance.GetStringResource("EventNumberText"))
                filteredEvents = filteredEvents.OrderBy(events => events.EventNumber).ToList();
            AddToCollection(filteredEvents);
        }

        public override async void UnLoad()
        {
            if (allEvents != null && allEvents.Count > 0)
                allEvents.Clear();
            base.UnLoad();
        }

        #endregion
    }
}
