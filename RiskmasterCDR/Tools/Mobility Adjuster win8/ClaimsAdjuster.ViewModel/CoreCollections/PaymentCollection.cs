﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Core;
using System.Windows.Input;
using ClaimsAdjuster.ViewModel.Services;

namespace ClaimsAdjuster.ViewModel
{
    public class Headers : ResultItem
    {
        private ICommand sortCommand;

        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }
        public int AscORDesc { get; set; }
        public int Index { get; set; }

        public Headers(string strName, string value, PaymentCollection paymentCollection, int index, bool isSelected = false, int ascORDesc = -1)
        {
            Name = strName;
            Value = value;
            IsSelected = isSelected;
            AscORDesc = ascORDesc;
            PaymentCollection = paymentCollection;
            Index = index;
        }

        public PaymentCollection PaymentCollection { get; set; }

        public ICommand SortCommand
        {
            get
            {
                if (sortCommand == null)
                    sortCommand = new RelayCommand<object>(OnSortCommand);
                return sortCommand;
            }

        }

        public void UnSelect()
        {
            this.IsSelected = false;
            this.AscORDesc = -1;
            RaisePropertyChanged("IsSelected");
            RaisePropertyChanged("AscORDesc");
        }
        private void Select()
        {
            this.IsSelected = true;
            this.AscORDesc = this.AscORDesc * (-1);
            this.PaymentCollection.SelectedHeader = this;
            RaisePropertyChanged("IsSelected");
            RaisePropertyChanged("AscORDesc");
        }
        private async void OnSortCommand(object obj)
        {
            Headers header = obj as Headers;
            Select();

            PaymentCollection.RefreshData(header.Value, this.AscORDesc, header.Index);
        }
    }
    public class PaymentCollection : ResultItems<Payment>
    {
        #region Feilds

        private string selectedPaymentType;
        private string selectedPaymentStatus;
        private ObservableCollection<string> paymentTypes;
        private ObservableCollection<string> paymentStatusList;

        #endregion

        #region Properties

        public Claim CurrentClaim { get; set; }

        public ObservableCollection<Headers> Headers { get; set; }

        public ObservableCollection<Payment> AllPayments { get; set; }

        public ObservableCollection<string> PaymentTypeList
        {
            get
            {
                if (paymentTypes == null)
                    paymentTypes = new ObservableCollection<string>() { MainViewModel.Instance.GetStringResource("AllText") };
                return paymentTypes;
            }
        }

        public string SelectedPaymentType
        {
            get
            {
                return selectedPaymentType;
            }
            set
            {
                selectedPaymentType = value;
                RefreshData(SelectedHeader.Value, SelectedHeader.AscORDesc, SelectedHeader.Index);
            }
        }

        public ObservableCollection<string> PaymentStatusList
        {
            get
            {
                if (paymentStatusList == null)
                    paymentStatusList = new ObservableCollection<string>() { MainViewModel.Instance.GetStringResource("AllText") };
                return paymentStatusList;
            }
        }

        public string SelectedPaymentStatus
        {
            get
            {
                return selectedPaymentStatus;
            }
            set
            {
                selectedPaymentStatus = value;
                RefreshData(SelectedHeader.Value, SelectedHeader.AscORDesc, SelectedHeader.Index);
            }
        }

        public Headers SelectedHeader { get; set; }
        #endregion

        #region Constructors

        public PaymentCollection()
        {
            Headers = new ObservableCollection<Headers>();

        }

        #endregion

        #region Methods

        public  void RefreshData(string param, int ascDesc, int index)
        {
            Collection.Clear();
            bool direction = ascDesc == 1 ? true : false;
            var sortedList = new SortUtility<Payment>().GetSortedList(AllPayments, "CSCPayment", param, direction).ToList();
            //Clear isSelected for other headers 
            //Set direction for current header
            for (int i = 0; i < this.Headers.Count; i++)
            {
                if (i != index)
                {
                    this.Headers[i].UnSelect();
                }

            }


            foreach (var item in sortedList)
            {
                if ((selectedPaymentType == MainViewModel.Instance.GetStringResource("AllText") || (item.CSCPayment != null && item.CSCPayment.PaymentType.Equals(selectedPaymentType)))
                    &&
                    (selectedPaymentStatus == MainViewModel.Instance.GetStringResource("AllText") || (item.CSCPayment != null && item.CSCPayment.PaymentStatus.Equals(selectedPaymentStatus)))
                    )
                {
                    Collection.Add(item);
                }
            }

        }

        public void InitializeFilterValues()
        {
            selectedPaymentType = MainViewModel.Instance.GetStringResource("AllText");

            selectedPaymentStatus = MainViewModel.Instance.GetStringResource("AllText");

        }

        public async Task GetPersonsList(Claim currentClaim, bool isAsyncCall = true)
        {
            try
            {
                string requestXml = string.Format(MobileCommonService.GetPersonsInvolvedList, MainViewModel.Instance.AuthenticationToken, currentClaim.ClaimNumber);
                string personsList = await MobileCommonService.ProcessRequestAsync(requestXml);
                if (isAsyncCall)
                    ParsingHelper.ParsePersonsList(personsList, currentClaim);
                else
                    await ParsingHelper.ParsePersonsList(personsList, currentClaim);
            }
            catch (Exception ex)
            {

            }
        }


        public async Task GetPaymentDetails(Claim currentClaim, bool isAsyncCall = true)
        {
            try
            {
               
                string requestXml = string.Format(MobileCommonService.GetPaymentDetails, MainViewModel.Instance.AuthenticationToken, currentClaim.ClaimNumber);
                string paymentDetails = await MobileCommonService.ProcessRequestAsync(requestXml);
                if (isAsyncCall)
                    ParsingHelper.ParsePaymentHistory(paymentDetails, currentClaim);
                else
                    await ParsingHelper.ParsePaymentHistory(paymentDetails, currentClaim);
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadPayment(List<PaymentHistory> payment, Claim claim)
        {
            InitializeFilterValues();
            Collection.Clear();
            CurrentClaim = claim;
            Headers.Clear();
            SelectedHeader = new Headers("Date", "PaymentDate", this, 0);
            Headers.Add(SelectedHeader);
            Headers.Add(new Headers("Payment Type", "PaymentType", this, 1));
            Headers.Add(new Headers("Payee", "Payee", this, 2));
            Headers.Add(new Headers("Status", "Status", this, 3));
            Headers.Add(new Headers("Check Amount", "Amount", this, 4));
            RaisePropertyChanged("Headers");
            AllPayments = new ObservableCollection<Payment>();
            foreach (var item in payment)
            {
                Payment cscPayment = new Payment();
                cscPayment.CSCPayment = item;

                if (!string.IsNullOrWhiteSpace(item.PaymentType) && !PaymentTypeList.Contains(item.PaymentType))
                    PaymentTypeList.Add(item.PaymentType);
                if (!string.IsNullOrWhiteSpace(item.PaymentStatus) && !PaymentStatusList.Contains(item.PaymentStatus))
                    PaymentStatusList.Add(item.PaymentStatus);
                AllPayments.Add(cscPayment);
                Collection.Add(cscPayment);
            }

            RaisePropertyChanged("Count");
        }


        
        #endregion
    }
}
