﻿using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Core;

namespace ClaimsAdjuster.ViewModel
{
    public class PartyCollection : ResultItems<Party>
    {
        #region Fields

        private List<Code> personInvolvedTypes;
        private List<string> personInvolvedSubTypes;
        private ICommand refreshPartiesInvolved;

        #endregion

        #region Properties
        public Claim CurrentClaim { get; set; }
        
        public List<Code> PersonInvolvedTypes 
        {
            get
            {
                return personInvolvedTypes;
            }
            set
            {
                personInvolvedTypes = value;
                RaisePropertyChanged("PersonInvolvedTypes");
            }
        }

        public List<string> PersonInvolvedSubTypes
        {
            get
            {
                return personInvolvedSubTypes;
            }
            set
            {
                personInvolvedSubTypes = value;
                RaisePropertyChanged("PersonInvolvedSubTypes");
            }
        }

        public List<PartyInvolved> AllPartiesInvolveed { get; set; }
        private Code selectedType;

        public Code SelectedType
        {
            get { return selectedType; }
            set 
            { 
                selectedType = value;
                RefreshData(selectedType);
            }
        }

        public ICommand RefreshPartiesInvolved
        {
            get
            {
                if (refreshPartiesInvolved == null)
                    refreshPartiesInvolved = new RelayCommand(OnRefreshPartiesInvolved);

                return refreshPartiesInvolved;
            }
        }

        public bool IsNoRecordTextVisible
        {
            get
            {
                return !MainViewModel.Instance.IsAppBusy && (this.Collection == null || Collection.Count == 0);
            }
        }

        #endregion

        #region Methods

        public async Task LoadData(string claimNumber)
        {
            Claim claim = await MainViewModel.Instance.CscRepository.GetClaimsByClaimNumberAsync(claimNumber);
            this.CurrentClaim = claim;
            var personTypes = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.PERSON_INV_TYPE);
            var personSubTypes = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.OTHER_PEOPLE);
            var allType=new Code()
            {
                Id = 0,
                CodeId = "-1",
                ShortCode = "",
                Description = MainViewModel.Instance.GetStringResource("AllTypesText"),
            };
            personTypes.Insert(0, allType);
            //[MS] :   Not required as this is not the main type. Not sure why we decided to add this

            //personTypes.Insert(1, new Code() 
            //{ 
            //    Id=1,
            //    CodeId="0",
            //    ShortCode="",
            //    Description = MainViewModel.Instance.GetStringResource("PolicyHolderText"),
            //});
            PersonInvolvedTypes = personTypes;
            PersonInvolvedSubTypes = personSubTypes.Select(pi=>  pi.Description).ToList();
            PopulatePartiesInvolved(claim);

            selectedType = allType;
            RaisePropertyChanged("SelectedType");
        }

        public void AddToCollection(List<PartyInvolved> parties)
        {
            MainViewModel.Instance.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Collection.Clear();
                foreach (PartyInvolved party in parties.OrderBy(party=>party.Name))
                {
                    Party cscPartyVM = new Party();
                    cscPartyVM.CscPartyInvolved = party;
                    cscPartyVM.DisplayName = string.Format("{0} {1}", party.Name, party.EntityType);
                    Collection.Add(cscPartyVM);
                }

                RaisePropertyChanged("IsNoRecordTextVisible");

            });
        }

        public void RefreshData(Code selectedType)
        {
            if (selectedType == null) return;
            if (selectedType.Description.Equals(MainViewModel.Instance.GetStringResource("AllTypesText")))
            {
                AddToCollection(AllPartiesInvolveed);
            }
            else
            {

              
                var filteredParties = AllPartiesInvolveed.Where(party => party.EntityType!=null && party.EntityType.Split(' ')[0].ToLowerInvariant().Equals(selectedType.ShortCode.ToLowerInvariant())).ToList();
                AddToCollection(filteredParties);
            }
           

        }

        private async void PopulatePartiesInvolved(Claim claim)
        {
            AllPartiesInvolveed = await MainViewModel.Instance.CscRepository.GetPartyInvolvedByClaimAsync(claim.ClaimNumber);

            //[MS] :  Not required as this is not the main type. Not sure why we decided to add this
            
            //string name = string.Empty;
            //name = claim.ClaimantLasttName;
            //if (name.Length > 0)
            //    name = name + " , ";
            //name = name + claim.ClaimantLasttName;
            //AllPartiesInvolveed.Insert(0, new PartyInvolved()
            //{
            //    ClaimNumber = claim.ClaimNumber,
            //    ServerEntityID = claim.ClaimantServerEntityID,
            //    EntityType = string.Format("## {0}",MainViewModel.Instance.GetStringResource("PolicyHolderText")),
            //    Name = name,
            //    Address1 = claim.ClaimantAddress1,
            //    Address2 = claim.ClaimantAddress2,
            //    City = claim.ClaimantCity,
            //    State = claim.ClaimantState,
            //    County = claim.ClaimantCounty,
            //    ZipCode = claim.ClaimantZipCode,
            //    EmailAddress = claim.ClaimantEmailAddress,
            //    HomePhone = claim.ClaimantHomePhone,
            //    OfficePhone = claim.ClaimantOfficePhone,
            //});

            AddToCollection(AllPartiesInvolveed);
        }

        private async void OnRefreshPartiesInvolved()
        {
            Collection.Clear();
            
            await MainViewModel.Instance.GetClaimDetails(this.CurrentClaim, false);
            PopulatePartiesInvolved(this.CurrentClaim);
        }

        #endregion
    }
}
