﻿using ClaimsAdjuster.ViewModel.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Validation
{
    public class RequiredIfAttribute : ValidationAttribute
    {
        private const string ClaimProperty = "ClaimNumber";
        private const string EventProperty = "EventNumber";

        private string SwitchProperty { get; set; }
        private Object DesiredValue { get; set; }
        private string ErrorMsg { get; set; }

        public RequiredIfAttribute(string switchProperty, Object desiredValue, string errorMsg)
        {
            this.SwitchProperty = switchProperty;
            this.DesiredValue = desiredValue;
            this.ErrorMsg = errorMsg;
        }

        protected override ValidationResult IsValid(object obj, ValidationContext context)
        {
            string claimNumber = string.Empty;
            string eventNumber = string.Empty;
            PropertyInfo property = null;

            Object instance = context.ObjectInstance;
            IEnumerable<PropertyInfo> properties = instance.GetType().GetTypeInfo().DeclaredProperties;
            
            foreach (PropertyInfo prop in properties)
            {
                if (Convert.ToString(prop).Contains(ClaimProperty))
                    claimNumber = Convert.ToString(prop.GetValue(instance));
                if (Convert.ToString(prop).Contains(EventProperty))
                    eventNumber = Convert.ToString(prop.GetValue(instance));
                if (Convert.ToString(prop).Contains(SwitchProperty))
                    property = prop;
            }

            if (Convert.ToString(property.GetValue(instance)) == Convert.ToString(DesiredValue))
            {
                if (!string.IsNullOrEmpty(claimNumber) || !string.IsNullOrEmpty(eventNumber))
                    return ValidationResult.Success;
                else
                    return new ValidationResult(ErrorMsg);
            }
            return ValidationResult.Success;
        }
    }
}
