﻿
namespace ClaimsAdjuster.ViewModel
{
    public static class Constants
    {
        #region Services

        public const string MobileAuthenticationService = @"{0}/RMService/MobileAuthenticationService.svc/MS";
        public const string MobileCodesService = @"{0}/RMService/MobileCodesService.svc/MS";
        public const string MobileCommonService = @"{0}/RMService/MobileCommonWCFService.svc/MS";
        public const string MobileProgressNoteService = @"{0}/RMService/MobileProgressNoteService.svc/MS";
        public const string MobileUploadService = @"{0}/RMService/MobileUploadService.svc/MS";
        public const string MobileDocumentService = @"{0}/RMService/MobileDocumentService.svc/MS";
        #region ServiceBinding

        public const string MobileAuthenticationService_Binding = "BasicHttpBinding_IMobileAuthenticationService";
        public const string MobileCodesService_Binding = "BasicHttpBinding_IMobileCodesService";
        public const string MobileCommonService_Binding = "BasicHttpBinding_IMobileCommonService";
        public const string MobileProgressNoteService_Binding = "BasicHttpBinding_IMobileProgressNoteService";
        public const string MobileUploadService_Binding = "BasicHttpBinding_IMobileUploadService";
        public const string MobileDocumentService_Binding = "BasicHttpBinding_IMobileDocumentService";

        #endregion

        #endregion

        #region Path

        public const string LocalizationFolderPath = "Resources";

        #endregion

        public const string DefaultAppSettingsId = "ClaimsAdjusterAppSettings";
        public const string PaymentClearedText = "Released";
        public const string PaymentNotClearedText = "Not Released";
        public const int TaskListingDateRange = 10;
        public const string EVENT = "EVENT";
        public const string CLAIM = "CLAIM";
        public const int MaxUploadRetryCount = 3;
        public const string Local = "Local";

        #region ClassIds

        public const string AddTaskClassID = "CscTask_AddTaskClassID";
        public const string AddNoteClassID = "CscTask_AddNoteClassID";

        public const string ID = "ID";
        public const string Type = "TYPE";

        #endregion

        #region Directory Names

        public const string UserDirectoryName = "User Data";
        public const string PreviewImagePath = "ms-appdata:///local/User Data/{0}/{1}/{2}/{3}";
        public const string PhotoFolder = "Pic";
        public const string VideoFolder = "Video";
        public const string AudioFolder = "Audio";
        #endregion

        #region WebUrls

        public const string Settings_AboutClaimsAdjusterUrl = "http://en.wikipedia.org/wiki/Claims_adjuster";
        public const string Settings_AboutCSCUrl = "http://www.csc.com/about_us";
        public const string Settings_TermsAndConditionsUrl = "http://www.csc.com/about_us";
        public const string Settings_PrivacyStatementUrl = "http://www.csc.com/privacy";

        #endregion

        #region AppBusyStateKeys

        public const string State_PageNavigation = "PageNavigation";
        public const string State_Authentication = "Authentication";
        public const string State_Filtering = "Filtering";
        public const string State_SearchLoading = "SearchLoading";

        #endregion

        #region Attachments
        public const string Subject = "Adding Attachments";
        public const string Notes = "Claim number :  ";
        public const string EventNotes = "Event number :  ";
        public const string PictureTypeID = "366";
        public const string VideoTypeID = "365";
        public const string AudioTypeID = "363";
        public const int MaxDuration = 120;
        #endregion

        #region BackgroundTask

        public const string AppVisibilityKey = "IsAppVisibleForeground";

        #endregion

        #region Toast Templates

        public const string ToastDataFilename = "ToastData.xml";

        /// <summary>
        /// Argument passed when toast notification is selected
        /// </summary>
        public const string ToastSelectedArgument = "toast_goto_tasks";

        #endregion

        #region Validation

        public static char[] InvalidCharacters = { '<', '&' };

        #endregion

        #region Page Name

        public const string TasksPage = "TasksPage";
        public const string EventsPage = "EventsPage";
        public const string ClaimsPage = "ClaimsPage";
        public const string MediaCapturPage = "MediaCapturPage";

        #endregion
    }

    public enum SortDirection
    {
        ASC = 1,
        DESC = 2

    }

    public enum FileType
    {
        BITMAP = 1,
        VIDEO = 2,
        AUDIO = 3,
        UNKNOWN

    }
    public enum PinType
    {
        TASK,
        EVENT,
        CLAIM

    }
}
