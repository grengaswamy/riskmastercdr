﻿using CSC.ServiceFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Services
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "MobileDocumentService.IMobileDocumentService")]
    public interface IMobileDocumentService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileDocumentService/CreateDocTypeFromXml_MobAdj", ReplyAction = "http://tempuri.org/IMobileDocumentService/CreateDocTypeFromXml_MobAdjResponse")]

        void CreateDocTypeFromXml_MobAdj(string request);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileDocumentService/CreateDocTypeFromXml_MobAdj", ReplyAction = "http://tempuri.org/IMobileDocumentService/CreateDocTypeFromXml_MobAdjResponse")]
        System.Threading.Tasks.Task CreateDocTypeFromXml_MobAdjAsync(string request);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileDocumentService/MobileDocumentUploadRequest", ReplyAction = "http://tempuri.org/IMobileDocumentService/MobileDocumentUploadRequestResponse")]

        string MobileDocumentUploadRequest(string request);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileDocumentService/MobileDocumentUploadRequest", ReplyAction = "http://tempuri.org/IMobileDocumentService/MobileDocumentUploadRequestResponse")]
        System.Threading.Tasks.Task<string> MobileDocumentUploadRequestAsync(string request);

    }

    public static class MobileDocumentService
    {
        #region Fields
        public static string CreateAttachmentRequest = "<Message><Authorization>{0}</Authorization><Document><MobAdjuster>true</MobAdjuster> <Documents> <CLAIM_NUMBER>{1}</CLAIM_NUMBER> <DocumentUpload filename='{2}' title='{3}' subject='{4}' notes='{5}' type_id='366' type='{6}' content='{7}'/> </Documents></Document></Message>";
        public static string CreateEventAttachmentRequest = "<Message><Authorization>{0}</Authorization><Document><MobAdjuster>true</MobAdjuster>{1}</Document></Message>";
        private static IMobileDocumentService serviceInstance;

        #endregion

        #region Properties

        private static IMobileDocumentService ServiceInstance
        {
            get
            {
                if (serviceInstance == null)
                    serviceInstance = ServiceFactory.Create<IMobileDocumentService>(Constants.MobileDocumentService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileDocumentService));
                return serviceInstance;
            }
        }

        public static void ResetServiceInstance()
        {
            serviceInstance = null;
        }

        #endregion

        #region Methods

        public static async Task<string> MobileDocumentUploadRequestAsync(string request)
        {
            string response = string.Empty;
            try
            {
                response = await ServiceInstance.MobileDocumentUploadRequestAsync(request);
            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return response;
        }

        #endregion

    }


}
