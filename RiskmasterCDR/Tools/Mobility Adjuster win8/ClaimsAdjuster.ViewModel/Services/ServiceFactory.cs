﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace CSC.ServiceFactory
{
    public static class ServiceFactory
    {
        public static T Create<T>(string bindingConfig,string address)
        {
            T context = default(T);
            Binding b = BindingFactory.BasicHttpBinding(bindingConfig);
            b.SendTimeout = new TimeSpan(1, 10, 0);
            b.OpenTimeout = new TimeSpan(1, 10, 0);
            b.ReceiveTimeout = new TimeSpan(1, 10, 0);
            b.CloseTimeout = new TimeSpan(1, 10, 0);
            b.Name = "BasicHttpBinding_Text_MS";
            context = new ChannelFactory<T>(b, new EndpointAddress(address)).CreateChannel(new EndpointAddress(address));
            return context;
        }
        public static class BindingFactory
        {
            
            public static Binding BasicHttpBinding(string configurationName)
            {
                BasicHttpBinding httpBinding = new System.ServiceModel.BasicHttpBinding();
                httpBinding.Name = "BasicHttpBinding_Text_MS";
                httpBinding.TextEncoding = System.Text.Encoding.UTF8;
                httpBinding.TransferMode = TransferMode.Buffered;
                httpBinding.MaxReceivedMessageSize = 2147483647;
                httpBinding.MaxBufferPoolSize = 2147483647;
                httpBinding.AllowCookies = false;
                httpBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
                httpBinding.ReaderQuotas.MaxArrayLength = 2147483647;
                httpBinding.ReaderQuotas.MaxBytesPerRead = 4096;
                httpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
                httpBinding.Security.Mode = BasicHttpSecurityMode.None;
                httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
               
                return (Binding)httpBinding;
            }

            public static Binding CustomHttpBinding(string configurationName)
            {
                CustomBinding binding = new CustomBinding();
                
                binding.Name = configurationName;
                HttpTransportBindingElement httpBinding = new HttpTransportBindingElement();
               // CustomMTOMMessageEncodingBindingElement msgEncoding = new CustomMTOMMessageEncodingBindingElement();
               
                TextMessageEncodingBindingElement msgEncoding = new TextMessageEncodingBindingElement(MessageVersion.Soap11,Encoding.UTF8);
                binding.Elements.Add(msgEncoding);
                binding.Elements.Add(httpBinding);
                return (Binding)binding;
            }
        }

    }


    [ServiceContract]
    public interface ICSCServices
    {

      

    }

}
