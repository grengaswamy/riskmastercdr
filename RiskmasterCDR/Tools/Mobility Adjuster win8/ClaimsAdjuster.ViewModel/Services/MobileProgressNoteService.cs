﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSC.ServiceFactory;
using System.ServiceModel;
using System.Xml.Serialization;

namespace ClaimsAdjuster.ViewModel.Services
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "NoteService.IMobileProgressNoteService")]
    public interface IMobileProgressNoteService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/DoWork", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/DoWorkResponse")]
        void DoWork();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/DoWork", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/DoWorkResponse")]
        System.Threading.Tasks.Task DoWorkAsync();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/LoadProgressNotesPartialForMobileAd" +
            "j", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/LoadProgressNotesPartialForMobileAd" +
            "jResponse")]
        string LoadProgressNotesPartialForMobileAdj(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/LoadProgressNotesPartialForMobileAd" +
            "j", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/LoadProgressNotesPartialForMobileAd" +
            "jResponse")]
        System.Threading.Tasks.Task<string> LoadProgressNotesPartialForMobileAdjAsync(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/SaveNotesForMobileAdj", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/SaveNotesForMobileAdjResponse")]
        string SaveNotesForMobileAdj(string sRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileProgressNoteService/SaveNotesForMobileAdj", ReplyAction = "http://tempuri.org/IMobileProgressNoteService/SaveNotesForMobileAdjResponse")]
        System.Threading.Tasks.Task<string> SaveNotesForMobileAdjAsync(string sRequest);
    }

    #region Requests

    public static class MobileProgressNoteService
    {
        #region Fields

        public static IMobileProgressNoteService serviceInstance;

        public static string CreateNoteRequest = "<Message>   <Authorization>{0}</Authorization>   <Document>     <ProgressNotes>       <FORM_NAME>Add Note</FORM_NAME>       <NEW_RECORD>true</NEW_RECORD>       <CLAIM_NUMBER>{1}</CLAIM_NUMBER>       <ACTIVITY_DATE>{2}</ACTIVITY_DATE>       <NOTE_TEXT>{3}</NOTE_TEXT>       <NOTE_TYPE_CODE>{4}</NOTE_TYPE_CODE>     </ProgressNotes>   </Document> </Message>";

        #endregion

        #region Properties

        private static IMobileProgressNoteService ServiceInstance
        {
            get
            {
                if (serviceInstance == null)
                    serviceInstance = ServiceFactory.Create<IMobileProgressNoteService>(Constants.MobileProgressNoteService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileProgressNoteService));
                return serviceInstance;
            }
        }

        public static void ResetServiceInstance()
        {
            serviceInstance = null;
        }

        #endregion

        #region Methods

        public static async Task<string> SaveNotesForMobileAdjAsync(string request)
        {
            string response = string.Empty;
            try
            {
                response = await ServiceInstance.SaveNotesForMobileAdjAsync(request);
            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return response;
        }

        #endregion
    }

    #endregion
}
