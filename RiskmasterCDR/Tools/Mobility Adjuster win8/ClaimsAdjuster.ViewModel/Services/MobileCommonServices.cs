﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSC.ServiceFactory;
using System.ServiceModel;
using System.Xml.Serialization;
using Windows.Data.Xml;
using Windows.Storage;
using Windows.Data.Xml.Dom;
namespace ClaimsAdjuster.ViewModel.Services
{

    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://csc.com/Riskmaster/Webservice/Common")]
    public interface IMobileCommonWCFService : ICSCServices
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://csc.com/Riskmaster/Webservice/Common/IMobileCommonWCFService/ProcessReques" +
            "t", ReplyAction = "http://csc.com/Riskmaster/Webservice/Common/IMobileCommonWCFService/ProcessReques" +
            "tResponse")]
        string ProcessRequest(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://csc.com/Riskmaster/Webservice/Common/IMobileCommonWCFService/ProcessReques" +
            "t", ReplyAction = "http://csc.com/Riskmaster/Webservice/Common/IMobileCommonWCFService/ProcessReques" +
            "tResponse")]
        System.Threading.Tasks.Task<string> ProcessRequestAsync(string xmlRequest);
    }

    #region Requests

    public static class MobileCommonService
    {
        #region Fields

        private static string _claimsRequest;
        public static IMobileCommonWCFService serviceInstance;

        public static string GetAuthRequest = " <Message> <Call> <Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>{0}</UserName><DSNName>{1}</DSNName><Password>{2}</Password></UserInfo></Authorization></Document></Message>";
        public static string GetClaimsRequest = "<Message><Authorization>{0}</Authorization><Call><Function>SearchAdaptor.SearchRun</Function></Call><Document><Search><caller>MobilityAdjusterClaimList</caller><PageNo /><TotalRecords /><SortColPass /><LookUpId>0</LookUpId><PageSize>10</PageSize><MaxResults>500</MaxResults><SortColumn /><Order>ascending</Order><IfBack /><SearchMain><IfLookUp>0</IfLookUp><InProcess>0</InProcess><ScreenFlag>1</ScreenFlag><ViewId>-1000</ViewId><FormName>claim</FormName><FormNameTemp /><TableRestrict></TableRestrict><codefieldrestrict></codefieldrestrict><codefieldrestrictid></codefieldrestrictid><tablename></tablename><rowid></rowid><eventdate></eventdate><claimdate></claimdate><policydate></policydate><filter></filter><SysEx /><Settings /><TableID /><SoundEx>-1</SoundEx><DefaultSearch /><DefaultViewId /><Views /><CatId>2</CatId><DisplayFields><OrderBy1></OrderBy1><OrderBy2></OrderBy2><OrderBy3></OrderBy3></DisplayFields><SearchCat /><AdmTable /><EntityTableId /><Type /><LookUpType /><SearchFields><field fieldtype=\"1\" id=\"FLD100\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_NUMBER\" type=\"text\" tabindex=\"1\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD8501\" issupp=\"0\" table=\"POLICY\" name=\"POLICY_NAME\" type=\"text\" tabindex=\"2\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD1001\" issupp=\"0\" table=\"CLAIMANT_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"3\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD1000\" issupp=\"0\" table=\"CLAIMANT_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"4\"><Operation>=</Operation></field><field fieldtype=\"7\" id=\"FLD101\" issupp=\"0\" table=\"CLAIM\" name=\"DATE_OF_CLAIM\" type=\"date\" tabindex=\"5\"><Operation>between</Operation><StartDate></StartDate><EndDate></EndDate></field><field fieldtype=\"3\" id=\"FLD104\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_STATUS_CODE\" codetable=\"CLAIM_STATUS\" type=\"code\" tabindex=\"6\"><Operation>=</Operation><_cid></_cid><_tableid>CLAIM</_tableid></field><field fieldtype=\"1\" id=\"FLD2001\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"7\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD2000\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"8\"><Operation>=</Operation></field></SearchFields><SelOrgLevels></SelOrgLevels><SettoDefault>-1</SettoDefault><AllowPolicySearch></AllowPolicySearch></SearchMain></Search></Document></Message>";

        public static string GetClaimsDetail = "<Message>   <Authorization>{0}</Authorization>   <Call>     <Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function>   </Call>   <Document>     <ExecutiveSummaryReport>       <caller>MobilityAdjuster</caller>       <ClaimNumber>{1}</ClaimNumber>       <File></File>     </ExecutiveSummaryReport>   </Document> </Message>";
        public static string GetEventsList = "<Message><Authorization>{0}</Authorization><Call><Function>SearchAdaptor.SearchRun</Function></Call><Document><Search><caller>MobilityAdjusterEventList</caller><PageNo /><TotalRecords /><SortColPass /><LookUpId>0</LookUpId><PageSize>10</PageSize><MaxResults>500</MaxResults><SortColumn /><Order>ascending</Order><IfBack /><SearchMain><IfLookUp>0</IfLookUp><InProcess>0</InProcess><ScreenFlag>1</ScreenFlag><ViewId>-1002</ViewId><FormName>event</FormName><FormNameTemp /><TableRestrict></TableRestrict><codefieldrestrict></codefieldrestrict><codefieldrestrictid></codefieldrestrictid><tablename></tablename><rowid></rowid><eventdate></eventdate><claimdate></claimdate><policydate></policydate><filter></filter><SysEx /><Settings /><TableID /><SoundEx>-1</SoundEx><DefaultSearch /><DefaultViewId /><Views /><CatId>2</CatId><DisplayFields><OrderBy1></OrderBy1><OrderBy2></OrderBy2><OrderBy3></OrderBy3></DisplayFields><SearchCat /><AdmTable /><EntityTableId /><Type /><LookUpType /><SearchFields><field fieldtype=\"1\" id=\"FLD70370\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"1\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD70371\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"2\"><Operation>=</Operation></field><field fieldtype=\"3\" id=\"FLD70369\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_STATUS_CODE\" codetable=\"\" type=\"code\" tabindex=\"3\"><Operation>=</Operation><_cid></_cid><_tableid>CLAIM</_tableid></field></SearchFields><SelOrgLevels></SelOrgLevels><SettoDefault>-1</SettoDefault><AllowPolicySearch></AllowPolicySearch></SearchMain></Search></Document></Message>";
        public static string GetTaskRequest = "<Message>   <Authorization>{0}</Authorization>   <Call>     <Function>WPAAdaptor.DiaryList</Function>   </Call>   <Document>     <DiaryList assigneduser=\"{1}\" fromdate=\"\" sortby=\"5\" taskdesc=\"\" todate=\"\" usetaskdesc=\"True\" claimnumber=\"\">       <caller>MobilityAdjuster</caller>     </DiaryList>   </Document> </Message>";
        public static string CreateTaskRequest = "<Message>  <Authorization>{0}</Authorization>  <Call>    <Function>WPAAdaptor.SaveDiary</Function>  </Call>  <Document>    <SaveDiary>      <caller>MobilityAdjuster</caller>      <EntryId></EntryId>      <TaskSubject>{1}</TaskSubject>      <AssigningUser>currentuser</AssigningUser>      <AssignedUser>{2}</AssignedUser>      <StatusOpen>1</StatusOpen>      <TeTracked></TeTracked>      <TeTotalHours></TeTotalHours>      <TeExpenses></TeExpenses>      <TeStartTime></TeStartTime>      <TeEndTime></TeEndTime>      <ResponseDate></ResponseDate>      <Response></Response>      <TaskNotes>{3}</TaskNotes>      <DueDate>{4}</DueDate>      <CompleteTime>{5}</CompleteTime>      <EstimateTime></EstimateTime>      <Priority></Priority>      <ActivityString>{6}</ActivityString>      <AutoConfirm></AutoConfirm>      <NotifyFlag></NotifyFlag>      <EmailNotifyFlag></EmailNotifyFlag>      <OverDueDiaryNotify></OverDueDiaryNotify>      <AttachRecordId>{7}</AttachRecordId>      <AttachTable>{8}</AttachTable>      <Action>Create</Action>    </SaveDiary>  </Document></Message>";
        public static string GetPaymentDetails = "<Message><Authorization>{0}</Authorization><Call><Function>FundManagementAdaptor.GetPaymentHistory</Function></Call><Document><PaymentHistory><ClaimId>1</ClaimId><caller>MobilityAdjuster</caller><ClaimNumber>{1}</ClaimNumber><FrozenFlag>0</FrozenFlag><SubTitle></SubTitle><ClaimantEid>0</ClaimantEid><UnitId></UnitId><SortCol></SortCol><Ascending></Ascending><FormTitle></FormTitle><PaymentsLessVoids></PaymentsLessVoids><TransId></TransId><ShowAutoCheckbtn></ShowAutoCheckbtn><Title></Title><EntityId></EntityId><NextPageNumber>0</NextPageNumber><FilterColumn></FilterColumn><FilterValue></FilterValue><TotalPay></TotalPay><TotalCollect></TotalCollect><TotalAll></TotalAll><TotalVoid></TotalVoid></PaymentHistory></Document></Message>";

        public static string GetPersonsInvolvedList = "<Message> <Authorization>{0}</Authorization><Call> <Function>SearchAdaptor.SearchRun</Function></Call><Document><Search><caller>MobileAdjuster</caller><PageNo /><TotalRecords /><SortColPass/><LookUpId>0</LookUpId><PageSize>10</PageSize><MaxResults>500</MaxResults><SortColumn /><Order>ascending</Order><IfBack /><SearchMain><IfLookUp>0</IfLookUp><InProcess>0</InProcess><ScreenFlag>1</ScreenFlag><ViewId>-102</ViewId><FormName>claim</FormName><FormNameTemp /><TableRestrict></TableRestrict><codefieldrestrict></codefieldrestrict><codefieldrestrictid></codefieldrestrictid><tablename></tablename><rowid></rowid><eventdate></eventdate><claimdate></claimdate><policydate></policydate><filter></filter><SysEx /><Settings /><TableID /><SoundEx></SoundEx><DefaultSearch /><DefaultViewId /><Views /><CatId>2</CatId><DisplayFields><OrderBy1></OrderBy1><OrderBy2></OrderBy2><OrderBy3></OrderBy3></DisplayFields><SearchCat /><AdmTable /><EntityTableId /><Type /><LookUpType /><SearchFields><field fieldtype=\"1\" id=\"FLD100\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_NUMBER\" type=\"text\" tabindex=\"1\"><Operation>=</Operation>{1}</field></SearchFields><SelOrgLevels></SelOrgLevels><SettoDefault>-1</SettoDefault><AllowPolicySearch></AllowPolicySearch></SearchMain></Search></Document></Message>";
        #endregion

        #region Properties

        private static IMobileCommonWCFService ServiceInstance
        {
            get
            {
                if (serviceInstance == null)
                    serviceInstance = ServiceFactory.Create<IMobileCommonWCFService>(Constants.MobileCommonService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileCommonService));
                return serviceInstance;
            }
        }

        #endregion

        #region Methods

        public static async Task<string> ProcessRequestAsync(string request)
        {
            string result = string.Empty;
            try
            {
                result = await ServiceInstance.ProcessRequestAsync(request);

            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return result;
        }

        public static void ResetServiceInstance()
        {
            serviceInstance = null;
        }

        #endregion
    }

    #endregion

}
