﻿using ClaimsAdjuster.ViewModel.Repository;
using CSC.ServiceFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClaimsAdjuster.ViewModel.Services
{
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "CodesService.IMobileCodesService")]
    public interface IMobileCodesService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileCodesService/GetCodesForMobileAdj", ReplyAction = "http://tempuri.org/IMobileCodesService/GetCodesForMobileAdjResponse")]
        string GetCodesForMobileAdj(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileCodesService/GetCodesForMobileAdj", ReplyAction = "http://tempuri.org/IMobileCodesService/GetCodesForMobileAdjResponse")]
        System.Threading.Tasks.Task<string> GetCodesForMobileAdjAsync(string xmlRequest);
    }

    public class MobileCodesService
    {
        #region Fields

        private static IMobileCodesService serviceInstance;

        public static string GetCodesRequest = "<Message>  <Authorization>{0}</Authorization>  <Document>    <CodeTables>      <CodeTable name=\"STATES\"  formname=\"search\" ></CodeTable>      <CodeTable name=\"NOTE_TYPE_CODE\"  formname=\"search\" ></CodeTable>      <CodeTable name=\"WPA_ACTIVITIES\"  formname=\"search\" ></CodeTable>       <CodeTable name=\"PERSON_INV_TYPE\"  formname=\"search\" ></CodeTable> <CodeTable name=\"CLAIM_STATUS\"  formname=\"search\" ></CodeTable>  <CodeTable name=\"OTHER_PEOPLE\"  formname=\"search\" ></CodeTable>   </CodeTables>  </Document></Message>";

        #endregion

        #region Properties

        private static IMobileCodesService ServiceInstance
        {
            get
            {
                if (serviceInstance == null)
                    serviceInstance = ServiceFactory.Create<IMobileCodesService>(Constants.MobileCodesService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileCodesService));
                return serviceInstance;
            }
        }

        #endregion

        #region Methods

        public static async Task<string> GetCodesForMobileAdjAsync(string request)
        {
            string response = string.Empty;
            try
            {
                response = await ServiceInstance.GetCodesForMobileAdjAsync(request);
            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return response;
        }

        public static void ResetServiceInstance()
        {
            serviceInstance = null;
        }

        /// <summary>
        /// Inserts parsed items to DB
        /// </summary>
        /// <param name="response"></param>
        public static async void ParseCodesData(string response)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(response);
            }
            catch
            {
                return;
            }
            List<XElement> codeTables = xDoc.Descendants("CodeTable").ToList();
            if (codeTables != null && codeTables.Count > 0)
            {
                List<Code> codes = new List<Code>();
                await MainViewModel.Instance.CscRepository.DeleteAllAsync<Code>();//TODO: check user field before removing items
                foreach (XElement codeTable in codeTables)
                {

                    CodeType codeTableType = (CodeType)CodeType.Parse(typeof(CodeType), codeTable.Attribute("name").Value);
                    if (codeTableType != CodeType.None)
                    {
                        List<XElement> codesList = codeTable.Descendants("code").ToList();
                        foreach (XElement codeElement in codesList)
                        {
                            var codeId = codeElement.Attribute("codeid");
                            if (codeId != null)//To avoid Items without codeid
                            {
                                Code code = new Code() { CodeType = codeTableType };
                                if (code.CodeType == CodeType.OTHER_PEOPLE)
                                {
                                    code.ShortCode = "";
                                }
                                else
                                code.ShortCode = codeElement.Attribute("shortcode").Value;
                                code.Description = codeElement.Attribute("desc").Value;
                                code.CodeId = codeId.Value;
                                code.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                                codes.Add(code);
                            }
                        }
                    }

                }
                await MainViewModel.Instance.CscRepository.InsertItemsAsync(codes);
            }
        }

        #endregion

    }
}
