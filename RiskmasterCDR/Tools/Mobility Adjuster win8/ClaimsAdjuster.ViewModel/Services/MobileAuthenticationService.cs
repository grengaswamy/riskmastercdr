﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSC.ServiceFactory;
using System.ServiceModel;
using System.Xml.Serialization;
using ClaimsAdjuster.ViewModel.Repository;

namespace ClaimsAdjuster.ViewModel.Services
{

    [ServiceContract]
    public interface IMobileAuthenticationService : ICSCServices
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileAuthenticationService/AuthenticateMobileUser", ReplyAction = "http://tempuri.org/IMobileAuthenticationService/AuthenticateMobileUserResponse")]
        string AuthenticateMobileUser(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileAuthenticationService/AuthenticateMobileUser", ReplyAction = "http://tempuri.org/IMobileAuthenticationService/AuthenticateMobileUserResponse")]
        System.Threading.Tasks.Task<string> AuthenticateMobileUserAsync(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileAuthenticationService/GetUserDSNsMobileAdjuster", ReplyAction = "http://tempuri.org/IMobileAuthenticationService/GetUserDSNsMobileAdjusterResponse" +
            "")]
        string GetUserDSNsMobileAdjuster(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileAuthenticationService/GetUserDSNsMobileAdjuster", ReplyAction = "http://tempuri.org/IMobileAuthenticationService/GetUserDSNsMobileAdjusterResponse" +
            "")]
        System.Threading.Tasks.Task<string> GetUserDSNsMobileAdjusterAsync(string xmlRequest);

    }

    public static class MobileAuthenticationService
    {
        #region Methods

        private static IMobileAuthenticationService GetServiceInstance()
        {
            return ServiceFactory.Create<IMobileAuthenticationService>(Constants.MobileAuthenticationService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileAuthenticationService));
        }

        public static async Task<string> GetUserDSNsMobileAdjusterAsync(string request)
        {
            string response = string.Empty;
            try
            {
                IMobileAuthenticationService client = GetServiceInstance();
                response = await client.GetUserDSNsMobileAdjusterAsync(request);
            }
            catch (EndpointNotFoundException)
            {
                response = string.Empty;
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("InvalidWebServerUrlMessage");
            }
            catch (Exception)
            {
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedText");
                MainViewModel.Instance.AuthenticationVM.IsSigningIn = false;
            }
            return response;
        }

        public static async Task<string> AuthenticateMobileUserAsync(string request)
        {
            string response = string.Empty;
            try
            {
                IMobileAuthenticationService client = GetServiceInstance();
                response = await client.AuthenticateMobileUserAsync(request);

            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return response;
        }

        #endregion
    }

    public class Message
    {

        public Call Call { get; set; }
        public Document Document { get; set; }
        [XmlElement(IsNullable = true)]
        public string Authorization { get; set; }


    }
    public class Call
    {
        public string Function { get; set; }

    }

    public class Document
    {
        public Authorization Authorization { get; set; }

        public bool MobAdjuster { get; set; }
        [XmlElement(IsNullable = true)]
        public Documents Documents { get; set; }




    }

    public class Documents
    {
        public string CLAIM_NUMBER { get; set; }
        public string EVENT_NUMBER { get; set; }
        [XmlElement(IsNullable = true)]
        public List<DocumentUpload> DocumentUpload { get; set; }


    }

    public class DocumentUpload
    {

        [XmlAttribute]
        public string filename { get; set; }
        [XmlAttribute]
        public string title { get; set; }
        [XmlAttribute]
        public string subject { get; set; }
        [XmlAttribute]
        public string notes { get; set; }
        [XmlAttribute]
        public string type_id { get; set; }
        [XmlAttribute]
        public string type { get; set; }
        [XmlAttribute]
        public string content { get; set; }



    }
    public class Authorization
    {
        public User UserInfo { get; set; }
    }

    [XmlRoot("ResultMessage")]
    public class ResultMessage<T>
    {
        [XmlElement("MsgStatus")]
        public Status Status { get; set; }
        public T Document { get; set; }
    }

    public class Status
    {
        [XmlElement("MsgStatusCd")]
        public StatusCodes StatusCode { get; set; }
    }

    public class DSNDocument
    {
        [XmlArrayItemAttribute("DSNName")]
        public string[] DSNList { get; set; }
    }

    public class TokenDocument
    {
        [XmlElement("result")]
        public TokenResult Result { get; set; }
    }

    public class TokenResult
    {
        public string Token { get; set; }
    }
}
