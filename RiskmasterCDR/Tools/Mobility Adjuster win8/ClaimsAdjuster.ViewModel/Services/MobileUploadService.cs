﻿using CSC.ServiceFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Services
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "UploadService.IMobileUploadService")]
    public interface IMobileUploadService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/DoWork", ReplyAction = "http://tempuri.org/IMobileUploadService/DoWorkResponse")]
        void DoWork();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/DoWork", ReplyAction = "http://tempuri.org/IMobileUploadService/DoWorkResponse")]
        System.Threading.Tasks.Task DoWorkAsync();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/UploadRequest", ReplyAction = "http://tempuri.org/IMobileUploadService/UploadRequestResponse")]
        string UploadRequest(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/UploadRequest", ReplyAction = "http://tempuri.org/IMobileUploadService/UploadRequestResponse")]
        System.Threading.Tasks.Task<string> UploadRequestAsync(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/ProcessRequest", ReplyAction = "http://tempuri.org/IMobileUploadService/ProcessRequestResponse")]
        string ProcessRequest(string xmlRequest);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IMobileUploadService/ProcessRequest", ReplyAction = "http://tempuri.org/IMobileUploadService/ProcessRequestResponse")]
        System.Threading.Tasks.Task<string> ProcessRequestAsync(string xmlRequest);

    }

    public static class MobileUploadService
    {
        #region Fields

        private static IMobileUploadService serviceInstance;

        public static string TaskCloseUploadRequest = "<Upload>  <Save>    <Type>updatediary</Type>    <Message>      <Authorization>{0}</Authorization>      <Call>        <Function>WPAAdaptor.SaveDiary</Function>        <MultipleCalls>true</MultipleCalls>      </Call>      <Document>        <SaveDiary>          <DiaryUpload>true</DiaryUpload>          <MobAdjuster>true</MobAdjuster>                    <DiaryAction>Complete</DiaryAction>          <EntryId>{1}</EntryId>          <TaskSubject></TaskSubject>          <AssigningUser></AssigningUser>          <StatusOpen>false</StatusOpen>          <DueDate></DueDate>          <CompleteTime></CompleteTime>          <EstimateTime></EstimateTime>          <Priority></Priority>          <AutoConfirm>false</AutoConfirm>          <NotifyFlag>false</NotifyFlag>          <TaskNotes></TaskNotes>          <ActivityString></ActivityString>          <Action>Edit</Action>        </SaveDiary>        {2}      </Document>    </Message>  </Save></Upload>";
        public static string UploadDocumentElementRequest = "<DocumentUpload filename=\"{0}\" title=\"{1}\" subject=\"{2}\" notes=\"{3}\" type_id=\"{4}\" type=\"{5}\" content=\"{6}\"/>";

        #endregion

        #region Properties

        private static IMobileUploadService ServiceInstance
        {
            get
            {
                if (serviceInstance == null)
                    serviceInstance = ServiceFactory.Create<IMobileUploadService>(Constants.MobileUploadService_Binding, MainViewModel.Instance.GetServiceAddress(Constants.MobileUploadService));
                return serviceInstance;
            }
        }

        public static void ResetServiceInstance()
        {
            serviceInstance = null;
        }

        #endregion

        #region Methods

        public static async Task<string> UploadRequestAsync(string request)
        {
            string response = string.Empty;
            try
            {
                response = await ServiceInstance.UploadRequestAsync(request);
            }
            catch (EndpointNotFoundException)
            {
                MainViewModel.Instance.ShowServiceUnavailableMessage();
            }
            catch (Exception)
            {
                //To catch unhandled exception while executing service    
            }
            return response;
        }

        #endregion

    }
}
