﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Validation;
using ClaimsAdjuster.ViewModel.Validation.Interfaces;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ClaimsAdjuster.ViewModel
{
    /// <summary>
    /// Base class for claims adjuster app
    /// </summary>
    public class ClaimsAdjuster : ResultItem, IValidatableBindableBase
    {

        #region Fields

        private ICommand launchMapCommand;
        private ICommand launchCalendarCommand;
        private ICommand launchSkypeCommand;
        private ICommand launchMailCommand;
        private ICommand navigateToMediaListingsPage;
        private ICommand navigateToTasksCommand;
        private ObservableCollection<MediaViewModel> pictures;
        private ObservableCollection<Payment> payment;
        private ObservableCollection<MediaViewModel> videos;
        private ObservableCollection<MediaViewModel> audios;

        #endregion

        #region Constructor

        public ClaimsAdjuster()
        {

        }

        #endregion

        #region Commands

        /// <summary>
        /// Command for launching Map app.
        /// </summary>
        public ICommand LaunchMapCommand
        {
            get
            {
                if (launchMapCommand == null)
                {
                    Func<bool> mapLaunchEnabled = () => HasFullAddress;
                    launchMapCommand = new RelayCommand(OnLaunchMapCommand, mapLaunchEnabled);
                }
                return launchMapCommand;
            }
        }

        /// <summary>
        /// Command for launching Calendar app.
        /// </summary>
        public ICommand LaunchCalendarCommand
        {
            get
            {
                if (launchCalendarCommand == null)
                    launchCalendarCommand = new RelayCommand(OnLaunchCalendarCommand);
                return launchCalendarCommand;
            }
        }

        /// <summary>
        /// Command for launching Skype app.
        /// </summary>
        public ICommand LaunchSkypeCommand
        {
            get
            {
                if (launchSkypeCommand == null)
                {
                    Func<bool> skypeLaunchEnabled = () => HasPhoneNumber;
                    launchSkypeCommand = new RelayCommand(OnLaunchSkypeCommand, skypeLaunchEnabled);
                }
                return launchSkypeCommand;
            }
        }

        /// <summary>
        /// Command for launching Mail app.
        /// </summary>
        public ICommand LaunchMailCommand
        {
            get
            {
                if (launchMailCommand == null)
                {
                    Func<bool> mailappLaunchEnabled = () => HasEmailId;
                    launchMailCommand = new RelayCommand(OnLaunchMailCommand, mailappLaunchEnabled);
                }
                return launchMailCommand;
            }
        }

        /// <summary>
        /// Command for navigating to tasks page
        /// </summary>
        public ICommand NavigateToTasksCommand
        {
            get
            {
                if (navigateToTasksCommand == null)
                    navigateToTasksCommand = new RelayCommand(OnNavigateToTasksCommand);
                return navigateToTasksCommand;
            }
        }

        public virtual ICommand NavigateToMediaListingsPage
        {
            get
            {
                if (navigateToMediaListingsPage == null)
                    navigateToMediaListingsPage = new RelayCommand<string>(OnNavigateToMediaListingsPage);
                return navigateToMediaListingsPage;
            }
        }
        #endregion

        #region Command Members

        /// <summary>
        /// Command logic for launching Map app.
        /// </summary>
        protected virtual void OnLaunchMapCommand()
        {
            LaunchHelper.LaunchMap(string.Empty);
        }

        /// <summary>
        /// Command logic for launching Calendar app.
        /// </summary>
        protected virtual void OnLaunchCalendarCommand()
        {
            LaunchHelper.LaunchAppointment();
        }

        /// <summary>
        /// Command logic for launching Skype app.
        /// </summary>
        protected virtual void OnLaunchSkypeCommand()
        {
            LaunchHelper.LaunchSkype();
        }

        /// <summary>
        /// Command logic for launching Mail app.
        /// </summary>
        protected virtual void OnLaunchMailCommand()
        {
            LaunchHelper.LaunchMail();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the pictures captured against the claims
        /// </summary>
        public ObservableCollection<MediaViewModel> Pictures
        {
            get
            {
                if (pictures == null)
                    pictures = new ObservableCollection<MediaViewModel>();
                return pictures;
            }
        }

        public ObservableCollection<Payment> Payment
        {
            get
            {
                if (payment == null)
                    payment = new ObservableCollection<Payment>();
                return payment;
            }
        }

        /// <summary>
        /// Gets or sets the videos recorded against the claims
        /// </summary>
        public ObservableCollection<MediaViewModel> Videos
        {
            get
            {
                if (videos == null)
                    videos = new ObservableCollection<MediaViewModel>();
                return videos;
            }
        }

        /// <summary>
        /// Gets or sets the Audios recorded against the claims
        /// </summary>
        public ObservableCollection<MediaViewModel> Audios
        {
            get
            {
                if (audios == null)
                    audios = new ObservableCollection<MediaViewModel>();
                return audios;
            }
        }

        /// <summary>
        /// Gets whether claim has any pictures
        /// </summary>
        public bool HasPictures
        {
            get
            {
                return this.Pictures.Count > 0;
            }
        }


        public bool HasPayment
        {
            get
            {
                return this.Pictures.Count > 0;
            }
        }

        /// <summary>
        /// Gets whether claim has any Videos
        /// </summary>
        public bool HasVideos
        {
            get
            {
                return this.Videos.Count > 0;
            }
        }

        /// <summary>
        /// Gets whether claim has any Audios
        /// </summary>
        public bool HasAudios
        {
            get
            {
                return this.Audios.Count > 0;
            }
        }

        /// <summary>
        /// Gets count of pictures
        /// </summary>
        public int PicturesCount
        {
            get
            {
                return this.Pictures.Count;
            }
        }

        /// <summary>
        /// Gets count of Videos
        /// </summary>
        public int VideosCount
        {
            get
            {
                return this.Videos.Count ;
            }
        }

        /// <summary>
        /// Gets whether claim has any Audios
        /// </summary>
        public int AudiosCount
        {
            get
            {
                return this.Audios.Count;
            }
        }

        public virtual bool HasFullAddress { get; set; }

        public virtual bool HasPhoneNumber { get; set; }

        public virtual bool HasEmailId { get; set; }

        public virtual bool HasToSync
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Videos pictures
        /// </summary>
        /// <param name="parentId">Can be claim number or task server entry id</param>
        public async Task LoadMediaContent(string parentId, MediaCaptureType mediaCaptureType, ParentType parentType,bool clearExisting=true)
        {
            ObservableCollection<MediaViewModel> mediaAttachments = null;
            if (mediaCaptureType == MediaCaptureType.Video)
                mediaAttachments = this.Videos;
            else if (mediaCaptureType == MediaCaptureType.Photo)
                mediaAttachments = this.Pictures;
            else if (mediaCaptureType == MediaCaptureType.Audio)
                mediaAttachments = this.Audios;
            if (clearExisting)
            mediaAttachments.Clear();
            var attachments = await MainViewModel.Instance.CscRepository.GetAttachments(parentId, parentType, mediaCaptureType);

            foreach (var item in attachments.Where(n => n.AttachmentType == mediaCaptureType))
            {
                var mediaAttacmmentVm = new MediaViewModel();
                mediaAttacmmentVm.MediaElement = item;
                if (mediaCaptureType != MediaCaptureType.Audio)
                    await mediaAttacmmentVm.GetThumbNailImage();
                if (!mediaAttachments.Any(mediaAttacmment => mediaAttacmment.MediaElement.FileLocation.Equals(mediaAttacmmentVm.MediaElement.FileLocation)))
                mediaAttachments.Add(mediaAttacmmentVm);
            }


            RefreshMediaCollectionBindings();
        }


        public virtual void OnNavigateToTasksCommand()
        {
        }

        public void RefreshMediaCollectionBindings()
        {
            RaisePropertyChanged("HasVideos");
            RaisePropertyChanged("HasPictures");
            RaisePropertyChanged("HasAudios");
            RaisePropertyChanged("PicturesCount");
            RaisePropertyChanged("VideosCount");
            RaisePropertyChanged("AudiosCount");
        }

        protected virtual async void OnNavigateToMediaListingsPage(string mediaCaptureTypeParameter = "")
        {
            
        }

        protected void NavigatingToMediaListingsPage(string ParentId,ParentType parentType, MediaCaptureType mediaCaptureType)
        {
            PhotoPageRequest medialListingPageRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.PhotoPageRequest) as PhotoPageRequest;
            if (medialListingPageRequest == null)
                medialListingPageRequest = new PhotoPageRequest(ServiceRequestIds.PhotoPageRequest);
            medialListingPageRequest.ParentId = ParentId;
            medialListingPageRequest.ParentType = parentType;
            medialListingPageRequest.MediaCaptureType = mediaCaptureType;
            MainViewModel.Instance.Navigate(ServiceRequestIds.PhotoPageRequest);
        }

        #endregion

        #region Validation Logic

        /*
         * AA-WARNING: DO NOT COPY VALIDATION LOGIC as it is. Instead, move the validation logic to a base class or ResultItem and inherit from there.
         * BindableValidator instance is created because Repository item is not being inherited from ResultItem, due to the nature of project.
         */

        #region Fields

        private BindableValidator _bindableValidator;
        private ReadOnlyCollection<string> _allErrors;
        private ViewModelBase _itembase;

        #endregion

        #region Properties

        /// <summary>
        /// Gets all the validation errors
        /// </summary>
        public ReadOnlyCollection<string> AllErrors
        {
            get
            {
                if (_allErrors == null)
                    _allErrors = BindableValidator.EmptyErrorsCollection;
                return _allErrors;
            }
            private set
            {
                _allErrors = value;
                RaisePropertyChanged("AllErrors");
            }
        }

        #endregion

        #region ValidationInterface Implementation

        /// <summary>
        /// Gets or sets a value indicating whether this instance is validation enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if validation is enabled for this instance; otherwise, <c>false</c>.
        /// </value>
        public bool IsValidationEnabled
        {
            get
            {
                return this._bindableValidator.IsValidationEnabled;
            }
            set
            {
                this._bindableValidator.IsValidationEnabled = value;
            }
        }

        /// <summary>
        /// Returns the BindableValidator instance that has an indexer property.
        /// </summary>
        /// <value>
        /// The Bindable Validator Indexer property.
        /// </value>
        public BindableValidator Errors
        {
            get
            {
                return _bindableValidator;
            }
        }

        /// <summary>
        /// Occurs when the Errors collection changed because new errors were added or old errors were fixed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged
        {
            add
            {
                _bindableValidator.ErrorsChanged += value;
            }

            remove
            {
                _bindableValidator.ErrorsChanged -= value;
            }
        }

        /// <summary>
        /// Gets all errors.
        /// </summary>
        /// <returns> A ReadOnlyDictionary that's key is a property name and the value is a ReadOnlyCollection of the error strings.</returns>
        public ReadOnlyDictionary<string, ReadOnlyCollection<string>> GetAllErrors()
        {
            return _bindableValidator.GetAllErrors();
        }

        /// <summary>
        /// Validates the properties of the current instance.
        /// </summary>
        /// <returns>
        /// Returns <c>true</c> if all properties pass the validation rules; otherwise, false.
        /// </returns>
        public bool ValidateProperties()
        {
            return _bindableValidator.ValidateProperties();
        }

        /// <summary>
        /// Sets the error collection of this instance.
        /// </summary>
        /// <param name="entityErrors">The entity errors.</param>
        public void SetAllErrors(IDictionary<string, ReadOnlyCollection<string>> entityErrors)
        {
            _bindableValidator.SetAllErrors(entityErrors);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Performs validation
        /// </summary>
        /// <param name="user"></param>
        protected void PerformValidation(User user)
        {
            if (_bindableValidator == null)
            {
                _bindableValidator = new BindableValidator(user);
                RaisePropertyChanged("Errors");
            }

            _itembase = user as ViewModelBase;
            ContinueValiation();
        }

        protected void PerformValidation(ClaimAdjusterTask task)
        {
            if (_bindableValidator == null)
            {
                _bindableValidator = new BindableValidator(task);
                RaisePropertyChanged("Errors");
            }

            _itembase = task as ViewModelBase;
            ContinueValiation();
        }

        protected void PerformValidation(NoteViewModel note)
        {
            if (_bindableValidator == null)
            {
                _bindableValidator = new BindableValidator(note);
                RaisePropertyChanged("Errors");
            }

            _itembase = note as ViewModelBase;
            ContinueValiation();
        }

        private void ContinueValiation()
        {
            _bindableValidator.ErrorsChanged -= _bindableValidator_ErrorsChanged;
            _bindableValidator.ErrorsChanged += _bindableValidator_ErrorsChanged;
            if (_itembase!=null)
            { 
            _itembase.PropertyChanged -= ItemBase_PropertyChanged;
            _itembase.PropertyChanged += ItemBase_PropertyChanged;
            }
            ValidateProperties();
        }

        private void ItemBase_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AllErrors")  // Prevent "AllErrors" from getting raised, which may result in stackoverflow.
                return;

            _bindableValidator.ValidateProperty(e.PropertyName);
            UpdateErrors();
        }

        void _bindableValidator_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            UpdateErrors();
        }

        /// <summary>
        /// Forgets bindable validation
        /// </summary>
        protected void ForgetValidation()
        {
            if (_itembase != null)
            {
                _itembase.PropertyChanged -= ItemBase_PropertyChanged;
                _itembase = null;
            }

            if (_bindableValidator != null)
            {
                _bindableValidator.ErrorsChanged -= _bindableValidator_ErrorsChanged;
                _bindableValidator = null;
            }
        }

        /// <summary>
        /// Updates the error message collection
        /// </summary>
        private void UpdateErrors()
        {
            this.AllErrors = new ReadOnlyCollection<string>(_bindableValidator.GetAllErrors().Values.SelectMany(n => n).ToList());
        }

        #endregion

        #endregion

    }
}
