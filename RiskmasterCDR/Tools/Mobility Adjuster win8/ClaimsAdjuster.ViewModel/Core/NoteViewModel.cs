﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ClaimsAdjuster.ViewModel
{
    public class NoteViewModel : ClaimsAdjuster
    {
        #region Fields
        private ICommand saveCurrentNote;
        private ICommand cancelCommand;
        private Code selectedType;
        private string content;
        private string validationErrorMessage;
        public bool isSaveButtonEnabled;
        public bool isCancelButtonEnabled;
        public bool isNoteDetails;
      

        #endregion

        #region Properties
        public Claim CurrentClaim { get; set; }
        public Event CurrentEvent { get; set; }
        public List<Code> NoteTypes { get; set; }
		public int ClaimViewId { get; set; } //mbahl3 Mits 36145
        public Code SelectedType
        {
            get
            {
                return selectedType;
            }
            set
            {
                if (value != null)
                    selectedType = value;
                RaisePropertyChanged("SelectedType");
            }
        }
        public Notes CurrentNote 
        { get;
          set; 
        }

        [Required(AllowEmptyStrings = false)]
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
                RaisePropertyChanged("Content");
            }
        }

        public ICommand SaveCurrentNote
        {
            get
            {
                if (saveCurrentNote == null)
                    saveCurrentNote = new RelayCommand(OnSaveNoteCommand);

                return saveCurrentNote;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                    cancelCommand = new RelayCommand(OnCancelCommand);
                return cancelCommand;
            }
        }

        public string ValidationErrorMessage
        {
            get
            {
                return validationErrorMessage;
            }
            private set
            {
                validationErrorMessage = value;
                RaisePropertyChanged("ValidationErrorMessage");
            }
        }

        /// <summary>
        /// Used to bind ClaimDetails user control
        /// </summary>
        public Claim CscClaim
        {
            get
            {
                return CurrentClaim;
            }
        }

        public bool IsSaveButtonEnabled
        {
            get
            {
                return isSaveButtonEnabled;
            }
            set
            {
                isSaveButtonEnabled = value;
                RaisePropertyChanged("IsSaveButtonEnabled");
            }
        }

        public bool IsCancelButtonEnabled
        {
            get
            {
                return isCancelButtonEnabled;
            }
            set
            {
                isCancelButtonEnabled = value;
                RaisePropertyChanged("IsCancelButtonEnabled");
            }
        }

        public bool IsNew
        {
            get
            {
                return CurrentNote != null && CurrentNote.Status == (int)DataStatus.NewlyAddedFromServer;
            }
        }

        public bool IsNoteDetails
        {
            get
            {
                return isNoteDetails;
            }
            set
            {
                isNoteDetails = value;
                RaisePropertyChanged("IsNoteDetails");
            }
        }

        public string UnsnapMessage
        {
            get
            {
                return IsNoteDetails ? MainViewModel.Instance.GetStringResource("UnsanpToViewNoteText") : MainViewModel.Instance.GetStringResource("UnsanpToAddNoteText");
            }

        }

        #endregion

        #region Constructor
        public NoteViewModel()
        {
            this.CurrentNote = new Notes();
        }
        #endregion

        #region Methods

        public override IServiceRequest CreateServiceRequest()
        {
            string requestId = string.Format(ServiceRequestIds.NoteDetailsRequest, CurrentNote.NoteServerId);
            NoteDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(requestId) as NoteDetailsRequest;
            if (request == null)
                request = new NoteDetailsRequest(requestId);
            IsNoteDetails = true;
            request.Result = this;
            return request;
        }

        public async Task LoadData(string claimNumber)
        {
			//mbahl3 Mits 36145
            Claim claim = this.ClaimViewId != default(int) ? await MainViewModel.Instance.CscRepository.GetClaimsByClaimNumberAsync(this.ClaimViewId)
                : await MainViewModel.Instance.CscRepository.GetClaimsByClaimNumberAsync(claimNumber); 
			//mbahl3 Mits 36145
            
			this.Content = string.Empty;
                    
            //mbahl3  Mits 36145 commented code Claim claim = await MainViewModel.Instance.CscRepository.GetClaimsByClaimNumberAsync(claimNumber);
            this.CurrentClaim = claim;
            this.CurrentEvent = await MainViewModel.Instance.CscRepository.GetEventByEventNumberAsync(CurrentClaim.EventNumber);
            this.NoteTypes = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.NOTE_TYPE_CODE);
            selectedType = NoteTypes.FirstOrDefault();
            RaisePropertyChanged("SelectedType");
        }

        private async void OnSaveNoteCommand()
        {
            if (!IsSaveButtonEnabled)///bug #20268 fix,avoids multiple Enter key presses
                return;

            if (!string.IsNullOrWhiteSpace(Content))
                IsSaveButtonEnabled = false;

            ValidationErrorMessage = string.Empty;

            PerformValidation(this);

            if (this.GetAllErrors().Count > 0)
            {
                ValidationErrorMessage = MainViewModel.Instance.GetStringResource("AddTaskRequiredFieldsErrorMsg");
                return;
            }

            ResetValidation(); // AA: Unhook validation events, if any.

            if (StringHelpers.CheckValidity(Content))// validation check to avoid special char like (<,&)
            {
                ValidationErrorMessage = MainViewModel.Instance.GetStringResource("InvalidTextInputText");
                IsSaveButtonEnabled = true;
                return;
            }
            try
            {
                int count = await MainViewModel.Instance.CscRepository.GetNotesCountByClaimAsync(this.CurrentClaim.ClaimNumber);
                this.CurrentNote = new Notes();
                this.CurrentNote.Content = this.Content;
                this.CurrentNote.DateCreated = DateTime.Now;
                this.CurrentNote.NoteType = this.SelectedType.Description;
                this.CurrentNote.NoteTypeCodeId = this.SelectedType.CodeId;
                this.CurrentNote.ParentId = this.CurrentClaim.ClaimNumber;
                count++;
                this.CurrentNote.NoteServerId =this.CurrentClaim.ClaimNumber+count.ToString();
                this.CurrentNote.Heading = string.Format("{0} {1}", this.SelectedType.ShortCode, this.SelectedType.Description);
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    var success = await MainViewModel.Instance.UploadNote(this.CurrentNote);
                    if (success)
                    {
                        this.CurrentNote.Status = (int)DataStatus.SyncSuccess;

                    }
                    else
                    {
                        this.CurrentNote.Status = (int)DataStatus.SyncFail;

                    }
                }
                else
                {
                    this.CurrentNote.Status = (int)DataStatus.Modified;
                }
                await MainViewModel.Instance.CscRepository.SaveNotesAsync(this.CurrentNote);
                MainViewModel.Instance.AddNewlyCreatedNote(this.CurrentNote);
            }
            catch
            {
                this.CurrentNote.Status = (int)DataStatus.SyncFail;
            }
            finally
            {
                this.CurrentNote.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                if (MainViewModel.Instance.CurrentFrame.CanGoBack)
                    MainViewModel.Instance.CurrentFrame.GoBack();
            }
        }

        public void ResetValidation()
        {
            if (!string.IsNullOrEmpty(ValidationErrorMessage))
                ValidationErrorMessage = string.Empty;
            ForgetValidation();
        }

        private void OnCancelCommand()
        {
            if (IsCancelButtonEnabled)
                IsCancelButtonEnabled = false;

            MainViewModel.Instance.GoBackCommand.Execute(null);
        }

        #endregion
    }
}
