﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel
{
    public class Authentication : ClaimsAdjuster
    {
        #region Fields

        private Message authenticationMessage;
        private string signInErrorText;
        private bool isSigningIn;
        private ICommand validateLoginCommand;
        private List<Dsn> dsnList;

        #endregion

        #region EventHandler

        public event EventHandler AuthenticationFailed;

        #endregion

        #region Properties

        public string SignInErrorText
        {
            get
            {
                return signInErrorText;
            }
            set
            {
                signInErrorText = value;
                RaisePropertyChanged("SignInErrorText");
            }
        }

        public bool IsSigningIn
        {
            get
            {
                return isSigningIn;
            }
            set
            {
                isSigningIn = value;
                RaisePropertyChanged("IsSigningIn");
            }
        }

        public ICommand ValidateLoginCommand
        {
            get
            {
                if (validateLoginCommand == null)
                    validateLoginCommand = new RelayCommand(OnValidateLoginCommand);
                return validateLoginCommand;
            }
        }

        public List<Dsn> DSNList
        {
            get
            {
                return dsnList;
            }
            set
            {
                dsnList = value;
                RaisePropertyChanged("DSNList");
            }
        }

        #endregion

        #region Methods

        private void OnValidateLoginCommand()
        {
            SignInErrorText = string.Empty;
            if(MainViewModel.Instance.CurrentUserInfo == null)
            {
                SignInErrorText = MainViewModel.Instance.GetStringResource("UnableToConnectText");
                return;
            }

            PerformValidation(MainViewModel.Instance.CurrentUserInfo);
            if (AllErrors.Count == 0)
            {
                bool hasValidCharacters = CheckValidCharacters();
                if (!hasValidCharacters)
                {
                    SignInErrorText = MainViewModel.Instance.GetStringResource("InvalidTextInputText");
                    return;
                }
                try
                {
                    if (!MainViewModel.Instance.IsInternetAvailable)
                    {
                        MessageHelper.Show(MainViewModel.Instance.GetStringResource("UnableToConnectText"));
                        return;
                    }
                    AuthenticateUser();
                }
                catch (Exception)
                {
                    MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedText");
                }
            }
            else
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedMandatoryFieldEmptyText");
        }

        /// <summary>
        /// validation check to avoid special char like (<,&)
        /// </summary>
        public bool CheckValidCharacters()
        {
            User currentUser = MainViewModel.Instance.CurrentUserInfo;
            bool isValid = true;
            if (StringHelpers.CheckValidity(currentUser.UserName))
                isValid = false;
            else if (StringHelpers.CheckValidity(currentUser.Password))
                isValid = false;
            else if (StringHelpers.CheckValidity(currentUser.DSNName))
                isValid = false;
            return isValid;
        }

        public async void AuthenticateUser()
        {
            
            IsSigningIn = true;
            SignInErrorText = string.Empty;
            bool isValidDSN = await CheckDsnName();
            if (isValidDSN)
            {
                await CreateTokenAndNavigate(true);
            }
            else
            {
                if (string.IsNullOrEmpty(SignInErrorText))  // Avoid overwriting the error message.
                    SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedText");
                IsSigningIn = false;
                return;
            }

            IsSigningIn = false;
        }

        /// <summary>
        /// Checks if the Dsn name is valid
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckDsnName()
        {
            try
            {
                CreateAuthenticationMessage();

                string userDSNsResult = await MobileAuthenticationService.GetUserDSNsMobileAdjusterAsync(StringHelpers.SerializeCustom(authenticationMessage));
                ResultMessage<DSNDocument> userDSNsObject = null;
                if (!string.IsNullOrEmpty(userDSNsResult))
                    userDSNsObject = StringHelpers.DeserializeCustom<ResultMessage<DSNDocument>>(userDSNsResult);

                if (userDSNsObject != null && userDSNsObject.Status.StatusCode == StatusCodes.Success)
                {
                    dsnList = new List<Dsn>();
                    foreach (var item in userDSNsObject.Document.DSNList)
                    {
                        Dsn dsn = new Dsn() { Name = item };
                        dsnList.Add(dsn);
                    }


                    if (userDSNsObject.Document.DSNList.Contains(MainViewModel.Instance.CurrentUserInfo.DSNName))
                        return true;//checks if Dsn name is valid
                    else
                    {
                        SignInErrorText = MainViewModel.Instance.GetStringResource("InvalidDsnNameMessage");
                    }
                }
                else if (userDSNsObject != null)
                {
                    SignInErrorText = MainViewModel.Instance.GetStringResource("InvalidUserNamePasswordMessage");
                }
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                SignInErrorText = MainViewModel.Instance.GetStringResource("InvalidWebServerUrlMessage");
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        /// <summary>
        /// Message element to serialized to the MobileService
        /// </summary>
        private void CreateAuthenticationMessage()
        {
            authenticationMessage = new Message();
            authenticationMessage.Call = new Call();
            authenticationMessage.Call.Function = "LoginAdaptor.Authorization";
            authenticationMessage.Document = new Document();
            authenticationMessage.Document.Authorization = new Authorization();
            authenticationMessage.Document.Authorization.UserInfo = MainViewModel.Instance.CurrentUserInfo;
        }

        public async Task ReLogin()
        {
            MainViewModel.Instance.SetAppBusy(Constants.State_Authentication, true);
            CheckAuthenticated();
            if (MainViewModel.Instance.IsAuthenticated)
            {
                User userDetails = await MainViewModel.Instance.CscRepository.GetUserDetails(MainViewModel.Instance.AppSettings.LastLoggedUserId);
                if (userDetails != null)
                {
                    MainViewModel.Instance.CurrentUserInfo = userDetails;
                    CreateAuthenticationMessage();
                    await RefreshToken(false);
                    MainViewModel.Instance.HomePageVM.Initialize();
                }
                else
                {
                    MainViewModel.Instance.CurrentUserInfo = new User();
                }

            }
            else
            {
                MainViewModel.Instance.CurrentUserInfo = new User();
            }
            MainViewModel.Instance.SetAppBusy(Constants.State_Authentication, false);
        }

        /// <summary>
        /// Refreshes new user Token
        /// </summary>        
        public async Task<bool> RefreshToken(bool isInitialLogin)
        {
            try
            {
                string authenticationResult = string.Empty;
                ResultMessage<TokenDocument> authenticationObject = null;
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    authenticationResult = await MobileAuthenticationService.AuthenticateMobileUserAsync(StringHelpers.SerializeCustom(authenticationMessage));
                    authenticationObject = StringHelpers.DeserializeCustom<ResultMessage<TokenDocument>>(authenticationResult);
                }
                if (authenticationObject != null && authenticationObject.Status.StatusCode == StatusCodes.Success)
                {
                    MainViewModel.Instance.AuthenticationToken = authenticationObject.Document.Result.Token;
                    CreateUserDirectory();
                    return true;
                }
                else
                {
                    if (!isInitialLogin)
                        CreateUserDirectory();
                    return false;
                }
            }
            catch (Exception)
            {
                MainViewModel.Instance.SetAppBusy(Constants.State_Authentication, false);
                return false;
            }
        }


        /// <summary>
        /// Inserts the logged in user to DB
        /// </summary>        
        public async void InsertUserToDB()
        {
            await MainViewModel.Instance.CscRepository.SaveUserAsync(MainViewModel.Instance.CurrentUserInfo);
            MainViewModel.Instance.AppSettings.LastLoggedUserId = MainViewModel.Instance.CurrentUserInfo.Id;
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<Dsn>();
            await MainViewModel.Instance.CscRepository.InsertItemsAsync(dsnList);
        }

        private void CheckAuthenticated()
        {
            MainViewModel.Instance.IsAuthenticated = MainViewModel.Instance.AppSettings.LastLoggedUserId != 0;
        }

        /// <summary>
        /// Creates user directory
        /// </summary>
        private async void CreateUserDirectory()
        {
            BackgroundTaskHelper.Register();    // AA: Refractor to reusable method blocks.
            StorageFolder local = ApplicationData.Current.LocalFolder;
            StorageFolder userFolder = await local.CreateFolderAsync(Constants.UserDirectoryName, CreationCollisionOption.OpenIfExists);    // Create user exclusive directory
            if (MainViewModel.Instance.UserDSNFolder != null && MainViewModel.Instance.UserDSNFolder.Name != MainViewModel.Instance.CurrentUserInfo.DSNName)
                await MainViewModel.Instance.UserDSNFolder.DeleteAsync();
            MainViewModel.Instance.UserDSNFolder = await userFolder.CreateFolderAsync(MainViewModel.Instance.CurrentUserInfo.DSNName, CreationCollisionOption.OpenIfExists);    // Create DSN directory
        }

        public override IServiceRequest CreateServiceRequest()
        {
            SignInPageRequest signInPageRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.SignInPageRequest) as SignInPageRequest;
            if (signInPageRequest == null)
            {
                signInPageRequest = new SignInPageRequest(ServiceRequestIds.SignInPageRequest);
                signInPageRequest.Result = this;
            }
            return signInPageRequest;
        }

        /// <summary>
        /// Clears signIn credentials from the signIn page and deletes the current user from DB and then navigates to the login page
        /// </summary>
        public async void ClearSignInDetails()
        {
            var currentUserInfo = MainViewModel.Instance.CurrentUserInfo;

            await CleanupDB();
            await MainViewModel.Instance.UserDSNFolder.DeleteAsync();
            MainViewModel.Instance.UserDSNFolder = null;
            await MainViewModel.Instance.CscRepository.DeleteAsync<User>(currentUserInfo);
            DSNList = null;

            currentUserInfo.Id = 0;
            currentUserInfo.UserName = string.Empty;
            currentUserInfo.Password = string.Empty;
            currentUserInfo.WebServerUrl = string.Empty;
            currentUserInfo.DSNName = string.Empty;
            currentUserInfo.IsHttps = false;
            MainViewModel.Instance.AppSettings.LastLoggedUserId = 0;
            MainViewModel.Instance.IsAuthenticated = false;
            MainViewModel.Instance.ClearToastData();
            BadgeHelper.Clear();
            if (MainViewModel.Instance.CurrentFrame.CanGoBack)
                MainViewModel.Instance.CurrentFrame.BackStack.Clear();
            this.InvokeService.Execute(null);
        }

        public async Task<bool> ChangeWebServerUrl(User newUser)
        {
            IsSigningIn = true;
            bool closeFlyout = false;
            User currentLoggedUser = MainViewModel.Instance.CurrentUserInfo;

            MainViewModel.Instance.CurrentUserInfo = newUser;
            bool isValidDsn = await CheckDsnName();
            if (isValidDsn)
            {

                closeFlyout = await CreateTokenAndNavigate(false);
            }
            else
                MainViewModel.Instance.CurrentUserInfo = currentLoggedUser; //reverting to the old user in case of a login failure

            IsSigningIn = false;
            return closeFlyout;

        }

        private async Task<bool> CreateTokenAndNavigate(bool isFirstLogin)//isFirstLogin diffrentiates between login from the sign in page & changing login details from settings pane
        {
            bool isAuthenticated = await RefreshToken(true);
            if (isAuthenticated)
            {
                if (!isFirstLogin)
                    await CleanupDB();

                InsertUserToDB();

                // resets the ServiceInstance values whenever the web server URL changes
                MobileUploadService.ResetServiceInstance();
                MobileProgressNoteService.ResetServiceInstance();
                MobileDocumentService.ResetServiceInstance();
                MobileCommonService.ResetServiceInstance();
                MobileCodesService.ResetServiceInstance();
                MainViewModel.Instance.RefreshCommand.Execute(null);
                MainViewModel.Instance.IsAuthenticated = isAuthenticated;
            }
            return isAuthenticated;
        }

        private async Task CleanupDB()
        {
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<Event>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<Claim>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<ClaimAdjusterTask>();
            MainViewModel.Instance.CscRepository.DeleteAllAsync<Attachment>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<Notes>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<PaymentHistory>();
            MainViewModel.Instance.CscRepository.DeleteAllAsync<Code>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<PartyInvolved>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<User>();
            await MainViewModel.Instance.CscRepository.DeleteAllAsync<Dsn>();
        }

        public async Task PopulateDSNList()
        {
            if (DSNList == null)
                DSNList = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Dsn>();
        }

        public async Task<bool> CheckPendingUploadsAsync()
        {
            bool hasPendingUploads = false;
            var taskList = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
            hasPendingUploads = taskList.Exists(p => p.Status == (int)DataStatus.Modified || p.Status == (int)DataStatus.ClosedOffline);
            if (!hasPendingUploads)
            {
                var claimsList = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
                hasPendingUploads = claimsList.Exists(p => p.Status == (int)DataStatus.Modified || p.Status == (int)DataStatus.ClosedOffline);
            }
            return hasPendingUploads;
        }

        public void ResetValidation()
        {
            ForgetValidation();
        }

        #endregion
    }
}
