﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml.Media.Imaging;

namespace ClaimsAdjuster.ViewModel
{
    public class MediaViewModel : ClaimsAdjuster
    {
        private BitmapImage attachmentImage;

        #region Properties

        public Attachment MediaElement { get; set; }

        public string FileName
        {
            get
            {
                return MediaElement!=null?MediaElement.FileName:string.Empty;
            }
        }

        public string CreatedOnText
        {
            get
            {
                return MediaElement != null ? MediaElement.CreatedOnText : string.Empty;
            }
        }

        public string FileLocation
        {
            get
            {
                return MediaElement != null ? MediaElement.FileLocation : string.Empty;
            }
        }

        public bool IsSelected { get; set; }

        public BitmapImage AttachmentImage
        {
            get
            {
                return attachmentImage;
            }
        }
        #endregion

        #region Methods

        public async Task GetThumbNailImage()
        {
            Uri filepath = MediaElement.AttachmentType == MediaCaptureType.Audio ? new Uri("ms-appx:///Assets/AudioPlaceHolder.png") : new Uri(MediaElement.FileLocation);

            var file = await StorageHelper.FileExists(filepath);
            if (file != null)
            {
                const ThumbnailMode thumbnailMode = ThumbnailMode.SingleItem;
                const uint size = 100;
                using (StorageItemThumbnail thumbnail = await file.GetThumbnailAsync(thumbnailMode))
                {
                    if (thumbnail != null)
                    {
                        attachmentImage = new BitmapImage();
                        attachmentImage.SetSource(thumbnail);
                    }

                }
            }

        }

        #endregion


    }
}
