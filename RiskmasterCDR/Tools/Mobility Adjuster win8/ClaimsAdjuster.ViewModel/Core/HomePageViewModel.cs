﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using CSC.ServiceFactory;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ClaimsAdjuster.ViewModel
{
    public class HomePageViewModel : ClaimsAdjuster
    {
        #region Fields

        private int tasksCount;
        private int claimsCount;
        private int eventsCount;
        private ICommand refreshTaskCommand;
        private ServerClaimsRequest serverClaimsRequest;
        private ServerTasksRequest serverTasksRequest;
        private ServerEventsRequest serverEventsRequest;
        private bool isPopulatingNewTask;

        #endregion

        #region Properties

        public TaskCollection Tasks { get; set; }

        public ClaimCollection Claims { get; set; }

        public EventCollection Events { get; set; }

        public int TasksCount
        {
            get
            {
                return tasksCount;
            }
            set
            {
                tasksCount = value;
                RaisePropertyChanged("TasksText");
            }
        }

        public int ClaimsCount
        {
            get
            {
                return claimsCount;
            }
            set
            {
                claimsCount = value;
                RaisePropertyChanged("ClaimsText");
            }
        }

        public int EventsCount
        {
            get
            {
                return eventsCount;
            }
            set
            {
                eventsCount = value;
                RaisePropertyChanged("EventsText");
            }
        }

        public string TasksText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("TasksCountText"), Tasks.Count);
            }
        }

        public string ClaimsText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("ClaimsCountText"), ClaimsCount);
            }
        }

        public string EventsText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("EventsCountText"), EventsCount);
            }
        }

        #endregion

        #region Commands

        public ICommand RefreshTaskCommand
        {
            get
            {
                if (refreshTaskCommand == null)
                    refreshTaskCommand = new RelayCommand(Refresh);
                return refreshTaskCommand;
            }
        }

        #endregion

        #region Constructor

        public HomePageViewModel()
        {
            InitializeRequests();
        }

        #endregion

        #region Methods

        public async void Initialize()
        {
            await MainViewModel.Instance.UploadUnsyncItem(true);
            if (MainViewModel.Instance.IsAuthenticated)
            {
                LoadClaims();
                LoadTasks();
                LoadEvents();
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        GetCodes();
                        GetSecondaryTilesList();

                    });
                }

                MainViewModel.Instance.NavigateToTileParam();

            }
            //Check if secondary tile nav occured and move to claims /tasks Page

        }

        public void InitializeRequests()
        {
            serverClaimsRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.ServerClaimsRequest) as ServerClaimsRequest;
            if (serverClaimsRequest == null)
                serverClaimsRequest = new ServerClaimsRequest(ServiceRequestIds.ServerClaimsRequest);
            Claims = serverClaimsRequest.Result as ClaimCollection;
            serverTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.ServerTasksRequest) as ServerTasksRequest;
            if (serverTasksRequest == null)
                serverTasksRequest = new ServerTasksRequest(ServiceRequestIds.ServerTasksRequest);
            Tasks = serverTasksRequest.Result as TaskCollection;
            serverEventsRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.ServerEventsRequest) as ServerEventsRequest;
            if (serverEventsRequest == null)
                serverEventsRequest = new ServerEventsRequest(ServiceRequestIds.ServerEventsRequest);
            Events = serverEventsRequest.Result as EventCollection;
        }

        /// <summary>
        /// Updates Application tile when claims and tasks are loaded
        /// </summary>
        public void UpdateApplicationTile()
        {
            if (!serverClaimsRequest.IsBusy && !serverTasksRequest.IsBusy)
            {
                if (MainViewModel.Instance.IsInternetAvailable)
                    MainViewModel.Instance.AppSettings.SyncDate = DateTime.Now;                    
                MainViewModel.Instance.RaisePropertyChanged("SyncDateText");
                MainViewModel.Instance.RaisePropertyChanged("LastSyncText");
                MainViewModel.Instance.RaisePropertyChanged("ShowSyncDate");
                PinHelper.UpdateApplicationTile();
            }
        }

        /// <summary>
        /// Gets all codes to be inserted into Code table
        /// </summary>
        private async void GetCodes()
        {
            string response = await MobileCodesService.GetCodesForMobileAdjAsync(string.Format(MobileCodesService.GetCodesRequest, MainViewModel.Instance.AuthenticationToken));
            MobileCodesService.ParseCodesData(response);
        }

        /// <summary>
        /// Clears and add Claims
        /// </summary>
        public async void LoadClaims()
        {
            await serverClaimsRequest.ExecuteAsync();
            ClaimsCount = await MainViewModel.Instance.CscRepository.GetCount<Claim>();
            UpdateApplicationTile();
        }

        /// <summary>
        /// Clears and add Events
        /// </summary>
        public async void LoadEvents()
        {
            await serverEventsRequest.ExecuteAsync();
            EventsCount = await MainViewModel.Instance.CscRepository.GetCount<Event>();
        }

        /// <summary>
        /// Clears and add Tasks
        /// </summary>
        public async void LoadTasks()
        {
            await serverTasksRequest.ExecuteAsync();
            TasksCount = await MainViewModel.Instance.CscRepository.GetCount<ClaimAdjusterTask>();
            MainViewModel.Instance.GenerateToastData();
            UpdateApplicationTile();
        }

        public void Refresh()
        {
            MainViewModel.Instance.DismissAppBar(false);
            if (serverClaimsRequest.IsBusy || serverTasksRequest.IsBusy) return;
            if (!MainViewModel.Instance.IsInternetAvailable) return;
            Claims.Collection.Clear();
            ClaimsCount = 0;
            Tasks.Collection.Clear();
            TasksCount = 0;
            Events.Collection.Clear();
            EventsCount = 0;
            Initialize();
        }

        private async void GetSecondaryTilesList()
        {
            MainViewModel.Instance.SecondaryTiles = await Windows.UI.StartScreen.SecondaryTile.FindAllForPackageAsync();

        }


        /// <summary>
        /// Uploads a set of new Tasks created when network was offline
        /// </summary>
        public async Task UploadNewTasks(bool isFromRefresh)
        {
            if (isPopulatingNewTask)
                return;

            isPopulatingNewTask = true;

            try
            {
                List<ClaimAdjusterTask> tasks = await MainViewModel.Instance.CscRepository.GetTaskByStatusAsync((int)DataStatus.Modified);
                int taskCount = tasks.Count;
                if (taskCount > 0)
                {
                    StringBuilder uploadRequest = new StringBuilder();
                    uploadRequest.Append("<Upload>");
                    foreach (var task in tasks)
                    {
                        string taskTypeText = string.Format("{0}|{1}", task.TaskTypeId, task.TaskType);
                        string id = string.Empty;
                        string attachTableText = string.Empty;
                        if (task.TaskParentType == ParentType.Claim)
                        {
                            attachTableText = Constants.CLAIM;
                            id = task.ClaimNumber;
                        }
                        else // Event
                        {
                            attachTableText = Constants.EVENT;
                            id = task.EventNumber;
                        }
                        string createTaskRequest = string.Format(MobileCommonService.CreateTaskRequest, MainViewModel.Instance.AuthenticationToken, task.TaskName, MainViewModel.Instance.CurrentUserInfo.UserName,
                            task.TaskDescription, task.TaskDateTime.ToString("MM/dd/yyyy"), task.TaskDateTime.ToString("hh:mm tt"), taskTypeText, id, attachTableText);
                        uploadRequest.AppendFormat("<Save><Type>savediary</Type>{0}</Save>", createTaskRequest);
                    }
                    uploadRequest.Append("</Upload>");
                    string uploadedResponse = await MobileUploadService.UploadRequestAsync(uploadRequest.ToString());
                    List<int> entryIds = ParsingHelper.ParseNewTasksUpload(uploadedResponse);
                    if (entryIds.Count > 0)
                    {
                        for (int i = 0; i < taskCount; i++)
                        {
                            if (entryIds[i] != -1)
                            {
                                tasks[i].ServerEntryId = entryIds[i];
                                tasks[i].Status = (int)DataStatus.SyncSuccess;
                            }
                        }
                    }
                    await MainViewModel.Instance.CscRepository.UpdateItemListAsync(tasks);
                    if (!isFromRefresh)
                    {
                        UpdateVMTasks(tasks);
                    }
                }
            }
            catch (Exception)
            {

            }

            isPopulatingNewTask = false;
        }

        /// <summary>
        /// Uploads offline notes
        /// </summary>
        /// <returns></returns>
        public async Task UploadNewNotes()
        {
            List<Notes> allNotes = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Notes>();
            if (allNotes.Count > 0)
            {
                var newNotes = allNotes.Where(s => s.Status == (int)DataStatus.Modified || s.Status == (int)DataStatus.SyncFail).ToList();
                if (newNotes.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<Upload>");
                    foreach (var note in newNotes)
                    {
                        string requestXml = string.Format(MobileProgressNoteService.CreateNoteRequest, MainViewModel.Instance.AuthenticationToken, note.ParentId, DateTime.UtcNow.ToString("MM/dd/yyyy"), note.Content, note.NoteTypeCodeId);
                        sb.AppendFormat("<Save><Type>progressnotes</Type>{0}</Save>", requestXml);
                    }
                    sb.Append("</Upload>");
                    string notesUploadResponse = await MobileUploadService.UploadRequestAsync(sb.ToString());
                    List<StatusCodes> codes = ParsingHelper.ParseUploadNewNotes(notesUploadResponse);
                    if (codes != null && codes.Count > 0)
                    {
                        for (int i = 0; i < newNotes.Count(); i++)
                        {
                            if (codes[i] == StatusCodes.Success)
                                newNotes[i].Status = (int)DataStatus.SyncSuccess;
                        }
                        await MainViewModel.Instance.CscRepository.UpdateItemListAsync(newNotes);
                    }
                }

            }
        }

        /// <summary>
        /// Uploads claim photos
        /// </summary>
        /// <returns></returns>
        public async Task UploadClaimItems()
        {
            List<Claim> allClaims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            if (allClaims.Count > 0)
            {
                var uploadClaims = allClaims.Where(s => s.Status == (int)DataStatus.Modified || s.Status == (int)DataStatus.SyncFail).ToList();
                if (uploadClaims.Count > 0)
                {
                    MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                        foreach (var claim in uploadClaims)
                        {
                            ClaimViewModel claimVM = new ClaimViewModel();
                            claimVM.CscClaim = claim;
                            var claimDetailsRequest = claimVM.CreateServiceRequest();
                            await claimDetailsRequest.ExecuteAsync();
                            claimVM.RefreshClaimDetail.Execute(true);
                        }
                    });
                }
            }
        }

        /// <summary>
        /// Updates offline new tasks when items are updated
        /// </summary>
        /// <param name="tasks"></param>
        private void UpdateVMTasks(List<ClaimAdjusterTask> tasks)
        {
            //Updates task item from HomePage VM
            HomePageViewModel homeVM = MainViewModel.Instance.HomePageVM;
            TasksRequest tasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, string.Empty)) as TasksRequest;
            foreach (var taskItem in tasks)
            {
                if (homeVM.Tasks != null && homeVM.Tasks.Count > 0)
                {
                    TaskViewModel task = homeVM.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                    if (task != null)
                        task.CscTask = taskItem;
                }

                //Updates task item from Tasks VM
                if (tasksRequest != null)
                {
                    TaskCollection taskCollection = tasksRequest.Result as TaskCollection;
                    if (tasks != null && taskCollection.Collection.Count > 0)
                    {
                        TaskViewModel task = taskCollection.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                        if (task != null)
                        {
                            if (taskCollection.AllTasks != null)//Removes current offline task adds new item to AllTasks
                            {
                                taskCollection.AllTasks.Remove(task.CscTask);
                                taskCollection.AllTasks.Add(taskItem);
                            }
                            task.CscTask = taskItem;
                        }
                    }
                }

                if (taskItem.TaskParentType == ParentType.Claim)
                {
                    //Updates task item from claim Tasks collection VM
                    TasksRequest claimTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, taskItem.ClaimNumber)) as TasksRequest;
                    if (claimTasksRequest != null)
                    {
                        TaskCollection taskCollection = claimTasksRequest.Result as TaskCollection;
                        if (tasks != null && taskCollection.Collection.Count > 0)
                        {
                            TaskViewModel task = taskCollection.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                            if (task != null)
                            {
                                if (taskCollection.AllTasks != null)//Removes current offline task adds new item
                                {
                                    taskCollection.AllTasks.Remove(task.CscTask);
                                    taskCollection.AllTasks.Add(taskItem);
                                }
                                task.CscTask = taskItem;
                            }
                        }
                    }

                    //Updates task item from ClaimDetails VM
                    ClaimDetailsRequest claimDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.ClaimDetailsRequest, taskItem.ClaimNumber,taskItem.ParentViewId)) as ClaimDetailsRequest; //mbahl3 Mits 36145
                    if (claimDetailsRequest != null)
                    {
                        ClaimViewModel claim = claimDetailsRequest.Result as ClaimViewModel;
                        var task = claim.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                        if (task != null)
                            task.CscTask = taskItem;
                    }
                }
                else if (taskItem.TaskParentType == ParentType.Event)
                {
                    //Updates task item from event Tasks collection VM
                    TasksRequest eventTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, taskItem.EventNumber)) as TasksRequest;
                    if (eventTasksRequest != null)
                    {
                        TaskCollection taskCollection = eventTasksRequest.Result as TaskCollection;
                        if (tasks != null && taskCollection.Collection.Count > 0)
                        {
                            TaskViewModel task = taskCollection.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                            if (task != null)
                            {
                                if (taskCollection.AllTasks != null)//Removes current offline task adds new item
                                {
                                    taskCollection.AllTasks.Remove(task.CscTask);
                                    taskCollection.AllTasks.Add(taskItem);
                                }
                                task.CscTask = taskItem;
                            }
                        }
                    }

                    //Updates task item from eventdetails VM
                    EventDetailsRequest eventDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.EventDetailsRequest, taskItem.EventNumber)) as EventDetailsRequest;
                    if (eventDetailsRequest != null)
                    {
                        EventViewModel eventVM = eventDetailsRequest.Result as EventViewModel;
                        var task = eventVM.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == taskItem.Id);
                        if (task != null)
                            task.CscTask = taskItem;
                    }
                }
            }
        }

        public void AddToHomeEvents(Event claimEvent)
        {
            if (!MainViewModel.Instance.HomePageVM.Events.Collection.Any(s => s.CscEvent.EventNumber == claimEvent.EventNumber))
            {
                EventViewModel eventVM = new EventViewModel();
                eventVM.CscEvent = claimEvent;
                MainViewModel.Instance.HomePageVM.Events.Collection.Add(eventVM);
                EventsCount = EventsCount + 1;
            }
        }

        #endregion

    }
}
