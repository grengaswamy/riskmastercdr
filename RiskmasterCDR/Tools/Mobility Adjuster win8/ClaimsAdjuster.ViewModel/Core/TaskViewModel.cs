﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel
{
    public class TaskViewModel : ClaimsAdjuster
    {
        #region Fields

        private ICommand saveTaskCommand;
        private ObservableCollection<Claim> cscClaims;
        private ObservableCollection<Event> cscEvents;
        private ClaimViewModel claimVM;
        private List<Code> taskTypes;
        private ICommand closeTaskCommand;
        private bool isTaskClosing;
        private bool isCreatingTask;
        private ICommand addTaskCommand;
        private string validationErrorMessage;
        private EventViewModel eventVM;
        private ClaimAdjusterTask cscTask;
        private bool isEvent;
        private ICommand mediaCaptureCommand;

        #endregion

        #region Properties
        public int ClaimViewId { get; set; } //mbahl3 Mits 36145
        public ClaimAdjusterTask CscTask
        {
            get
            {
                return cscTask;
            }
            set
            {
                cscTask = value;
                RaisePropertyChanged("CscTask");
                RaisePropertyChanged("HasToSync");
                RaisePropertyChanged("IsTaskCloseEnabled");
                RaisePropertyChanged("HasServerEntryId");
                RaisePropertyChanged("TaskServerEntryIdWithType");
            }
        }

        public string TaskServerEntryIdWithType
        {
            get
            {
                return string.Format("{0},task", this.CscTask.ServerEntryId);
            }
        }

        public List<Code> TaskTypes
        {
            get
            {
                return taskTypes;
            }
            set
            {
                taskTypes = value;
                RaisePropertyChanged("TaskTypes");
            }
        }

        public ObservableCollection<Claim> CscClaims
        {
            get
            {
                if (cscClaims == null)
                    cscClaims = new ObservableCollection<Claim>();
                return cscClaims;
            }
        }

        public ObservableCollection<Event> CscEvents
        {
            get
            {
                if (cscEvents == null)
                    cscEvents = new ObservableCollection<Event>();
                return cscEvents;
            }
        }

        public ClaimViewModel ClaimVM
        {
            get
            {
                if (claimVM == null)
                    claimVM = new ClaimViewModel();
                return claimVM;
            }
        }

        public EventViewModel EventVM
        {
            get
            {
                if (eventVM == null)
                    eventVM = new EventViewModel();
                return eventVM;
            }
        }

        public string TaskTimeText
        {
            get
            {
                TimeSpan time = CscTask.TaskDateTime - DateTime.Now;
                if (time.TotalHours < 1 && time.TotalHours >= 0 && time.TotalMinutes <= 15)
                    return string.Format(MainViewModel.Instance.GetStringResource("InMinsText"), Math.Round(time.TotalMinutes));
                else if (CscTask.TaskDateTime.Date == DateTime.Now.Date)
                    return string.Format(MainViewModel.Instance.GetStringResource("TodayText"));
                return string.Empty;
            }
        }

        public Code TaskType
        {
            get
            {
                return this.CscTask.SelectedTaskCodeType;
            }
        }

        public bool IsNew
        {
            get
            {
                return CscTask != null && CscTask.Status == (int)DataStatus.NewlyAddedFromServer;
            }
        }

        public override bool HasToSync
        {
            get
            {
                return CscTask != null && (CscTask.Status == (int)DataStatus.SyncFail || CscTask.Status == (int)DataStatus.Modified);

            }
        }

        public bool IsTaskCloseEnabled
        {
            get
            {
                return !isTaskClosing && HasServerEntryId;
            }
        }

        public bool IsCreatingTask
        {
            get
            {
                return isCreatingTask;
            }
            set
            {
                isCreatingTask = value;
                RaisePropertyChanged("IsCreatingTask");
            }
        }

        public bool HasServerEntryId
        {
            get
            {
                return CscTask.ServerEntryId != 0;
            }
        }

        public string ValidationErrorMessage
        {
            get
            {
                return validationErrorMessage;
            }
            set
            {
                validationErrorMessage = value;
                RaisePropertyChanged("ValidationErrorMessage");
            }
        }

        public bool IsEvent
        {
            get
            {
                return isEvent;
            }
            set
            {
                isEvent = value;
                RaisePropertyChanged("IsEvent");
            }
        }
        #endregion

        #region Commands

        public ICommand SaveTaskCommand
        {
            get
            {
                if (saveTaskCommand == null)
                    saveTaskCommand = new RelayCommand(OnSaveTaskCommand);
                return saveTaskCommand;
            }
        }

        public ICommand CloseTaskCommand
        {
            get
            {
                if (closeTaskCommand == null)
                    closeTaskCommand = new RelayCommand<string>(OnCloseTaskCommand);
                return closeTaskCommand;
            }
        }

        public ICommand AddTaskCommand
        {
            get
            {
                if (addTaskCommand == null)
                    addTaskCommand = new RelayCommand(OnAddTaskCommand);
                return addTaskCommand;
            }
        }

        public ICommand MediaCaptureCommand
        {
            get
            {
                if (mediaCaptureCommand == null)
                    mediaCaptureCommand = new RelayCommand<string>(OnMediaCaptureCommand);
                return mediaCaptureCommand;
            }
        }

        #endregion

        #region Methods

        public override IServiceRequest CreateServiceRequest()
        {
            string serviceId = string.Empty;
            if (HasServerEntryId)
                serviceId = string.Format(ServiceRequestIds.TaskDetailsRequest, this.CscTask.ServerEntryId);
            else
            {
                string id = string.Format("{0}{1}", Constants.Local, CscTask.Id);
                serviceId = string.Format(ServiceRequestIds.TaskDetailsRequest, id);
            }
            TaskDetailsRequest taskDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TaskDetailsRequest;
            if (taskDetailsRequest == null)
                taskDetailsRequest = new TaskDetailsRequest(serviceId, this.CscTask.ServerEntryId);
            taskDetailsRequest.Result = this;
            return taskDetailsRequest;
        }

        private async void OnSaveTaskCommand()
        {
            IsCreatingTask = true;
            try
            {
                if (TaskType == null || (string.IsNullOrEmpty(CscTask.ClaimNumber) && string.IsNullOrEmpty(CscTask.EventNumber)))
                    return;
                CscTask.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                CscTask.TaskType = string.Format("{0} {1}", TaskType.ShortCode, TaskType.Description);
                CscTask.TaskTypeId = TaskType.CodeId;
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    bool uploadSuccess = await ExecuteSaveTask();
                    CscTask.Status = (int)(uploadSuccess ? DataStatus.SyncSuccess : DataStatus.Modified);
                    await MainViewModel.Instance.CscRepository.SaveClaimAdjusterTaskAsync(CscTask);
                    //MainViewModel.Instance.HomePageVM.LoadTasks();
                }
                else
                {
                    CscTask.Status = (int)DataStatus.Modified;
                    await MainViewModel.Instance.CscRepository.SaveClaimAdjusterTaskAsync(CscTask);
                }
                MainViewModel.Instance.AddNewlyCreatedTask(CscTask);
                MainViewModel.Instance.GenerateToastData();
                if (MainViewModel.Instance.CurrentFrame.CanGoBack)
                    MainViewModel.Instance.CurrentFrame.GoBack();

            }
            catch (Exception ex)
            {
                MessageHelper.Show(MainViewModel.Instance.GetStringResource("SaveFailedText"));
            }
            finally
            {
            }
            IsCreatingTask = false;
        }

        /// <summary>
        /// Executes the request and Gets Response to Create New Task
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ExecuteSaveTask()
        {
            bool success = false;
            try
            {
                string taskTypeText = string.Format("{0}|{1} {2}", TaskType.CodeId, TaskType.ShortCode, TaskType.Description);
                string idTypeText = IsEvent ? CscTask.EventNumber : CscTask.ClaimNumber;
                string attachTableText = IsEvent ? Constants.EVENT : Constants.CLAIM;
                string createTaskRequest = string.Format(MobileCommonService.CreateTaskRequest, MainViewModel.Instance.AuthenticationToken, CscTask.TaskName, MainViewModel.Instance.CurrentUserInfo.UserName,
                    CscTask.TaskDescription, CscTask.TaskDateTime.ToString("MM/dd/yyyy"), CscTask.TaskDateTime.ToString("hh:mm tt"), taskTypeText, idTypeText, attachTableText);
                string response = await MobileCommonService.ProcessRequestAsync(createTaskRequest);
                await ParseAndUpdateTask(response);
                success = true;
            }
            catch
            {
                success = false;

            }
            return success;
        }

        /// <summary>
        /// Parse the add task response and updates the Task
        /// </summary>
        public async Task ParseAndUpdateTask(string response)
        {
            XDocument xDoc = XDocument.Parse(response);
            var msgStatus = xDoc.Descendants("MsgStatus").FirstOrDefault();
            if (msgStatus != null)
            {
                var msgCode = msgStatus.Descendants("MsgStatusCd").FirstOrDefault();
                if (msgCode != null)
                {
                    string msg = msgCode.Value;
                    if (msg == "Success")
                    {
                        var document = xDoc.Descendants("Document").FirstOrDefault();
                        if (document != null)
                        {
                            var diary = document.Descendants("Diary").FirstOrDefault();
                            if (diary != null)
                            {
                                string entryId = diary.Attribute("EntryId").Value;
                                CscTask.ServerEntryId = Convert.ToInt32(entryId);
                                await MainViewModel.Instance.CscRepository.SaveClaimAdjusterTaskAsync(CscTask);
                            }
                        }
                    }
                }
            }
        }

        public async Task LoadClaims()
        {
            if (CscTask != null)
            {
                List<Claim> claimItems = null;
                //TODO:Filter claims when Events are available uncomment below statements
                //if (!string.IsNullOrEmpty(CscTask.EventId))
                //    claimItems = await MainViewModel.Instance.CscRepository.GetClaimsByEventAsync(CscTask.EventId);
                //else
                claimItems = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
                CscClaims.Clear();
                foreach (Claim claim in claimItems)
                {
                    CscClaims.Add(claim);
                }
            }
        }

        public async Task LoadEvents()
        {
            if (CscTask != null)
            {
                List<Event> eventItems = null;
                //TODO:Filter claims when Events are available uncomment below statements
                //if (!string.IsNullOrEmpty(CscTask.EventId))
                //    claimItems = await MainViewModel.Instance.CscRepository.GetClaimsByEventAsync(CscTask.EventId);
                //else
                eventItems = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
                CscEvents.Clear();
                foreach (Event events in eventItems)
                {
                    CscEvents.Add(events);
                }
            }
        }

        public async Task InitializeNewTask()
        {


            if (!IsEvent)
                await LoadClaims();
            else
                await LoadEvents();
            TaskTypes = await MainViewModel.Instance.CscRepository.GetCodesByTypeAsync(CodeType.WPA_ACTIVITIES);
        }

        /// <summary>
        /// Gets initial items claimDetails etc
        /// </summary>
        public async Task InitializeTaskDetails()
        {
            if (CscTask.TaskParentType == ParentType.Claim)
            {
                //mbahl3 Mits 36145
                ClaimVM.CscClaim = this.ClaimViewId != default(int) ? await MainViewModel.Instance.CscRepository.GetClaimDetails(this.ClaimViewId) :
                    await MainViewModel.Instance.CscRepository.GetClaimDetails(CscTask.ClaimNumber);
                //mbahl3 Mits 36145
                if (!ClaimVM.CscClaim.IsDetailDownloaded && MainViewModel.Instance.IsInternetAvailable)
                    await MainViewModel.Instance.GetClaimDetails(ClaimVM.CscClaim,false);
                RaisePropertyChanged("ClaimVM");
                ClaimVM.RaisePropertyChanged("CscClaim");
                //bug #20384 fix
                (ClaimVM.LaunchMapCommand as RelayCommand).RaiseCanExecuteChanged();
                (ClaimVM.LaunchMailCommand as RelayCommand).RaiseCanExecuteChanged();
                (ClaimVM.LaunchSkypeCommand as RelayCommand).RaiseCanExecuteChanged();
            }
            else if (CscTask.TaskParentType == ParentType.Event)
            {
                EventVM.CscEvent = await MainViewModel.Instance.CscRepository.GetEventByEventNumberAsync(CscTask.EventNumber);
                var eventVMLaunchMapCommand = (EventVM.LaunchMapCommand as RelayCommand);
                eventVMLaunchMapCommand.RaiseCanExecuteChanged();
                eventVMLaunchMapCommand.RaiseCanExecuteChanged();
                eventVMLaunchMapCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Uploads items if any and closed the task
        /// </summary>
        private async void OnCloseTaskCommand(string isBackgroundRemove)
        {
            Action closeAction = new Action(async () =>
            {
                isTaskClosing = true;
                if (MainViewModel.Instance.ClosedTaskIds.Count > 0 && MainViewModel.Instance.ClosedTaskIds.Contains(CscTask.ServerEntryId))
                    return;
                else
                    MainViewModel.Instance.ClosedTaskIds.Add(CscTask.ServerEntryId);
                RaisePropertyChanged("IsTaskCloseEnabled");
                CscTask.Status = (int)DataStatus.ClosedOffline;
                MainViewModel.Instance.ClosedTaskList.Add(CscTask.Id);
                await MainViewModel.Instance.CscRepository.SaveClaimAdjusterTaskAsync(CscTask);
                RemoveClosedTaskFromVM();
                List<MediaViewModel> tempPictures = Pictures.ToList();
                List<MediaViewModel> tempVideos = Videos.ToList();
                List<MediaViewModel> tempAudios = Audios.ToList();
                if (isBackgroundRemove == "false")
                    GoBack();
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    string noteItem = string.Empty;
                    Message uploadMessage = new Message();
                    uploadMessage.Authorization = MainViewModel.Instance.AuthenticationToken;
                    uploadMessage.Document = new Document();
                    uploadMessage.Document.MobAdjuster = true;

                    Documents attachments = new Documents();

                    if (CscTask.TaskParentType == ParentType.Event)
                    {
                        attachments.EVENT_NUMBER = CscTask.EventNumber;
                        noteItem = string.Format("{0}{1}", Constants.EventNotes, CscTask.EventNumber);
                    }
                    else if (CscTask.TaskParentType == ParentType.Claim)
                    {
                        attachments.CLAIM_NUMBER = CscTask.ClaimNumber;
                        noteItem = string.Format("{0}{1}", Constants.Notes, CscTask.ClaimNumber);
                    }
                    attachments.DocumentUpload = new List<DocumentUpload>();

                    StorageFolder storageFolder = null;
                    //preparing Pictures for upload
                    if (tempPictures != null && tempPictures.Count > 0)
                    {
                        if (storageFolder == null)
                            storageFolder = await MainViewModel.Instance.UserDSNFolder.GetFolderAsync(CscTask.ServerEntryId.ToString());
                        StorageFolder picFolder = await storageFolder.GetFolderAsync(Constants.PhotoFolder);
                        foreach (var item in tempPictures)
                        {
                            StorageFile storageFile = await picFolder.GetFileAsync(item.FileName);

                            DocumentUpload upload = await CreateDocumentUpload(storageFile, noteItem);
                            upload.type_id = Constants.PictureTypeID;
                            attachments.DocumentUpload.Add(upload);
                        }
                    }

                    //preparing Videos for upload
                    if (tempVideos != null && tempVideos.Count > 0)
                    {
                        if (storageFolder == null)
                            storageFolder = await MainViewModel.Instance.UserDSNFolder.GetFolderAsync(CscTask.ServerEntryId.ToString());
                        StorageFolder videoFolder = await storageFolder.GetFolderAsync(Constants.VideoFolder);
                        foreach (var item in tempVideos)
                        {
                            StorageFile storageFile = await videoFolder.GetFileAsync(item.FileName);

                            DocumentUpload upload = await CreateDocumentUpload(storageFile, noteItem);
                            upload.type_id = Constants.VideoTypeID;
                            attachments.DocumentUpload.Add(upload);
                        }
                    }

                    //preparing Audio files for upload
                    if (tempAudios != null && tempAudios.Count > 0)
                    {
                        if (storageFolder == null)
                            storageFolder = await MainViewModel.Instance.UserDSNFolder.GetFolderAsync(CscTask.ServerEntryId.ToString());
                        StorageFolder audioFolder = await storageFolder.GetFolderAsync(Constants.AudioFolder);
                        foreach (var item in tempAudios)
                        {
                            StorageFile storageFile = await audioFolder.GetFileAsync(item.FileName);

                            DocumentUpload upload = await CreateDocumentUpload(storageFile, noteItem);
                            upload.type_id = Constants.AudioTypeID;
                            attachments.DocumentUpload.Add(upload);
                        }
                    }

                    if (attachments.DocumentUpload.Count > 0)
                    {
                        uploadMessage.Document.Documents = attachments;
                        bool hasUploaded = false;
                        int uploadRetryCount = 0;
                           do
                        {
                            if (uploadRetryCount<=0)
                                hasUploaded = await UploadDocument(uploadMessage);
                            else
                            {
                                bool allsuccess= true;
                                foreach (var doc in attachments.DocumentUpload)
                                {
                                    uploadMessage.Document.Documents.DocumentUpload = new List<DocumentUpload>();
                                    uploadMessage.Document.Documents.DocumentUpload.Add(doc);
                                    allsuccess = allsuccess  && await UploadDocument(uploadMessage);
                                    if (!allsuccess)
                                        break;
                                }
                                hasUploaded = allsuccess;
                            }
                            
                            uploadRetryCount += 1;
                        } while (!hasUploaded && uploadRetryCount < Constants.MaxUploadRetryCount);
                        if (!hasUploaded)
                        {
                            isTaskClosing = false;
                            RaisePropertyChanged("IsTaskCloseEnabled");
                            return;
                        }
                    }
                    string uploadRequest = string.Format(MobileUploadService.TaskCloseUploadRequest, MainViewModel.Instance.AuthenticationToken, CscTask.ServerEntryId, string.Empty);
                    bool isTaskClosed = await ExecuteCloseRequest(uploadRequest);
                    if (isTaskClosed == true)
                    {
                        MainViewModel.Instance.GenerateToastData();

                        //Deleting files after upload from DB and folder
                        if (tempPictures.Count > 0)
                        {
                            var photoAttachments = tempPictures.Select(p => p.MediaElement).ToList();
                            await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(photoAttachments);
                        }
                        if (tempVideos.Count > 0)
                        {
                            var videoAttachments = tempVideos.Select(p => p.MediaElement).ToList();
                            await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(videoAttachments);
                        }
                        if (tempAudios.Count > 0)
                        {
                            var audioAttachments = tempAudios.Select(p => p.MediaElement).ToList();
                            await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(audioAttachments);
                        }


                        storageFolder = await StorageHelper.FolderExist(MainViewModel.Instance.UserDSNFolder, CscTask.ServerEntryId.ToString());
                        if (storageFolder != null)
                            await storageFolder.DeleteAsync(StorageDeleteOption.Default);

                        List<ClaimAdjusterTask> allTasks = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
                        var task = allTasks.FirstOrDefault(s => s.ServerEntryId.ToString().ToLowerInvariant() == CscTask.ServerEntryId.ToString());
                        await MainViewModel.Instance.CscRepository.DeleteAsync<ClaimAdjusterTask>(task);
                        
                    }
                }
                MainViewModel.Instance.ClosedTaskIds.Remove(CscTask.ServerEntryId);
                isTaskClosing = false;
                RaisePropertyChanged("IsTaskCloseEnabled");
            });
            if (isBackgroundRemove == "false")
            {
                MessageHelper.Show(MainViewModel.Instance.GetStringResource("TaskCloseMsg"),
                    MainViewModel.Instance.GetStringResource("OKText"),
                    MainViewModel.Instance.GetStringResource("CancelText"), closeAction);
            }
            else
            {
                closeAction();
            }
        }

        /// <summary>
        /// Creates and returns DocumentUpload
        /// </summary>
        private async Task<DocumentUpload> CreateDocumentUpload(StorageFile storageFile, string note)
        {
            string dataString = await GetFileString(storageFile);
            DocumentUpload upload = new DocumentUpload();
            upload.content = dataString;
            upload.filename = storageFile.DisplayName;
            upload.title = storageFile.DisplayName;
            upload.subject = Constants.Subject;
            upload.type = StringHelpers.GetFileType(storageFile.FileType.Replace(".", string.Empty));
            upload.notes = note;
            return upload;
        }

        /// <summary>
        /// Executes and uploads the document upload item
        /// </summary>
        /// <param name="uploadMessage"></param>
        /// <returns></returns>
        private async Task<bool> UploadDocument(Message uploadMessage)
        {
            bool success = false;
            try
            {
                string requestXml = StringHelpers.SerializeCustom(uploadMessage);
                string documentCreationResponse = await MobileDocumentService.MobileDocumentUploadRequestAsync(requestXml);
                List<string> strFilesUploaded = ParsingHelper.ParseAttachmentCreatResponse(documentCreationResponse);
                //If null Retry Upload
                //If not then loop through all items and compare with the list we uploaded to mark the DB file appropriately for delete.
                if (strFilesUploaded != null && strFilesUploaded.Count == uploadMessage.Document.Documents.DocumentUpload.Count)
                    success = true;
            }
            catch (Exception ex)
            {
                success = false;
            }
            return success;
        }

        public async Task<bool> ExecuteCloseRequest(string uploadRequest)
        {
            bool isSuccessFull = false;
            try
            {
                string uploadResult = await MobileUploadService.UploadRequestAsync(uploadRequest);
                isSuccessFull = ParsingHelper.UploadResultParse(uploadResult);
            }
            catch (Exception ex)
            {
                return false;
            }
            return isSuccessFull;
        }

        /// <summary>
        /// Removes closed Tasks from View Collection
        /// </summary>
        private void RemoveClosedTaskFromVM()
        {
            //Remove item from HomePage VM
            HomePageViewModel homeVM = MainViewModel.Instance.HomePageVM;
            if (homeVM.Tasks != null && homeVM.Tasks.Count > 0)
            {
                TaskViewModel task = homeVM.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                if (task != null)
                    homeVM.Tasks.Collection.Remove(task);
            }

            //Remove item from Tasks VM
            TasksRequest tasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, string.Empty)) as TasksRequest;
            if (tasksRequest != null)
            {
                TaskCollection tasks = tasksRequest.Result as TaskCollection;
                if (tasks != null && tasks.Collection.Count > 0)
                {
                    TaskViewModel task = tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                    if (task != null)
                    {
                        if (tasks.AllTasks != null)//Removes item from all task and collection
                            tasks.AllTasks.Remove(task.CscTask);
                        tasks.Collection.Remove(task);
                        tasks.RaisePropertyChanged("Count");
                    }
                }
            }

            if (CscTask.TaskParentType == ParentType.Claim)
            {
                //Remove item from claim Tasks collection VM
                TasksRequest claimTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, CscTask.ClaimNumber)) as TasksRequest;
                if (claimTasksRequest != null)
                {
                    TaskCollection tasks = claimTasksRequest.Result as TaskCollection;
                    if (tasks != null && tasks.Collection.Count > 0)
                    {
                        TaskViewModel task = tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                        if (task != null)
                            if (task != null)
                            {
                                if (tasks.AllTasks != null)
                                    tasks.AllTasks.Remove(task.CscTask);
                                tasks.Collection.Remove(task);
                                tasks.RaisePropertyChanged("Count");
                            }
                    }
                }

                //Remove item from ClaimDetails VM
                ClaimDetailsRequest claimDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.ClaimDetailsRequest, CscTask.ClaimNumber,CscTask.ParentViewId)) as ClaimDetailsRequest;   //mbahl3 Mits 36145
                if (claimDetailsRequest != null)
                {
                    ClaimViewModel claim = claimDetailsRequest.Result as ClaimViewModel;
                    var task = claim.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                    if (task != null)
                        claim.Tasks.Collection.Remove(task);
                    claim.Tasks.RaisePropertyChanged("Count");
                    claim.RaisePropertyChanged("HasTasks");
                }
            }
            else if (CscTask.TaskParentType == ParentType.Event)
            {
                //Remove item from Event Tasks collection VM
                TasksRequest eventTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.CscTasksRequest, CscTask.EventNumber)) as TasksRequest;
                if (eventTasksRequest != null)
                {
                    TaskCollection tasks = eventTasksRequest.Result as TaskCollection;
                    if (tasks != null && tasks.Collection.Count > 0)
                    {
                        TaskViewModel task = tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                        if (task != null)
                            if (task != null)
                            {
                                if (tasks.AllTasks != null)
                                    tasks.AllTasks.Remove(task.CscTask);
                                tasks.Collection.Remove(task);
                                tasks.RaisePropertyChanged("Count");

                            }
                    }
                }

                //Remove item from EventDetails VM
                EventDetailsRequest eventDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.EventDetailsRequest, CscTask.EventNumber)) as EventDetailsRequest;
                if (eventDetailsRequest != null)
                {
                    EventViewModel eventVM = eventDetailsRequest.Result as EventViewModel;
                    var task = eventVM.Tasks.Collection.FirstOrDefault(s => s.CscTask.Id == CscTask.Id);
                    if (task != null)
                        eventVM.Tasks.Collection.Remove(task);
                    eventVM.Tasks.RaisePropertyChanged("Count");
                    eventVM.RaisePropertyChanged("HasTasks");
                }
            }
        }

        private void OnAddTaskCommand()
        {

            PerformValidation(this.CscTask);

            if (AllErrors.Count > 0)
            {
                this.ValidationErrorMessage = MainViewModel.Instance.GetStringResource("AddTaskRequiredFieldsErrorMsg");
                return;
            }
            bool hasValidCharacters = CheckValidCharacters();
            if (!hasValidCharacters)
            {
                ValidationErrorMessage = MainViewModel.Instance.GetStringResource("InvalidTextInputText");
                return;
            }

            // Validate date time
            CscTask.TaskDateTime = CscTask.SelectedDateTimeOffset.Date + CscTask.SelectedTimeSpan;

            if (CscTask.TaskDateTime.Ticks < DateTime.Now.Ticks /*|| CscTask.TaskDateTime.Ticks > MainViewModel.Instance.TaskFilterToDate.Ticks*/)
            {
                ValidationErrorMessage = String.Format(MainViewModel.Instance.GetStringResource("AddTaskInvalidDateTimeErrorMsg"), Constants.TaskListingDateRange);
                return;
            }

            SaveTaskCommand.Execute(null);
        }

        /// <summary>
        /// validation check to avoid special char like (<,&)
        /// </summary>
        public bool CheckValidCharacters()
        {
            bool isValid = true;
            if (StringHelpers.CheckValidity(CscTask.TaskName))
                isValid = false;
            else if (!string.IsNullOrWhiteSpace(CscTask.TaskDescription) && StringHelpers.CheckValidity(CscTask.TaskDescription))
                isValid = false;
            return isValid;
        }

        public void ClearCollection()
        {
            Videos.Clear();
            Pictures.Clear();
            Audios.Clear();
        }

        public void GoBack()
        {
            if (MainViewModel.Instance.CurrentFrame.CurrentSourcePageType != null && MainViewModel.Instance.CurrentFrame.CurrentSourcePageType.Name == "TaskDetailsPage" && MainViewModel.Instance.CurrentFrame.CanGoBack)
                MainViewModel.Instance.CurrentFrame.GoBack();
        }

        private async Task<string> GetFileString(StorageFile file)
        {
            Stream stream = await file.OpenStreamForReadAsync();
            BinaryReader binaryReader = new BinaryReader(stream);
            byte[] bytes = binaryReader.ReadBytes((int)stream.Length);
            return Convert.ToBase64String(bytes);
        }


        protected override async void OnNavigateToMediaListingsPage(string mediaCaptureTypeParameter)
        {
            base.OnNavigateToMediaListingsPage(mediaCaptureTypeParameter);
            var mediaCaptureType = (MediaCaptureType)Enum.Parse(typeof(MediaCaptureType), mediaCaptureTypeParameter, true);
            NavigatingToMediaListingsPage(CscTask.ServerEntryId.ToString(), ParentType.Task, mediaCaptureType);
        }

        private void OnMediaCaptureCommand(string mediaCaptureType)
        {
            string id = string.Format("{0}{1}", Constants.Local, CscTask.Id);
            string localServiceId = string.Format(ServiceRequestIds.TaskDetailsRequest, id);
            if (ServiceId == localServiceId)
            {
               IServiceRequest newRequest =  CreateServiceRequest();
               (MainViewModel.Instance.CurrentFrame.Content as Windows.UI.Xaml.Controls.Page).DataContext = newRequest.Result;
            }
            switch (mediaCaptureType)
            {
                case "Photo":
                    MainViewModel.Instance.NavigateToTakePhotoCommand.Execute(TaskServerEntryIdWithType);
                    break;
                case "Video":
                    MainViewModel.Instance.NavigateToTakeVideoCommand.Execute(TaskServerEntryIdWithType);
                    break;
                case "Audio":
                    MainViewModel.Instance.NavigateToRecordAudioCommand.Execute(TaskServerEntryIdWithType);
                    break;
            }

        }

        #endregion
    }

    //Fix to resolve issue with comboBox
    public class CustomKeyValuePair<TKey, TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }
    }
}
