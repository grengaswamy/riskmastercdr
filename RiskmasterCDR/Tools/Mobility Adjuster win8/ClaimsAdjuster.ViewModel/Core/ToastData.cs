﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ClaimsAdjuster.ViewModel
{
    [XmlRoot("ToastData")]
    public class ToastData
    {
        public DueTask[] DueTasks { get; set; }

        [XmlAttribute("TotalDueTasks")]
        public int TotalDueTasks { get; set; }
    }

    [XmlRoot("DueTask")]
    public class DueTask
    {
        [XmlAttribute("DueDate")]
        public string DueDate { get; set; }

        [XmlAttribute("TaskName")]
        public string TaskName { get; set; }

        [XmlAttribute("Type")]
        public string TaskTypeText { get; set; }
    }
}
