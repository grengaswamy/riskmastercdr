﻿using System;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System.Threading.Tasks;
using System.Windows.Input;
using ClaimsAdjuster.ViewModel.Helpers;
using Windows.Devices.Enumeration;
using Windows.Media.Devices;
using Windows.Foundation;
using Windows.Media;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using System.Threading;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel
{
    public class MediaCaptureViewModel : ClaimsAdjuster
    {

        #region Fields

        private string idWithType;
        private bool hideVideoPreview;
        private MediaCaptureType captureType;
        private ICommand storePhotoCommand;

        private Windows.Media.Capture.MediaCapture mediaCaptureMgr_Audio;
        private Windows.Storage.StorageFile recordStorageFile_Audio;
        private bool isRecording;
        private bool isRecorded;
        private string recordingTimeText = "00:00:00";
        private bool canRecordDurationVisible;
        private bool m_bUserRequestedRaw;
        private bool m_bRawAudioSupported;

        private ICommand recordAudioCommand;
        private ICommand retakeAudioCommand;
        private ICommand cancelAudioCommand;
        private ICommand storeAudioDataCommand;
        
        #endregion

        #region Constructor

        public MediaCaptureViewModel()
        {

        }
        #endregion

        #region Events

        public event EventHandler RecordAudioStarted;
        public event EventHandler RecordAudioStopped;

        #endregion

        #region Properties
        public string RelatedParentId { get; private set; }

        /// <summary>
        /// Gets or sets the media capture type
        /// </summary>
        public MediaCaptureType CaptureType
        {
            get
            {
                return captureType;
            }
            set
            {
                captureType = value;
                RaisePropertyChanged("IsPictureMode");
                RaisePropertyChanged("IsVideoMode");
                RaisePropertyChanged("IsAudioMode");
            }
        }

        public ParentType RelatedParentType { get; set; }

        public bool IsPictureMode
        {
            get
            {
                return this.CaptureType == MediaCaptureType.Photo;
            }
        }

        public bool IsVideoMode
        {
            get
            {
                return this.CaptureType == MediaCaptureType.Video;
            }
        }

        public bool IsAudioMode
        {
            get
            {
                return this.CaptureType == MediaCaptureType.Audio;
            }
        }

        public string PreviewImageName { get; set; }

        public string PreviewImagePath
        {
            get
            {
                string folder = Constants.PhotoFolder;
                if(IsVideoMode)
                    folder = Constants.VideoFolder;
                else if (IsAudioMode)
                    folder = Constants.AudioFolder;
                return string.Format(Constants.PreviewImagePath, MainViewModel.Instance.CurrentUserInfo.DSNName, this.RelatedParentId, folder, this.PreviewImageName);
            }
        }

        public string IdWithType
        {
            get
            {
                return idWithType;
            }
            set
            {
                idWithType = value;

                var data = idWithType.Split(',');   // Seperate ParentId and ParentType

                this.RelatedParentId = data[0];
                this.RelatedParentType = (data[1].ToLower() == "claim") ? ParentType.Claim : ParentType.Task;
            }
        }

        public bool IsRecording
        {
            get
            {
                return isRecording;
            }
            set
            {
                isRecording = value;
                RaisePropertyChanged("IsRecording");
            }
        }

        public bool IsRecorded
        {
            get
            {
                return isRecorded;
            }
            set
            {
                isRecorded = value;
                RaisePropertyChanged("IsRecorded");
            }
        }

        public string RecordingTimeText
        {
            get
            {
                return recordingTimeText;
            }
            set
            {
                recordingTimeText = value;
                RaisePropertyChanged("RecordingTimeText");
            }
        }

        public bool CanRecordDurationVisible
        {
            get
            {
                return canRecordDurationVisible;
            }
            set
            {
                canRecordDurationVisible = value;
                RaisePropertyChanged("CanRecordDurationVisible");
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Saves the captured item to Db
        /// </summary>
        public async void SaveCapturedItem()
        {
            Attachment capturedItem = new Attachment()
            {
                ParentType = this.RelatedParentType,
                ParentId = this.RelatedParentId,
                FileName = this.PreviewImageName,
                FileLocation = this.PreviewImagePath,
                Status = 1,
                UserId = 1,
                AttachmentType = this.CaptureType,
                CreatedOn = DateTime.Now
            };

            await MainViewModel.Instance.CscRepository.SaveAttachmentAsync(capturedItem);

            // Reload photo collection
            ReloadCollectionAsync();
        }

        /// <summary>
        /// Reload collection
        /// </summary>
        public async void ReloadCollectionAsync()
        {
            string serviceId = string.Empty;
            ClaimsAdjuster claimsAdjusterVM = null;
            if (this.RelatedParentType == ParentType.Claim)
            {
                serviceId = string.Format(ServiceRequestIds.ClaimDetailsRequest, this.RelatedParentId,0); //mbahl3 Mits 36145
                ClaimDetailsRequest claimRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimDetailsRequest;
                if (claimRequest != null)
                {
                    claimsAdjusterVM = claimRequest.Result as ClaimViewModel;
                }
            }
            else
            {
                serviceId = string.Format(ServiceRequestIds.TaskDetailsRequest, this.RelatedParentId);
                TaskDetailsRequest taskRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TaskDetailsRequest;
                if (taskRequest != null)
                {
                    claimsAdjusterVM = taskRequest.Result as TaskViewModel;
                }
            }
            if (claimsAdjusterVM != null)
            {
                switch (this.CaptureType)
                {
                    case MediaCaptureType.Audio:
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Audio, this.RelatedParentType,false);
                        break;
                    case MediaCaptureType.Video:
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Video, this.RelatedParentType,false);
                        break;
                    case MediaCaptureType.Photo:
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Photo, this.RelatedParentType,false);
                        break;
                    default:
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Audio, this.RelatedParentType);
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Video, this.RelatedParentType);
                        claimsAdjusterVM.LoadMediaContent(this.RelatedParentId, MediaCaptureType.Photo, this.RelatedParentType);
                        break;

                }
            }
        }
        #endregion

        #region Record Audio

        public async void InitializeAudioCapture()
        {
            this.IsRecording = false;
            this.IsRecorded = false;
            this.RecordingTimeText = "00:00:00";
            this.CanRecordDurationVisible = false;

            //Read system's raw audio stream support
            String[] propertiesToRetrieve = { "System.Devices.AudioDevice.RawProcessingSupported" };
            try
            {
                var device = await DeviceInformation.CreateFromIdAsync(MediaDevice.GetDefaultAudioCaptureId(AudioDeviceRole.Communications), propertiesToRetrieve);
                m_bRawAudioSupported = device.Properties["System.Devices.AudioDevice.RawProcessingSupported"].Equals(true);
            }
            catch (Exception e)
            {
                //ShowExceptionMessage(e);
            }
        }

        public async void CloseAudioCapture()
        {
            if (IsRecording)
            {
                await mediaCaptureMgr_Audio.StopRecordAsync();
                this.IsRecording = false;
                this.IsRecorded = false;
                this.CanRecordDurationVisible = false;
                mediaCaptureMgr_Audio.Dispose();
            }

            if (mediaCaptureMgr_Audio != null)
            {
                mediaCaptureMgr_Audio.Dispose();
            }

        }

        public async Task StartAudioDeviceReady()
        {
                mediaCaptureMgr_Audio = new Windows.Media.Capture.MediaCapture();
                var settings = new Windows.Media.Capture.MediaCaptureInitializationSettings();
                settings.StreamingCaptureMode = Windows.Media.Capture.StreamingCaptureMode.Audio;
                settings.MediaCategory = Windows.Media.Capture.MediaCategory.Other;
                settings.AudioProcessing = (m_bRawAudioSupported && m_bUserRequestedRaw) ? Windows.Media.AudioProcessing.Raw : Windows.Media.AudioProcessing.Default;
                await mediaCaptureMgr_Audio.InitializeAsync(settings);

                mediaCaptureMgr_Audio.RecordLimitationExceeded += new Windows.Media.Capture.RecordLimitationExceededEventHandler(RecordLimitationExceeded);
                mediaCaptureMgr_Audio.Failed += new Windows.Media.Capture.MediaCaptureFailedEventHandler(Failed);
        }
        
        public async void RecordLimitationExceeded(Windows.Media.Capture.MediaCapture currentCaptureObject)
        {
            try
            {
                if (IsRecording)
                {
                    await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                        try
                        {
                            await mediaCaptureMgr_Audio.StopRecordAsync();
                            IsRecording = false;
                        }
                        catch (Exception e)
                        {
                        }
                    });
                }
            }
            catch (Exception e)
            {
            }
        }

        public void Failed(Windows.Media.Capture.MediaCapture currentCaptureObject, MediaCaptureFailedEventArgs currentFailure)
        {
            try
            {
                var ignored = MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    try
                    {
                        //ShowStatusMessage("Fatal error" + currentFailure.Message);

                    }
                    catch (Exception e)
                    {
                        //ShowExceptionMessage(e);
                    }
                });
            }
            catch (Exception e)
            {
                //ShowExceptionMessage(e);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Command for taking video
        /// </summary>
        public ICommand RecordAudioCommand
        {
            get
            {
                if (recordAudioCommand == null)
                    recordAudioCommand = new RelayCommand(OnStartStopRecord);
                return recordAudioCommand;
            }
        }

        /// <summary>
        /// Command for taking audio
        /// </summary>
        public ICommand RetakeAudioCommand
        {
            get
            {
                if (retakeAudioCommand == null)
                    retakeAudioCommand = new RelayCommand(OnRetakeCommand);
                return retakeAudioCommand;
            }
        }

        /// <summary>
        /// Command for taking video
        /// </summary>
        public ICommand CancelAudioCommand
        {
            get
            {
                if (cancelAudioCommand == null)
                    cancelAudioCommand = new RelayCommand(OnCancelStopRecord);
                return cancelAudioCommand;
            }
        }

        /// <summary>
        /// Command for store audio data
        /// </summary>
        public ICommand StoreAudioDataCommand
        {
            get
            {
                if (storeAudioDataCommand == null)
                    storeAudioDataCommand = new RelayCommand(OnStoreAudioDataCommand);
                return storeAudioDataCommand;
            }
        }

        private async void OnRetakeCommand()
        {
            await recordStorageFile_Audio.DeleteAsync();
            //await StartAudioDeviceReady();

            OnStartStopRecord();
        }

        private async void OnStartStopRecord()
        {
            try
            {
                if (!IsRecording)
                {
                    IsRecording = true;
                    RecordingTimeText = "00:00:00";
                    await StartAudioDeviceReady();
                    StorageFolder parentFolder = await MainViewModel.Instance.UserDSNFolder.CreateFolderAsync(this.RelatedParentId, Windows.Storage.CreationCollisionOption.OpenIfExists);
                    parentFolder = await parentFolder.CreateFolderAsync("Audio", Windows.Storage.CreationCollisionOption.OpenIfExists);

                    this.IsRecorded = false;
                    this.CanRecordDurationVisible = true;

                    this.PreviewImageName = MainViewModel.Instance.GenerateAudioFilename();

                    recordStorageFile_Audio = await parentFolder.CreateFileAsync(this.PreviewImageName, Windows.Storage.CreationCollisionOption.ReplaceExisting);

                    MediaEncodingProfile recordProfile = null;
                    recordProfile = MediaEncodingProfile.CreateMp3(Windows.Media.MediaProperties.AudioEncodingQuality.Auto);

                    await mediaCaptureMgr_Audio.StartRecordToStorageFileAsync(recordProfile, this.recordStorageFile_Audio);
                    

                    if (RecordAudioStarted != null)
                    {
                        RecordAudioStarted.Invoke(this, null);
                    }
                }
                else
                {
                    IsRecording = false;
                    this.IsRecorded = true;

                    if (RecordAudioStopped != null)
                    {
                        RecordAudioStopped.Invoke(this, null);
                    }

                    await mediaCaptureMgr_Audio.StopRecordAsync();
                }
            }
            catch (Exception exception)
            {
                if (!exception.Message.Contains("The object has been closed"))
                    MessageHelper.Show("Turn On Micro phone to use it", "ok");
                IsRecording = false;
                this.CanRecordDurationVisible = false;
            }
        }
        
        private async void OnCancelStopRecord()
        {
                this.IsRecording = false;
                this.CanRecordDurationVisible = false;
                await mediaCaptureMgr_Audio.StopRecordAsync();
                await recordStorageFile_Audio.DeleteAsync();

                if (RecordAudioStopped != null)
                {
                    RecordAudioStopped.Invoke(this, null);
                }
        }

        private void OnStoreAudioDataCommand()
        {
            this.SaveCapturedItem();
            MainViewModel.Instance.GoBackCommand.Execute(null);
        }

        #endregion



    }
}
