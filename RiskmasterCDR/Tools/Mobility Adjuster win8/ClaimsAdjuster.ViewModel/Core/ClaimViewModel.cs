﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel
{
    public class ClaimViewModel : ClaimsAdjuster
    {

        #region Fields

        private TaskCollection tasks;
        private NoteCollection notes;
        private ICommand navigateToParties;
        private ICommand refreshClaim;
        private Claim cscClaim;
        private bool _isPictureSelected;
        private ICommand navigateToPayment;
        private ICommand navigateToNotesCommand;

        #endregion

        #region Properties

        public Claim CscClaim
        {
            get
            {
                return cscClaim;
            }
            set
            {
                cscClaim = value;
                RaisePropertyChanged("CscClaim");
            }
        }

        /// <summary>
        /// Gets the tasks associated with the claim
        /// </summary>
        public TaskCollection Tasks
        {
            get
            {
                if (tasks == null)
                    tasks = new TaskCollection();
                return tasks;
            }
        }

        /// <summary>
        /// Gets the notes associated with claim
        /// </summary>
        public NoteCollection Notes
        {
            get
            {
                if (notes == null)
                    notes = new NoteCollection();
                return notes;
            }
        }

        /// <summary>
        /// Gets the claim details title
        /// </summary>
        public string ClaimTitle
        {
            get
            {
                if (string.IsNullOrEmpty(this.CscClaim.PolicyName))
                    return this.CscClaim.ClaimNumber;
                else
                    return string.Format("{0} - {1}", this.CscClaim.ClaimNumber, this.CscClaim.PolicyName);
            }
        }

        /// <summary>
        /// Gets whether claim has any tasks.
        /// </summary>
        public bool HasTasks
        {
            get
            {
                return this.Tasks.Count > 0;
            }
        }

        public string ClaimNumberWithType
        {
            get
            {
                return string.Format("{0},claim", this.CscClaim.ClaimNumber);
            }
        }

        /// <summary>
        /// Gets whether claims has any notes.
        /// </summary>
        public bool HasNotes
        {
            get
            {
                return this.Notes.Count > 0;
            }
        }

        public ICommand NavigateToParties
        {
            get
            {
                if (navigateToParties == null)
                    navigateToParties = new RelayCommand(OnNavigateToParties);

                return navigateToParties;
            }
        }

        public ICommand RefreshClaimDetail
        {
            get
            {
                if (refreshClaim == null)
                    refreshClaim = new RelayCommand<object>(OnRefreshClaim);

                return refreshClaim;
            }
        }

        public ICommand NavigateToPayment
        {
            get
            {
                if (navigateToPayment == null)
                    navigateToPayment = new RelayCommand(OnNavigateToPayment);

                return navigateToPayment;
            }
        }


        /// <summary>
        /// Command to navigate to notes listing page
        /// </summary>
        public ICommand NavigateToNotesCommand
        {
            get
            {
                if (navigateToNotesCommand == null)
                    navigateToNotesCommand = new RelayCommand<string>(OnNavigateToNotesCommand);
                return navigateToNotesCommand;
            }
        }

        /// <summary>
        /// Gets or sets the pictures captured against the claims
        /// </summary>

        public bool IsNew
        {
            get
            {
                return CscClaim != null && CscClaim.Status == (int)DataStatus.NewlyAddedFromServer;
            }
        }

        public override bool HasFullAddress
        {
            get
            {
                return CscClaim != null && !string.IsNullOrWhiteSpace(this.CscClaim.FullAddress);
            }
        }

        public override bool HasPhoneNumber
        {
            get
            {
                return CscClaim != null && (!string.IsNullOrWhiteSpace(this.CscClaim.ClaimantHomePhone) || !string.IsNullOrWhiteSpace(this.CscClaim.ClaimantOfficePhone));
            }
        }

        public override bool HasEmailId
        {
            get
            {
                return CscClaim != null && !string.IsNullOrWhiteSpace(this.CscClaim.ClaimantEmailAddress);
            }
        }


        public bool IsPictureSelected
        {
            get
            {
                return this._isPictureSelected;
            }

            set
            {
                if (this._isPictureSelected != value)
                {
                    this._isPictureSelected = value;
                    this.RaisePropertyChanged("IsPictureSelected");

                }
            }
        }


        #endregion

        #region Constructor

        public ClaimViewModel()
        {
        }

        #endregion

        #region Overrides

        protected override void OnLaunchMapCommand()
        {
            LaunchHelper.LaunchMap(this.CscClaim.FullAddress);
        }

        protected override void OnLaunchMailCommand()
        {
            LaunchHelper.LaunchMail(CscClaim.ClaimantEmailAddress);
        }
        //mbahl3 11/04/2014 changed for skype claimant number display 
        protected override void OnLaunchSkypeCommand()
        {
            if (!string.IsNullOrWhiteSpace(this.CscClaim.ClaimantOfficePhone))
            {
                LaunchHelper.LaunchSkype(CscClaim.ClaimantOfficePhone);

            }
            else if (!string.IsNullOrWhiteSpace(this.CscClaim.ClaimantHomePhone))
            {
                LaunchHelper.LaunchSkype(CscClaim.ClaimantHomePhone);
            }
          
        }
        //mbahl3 11/04/2014 changed for skype claimant number display 

        private void OnNavigateToNotesCommand(string id = "")
        {
            string serviceId = string.Format(ServiceRequestIds.CscNotesRequest, id);
            NotesRequest cscNotesRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as NotesRequest;
            if (cscNotesRequest == null)
                cscNotesRequest = new NotesRequest(serviceId);
            cscNotesRequest.ClaimNumber = id;

            cscNotesRequest.ClaimViewId = cscClaim.Id; //mbahl3  Mits 36145
            MainViewModel.Instance.Navigate(serviceId);
        }

        public override void OnNavigateToTasksCommand()
        {
            string serviceId = string.Format(ServiceRequestIds.CscTasksRequest, CscClaim.ClaimNumber);

            TasksRequest cscTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
            if (cscTasksRequest == null)
                cscTasksRequest = new TasksRequest(serviceId);
            cscTasksRequest.ParentNumber = CscClaim.ClaimNumber;

            cscTasksRequest.ClaimViewId = cscClaim.Id; //mbahl3 Mits 36145
            cscTasksRequest.ParentType = ParentType.Claim;
            MainViewModel.Instance.Navigate(serviceId);
        }

        #endregion

        #region Methods

        public override IServiceRequest CreateServiceRequest()
        {
            string serviceId = string.Format(ServiceRequestIds.ClaimDetailsRequest, this.CscClaim.ClaimNumber, this.CscClaim.Id); //mbahl3 Mits 36145

            ClaimDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimDetailsRequest;
            if (request == null)
                request = new ClaimDetailsRequest(serviceId, this.CscClaim.ClaimNumber, this.CscClaim.Id); //mbahl3 Mits 36145
        
            request.Result = this;
            return request;
        }

        public override void SetResult(string result)
        {
            base.SetResult(result);
        }

        /// <summary>
        /// Loads tasks for a claim number.
        /// </summary>
        /// <param name="claimNumber">claim number</param>
        /// 

        public async Task LoadTasksAndNotes(string claimNumber)
        {
            LoadTasks(claimNumber);
            LoadNotes(claimNumber);
        }
        public async void LoadTasks(string claimNumber)
        {
            List<ClaimAdjusterTask> taskItems = await MainViewModel.Instance.CscRepository.GetTaskByClaimAsync(claimNumber);
            //Tasks.AddToCollection(taskItems);
            Tasks.AddToCollection(MainViewModel.Instance.ReArrangeTasks(taskItems), this.CscClaim.Id);  //mbahl3 Mits 36145
            RaisePropertyChanged("HasTasks");
        }

        /// <summary>
        /// Load notes for a claim number.
        /// </summary>
        /// <param name="claimNumber">claim number</param>
        public async void LoadNotes(string claimNumber)
        {
            this.Notes.Collection.Clear();
            RaisePropertyChanged("HasNotes");
            var notes = await MainViewModel.Instance.CscRepository.GetNotesByClaimAsync(claimNumber);
            Notes.LoadNotes(notes,this.CscClaim.Id);//mbahl3 Mits 36145
            RaisePropertyChanged("HasNotes");
        }


        private void OnNavigateToParties()
        {
            PartiesInvolvedRequest partiesInvolvedRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.PartiesInvolvedRequest) as PartiesInvolvedRequest;
            if (partiesInvolvedRequest == null)
                partiesInvolvedRequest = new PartiesInvolvedRequest(ServiceRequestIds.PartiesInvolvedRequest);
            partiesInvolvedRequest.ClaimNumber = CscClaim.ClaimNumber;
            MainViewModel.Instance.Navigate(ServiceRequestIds.PartiesInvolvedRequest);
        }

        private async void OnRefreshClaim(object isFromBackgroundObject)
        {
            bool isFromBackground = Convert.ToBoolean(isFromBackgroundObject);
            //await MainViewModel.Instance.GetClaimDetails(this.CscClaim, false);
            List<MediaViewModel> tempPictures = Pictures.ToList();//Maintaining local copy since collection unloaded in back navigation
            List<MediaViewModel> tempVideos = Videos.ToList();//Maintaining local copy since collection unloaded in back navigation
            List<MediaViewModel> tempAudios = Audios.ToList();//Maintaining local copy since collection unloaded in back navigation
            //Upload Pictures & Videos & Audio files to server
            if (!isFromBackground)
            {
                MainViewModel.Instance.RefreshCommand.Execute(null);
            }
            if (MainViewModel.Instance.IsInternetAvailable)
            {
                UploadDocuments(tempPictures, tempVideos, tempAudios);
            }
            else
            {
                CscClaim.Status = (int)DataStatus.Modified;
                MainViewModel.Instance.CscRepository.SaveClaimAsync(CscClaim);
            }
            //LoadTasks(this.CscClaim.ClaimNumber);
            //LoadNotes(this.CscClaim.ClaimNumber);
            //LoadPictures(this.CscClaim.ClaimNumber);
            //LoadMediaContent(this.CscClaim.ClaimNumber, MediaCaptureType.Photo, ParentType.Claim);
            //LoadMediaContent(this.CscClaim.ClaimNumber, MediaCaptureType.Video, ParentType.Claim);
            //LoadMediaContent(this.CscClaim.ClaimNumber, MediaCaptureType.Audio, ParentType.Claim);
        }

        private async void OnNavigateToPayment()
        {

            string serviceId = string.Format(ServiceRequestIds.PaymentPageRequest);

            PaymentPageRequest cscPaymentRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as PaymentPageRequest;
            if (cscPaymentRequest == null)
                cscPaymentRequest = new PaymentPageRequest(serviceId);
            cscPaymentRequest.CurrentClaim = CscClaim;
            MainViewModel.Instance.Navigate(ServiceRequestIds.PaymentPageRequest);
        }

        private async Task GetPaymentDetails(Claim currentClaim, bool isAsyncCall = true)
        {
            try
            {
                string requestXml = string.Format(MobileCommonService.GetPaymentDetails, MainViewModel.Instance.AuthenticationToken, currentClaim.ClaimNumber);
                string paymentDetails = await MobileCommonService.ProcessRequestAsync(requestXml);
                if (isAsyncCall)
                    ParsingHelper.ParsePaymentHistory(paymentDetails, currentClaim);
                else
                    await ParsingHelper.ParsePaymentHistory(paymentDetails, currentClaim);
            }
            catch (Exception ex)
            {

            }
        }

        private async void UploadDocuments(List<MediaViewModel> tempPictures, List<MediaViewModel> tempVideos, List<MediaViewModel> tempAudios)
        {
            if ((tempPictures == null || tempPictures.Count == 0) && (tempVideos == null || tempVideos.Count == 0) && (tempAudios == null || tempAudios.Count == 0))
                return;
            if (MainViewModel.Instance.UploadingClaimNumbers.Count > 0 && MainViewModel.Instance.UploadingClaimNumbers.Contains(CscClaim.ClaimNumber))
                return;
            else
                MainViewModel.Instance.UploadingClaimNumbers.Add(CscClaim.ClaimNumber);
            Message uploadMessage = new Message();
            uploadMessage.Authorization = MainViewModel.Instance.AuthenticationToken;
            uploadMessage.Document = new Document();
            uploadMessage.Document.MobAdjuster = true;

            Documents attachments = new Documents();
            attachments.CLAIM_NUMBER = CscClaim.ClaimNumber;
            attachments.DocumentUpload = new List<DocumentUpload>();

            StorageFolder storageFolder = await MainViewModel.Instance.UserDSNFolder.GetFolderAsync(this.CscClaim.ClaimNumber);

            //Preparing pictures for upload
            if (tempPictures != null && tempPictures.Count > 0)
            {
                if (CscClaim != null && !string.IsNullOrEmpty(CscClaim.ClaimNumber))
                {
                    StorageFolder picFolder = await storageFolder.GetFolderAsync(Constants.PhotoFolder);
                    foreach (var item in tempPictures)
                    {
                        StorageFile storageFile = await picFolder.GetFileAsync(item.FileName);
                        Stream stream = await storageFile.OpenStreamForReadAsync();
                        BinaryReader binaryReader = new BinaryReader(stream);
                        byte[] bytes = binaryReader.ReadBytes((int)stream.Length);
                        string imageString = Convert.ToBase64String(bytes);

                        DocumentUpload upload = new DocumentUpload();
                        upload.content = imageString;
                        upload.filename = storageFile.DisplayName;
                        upload.title = storageFile.DisplayName;
                        upload.subject = Constants.Subject;
                        upload.notes = Constants.Notes + CscClaim.ClaimNumber;
                        upload.type_id = Constants.PictureTypeID;
                        upload.type = StringHelpers.GetFileType(storageFile.FileType.Replace(".", string.Empty));

                        attachments.DocumentUpload.Add(upload);

                    }
                }
            }

            //Preparing videos for upload
            if (tempVideos != null && tempVideos.Count > 0)
            {
                if (CscClaim != null && !string.IsNullOrEmpty(CscClaim.ClaimNumber))
                {
                    StorageFolder videoFolder = await storageFolder.GetFolderAsync(Constants.VideoFolder);
                    foreach (var item in tempVideos)
                    {
                        StorageFile storageFile = await videoFolder.GetFileAsync(item.FileName);
                        Stream stream = await storageFile.OpenStreamForReadAsync();
                        BinaryReader binaryReader = new BinaryReader(stream);
                        byte[] bytes = binaryReader.ReadBytes((int)stream.Length);
                        string videoString = Convert.ToBase64String(bytes);

                        DocumentUpload upload = new DocumentUpload();
                        upload.content = videoString;
                        upload.filename = storageFile.DisplayName;
                        upload.title = storageFile.DisplayName;
                        upload.subject = Constants.Subject;
                        upload.notes = Constants.Notes + CscClaim.ClaimNumber;
                        upload.type_id = Constants.VideoTypeID;
                        upload.type = StringHelpers.GetFileType(storageFile.FileType.Replace(".", string.Empty));

                        attachments.DocumentUpload.Add(upload);
                    }

                }
            }

            //Preparing audio files for upload
            if (tempAudios != null && tempAudios.Count > 0)
            {
                if (CscClaim != null && !string.IsNullOrEmpty(CscClaim.ClaimNumber))
                {
                    StorageFolder audioFolder = await storageFolder.GetFolderAsync(Constants.AudioFolder);
                    foreach (var item in tempAudios)
                    {
                        StorageFile storageFile = await audioFolder.GetFileAsync(item.FileName);
                        Stream stream = await storageFile.OpenStreamForReadAsync();
                        BinaryReader binaryReader = new BinaryReader(stream);
                        byte[] bytes = binaryReader.ReadBytes((int)stream.Length);
                        string audioString = Convert.ToBase64String(bytes);

                        DocumentUpload upload = new DocumentUpload();
                        upload.content = audioString;
                        upload.filename = storageFile.DisplayName;
                        upload.title = storageFile.DisplayName;
                        upload.subject = Constants.Subject;
                        upload.notes = Constants.Notes + CscClaim.ClaimNumber;
                        upload.type_id = Constants.AudioTypeID;
                        upload.type = StringHelpers.GetFileType(storageFile.FileType.Replace(".", string.Empty));

                        attachments.DocumentUpload.Add(upload);
                    }
                }
            }


            uploadMessage.Document.Documents = attachments;
            bool hasUploaded = false;
            int uploadRetryCount = 0;
            do
            {
                if (uploadRetryCount <= 0)
                    hasUploaded = await UploadDocument(uploadMessage);
                else
                {
                    bool allsuccess = true;
                    foreach (var doc in attachments.DocumentUpload)
                    {
                        uploadMessage.Document.Documents.DocumentUpload = new List<DocumentUpload>();
                        uploadMessage.Document.Documents.DocumentUpload.Add(doc);
                        allsuccess = allsuccess && await UploadDocument(uploadMessage);
                        if (!allsuccess)
                            break;
                    }
                    hasUploaded = allsuccess;
                }
                uploadRetryCount += 1;
            } while (!hasUploaded && uploadRetryCount < Constants.MaxUploadRetryCount);
            if (hasUploaded)
            {
                //Deleting files after upload from DB and folder
                if (tempPictures.Count > 0)
                {
                    var photoAttachments = tempPictures.Select(p => p.MediaElement).ToList();
                    await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(photoAttachments);
                }
                if (tempVideos.Count > 0)
                {
                    var videoAttachments = tempVideos.Select(p => p.MediaElement).ToList();
                    await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(videoAttachments);
                }
                if (tempAudios.Count > 0)
                {
                    var audioAttachments = tempAudios.Select(p => p.MediaElement).ToList();
                    await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Attachment>(audioAttachments);
                }

                //Remove the items from the current ClaimViewModel 
                IServiceRequest claimDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(string.Format(ServiceRequestIds.ClaimDetailsRequest, CscClaim.ClaimNumber,CscClaim.Id)); //mbahl3 Mits 36145
                ClaimViewModel claimVM = null;
                if (claimDetailsRequest != null)
                {
                    claimVM = claimDetailsRequest.Result as ClaimViewModel;
                    if (claimVM.Pictures.Count > 0)
                    {
                        RemoveUploadedItems(tempPictures, claimVM.Pictures, MediaCaptureType.Photo);
                        claimVM.RaisePropertyChanged("HasPictures");
                        claimVM.RaisePropertyChanged("PicturesCount");
                    }
                    if (claimVM.Videos.Count > 0)
                    {
                        RemoveUploadedItems(tempVideos, claimVM.Videos, MediaCaptureType.Video);
                        claimVM.RaisePropertyChanged("HasVideos");
                        claimVM.RaisePropertyChanged("VideosCount");
                    }
                    if (claimVM.Audios.Count > 0)
                    {
                        RemoveUploadedItems(tempAudios, claimVM.Audios, MediaCaptureType.Audio);
                        claimVM.RaisePropertyChanged("HasAudios");
                        claimVM.RaisePropertyChanged("AudiosCount");
                    }
                }

                if (claimVM == null || (claimVM != null && claimVM.Pictures.Count == 0 && claimVM.Videos.Count == 0 && claimVM.Audios.Count == 0))
                {
                    var currentFolder = await MainViewModel.Instance.UserDSNFolder.GetFolderAsync(this.CscClaim.ClaimNumber);
                    await currentFolder.DeleteAsync(StorageDeleteOption.Default);
                }
            }
            MainViewModel.Instance.UploadingClaimNumbers.Remove(CscClaim.ClaimNumber);
        }

        /// <summary>
        /// Removes the uploaded item from pictures,videos, audios collection
        /// </summary>
        private void RemoveUploadedItems(List<MediaViewModel> tempMediaItems, ObservableCollection<MediaViewModel> mediItems, MediaCaptureType mediaType)
        {
            PhotoPageRequest mediaCollectionRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.PhotoPageRequest) as PhotoPageRequest;
            MediaCollection mediaCollection = null;
            if (mediaCollectionRequest != null && mediaCollectionRequest.ParentId == CscClaim.ClaimNumber)
                mediaCollection = mediaCollectionRequest.Result as MediaCollection;
            foreach (var item in tempMediaItems)
            {
                MediaViewModel mediaItem = mediItems.FirstOrDefault(s => s.MediaElement.Id == item.MediaElement.Id);
                if (mediaItem != null)
                    mediItems.Remove(mediaItem);
                if (mediaCollectionRequest != null && mediaCollectionRequest.MediaCaptureType == mediaType && mediaCollection != null && mediaCollection.Count > 0)
                {
                    mediaItem = mediaCollection.Collection.FirstOrDefault(s => s.MediaElement.Id == item.MediaElement.Id);
                    if (mediaItem != null)
                        mediaCollection.Collection.Remove(mediaItem);
                }
            }

            if (mediaCollection != null)
                mediaCollection.RaiseCountProperties();
        }

        private async Task<bool> UploadDocument(Message uploadMessage)
        {
            bool success = false;
            try
            {
                string requestXml = StringHelpers.SerializeCustom(uploadMessage);
                string documentCreationResponse = await MobileDocumentService.MobileDocumentUploadRequestAsync(requestXml);
                List<string> strFilesUploaded = ParsingHelper.ParseAttachmentCreatResponse(documentCreationResponse);
                //If null Retry Upload
                //If not then loop through all items and compare with the list we uploaded to mark the DB file appropriately for delete.
                if (strFilesUploaded != null && strFilesUploaded.Count == uploadMessage.Document.Documents.DocumentUpload.Count)
                {
                    cscClaim.Status = (int)DataStatus.SyncSuccess;
                    await MainViewModel.Instance.CscRepository.SaveClaimAsync(CscClaim);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                success = false;
            }
            return success;
        }

        public void ClearCollection()
        {
            Tasks.Collection.Clear();
            Notes.Collection.Clear();
            Videos.Clear();
            Pictures.Clear();
            Audios.Clear();
        }


        protected override async void OnNavigateToMediaListingsPage(string mediaCaptureTypeParameter )
        {
            base.OnNavigateToMediaListingsPage(mediaCaptureTypeParameter);
            var mediaCaptureType = (MediaCaptureType)Enum.Parse(typeof(MediaCaptureType), mediaCaptureTypeParameter, true);
            NavigatingToMediaListingsPage(CscClaim.ClaimNumber, ParentType.Claim, mediaCaptureType);
        }
        #endregion
    }
}
