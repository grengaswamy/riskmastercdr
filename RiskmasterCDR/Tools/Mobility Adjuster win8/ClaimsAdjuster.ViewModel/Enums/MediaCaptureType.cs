﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{

    /// <summary>
    /// Specifies the media capture type
    /// </summary>
    public enum MediaCaptureType
    {

        /// <summary>
        /// Capture photo
        /// </summary>
        Photo,

        /// <summary>
        /// Capture video
        /// </summary>
        Video,

        /// <summary>
        /// Capture audio
        /// </summary>
        Audio
    }
}
