﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{

    /// <summary>
    /// Specifies the apps view state
    /// </summary>
    public enum ViewState
    {
        Filled,
        FullScreenPortrait,
        FullScreenLandscape,
        Snapped
    }
}
