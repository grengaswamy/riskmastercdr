﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

namespace ClaimsAdjuster.ViewModel.Sync
{
    public interface ISyncFileState
    {
        int Id { get; set; }
        string FolderName { get; set; }
        DateTimeOffset? TimeStamp { get; set; }
        string FileName { get; set; }
        int StatusAsInt { get; }
        int PreviousStatusAsInt { get; }
        SyncFileStatus Status { get; set; }
        SyncFileStatus PreviousStatus { get; set; }

        void ApplyPropertiesFrom(ISyncFile file);
    }

    public enum SyncFileStatus
    {
        Undefined = 0,
        AsServer = 1,
        Created = 2,
        Updated = 3,
        Deleted = 4,
    }

    public interface ISyncFile
    {
       
        bool Exists { get; set; }
        string FileName { get; set; }
        string FolderName { get; }
        DateTimeOffset? LastModified { get; set; }
        Uri Uri { get; set; }
        SyncFileStatus Status { get; set; }

        Task<IRandomAccessStream> OpenReadAsync();
        Task<IRandomAccessStream> OpenWriteAsync();
        Task DeleteAsync();
        Task InsertAndCommitAsync(IRandomAccessStream source);
        Task UpdateAndCommitAsync(IRandomAccessStream source);
        Task CommitAsync();

        void ApplyPropertiesFrom(ISyncFileState state);
    }

    public class SyncFile : ISyncFile
    {
        private readonly ISyncFolder _folder;

        public SyncFile(ISyncFolder folder)
        {
            if (folder == null)
                throw new Exception("no folder");

            _folder = folder;
        }

        
        public bool Exists { get; set; }
        public string FileName { get; set; }

        public string FolderName
        {
            get { return _folder.FolderName; }
        }

        public DateTimeOffset? LastModified { get; set; }
        public Uri Uri { get; set; }
        public SyncFileStatus Status { get; set; }

        public async Task<IRandomAccessStream> OpenReadAsync()
        {
            return await _folder.OpenReadAsync(this);
        }

        public async Task<IRandomAccessStream> OpenWriteAsync()
        {
            return await _folder.OpenWriteAsync(this);
        }

        public async Task DeleteAsync()
        {
            await _folder.DeleteAsync(this);
        }

        /// <summary>
        ///     Update a file and mark it ready to sync
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public async Task InsertAndCommitAsync(IRandomAccessStream source)
        {
            await _folder.InsertAndCommitAsync(this, source);
        }

        /// <summary>
        ///     Update a file and mark it ready to sync
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public async Task UpdateAndCommitAsync(IRandomAccessStream source)
        {
            await _folder.UpdateAndCommitAsync(this, source);
        }

        /// <summary>
        ///     Marks the file as ready to sync (inserted or updated based on previous state)
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            await _folder.CommitAsync(this);
        }

        public void ApplyPropertiesFrom(ISyncFileState state)
        {
            Status = SyncFileStatus.Undefined;

            if (state != null)
            {
               
                FileName = state.FileName;
                LastModified = state.TimeStamp;
                Status = state.Status;
            }
        }
    }
}
