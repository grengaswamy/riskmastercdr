﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Sync
{
    public class SyncConnectionException : Exception
    {
        public SyncConnectionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public SyncConnectionException(string message)
            : base(message)
        {
        }


    }

    public class SyncException : Exception
    {
        public SyncException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public SyncException(string message)
            : base(message)
        {
        }


    }
}
