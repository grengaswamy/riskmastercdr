﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Media.Capture;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.ViewManagement;

namespace ClaimsAdjuster.ViewModel
{
    public class MainViewModel : MainVMBase
    {
        #region Constants
        public const string AppVersion = "1.0.0";
        public const string Pname = "WIN8";
        public string OsVersion = "WIN8";
        public string OsVersionNo = "";
        public string DeviceID = "";
        public const string ClientType = "WIN8";

        private const int _maxResult = 10;
        private bool isLoadHome = false;
        #endregion

        #region Constructor
        private MainViewModel()
        {
            ClassID = Id = "HomePage";
            IsLoaded = true;

            try
            {
                DeviceID = Windows.Networking.Connectivity.NetworkInformation.GetInternetConnectionProfile().NetworkAdapter.NetworkAdapterId.ToString();
                GetSecondaryTilesList();
            }
            catch { }

            Placeholders = new Dictionary<string, Uri>();
            Placeholders.Add("PreviewSmall", new Uri("/Assets/PreviewImage.png", UriKind.RelativeOrAbsolute));
            //            serviceRequestfactory = new ServiceRequestConcreteFactory();

            InternetAvailabilityChanged += MainViewModel_InternetAvailabilityChanged;
            NetworkInformation.NetworkStatusChanged += NetworkInformation_NetworkStatusChanged;
        }
        #endregion

        #region Fields

        ServiceRequestFactory _factory;
        bool _canGoBack;
        string _headerText;


        private static string appName = "CSCClaimsAdjuster_Win8";
        private double availableheaderWidth;
        private bool isAuthenticated;
        private int userId;
        private bool isNetworkBroken;
        private StorageFolder dsnName;
        private HomePageViewModel homePageVM;
        private ObservableCollection<Event> serverEvents;
        private User currentUserInfo;
        private string webServerUrl;
        private Authentication authenticationVM;
        private bool isHomePage;
        List<string> _busyStates = new List<string>();
        HashSet<int> closedTaskList;

        private ICommand newTaskCommand;
        private ICommand navigateToTakePhotoCommand;
        private ICommand navigateToTakeVideoCommand;
        private ICommand navigateToRecordAudioCommand;
        private ICommand navigateToHomeHubCommand;
        private ICommand navigateToClaimsCommand;
        private ICommand navigateToTasksCommand;
        private ICommand navigateToEventsCommand;
        private ICommand navigateToAddNoteCommand;
        private ICommand openPhotoCommand;
        private ICommand refreshCommand;
        private ICommand unsnapToContinueCommand;
        private ICommand selectImageCommand;
        private ICommand searchCommand;
        //Secodary Live Tiles
        private IReadOnlyList<SecondaryTile> pinnedTiles;
        private ICommand pinTileCommand;
        private ICommand deleteTileCommand;
        private bool isPinned = true;
        private ViewState currentView;
        private bool isSearchOpen;
        private ICommand goBackCommand;
        private Dictionary<string, bool> _busyStateValue;
        private List<string> uploadingClaimNumbers;
        private string lastSearchedText=string.Empty;
        private List<int> closedTaskIds;

        #endregion

        #region Properties

        public static MainViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MainViewModel();
                }
                return (MainViewModel)_instance;
            }
        }

        public override ServiceRequestFactory ServiceFactory
        {
            get
            {
                if (_factory == null)
                    _factory = new ServiceRequestConcreteFactory();

                return _factory;
            }
        }

        public static string AppName
        {
            get { return MainViewModel.appName; }
        }


        public override INavigationApp AppInstance { get; set; }

        public Dictionary<string, Uri> Placeholders { get; set; }

        public override IServiceRequest CurrentService { get; set; }

        public bool CanGoBack
        {
            get
            {
                return _canGoBack;
            }
            set
            {
                if (_canGoBack != value)
                {
                    _canGoBack = value;
                    RaisePropertyChanged("CanGoBack");
                }
            }
        }

        public string HeaderText
        {
            get
            {
                return _headerText;
            }
            set
            {
                if (_headerText != value)
                {
                    _headerText = value;
                    RaisePropertyChanged("HeaderText");
                }
            }
        }

        public AppSettings AppSettings
        {
            get
            {
                return Settings as AppSettings;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return isAuthenticated;
            }
            set
            {
                isAuthenticated = value;
                RaisePropertyChanged("IsAuthenticated");
                RaisePropertyChanged("IsAppHeaderSearch");
                RaiseFrameItemProperties();
            }
        }

        public bool IsAppHeaderSearch
        {
            get
            {
                return (IsAuthenticated && (UserDSNFolder!=null));
            }
        }

        public double AvailableHeaderWidth
        {
            get
            {
                return availableheaderWidth;
            }
            set
            {
                availableheaderWidth = value;
                RaisePropertyChanged("AvailableHeaderWidth");
            }
        }


        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
                RaisePropertyChanged("UserId");
            }
        }

        public int MaxResult
        {
            get
            {
                return _maxResult;
            }
        }

        public HomePageViewModel HomePageVM
        {
            get
            {
                if (homePageVM == null)
                    homePageVM = new HomePageViewModel();
                return homePageVM;
            }
        }

        /// <summary>
        /// For using navigation services
        /// </summary>
        public Frame CurrentFrame
        {
            get
            {
                return (Frame)Window.Current.Content;
            }
        }

        public ICSCRepository CscRepository { get; set; }

        public ObservableCollection<Event> ServerEvents
        {
            get
            {
                if (serverEvents == null)
                    serverEvents = new ObservableCollection<Event>();
                return serverEvents;
            }
        }

        public string SyncDateText
        {
            get
            {
                return AppSettings.SyncDate.ToString("hh:mm tt - dddd, MM/dd/yyyy");
            }
        }

        public string LastSyncText
        {
            get
            {
                string lastSyncText = string.Empty;
                if (AppSettings.SyncDate == DateTime.MinValue)
                    lastSyncText = string.Format(GetStringResource("LastSyncText"), GetStringResource("NeverText"), string.Empty);
                else
                {
                    int days = (DateTime.Now - AppSettings.SyncDate).Days;
                    if (days == 1)
                        lastSyncText = string.Format(GetStringResource("LastSyncText"), days, GetStringResource("DayAgoText"));
                    else if (days > 1)
                        lastSyncText = string.Format(GetStringResource("LastSyncText"), days, GetStringResource("DaysAgoText"));
                }
                return lastSyncText;
            }
        }

        public bool ShowSyncDate
        {
            get
            {
                return (IsAuthenticated && (AppSettings.SyncDate != DateTime.MinValue) && !CheckCurrentPage(Constants.MediaCapturPage));
            }
        }

        public bool ShowHeaderControl
        {
            get
            {
                return (IsAuthenticated && !CheckCurrentPage(Constants.MediaCapturPage));
            }
        }

        public string AuthenticationToken { get; set; }

        public User CurrentUserInfo
        {
            get
            {
                return currentUserInfo;
            }
            set
            {
                currentUserInfo = value;
                RaisePropertyChanged("CurrentUserInfo");
            }
        }

        public string WebServerUrl
        {
            get
            {
                string scheme = CurrentUserInfo.IsHttps ? "https" : "http";
                string formattedURL = CurrentUserInfo.WebServerUrl.Trim().Replace("http://", string.Empty).Replace("https://", string.Empty);
                formattedURL = formattedURL.Split('/')[0];
                webServerUrl = string.Format("{0}://{1}", scheme, formattedURL);
                return webServerUrl;
            }
        }

        /// <summary>
        /// Gets or sets user's DSN folder
        /// </summary>
        public StorageFolder UserDSNFolder
        {
            get
            {
                return dsnName;
            }
            set
            {
                dsnName = value;
                RaisePropertyChanged("IsAppHeaderSearch");
            }




        }

        public Authentication AuthenticationVM
        {
            get
            {
                if (authenticationVM == null)
                    authenticationVM = new Authentication();
                return authenticationVM;
            }
        }

        public bool IsAppBusy
        {
            get
            {
                return _busyStates.Count > 0;
            }
        }

        public string LastSearchedText
        {
            get
            {
                return lastSearchedText;
            }
            set
            {
                lastSearchedText = string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the current view
        /// </summary>
        public ViewState CurrentView
        {
            get
            {
                return currentView;
            }
            set
            {
                currentView = value;
                RaisePropertyChanged("CurrentView");
                RaisePropertyChanged("IsFullScreen");
                RaisePropertyChanged("IsMinSnapped");
            }
        }

        /// <summary>
        /// Gets whether the app is full screen
        /// </summary>
        public bool IsFullScreen
        {
            get
            {
                return (this.CurrentView == ViewState.FullScreenLandscape || this.CurrentView == ViewState.FullScreenPortrait) ? true : false;
            }
        }

        public bool IsMinSnapped
        {
            get
            {
                return CurrentView == ViewState.Snapped;
            }
        }

        public bool IsHomePage
        {
            get
            {
                return isHomePage;
            }
            set
            {
                isHomePage = value;
                RaisePropertyChanged("IsHomePage");
            }
        }

        public bool IsSearchOpen
        {
            get
            {
                return isSearchOpen;
            }
            set
            {
                isSearchOpen = value;
                RaisePropertyChanged("IsSearchOpen");
            }
        }

        public bool IsLoadHome
        {
            get
            {
                return isLoadHome;
            }
            set
            {
                isLoadHome = value;
                RaisePropertyChanged("IsLoadHome");
            }
        }

        string searchTerm;
        public string SearchTerm
        {
            get
            {
                return searchTerm;
            }
            set
            {
                searchTerm = value;
                RaisePropertyChanged("SearchTerm");
            }
        }

        public DateTime TaskFilterFromDate
        {
            get
            {
                return DateTime.Now.AddDays(-Constants.TaskListingDateRange);
            }
        }
        public DateTime TaskFilterToDate
        {
            get
            {
                return DateTime.Now.AddDays(Constants.TaskListingDateRange);
            }
        }

        public string TileParams
        {
            get;
            set;
        }

        public DateTime LastServiceDownTime { get; set; }

        public HashSet<int> ClosedTaskList
        {
            get
            {
                if (closedTaskList == null)
                    closedTaskList = new HashSet<int>();
                return closedTaskList;
            }
            set
            {
                closedTaskList = value;
            }
        }

        public List<string> UploadingClaimNumbers
        {
            get
            {
                if (uploadingClaimNumbers == null)
                    uploadingClaimNumbers = new List<string>();
                return uploadingClaimNumbers;
            }
        }

        public List<int> ClosedTaskIds 
        { 
            get 
            {
                if (closedTaskIds == null)
                    closedTaskIds = new List<int>();
                return closedTaskIds;
            } 
        }

        #endregion

        #region Command and Members

        #region Commands

        /// <summary>
        /// Command for adding new task
        /// </summary>
        public ICommand NewTaskCommand
        {
            get
            {
                if (newTaskCommand == null)
                    newTaskCommand = new RelayCommand<object>(OnNewTaskCommand);
                return newTaskCommand;
            }
        }

        /// <summary>
        /// Command for taking photo
        /// </summary>
        public ICommand NavigateToTakePhotoCommand
        {
            get
            {
                if (navigateToTakePhotoCommand == null)
                    navigateToTakePhotoCommand = new RelayCommand<string>(OnNavigateToTakePhotoCommand);
                return navigateToTakePhotoCommand;
            }
        }

        /// <summary>
        /// Command for taking video
        /// </summary>
        public ICommand NavigateToTakeVideoCommand
        {
            get
            {
                if (navigateToTakeVideoCommand == null)
                    navigateToTakeVideoCommand = new RelayCommand<string>(OnNavigateToTakeVideoCommand);
                return navigateToTakeVideoCommand;
            }
        }

        /// <summary>
        /// Command for taking video
        /// </summary>
        public ICommand NavigateToRecordAudioCommand
        {
            get
            {
                if (navigateToRecordAudioCommand == null)
                    navigateToRecordAudioCommand = new RelayCommand<string>(OnNavigateToRecordAudioCommand);
                return navigateToRecordAudioCommand;
            }
        }

        /// <summary>
        /// Command for navigating to home hug
        /// </summary>
        public ICommand NavigateToHomeHubCommand
        {
            get
            {
                if (navigateToHomeHubCommand == null)
                    navigateToHomeHubCommand = new RelayCommand(OnNavigateToHomeHubCommand);
                return navigateToHomeHubCommand;
            }
        }

        /// <summary>
        /// Command for navigating to claim listing page
        /// </summary>
        public ICommand NavigateToClaimsCommand
        {
            get
            {
                if (navigateToClaimsCommand == null)
                    navigateToClaimsCommand = new RelayCommand(OnNavigateToClaimsCommand);
                return navigateToClaimsCommand;
            }
        }

        /// <summary>
        /// Command for navigating to tasks page
        /// </summary>
        public ICommand NavigateToTasksCommand
        {
            get
            {
                if (navigateToTasksCommand == null)
                    navigateToTasksCommand = new RelayCommand<string>(OnNavigateToTasksCommand);
                return navigateToTasksCommand;
            }
        }

        /// <summary>
        /// Command for navigating to events page
        /// </summary>
        public ICommand NavigateToEventsCommand
        {
            get
            {
                if (navigateToEventsCommand == null)
                    navigateToEventsCommand = new RelayCommand(OnNavigateToEventsCommand);
                return navigateToEventsCommand;
            }
        }

        /// <summary>
        /// Command for navigating to Note Adding page
        /// </summary>
        public ICommand NavigateToAddNoteCommand
        {
            get
            {
                if (navigateToAddNoteCommand == null)
				//mbahl3 Mits 36145
                    navigateToAddNoteCommand = new RelayCommand<Claim>(OnNavigateToAddNoteCommand);
               	//mbahl3 Mits 36145 
                return navigateToAddNoteCommand;
            }
        }

        /// <summary>
        /// Command for opening photo viewer
        /// </summary>
        public ICommand OpenPhotoCommand
        {
            get
            {
                if (openPhotoCommand == null)
                    openPhotoCommand = new RelayCommand<string>(OnOpenPhotoCommand);
                return openPhotoCommand;
            }
        }


        public ICommand SelectImageCommand
        {
            get
            {
                if (selectImageCommand == null)
                    selectImageCommand = new RelayCommand<string>(OnSelectImage);
                return openPhotoCommand;
            }
        }

        /// <summary>
        /// Command for opening photo viewer
        /// </summary>
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                    refreshCommand = new RelayCommand(OnRefreshEntireData);
                return refreshCommand;
            }
        }

        public ICommand UnsnapToContinueCommand
        {
            get
            {
                if (unsnapToContinueCommand == null)
                    unsnapToContinueCommand = new RelayCommand(() =>
                    {
                        MessageHelper.Show(GetStringResource("SwitchToFullScreenText"));
                    });
                return unsnapToContinueCommand;
            }
        }

        public ICommand GoBackCommand
        {
            get
            {
                if (goBackCommand == null)
                    goBackCommand = new RelayCommand(() =>
                    {
                        if (MainViewModel.Instance.CurrentFrame.CanGoBack)
                            MainViewModel.Instance.CurrentFrame.GoBack();

                    });
                return goBackCommand;
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                if (searchCommand == null)
                    searchCommand = new RelayCommand<SearchBoxQuerySubmittedEventArgs>(OnSearchCommand);
                return searchCommand;
            }
        }

        #endregion

        #region Members

        /// <summary>
        /// Command logic for navigating to events page
        /// </summary>
        public void OnNavigateToEventsCommand()
        {
            DismissAppBar(false);
            if (CheckCurrentPage(Constants.EventsPage))
                return;
            EventsRequest cscEventsRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.CscEventsRequest) as EventsRequest;
            if (cscEventsRequest == null)
                cscEventsRequest = new EventsRequest(ServiceRequestIds.CscEventsRequest);
            MainViewModel.Instance.Navigate(ServiceRequestIds.CscEventsRequest);
        }

        /// <summary>
        /// Command logic for navigating to page to add note
        /// </summary> 
        public void OnNavigateToAddNoteCommand(Claim claim)  	//mbahl3 Mits 36145
        {
            AddNoteRequest addNoteRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.AddNoteRequest) as AddNoteRequest;
            if (addNoteRequest == null)
                addNoteRequest = new AddNoteRequest(ServiceRequestIds.AddNoteRequest);
              addNoteRequest.ClaimNumber = claim.ClaimNumber; //mbahl3 Mits 36145
              
            var noteVM = addNoteRequest.Result as NoteViewModel;
            noteVM.ClaimViewId = claim.Id; //mbahl3 Mits 36145
            noteVM.IsSaveButtonEnabled = true;
            noteVM.IsCancelButtonEnabled = true;
            MainViewModel.Instance.Navigate(ServiceRequestIds.AddNoteRequest);
        }

        /// <summary>
        /// Command logic for navigating to tasks page
        /// </summary>
        /// <param name="id">Can be claim number or event number</param>
        protected void OnNavigateToTasksCommand(string id = "")
        {
            DismissAppBar(false);
            if (CheckCurrentPage(Constants.TasksPage))
            {
                string serviceRequestId = string.Format(ServiceRequestIds.CscTasksRequest, string.Empty);
                if (MainViewModel.Instance.CurrentService.Id == serviceRequestId)
                    return;
            }
            // AA: Add logic to handle navigation to event tasks in phase #2
            string serviceId = string.Format(ServiceRequestIds.CscTasksRequest, id);

            TasksRequest cscTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
            if (cscTasksRequest == null)
                cscTasksRequest = new TasksRequest(serviceId);
            cscTasksRequest.ParentType = ParentType.Task;
            MainViewModel.Instance.Navigate(serviceId);
        }

        /// <summary>
        /// Command logic for navigating to claim listing page
        /// </summary>
        private void OnNavigateToClaimsCommand()
        {
            DismissAppBar(false);
            if (CheckCurrentPage(Constants.ClaimsPage))
            {
                string serviceRequestId = string.Format(ServiceRequestIds.CscClaimsRequest, string.Empty);
                if (MainViewModel.Instance.CurrentService.Id == serviceRequestId)
                    return;
            }
            string serviceId = string.Format(ServiceRequestIds.CscClaimsRequest, string.Empty);
            ClaimsRequest cscClaimsRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimsRequest;
            if (cscClaimsRequest == null)
                cscClaimsRequest = new ClaimsRequest(serviceId);
            MainViewModel.Instance.Navigate(serviceId);
        }

        /// <summary>
        /// Command logic for navigating to home hub
        /// </summary>
        private void OnNavigateToHomeHubCommand()
        {
            DismissAppBar(false);
            while (CurrentFrame.CanGoBack)
            {
                CurrentFrame.GoBack();
            }
            if (CheckCurrentPage("HomePage"))
                return;
            else
            {
                HomePageRequest request = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.HomeRequest) as HomePageRequest;
                if (request == null)
                    request = new HomePageRequest(ServiceRequestIds.HomeRequest);
                MainViewModel.Instance.Navigate(ServiceRequestIds.HomeRequest);
            }

        }

        /// <summary>
        /// Check's if current page is 'pageName'
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
        public bool CheckCurrentPage(string pageName)
        {
            return CurrentFrame.CurrentSourcePageType != null && CurrentFrame.CurrentSourcePageType.Name == pageName;
        }

        /// <summary>
        /// Command logic for adding new task
        /// </summary>
        /// <param name="id"></param>
        private async void OnNewTaskCommand(object obj)
        {
            DismissAppBar(false);
            var type = string.Empty;
            AddTaskRequest addTaskRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.AddTaskRequest) as AddTaskRequest;
            if (addTaskRequest == null)
                addTaskRequest = new AddTaskRequest(ServiceRequestIds.AddTaskRequest);
            TaskViewModel cscTaskVM = new TaskViewModel() { ClassID = Constants.AddTaskClassID, CscTask = new ClaimAdjusterTask() };
            if (obj != null)
            {
                type = Convert.ToString(obj).Contains(Constants.EventsPage) ? Constants.EventsPage : Convert.ToString(obj.GetType().Name);
                if (type.Equals("Event") || type.Equals(Constants.EventsPage))
                {
                    cscTaskVM.IsEvent = true;
                    cscTaskVM.CscTask.TaskParentType = ParentType.Event;
                }
                else
                    cscTaskVM.CscTask.TaskParentType = ParentType.Claim;
            }
            await cscTaskVM.InitializeNewTask();
            if (!string.IsNullOrWhiteSpace(type))
            {
                if (type.Equals("Claim"))
                {
                    Claim claim = obj as Claim;
                    if (claim != null)
                        cscTaskVM.CscTask.ClaimNumber = claim.ClaimNumber;
                }
                else if (type.Equals("Event"))
                {
                    Event eventItem = obj as Event;
                    if (eventItem != null)
                        cscTaskVM.CscTask.EventNumber = eventItem.EventNumber;
                }
            }

            addTaskRequest.Result = cscTaskVM;
            MainViewModel.Instance.Navigate(ServiceRequestIds.AddTaskRequest);
        }

        /// <summary>
        /// Command logic for taking photo
        /// </summary>
        /// <param name="idWithType">Can be claim, task or event identifier</param>
        private async void OnNavigateToTakePhotoCommand(string idWithType)
        {
            CameraCaptureUI cameraUI = new CameraCaptureUI();
            StorageFile photo = await cameraUI.CaptureFileAsync(CameraCaptureUIMode.Photo);

            if (photo != null)
            {
                string filename = GeneratePictureFilename();

                var mediaContext = new MediaCaptureViewModel();
                mediaContext.CaptureType = MediaCaptureType.Photo;
                mediaContext.PreviewImageName = filename;
                mediaContext.IdWithType = idWithType;
                // AA: Revisit - Refractor this code.
                StorageFolder parentFolder = await MainViewModel.Instance.UserDSNFolder.CreateFolderAsync(mediaContext.RelatedParentId, Windows.Storage.CreationCollisionOption.OpenIfExists);
                StorageFolder photoFolder = await parentFolder.CreateFolderAsync(Constants.PhotoFolder, Windows.Storage.CreationCollisionOption.OpenIfExists);
                await photo.MoveAsync(photoFolder, filename);

                mediaContext.SaveCapturedItem();
            }
        }

        /// <summary>
        /// Command logic for taking video
        /// </summary>
        /// <param name="idWithType">Can be claim, task or event identifier</param>
        private async void OnNavigateToTakeVideoCommand(string idWithType)
        {
            CameraCaptureUI cameraUI = new CameraCaptureUI();
            cameraUI.VideoSettings.Format = CameraCaptureUIVideoFormat.Wmv;

            cameraUI.VideoSettings.MaxResolution = CameraCaptureUIMaxVideoResolution.StandardDefinition;
            StorageFile video = await cameraUI.CaptureFileAsync(CameraCaptureUIMode.Video);

            if (video != null)
            {
                string filename = GenerateVideoFilename();

                var mediaContext = new MediaCaptureViewModel();
                mediaContext.CaptureType = MediaCaptureType.Video;
                mediaContext.PreviewImageName = filename;
                mediaContext.IdWithType = idWithType;

                // AA: Revisit - Refractor this code.
                StorageFolder parentFolder = await MainViewModel.Instance.UserDSNFolder.CreateFolderAsync(mediaContext.RelatedParentId, Windows.Storage.CreationCollisionOption.OpenIfExists);
                StorageFolder videoFolder = await parentFolder.CreateFolderAsync(Constants.VideoFolder, Windows.Storage.CreationCollisionOption.OpenIfExists);
                await video.MoveAsync(videoFolder, filename);

                mediaContext.SaveCapturedItem();

            }
        }

        /// <summary>
        /// Command logic for taking video
        /// </summary>
        /// <param name="idWithType">Can be claim, task or event identifier</param>
        private void OnNavigateToRecordAudioCommand(string idWithType)
        {
            //string fileName = GenerateAudioFilename();

            DismissAppBar(false);
            if (CurrentFrame.CurrentSourcePageType != null && CurrentFrame.CurrentSourcePageType.Name == "MediaCapturPage")
                return;
            //var data = idWithType.Split(',');
            MediaCaptureRequest mediaCaptureRequest = MainViewModel.Instance.ServiceFactory.GetService(ServiceRequestIds.MediaCaptureRequest) as MediaCaptureRequest;
            if (mediaCaptureRequest == null)
                mediaCaptureRequest = new MediaCaptureRequest(ServiceRequestIds.MediaCaptureRequest);
            MediaCaptureViewModel mediaContext = new MediaCaptureViewModel();
            mediaContext.CaptureType = MediaCaptureType.Audio;
            mediaContext.IdWithType = idWithType;
            mediaCaptureRequest.Result = mediaContext;
            MainViewModel.Instance.Navigate(ServiceRequestIds.MediaCaptureRequest);
        }

        /// <summary>
        /// Command logic for opening an image
        /// </summary>
        /// <param name="imageName"></param>
        private void OnOpenPhotoCommand(string imageName)
        {
            LaunchHelper.LaunchPhoto(imageName);

        }

        private void OnSelectImage(string imageName)
        {

        }

        private void OnSearchCommand(SearchBoxQuerySubmittedEventArgs search)
        {
            IsSearchOpen = false;
            string searchTerm = search.QueryText.Trim();
            if(!this.CurrentFrame.CurrentSourcePageType.Name.Equals("SearchResultsPage"))
                MainViewModel.Instance.LastSearchedText = string.Empty;
            if (lastSearchedText.Trim().Equals(searchTerm.Trim()))
                return;
            else
                lastSearchedText = searchTerm;

            if (searchTerm.Length >= 3) //min. number of characters required is 3
            {
                string requestId = string.Format(ServiceRequestIds.SearchRequest, searchTerm);
                SearchRequest searchRequest = MainViewModel.Instance.ServiceFactory.GetService(requestId) as SearchRequest;
                if (searchRequest == null)
                    searchRequest = new SearchRequest(requestId);
                (searchRequest.Result as SearchViewModel).SearchTerm = searchTerm;
                MainViewModel.Instance.Navigate(requestId);
            }
        }

        #endregion

        #endregion

        #region Functions

        public override string GetStringResource(string key)
        {
            return AppInstance.GetResourceString(key);
        }

        public async override void DeActivate(bool isTombStoning = false)
        {
            base.DeActivate(isTombStoning);
            await SaveToStorageFile<AppSettings>(AppSettings);
        }

        public async override Task Activate(bool isTombStoning = false, bool isAppInstancePreserved = false)
        {
            await base.Activate(isTombStoning, isAppInstancePreserved);
            if (!isAppInstancePreserved)
            {
                //TODO: Load defaults in AppSettings
                //MainViewModel.Instance.AppSettings.LoadDefaults();

                //TODO: Add Placeholder images
                Placeholders = new Dictionary<string, Uri>();
                Placeholders.Add("PreviewVideoLarge", new Uri("/Images/VideoPreview_L.png", UriKind.RelativeOrAbsolute));

#if !NETFX_CORE
                //Upgrade scenario 
                if (AppSettings.IsoAppVersion != AppSettings.PackageVersion)
                {

                }
#endif
                await InitializeDatabase();
                LoadInitServices();

            }
        }

        public async Task<T> FromStorageFile<T>(string file) where T : ResultItem, new()
        {
            T dm = default(T);

            try
            {
                var storageFile = await IsolatedStorage.TryGetItemAsync(file);
                if (storageFile == null)
                {
                    dm = new T();
                    return dm;
                }
                using (var stream = await IsolatedStorage.OpenStreamForReadAsync(file))
                {
                    if (stream != null)
                    {
                        try
                        {
                            DataContractSerializer dcs = new DataContractSerializer(typeof(T));
                            dm = dcs.ReadObject(stream) as T;
                        }
                        catch
                        {
                            dm = new T();
                        }
                    }
                    else
                        dm = new T();
                }
            }
            catch
            {
                dm = new T();
            }

            return dm;
        }

        public async Task SaveToStorageFile<T>(T result) where T : ResultItem
        {
            var file = IsolatedStorage.CreateFileAsync(result.Id, CreationCollisionOption.ReplaceExisting);
            file.AsTask().Wait();
            var streamTask = file.AsTask().Result.OpenStreamForWriteAsync();
            streamTask.Wait();
            using (Stream stream = streamTask.Result)
            {
                DataContractSerializer dcs = new DataContractSerializer(typeof(T));
                dcs.WriteObject(stream, result);
            }
        }

        private async Task InitializeDatabase()
        {
            string datbasePath = Windows.Storage.ApplicationData.Current.LocalFolder.Path + "\\OffLine.db";
            Database database = new Database(datbasePath);
            await database.Initialize();
            CscRepository = new CSCRepository(database);
        }

        private void LoadInitServices()
        {
            CheckInternetStatus();
            if (!IsInternetAvailable)
                AuthenticationVM.ReLogin();
        }

        private async void CheckInternetStatus()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            IsInternetAvailable = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
        }

        void NetworkInformation_NetworkStatusChanged(object sender)
        {
            CheckInternetStatus();
        }

        async void MainViewModel_InternetAvailabilityChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AuthenticationToken))
                await AuthenticationVM.ReLogin();
            else
            {
                UploadUnsyncItem(false);
            }
        }

        /// <summary>
        /// Uploads items cliams (photo's,video..),offline (new tasks, new note),close task
        /// </summary>
        public async Task UploadUnsyncItem(bool needsAwait)
        {
            if (IsInternetAvailable)
            {
                SyncFailedCloseTasks();
                if (!needsAwait)
                {
                    HomePageVM.UploadNewTasks(false);
                    HomePageVM.UploadNewNotes();
                    HomePageVM.UploadClaimItems();
                }
                else
                {
                    await HomePageVM.UploadNewTasks(true);
                    await HomePageVM.UploadNewNotes();
                    await HomePageVM.UploadClaimItems();
                }
            }
        }

        private async void SyncFailedCloseTasks()
        {
            List<ClaimAdjusterTask> tasks = await MainViewModel.Instance.CscRepository.GetTaskByStatusAsync((int)DataStatus.ClosedOffline);
            MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                foreach (var item in tasks)
                {
                    string serviceId = string.Format(ServiceRequestIds.TaskDetailsRequest, item.ServerEntryId);
                    TaskDetailsRequest taskDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TaskDetailsRequest;
                    if (taskDetailsRequest == null)
                        taskDetailsRequest = new TaskDetailsRequest(serviceId, item.ServerEntryId);
                    TaskViewModel taskVM = new TaskViewModel();
                    taskDetailsRequest.Result = taskVM;
                    taskVM.CscTask = item;
                    await taskDetailsRequest.ExecuteAsync();
                    taskVM.CloseTaskCommand.Execute("true");
                }
            });
        }

        public string GetServiceAddress(string url)
        {
            return string.Format(url, WebServerUrl.Replace(" ", ""));
        }



        public async Task GetClaimDetails(Claim claim, bool isAsyncCall = true)
        {
            try
            {
                string requestXml = string.Format(MobileCommonService.GetClaimsDetail, MainViewModel.Instance.AuthenticationToken, claim.ClaimNumber);
                string claimsResult = await MobileCommonService.ProcessRequestAsync(requestXml);
                if (isAsyncCall)
                    ParsingHelper.ParseClaimDetail(claimsResult, claim);
                else
                    await ParsingHelper.ParseClaimDetail(claimsResult, claim);
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<bool> UploadNote(Notes note)
        {
            bool success = false;
            try
            {
                string requestXml = string.Format(MobileProgressNoteService.CreateNoteRequest, MainViewModel.Instance.AuthenticationToken, note.ParentId, DateTime.UtcNow.ToString("MM/dd/yyyy"), note.Content, note.NoteTypeCodeId);
                string noteCreateResponse = await MobileProgressNoteService.SaveNotesForMobileAdjAsync(requestXml);
                success = ParsingHelper.ParseNoteCreatResponse(noteCreateResponse);
            }
            catch (Exception ex)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Renerates a picture file name with current timestamp
        /// </summary>
        /// <returns></returns>
        public string GeneratePictureFilename()
        {
            return string.Format("{0}.jpg", DateTime.Now.ToString("yyyyMMddhhmmss"));
        }

        public string GenerateVideoFilename()
        {
            return string.Format("{0}.wmv", DateTime.Now.ToString("yyyyMMddhhmmss"));
        }

        public string GenerateAudioFilename()
        {
            return string.Format("{0}.mp3", DateTime.Now.ToString("yyyyMMddhhmmss"));
        }

        public async void OnRefreshEntireData()
        {
            this.HomePageVM.Refresh();
            if (!(MainViewModel.Instance.CurrentFrame.Content is HomePageViewModel))
            {
                OnNavigateToHomeHubCommand();
            }
        }

        public void SetAppBusy(string stateName, bool isBusy)
        {
            if (isBusy)
            {
                if (_busyStates.Contains(stateName) == false)
                {
                    _busyStates.Add(stateName);

                    RaisePropertyChanged("IsAppBusy");
                }
            }
            else
            {
                if (_busyStates.Contains(stateName))
                {
                    _busyStates.Remove(stateName);
                }

                RaisePropertyChanged("IsAppBusy");
            }
        }
        public async void RemoveAppBusyState(string busyStateKey)
        {
            if (this._busyStateValue != null && !string.IsNullOrEmpty(busyStateKey))
            {
                if (this._busyStateValue.ContainsKey(busyStateKey))
                {
                    this._busyStateValue.Remove(busyStateKey);
                }
            }
            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => this.RaisePropertyChanged("IsAppBusy"));
        }

        public IReadOnlyList<SecondaryTile> SecondaryTiles
        {
            get
            {
                return pinnedTiles;
            }
            set
            {
                pinnedTiles = value;
                RaisePropertyChanged("SecondaryTiles");
            }
        }
        public bool CanPin
        {
            get
            {
                return isPinned;
            }
            set
            {
                if (isPinned != value)
                {
                    isPinned = value;
                    RaisePropertyChanged("CanPin");
                }
            }
        }

        public ICommand AddTileCommand
        {
            get
            {
                if (pinTileCommand == null)
                    pinTileCommand = new RelayCommand<object>(OnNewTileCommand);
                return pinTileCommand;
            }
        }
        public ICommand RemoveTileCommand
        {
            get
            {
                if (deleteTileCommand == null)
                    deleteTileCommand = new RelayCommand<object>(OnRemoveTileCommand);
                return deleteTileCommand;
            }
        }

        private async void OnNewTileCommand(object obj)
        {
            string tileID = string.Empty;
            string displayName = string.Empty, arguments = string.Empty;

            if (obj.GetType() == typeof(Claim))
            {
                Claim claim = obj as Claim;
                tileID = claim.ClaimNumber;
                displayName = string.Format("Claim #{0}", claim.ClaimNumber);
                arguments = string.Format("{0}={2}&{1}={3}", Constants.Type, Constants.ID, PinType.CLAIM.ToString(), claim.ClaimNumber);
            }
            else if (obj.GetType() == typeof(ClaimAdjusterTask))
            {
                ClaimAdjusterTask task = obj as ClaimAdjusterTask;
                tileID = task.ServerEntryId.ToString();
                displayName = string.IsNullOrWhiteSpace(task.TaskName) ? MainViewModel.Instance.GetStringResource("NewTileText") : task.TaskName;
                arguments = string.Format("{0}={2}&{1}={3}", Constants.Type, Constants.ID, PinType.TASK.ToString(), task.ServerEntryId);
            }
            else if (obj.GetType() == typeof(Event))
            {
                Event events = obj as Event;
                tileID = events.EventNumber.ToString();
                displayName = string.Format("Event #{0} ", events.EventNumber);
                arguments = string.Format("{0}={2}&{1}={3}", Constants.Type, Constants.ID, PinType.EVENT.ToString(), events.EventNumber);
            }

            if (string.IsNullOrEmpty(tileID))
                return;
            else
            {

                if (!SecondaryTile.Exists(tileID))
                {
                    Uri square150x150Logo = new Uri("ms-appx:///Assets/310x310.scale-100.png");
                    SecondaryTile secondaryTile = new SecondaryTile(tileID, displayName, arguments, square150x150Logo, TileSize.Square150x150);

                    secondaryTile.VisualElements.ShowNameOnSquare150x150Logo = true;
                    secondaryTile.VisualElements.ForegroundText = ForegroundText.Dark;
                    bool success = await secondaryTile.RequestCreateAsync(new Windows.Foundation.Point(Window.Current.Bounds.Width, Window.Current.Bounds.Height));
                    if (success)
                        CanPin = false;
                }

            }
        }


        private async void OnRemoveTileCommand(object obj)
        {
            string tileID = Convert.ToString(obj);

            if (string.IsNullOrEmpty(tileID))
                return;
            else
            {
                SecondaryTile secondaryTile = new SecondaryTile(tileID);
                if (secondaryTile != null)
                {
                    bool success = await secondaryTile.RequestDeleteAsync(new Windows.Foundation.Point(Window.Current.Bounds.Width, Window.Current.Bounds.Height));
                    if (success)
                        CanPin = true;
                }
            }

        }
        private async void GetSecondaryTilesList()
        {
            SecondaryTiles = await Windows.UI.StartScreen.SecondaryTile.FindAllAsync();
        }

        public void TileExists(string appbarTileID)
        {

            if (SecondaryTiles != null)
            {
                CanPin = !SecondaryTile.Exists(appbarTileID);
                return;
            }
            else
            {
                GetSecondaryTilesList();
            }


        }

        public void AddNewlyCreatedTask(ClaimAdjusterTask newTask)
        {

            this.HomePageVM.Tasks.Collection.Insert(0, new TaskViewModel()
                {
                    CscTask = newTask
                });

            string serviceId = string.Format(ServiceRequestIds.CscTasksRequest, string.Empty);
            TasksRequest cscTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
            if (cscTasksRequest != null)
            {
                var taskListingPage = cscTasksRequest.Result as TaskCollection;
                if (taskListingPage.AllTasks != null)
                {
                    taskListingPage.AllTasks.Add(newTask);
                    taskListingPage.RefreshData();
                }
            }


            if (newTask.TaskParentType == ParentType.Claim)
            {
                serviceId = string.Format(ServiceRequestIds.CscTasksRequest, newTask.ClaimNumber);
                TasksRequest cscClaimTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
                if (cscClaimTasksRequest != null)
                {
                    var taskListingPage = cscClaimTasksRequest.Result as TaskCollection;
                    if (taskListingPage.AllTasks != null)
                    {
                        taskListingPage.AllTasks.Add(newTask);
                        taskListingPage.RefreshData();
                    }
                }

                serviceId = string.Format(ServiceRequestIds.ClaimDetailsRequest, newTask.ClaimNumber, newTask.ParentViewId); //mbahl3 Mits 36145

                ClaimDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimDetailsRequest;
                if (request != null)
                {
                    var claimDetailVM = request.Result as ClaimViewModel;
                    if (claimDetailVM.CscClaim != null && claimDetailVM.CscClaim.ClaimNumber == newTask.ClaimNumber)
                    {
                        claimDetailVM.Tasks.Collection.Insert(0, new TaskViewModel()
                        {
                            CscTask = newTask
                        });
                        claimDetailVM.RaisePropertyChanged("HasTasks");
                        claimDetailVM.Tasks.RaisePropertyChanged("Count");
                    }
                }
            }
            else if (newTask.TaskParentType == ParentType.Event)
            {
                serviceId = string.Format(ServiceRequestIds.CscTasksRequest, newTask.EventNumber);
                TasksRequest eventTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
                if (eventTasksRequest != null)
                {
                    var taskCollection = eventTasksRequest.Result as TaskCollection;
                    if (taskCollection.AllTasks != null)
                    {
                        taskCollection.AllTasks.Add(newTask);
                        taskCollection.RefreshData();
                    }
                }

                serviceId = string.Format(ServiceRequestIds.EventDetailsRequest, newTask.EventNumber);
                EventDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as EventDetailsRequest;
                if (request != null)
                {
                    var eventVM = request.Result as EventViewModel;
                    if (eventVM.CscEvent != null && eventVM.CscEvent.EventNumber == newTask.EventNumber)
                    {
                        eventVM.Tasks.Collection.Insert(0, new TaskViewModel()
                        {
                            CscTask = newTask
                        });
                        eventVM.RaisePropertyChanged("HasTasks");
                        eventVM.Tasks.RaisePropertyChanged("Count");
                    }
                }
            }
        }

        public void DismissAppBar(bool hide)
        {
            Page currentPage = MainViewModel.Instance.CurrentFrame.Content as Page;
            if (currentPage == null) return;
            if (currentPage.BottomAppBar != null)
                currentPage.BottomAppBar.IsOpen = hide;
            if (currentPage.TopAppBar != null)
                currentPage.TopAppBar.IsOpen = hide;
        }

        public void AddNewlyCreatedNote(Notes newNote)
        {

            string serviceId = string.Format(ServiceRequestIds.ClaimDetailsRequest, newNote.ParentId, 0); //mbahl3 Mits 36145 view id sent 0 to avoid multiple creation of notes for different views(in case of different claimant s)

            ClaimDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimDetailsRequest;
            if (request != null)
            {
                var claimDetailVM = request.Result as ClaimViewModel;
                if (claimDetailVM.CscClaim != null && claimDetailVM.CscClaim.ClaimNumber == newNote.ParentId)
                {
                    claimDetailVM.Notes.Collection.Insert(0, new NoteViewModel() { CurrentNote = newNote });
                    claimDetailVM.RaisePropertyChanged("HasNotes");
                    claimDetailVM.Notes.RaisePropertyChanged("Count");
                }
            }

            serviceId = string.Format(ServiceRequestIds.CscNotesRequest, newNote.ParentId);
            NotesRequest cscNotesRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as NotesRequest;
            if (cscNotesRequest != null && cscNotesRequest.Result != null && cscNotesRequest.Result is NoteCollection)
            {
                var noteCollection = cscNotesRequest.Result as NoteCollection;
                if (noteCollection != null && noteCollection.Collection != null)
                {
                    noteCollection.Collection.Insert(0, new NoteViewModel()
                    {
                        CurrentNote = newNote
                    });
                    noteCollection.RefreshData();
                }
            }


        }

        public void RemoveAttachment(Attachment attachment)
        {

            ObservableCollection<MediaViewModel> attachmentList = null;
            string serviceId = string.Empty;
            ClaimsAdjuster baseViewModel = null;

            if (attachment.ParentType == ParentType.Claim)
            {
                serviceId = string.Format(ServiceRequestIds.ClaimDetailsRequest, attachment.ParentId,0);//mbahl3 Mits 36145 view id sent 0 
                ClaimDetailsRequest request = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimDetailsRequest;
                if (request != null)
                {
                    var claimDetailVM = request.Result as ClaimViewModel;
                    baseViewModel = claimDetailVM;
                    if (claimDetailVM.CscClaim != null && claimDetailVM.CscClaim.ClaimNumber == attachment.ParentId)
                    {
                        if (attachment.AttachmentType == MediaCaptureType.Video)
                            attachmentList = claimDetailVM.Videos;
                        else if (attachment.AttachmentType == MediaCaptureType.Photo)
                            attachmentList = claimDetailVM.Pictures;
                        else if (attachment.AttachmentType == MediaCaptureType.Audio)
                            attachmentList = claimDetailVM.Audios;

                    }
                }
            }
            else
            {
                serviceId = string.Format(ServiceRequestIds.TaskDetailsRequest, attachment.ParentId);
                TaskDetailsRequest cscClaimTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TaskDetailsRequest;
                if (cscClaimTasksRequest != null)
                {
                    var taskDetailVM = cscClaimTasksRequest.Result as TaskViewModel;
                    baseViewModel = taskDetailVM;
                    if (taskDetailVM.CscTask != null && taskDetailVM.CscTask.ServerEntryId.ToString() == attachment.ParentId)
                    {
                        if (attachment.AttachmentType == MediaCaptureType.Video)
                            attachmentList = taskDetailVM.Videos;
                        else if (attachment.AttachmentType == MediaCaptureType.Photo)
                            attachmentList = taskDetailVM.Pictures;
                        else if (attachment.AttachmentType == MediaCaptureType.Audio)
                            attachmentList = taskDetailVM.Audios;

                    }
                }
            }
            if (attachmentList != null && attachmentList.Count > 0)
            {
                var elementToBeRemoved = attachmentList.FirstOrDefault(mediaElement => mediaElement.MediaElement != null && mediaElement.MediaElement.FileName == attachment.FileName
                    && mediaElement.MediaElement.FileLocation == attachment.FileLocation && mediaElement.MediaElement.ParentId == attachment.ParentId);
                if (elementToBeRemoved != null)
                    attachmentList.Remove(elementToBeRemoved);
                baseViewModel.RefreshMediaCollectionBindings();
            }
        }

        public async void NavigateToTileParam()
        {
            if (!string.IsNullOrEmpty(MainViewModel.Instance.TileParams))
            {

                Dictionary<string, string> tile = StringHelpers.ParseTileArgument();
                string type = tile[Constants.Type];
                string typeID = tile[Constants.ID].ToLowerInvariant();
                if (StringHelpers.GetPinType(type) == PinType.CLAIM)
                {

                    List<Claim> allClaims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();

                    var claim = allClaims.Where(s => s.ClaimNumber.ToLowerInvariant() == typeID).SingleOrDefault();
                    if (claim == null)
                    {
                        MainViewModel.Instance.IsLoadHome = true;
                        RaisePropertyChanged("IsLoadHome");
                        MessageHelper.Show(MainViewModel.Instance.GetStringResource("UnavailableMessageText"));
                        MainViewModel.Instance.TileParams = string.Empty;
                        return;

                    }
                    ClaimViewModel claimVM = new ClaimViewModel();
                    claimVM.CscClaim = claim;
                    var claimDetailsRequest = claimVM.CreateServiceRequest();

                    MainViewModel.Instance.Navigate(claimDetailsRequest.Id);
                }
                else if (StringHelpers.GetPinType(type) == PinType.TASK)
                {

                    List<ClaimAdjusterTask> allTasks = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
                    if (CheckCurrentPage("TaskDetailsPage") && MainViewModel.Instance.CurrentService.Id == string.Format(ServiceRequestIds.TaskDetailsRequest, typeID))
                    {
                        MainViewModel.Instance.TileParams = string.Empty;
                        return;
                    }
                    var task = allTasks.FirstOrDefault(s => s.ServerEntryId.ToString().ToLowerInvariant() == typeID);
                    if (task == null)
                    {
                        MainViewModel.Instance.IsLoadHome = true;
                        RaisePropertyChanged("IsLoadHome");
                        MessageHelper.Show(MainViewModel.Instance.GetStringResource("UnavailableMessageText"));
                        MainViewModel.Instance.TileParams = string.Empty;
                        return;
                    }
                    else
                    {
                        if (task.Status == (int)DataStatus.ClosedOffline)
                        {
                            MainViewModel.Instance.IsLoadHome = true;
                            RaisePropertyChanged("IsLoadHome");
                            MessageHelper.Show(MainViewModel.Instance.GetStringResource("UnavailableMessageText"));
                            MainViewModel.Instance.TileParams = string.Empty;
                            return;
                        }
                    }
                    TaskViewModel taskVM = new TaskViewModel();
                    taskVM.CscTask = task;
                    var taskDetailsRequest = taskVM.CreateServiceRequest();

                    MainViewModel.Instance.Navigate(taskDetailsRequest.Id);
                }
                else
                {

                    List<Event> allEvents = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();

                    var events = allEvents.Where(s => s.EventNumber.ToString().ToLowerInvariant() == typeID).SingleOrDefault();
                    if (events == null)
                    {
                        MainViewModel.Instance.IsLoadHome = true;
                        RaisePropertyChanged("IsLoadHome");
                        MessageHelper.Show(MainViewModel.Instance.GetStringResource("UnavailableMessageText"));
                        MainViewModel.Instance.TileParams = string.Empty;
                        return;
                    }
                    EventViewModel taskVM = new EventViewModel();
                    taskVM.CscEvent = events;
                    var eventDetailsRequest = taskVM.CreateServiceRequest();

                    MainViewModel.Instance.Navigate(eventDetailsRequest.Id);



                }

                MainViewModel.Instance.TileParams = string.Empty;
            }
            else
            {
                MainViewModel.Instance.IsLoadHome = true;
                RaisePropertyChanged("IsLoadHome");
            }

        }

        public void ShowServiceUnavailableMessage()
        {
            if (LastServiceDownTime < DateTime.Now && (DateTime.Now - LastServiceDownTime).Minutes > 1)
            {
                MessageHelper.Show(GetStringResource("ServiceUnavailableMessage"));
                LastServiceDownTime = DateTime.Now;
            }
        }

        public List<ClaimAdjusterTask> ReArrangeTasks(List<ClaimAdjusterTask> taskItems)
        {
            taskItems = taskItems.OrderByDescending(task => task.TaskDateTime).ToList();
            var tasksToDisplay = taskItems.Where(task => task.TaskDateTime.Date == DateTime.Now.Date).OrderByDescending(task => task.TaskDateTime).ToList();
            if (tasksToDisplay != null && tasksToDisplay.Count > 0)
            {
                var othertasks = taskItems.Where(otherTask => !tasksToDisplay.Contains(otherTask)).ToList();
                if (othertasks != null && othertasks.Count > 0)
                    tasksToDisplay.AddRange(othertasks);
            }
            else
            {
                tasksToDisplay = taskItems;
            }
            return tasksToDisplay;
        }

        /// <summary>
        /// Generates toast template that will be used by the background task
        /// </summary>
        public async void GenerateToastData()
        {
            User user = await MainViewModel.Instance.CscRepository.GetUserDetails(MainViewModel.Instance.AppSettings.LastLoggedUserId);
            if (user != null)
            {
                ClearToastData();   // Clear Toast Data

                try
                {
                    List<ClaimAdjusterTask> tasks = await MainViewModel.Instance.CscRepository.GetOpenTaskAsync();
                    var openTasks = tasks.Where(p => p.TaskDateTime >= DateTime.Now).OrderBy(t => t.TaskDateTime).ToList();  // Get total due tasks
                    var filteredTask = openTasks.Where(p => p.TaskDateTime <= DateTime.Now.AddHours(24)).OrderBy(p => p.TaskDateTime).ToList();  // Get due task for next 1 Hours
                    //Changing to get for 24 hours and then filtering in the background task.

                    int filteredTaskCount = filteredTask.Count;

                    if (filteredTaskCount > 0)
                    {
                        ToastData tdata = new ToastData();
                        tdata.TotalDueTasks = openTasks.Count;
                        tdata.DueTasks = new DueTask[filteredTaskCount];

                        int i = 0;
                        foreach (var item in filteredTask)
                        {
                            DueTask dt = new DueTask();
                            dt.DueDate = item.TaskDateTime.ToString();
                            dt.TaskName = item.TaskName;
                            dt.TaskTypeText = item.TaskTypeDisplayText;
                            tdata.DueTasks[i] = dt;
                            i++;
                        }

                        var xmlToastData = StringHelpers.SerializeCustom<ToastData>(tdata);

                        StorageFile templatefile = await ApplicationData.Current.LocalFolder.CreateFileAsync(Constants.ToastDataFilename, CreationCollisionOption.OpenIfExists);
                        FileIO.WriteTextAsync(templatefile, xmlToastData);
                    }
                }

                catch (Exception exp)
                {

                }
            }
        }

        /// <summary>
        /// Deletes toast template that will be used by the background task
        /// </summary>
        public async void ClearToastData()
        {
            try
            {
                StorageFile templatefile = await ApplicationData.Current.LocalFolder.GetFileAsync(Constants.ToastDataFilename);
                await FileIO.WriteTextAsync(templatefile, string.Empty);
            }
            catch (Exception)
            {

            }
        }

        public bool SamePageExists(string currentRequestId)
        {
            return CurrentFrame.BackStack.ToList().Any(p => (p.Parameter as string) == currentRequestId);
        }

        /// <summary>
        /// Raises the syncDate and header control visibility properties
        /// </summary>
        public void RaiseFrameItemProperties()
        {
            RaisePropertyChanged("ShowSyncDate");
            RaisePropertyChanged("ShowHeaderControl");
        }

        #endregion

    }
}



