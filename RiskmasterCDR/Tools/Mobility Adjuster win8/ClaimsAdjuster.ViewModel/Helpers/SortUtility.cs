﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.ObjectModel;
using ClaimsAdjuster.ViewModel.Repository;
namespace ClaimsAdjuster.ViewModel.Helpers
{
    public class CustomFilter
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public WhereOperationType WhereType  { get; set; }
        public bool NullCheck { get; set; }

        public CustomFilter(string key, string value, WhereOperationType type,bool nullCheck)
        {
            Key = key;
            Value = value;
            WhereType = type;
            NullCheck = nullCheck;
        }
    }
    public enum WhereOperationType
    {
        Contains,
        Equals,
        NotEqual,
        DoesNotContain
    }
    public class SortUtility<T>
    {
        /// <Summary>
        /// Gets the sorted list.
        /// </Summary>
        /// <param name="source" />The source.
        /// <param name="sortColumn" />The sort column.
        /// <param name="sortDirection" />The sort direction.
        /// <The sorted list. />
        public List<T> GetSortedList(List<T> source, string sortColumn, bool sortAsc)
        {
            // Prepare the dynamic sort expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp =
                             Expression.Convert(Expression.Property(paramExp, sortColumn), typeof(object));
            var sortExp = Expression.Lambda<Func<T, object>>(propConvExp, paramExp);

            if (sortAsc)
            {
                return source.AsQueryable().OrderBy(sortExp).ToList();
            }
            else
            {
                return source.AsQueryable().OrderByDescending(sortExp).ToList();
            }
        }
     
        public List<T> GetSortedListMultiple(List<T> source, string sortColumn1, string sortColumn2, bool sortAsc)
        {
            // Prepare the dynamic sort expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp1 =
                             Expression.Convert(Expression.Property(paramExp, sortColumn1), typeof(object));
            var sortExp1 = Expression.Lambda<Func<T, object>>(propConvExp1, paramExp);

            Expression propConvExp2 =
                            Expression.Convert(Expression.Property(paramExp, sortColumn2), typeof(object));
            var sortExp2 = Expression.Lambda<Func<T, object>>(propConvExp2, paramExp);

            if (sortAsc)
            {
                return source.AsQueryable().OrderBy(sortExp1).ThenBy(sortExp2).ToList();
            }
            else
            {
                return source.AsQueryable().OrderByDescending(sortExp1).ThenByDescending(sortExp2).ToList();
            }
        }

        public List<T> GetSortedListMultiple(List<T> source, string sortColumn1, string sortColumn2, bool sortAsc1, bool sortAsc2)
        {
            // Prepare the dynamic sort expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp1 =
                             Expression.Convert(Expression.Property(paramExp, sortColumn1), typeof(object));
            var sortExp1 = Expression.Lambda<Func<T, object>>(propConvExp1, paramExp);

            Expression propConvExp2 =
                            Expression.Convert(Expression.Property(paramExp, sortColumn2), typeof(object));
            var sortExp2 = Expression.Lambda<Func<T, object>>(propConvExp2, paramExp);

            if (sortAsc1)
            {
                if (sortAsc2)
                    return source.AsQueryable().OrderBy(sortExp1).ThenBy(sortExp2).ToList();
                else
                    return source.AsQueryable().OrderBy(sortExp1).ThenByDescending(sortExp2).ToList();
            }
            else
            {
                if (sortAsc2)
                    return source.AsQueryable().OrderByDescending(sortExp1).ThenBy(sortExp2).ToList();
                else
                    return source.AsQueryable().OrderByDescending(sortExp1).ThenByDescending(sortExp2).ToList();

            }
        }


        public IEnumerable<T> GetSortedListMultiple(ObservableCollection<T> source, string sortColumn1, string sortColumn2, bool sortAsc)
        {
            // Prepare the dynamic sort expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp1 =
                             Expression.Convert(Expression.Property(paramExp, sortColumn1), typeof(object));
            var sortExp1 = Expression.Lambda<Func<T, object>>(propConvExp1, paramExp);

            Expression propConvExp2 =
                            Expression.Convert(Expression.Property(paramExp, sortColumn2), typeof(object));
            var sortExp2 = Expression.Lambda<Func<T, object>>(propConvExp2, paramExp);

            if (sortAsc)
            {
                return source.AsQueryable().OrderBy(sortExp1).ThenBy(sortExp2).ToList();
            }
            else
            {
                return source.AsQueryable().OrderByDescending(sortExp1).ThenByDescending(sortExp2).ToList();
            }
        }

        public IEnumerable<T> GetSortedListMultiple(ObservableCollection<T> source, string sortColumn1, string sortColumn2, bool sortAsc1, bool sortAsc2)
        {
            // Prepare the dynamic sort expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp1 =
                             Expression.Convert(Expression.Property(paramExp, sortColumn1), typeof(object));
            var sortExp1 = Expression.Lambda<Func<T, object>>(propConvExp1, paramExp);

            Expression propConvExp2 =
                            Expression.Convert(Expression.Property(paramExp, sortColumn2), typeof(object));
            var sortExp2 = Expression.Lambda<Func<T, object>>(propConvExp2, paramExp);

            if (sortAsc1)
            {
                if (sortAsc2)
                    return source.AsQueryable().OrderBy(sortExp1).ThenBy(sortExp2).ToList();
                else
                    return source.AsQueryable().OrderBy(sortExp1).ThenByDescending(sortExp2).ToList();
            }
            else
            {
                if (sortAsc2)
                    return source.AsQueryable().OrderByDescending(sortExp1).ThenBy(sortExp2).ToList();
                else
                    return source.AsQueryable().OrderByDescending(sortExp1).ThenByDescending(sortExp2).ToList();

            }
        }

        public IEnumerable<T> GetSortedList(ObservableCollection<T> source,string propertyName,string sortColumn, bool sortAsc)
        {
            // Prepare the dynamic sort expression
            var paramExpParent = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression instance = Expression.Property(paramExpParent, typeof(T), propertyName);
           
            Expression propConvExp =
                             Expression.Convert(Expression.Property(instance, sortColumn), typeof(object));
            var sortExp = Expression.Lambda<Func<T, object>>(propConvExp, paramExpParent);

            if (!sortAsc)
            {
                return source.ToList().AsQueryable().OrderBy(sortExp).ToList();
            }
            else
            {
                return source.AsQueryable().OrderByDescending(sortExp).ToList();
            }
        }

        public List<T> GetFilteredList(List<T> source, List<CustomFilter> filters)
        {
            // Prepare the dynamic filter expression
            var paramExp = Expression.Parameter(typeof(T), typeof(T).ToString());
            Expression propConvExp = null , equals =null,value = null,combined =null;
           Expression<Func<T,bool>> filterExp = null;

           foreach (CustomFilter kvp in filters)
           {
               propConvExp = Expression.Property(paramExp, kvp.Key);
               value = Expression.Constant(kvp.Value);
               
               MethodInfo method = typeof(string).GetRuntimeMethods().Where(p => p.Name == kvp.WhereType.ToString()).FirstOrDefault();

               if (kvp.NullCheck)
               {
                   
                   Expression nullCheck = Expression.NotEqual(propConvExp,  Expression.Constant(null));
                   equals = Expression.Call(propConvExp, method, value);
                   equals = Expression.AndAlso(nullCheck, equals);
               }
               else
                equals = Expression.Call(propConvExp, method, value);
               if (combined != null)
                   combined = Expression.AndAlso(combined, equals);
               else
                   combined = equals;
           }

           if (combined != null)
           {
               filterExp = Expression.Lambda<Func<T, bool>>(combined, paramExp);
               return source.AsQueryable().Where(filterExp).ToList();
           }
            return source;
        }


    }
}
