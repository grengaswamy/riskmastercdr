﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    /// <summary>
    /// Defines the helper class for background task registering and unregistering
    /// </summary>
    public static class BackgroundTaskHelper
    {
        /// <summary>
        /// Background task entry point
        /// </summary>
        private const string BackgroundTaskEntryPoint = "ClaimAdjuster.Notifier.TaskNotifier";

        /// <summary>
        /// Background task name
        /// </summary>
        private const string BackgroundTaskName = "ClaimAdjusterNotificationTask";

        /// <summary>
        /// Register a background task with the specified taskEntryPoint, name, trigger,
        /// and condition (optional).
        /// </summary>
        public static IBackgroundTaskRegistration Register()
        {
            string taskEntryPoint = BackgroundTaskEntryPoint;
            string taskName = BackgroundTaskName;
            IBackgroundTaskRegistration task = null;
            IBackgroundTrigger trigger = new TimeTrigger(15, false);
            IBackgroundCondition condition = null;
            
            Unregister(taskName);   // AA: Revisit

            var builder = new BackgroundTaskBuilder();

            builder.Name = taskName;
            builder.TaskEntryPoint = taskEntryPoint;
            builder.SetTrigger(trigger);

            if (condition != null)
            {
                builder.AddCondition(condition);

                // If the condition changes while the background task is executing then it will be canceled.
                //builder.CancelOnConditionLoss = true;
            }

            task = builder.Register();
            return task;
        }

        /// <summary>
        /// Unregister background tasks with specified name.
        /// </summary>
        /// <param name="name">Name of the background task to unregister.</param>
        public static void Unregister(string name)
        {
            foreach (var cur in BackgroundTaskRegistration.AllTasks)
            {
                if (cur.Value.Name == name)
                {
                    cur.Value.Unregister(true);
                }
            }
        }

        public static IBackgroundTaskRegistration IsTaskRegistered(string name)
        {
            return BackgroundTaskRegistration.AllTasks.FirstOrDefault(n => n.Value.Name == name).Value;
        }
    }
}
