﻿namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class ServiceRequestIds
    {
        public static string HomeRequest = "HomePageServiceRequest";
        public static string CscEventsRequest = "CscEventsRequest";
        public static string CscTasksRequest = "CscTasksRequest{0}";
        public static string CscClaimsRequest = "CscClaimsRequest{0}";
        public static string AddTaskRequest = "AddTaskRequest";
        public static string EventDetailsRequest = "EventDetailsRequest{0}";
        public static string ClaimDetailsRequest = "ClaimDetailsRequest{0}{1}";
        public static string TaskDetailsRequest = "TaskDetailsRequest{0}";
        public static string PartiesInvolvedRequest = "PartiesInvolvedRequest";
        public static string TakePhotoRequest = "TakePhotoRequest";
        public static string AddNoteRequest = "AddNoteRequest";
        public static string SignInPageRequest = "SignInPageRequest";
        public static string ServerClaimsRequest = "ServerClaimsRequest";
        public static string ServerTasksRequest = "ServerTasksRequest";
        public static string PhotoPageRequest = "PhotoPageRequest";
        public static string CscNotesRequest = "CscNotesRequest{0}";
        public static string PaymentPageRequest = "PaymentPageRequest";
        public static string ServerEventsRequest = "ServerEventsRequest";
        public static string MediaCaptureRequest = "MediaCaptureRequest";
        public static string SearchRequest = "SearchRequest{0}";
        public static string NoteDetailsRequest = "NoteDetailsRequest{0}";
    }    
}
