﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;

namespace ClaimsAdjuster.ViewModel.Helpers
{

    /// <summary>
    /// Contains helper class for snapping behavior.
    /// Note: This should be moved to BasePage, once BasePage is added.
    /// </summary>
    public static class SnapHelper
    {
        public const double MIN_SUPPORTED_SIZE = 500;

        /// <summary>
        /// Gets the default visual state
        /// </summary>
        public static ViewState DetermineDefaultVisualState()
        {
            ApplicationView currentView = ApplicationView.GetForCurrentView();
            ViewState newState = ViewState.Filled;
            if (currentView != null)
            {
                if (currentView.IsFullScreen)
                {
                    if (currentView.Orientation == ApplicationViewOrientation.Portrait)
                        newState = ViewState.FullScreenPortrait;
                    else
                        newState = ViewState.FullScreenLandscape;
                }
                else if (Window.Current.Bounds.Width < MIN_SUPPORTED_SIZE)
                    newState = ViewState.Snapped;
            }
            return newState;
        }
    }
}
