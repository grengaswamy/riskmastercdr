﻿using IdentityMine.ViewModel;
using System;
using Windows.UI.Core;
using Windows.UI.Popups;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class MessageHelper
    {

        /// <summary>
        /// Displays message dialog box
        /// </summary>
        /// <param name="message"></param>
        public static void Show(string message, string button1Name = null, string button2Name = null, Action button1Action = null, Action button2Action = null)
        {
            MainVMBase.InstanceBase.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog md = new MessageDialog(message);

                if (button1Name != null)
                {
                    md.Commands.Add(new UICommand(button1Name, (command) =>
                    {
                        if (button1Action != null)
                            button1Action.Invoke();
                    }));
                }
                if (button2Name != null)
                {
                    md.Commands.Add(new UICommand(button2Name, (command) =>
                    {
                        if (button2Action != null)
                            button2Action.Invoke();
                    }));
                }


                await md.ShowAsync();
            });
        }
    }
}
