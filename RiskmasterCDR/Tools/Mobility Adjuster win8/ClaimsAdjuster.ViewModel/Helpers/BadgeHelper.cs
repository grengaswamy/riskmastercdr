﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace ClaimsAdjuster.ViewModel.Helpers
{

    /// <summary>
    /// Helper class for Badge related actions
    /// </summary>
    public static class BadgeHelper
    {
        /// <summary>
        /// Updates the badge count
        /// </summary>
        /// <param name="value"></param>
        public static void UpdateCount(int value)
        {
            string badgeCountTemplate = string.Format(@"<badge value=""{0}""/>", value);

            XmlDocument badgeDOM = new XmlDocument();
            badgeDOM.LoadXml(badgeCountTemplate);

            BadgeNotification badge = new BadgeNotification(badgeDOM);
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badge);
        }

        /// <summary>
        /// Clears the badge content
        /// </summary>
        public static void Clear()
        {
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
        }
    }
}
