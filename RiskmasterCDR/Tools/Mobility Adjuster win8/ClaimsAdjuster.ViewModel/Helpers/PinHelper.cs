﻿using ClaimsAdjuster.ViewModel.Repository;
using NotificationsExtensions.TileContent;
using System;
using System.Linq;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.Storage;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class PinHelper
    {
        #region ApplicationTile

        public async static void UpdateApplicationTile()
        {
           
            try
            {
                Update();

            }
            catch (Exception ex)
            {
            }
        }


        private async static void UpdateAllTiles(List<ClaimAdjusterTask> tasks)
        {

            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();


            //For every 3 large tiles create a new medium/wide tile
            for (int i = 0; i < tasks.Count; i = i + 3)
            {
                if (i > 5)
                    break;
                ITileSquare310x310TextList01 largeTile = TileContentFactory.CreateTileSquare310x310TextList01();
                ITileWide310x150Text03 wideTile = TileContentFactory.CreateTileWide310x150Text03();
                ITileSquare150x150Text04 mediumTile = TileContentFactory.CreateTileSquare150x150Text04();
                bool second = false, third = false;
                for (int j = 0; (j + i) < tasks.Count && j < 3; j++)
                {
                    if (j % 3 == 0)
                    {
                        largeTile.TextHeading1.Text = tasks[j + i].TaskName;
                        largeTile.TextBodyGroup1Field1.Text = "Task type : " + tasks[j + i].TaskTypeDisplayText;
                        largeTile.TextBodyGroup1Field2.Text = tasks[j + i].TaskDateTime.ToString();
                        wideTile.TextHeadingWrap.Text = largeTile.TextHeading1.Text + " - " + largeTile.TextBodyGroup1Field2.Text;
                        mediumTile.TextBodyWrap.Text = wideTile.TextHeadingWrap.Text;
                        wideTile.Square150x150Content = mediumTile;
                        largeTile.Wide310x150Content = wideTile;


                    }
                    else if (j % 3 == 1)
                    {
                        largeTile.TextHeading2.Text = tasks[j + i].TaskName;
                        largeTile.TextBodyGroup2Field1.Text = "Task type : " + tasks[j + i].TaskTypeDisplayText;
                        largeTile.TextBodyGroup2Field2.Text = tasks[j + i].TaskDateTime.ToString();
                        second = true;

                    }
                    else if (j % 3 == 2)
                    {
                        largeTile.TextHeading3.Text = tasks[j + i].TaskName;
                        largeTile.TextBodyGroup3Field1.Text = "Task type : " + tasks[j + i].TaskTypeDisplayText;
                        largeTile.TextBodyGroup3Field2.Text = tasks[j + i].TaskDateTime.ToString();
                        third = true;
                    }

                }

                largeTile.ContentId = "Main" + i.ToString();
                TileNotification tileNotification = largeTile.CreateNotification();
                tileNotification.Tag = i.ToString();
                updater.Update(tileNotification);
                if (second)
                {
                    ITileWide310x150Text03 wideTile2 = TileContentFactory.CreateTileWide310x150Text03();
                    ITileSquare150x150Text04 mediumTile2 = TileContentFactory.CreateTileSquare150x150Text04();

                    wideTile2.TextHeadingWrap.Text = largeTile.TextHeading2.Text + " - " + largeTile.TextBodyGroup2Field2.Text;

                    mediumTile2.TextBodyWrap.Text = wideTile2.TextHeadingWrap.Text;

                    wideTile2.Square150x150Content = mediumTile2;
                    largeTile.Wide310x150Content = wideTile2;


                    TileNotification notification2 = largeTile.CreateNotification();
                    notification2.Tag = "Cycle 2" + i.ToString();
                    notification2.ExpirationTime = DateTimeOffset.MaxValue;

                    updater.Update(notification2);
                }
                if (third)
                {

                    ITileWide310x150Text03 wideTile3 = TileContentFactory.CreateTileWide310x150Text03();
                    ITileSquare150x150Text04 mediumTile3 = TileContentFactory.CreateTileSquare150x150Text04();


                    wideTile3.TextHeadingWrap.Text = largeTile.TextHeading3.Text + " - " + largeTile.TextBodyGroup3Field2.Text;
                    mediumTile3.TextBodyWrap.Text = wideTile3.TextHeadingWrap.Text;

                    wideTile3.Square150x150Content = mediumTile3;
                    largeTile.Wide310x150Content = wideTile3;

                    TileNotification notification3 = largeTile.CreateNotification();
                    notification3.Tag = "Cycle 3" + i.ToString();
                    notification3.ExpirationTime = DateTimeOffset.MaxValue;

                    updater.Update(notification3);
                }

            }
        }
        private async static void Update()
        {


            List<ClaimAdjusterTask> tasks = await MainViewModel.Instance.CscRepository.GetOpenTaskAsync();
         
            var filteredTask = tasks.Where(p => ( p.TaskDateTime <= DateTime.Now.AddHours(24) && p.TaskDateTime >= DateTime.Now)).OrderBy(p=> p.TaskDateTime).ToList();  // Get due task for next 24 Hours

            int filteredTaskCount = filteredTask.Count;

            if (filteredTaskCount > 0)
            {
                UpdateAllTiles(filteredTask);
            }

        }


        #endregion

    }
}
