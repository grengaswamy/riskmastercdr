﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class StorageHelper
    {

        /// <summary>
        /// Checks whether folder exists or not. If exists, return the folder instance
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static async Task<StorageFolder> FolderExist(StorageFolder folder, string name)
        {
            try
            {
                return await folder.GetFolderAsync(name);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception)
            {
                throw;  // AA: Handle this
            }
        }

        /// <summary>
        /// Checks whether file exists or not. If exists, return the file instance
        /// </summary>
        public static async Task<StorageFile> FileExists(Uri filepath)
        {
            try
            {
                return await StorageFile.GetFileFromApplicationUriAsync(filepath);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
