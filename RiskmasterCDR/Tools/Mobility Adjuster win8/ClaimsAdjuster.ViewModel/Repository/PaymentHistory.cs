﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("PaymentHistory")]
    public class PaymentHistory
    {
       
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public string ClaimNumber   { get; set; }
        public DateTime PaymentDate { get; set; }
       
        public string PaymentType { get; set; }
        public string Payee { get; set; }
        public string PaymentStatus { get; set; }
        public double Amount { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }
        public string PaymentDescription { get; set; }
        public string PayeeAddress { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", Amount);
        }
    }
}
