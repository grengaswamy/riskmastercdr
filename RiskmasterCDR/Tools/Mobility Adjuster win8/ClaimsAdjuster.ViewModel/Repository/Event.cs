﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Event")]
    public class Event
    {
        private string fullAddress;
        private string secondaryTitle;
        private string dateLocationText;

        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public string EventNumber { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public string EventAddress1 { get; set; }
        public string EventAddress2 { get; set; }
        public string EventDescription { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }
        public DateTime LastSyncDate { get; set; }
        public int TasksCount { get; set; }
        public int ClaimsCount { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}", EventName, EventNumber);
        }

        [Ignore]
        public string EventDateFormatted
        {
            get
            {
                return EventDate.ToString("MM/dd/yyyy");
            }
        }

        [Ignore]
        public string CountText
        {
            get
            {
                string claimText = ClaimsCount == 1 ? MainViewModel.Instance.GetStringResource("ClaimText") : MainViewModel.Instance.GetStringResource("ClaimsText");
                string taskText = TasksCount == 1 ? MainViewModel.Instance.GetStringResource("TaskText") : MainViewModel.Instance.GetStringResource("TasksText");
                string countText = string.Format("{0} {1}, {2} {3}", ClaimsCount, claimText, TasksCount, taskText);
                return countText;
            }
        }

        [Ignore]
        public string SecondaryTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(secondaryTitle))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat(EventDateFormatted);
                    if (!string.IsNullOrWhiteSpace(EventLocation))
                        sb.AppendFormat(", {0}", EventLocation);
                    secondaryTitle = sb.ToString();
                }
                return secondaryTitle;
            }
        }

        [Ignore]
        public string FullAddress
        {
            get
            {
                if (string.IsNullOrWhiteSpace(fullAddress))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(EventAddress1);
                    if (!string.IsNullOrWhiteSpace(EventAddress2))
                        sb.AppendFormat(", {0}.", EventAddress2);
                    else if (!string.IsNullOrWhiteSpace(sb.ToString()))
                        sb.Append(".");
                    sb.Append(City);
                    if (!string.IsNullOrWhiteSpace(City))
                        sb.Append(", ");
                    sb.Append(State);
                    if (!string.IsNullOrWhiteSpace(State))
                        sb.Append(", ");
                    sb.Append(Country);
                    if (!string.IsNullOrWhiteSpace(Country) && !string.IsNullOrWhiteSpace(PostalCode))
                        sb.Append(", ");
                    sb.Append(PostalCode);
                    fullAddress = sb.ToString();
                }
                return fullAddress;
            }
        }

        [Ignore]
        public string DateLocationText
        {
            get
            {
                if (string.IsNullOrWhiteSpace(dateLocationText))
                {
                    if (!string.IsNullOrWhiteSpace(EventLocation))
                        dateLocationText = string.Format("{0}, {1}", EventDateFormatted, EventLocation);
                    else
                        dateLocationText = EventDateFormatted;
                }
                return dateLocationText;
            }
        }

        [Ignore]
        public string EventHashNumber
        {
            get
            {
                return string.Format("{0}{1}", MainViewModel.Instance.GetStringResource("EventHashText"), EventNumber);
            }
        }

        [Ignore]
        public string EventLocation
        {
            get
            {
                return !string.IsNullOrWhiteSpace(State) ? State : (!string.IsNullOrWhiteSpace(City) ? City : (!string.IsNullOrWhiteSpace(EventAddress1) ? EventAddress1 : string.Empty));
            }
        }

    }
}
