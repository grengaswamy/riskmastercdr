﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Attachments")]
    public class Attachment
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public MediaCaptureType AttachmentType { get; set; }
        public ParentType ParentType { get; set; }
        public string ParentId { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }

        public DateTime CreatedOn { get; set; }

        [Ignore]
        public string CreatedOnText
        {
            get
            {
                return this.CreatedOn.ToString("hh:mm:ss tt, MM/dd/yyyy");
            }
        }
        
        public override string ToString()
        {
            return string.Format("{0}", FileName);
        }
    }
}
