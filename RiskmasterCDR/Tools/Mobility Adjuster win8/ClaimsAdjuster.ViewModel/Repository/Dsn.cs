﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Dsn")]
    public class Dsn
    {
        public string Name { get; set; }
    }
}
