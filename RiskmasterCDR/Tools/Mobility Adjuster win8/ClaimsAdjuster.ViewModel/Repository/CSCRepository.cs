﻿using SQLite;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Repository
{
    #region Constants

    public enum DataStatus
    {
        NewlyAddedFromServer,
        PreviouslyAddedFromServer,
        Modified,
        SyncSuccess,
        SyncFail,
        ClosedOffline
    }
    public enum ParentType
    {
        Claim,
        Event,
        Task
    }
    #endregion

    #region SQLite DB

    public interface IDatabase
    {
        Task Initialize();
        SQLiteAsyncConnection GetAsyncConnection();
    }
    public class Database : IDatabase
    {
        private readonly SQLiteAsyncConnection _dbConnection;

        public Database(string databasePath)
        {
            _dbConnection = new SQLiteAsyncConnection(databasePath);
        }

        public async Task Initialize()
        {
            await _dbConnection.CreateTableAsync<User>();
            await _dbConnection.CreateTableAsync<Event>();
            await _dbConnection.CreateTableAsync<Claim>();
            await _dbConnection.CreateTableAsync<ClaimAdjusterTask>();
            await _dbConnection.CreateTableAsync<Attachment>();
            await _dbConnection.CreateTableAsync<Notes>();
            await _dbConnection.CreateTableAsync<PaymentHistory>();
            await _dbConnection.CreateTableAsync<Code>();
            await _dbConnection.CreateTableAsync<PartyInvolved>();
            await _dbConnection.CreateTableAsync<Dsn>();
        }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            return _dbConnection;
        }
    }
    #endregion

    #region Repository

    public interface ICSCRepository
    {
        #region User

        Task SaveUserAsync(User user);

        Task<User> GetUserDetails(int userId);

        #endregion

        #region Event

        Task SaveEventAsync(Event eventdata);

        Task<Event> GetEventByEventNumberAsync(string eventNumber);
        #endregion

        #region Claim

        Task SaveClaimAsync(Claim claimdata);
        Task SaveBulkDataAsync(List<object> lstRecords);

        Task<List<Claim>> GetClaimsByEventAsync(string eventNumber);

        Task<Claim> GetClaimDetails(string claimNumber);

        Task<Claim> GetClaimsByClaimNumberAsync(string claimNumber);
        Task<Claim> GetClaimsByClaimViewIdAsync(int viewId);  //mbahl3 Mits 36145
        #endregion

        #region ClaimAdjusterTask

        Task SaveClaimAdjusterTaskAsync(ClaimAdjusterTask claimAdjusterTask);


        Task<List<ClaimAdjusterTask>> GetTaskByClaimAsync(string claimNumber);

        Task<List<ClaimAdjusterTask>> GetTaskByStatusAsync(int status);

       Task<List<ClaimAdjusterTask>> GetTasksByFilteringStatusAsync(int status);

        Task<List<ClaimAdjusterTask>> GetTaskByEventAsync(string eventNumber);

        Task<List<ClaimAdjusterTask>> GetOpenTaskAsync();

        #endregion

        #region Attachment

        Task SaveAttachmentAsync(Attachment attachment);

        Task<List<Attachment>> GetAttachments(string parentId);

        Task<List<Attachment>> GetAttachments(string parentId, ParentType parentType, MediaCaptureType mediaCaptureType);
        #endregion

        #region Notes

        Task SaveNotesAsync(Notes notes);


        Task<int> GetNotesCountByClaimAsync(string claimNumber);

        Task<List<Notes>> GetNotesByClaimAsync(string claimNumber);

        Task<List<PaymentHistory>> GetPaymentByClaimAsync(string claimNumber);

        Task<bool> DeleteNotesByClaimAsync(string claimNumber);

        Task<bool> DeletePaymentByClaimAsync(string claimNumber);
        #endregion

        #region PaymentHistory

        Task SavePaymentHistoryAsync(PaymentHistory paymentHistory);

        Task<List<PaymentHistory>> GetPaymentHistoryByClaimAsync(string claimNumber);

        Task<bool> DeletePaymentHistoryByClaimAsync(string claimNumber);

        #endregion

        #region PartyInvolved

        Task SavePartyInvolvedAsync(PartyInvolved partyInvolved);

        Task<List<PartyInvolved>> GetPartyInvolvedByClaimAsync(string claimNumber);

        Task<bool> DeletePartyInvolvedByClaimAsync(string claimNumber);

        #endregion

        #region Codes

        Task<List<Code>> GetCodesByTypeAsync(CodeType codeType);

        #endregion

        #region Method

        Task<List<T>> GetObjectsAsync<T>() where T : new();

        Task DeleteAsync<T>(T item);

        Task<int> GetCount<T>() where T : new();

        Task InsertItemsAsync(IEnumerable list);

        Task DeleteAllAsync<T>();

        Task DeleteItemListAsync<T>(IEnumerable list);

        Task UpdateItemListAsync(IEnumerable list);

        #endregion
		//mbahl3 Mits 36145
        Task<Claim> GetClaimDetails(int parentViewId);
        Task<Claim> GetClaimsByClaimNumberAsync(int parentViewId);
		//mbahl3 Mits 36145
    }
    public class CSCRepository : ICSCRepository
    {

        private readonly SQLiteAsyncConnection _database;

        #region User

        public CSCRepository(IDatabase database)
        {
            _database = database.GetAsyncConnection();
        }

        public async Task SaveUserAsync(User user)
        {
            if (user.Id == 0)
                await _database.InsertAsync(user);
            else
                await _database.UpdateAsync(user);
        }

        public async Task<User> GetUserDetails(int userId)
        {
            return await _database.Table<User>().Where(s => s.Id == userId).FirstOrDefaultAsync();
        }

        #endregion

        #region Event

        public async Task SaveEventAsync(Event eventdata)
        {
            if (eventdata.Id == 0)
                await _database.InsertAsync(eventdata);
            else
                await _database.UpdateAsync(eventdata);
        }

        public async Task<Event> GetEventByEventNumberAsync(string eventNumber)
        {
            var events = await _database.Table<Event>().ToListAsync();
            return events.Where(claimevent => claimevent.EventNumber.Contains(eventNumber)).FirstOrDefault();
        }

        #endregion

        #region Claim

        public async Task SaveClaimAsync(Claim claimdata)
        {
            if (claimdata.Id == 0)
                await _database.InsertAsync(claimdata);
            else
                await _database.UpdateAsync(claimdata);
        }

        public async Task SaveBulkDataAsync(List<object> lstDataRecords)
        {
            await _database.BulkUpdate(lstDataRecords);
        }

        public async Task<List<Claim>> GetClaimsByEventAsync(string eventNumber)
        {
            var claims = await _database.Table<Claim>().Where(t => t.EventNumber == eventNumber).ToListAsync();
            return claims;
        }

        public async Task<Claim> GetClaimDetails(string claimNumber)
        {
            return await _database.Table<Claim>().Where(s => s.ClaimNumber == claimNumber).FirstOrDefaultAsync();
        }
		//mbahl3 Mits 36145
        public async Task<Claim> GetClaimDetails(int parentViewId)
        {
            return await _database.Table<Claim>().Where(s => s.Id == parentViewId).FirstOrDefaultAsync();
        }
		//mbahl3 Mits 36145
        public async Task<Claim> GetClaimsByClaimNumberAsync(string claimNumber)
        {
            var claims = await _database.Table<Claim>().ToListAsync();
            return claims.Where(claim => claim.ClaimNumber.Contains(claimNumber)).FirstOrDefault();
        }

        //mbahl3 Mits 36145
        public async Task<Claim> GetClaimsByClaimNumberAsync(int parentViewId)
        {
            var claims = await _database.Table<Claim>().ToListAsync();
            return claims.FirstOrDefault(x => x.Id.Equals(parentViewId));
        }

        public async Task<Claim> GetClaimsByClaimViewIdAsync(int viewId)
        {
            var claims = await _database.Table<Claim>().ToListAsync();
            return claims.FirstOrDefault(claim=>claim.Id.Equals(viewId));
        }
	 //mbahl3 Mits 36145

        #endregion

        #region ClaimAdjusterTask

        public async Task SaveClaimAdjusterTaskAsync(ClaimAdjusterTask claimAdjusterTask)
        {
            if (claimAdjusterTask.Id == 0)
                await _database.InsertAsync(claimAdjusterTask);
            else
                await _database.UpdateAsync(claimAdjusterTask);
        }

        public async Task<List<ClaimAdjusterTask>> GetTaskByClaimAsync(string claimNumber)
        {
            var openTasks = await GetTasksByFilteringStatusAsync((int)DataStatus.ClosedOffline);
            return openTasks.Where(s => s.ClaimNumber == claimNumber).ToList();
        }

        public async Task<List<ClaimAdjusterTask>> GetTaskByStatusAsync(int status)
        {
            return await _database.Table<ClaimAdjusterTask>().Where(s => s.Status == status).ToListAsync();
        }

        public async Task<List<ClaimAdjusterTask>> GetTasksByFilteringStatusAsync(int status)
        {
            return await _database.Table<ClaimAdjusterTask>().Where(s => s.Status != status).ToListAsync();
        }
        public async Task<List<ClaimAdjusterTask>> GetTaskByEventAsync(string eventNumber)
        {
            var openTasks = await GetTasksByFilteringStatusAsync((int)DataStatus.ClosedOffline);
            return openTasks.Where(s => s.EventNumber == eventNumber).ToList();
        }

        public async Task<List<ClaimAdjusterTask>> GetOpenTaskAsync()
        {
            return await _database.Table<ClaimAdjusterTask>().Where(s => s.Status != (int)DataStatus.ClosedOffline).ToListAsync();
        }

        #endregion

        #region Attachment

        public async Task SaveAttachmentAsync(Attachment attachment)
        {
            if (attachment.Id == 0)
                await _database.InsertAsync(attachment);
            else
                await _database.UpdateAsync(attachment);
        }

        public async Task<List<Attachment>> GetAttachments(string parentId)
        {
            return await _database.Table<Attachment>().Where(a => a.ParentId == parentId).ToListAsync();
        }

        public async Task<List<Attachment>> GetAttachments(string parentId, ParentType parentType, MediaCaptureType mediaCaptureType)
        {
            return await _database.Table<Attachment>().Where(a => a.ParentId == parentId && a.ParentType == parentType && a.AttachmentType == mediaCaptureType).ToListAsync();
        }
        #endregion

        #region Notes

        public async Task SaveNotesAsync(Notes notes)
        {
            if (notes.Id == 0)
                await _database.InsertAsync(notes);
            else
                await _database.UpdateAsync(notes);
        }

        public async Task<List<Notes>> GetNotesByClaimAsync(string claimNumber)
        {
            var notes = await _database.Table<Notes>().ToListAsync();
            return notes.Where(note => note.ParentType == (int)ParentType.Claim && note.ParentId.Trim().Equals(claimNumber.Trim())).ToList();
        }
        public async Task<int> GetNotesCountByClaimAsync(string claimNumber)
        {
            var notes = await _database.Table<Notes>().ToListAsync();
            if (notes == null)
                return 0;
            return notes.Count;
        }

        public async Task<bool> DeleteNotesByClaimAsync(string claimNumber)
        {
            bool success = true;
            try
            {
                var notes = await GetNotesByClaimAsync(claimNumber);
                if (notes != null)
                    notes = notes.Where(note => note.Status != (int)DataStatus.SyncFail || note.Status != (int)DataStatus.Modified).ToList();
                await DeleteItemListAsync<Notes>(notes);
            }
            catch
            {
                success = false;
            }


            return success;
        }
        public async Task<bool> DeletePaymentByClaimAsync(string claimNumber)
        {
            bool success = true;
            try
            {
                var payment = await GetPaymentByClaimAsync(claimNumber);
                if (payment != null)
                    payment = payment.ToList();
                await DeleteItemListAsync<PaymentHistory>(payment);
            }
            catch
            {
                success = false;
            }


            return success;
        }

        public async Task<List<PaymentHistory>> GetPaymentByClaimAsync(string claimNumber)
        {
            var payment = await _database.Table<PaymentHistory>().ToListAsync();
            return payment.Where(pay => pay.ClaimNumber.Trim().Equals(claimNumber.Trim())).ToList();
        }
        
        #endregion

        #region PaymentHistory

        public async Task SavePaymentHistoryAsync(PaymentHistory paymentHistory)
        {
            if (paymentHistory.Id == 0)
                await _database.InsertAsync(paymentHistory);
            else
                await _database.UpdateAsync(paymentHistory);
        }
     
        public async Task<List<PaymentHistory>> GetPaymentHistoryByClaimAsync(string claimNumber)
        {
            var paymentHistories = await _database.Table<PaymentHistory>().ToListAsync();
            return paymentHistories.Where(paymentHistory =>  paymentHistory.ClaimNumber.Trim().Equals(claimNumber.Trim())).ToList();
        }

        public async Task<bool> DeletePaymentHistoryByClaimAsync(string claimNumber)
        {
            bool success = true;
            try
            {
                var paymentHistories = await GetPaymentHistoryByClaimAsync(claimNumber);
                foreach (var paymentHistory in paymentHistories)
                {
                    await DeleteAsync(paymentHistory);
                }
            }
            catch
            {
                success = false;
            }


            return success;
        }

        #endregion

        #region PartyInvolved

        public async Task SavePartyInvolvedAsync(PartyInvolved partyInvolved)
        {
            if (partyInvolved.Id == 0)
                await _database.InsertAsync(partyInvolved);
            else
                await _database.UpdateAsync(partyInvolved);
        }

        public async Task<List<PartyInvolved>> GetPartyInvolvedByClaimAsync(string claimNumber)
        {
            var partiesInvolved = await _database.Table<PartyInvolved>().ToListAsync();
            return partiesInvolved.Where(partyInvolved => partyInvolved.ClaimNumber.Trim().Equals(claimNumber.Trim())).ToList();
        }

        public async Task<bool> DeletePartyInvolvedByClaimAsync(string claimNumber)
        {
            bool success = true;
            try
            {
                var partiesInvolved = await GetPartyInvolvedByClaimAsync(claimNumber);
                foreach (var partyInvolved in partiesInvolved)
                {
                    await DeleteAsync(partyInvolved);
                }
            }
            catch
            {
                success = false;
            }


            return success;
        }

        #endregion
       
        #region Codes

        public async Task<List<Code>> GetCodesByTypeAsync(CodeType codeType)
        {
           return await _database.Table<Code>().Where(code => code.CodeType == codeType).ToListAsync();
        }


        #endregion

        #region Methods

        /// <summary>
        /// Generic method to fetch all items from db
        /// </summary>
        public async Task<List<T>> GetObjectsAsync<T>() where T : new()
        {
            var result = await _database.Table<T>().ToListAsync();
            return result;
        }

        /// <summary>
        /// Generic method to delete an item from db
        /// </summary>
        public async Task DeleteAsync<T>(T item)
        {
            await _database.DeleteAsync(item);
        }

        /// <summary>
        /// Generic method to fetch Count of items in db
        /// </summary>
        public async Task<int> GetCount<T>() where T : new()
        {
            return await _database.Table<T>().CountAsync();
        }

        /// <summary>
        /// Insert list of items to respective table
        /// </summary>
        public async Task InsertItemsAsync(IEnumerable list)
        {
            await _database.InsertAllAsync(list);
        }

        /// <summary>
        /// Removes all items from the table
        /// </summary>
        public async Task DeleteAllAsync<T>()
        {
            await _database.DeleteAllAsync<T>();
        }

        /// <summary>
        /// Removes all items from the table
        /// </summary>
        public async Task DeleteItemListAsync<T>(IEnumerable list)
        {
            await _database.BulkDelete(list);
        }

        public async Task UpdateItemListAsync(IEnumerable list)
        {
            await _database.UpdateAllAsync(list);
        }

        #endregion
    }
    #endregion
}
