﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Notes")]
    public class Notes
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public int ParentType { get; set; }
        public string NoteServerId { get; set; }
        public string ParentId { get; set; }
        public string Heading { get; set; }
        public string NoteType { get; set; }
        public string NoteTypeCodeId { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }

        [Ignore]
        public string CreatedDateTimeText
        {
            get
            {
                // AA: Replace this when created date is populated in table: Notes
                return DateCreated.ToString("MM/dd/yyyy H:m:s tt");
            }
        }

        public override string ToString()
        {
            return string.Format("{0}", Heading);
        }
        [Ignore]
        public bool IsNew
        {
            get
            {
                return Status == (int)DataStatus.NewlyAddedFromServer;
            }
        }
    }
}
