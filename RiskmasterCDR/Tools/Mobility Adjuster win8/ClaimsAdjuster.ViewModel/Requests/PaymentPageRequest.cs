﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityMine.ViewModel;
using ClaimsAdjuster.ViewModel.Repository;

namespace ClaimsAdjuster.ViewModel
{
    public class PaymentPageRequest : ServiceRequestBaseEx
    {
        #region Constructors
        
        public PaymentPageRequest()
        {

        }

        public PaymentPageRequest(string id)
            :base(id)
        {
            Result = new PaymentCollection();
        }

        #endregion

        #region Properties

        public Claim CurrentClaim { get; set; }

        

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            var paymentCollection = this.Result as PaymentCollection;
            paymentCollection.CurrentClaim = this.CurrentClaim;
            await ExecuteRequest(paymentCollection);
            await base.OnLoad();
        }

        private async Task ExecuteRequest(PaymentCollection paymentCollection)
        {
            var payment = await MainViewModel.Instance.CscRepository.GetPaymentByClaimAsync(CurrentClaim.ClaimNumber);
            if (payment.Count <= 0 && MainViewModel.Instance.IsInternetAvailable)
            {
                await paymentCollection.GetPaymentDetails(this.CurrentClaim, false);
                payment = await MainViewModel.Instance.CscRepository.GetPaymentByClaimAsync(CurrentClaim.ClaimNumber);
            }
            paymentCollection.LoadPayment(payment, CurrentClaim);
        }

        #endregion
    }
}
