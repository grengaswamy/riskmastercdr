﻿using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using CSC.ServiceFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ClaimDetailsRequest : ServiceRequestBaseEx
    {

        private string claimNumber;
        public int ViewId { get; set; } //mbahl3 Mits 36145
        #region Constructor

        public ClaimDetailsRequest()
        { }

        public ClaimDetailsRequest(string id, string claimNumber)
            : base(id)
        {
            this.claimNumber = claimNumber;
        }
		//mbahl3 Mits 36145
        public ClaimDetailsRequest(string id, string claimNumber, int viewId)
            : this(id, claimNumber)
        {
            this.ViewId = viewId;
        }
		//mbahl3 Mits 36145
        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await  LoadAdditionalData();
            await base.OnLoad();
        }


        private async Task LoadAdditionalData()
        {
            var datacontext = Result as ClaimViewModel;
			//mbahl3 Mits 36145
            //datacontext.CscClaim = await MainViewModel.Instance.CscRepository.GetClaimsByClaimNumberAsync(this.claimNumber);
            datacontext.CscClaim = await MainViewModel.Instance.CscRepository.GetClaimsByClaimViewIdAsync(this.ViewId);
            //mbahl3 Mits 36145
			if (!datacontext.CscClaim.IsDetailDownloaded && MainViewModel.Instance.IsInternetAvailable)
              await   MainViewModel.Instance.GetClaimDetails(datacontext.CscClaim, false);
            await datacontext.LoadTasksAndNotes(this.claimNumber);
            //datacontext.LoadNotes(this.claimNumber);
            //datacontext.LoadPictures(this.claimNumber);
            await datacontext.LoadMediaContent(this.claimNumber, MediaCaptureType.Photo, ParentType.Claim);
            await datacontext.LoadMediaContent(this.claimNumber, MediaCaptureType.Video, ParentType.Claim);
            await datacontext.LoadMediaContent(this.claimNumber, MediaCaptureType.Audio, ParentType.Claim);
            MainViewModel.Instance.TileExists(datacontext.CscClaim.ClaimNumber);
          
        }

        #endregion
    }
}
