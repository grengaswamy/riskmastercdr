﻿
using ClaimsAdjuster.ViewModel.Repository;
using System;
using System.Threading.Tasks;
namespace ClaimsAdjuster.ViewModel
{
    public class TaskDetailsRequest : ServiceRequestBaseEx
    {

        #region Fields

        private int serverEntryId;

        #endregion

        #region Constructors

        public TaskDetailsRequest()
        {
        }

        public TaskDetailsRequest(string id, int serverEntryId) :
            base(id)
        {
            this.serverEntryId = serverEntryId;
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await LoadAdditionalData();
            await base.OnLoad();
        }

        private async Task LoadAdditionalData()
        {
            TaskViewModel datacontext = Result as TaskViewModel;
            await datacontext.InitializeTaskDetails();
            //await datacontext.LoadPictures(this.serverEntryId.ToString());
            await datacontext.LoadMediaContent(this.serverEntryId.ToString(), MediaCaptureType.Photo, ParentType.Task);
            await datacontext.LoadMediaContent(this.serverEntryId.ToString(),MediaCaptureType.Video,ParentType.Task);
            await datacontext.LoadMediaContent(this.serverEntryId.ToString(), MediaCaptureType.Audio, ParentType.Task);
            MainViewModel.Instance.TileExists(Convert.ToString(datacontext.CscTask.ServerEntryId));
        }

        #endregion
    }
}
