﻿
namespace ClaimsAdjuster.ViewModel
{
    public class SignInPageRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public SignInPageRequest()
        {

        }

        public SignInPageRequest(string id)
            : base(id)
        {

        }

        #endregion
    }
}
