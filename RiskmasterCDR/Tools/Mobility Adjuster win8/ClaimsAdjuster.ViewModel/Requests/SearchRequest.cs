﻿using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class SearchRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public SearchRequest()
        {

        }

        public SearchRequest(string id)
            : base(id)
        {
            Result = new SearchViewModel();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            ExecuteRequest();
            await base.OnLoad();
        }

        private void ExecuteRequest()
        {
            (Result as SearchViewModel).LoadResultsCommand.Execute(SearchType.Claims.ToString());
        }

        #endregion
    }
}
