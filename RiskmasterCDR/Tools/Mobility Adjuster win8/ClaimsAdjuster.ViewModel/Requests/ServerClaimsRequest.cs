﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.ViewModel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ServerClaimsRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public ServerClaimsRequest()
        {
        }

        public ServerClaimsRequest(string id) :
            base(id)
        {
            Result = new ClaimCollection();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            string claimsResult = string.Empty;
            if (MainViewModel.Instance.IsInternetAvailable)
            {
                string requestXml = string.Format(MobileCommonService.GetClaimsRequest, MainViewModel.Instance.AuthenticationToken, string.Empty);
                claimsResult = await MobileCommonService.ProcessRequestAsync(requestXml); 
            }
            await (Result as ClaimCollection).SetServerClaims(claimsResult);
        }

        #endregion
    }
}
