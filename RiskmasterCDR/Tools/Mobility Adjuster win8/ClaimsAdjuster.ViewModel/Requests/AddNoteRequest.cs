﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class AddNoteRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public AddNoteRequest()
        {
        }

        public AddNoteRequest(string id) :
            base(id)
        {
            Result = new NoteViewModel() { ClassID = Constants.AddNoteClassID };
        }

        #endregion

        #region Properties

        public string ClaimNumber { get; set; }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            NoteViewModel noteVM = (Result as NoteViewModel);
            noteVM.DisplayName = MainViewModel.Instance.GetStringResource("AddNoteText");
            await noteVM.LoadData(this.ClaimNumber);
        }

        #endregion
    }
}
