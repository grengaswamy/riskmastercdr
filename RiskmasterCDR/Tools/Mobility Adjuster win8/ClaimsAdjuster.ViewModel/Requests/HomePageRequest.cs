﻿
namespace ClaimsAdjuster.ViewModel
{
    public class HomePageRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public HomePageRequest()
        {
        }

        public HomePageRequest(string id) :
            base(id)
        {
            Result = MainViewModel.Instance.HomePageVM;
        }

        #endregion
    }
}
