﻿
namespace ClaimsAdjuster.ViewModel
{
    public class AddTaskRequest : ServiceRequestBaseEx
    {
        #region Constructors
        
        public AddTaskRequest()
        {
        }

        public AddTaskRequest(string id) :
            base(id)
        {
        } 

        #endregion
    }
}
