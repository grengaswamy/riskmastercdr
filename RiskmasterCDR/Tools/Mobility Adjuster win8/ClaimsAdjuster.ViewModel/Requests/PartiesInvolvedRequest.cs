﻿using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class PartiesInvolvedRequest: ServiceRequestBaseEx
    {
        #region Constructors

        public PartiesInvolvedRequest()
        {
        }

        public PartiesInvolvedRequest(string id) :
            base(id)
        {
            Result = new PartyCollection();
        }

        #endregion

        #region Properties

        public string ClaimNumber { get; set; }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
             await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
             await (Result as PartyCollection).LoadData(this.ClaimNumber);
        }

        #endregion

    }
}
