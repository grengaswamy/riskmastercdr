﻿using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class TasksRequest : ServiceRequestBaseEx
    {

        #region Properties

        /// <summary>
        /// Gets or sets the claim/event number
        /// </summary>
        public string ParentNumber { get; set; }

        public int ClaimViewId { get; set; } //mbahl3 Mits 36145

        /// <summary>
        /// Contains Claim/Event/Tasks
        /// </summary>
        public ParentType ParentType { get; set; }

        #endregion

        #region Constructors

        public TasksRequest()
        {
        }

        public TasksRequest(string id) :
            base(id)
        {
            Result = new TaskCollection();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            var taskCollection = Result as TaskCollection;
            taskCollection.Collection.Clear();  // clear previous collection

            List<ClaimAdjusterTask> tasks = null;
            switch (ParentType)
            {
                case ParentType.Claim://Get tasks repective to the Claim
                    tasks = await MainViewModel.Instance.CscRepository.GetTaskByClaimAsync(this.ParentNumber);
                    break;
                case ParentType.Event://Get tasks repective to the Event
                    tasks = await MainViewModel.Instance.CscRepository.GetTaskByEventAsync(this.ParentNumber);
                    break;
                case ParentType.Task://Get all tasks
                    tasks = await MainViewModel.Instance.CscRepository.GetTasksByFilteringStatusAsync((int)DataStatus.ClosedOffline );
                    break;
            }
            await taskCollection.LoadData(tasks, ClaimViewId); //mbahl3 Mits 36145
        }

        
        #endregion

    }
}
