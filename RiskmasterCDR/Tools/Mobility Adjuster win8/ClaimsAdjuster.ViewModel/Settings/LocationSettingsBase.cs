﻿using System;
using System.Net;
using System.Windows;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Windows.Devices.Geolocation;
using IdentityMine.ViewModel;

namespace ClaimsAdjuster.ViewModel
{
    [DataContract]
    public class LocationSettingsBase : SettingsBase
    {
        #region Fields

        private Geolocator geoWatcher;
        private PositionStatus locationStatus;
        private string postalCode;
        private Geocoordinate coordinate;

        public event EventHandler LocationChanged;
        public event EventHandler GeoLocatorStatusChanged;

        #endregion

        #region Constructor

        public LocationSettingsBase()
        {
            geoWatcher = new Geolocator();
        }

        #endregion

        #region Properties

        [DataMember]
        public string PostalCode
        {
            get
            {
                return postalCode;
            }
            set
            {
                if (value == postalCode) return;
                postalCode = value;
                RaisePropertyChanged("PostalCode");
                if (LocationChanged != null)
                {
                    LocationChanged(null, null);
                }
            }
        }

        public Geocoordinate Coordinate
        {
            get
            {
                return coordinate;
            }
            set
            {
                if (value == coordinate) return;
                coordinate = value;
                RaisePropertyChanged("Coordinate");

                if (LocationChanged != null)
                {
                    LocationChanged(null, null);
                }
            }
        }

        public PositionStatus LocationStatus
        {
            get
            {
                return locationStatus;
            }
        }

        #endregion

        #region Methods

        public async void RefreshLocation()
        {
            try
            {
                if (geoWatcher == null)
                    geoWatcher = new Geolocator();
                locationStatus = geoWatcher.LocationStatus;
                geoWatcher.PositionChanged += geoWatcher_PositionChanged;
                geoWatcher.StatusChanged += geoWatcher_StatusChanged;
                Geoposition position = await geoWatcher.GetGeopositionAsync();

                Coordinate = position == null ? null : position.Coordinate;
            }
            catch
            {
                Coordinate = null;
            }

        }

        void geoWatcher_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            locationStatus = geoWatcher.LocationStatus;
        }

        void geoWatcher_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            locationStatus = args.Status;

            if (args.Status == PositionStatus.Disabled)
            {
                //http://msdn.microsoft.com/en-us/library/windows/apps/xaml/Hh465127(v=win.10).aspx says:
                //"If the user re-enables location access after disabling it, there is no notification to the app. The status property does not change, and there is 
                //no StatusChanged event. Your app should reattempt access by creating a new Geolocator object and calling GetGeopositionAsync to get updated location data, 
                //or resubscribing to PositionChanged events."

                geoWatcher = new Geolocator();

                Coordinate = null;
            }

            if (GeoLocatorStatusChanged != null)
            {
                GeoLocatorStatusChanged(null, null);
            }
        }

        #endregion
    }

}

