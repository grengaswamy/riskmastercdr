﻿using ClaimsAdjuster.View.Pages;
using ClaimsAdjuster.ViewModel;
using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using System;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace ClaimsAdjuster.View.Helper
{

    /// <summary>
    /// Helper class for managing Settings Pane in Windows 8
    /// </summary>
    public class SettingsHelper
    {
        #region Static Members

        static SettingsHelper _instance;

        #endregion

        #region Properties

        public static SettingsHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingsHelper();
                }
                return (SettingsHelper)_instance;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attaches event(s) necessary for loading Settings Pane and related item(s)
        /// </summary>
        public void Load()
        {
            SettingsPane.GetForCurrentView().CommandsRequested += SettingPane_CommandsRequested;
        }

        public void Unload()
        {
            SettingsPane.GetForCurrentView().CommandsRequested -= SettingPane_CommandsRequested;
        }

        /// <summary>
        /// Populate settings command for each Settings Pane item(s)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private async void SettingPane_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {

            args.Request.ApplicationCommands.Clear();

            SettingsCommand sc;

            if (!MainViewModel.Instance.IsAuthenticated)    // If not authenticated, show only privacy statement
            {
                sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_PrivacyStatementText"), async (handler) =>
                {
                    LaunchHelper.LaunchBrowser(Constants.Settings_PrivacyStatementUrl);
                });
                args.Request.ApplicationCommands.Add(sc);
                return;
            }

            // GUID was added because of error - " Object in an IPropertyValue is of type 'String', which cannot be converted to a 'Guid'" in some machines
            // Reference: http://connect.microsoft.com/VisualStudio/feedback/details/797900/invalidcastexception-when-creating-new-settingscommand-instance-in-vs-2013

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("ChangeDBServerText"), async (handler) =>
            {
                DSNSettingsFlyout serverEditSettingsFlyout = new DSNSettingsFlyout();
                await MainViewModel.Instance.AuthenticationVM.PopulateDSNList();
                serverEditSettingsFlyout.DataContext = new User()
                {
                    UserName = MainViewModel.Instance.CurrentUserInfo.UserName,
                    WebServerUrl = MainViewModel.Instance.CurrentUserInfo.WebServerUrl,
                    DSNName = MainViewModel.Instance.CurrentUserInfo.DSNName
                };

                serverEditSettingsFlyout.Show();
            });
            args.Request.ApplicationCommands.Add(sc);

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_AboutClaimAdjusterText"), async (handler) =>
            {
                AboutPageSettingsFlyout aboutPage = new AboutPageSettingsFlyout();
                aboutPage.Show();
            });
            args.Request.ApplicationCommands.Add(sc);

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_AboutCSCText"), async (handler) =>
            {
                LaunchHelper.LaunchBrowser(Constants.Settings_AboutCSCUrl);
            });
            args.Request.ApplicationCommands.Add(sc);

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_PrivacyStatementText"), async (handler) =>
            {
                LaunchHelper.LaunchBrowser(Constants.Settings_PrivacyStatementUrl);
            });
            args.Request.ApplicationCommands.Add(sc);

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_LogoutText"), async (handler) =>
            {
                bool hasUploads = await MainViewModel.Instance.AuthenticationVM.CheckPendingUploadsAsync();
                if (hasUploads)
                {
                    ConfirmLogoutFlyout logoutFlyout = new ConfirmLogoutFlyout();
                    logoutFlyout.Show();
                }
                else
                {
                    args.Request.ApplicationCommands.Clear();
                    Unload();
                    MainViewModel.Instance.AuthenticationVM.ClearSignInDetails();
                }
            });
            args.Request.ApplicationCommands.Add(sc);

            sc = new SettingsCommand(Guid.NewGuid(), MainViewModel.Instance.GetStringResource("Settings_ContactClaimAdjusterText"), async (handler) =>
            {
                ContactPage contactPage = new ContactPage();
                contactPage.Show();
            });
            args.Request.ApplicationCommands.Add(sc);

        }

        #endregion


    }
}
