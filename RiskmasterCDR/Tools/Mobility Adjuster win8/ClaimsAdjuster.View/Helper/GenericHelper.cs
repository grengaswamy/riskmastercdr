﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace ClaimsAdjuster.View.Helper
{
    class GenericHelper
    {
        public static FrameworkElement FindNameInTree(FrameworkElement whereToFind, String name)
        {
            if (whereToFind == null) return null;
            if (whereToFind.Name == name) return whereToFind;
            if (whereToFind as Panel != null)
            {
                Panel panelToExplore = whereToFind as Panel;
                foreach (FrameworkElement child in panelToExplore.Children)
                {
                    FrameworkElement result = FindNameInTree(child, name);
                    if (result != null) return result;
                }
                return null;
            }
            else if (whereToFind as UserControl != null)
            {
                FrameworkElement content = ((UserControl)whereToFind).GetValue(UserControl.ContentProperty) as FrameworkElement;
                return FindNameInTree(content, name);
            }
            else if (whereToFind as ItemsControl != null)
            {
                ItemCollection items = (whereToFind as ItemsControl).Items;
                foreach (FrameworkElement child in items)
                {
                    FrameworkElement result = FindNameInTree(child, name);
                    if (result != null) return result;
                }
                return null;
            }
            else if (whereToFind as ScrollViewer != null)
            {
                FrameworkElement fre = (FrameworkElement)((ScrollViewer)whereToFind).Content;
                return FindNameInTree(fre, name);
            } return null;
        }

        public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                string controlName = child.GetValue(Control.NameProperty) as string;
                if (controlName == name)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByName<T>(child, name);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

    
    }
}
