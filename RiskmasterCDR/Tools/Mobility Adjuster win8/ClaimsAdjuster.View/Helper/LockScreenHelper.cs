﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace ClaimsAdjuster.View.Helper
{

    /// <summary>
    /// Helper class for lock screen related actions
    /// </summary>
    public static class LockScreenHelper
    {

        /// <summary>
        /// Request access to add the app in lock screen
        /// </summary>
        public async static void RequestAccess()
        {
            try
            {
                if (BackgroundExecutionManager.GetAccessStatus() == BackgroundAccessStatus.Unspecified)
                    await BackgroundExecutionManager.RequestAccessAsync();
            }
            catch (Exception)
            {
                // AA: You might get an exception, if Windows is not activated.
            }
        }

        /// <summary>
        /// Removes the lock screen access of the application
        /// </summary>
        public static void RemoveAccess()
        {
            BackgroundExecutionManager.RemoveAccess();
        }
    }
}
