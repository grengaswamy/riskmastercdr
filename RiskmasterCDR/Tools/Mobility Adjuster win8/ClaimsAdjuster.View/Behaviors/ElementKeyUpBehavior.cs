﻿using ClaimsAdjuster.Behaviors;
using ClaimsAdjuster.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace ClaimsAdjuster.View.Behaviors
{
    public class ElementKeyUpBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            AssociatedObject.KeyUp += AssociatedObject_KeyUp;
        }

        private void AssociatedObject_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key != Windows.System.VirtualKey.Enter)
                return;

            Control tb = sender as TextBox;

            if (tb == null)
            {
                tb = sender as PasswordBox; // Check for password box
                if (tb == null)
                {
                    tb = sender as CheckBox;
                    if (tb == null)
                        return;
                }
            }
            
            var datacontext = tb.DataContext;
            if (datacontext == null)
                return;

            // Change the focus, so that BindableValidationBehavior will be fired.
            MainViewModel.Instance.CurrentFrame.Focus(FocusState.Keyboard);

            string typeName = datacontext.GetType().Name;

            switch (typeName)
            {
                case "Authentication":
                    var authcontext = datacontext as Authentication;
                    if (authcontext.IsSigningIn)
                        return;
                    authcontext.ValidateLoginCommand.Execute(null);
                    break;
                case "TaskViewModel":
                    var taskcontext = datacontext as TaskViewModel;
                    if (taskcontext.IsCreatingTask)
                        return;
                    taskcontext.AddTaskCommand.Execute(null);
                    break;
                case "NoteViewModel":
                    var notecontext = datacontext as NoteViewModel;
                    notecontext.SaveCurrentNote.Execute(null);
                    break;
            }

            var parent = tb.Parent;
        }

        protected override void OnDetached()
        {
            AssociatedObject.KeyUp -= AssociatedObject_KeyUp;
        }
    }
}
