﻿using IdentityMine.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class ContactCardControl : LayoutAwareUserControl
    {
        public ContactCardControl()
        {
            this.InitializeComponent();
        }
    }
}
