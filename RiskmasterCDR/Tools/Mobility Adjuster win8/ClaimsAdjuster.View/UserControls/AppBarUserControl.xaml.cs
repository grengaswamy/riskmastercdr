﻿using ClaimsAdjuster.ViewModel;
using IdentityMine.Controls;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class AppBarUserControl : LayoutAwareUserControl
    {
        #region Fields

        private double windowSize;

        #endregion

        #region Constructors

        public AppBarUserControl()
        {
            this.InitializeComponent();
           
            SetSnapState();
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            SetSnapState();
        }
      
        private bool SnapState
        {
            get;
            set;
        }

        private void SetSnapState()
        {
            
            
            if (MainViewModel.Instance.CurrentView== ViewState.Snapped )
            {
                VisualStateManager.GoToState(this, "Snapped", false);
            }
                
            else
                VisualStateManager.GoToState(this, "Normal", false);
        }

        #endregion

        #region Events

        private void AppBarUserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetSnapState();
          
        }

        #endregion

    }
}
