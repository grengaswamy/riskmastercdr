﻿using IdentityMine.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class TaskClaimAppBarFooterControl : LayoutAwareUserControl
    {
        #region Fields

        private bool showCloseTask;
        private bool showRefresh;

        #endregion

        #region Properties

        public bool ShowCloseTask
        {
            get
            {
                return showCloseTask;
            }
            set
            {
                showCloseTask = value;
                CloseTaskAppBarButton.Visibility = showCloseTask ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
                RefreshAppBarButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        public bool ShowRefresh
        {
            get
            {
                return showRefresh;
            }
            set
            {
                showRefresh = value;
                RefreshAppBarButton.Visibility = showRefresh ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
                CloseTaskAppBarButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        #endregion

        public TaskClaimAppBarFooterControl()
        {
            this.InitializeComponent();
        }
    }
}
