﻿using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Core;
using System;
using ClaimsAdjuster.ViewModel;
namespace ClaimsAdjuster.View.UserControls
{
    public class AdaptableGridView : GridView 
    {
        // default itemWidth 
        private const double itemWidth = 100.00;
        private const double itemHeight = 106.00;
        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }
        public double ItemHeight
        {
            get { return (double)GetValue(ItemHeightProperty); }
            set { SetValue(ItemHeightProperty, value); }
        }
        public static readonly DependencyProperty ItemWidthProperty =
            DependencyProperty.Register("ItemWidth", typeof(double), typeof(AdaptableGridView), new PropertyMetadata(itemWidth));
        public static readonly DependencyProperty ItemHeightProperty =
        DependencyProperty.Register("ItemHeight", typeof(double), typeof(AdaptableGridView), new PropertyMetadata(itemHeight));

        // default max number of rows or columns 
        private const int maxRowsOrColumns = 3;
        public int MaxRowsOrColumns
        {
            get { return (int)GetValue(MaxRowColProperty); }
            set { SetValue(MaxRowColProperty, value); }
        }
        public Orientation Orientation
        {
            get;
            set;
        }
        
        public static readonly DependencyProperty MaxRowColProperty =
            DependencyProperty.Register("MaxRowsOrColumns", typeof(int), typeof(AdaptableGridView), new PropertyMetadata(maxRowsOrColumns));

        private void SetOrientation()
        {
            if (MainViewModel.Instance.CurrentView == ViewState.FullScreenPortrait)
                this.Orientation = Orientation.Horizontal;
            else
                this.Orientation = Orientation.Vertical;
        }

        public AdaptableGridView()
        {
            SetOrientation();
            this.SizeChanged += MyGridViewSizeChanged;
        }


        private void MyGridViewSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetOrientation();
            // Calculate the proper max rows or columns based on new size  
            if(this.Orientation== Orientation.Vertical)
                this.MaxRowsOrColumns = this.ItemWidth > 0 ? Convert.ToInt32(Math.Floor(e.NewSize.Width / this.ItemWidth)) : maxRowsOrColumns;
            else
                this.MaxRowsOrColumns = this.ItemHeight > 0 ? Convert.ToInt32(Math.Floor(e.NewSize.Height / this.ItemHeight)) : maxRowsOrColumns;

        }
    } 
 
}
