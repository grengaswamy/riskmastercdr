﻿using ClaimsAdjuster.ViewModel;
using System.Windows.Input;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class SearchBoxControl : UserControl
    {

        private bool hasFocus;
        private ApplicationViewOrientation orientation;
        public SearchBoxControl()
        {
            this.InitializeComponent();
            this.Loaded += SearchBoxControl_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        public bool ShowMinimizedMode
        {
            get { return (bool)GetValue(ShowMinimizedModeProperty); }
            set { SetValue(ShowMinimizedModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowMinimizedMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowMinimizedModeProperty =
            DependencyProperty.Register("ShowMinimizedMode", typeof(bool), typeof(SearchBoxControl), new PropertyMetadata(true));


        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(SearchBoxControl), new PropertyMetadata(null));


        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(SearchBoxControl), new PropertyMetadata(null));



        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            DetermineState();
        }

        void SearchBoxControl_Loaded(object sender, RoutedEventArgs e)
        {
            MainViewModel.Instance.CurrentFrame.Navigated += CurrentFrame_Navigated;
            DetermineState();
        }

        private void CurrentFrame_Navigated(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            //search term clearing code
            if (!string.IsNullOrWhiteSpace(MainViewModel.Instance.SearchTerm))
                MainViewModel.Instance.SearchTerm = string.Empty;
            if (string.IsNullOrWhiteSpace(SearchControl.QueryText))
                SearchControl.PlaceholderText = MainViewModel.Instance.GetStringResource("SearchPlaceholderText");
            else
                SearchControl.PlaceholderText = string.Empty;
        }

        void DetermineState()
        {
            if (ShowMinimizedMode)
            {
                orientation = ApplicationView.GetForCurrentView().Orientation;


                if (orientation == ApplicationViewOrientation.Portrait || !ApplicationView.GetForCurrentView().IsFullScreen)
                {
                    if (!hasFocus)
                    {
                        VisualStateManager.GoToState(this, "Portrait", false);
                        MainViewModel.Instance.IsSearchOpen = false;
                    }
                    else if (hasFocus)
                        MainViewModel.Instance.IsSearchOpen = true;
                }
                else if (orientation == ApplicationViewOrientation.Landscape)
                {
                    VisualStateManager.GoToState(this, "Normal", false);
                    (this.Resources["SearchBoxOpened"] as Storyboard).Begin();
                    MainViewModel.Instance.IsSearchOpen = false;
                }

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (ShowMinimizedMode)
            {
                (this.Resources["SearchBoxOpened"] as Storyboard).Begin();
                SearchControl.Focus(FocusState.Programmatic);
                MainViewModel.Instance.IsSearchOpen = true;
            }

        }
        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var searchBox = sender as SearchBox;
            if (string.IsNullOrWhiteSpace(searchBox.QueryText) && string.IsNullOrWhiteSpace(searchBox.PlaceholderText))
                searchBox.PlaceholderText = MainViewModel.Instance.GetStringResource("SearchPlaceholderText");

            if (ShowMinimizedMode)
            {
                if (orientation == ApplicationViewOrientation.Portrait || !ApplicationView.GetForCurrentView().IsFullScreen)
                {
                    (this.Resources["SearchBoxClosed"] as Storyboard).Begin();
                    MainViewModel.Instance.IsSearchOpen = false;
                }
                hasFocus = false;
            }
        }

        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            if (string.IsNullOrWhiteSpace(args.QueryText) && !string.IsNullOrWhiteSpace(sender.PlaceholderText))
                sender.PlaceholderText = string.Empty;
            else if (!string.IsNullOrWhiteSpace(args.QueryText))
                sender.PlaceholderText = MainViewModel.Instance.GetStringResource("SearchPlaceholderText");

            Command.Execute(args);
            if (ShowMinimizedMode)
            {
                if (orientation == ApplicationViewOrientation.Portrait || !ApplicationView.GetForCurrentView().IsFullScreen)
                {
                    (this.Resources["SearchBoxClosed"] as Storyboard).Begin();
                    MainViewModel.Instance.IsSearchOpen = false;
                }
            }
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var searchBox = sender as SearchBox;
            if (string.IsNullOrWhiteSpace(searchBox.QueryText) && !string.IsNullOrWhiteSpace(searchBox.PlaceholderText))
                searchBox.PlaceholderText = string.Empty;

            if (ShowMinimizedMode)
            {
                hasFocus = true;
            }
        }
    }
}
