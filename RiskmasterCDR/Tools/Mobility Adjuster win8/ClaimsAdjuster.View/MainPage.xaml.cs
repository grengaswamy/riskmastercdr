﻿using ClaimsAdjuster.ViewModel;
using ClaimsAdjuster.ViewModel.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClaimsAdjuster.ViewModel.Services;
using CSC.ServiceFactory;
using System.Diagnostics;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {


        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }

        async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Audio_Click(object sender, RoutedEventArgs e)
        {

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];



                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }


            if (!rootFrame.Navigate(typeof(ClaimsAdjuster.View.Pages.AudioCapture), false))
            {
                throw new Exception("Failed to create initial page");
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        private void Photo_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];



                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }


            if (!rootFrame.Navigate(typeof(ClaimsAdjuster.View.Pages.PhotoCapturePage), false))
            {
                throw new Exception("Failed to create initial page");
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        private void Video_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];



                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }


            if (!rootFrame.Navigate(typeof(ClaimsAdjuster.View.Pages.PhotoCapturePage), true))
            {
                throw new Exception("Failed to create initial page");
            }

            // Ensure the current window is active
            Window.Current.Activate();

        }

        private async void UploadOne_Click(object sender, RoutedEventArgs e)
        {
            string xmlRequest = "<Upload> <Save> <Type>updatediary</Type> <Message> <Authorization>{0}</Authorization> <Call> <Function>WPAAdaptor.SaveDiary</Function> <MultipleCalls>true</MultipleCalls> </Call> <Document> <SaveDiary> <DiaryUpload>true</DiaryUpload> <MobAdjuster>true</MobAdjuster> <DiaryAction></DiaryAction> <EntryId>4259908</EntryId> <TaskSubject>Task_Test attaching image test</TaskSubject> <AssigningUser></AssigningUser> <StatusOpen>false</StatusOpen> <DueDate></DueDate> <CompleteTime></CompleteTime> <EstimateTime>0</EstimateTime> <Priority>1</Priority> <AutoConfirm>false</AutoConfirm> <NotifyFlag>false</NotifyFlag> <TaskNotes></TaskNotes> <ActivityString></ActivityString> <Action>Edit</Action> </SaveDiary> <Documents> <CLAIM_NUMBER>GCGL1990000001</CLAIM_NUMBER> <DocumentUpload filename=\"UploadTestIM1.png\" title=\"test_doc\" subject=\"Test data\" notes=\"the note be checked is done here in mobile as well\" type_id=\"366\" type=\"Bitmap\" content=\"{1}\"/> </Documents> </Document> </Message> </Save> </Upload>";
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            IStorageItem file = await folder.TryGetItemAsync("SmallLogo.scale-100.png");
            Stream fs = await (file as StorageFile).OpenStreamForReadAsync();
            BinaryReader br = new BinaryReader(fs);
            byte[] bytes = br.ReadBytes((int)fs.Length);
            string imageString = Convert.ToBase64String(bytes);
            string request = string.Format(xmlRequest, MainViewModel.Instance.AuthenticationToken, imageString);
            IMobileUploadService client = MobileUploadService.ServiceInstance;
            string response = await client.UploadRequestAsync(request);
            Debug.WriteLine(response);
        }

        private async void UploadMultipleSuccess_Click(object sender, RoutedEventArgs e)
        {
            string xmlRequest = "<Upload> <Save> <Type>updatediary</Type> <Message> <Authorization>{0}</Authorization> <Call> <Function>WPAAdaptor.SaveDiary</Function> <MultipleCalls>true</MultipleCalls> </Call> <Document> <SaveDiary> <DiaryUpload>true</DiaryUpload> <MobAdjuster>true</MobAdjuster> <DiaryAction></DiaryAction> <EntryId>4259909</EntryId> <TaskSubject>Task_Test attaching image test</TaskSubject> <AssigningUser></AssigningUser> <StatusOpen>false</StatusOpen> <DueDate></DueDate> <CompleteTime></CompleteTime> <EstimateTime>0</EstimateTime> <Priority>1</Priority> <AutoConfirm>false</AutoConfirm> <NotifyFlag>false</NotifyFlag> <TaskNotes></TaskNotes> <ActivityString></ActivityString> <Action>Edit</Action> </SaveDiary> <Documents> <CLAIM_NUMBER>GCGL1990000001</CLAIM_NUMBER> <DocumentUpload filename=\"UploadMultipleSuccessIM1.png\" title=\"test_doc\" subject=\"Test data\" notes=\"the note be checked is done here in mobile as well\" type_id=\"366\" type=\"Bitmap\" content=\"{1}\"/> </Documents> </Document> </Message> </Save> <Save> <Type>updatediary</Type> <Message> <Authorization>{0}</Authorization> <Call> <Function>WPAAdaptor.SaveDiary</Function> <MultipleCalls>true</MultipleCalls> </Call> <Document> <SaveDiary> <DiaryUpload>true</DiaryUpload> <MobAdjuster>true</MobAdjuster> <DiaryAction></DiaryAction> <EntryId>4259910</EntryId> <TaskSubject>Task_Test attaching image test</TaskSubject> <AssigningUser></AssigningUser> <StatusOpen>false</StatusOpen> <DueDate></DueDate> <CompleteTime></CompleteTime> <EstimateTime>0</EstimateTime> <Priority>1</Priority> <AutoConfirm>false</AutoConfirm> <NotifyFlag>false</NotifyFlag> <TaskNotes></TaskNotes> <ActivityString></ActivityString> <Action>Edit</Action> </SaveDiary> <Documents> <CLAIM_NUMBER>GCGL1990000001</CLAIM_NUMBER> <DocumentUpload filename=\"UploadMultipleSuccessIM2.png\" title=\"test_doc\" subject=\"Test data\" notes=\"the note be checked is done here in mobile as well\" type_id=\"366\" type=\"Bitmap\" content=\"{1}\"/> </Documents> </Document> </Message> </Save> </Upload>";
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            IStorageItem file = await folder.TryGetItemAsync("SmallLogo.scale-100.png");
            Stream fs = await (file as StorageFile).OpenStreamForReadAsync();
            BinaryReader br = new BinaryReader(fs);
            byte[] bytes = br.ReadBytes((int)fs.Length);
            string imageString = Convert.ToBase64String(bytes);
            string request = string.Format(xmlRequest, MainViewModel.Instance.AuthenticationToken, imageString);
            IMobileUploadService client = MobileUploadService.ServiceInstance;
            string response = await client.UploadRequestAsync(request);
            Debug.WriteLine(response);
        }

        private async void UploadMultipleOneError_Click(object sender, RoutedEventArgs e)
        {
            string xmlRequest = "<Upload> <Save> <Type>updatediary</Type> <Message> <Authorization>{0}</Authorization> <Call> <Function>WPAAdaptor.SaveDiary</Function> <MultipleCalls>true</MultipleCalls> </Call> <Document> <SaveDiary> <DiaryUpload>true</DiaryUpload> <MobAdjuster>true</MobAdjuster> <DiaryAction></DiaryAction> <EntryId>4259914</EntryId> <TaskSubject>Task_Test attaching image test</TaskSubject> <AssigningUser></AssigningUser> <StatusOpen>false</StatusOpen> <DueDate></DueDate> <CompleteTime></CompleteTime> <EstimateTime>0</EstimateTime> <Priority>1</Priority> <AutoConfirm>false</AutoConfirm> <NotifyFlag>false</NotifyFlag> <TaskNotes></TaskNotes> <ActivityString></ActivityString> <Action>Edit</Action> </SaveDiary> <Documents> <CLAIM_NUMBER>GCGL1990000001</CLAIM_NUMBER> <DocumentUpload filename=\"UploadMultipleOneErrorIM1.png\" title=\"test_doc\" subject=\"Test data\" notes=\"the note be checked is done here in mobile as well\" type_id=\"366\" type=\"Bit\" content=\"{1}\"/> </Documents> </Document> </Message> </Save> <Save> <Type>updatediary</Type> <Message> <Authorization>{0}</Authorization> <Call> <Function>WPAAdaptor.SaveDiary</Function> <MultipleCalls>true</MultipleCalls> </Call> <Document> <SaveDiary> <DiaryUpload>true</DiaryUpload> <MobAdjuster>true</MobAdjuster> <DiaryAction></DiaryAction> <TaskSubject>Task_Test attaching image test</TaskSubject> <AssigningUser></AssigningUser> <StatusOpen>false</StatusOpen> <DueDate></DueDate> <CompleteTime></CompleteTime> <EstimateTime>0</EstimateTime> <Priority>1</Priority> <AutoConfirm>false</AutoConfirm> <NotifyFlag>false</NotifyFlag> <TaskNotes></TaskNotes> <ActivityString></ActivityString> <Action>Edit</Action> </SaveDiary> <Documents> <CLAIM_NUMBER>GCGL1990000001</CLAIM_NUMBER> <DocumentUpload filename=\"UploadMultipleOneErrorIM2\" title=\"test_doc\" subject=\"Test data\" notes=\"the note be checked is done here in mobile as well\" type_id=\"366\" type=\"Bitmap\" content=\"{1}\"/> </Documents> </Document> </Message> </Save> </Upload>";
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            IStorageItem file = await folder.TryGetItemAsync("SmallLogo.scale-100.png");
            Stream fs = await (file as StorageFile).OpenStreamForReadAsync();
            BinaryReader br = new BinaryReader(fs);
            byte[] bytes = br.ReadBytes((int)fs.Length);
            string imageString = Convert.ToBase64String(bytes);
            string request = string.Format(xmlRequest, MainViewModel.Instance.AuthenticationToken, imageString);
            IMobileUploadService client = MobileUploadService.ServiceInstance;
            string response = await client.UploadRequestAsync(request);
            Debug.WriteLine(response);
        }

    }
}
