﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.StartScreen;
using ClaimsAdjuster.ViewModel.Helpers;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TaskDetailsPage : Page
    {
        #region Fields

        TaskViewModel dataContext;

        #endregion

        #region Constructors

        public TaskDetailsPage()
        {
            this.InitializeComponent();
            this.Loaded += TaskDetailsPage_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;

        }

        #endregion

        #region Methods
        void SwitchAppBarTemplates()
        {
            TopAppBar.ContentTemplate = (DataTemplate)(MainViewModel.Instance.CurrentView == ViewState.Snapped ? App.Current.Resources["TaskDetailsSnapAppBarTemplate"] : App.Current.Resources["TaskDetailsAppBarTemplate"]);
        }


        void BottomAppBar_Opened(object sender, object e)
        {
            dataContext = this.DataContext as TaskViewModel;
            ToggleAppBarButton(!SecondaryTile.Exists(dataContext.CscTask.ServerEntryId.ToString()));
        }
        private void ToggleAppBarButton(bool canPin)
        {
            MainViewModel.Instance.CanPin = canPin;

        }


        #endregion

        #region Events

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.TopAppBar.Opened += BottomAppBar_Opened;
            base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            this.TopAppBar.IsOpen = true;
            dataContext = this.DataContext as TaskViewModel;
            if (e.NavigationMode == NavigationMode.Back && dataContext != null)
            {
                string id = string.Format("{0}{1}", Constants.Local, dataContext.CscTask.Id);
                string localServiceId = string.Format(ServiceRequestIds.TaskDetailsRequest, id);
                bool hasSamePageOfDBId = MainViewModel.Instance.SamePageExists(localServiceId);
                if(hasSamePageOfDBId)
                {
                    TaskDetailsRequest taskDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(localServiceId) as TaskDetailsRequest;
                    if (taskDetailsRequest != null)
                    {
                        MainViewModel.Instance.ServiceFactory.RemoveService(localServiceId);
                        taskDetailsRequest.Result = dataContext;
                        MainViewModel.Instance.ServiceFactory.AddService(taskDetailsRequest);
                    }
                }
                if (!hasSamePageOfDBId && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                    dataContext.ClearCollection();
            }

            this.TopAppBar.Opened -= BottomAppBar_Opened;

            base.OnNavigatedFrom(e);
        }

        void TaskDetailsPage_Loaded(object sender, RoutedEventArgs e)
        {
            SwitchAppBarTemplates();
            dataContext = this.DataContext as TaskViewModel;        
            TopAppBar.IsOpen = true;
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            SwitchAppBarTemplates();
        }

        #endregion

    }
}
