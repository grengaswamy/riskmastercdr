﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EventsPage : Page
    {
        #region Fields

        EventCollection dataContext;

        #endregion

        #region Constructors

        public EventsPage()
        {
            this.InitializeComponent();
        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            dataContext = this.DataContext as EventCollection;
            if (e.NavigationMode == NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                dataContext.UnLoad();
            base.OnNavigatedFrom(e);
        }

        #endregion
    }
}
