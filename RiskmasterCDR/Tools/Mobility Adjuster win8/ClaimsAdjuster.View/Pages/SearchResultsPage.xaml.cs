﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchResultsPage : Page
    {

        #region Fields

        SearchViewModel dataContext;

        #endregion

        #region Constructor
        public SearchResultsPage()
        {
            this.InitializeComponent();
            this.Loaded += SearchResultsPage_Loaded;
        }

        #endregion

        #region Methods

        protected override void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            dataContext = this.DataContext as SearchViewModel;
            if (e.NavigationMode == Windows.UI.Xaml.Navigation.NavigationMode.Back && dataContext != null && MainViewModel.Instance.ClosedTaskList.Count > 0)
                dataContext.RemoveClosedTasks();

            if (dataContext != null)
                MainViewModel.Instance.SearchTerm = dataContext.SearchTerm;

            MainViewModel.Instance.CurrentFrame.Focus(Windows.UI.Xaml.FocusState.Programmatic);
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            //MainViewModel.Instance.LastSearchedText = string.Empty;
            base.OnNavigatedFrom(e);
        }

        #endregion

        #region EventHandlers

        void SearchResultsPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            dataContext = this.DataContext as SearchViewModel;
        }

        protected override void OnNavigatingFrom(Windows.UI.Xaml.Navigation.NavigatingCancelEventArgs e)
        {
            dataContext = this.DataContext as SearchViewModel;
            if (e.NavigationMode == Windows.UI.Xaml.Navigation.NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                dataContext.ClearCollections();

            if (e.NavigationMode == Windows.UI.Xaml.Navigation.NavigationMode.New && !string.IsNullOrWhiteSpace(MainViewModel.Instance.SearchTerm))
                MainViewModel.Instance.SearchTerm = string.Empty;

            base.OnNavigatingFrom(e);
        }

        #endregion

    }
}
