﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddNotePage : Page
    {
        public AddNotePage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            var datacontext = this.DataContext as NoteViewModel;
            datacontext.ResetValidation();

            base.OnNavigatedFrom(e);
        }

    }
}
