﻿using ClaimsAdjuster.ViewModel;
using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using Windows.UI.ApplicationSettings;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace ClaimsAdjuster.View.Pages
{

    public sealed partial class DSNSettingsFlyout : SettingsFlyout
    {

        #region Fields

        private EdgeGesture edgeGesture;

        #endregion

        #region Constructor

        public DSNSettingsFlyout()
        {
            this.InitializeComponent();
            this.Loaded += DSNSettingsFlyout_Loaded;
            this.Unloaded += DSNSettingsFlyout_Unloaded;
            this.RightTapped += DSNSettingsFlyout_RightTapped;
            edgeGesture = EdgeGesture.GetForCurrentView();
            edgeGesture.Completed += edgeGesture_Completed;
        }

        void edgeGesture_Completed(EdgeGesture sender, EdgeGestureEventArgs args)
        {
            if (args.Kind == EdgeGestureKind.Touch)
                this.Hide();
        }

        void DSNSettingsFlyout_RightTapped(object sender, Windows.UI.Xaml.Input.RightTappedRoutedEventArgs e)
        {
            e.Handled = true;
        }

        void DSNSettingsFlyout_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(MainViewModel.Instance.AuthenticationVM.SignInErrorText))
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = string.Empty;
        }

        #endregion

        #region EventHandlers

        private void DSNSettingsFlyout_Loaded(object sender, RoutedEventArgs e)
        {
            MainViewModel.Instance.AuthenticationVM.SignInErrorText = string.Empty;
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Cancel_ButtonClick(object sender, RoutedEventArgs e)
        {
            this.Hide();
            SettingsPane.Show();
        }

        #endregion

        #region Methods

        private async void ChangeWebServerUrl()
        {
            bool closeFlyout = false;
            try
            {
                closeFlyout = await MainViewModel.Instance.AuthenticationVM.ChangeWebServerUrl(this.DataContext as User);

            }
            catch
            {
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedText");
                MainViewModel.Instance.AuthenticationVM.IsSigningIn = false;
            }
            if (closeFlyout)
                this.Hide();
        }

        private async void InitiateLogin()
        {
            MainViewModel.Instance.AuthenticationVM.SignInErrorText = string.Empty;
            ChangeWebServerUrl();
        }

        private async void Login()
        {
            if (!MainViewModel.Instance.IsInternetAvailable)
            {
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedNoInternetText");
                return;
            }
            User user = this.DataContext as User;
            if (string.IsNullOrWhiteSpace(user.DSNName) || string.IsNullOrWhiteSpace(user.WebServerUrl) || string.IsNullOrWhiteSpace(user.Password))
            {
                MainViewModel.Instance.AuthenticationVM.SignInErrorText = MainViewModel.Instance.GetStringResource("LoginFailedText");
                return;
            }

            bool hasUploads = await MainViewModel.Instance.AuthenticationVM.CheckPendingUploadsAsync();
            if (hasUploads)
                MessageHelper.Show(MainViewModel.Instance.GetStringResource("LogoutPendingVideosMsg"),
                                   MainViewModel.Instance.GetStringResource("Settings_LogoutText"),
                                   MainViewModel.Instance.GetStringResource("CancelText"),
                                   InitiateLogin);
            else
                InitiateLogin();
        }

        #endregion

        private void PasswordBox_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
                Login();
        }
    }
}
