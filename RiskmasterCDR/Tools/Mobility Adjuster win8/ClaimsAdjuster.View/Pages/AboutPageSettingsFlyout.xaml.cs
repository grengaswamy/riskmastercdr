﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace ClaimsAdjuster.View.Pages
{
    public sealed partial class AboutPageSettingsFlyout : SettingsFlyout
    {
        #region Fields

        private EdgeGesture edgeGesture;

        #endregion

        public AboutPageSettingsFlyout()
        {
            this.InitializeComponent();
            this.RightTapped += AboutPageSettingsFlyout_RightTapped;
            edgeGesture = EdgeGesture.GetForCurrentView();
            edgeGesture.Completed += edgeGesture_Completed;
        }

        private void edgeGesture_Completed(EdgeGesture sender, EdgeGestureEventArgs args)
        {
            if (args.Kind == EdgeGestureKind.Touch)
                this.Hide();
        }

        void AboutPageSettingsFlyout_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
