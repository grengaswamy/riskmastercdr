﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.StartScreen;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EventDetailsPage : Page
    {
        #region Fields

        EventViewModel dataContext;

        #endregion

        #region Constructor
        public EventDetailsPage()
        {
            this.InitializeComponent();
           
        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            dataContext = this.DataContext as EventViewModel;
            if (e.NavigationMode == NavigationMode.Back && this.DataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                dataContext.ClearCollection();
            
            this.TopAppBar.Opened -= BottomAppBar_Opened;
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.TopAppBar.Opened += BottomAppBar_Opened;
            base.OnNavigatedTo(e);
        }

        void BottomAppBar_Opened(object sender, object e)
        {
            dataContext = this.DataContext as EventViewModel;
            ToggleAppBarButton(!SecondaryTile.Exists(dataContext.CscEvent.EventNumber));
        }
        private void ToggleAppBarButton(bool canPin)
        {
            MainViewModel.Instance.CanPin = canPin;

        }



        #endregion

    }
}
