﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class PaymentHistoryPage : Page
    {
        #region Fields

        PaymentCollection dataContext;

        #endregion

        #region Constructor
        public PaymentHistoryPage()
        {
            this.InitializeComponent();
            this.Loaded += PaymentHistoryPage_Loaded;
        }

        #endregion

        #region EventHandlers

        void PaymentHistoryPage_Loaded(object sender, RoutedEventArgs e)
        {
            dataContext = this.DataContext as PaymentCollection;
        }

        protected override void OnNavigatingFrom(Windows.UI.Xaml.Navigation.NavigatingCancelEventArgs e)
        {
            dataContext = this.DataContext as PaymentCollection;
            if (e.NavigationMode == Windows.UI.Xaml.Navigation.NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                dataContext.UnLoad();
        }

        #endregion


    }
}
