﻿using ClaimsAdjuster.View.Helper;
using ClaimsAdjuster.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddTask : Page
    {
        #region Constructors

        public AddTask()
        {
            this.InitializeComponent();
        }
        
        #endregion

    }
}
