﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml;
using ClaimsAdjuster.ViewModel.Helpers;
using Windows.UI.Xaml.Media;
using ClaimsAdjuster.View.Helper;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TasksPage : Page
    {
        #region Fields

        TaskCollection dataContext;


        #endregion

        #region Constructors

        public TasksPage()
        {
            this.InitializeComponent();
            this.Loaded += TasksPage_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;
            //Adjust back button margins per design
            mainContent.BackButtonVerticalAlignment = VerticalAlignment.Top;
            mainContent.BackButtonMargin = (Thickness)App.Current.Resources["HeaderBackButtonMargin"];

        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            UpdateGridViewMargins();
        }

        void TasksPage_Loaded(object sender, RoutedEventArgs e)
        {
            //To Adjust gridView padding and margins per ViewState; update Gridview style appropriately
            UpdateGridViewMargins();
        }

        #endregion

        #region Methods()

        void UpdateGridViewMargins()
        {
            if (MainViewModel.Instance.CurrentView == ViewState.FullScreenLandscape || MainViewModel.Instance.CurrentView == ViewState.Filled)
            {
                tasksGridView.Style = this.Resources["DefaultLandscapeGridViewStyle"] as Style;
            }
            else
            {
                tasksGridView.Style = this.Resources["DefaultPortraitGridViewStyle"] as Style;
            }
        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (this.DataContext != null)
            {
                dataContext = this.DataContext as TaskCollection;
                if (e.NavigationMode == NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                    dataContext.UnLoad();
                dataContext.PropertyChanged -= dataContext_PropertyChanged;
            }
            base.OnNavigatingFrom(e);
        }

        void dataContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Count")
            {
                var gridViewScrollViewer = GenericHelper.FindVisualChildByName<ScrollViewer>(tasksGridView, "ScrollViewer");
                if (gridViewScrollViewer != null)
                    gridViewScrollViewer.ScrollToHorizontalOffset(0);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            dataContext = this.DataContext as TaskCollection;
            if (dataContext != null)
                dataContext.PropertyChanged += dataContext_PropertyChanged;
            base.OnNavigatedTo(e);
        }

        void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender != null)
                (sender as ScrollViewer).ChangeView(0, null, null);
        }

        #endregion

    }
}
