﻿using ClaimsAdjuster.View.Helper;
using ClaimsAdjuster.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace ClaimsAdjuster.View.Pages
{
    public sealed partial class ConfirmLogoutFlyout : SettingsFlyout
    {
        public ConfirmLogoutFlyout()
        {
            this.InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            SettingsPane.Show();
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsHelper.Instance.Unload();
            MainViewModel.Instance.AuthenticationVM.ClearSignInDetails();
        }
    }
}
