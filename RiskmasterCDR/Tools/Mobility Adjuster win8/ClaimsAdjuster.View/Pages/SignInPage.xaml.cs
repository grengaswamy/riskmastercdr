﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Popups;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ClaimsAdjuster.ViewModel.Repository;
using ClaimsAdjuster.View.Helper;
using ClaimsAdjuster.ViewModel.Helpers;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;
using IdentityMine.ViewModel;
using Windows.UI.Core;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SignInPage : Page
    {

        #region Fields

        TextBox dataserverTextBox, webServerTextBox, userNameBox;
        PasswordBox passowrdBox;

        #endregion

        #region Constructors

        public SignInPage()
        {
            this.InitializeComponent();
            this.Loaded += SignInPage_Loaded;
        }

        #endregion

        #region Methods

        protected override void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            var datacontext = this.DataContext as Authentication;
            if (datacontext != null && MainViewModel.Instance.CurrentUserInfo!=null)
                datacontext.ResetValidation();

            if (MainViewModel.Instance.CurrentFrame.BackStack.Count > 0)
                MainViewModel.Instance.CurrentFrame.BackStack.RemoveAt(0);
            (MainViewModel.Instance.CurrentFrame.Content as Page).Focus(FocusState.Programmatic);

            if (e.NavigationMode == NavigationMode.New || e.NavigationMode == NavigationMode.Back)
                SettingsHelper.Instance.Load();
        }

        #endregion

        #region Events

        private void SignInPage_Loaded(object sender, RoutedEventArgs e)
        {
            dataserverTextBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "DSNTextBox") as TextBox;
            webServerTextBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "WebServerUrlTextBox") as TextBox;
            userNameBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "UserNameTextBox") as TextBox;
            passowrdBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "PasswordTextBox") as PasswordBox;


            if (!MainViewModel.Instance.IsInternetAvailable)
                ShowMessageBox();
            this.DataContext = MainViewModel.Instance.AuthenticationVM;
            this.Focus(Windows.UI.Xaml.FocusState.Programmatic);

            LockScreenHelper.RequestAccess();
        }

        private async void ShowMessageBox(int attempt = 0)
        {
            if (!MainViewModel.Instance.IsInternetAvailable)
            {
                MainVMBase.InstanceBase.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                   {
                       try
                       {
                           if (attempt > 0)
                               await Task.Delay(TimeSpan.FromSeconds(2));

                           MessageDialog md = new MessageDialog(MainViewModel.Instance.GetStringResource("InternetNotAvailableText"));
                           md.Commands.Add(new UICommand(MainViewModel.Instance.GetStringResource("OKText"), null));

                           await md.ShowAsync();
                           attempt = 0;
                       }
                       catch (UnauthorizedAccessException ex)
                       {
                           ShowMessageBox(attempt++);
                       }
                   });
            }


        }
        #endregion

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null)
                textBox.PlaceholderText = string.Empty;
            else
                (sender as PasswordBox).PlaceholderText = string.Empty;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (dataserverTextBox == null)
                dataserverTextBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "DSNTextBox") as TextBox;

            if (webServerTextBox == null)
                webServerTextBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "WebServerUrlTextBox") as TextBox;

            if (userNameBox == null)
                userNameBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "UserNameTextBox") as TextBox;

            if (passowrdBox == null)
                passowrdBox = GenericHelper.FindNameInTree(ScrollableContentControl.Content as FrameworkElement, "PasswordTextBox") as PasswordBox;

            if (dataserverTextBox.PlaceholderText == string.Empty)
                dataserverTextBox.PlaceholderText = MainViewModel.Instance.GetStringResource("DatabaseExampleName");

            if (webServerTextBox.PlaceholderText == string.Empty)
                webServerTextBox.PlaceholderText = MainViewModel.Instance.GetStringResource("ServerExampleName");

            if (userNameBox.PlaceholderText == string.Empty)
                userNameBox.PlaceholderText = MainViewModel.Instance.GetStringResource("EnterUserNameText");

            if (passowrdBox.PlaceholderText == string.Empty)
                passowrdBox.PlaceholderText = MainViewModel.Instance.GetStringResource("EnterPasswordText");
        }

    }
}
