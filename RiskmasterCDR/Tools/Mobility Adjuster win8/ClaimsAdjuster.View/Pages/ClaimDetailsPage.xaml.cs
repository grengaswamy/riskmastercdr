﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.StartScreen;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ClaimDetailsPage : Page
    {

        #region Fields

        ClaimViewModel dataContext;

        #endregion

        #region Constructor
        public ClaimDetailsPage()
        {
            this.InitializeComponent();
            this.Loaded += ClaimDetailsPage_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;
            this.TopAppBar.Opened += BottomAppBar_Opened;
        }

        #endregion

        #region Methods
        void SwitchAppBarTemplates()
        {
            TopAppBar.ContentTemplate = (DataTemplate)(MainViewModel.Instance.CurrentView == ViewState.Snapped ? App.Current.Resources["ClaimDetailsSnapAppBarTemplate"] : App.Current.Resources["ClaimDetailsAppBarTemplate"]);
        }

        void BottomAppBar_Opened(object sender, object e)
        {
            dataContext = this.DataContext as ClaimViewModel;
            ToggleAppBarButton(!SecondaryTile.Exists(dataContext.CscClaim.ClaimNumber));
        }
        private void ToggleAppBarButton(bool canPin)
        {
            MainViewModel.Instance.CanPin = canPin;

        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            dataContext = this.DataContext as ClaimViewModel;
            if (e.NavigationMode == NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                dataContext.ClearCollection();
           this.TopAppBar.Opened -= BottomAppBar_Opened;
            base.OnNavigatingFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.TopAppBar.Opened += BottomAppBar_Opened;
            if (RootElement != null && e.NavigationMode != NavigationMode.Back)
                RootElement.ScrollToPosition(0);
            base.OnNavigatedTo(e);
        }

      
        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            SwitchAppBarTemplates();
        }

        void ClaimDetailsPage_Loaded(object sender, RoutedEventArgs e)
        {
            SwitchAppBarTemplates();
            dataContext = this.DataContext as ClaimViewModel;
        }

        #endregion
    }
}
