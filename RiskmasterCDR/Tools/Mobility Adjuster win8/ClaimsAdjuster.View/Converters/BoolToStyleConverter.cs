﻿using System;
using System.Text;
using System.IO; 
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using ClaimsAdjuster.ViewModel;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

namespace ClaimsAdjuster.View.Converters
{

    public class TrimTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
           if(value is string)
           {
               var v = (string)value;
               return v.Trim();
           }

           return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {

            if (value is string)
            {
                var v = (string)value;
                return v.Trim();
            }

            return value;
        }
    }
    public class IDToVisiblityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is int)
            {
                var v = (int)value;
                if (v<=0)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }

    public class ReverseBooleanToVisiblity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is bool)
            {
                var v = (bool)value;
                if (v)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
    public class BoolToStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return string.Empty;

            return (value is bool && (bool)value) ? ("&#xE141;") : ("&#xE196;");
         
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }

    public class BoolToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return string.Empty;
           
            return (value is bool && (bool)value) ? MainViewModel.Instance.GetStringResource("PinText") : MainViewModel.Instance.GetStringResource("UnpinText");

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }

    public class CurrencyFormatter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return string.Empty;

            var cultureInfo = new System.Globalization.CultureInfo("en-US");
            return String.Format(cultureInfo, "{0:C}", value);

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
    public class DateFormatter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return string.Empty;
            DateTime date = DateTime.Parse(value.ToString());
            return date.ToString("MM/dd/yyyy");

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }

    public class BoolToArrowConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            ImageSource retImage = null;

            if (value == null)
                return string.Empty;
            int key = System.Convert.ToInt16(value);
            if (key == 1)
            {
                return retImage =  new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/DownArrow.png"));
            }
                
            else if (key == -1)
                return   retImage =  new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/UpArrow.png"));
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
