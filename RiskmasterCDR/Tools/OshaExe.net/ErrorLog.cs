using System;
using System.IO;
using Riskmaster.Common;

namespace Osha
{
    public  class ErrorLog
    {
        private const string LOGFILENAME = "OSHAEXE_error.log";
        private const string DIR_SEPERATOR_CHAR = @"\";
        public static void LogError(string sProcedure, Int32 ErrLine, Int32 ErrNum, string ErrSrc, string ErrDesc, int p_iClientId) //dvatsa-cloud(Added ClientId as parameter)
        {

            //string sFileName = String.Empty;
            //try
            //{
            //    sFileName = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //    sFileName = sFileName.Substring(0, sFileName.LastIndexOf(DIR_SEPERATOR_CHAR));
            //    if (sFileName.Substring(sFileName.Length - 1) != DIR_SEPERATOR_CHAR)
            //    {
            //        sFileName = sFileName + DIR_SEPERATOR_CHAR;
            //    }
            //    sFileName = sFileName + LOGFILENAME;

            //    File.AppendAllText(sFileName, "--------------------------------------------------------" + Environment.NewLine);
            //    File.AppendAllText(sFileName, DateTime.Now.ToString("[YYYYMMDD hh:mm:ss] ") + ErrDesc + Environment.NewLine);
            //}
            //catch { }
            Log.Write(ErrDesc, "CommonWebServiceLog", p_iClientId);//dvatsa-cloud(Added ClientId as parameter)
        }

        public static void LogError(Exception ex, int p_iClientId)//dvatsa-cloud(Added ClientId as parameter)
        {
            //string sFileName = String.Empty;
            //string sProcName =string.Empty;
            //Int32 iErrCode=0;
            //Int32 iLinenum = 0;
            //try
            //{
            //    iLinenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(":line") + 5));
            //    if(ex.InnerException!=null)
            //        LogError(sProcName, iLinenum, iErrCode, ex.Source.ToString(), ex.InnerException.StackTrace.ToString());
            //    else
            //        LogError(sProcName, iLinenum, iErrCode, ex.Source.ToString(), ex.Message);


            //}
            //catch {}
            Log.Write(ex.Message, "CommonWebServiceLog", p_iClientId);//dvatsa-cloud(Added ClientId as parameter)
        }
    }

}
