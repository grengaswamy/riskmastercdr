using System;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.ReportInterfaces;
using Riskmaster.Application.OSHALib;
using Riskmaster.Application.ZipUtil;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using System.Xml;
using Riskmaster.ExceptionTypes;
using System.Net.Mail;
using System.IO;
namespace Osha
{
    public class CWorker : IWorkerNotifications
    {
        // Scans the Job table for the reports to run
        // It also implements the Sortmaster callback
        // Interface to it can cancel the report and
        // share the database connection
        private bool m_Initialized = false;
        private bool m_Continue = false;
        private string m_WorkerName = String.Empty;
        private CJob m_CurrentJob = null;
        private string m_Mirror = String.Empty;
        private bool m_UseMirror = false;
        private const string OUTPUT_PRINT = "print";
        private const string OUTPUT_HTML = "html";
        private const string OUTPUT_XML = "xml";
        private const string OUTPUT_PDF = "pdf";
        private const string OUTPUT_CSV = "csv";
        private const string OUTPUT_XLS = "xls";
        private const string NOTIFY_NONE = "none";
        private const string NOTIFY_ONLY = "notifyonly";
        private const string NOTIFY_LINK = "notifylink";
        private const string NOTIFY_EMBED = "notifyembed";
        private const string MSG_PROGRESS = "progress";
        private const string MSG_START = "start";
        private const string MSG_COMPLETE = "complete";
        private const string MSG_ERROR = "error";
        private const string CMD_SHUTDOWN = "shutdown";
        private string m_ReportFolder = String.Empty;
        private string m_SMTPServer = String.Empty;
        private string m_ReportURL = String.Empty;
        private StorageType m_DocPathType;

        private const string DEFAULT_MAIL_SUBJECT = "Subject";
        private const string DEFAULT_MAIL_FROMEMAIL = "FromEmail";
        private const string DEFAULT_MAIL_FROMNAME = "FromName";
       // private const string DTG_REG_SUBKEY = @"Software\DTG\SORTMASTER Server";
        private string m_sConnectString;
        private int m_iClientId=0;//dvatsa-cloud

        public int ClientId
        {
            get { return m_iClientId; }
            set { m_iClientId = value; }
        }
       
        public void Init()
        {
            try
            {
                if (m_Initialized)
                    return;

                if (m_sConnectString != "")
                {
                    m_CurrentJob = null;
                    m_Initialized = true;
                    m_Continue = true;
                    m_WorkerName = ModMain.GetMachineName(m_iClientId);
                    m_WorkerName = m_WorkerName + "-" + System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].ModuleHandle.MDStreamVersion.ToString();
                    //m_WorkerName = m_WorkerName + "-" + System.Runtime.InteropServices.Marshal.GetHINSTANCE(System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0]);
                                        
                    Console.WriteLine("objWorker.Init()");
                    m_UseMirror = Convert.ToBoolean( RMConfigurationManager.GetAppSetting("UseMirror"));
                    //Console.WriteLine("objWorker.Init()-> UseMirror converted to boolean");
                    m_Mirror = RMConfigurationManager.GetAppSetting("Mirror");
                }

            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
                m_Initialized = false;
                m_Continue = false;
            }
        }

        private void LogJob(Int32 ErrNum, string ErrSource, string ErrDesc)
        {
            string sSQL = String.Empty;
            DbReader objReader = null;
            DbWriter objWriter = null;
            try
            {
                // Log the job

                sSQL = "SELECT * FROM TM_REP_JOB_LOG WHERE JOB_ID=" + m_CurrentJob.JobId;
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                if (objReader.Read())
                {
                    objWriter = DbFactory.GetDbWriter(objReader, true);
                    if (ErrNum == 0)
                    {
                        objWriter.Fields["MSG_TYPE"].Value = MSG_COMPLETE;
                        objWriter.Fields["MSG"].Value = m_CurrentJob.OutputFileName;
                    }
                    else
                    {
                        objWriter.Fields["MSG_TYPE"].Value = MSG_ERROR;
                        objWriter.Fields["MSG"].Value = ErrNum + ", " + ErrSource + ", " + ErrDesc;
                    }
                }
                else
                {
                    objWriter = DbFactory.GetDbWriter(m_sConnectString);
                    objWriter.Tables.Add("TM_REP_JOB_LOG");
                    objWriter.Fields.Add(new DbField("LOG_ID", ModMain.lGetNextUID("TM_REP_JOB_LOG", m_sConnectString)));
                    objWriter.Fields.Add(new DbField("JOB_ID", m_CurrentJob.JobId));
                }

                objWriter.Fields["DTTM_LOGGED"].Value = DateTime.Now.ToString(@"yyyyMMddHHmmss");
                objWriter.Execute();

            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc, m_iClientId);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objWriter = null;
            }
        }

        private void LogProgress(string sReportStage, Int32 timeElapsed, Int32 timeLeft, Int32 percentComplete)
        {
            string sSQL = String.Empty;
            DbReader objReader = null;
            DbWriter objWriter = null;
            try
            {
                // Log the job
                sSQL = "SELECT * FROM TM_REP_JOB_LOG WHERE JOB_ID=" + m_CurrentJob.JobId;
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                if (objReader.Read())
                    objWriter = DbFactory.GetDbWriter(objReader, true);
                else
                {
                    objWriter = DbFactory.GetDbWriter(m_sConnectString);
                    objWriter.Tables.Add("TM_REP_JOB_LOG");
                    objWriter.Fields.Add(new DbField("LOG_ID", ModMain.lGetNextUID("TM_REP_JOB_LOG", m_sConnectString)));
                    objWriter.Fields.Add(new DbField("JOB_ID", m_CurrentJob.JobId));
                }

                objWriter.Fields["MSG_TYPE"].Value = MSG_PROGRESS;
                objWriter.Fields["MSG"].Value = "Stage: " + sReportStage + "\r\n" + "Time Elapsed: " + timeElapsed + "\r\n" + "Time Left: " + timeLeft + "\r\n" + "Percent Complete: " + percentComplete;

                objWriter.Fields["DTTM_LOGGED"].Value = DateTime.Now.ToString(@"yyyyMMddHHmmss");
                objWriter.Execute();
            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objWriter = null;
            }
        }

        private void LogStart()
        {
            string sSQL = String.Empty;
            DbReader objReader = null;
            DbWriter objWriter = null;
            try
            {
                // Log the job
                sSQL = "SELECT * FROM TM_REP_JOB_LOG WHERE JOB_ID=" + m_CurrentJob.JobId;
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                if (objReader.Read())
                {
                    objWriter = DbFactory.GetDbWriter(objReader, true);
                    objWriter.Fields["MSG_TYPE"].Value = MSG_START;
                    objWriter.Fields["DTTM_LOGGED"].Value = DateTime.Now.ToString(@"yyyyMMddHHmmss");
                }
                else
                {
                    objWriter = DbFactory.GetDbWriter(m_sConnectString);
                    objWriter.Tables.Add("TM_REP_JOB_LOG");
                    objWriter.Fields.Add(new DbField("LOG_ID", ModMain.lGetNextUID("TM_REP_JOB_LOG", m_sConnectString)));
                    objWriter.Fields.Add(new DbField("JOB_ID", m_CurrentJob.JobId));
                    objWriter.Fields.Add(new DbField("MSG_TYPE", MSG_START));
                    objWriter.Fields.Add(new DbField("DTTM_LOGGED", DateTime.Now.ToString(@"yyyyMMddHHmmss")));
                }
                objWriter.Execute();
            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc, m_iClientId);//dvatsa-cloud
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objWriter = null;
            }
        }

        public void LookForWork()
        {
            ErrorLog.LogError("", 0, 0, "", "CWorker.cs" + " LookForWork() Started",m_iClientId);//dvatsa-cloud
            string sSQL = String.Empty;
            DbReader objReader = null;
            Int32 l = 0;
            Int32 lJobId = 0;
            bool bRepeat = false;

            try
            {
                // Make sure it is initialized
                if (!m_Initialized)
                {
                    // Auto initialize
                    Init();
                    // If errored out exit
                    if (!m_Initialized)
                        return;
                }
                m_CurrentJob = null;
                // Check do we need to cancel the app
                if (CancelWork())
                {
                    m_Continue = false;
                    return;
                }
                // Look for work
                bRepeat = false;
                do
                {
                    sSQL = "SELECT * FROM TM_REP_JOBS WHERE (ASSIGNED=0 OR ASSIGNED IS NULL) AND (COMPLETE=0 OR COMPLETE IS NULL)" +
                        " AND (START_DTTM IS NULL OR START_DTTM <= '" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "' OR START_DTTM <= '" + DateTime.Now.ToString(@"HHmmss") + "')" +
                        " ORDER BY JOB_PRIORITY";
                    //ErrorLog.LogError("", 0, 0, "", "CWorker.cs" + "LookForWork()->SQL: " + sSQL);
                    objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (!objReader.Read())
                    {
                        break;
                    }
                    // Try to reserve the first one for us
                    lJobId = objReader.GetInt32("JOB_ID");
                    sSQL = "UPDATE TM_REP_JOBS SET ASSIGNED=-1, ASSIGNED_TO='" + m_WorkerName + "',ASSIGNED_DTTM='" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "' WHERE JOB_ID=" + lJobId + " AND (ASSIGNED=0 OR ASSIGNED IS NULL)";
                    //ErrorLog.LogError("", 0, 0, "", "CWorker.cs" + "LookForWork()-> SQL: " + sSQL);
                    l = DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);
                    if (l == 1)
                    {
                        // Successfully reserved, go ahead and do it
                        m_CurrentJob = new CJob();
                        if (m_UseMirror)
                            m_CurrentJob.DSN = ModMain.ApplyMirror(m_Mirror, m_CurrentJob.DSN);

                        m_CurrentJob.JobDesc = objReader.GetString("JOB_DESC");
                        m_CurrentJob.JobId = lJobId;
                        m_CurrentJob.JobName = objReader.GetString("JOB_NAME");
                        m_CurrentJob.NotificationEmail = objReader.GetString("NOTIFY_EMAIL");
                        m_CurrentJob.NotificationType = (objReader.GetString("NOTIFICATION_TYPE")).ToLower();
                        m_CurrentJob.NotifyMessage = objReader.GetString("NOTIFY_MSG");
                        m_CurrentJob.OutputOptions = objReader.GetString("OUTPUT_OPTIONS");
                        m_CurrentJob.OutputPath = objReader.GetString("OUTPUT_PATH");
                        m_CurrentJob.OutputPathURL = objReader.GetString("OUTPUT_PATH_URL");
                        m_CurrentJob.OutputType = (objReader.GetString("OUTPUT_TYPE")).ToLower();
                        m_CurrentJob.ReportXML = objReader.GetString("REPORT_XML");
                        //ErrorLog.LogError("", 0, 0, "", "CWorker.cs" + "LookForWork()-> Reportxml: " + m_CurrentJob.ReportXML.ToString());
                        break;
                    }
                    else
                        bRepeat = true;
                    objReader.Close();
                } while (bRepeat);
                objReader.Close();

                if (!(m_CurrentJob == null))
                    RunReport();
                else
                    ModMain.IsReportProcessed = true;

                objReader.Close();
            }
            catch (Exception exc)
            {

                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
                Restart();
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        private void Restart()
        {
            Thread.Sleep(1000);
            Shutdown();
            Thread.Sleep(1000);
            Init();
        }

        private void Dbg(string s)
        {
#if BB_DEBUG
System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog() ;
appLog.Source = "RiskMaster";
appLog.WriteEntry(s);
#endif
        }

        private string GeneratePreferredFileName(object objXML)
        {
            string sFileName = String.Empty;
            string sTmp = String.Empty;
            string sExt = String.Empty;
            Int32 i = 0;

            string sExcelVer = String.Empty;
            // Pick ext
            switch (m_CurrentJob.OutputType)
            {
                case OUTPUT_PDF:
                    sExt = "pdf";
                    break;

                case OUTPUT_CSV:
                    sExt = "csv";
                    break;

                case OUTPUT_HTML:
                    sExt = "html";
                    break;

                case OUTPUT_XML:
                    sExt = "xml";
                    break;

                case OUTPUT_XLS:
                    /*// Vsoni5 : 04/16/2010 : MITS 19987 - Excel file extension will be based on the Version of Excel aplication installed on RMX Server
                    // i.e. for Excel 2007, extension will be '.xlsx' and for Excel 2003 it will be '.xls' as it used to be
                    Console.WriteLine("ExcelInterop approached");
                    Microsoft.Office.Interop.Excel.Application oExcel;
                    Console.WriteLine("ExcelInterop instantieted successfully");
                    oExcel = new Microsoft.Office.Interop.Excel.Application();
                    if (!(oExcel == null))
                    {
                        // ISSUE: Method or data member not found: Version

                        sExcelVer = oExcel.Version;
                    }
                    else
                    {
                        sExcelVer = "";
                    }

                    oExcel = null;
                    if (sExcelVer == "12.0")
                    {
                        sExt = "xlsx";
                    }
                    else
                    {
                        sExt = "xls";
                    }
                    // End of MITS 19987*/
                    break;
            }
            // Check for User provided file name

            sFileName = ((XmlDocument)objXML).DocumentElement.GetAttribute("outputfilename") + "";
            // Make sure the user provided string is "file name friendly"
            if (sFileName != "")
            {
                if (ModMain.IsAlphaNum((short)(sFileName.Substring(0, 1)[0])) != 1)
                {
                    sFileName = "A" + sFileName;
                }
                // Remove any unnacceptable characters.
                for (i = 1;
                    i <= sFileName.Length;
                    i = Convert.ToInt32(i + 1))
                {

                    sTmp = sTmp + (Convert.ToBoolean(ModMain.IsAlphaNum((short)(sFileName.Substring((int)(i), 1)[0]))) ? sFileName.Substring((int)(i), 1) : "");
                }
                sFileName = sTmp;
            }
            // Not "else" since "Fixing" the string could remove all characters...
            if (sFileName == "")
            {
                // User not specified or invalid specified.
                sFileName = "sm" + sExt + "rep" + m_CurrentJob.JobId + "." + sExt;
            }
            else
            {
                sFileName = sFileName + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "." + sExt;
            }

            // Plug in the result.
            return sFileName;
        }

        private int FetchResults(IReportEngine pEngine, object objXML)
        {
            m_CurrentJob.NumOfPages = 1;
            try
            {
                string sPreffilename = GeneratePreferredFileName(objXML);
                m_CurrentJob.OutputFileName = ModMain.getUniqueFileName(sPreffilename, m_ReportFolder, false);
                //Console.WriteLine(m_CurrentJob.OutputFileName);
                if (pEngine.ReportOutput[0].ToString() != m_CurrentJob.OutputFileName)
                {
                    // BSB 02.06.2003 If the engine already zipped the file, we must honor that extension....
                    if (pEngine.ReportOutput[0].ToString().EndsWith(".zip"))
                    {
                        m_CurrentJob.OutputFileName = (m_CurrentJob.OutputFileName).Substring(0, m_CurrentJob.OutputFileName.LastIndexOf(".") - 1) + ".zip";
                    }
                    File.Copy(pEngine.ReportOutput[0].ToString(), m_CurrentJob.OutputFileName);
                    File.Delete(pEngine.ReportOutput[0].ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.StackTrace);
            }
            return 1;
        }

        public bool __Continue
        {
            get
            {
                return m_Continue;
            }

        }

        private void RunReport()
        {
            OshaManager objOSHA = null;
            FileStorageManager objCSCFileStorage = null;
            IReportEngine pOSHAEngine = null;
            XmlDocument objXML = null;
            int lRet = 0;
            Int32 l = 0;
            Int32 RptType = 0;
            string sFileName = String.Empty;
            string s = String.Empty;
            string sName = String.Empty;
            string sExt = String.Empty;
            string sKillList = String.Empty;
            string sCount = String.Empty;
            string sMessage = String.Empty;
            string sOutputPath = String.Empty;
            string sSQL = String.Empty;
            Int32 ErrNum = 0;
            bool bErrror = false;
            string ErrSource = String.Empty;
            string ErrDesc = String.Empty;
            Int32 ErrLine = 0;
            string[] v = null;
            int iFileCopied = 0;
            Boolean bFileExists = false;
            try
            {
                Console.WriteLine("RunReport() started");

                LogStart();
                // Open xml so we can pick up any job params stuffed inside the report xml.
                objXML = new XmlDocument();
                objXML.LoadXml(m_CurrentJob.ReportXML);
                // Determine the Proper Report Engine to Use.
                RptType = -10;
                RptType = Convert.ToInt32(objXML.SelectSingleNode("report").Attributes.GetNamedItem("type").Value);
                if (RptType != -10)
                {
                    // Custom Report
                    // Initialize default values for failure cases.
                    m_CurrentJob.NumOfPages = 0;
                    m_CurrentJob.OutputFileName = "none";
                    switch (RptType)
                    {
                        case 1:
                        case 3:
                        case 4:
                        case 5:
                            // Is OSHA Report
                            objOSHA = new OshaManager(ModMain.MainDSN, (int)RptType,m_iClientId);//dvatsa-cloud
                            objOSHA.ConnectionString = ModMain.MainDSN;
                            objOSHA.Init();
                            objOSHA.Notify = this;
                            pOSHAEngine = objOSHA; // Doesn't use the GetIReportEngine since it does not need IDispatch
                            pOSHAEngine.OutputDirectory = m_ReportFolder;
                            pOSHAEngine.CriteriaXmlDom = objXML;
                            if (m_CurrentJob.OutputType == OUTPUT_PDF)
                            {
                                pOSHAEngine.OutputFormat = eReportFormat.Pdf;

                                pOSHAEngine.RunReport();
                                // Test for success.
                                if (objOSHA.Errors == null || objOSHA.Errors.count == 0)
                                    lRet = FetchResults(pOSHAEngine, objXML);
                            }
                            else
                            {
                                throw new RMAppException("Unsupported report output format requested.");
                            }

                            break;

                        default:
                            throw new RMAppException("Unsupported report type requested.");
                    }
                }
                sFileName = m_CurrentJob.OutputFileName;
                sOutputPath = (m_CurrentJob.OutputPath).Trim();
                if (lRet != 0)
                {
                    // Deliver it
                    if (sOutputPath != "")
                    {
                        SetDocPathType(sOutputPath);
                        if (m_DocPathType == StorageType.DatabaseStorage)
                        {
                            objCSCFileStorage = new FileStorageManager(StorageType.DatabaseStorage, sOutputPath,m_iClientId);//dvatsa-cloud
                        }
                        // BSB Note: Even in DB Storage Case, later clients need to be able to parse
                        // the filename out of the output path field.  Hence we'll add the "\" and
                        // use it as a delimiter.
                        if (!sOutputPath.EndsWith("\\"))
                        {
                            sOutputPath = sOutputPath + "\\";
                        }
                        l = sFileName.LastIndexOf("\\");
                        if (l > 0)
                        {
                            sName = sFileName.Substring((int)(l + 1));
                            l = sName.LastIndexOf(".");
                            if (l > 0)
                            {
                                sExt = sName.Substring((int)(l));
                                sName = sName.Substring(0, (int)(l));
                                // Copy the files
                                if (m_CurrentJob.OutputType == OUTPUT_HTML && m_CurrentJob.NumOfPages > 1)
                                {
                                    // Multi file delivery
                                    if (m_DocPathType == StorageType.DatabaseStorage)
                                    {
                                        sCount = "&count=" + m_CurrentJob.NumOfPages;
                                    }
                                    // tkr 12/2002 send count of number of pages generated (no longer used to generate files; retained as debugging aid)
                                    sKillList = sKillList + "," + m_ReportFolder + sName + sExt;
                                    for (l = 1; l <= m_CurrentJob.NumOfPages; l = Convert.ToInt32(l + 1))
                                    {
                                        if (m_DocPathType == StorageType.FileSystemStorage)
                                        {
                                            iFileCopied = objCSCFileStorage.CopyFile(m_ReportFolder + sName + l + sExt, sOutputPath + sName + l + sExt);
                                            if (iFileCopied == 1)
                                            {
                                                sKillList = sKillList + "," + m_ReportFolder + sName + l + sExt;
                                            }
                                        }
                                        else
                                        {
                                            objCSCFileStorage.StoreFile(m_ReportFolder + sName + l + sExt, sName + l + sExt);
                                            sKillList = sKillList + "," + m_ReportFolder + sName + l + sExt;
                                        }
                                    }
                                }
                                else
                                {
                                    // Single file
                                    if (m_DocPathType == StorageType.FileSystemStorage)
                                    {
                                        try
                                        {
                                            if (!Path.GetFullPath(sFileName).Equals(Path.GetFullPath(sOutputPath + sName + sExt)))
                                            {
                                                File.Copy(sFileName, sOutputPath + sName + sExt, true);
                                                sKillList = sFileName;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new RMAppException(ex.Message);
                                        }
                                    }
                                    else
                                    {
                                        objCSCFileStorage.StoreFile(sFileName, sName + sExt);
                                        sKillList = sFileName;
                                    }
                                }
                                // Copy image if html report
                                if (m_CurrentJob.OutputType == OUTPUT_HTML)
                                {
                                    objCSCFileStorage.FileExists(m_ReportFolder + sName + ".jpg", out bFileExists);
                                    if (bFileExists)
                                    {
                                        if (m_DocPathType == StorageType.FileSystemStorage)
                                        {
                                            iFileCopied = objCSCFileStorage.CopyFile(m_ReportFolder + sName + ".jpg", sOutputPath + sName + ".jpg");
                                            if (iFileCopied == 1)
                                            {
                                                sKillList = sKillList + "," + m_ReportFolder + sName + ".jpg";
                                            }
                                        }
                                        else
                                        {
                                            objCSCFileStorage.StoreFile(m_ReportFolder + sName + ".jpg", sName + ".jpg");
                                            sKillList = sKillList + "," + m_ReportFolder + sName + ".jpg";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    objCSCFileStorage = null;
                }

                // Log the job ...
                //Mukesh// Need to handle optional arguments
                LogJob(0, null, null);
                bErrror = false;
            }
            catch (Exception ex)
            {
                bErrror = true;
                ErrNum = 0;
                ErrSource = ex.Source.ToString();
                ErrDesc = ex.Message;
                ErrorLog.LogError(ex,m_iClientId);//dvatsa-cloud
                SendNotification(bErrror, ErrSource, ErrDesc + ", Line: " + ErrLine, sCount);

            }
            finally
            {
                sFileName = m_CurrentJob.OutputFileName;
                sOutputPath = (m_CurrentJob.OutputPath).Trim();
                if (!bErrror)
                {
                    // Mark job as completed and update output path to include actual filename assigned
                    sSQL = "UPDATE TM_REP_JOBS SET COMPLETE=-1,COMPLETE_DTTM='" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "',ERROR_FLAG=0";

                    if (m_CurrentJob.OutputType == OUTPUT_HTML && m_CurrentJob.NumOfPages > 1)
                    {
                        sSQL = sSQL + ",OUTPUT_PATH='" + (sOutputPath.EndsWith("\\") ? sOutputPath : sOutputPath + "\\") + sName + "1" + sExt + "'";
                        sSQL = sSQL + ",OUTPUT_PATH_URL='" + AddFileName2URL(m_CurrentJob.OutputPathURL, sName + "1" + sExt + sCount) + "'";
                    }
                    else if (m_CurrentJob.NumOfPages > 0)
                    {
                        sSQL = sSQL + ",OUTPUT_PATH='" + (sOutputPath.EndsWith("\\") ? sOutputPath : sOutputPath+"\\") + sName + sExt + "'";
                        sSQL = sSQL + ",OUTPUT_PATH_URL='" + AddFileName2URL(m_CurrentJob.OutputPathURL, sName + sExt) + "'";
                    }
                    else
                    {
                        sSQL = sSQL + ",OUTPUT_PATH='none'";
                        sSQL = sSQL + ",OUTPUT_PATH_URL='" + AddFileName2URL(m_CurrentJob.OutputPathURL, "none") + "'";
                    }

                    sSQL = sSQL + " WHERE JOB_ID=" + m_CurrentJob.JobId;
                    DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);

                    // Notification Handling + Attachment if needed
                    //Mukesh// Need to handle optional arguments
                    SendNotification(bErrror, null, null, sCount);
                    //sendMail();
                }
                else
                {
                    // job errored out
                    // Mark job as completed and update output path to include actual filename assigned
                    sSQL = "UPDATE TM_REP_JOBS SET COMPLETE=-1,COMPLETE_DTTM='" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "', ERROR_FLAG=-1";
                    sSQL = sSQL + " WHERE JOB_ID=" + m_CurrentJob.JobId;
                    DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);
                    if (ErrDesc != null && ErrDesc != "")
                    {
                        int iMsgStartPtr = ErrDesc.LastIndexOf("|^|");
                        if (iMsgStartPtr != -1)
                        {
                            sSQL = "UPDATE TM_REP_JOB_LOG SET MSG='" + ErrDesc.Substring(iMsgStartPtr+3).Replace("'", "''") + "'";
                        }
                        else
                        {
                            sSQL = "UPDATE TM_REP_JOB_LOG SET MSG='" + ErrDesc.Replace("'", "''") + "'";
                        }
                        sSQL = sSQL + " WHERE JOB_ID=" + m_CurrentJob.JobId;
                        DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);
                    }

                }

                // tkr 12/2002 kill temp files in "RMNetUserData\ReportData" folder AFTER SendNotification(could be embedded in email)
                if (sKillList.Length > 0)
                {
                    if (sKillList.Substring(1, 1) == ",")
                        sKillList = sKillList.Substring(2);

                    v = sKillList.Split(',');

                    foreach (string str in v)
                    {
                        sKillList = str;
                        try
                        {
                            File.Delete(sKillList);
                        }
                        catch { continue; }
                    }

                }
            }
        }

        // If the Querystring has multiple name/value pairs, the one related to file
        // may not be the last one
        private string AddFileName2URL(string sURL, string sFileName)
        {
            int iFile = 0;
            int iQuestion = 0;
            string sReturn = String.Empty;

            sReturn = sURL;
            // Try to find "file=" in the querystring
            iFile = sReturn.IndexOf("file=");
            if (iFile == -1)
            {
                // Not found
                iQuestion = sReturn.IndexOf("?");
                if (iQuestion == 0)
                {
                    // no others
                    sReturn = sReturn + "?";
                }
                else
                {
                    sReturn = sReturn + "&";
                }
                sReturn = sReturn + "file=" + sFileName;
            }
            else
            {
                // If "file=" is at the end of the string
                if (sReturn.EndsWith("file="))
                {
                    sReturn = sReturn + sFileName;
                }
                else if (iFile + 5 < sReturn.Length)
                {
                    // in the middle
                    sReturn = sReturn.Insert(iFile + 5, sFileName);
                }
            }
            return sReturn;
        }

        // if output path begins \\ or [char]: is file storage, otherwise is database storage and the string is a connection string
        private void SetDocPathType(string sOutputPath)
        {
            const string sComputer = "\\\\";
            const string sColon = ":";
            if (sOutputPath.StartsWith(sComputer) || sOutputPath.IndexOf(sColon) == 1)
            {
                m_DocPathType = StorageType.FileSystemStorage;
            }
            else
            {
                m_DocPathType = StorageType.DatabaseStorage;
            }
        }

        public void ScheduleReports()
        {
            string sSQL = String.Empty;
            DbReader objReader = null;
            string sDate = String.Empty;
            Int32 lScheduleType = 0;
            Int32 lScheduleId = 0;
            Int32 lReportId = 0;
            string sStartTime = String.Empty;
            string sNextRunDate = String.Empty;
            string sNewNextRunDate = String.Empty;
            DateTime vDate;
            Int32 lIndex = 0;
            Int32 lJobId = 0;
            Int32 lTMJobId = 0;
            string sOutputPathURL = String.Empty;
            Console.WriteLine("CWorker.Schedule(); Schedule started");
            try
            {
                // Make sure it is initialized
                if (!m_Initialized)
                {
                    // Auto initialize
                    Init();
                    // If errored out exit
                    if (!m_Initialized)
                    {
                        return;
                    }
                }
                Console.WriteLine("CWorker.Schedule(); Init executed");
                sDate = DateTime.Now.ToString(@"yyyyMMdd");
                sSQL = "SELECT * FROM TM_REP_SCHEDULE WHERE NEXT_RUN_DATE<='" + sDate + "' AND START_TIME<='" + DateTime.Now.ToString(@"HHmmss") + "'";
                //Console.WriteLine(sSQL + " ->ScheduleReports");
                //Console.WriteLine("ConnStr: " + m_sConnectString);
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                //Console.WriteLine(sSQL + " executed");
                if (objReader.Read())
                {
                    Console.WriteLine("objReader.Read() -> ScheduleReports");
                    lScheduleId = objReader.GetInt32("SCHEDULE_ID");
                    lScheduleType = objReader.GetInt32("SCHEDULE_TYPE");
                    lReportId = objReader.GetInt32("REPORT_ID");
                    sStartTime = objReader.GetString("START_TIME");
                    sNextRunDate = objReader.GetString("NEXT_RUN_DATE");
                    sOutputPathURL = objReader.GetString("OUTPUT_PATH_URL");
                    // Calculate new next run date

                    vDate = new DateTime(Convert.ToInt32(sNextRunDate.Substring(0, 4)), Convert.ToInt32(sNextRunDate.Substring(4, 2)), Convert.ToInt32(sNextRunDate.Substring(6, 2)));
                    if (lScheduleType == 1)
                    {
                        vDate = vDate.AddDays(1);
                        if ((DateTime)vDate <= DateTime.Today)
                            vDate = DateTime.Today.AddDays(1);

                        for (lIndex = 1; lIndex <= 7; lIndex = Convert.ToInt32(lIndex + 1))
                        {
                            if ((vDate.DayOfWeek == DayOfWeek.Monday && objReader.GetInt32("MON_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Tuesday && objReader.GetInt32("TUE_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Wednesday && objReader.GetInt32("WED_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Thursday && objReader.GetInt32("THU_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Friday && objReader.GetInt32("FRI_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Saturday && objReader.GetInt32("SAT_RUN") == 1) ||
                                (vDate.DayOfWeek == DayOfWeek.Sunday && objReader.GetInt32("SUN_RUN") == 1))
                            {
                                sNewNextRunDate = vDate.ToString(@"yyyyMMdd");
                                break;
                            }
                            vDate = vDate.AddDays(1);
                        }
                    }
                    else
                    {
                        vDate = vDate.AddMonths(1);
                        if (Convert.ToDateTime(vDate).Month <= DateTime.Today.Month || Convert.ToDateTime(vDate).Year <= Convert.ToDateTime(vDate).Month)
                            vDate = DateTime.Today.AddMonths(1);

                        for (lIndex = 1; lIndex <= 12; lIndex = Convert.ToInt32(lIndex + 1))
                        {
                            if ((Convert.ToDateTime(vDate).Month == 1 && objReader.GetInt32("JAN_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 2 && objReader.GetInt32("FEB_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 3 && objReader.GetInt32("MAR_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 4 && objReader.GetInt32("APR_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 5 && objReader.GetInt32("MAY_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 6 && objReader.GetInt32("JUN_RUN") == 1))
                            {
                                sNewNextRunDate = vDate.ToString(@"yyyyMMdd");
                                break;
                            }
                            else if ((Convert.ToDateTime(vDate).Month == 7 && objReader.GetInt32("JUL_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 8 && objReader.GetInt32("AUG_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 9 && objReader.GetInt32("SEP_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 10 && objReader.GetInt32("OCT_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 11 && objReader.GetInt32("NOV_RUN") == 1) || (Convert.ToDateTime(vDate).Month == 12 && objReader.GetInt32("DEC_RUN") == 1))
                            {
                                sNewNextRunDate = vDate.ToString(@"yyyyMMdd");
                                break;
                            }
                            vDate = vDate.AddMonths(1);
                        }
                    }
                    if (sNewNextRunDate != "")
                    {
                        sSQL = "UPDATE TM_REP_SCHEDULE SET NEXT_RUN_DATE='" + sNewNextRunDate + "', LAST_RUN_DTTM='" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + "' WHERE REPORT_ID=" + lReportId + " AND SCHEDULE_ID=" + lScheduleId + " AND NEXT_RUN_DATE='" + sNextRunDate + "'";
                        if (DbFactory.ExecuteNonQuery(m_sConnectString, sSQL) == 1)
                        {
                            // Throw it to the wolfs
                            lJobId = ModMain.lGetNextUID("TM_REP_JOBS", m_sConnectString);
                            lTMJobId = ModMain.lTMGetNextUID("TM_JOBS", m_sConnectString);
                            sOutputPathURL = sOutputPathURL + "&JobID=" + lJobId;
                            sSQL = "INSERT INTO TM_REP_JOBS (JOB_ID,JOB_PRIORITY,JOB_NAME,JOB_DESC,OUTPUT_TYPE,OUTPUT_PATH,OUTPUT_PATH_URL,OUTPUT_OPTIONS,REPORT_XML,START_DTTM,NOTIFICATION_TYPE,NOTIFY_EMAIL,NOTIFY_MSG,USER_ID,COMPLETE,ASSIGNED,ARCHIVED,TM_JOBS_ID)"; // ASSIGNED,ASSIGNED_TO,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,USER_ID,ARCHIVED,ERROR_FLAG)"
                            //rkulavil : RMA-13695 starts
                            //sSQL = sSQL + " SELECT " + lJobId + ",0,JOB_NAME,JOB_DESC,OUTPUT_TYPE,OUTPUT_PATH,'" + sOutputPathURL + "',OUTPUT_OPTIONS,REPORT_XML,'" + sNextRunDate + sStartTime + "',NOTIFICATION_TYPE,NOTIFY_EMAIL,NOTIFY_MSG,USER_ID,0,0,0," + lTMJobId + "FROM TM_REP_SCHEDULE WHERE SCHEDULE_ID=" + lScheduleId;
                            sSQL += string.Format("SELECT {0} ,0,JOB_NAME,JOB_DESC,OUTPUT_TYPE,OUTPUT_PATH,'{1}',OUTPUT_OPTIONS,REPORT_XML,'{2}',NOTIFICATION_TYPE,NOTIFY_EMAIL,NOTIFY_MSG,USER_ID,0,0,0,{3} FROM TM_REP_SCHEDULE WHERE SCHEDULE_ID= {4}", lJobId, sOutputPathURL, sNextRunDate + sStartTime, lTMJobId, lScheduleId); 
                            //rkulavil : RMA-13695 ends
                            DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);

                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
                Restart();

            }
        }


        private void SendNotification(bool bError, string ErrSource, string ErrDesc, string sCount)
        {
            Int32 l = 0;
            MailMessage objMailMessage = null;
            Attachment objAttachment = null;

            string sFromName = String.Empty;
            string sFromAdd = String.Empty;
            string sSubject = String.Empty;
            string sAttachment = string.Empty;

            string sMessage = String.Empty;
            string sFileName = String.Empty;
            string sTemp = String.Empty;
            string sName = String.Empty;
            string sExt = String.Empty;
            ZipUtility objZIP = null;
            FileStorageManager objFileMgr;
            bool bFileExists;
            
            try
            {
               sFileName = m_CurrentJob.OutputFileName;
               if (m_CurrentJob.NotificationType != NOTIFY_NONE && m_CurrentJob.NotificationType != "" && m_CurrentJob.NotificationEmail != "")
               {
                    objMailMessage = new MailMessage();
                    sSubject = RMConfigurationManager.GetAppSetting(DEFAULT_MAIL_SUBJECT);
                    if (sSubject != null && sSubject != "")
                    {
                        objMailMessage.Subject = sSubject;
                    }
                    else
                    {
                        objMailMessage.Subject = "SORTMASTER Report Notification";
                    }

                    sFromName = RMConfigurationManager.GetAppSetting(DEFAULT_MAIL_FROMNAME);
                    sFromAdd = RMConfigurationManager.GetAppSetting(DEFAULT_MAIL_FROMEMAIL);
                    objMailMessage.From = new System.Net.Mail.MailAddress((sFromAdd == "" || sFromAdd ==null)? "smserver@riskmaster.net" : sFromAdd);
                    objMailMessage.To.Add(m_CurrentJob.NotificationEmail);
                    if (!bError)
                    {
                        // Create Message
                        sMessage = "Following report is completed: " + m_CurrentJob.JobName + "\r\n" + "(" + m_CurrentJob.JobDesc + ")";
                        if (m_CurrentJob.NotifyMessage != "")
                        {
                            sMessage = sMessage + "\r\n" + "\r\n" + m_CurrentJob.NotifyMessage + "\r\n";
                        }

                        if (m_CurrentJob.NotificationType == NOTIFY_EMBED && m_CurrentJob.NumOfPages >= 1)
                        {
                            if (m_CurrentJob.OutputType == OUTPUT_HTML && m_CurrentJob.NumOfPages > 1)
                            {
                                // This is the case when we have more than one output file
                                l = sFileName.LastIndexOf(".");
                                if (l > 0)
                                {
                                    sAttachment = sFileName.Substring(sFileName.Length - ((int)(sFileName.Length - l)));
                                    sFileName = sFileName.Substring(0, (int)(l + 1));
                                }
                                for (l = 1; l <= m_CurrentJob.NumOfPages; l = Convert.ToInt32(l + 1))
                                {
                                    objAttachment = new Attachment(sFileName + l + sAttachment);
                                    objMailMessage.Attachments.Add(objAttachment);
                                }
                            }
                            else
                            {
                                // ZIP up file if needed
                                FileInfo FI = new FileInfo(sFileName);
                                if (ModMain.g_ZIPFileSize > 0 && FI.Length >= ModMain.g_ZIPFileSize)
                                {
                                    objZIP = new ZipUtility(m_iClientId);//dvatsa-cloud
                                    try
                                    {
                                        objZIP.CompressFile(sFileName, ModMain.GetZipFileName(sFileName));
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorLog.LogError(ex,m_iClientId);//dvatsa-cloud
                                        objAttachment = new Attachment(sFileName);
                                        objMailMessage.Attachments.Add(objAttachment);

                                    }
                                    objAttachment = new Attachment(ModMain.GetZipFileName(sFileName));
                                    objMailMessage.Attachments.Add(objAttachment);
                                }
                                else
                                {
                                    objAttachment = new Attachment(sFileName);
                                    objMailMessage.Attachments.Add(objAttachment);

                                }
                            }
                            // Attach the image if needed
                            objFileMgr = new FileStorageManager(m_iClientId);//dvatsa-cloud
                            if (m_CurrentJob.OutputType == OUTPUT_HTML)
                            {
                                l = sFileName.LastIndexOf(".");
                                if (l > 0)
                                {
                                    bFileExists = File.Exists(sFileName.Substring(0, (int)(l)) + ".jpg");
                                    if (bFileExists)
                                    {
                                        objAttachment = new Attachment(sFileName.Substring(0, (int)(l)) + ".jpg");
                                        objMailMessage.Attachments.Add(objAttachment);
                                    }
                                }
                            }
                        }
                        else if (m_CurrentJob.NotificationType == NOTIFY_LINK && m_CurrentJob.NumOfPages >= 1)
                        {
                            l = sFileName.LastIndexOf("\\");
                            if (l > 0)
                            {
                                sName = sFileName.Substring((int)(l + 1));
                                l = sName.LastIndexOf(".");
                                if (l > 0)
                                {
                                    sExt = sName.Substring((int)(l));
                                    sName = sName.Substring(0, (int)(l));
                                }
                            }

                            if (sName != "")
                            {
                                sMessage = sMessage + "\r\n" + "\r\n" + "For Report Output click here: ";

                                if (m_CurrentJob.OutputType == OUTPUT_HTML && m_CurrentJob.NumOfPages > 1)
                                {
                                    sMessage = sMessage + AddFileName2URL(m_CurrentJob.OutputPathURL, sName + "1" + sExt + sCount) + "\r\n";
                                }
                                else
                                {
                                    sMessage = sMessage + AddFileName2URL(m_CurrentJob.OutputPathURL, sName + sExt) + "\r\n";
                                }
                            }
                        }
                        else if (m_CurrentJob.NumOfPages == 0)
                        {
                            sMessage = sMessage + "\r\n" + "\r\n" + "** Report Generated No Output **";
                        }
                    }
                    else
                    {
                        objMailMessage.Subject = "SORTMASTER Report Notification, Report Failed";
                        sMessage = "Report " + m_CurrentJob.JobName + "\r\n" + m_CurrentJob.JobDesc + "\r\n" + "\r\n";
                        sMessage = "Error running the report. Following error were reported: " + "\r\n" + ", " + ErrSource + "," + ErrDesc + "\r\n";
                    }
                    objMailMessage.Body = sMessage;

                    // Subhendu 11/30/2010 MITs 22615 (Return the URL without any word wrapping)
                    Riskmaster.Common.Mailer.SendMail(objMailMessage,m_iClientId);//dvatsa-cloud
                    objMailMessage = null;
                    objZIP.Dispose();
                }

            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
            }
        }

       

        private bool CancelWork()
        {
            DbReader objReader = null;
            string sSQL = String.Empty;
            string s = String.Empty;
            try
            {
                sSQL = "SELECT * FROM TM_REP_WORKER_COMMANDS WHERE WORKER_PROCESS IS NULL OR WORKER_PROCESS='" + m_WorkerName + "'";
                objReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                // Execute commands if any
                if (objReader.Read())
                {

                    s = (objReader.GetString("COMMAND_TEXT")).ToLower();
                    if (s == CMD_SHUTDOWN)
                    {
                        return (true);
                    }
                }
            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
            }
            return false;
        }

        public void Shutdown()
        {
            bool m_Initialized = false;
            try
            {

                if (!m_Initialized)
                    return;
                m_Initialized = false;

            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
            }
        }

        private Int32 ISMEngineCallback_CheckAbort()
        {
            Int32 ISMEngineCallback_CheckAbort = 0;
            if (CancelWork())
            {
                ISMEngineCallback_CheckAbort = Convert.ToInt32(true);
            }
            return ISMEngineCallback_CheckAbort;
        }

        private void ISMEngineCallback_UpdateProgress(string sReportStage, Int32 timeElapsed, Int32 timeLeft, Int32 percentComplete)
        {
            LogProgress(sReportStage, timeElapsed, timeLeft, percentComplete);
        }

        private bool IWorkerNotifications_CheckAbort()
        {
            bool IWorkerNotifications_CheckAbort = false;
            if (CancelWork())
            {
                IWorkerNotifications_CheckAbort = true;
            }
            return IWorkerNotifications_CheckAbort;
        }

        private void IWorkerNotifications_UpdateProgress(string sReportStage, Int32 timeElapsed, Int32 timeLeft, Int32 percentComplete)
        {
            LogProgress(sReportStage, timeElapsed, timeLeft, percentComplete);
        }


        public string DSN
        {
            get
            {
                return m_sConnectString;
            }

            set
            {
                m_sConnectString = value;
            }

        }
        
        public string ReportFolder
        {
            get
            {
                return m_ReportFolder;
            }

            set
            {
                m_ReportFolder = value;
                if (m_ReportFolder != "")
                {
                    if (m_ReportFolder.Substring(m_ReportFolder.Length) != "\\")
                    {
                        m_ReportFolder = m_ReportFolder + "\\";
                    }
                }
            }

        }

        public string SMTPServer
        {
            get
            {
                return m_SMTPServer;
            }

            set
            {
                m_SMTPServer = value;
            }

        }

        public string ReportURL
        {
            get
            {
                return m_ReportURL;
            }

            set
            {
                m_ReportURL = value;
            }

        }

        public int CheckAbort()
        {

            return (0);
        }

        public void UpdateProgress(string p_sReportStage, int p_iTimeElapsed, int p_iTimeLeft, int p_iPercentComplete)
        {

        }



    }

}
