using System;
namespace Osha
{
    public class CJob
    {

        public Int32 JobId = 0;
        public string JobName = String.Empty;
        public string JobDesc = String.Empty;
        public string DSN = String.Empty;
        public string OutputType = String.Empty;
        public string OutputPath = String.Empty;
        public string OutputPathURL = String.Empty;
        public string OutputOptions = String.Empty;
        public string ReportXML = String.Empty;
        public string NotificationType = String.Empty;
        public string NotificationEmail = String.Empty;
        public string NotifyMessage = String.Empty;
        public string OutputFileName = String.Empty;
        public Int32 NumOfPages = 0;
    }

}
