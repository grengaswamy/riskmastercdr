using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Microsoft.Vsa;
using Riskmaster;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;
using Riskmaster.Db;
using Riskmaster.Scripting;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace ScriptEditor
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmScriptEditor : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RichTextBox tboxView;
		private System.Windows.Forms.Button btnLoad;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		RiskmasterDatabase objRMDB = null;
		private System.Windows.Forms.Button btnSaveScript;
		private ScriptEngine objScriptEngine=null;
		private System.Windows.Forms.Button btnExecScript;
		private System.Windows.Forms.Button btnAddScript;
		private System.Windows.Forms.ComboBox cmbScriptType;

		private string m_ConnectionString="Driver=SQL Server;Server=KOH;Database=TRAINDB;UID=sa;PWD=riskmaster;";

		public frmScriptEditor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tboxView = new System.Windows.Forms.RichTextBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnSaveScript = new System.Windows.Forms.Button();
			this.btnExecScript = new System.Windows.Forms.Button();
			this.btnAddScript = new System.Windows.Forms.Button();
			this.cmbScriptType = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// tboxView
			// 
			this.tboxView.Location = new System.Drawing.Point(8, 8);
			this.tboxView.Name = "tboxView";
			this.tboxView.Size = new System.Drawing.Size(400, 224);
			this.tboxView.TabIndex = 0;
			this.tboxView.Text = "tboxView";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(24, 248);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(88, 40);
			this.btnLoad.TabIndex = 1;
			this.btnLoad.Text = "Load Script";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnSaveScript
			// 
			this.btnSaveScript.Location = new System.Drawing.Point(232, 248);
			this.btnSaveScript.Name = "btnSaveScript";
			this.btnSaveScript.Size = new System.Drawing.Size(88, 40);
			this.btnSaveScript.TabIndex = 2;
			this.btnSaveScript.Text = "Save Script";
			this.btnSaveScript.Click += new System.EventHandler(this.btnSaveScript_Click);
			// 
			// btnExecScript
			// 
			this.btnExecScript.Location = new System.Drawing.Point(464, 40);
			this.btnExecScript.Name = "btnExecScript";
			this.btnExecScript.Size = new System.Drawing.Size(88, 40);
			this.btnExecScript.TabIndex = 3;
			this.btnExecScript.Text = "Executive Script";
			this.btnExecScript.Click += new System.EventHandler(this.btnExecScript_Click);
			// 
			// btnAddScript
			// 
			this.btnAddScript.Location = new System.Drawing.Point(128, 248);
			this.btnAddScript.Name = "btnAddScript";
			this.btnAddScript.Size = new System.Drawing.Size(88, 40);
			this.btnAddScript.TabIndex = 4;
			this.btnAddScript.Text = "Add Script";
			this.btnAddScript.Click += new System.EventHandler(this.btnAddScript_Click);
			// 
			// cmbScriptType
			// 
			this.cmbScriptType.Items.AddRange(new object[] {
															   "Validate",
															   "Calculate",
															   "Initialize",
															   "Before Save",
															   "After Save"});
			this.cmbScriptType.Location = new System.Drawing.Point(416, 8);
			this.cmbScriptType.Name = "cmbScriptType";
			this.cmbScriptType.Size = new System.Drawing.Size(112, 21);
			this.cmbScriptType.TabIndex = 5;
			this.cmbScriptType.SelectedIndex=0;
			this.cmbScriptType.SelectedItem=0;
			this.cmbScriptType.SelectedIndexChanged += new System.EventHandler(this.cmbScriptType_SelectedIndexChanged);
			// 
			// frmScriptEditor
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(560, 302);
			this.Controls.Add(this.cmbScriptType);
			this.Controls.Add(this.btnAddScript);
			this.Controls.Add(this.btnExecScript);
			this.Controls.Add(this.btnSaveScript);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.tboxView);
			this.Name = "frmScriptEditor";
			this.Text = "Script Editor";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmScriptEditor());
		}

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			objScriptEngine=new ScriptEngine();
			DbConnection objConn = DbFactory.GetDbConnection(m_ConnectionString);
			objConn.Open();
			objScriptEngine.LoadScript(1,"EVENT",objConn);
			tboxView.Text=objScriptEngine.Script;
			objConn.Close();
		}

		private void btnSaveScript_Click(object sender, System.EventArgs e)
		{
			DbConnection DbConn = DbFactory.GetDbConnection(m_ConnectionString);
			DbConn.Open();

			DbWriter objWriter = DbFactory.GetDbWriter(m_ConnectionString);

			objScriptEngine=new ScriptEngine(DbConn);
//			objScriptEngine.StartEngine();

			objScriptEngine.AddScript(tboxView.Text);

			objScriptEngine.RowId=0;
			objScriptEngine.Type=0;
			objScriptEngine.ObjectName="ALL";
			objScriptEngine.Description="All Binary";

			objScriptEngine.SaveScript(objWriter);
			DbConn.Close();
//			objScriptEngine.StartEngine();
//			objScriptEngine.StopEngine();
		}

		private void btnExecScript_Click(object sender, System.EventArgs e)
		{

			DbConnection DbConn = DbFactory.GetDbConnection(m_ConnectionString);
			DbConn.Open();
			objScriptEngine=new ScriptEngine(DbConn);
			try
			{
				objScriptEngine.StartEngine();
			}
			catch(Exception e1)
			{
				if (e1.Message.Substring(0,9)=="Could not")
					Console.WriteLine(cmbScriptType.SelectedText + " Not Found");
				else
                    throw new Riskmaster.ExceptionTypes.RMAppException(Globalization.GetString("Load Binary Failed."),e1);
			}
			objScriptEngine.RunScript(cmbScriptType.SelectedIndex+1,"EVENT");
			DbConn.Close();
		}

		private void btnAddScript_Click(object sender, System.EventArgs e)
		{
//			objScriptEngine=new ScriptEngine();
//
//			objScriptEngine.AddScript(tboxView.Text);
//
//			objScriptEngine.RowId=0;
//			objScriptEngine.Type=0;
//			objScriptEngine.ObjectName="ALL";
//			objScriptEngine.Description="All Binary";
//
//			objScriptEngine.StopEngine();
		}

		private void cmbScriptType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			tboxView.Text=(string) cmbScriptType.SelectedIndex.ToString();
		}

	}
}
