Sub ASCFunds(ByVal objFunds As Riskmaster.DataModel.Funds)

        Dim dReserveBalance As Double = 0.0
        Dim objReserveCurrent As Riskmaster.DataModel.ReserveCurrent
        Dim sSql As String
        Dim objNewReserveHistory As Riskmaster.DataModel.ReserveHistory
        Dim sDate As String

        

        objReserveCurrent = CType(objFunds.Context.Factory.GetDataModelObject("ReserveCurrent", False), Riskmaster.DataModel.ReserveCurrent)

        For Each objTransSplit As FundsTransSplit In objFunds.TransSplitList
            If (objTransSplit.RCRowId > 0) Then
                objReserveCurrent.MoveTo(objTransSplit.RCRowId)

            sDate = Riskmaster.Common.Conversion.ToDbDate(DateTime.Now)
            sSql = "SELECT BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE RC_ROW_ID=" & objReserveCurrent.RcRowId
            dReserveBalance = objReserveCurrent.Context.DbConnLookup.ExecuteDouble(sSql)

            If Not (objReserveCurrent Is Nothing) Then
                If ((objReserveCurrent.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ResStatusCode) = objReserveCurrent.Context.LocalCache.GetCodeId("C", "RESERVE_STATUS_PARENT")) And objFunds.FinalPaymentFlag = True Or (objReserveCurrent.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ResStatusCode) = objReserveCurrent.Context.LocalCache.GetCodeId("F", "RESERVE_STATUS_PARENT")) And objTransSplit.IsFirstFinal = True) Then ' if final payment is made on the last reserve, close the claim.                            
                    Try
                        objReserveCurrent.ReserveAmount = objReserveCurrent.ReserveAmount - dReserveBalance

                        objReserveCurrent.sUpdateType = "Funds"
                        'Before Saving - Make sure that the scripts are not fired again
                        objReserveCurrent.IsValidationScriptCalled = True
                        objReserveCurrent.FiringScriptFlag = 0
                        objReserveCurrent.Save()
                    Catch ex As Exception
                        Log.Write("Error in saving Reserve Current: " & ex.ToString())
                    End Try

                    Try
                        objNewReserveHistory = CType(objFunds.Context.Factory.GetDataModelObject("ReserveHistory", False), Riskmaster.DataModel.ReserveHistory)
                        objNewReserveHistory.ClaimId = objReserveCurrent.ClaimId
                        objNewReserveHistory.ClaimantEid = objReserveCurrent.ClaimantEid
                        objNewReserveHistory.UnitId = objReserveCurrent.UnitId
                        objNewReserveHistory.ReserveTypeCode = objReserveCurrent.ReserveTypeCode


                        objNewReserveHistory.PolicyCvgSeqNo = objReserveCurrent.PolicyCvgSeqNo


                        objNewReserveHistory.ReserveAmount = objReserveCurrent.ReserveAmount
                        objNewReserveHistory.ChangeAmount = -1 * dReserveBalance
                        objNewReserveHistory.BalanceAmount = 0
                        objNewReserveHistory.IncurredAmount = objReserveCurrent.CalcIncurred()
                        objNewReserveHistory.PaidTotal = objReserveCurrent.CalcPaid()
                        objNewReserveHistory.CollectionTotal = objReserveCurrent.CalcCollected()

                        objNewReserveHistory.BaseToClaimCurrRate = objReserveCurrent.BaseToClaimCurrRate
                        objNewReserveHistory.ClaimCurrencyCode = objReserveCurrent.ClaimCurrencyCode
                        objNewReserveHistory.ClaimToBaseCurrRate = objReserveCurrent.ClaimToBaseCurrRate
                        objNewReserveHistory.ClaimCurrReserveAmt = objNewReserveHistory.ReserveAmount * objNewReserveHistory.BaseToClaimCurrRate
                        objNewReserveHistory.ClaimCurrIncurrredAmt = objNewReserveHistory.IncurredAmount * objNewReserveHistory.BaseToClaimCurrRate
                        objNewReserveHistory.ClaimCurrCollectionTotal = objNewReserveHistory.CollectionTotal * objNewReserveHistory.BaseToClaimCurrRate
                        objNewReserveHistory.ClaimCurrPaidTotal = objNewReserveHistory.PaidTotal * objNewReserveHistory.BaseToClaimCurrRate
                        objNewReserveHistory.ClaimCurrBalanceAmt = dReserveBalance * objNewReserveHistory.BaseToClaimCurrRate

                        objNewReserveHistory.DateEntered = objReserveCurrent.DateEntered
                        objNewReserveHistory.EnteredByUser = objReserveCurrent.Context.RMUser.LoginName
                        objNewReserveHistory.DttmRcdAdded = sDate
                        objNewReserveHistory.AddedByUser = objReserveCurrent.AddedByUser
                        objNewReserveHistory.Reason = objReserveCurrent.Reason
                        objNewReserveHistory.DttmRcdLastUpd = sDate
                        objNewReserveHistory.UpdatedByUser = objReserveCurrent.UpdatedByUser
                        objNewReserveHistory.Crc = objReserveCurrent.Crc
                        objNewReserveHistory.ResStatusCode = objReserveCurrent.ResStatusCode
                        objNewReserveHistory.ReserveCategory = objReserveCurrent.ReserveCategory
                        

                        If (objReserveCurrent.Context.InternalSettings.SysSettings.MultiCovgPerClm <> 0) Then
                            objNewReserveHistory.CoverageLossId = objReserveCurrent.CoverageLossId
                        End If
                        'Before Saving - Make sure that the scripts are not fired again
                        objNewReserveHistory.IsValidationScriptCalled = True
                        objNewReserveHistory.FiringScriptFlag = 0
                        objNewReserveHistory.Save()
                    Catch ex As Exception
                        Log.Write("Error in saving Reserve History: " & ex.ToString())
                    Finally
                        objNewReserveHistory.Dispose()
                    End Try
                End If
            End If
            
            End If
        Next
objReserveCurrent.Dispose()

    End Sub