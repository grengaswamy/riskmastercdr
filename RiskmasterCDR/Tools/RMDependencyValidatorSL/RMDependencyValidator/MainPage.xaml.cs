﻿using System;
using System.Collections.Generic;
using System.Linq;      
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace RMDependencyValidator
{
    public partial class MainPage : UserControl
    {
        System.IO.Stream fileStream;
        XDocument objXd;

        public MainPage()
        {
            InitializeComponent();   
            if (App.Current.HasElevatedPermissions)
            {
                btnRefesh_Click(null, null);

                lblFileList.Visibility = System.Windows.Visibility.Visible;
                cbXmlList.Visibility = System.Windows.Visibility.Visible;
                btnRefresh.Visibility = System.Windows.Visibility.Visible;
                btnLoad.Visibility = System.Windows.Visibility.Visible;
                btnShowLoadDialogBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                lblFileList.Visibility = System.Windows.Visibility.Collapsed;
                cbXmlList.Visibility = System.Windows.Visibility.Collapsed;
                btnRefresh.Visibility = System.Windows.Visibility.Collapsed;
                btnLoad.Visibility = System.Windows.Visibility.Collapsed;
                btnShowLoadDialogBox.Visibility = System.Windows.Visibility.Visible;
            }

            //myButton.Click += new RoutedEventHandler(myButton_Click);

            TheList.SelectionChanged += new SelectionChangedEventHandler(lstTheList_Click);
        }

        //button to be made visible in SL 4 elevated permissions
        private void btnRefesh_Click(object sender, RoutedEventArgs e)
        { 
            try
            {
                DirectoryInfo di = new DirectoryInfo(@"D:\RS WIP files backup\dependency\Main\RMDependencyValidator\Dgml");
                var aFiles = di.EnumerateFiles();// Directory.GetFiles(System.IO.Path.GetFullPath(di.Name));
                
                foreach (FileInfo s in aFiles)
                {
                    //   MessageBox.Show("file: " + s.Name);
                    cbXmlList.Items.Add(s.Name);
                }
                
            }
            catch (Exception ex)
            {
                cbXmlList.Items.Clear();
                cbXmlList.Items.Add("ERROR: " + ex.Message);
                MessageBox.Show("ERROR: " + ex.Message);
            }

            
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {               
            MessageBox.Show(cbXmlList.SelectedIndex + " " + cbXmlList.SelectedItem);
        }

        private void btnShowLoadDialogBox_Click(object sender, RoutedEventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Dbml Files (.dgml)|*.dgml|Xml Files (.xml)|*.xml|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;      
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                txtFile.Text = openFileDialog1.File.Name;
                fileStream   = openFileDialog1.File.OpenRead();                
                //XmlReader reader = XmlReader.Create(fileStream);
                objXd = XDocument.Load(fileStream);                
                
                //XmlNamespaceManager r = new XmlNamespaceManager(new NameTable());
                //r.AddNamespace("xmlns",@"http://schemas.microsoft.com/vs/2009/dgml");
                //var test = objXd.XPathSelectElements(@"Nodes", objXd.Root.CreateNavigator());
                //var test = objXd.XPathSelectElement(@"DirectedGraph");
                
                var test = from c in objXd.Root.Elements("Nodes").Elements("Node")
                           where c.Attribute("Category").Value == "FileSystem.Category.FileOfType.dll"
                           //CodeSchema_Namespace
                           //"FileSystem.Category.FileOfType.exe"
                           select c; //.Attribute("Label").Value
                //var test2 = objXd.XPathEvaluate("DirectedGraph/Nodes/Node[@Id=(/DirectedGraph/Links/Link[@Source=(/DirectedGraph/Nodes/Node[@Category='FileSystem.Category.FileOfType.exe']/@Id)]/@Target)]/@Label");
                TheList.Items.Clear();
                foreach (XElement item in test)
                {
                    TheList.Items.Add(item.Attribute("Label").Value);
                }                

                //reader.Close();
                //fileStream.Close();                                       
            }

        }

        private void btnShowDependencies_Click(object sender, RoutedEventArgs e)
        {


        }

        private void lstTheList_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable <XElement> test2 =  objXd.XPathSelectElements
                (@"DirectedGraph/Nodes/Node[@Id=(/DirectedGraph/Links/Link[@Source=(/DirectedGraph/Nodes/Node[@Category='FileSystem.Category.FileOfType.dll']/@Id) and @Category='Contains']/@Target)]"); //@Label
            //FileSystem.Category.FileOfType.exe
            TheList2.Items.Clear();
            foreach (XElement item in test2)
            {
                TheList2.Items.Add(item.Attribute("Label").Value);
            }   
            //MessageBox.Show("lolz");

        }
        
        
        #region tempScrapcode

        //FileStream stream = openFileDialog1.File.OpenRead();
        //StringReader sr = new StringReader(stream.);


        // Open the selected file to read.
        //System.IO.Stream fileStream = openFileDialog1.File.OpenRead();

        
        //using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
        //{
        //    // Read the first line from the file and write it the textbox.
        //    //tbResults.Text = reader.ReadLine();
        //    //TheList.

        //}

        //if (reader.HasAttributes)
        //{

        //if that perticular node contains attribute then store it
        //for (int j = 0; j < reader.AttributeCount + 1; j++)
        //{

        //    strAttributeName[j] = reader.Name;

        //    reader.MoveToNextAttribute();

        //}

        //k++;

        //}
        //while (reader.Read())
        //{
        //    //Console.WriteLine(reader.Value);
        //    TheList.Items.Add(reader);

        //}

        //XDocument document = XDocument.Load("MapImages.xml");                                                                     

        //foreach (XElement element in document.Descendants("Area"))
        //{

        //    areaItem = new TreeViewItem();

        //    areaItem.Header = element.FirstAttribute.Value;

        //    rootItem.Items.Add(areaItem);



        //    foreach (XElement imageElement in element.Descendants("Image"))
        //    {

        //        TreeViewItem imageItem = new TreeViewItem();

        //        imageItem.Tag = imageElement.Value;

        //        imageItem.Header = imageElement.FirstAttribute.Value;

        //        areaItem.Items.Add(imageItem);

        //    }

        //}


        #endregion


    }
}
