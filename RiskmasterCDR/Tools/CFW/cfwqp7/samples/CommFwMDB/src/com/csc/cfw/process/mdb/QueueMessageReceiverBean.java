package com.csc.cfw.process.mdb;
/*****************************************************************************************************
 * $Id: QueueMessageReceiverBean.java,v 1.0 2009-09-21 dmcdaniel Exp $
 *
 * Copyright (c) 2009 Computer Sciences Corporation, Inc. All Rights Reserved.
 *
 * $State: Exp $
 *****************************************************************************************************/

import javax.ejb.EJBException;
import com.csc.cfw.util.CommFwSystem;

/**
 * Bean implementation class for Enterprise Bean: QueueMessageReceiver
 */
public class QueueMessageReceiverBean 
	extends
		com.csc.cfw.process.QueueMessageHandler
	implements
		javax.ejb.MessageDrivenBean,
		javax.jms.MessageListener 
{
	/**
	 * Generated serialVersionUID 
	 */
	private static final long serialVersionUID = 1682415575907043316L;

	/**
	 * Default listener index is for CFWAsynchRequestQueue
	 */
	static int DFLT_CFW_QUEUE_LISTENER_IDX = 1;
	
	private javax.ejb.MessageDrivenContext messageDrivenCtx;

	public QueueMessageReceiverBean()
	{
		super();
	}
	
	/**
	 * getMessageDrivenContext
	 */
	public javax.ejb.MessageDrivenContext getMessageDrivenContext()
	{
		return messageDrivenCtx;
	}
	/**
	 * setMessageDrivenContext
	 */
	public void setMessageDrivenContext(javax.ejb.MessageDrivenContext ctx) 
	{
		messageDrivenCtx = ctx;
	}
	/**
	 * ejbCreate
	 */
	public void ejbCreate() 
	{
		setMDB(true);
		int listenerIdx = DFLT_CFW_QUEUE_LISTENER_IDX;
		String s = (String)CommFwSystem.current().getProperty(SYSPROP_JMS_MDB_ListenerIdx);
		if (s != null && s.length() > 0)
		{
			listenerIdx = Integer.parseInt(s);
		}
		boolean handlerActive = initHandlerProperties("." + listenerIdx);
		setExternalListener(false);
		setPolling(false);
		if (handlerActive)
		{
			try
			{
				initMessageQueues();
				initReceiverConnection();
			}
			catch (Exception e)
			{
				logInfoPrintExceptionStack(e);
				throw new EJBException(e);
			}
		}
		logInfoPrintln("CFW MDB Queue initialized for: " + getRequestQueueName());
	}

	/**
	 * Called when a message is added the receiver queue. 
	 * Send the received message to Communications 
	 * Framework for processing.
	 * @see MessageListener#onMessage(Message)
	 * @param message CFW message from the receive queue.
	 */
	public void onMessage(javax.jms.Message msg)
	{
		try
		{
			processMessage(msg);
		}
		catch(Exception e)
		{
			if (isResultSavedOnError())
				return;
			else
			{
				throw new EJBException(e);
			}
		}
	}
	
	/**
	 * ejbRemove
	 */
	public void ejbRemove() 
	{
		resetConnections();
	}

	/**
	 *  
	 */
	protected void logInfoPrintln(String txt) 
	{
		logInfoSysOutPrintln(txt);
	}
	
	
}
