<?xml version="1.0" encoding="UTF-8"?>
<!--
109914 - Created new templates for conversion of ACORD to Native codes and vice-versa.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java">
	<xsl:template name="ACORDToNative">
		<xsl:param name="TableName"/>
		<xsl:param name="ACORDValue"/>
		<xsl:when test="normalize-space($TableName)!='' and normalize-space($ACORDValue)!=''">
			<xsl:value-of select="java:com.csc.cfw.codetrans.CodeTranslator.ExternalToNative(normalize-space($TableName),normalize-space($ACORDValue))"/>
		</xsl:when>					
	</xsl:template>
	<xsl:template name="NativeToACORD">
		<xsl:param name="TableName"/>
		<xsl:param name="NativeValue"/>
		<xsl:when test="normalize-space($TableName)!='' and normalize-space($NativeValue)!=''">
			<xsl:value-of select="java:com.csc.cfw.codetrans.CodeTranslator.NativeToExternal(normalize-space($TableName),normalize-space($NativeValue))"/>
		</xsl:when>			
	</xsl:template>
</xsl:stylesheet>
