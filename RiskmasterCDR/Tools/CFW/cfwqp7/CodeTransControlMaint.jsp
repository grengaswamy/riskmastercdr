<%@ include file="CodeTransReadTable.jsp" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<html>
    <link href="includes/INFUtil.css" type="text/css" rel="stylesheet">
    <head>
  	  <title>Code Translation Control File Maintenance - Communications Framework</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">		
        <script language="JavaScript">
            function AppendTD()
            {
                var td = document.createElement("td");
                td.className='';				
                return td;
            }

            function AppendRow()
            {
                var tblObj = document.getElementById('datatable');
                var tblBody = tblObj.childNodes;
                var cellHTML = "";
                var tr = document.createElement('tr');
                tr.setAttribute("bgcolor","white");
                //adding first column
                var td = AppendTD();
                //adding text box
                cellHTML = "<input type='text' name='TableName' value='' />";
                td.innerHTML = cellHTML;
                tr.appendChild(td);
                
                //adding second column
                var td = AppendTD("NativeColumn");
                //adding text box
                cellHTML = "<input type='text' name='NativeColumn' value='' />";
                td.innerHTML = cellHTML;
                tr.appendChild(td);
                
		        //adding third column
                var td = AppendTD("ExternalColumn");
                //adding text box
                cellHTML = "<input type='text' name='ExternalColumn' value='' />";
                td.innerHTML = cellHTML;
                tr.appendChild(td);
		        
                tblBody[0].appendChild(tr);
                return false;
            }
            
            function formsubmit()
            {
                var frm = document.ExeForm;
                document.ExeForm.submit();
            }
        </script>
    </head>
    <body BGCOLOR="#C0C0C0">
     <div align="center">
      <H2><u>Code Translation Control File Maintenance</u></H2>
      <form name="ExeFrm" action="CodeTransWriteTable.jsp" method="POST">
        <input TYPE="button" onClick="javascript:AppendRow();" value="Add" />
        <input TYPE="submit" value="Save"/>
        <a href="CodeTransMenu.jsp">Return to Code Translation Menu</a>	
        <br /><br />
<%
    try
    { 
        String path = (String)CommFwSystem.current().getProperty(CommFwSystem.SYSPROP_CodeTransPath);
        if (path != null && path.length() > 0)
        {
            path += '/' + CommFwSystem.CODE_TRANS_TABLE_LIST_FILENAME;
            String htmltable = getTableXML(path);
            out.print(htmltable);
        }
        else
        {
%>
        <h2>Code Translation file path property CodeTransPath not defined in CommFw.properties</h2>
<%
        }
    }
    catch (Exception e)
    {
%>
        <br/><br/>
        <b>Error accessing Code Translation control file: </b><%=e.toString() %>
<%
    }
%>
      </form>
     </div>
    </body>
</html>
