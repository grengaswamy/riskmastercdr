<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ******************************************************************* --%>
<%-- * This is a JSP to configure the Framework Utilities properties.  * --%>
<%-- ******************************************************************* --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.fw.util.*" %>
<%@ page errorPage="error.jsp" %>

<%@ include file="WEB-INF/jspf/FwSysFunc.jspf" %>
<%@ include file="WEB-INF/jspf/FwSysInit.jspf" %>
<%
        
        String contextPath = request.getPathInfo();
        if (contextPath != null) 
        {
        	if (contextPath.startsWith("/"))
        	{
        		contextPath = contextPath.substring(1);
        	}
        	int i = contextPath.indexOf('/');
        	String logCtx = null;
        	String fileName = null;
        	Logger lgr = null;
        	if (i > 0)
        	{
        		logCtx = contextPath.substring(0, i);
        		fileName = contextPath.substring(i + 1);
        	}
        	
        	SystemStatusConfigManager configMan = new SystemStatusConfigManager(fwSystem, null);
        	if (logCtx != null)
        	{
        		configMan.setLogger(logCtx);
        		lgr = configMan.getCurrentLogger();
        	}
        		
            if (lgr == null || fileName == null)
            {
                String err = "<FONT SIZE=+1><B>No log file found for display or invalid logging context</B></FONT>";
              %>
                <HTML>
                  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
                  <BODY BGCOLOR="#C0C0C0">
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" /><BR />
                            <%=err %><BR />
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" />
                  </BODY>
                </HTML>
              <%                
            }
            else
            try
            {
            	if (lgr.isLogDirectory())
            	{
            		fileName = lgr.getFileName() + '/' + fileName;
            	}
            	else
            	{
            		fileName = lgr.getFileName();
            	}
            	
                FileInputStream stream = new FileInputStream(fileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream, FwEnvelope.INTERNAL_ENCODING));
                Writer writer = response.getWriter();
                response.setContentType("text/plain");
                char[] buff = new char[16384];
                int cnt;
                while ((cnt = reader.read(buff)) >= 0)
                {
                    writer.write(buff, 0, cnt);
                }
                        
                reader.close();
                stream.close();
                writer.close();
            } 
            catch (IOException ex)
            {
                String err = "<FONT SIZE=+1><B>Error while loading Trace File: </B></FONT>" + ex.toString();
              %>
                <HTML>
                  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
                  <BODY BGCOLOR="#C0C0C0">
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" /><BR />
                            <%=err %><BR />
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" />
                  </BODY>
                </HTML>
              <%                
            }
        } /* if (!dispFileImage) */
%>
        