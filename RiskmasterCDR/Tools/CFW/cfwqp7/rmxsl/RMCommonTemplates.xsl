﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template name="SignonInfo">
    <xsl:param name="user-id"/>
    <xsl:param name="session-id"/>
    <xsl:param name="app-org"/>
    <xsl:param name="app-name"/>
    <xsl:param name="app-version"/>
    <!--mits 35925 start-->
    <xsl:param name="client-file"/>
    <!--mits 35925 end-->
    <SignonRs>
      <Status>
        <StatusCd/>
        <StatusDesc/>
      </Status>
      <SignonRoleCd/>
      <CustId>
        <SPName/>
        <CustLoginId><xsl:value-of select="$user-id"/></CustLoginId>
      </CustId>
      <GenSessKey/>
      <ClientDt/>
      <CustLangPref/>
      <ClientApp>
        <Org>
          <xsl:value-of select="$app-org"/>
        </Org>
        <Name>
          <xsl:value-of select="$app-name"/>
        </Name>
        <Version>
          <xsl:value-of select="$app-version"/>
        </Version>
        <!--mits 35925 start-->
        <com.csc_ClientFile>
			<xsl:value-of select="$client-file"/> 
		</com.csc_ClientFile>
        <!--mits 35925 end-->
      </ClientApp>
      <com.csc_SessKey>
        <xsl:value-of select="$session-id"/>
      </com.csc_SessKey>
      <ServerDt/>
      <Language/>
    </SignonRs>
  </xsl:template>
  <!-- reusable FormatParameterValue function -->
  <xsl:template name="FormatParameterValue">
    <xsl:param name="value"/>
    <xsl:param name="size"/>

    <xsl:variable name="DiffLength" select="number($size - string-length($value))"/>
    <xsl:choose>
      <xsl:when test="$DiffLength = 0">
        <xsl:value-of select="$value"/>
      </xsl:when>
      <xsl:when test="$DiffLength &lt; 0">
        <xsl:value-of select="substring($value,1,$size)"/>
      </xsl:when>
      <xsl:when test="$DiffLength &gt; 0">
        <xsl:value-of select="$value"/>
        <xsl:call-template name="Loop">
          <xsl:with-param name="cycles" select="$DiffLength"/>
          <xsl:with-param name="Tag" select="' '"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatIntParameterValue">
    <xsl:param name="value"/>
    <xsl:param name="size"/>

    <xsl:variable name="DiffLength" select="number($size - string-length($value))"/>
    <xsl:choose>
      <xsl:when test="$DiffLength = 0">
        <xsl:value-of select="$value"/>
      </xsl:when>
      <xsl:when test="$DiffLength &lt; 0">
        <xsl:value-of select="substring($value,1,$size)"/>
      </xsl:when>
      <xsl:when test="$DiffLength &gt; 0">
        <xsl:call-template name="Loop">
          <xsl:with-param name="cycles" select="$DiffLength"/>
          <xsl:with-param name="Tag" select="'0'"/>
        </xsl:call-template>
        <xsl:value-of select="$value"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="Loop">
    <xsl:param name="cycles" />
    <xsl:param name="Tag" />
    <xsl:choose>
      <xsl:when test="number($cycles) &gt; 0">
        <xsl:value-of select="$Tag"/>
        <xsl:call-template name="Loop">
          <xsl:with-param name="cycles" select="number($cycles)-1" />
          <xsl:with-param name="Tag" select="$Tag"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cvtdate">
    <xsl:param name="datefld"/>
    <xsl:choose>
      <xsl:when test="string-length($datefld)>0">
        <xsl:variable name="year" select="substring($datefld,2,2)"/>
        <xsl:variable name="month" select="substring($datefld,4,2)"/>
        <xsl:variable name="day" select="substring($datefld,6,2)"/>
        <xsl:value-of select="concat($month,'/',$day,'/',$year)"/>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="remove-leading-zeros">
    <xsl:param name="value"/>
    <xsl:choose>
      <xsl:when test="starts-with($value,'0.')">
        <xsl:value-of select="$value"/>
      </xsl:when>
      <xsl:when test="starts-with($value,'0')">
        <xsl:call-template name="remove-leading-zeros">
          <xsl:with-param name="value"
              select="substring-after($value,'0')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="FormatAmountValue">
    <xsl:param name="amount"/>
    <xsl:variable name="temp"  > 
      <xsl:call-template name="remove-leading-zeros">
        <xsl:with-param name="value" select="$amount"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:choose>  
      <xsl:when test="substring($temp, string-length($temp)-1) = '00'">
          <xsl:value-of select="format-number((substring($temp, 0,string-length($temp)-1)), '#.00')" />
      </xsl:when>
      <!--tkatsarski: 11/28/14 - RMA-4277: Deductible field displaying "NAN" error Start-->
      <xsl:when test="$temp = ''">
        <xsl:value-of select="'0.00'" />
      </xsl:when>
      <!--tkatsarski: 11/28/14 - RMA-4277: End-->
      <xsl:otherwise>
        <xsl:value-of select="format-number($temp,'#.00')" />
      </xsl:otherwise>
    </xsl:choose>   
  </xsl:template>
  <xsl:template name="ConvertLowercase">
    <xsl:param name="text"/>
    <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    
    <xsl:value-of select="translate($text, $uppercase, $smallcase)" />
  </xsl:template>

  <xsl:template name="ConvertUppercase">
    <xsl:param name="text"/>
    <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

    <xsl:value-of select="translate($text, $smallcase, $uppercase)" />
  </xsl:template>
</xsl:stylesheet>

