<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <CPPBCONTRq>
      <xsl:call-template name="test"/>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>BContract</RECORD__NAME>
        </RECORD__NAME__ROW>
        <POLICY__INFORMATION__REC__ROW>
          <PIF__LOCATION>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
          </PIF__LOCATION>
          <PIF__MASTER__CO__NUMBER>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
          </PIF__MASTER__CO__NUMBER>
          <PIF__SYMBOL>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd"/>
          </PIF__SYMBOL>
          <PIF__POLICY__NUMBER>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber"/>
          </PIF__POLICY__NUMBER>
          <PIF__MODULE>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
          </PIF__MODULE>
          <PROC__EFFECTIVE__DATE>
            <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
              <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/ContractTerm/EffectiveDt"/>
            </xsl:call-template>
          </PROC__EFFECTIVE__DATE>
          <PIF__ID>02</PIF__ID>
          <PROC__TRANSACTION__TYPE>
            <xsl:value-of select=".//PROC_TRANSACTION_TYPE"/>
          </PROC__TRANSACTION__TYPE>
          <PIF__ENDORSE__TEXT>
            <xsl:value-of select=".//PIF_ENDORSE_TEXT"/>
          </PIF__ENDORSE__TEXT>
          <PIF__REASON__AMEND__DIGIT1>
            <xsl:value-of select="//PIF_REASON_AMEND_DIGIT1"/>
          </PIF__REASON__AMEND__DIGIT1>
          <PIF__REASON__AMEND__DIGIT2>
            <xsl:value-of select="//PIF_REASON_AMEND_DIGIT2"/>
          </PIF__REASON__AMEND__DIGIT2>
          <PIF__REASON__AMEND__DIGIT3>
            <xsl:value-of select="//PIF_REASON_AMEND_DIGIT3"/>
          </PIF__REASON__AMEND__DIGIT3>
          <PIF__POLICY__OP__IND>
            <xsl:value-of select="//PIF_POLICY_OP_IND"/>
          </PIF__POLICY__OP__IND>
          <PIF__NON__RENEWAL__IND>
            <xsl:value-of select="//PIF_NON_RENEWAL_IND"/>
          </PIF__NON__RENEWAL__IND>
          <PIF__INSURED__NAME__IND>
            <xsl:value-of select="//PIF_INSURED_NAME_IND"/>
          </PIF__INSURED__NAME__IND>
          <PIF__INSURED__MAILING__IND>
            <xsl:value-of select="//PIF_INSURED_MAILING_IND"/>
          </PIF__INSURED__MAILING__IND>
          <PIF__CHANGE__EXPOSURE__IND>
            <xsl:value-of select="//PIF_CHANGE_EXPOSURE_IND"/>
          </PIF__CHANGE__EXPOSURE__IND>
          <PIF__ADD__COVERAGE__IND>
            <xsl:value-of select="//PIF_ADD_COVERAGE_IND"/>
          </PIF__ADD__COVERAGE__IND>
          <PIF__DELETE__COVERAGE__IND>
            <xsl:value-of select="//PIF_DELETE_COVERAGE_IND"/>
          </PIF__DELETE__COVERAGE__IND>
          <PIF__CANCL__DATE>0000000</PIF__CANCL__DATE>
        </POLICY__INFORMATION__REC__ROW>
      </BUS__OBJ__RECORD>
    </CPPBCONTRq>
  </xsl:template>
  <xsl:template name= "test">
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>action</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ACTION__RECORD__ROW>
        <ACTION>CPPBCONTINQRq</ACTION>
      </ACTION__RECORD__ROW>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>