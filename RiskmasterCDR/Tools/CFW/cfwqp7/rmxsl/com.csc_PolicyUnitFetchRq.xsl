﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="Lob">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd)"/>
    </xsl:variable>
    <xsl:variable name="baseLOBline">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_BaseLOBLine)"/>
    </xsl:variable>
    <xsl:variable name="IssueCode">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_IssueCd)"/>
    </xsl:variable>
    <xsl:variable name="UnitTypeIndVal">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitTypeInd']/OtherId)" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$IssueCode = 'M' or $UnitTypeIndVal = 'SU'">
        <xsl:call-template name="MRRq"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$baseLOBline = 'AL'">
            <xsl:call-template name="APVRq"/>
          </xsl:when>
          <xsl:when test="$baseLOBline = 'PL'">
            <xsl:call-template name="HPRq"/>
          </xsl:when>
        
          <xsl:when test="$baseLOBline = 'CL'">
            <xsl:call-template name="CPPRq"/>
          </xsl:when>
          <xsl:when test="$baseLOBline = 'WL'">
            <xsl:call-template name="WCVRq"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name= "APVRq" >
    <PAPUNITSRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>PAPUNITSINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>VehUnits</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ALR__VEHICLE__UNIT__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>      </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>     </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus"/>
            </KEY__RECORD__STATUS>
            <KEY__DRIVER__ID>     </KEY__DRIVER__ID>
            <KEY__DRIVER__CODE>      </KEY__DRIVER__CODE>
            <KEY__DRIVER__CODE__SEQUENCE>     </KEY__DRIVER__CODE__SEQUENCE>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </ALR__VEHICLE__UNIT__ROW>
      </BUS__OBJ__RECORD>
    </PAPUNITSRq>
  </xsl:template>

  <xsl:template name= "HPRq" >
    <HOWUNITSRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>HOWUNITSINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>HomeUnits</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PERSONAL__UNIT__REC__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>      </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>     </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus"/>
                <xsl:with-param name="size">1</xsl:with-param>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__NTPD__SEQ__NUM>       </KEY__NTPD__SEQ__NUM>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PERSONAL__UNIT__REC__ROW>
      </BUS__OBJ__RECORD>
    </HOWUNITSRq>
  </xsl:template>

  <xsl:template name= "FPRq" >
    <DFIUNITSRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>DFIUNITSINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>DFIUnits</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PERSONAL__UNIT__REC__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>      </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>     </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus"/>
                <xsl:with-param name="size">1</xsl:with-param>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__NTPD__SEQ__NUM>       </KEY__NTPD__SEQ__NUM>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PERSONAL__UNIT__REC__ROW>
      </BUS__OBJ__RECORD>
    </DFIUNITSRq>
  </xsl:template>

  <xsl:template name= "CPPRq" >
    <CPPUNITSRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>CPPUNITSINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>Unit</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ALR__IMPLEMENTED__RECORD__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>      </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>     </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus"/>
                <xsl:with-param name="size">1</xsl:with-param>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__RATE__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </KEY__RATE__STATE>
          </ALR__KEY>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <ALR__COMMON__FIELDS>
            <CHANGE__EFFECTIVE__DATE>0000000</CHANGE__EFFECTIVE__DATE>
          </ALR__COMMON__FIELDS>
        </ALR__IMPLEMENTED__RECORD__ROW>
      </BUS__OBJ__RECORD>
    </CPPUNITSRq>
  </xsl:template>

  <xsl:template name= "WCVRq" >
    <WCVSITESRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>WCVSITESINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>Sites</RECORD__NAME>
        </RECORD__NAME__ROW>
        <WC__SITE__FIELDS__RECORDS__ROW>
          <KEY-FIELDS-ROW>
            <KEY__LOCATION>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION>
            <KEY__MCO>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MCO>
            <KEY__POLICY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
            </KEY__POLICY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__POLICY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__POLICY__MODULE>
            <KEY__STATE__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </KEY__STATE__NUMBER>
            <KEY__SITE__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__SITE__NUMBER>
          </KEY-FIELDS-ROW>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
				<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/StateProvCd"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
        </WC__SITE__FIELDS__RECORDS__ROW>
      </BUS__OBJ__RECORD>
    </WCVSITESRq>
  </xsl:template>

  <xsl:template name= "MRRq">
    <BASISA05Rq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>BASISA05INQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>ISA05Units</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PSA__RECORDS__ROW>
          <PSA__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd"/>
                <xsl:with-param name="size">3</xsl:with-param>
              </xsl:call-template>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber"/>
                <xsl:with-param name="size">7</xsl:with-param>
              </xsl:call-template>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
                <xsl:with-param name="size">2</xsl:with-param>
              </xsl:call-template>
            </KEY__MODULE>
            <KEY__UNIT__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__UNIT__NUMBER>
          </PSA__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd"/>
                <xsl:with-param name="size">3</xsl:with-param>
              </xsl:call-template>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:call-template name="FormatParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
                <xsl:with-param name="size">2</xsl:with-param>
              </xsl:call-template>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE/>
            <PROC__EFFECTIVE__DATE>
              <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
                <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PSA__RECORDS__ROW>
      </BUS__OBJ__RECORD>
    </BASISA05Rq>
  </xsl:template>
</xsl:stylesheet>