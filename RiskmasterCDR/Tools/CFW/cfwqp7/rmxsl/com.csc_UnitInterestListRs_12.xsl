<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="baseLOBline" select="default"/>
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_UnitInterestListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <xsl:apply-templates select="/RMAcordPOINTJDBCRs/Rows/Row" >
            <xsl:with-param name="baseLOBline">
              <xsl:value-of select="normalize-space($baseLOBline)"/>
            </xsl:with-param>
          </xsl:apply-templates>
        </com.csc_UnitInterestListRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>

  <xsl:template match="/RMAcordPOINTJDBCRs/Rows/Row" >
    <xsl:param name="baseLOBline"/>

  
      <!--NA = ON-->


   <xsl:if test="$baseLOBline = 'WL'">
    
       <xsl:call-template name="WCInterestRow"/>
    

   </xsl:if>
    <xsl:if test="$baseLOBline = 'CL' or  $baseLOBline = 'AL' or  $baseLOBline = 'PL' or  $baseLOBline = 'GL'">
   
  
       <xsl:if test="string-length(@PI-INT-TYPE) > 0 and string-length(@PI-NAME) > 0">
         <xsl:call-template name="InterestRow"/>
       </xsl:if>

   </xsl:if>

  </xsl:template>
  

  <xsl:template name="InterestRow">
    <InsuredOrPrincipal>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Location</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(@PI-INT-LOCATION)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
		      <OtherIdTypeCd>com.csc_SeqNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(@PI-INT-SEQ)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <GeneralPartyInfo>
        <NameInfo>
          <CommlName>
            <CommercialName>
              <xsl:value-of select="normalize-space(@PI-NAME )"/>
            </CommercialName>
          </CommlName>
          <PersonName>
            <TitlePrefix/>
            <NameSuffix/>
            <Surname/>
            <GivenName/>
            <OtherGivenName/>
            <NickName/>
          </PersonName>
          <LegalEntityCd></LegalEntityCd>
          <TaxIdentity/>
        </NameInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1>
            <xsl:value-of select="normalize-space(@PI-ADDRESS)"/>
          </Addr1>
          <City>
            <xsl:value-of select="normalize-space(@PI-CITY)"/>
          </City>
          <StateProvCd>
            <xsl:value-of select="normalize-space(@PI-STATE)"/>
          </StateProvCd>
          <PostalCode/>
          <County/>
        </Addr>
      </GeneralPartyInfo>
      <InsuredOrPrincipalInfo>
        <InsuredOrPrincipalRoleCd>
          <xsl:value-of select="normalize-space(@PI-INT-TYPE)"/>
        </InsuredOrPrincipalRoleCd>
        <BusinessInfo>
          <SICCd></SICCd>
        </BusinessInfo>
      </InsuredOrPrincipalInfo>
    </InsuredOrPrincipal>
  </xsl:template>
  <xsl:template name="WCInterestRow">
    <InsuredOrPrincipal>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Location</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(@LOCATION)"/>
          </OtherId>
        </OtherIdentifier>
       
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_SeqNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(@DESC0SEQ)"/>
          </OtherId>
        </OtherIdentifier>

   


      </ItemIdInfo>
      <GeneralPartyInfo>
        <NameInfo>
          <CommlName>
            <CommercialName>
              <xsl:value-of select="normalize-space(@DESC0LINE1)"/>
            </CommercialName>
          </CommlName>
          <PersonName>
            <TitlePrefix/>
            <NameSuffix/>
            <Surname/>
            <GivenName/>
            <OtherGivenName/>
            <NickName/>
          </PersonName>
          <LegalEntityCd></LegalEntityCd>
          <TaxIdentity/>
        </NameInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1>
            <xsl:value-of select="normalize-space(@DESC0LINE3)"/>
          </Addr1>
          <City>
            <xsl:value-of select="normalize-space(@DESC0LINE4)"/>
          </City>
          <PostalCode/>
          <County/>
        </Addr>
      </GeneralPartyInfo>
      <InsuredOrPrincipalInfo>
        <InsuredOrPrincipalRoleCd>
          <xsl:value-of select="normalize-space(@USE0CODE)"/>
        </InsuredOrPrincipalRoleCd>
        <BusinessInfo>
          <SICCd></SICCd>
        </BusinessInfo>
      </InsuredOrPrincipalInfo>
    </InsuredOrPrincipal>
  </xsl:template>
</xsl:stylesheet>