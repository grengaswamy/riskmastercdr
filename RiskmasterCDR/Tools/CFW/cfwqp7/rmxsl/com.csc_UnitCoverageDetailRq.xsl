﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="Change-Status.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="Lob">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd)"/>
    </xsl:variable>
    <xsl:variable name="BaseLobLine">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_BaseLOBLine)"/>
    </xsl:variable>
    <xsl:variable name="IssueCode">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_IssueCd)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$IssueCode = 'M'">
        <xsl:call-template name="MRRq"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$BaseLobLine = 'AL'">
            <xsl:call-template name="APVRq"/>
          </xsl:when>
          <xsl:when test="$BaseLobLine = 'PL'">
            <xsl:call-template name="HPRq"/>
          </xsl:when>
          <xsl:when test="$Lob = 'FP'">
            <xsl:call-template name="FPRq"/>
          </xsl:when>
          <xsl:when test="$Lob = 'CPP'">
            <xsl:call-template name="CPPRq"/>
          </xsl:when>
          <xsl:when test="$Lob = 'WCV'">
            <xsl:call-template name="WCVRq"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name= "APVRq" >
    <PAPCOVERRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>PAPCOVERINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>VehCover</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ALR__VEHICLE__COVERAGE__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId"/>
            </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="Change-Status">
                <xsl:with-param name="status" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId)"/>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__DRIVER__ID>     </KEY__DRIVER__ID>
            <KEY__DRIVER__CODE>      </KEY__DRIVER__CODE>
            <KEY__DRIVER__CODE__SEQUENCE>     </KEY__DRIVER__CODE__SEQUENCE>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>00</BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </ALR__VEHICLE__COVERAGE__ROW>
      </BUS__OBJ__RECORD>
    </PAPCOVERRq>
  </xsl:template>

  <xsl:template name= "HPRq" >
    <HOWCOVERRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>HOWCOVERINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>HomeCover</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PERSONAL__COVERAGE__REC__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId"/>
            </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="Change-Status">
                <xsl:with-param name="status" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId)"/>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__NTPD__SEQ__NUM>       </KEY__NTPD__SEQ__NUM>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY></BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PERSONAL__COVERAGE__REC__ROW>
      </BUS__OBJ__RECORD>
    </HOWCOVERRq>
  </xsl:template>

  <xsl:template name= "FPRq" >
    <DFICOVERRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>DFICOVERINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>DFICover</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PERSONAL__COVERAGE__REC__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId"/>
            </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="Change-Status">
                <xsl:with-param name="status" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId)"/>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__NTPD__SEQ__NUM>       </KEY__NTPD__SEQ__NUM>
          </ALR__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY></BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PERSONAL__COVERAGE__REC__ROW>
      </BUS__OBJ__RECORD>
    </DFICOVERRq>
  </xsl:template>

  <xsl:template name= "CPPRq" >
    <CPPCOVERRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>CPPCOVERINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>Coverage</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ALR__IMPLEMENTED__RECORD__ROW>
          <ALR__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__INSURANCE__LINE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            </KEY__INSURANCE__LINE>
            <KEY__PRODUCT>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
            </KEY__PRODUCT>
            <KEY__RISK__LOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
            </KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
            </KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVERAGE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId"/>
            </KEY__COVERAGE>
            <KEY__COVERAGE__SEQUENCE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__COVERAGE__SEQUENCE>
            <KEY__ITEM>      </KEY__ITEM>
            <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
            <KEY__RECORD__STATUS>
              <xsl:call-template name="Change-Status">
                <xsl:with-param name="status" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId)"/>
              </xsl:call-template>
            </KEY__RECORD__STATUS>
            <KEY__RATE__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </KEY__RATE__STATE>
          </ALR__KEY>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY></BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <ALR__COMMON__FIELDS>
            <CHANGE__EFFECTIVE__DATE>0000000</CHANGE__EFFECTIVE__DATE>
          </ALR__COMMON__FIELDS>
        </ALR__IMPLEMENTED__RECORD__ROW>
      </BUS__OBJ__RECORD>
    </CPPCOVERRq>
  </xsl:template>

  <xsl:template name= "WCVRq" >
    <WCVCLASSRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>WCVCLASSINQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>Class</RECORD__NAME>
        </RECORD__NAME__ROW>
        <WC__EMP__CLASS__RECORDS__ROW>
          <KEY__FIELDS__ROW>
            <KEY__LOCATION>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION>
            <KEY__MCO>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MCO>
            <KEY__POLICY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__POLICY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__POLICY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__POLICY__MODULE>
            <KEY__STATE__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </KEY__STATE__NUMBER>&gt;
            <KEY__SITE__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__SITE__NUMBER>&gt;
            <KEY__EMP__COVERAGE__SEQ>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__EMP__COVERAGE__SEQ>
            <KEY__EMP__CLASS__CODE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId"/>
            </KEY__EMP__CLASS__CODE>
            <KEY__EMP__CLASS__V__C__INDICATOR> </KEY__EMP__CLASS__V__C__INDICATOR>
            <KEY__EMP__CLASS__DESCRIPTION__SEQ>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__EMP__CLASS__DESCRIPTION__SEQ>
          </KEY__FIELDS__ROW>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
        </WC__EMP__CLASS__RECORDS__ROW>
      </BUS__OBJ__RECORD>
    </WCVCLASSRq>
  </xsl:template>

  <xsl:template name= "MRRq">
    <BASISA15Rq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>BASISA15INQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>ISA15Cover</RECORD__NAME>
        </RECORD__NAME__ROW>
        <PSA15__RECORDS__ROW>
          <PSA__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__UNIT__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            </KEY__UNIT__NUMBER>
            <KEY__COVG__SEQ__NO>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>
            </KEY__COVG__SEQ__NO>
            <KEY__TRANS__SEQ__NO>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSeq']/OtherId"/>
            </KEY__TRANS__SEQ__NO>
          </PSA__KEY>
          <BASIC__CONTRACT__INFO>
            <BC__LINE__OF__BUSINESS>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd"/>
            </BC__LINE__OF__BUSINESS>
            <BC__POLICY__COMPANY></BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </BC__STATE>
          </BASIC__CONTRACT__INFO>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE/>
            <PROC__EFFECTIVE__DATE></PROC__EFFECTIVE__DATE>
          </PROCESSING__INFO>
        </PSA15__RECORDS__ROW>
      </BUS__OBJ__RECORD>
    </BASISA15Rq>
  </xsl:template>
</xsl:stylesheet>