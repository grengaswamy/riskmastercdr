﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="lob" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/LOBCd)"/>
    <xsl:variable name="baseLOBline" select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_BaseLOBLine)"/>
    <xsl:if test="$baseLOBline = 'AL'">
      <PAPDRVDTRq>
        <BUS__OBJ__RECORD>
          <RECORD__NAME__ROW>
            <RECORD__NAME>action</RECORD__NAME>
          </RECORD__NAME__ROW>
          <ACTION__RECORD__ROW>
            <ACTION>PAPDRVDTINQRq</ACTION>
          </ACTION__RECORD__ROW>
        </BUS__OBJ__RECORD>
        <xsl:call-template name="DriverData">
          <xsl:with-param name="insline" select="$lob"/>
        </xsl:call-template>
      </PAPDRVDTRq>
    </xsl:if>
    <xsl:if test="$baseLOBline = 'CL'">
      <PAPDCADTRq>
        <BUS__OBJ__RECORD>
          <RECORD__NAME__ROW>
            <RECORD__NAME>action</RECORD__NAME>
          </RECORD__NAME__ROW>
          <ACTION__RECORD__ROW>
            <ACTION>PAPDCADTINQRq</ACTION>
          </ACTION__RECORD__ROW>
        </BUS__OBJ__RECORD>
        <xsl:call-template name="DriverData">
          <xsl:with-param name="insline" select="'CA'"/>
        </xsl:call-template>
      </PAPDCADTRq>
    </xsl:if>
  </xsl:template>

  <xsl:template name="DriverData">
    <xsl:param name="insline" />
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>Drvdtl</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ALR__VEHICLE__DRIVER__ROW>
        <ALR__KEY>
          <KEY__LOCATION__COMPANY>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
          </KEY__LOCATION__COMPANY>
          <KEY__MASTER__COMPANY>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
          </KEY__MASTER__COMPANY>
          <KEY__SYMBOL>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/CompanyProductCd"/>
          </KEY__SYMBOL>
          <KEY__POLICY__NUMBER>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/PolicyNumber"/>
          </KEY__POLICY__NUMBER>
          <KEY__MODULE>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
          </KEY__MODULE>
          <KEY__INSURANCE__LINE>
            <xsl:value-of select="$insline"/>
          </KEY__INSURANCE__LINE>
          <KEY__PRODUCT>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
          </KEY__PRODUCT>
          <KEY__RISK__LOCATION__NUMBER>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </KEY__RISK__LOCATION__NUMBER>
          <KEY__RISK__SUBLOCATION__NUMBER>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </KEY__RISK__SUBLOCATION__NUMBER>
          <KEY__UNIT__NUMBER>     </KEY__UNIT__NUMBER>
          <KEY__COVERAGE>      </KEY__COVERAGE>
          <KEY__COVERAGE__SEQUENCE>     </KEY__COVERAGE__SEQUENCE>
          <KEY__ITEM>      </KEY__ITEM>
          <KEY__ITEM__SEQUENCE>     </KEY__ITEM__SEQUENCE>
          <KEY__RECORD__STATUS>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_DriverStatus"/>
          </KEY__RECORD__STATUS>
          <KEY__DRIVER__ID>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId"/>
          </KEY__DRIVER__ID>
          <KEY__DRIVER__CODE>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverType']/OtherId"/>
          </KEY__DRIVER__CODE>
          <KEY__DRIVER__CODE__SEQUENCE>     </KEY__DRIVER__CODE__SEQUENCE>
        </ALR__KEY>
        <BASIC__CONTRACT__INFO>
          <BC__LINE__OF__BUSINESS>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/LOBCd"/>
          </BC__LINE__OF__BUSINESS>
          <BC__POLICY__COMPANY>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
          </BC__POLICY__COMPANY>
          <BC__STATE>
            <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverState']/OtherId"/>
          </BC__STATE>
        </BASIC__CONTRACT__INFO>
        <PROCESSING__INFO>
          <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
          <PROC__EFFECTIVE__DATE/>
        </PROCESSING__INFO>
      </ALR__VEHICLE__DRIVER__ROW>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>