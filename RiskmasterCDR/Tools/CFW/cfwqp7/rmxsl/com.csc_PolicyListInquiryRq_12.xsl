<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:param name="client-file" select="default" />
  <xsl:template match="/">
    <POINTJDBC>
      <xsl:call-template name="ProcName" />
    </POINTJDBC>
  </xsl:template>
  <xsl:template name= "ProcName" >
    <xsl:variable name="Name">
      <xsl:call-template name="replace-string">
        <xsl:with-param name="text" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
        <xsl:with-param name="from" select="'%'"/>
        <xsl:with-param name="to" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="loss-date">
      <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
        <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/LossDt"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="lossdate">
      <xsl:choose>
        <xsl:when test="$loss-date=''">0000000</xsl:when>
        <xsl:otherwise><xsl:value-of select='$loss-date'/></xsl:otherwise> <!-- default value -->
      </xsl:choose>
    </xsl:variable>
    <SQLStmt>
      <xsl:choose>
        <!--NA = ON-->
        <xsl:when test="$client-file = 'true'">
          CALL RSKDBIO021('<xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/SignonRq/SignonPswd/CustId/CustLoginId"></xsl:with-param>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="substring-after($Name,'|')"/>
            <xsl:with-param name="size">60</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">1</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="substring-before($Name,'|')"/>
            <xsl:with-param name="size">40</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">1</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD//ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/StateProvCd"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/CustPermId"/>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City"/>
            <xsl:with-param name="size">28</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">4</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/ContractNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">32</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity"/>
            <xsl:with-param name="size">11</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/GroupId"/>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="$lossdate"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatIntParameterValue">
            <xsl:with-param name="value" select="number(/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_RoutingInfo) - 1"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>')
        </xsl:when>
        <xsl:otherwise>
          <!--NA = OFF-->
          CALL RSKDBIO028('<xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/SignonRq/SignonPswd/CustId/CustLoginId"></xsl:with-param>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="concat(substring-before($Name,'|'),' ',substring-after($Name,'|'))"/>
            <xsl:with-param name="size">30</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD//ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/StateProvCd"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/CustPermId"/>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">28</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
            <xsl:with-param name="size">10</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">4</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/ContractNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
	              <xsl:with-param name="value" select="$lossdate"/>
	              <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatIntParameterValue">
            <xsl:with-param name="value" select="number(/ACORD/ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_RoutingInfo) - 1"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>')
        </xsl:otherwise>
      </xsl:choose>
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>