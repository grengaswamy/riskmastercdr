<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:template name="DecomposeUserArea">
    <!--<xsl:value-of select="string-length(normalize-space(substring(//USER__AREA,5081,23)))"/>-->
    <!-- This template will loop thru all the USER-AREA and output all name-value tags -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1,23)))>0">
      <xsl:if test="not(normalize-space(substring(//USER__AREA,1,23)) ='GU_CLASS_DESCRIPTION')">
        <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
        <xsl:call-template name="FormatAcordName">
          <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/>
        </xsl:call-template>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:value-of select="normalize-space(substring(//USER__AREA,24,16))"/>
        <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
        <xsl:call-template name="FormatAcordName">
          <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/>
        </xsl:call-template>
        <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1,23))"/>-->
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      </xsl:if>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,41,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,41,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,41,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,64,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,41,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,41,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,81,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,81,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,81,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,81,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,81,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,121,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,144,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,184,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,201,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,224,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,264,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,281,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,304,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,344,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,361,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,384,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,424,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,441,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,464,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,504,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,521,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,544,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,561,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,584,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,601,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,624,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,641,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,664,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,681,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,704,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,721,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,744,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,761,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,784,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,801,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,824,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,841,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,864,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,881,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,904,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,921,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,944,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,961,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,984,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1001,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1024,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1041,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1064,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1121,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1144,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1184,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1201,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1224,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1264,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1281,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1304,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1344,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1361,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1384,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1424,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1441,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1464,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1504,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1521,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1544,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1561,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1584,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1601,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1624,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1641,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1641,23))"/>
      </xsl:call-template>
     <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1664,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1681,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1704,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1721,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1744,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1761,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1784,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1801,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1824,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1841,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1864,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1881,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1904,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1921,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1944,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1961,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1984,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2001,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2024,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2041,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2064,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2121,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2144,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2184,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2201,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2224,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2264,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2281,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2304,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2344,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2361,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2384,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2424,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2441,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2464,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2504,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2521,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2544,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2561,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2584,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2601,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2624,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2641,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2664,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2681,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2704,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2721,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2744,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2761,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2784,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2801,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2824,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2841,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2864,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2881,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2904,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2921,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2944,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2961,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2984,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3001,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3024,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3041,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3064,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3121,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3144,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3184,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3201,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3224,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3264,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3281,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3304,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3344,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3361,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3384,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3424,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3441,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3464,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3504,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3521,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3544,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3561,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3584,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3601,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3624,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3641,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3664,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3681,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3704,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3721,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3744,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3761,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3784,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3801,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3824,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3841,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3864,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3881,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3904,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3921,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3944,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3961,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3984,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4001,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4024,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4041,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4064,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4121,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4144,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4121,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4184,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4201,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4224,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4201,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4264,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4281,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4304,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4281,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4344,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4361,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4384,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4361,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4424,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4441,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4464,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4441,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4504,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4521,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4544,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4521,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4561,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4584,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4561,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4601,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4624,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4601,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4641,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4664,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4641,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4681,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4704,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4681,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4721,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4744,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4721,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4761,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4784,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4761,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4801,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4824,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4801,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4841,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4864,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4841,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4881,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4904,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4881,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4921,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4944,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4921,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4961,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4984,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4961,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5001,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5024,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5001,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5041,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5064,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5041,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5104,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template name="DecomposeUserArea_PIJ">
    <!--<xsl:value-of select="string-length(normalize-space(substring(//USER__AREA,5081,23)))"/>-->
    <!-- This template will loop thru all the USER-AREA and output all name-value tags -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1,23)))>0">
      <xsl:if test="not(normalize-space(substring(//USER__AREA,1,23)) ='GU_CLASS_DESCRIPTION')">
        <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
        <xsl:call-template name="FormatAcordName">
          <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/>
        </xsl:call-template>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,24,16))"/> -->
        <xsl:value-of select="normalize-space(substring(//USER__AREA,24, 30))"/>
        <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
        <xsl:call-template name="FormatAcordName">
          <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/>
        </xsl:call-template>
        <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1,23))"/>-->
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      </xsl:if>
    </xsl:if>
    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,41,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA, 55,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,41,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,55,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,41,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,64,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,78, 30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,41,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,55,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,41,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,81,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,109,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,81,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,109,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,81,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,104,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,132,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,81,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,109,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,81,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,121,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,163,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,163,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,144,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,186,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,163,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,161,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,217,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,217,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,184,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,240,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,217,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,201,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,271,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,271,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,224,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,294,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,271,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,241,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,325,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,325,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,264,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,348,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,325,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,281,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,379,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,379,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,304,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,402,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,379,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,321,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,433,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,433,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,344,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,456,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,433,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,361,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,487,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,487,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,384,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,510,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,487,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,401,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,541,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,541,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,424,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,564,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,541,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,441,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,595,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,595,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,464,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,618,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,595,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,481,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,649,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,481,23))"/>-->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,649,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,504,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,672,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,649,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,521,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,703,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,703,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,726,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,703,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,561,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,757,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,757,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,584,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,780,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,757,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,601,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,811,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,811,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,624,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,834,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,811,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,641,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,865,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,865,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,888,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,641,23))"/>-->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,865,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,681,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,919,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,919,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,704,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,942,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,919,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,721,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,973,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,973,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,744,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,996,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,973,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,761,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1027,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1027,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,784,16))"/>-->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1050,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,761,23))"/>-->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1027,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,801,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1081,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,824,16))"/>-->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1104,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,841,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1135,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1135,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,864,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1158,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,841,23))"/>  -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1135,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,881,23)))>0">-->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1189,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1189,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,904,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1212,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1189,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,921,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1243,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--     <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1243,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,944,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1266,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1243,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,961,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1297,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1297,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,984,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1320,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1297,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1001,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1351,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1351,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1024,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1374,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1351,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1041,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1405,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1405,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1064,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1428,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1405,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1081,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1459,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1459,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--     <xsl:value-of select="normalize-space(substring(//USER__AREA,1104,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1482,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1459,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1121,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1513,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1513,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1144,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1536,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1513,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1161,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1567,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1567,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1184,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1590,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1567,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1201,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1621,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1621,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1224,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1644,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1621,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1241,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1675,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1675,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,1264,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1698,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1675,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1281,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1729,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1729,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,1304,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1752,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1729,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1321,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1783,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--       <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1783,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1344,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1806,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1783,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1361,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1837,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--     <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1837,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--  <xsl:value-of select="normalize-space(substring(//USER__AREA,1384,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1860,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1837,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1401,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1891,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1891,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,1424,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1914,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--     <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1891,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1441,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1945,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1945,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,1464,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,1968,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1945,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1481,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1999,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1999,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1504,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2022,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1999,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1521,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2053,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2053,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1544,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2076,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--   <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2053,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1561,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2107,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2107,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!-- <xsl:value-of select="normalize-space(substring(//USER__AREA,1584,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2130,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2107,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1601,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2161,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1624,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2184,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!-- <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1641,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2215,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2215,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1664,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2238,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2215,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,1681,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2269,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2269,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1704,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2292,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2269,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1721,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2323,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2323,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1744,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2346,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2323,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1761,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2377,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2377,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1784,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2400,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2377,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1801,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2431,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--   <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2431,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1824,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2454,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--   <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2431,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1841,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2485,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2485,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1864,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2508,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2485,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1881,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2539,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2539,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1904,16))"/>-->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2562,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2539,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1921,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2593,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2593,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1944,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2616,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2593,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,1961,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2647,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2647,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1984,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2670,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2647,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,1961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2001,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2701,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2701,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2024,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2724,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2701,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2041,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2755,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2755,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2064,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2778,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2755,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2081,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2809,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2809,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2104,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2832,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2809,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2121,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2863,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2863,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2144,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2886,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2863,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2161,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2917,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2917,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2184,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2940,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2917,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2201,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2971,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2971,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2224,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,2994,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2971,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2241,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3025,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3025,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2264,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3048,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3025,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2281,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3079,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3079,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2304,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3102,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3079,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2321,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3133,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3133,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2344,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3156,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3133,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2361,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3187,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3187,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2384,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3210,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3187,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2401,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3241,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2424,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3264,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2441,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3295,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3295,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2464,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3318,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3295,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2481,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3349,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3349,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2504,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3372,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3349,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2521,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3403,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3403,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2544,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3426,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3403,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2561,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3457,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3457,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2584,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3480,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3457,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2601,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3511,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3511,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2624,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3534,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3511,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,2641,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3565,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3565,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2664,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3588,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3565,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2681,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3619,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3619,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2704,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3642,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3619,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2721,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3673,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3673,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2744,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3696,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3673,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2761,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3727,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3727,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2784,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3750,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3727,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2801,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3781,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3781,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2824,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3804,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3781,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2841,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3835,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3835,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2864,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3858,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3835,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2881,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3889,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3889,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2904,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3912,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3889,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--   <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2921,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3943,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3943,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2944,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,3966,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3943,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,2961,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3997,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2961,23))"/>-->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3997,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2984,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4020,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,2961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3997,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,2961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3001,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4051,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4051,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3024,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4074,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4051,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3041,23)))>0"> -->
    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3041,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4105,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4105,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3064,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4128,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3041,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4105,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3081,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4159,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4159,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3104,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4182,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3081,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4159,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3121,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4213,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4213,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3144,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4236,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3121,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4213,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3161,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4267,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4267,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3184,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4290,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3161,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4267,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3201,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4321,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3224,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4344,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3201,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4321,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3241,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4375,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4375,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3264,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4398,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3241,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4375,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3281,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4429,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4429,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3304,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4452,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3281,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4429,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3321,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4483,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4483,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3344,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4506,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3321,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4483,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3361,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4537,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4537,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3384,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4560,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3361,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4537,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!-- <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3401,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4591,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3401,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4591,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3424,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4614,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,1,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4591,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3441,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4645,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4645,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3464,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4668,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3441,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4645,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3481,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4699,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4699,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3504,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4722,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3481,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4699,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3521,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4753,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4753,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3544,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4776,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3521,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4753,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--  <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3561,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4807,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4807,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3584,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4830,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3561,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4807,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3601,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4861,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4861,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3624,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4884,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3601,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4867,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3641,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4915,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4915,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3664,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4938,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3641,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4915,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3681,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4969,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4969,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3704,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,4992,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3681,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4969,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3721,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5023,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5023,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3744,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5046,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3721,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5023,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3761,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5077,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5077,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3784,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5100,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3761,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5077,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3801,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5131,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5131,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3824,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5154,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3801,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5131,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3841,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5185,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5185,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3864,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5208,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3841,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5185,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3881,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5239,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5239,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3904,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5262,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3881,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5239,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,3921,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5293,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5293,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3944,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5316,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--  <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3921,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5293,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,3961,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5347,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5347,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3984,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5370,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,3961,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5347,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,3961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4001,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5401,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4024,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5424,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <!--<xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,4001,23))"/> -->
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5401,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,4041,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5455,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5455,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4064,16))"/> -->
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5478,16))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5455,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <!--<xsl:if test="string-length(normalize-space(substring(//USER__AREA,4081,23)))>0"> -->
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5509,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5509,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5532,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5509,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5563,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5563,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5586,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5563,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4121,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5617,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5617,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5640,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5617,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4161,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5671,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5671,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5694,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5671,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4201,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>

    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5725,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5725,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5748,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5725,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4241,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5779,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5779,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5802,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5779,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4281,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5833,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5833,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5856,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5833,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4321,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5887,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5887,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5910,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5887,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4361,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5941,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5941,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,5964,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5941,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4401,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,5995,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5995,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6018,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,5995,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4441,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6049,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6049,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6072,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6049,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4481,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6103,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6103,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6126,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6103,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4521,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6157,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6157,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6180,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6157,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4561,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6211,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6211,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6234,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6211,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4601,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6265,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6265,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6288,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6265,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4641,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6319,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6319,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6342,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6319,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4681,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6373,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6373,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6396,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6373,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4721,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6427,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6427,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6450,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6427,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4761,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6481,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6504,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6481,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4801,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6535,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6535,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6558,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6535,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4841,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6589,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6589,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6612,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6589,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4881,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6643,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6643,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6666,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6643,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4921,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6697,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6697,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6720,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6697,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,4961,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6751,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6751,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6774,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6751,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5001,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6805,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6805,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6828,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6805,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5041,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(substring(//USER__AREA,6859,23)))>0">
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6859,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="normalize-space(substring(//USER__AREA,6882,30))"/>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:call-template name="FormatAcordName">
        <xsl:with-param name="tagname" select="normalize-space(substring(//USER__AREA,6859,23))"/>
      </xsl:call-template>
      <!--<xsl:value-of select="normalize-space(substring(//USER__AREA,5081,23))"/>-->
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="FormatAcordName">
    <xsl:param name="tagname"/>
    <xsl:choose>
      <xsl:when test="string-length($tagname)>0">
        <xsl:value-of select="concat('com.csc_',$tagname)"/>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>