<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="$user-id"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="$session-id"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="$app-org"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="$app-name"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="$app-version"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="$rq-uid"/>
        </RqUID>
        <com.csc_PolicyChangeDateListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <com.csc_PolicyAmendmentList>
            <xsl:apply-templates select="/RMAcordPOINTJDBCRs/Rows/Row">
            </xsl:apply-templates>
          </com.csc_PolicyAmendmentList>
        </com.csc_PolicyChangeDateListRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template match="/RMAcordPOINTJDBCRs/Rows/Row" >
    <xsl:call-template name="EffDateRow"/>     
  </xsl:template>

  <xsl:template name="EffDateRow">
    <com.csc_PolicyAmendment>
      <com.csc_ReasonAmended>
        <xsl:value-of select="@REASAMNDCD"/>
      </com.csc_ReasonAmended>
      <com.csc_EnterDate>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="@ENTERDATE"/>
        </xsl:call-template>
      </com.csc_EnterDate>
      <com.csc_ChangeEffDate>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="@EFFECTDATE"/>
        </xsl:call-template>
      </com.csc_ChangeEffDate>
    </com.csc_PolicyAmendment>      
  </xsl:template> 
  
</xsl:stylesheet>