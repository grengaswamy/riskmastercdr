<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
    <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <POINTJDBC>
      <xsl:call-template name="ProcName" />
    </POINTJDBC>
  </xsl:template>
  <xsl:template name= "ProcName" >
                <xsl:variable name="loss-date">
		<xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
			<xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/LossDt"/>
		</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="lossdate">
		  <xsl:choose>
			<xsl:when test="$loss-date=''">0000000</xsl:when>
			<xsl:otherwise><xsl:value-of select='$loss-date'/></xsl:otherwise> <!-- default value -->
		</xsl:choose>
                </xsl:variable>
    <SQLStmt>
      <!--mits 35925 start-->
      CALL RSKDBIO066('<xsl:call-template name="FormatParameterValue">
        <!--mits 35925 end-->
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
          <xsl:with-param name="size">2</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FormatParameterValue">
          <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
          <xsl:with-param name="size">2</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FormatParameterValue">
          <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/CompanyProductCd"/>
          <xsl:with-param name="size">3</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FormatParameterValue">
          <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/PolicyNumber"/>
          <xsl:with-param name="size">7</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FormatParameterValue">
          <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
          <xsl:with-param name="size">2</xsl:with-param>
        </xsl:call-template>
	<xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
	<xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">31</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">31</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">29</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">3</xsl:with-param>
         </xsl:call-template>
	 <xsl:call-template name="FormatParameterValue">
		  <xsl:with-param name="value" select="$lossdate"/>
		  <xsl:with-param name="size">7</xsl:with-param>
	</xsl:call-template>')
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>