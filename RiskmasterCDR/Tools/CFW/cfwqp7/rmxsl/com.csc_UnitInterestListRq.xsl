<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    
    <POINTJDBC>
      <xsl:call-template name="ProcName" />
    </POINTJDBC>
  </xsl:template>

  <xsl:template name= "ProcName" >
    <xsl:variable name="baseLOBline">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_BaseLOBLine)"/>
    </xsl:variable>
 
    <SQLStmt>
      <xsl:choose>
      
      
        <xsl:when test="$baseLOBline = 'WL' ">


          <xsl:variable name="PolicyNum" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/PolicyNumber"/>
          <xsl:variable name="Symbol" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/CompanyProductCd" />

          <xsl:variable name="RiskLoc"  select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>

          <xsl:variable name="MasterCo"     select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
          <xsl:variable name="Module"  select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
          <xsl:variable name="Unitno"  select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
          SELECT * FROM PMSP1200    WHERE SYMBOL= '<xsl:value-of select="$Symbol"/>' and POLICY0NUM='<xsl:value-of select="$PolicyNum"/>'
          and MODULE=  '<xsl:value-of select="$Module"/>' and MASTER0CO= '<xsl:value-of select="$MasterCo"/>' and LOCATION = '<xsl:value-of select="$RiskLoc"/>'
          and  USE0LOC =  '<xsl:value-of select="$Unitno"/>'  and FLAG = 'Y' and not(USE0CODE in ( select
          INTTYPE from TBWC77) and DEEMSTAT = 'V')


        </xsl:when>
        <xsl:otherwise>
          <!--mits 35925 start-->
         CALL RSKDBIO068('<xsl:call-template name="FormatParameterValue">
           <!--mits 35925 end-->
           <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/CompanyProductCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/PolicyNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">31</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">31</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">29</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">6</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>')
        </xsl:otherwise>
      </xsl:choose>
    </SQLStmt>
  </xsl:template>
  
 
</xsl:stylesheet>