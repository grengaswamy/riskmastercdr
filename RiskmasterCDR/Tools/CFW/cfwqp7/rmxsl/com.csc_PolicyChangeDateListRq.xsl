<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <POINTJDBC>
      <xsl:call-template name="ProcName" />
    </POINTJDBC>
  </xsl:template>
  <xsl:template name= "ProcName" >   
    <SQLStmt>
      SELECT ENTERDATE, EFFECTDATE, REASAMNDCD FROM PMSP0200A WHERE
        SYMBOL = '<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/CompanyProductCd"/>' AND
        POLICY0NUM = '<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/PolicyNumber"/>' AND
        MODULE = '<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>' AND 
	LOCATION  = '<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>' AND
        MASTER0CO = '<xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>'        
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>