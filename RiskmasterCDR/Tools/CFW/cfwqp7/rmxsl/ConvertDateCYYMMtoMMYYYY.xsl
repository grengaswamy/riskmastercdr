<?xml version="1.0" encoding="UTF-8"?>
<!--
//********************************************************************************************
//* Programmer: Anwar, Taleeb       Date: 05/14/2012				                		 *
//* MITS: 30910												                         		 *
//* Package   :																		    	 *
//* Description:																		     *
//*				 Converts date from format CYYMM to MM/YYYY									 *
//*				 The value of C=1 denotes year 2000 and above								 *
//********************************************************************************************
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template name="cvtdateCYYMMtoMMYYYY">
		<xsl:param name="datefld"/>
		<xsl:choose>
			<xsl:when test="string-length($datefld) = 5">
				<xsl:variable name="year" select="substring($datefld,2,2)"/>
				<xsl:variable name="cent" select="substring($datefld,1,1)"/>
				<xsl:variable name="month" select="substring($datefld,4,2)"/>
				<xsl:if test="$cent=1">
					<xsl:value-of select="concat($month,'/',20,$year)"/>
				</xsl:if>
				<xsl:if test="$cent=0">
					<xsl:value-of select="concat($month,'/',19,$year)"/>
				</xsl:if>
			</xsl:when> 
		</xsl:choose>
	</xsl:template>
	<xsl:template name="cvtdateCYYMMtoYYYYMM">
		<xsl:param name="datefld"/>
		<xsl:choose>
			<xsl:when test="string-length($datefld) = 5">
				<xsl:variable name="year" select="substring($datefld,2,2)"/>
				<xsl:variable name="cent" select="substring($datefld,1,1)"/>
				<xsl:variable name="month" select="substring($datefld,4,2)"/>
				<xsl:if test="$cent=1">
					<xsl:value-of select="concat(20,$year,$month)"/>
				</xsl:if>
				<xsl:if test="$cent=0">
					<xsl:value-of select="concat(19,$year,$month)"/>
				</xsl:if>
			</xsl:when> 
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>