<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="Change-Status">
    <xsl:param name="status"/>
    <xsl:choose>
      <xsl:when test="($status='V')">
        <xsl:variable name="FinalVal">
          <xsl:call-template name="Replace-String">
            <xsl:with-param name="text" select="$status"/>
            <xsl:with-param name="from" select="'V'"/>
            <xsl:with-param name="to" select="'Verified'"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$FinalVal"/>
      </xsl:when>
      <xsl:when test="($status='P')">
        <xsl:variable name="FinalVal">
          <xsl:call-template name="Replace-String">
            <xsl:with-param name="text" select="$status"/>
            <xsl:with-param name="from" select="'P'"/>
            <xsl:with-param name="to" select="'Pending'"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$FinalVal"/>
      </xsl:when>
      <xsl:when test="($status='Drop Pending')">
        <xsl:variable name="FinalVal">
          <xsl:call-template name="Replace-String">
            <xsl:with-param name="text" select="$status"/>
            <xsl:with-param name="from" select="'Drop Pending'"/>
            <xsl:with-param name="to" select="'Pending'"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$FinalVal"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$status"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="Replace-String">
    <xsl:param name="text"/>
    <xsl:param name="from"/>
    <xsl:param name="to"/>
    <xsl:choose>
      <xsl:when test="contains($text, $from)">
        <xsl:variable name="before" select="substring-before($text, $from)"/>
        <xsl:variable name="after" select="substring-after($text, $from)"/>
        <xsl:variable name="prefix" select="concat($before, $to)"/>

        <xsl:value-of select="$before"/>
        <xsl:value-of select="$to"/>
        <xsl:call-template name="Replace-String">
          <xsl:with-param name="text" select="$after"/>
          <xsl:with-param name="from" select="$from"/>
          <xsl:with-param name="to" select="$to"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>