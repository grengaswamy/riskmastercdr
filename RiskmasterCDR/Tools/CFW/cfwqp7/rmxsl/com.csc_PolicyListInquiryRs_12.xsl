<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="loss-date" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="$user-id"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="$session-id"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="$app-org"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="$app-name"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="$app-version"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <InsuranceSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="$rq-uid"/>
        </RqUID>
        <com.csc_PolicyListInquiryRs>
          <RqUID></RqUID>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_SelectList>
            <Status>
              <StatusCd/>
              <StatusDesc/>
              <com.csc_ServerStatusCode/>
              <com.csc_Severity/>
            </Status>
            <com.csc_RoutingInfo></com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
            <xsl:apply-templates select="/RMAcordPOINTJDBCRs/Rows/Row">
              <xsl:with-param name="loss-date" >
                <xsl:value-of select="concat(substring($loss-date,7,4),substring($loss-date,1,2),substring($loss-date,4,2))"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </com.csc_SelectList>
        </com.csc_PolicyListInquiryRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template match="/RMAcordPOINTJDBCRs/Rows/Row" >
    <xsl:param name="loss-date"/>
    <xsl:variable name="eff-date">
      <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
        <xsl:with-param name="datefld" select="@BC-EFFDT"/>
      </xsl:call-template>
    </xsl:variable>
    <!--1. <xsl:value-of select="$eff-date"/>-->
    <xsl:variable name="exp-date">
      <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
        <xsl:with-param name="datefld" select="@BC-EXPDT"/>
      </xsl:call-template>
    </xsl:variable>
    <!--2. <xsl:value-of select="$exp-date"/>-->
    <xsl:variable name="formatEffDate">
      <xsl:value-of select="concat(substring($eff-date,7,4),substring($eff-date,1,2),substring($eff-date,4,2))"/>
    </xsl:variable>
    <!--3. <xsl:value-of select="$formatEffDate"/>-->
    <xsl:variable name="formatExpDate">
      <xsl:value-of select="concat(substring($exp-date,7,4),substring($exp-date,1,2),substring($exp-date,4,2))"/>
    </xsl:variable>
    <!--4. <xsl:value-of select="$formatExpDate"/>-->
    <!--<xsl:value-of select="string-length($loss-date)"/>,
    <xsl:value-of select="$loss-date"/>,
    <xsl:value-of select="$formatEffDate"/>,
    <xsl:value-of select="$formatExpDate"/>-->
    <xsl:call-template name="RecordRow">
          <xsl:with-param name="eff-date" select="$eff-date" />
          <xsl:with-param name="exp-date" select="$exp-date" />
        </xsl:call-template>
  </xsl:template>

  <xsl:template name="RecordRow">
    <xsl:param name="eff-date"/>
    <xsl:param name="exp-date"/>
    <com.csc_ListEntry>
      <com.csc_PolicyLevel>
        <PolicySummaryInfo>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd></OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <PolicyNumber com.csc_colind="ReadOnly">
            <xsl:value-of select="@BC-POLICY0NUM"/>
          </PolicyNumber>
          <FullTermAmt>
            <Amt></Amt>
          </FullTermAmt>
          <PolicyStatusCd com.csc_colind="ReadOnly">
            <xsl:value-of select="@BC-STATUS-DESC"/>
          </PolicyStatusCd>
          <com.csc_TransactionStatusDesc com.csc_colind="ReadOnly"></com.csc_TransactionStatusDesc>
          <com.csc_TransAmt>
            <Amt></Amt>
          </com.csc_TransAmt>
          <LOBCd>
            <xsl:value-of select="@BC-LINE0BUS"/>
          </LOBCd>
          <GroupId>
            <xsl:value-of select="@BC-GROUPNO"/>
          </GroupId>
        </PolicySummaryInfo>
        <StateProvCd com.csc_colind="ReadOnly">
          <xsl:value-of select="@BC-STATE"/>
        </StateProvCd>
        <CompanyProductCd com.csc_colind="ReadOnly">
          <xsl:value-of select="@BC-SYMBOL"/>
        </CompanyProductCd>
        <NAICCd com.csc_colind="ReadOnly"></NAICCd>
        <ContractNumber>
          <xsl:value-of select="@BC-AGENCY"/>
        </ContractNumber>
        <com.csc_IssueCd>
          <xsl:value-of select="@BC-ISSUE0CODE"/>
        </com.csc_IssueCd>
        <com.csc_ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="@BC-LOCATION"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="@BC-MASTER0CO"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="@BC-MODULE"/>
            </OtherId>
          </OtherIdentifier>
        </com.csc_ItemIdInfo>
        <com.csc_QuoteInfo>
          <CompanysQuoteNumber com.csc_colind="ReadOnly"></CompanysQuoteNumber>
          <IterationNumber com.csc_colind="ReadOnly"></IterationNumber>
          <QuotePreparedDt com.csc_colind="ReadOnly"></QuotePreparedDt>
        </com.csc_QuoteInfo>
        <com.csc_BranchCd></com.csc_BranchCd>
        <com.csc_ContractTerm>
          <EffectiveDt com.csc_colind="ReadOnly">
            <xsl:value-of select="$eff-date"/>
          </EffectiveDt>
          <ExpirationDt com.csc_colind="ReadOnly">
            <xsl:value-of select="$exp-date"/>
          </ExpirationDt>
        </com.csc_ContractTerm>
        <com.csc_DataAffectedCd com.csc_colind="ReadOnly"/>
        <com.csc_ActivityDescCd com.csc_colind="ReadOnly"></com.csc_ActivityDescCd>
        <com.csc_TransactionEffectiveDt com.csc_colind="ReadOnly"></com.csc_TransactionEffectiveDt>
        <InsuredOrPrincipal>
          <ItemIdInfo>
            <AgencyId></AgencyId>
            <CustPermId>
              <xsl:value-of select="@BC-CUST0NO"/>
            </CustPermId>
          </ItemIdInfo>
          <GeneralPartyInfo>
            <NameInfo>
              <CommlName>
                <CommercialName>
                  <xsl:value-of select="@BC-ADD0LINE01"/>
                </CommercialName>
              </CommlName>
              <PersonName>
                <TitlePrefix/>
                <NameSuffix/>
                <Surname>
                  <xsl:value-of select="@BC-LASTNAME"/>
                </Surname>
                <GivenName>
                  <xsl:value-of select="@BC-FIRSTNAME"/>
                </GivenName>
                <OtherGivenName>
                  <xsl:value-of select="@BC-MIDDLENAME"/>
                </OtherGivenName>
                <NickName>
                  <xsl:value-of select="@BC-SORT0NAME"/>
                </NickName>
              </PersonName>
              <LegalEntityCd></LegalEntityCd>
              <TaxIdentity>
                <xsl:value-of select="@BC-SSN"/>
              </TaxIdentity>
              <NameType>
                <xsl:value-of select="@BC-NAMETYPE"/>
              </NameType>
            </NameInfo>
            <Addr>
              <AddrTypeCd>MailingAddress</AddrTypeCd>
              <Addr1>
                <xsl:choose>
                  <xsl:when test="string-length(@BC-ADD0LINE02)>0">
                    <xsl:value-of select="concat(@BC-ADD0LINE02, ' ', @BC-ADD0LINE03)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@BC-ADD0LINE03"/>
                  </xsl:otherwise>
                </xsl:choose>
              </Addr1>
              <City>
                <xsl:value-of select="@BC-ADD0LINE04"/>
              </City>
              <StateProvCd>
                <xsl:value-of select="@BC-STATE"/>
              </StateProvCd>
              <PostalCode>
                <xsl:value-of select="@BC-ZIP0POST"/>
              </PostalCode>
              <County></County>
            </Addr>
          </GeneralPartyInfo>
          <InsuredOrPrincipalInfo>
            <InsuredOrPrincipalRoleCd>Insured</InsuredOrPrincipalRoleCd>
            <BusinessInfo>
              <SICCd></SICCd>
            </BusinessInfo>
          </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>
        <com.csc_LongName com.csc_colind="ReadOnly"></com.csc_LongName>
        <com.csc_UserId com.csc_colind="ReadOnly"></com.csc_UserId>
        <com.csc_CancelRewriteCd com.csc_colind="ReadOnly"/>
        <com.csc_OOSInProcessInd></com.csc_OOSInProcessInd>
        <com.csc_TierRatingCd/>
        <com.csc_WIPOpenInd></com.csc_WIPOpenInd>
        <com.csc_RestrictedAccessInd></com.csc_RestrictedAccessInd>
        <com.csc_SupportDataDt com.csc_colind="ReadOnly"></com.csc_SupportDataDt>
        <com.csc_MultiObjectsInd/>
        <com.csc_ReasonAmdDesc/>
      </com.csc_PolicyLevel>
    </com.csc_ListEntry>
  </xsl:template>
</xsl:stylesheet>