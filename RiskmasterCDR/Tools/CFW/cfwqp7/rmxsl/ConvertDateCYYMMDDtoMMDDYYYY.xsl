<?xml version="1.0" encoding="UTF-8"?>
<!-- ********************************************************************************************* -->
<!-- Programmer    : Gaurav R                                 Date: 04/12/2006                     -->
<!-- Retrofitted by: Chaman Agrawal                           Date: 07/02/2007                     -->
<!-- FSIT Number   : Issue # 67586 / Resolution # 41223       Package#: Q00240                     -->
<!-- Description   : POL - Entry of a DOB less than 1910 doesn't display on the Personal Auto      -->
<!--                 Driver screen                                                                 -->
<!--                 Retrofitted from BASQIN to BNR Environment                                    -->
<!-- ********************************************************************************************* -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template name="cvtdateCYYMMDDtoMMDDYYYY">
	<xsl:param name="datefld"/>
	<xsl:choose>
	<!-- Taking the case of CYYMMDD -->
	<xsl:when test="string-length($datefld) = 7">
	<xsl:variable name="year" select="substring($datefld,2,2)"/>
	<xsl:variable name="month" select="substring($datefld,4,2)"/>
	<xsl:variable name="day" select="substring($datefld,6,2)"/>
	<xsl:variable name="cent" select="substring($datefld,1,1)"/>
	<xsl:if test="$cent=1">
	<xsl:value-of select="concat($month,'/',$day,'/',20,$year)"/>
	</xsl:if>
	<xsl:if test="$cent=0">
	<xsl:value-of select="concat($month,'/',$day,'/',19,$year)"/>
	</xsl:if>
	</xsl:when>
	<!-- Resolution# 41223 - Start (Commented out the code)-->
	<!-- taking the case of date in format CYYMM -->
	<!-- <xsl:when test="string-length($datefld) = 5"> -->
	<!-- <xsl:value-of select="concat(substring($datefld,4,2),'/',substring($datefld,2,2))"/> -->
	<!-- </xsl:when> -->
	<!-- taking the case of CYYMMDD where century indicator is 0 which would cause leading 0 to be ignored -->
	<!-- Resolution# 41223 - End -->
	<xsl:when test="string-length($datefld) = 6">
	<xsl:variable name="year1" select="substring($datefld,1,2)"/>
	<xsl:variable name="month1" select="substring($datefld,3,2)"/>
	<xsl:variable name="day1" select="substring($datefld,5,2)"/>
	<xsl:variable name="dispcent1" select="19"/>
	<xsl:value-of select="concat($month1,'/',$day1,'/',$dispcent1,$year1)"/>
	</xsl:when>
	<!-- Resolution# 41223 - Start -->
	<!-- taking the case of date in format CYYMM -->
	<xsl:when test="string-length($datefld) = 5">
	<xsl:variable name="year1" select="substring($datefld,1,1)"/>
	<xsl:variable name="month1" select="substring($datefld,2,2)"/>
	<xsl:variable name="day1" select="substring($datefld,4,2)"/>
	<xsl:variable name="dispcent1" select="190"/>
	<xsl:value-of select="concat($month1,'/',$day1,'/',$dispcent1,$year1)"/>
	</xsl:when>
	<!-- taking the case of date in format CYYMM where YYYY ranges from 1901-1909 -->
	<xsl:when test="string-length($datefld) = 4">
	<xsl:variable name="month1" select="substring($datefld,1,2)"/>
	<xsl:variable name="day1" select="substring($datefld,3,2)"/>
	<xsl:variable name="year1" select="1900"/>
	<xsl:value-of select="concat($month1,'/',$day1,'/',$year1)"/>
	</xsl:when>
	<!-- taking the case of date in format CYYMM where YYYY equals 1900 -->
	<xsl:when test="string-length($datefld) = 3">
	<xsl:variable name="month1" select="substring($datefld,1,1)"/>
	<xsl:variable name="day1" select="substring($datefld,2,2)"/>
	<xsl:variable name="year1" select="1900"/>
	<xsl:value-of select="concat('0',$month1,'/',$day1,'/',$year1)"/>
	</xsl:when>
	<!-- Resolution# 41223 - End -->
	<xsl:otherwise>
	<!-- taking the case of null date values -->
	<xsl:variable name="zerovalue">0</xsl:variable>
	<xsl:value-of select="$zerovalue"/>
	</xsl:otherwise>

	</xsl:choose>
	</xsl:template>
</xsl:stylesheet>