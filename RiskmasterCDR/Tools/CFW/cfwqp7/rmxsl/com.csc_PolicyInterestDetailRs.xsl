<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="baseLOBline" select="default"/>
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
        <xsl:with-param name="baseLOBline">
          <xsl:value-of select="normalize-space($baseLOBline)"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <xsl:apply-templates select="//ERROR__LST__ROW"/>
        <com.csc_PolicyInterestDetailRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          
          <InsuredOrPrincipal>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_Location</OtherIdTypeCd>
                <OtherId>
               

                    <xsl:value-of select="normalize-space(//KEY__USE__LOCATION)"/>
                  
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_SequenceNum</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__SEQUENCE__NUMBER)"/>
                </OtherId>
              </OtherIdentifier>
              <!--mits 35925 start-->
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_ClientSeqNum</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//PI-CLIENT-SEQ)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_AddressSeqNum</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//PI-ADDRESS-SEQ)"/>
                </OtherId>
              </OtherIdentifier>
              <!--mits 35925 end-->
            </ItemIdInfo>
            <GeneralPartyInfo>
              <NameInfo>
                <CommlName>
                  <CommercialName>
                    <xsl:choose>
                      <xsl:when test="//KEY__SITE__NUMBER">
                        <xsl:value-of select="normalize-space(//ADDLINT__NAME)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="normalize-space(//NAME)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </CommercialName>
                </CommlName>
                <PersonName>
                  <TitlePrefix/>
                  <NameSuffix/>
                  <Surname/>
                  <GivenName/>
                  <OtherGivenName/>
                  <NickName>
                    <xsl:choose>
                      <xsl:when test="//KEY__SITE__NUMBER">
                        <xsl:value-of select="normalize-space(//ADDLINT__SORT__NAME)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="normalize-space(//SORT__NAME)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </NickName>
                </PersonName>
                <LegalEntityCd></LegalEntityCd>
                <TaxIdentity>
                  <TaxIdTypeCd>FEIN/SSN</TaxIdTypeCd>
                  <TaxId>
                    <xsl:choose>
                      <xsl:when test="//KEY__SITE__NUMBER">
                        <xsl:value-of select="normalize-space(//STATUS__SSN__NO)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="normalize-space(//SSN__NO)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </TaxId>
                </TaxIdentity>
                <TaxIdentity>
                  <TaxIdTypeCd>TaxId</TaxIdTypeCd>
                  <TaxId>
                    <xsl:value-of select="normalize-space(//TAX__ID)"/>
                  </TaxId>
                </TaxIdentity>
              </NameInfo>
              <Addr>
                <AddrTypeCd>MailingAddress</AddrTypeCd>
                <Addr1>
                  <xsl:choose>
                    <xsl:when test="//KEY__SITE__NUMBER">
                      <xsl:value-of select="normalize-space(//ADDLINT__ADDRESS__LINE__1)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(//ADDRESS__LINE__1)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </Addr1>
                <Addr2>
                  <xsl:choose>
                    <xsl:when test="//KEY__SITE__NUMBER">
                      <xsl:value-of select="normalize-space(//ADDLINT__ADDRESS__LINE__2)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(//ADDRESS__LINE__2)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </Addr2>
                <City>
                  <xsl:choose>
                    <xsl:when test="//KEY__SITE__NUMBER">
                      <xsl:value-of select="normalize-space(//ADDLINT__CITY)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(//CITY)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </City>
                <StateProvCd>
                  <xsl:choose>
                    <xsl:when test="//KEY__SITE__NUMBER">
                      <xsl:value-of select="normalize-space(//ADDLINT__STATE)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(//STATE)"/>
                    </xsl:otherwise>
                  </xsl:choose>                 
                </StateProvCd>
                <PostalCode>
                  <xsl:choose>
                    <xsl:when test="//KEY__SITE__NUMBER">
                      <xsl:value-of select="normalize-space(//ADDLINT__ZIP__CODE)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(//ZIP__CODE)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </PostalCode>
                <County/>
              </Addr>
            </GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
              <InsuredOrPrincipalRoleCd>
                <xsl:value-of select="normalize-space(//KEY__USE__CODE)"/>
              </InsuredOrPrincipalRoleCd>
              <InsuredOrPrincipalRoleDesc>
                <xsl:value-of select="normalize-space(//KEY__USE__CODE__DESC)"/>
              </InsuredOrPrincipalRoleDesc>
              <BusinessInfo>
                <SICCd></SICCd>
              </BusinessInfo>
            </InsuredOrPrincipalInfo>
            <com.csc_LoanAccNum>
              <xsl:value-of select="normalize-space(//LOAN__NUMBER)"/>
            </com.csc_LoanAccNum>
            <ContractTerm>
              <EffectiveDt>
                <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
                  <xsl:with-param name="datefld" select="//PROC__EFFECTIVE__DATE"/>
                </xsl:call-template>
              </EffectiveDt>
              <ExpirationDt>
                <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
                  <xsl:with-param name="datefld" select="//PROC__LDA__ACCTG__DATE"/>
                </xsl:call-template>
              </ExpirationDt>
            </ContractTerm>
          </InsuredOrPrincipal>
        </com.csc_PolicyInterestDetailRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
</xsl:stylesheet>