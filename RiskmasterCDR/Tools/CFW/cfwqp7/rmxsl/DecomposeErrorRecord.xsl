<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

 <xsl:template name="DecomposeErrorRecord" match="//ERROR__LST__ROW">
    <xsl:text disable-output-escaping="yes">&lt;com.csc_ErrorList&gt;</xsl:text>
    <xsl:for-each select="ERROR__LST">
      <xsl:if test="string-length(ERROR__MSG)>0">
        <xsl:text disable-output-escaping="yes">&lt;com.csc_Error&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;ERROR</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:value-of select="ERROR__MSG"/>
        <xsl:text disable-output-escaping="yes">&lt;/ERROR</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;ERRORSEV</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:value-of select="ERROR__SEVERITY"/>
        <xsl:text disable-output-escaping="yes">&lt;/ERRORSEV</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;ERRORSWITCH</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:value-of select="ERROR__SWITCH"/>
        <xsl:text disable-output-escaping="yes">&lt;/ERRORSWITCH</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;ERRORFIELD</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:call-template name="ReplaceString">
          <xsl:with-param name="text" select="ERROR__FIELD"/>
          <xsl:with-param name="from" select="'-'"/>
          <xsl:with-param name="to" select="'_'"/>
        </xsl:call-template>
        <xsl:text disable-output-escaping="yes">&lt;/ERRORFIELD</xsl:text>
        <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;/com.csc_Error&gt;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text disable-output-escaping="yes">&lt;/com.csc_ErrorList&gt;</xsl:text>
  </xsl:template>

  <xsl:template name="ReplaceString">
    <xsl:param name="text"/>
    <xsl:param name="from"/>
    <xsl:param name="to"/>
    <xsl:choose>
      <xsl:when test="contains($text, $from)">
        <xsl:variable name="before" select="substring-before($text, $from)"/>
        <xsl:variable name="after" select="substring-after($text, $from)"/>
        <xsl:variable name="prefix" select="concat($before, $to)"/>

        <xsl:value-of select="$before"/>
        <xsl:value-of select="$to"/>
        <xsl:call-template name="ReplaceString">
          <xsl:with-param name="text" select="$after"/>
          <xsl:with-param name="from" select="$from"/>
          <xsl:with-param name="to" select="$to"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>