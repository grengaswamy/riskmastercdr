<?xml version="1.0" encoding="UTF-8"?>
<!--40207 Start - returns XXX on invalid input date.-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="Replace-String.xsl"/>
	<xsl:template name="cvtdateMMDDYYYYtoCYYMMDD">
	<xsl:param name="datefld"/>
	<xsl:variable name="normalizeddate" select="normalize-space($datefld)"/>
	<xsl:if test="string-length($normalizeddate)>0">
		<xsl:variable name="firstpass"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="$normalizeddate"/><xsl:with-param name="from" select="'/'"/><xsl:with-param name="to" select="''"/></xsl:call-template></xsl:variable>
		<xsl:variable name="secondpass"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="$firstpass"/><xsl:with-param name="from" select="'/'"/><xsl:with-param name="to" select="''"/></xsl:call-template></xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($secondpass)= 8">
				<xsl:variable name="year" select="substring(normalize-space($secondpass),7,2)"/>
				<xsl:variable name="mo-day" select="substring(normalize-space($secondpass),1,4)"/>
				<xsl:variable name="century" select="substring(normalize-space($secondpass),5,2)"/>
				<xsl:choose>
					<xsl:when test="($century > 19)"><xsl:value-of select="concat('1',$year,$mo-day)"/></xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat('0',$year,$mo-day)"/></xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!--Resolution id 40207 start -->
			<xsl:when test="string-length($datefld) = 7">
				<xsl:value-of select="$datefld"/>
			</xsl:when>
			<!--Resolution id 40207 end -->
			<xsl:otherwise>XXXXXXX</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	</xsl:template>
</xsl:stylesheet>