<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template name="cvtdateMMDDYYtoCYYMMDD">
	<xsl:param name="datefld"/>
	<xsl:choose>
	<xsl:when test="string-length($datefld)> 6 and contains($datefld , '/')">
	
		<xsl:variable name="year" select="substring($datefld,7,2)"/>
		<xsl:variable name="mo" select="substring($datefld,1,2)"/>
		<xsl:variable name="day" select="substring($datefld,4,2)"/>
		<xsl:choose>
			<xsl:when test="($year > 39)"><xsl:value-of select="concat('0',$year,$mo,$day)"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat('1',$year,$mo,$day)"/></xsl:otherwise>
		</xsl:choose>
		
	</xsl:when>
	<xsl:when test="string-length($datefld) = 6 ">
	
		<xsl:variable name="year" select="substring($datefld,5,2)"/>
		<xsl:variable name="mo" select="substring($datefld,1,2)"/>
		<xsl:variable name="day" select="substring($datefld,3,2)"/>
		<xsl:choose>
			<xsl:when test="($year > 39)"><xsl:value-of select="concat('0',$year,$mo,$day)"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat('1',$year,$mo,$day)"/></xsl:otherwise>
		</xsl:choose>
		
	</xsl:when>
	<!-- This condition is to test whether a date field has 7 digit value .There are cases when date fields are not for display purposes -->
	<xsl:when test="string-length($datefld) = 7">
		<xsl:value-of select="$datefld"/>
	</xsl:when>
	<xsl:otherwise>
	<xsl:variable name="zeroval">0000000</xsl:variable>
	<xsl:value-of select="$zeroval"/>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:template>
</xsl:stylesheet>