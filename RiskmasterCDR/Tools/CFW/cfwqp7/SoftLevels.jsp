<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ******************************************************************* --%>
<%-- * This is a JSP to configure the Framework Utilities properties.  * --%>
<%-- ******************************************************************* --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.*" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.fw.util.*" %>
<%@ page errorPage="error.jsp" %>

<%
        String jspTitle = "Software Levels";
        String jspHeader = "Software Levels";
        String jspTitleStatus = "Software Levels";
        String jspTitleAppVersion = "Communications Framework Version";
        String jspAppVersion = com.csc.cfw.Version.version();

        //Detail Cache list flag
        boolean fCacheDetailList = false;
        boolean eventWindow = false;
        String eventId = null;
        long eventTimestamp = 0;
        
        //Boolean variable for Aux windows
        boolean traceWindow = false;
        boolean sysErrorWindow = false;
        boolean traceListDetailWindow = false;
        boolean sysErrorListDetailWindow = false;

        // Main Window flags
        boolean dispFileImage = false;
        boolean editPropWindow = true;
        boolean statusWindow = true;
        boolean javaPropWindow = true;
        boolean traceSummaryWindow = true;
        boolean sysErrorSummaryWindow = true;
        boolean ccfWindow = true;
        boolean as400Window = true;
        
%>
<HTML>
  <HEAD>
    <LINK href="includes/INFUtil.css" type=text/css rel=stylesheet>
    <TITLE><%=jspTitle%></TITLE>
    <META name="GENERATOR" content="IBM WebSphere Studio">
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    <SCRIPT LANGUAGE="JAVASCRIPT">
    </SCRIPT>
    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD  >

          <TABLE CELLSPACING="10">
            <TR ALIGN="Center">
             <TD ALIGN="Center"  >
              <TABLE CELLSPACING="15">

                <TR>
                  <TD WIDTH="100%"  >
                    <H2 ALIGN="CENTER"><%=jspHeader%></H2>
                  </TD>
                </TR>
<%

        /*************************************************************
        *   Start displaying windows table sections
        **************************************************************/
        
        if (statusWindow)
        {
            /*************************************************************
            *  Status section
            **************************************************************/

%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfgStatusForm" ACTION="">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Software Levels</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TABLE BGCOLOR="#FFFFDD" BORDER="1" "WIDTH=100%">
<%
                                if (jspAppVersion != null)
                                {

%>
                                  <TR>
                                    <TD class="SRE"><%=jspTitleAppVersion%></TD>
                                    <TD class="SRE" align="right"><%=jspAppVersion%></TD>
                                  </TR>
<%
                                }
%>
                                  <TR>
                                    <TD class="SRE">Framework Utilities Version</TD>
                                    <TD class="SRE" align="right"><%=com.csc.fw.util.Version.version()%></TD>
                                  </TR>

                                  <TR>
                                    <TD class="SRE">Xalan (XSL Transformer) Version</TD>
                                    <TD class="SRE" align="right"><%=org.apache.xalan.Version.getVersion()%></TD>
                                  </TR>

                                  <TR>
                                    <TD class="SRE">Xerces (XML parser) Version</TD>
                                    <TD class="SRE" align="right"><%=org.apache.xerces.impl.Version.getVersion()%></TD>
                                  </TR>

                                  <TR>
                                    <TD class="SRE">XSL Transformer class</TD>
                                    <TD class="SRE" align="right"><%=javax.xml.transform.TransformerFactory.newInstance().getClass().getName()%></TD>
                                  </TR>

                                  <TR>
                                    <TD class="SRE">XML Parser class</TD>
                                    <TD class="SRE" align="right"><%=javax.xml.parsers.DocumentBuilderFactory.newInstance().getClass().getName()%></TD>
                                  </TR>

                                  <TR>
                                    <TD class="SRE">Free Memory</TD>
                                    <TD class="SRE" align="right"><%=NumberFormat.getInstance().format(Runtime.getRuntime().freeMemory())%></TD>
                                  </TR>
                                  
                                  <TR>
                                    <TD class="SRE">Total Memory</TD>
                                    <TD class="SRE" align="right"><%=NumberFormat.getInstance().format(Runtime.getRuntime().totalMemory())%></TD>
                                  </TR>


                                </TABLE>
                              </TD>
                            </TR>

                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
        } /* end if (statusWindow) */
%>
              </TABLE>
             </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
        