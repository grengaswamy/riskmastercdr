<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="BuildProductRecord">
<xsl:param name="InsLine"></xsl:param>
<xsl:param name="RateState"></xsl:param>
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBCCPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBCCPL1__RECORD>
<BCAACD><xsl:value-of select="$LOC"/></BCAACD>
<BCABCD><xsl:value-of select="$MCO"/></BCABCD>
<BCARTX><xsl:value-of select="$SYM"/></BCARTX>
<BCASTX><xsl:value-of select="$POL"/></BCASTX>
<BCADNB><xsl:value-of select="$MOD"/></BCADNB>
<BCAGTX><xsl:value-of select="$InsLine"/></BCAGTX>
<BCADCD><xsl:value-of select="$RateState"/></BCADCD>
<BCANTX><xsl:value-of select="$InsLine"/></BCANTX>
<BCAOTX>*ALL</BCAOTX>
<BCC6ST>P</BCC6ST>
<BCAFNB>0</BCAFNB>
<BCTYPE0ACT>
<!-- 34771 start -->
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when>
		--><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when>
		--><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when>
		--><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when>
		--><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
<!-- 34771 end -->
</BCTYPE0ACT>
<BCAEPC>
<xsl:choose>
<xsl:when test="$InsLine='CA'">
	<xsl:choose>
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='LEXP']">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='LEXP']/NumericValue/FormatModFactor"/>
	</xsl:when>
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<xsl:when test="$InsLine!='CA' and $InsLine!='BOP'">
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='com.csc_ML']/NumericValue/FormatModFactor" />
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</BCAEPC>
<BCAFPC>
<xsl:choose>
<xsl:when test="$InsLine='CA'">
	<xsl:choose>
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='PDEXP']">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='PDEXP']/NumericValue/FormatModFactor"/>
	</xsl:when>
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</BCAFPC>
<xsl:variable name="BCALPC">
<xsl:choose>
<xsl:when test="$InsLine='IMC'">
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='RDF']/NumericValue/FormatModFactor"/>
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<BCALPC><xsl:value-of select="$BCALPC" /></BCALPC>
<xsl:variable name="BCAMPC">
<xsl:choose>
<xsl:when test="$InsLine='GL'">
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='EXP']/NumericValue/FormatModFactor">
</xsl:value-of>
</xsl:when>
<xsl:when test="$InsLine='CA'">
<xsl:value-of select="string('1')"/> 
</xsl:when>
<!-- 59878 starts-->
<xsl:when test="$InsLine='CR'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='EXP']/NumericValue/FormatModFactor">
	</xsl:value-of>
</xsl:when>
<!-- 59878 ends-->
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<BCAMPC><xsl:value-of select="$BCAMPC" /></BCAMPC>
<BCANPC>
<xsl:choose>
<xsl:when test="$InsLine='BOP'"> 
<!-- Case 37036 start -->
<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor" />-->
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor" />
<!-- Case 37036 start -->
</xsl:when>
<xsl:when test="$InsLine='CA'">
	<xsl:choose>
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='IRPM']">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor"/>
	</xsl:when>
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<!-- 59878 starts -->
<xsl:when test="$InsLine ='CF' or $InsLine='IMC'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor" />
</xsl:when>
<xsl:when test="$InsLine='CR'">1.0000</xsl:when>
<xsl:otherwise>1.00000</xsl:otherwise>
<!--<xsl:otherwise>
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor"/>
</xsl:otherwise>-->
<!-- 59878 ends-->
</xsl:choose>
</BCANPC>
<BCBGPC>
<xsl:choose>
<xsl:when test="$InsLine='CA'">
	<xsl:choose>
	<!--Issue 59878 Start-->
	<!--<xsl:when test="/CreditOrSurcharge[CreditSurchargeCd='LEXP']">	-->
	<!--<xsl:value-of select="/CreditOrSurcharge[CreditSurchargeCd='LEXP']/NumericValue/FormatModFactor"/> -->
	<!--Issue 59878 start-->
	<!--Commented this code for 59878-->
	<!--<xsl:when test="../CreditOrSurcharge[CreditSurchargeCd='LEXP']">	
	<xsl:value-of select="../CreditOrSurcharge[CreditSurchargeCd='LEXP']/NumericValue/FormatModFactor"/> -->
	<!--Added this code for 59878-->
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='EXN']">	
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='EXN']/NumericValue/FormatModFactor"/>
	<!--Issue 59878 end-->
	</xsl:when>
	<!--Issue 59878 End-->
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<xsl:when test="$InsLine='BOP'">
<!-- Case 37036 start -->
<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='EXN']/NumericValue/FormatModFactor" />-->
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='EXN']/NumericValue/FormatModFactor" />
<!-- Case 37036 start -->
</xsl:when>
<!-- 59878 starts -->
<xsl:when test="$InsLine='CF'">
	<xsl:choose>
		<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='UNIEXP']/NumericValue/FormatModFactor">
			<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='UNIEXP']/NumericValue/FormatModFactor"/>
		</xsl:when>
		<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<xsl:when test="$InsLine='CR'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='UNIEXP']/NumericValue/FormatModFactor"/>
</xsl:when>
<xsl:when test="$InsLine='IMC'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='UNIEXP']/NumericValue/FormatModFactor"/>
</xsl:when>
<xsl:when test="$InsLine='GL'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='UNIEXP']/NumericValue/FormatModFactor"/>
</xsl:when>
<!-- 59878 ends -->
</xsl:choose>
</BCBGPC>
<xsl:variable name="BCBLPC">
<xsl:choose>
<xsl:when test="$InsLine='GL' or $InsLine='CR'">
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor"/>
</xsl:when>
<xsl:when test="$InsLine='CA'">
	<xsl:choose>
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='LSCH']">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='LSCH']/NumericValue/FormatModFactor"/>
	</xsl:when>
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</xsl:when>
<xsl:when test="$InsLine='BOP'">
<!-- Case 37036 start -->
<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor" />-->
<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor" />
<!-- Case 37036 start -->
</xsl:when>
<!-- Issue 59878 - Start -->
<xsl:when test="$InsLine='CF'">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor"/>
</xsl:when>
<!-- Issue 59878 - Start -->
<xsl:otherwise>0</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<BCBLPC><xsl:value-of select="$BCBLPC" /></BCBLPC>
<xsl:if test="$InsLine='CA'">
<BCBNPC>
	<xsl:choose>
	<xsl:when test="CreditOrSurcharge[CreditSurchargeCd='PDSCH']">
	<xsl:value-of select="CreditOrSurcharge[CreditSurchargeCd='PDSCH']/NumericValue/FormatModFactor"/>
	</xsl:when>
	<xsl:otherwise>1.00000</xsl:otherwise>
	</xsl:choose>
</BCBNPC>
</xsl:if>
</ASBCCPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->