<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateSubLocation">
		<xsl:param name="LOBCd" select="XX"/>
         <xsl:variable name="Location" select="BVBRNB"/>
         <xsl:variable name="SubLocation" select="BVEGNB"/>
        <SubLocation>
                <xsl:attribute name="id">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
           <ItemIdInfo>
              <InsurerId><xsl:value-of select="$SubLocation"/></InsurerId>
               <!-- 34771 start -->
			  <OtherIdentifier>
			  	<OtherIdTypeCd>HostId</OtherIdTypeCd>
				<OtherId><xsl:value-of select="BVEGNB"/>		
				</OtherId>
			</OtherIdentifier>
			  <!-- 34771 end -->
           </ItemIdInfo>
           <SubLocationDesc><xsl:value-of select="BVEFTX"/></SubLocationDesc>
           <Addr>
               <AddrTypeCd/>
               <Addr1/>
               <Addr2/>
               <Addr3/>
               <Addr4/>
               <City/>
               <StateProvCd/>
               <PostalCode/>
               <Country/>
               <County/>
            </Addr>
			<!-- Starte Issue 53911 -->
			<!--
            <xsl:for-each select="/*/PMSP1200__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length($Location)) and $SubLocation = substring(DESC0SEQ, string-length(DESC0SEQ)-string-length($SubLocation)+1, string-length($SubLocation))]">
                  <xsl:call-template name="CreateAdditionalInterest"/>
           </xsl:for-each>
			-->
			<xsl:choose>
	           <xsl:when test="/*/PMSP0200__RECORD/LINE0BUS = 'BOP'">
		            <xsl:for-each select="/*/PMSP1200AT__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length($Location)) and $SubLocation = substring(RSUBLOCNUM, string-length(RSUBLOCNUM)-string-length($SubLocation)+1, string-length($SubLocation))]">
						<xsl:variable name="ATLocation" select="USE0LOC"/>
						<xsl:variable name="ATIntType" select="USE0CODE"/>
						<xsl:variable name="ATIntSeqNo" select="DESC0SEQ"/>
							<xsl:for-each select="/*/PMSP1200__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length($Location)) and $ATIntType = USE0CODE and $ATIntSeqNo = DESC0SEQ]">
										<xsl:call-template name="CreateAdditionalInterest"/>
				           </xsl:for-each>
				</xsl:for-each>
			   </xsl:when>
				<xsl:otherwise>
		            <xsl:for-each select="/*/PMSP1200__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length($Location)) and $SubLocation = substring(DESC0SEQ, string-length(DESC0SEQ)-string-length($SubLocation)+1, string-length($SubLocation))]">
		                  <xsl:call-template name="CreateAdditionalInterest"/>
		           </xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
			<!-- End Issue 53911 -->
        </SubLocation>
    </xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->