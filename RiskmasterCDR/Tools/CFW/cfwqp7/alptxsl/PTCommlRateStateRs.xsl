<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template name="CreateCommlRateState">
			<!--<xsl:variable name="ASBB" select="/*/ASBBCPL1__RECORD"/>-->		<!--Issue 59878 -->
            <xsl:variable name="ASBB" select="/*/ASBBCPL1__RECORD[BBAGTX='CA']"/>  <!--Issue 59878 -->
            <xsl:variable name="State" select="B5BCCD"/>						<!--Issue 59878 -->
             <CommlRateState>
               <StateProvCd><xsl:value-of select="B5BCCD"/></StateProvCd>
			   <!--<xsl:if test="B5AENB = '1'">	-->	<!--Issue 59878-->
	           <xsl:if test="position() = 1">		<!--Issue 59878-->	
               <CommlAutoHiredInfo>
                  <HiredLiabilityCostAmt>
                     <Amt><xsl:value-of select="$ASBB/BBCIVA"/></Amt>
                  </HiredLiabilityCostAmt>
                   <xsl:for-each select="/*/ASBYCPL1__RECORD[substring(BYAOTX,1,4)='HIRE']">
                    <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
					  <Limit>
                      	<FormatCurrencyAmt>                           
						   <xsl:choose>
						   		<xsl:when test="BYAOTX = 'HIRE'">
									<Amt>
										<xsl:value-of select="$ASBB/BBUSVA5"/>
									</Amt>
								</xsl:when>
								<xsl:when test="BYAOTX = 'HIREUM'">
									<Amt>
									 	<xsl:value-of select="$ASBB/BBUSVA3"/>
									 </Amt>
								</xsl:when>
								<xsl:when test="BYAOTX = 'HIRECM'">
									<Amt>
									 	<xsl:value-of select="BYPOTX"/>
									 </Amt>
								</xsl:when>
								<xsl:when test="BYAOTX = 'HIRECO'">
									<Amt>
									 	<xsl:value-of select="BYEGCD"/>
									 </Amt>
								</xsl:when>
								<xsl:otherwise>
									<Amt>
									 	<xsl:value-of select="BYAGVA"/>	
									</Amt>
								 </xsl:otherwise>											   
							</xsl:choose>
					   </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
                     <xsl:if test="string-length(BYA9NB) > 0">                    
                     <Deductible>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYPNTX"/></Amt> <!-- Issue 59878 -->
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                     
               </CommlAutoHiredInfo>
               <CommlAutoNonOwnedInfo>
                  <NonOwnedInfo>
                     <NonOwnedGroupTypeCd/>
                     <NumNonOwned><xsl:value-of select="$ASBB/BBBKNB"/></NumNonOwned>
                     <xsl:for-each select="/*/ASBYCPL1__RECORD[substring(BYAOTX,1,3)='NON']">
                     <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
                     <Limit>
                        <FormatCurrencyAmt>
                        <!--Issue 59878 -->
                          <xsl:choose>
						   		<xsl:when test="BYAOTX = 'NONOWN'">
									<Amt>
										<xsl:value-of select="$ASBB/BBUSVA5"/>
									</Amt>
								</xsl:when>
								<xsl:otherwise>
									<Amt>
									 	<xsl:value-of select="BYAGVA"/>	
									</Amt>
								 </xsl:otherwise>											   
							</xsl:choose>
							<!--Issue 59878 -->
                        </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
                     <xsl:if test="string-length(BYA9NB) > 0">                    
                     <Deductible>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYA9NB"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                     
                  </NonOwnedInfo>
               </CommlAutoNonOwnedInfo>
               <CommlAutoDriveOtherCarInfo>
                  <NumIndividualsCovered><xsl:value-of select="$ASBB/BBUSNB3"/></NumIndividualsCovered>
                     <xsl:for-each select="/*/ASBYCPL1__RECORD[substring(BYAOTX,1,3)='DOC']">
                     <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
                     <Limit>
                        <FormatCurrencyAmt>
						<!-- Issue 67226 Starts -->
						<xsl:choose>
							<xsl:when test="BYAOTX='DOCUMP'">
								<Amt><xsl:value-of select="BYUSVA4"/></Amt>
							</xsl:when>
							<xsl:when test="BYAOTX='DOC-L'">
								<Amt><xsl:value-of select="$ASBB/BBUSVA5"/></Amt>
							</xsl:when>
							<xsl:when test="BYAOTX='DOC-MP'">
								<Amt><xsl:value-of select="$ASBB/BBALCD"/></Amt>
							</xsl:when>
							<xsl:when test="BYAOTX='DOC-CM'">
								<Amt><xsl:value-of select="$ASBB/BBALCD"/></Amt>
							</xsl:when>
							<xsl:when test="BYAOTX='DOC-UM'">
								<Amt><xsl:value-of select="$ASBB/BBIZTX"/></Amt>
							</xsl:when>
							<!--Issue 59878 start-->
							<xsl:when test="BYAOTX='DOC-UN'">
								<Amt><xsl:value-of select="$ASBB/BBIYTX"/></Amt>
							</xsl:when>
							<!--Issue 59878 end-->
							<xsl:otherwise>
							<!-- Issue 67226 Ends -->
                           <Amt><xsl:value-of select="BYAGVA"/></Amt>
							</xsl:otherwise><!-- Issue 67226 -->
						</xsl:choose><!-- Issue 67226 -->
                        </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
							<!-- Issue 67226 BEGIN -->
					 <xsl:if test="BYAOTX='DOCUMP'">                   
                     <Deductible>
                        <FormatCurrencyAmt>
								<Amt><xsl:value-of select="BYALCD"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
							<!-- Issue 67226 END -->
                     <xsl:if test="string-length(BYA9NB) > 0">                    
                     <Deductible>
                        <FormatCurrencyAmt>
                             <Amt><xsl:value-of select="BYA9NB"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
                     <xsl:if test="BYAOTX='DOC-CM' or substring(BYAOTX,1,5)='DOC-L'">
                     <Option>
                        <OptionTypeCd>YNInd1</OptionTypeCd>
                        <OptionCd/>
                        <OptionValue>Y</OptionValue>
                     </Option>
                     </xsl:if>                     
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                                       
               </CommlAutoDriveOtherCarInfo>
			   </xsl:if>		<!--Issue 59878-->
                   <!--xsl:for-each select="/*/ASB5CPL1__RECORD"-->	<!-- Issue 59878 -->
				   	<!--<xsl:for-each select="key('StateKey', B5BCCD)">-->		<!--Issue 59878-->
				   	<xsl:for-each select="/*/ASB5CPL1__RECORD[B5BCCD = $State and B5ANTX='CA']">	<!--Issue 59878-->
                       <xsl:call-template name="CreateCommlVeh">
                       </xsl:call-template>
                    </xsl:for-each>               
                      <!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CA'and BEB9NB='0']">
	  		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	              
            </CommlRateState>               
    </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->