<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateBASCLT1400">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>BASCLT1400</RECORD__NAME>
         </RECORD__NAME__ROW>
                 
         
         <BASCLT1400__RECORD>
           	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
         	<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
         	<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
         	<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
         	<MODULE><xsl:value-of select="$MOD"/></MODULE>
 		 </BASCLT1400__RECORD>
      
        
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->