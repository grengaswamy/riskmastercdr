<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template name="CreateCommlCoverage">
	<!-- Issue 67910 Begin -->
	<xsl:choose>
		<xsl:when test="BYAGTX='CA'">
               <CommlCoverage>
                  <CoverageCd>
				<xsl:choose>
					<xsl:when test="BYAOTX='RENTAL'">
						<xsl:value-of select="concat(BYAOTX,'-',BYALCD)"/>
					</xsl:when>
					<xsl:otherwise>
	<!-- Issue 67910 End -->
                        <xsl:value-of select="BYAOTX"/>    
	<!-- Issue 67910 Begin -->
					</xsl:otherwise>
				</xsl:choose>
                  </CoverageCd>
                  <CoverageDesc/>
			<xsl:choose>
				<xsl:when test="BYAGTX='CA'">
					<Limit>
						<FormatCurrencyAmt>
							<xsl:choose>
								<xsl:when test="BYAOTX='MEDPAY'">
									<Amt>
										<xsl:value-of select="BYAKCD"/>
									</Amt>
								</xsl:when>
								<xsl:when test="BYAOTX='UN'">
									<xsl:if test="BYANCD = 'CSL'">
										<Amt>
											<xsl:value-of select="BYAHVA"/>
										</Amt>
									</xsl:if>
									<xsl:if test="BYANCD = 'SL/S' or BYANCD = 'SL'">
										<Amt>
											<xsl:value-of select="BYUSVA5"/>
										</Amt>
									</xsl:if>
									<xsl:if test="BYANCD = 'SPLIT' or BYANCD = 'SPL/S'">
										<Amt>
											<xsl:value-of select="BYUSVA3"/>
										</Amt>
									</xsl:if>
								</xsl:when>
								<xsl:when test="BYAOTX='UNPD'">
									<xsl:if test="BYUSVA4!='0.00'">
										<Amt>
											<xsl:value-of select="BYUSVA4"/>
										</Amt>
									</xsl:if>
								</xsl:when>
								<xsl:when test="BYAOTX='UM'">
									<Amt>
										<xsl:if test="/*/ASBBCPL1__RECORD/BBANCD='SL/S' or /*/ASBBCPL1__RECORD/BBANCD='SL' or /*/ASBBCPL1__RECORD/BBANCD='SLNST' or /*/ASBBCPL1__RECORD/BBANCD='SLSTK' or /*/ASBBCPL1__RECORD/BBANCD='CSLNST' or /*/ASBBCPL1__RECORD/BBANCD='CSLSTK' or /*/ASBBCPL1__RECORD/BBANCD='CSL'">
											<xsl:value-of select="/*/ASBBCPL1__RECORD/BBAHVA"/>	
										</xsl:if>
										<xsl:if test="/*/ASBBCPL1__RECORD/BBANCD='SPLIT' or /*/ASBBCPL1__RECORD/BBANCD='SPLNST' or /*/ASBBCPL1__RECORD/BBANCD='SPLBI' or /*/ASBBCPL1__RECORD/BBANCD='SPLSTK' or /*/ASBBCPL1__RECORD/BBANCD='SPL/S'">
											<xsl:value-of select="/*/ASBBCPL1__RECORD/BBUSVA3"/>	
										</xsl:if>
									</Amt>
								</xsl:when>
								<xsl:when test="BYAOTX='UMPD'">
									<Amt>
										<xsl:value-of select="BYAGVA"/>
									</Amt>
								</xsl:when>
								<xsl:when test="BYAOTX='LIAB'">
									<Amt>
									    <xsl:variable name="node" select="key('StateKey', 'CA')"/>
										<xsl:value-of select="/*/ASBBCPL1__RECORD[BBAGTX = 'CA']/BBUSVA5"/>
									</Amt>
								</xsl:when>
								<xsl:otherwise>
									<Amt>
										<xsl:value-of select="BYUSVA3"/>
									</Amt>
								</xsl:otherwise>
							</xsl:choose>
						</FormatCurrencyAmt>
					</Limit>
				</xsl:when>
				<xsl:otherwise>
	<!-- Issue 67910 End -->
         <xsl:if test="substring(BKBBTX,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="BYBBTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>                 
	<!-- Issue 67910 Begin -->
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="BYAGTX='CA'">
					<xsl:choose>
						<xsl:when test="BYAOTX = 'LIAB'">
							<Deductible>
								<FormatCurrencyAmt>
									<Amt>
										<xsl:value-of select="/*/ASBBCPL1__RECORD[BBAGTX = 'CA']/BBEHCD"/>
									</Amt>
								</FormatCurrencyAmt>
								<DeductibleTypeCd>FL</DeductibleTypeCd>
							</Deductible>
						</xsl:when>
						<!--Issue 59878 UM changes starts-->
						<xsl:when test="BYAOTX = 'UM'">
							<Deductible>
								<FormatCurrencyAmt>
									<xsl:if test="BYBCCD='GA'">
									<Amt>
										<xsl:value-of select="/*/ASB5CPL1__RECORD[B5ANTX='CA' and B5BCCD='GA']/B5EJCD"/>
									</Amt>
									</xsl:if>
									<xsl:if test="BYBCCD='IN'">
									<Amt>
										<xsl:value-of select="/*/ASB5CPL1__RECORD[B5ANTX='CA' and B5BCCD='IN']/B5EJCD"/>
									</Amt>
									</xsl:if>
								</FormatCurrencyAmt>
								<DeductibleTypeCd>FL</DeductibleTypeCd>
							</Deductible>
						</xsl:when>
						<!--Issue 59878 UM changes ends-->
						<!--CA 269 Starts-->
       					<xsl:when test="BYAOTX = 'UMPD'">
							<Deductible>
								<FormatCurrencyAmt>
									<Amt>
										<xsl:value-of select="BYALCD"/>
									</Amt>
								</FormatCurrencyAmt>
								<DeductibleTypeCd>FL</DeductibleTypeCd>
							</Deductible>
						</xsl:when>
						<!--CA 269 Ends-->
						<!--Issue 59878 - CA 307 Starts-->
       					<xsl:when test="BYAOTX = 'COMP' and (/*/ASB5CPL1__RECORD/B5BCCD='KY' or /*/ASB5CPL1__RECORD/B5BCCD='WI' or /*/ASB5CPL1__RECORD/B5BCCD='IN')">
							<Deductible>
								<FormatCurrencyAmt>
									<Amt>
										<xsl:value-of select="/*/ASB5CPL1__RECORD/B5USCD5"/>
									</Amt>
								</FormatCurrencyAmt>
								<DeductibleTypeCd>FL</DeductibleTypeCd>
							</Deductible>
						</xsl:when>
						<!--Issue 59878 - CA 307 Ends-->
						<xsl:otherwise>
							<Deductible>
								<FormatCurrencyAmt>
									<Amt>
										<xsl:value-of select="BYPPTX"/>
									</Amt>
								</FormatCurrencyAmt>
								<DeductibleTypeCd>FL</DeductibleTypeCd>
							</Deductible>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
	<!-- Issue 67910 End -->
         <xsl:if test="substring(BYA9NB,1,1) &gt; '0'">
                   <Deductible>
                     <FormatCurrencyAmt>
                        <Amt>                     
                        <xsl:value-of select="BYA9NB"/>
                     </Amt>   
                     </FormatCurrencyAmt>
                     <DeductibleTypeCd>FL</DeductibleTypeCd>
                  </Deductible>
         </xsl:if>                  
	<!-- Issue 67910 Begin -->
				</xsl:otherwise>
			</xsl:choose>
	<!-- Issue 67910 End -->
                  <CurrentTermAmt>
                     <Amt>
                        <xsl:value-of select="BYA3VA"/>
                     </Amt>                                 
                  </CurrentTermAmt>
               </CommlCoverage>         
	<!-- Issue 67910 Begin -->
		</xsl:when>
	</xsl:choose>
	<!-- Issue 67910 End -->
	</xsl:template>


	<!-- 29653 Start -->
	<!--    <xsl:template match="/*/ISLPPP1__RECORD" mode="WCCoverage"> -->
	<xsl:template match="/*/PMSPWC02__RECORD" mode="WCCoverage">
	<!-- 29653 End   -->
	    <CommlCoverage>
	       <CoverageCd>WCEL</CoverageCd>
	       <CoverageDesc>Employers Liability</CoverageDesc>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start -->
                     <!--<Amt><xsl:value-of select="ELPOLLIMIT"/></Amt>-->
                     <Amt><xsl:value-of select="BIDISPOL"/></Amt>
                     <!-- 29653 End   -->
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start -->
                     <!--<Amt><xsl:value-of select="ELDISELIMT"/></Amt>-->
                     <Amt><xsl:value-of select="BIDISEMP"/></Amt>
                     <!-- 29653 End   -->
                  </FormatCurrencyAmt>
	       <LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start -->
                     <!-- <Amt><xsl:value-of select="ELACC"/></Amt> -->
                     <Amt><xsl:value-of select="BIACCIDENT"/></Amt>
                     <!-- 29653 End  -->
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachClaim</LimitAppliesToCd>
	       </Limit>
	       <Deductible>
                  <FormatCurrencyAmt>
                     <Amt></Amt>
                  </FormatCurrencyAmt>
	          <DeductibleTypeCd></DeductibleTypeCd>
	          </Deductible>
	       <CurrentTermAmt>
		  <Amt/>
	       </CurrentTermAmt>
	    </CommlCoverage>
	    <CommlCoverage>
	       <CoverageCd>VOL</CoverageCd>
	       <CoverageDesc>Voluntary Liability</CoverageDesc>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start -->
                     <!--<Amt><xsl:value-of select="VCPOLLIMIT"/></Amt> -->
                     <Amt><xsl:value-of select="VCDISPOL"/></Amt>
                     <!-- 29653 End -->
                  </FormatCurrencyAmt>
               <LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start --> 
                     <!--<Amt><xsl:value-of select="VCDISLIMIT"/></Amt> -->
                     <Amt><xsl:value-of select="VCDISEMP"/></Amt>
                     <!-- 29653 End   -->
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <!-- 29653 Start --> 
                     <!--<Amt><xsl:value-of select="VCACCLIMIT"/></Amt> -->
                     <Amt><xsl:value-of select="VCACCIDENT"/></Amt>
                     <!-- 29653 End   -->
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachClaim</LimitAppliesToCd>
	       </Limit>
	    </CommlCoverage>
    </xsl:template>
    
<!-- Case 31594 Begin -->
    <xsl:template match="/*/PMSPWC02__RECORD" mode="CreateWCCommlCoverage">
	    <CommlCoverage>
	       <CoverageCd>WCEL</CoverageCd>
	       <CoverageDesc>Employers Liability</CoverageDesc>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="BIDISPOL"/></Amt>
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="BIDISEMP"/></Amt>
                  </FormatCurrencyAmt>
	       <LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="BIACCIDENT"/></Amt>
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachClaim</LimitAppliesToCd>
	       </Limit>
	       <Deductible>
                  <FormatCurrencyAmt>
                     <Amt></Amt>
                  </FormatCurrencyAmt>
	          <DeductibleTypeCd></DeductibleTypeCd>
	          </Deductible>
	       <CurrentTermAmt>
		  <Amt/>
	       </CurrentTermAmt>
	    </CommlCoverage>
	    <CommlCoverage>
	       <CoverageCd>VOL</CoverageCd>
	       <CoverageDesc>Voluntary Liability</CoverageDesc>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="VCDISPOL"/></Amt>
                  </FormatCurrencyAmt>
               <LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="VCDISEMP"/></Amt>
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
	       </Limit>
	       <Limit>
                  <FormatCurrencyAmt>
                     <Amt><xsl:value-of select="VCACCIDENT"/></Amt>
                  </FormatCurrencyAmt>
		  <LimitAppliesToCd>EachClaim</LimitAppliesToCd>
	       </Limit>
	    </CommlCoverage>
    </xsl:template>
<!-- Case 31594 End -->
</xsl:stylesheet>
