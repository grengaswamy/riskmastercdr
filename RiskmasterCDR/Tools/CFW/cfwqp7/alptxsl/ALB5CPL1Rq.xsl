<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="BuildALB5CPL1">
<xsl:param name="Location"/>
<xsl:param name="SubLocation"/>
<xsl:param name="InsLine"/>
<xsl:param name="Node"/>

<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ALB5CPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ALB5CPL1__RECORD>
<B5AACD><xsl:value-of select="$LOC"/></B5AACD>
<B5ABCD><xsl:value-of select="$MCO"/></B5ABCD>
<B5ARTX><xsl:value-of select="$SYM"/></B5ARTX>
<B5ASTX><xsl:value-of select="$POL"/></B5ASTX>
<B5ADNB><xsl:value-of select="$MOD"/></B5ADNB>
<B5AGTX><xsl:value-of select="$InsLine"/></B5AGTX>
<B5BRNB><xsl:value-of select="$Location"/></B5BRNB>
<B5EGNB><xsl:value-of select="$SubLocation"/></B5EGNB>
<B5ANTX><xsl:value-of select="$InsLine"/></B5ANTX>
<B5AENB>
   <xsl:choose>
      <xsl:when test="$InsLine='CA'">
         <xsl:value-of select="$Node/ItemIdInfo/InsurerId"/>
      </xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
</B5AENB>
<B5C6ST>P</B5C6ST>

<B5ALICTE>
	<xsl:choose>
      <xsl:when test="$InsLine='CA'">
	  	<xsl:value-of select="$Node/CommlVehSupplement/PrimaryClassCd"/>
	  </xsl:when>
	  <xsl:when test="$InsLine='BOP'">
	  	<xsl:value-of select="$Node/ClassCd"/>
	  </xsl:when>
	  <xsl:otherwise><!-- 59878 --><xsl:value-of select="$Node/ClassCd" /></xsl:otherwise>
	</xsl:choose>
</B5ALICTE>

<B5ALIDTE>
   <xsl:choose>
      <xsl:when test="$InsLine='CA'">
         <xsl:value-of select="$Node/CommlVehSupplement/UseLeasedVehDesc"/>
      </xsl:when>   
      <xsl:when test="$InsLine='CF'">
         <xsl:value-of select="$Node/com.csc_ClassCdDesc"/>
      </xsl:when>
      <xsl:when test="$InsLine='BOP'">
         <xsl:value-of select="$Node/com.csc_ClassCdDesc"/>
      </xsl:when>      
      <xsl:otherwise>
         <xsl:value-of select="$Node/ClassCdDesc" />
      </xsl:otherwise>
    </xsl:choose>
</B5ALIDTE>
<xsl:if test="$InsLine='CA'">
<B5ALIETE>LT SER ENTER ALL OTHER</B5ALIETE>
</xsl:if>
</ALB5CPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->