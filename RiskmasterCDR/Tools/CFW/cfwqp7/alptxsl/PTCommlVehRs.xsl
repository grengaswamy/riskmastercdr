<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes" />
  <xsl:template name="CreateCommlVeh">
    <CommlVeh>
      <xsl:variable name="VehNbr" select="B5AENB" />
      <xsl:attribute name="id">
        <xsl:value-of select="concat('v',$VehNbr)" />
      </xsl:attribute>
      <ItemIdInfo>
        <InsurerId>
          <xsl:value-of select="$VehNbr" />
        </InsurerId>
      </ItemIdInfo>
      <!--start issue 29405-->
      <!--<Manufacturer><xsl:value-of select="substring-before(B5DCNB,' ')"/></Manufacturer>-->
      <Manufacturer>
        <xsl:value-of select="substring-before(concat(B5DCNB,' '),' ')" />
      </Manufacturer>
      <!--end issue 29405-->
      <Model>
        <xsl:value-of select="substring-after(B5DCNB, ' ')" />
      </Model>
      <ModelYear>
        <xsl:value-of select="B5ANCD" />
      </ModelYear>
      <VehBodyTypeCd>
        <xsl:value-of select="B5AZTX" />
      </VehBodyTypeCd>
      <CostNewAmt>
        <Amt>
          <xsl:value-of select="B5A9NB" />
        </Amt>
      </CostNewAmt>
      <!-- Case 29405 Begin -->
      <com.csc_StatedAmt>
        <Amt>
          <xsl:value-of select="B5A0VA" />
        </Amt>         
      </com.csc_StatedAmt>
      <!-- Case 29405 End -->
      <NumDaysDrivenPerWeek></NumDaysDrivenPerWeek>
      <EstimatedAnnualDistance>
        <NumUnits>
          <xsl:value-of select="B5XXXX" />
        </NumUnits>
        <UnitMeasurementCd>MI</UnitMeasurementCd>
      </EstimatedAnnualDistance>
      <TerritoryCd>
        <xsl:value-of select="B5AGNB" />
      </TerritoryCd>
      <VehIdentificationNumber>
        <xsl:value-of select="B5DDNB" />
      </VehIdentificationNumber>
      <VehSymbolCd />
      <xsl:for-each select="/*/PMSP1200__RECORD[$VehNbr = substring(USE0LOC, string-length(USE0LOC)-string-length($VehNbr)+1, string-length($VehNbr))]">
        <xsl:call-template name="CreateAdditionalInterest" />
      </xsl:for-each>
      <CommlVehSupplement>
        <FarZoneCd>
          <xsl:value-of select="substring(B5KKTX,3,2)" />
        </FarZoneCd>
        <NearZoneCd>
          <xsl:value-of select="substring(B5KKTX,1,2)" />
        </NearZoneCd>
        <PrimaryClassCd>
          <xsl:value-of select="B5PTTX" />
        </PrimaryClassCd>
        <UseLeasedVehDesc>
          <xsl:value-of select="B5XXXX" />
        </UseLeasedVehDesc>
      </CommlVehSupplement>
      <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAENB=$VehNbr]">
        <xsl:call-template name="CreateCommlCoverage"></xsl:call-template>
      </xsl:for-each>
	      <!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CA'and BEB9NB=$VehNbr]">
	  		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	  
    </CommlVeh>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->
