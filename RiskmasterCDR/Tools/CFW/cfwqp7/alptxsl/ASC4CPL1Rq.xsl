<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateExtendedCoverages">
   <xsl:variable name="ExtendedCoveragecd" select="CoverageCd"/> 		<!--Issue # 39800-->
	<xsl:if test="normalize-space($ExtendedCoveragecd) != ''"> 			<!--Issue # 39800-->
	 <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASC4CPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASC4CPL1__RECORD>
            <C4AACD>
               <xsl:value-of select="$LOC"/>
            </C4AACD>
            <C4ABCD>
               <xsl:value-of select="$MCO"/>
            </C4ABCD>
            <C4ARTX>
               <xsl:value-of select="$SYM"/>
            </C4ARTX>
            <C4ASTX>
               <xsl:value-of select="$POL"/>
            </C4ASTX>
            <C4ADNB>
               <xsl:value-of select="$MOD"/>
            </C4ADNB>
            <C4AGTX>
               <xsl:value-of select="$LOB"/>
            </C4AGTX>
            <C4BRNB>00001</C4BRNB>
            <C4EGNB>00001</C4EGNB>
            <!-- 39111 Start -->
            <C4ANTX>
            <xsl:choose>
				<xsl:when test="$LOB='FP'">DP-<xsl:value-of select="../Dwell/PolicyTypeCd"/></xsl:when>
				<xsl:otherwise>HP</xsl:otherwise>
			</xsl:choose>
			</C4ANTX>
            <!-- 39111 End -->
            <C4AENB>00001</C4AENB>
            <C4AOTX><xsl:value-of select="CoverageCd"/></C4AOTX>
            <C4C0NB><xsl:value-of select="IterationNumber"/></C4C0NB>
            <C4C6ST>P</C4C6ST>
			 <!-- 34770 start -->
		 	<!--<C4TYPE0ACT>NB</C4TYPE0ACT>-->
	    <C4TYPE0ACT>	
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
	    </C4TYPE0ACT>
		<!-- 34770 end -->
            <xsl:choose>
               <xsl:when test="CoverageCd = 'HO0435'">
                  <xsl:for-each select="Limit">               
                  <xsl:choose>               
                     <xsl:when test ="LimitAppliesToCd = 'com.csc_Residence'">       
                        <C4BKVA><xsl:value-of select="FormatCurrencyAmt/Amt"/></C4BKVA>
                     </xsl:when>
                     <xsl:otherwise>
                        <C4BLVA><xsl:value-of select="FormatCurrencyAmt/Amt"/></C4BLVA>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:for-each>                     
               </xsl:when>
			   <xsl:when test="CoverageCd = 'ADDRES'"> 
			   		<!--Case 34770 Begin-->
			    	<!--<C4BKVA><xsl:value-of select="Option[OptionCd='com.csc_NumLocations']/OptionValue"/></C4BKVA>            
               		<C4BLVA><xsl:value-of select="Option[OptionCd='com.csc_NumEmployees']/OptionValue"/></C4BLVA>-->
					<C4BTCD><xsl:value-of select="Option[OptionCd='com.csc_NumLocations']/OptionValue"/></C4BTCD>           
               		<C4BUCD><xsl:value-of select="Option[OptionCd='com.csc_NumEmployees']/OptionValue"/></C4BUCD>
					<!--Case 34770 End-->
			   </xsl:when>
			   	<!-- Starts Code for residence Employee-->
			   <xsl:when test="CoverageCd = 'EMPLYS'"> 
			   	     <C4BKVA><xsl:value-of select="Option/OptionValue"/></C4BKVA><!-- case 34770 added on 4 may-->
					 <!--<C4BKVA><xsl:value-of select="../Dwell/NumEmployeesFullTimeResidence"/></C4BKVA> Case 34770 commented 4 may-->           
               </xsl:when>
			   	<!-- Ends Code for residence Employee-->
			   <xsl:when test="CoverageCd = 'HO2413'"> 
			   		<C4BKVA><xsl:value-of select="Option/OptionValue"/></C4BKVA>            
               </xsl:when>
			   <xsl:when test="CoverageCd = 'HO0420'"> 
			   		<C4IRTX><xsl:value-of select="Option/OptionValue"/>%</C4IRTX>            
               </xsl:when>
			   <xsl:when test="CoverageCd = 'IM 144'"> 
			   		<C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
					<C4IRTX><xsl:value-of select="Option/OptionCd"/></C4IRTX> 
               </xsl:when>
			   <xsl:when test="CoverageCd = 'HO2471'"> 
			   <!-- <C4IRTX><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4IRTX>
					<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
					<C4ITTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4ITTX>
					<C4IUTX><xsl:value-of select="Option[OptionCd='4']/OptionValue"/></C4IUTX>
					<C4IVTX><xsl:value-of select="Option[OptionCd='5']/OptionValue"/></C4IVTX>-->
			   		<xsl:if test="Option[OptionCd='1']/OptionValue != ''"> 
						<C4BTCD>CLERIC</C4BTCD> 
						<C4IRTX>CLERICAL</C4IRTX>
						<C4BKVA><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4BKVA>
					</xsl:if>
					<xsl:if test="Option[OptionCd='2']/OptionValue != ''"> 
						<C4BVCD>TEACH</C4BVCD> 
						<C4ITTX>TEACHERS</C4ITTX>
						<C4BMVA><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4BMVA>
					</xsl:if>
					<xsl:if test="normalize-space(Option[OptionCd='3']/OptionValue) != ''"> 
						<C4BXCD>CORP</C4BXCD> 
						<C4IVTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4IVTX>
						<C4BOVA>1</C4BOVA>
					</xsl:if>
					<xsl:if test="normalize-space(Option[OptionCd='4']/OptionValue) != ''"> 
						<C4BUCD>SALES</C4BUCD>
						<C4ISTX><xsl:value-of select="Option[OptionCd='4']/OptionValue"/></C4ISTX>
						<C4BLVA>1</C4BLVA>
					</xsl:if>
					<xsl:if test="Option[OptionCd='5']/OptionValue != ''"> 
						<C4BWCD>NOC</C4BWCD>
						<C4IUTX>TEACH NOC</C4IUTX>
						<C4BNVA><xsl:value-of select="Option[OptionCd='5']/OptionValue"/></C4BNVA>
					</xsl:if>
               </xsl:when>
				<!--Case 34770 Begin-->
			   <xsl:when test="CoverageCd = 'HO0454'"> 
			   		<C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
			    	<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
               		<C4ITTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4ITTX>
					<C4IUTX><xsl:value-of select="Option[OptionCd='4']/OptionValue"/></C4IUTX>
			   </xsl:when>
			   	<!--Case 34770 End-->
			   	<!-- Issue 39111 Begin -->
				<xsl:when test="CoverageCd = 'DP0419' or CoverageCd = 'DP0420'">
					<C4BTCD><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BTCD>
					<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DL2405'">
					<C4BMVA><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4BMVA>
					<C4IRTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4IRTX>
					<C4BLVA><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4BLVA>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DL2406'">
					<C4IRTX><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4IRTX>
					<C4BLVA><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4BLVA>
					<C4BMVA><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4BMVA>
					<C4BNVA><xsl:value-of select="Option[OptionCd='4']/OptionValue"/></C4BNVA>
					<C4BOVA><xsl:value-of select="Option[OptionCd='5']/OptionValue"/></C4BOVA>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DP0437'">
					<C4BTCD><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BTCD>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DP0468'">
					<C4BTCD><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BTCD>
					<C4BLVA><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4BLVA>
					<C4BMVA><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4BMVA>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DP0469'">
					<C4IRTX><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4IRTX>
					<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
					<C4ITTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4ITTX>
					<C4IUTX><xsl:value-of select="Option[OptionCd='4']/OptionValue"/></C4IUTX>
				</xsl:when>
				<xsl:when test="CoverageCd = 'DP0470'">
					<!--<C4IRTX><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4IRTX>--><!--99623-->
					<C4IRTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4IRTX><!--99623-->
					<C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>	<!--99623-->
				</xsl:when>
				<!--<xsl:when test="CoverageCd = 'DP0472' or CoverageCd = 'DP0473'">-->				<!--99623-->
				<xsl:when test="CoverageCd = 'DP0472'"><!--99623-->
					<C4IRTX><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4IRTX>
					<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
				</xsl:when>
				<!-- 99623 Starts -->
				<xsl:when test="CoverageCd = 'DP0473'">
					<C4IRTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4IRTX>
					<C4ISTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4ISTX>
				</xsl:when>
				<!-- 99623 Ends -->
				<xsl:when test="CoverageCd = 'FOST'">
					<C4BKVA><xsl:value-of select="Option[OptionCd='1']/OptionValue"/></C4BKVA>
					<C4ISTX><xsl:value-of select="Option[OptionCd='2']/OptionValue"/></C4ISTX>
				</xsl:when>
			   	<!-- Issue 39111 End -->
               <xsl:otherwise>
                   <C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
               </xsl:otherwise>
            </xsl:choose>
			<!-- 99623 Starts -->
			<xsl:choose>
				<xsl:when test="CoverageCd = 'DP0472'">
					<C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
					<C4ITTX><xsl:value-of select="Option[OptionCd='3']/OptionValue"/></C4ITTX>	
				</xsl:when>
				<xsl:when test="CoverageCd = 'DP0473'">
					<C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>					
				</xsl:when>
			</xsl:choose>
			<!-- 99623 Ends -->
          <!--  <xsl:if test="CoverageCd = 'EMPLYS'">
                 <C4BTCD><xsl:value-of select="../Dwell/NumEmployeesFullTimeResidence"/></C4BTCD>
            </xsl:if> 
            <xsl:if test="CoverageCd = 'IM 144'">
                 <C4BTCD><xsl:value-of select="Option/OptionCd"/></C4BTCD>
            </xsl:if>            
            <xsl:if test="CoverageCd = 'ADDRES'">
               <C4BTCD><xsl:value-of select="Option/OptionValue"/></C4BTCD>            
               <C4BUCD><xsl:value-of select="../Dwell/DwellInspectionValuation/NumFamilies"/></C4BUCD> 
            </xsl:if> -->                      
            <!-- Issue 64220 Begin : The description is split into 4 35 byte fields -->
            <!-- <C4RPTX><xsl:value-of select="CoverageDesc"/></C4RPTX>> -->
            <xsl:variable name="ItemDescription" select="translate(CoverageDesc,' ','_')"/>
            <C4RPTX><xsl:value-of select="substring($ItemDescription,1,35)" /></C4RPTX>
            <C4RQTX><xsl:value-of select="substring($ItemDescription,36,35)" /></C4RQTX>
            <C4RRTX><xsl:value-of select="substring($ItemDescription,71,35)" /></C4RRTX>
            <C4RSTX><xsl:value-of select="substring($ItemDescription,106,35)" /></C4RSTX>
            <!-- Issue 64220 End -->
         </ASC4CPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:if> <!--Issue # 39800-->
   </xsl:template>
   <xsl:template name="CreateScheduledProperty">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASC4CPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASC4CPL1__RECORD>
            <C4AACD>
               <xsl:value-of select="$LOC"/>
            </C4AACD>
            <C4ABCD>
               <xsl:value-of select="$MCO"/>
            </C4ABCD>
            <C4ARTX>
               <xsl:value-of select="$SYM"/>
            </C4ARTX>
            <C4ASTX>
               <xsl:value-of select="$POL"/>
            </C4ASTX>
            <C4ADNB>
               <xsl:value-of select="$MOD"/>
            </C4ADNB>
            <C4AGTX>
               <xsl:value-of select="$LOB"/>
            </C4AGTX>
            <C4BRNB>00001</C4BRNB>
            <C4EGNB>00001</C4EGNB>
            <!-- 39111 Start -->
            <C4ANTX>
            <xsl:choose>
				<xsl:when test="$LOB='FP'">DP-<xsl:value-of select="../Dwell/PolicyTypeCd"/></xsl:when>
				<xsl:otherwise>HP</xsl:otherwise>
			</xsl:choose>
			</C4ANTX>
            <!-- 39111 End -->
            <C4AENB>00001</C4AENB>
            <C4AOTX><xsl:value-of select="Coverage/CoverageCd"/></C4AOTX>
            <!-- Issue 64220 Begin -->
            <!--C4C0NB>00001</C4C0NB-->
            <C4C0NB><xsl:value-of select="substring(@id,5)"/></C4C0NB>
            <!-- Issue 64220 End -->
            <C4C6ST>P</C4C6ST>
            <!-- 34770 start -->
		 	<!--<C4TYPE0ACT>NB</C4TYPE0ACT>-->
			<C4TYPE0ACT>	
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//com.csc_AmendmentMode"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
	    </C4TYPE0ACT>
			<!-- 34770 end -->		
            <C4BKVA><xsl:value-of select="Coverage/Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
            <!-- Issue 64220 Begin : The description is split into 4 35 byte fields -->
            <!--C4IRTX><xsl:value-of select="ItemDefinition/ItemDesc"/></C4IRTX-->
            <xsl:variable name="ItemDescription" select="translate(ItemDefinition/ItemDesc,' ','_')"/>
            <C4RPTX><xsl:value-of select="substring($ItemDescription,1,35)" /></C4RPTX>
            <C4RQTX><xsl:value-of select="substring($ItemDescription,36,35)" /></C4RQTX>
            <C4RRTX><xsl:value-of select="substring($ItemDescription,71,35)" /></C4RRTX>
            <C4RSTX><xsl:value-of select="substring($ItemDescription,106,35)" /></C4RSTX>
            <!-- Issue 64220 End -->
         </ASC4CPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
   <xsl:template name="CreateWaterCraftLiability">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASC4CPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASC4CPL1__RECORD>
            <C4AACD>
               <xsl:value-of select="$LOC"/>
            </C4AACD>
            <C4ABCD>
               <xsl:value-of select="$MCO"/>
            </C4ABCD>
            <C4ARTX>
               <xsl:value-of select="$SYM"/>
            </C4ARTX>
            <C4ASTX>
               <xsl:value-of select="$POL"/>
            </C4ASTX>
            <C4ADNB>
               <xsl:value-of select="$MOD"/>
            </C4ADNB>
            <C4AGTX>
               <xsl:value-of select="$LOB"/>
            </C4AGTX>
            <C4BRNB>00001</C4BRNB>
            <C4EGNB>00001</C4EGNB>
            <!-- 39111 Start -->
            <C4ANTX>
            <xsl:choose>
				<xsl:when test="$LOB='FP'">DP-<xsl:value-of select="../Dwell/PolicyTypeCd"/></xsl:when>
				<xsl:otherwise>HP</xsl:otherwise>
			</xsl:choose>
			</C4ANTX>
            <!-- 39111 End -->
            <C4AENB>00001</C4AENB>
            <C4AOTX>HO2475</C4AOTX>
            <C4C0NB>00001</C4C0NB>
            <C4C6ST>P</C4C6ST>
			<!-- 34770 start -->
		 	<!--<C4TYPE0ACT>NB</C4TYPE0ACT>-->
			<C4TYPE0ACT>	
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//com.csc_AmendmentMode"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
	    </C4TYPE0ACT>
			<!-- 34770 end -->
            <C4BTCD><xsl:value-of select="PropulsionTypeCd"/></C4BTCD>
            <C4IRTX><xsl:value-of select="Horsepower/NumUnits"/></C4IRTX>
			<C4ISTX><xsl:value-of select="Length/NumUnits"/></C4ISTX>
         </ASC4CPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
   <xsl:template name="CreateExtendedCoverage">
   <xsl:variable name="ExtendedCoveragecd" select="CoverageCd"/> 		
	<xsl:if test="normalize-space($ExtendedCoveragecd) != ''"> 		
	 <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASC4CPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASC4CPL1__RECORD>
            <C4AACD>
               <xsl:value-of select="$LOC"/>
            </C4AACD>
            <C4ABCD>
               <xsl:value-of select="$MCO"/>
            </C4ABCD>
            <C4ARTX>
               <xsl:value-of select="$SYM"/>
            </C4ARTX>
            <C4ASTX>
               <xsl:value-of select="$POL"/>
            </C4ASTX>
            <C4ADNB>
               <xsl:value-of select="$MOD"/>
            </C4ADNB>
            <C4AGTX>
               <xsl:value-of select="$LOB"/>
            </C4AGTX>
            <C4BRNB>00001</C4BRNB>
            <C4EGNB>00001</C4EGNB>
            <!-- 39111 Start -->
            <C4ANTX>
            <xsl:choose>
				<xsl:when test="$LOB='FP'">DP-<xsl:value-of select="../Dwell/PolicyTypeCd"/></xsl:when>
				<xsl:otherwise>HP</xsl:otherwise>
			</xsl:choose>
			</C4ANTX>
            <!-- 39111 End -->
            <C4AENB>00001</C4AENB>
            <C4AOTX><xsl:value-of select="CoverageCd"/></C4AOTX>
            <C4C0NB><xsl:value-of select="IterationNumber"/></C4C0NB>
            <C4C6ST>P</C4C6ST>
	    <C4TYPE0ACT>	
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//com.csc_AmendmentMode"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
	    </C4TYPE0ACT>
            <xsl:choose>
               <xsl:when test="CoverageCd = 'FP0000'">
                  <xsl:for-each select="Limit">               
                  <xsl:choose>               
                     <xsl:when test ="LimitAppliesToCd = 'com.csc_Residence'">       
                        <C4BKVA><xsl:value-of select="FormatCurrencyAmt/Amt"/></C4BKVA>
                     </xsl:when>
                     <xsl:otherwise>
                        <C4BLVA><xsl:value-of select="FormatCurrencyAmt/Amt"/></C4BLVA>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:for-each>                     
               </xsl:when>
               <xsl:otherwise>
                   <C4BKVA><xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/></C4BKVA>
               </xsl:otherwise>
            </xsl:choose>
            <C4RPTX><xsl:value-of select="CoverageDesc"/></C4RPTX>>
         </ASC4CPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:if>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->