<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRs.xsl"/>
	 <xsl:include href="PTErrorRs.xsl"/>
    <xsl:include href="PTSignOnRs.xsl"/>
    <xsl:include href="PTProducerRs.xsl"/>
    <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
    <xsl:include href="PTCommlPolicyRs.xsl"/>
    <xsl:include href="PTFormsRs.xsl"/>
    <xsl:include href="PTAdditionalInterestRs.xsl"/>
    <xsl:include href="PTLocationRs.xsl"/>
    <xsl:include href="PTSubLocationRs.xsl"/>
    <xsl:include href="PTCommlRateStateRs.xsl"/>
    <xsl:include href="PTCommlVehRs.xsl"/>
    <xsl:include href="PTCommlCoverageRs.xsl"/>
    <xsl:include href="PTCoveredAutoSymbolRs.xsl"/>
    <xsl:include href="PTTruckersSupplementRs.xsl"/>    
   
    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <ACORD>
            <xsl:call-template name="BuildSignOn"/>
            <InsuranceSvcRs>
                <RqUID/>
                <CommlAutoPolicyQuoteInqRs>
                    <RqUID/>
                    <TransactionRequestDt/>
                    <CurCd>USD</CurCd>
    			    <xsl:call-template name="PointErrorsRs"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
                    <xsl:for-each select="/*/ASBUCPL1__RECORD">
                        <xsl:call-template name="CreateLocation"/>
                    </xsl:for-each>
                    <CommlAutoLineBusiness>
                        <LOBCd>CA</LOBCd>
                        <xsl:apply-templates select="/*/ASCSCPL1__RECORD"/>
                        <xsl:for-each select="/*/ASB5CPL1__RECORD[B5AENB='1']">
                           <xsl:call-template name="CreateCommlRateState"/>
                        </xsl:for-each>
                        <xsl:call-template name="CreateTrailerInterchange"/>
                    </CommlAutoLineBusiness>
                </CommlAutoPolicyQuoteInqRs>
            </InsuranceSvcRs>
        </ACORD>
    </xsl:template>
 </xsl:stylesheet>
