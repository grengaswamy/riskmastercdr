<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Add)
-->
  <xsl:template name="CreatePMSPSA15">
    <xsl:param name="Action"/>
    <xsl:variable name="TableName">PMSPSA15</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPSA15</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPSA15__RECORD>
      	<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>	
      	<UNITNO>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">UNITNO</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="substring(../@LocationRef,2,string-length(../@LocationRef)-1)"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </UNITNO>
        <COVSEQ>
       <!-- 29653 Start -->
		<!-- DST issue WC 017 start -->
        <xsl:choose>
            <xsl:when test="$TYPEACT = 'EN'">
			<!--103409 Starts-->		
			<!--<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
				<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:if>
			<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
                <xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
             		<xsl:with-param name="FieldLength">4</xsl:with-param>
					<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier/OtherId"/>					
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
			</xsl:if>-->
			<xsl:choose>
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
					<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 issue 103409 starts-->
				<!--<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd!='DeemedType']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>-->
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostID']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 issue 103409 ends-->
			</xsl:choose>
			<!--103409 Ends-->
			<!-- DST issue WC 017 end -->
            </xsl:when>
		    <xsl:otherwise>
                <xsl:value-of select="position()"/>
            </xsl:otherwise>
         </xsl:choose>
         <!-- 29653 End -->
         </COVSEQ>
		<TRANSSEQ>0001</TRANSSEQ>
        <SUBLINE>011</SUBLINE>
		<CLASSNUM>
         <!--xsl:value-of select="substring(RatingClassificationCd,1,4)"/ 78200 - Class code issue-->
         <xsl:value-of select="substring(RatingClassificationCd,1,6)"/>
        </CLASSNUM>
        <MAJPERIL>032</MAJPERIL>
		<COVEFFDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		    <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>-->	<!--103409-->
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/><!--103409-->
              </xsl:call-template>
            </xsl:when>
		    <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </COVEFFDTE>
        <COVEXPDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/ExpirationDt"/>
              </xsl:call-template>
            </xsl:when>
	        <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
				  <!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/ExpirationDt"/>-->		  <!--103409-->
				  <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/ExpirationDt"/>				  <!--103409-->
              </xsl:call-template>
            </xsl:when>
	        <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/ExpirationDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </COVEXPDTE>
		<TRANSDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
	        <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>-->				  <!--103409-->
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>				  <!--103409-->
              </xsl:call-template>
            </xsl:when>
	        <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </TRANSDTE>
        <ENTRYDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>-->				  <!--103409-->
				  <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>				  <!--103409-->
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </ENTRYDTE>
		<ACCTGDTE>00000</ACCTGDTE>
		<RETRODTE></RETRODTE>
		<EXTENDDTE></EXTENDDTE>
		<LOB>  
			<xsl:value-of select="$LOB"/> 
		</LOB>
		<SUBLOB></SUBLOB>
        <STATE>
          <xsl:value-of select="../../StateProvCd"/>
        </STATE>
		<TERRITORY></TERRITORY>
		<TAXLOC></TAXLOC>
		<ASLINE></ASLINE>
		<PRODLINE></PRODLINE>
		<TYPEBUREAU></TYPEBUREAU>
		<PART></PART>
		<!-- 97397 start-->
		<!--<AUDITFREQ>A</AUDITFREQ> -->
		<AUDITFREQ><xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/></AUDITFREQ>
		<!-- 97397 start-->
		<REINSIND></REINSIND>
		<REASON></REASON>
		<TRANS>18</TRANS>
        <EXPOSURE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">EXPOSURE</xsl:with-param>
            <xsl:with-param name="FieldLength">9</xsl:with-param>
            <xsl:with-param name="Value" select="ActualRemunerationAmt/Amt"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPOSURE>
		<LIMITOCCUR>000000000</LIMITOCCUR>
        <LIMITAGGER>000000000</LIMITAGGER>
        <DEDUCTIBLE>0000000</DEDUCTIBLE>
        <COMMISSION>000000</COMMISSION>
        <COMMAMT>00000000000</COMMAMT>
        <TOTALORIG>00000000000</TOTALORIG>
        <ORIGINAL>00000000000</ORIGINAL>
        <PREMIUM>00000000000</PREMIUM>
        <TOTALPREM>00000000000</TOTALPREM>
        <!-- 50764 start -->
		<xsl:choose>
			<xsl:when test="$TYPEACT='RB'">
				<TYPEACT>RB</TYPEACT>
			</xsl:when>
			<xsl:otherwise>
				<TYPEACT>NB</TYPEACT>
			</xsl:otherwise>
		</xsl:choose>
		<!-- 50764 end --> 		
		<BILLIND></BILLIND>
		<REINS></REINS>
		<COVSTATUS></COVSTATUS>
	</PMSPSA15__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext></MapperMetaTag>
</metaInformation>
--><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->