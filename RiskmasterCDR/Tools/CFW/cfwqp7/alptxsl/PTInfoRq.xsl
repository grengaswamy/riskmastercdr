<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="ACORD/InsuranceSvcRq/*" mode="CreatePTInfo">
  <xsl:variable name="TableName">INFOREC</xsl:variable>
    <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>INFOREC</RECORD__NAME>
         </RECORD__NAME__ROW>
         <PTInfo__RECORD>
            <REMOVEQUOTE>
				<!--103409-->
				<xsl:value-of select="../com.csc_HostProcessingDirectives/com.csc_RemoveQuoteInd"/>
				<!--103409-->
            </REMOVEQUOTE>
            <USEPOLGEN>
				<!--103409-->
				<xsl:value-of select="../com.csc_HostProcessingDirectives/com.csc_PolicyGenerationInd"/>
				<!--103409-->
            </USEPOLGEN>
            <USEDEBUG>
				<!--<xsl:value-of select="com.csc_HostInfo/com.csc_DebugInd"/>-->		<!--103409-->
				<xsl:value-of select="../com.csc_HostProcessingDirectives/com.csc_DebugInd"/>				<!--103409-->
            </USEDEBUG>
			<!-- 99284 start -->
			<VERIFYPOLICY>
				<!--<xsl:value-of select="com.csc_HostInfo/com.csc_VerifyPolicyInd"/>--><!--103409-->
				<xsl:value-of select="../com.csc_HostProcessingDirectives/com.csc_VerifyPolicyInd"/>		<!--103409-->
			</VERIFYPOLICY>
			<!-- 99284 end -->
         </PTInfo__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>