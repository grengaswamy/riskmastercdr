<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 14734 
***********************************************************************************************
-->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateWorkCompRateState">
        <xsl:param name="NodeList"/>
        <xsl:variable name="State" select="$NodeList/STATE"/>
 	    <WorkCompRateState>
	       <StateProvCd><xsl:value-of select="$State"/></StateProvCd>
		   <!-- issue # 64227 Starts-->
		   <!--<AnniversaryRatingDt/>-->
	       <AnniversaryRatingDt>
			<xsl:call-template name="ConvertPTDateToISODate">
				<xsl:with-param name="Value" select="/*/PMSPWC02__RECORD/ANVDATE"/>
			</xsl:call-template>
		  </AnniversaryRatingDt>
		   <!-- issue # 64227 Ends-->
	       <!-- Issue 63993 Begin <NCCIIDNumber/> -->
		<NCCIIDNumber><xsl:value-of select="$NodeList/ESCNO"/></NCCIIDNumber>
		<!-- Issue 63993 End -->
	          <!-- 29653 Start -->
	          <!-- <xsl:for-each select="/*/ISLPPP5__RECORD"> -->
	          <xsl:for-each select="/*/PMSPWC25__RECORD">
	          <!-- 29653 End  -->
                    <CreditOrSurcharge>
                        <!-- 29653 Start -->
                        <!-- <CreditSurchargeCd><xsl:value-of select="MODIDENT"/></CreditSurchargeCd> -->
                        <CreditSurchargeCd><xsl:value-of select="MODTYPE"/></CreditSurchargeCd>
                        <!-- 29653 End   -->
                          <NumericValue>
                             <FormatCurrencyAmt>
                                <Amt><xsl:value-of select="FLATAMT"/></Amt>
                             </FormatCurrencyAmt>   
                          </NumericValue>
                    </CreditOrSurcharge>
                </xsl:for-each>	 
                               <!-- 29653 Start -->
		          <!--- <xsl:for-each select="/WorkCompPolicyQuoteInqRs/ISLPPP4__RECORD[STATE=$State]"> -->
                  <!--  <xsl:call-template name="CreateWorkCompLocInfo"/> -->
                  <!-- </xsl:for-each> -->
                  <!-- 29653 End   -->
                                   
                  <!-- 29653 Start -->
                  
				                    
                  <xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMSPSA15__RECORD[STATE=$State]">
              	     <xsl:variable name="UnitNo" select="UNITNO"/>
                     <xsl:variable name="CovSeq" select="COVSEQ"/>
                     <xsl:variable name="TransSeq" select="TRANSSEQ"/>
					 <xsl:variable name="Classnum" select="CLASSNUM"/>
 	     		     <xsl:variable name="Bureau" select="/*/PMSPSA35__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/BUREAU34"/>
			         <xsl:variable name="Spec50" select="/*/PMSPSA36__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/SPEC50"/>  
		                   
			  	     <xsl:choose>
                         <xsl:when  test="$Classnum='XXXA'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
								<xsl:with-param name="Bureau" select="$Bureau"/>	<!--62825 -->
				            </xsl:call-template>	
			             </xsl:when>
			
			             <xsl:when  test="$Classnum='XXXM'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				            </xsl:call-template>	
			    		</xsl:when>

			            <xsl:when  test="$Classnum='XXXS'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:when  test="$Classnum='XXXP'">
			               <xsl:variable name="Rate">000.000</xsl:variable>  
                           <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:otherwise>
				           <xsl:variable name="Rate"> 
						   <xsl:call-template name="CreateWCRate"> 
		                       <xsl:with-param name="Bureau" select="$Bureau"/>
		                       <xsl:with-param name="Spec50" select="$Spec50"/>
        		           </xsl:call-template>
                           </xsl:variable> 
						                           
				           <xsl:call-template name="CreateWorkCompLocInfo">
				              <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>					   
					</xsl:otherwise>
			</xsl:choose>
						   
						   
						   
				  </xsl:for-each>
                  <!-- 29653 End -->
                  
	    </WorkCompRateState>
    </xsl:template>    
    
<!-- Case 31594 Begin -->
	<xsl:template name="CreateWCRateState">
		<xsl:variable name="State" select="STATE"/>
		<WorkCompRateState>
			<StateProvCd>
				<xsl:call-template name="ConvertNumericStateCdToAlphaStateCd">
					<xsl:with-param name="Value" select="$State"/>
				</xsl:call-template>
			</StateProvCd>
		   <!-- issue # 64227 Starts-->
			   <!--<AnniversaryRatingDt/>-->
		       <AnniversaryRatingDt>
			   <xsl:call-template name="ConvertPTDateToISODate">
				<xsl:with-param name="Value" select="/*/PMSPWC02__RECORD/ANVDATE"/>
			</xsl:call-template>
		   </AnniversaryRatingDt>
		   <!-- issue # 64227 Ends-->
			<!-- Issue 63993 Begin <NCCIIDNumber/> -->
			<NCCIIDNumber><xsl:value-of select="ESCNO"/></NCCIIDNumber>
			<!-- Issue 63993 End -->
			<xsl:for-each select="/*/PMSPWC25__RECORD[STATE=$State]">
				<!--<xsl:if test="string-length(PMSPWC25__RECORD/DELPND)=0">--><!--SYSDST Log Issue #70-->
				<xsl:if test="DELPND!='D'">	<!--SYSDST Log Issue #70-->
					<CreditOrSurcharge>
						<CreditSurchargeCd><xsl:value-of select="MODTYPE"/></CreditSurchargeCd>
						<!-- 55614 Begin
						<NumericValue>
							<FormatModFactor>
								<xsl:value-of select="RATE"/>
							</FormatModFactor>   
							<FormatCurrencyAmt>
								<Amt><xsl:value-of select="FLATAMT"/></Amt>
							</FormatCurrencyAmt>
							<FormatInteger>
								<xsl:value-of select="RATEOP"/>
							</FormatInteger>
						</NumericValue>-->
						<NumericValue>
							<FormatModFactor><xsl:value-of select="RATE"/></FormatModFactor>
						</NumericValue>
						<NumericValue>	   
							<FormatCurrencyAmt>
								<Amt><xsl:value-of select="FLATAMT"/></Amt>
							</FormatCurrencyAmt>
						</NumericValue>	
						<NumericValue>
							<FormatInteger><xsl:value-of select="RATEOP"/></FormatInteger>
						</NumericValue>					
						<!-- 55614 End -->
						<!--64227 start-->
						<xsl:if test="RATE2 &gt; 0"> 
						<NumericValue>
							<!--<com.csc_AnniversaryFactor><xsl:value-of select="RATE2"/></com.csc_AnniversaryFactor>--><!--103409-->
							<FormatPct><xsl:value-of select="RATE2"/></FormatPct><!--103409-->
						</NumericValue>
						</xsl:if>
						<!--64227 end-->
					</CreditOrSurcharge>
				</xsl:if>
			</xsl:for-each>	              	                 
			<!-- 101660 begin	-->
			<!--              	                 
			<xsl:for-each select="/*/PMSPSA15__RECORD[STATE=$State]">
				<xsl:variable name="SA15Location" select="UNITNO"/>
				<xsl:if test="/*/PMSPWC04__RECORD[SITE=$SA15Location]/DROPIND != 'Y'">
					<xsl:if test="string(EXPOSURE) != '0'">
						<xsl:call-template name="CreateWCLocInfo"/>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>-->
			<xsl:for-each select="/*/PMSPWC04__RECORD[STATE=$State and DROPIND != 'Y']">
						<xsl:call-template name="CreateWCLocInfo"/>
			</xsl:for-each>
			<!-- 101660 end -->
			<xsl:for-each select="/*/PMSPWC03__RECORD[STATE=$State]">
				<xsl:call-template name="CreateOptionalForms"/>
			</xsl:for-each>
			<!-- Issue 104053 Starts, Section Commented -->
			<!--SYSDST Log #36 Start-->
			<!--<xsl:variable name="PrimState" select="/*/PMSP0200__RECORD/RISK0STATE"></xsl:variable>
			<xsl:if test="$State = $PrimState">
				<xsl:for-each select="/*/PMSPWC03__RECORD[STATE='00']">
				<xsl:call-template name="CreateOptionalForms"/>
			</xsl:for-each>
			</xsl:if>-->
			<!--SYSDST Log #36 End-->
			<!-- Issue 104053 Ends -->
			<com.csc_ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_TableNumber</OtherIdTypeCd>
					<OtherId><xsl:value-of select="/*/PMSPWC04__RECORD[STATE=$State]/PDTABLE"/></OtherId>
				</OtherIdentifier>
			<!-- 55614 Begin	
			</com.csc_ItemIdInfo>
			<com.csc_ItemIdInfo>
			55614 End -->
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_FormInd</OtherIdTypeCd>
					<OtherId><xsl:value-of select="/*/PMSPWC04__RECORD[STATE=$State]/PDSLIND"/></OtherId>
				</OtherIdentifier>
				<!-- 94178 Start -->
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_GroupProgram</OtherIdTypeCd>
					<OtherId><xsl:value-of select="/*/BASPWC07E__RECORD[STATE=$State]/GRPCODE"/></OtherId>
				</OtherIdentifier>
				<!-- changed name of table from pmspwc07a to baspwc07e -->
				<!--94178 End -->
			</com.csc_ItemIdInfo>
			<!--SYSDST Log #36-->			
			<!--<xsl:for-each select="/*/PMSPWC03__RECORD">
				<xsl:call-template name="CreateOptionalForms"/>
			</xsl:for-each> -->
			<!--SYSDST Log #36-->
		</WorkCompRateState>
	</xsl:template>
<!-- Case 31594 End -->
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->