<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="CreateISLPPP0100">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ISLPPP0100</RECORD__NAME>
         </RECORD__NAME__ROW>
  
         <ISLPPP0100__RECORD>
            <CLIENTNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/></CLIENTNAME>  
            <FIRSTNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName"/></FIRSTNAME> 
            <MIDNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/OtherGivenName"/></MIDNAME> 
            <SUFFIXNM><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/NameSuffix"/></SUFFIXNM>
            <LONGNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/com/csc_LongName"/></LONGNAME> 
         	<DBA><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/></DBA> 
         	<PHONE1><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"/></PHONE1> 
         	<FEDTAXID><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/></FEDTAXID>
         	<SSN><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/></SSN>
         	<SEX><xsl:value-of select="InsuredOrPrincipal/DriverInfo/PersonInfo/GenderCd"/></SEX>
         	<BIRTHDATE>2001-01-02</BIRTHDATE>
        </ISLPPP0100__RECORD>

      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->