<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
  <xsl:template name="CreateISLPPP3">
    <xsl:variable name="TableName">ISLPPP3</xsl:variable>
    <xsl:variable name="LocationId" select="@id"/>
    <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)"/>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP3</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP3__RECORD>
        <RECID>PP3</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="Addr/StateProvCd"/>
        </STATE>
        <SITE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">SITE</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="$LocationNumber"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </SITE>
        <EXPMODE1>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">SITE</xsl:with-param>
            <xsl:with-param name="FieldLength">4</xsl:with-param>
            <xsl:with-param name="Precision">3</xsl:with-param>
            <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/CreditOrSurcharge[CreditSurchargeCd='EXP1']/NumericValue/FormatModFactor"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPMODE1>
        <EXPMODE2>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">SITE</xsl:with-param>
            <xsl:with-param name="FieldLength">4</xsl:with-param>
            <xsl:with-param name="Precision">3</xsl:with-param>
            <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/CreditOrSurcharge[CreditSurchargeCd='EXP2']/NumericValue/FormatModFactor"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPMODE2>
        <NAME>
          <xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
        </NAME>
        <OPTIONAL1>
          <xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
          <!--xsl:value-of select="Addr/Addr2"/-->
        </OPTIONAL1>
        <ADDRESS1>
          <xsl:value-of select="Addr/Addr1"/>
        </ADDRESS1>
        <CITYST>
          <xsl:value-of select="substring(concat(Addr/City, '                                                                                                    '), 1, 28)"/>
          <xsl:value-of select="Addr/StateProvCd"/>
        </CITYST>
        <ZIP>
          <xsl:value-of select="Addr/PostalCode"/>
        </ZIP>
        <CONTACT>
        </CONTACT>
        <PDTABLE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
            <xsl:with-param name="FieldLength">2</xsl:with-param>
            <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </PDTABLE>
        <PDSLIND>
          <xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/>
        </PDSLIND>
        <SCHPCT>10000</SCHPCT>
        <ARAPMOD>000</ARAPMOD>
        <ARAPRATE>000</ARAPRATE>
        <BUREAUID>
        </BUREAUID>
        <NUMEMPL>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">NUMEMPL</xsl:with-param>
            <xsl:with-param name="FieldLength">6</xsl:with-param>
            <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/WorkCompLocInfo/NumEmployees"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </NUMEMPL>
        
        <!-- Case 39343 Start -->
        <AUDTYPE>
            <xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd"/>
        </AUDTYPE>
        <AUDBASIS>P</AUDBASIS>
        <!-- Case 39343 End   -->
        
        <FILLR>
        </FILLR>
      </ISLPPP3__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>

  <!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
  
<!-- Case 31594 begin - Modified the template to build one PP3 segement for each location -->
<xsl:template name="CreateISLPPP3Defaults">
    <xsl:variable name="TableName">ISLPPP3</xsl:variable>
	<xsl:variable name="LocationNbr" select="@LocationRef"/>
	<xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
	<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode"/>
	<xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP3</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP3__RECORD>
        <RECID>PP3</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="../StateProvCd"/>
        </STATE>
        <SITE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">SITE</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </SITE>
        <EXPMODE1>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">EXPMODE1</xsl:with-param>
            <xsl:with-param name="FieldLength">4</xsl:with-param>
            <xsl:with-param name="Precision">3</xsl:with-param>
            <xsl:with-param name="Value" select="../CreditOrSurcharge[CreditSurchargeCd='EXP1']/NumericValue/FormatModFactor"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPMODE1>
        <EXPMODE2>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">EXPMODE2</xsl:with-param>
            <xsl:with-param name="FieldLength">4</xsl:with-param>
            <xsl:with-param name="Precision">3</xsl:with-param>
            <xsl:with-param name="Value" select="../CreditOrSurcharge[CreditSurchargeCd='EXP2']/NumericValue/FormatModFactor"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPMODE2>
        <NAME>
        	<xsl:variable name="LocationNumber" select="substring(/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef,2)"/>
        	<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal
        						[substring(@id,2)=$LocationNumber]
        						/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
        </NAME>
		<xsl:choose>
			<xsl:when test="$LocationPath/Addr1!=''">
        		<OPTIONAL1>
        			<xsl:variable name="NameRef" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef"/>
        			<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[@id=$NameRef]/GeneralPartyInfo/
        									NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
        		</OPTIONAL1>
        		<ADDRESS1><xsl:value-of select="$LocationPath/Addr1"/></ADDRESS1> 
        		<CITYST>
        			<xsl:value-of select="substring(concat($LocationPath/City, '                                                                                                    '), 1, 28)"/>
          			<xsl:value-of select="$LocationPath/StateProvCd"/>
          		</CITYST>
        		<ZIP><xsl:value-of select="$LocationPath/PostalCode"/></ZIP>
			</xsl:when>
			<xsl:otherwise>
				<OPTIONAL1>Generated Optional 1</OPTIONAL1>
        		<ADDRESS1>Generated Address 1</ADDRESS1>
        		<CITYST>
          			<xsl:value-of select="substring(concat('Generated City', '                                                                                                    '), 1, 28)"/>
          			<xsl:if test="string-length($LocationPath/StateProvCd) != 0">
          				<xsl:value-of select="$LocationPath/StateProvCd"/>
          			</xsl:if>
          			<xsl:if test="string-length($LocationPath/StateProvCd) = 0">
          				<xsl:value-of select="'DC'"/>
          			</xsl:if>
        		</CITYST>
        		<ZIP>12345</ZIP>
			</xsl:otherwise>
		</xsl:choose>
        <CONTACT>
        </CONTACT>
        <PDTABLE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
            <xsl:with-param name="FieldLength">2</xsl:with-param>
            <xsl:with-param name="Value" select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </PDTABLE>
        <PDSLIND>
          <xsl:value-of select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/>
        </PDSLIND>
        <SCHPCT>10000</SCHPCT>
        <ARAPMOD>000</ARAPMOD>
        <ARAPRATE>000</ARAPRATE>
        <BUREAUID>
        </BUREAUID>
        <NUMEMPL>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">NUMEMPL</xsl:with-param>
            <xsl:with-param name="FieldLength">6</xsl:with-param>
            <xsl:with-param name="Value" select="NumEmployees"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </NUMEMPL>
        
        <!-- Case 39343 Start -->
        <AUDTYPE>
            <xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd"/>
        </AUDTYPE>
        <AUDBASIS>P</AUDBASIS>
        <!-- Case 39343 End   -->
             
        <FILLR>
        </FILLR>
      </ISLPPP3__RECORD>
    </BUS__OBJ__RECORD>
    </xsl:if>
  </xsl:template>
  <!-- Case 31594 end -->


</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->