<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="CreateSubLocation">
<xsl:param name="Location"/>
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBVCPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBVCPL1__RECORD>
<BVAACD><xsl:value-of select="$LOC"/></BVAACD>
<BVABCD><xsl:value-of select="$MCO"/></BVABCD>
<BVARTX><xsl:value-of select="$SYM"/></BVARTX>
<BVASTX><xsl:value-of select="$POL"/></BVASTX>
<BVADNB><xsl:value-of select="$MOD"/></BVADNB>
<BVBRNB><xsl:value-of select="../ItemIdInfo/InsurerId" /></BVBRNB>
<BVEGNB><xsl:value-of select="ItemIdInfo/InsurerId" /></BVEGNB>
<BVC6ST>P</BVC6ST>
<BVTYPE0ACT>
<!-- 34771 start -->
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
<!-- Issue 103409 Ends -->
<!-- 34771 end -->
</BVTYPE0ACT>
<BVEFTX><xsl:value-of select="SubLocationDesc" /></BVEFTX>
<BVEGTX><xsl:value-of select="../Addr/Addr1" /></BVEGTX>
<BVEITX><xsl:value-of select="substring-after(../Addr/County,'-')" /></BVEITX>
<BVEJTX><xsl:value-of select="../Addr/StateProvCd" /></BVEJTX>
<BVAPNB><xsl:value-of select="substring-before(../Addr/County,'-')" /></BVAPNB>
</ASBVCPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->