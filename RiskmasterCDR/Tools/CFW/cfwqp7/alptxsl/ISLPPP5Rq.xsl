<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge (Mod) - 31594
-->
  <xsl:template name="CreateISLPPP5">
    <xsl:variable name="TableName">ISLPPP5</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP5</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP5__RECORD>
        <RECID>PP5</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="../StateProvCd"/>
        </STATE>
        <MODIDENT>
          <xsl:value-of select="CreditSurchargeCd"/>
        </MODIDENT>
        <RATINGOP>
          <xsl:value-of select="NumericValue/FormatInteger"/>
        </RATINGOP>
        <RATINGIN>
        </RATINGIN>
        <FLATAMT>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">FLATAMT</xsl:with-param>
            <xsl:with-param name="FieldLength">12</xsl:with-param>
            <xsl:with-param name="Value" select="NumericValue/FormatCurrencyAmt/Amt"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
         </FLATAMT>
        <OPRATESEQ>000</OPRATESEQ>
        <RATE>
        	<!-- Case 31594 Begin -->
        	<xsl:if test="CreditSurchargeCd != 'INFF'">
        		<xsl:value-of select="NumericValue/FormatModFactor"/>
        	</xsl:if>
        	<xsl:if test="CreditSurchargeCd = 'INFF' 
        				  and string-length(NumericValue/FormatCurrencyAmt/Amt) = 0">
          		<xsl:value-of select="NumericValue/FormatModFactor"/>
          	</xsl:if>
          	<!-- Case 31594 Begin -->
        </RATE>
        <RATE2>
		<!-- Case 39344 Begin -->
              <xsl:value-of select="NumericValue/com.csc_AnniversaryFactor"/>

		<!-- Case 39344 End -->
	</RATE2>
      </ISLPPP5__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->