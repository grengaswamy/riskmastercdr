<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Add)
-->
  <xsl:template name="CreatePMSPSA35">
    <xsl:param name="Action"/>
    <xsl:variable name="TableName">PMSPSA35</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPSA35</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPSA35__RECORD>
      	<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
      	<UNITNO>
			<xsl:call-template name="FormatData">
            	<xsl:with-param name="FieldName">UNITNO</xsl:with-param>
            	<xsl:with-param name="FieldLength">5</xsl:with-param>
            	<xsl:with-param name="Value" select="substring(../@LocationRef,2,string-length(../@LocationRef)-1)"/>
            	<xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
		</UNITNO>
		<COVSEQ>
		<!-- 29653 Start -->
        <!-- 29653 Start -->
		<!-- DST issue WC 017 start -->
        <xsl:choose>
            <xsl:when test="$TYPEACT = 'EN'">
			<!--103409 Starts-->		
			<!--<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
				<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:if>
			<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
                <xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
             		<xsl:with-param name="FieldLength">4</xsl:with-param>
					<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier/OtherId"/>					
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
			</xsl:if>-->
			<xsl:choose>
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
					<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 Issue 103409 starts
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd!='DeemedType']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>-->
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostID']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 Issue 103409 ends-->
			</xsl:choose>
			<!--103409 Ends-->
			<!-- DST issue WC 017 end -->
            </xsl:when>
		    <xsl:otherwise>
                <xsl:value-of select="position()"/>
            </xsl:otherwise>
         </xsl:choose>
         <!-- 29653 End -->
		</COVSEQ>
		<TRANSSEQ>0001</TRANSSEQ>
		<TYPEBUREAU>WC</TYPEBUREAU>
		<BUREAU34>
			<!--xsl:value-of select="substring(RatingClassificationCd,5,2)"/  78200 - Class code issue-->
			<xsl:value-of select="substring(RatingClassificationCd,7,2)"/> 
		</BUREAU34>
		<TRANS>18</TRANS>
		<!-- 50764 start -->
		<xsl:choose>
			<xsl:when test="$TYPEACT='RB'">
				<TYPEACT>RB</TYPEACT>
			</xsl:when>
			<xsl:otherwise>
				<TYPEACT>NB</TYPEACT>
			</xsl:otherwise>
		</xsl:choose>
		<!-- 50764 end --> 
	</PMSPSA35__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->