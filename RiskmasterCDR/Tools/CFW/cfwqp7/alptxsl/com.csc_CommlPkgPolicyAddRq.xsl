<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Formatting Routines -->
    <xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->
    <xsl:include href="PMSP0200Rq.xsl"/>  <!-- Basic Contract Record -->
    <xsl:include href="PMSP1200Rq.xsl"/>  <!-- Additional Interest Record -->
    <xsl:include href="ASBACPL1Rq.xsl"/>  <!-- Policy Level Record -->
    <xsl:include href="ASBBCPL1Rq.xsl"/>  <!-- Insurance Line Record -->
    <xsl:include href="ASBCCPL1Rq.xsl"/>  <!-- Product Record -->
    <xsl:include href="ASBUCPL1Rq.xsl"/>  <!-- Location Record -->
    <xsl:include href="ASBVCPL1Rq.xsl"/>  <!-- Sublocation Record -->
    <xsl:include href="ASBYCPL1Rq.xsl"/>  <!-- Coverage Record -->
    <xsl:include href="ASB5CPL1Rq.xsl"/>  <!-- Unit Record -->
    <xsl:include href="ALB5CPL1Rq.xsl"/>  <!-- Unit Record (Add'l Info) -->
	<xsl:include href="ASCSCPL1Rq.xsl"/>	<!--UNI00016-->
	<xsl:include href="ASBZCPL1Rq.xsl"/>	<!--UNI00016-->
	<xsl:include href="PMSP1200ATRq.xsl"/>	<!--UNI00016-->
	<!--<xsl:include href="BPOP0200E.xsl"/>	--><!--clp00015-->	<!--UNI00173B-->
    
    <!-- Start Case 27708 - Add client files Jeff Simmons --> 
    <xsl:include href="BASCLT0100Rq.xsl"/>
    <xsl:include href="BASCLT0300Rq.xsl"/>
    <xsl:include href="BASCLT1400Rq.xsl"/>
    <xsl:include href="BASCLT1500Rq.xsl"/> 
	<xsl:include href="ASBECPL1Rq.xsl"/>
    <!-- End   Case 27708 - Add client files Jeff Simmons --> 
    
    <!-- UNI00089 - ATB - Start -->
    <xsl:include href="PTInfoRq.xsl"/>
    <!-- UNI00089 - ATB - End   -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
<!-- UNI00180 Begin - force CPP policies to have CAP symbol when issued -->
    <xsl:variable name="SYM" >
	<xsl:choose>
	   <xsl:when test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/CompanyProductCd='CPP'">CPP</xsl:when>
	   <xsl:when test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/CompanyProductCd='QAP'">CPP</xsl:when>
  	   <xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/CompanyProductCd"/></xsl:otherwise>
	</xsl:choose>
    </xsl:variable>
<!-- UNI00180 Begin - force CPP policies to have CAP symbol when issued -->
    <!-- 29653 start -->
    <xsl:variable name="TYPEACT">
  	<xsl:choose>
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">NB</xsl:when>-->		<!--103409-->
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">NB</xsl:when>		<!--103409-->
		<xsl:otherwise>EN</xsl:otherwise>
  	</xsl:choose>
    </xsl:variable> 
    <!-- 29653 end -->
    <xsl:variable name="POL">
	  	    <xsl:call-template name="FormatData">
	   	  		<xsl:with-param name="FieldName">$POL</xsl:with-param>      
		     		 <xsl:with-param name="FieldLength">7</xsl:with-param>
		     		 <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/PolicyNumber"/>
		      		<xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template> 
	</xsl:variable>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	  	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/LOBCd"/>   
    <xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/ControllingStateProvCd"/>
                
    <xsl:template match="/">
        <xsl:element name="com.csc_CommlPkgPolicyAddRq">
		<!-- Start Changes done for CPP D12-->
		<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq" mode="CreatePTInfo"/> 
		<!-- End  Changes done for CPP D12-->
           <!-- 39568 Start --><!-- UNI00089 - ATB -->
		    <!--<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq" mode="CreatePTInfo"/> -->
<!-- UNI00089C			<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq" mode="CreatePTInfo"/> 	-->
            <!-- 39568 End   --><!-- UNI00089 - ATB -->
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy" mode="PMSP0000"/>
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq" mode="PMSP0200"/>
<!-- Build Policy Level Additional Interest Records -->
<!--UNI00297 Start-->
	<!--xsl:variable name="AddInsName" select="AdditionalInterest/AdditionalInterestInfo/NatureInterestCd"/-->		<!--UNI00170-->
	<!--xsl:if test="$AddInsName != ''"-->										<!--UNI00170-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/AdditionalInterest">
              <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
		<xsl:call-template name="CreateAddlInterest"/>
	      </xsl:if>
	    </xsl:for-each> 
	<!--/xsl:if-->													<!--UNI00170-->           
<!--UNI00297 End-->

<!--UNI00173B Starts -->
<!--clp00015 not base-->
<!--	<xsl:variable name="UserId" select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Producer/com.csc_UserId"/>
		<xsl:call-template name="CreateBPOP0200E">
        	<xsl:with-param name="UserId" select="$UserId"/>    
        </xsl:call-template>  -->
<!--UNI00173B Ends -->

            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location">
                <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>
<!-- Build Location Records -->
               <xsl:call-template name="CreateLocation">
                     <xsl:with-param name="Location" select="$Location"/>    
                </xsl:call-template>
<!-- Build SubLocation Records -->
                <xsl:for-each select="SubLocation">
                    <xsl:call-template name="CreateSubLocation">
                        <xsl:with-param name="Location" select="$Location"/>
                    </xsl:call-template>
                </xsl:for-each>
<!-- Build Additional Interests Attached to SubLocations -->
                <xsl:for-each select="SubLocation/AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                      <xsl:call-template name="CreateAddlInterest">
                         <xsl:with-param name="Location" select="$Location"/>
                         <xsl:with-param name="SubLocation" select="ItemIdInfo/InsurerId"/>                         
                      </xsl:call-template>
                   </xsl:if>                    
                </xsl:for-each> 
<!-- Build Additional Interests Attached to this Location -->
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="Location" select="$Location"/>    
                        </xsl:call-template>
                   </xsl:if>                           
                </xsl:for-each> 
            </xsl:for-each>
<!-- Build CPP Policy Level Record -->
            <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq" mode="ASBACPL1"/>
<!-- Build CPP Insurance Line Information Records -->
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CommlPropertyLineBusiness"/>
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_GeneralLiabilityLineBusiness"/>
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CrimeLineBusiness"/>
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CommlInlandMarineLineBusiness"/>
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness"/>		<!--UNI00016-->
           
           <!-- Start Case 27708 - Add client files Jeff Simmons --> 
		   <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq">
              <xsl:call-template name="CreateBASCLT0100"></xsl:call-template>
              <xsl:call-template name="CreateBASCLT0300"></xsl:call-template>
              <xsl:call-template name="CreateBASCLT1400"></xsl:call-template>
           </xsl:for-each>
    
           <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/AdditionalInterest"> 
              <xsl:call-template name="CreateBASCLT1500"/> 
           </xsl:for-each> 
           
           <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location">  
              <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>  
              <xsl:for-each select="SubLocation/AdditionalInterest">
                 <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                   <xsl:call-template name="CreateBASCLT1500">
                      <xsl:with-param name="Location" select="$Location"/> 
                      <xsl:with-param name="SubLocation" select="ItemIdInfo/InsurerId"/>      
                          
                   </xsl:call-template>
                 </xsl:if>                    
              </xsl:for-each>
           
              <xsl:for-each select="AdditionalInterest">
                 <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                   <xsl:call-template name="CreateBASCLT1500">
                      <xsl:with-param name="Location" select="$Location"/>    
                   </xsl:call-template>
                 </xsl:if>                          
              </xsl:for-each>
           </xsl:for-each> 
           <!-- End   Case 27708 - Add client files Jeff Simmons --> 
	<!-- UNI00089C	moved from top to bottom  -->
<!-- Start Changes done for CPP D12-->	
	   <!--<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq" mode="CreatePTInfo"/> 	-->
	   <!-- End Changes done for CPP D12-->
       </xsl:element>           
    </xsl:template>
           
    <xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_GeneralLiabilityLineBusiness">           
                <xsl:variable name="InsLine">GL</xsl:variable>
 <!-- Build GL Insurance Line Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine" select="$InsLine"/>
                </xsl:call-template>
  <!-- Build GL Unit Records  -->
         <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location">
            <xsl:variable name="id" select="@id"/>
            <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_GeneralLiabilityLineBusiness/LiabilityInfo/GeneralLiabilityClassification[@LocationRef=$id and CommlCoverage/CoverageCd!='PCO']"/>
          <xsl:if test="$Node !=''">		<!--UNI00581-->
            <xsl:call-template name="BuildUnitRecord">
                <xsl:with-param name="Location" select="substring($id,2,string-length($id)-1)"/>
                <xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$id]/Addr/StateProvCd"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Product" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>
            </xsl:call-template> 
            <xsl:call-template name="BuildALB5CPL1">
               <xsl:with-param name="Location" select="substring($id,2,string-length($id)-1)"/>
               <xsl:with-param name="SubLocation" select="'0'"/>
               <xsl:with-param name="InsLine" select="$InsLine"/>
               <xsl:with-param name="Node" select="$Node"/>
            </xsl:call-template>                
	  </xsl:if>				<!--UNI00581-->               
        </xsl:for-each>
<!-- Build GL Product Record -->
                <xsl:for-each select="com.csc_ModificationFactors">
                    <xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="StateProvCd"/>
                    </xsl:call-template>
                </xsl:for-each>
				<!-- Issue 59878 - Start -->
				<!-- Build the Forms -->
				<xsl:for-each select="com.csc_Form">
		    		<xsl:if test="FormNumber !=''">
						<xsl:call-template name="BuildForms">
							<xsl:with-param name="InsLine">GL</xsl:with-param>
						</xsl:call-template>
		    		</xsl:if>
				</xsl:for-each>
				<!-- Issue 59878 - End -->
<!-- Build GL Coverage Records -->
		<!-- Begin UNI00042VQ4 -->  
		<!-- 	Exclude Covs that are distinct in iSol but are merged into sequences of one cov in Point -->
		<!-- 	Currently these include: BADDCA/BADDLA and PLBARB/PLBEAU/PLPRIN -->
		<!--<xsl:for-each select="LiabilityInfo/GeneralLiabilityClassification[CommlCoverage/CoverageCd!='PCO']"> -->
		<xsl:for-each select="LiabilityInfo/GeneralLiabilityClassification[CommlCoverage/CoverageCd!='PCO' and CommlCoverage/CoverageCd!='BADDCA' and CommlCoverage/CoverageCd!='BADDLA' and CommlCoverage/CoverageCd!='PLBARB' and CommlCoverage/CoverageCd!='PLBEAU' and CommlCoverage/CoverageCd!='PLPRIN']">
		<!-- End UNI00042VQ4 -->
			<xsl:variable name="LocationRef" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=concat('l',$LocationRef)]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="$LocationRef"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Begin UNI00042VQ4 -->  
		<!-- 	Process BADDCA/BADDLA Covs that are distinct in iSol but are merged into sequences of one cov in Point -->
		<xsl:for-each select="LiabilityInfo/GeneralLiabilityClassification[CommlCoverage/CoverageCd='BADDCA' or CommlCoverage/CoverageCd='BADDLA']">
			<xsl:variable name="LocationRef" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=concat('l',$LocationRef)]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="$LocationRef"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="SeqNbr" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Process PLBARB/PLBUAE/PLPRIN Covs that are distinct in iSol but are merged into sequences of one cov in Point -->
		<xsl:for-each select="LiabilityInfo/GeneralLiabilityClassification[CommlCoverage/CoverageCd='PLBARB' or CommlCoverage/CoverageCd='PLBEAU' or CommlCoverage/CoverageCd='PLPRIN']">
			<xsl:variable name="LocationRef" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=concat('l',$LocationRef)]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="$LocationRef"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="SeqNbr" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- End UNI00042VQ4 -->
    </xsl:template>                                                 

    <xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CommlPropertyLineBusiness">           
                <xsl:variable name="InsLine">CF</xsl:variable>
 <!-- Build CF Insurance Line Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine" select="$InsLine"/>
                </xsl:call-template>
  <!-- Build CF Unit Records  -->
         <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation">
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
            <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CommlPropertyLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef and SubjectInsuranceCd!='BLNKT']"/>
	  <xsl:if test="$Node !=''">		<!--UNI00581-->
            <xsl:call-template name="BuildUnitRecord">
                <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
                <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
                <xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Product" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>                
            </xsl:call-template>
            <xsl:call-template name="BuildALB5CPL1">
               <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
               <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
               <xsl:with-param name="InsLine" select="$InsLine"/>
               <xsl:with-param name="Node" select="$Node"/>
            </xsl:call-template>                             
	  </xsl:if>				<!--UNI00581-->                                        
        </xsl:for-each>
<!-- Build CF Product Record -->
                <xsl:for-each select="com.csc_ModificationFactors">
                    <xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="StateProvCd"/>
                    </xsl:call-template>
                </xsl:for-each>
				<!-- Issue 59878 - Start -->
				<!-- Build the Forms -->
				<xsl:for-each select="com.csc_Form">
		    		<xsl:if test="FormNumber !=''">
						<xsl:call-template name="BuildForms">
							<xsl:with-param name="InsLine">CF</xsl:with-param>
						</xsl:call-template>
		    		</xsl:if>
				</xsl:for-each>
				<!-- Issue 59878 - End -->
<!-- Build CF Coverage Records -->
        <!--xsl:for-each select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd!='BLNKT']"-->	<!--UNI00281-->
	<xsl:for-each select="PropertyInfo/CommlPropertyInfo[@LocationRef!='l0']">			<!--UNI00281-->
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
            <!--Issue 102807 starts-->
			<xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
			<xsl:variable name="AlarmClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_AlarmClassCd"/>
			<xsl:variable name="WatchmanCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_WatchmanCd"/>
			<xsl:variable name="AlarmConnectionCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_AlarmConnectionCd"/>
			<!--Issue 102807 ends-->
            <xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
            <xsl:call-template name="BuildCoverage">
                <xsl:with-param name="Location">
                   <xsl:choose>
                      <xsl:when test="string-length($LocationRef)">
                         <xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="'0'" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
                <xsl:with-param name="SubLocation" select="$SubLocation"/>                 
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="RateState">
                   <xsl:choose>
                      <xsl:when test="string-length($LocState)">
                         <xsl:value-of select="$LocState" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="$PrimaryState" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
                 	<!--Issue 102807 starts-->
				<xsl:with-param name="AlarmClassCd" select="$AlarmClassCd"/>
				<xsl:with-param name="WatchmanCd" select="$WatchmanCd"/>
				<xsl:with-param name="AlarmConnectionCd" select="$AlarmConnectionCd"/>
				<!--Issue 102807 ends-->
            </xsl:call-template>
        </xsl:for-each>
   </xsl:template>

   <xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CrimeLineBusiness">           
                <xsl:variable name="InsLine">CR</xsl:variable>
 <!-- Build Crime Insurance Line Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine" select="$InsLine"/>
                </xsl:call-template>
  <!-- Build Crime Unit Records  -->
         <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation">
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
            <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CrimeLineBusiness/com.csc_CommlCrimeInfo/com.csc_CommlCrimeClassification[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]"/>
			<xsl:if test="$Node !=''">				<!--UNI00254-->
             <xsl:call-template name="BuildUnitRecord">
                <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
                <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
                <xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Product" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>
             </xsl:call-template>
             <xsl:call-template name="BuildALB5CPL1">
                <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
                <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>
             </xsl:call-template>                                  
			 </xsl:if>								<!--UNI000254-->        
        </xsl:for-each>
<!-- Build Crime Product Record -->
                <xsl:for-each select="com.csc_ModificationFactors">
                    <xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="StateProvCd"/>
                    </xsl:call-template>
                </xsl:for-each>
				<!-- Issue 59878 - Start -->
				<!-- Build the Forms -->
				<xsl:for-each select="com.csc_Form">
		    		<xsl:if test="FormNumber !=''">
						<xsl:call-template name="BuildForms">
							<xsl:with-param name="InsLine">CR</xsl:with-param>
						</xsl:call-template>
		    		</xsl:if>
				</xsl:for-each>
				<!-- Issue 59878 - End -->
<!-- Build Crime Coverage Records -->
        <xsl:for-each select="com.csc_CommlCrimeInfo/com.csc_CommlCrimeClassification">
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
            <xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
            <xsl:call-template name="BuildCoverage">
                <xsl:with-param name="Location">
                   <xsl:choose>
                      <xsl:when test="string-length($LocationRef)">
                         <xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="'0'" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
                <xsl:with-param name="SubLocation" select="$SubLocation"/>                 
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="RateState">
                   <xsl:choose>
                      <xsl:when test="string-length($LocState)">
                         <xsl:value-of select="$LocState" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="$PrimaryState" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

      <xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/com.csc_CommlInlandMarineLineBusiness">           
                <xsl:variable name="InsLine">IMC</xsl:variable>
 <!-- Build Inland Marine Insurance Line Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine" select="$InsLine"/>
                </xsl:call-template>
  <!-- Build Inland Marine Unit Records  -->
         <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlSubLocation">
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
            <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CommlInlandMarineLineBusiness/com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]"/>
			<xsl:if test="$Node !=''">				<!--UNI00254-->
            <xsl:call-template name="BuildUnitRecord">
                <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
                <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
                <xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Product" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>
            </xsl:call-template>
            <xsl:call-template name="BuildALB5CPL1">
                <xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
                <xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="Node" select="$Node"/>
            </xsl:call-template>                                   
			</xsl:if>								<!--UNI00254-->       
        </xsl:for-each>
<!-- Build Inland Marine Product Record -->
                <xsl:for-each select="com.csc_ModificationFactors">
                    <xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="StateProvCd"/>
                    </xsl:call-template>
                </xsl:for-each>
				<!-- Issue 59878 - Start -->
				<!-- Build the Forms -->
				<xsl:for-each select="com.csc_Form">
		    		<xsl:if test="FormNumber !=''">
						<xsl:call-template name="BuildForms">
							<xsl:with-param name="InsLine">IMC</xsl:with-param>
						</xsl:call-template>
		    		</xsl:if>
				</xsl:for-each>
				<!-- Issue 59878 - End -->
<!-- Build Inland Marine Coverage Records -->
       <xsl:for-each select="com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification">
            <xsl:variable name="LocationRef" select="@LocationRef"/>
            <xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
            <xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
            <xsl:call-template name="BuildCoverage">
                <xsl:with-param name="Location">
                   <xsl:choose>
                      <xsl:when test="string-length($LocationRef)">
                         <xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="'0'" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
                <xsl:with-param name="SubLocation" select="$SubLocation"/>                 
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="RateState">
                   <xsl:choose>
                      <xsl:when test="string-length($LocState)">
                         <xsl:value-of select="$LocState" />
                      </xsl:when>
                      <xsl:otherwise>
                         <xsl:value-of select="$PrimaryState" />
                      </xsl:otherwise>
                   </xsl:choose>                
                 </xsl:with-param>
            </xsl:call-template>
						<!-- Issue 59878 - Start-->
			<xsl:for-each select="CommlCoverage/PropertySchedule">
				<xsl:variable name="PropId" select="ItemIdInfo"/>
				<xsl:if test="$PropId !=''">
					<xsl:call-template name="BuildCoverageSupplement">
						<xsl:with-param name="Location">
							<xsl:choose>
								<xsl:when test="string-length($LocationRef)">
									<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'0'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="SubLocation" select="$SubLocation"/>
						<xsl:with-param name="InsLine" select="$InsLine"/>
						<xsl:with-param name="Position" select="1"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
			<xsl:for-each select="CommlCoverage/Watercraft">
				<xsl:variable name="WaterId" select="com.csc_ItemIdInfo"/>
				<xsl:if test="$WaterId !=''">
					<xsl:call-template name="BuildCoverageSupplement">
						<xsl:with-param name="Location">
							<xsl:choose>
								<xsl:when test="string-length($LocationRef)">
									<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'0'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="SubLocation" select="$SubLocation"/>
						<xsl:with-param name="InsLine" select="$InsLine"/>
						<xsl:with-param name="Position" select="1"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
			<!--Issue 59878 - END-->
        </xsl:for-each>
      </xsl:template>

	  <!-- UNI00016 Start : Added CommlAuto Section to Add Rq-->
<!-- Begin ICH00020 Commercial Auto. eCase 35429. -->
	<!-- ICH00020. eCase 35429. -->
	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness ">
		<!-- ICH00020. eCase 35429. -->
		<!-- Begin: Cloned from "CommlAutoPolicyAddRq.xsl", changing XPATHs where needed-->
		<!-- ICH00020. eCase 35429. -->
		<!-- <xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyAddRq/CommlPolicy/ControllingStateProvCd"/> -->
		<!-- ICH00020. eCase 35429. -->
		<xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlPolicy/ControllingStateProvCd"/>
		<!-- ICH00020. eCase 35429. -->
		<!-- Build Product Record -->
		<!-- ICH00020. eCase 35429. -->
		<!-- Begin UNI00016 -->
		<!--        <xsl:call-template name="BuildProductRecord">						-->
		<!-- ICH00020. eCase 35429. -->
		<!--             <xsl:with-param name="InsLine">CA</xsl:with-param>				-->
		<!-- ICH00020. eCase 35429. -->
		<!--            <xsl:with-param name="RateState" select="$PrimaryState"/>		-->
		<!-- ICH00020. eCase 35429. -->
		<!--        </xsl:call-template>												-->
		<!-- ICH00020. eCase 35429. -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/com.csc_ModificationFactors/StateProvCd">
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine">CA</xsl:with-param>
				<xsl:with-param name="RateState" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- End UNI00016 -->

		<!-- Build Trailer Interchange ASBY Record  -->
		<!-- ICH00020. eCase 35429. -->
		<xsl:for-each select="TruckersSupplement/TruckersTrailerInterchange/CommlCoverage">
			<!-- ICH00020. eCase 35429. -->
			<xsl:if test="CoverageCd!=''">
				<!-- ICH00020. eCase 35429. -->
				<xsl:call-template name="BuildTrailerCoverage">
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="InsLine">CA</xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="RateState" select="$PrimaryState"/>
					<!-- ICH00020. eCase 35429. -->
				</xsl:call-template>
				<!-- ICH00020. eCase 35429. -->
			</xsl:if>
			<!-- ICH00020. eCase 35429. -->
		</xsl:for-each>
		<!-- ICH00020. eCase 35429. -->
				<!-- Issue 59878 - Start -->
				<!-- Build the Forms -->
				<xsl:for-each select="com.csc_Form">
		    		<xsl:if test="FormNumber !=''">
						<xsl:call-template name="BuildForms">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
						</xsl:call-template>
		    		</xsl:if>
				</xsl:for-each>
				<!-- Issue 59878 - End -->
		<!-- ICH00300 - AB : start -->
		<!-- Insurance Line Information -->
		<xsl:for-each select="CommlRateState[StateProvCd=$PrimaryState]">
			<xsl:call-template name="BuildInsuranceLine">
				<!-- ICH00020. eCase 35429. -->
				<xsl:with-param name="InsLine">CA</xsl:with-param>
				<!-- ICH00020. eCase 35429. -->
				<xsl:with-param name="RateState" select="$PrimaryState"></xsl:with-param>
				<!-- ICH00020. eCase 35429. -->
			</xsl:call-template>
			<!-- ICH00020. eCase 35429. -->
		</xsl:for-each>
		<!-- State and Vehicle Coverages -->
		<xsl:for-each select="CommlRateState">
			<xsl:variable name="RateState" select="StateProvCd"/>
			<xsl:variable name="VehCount" select="count(CommlVeh)"/>
			<!-- UNI00948 - Start -->
			<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/ItemIdInfo/InsurerId != ''">
			<!-- UNI00948 - End -->
			<xsl:for-each select="CommlVeh">
				<!-- ICH00020. eCase 35429. -->
				<xsl:variable name="Node" select="."/>
				<!-- ICH00020. eCase 35429. -->
				<!-- Build Unit Records -->
				<xsl:call-template name="BuildUnitRecord">
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="Location">0</xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="SubLocation">0</xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="RateState" select="$RateState"/>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="InsLine">CA</xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="Product">CA</xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
					<xsl:with-param name="Node" select="$Node"/>
					<xsl:with-param name="UnitNbr" select="@id"></xsl:with-param>
					<!-- ICH00020. eCase 35429. -->
				</xsl:call-template>
				<!-- ICH00020. eCase 35429. -->
				<!-- Commented as a second ASB5CPP is generated BK starts-->
				<!--xsl:call-template name="BuildALB5CPL1"-->
				<!-- ICH00020. eCase 35429. -->
				<!--xsl:with-param name="Location">0</xsl:with-param-->
				<!-- ICH00020. eCase 35429. -->
				<!--xsl:with-param name="SubLocation">0</xsl:with-param-->
				<!-- ICH00020. eCase 35429. -->
				<!--xsl:with-param name="InsLine">CA</xsl:with-param-->
				<!-- ICH00020. eCase 35429. -->
				<!--xsl:with-param name="Node" select="$Node"/-->
				<!-- ICH00020. eCase 35429. -->
				<!--/xsl:call-template-->
				<!-- ICH00020. eCase 35429. -->
				<!-- Commented as a second ASB5CPP is generated BK ends-->
				<!--Additional Interests attached to this Vehicle-->
				<xsl:for-each select="AdditionalInterest">
					<!-- ICH00020. eCase 35429. -->
					<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
						<!-- ICH00020. eCase 35429. -->
						<xsl:call-template name="CreateAddlInterest">
							<!-- ICH00020. eCase 35429. -->
							<xsl:with-param name="Location">0</xsl:with-param>
							<!-- ICH00020. eCase 35429. -->
							<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>
							<!-- ICH00020. eCase 35429. -->
						</xsl:call-template>
						<!-- ICH00020. eCase 35429. -->
					</xsl:if>
					<!-- ICH00020. eCase 35429. -->
				</xsl:for-each>
				<!-- ICH00020. eCase 35429. -->

				<!-- Build Coverage Records -->
				<!-- Assign all State level coverages to each vehicle in that rate-state -->
				<xsl:variable name="VehicleNbr" select="ItemIdInfo/InsurerId"/>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd!='']">
<!--Issue 66352 begin -->
					<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd = 'UN']">
						<xsl:choose>
							<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUNInd = 'Y'">
<!--Issue 66352 end -->
					<xsl:call-template name="BuildCoverage">
						<!-- ICH00020. eCase 35429. -->
						<xsl:with-param name="InsLine">CA</xsl:with-param>
						<!-- ICH00020. eCase 35429. -->
						<xsl:with-param name="RateState" select="$RateState"/>
						<xsl:with-param name="UnitNbr" select="$VehicleNbr"></xsl:with-param>
						<!-- ICH00020. eCase 35429. -->
					</xsl:call-template>
					<!-- UNI00479 starts-->
					<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd = 'UN']">
						<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage/Limit[LimitAppliesToCd = 'UNDPD']/FormatCurrencyAmt/Amt != ''">
							<xsl:variable name="CovCode">UNPD</xsl:variable>
							<xsl:choose>
<!-- UNI1B020							<xsl:when test="$PrimaryState = 'WA'"> -->
<!-- UNI1B020 - START -->
							<xsl:when test="$RateState = 'WA'">
<!-- UNI1B020 - END -->
							<xsl:call-template name="BuildCoverage">
								<xsl:with-param name="InsLine">CA</xsl:with-param>
								<xsl:with-param name="RateState" select="$RateState"/>
								<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
								<xsl:with-param name="UNPDInd">Y</xsl:with-param>
							</xsl:call-template>
							</xsl:when>
							</xsl:choose>
						</xsl:if>
					</xsl:if>
					<!-- UNI00479 Ends -->									
<!--Issue 66352 begin -->
							</xsl:when>
						</xsl:choose>
					</xsl:if>
<!--Issue 66352 end -->
				</xsl:for-each>


				<!-- Vehicle Level Coverages -->
				<xsl:for-each select="CommlCoverage[CoverageCd != '']">
					<xsl:variable name="CovCd" select="./CoverageCd"/>
					<xsl:if test="$CovCd != 'CMP'">
						<xsl:if test="$CovCd != 'COL'">
							<!-- ICH00020. eCase 35429. -->
							<xsl:if test="CoverageCd !='AMU'">
								<!-- ICH00020. eCase 35429. -->
								<!-- UNI00016 - Start - 10-18-2004 -->
								<!-- ICH00020. eCase 35429. -->
								<xsl:if test="$CovCd != 'ACM'">
									<!-- UNI00016 Auto Loan Comp -->
									<xsl:if test="$CovCd != 'ACL'">
										<!-- UNI00016 Auto Loan Coll -->
										<!-- UNI00016 - End - 10-18-2004 -->
								<!-- UNI00016 - Start - 10-25-2004 -->
								<xsl:if test="$CovCd != 'LCM'">
									<!-- UNI00016 Loan Lease Gap Comp -->
									<xsl:if test="$CovCd != 'LCO'">
										<!-- UNI00016 Loan Lease Gap Coll -->
										<!-- UNI00016 - End - 10-25-2004 -->
										<!-- ICH00020. eCase 35429. -->
										<xsl:call-template name="BuildCoverage">
											<!-- ICH00020. eCase 35429. -->
											<xsl:with-param name="InsLine">CA</xsl:with-param>
											<!-- ICH00020. eCase 35429. -->
											<xsl:with-param name="RateState" select="$RateState"/>
											<!-- ICH00020. eCase 35429. -->
											<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"></xsl:with-param>
											<!-- ICH00020. eCase 35429. -->
											<!-- UNI00365 - START -->
											<xsl:with-param name="IterationNbr" select="IterationNumber"></xsl:with-param>
											<!-- UNI00365 - END -->
										</xsl:call-template>
										<!-- ICH00020. eCase 35429. -->
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<!-- ICH00020. eCase 35429. -->
							<!-- UNI00016 - Start - 10-18-2004 -->
							<!-- ICH00020. eCase 35429. -->
						</xsl:if>
						<!-- UNI00016 Auto Loan Comp -->
					</xsl:if>
					<!-- UNI00016 Auto Loan Coll -->
					<!-- UNI00016 - End - 10-18-2004 -->
							<!-- UNI00016 - Start - 10-25-2004 -->
						</xsl:if>
						<!-- UNI00016 Loan Lease Gap Comp -->
					</xsl:if>
					<!-- UNI00016 Loan Lease Gap Coll -->
					<!-- UNI00016 - End - 10-25-2004 -->
					<!-- ICH00020. eCase 35429. -->
				</xsl:for-each>
				<!-- Begin UNI00016 - All ins line covs are generated by Point.  No ASBY recs should be sent here. -->
				<!-- Policy Level Coverages BK starts-->
				<!--xsl:if test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage/Option[OptionCd = 'com.unitrin_ApplyToAllVehicles']/OptionValue = 'Y'">           
			 <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlRateState/CommlVeh"> 
				<xsl:variable name="UnitNumber" select="substring-after(@id,'v')"/> 
			 <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage[CoverageCd != 'CAEXP']">																								
                <xsl:call-template name="BuildCoverage">																							
                   <xsl:with-param name="InsLine">CA</xsl:with-param>																				
                   <xsl:with-param name="RateState" select="$RateState"/>																			
					 <xsl:with-param name="UnitNbr"><xsl:value-of select="$UnitNumber"/></xsl:with-param>											
                </xsl:call-template>																												
			</xsl:for-each>
			</xsl:for-each>	
			</xsl:if
			<xsl:if test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage/Option[OptionCd = 'com.unitrin_ApplyToAllVehicles']/OptionValue = 'N'">           
			 <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage">																								
                <xsl:call-template name="BuildCoverage">																							
                   <xsl:with-param name="InsLine">CA</xsl:with-param>																				
                   <xsl:with-param name="RateState" select="$RateState"/>																			
					 <xsl:with-param name="UnitNbr">0</xsl:with-param>																		
                </xsl:call-template>																												
			</xsl:for-each>	
			</xsl:if> -->
				<!-- Policy Level Coverage BK Ends -->
				<!-- End UNI00016 -->
				<!-- UNI00445 starts-->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']">
<!--Issue 66352 begin -->
					<xsl:if test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd = 'UMPD']/FormatCurrencyAmt/Amt != '' and /ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUMInd = 'Y'">
<!--Issue 66352 end -->
<!--UNI00443				<xsl:if test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage/Limit/LimitAppliesToCd = 'UMPD'"> -->
<!--UNI00443 - START -->
<!--Issue 66352 begin			<xsl:if test="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd = 'UMPD']/FormatCurrencyAmt/Amt != ''"> end Issue 66352-->
<!--UNI00443 - END -->
					<xsl:variable name="CovCode" select="CoverageCd"/>
					<xsl:choose>
<!-- UNI00549						<xsl:when test="$PrimaryState = 'AR'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'AR' and $RateState = 'AR'">
<!-- UNI00549 - END -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- UNI00549						<xsl:when test="$PrimaryState = 'NM'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'NM' and $RateState = 'NM'">
<!-- UNI00549 - END -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<!-- UNI1B009 - START -->
<!-- UNI00549						<xsl:when test="$PrimaryState = 'IL'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'IL' and $RateState = 'IL'">
<!-- UNI00549 - END -->
<!-- UNI00918 - Start -->
						<!--WAG000019-UNI01111 -UNI00951 added class code to the QuoteRq but not the AddRq -<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable> -->
						<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502:21102</xsl:variable><!--WAG000019-UNI01111-->
<!-- UNI01038						<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/> -->
<!-- UNI01038 - START -->
						<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
<!-- UNI01038 - END -->
						<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
<!-- UNI00918 - End -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:if>				<!-- UNI00918 -->
						</xsl:when>
<!-- UNI00549						<xsl:when test="$PrimaryState = 'LA'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'LA' and $RateState = 'LA'">
<!-- UNI00549 - END -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- UNI00549						<xsl:when test="$PrimaryState = 'TX'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'TX' and $RateState = 'TX'">
<!-- UNI00549 - END -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- UNI00549						<xsl:when test="$PrimaryState = 'UT'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'UT' and $RateState = 'UT'">
<!-- UNI00549 - END -->
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<!-- UNI1B009 - END -->
						<!-- UNI00409 START -->
<!-- UNI00549						<xsl:when test="$PrimaryState = 'OR'"> -->
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'OR' and $RateState = 'OR'">
<!-- UNI00549 - END -->
						<!-- UNI00951 starts - Added another class code -->
						<!--<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable>-->
<!-- UNI01038						<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502:21102</xsl:variable> -->
<!-- UNI01038 - START -->
						<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable>
<!-- UNI01038 - END -->
						<!-- UNI00951 ends -->
								<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
								<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
								<!-- UNI00409 END -->
									<xsl:call-template name="BuildCoverage">
										<xsl:with-param name="InsLine">CA</xsl:with-param>
										<xsl:with-param name="RateState" select="$RateState"/>
										<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
									</xsl:call-template>
								</xsl:if>				<!-- UNI00409 -->
						</xsl:when>
<!-- UNI00549 - START -->
						<xsl:when test="$PrimaryState = 'CA' and $RateState = 'CA'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'IN' and $RateState = 'IN'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'GA' and $RateState = 'GA'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- UNI00549 - END -->
<!-- WAG000019-UNI01111 - START -->
						<xsl:when test="$PrimaryState = 'MS' and $RateState = 'MS'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'OH' and $RateState = 'OH'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'TN' and $RateState = 'TN'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- UNI01111 - END -->
<!--Issue 66352B begin -->
									<xsl:when test="($PrimaryState != 'AR' and $RateState = 'AR')
													or ($PrimaryState != 'CA' and $RateState = 'CA')
													or ($PrimaryState != 'GA' and $RateState = 'GA')
													or ($PrimaryState != 'IN' and $RateState = 'IN')
													or ($PrimaryState != 'LA' and $RateState = 'LA')
													or ($PrimaryState != 'MS' and $RateState = 'MS')
													or ($PrimaryState != 'NM' and $RateState = 'NM')
													or ($PrimaryState != 'OH' and $RateState = 'OH')
													or ($PrimaryState != 'TN' and $RateState = 'TN')
													or ($PrimaryState != 'TX' and $RateState = 'TX')
													or ($PrimaryState != 'UT' and $RateState = 'UT')">
										<xsl:call-template name="BuildCoverage">
											<xsl:with-param name="InsLine">CA</xsl:with-param>
											<xsl:with-param name="RateState" select="$RateState"/>
											<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
										</xsl:call-template>
									</xsl:when>
									
									<xsl:when test="$PrimaryState != 'IL' and $RateState = 'IL'">
										<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502:21102</xsl:variable>
										<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
										<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
											<xsl:call-template name="BuildCoverage">
												<xsl:with-param name="InsLine">CA</xsl:with-param>
												<xsl:with-param name="RateState" select="$RateState"/>
												<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:when>									
									
									<xsl:when test="$PrimaryState != 'OR' and $RateState = 'OR'">
										<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable>
										<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
										<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
											<xsl:call-template name="BuildCoverage">
												<xsl:with-param name="InsLine">CA</xsl:with-param>
												<xsl:with-param name="RateState" select="$RateState"/>
												<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:when>
<!--Issue 66352B end -->
					</xsl:choose>
				</xsl:if>
				</xsl:for-each>
				<!-- UNI00445 Ends -->
				<!-- Hired Auto Level Coverages BK starts-->
<!-- UNI00882				<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlRateState/CommlAutoHiredInfo/CommlCoverage[CoverageCd != '']"> -->
					<!-- ICH00020. eCase 35429. -->
<!-- UNI00882					<xsl:variable name="CovCode" select="CoverageCd"/> -->
<!-- UNI00882					<xsl:if test="$CovCode != 'HCM' and $CovCode != 'HCO'"> -->
<!-- UNI00882						<xsl:call-template name="BuildCoverage"> -->
							<!-- ICH00020. eCase 35429. -->
<!-- UNI00882							<xsl:with-param name="InsLine">CA</xsl:with-param> -->
							<!-- ICH00020. eCase 35429. -->
<!-- UNI00882							<xsl:with-param name="RateState" select="$RateState"/> -->
							<!-- ICH00020. eCase 35429. -->
<!-- UNI00882							<xsl:with-param name="UnitNbr">0</xsl:with-param> -->
							<!-- ICH00020. eCase 35429. -->
<!-- UNI00882						</xsl:call-template> -->
<!-- UNI00882					</xsl:if> -->
					<!-- ICH00020. eCase 35429. -->
<!-- UNI00882				</xsl:for-each> -->
				<!-- Hired Auto Level Coverage BK Ends -->

				<!-- CommlAutoDriveOtherCar Level Coverages BK starts-->
				<!--UNI00618 - START - stop all DOC coverages from building a ASBY record -->
				<!--UNI00618 <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd != '']"> -->
					<!-- ICH00020. eCase 35429. -->
					<!--UNI00618 <xsl:if test="CoverageCd != 'MED'">
						<xsl:call-template name="BuildCoverage"> -->
							<!-- ICH00020. eCase 35429. -->
							<!--UNI00618 <xsl:with-param name="InsLine">CA</xsl:with-param> -->
							<!-- ICH00020. eCase 35429. -->
							<!--UNI00618 <xsl:with-param name="RateState" select="$RateState"/> -->
							<!-- ICH00020. eCase 35429. -->
							<!--UNI00618 <xsl:with-param name="UnitNbr">0</xsl:with-param> -->
							<!-- ICH00020. eCase 35429. -->
						<!--UNI00618 </xsl:call-template> -->
					<!--UNI00618 </xsl:if> -->
					<!-- ICH00020. eCase 35429. -->
				<!--UNI00618 </xsl:for-each> -->
				<!-- CommlAutoDriveOtherCar Level Coverage BK Ends -->
				<!--UNI00618 - END - stop all DOC coverages from building a ASBY record -->
<!--WAG000019-UNI01111 start -->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd != '']"> 
					<xsl:if test="CoverageCd = 'DUP'">
						<xsl:call-template name="BuildCoverage"> 
							<xsl:with-param name="InsLine">CA</xsl:with-param> 
							<xsl:with-param name="RateState" select="$RateState"/> 
							<xsl:with-param name="UnitNbr">0</xsl:with-param> 
						</xsl:call-template> 
					</xsl:if> 
				</xsl:for-each> 
<!--WAG000019-UNI01111 end -->

				<!-- ICH00285 - AB : START -->
				<!-- Broadened PIP - Named Individuals -->
				<xsl:for-each select="AdditionalInterest[AdditionalInterestInfo/NatureInterestCd='BP']">
					<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
						<xsl:call-template name="CreateNIInfo">
							<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:for-each>
				<!-- ICH00285 - AB : END -->
				<!-- ICH00020. eCase 35429. -->
			</xsl:for-each>
			<!-- ICH00020. eCase 35429. -->
			<!-- UNI00948 - Start -->
			</xsl:if>
			<!-- UNI00948 - End -->
		</xsl:for-each>
		<!--RateState-->
		<!-- ICH00300 - AB : end   -->
		<!-- ICH00020. eCase 35429. -->
		<!-- ICH00020. eCase 35429. -->
		<!-- Build Coverage Symbols Record -->
		<!-- ICH00020. eCase 35429. -->
		<!--<xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyAddRq/CommlAutoLineBusiness">-->
		<!-- ICH00020. eCase 35429. -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyAddRq/CommlAutoLineBusiness">
			<!-- ICH00020. eCase 35429. -->
			<xsl:call-template name="CoveredAutoSymbol"/>
			<!-- ICH00020. eCase 35429. -->
		</xsl:for-each>
		<!-- ICH00020. eCase 35429. -->
		<!-- End: Cloned from "CommlAutoPolicyAddRq.xsl", changing XPATHs where needed-->
		<!-- ICH00020. eCase 35429. -->
	</xsl:template>
	<!-- UNI00016 End : Added CommlAuto Section to Add Rq-->
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->