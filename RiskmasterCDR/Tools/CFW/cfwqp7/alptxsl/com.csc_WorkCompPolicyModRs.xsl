<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform POINT XML into ACORD XML to return 
back to iSolutions   
E-Service case 31594 
***********************************************************************************************
-->

  <xsl:include href="CommonFuncRs.xsl"/>
  <xsl:include href="PTSignOnRs.xsl"/>
  <xsl:include href="PTErrorRs.xsl"/>
  <xsl:include href="PTProducerRs.xsl"/>
  <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
  <xsl:include href="PTCommlPolicyRs.xsl"/>
  <xsl:include href="PTWCLocationRs.xsl"/>
  <xsl:include href="PTAdditionalInterestRs.xsl"/>
  <xsl:include href="PTWorkCompRateStateRs.xsl"/>
  <xsl:include href="PTWorkCompLocInfoRs.xsl"/>
  <xsl:include href="PTCommlCoverageRs.xsl"/>
  <xsl:include href="PTFormsRs.xsl"/>


  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<ACORD>
		 <xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<com.csc_WorkCompPolicyModRs>
				<RqUID/>
          		<TransactionResponseDt/>
          		<TransactionEffectiveDt/>
          		<CurCd>USD</CurCd>
				  	<xsl:call-template name="PointErrorsRs"/>
			        <!-- 29653 Start -->
         		 	<!--  <xsl:apply-templates select="//ISLPPP1__RECORD" mode="WC"/>
          			<xsl:apply-templates select="//ISLPPP1__RECORD" mode="WClnsured"/>
          			<xsl:apply-templates select="//ISLPPP1__RECORD" mode="WCPolicy"/> -->
	  	  			<xsl:apply-templates select="//PMSP0200__RECORD" mode="WC"/>
          			<xsl:apply-templates select="//PMSP0200__RECORD" mode="WClnsured"/>
          			<xsl:apply-templates select="//PMSP0200__RECORD" mode="WCPolicy"/>
	  	  			<!-- 29653 End   -->
          			<!-- 29653 Start -->
                    <!-- <xsl:for-each select="//ISLPPP3__RECORD"> -->
                    <xsl:for-each select="//PMSPWC04__RECORD">
                    <!-- 29653 End   -->
            			<xsl:call-template name="CreateLocation"/>
          			</xsl:for-each>
          		
				<WorkCompLineBusiness>
            		<LOBCd>WC</LOBCd>
            		<WorkCompAssignedRisk>
              			<ApplicantsStatementDesc/>
              			<NumInsurersDeclined/>
            		</WorkCompAssignedRisk>
            	    <!-- 29653 Start -->           
	        		<!-- 	<xsl:for-each select="/*/ISLPPP4__RECORD"> -->
	        		<xsl:for-each select="/com.csc_WorkCompPolicyModRs/PMSPSA15__RECORD">
	        			<!--<xsl:sort select="STATE"/> -->
                              <xsl:sort select="UNITNO"/>
                              <!-- 29653 End   -->
              				<xsl:variable name="State" select="STATE"/>
              			 <!-- 29653 Start -->
              			<!-- <xsl:if test="not(preceding-sibling::ISLPPP4__RECORD[STATE=$State])"> -->
              			<xsl:if test="not(preceding-sibling::PMSPSA15__RECORD[STATE=$State])">
              			<!-- 29653 End   -->
                		<WorkCompRateState>
                  			<StateProvCd><xsl:value-of select="$State"/></StateProvCd>
                  			<AnniversaryRatingDt/>
                  			<NCCIIDNumber/>
                  			<!-- 29653 Start -->
                  			<!--  <xsl:for-each select="//ISLPPP5__RECORD[STATE=$State]"> -->
                  			<xsl:for-each select="//PMSPWC25__RECORD[STATE=$State]">
                  			<!-- 29653 End  -->
                    		<CreditOrSurcharge>
                     	 		<CreditSurchargeCd>	
                     	 			<!-- 29653 Start -->
                        			<!--<xsl:value-of select="MODIDENT"/> -->
                        			<xsl:value-of select="MODTYPE"/>
                        			<!-- 29653 End   -->
                        		</CreditSurchargeCd>
                     	 		<!-- 29653 Start -->
                       			<NumericValue>
                        			<FormatCurrencyAmt>
                          				<Amt><xsl:value-of select="FLATAMT"/></Amt>
                        			</FormatCurrencyAmt>
                      			</NumericValue>
                    		</CreditOrSurcharge>
                  			</xsl:for-each>
                  		    <!-- 29653 Start -->
		          <!--- <xsl:for-each select="/WorkCompPolicyQuoteInqRs/ISLPPP4__RECORD[STATE=$State]"> -->
                  <!--  <xsl:call-template name="CreateWorkCompLocInfo"/> -->
                  <!-- </xsl:for-each> -->
                  <!-- 29653 End   -->
                                   
                  <!-- 29653 Start -->
                  
				                    
                  <xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMSPSA15__RECORD[STATE=$State]">
              	     <xsl:variable name="UnitNo" select="UNITNO"/>
                     <xsl:variable name="CovSeq" select="COVSEQ"/>
                     <xsl:variable name="TransSeq" select="TRANSSEQ"/>
					 <xsl:variable name="Classnum" select="CLASSNUM"/>
 	     		     <xsl:variable name="Bureau" select="/com.csc_WorkCompPolicyModRs/PMSPSA35__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/BUREAU34"/>
			         <xsl:variable name="Spec50" select="/com.csc_WorkCompPolicyModRs/PMSPSA36__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/SPEC50"/>  
		                   
			  	     <xsl:choose>
                         <xsl:when  test="$Classnum='XXXA'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
								<xsl:with-param name="Bureau" select="$Bureau"/>	<!--62825 -->
				            </xsl:call-template>	
			             </xsl:when>
			
			             <xsl:when  test="$Classnum='XXXM'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				            </xsl:call-template>	
			    		</xsl:when>

			            <xsl:when  test="$Classnum='XXXS'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:when  test="$Classnum='XXXP'">
			               <xsl:variable name="Rate">000.000</xsl:variable>  
                           <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:otherwise>
				           <xsl:variable name="Rate"> 
						   <xsl:call-template name="CreateWCRate"> 
		                       <xsl:with-param name="Bureau" select="$Bureau"/>
		                       <xsl:with-param name="Spec50" select="$Spec50"/>
        		           </xsl:call-template>
                           </xsl:variable> 
						                           
				           <xsl:call-template name="CreateWorkCompLocInfo">
				              <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>					   
					</xsl:otherwise>
			</xsl:choose>
						   
						   
						   
				  </xsl:for-each>
                  <!-- 29653 End -->
                  
                		</WorkCompRateState>
              			</xsl:if>
           	 		</xsl:for-each>
            	 	<!-- 29653 Start -->
	        		<!--<xsl:apply-templates select="//ISLPPP1__RECORD" mode="WCCoverage"/> -->
            		<xsl:apply-templates select="//PMSPWC02__RECORD" mode="WCCoverage"/>
 	        		<!-- 29653 End   -->
          		</WorkCompLineBusiness>
          		<RemarkText IdRef="policy"></RemarkText>
				</com.csc_WorkCompPolicyModRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->