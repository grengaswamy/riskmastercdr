<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreateAccidentViolation">
            <AccidentViolation>
				<xsl:attribute name="id"><xsl:value-of select="concat('s',BMEYNB)"/></xsl:attribute>  <!--106215-->
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d',BMA0NB)"/></xsl:attribute>
               <AccidentViolationCd>
                    <xsl:value-of select="BMBKTX"/> 
               </AccidentViolationCd>
               <AccidentViolationDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="BMAIDT"/>
                    </xsl:call-template>
               </AccidentViolationDt>
               <AccidentViolationDesc/>
            </AccidentViolation>	
	</xsl:template>
</xsl:stylesheet>

