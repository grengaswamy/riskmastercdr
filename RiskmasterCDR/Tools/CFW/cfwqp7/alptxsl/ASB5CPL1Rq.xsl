<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name="BuildUnitRecord">
    <xsl:param name="Location" />
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="RateState" />
    <xsl:param name="InsLine" />
    <xsl:param name="Product" />
    <xsl:param name="Node" />
	<xsl:param name="UnitNbr"/>	<!-- Issue 59878 -->
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ASB5CPL1</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ASB5CPL1__RECORD>
        <B5AACD>
          <xsl:value-of select="$LOC" />
        </B5AACD>
        <B5ABCD>
          <xsl:value-of select="$MCO" />
        </B5ABCD>
        <B5ARTX>
          <xsl:value-of select="$SYM" />
        </B5ARTX>
        <B5ASTX>
          <xsl:value-of select="$POL" />
        </B5ASTX>
        <B5ADNB>
          <xsl:value-of select="$MOD" />
        </B5ADNB>
        <B5AGTX>
          <xsl:value-of select="$InsLine" />
        </B5AGTX>
        <B5BRNB>
          <xsl:value-of select="$Location" />
        </B5BRNB>
		<B5EGCD>
			<!--Issue 59878 starts-->
			<!--<xsl:when test="$RateState = 'IN'">0</xsl:when>-->
			<!--xsl:choose>
				<xsl:when test="$RateState = 'IN' " >
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']/Deductible[DeductibleTypeCd='UMDed']/FormatCurrencyAmt/Amt" />
				</xsl:when>
			</xsl:choose-->
			<!--Issue 59878 ends-->
		</B5EGCD>
		<!--Issue 59878 UM deductible change starts-->
		<B5EJCD>
			<xsl:choose>
				<xsl:when test="$RateState = 'GA' " >
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd='GA']/CommlVeh/CommlCoverage[CoverageCd = 'UM ']/Deductible/FormatCurrencyAmt/Amt" />
				</xsl:when>
				<xsl:when test="$RateState = 'IN' " >
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd='IN']/CommlVeh/CommlCoverage[CoverageCd = 'UM ']/Deductible/FormatCurrencyAmt/Amt" />
				</xsl:when>
			</xsl:choose>
		</B5EJCD>
		<!--Issue 59878  UM deductible change ends-->
        <B5EGNB>
          <xsl:value-of select="$SubLocation" />
        </B5EGNB>
        <B5ANTX>
          <xsl:value-of select="$InsLine" />
        </B5ANTX>
        <B5AENB>
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/ItemIdInfo/InsurerId" />
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </B5AENB>
        <B5C6ST>P</B5C6ST>
        <B5AFNB>0</B5AFNB>
        <B5TYPE0ACT>
        	<!-- 34771 start -->
			<!--103409 Starts-->
	<!--xsl:choose-->
		<!--<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 -->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>			
		</xsl:call-template>
	<!--/xsl:choose-->
			<!--103409 Ends-->
	<!-- 34771 end -->
		</B5TYPE0ACT>
		<!--Issue 59878 - Start-->
		<xsl:if test="$InsLine = 'CA'">
			<B5BATX>AV</B5BATX>
		</xsl:if>
		<!-- 59878 ends -->
		<B5IYTX>
			<xsl:choose>
			<xsl:when test="$InsLine='CR'">
				<xsl:value-of select="AlarmAndSecurity/BulletResistingEnclosureTypeCd"/>
			</xsl:when>
			</xsl:choose>
		</B5IYTX>
		<B5C0ST>
			<xsl:choose>
			<xsl:when test="$InsLine='CR'">
				<xsl:value-of select="AlarmAndSecurity/LocalGongInd"/>
			</xsl:when>
			</xsl:choose>
		</B5C0ST>
		<!-- 59878 ends --> 
        <B5PTTX>
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/CommlVehSupplement/PrimaryClassCd" />
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/ClassCd" />
            </xsl:when>
			<!-- 59878 starts--> 
			<xsl:when test="$InsLine='CR'">
				<xsl:value-of select="$Node/ClassCd"/>
			</xsl:when>
			<xsl:when test="$InsLine='CF'">						
				<xsl:value-of select="concat(com.csc_SubLocationInfo/com.csc_ClassCdCF, '')"/>
			</xsl:when>
			<xsl:when test="$InsLine='IMC'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdIM"/>
			</xsl:when>
			<!-- 59878 ends --> 
            <xsl:otherwise>
              <xsl:value-of select="substring-before(com.csc_SubLocationInfo/com.csc_ClassCd,'-')" />
            </xsl:otherwise>
          </xsl:choose>
        </B5PTTX>
        <B5KKTX>
          <xsl:choose>
		  	<!-- Issue 59878 - Start -->
            <!--xsl:when test="$InsLine='CF'">
              <xsl:call-template name="FormatData">
                <xsl:with-param name="FieldName">com.csc_OccupancyClassSymbolCd</xsl:with-param>
                <xsl:with-param name="FieldLength">6</xsl:with-param>
                <xsl:with-param name="Value" select="com.csc_SubLocationInfo/com.csc_OccupancyClassSymbolCd" />
                <xsl:with-param name="FieldType">A</xsl:with-param>
              </xsl:call-template>
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_NonHabitationalOccupancySymbolCd" />
              <xsl:value-of select="com.csc_SubLocationInfo/TaxCodeInfo[TaxTypeCd='com.csc_COUNTY']/TaxCd" />
            </xsl:when-->
			<!-- Issue 59878 - End -->
            <xsl:when test="$InsLine='CA'">
			<!-- Issue 59878 - Start -->
              <!--xsl:value-of select="$Node/CommlVehSupplement/NearZoneCd" />
              <xsl:value-of select="$Node/CommlVehSupplement/FarZoneCd" /-->
			  <xsl:value-of select="$Node/CommlVehSupplement/com.csc_PrimaryClassCdDesc"/>
			<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/com.csc_ClassCdDesc" />
            </xsl:when>
			<!-- 59878 starts -->
			<xsl:when test="$InsLine='CR'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdDescCR"/>
			</xsl:when>
			<xsl:when test="$InsLine='CF'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdDescCF"/>
			</xsl:when>
			<xsl:when test="$InsLine='IMC'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdDescIM"/>
			</xsl:when>
			<!-- 59878 ends -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </B5KKTX>
        <xsl:variable name="B5KWTX">
          <xsl:choose>
			  <!-- Issue 59878 - Start -->
            <!--xsl:when test="$InsLine='IMC' or $InsLine='CR'">
              <xsl:value-of select="$Node/PMACd"></xsl:value-of>
            </xsl:when-->
			<xsl:when test="$InsLine='CR'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_PMACdCR"/>
			</xsl:when>
			<xsl:when test="$InsLine='CF'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_PMACdCF"/>
			</xsl:when>
			<xsl:when test="$InsLine='IMC'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_PMACdIM"/>
			</xsl:when>
            <!--xsl:when test="$InsLine='CF'">
              <xsl:value-of select="$Node/com.csc_PMACd"></xsl:value-of>
            </xsl:when-->
			<!-- Issue 59878 - End -->
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="BldgOccupancy/NatureBusinessCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:value-of select="../../PMACd"></xsl:value-of>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5KWTX>
          <xsl:value-of select="$B5KWTX" />
        </B5KWTX>
        <B5BCCD>
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/Addr/StateProvCd" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$RateState" />
            </xsl:otherwise>
          </xsl:choose>
        </B5BCCD>
        <xsl:variable name="B5AGNB">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">999</xsl:when>
            <xsl:when test="$InsLine='CR' or $InsLine='CA'">
              <xsl:value-of select="$Node/TerritoryCd" />
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <!-- Issue 59878 - Start -->
			  <!--xsl:value-of select="$Node/com.csc_TerritoryCd" /-->
			  <xsl:call-template name="FormatData">
	   	      <xsl:with-param name="FieldName">$B5AGNB</xsl:with-param>      
			  <xsl:with-param name="FieldLength">3</xsl:with-param>
			  <xsl:with-param name="Value" select="com.csc_SubLocationInfo/com.csc_TerritoryCd"/>
			  <xsl:with-param name="FieldType">N</xsl:with-param>
			  </xsl:call-template>      
			  <!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_SubLocationInfo/TerritoryCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AGNB>
          <xsl:value-of select="$B5AGNB" />
        </B5AGNB>
        <xsl:variable name="B5BJCD">
          <xsl:choose>
		  	<!-- Issue 59878 - Start -->
            <!--xsl:when test="$InsLine='IMC' or $InsLine='CR' or $InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/TaxCodeInfo[TaxTypeCd='com.csc_CITY']/TaxCd"></xsl:value-of>
            </xsl:when-->
			<xsl:when test="$InsLine='IMC' or $InsLine='CR' or $InsLine='CF' or $InsLine='GL'">
				<xsl:choose>
					<xsl:when test="$RateState='AR'">
						<xsl:choose>
							<xsl:when test="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/TaxCd='N/A' or /ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='County']/TaxCd">0</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="FormatData">
   									<xsl:with-param name="FieldName">$B5BJCD</xsl:with-param>      
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='']/TaxCd"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template> 
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$RateState='ND'">
						<xsl:choose>
							<xsl:when test="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/TaxCd='N/A' or /ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='County']/TaxCd">0</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">$B5BJCD</xsl:with-param>      
									<xsl:with-param name="FieldLength">4</xsl:with-param>
									<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='']/TaxCd"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template> 
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/TaxCd='N/A' or /ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/TaxCd='0'">0</xsl:when>
							<xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='']/TaxCd"></xsl:value-of></xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>							    
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$InsLine='CA'">
				<xsl:choose>
					<xsl:when test="$RateState='AR'">
						<xsl:choose>
							<xsl:when test="$Node/TaxCodeInfo/TaxCd='N/A' or $Node/TaxCodeInfo[TaxTypeCd='County']/TaxCd">0</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="FormatData">
     								<xsl:with-param name="FieldName">$B5BJCD</xsl:with-param>      
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="$Node/TaxCodeInfo[TaxTypeCd='']/TaxCd"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template> 
							</xsl:otherwise>
						</xsl:choose>
						</xsl:when>
							<xsl:when test="$RateState='ND'">
								<xsl:choose>
									<xsl:when test="$Node/TaxCodeInfo/TaxCd='N/A' or $Node/TaxCodeInfo[TaxTypeCd='County']/TaxCd">0</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="FormatData">
		   	   								<xsl:with-param name="FieldName">$B5BJCD</xsl:with-param>      
											<xsl:with-param name="FieldLength">4</xsl:with-param>
											<xsl:with-param name="Value" select="$Node/TaxCodeInfo[TaxTypeCd='']/TaxCd"/>
											<xsl:with-param name="FieldType">N</xsl:with-param>
										</xsl:call-template> 
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="$Node/TaxCodeInfo/TaxCd='N/A'">0</xsl:when>
										<xsl:otherwise><xsl:value-of select="$Node/TaxCodeInfo[TaxTypeCd='']/TaxCd"></xsl:value-of></xsl:otherwise>
									</xsl:choose>
							</xsl:otherwise>							    
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - end -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5BJCD>
          <xsl:value-of select="$B5BJCD" />
        </B5BJCD>
        <xsl:variable name="B5BKCD">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC' or $InsLine='CR'">
              <xsl:value-of select="com.csc_SubLocationInfo/TaxCodeInfo[com.csc_COUNTY]/TaxCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="Construction/NumUnits"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5BKCD>
          <xsl:value-of select="$B5BKCD" />
        </B5BKCD>
		<!-- 59878 starts -->
		<B5EHCD>
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
				    <xsl:choose>						
						<xsl:when test="$RateState='KY'">
							<xsl:choose>
								<xsl:when test="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/CntyTaxCd='N/A'">0</xsl:when>
								<xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/CntyTaxCd"></xsl:value-of></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo/TaxCd='N/A'">0</xsl:when>
								<xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/TaxCodeInfo[TaxTypeCd='County']/TaxCd"></xsl:value-of></xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>							    		
				    </xsl:choose>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</B5EHCD>
		<!-- 59878 ends-->
        <xsl:variable name="B5H1TX">
          <xsl:choose>
		  	<!-- Issue 59878 - Start -->
            <!--xsl:when test="$InsLine='IMC' or $InsLine='CF' or $InsLine='BOP'"-->
			<xsl:when test="$InsLine='IMC' or $InsLine='BOP'">
			<!-- Issue 59878 - End -->
              <xsl:value-of select="BldgProtection/FireProtectionClassCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CF'">
		    <!-- only pad when size is one character -->	
			    <xsl:choose>
					<xsl:when test="string-length(BldgProtection/FireProtectionClassCd)=1">
						<xsl:call-template name="FormatData">
	  	     					<xsl:with-param name="FieldName">$B5H1TX</xsl:with-param>      
								<xsl:with-param name="FieldLength">2</xsl:with-param>
								<xsl:with-param name="Value" select="BldgProtection/FireProtectionClassCd"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
						</xsl:call-template> 
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat(BldgProtection/FireProtectionClassCd,'')"/>
					</xsl:otherwise>
			    </xsl:choose>
			</xsl:when>     
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5H1TX>
          <xsl:value-of select="$B5H1TX" />
        </B5H1TX>
        <xsl:variable name="B5HYTX">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC' or $InsLine='CF'">
              <xsl:value-of select="Construction/ConstructionCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5HYTX>
          <xsl:value-of select="$B5HYTX" />
        </B5HYTX>
        <xsl:variable name="B5O6TX">
          <xsl:choose>
		  	<!-- Issue 59878 Start -->
			<xsl:when test="$InsLine='IMC' or $InsLine='CF'">
			<!-- Issue 59878 End -->
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_RatingPlanCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5O6TX>
          <xsl:value-of select="$B5O6TX" />
        </B5O6TX>
        <xsl:variable name="B5PQTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF' or $InsLine='BOP'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassRateGroupCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:value-of select="substring($Node/CommlVehSupplement/com.csc_PostalCode,1,5)">
				</xsl:value-of>
			</xsl:when>
		<xsl:when test="$InsLine='GL'">
			<xsl:value-of select="substring(../Location[substring(@id,2,string-length(@id)-1)=$Location]/Addr/PostalCode,1,5)">
			</xsl:value-of>
		</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5PQTX>
          <xsl:value-of select="$B5PQTX" />
        </B5PQTX>
        <xsl:variable name="B5PRTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">
              <!-- Issue 59878 - Start -->
			  <!--xsl:choose>
                <xsl:when test="$RateState='IN'">CSL</xsl:when>
                <xsl:when test="$RateState='CO'">SL</xsl:when>
                <xsl:otherwise>N</xsl:otherwise>
              </xsl:choose-->
              <xsl:variable name="Val" select="$Node/CommlVehSupplement/com.csc_VehicleUMType"/>
			  <xsl:if test="normalize-space($Val)=''">N</xsl:if>
			  <xsl:if test="normalize-space($Val)!=''"><xsl:value-of select="$Val"/></xsl:if>
			  <!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5PRTX>
          <xsl:value-of select="$B5PRTX" />
        </B5PRTX>
        <xsl:variable name="B5FJNB">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="Construction/BldgCodeEffectivenessGradeCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">0</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5FJNB>
          <xsl:value-of select="$B5FJNB" />
        </B5FJNB>
        <xsl:variable name="B5FKNB">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">0</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5FKNB>
          <xsl:value-of select="$B5FKNB" />
        </B5FKNB>
        <xsl:variable name="B5FLNB">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">
              <xsl:value-of select="Construction/NumUnits"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ISODistrictNumber"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">0</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5FLNB>
          <xsl:value-of select="$B5FLNB" />
        </B5FLNB>
        <xsl:variable name="B5A0VA">
          <xsl:choose>
            <!-- Case 29405 Begin -->
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/com.csc_StatedAmt/Amt"></xsl:value-of>
            </xsl:when>
            <!-- Case 29405 End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5A0VA>
          <xsl:value-of select="$B5A0VA" />
        </B5A0VA>
        <xsl:variable name="B5B7VA">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">0</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5B7VA>
          <xsl:value-of select="$B5B7VA" />
        </B5B7VA>
        <xsl:variable name="B5AZVA">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">0</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AZVA>
          <xsl:value-of select="$B5AZVA" />
        </B5AZVA>
        <xsl:variable name="B5CZST">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/com.csc_OutsideStationInd"></xsl:value-of>	<!-- Issue 59878 -->
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_NewYorkFireFeeInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="InterestCd" />
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
			<!-- Issue 59878 Begin
              <xsl:variable name="Val" select="$Node/CommlCoverage[CoverageCd='UM']" />
              <xsl:if test="normalize-space($Val)=''">N</xsl:if>
              <xsl:if test="$Val!=''">Y</xsl:if> -->
					<xsl:variable name="VehClassCd">671:674:681:684:691:694:672:675:682:685:692:695:673:676:683:686:693:696:693:696</xsl:variable>
					 <xsl:choose>
						<xsl:when test="contains($VehClassCd, substring($Node/CommlVehSupplement/PrimaryClassCd,1,3))">N</xsl:when> 
						
					<!-- Issue 59878 End -->
            		
		            <xsl:otherwise>	<xsl:value-of select="$Node/CommlVehSupplement/com.csc_VehicleUMInd"/></xsl:otherwise>
				 </xsl:choose>

            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5CZST>
          <xsl:value-of select="$B5CZST" />
        </B5CZST>
        <xsl:variable name="B5C0ST">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/LocalGongInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ReinforcedMasonaryInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/Limit/ValuationCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5C0ST>
          <xsl:value-of select="$B5C0ST" />
        </B5C0ST>
        <xsl:variable name="B5C1ST">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/WithKeysInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_SprinklerLeakageCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="BldgProtection/ProtectionDeviceSprinklerCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
				<xsl:if test="@id = $UnitNbr"><xsl:choose><xsl:when test="CommlCoverage/CoverageCd = 'AMU'">Y</xsl:when><xsl:otherwise>N</xsl:otherwise></xsl:choose></xsl:if>	<!-- Issue 59878 -->
			</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5C1ST>
          <xsl:value-of select="$B5C1ST" />
        </B5C1ST>
        <xsl:variable name="B5C2ST">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/WatchPersonsFactorCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_PartialSprinklerInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='XCBUL']/Option/OptionValue"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5C2ST>
          <xsl:value-of select="$B5C2ST" />
        </B5C2ST>
        <xsl:variable name="B5IYTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/BulletResistingEnclosureTypeCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_HighRiseInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_WindHailDeductiblePct"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5IYTX>
          <xsl:value-of select="$B5IYTX" />
        </B5IYTX>
        <xsl:variable name="B5IZTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/AlarmDescCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_MoltenMaterialInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_SubLocationInfo/StormShuttersCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
              <xsl:variable name="Val" select="$Node/CommlCoverage[CoverageCd='TL']/Option/OptionValue" />
              <xsl:if test="normalize-space($Val)=''">N</xsl:if>
              <xsl:if test="$Val!=''">
                <xsl:value-of select="$Val" />
              </xsl:if>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5IZTX>
          <xsl:value-of select="$B5IZTX" />
        </B5IZTX>
        <xsl:variable name="B5I0TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/com.csc_OutsidePropertyProtectionCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<!-- For CF, set Heavy Timber Joist to 'N' -->
			<xsl:when test="$InsLine='CF'">N</xsl:when>
	        <!--xsl:when test="$InsLine='CA'">
              <xsl:choose>
                <xsl:when test="$Node/CommlCoverage[CoverageCd='LIA']">Y</xsl:when>
                <xsl:otherwise>N</xsl:otherwise>
              </xsl:choose>
            </xsl:when-->
			<xsl:when test="$InsLine='CA'">
				<xsl:variable name="VehClassCd">671:674:681:684:691:694:672:675:682:685:692:695:673:676:683:686:693:696:693:696</xsl:variable>
				<xsl:choose>
					<xsl:when test="$Node/../../CommlCoverage[CoverageCd='MP'] and $Node/../../CommlCoverage[CoverageCd = 'MP']/Option/OptionCd = 'com.csc_MedPayExcl' and $Node/../../CommlCoverage[CoverageCd = 'MP']/Option/OptionValue = 'Y' and contains($VehClassCd, substring($Node/CommlVehSupplement/PrimaryClassCd,1,3))">N</xsl:when> 
					<xsl:when test="contains($VehClassCd, substring($Node/CommlVehSupplement/PrimaryClassCd,1,3))">N</xsl:when> 
					<xsl:when test="$Node/CommlVehSupplement/com.csc_VehicleMPInd = 'Y'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I0TX>
          <xsl:value-of select="$B5I0TX" />
        </B5I0TX>
        <xsl:variable name="B5I1TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_LightSteelConstructionInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">N</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I1TX>
          <xsl:value-of select="$B5I1TX" />
        </B5I1TX>
        <xsl:variable name="B5I2TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_SusceptibilityCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="BldgProtection/ProtectionDeviceBurglarCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">N</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I2TX>
          <xsl:value-of select="$B5I2TX" />
        </B5I2TX>
        <xsl:variable name="B5I3TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_SprinklerLeakageInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node[SubjectInsuranceCd='BLDG']/com.csc_UtilitiesCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:variable name="Val" select="$Node/CommlVehSupplement/com.csc_FleetInd"/>
				<xsl:choose>
					<xsl:when test="$Val='False'">N</xsl:when>
					<xsl:when test="$Val='True'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
				</xsl:choose>	
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I3TX>
          <xsl:value-of select="$B5I3TX" />
        </B5I3TX>
        <xsl:variable name="B5I4TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_BuildingInConstructionInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/Option[OptionCd='com.csc_OPC']/OptionValue"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			 <xsl:when test="$InsLine='CA'">
			 <!--105713 Hired vehicle change starts-->
			 <!--xsl:variable name="Leased" select="$Node/LeasedVehInd"/-->
			  <xsl:choose>
			
			 <xsl:when test="$Node/LeasedVehInd = 'Y'">Y</xsl:when>
			 <xsl:when test="$Node/HiredVehInd = 'Y'">H</xsl:when>
			 <xsl:otherwise>N</xsl:otherwise>
			   </xsl:choose>
			 <!--xsl:value-of select="$Node/LeasedVehInd"/-->
			 	<!--105713 Hired vehicle change ends-->
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I4TX>
          <xsl:value-of select="$B5I4TX" />
        </B5I4TX>
        <xsl:variable name="B5I5TX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_FireLegalAddlInsuredInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/Option[OptionCd='com.csc_OPP']/OptionValue"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
            <!--xsl:when test="$InsLine='CA'">
              <xsl:choose>
                <xsl:when test="$Node/CommlCoverage[CoverageCd='LIA']">Y</xsl:when>
                <xsl:otherwise>N</xsl:otherwise>
              </xsl:choose>
            </xsl:when-->
			<xsl:when test="$InsLine='CA'">
				<xsl:variable name="VehClassCode">691:692:693:694:695:696</xsl:variable>
				<xsl:choose>
					<xsl:when test="$Node/../../CommlCoverage[CoverageCd='LIA'] and $Node/../../CommlCoverage[CoverageCd = 'LIA']/Option/OptionCd = 'com.csc_ApplyToAllVehicles' and $Node/../../CommlCoverage[CoverageCd = 'LIA']/Option/OptionValue = 'Y' and contains($VehClassCode, substring($Node/CommlVehSupplement/PrimaryClassCd,1,3))">N</xsl:when>
					<xsl:when test="contains($VehClassCode , substring($Node/CommlVehSupplement/PrimaryClassCd,1,3))">N</xsl:when> 
					<!--Issue 59878 Starts-->
					<!--xsl:when test="../../CommlCoverage[CoverageCd='LIA']">Y</xsl:when-->
					<!--xsl:otherwise>N</xsl:otherwise-->
				<xsl:when test="$Node/CommlVehSupplement/com.csc_VehicleLIAInd = 'Y'">Y</xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
					<!--Issue 59878 ends-->
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5I5TX>
          <xsl:value-of select="$B5I5TX" />
        </B5I5TX>
        <xsl:variable name="B5USIN13">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_NonRatedSprinklerInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/Option[OptionCd='com.csc_OHPT']/OptionValue"></xsl:value-of>
            </xsl:when>
			<xsl:when test="$InsLine = 'CA' and $Node/CommlVehSupplement/com.csc_VehiclePIPInd = 'Y'"><!-- Issue 59878 Begin -->
				<xsl:value-of select="CommlVehSupplement/com.csc_PIPRatingBasis"/>
			</xsl:when><!-- Issue 59878 End -->
            <xsl:when test="$InsLine='CA'">N</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN13>
          <xsl:value-of select="$B5USIN13" />
        </B5USIN13>
        <xsl:variable name="B5USIN14">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_CondoAssocCoverageInd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/Option[OptionCd='com.csc_OHCL']/OptionValue"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:variable name="Amt" select="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_PIPStacking']/OptionValue"/>
				<xsl:choose>
					<xsl:when test="$Amt != '0' or $Amt != ''"><xsl:value-of select="$Amt"/></xsl:when>
					<xsl:when test="$RateState='KS' or $RateState='LA' or $RateState='TX'">N</xsl:when>		<!-- UNI00969 -->
					<xsl:when test="$RateState='WA'">
            	       <xsl:value-of select="CommlVehSupplement/com.csc_LoggingCd"/>								
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN14>
          <xsl:value-of select="$B5USIN14" />
        </B5USIN14>
        <xsl:variable name="B5USIN15">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/Option[OptionCd='com.csc_OPW']/OptionValue"></xsl:value-of>
            </xsl:when>
			<!-- 59878 starts -->
			<xsl:when test="$InsLine='CF'">N</xsl:when>
			<xsl:when test="$InsLine='CA'">
				<xsl:value-of select="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_PIPStacking']/OptionValue"/>
			</xsl:when>
			<!-- 59878 ends -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN15>
          <xsl:value-of select="$B5USIN15" />
        </B5USIN15>
        <xsl:variable name="B5USIN16">
          <xsl:choose>
            <!-- 59878 starts -->
			<!--<xsl:when test="$InsLine='CA'">N</xsl:when>
            <xsl:otherwise></xsl:otherwise>-->			
            
			<xsl:when test="$InsLine = 'CA' and $Node/CommlVehSupplement/com.csc_VehiclePIPInd = 'Y' and ../CommlCoverage[CoverageCd = 'PIP']/Option/OptionCd = 'com.csc_PIPType'">
				<xsl:value-of select="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_PIPType']/OptionValue"/>
            </xsl:when>
			<xsl:when test="$InsLine = 'CA' and $Node/CommlVehSupplement/com.csc_VehiclePIPInd = 'Y' and ../CommlCoverage[CoverageCd = 'PIP']/Option/OptionCd = 'com.csc_ExclLossOfIncomeBenefits'">
				<xsl:if test="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_ExclLossOfIncomeBenefits']/OptionValue = 'Y'">E</xsl:if>
				<xsl:if test="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_ExclLossOfIncomeBenefits']/OptionValue = 'N'">I</xsl:if>
            </xsl:when>
			<xsl:when test="$InsLine = 'CA' and $Node/CommlVehSupplement/com.csc_VehiclePIPInd = 'Y' and ../CommlCoverage/CoverageCd = 'PIP'">Y</xsl:when>
			<xsl:when test="$InsLine = 'CA'">N</xsl:when>
                        
			<xsl:when test="$InsLine='CF'">N</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
			<!-- 59878 ends -->
          </xsl:choose>
        </xsl:variable>
        <B5USIN16>
          <xsl:value-of select="$B5USIN16" />
        </B5USIN16>
		<!-- 59878 starts-->
		<xsl:variable name="B5USIN17">
			<xsl:choose>
				<xsl:when test="$InsLine = 'CA' and ../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_WorkLossExcl']/OptionValue != '0'">
				<xsl:value-of select="../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd = 'com.csc_WorkLossExcl']/OptionValue"/>
				</xsl:when>
				<xsl:when test="$InsLine='CF'"> </xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN17>
			<xsl:value-of select="$B5USIN17"/>
		</B5USIN17>
		<xsl:variable name="B5USIN18">
			<xsl:choose>
				<xsl:when test="$InsLine='CA'">N</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN18>
			<xsl:choose>
				<xsl:when test="$InsLine = 'CA' and ../CommlCoverage[CoverageCd = 'PIP']/Option[OptionCd='com.csc_DeductibleType']">
					<xsl:value-of select="../CommlCoverage[CoverageCd='PIP']/Option/OptionValue"/>
				</xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
			</xsl:choose>
		</B5USIN18>
		<!-- 59878 ends -->
        <xsl:variable name="B5USIN19">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">N</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN19>
          <xsl:value-of select="$B5USIN19" />
        </B5USIN19>
        <xsl:variable name="B5USIN20">
          <xsl:choose>
            <!-- Issue 59878 - Start -->
			<!--xsl:when test="$InsLine='CF'">
              <xsl:choose>
                <xsl:when test="$RateState='RI'">N</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:when-->
			<xsl:when test="$InsLine='CF'">N</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/Deductible[DeductibleTypeCd='HURR']/FormatCurrencyAmt/Amt"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN20>
          <xsl:value-of select="$B5USIN20" />
        </B5USIN20>
		<!-- 59878 starts -->
		<xsl:variable name="B5USIN21">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ARated">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN21>
			<xsl:value-of select="$B5USIN21"/>
		</B5USIN21>
		<!-- 59878 ends -->
        <xsl:variable name="B5USIN22">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_StormShuttersCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN22>
          <xsl:value-of select="$B5USIN22" />
        </B5USIN22>
		<!-- 59878 starts -->
     	<!--   <B5USIN23>N</B5USIN23>-->
	 	<xsl:variable name="B5USIN23">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_RoofWindInd">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$InsLine!= 'GL' or $InsLine!= 'IM' or $InsLine!= 'CR' or $InsLine!= 'CA'">
						<B5USIN23>N</B5USIN23>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN23>
			<xsl:value-of select="$B5USIN23"/>
		</B5USIN23>
        <xsl:variable name="B5USIN24">
          <xsl:choose>
			<xsl:when test="$InsLine='IMC'"><xsl:value-of select="../com.csc_CommlInlandMarineLineBusiness/com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification[substring(@LocationRef,2,string-length(@LocationRef)-1)=$Location and substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)=$SubLocation]/CommlCoverage/Option[OptionCd = 'com.csc_ContentsGroupCd']/OptionValue"/></xsl:when>
			<xsl:when test="$InsLine='CF'">
				<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ContentsGroupCd">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$InsLine='CA'">
				<xsl:value-of select="CommlVehSupplement/com.csc_AccPrevCredit"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN24>
          <xsl:value-of select="$B5USIN24" />
        </B5USIN24>
		<xsl:variable name="B5USIN25">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'"> </xsl:when>
				<xsl:when test="$InsLine='CA'">
					<!--Issue 59878 start-->
					<xsl:choose>
						<xsl:when test="$RateState = 'NV'">
							<xsl:value-of select="CommlVehSupplement/com.csc_AccPrevCredit"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="CommlVehSupplement/com.csc_AntiTheftDisc"/>
						</xsl:otherwise>
					</xsl:choose>
					<!--Issue 59878 end-->
					<!--<xsl:value-of select="CommlVehSupplement/com.csc_AntiTheftDisc"/>--> <!--Issue 59878-->
				</xsl:when>						
				<xsl:otherwise>N</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN25>
			<xsl:value-of select="$B5USIN25"/>
		</B5USIN25>
		<!-- 59878 ends -->
        <xsl:variable name="B5USIN26">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_IncidentalApartmentInd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:choose>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='LCM']">Y</xsl:when>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='ACM']">Y</xsl:when>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='LOANCM']">Y</xsl:when>  <!--Issue 59878-->
					<xsl:otherwise>N</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN26>
          <xsl:value-of select="$B5USIN26" />
        </B5USIN26>
        <xsl:variable name="B5USIN27">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_ContentsRateGradeCd"></xsl:value-of>
            </xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="$InsLine='CA'">
				<xsl:choose>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='LCO']">Y</xsl:when>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='ACL']">Y</xsl:when>
					<xsl:when test="$Node/CommlCoverage[CoverageCd='LOANCO']">Y</xsl:when>		<!--Issue 59878-->
					<xsl:otherwise>N</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN27>
          <xsl:value-of select="$B5USIN27" />
        </B5USIN27>
        <xsl:variable name="B5USIN28">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_IncidentalOfficeInd"></xsl:value-of>
            </xsl:when>
			<!--Issue 59878 start-->
			<xsl:when test="$InsLine='CA' and $RateState = 'KY'">
              <xsl:value-of select="../../CommlCoverage[CoverageCd='LIA']/Option[OptionCd='com.csc_TortLimitationInd']/OptionValue"></xsl:value-of>
            </xsl:when>
			<!--Issue 59878 end-->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USIN28>
          <xsl:value-of select="$B5USIN28" />
        </B5USIN28>
		<!-- 59878 starts -->
		<xsl:variable name="B5USIN29">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="BldgProtection/com.csc_DistanceToHydrant">
					</xsl:value-of>
				</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='CA'">N</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='KS'">N</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='LA'">N</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN29>
			<xsl:value-of select="$B5USIN29"/>
		</B5USIN29>
		<!-- 59878 ends -->
        <xsl:variable name="B5AKCD">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ISODistrictNumber"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="normalize-space(AlarmAndSecurity/AlarmTypeCd)"></xsl:value-of><!--Issue 59878 -->
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_GroupIISymbolCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AKCD>
          <xsl:value-of select="$B5AKCD" />
        </B5AKCD>
        <xsl:variable name="B5ALCD">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_RatingPlanCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ISORiskId"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/CommlCoverageSupplement/CoinsurancePct"></xsl:value-of>
            </xsl:when>
            <!-- Case 29405 Begin -->
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/CommlVehSupplement/com.csc_AuthorityTypeCargoCd"></xsl:value-of>
            </xsl:when>
            <!-- Case 29405 End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5ALCD>
          <xsl:value-of select="$B5ALCD" />
        </B5ALCD>
		<!-- 59878 starts -->
		<xsl:variable name="B5AL1VCDE">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="Construction/BldgArea">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5AL1VCDE>
			<xsl:value-of select="$B5AL1VCDE"/>
		</B5AL1VCDE>
		<xsl:variable name="B5AL1WCDE">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="Construction/YearBuilt">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5AL1WCDE>
			<xsl:value-of select="$B5AL1WCDE"/>
		</B5AL1WCDE>
		<!--Issue 59878 Leasing concerns issue starts-->
		<xsl:variable name="B5AL1TCDE">
          <xsl:choose>
              <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/CommlVehSupplement/LeasingConcernCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AL1TCDE>
          <xsl:value-of select="$B5AL1TCDE" />
        </B5AL1TCDE>
		<!--B5AL1TCDE>N</B5AL1TCDE-->
		<!--Issue 59878 Leasing concerns issue ends- -->
		<xsl:variable name="B5IBNB">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="Construction/BldgCodeEffectivenessGradeCd">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5IBNB>
			<xsl:value-of select="$B5IBNB"/>
		</B5IBNB>
		<B5ICNB/>
		<B5IGNB>
		    <xsl:choose>
					<xsl:when test="$InsLine='CA'">
				    <xsl:choose>
						<xsl:when test="$RateState='KS'"></xsl:when>
						<xsl:when test="$RateState='LA'"></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
				    </xsl:choose>
				</xsl:when>
		    </xsl:choose>
		</B5IGNB>
		<xsl:variable name="B5IHNB">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="BldgProtection/com.csc_MilesToStation">
					</xsl:value-of>
				</xsl:when>
					<xsl:when test="$InsLine='CA'">
				    <xsl:choose>
						<xsl:when test="$RateState='KS'"></xsl:when>
						<xsl:when test="$RateState='LA'"></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
				    </xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5IHNB>
			<xsl:value-of select="$B5IHNB"/>
		</B5IHNB>
		<xsl:variable name="B5ALIQNBR">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:choose>
						<xsl:when test="BldgProtection/com.csc_AlternateDistanceToHydrant = 'Y'">1</xsl:when>
						<xsl:otherwise>2</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5ALIQNBR>
			<xsl:value-of select="$B5ALIQNBR"/>
		</B5ALIQNBR>
		<xsl:variable name="B5ALIRNBR">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="Construction/NumStories">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5ALIRNBR>
			<xsl:value-of select="$B5ALIRNBR"/>
		</B5ALIRNBR>
		<xsl:variable name="B5ALISNBR">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:variable name="CFSquareFeet" select="Construction/BldgArea" />
					<xsl:choose>
						<xsl:when test="string-length($CFSquareFeet) > 5">99999</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$CFSquareFeet" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5ALISNBR>
			<xsl:value-of select="$B5ALISNBR"/>
		</B5ALISNBR>
		<xsl:variable name="B5ALIVNBR">
			<xsl:choose>
				<xsl:when test="$InsLine='CA'">
					<xsl:value-of select="CommlVehSupplement/FarmLayupCreditCd">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5ALIVNBR>
			<xsl:value-of select="$B5ALIVNBR"/>
		</B5ALIVNBR>
		<!-- 59878 ends -->
        <xsl:variable name="B5AMCD">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ISORiskId"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ContentsRateGroupCd"></xsl:value-of>
            </xsl:when>
			<!--Issue 59878 Start-->
			<xsl:when test="$InsLine='CA' and ../CommlCoverage[CoverageCd='PIP'] and $RateState = 'MN'">
              <xsl:value-of select="../CommlCoverage[CoverageCd='PIP']/Option[OptionCd='com.csc_DeductibleType']/OptionValue"></xsl:value-of>
			</xsl:when>
             <xsl:when test="$InsLine='CA' and ../CommlCoverage[CoverageCd='PIP'] and $RateState = 'OR'">
				<xsl:value-of select="../CommlCoverage[CoverageCd='PIP']/Deductible/FormatCurrencyAmt/Amt"></xsl:value-of>
             </xsl:when>
			<xsl:when test="$InsLine='CA' and ../CommlCoverage[CoverageCd='PIP'] and $RateState = 'KY'">
				<xsl:value-of select="../CommlCoverage[CoverageCd='PIP']/Deductible/FormatCurrencyAmt/Amt"></xsl:value-of>
             </xsl:when>
            <!--Issue 59878 end-->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AMCD>
          <xsl:value-of select="$B5AMCD" />
        </B5AMCD>
        <xsl:variable name="B5ANCD">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/ModelYear" />
            </xsl:when>
            <xsl:when test="$InsLine='IMC'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_RiskClassCd" />
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_ZoneCd" />
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node[SubjectInsuranceCd='BPP']/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/Deductible/FormatCurrencyAmt/Amt"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5ANCD>
          <xsl:value-of select="$B5ANCD" />
        </B5ANCD>
        <xsl:variable name="B5USCD5">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC'">
				<!-- Issue 59878 Start -->
				<xsl:call-template name="FormatData">
	   	     		<xsl:with-param name="FieldName">$B5USCD5</xsl:with-param>      
					<xsl:with-param name="FieldLength">3</xsl:with-param>
					<xsl:with-param name="Value" select="$Node[CommlCoverage/CoverageCd = 'ACCTS' or CommlCoverage/CoverageCd = 'EQPDP' or CommlCoverage/CoverageCd = 'PHYSUR' or CommlCoverage/CoverageCd = 'VALPAP']/TerritoryCd"/>	<!--UNI00383-->
					<xsl:with-param name="FieldType">N</xsl:with-param>
				</xsl:call-template>      
				<!-- Issue 59878 End -->
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
              <!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/InsuredOrPrincipalInfo/BusinessInfo/SICCd"></xsl:value-of>--> 	<!--Issue 59878-->
			  <xsl:value-of select="com.csc_SubLocationInfo/com.csc_SICCd"></xsl:value-of>															<!--Issue 59878-->
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_GroupIIMultiplierCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="Construction/ConstructionCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
              <xsl:variable name="Amt" select="$Node/CommlCoverage[CoverageCd='CMP']/Deductible/FormatCurrencyAmt/Amt" />
              <xsl:if test="normalize-space($Amt)='0'">FULL</xsl:if>
              <xsl:if test="$Amt!='0'">
                <xsl:value-of select="$Amt" />
              </xsl:if>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USCD5>
          <xsl:value-of select="$B5USCD5" />
        </B5USCD5>
        <xsl:variable name="B5PLTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/CommlCoverage/Deductible/FormatPct"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="Construction/BldgCodeEffectivenessGradeCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='COL']/Deductible/FormatCurrencyAmt/Amt" />
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5PLTX>
          <xsl:value-of select="$B5PLTX" />
        </B5PLTX>
        <xsl:variable name="B5PMTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_RatingConstructionProtectionCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="Construction/BldgEffectivenessGradeTypeCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="$Node/CommlCoverage[CoverageCd='SP']/Option[OptionCd='com.csc_SpecPerilType']/OptionValue"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5PMTX>
          <xsl:value-of select="$B5PMTX" />
        </B5PMTX>
        <xsl:variable name="B5PNTX">
          <xsl:choose>
            <xsl:when test="$InsLine='IMC' or $InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_ContentsRateGroupCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5PNTX>
          <xsl:value-of select="$B5PNTX" />
        </B5PNTX>
        <xsl:variable name="B5POTX">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_BuildingClassCd"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5POTX>
          <xsl:value-of select="$B5POTX" />
        </B5POTX>
		<!-- 59878 starts -->
		<xsl:variable name="B5PPTX">
			<xsl:choose>
				<xsl:when test="$InsLine='CR'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdCR"/>
				</xsl:when>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="concat(com.csc_SubLocationInfo/com.csc_ContentsRateGroupCd,'')">
					</xsl:value-of>
				</xsl:when>
				<xsl:when test="$InsLine='IMC'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ClassCdIM"/>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5PPTX>
			<xsl:value-of select="$B5PPTX" />
		</B5PPTX>
		<!-- 59878 ends -->
        <xsl:variable name="B5B5PC">
          <xsl:choose>
            <xsl:when test="$InsLine='CF'">
              <xsl:choose>
                <xsl:when test="com.csc_SubLocationInfo/com.csc_WindHailDeductiblePct != ''">
                  <!-- Issue 59878 - Start -->
				  <!--xsl:value-of select="com.csc_SubLocationInfo/com.csc_WindHailDeductiblePct"></xsl:value-of-->
					<xsl:call-template name="FormatData">
	   	     			<xsl:with-param name="FieldName">$B5B5PC</xsl:with-param>      
						<xsl:with-param name="FieldLength">8</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_SubLocationInfo/com.csc_WindHailDeductiblePct"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>      
					<!-- Issue 59878 - End -->
                </xsl:when>
                <xsl:otherwise>000</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>000</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5B5PC>
          <xsl:value-of select="$B5B5PC" />
        </B5B5PC>
        <xsl:variable name="B5AGVA">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_PropertyOwnedByOther" />
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AGVA>
          <xsl:value-of select="$B5AGVA" />
        </B5AGVA>
        <xsl:variable name="B5AHVA">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node[SubjectInsuranceCd='BLDG']/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/Limit/FormatCurrencyAmt/Amt" />
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5AHVA>
          <xsl:value-of select="$B5AHVA" />
        </B5AHVA>
        <xsl:variable name="B5USVA3">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="$Node[SubjectInsuranceCd='BPP']/CommlCoverage[CoverageCd='SPC' or CoverageCd='STD']/Limit/FormatCurrencyAmt/Amt" />
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USVA3>
          <xsl:value-of select="$B5USVA3" />
        </B5USVA3>
        <xsl:variable name="B5USVA4">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="CrimeInfo/AnnualGrossReceiptsAmt/Amt" />
            </xsl:when>
			<!-- Issue 59878 Begin -->
			<xsl:when test="$InsLine='CA' and $RateState='TX'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd='PIP']/Option/OptionValue"/>
			</xsl:when>
			<!-- Issue 59878 END -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USVA4>
          <xsl:value-of select="$B5USVA4" />
        </B5USVA4>
        <xsl:variable name="B5BKNB">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:call-template name="FormatData">
                <xsl:with-param name="FieldName">B5BKNB</xsl:with-param>
                <xsl:with-param name="FieldLength">5</xsl:with-param>
                <xsl:with-param name="Value" select="$Node/CommlCoverage[CoverageCd='INFL']/Option/OptionValue" />
                <xsl:with-param name="FieldType">N</xsl:with-param>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="$InsLine='CA'">
              <xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_PolicyTermMonths" />
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5BKNB>
          <xsl:value-of select="$B5BKNB" />
        </B5BKNB>
        <xsl:variable name="B5BLNB">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/ProtectionClassGradeCd"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:value-of select="translate(com.csc_SubLocationInfo/com.csc_ClassLimitAmt/Amt,'$,,','')"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="com.csc_NumSwimmingPools"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5BLNB>
          <xsl:value-of select="$B5BLNB" />
        </B5BLNB>
        <xsl:variable name="B5USNB3">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/NumWatchPersons"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USNB3>
          <xsl:value-of select="$B5USNB3" />
        </B5USNB3>
        <xsl:variable name="B5USNB4">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="AlarmAndSecurity/NumGuards"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
    			<!-- Issue 59878 - Start -->
	          <!--xsl:value-of select="Construction/NumStories"></xsl:value-of-->
				<xsl:call-template name="FormatData">
	   				<xsl:with-param name="FieldName">$B5USNB4</xsl:with-param>      
					<xsl:with-param name="FieldLength">5</xsl:with-param>
					<xsl:with-param name="Value" select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_EQNumberOfStories"/>
					<xsl:with-param name="FieldType">N</xsl:with-param>
				</xsl:call-template>      
				<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USNB4>
          <xsl:value-of select="$B5USNB4" />
        </B5USNB4>
        <xsl:variable name="B5USNB5">
          <xsl:choose>
            <xsl:when test="$InsLine='CR'">
              <xsl:value-of select="CrimeInfo/AvgNumEmployees"></xsl:value-of>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
				<!-- Issue 59878 - Start -->
              <!--xsl:value-of select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/CommlCoverage/Limit/FormatCurrencyAmt/Amt"></xsl:value-of-->
				<xsl:call-template name="FormatData">
     				<xsl:with-param name="FieldName">$B5USNB5</xsl:with-param>      
					<xsl:with-param name="FieldLength">5</xsl:with-param>
					<xsl:with-param name="Value" select="com.csc_SubLocationInfo/com.csc_EarthquakeInfo/com.csc_EQMasonryVeneerLimitation"/>
					<xsl:with-param name="FieldType">N</xsl:with-param>
				</xsl:call-template>      
            </xsl:when>
			<xsl:when test="$InsLine='CA'">
			    <xsl:choose>
					<xsl:when test="$RateState='KS'"></xsl:when>
					<xsl:when test="$RateState='LA'"></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
			    </xsl:choose>
			</xsl:when>
			<!-- Issue 59878 - End -->
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5USNB5>
          <xsl:value-of select="$B5USNB5" />
        </B5USNB5>
        <xsl:variable name="B5DCNB">
          <xsl:choose>
            <xsl:when test="$InsLine='BOP'">
              <xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/SubLocation[substring(@id,2,string-length(@id)-1)=$SubLocation]/SubLocationDesc"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <B5DCNB>
          <xsl:value-of select="$B5DCNB" />
        </B5DCNB>
        <xsl:if test="$InsLine='CA'">
          <B5DDNB>
            <xsl:value-of select="$Node/VehIdentificationNumber" />
          </B5DDNB>
          <B5DCNB>
            <xsl:value-of select="concat($Node/Manufacturer,' ',$Node/Model)" />
          </B5DCNB>
          <B5A9NB>
            <xsl:value-of select="$Node/CostNewAmt/Amt" />
          </B5A9NB>
        </xsl:if>
		<!-- 59878 starts -->
		<B5CXVA></B5CXVA>
		<xsl:variable name="B5USIN30">
			<xsl:choose>
				<xsl:when test="$InsLine='CA' and $RateState='KS'">N</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='LA'">N</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='MN'">N</xsl:when>
				<xsl:when test="$InsLine='CA' and $RateState='NV'">N</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5USIN30>
			<xsl:value-of select="$B5USIN30"/>
		</B5USIN30>
		<xsl:if test="$InsLine='CF'">
			<B5AL4HTXT>N</B5AL4HTXT>	<!-- Issue 59878 -->
		</xsl:if>
		<!--Issue 59878 Start-->
		<xsl:if test="$InsLine='CA'">
			<B5AL4HTXT>
				<xsl:value-of select="CommlVehSupplement/com.csc_BIBuyback">
				</xsl:value-of>
			</B5AL4HTXT>
		</xsl:if>
		<!--Issue 59878 end-->
		<xsl:if test="$InsLine='CF'">
			<B5AL4PTXT>N</B5AL4PTXT>
		</xsl:if>
		<xsl:variable name="B5AL4QTXT">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_CityTaxCdMN">
					</xsl:value-of>
				</xsl:when>
				<xsl:when test="$InsLine='CA'">
					<xsl:value-of select="CommlVehSupplement/com.csc_CoveredAutoCd">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5AL4QTXT>
			<xsl:value-of select="$B5AL4QTXT"/>
		</B5AL4QTXT>
		<xsl:variable name="B5AL36TXT">
			<xsl:choose>
				<xsl:when test="$InsLine='CF'">
					<xsl:value-of select="com.csc_SubLocationInfo/com.csc_ISORiskId">
					</xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<B5AL36TXT>
			<xsl:value-of select="$B5AL36TXT"/>
		</B5AL36TXT>
		<!-- 59878 ends -->
      </ASB5CPL1__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->