<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreateTrailerInterchange">
	        <xsl:variable name="TRL" select="/*/ASBYCPL1__RECORD[substring(BYAOTX,1,3)='TRL']"/>
	        <xsl:if test="string-length($TRL)">
            <TruckersSupplement>
               <TruckersTrailerInterchange>
                  <FarthestTerminalZoneCd><xsl:value-of select="$TRL/BYPPTX"/></FarthestTerminalZoneCd>
                  <NumDays><xsl:value-of select="$TRL/BYUSNB3"/></NumDays>
                  <NumTrailers><xsl:value-of select="$TRL/BYBLNB"/></NumTrailers>
                  <RadiusUse><xsl:value-of select="$TRL/BYPOTX"/></RadiusUse>
                  <RadiusCd><xsl:value-of select="$TRL/BYALCD"/></RadiusCd>
                   <xsl:for-each select="/*/ASBYCPL1__RECORD[substring(BYAOTX,1,3)='TRL']">
                       <xsl:call-template name="CreateCommlCoverage">
                       </xsl:call-template>
                    </xsl:for-each>
               </TruckersTrailerInterchange>
            </TruckersSupplement>
            </xsl:if>
	</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->