<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateBASCLT1500">
      <xsl:param name="Location">0</xsl:param>
      <xsl:param name="SubLocation">0</xsl:param>
      <xsl:param name="UnitNbr">0</xsl:param> 
      <xsl:variable name="UnitSeq" select="'0'"/> 
       <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>BASCLT1500</RECORD__NAME>
         </RECORD__NAME__ROW>
                 
         
         <BASCLT1500__RECORD>
         	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
         	<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
         	<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
         	<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
         	<MODULE><xsl:value-of select="$MOD"/></MODULE>
          	<USE0CODE><xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/></USE0CODE>
            <xsl:choose>
              <xsl:when test="$UnitNbr &gt; '0'">
              <USE0LOC>
                 <xsl:call-template name="FormatData">
	   	          	<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>      
		            <xsl:with-param name="FieldLength">5</xsl:with-param>
		            <xsl:with-param name="Precision">0</xsl:with-param>		      
		            <xsl:with-param name="Value" select="$UnitNbr"/>
		            <xsl:with-param name="FieldType">N</xsl:with-param>
	             </xsl:call-template>
              </USE0LOC>
               <DESC0SEQ><xsl:value-of select="position()"/></DESC0SEQ>
              </xsl:when>
            <xsl:otherwise>
              <USE0LOC>
                 <xsl:call-template name="FormatData">
	   	     	    <xsl:with-param name="FieldName">USE0LOC</xsl:with-param>      
		            <xsl:with-param name="FieldLength">5</xsl:with-param>
		            <xsl:with-param name="Precision">0</xsl:with-param>		      
		            <xsl:with-param name="Value" select="$Location"/>
		            <xsl:with-param name="FieldType">N</xsl:with-param>
	             </xsl:call-template>
              </USE0LOC>
			  <!-- issue # 50774 starts-->
    <!--             <xsl:choose>
                   <xsl:when test="$SubLocation = '0'">
						<DESC0SEQ><xsl:value-of select="position()"/></DESC0SEQ>
                   </xsl:when>
                   <xsl:otherwise>
					   <DESC0SEQ><xsl:value-of select="$SubLocation"/></DESC0SEQ>
                 	</xsl:otherwise>
                 </xsl:choose> -->
				 <DESC0SEQ>
				   		<xsl:call-template name="FormatData">
	   	     	    		<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>      
		            		<xsl:with-param name="FieldLength">5</xsl:with-param>
		            		<xsl:with-param name="Precision">0</xsl:with-param>		      
		            		<xsl:with-param name="Value" select="position()"/>
		            		<xsl:with-param name="FieldType">N</xsl:with-param>
	             		</xsl:call-template>
				 	</DESC0SEQ>
				 <!-- issue # 50774 ends-->
            </xsl:otherwise>
            </xsl:choose> 
        </BASCLT1500__RECORD>
      
        
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->