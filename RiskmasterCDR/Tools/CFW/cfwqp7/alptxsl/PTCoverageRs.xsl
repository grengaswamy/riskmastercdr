<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	    <!-- 55614 begin -->
	    <!--		
	<xsl:template name="CreateCoverage">
	    <xsl:param name="LOB"/>

		<xsl:variable name="coveragetype" select="C4AOTX"/>
	    <xsl:choose>
			Case 34770
			<xsl:when test="$LOB='HP'">
			-->
		<!--	<xsl:if test="$coveragetype ='HO2475'">
			<Watercraft>
				<WaterUnitTypeCd/>
			  	<ItemDefinition>
                	<ItemTypeCd/>
					<SerialIdNumber>
					 <xsl:value-of select="C4C0NB"/> 
					</SerialIdNumber>
                	<Manufacturer/>
                	<Model/>
                	<ModelYear/>
                	<ItemDesc>
						<xsl:value-of select="C4RPTX"/>    
					</ItemDesc>
			  	</ItemDefinition>
			  	<Length>
					<NumUnits>
				  		<xsl:value-of select="C4ISTX"/>
					</NumUnits>
                  	<UnitMeasurementCd/>
				</Length>
			   	<Speed>
                	<NumUnits/>
				  	<UnitMeasurementCd/>
               	</Speed>
			    <Horsepower>
                	<NumUnits>
				  		<xsl:value-of select="C4IRTX"/>
				    </NumUnits>
                    <UnitMeasurementCd/>
				</Horsepower>
                <PropulsionTypeCd/>
               <Coverage>
                  <CoverageCd>
                        <xsl:value-of select="C4AOTX"/>    
                  </CoverageCd>
				    <Limit>
                	  <FormatCurrencyAmt>
                      	<Amt/>
                      </FormatCurrencyAmt>
                    </Limit>
               	  	<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="C4A3VA"/>
						</Amt>
					</CurrentTermAmt>
					<Form>
                    	<FormNumber/>
                    	<FormName/>
                    	<EditionDt/>
                	</Form>
            	</Coverage>
		 	</Watercraft>
		</xsl:if>	  -->
	    <!--<xsl:when test="$LOB='HP'">-->
		<!--<xsl:if test="$coveragetype !='HO2475'">-->
		<!-- Case 34770 ends  5 july-->
		<!--
               <Coverage>
                  <CoverageCd>
                        <xsl:value-of select="C4AOTX"/>    
                  </CoverageCd>
				   Case 34770Starts -->
				  <!--<IterationNumber>
				  <xsl:value-of select="C4C0NB"/> 
				  </IterationNumber>-->
				  
                  <!--<CoverageDesc/>-->
				  <!--<CoverageDesc>
				  <xsl:value-of select="C4RPTX"/>    
				  </CoverageDesc>-->
					
				  <!-- Case 34770 Ends -->
				  <!-- Case 34770Starts -->
					<!--Issue start jun 17-->
					<!--
				<xsl:variable name="iterationnum" select="C4C0NB"/>
					<xsl:variable name="extcovcode" select="C4AOTX"/>
					<xsl:variable name="iterationcount" select="count(//ASC4CPL1__RECORD[C4AOTX=$extcovcode])"/>

					<xsl:if test="$iterationcount > 0">
						<xsl:for-each select="//ASC4CPL1__RECORD[C4AOTX=$extcovcode]">
							<xsl:if test="position() = count(//ASC4CPL1__RECORD[C4AOTX=$extcovcode])">
								<xsl:variable name="maxcount" select="C4C0NB"/>
								<IterationNumber>
									<xsl:value-of select="concat($iterationnum,'#',$maxcount)"/>
								</IterationNumber>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					Issue end jun 17-->
					<!--
					<xsl:if test="$iterationcount = 0"> issue 34770 jun 17
						<IterationNumber>
							<xsl:value-of select="C4C0NB"/>
						</IterationNumber>
					</xsl:if> issue 34770 jun 17-->

					<!-- Case 34770 starts 30 april -->
					<!--
			<xsl:choose> 					
			<xsl:when test="$coveragetype ='HO2471'">
				
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>1</OptionCd>
					 <xsl:if test="C4BKVA &gt; '0'">
	                 <OptionValue>1</OptionValue>
					 </xsl:if>
	              </Option> 
				  
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>2</OptionCd>
					 <xsl:if test="C4BMVA &gt; '0'">
	                 	<OptionValue>1</OptionValue>
					 </xsl:if>
	              </Option> 
				
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>3</OptionCd>
					 <xsl:if test="C4BOVA &gt; '0'">
	                 	<OptionValue><xsl:value-of select="C4IVTX"/></OptionValue>
					 </xsl:if>
	              </Option> 
				
				
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>4</OptionCd>
					 <xsl:if test="C4BLVA &gt; '0'">
	                 	<OptionValue><xsl:value-of select="C4ISTX"/></OptionValue>
					 </xsl:if>
	              </Option> 
				
			
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>5</OptionCd>
					 <xsl:if test="C4BNVA &gt; '0'">
	                 <OptionValue>1</OptionValue>
					 </xsl:if>
	              </Option> 
				
			</xsl:when>
			<xsl:otherwise> 
				<xsl:if test="string-length(C4IRTX) &gt; '0'">                            
                  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd></OptionCd>
	                 <OptionValue><xsl:value-of select="C4IRTX"/></OptionValue>
	              </Option> 
          </xsl:if>

					Case 34770 Ends  30 april-->
<!-- Case 34770 starts 30 april -->
					

					<!-- Case 34770 Ends  30 april-->
					<!-- Case 34770 starts 30 april -->
				<!--		<xsl:if test="substring(C4ISTX,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="C4ISTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>-->

					<!-- Case 34770 Ends  30 april-->
					<!--
					<xsl:if test="string-length(C4ISTX) &gt; '0'">
	              <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd></OptionCd>
	                 <OptionValue><xsl:value-of select="C4ISTX"/></OptionValue>
	              </Option>         					
          </xsl:if>  
		  	<xsl:if test="string-length(C4ITTX) &gt; '0'"> 
		  	-->                           
                 <!-- <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="C4ITTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>-->
                  <!--
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd></OptionCd>
	                 <OptionValue><xsl:value-of select="C4ITTX"/></OptionValue>
	              </Option>         					
          </xsl:if>

					 Case 34770 starts 30 april -->
					<!--
						<xsl:if test="string-length(C4IUTX) &gt; '0'">
						-->                            
                 <!-- <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="C4IUTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>-->
                  <!--
				   <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd></OptionCd>
	                 <OptionValue><xsl:value-of select="C4IUTX"/></OptionValue>
	              </Option>  
          </xsl:if>
                    -->
					<!-- Case 34770 Ends  30 april-->
					<!-- Case 34770 starts 30 april -->
					<!--
						<xsl:if test="string-length(C4IVTX) &gt; '0'">                          
                  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd></OptionCd>
	                 <OptionValue><xsl:value-of select="C4IVTX"/></OptionValue>
	              </Option>
          </xsl:if>
          -->
					<!-- Case 34770 Ends  30 april-->
					<!-- Starts Code for residence Employee-->
					<!--
         <xsl:if test="substring(C4BKVA,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="C4BKVA"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>
             End Code for residence Employee-->
		  	<!--
         <xsl:if test="substring(C4BLVA,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="C4BLVA"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>                                            
         <xsl:if test="substring(C4BTCD,1,1) &gt; '0'">
	              <Option>
	                 <OptionTypeCd>Num1</OptionTypeCd>
	                 <OptionCd>com.csc_NumLocations</OptionCd>
	                 <OptionValue><xsl:value-of select="C4BTCD"/></OptionValue>
	              </Option>         
          </xsl:if>                  
		  Starts Case 34770 added on 3 may-->
		  <!--
		  <xsl:if test="substring(C4BUCD,1,1) &gt; '0'">
	              <Option>
	                 <OptionTypeCd>Fum1</OptionTypeCd>
	                 <OptionCd>com.csc_NumFamilys</OptionCd>
	                 <OptionValue><xsl:value-of select="C4BUCD"/></OptionValue>
	              </Option>         
          </xsl:if> 
		  </xsl:otherwise>
		  </xsl:choose>
		    Ends  Case 34770 added on 3 may-->
		    <!--                
                  <CurrentTermAmt>
                     <Amt>
                        <xsl:value-of select="C4A3VA"/>
                     </Amt>                                 
                  </CurrentTermAmt>
               </Coverage>
               -->
			   <!--	</xsl:if>--><!--	  Added   5 july-->
			   <!--
          </xsl:when>
          <xsl:otherwise>
                <Coverage>
                  <CoverageCd>
                        <xsl:value-of select="BKAOTX"/>    
                  </CoverageCd>
                  <CoverageDesc/>
         <xsl:if test="substring(BKBBTX,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="BKBBTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>                 
         <xsl:if test="substring(BKA9NB,1,1) &gt; '0'">
                   <Deductible>
                     <FormatCurrencyAmt>
                        <Amt>                     
                        <xsl:value-of select="BKA9NB"/>
                     </Amt>   
                     </FormatCurrencyAmt>
                     <DeductibleTypeCd>FL</DeductibleTypeCd>
                  </Deductible>
         </xsl:if>                  
                  <CurrentTermAmt>
                     <Amt>
                        <xsl:value-of select="BKA3VA"/>
                     </Amt>                                 
                  </CurrentTermAmt>
               </Coverage>         
          </xsl:otherwise>     
        </xsl:choose>
	</xsl:template>
	-->

	<xsl:template name="CreateCoverage">
	    <xsl:param name="LOB"/>
	    <xsl:choose>
			<xsl:when test="$LOB='HP' or $LOB='FP'">
		        <Coverage>
                  <CoverageCd>
                        <xsl:value-of select="C4AOTX"/>    
                  </CoverageCd>
                   <!-- Case 34770Starts -->
					<!--Issue start jun 17-->
				<xsl:variable name="iterationnum" select="C4C0NB"/>
					<xsl:variable name="extcovcode" select="C4AOTX"/>
					<xsl:variable name="iterationcount" select="count(//ASC4CPL1__RECORD[C4AOTX=$extcovcode])"/>

          <xsl:if test="$iterationcount &gt; 0">
						<xsl:for-each select="//ASC4CPL1__RECORD[C4AOTX=$extcovcode]">
							<xsl:if test="position() = count(//ASC4CPL1__RECORD[C4AOTX=$extcovcode])">
								<xsl:variable name="maxcount" select="C4C0NB"/>
								<IterationNumber>
								   <!-- 55614 begin -->
									<!--<xsl:value-of select="concat($iterationnum,'#',$maxcount)"/>-->
									<xsl:value-of select="concat($iterationnum,'.',$maxcount)"/>
									<!-- 55614 end -->
								</IterationNumber>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					<!--Issue end jun 17-->
					<xsl:if test="$iterationcount = 0"> <!--issue 34770 jun 17-->
						<IterationNumber>
							<xsl:value-of select="C4C0NB"/>
						</IterationNumber>
					</xsl:if> <!--issue 34770 jun 17-->
		  <!-- Issue 64220 starts -->
		  <CoverageDesc>
			  <xsl:choose>
	            <xsl:when test="C4AOTX='ADDRES' or C4AOTX='HO0420' or C4AOTX='HO0441' or C4AOTX='HO2471' or C4AOTX='HO0454' or C4AOTX='HO2413' or C4AOTX='HO0435' or C4AOTX='EMPLYS' or C4AOTX='HO0480' or C4AOTX='HO0499' or C4AOTX='HO0414' or C4AOTX='H-1003' or C4AOTX='HO2475'"> 
	              <xsl:value-of select="C4RPTX" />
	            </xsl:when>
			  </xsl:choose>			
		  </CoverageDesc>
		  <!-- Issue 64220 ends -->
                  <xsl:choose>
            <xsl:when test="C4AOTX='LAC' or C4AOTX='HO0453' or C4AOTX='HO0454' or C4AOTX='COMPUT' or C4AOTX='IM 144' or C4AOTX='HO2413' or C4AOTX='HO0448' or C4AOTX='HO0435' or C4AOTX='EMPLYS' or C4AOTX='DP0480' or C4AOTX='EMPLOY' or normalize-space(C4AOTX)='HP031' or C4AOTX='DP0437' or C4AOTX='DP0419' or C4AOTX='DP0420' or C4AOTX='FOST' or C4AOTX='DP0468' or C4AOTX='DP0414' or C4AOTX='DP0473' or C4AOTX='DL2414' or C4AOTX='DP0463' or C4AOTX='DP1766' or C4AOTX='DP0472' or C4AOTX='DP0470'"> <!-- Issue 64220 --><!--SYS/DST Log # 268 Added Coverages DP0414,DP0473,DL2414,DP0463,DP1766,DP0472,DP0470-->
              <Limit>
                <FormatCurrencyAmt>
                  <Amt>
                    <xsl:value-of select="C4BKVA" />
                  </Amt>
                </FormatCurrencyAmt>
              </Limit>
			  	<!--Issue 64220 Starts-->
				<xsl:if test="C4AOTX='HO0435'">
					<Limit>
		                <FormatCurrencyAmt>
		                  <Amt>
		                    <xsl:value-of select="C4BLVA" />
		                  </Amt>
		                </FormatCurrencyAmt>
						<LimitAppliesToCd>com.csc_Location</LimitAppliesToCd>
		            </Limit>
				</xsl:if>
				<!--Issue 64220 Ends-->
            </xsl:when>			
                  <xsl:when test="C4AOTX='MOLD'">
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt><xsl:value-of select="C4BKVA"/></Amt>
                     </FormatCurrencyAmt>
                     <LimitAppliesToCd><xsl:value-of select="C4BTCD"/></LimitAppliesToCd>
                  </Limit>
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt><xsl:value-of select="C4BLVA"/></Amt>
                     </FormatCurrencyAmt>
                     <LimitAppliesToCd><xsl:value-of select="C4BUCD"/></LimitAppliesToCd>
                  </Limit>
                  </xsl:when>
                  <xsl:when test="C4BTCD='$AMT' and C4AOTX!='LAC'">
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt><xsl:value-of select="C4A5VA"/></Amt>
                     </FormatCurrencyAmt>
                  </Limit>
                  </xsl:when>
                  <xsl:when test="C4AHVA >'0.00'">
                  <Limit>
                     <FormatModFactor><xsl:value-of select="C4AHVA"/></FormatModFactor>
                  </Limit>
                  </xsl:when>
		  	<!-- Issue 64220 starts -->
			<xsl:when test="C4AOTX ='ADDRES'">
				<Option>
	             <OptionTypeCd></OptionTypeCd>
	             <OptionCd>com.csc_NumLocations</OptionCd>	                            
	             <OptionValue><xsl:value-of select="C4BTCD"/></OptionValue>	                            
	          	</Option> 
				  <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>com.csc_NumEmployees</OptionCd>	                                 
	                 <OptionValue><xsl:value-of select="C4BUCD"/></OptionValue>
	              </Option> 
				   <Option>
	                 <OptionTypeCd></OptionTypeCd>
	                 <OptionCd>2</OptionCd>	                                 
	                 <OptionValue><xsl:value-of select="C4BUCD"/></OptionValue>
	              </Option> 
			</xsl:when>
			<!--Issue 64220 ends-->
                  <xsl:otherwise>
                  <xsl:if test="C4AOTX != 'ORDLAW' and C4AOTX != 'HO0493'">
                  <Option>
							<OptionTypeCd>Ind1</OptionTypeCd>
							<OptionValue><xsl:value-of select="C4BTCD"/></OptionValue>
						</Option>
                  </xsl:if>
                  </xsl:otherwise>     
                 </xsl:choose>
                  <xsl:if test="C4AOTX = 'ORDLAW' or C4AOTX = 'HO0493'">
                  <Limit>
                     <FormatPct><xsl:value-of select="C4BKVA"/></FormatPct>
                     <LimitAppliesToCd><xsl:value-of select="C4BTCD"/></LimitAppliesToCd>
                  </Limit>
                  </xsl:if>
                  <CurrentTermAmt>
                     <Amt>
                        <xsl:value-of select="C4A3VA"/>
                     </Amt>                                 
                  </CurrentTermAmt>
               </Coverage>         
       </xsl:when>
          <xsl:otherwise>
                <Coverage>
                  <CoverageCd>
                        <xsl:value-of select="BKAOTX"/>    
                  </CoverageCd>
                  <CoverageDesc/>
         <xsl:if test="substring(BKBBTX,1,1) &gt; '0'">                            
                  <Limit>
                     <FormatCurrencyAmt>
                        <Amt>
                        <xsl:value-of select="BKBBTX"/>              
                        </Amt>
                     </FormatCurrencyAmt>
                  </Limit>
          </xsl:if>                 
         <xsl:if test="substring(BKA9NB,1,1) &gt; '0'">
                   <Deductible>
                     <FormatCurrencyAmt>
                        <Amt>                     
                        <xsl:value-of select="BKA9NB"/>
                     </Amt>   
                     </FormatCurrencyAmt>
                     <DeductibleTypeCd>FL</DeductibleTypeCd>
                  </Deductible>
         </xsl:if>                  
                  <CurrentTermAmt>
                     <Amt>
                        <xsl:value-of select="BKA3VA"/>
                     </Amt>                                 
                  </CurrentTermAmt>
               </Coverage>         
          </xsl:otherwise>     
        </xsl:choose>
	</xsl:template>	
	
</xsl:stylesheet>

<!-- 55614 begin -->
