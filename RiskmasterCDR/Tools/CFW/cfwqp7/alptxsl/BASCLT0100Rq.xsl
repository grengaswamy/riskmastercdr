<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="CreateBASCLT0100">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>BASCLT0100</RECORD__NAME>
         </RECORD__NAME__ROW>
                 
         
         <BASCLT0100__RECORD>
         	<xsl:choose>                                                                                                                                     
       	      <xsl:when test = "PersPolicy/LOBCd = 'APV' or PersPolicy/LOBCd = 'HP' or PersPolicy/LOBCd = 'HO' or PersPolicy/LOBCd = 'FP'">
      	         <CLIENTNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname"/></CLIENTNAME>
      	      </xsl:when>                                                                                                                                             
              <xsl:otherwise>            
                 <CLIENTNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/></CLIENTNAME>   
              </xsl:otherwise>                                                                                                                                      
            </xsl:choose>        
           	<FIRSTNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName"/></FIRSTNAME>  
         	<MIDNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/OtherGivenName"/></MIDNAME>  
          	<SUFFIXNM><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/NameSuffix"/></SUFFIXNM>  
       	    <LONGNAME><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/com/csc_LongName"/></LONGNAME> 
			 <!--Issue 65778 Start-->
			 <xsl:choose>
				 <xsl:when test = "PersPolicy/LOBCd = 'HP' or PersPolicy/LOBCd = 'HO' or PersPolicy/LOBCd ='FP'">
					 <DBA>
					 <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
					 </DBA>					
				</xsl:when>
				 <xsl:otherwise>
			<!--Issue 65778 End-->
         	<DBA><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/></DBA>
			<!--Issue 65778 Start-->
				 </xsl:otherwise>
			 </xsl:choose>
			<!--Issue 65778 End-->
            <PHONE1><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"/></PHONE1> 
            <FEDTAXID><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/></FEDTAXID> 
      	    <SSN><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/></SSN> 
      	    <SEX><xsl:value-of select="InsuredOrPrincipal/DriverInfo/PersonInfo/GenderCd"/></SEX>
     	    <xsl:choose>                                                                                                                                     
      	      <xsl:when test ="PersPolicy/LOBCd = 'APV'or /ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/LOBCd = 'HO' or /ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/LOBCd = 'HP'">     
  				 <BIRTHDATE><xsl:value-of select="PersAutoLineBusiness/PersDriver/DriverInfo/PersonInfo/BirthDt"/></BIRTHDATE>   
              </xsl:when>                                                                                                                                             
              <xsl:otherwise>          
                 <BIRTHDATE>2001-01-01</BIRTHDATE> 
              </xsl:otherwise>                                                                                                                                      
            </xsl:choose>                                                                                                                                            
			<!--Issue 98500 start-->
			<xsl:choose>
				<xsl:when test ="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/LOBCd = 'WCV'">
  				<CONTACT><xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'PrimaryContact']/GeneralPartyInfo/NameInfo/com.csc_LongName"/></CONTACT>
  				<PHONE2><xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'PrimaryContact']/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"/></PHONE2> 
				</xsl:when>
      </xsl:choose>
			<!--Issue 98500 end-->                                                                                                                                           
         </BASCLT0100__RECORD>
      
        
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
