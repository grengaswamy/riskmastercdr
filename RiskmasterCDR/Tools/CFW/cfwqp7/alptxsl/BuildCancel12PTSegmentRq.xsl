<?xml version="1.0"?>
<!-- 50764 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="BuildCancel12PTSegmentRq">
		<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/com.csc_AmendmentMode"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200__RECORD>
				<TRANS0STAT>P</TRANS0STAT>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<ID12>12</ID12>
				<USE0CODE>UR</USE0CODE>
				<USE0LOC>00001</USE0LOC>
				<DESC0SEQ>1</DESC0SEQ>
				<DZIP0CODE></DZIP0CODE>
				<DESC0LINE1><xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonRemarksTxt,1,30)"/></DESC0LINE1>
				<DESC0LINE2><xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonRemarksTxt,31,30)"/></DESC0LINE2>
				<DESC0LINE3><xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonRemarksTxt,61,30)"/></DESC0LINE3>
				<DESC0LINE4><xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonRemarksTxt,91,30)"/></DESC0LINE4>
				<DESC0LINE5><xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonRemarksTxt,121,30)"/></DESC0LINE5>
				<EFF0DATE><xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ContractTerm/EffectiveDt"/></EFF0DATE>
				<EXP0DATE><xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ContractTerm/ExpirationDt"/></EXP0DATE>
				<OWNPERCENT>000000</OWNPERCENT>
				<OWNSALARY>000000000</OWNSALARY>
				<OWNTITLE></OWNTITLE>
				<DEEMCODE></DEEMCODE>
				<DEEMSTAT></DEEMSTAT>
				<SSN></SSN>
				<TAXID></TAXID>
				<FLAG></FLAG>
			</PMSP1200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->