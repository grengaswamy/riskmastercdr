<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template name="BuildSignOn">
   <SignonRs>
      <Status>
	 <StatusCd>0</StatusCd>
	 <StatusDesc>Success</StatusDesc>
      </Status>
      <ClientDt>2002-06-16T15:56:00</ClientDt>
      <CustLangPref>US-en</CustLangPref>
      <ClientApp>
         <Org>com.csc</Org>
         <Name>PT</Name>
         <Version>9.0</Version>
      </ClientApp>
      <ServerDt>2003-03-16</ServerDt>
      <Language>US-en</Language>            
   </SignonRs>
    </xsl:template>
    
</xsl:stylesheet>

