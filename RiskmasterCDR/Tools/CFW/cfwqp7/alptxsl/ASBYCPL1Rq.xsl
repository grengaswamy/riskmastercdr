<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name="BuildCoverage">
    <xsl:param name="Location">0</xsl:param>
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
<!-- 59878 starts -->
	<xsl:param name="UnitNbr"></xsl:param>		
    <xsl:param name="SeqNbr">0</xsl:param>		
    <xsl:param name="IterationNbr"></xsl:param>	
    <xsl:param name="UNPDInd"></xsl:param>		
<!-- 59878 ends-->
<!--Issue 102807 starts-->
<xsl:param name="AlarmClassCd"></xsl:param>	
<xsl:param name="WatchmanCd"></xsl:param>	
<xsl:param name="AlarmConnectionCd"></xsl:param>	
<!--Issue 102807 ends-->
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ASBYCPL1</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ASBYCPL1__RECORD>
        <xsl:variable name="CovCode">
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">
              <xsl:choose>
                <xsl:when test="CoverageCd='RCO'">RENTAL</xsl:when>
                <xsl:when test="CoverageCd='RCL'">RENTAL</xsl:when>
                <xsl:when test="CoverageCd='RNT'">RENTAL</xsl:when>
                <xsl:when test="CoverageCd='RSP'">RENTAL</xsl:when>
                <xsl:when test="CoverageCd='TCO'">TRLCOL</xsl:when>
                <xsl:when test="CoverageCd='TCP'">TRLCMP</xsl:when>
                <xsl:when test="CoverageCd='TSP'">TRLSP</xsl:when>
				<xsl:when test="CoverageCd='DUP'">DOCUMP</xsl:when><!-- Issue 67226 -->
					<!-- Issue 59878 Start -->
				<xsl:when test="CoverageCd='MP'">MEDPAY</xsl:when>
				<xsl:when test="CoverageCd='LIA'">LIAB</xsl:when>
				<xsl:when test="CoverageCd='DCM'">DOC-CM</xsl:when>
				<xsl:when test="CoverageCd='DCO'">DOC-CO</xsl:when>
				<xsl:when test="CoverageCd='DUM'">DOC-UM</xsl:when>
				<xsl:when test="CoverageCd='DUN'">DOC-UN</xsl:when>
				<xsl:when test="CoverageCd='CMP'">COMP</xsl:when>
				<xsl:when test="CoverageCd='COL'">COLL</xsl:when>
				<xsl:when test="CoverageCd='AMU'">AMUSE</xsl:when>
				<xsl:when test="CoverageCd='TAP'">TAPES</xsl:when>
				<xsl:when test="CoverageCd='SOU'">SOUND</xsl:when>
				<xsl:when test="CoverageCd='LCM'">LOANCM</xsl:when>
				<xsl:when test="CoverageCd='LCO'">LOANCO</xsl:when>
				<xsl:when test="CoverageCd='TL'">TOWING</xsl:when>
				<xsl:when test="CoverageCd='DCL'">DOC-L</xsl:when>
				<xsl:when test="CoverageCd='HAL'">HIRE</xsl:when>
				<xsl:when test="Limit[LimitAppliesToCd='UMPD']">UMPD</xsl:when>
				<xsl:when test="Limit[LimitAppliesToCd='UM']">UM</xsl:when>
				<xsl:when test="$UNPDInd and $RateState = 'WA'">UNPD</xsl:when>	<!--Issue 59878-->
				<!-- Issue 59878 End -->
				<xsl:otherwise>
					<!-- Issue 59878 Start -->
					<xsl:value-of select="CoverageCd"/>
					<!--xsl:message terminate="yes">Unrecognized ACORD Coverage Code:<xsl:value-of select="CommlCoverage/CoverageCd"></xsl:value-of>. The context is:<xsl:value-of select="name()"></xsl:value-of>.</xsl:message-->
					<!-- Issue 59878 End -->
				</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='CF'">
              <xsl:choose>
                <xsl:when test="SubjectInsuranceCd='BLDG'">BLDG</xsl:when>
                <xsl:when test="SubjectInsuranceCd='BRWOR'">BR</xsl:when>
                <xsl:when test="SubjectInsuranceCd='BUSEE' or SubjectInsuranceCd='BUSIN'">BUSINC</xsl:when>
                <xsl:when test="SubjectInsuranceCd='DEBRL'">DEBRIS</xsl:when>
                <xsl:when test="SubjectInsuranceCd='EE'">EE</xsl:when>
                <xsl:when test="SubjectInsuranceCd='OPTBD'">FIRE-B</xsl:when>
                <xsl:when test="SubjectInsuranceCd='OPTPP'">FIRE-P</xsl:when>
                <xsl:when test="SubjectInsuranceCd='LIIAB'">IMPBET</xsl:when>
                <xsl:when test="SubjectInsuranceCd='com.csc_LOSS'">LOSS</xsl:when>
                <xsl:when test="SubjectInsuranceCd='MIN'">MINE</xsl:when>
                <xsl:when test="SubjectInsuranceCd='com.csc_ORDA'">ORD-A</xsl:when>
                <xsl:when test="SubjectInsuranceCd='com.csc_ORDB'">ORD-B</xsl:when>
                <xsl:when test="SubjectInsuranceCd='com.csc_ORDC'">ORD-C</xsl:when>
                <xsl:when test="SubjectInsuranceCd='PS'">PEAK</xsl:when>
                <xsl:when test="SubjectInsuranceCd='BPP'">PERS</xsl:when>
                <xsl:when test="SubjectInsuranceCd='CNPOL'">POLL</xsl:when>
                <xsl:when test="SubjectInsuranceCd='POTOP'">PPO</xsl:when>
                <xsl:when test="SubjectInsuranceCd='SPOIL'">SPOIL</xsl:when>
                <xsl:when test="SubjectInsuranceCd='STOCK'">STOCK</xsl:when>
                <xsl:when test="SubjectInsuranceCd='VACPR'">VACANT</xsl:when>
                <xsl:otherwise>
				<!-- Issue 59878 Start -->
				<xsl:value-of select="CommlCoverage/CoverageCd"/>
				<!--xsl:message terminate="yes">Unrecognized ACORD Coverage Code:<xsl:value-of select="CommlCoverage/CoverageCd"></xsl:value-of>. The context is:<xsl:value-of select="name()"></xsl:value-of>.</xsl:message-->
				<!-- Issue 59878 End -->
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='GL'">
              <xsl:choose>
                <xsl:when test="CommlCoverage/CoverageCd='CCCLL'">CCC</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='DOCON'">CONDO</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EBL'">EBL</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='PROF'">PROF</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='VOLPD'">VOLPD</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='PREM'">PREMOP</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='POLUT'">POLL</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='OCP'">OCP</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='LIQUR'">LIQ</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='RRPRL'">RRPROT</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='ADDIN'">ADDINS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EEI'">ELEVTR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_LW'">LSDWKR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='PCO'">PRODCO</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_UGTANK'">UNDTNK</xsl:when>
                <xsl:otherwise>
					<!-- Issue 59878 Start -->
					<xsl:value-of select="CommlCoverage/CoverageCd"/>
					<!--xsl:message terminate="yes">Unrecognized ACORD Coverage Code:<xsl:value-of select="CommlCoverage/CoverageCd"></xsl:value-of>. The context is:<xsl:value-of select="name()"></xsl:value-of>.</xsl:message-->
					<!-- Issue 59878 End -->
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
              <xsl:choose>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_ARB'">ARBITR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_CLPROP'">CLIENT</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EMPTHFT'">EMDISC</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='EMPDH'">EMPDIS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FORMP'">EMPTPE</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FORMO'">EMPTPL</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EXTR'">EXTORT</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='CCF'">FORGRY</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_FRAUD'">FRAUD</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_FTF'">FUNDS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FORMK'">GUEST1</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FORML'">GUEST2</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_ITOP'">INSOTH</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_IRBOP'">INSRBO</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FORMQ'">INSRMS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_IRSBOP'">INSRSB</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_ITMS'">INSTMS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SDBLTDDS'">LESSE1</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SDBLRBOP'">LESSE2</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='OS'">OUTSDE</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_MOCM'">PAPER</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SDCPL'">SAFE1A</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SDRBCPPD'">SAFE1B</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SECDFI'">SEC1A</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_SECDPO'">SEC1B</xsl:when>
                <xsl:otherwise>
					<!-- Issue 59878 Start -->
					<xsl:value-of select="CommlCoverage/CoverageCd"/>
					<!--                  <xsl:message terminate="yes">Unrecognized ACORD Coverage Code: 
  <xsl:value-of select="CommlCoverage/CoverageCd"></xsl:value-of>. The context is: 
  <xsl:value-of select="name()"></xsl:value-of>.</xsl:message>
-->
				<!-- Issue 59878 End -->
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='IMC'">
              <xsl:choose>
                <xsl:when test="CommlCoverage/CoverageCd='ACCTS'">ACCTS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='BAILE'">BAILEE</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='BOAT'">BOAT</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='BRWOR'">BR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='FINEA'">COMMLA</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='CAMRA'">COMMLC</xsl:when>
				<xsl:when test="CommlCoverage/CoverageCd='COMMLC'">COMMLC</xsl:when> <!-- Issue 59878 -->
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_ORGAN'">COMMLO</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='MUSFL'">COMMLM</xsl:when>
				<xsl:when test="CommlCoverage/CoverageCd='COMMLM'">COMMLM</xsl:when> <!-- Issue 59878 -->
                <xsl:when test="CommlCoverage/CoverageCd='EDPEQ'">EDP</xsl:when>
				<xsl:when test="CommlCoverage/CoverageCd='CNTREQ'">CNTREQ</xsl:when> <!-- Issue 59878 -->
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EQDOP'">EQPDO</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='EQDFL'">EQPDP</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='com.csc_EQDS'">EQPDS</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='EQUFL'">EQUIP</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='PHYS'">PHYSUR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='RDOTV'">RDOTV</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='SIGN'">SIGN</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='THEATR'">THEATR</xsl:when>
                <xsl:when test="CommlCoverage/CoverageCd='PAPER'">VALPAP</xsl:when>
				<xsl:when test="CommlCoverage/CoverageCd='VALPAP'">VALPAP</xsl:when> <!-- Issue 59878 -->
                <xsl:otherwise>
					<!-- Issue 59878 Start -->
							<xsl:value-of select="CommlCoverage/CoverageCd"/>
						<!--                  <xsl:message terminate="yes">Unrecognized ACORD Coverage Code: 
      <xsl:value-of select="CommlCoverage/CoverageCd"></xsl:value-of>. The context is: 
      <xsl:value-of select="name()"></xsl:value-of>.</xsl:message>
-->
					<!-- Issue 59878 End -->
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='BOP'">
              <xsl:choose>
                <xsl:when test="CoverageCd='PRFDO'">PRFD&amp;O</xsl:when>
                <xsl:when test="CoverageCd='PRFEO'">PRFE&amp;O</xsl:when>
                <xsl:when test="CoverageCd='MSOFF'">M&amp;SOFF</xsl:when>
                <xsl:when test="CoverageCd='MSPRM'">M&amp;SPRM</xsl:when>
                <!-- Glass Coverage Codes -->
                <xsl:when test="ScheduleTypeCd='EGLASS'">EGLASS</xsl:when>
                <xsl:when test="ScheduleTypeCd='IGLASS'">IGLASS</xsl:when>
                <xsl:when test="ScheduleTypeCd='SIGNS'">SIGNS</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="CoverageCd" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <BYAACD>
          <xsl:value-of select="$LOC" />
        </BYAACD>
        <BYABCD>
          <xsl:value-of select="$MCO" />
        </BYABCD>
        <BYARTX>
          <xsl:value-of select="$SYM" />
        </BYARTX>
        <BYASTX>
          <xsl:value-of select="$POL" />
        </BYASTX>
        <BYADNB>
          <xsl:value-of select="$MOD" />
        </BYADNB>
        <BYAGTX>
          <xsl:value-of select="$InsLine" />
        </BYAGTX>
        <BYBRNB>
          <xsl:value-of select="$Location" />
        </BYBRNB>
        <BYEGNB>
          <xsl:choose>
            <xsl:when test="string-length($SubLocation)">
              <xsl:value-of select="$SubLocation" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'0'" />
            </xsl:otherwise>
          </xsl:choose>
        </BYEGNB>
        <BYANTX>
          <xsl:value-of select="$InsLine" />
        </BYANTX>
      <!--  <BYAENB>-->
	<!-- Issue 59878 - Start -->
		<!--<xsl:choose>
			<xsl:when test="$InsLine='CA' and $CovCode = 'UMPD'">
				<xsl:value-of select="../CommlRateState/CommlVeh[@id=concat('v',$UnitNbr)]/ItemIdInfo/InsurerId"/>
		        </xsl:when>
			<xsl:when test="$InsLine='CA' and $CovCode = 'UN'">
				<xsl:value-of select="../CommlVeh[@id=concat('v',$UnitNbr)]/ItemIdInfo/InsurerId" />
			</xsl:when>
			<xsl:when test="$InsLine='CA' and $CovCode = 'UM'">
				<xsl:value-of select="../CommlRateState/CommlVeh[@id=concat('v',$UnitNbr)]/ItemIdInfo/InsurerId" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="../ItemIdInfo/InsurerId"/>
			</xsl:otherwise>
		</xsl:choose>-->
	<!-- Issue 59878 - Ends -->
      <!--  </BYAENB>-->
        <BYAENB>
          <xsl:choose>
            <xsl:when test="$InsLine='CA'">			 
              <!--<xsl:value-of select="../ItemIdInfo/InsurerId" />-->
			  <xsl:value-of select="$UnitNbr" />
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </BYAENB>
        <BYAOTX>
          <xsl:value-of select="$CovCode" />
        </BYAOTX>
        <BYC0NB>
			<!-- Issue 59878 - Start -->
          <!--xsl:choose>
			<xsl:when test="IterationNumber">
              <xsl:value-of select="IterationNumber" />
            </xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
	          </xsl:choose-->
          <xsl:choose>
				<xsl:when test="$CovCode = 'RENTAL'">
					<xsl:value-of select="$IterationNbr" />
				</xsl:when>
				<xsl:when test="$SeqNbr!=0">
					<xsl:value-of select="$SeqNbr"/>	
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="number(normalize-space(CommlCoverage/IterationNumber))">
							<xsl:value-of select="normalize-space(CommlCoverage/IterationNumber)" />
						</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Issue 59878 - End -->
        </BYC0NB>
        <BYC6ST>P</BYC6ST>
        <xsl:choose>
          <xsl:when test="$InsLine='GL'">
            <xsl:call-template name="BuildGLCoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$InsLine='CR'">
            <xsl:call-template name="BuildCRCoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$InsLine='IMC'">
            <xsl:call-template name="BuildIMCCoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$InsLine='CF'">
            <xsl:call-template name="BuildCFCoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
			  <!--Issue 102807 starts-->
				<xsl:with-param name="AlarmClassCd" select="$AlarmClassCd"/>	
				<xsl:with-param name="WatchmanCd" select="$WatchmanCd"/>	
				<xsl:with-param name="AlarmConnectionCd" select="$AlarmConnectionCd"/>	
				<!--Issue 102807 ends-->
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$InsLine='CA'">
            <xsl:call-template name="BuildCACoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
			  <xsl:with-param name="UnitNbr" select="$UnitNbr" />	<!-- Issue 59878 -->
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$InsLine='BOP'">
            <xsl:call-template name="BuildBOCoverage">
              <xsl:with-param name="Location" select="$Location" />
              <xsl:with-param name="SubLocation" select="$SubLocation" />
              <xsl:with-param name="InsLine" select="$InsLine" />
              <xsl:with-param name="RateState" select="$RateState" />
              <xsl:with-param name="CovCode" select="$CovCode" />
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
<!-- Issue '69073 begin -->
	<BYAL4LTXT><xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExclusionOption1']/OptionValue" /></BYAL4LTXT>
<!--Issue '69073 end -->
      </ASBYCPL1__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
  <xsl:template name="BuildGLCoverage">
    <xsl:param name="Location">0</xsl:param>
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
    <xsl:variable name="PCOExcluded" select="CommlCoverage/Option[OptionCd='com.csc_PCO']/OptionValue" />
    <xsl:variable name="PCONode" select="parent::node()/GeneralLiabilityClassification[CommlCoverage/CoverageCd='PCO']" />
    <BYAFNB>0</BYAFNB>

    <BYTYPE0ACT>
	<!-- 34771 start -->
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
	<!-- 34771 end -->
     </BYTYPE0ACT>
   <xsl:variable name="CovType">
      <xsl:choose>
		<!-- Issue 59878 Start -->
		<!--xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd='1'">0</xsl:when-->
		<xsl:when test="$CovCode='CCC' or $CovCode='CONDO' or $CovCode='EBL' or $CovCode='PROF' or $CovCode='VOLPD'">0I</xsl:when>
        <xsl:when test="$CovCode='AIBLT' or $CovCode='SUPPD' or $CovCode='BLKWVR' or $CovCode='SPCWVR'">0I</xsl:when>
        <xsl:otherwise></xsl:otherwise>
			<!-- Issue 59878 Start -->
      </xsl:choose>
    </xsl:variable>
    <BYD3ST>
<!--Issue 59878 begins-->
	<xsl:choose>
		<xsl:when test= "$CovCode = 'ADDINS' and CommlCoverage/CurrentTermAmt[Amt='']"></xsl:when>
		<xsl:when test= "$CovCode = 'ADDINS' and CommlCoverage/CurrentTermAmt[Amt!=0]">0</xsl:when>
		<xsl:otherwise>
				<xsl:value-of select="$CovType" />
			</xsl:otherwise>
		</xsl:choose>      
<!--Issue 59878 ends-->

    </BYD3ST>
    <BYA3VA>
      <xsl:value-of select="translate(CommlCoverage/CurrentTermAmt/Amt,'$,,','')" />
    </BYA3VA>
    <BYBRVA>
      <xsl:value-of select="translate(com.csc_VendorPremiumAmt/Amt,'$,,','')" />
    </BYBRVA>
    <xsl:variable name="CovLevel">
      <xsl:choose>
        <xsl:when test="$CovCode='CCC' or $CovCode='CONDO' or $CovCode='EBL' or $CovCode='PROF' or $CovCode='VOLPD'">0I</xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYEETX>
      <xsl:value-of select="$CovLevel" />
    </BYEETX>
    <BYNDTX>
		<!--59878 starts-->
		<!--<xsl:value-of select="substring-before(ClassCd,'-')" />-->
		<xsl:value-of select="ClassCd"/>
		<!--59878 ends-->
    </BYNDTX>
    <BYKKTX>
      <xsl:value-of select="ClassCdDesc" />
    </BYKKTX>
    <BYBCCD>
      <xsl:value-of select="$RateState" />
    </BYBCCD>
    <BYAGNB>
      <xsl:value-of select="TerritoryCd" />
    </BYAGNB>
    <BYA5VA>
<!--Issue 59878 begins-->
		<xsl:choose>
			<xsl:when test="$CovCode='ADDINS'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_NumVendors']/OptionValue"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translate(Exposure,'$,,','')" />
			</xsl:otherwise>
		</xsl:choose>
<!--Issue 59878 ends-->

    </BYA5VA>
    <xsl:variable name="BYCZST">
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ARated']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='CONDO'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_DIR']/OptionValue"></xsl:value-of>
        </xsl:when>
			<!--59878 starts-->
			<xsl:when test="$CovCode='EBL'">N</xsl:when>		
    		<xsl:when test="CommlCoverage/CoverageCd='PLBARB'">B</xsl:when>
    		<xsl:when test="CommlCoverage/CoverageCd='PLBEAU'">P</xsl:when>
    		<xsl:when test="CommlCoverage/CoverageCd='PLPRIN'">E</xsl:when>
			<!--59878 ends--> 
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYCZST>
      <xsl:value-of select="$BYCZST" />
    </BYCZST>
    <BYC0ST>
      <xsl:value-of select="com.csc_IncreasedLimitTableAssignmentCd" />
    </BYC0ST>
    <BYC1ST>
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">

				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ProdIncrLmtFctr']/OptionValue" /><!--Issue 59878 -->
        </xsl:when>
        <xsl:when test="$CovCode='LIQ'">
<!--Issue 59878 begins-->

	  			<xsl:choose>
					<xsl:when test="CommlCoverage/Option[OptionCd='com.csc_IncLmtCd']/OptionValue">
				  		<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_IncLmtCd']/OptionValue" />
					</xsl:when>
					<xsl:otherwise>C</xsl:otherwise>	
	  			</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
<!--Issue 59878 ends-->

      </xsl:choose>
    </BYC1ST>
    <BYC2ST>
      <xsl:value-of select="com.csc_ExposureBasisCd" />
    </BYC2ST>
    <xsl:if test="$PCOExcluded='N'">
      <BYIYTX>
        <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ARated']/OptionValue" />
      </BYIYTX>
    </xsl:if>
    <BYIZTX>
      <xsl:value-of select="PremiumBasisCd" />
    </BYIZTX>
    <xsl:variable name="BYI0TX">
      <xsl:choose>
<!--Issue 59878 begins-->

		<xsl:when test="$CovCode='PREMOP'">
  			<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExcludeProductsInd']/OptionValue" />
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>	
<!--Issue 59878 ends-->

      </xsl:choose>
    </xsl:variable>
    <BYI0TX>
      <xsl:value-of select="$BYI0TX" />
    </BYI0TX>
	<!-- Issue 59878 Start -->
    <xsl:variable name="BYI2TX">
	  <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
		     <xsl:choose>
	    		<xsl:when test="$RateState='OH' or $RateState='ND' or $RateState='WA' or $RateState='WY'">
	      			<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_EmpLiabStopGapInd']/OptionValue" />
	    		</xsl:when>
	    		<xsl:otherwise>N</xsl:otherwise>	
      		</xsl:choose>	
    	</xsl:when>
        <xsl:otherwise></xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>
	<BYI2TX>
	  <xsl:value-of select="$BYI2TX" />
    </BYI2TX>
	<!-- Issue 59878 End -->
    <BYI4TX>
	<!--59878 starts-->
	<!--<xsl:value-of select="com.csc_CoverageTypeCd" />-->
	<xsl:choose>
		<xsl:when test="$CovCode='PREMOP'">
			<xsl:value-of select="CommlCoverage[CoverageCd='PREMOP']/Option[OptionCd='com.csc_AddlInsuredType']/OptionValue"/>
		</xsl:when>
		<xsl:when test="$CovCode='ADDINS'">
			<!--<xsl:value-of select="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd']/OptionValue"/>-->
			<!--<xsl:value-of select="concat(CommlCoverage/CoverageCd,'')"/>-->
			<xsl:choose>
		    		<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='1A']">A</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='3A']">K</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='4A']">G</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='A1']">C</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='A8']">M</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='C']">D</xsl:when>
				<xsl:when test="CommlCoverage[CoverageCd='ADDINS']/Option[OptionCd='com.csc_CoverageTypeCd'][OptionValue='K']">E</xsl:when>
		    		<xsl:otherwise>N</xsl:otherwise>	
      			</xsl:choose>	
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="com.csc_CoverageTypeCd"/>
		</xsl:otherwise>
	</xsl:choose>
	<!--59878 ends-->
    </BYI4TX>
    <xsl:variable name="BYAKCD">
      <xsl:choose>
        <xsl:when test="$CovCode='CCC'">325</xsl:when>
        <xsl:when test="$CovCode='CONDO'">325</xsl:when>
        <xsl:when test="$CovCode='EBL'">325</xsl:when>
        <xsl:when test="$CovCode='PROF'">325</xsl:when>
        <xsl:when test="$CovCode='VOLPD'">325</xsl:when>
        <xsl:when test="$CovCode='LIQ'">332</xsl:when>
        <xsl:when test="$CovCode='PREMOP'">334</xsl:when>
        <xsl:when test="$CovCode='OCP'">335</xsl:when>
        <xsl:when test="$CovCode='RRPROT'">335</xsl:when>
        <xsl:when test="$CovCode='PRODCO'">336</xsl:when>
        <xsl:when test="$CovCode='POLL'">350</xsl:when>
        <xsl:when test="$CovCode='EMPLL'">325</xsl:when><!--UNI01126--><!--Issue 59878 -->
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYAKCD>
      <xsl:value-of select="$BYAKCD" />
    </BYAKCD>
    <BYALCD>
	<!-- Issue 59878 Start -->
  	<xsl:choose>
    	<xsl:when test="$CovCode='PREMOP'">NA</xsl:when>
			<xsl:when test="$CovCode='GKCOLL' or $CovCode='GKOTC'">
			<xsl:choose>				
				<xsl:when test="CommlCoverage/Option[OptionCd='com.csc_GarageCovType']/OptionValue = 'DIREXC'">LEGAL</xsl:when>
				<xsl:otherwise>
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_GarageCovType']/OptionValue"/>
				</xsl:otherwise>
			</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			<!-- Issue 59878 End -->
		     	<xsl:value-of select="com.csc_DeletedExclusionsCd" />
			<!-- Issue 59878 Start -->
			</xsl:otherwise>
		</xsl:choose>
		<!-- Issue 59878 End -->
    </BYALCD>
    <BYAMCD>
      <xsl:value-of select="com.csc_PMACd" />
    </BYAMCD>
    <xsl:variable name="BYANCD">
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
          <xsl:choose>
            <xsl:when test="CommlCoverage/Deductible/DeductibleTypeCd='PD' or CommlCoverage/Deductible/DeductibleTypeCd='PDPC' or CommlCoverage/Deductible/DeductibleTypeCd='BIPD' or CommlCoverage/Deductible/DeductibleTypeCd='BPPC'">
              <xsl:choose>
                <xsl:when test="string-length(translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,' ','')) &gt; 0">
                  <xsl:value-of select="translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$CovCode='RRPROT'">
          <xsl:value-of select="com.csc_RateCd"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYANCD>
      <xsl:value-of select="$BYANCD" />
    </BYANCD>
    <xsl:variable name="BYUSCD5">
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
<!--Issue 59878 begins-->

			<xsl:value-of select="CommlCoverage/Deductible[DeductibleTypeCd='com.csc_ProdPD']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<xsl:when test="$CovCode='GKCOLL' or $CovCode='GKOTC' or $CovCode='LIQ'"></xsl:when>
<!--Issue 59878 ends-->

        <xsl:otherwise>
          <xsl:value-of select="translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSCD5>
      <xsl:value-of select="$BYUSCD5" />
    </BYUSCD5>
    <BYPLTX>
      <xsl:value-of select="$PCONode/CommlCoverage/Deductible/DeductibleTypeCd" />
    </BYPLTX>
	<!-- Issue 59878 Start -->
	<BYPMTX>
		<xsl:choose>
			<xsl:when test="$CovCode='PREMOP'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_PremRatingType']/OptionValue"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</BYPMTX>
	<!-- Issue 59878 End -->
    <xsl:variable name="BYPNTX">
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
          <xsl:choose>
            <xsl:when test="CommlCoverage/Deductible/DeductibleTypeCd='BI' or CommlCoverage/Deductible/DeductibleTypeCd='BIPC' or CommlCoverage/Deductible/DeductibleTypeCd='BIPD' or CommlCoverage/Deductible/DeductibleTypeCd='BPPC'">
              <xsl:choose>
                <xsl:when test="string-length(translate($PCONode/CommlCoverage/Deductible/FormatCurrencyAmt/Amt,' ','')) &gt; 0">
                  <xsl:value-of select="translate($PCONode/CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYPNTX>
      <xsl:value-of select="$BYPNTX" />
    </BYPNTX>
    <xsl:variable name="BYPOTX">
      <xsl:choose>
        <!-- Case 29405 Begin -->
        <xsl:when test="$InsLine='CA'">
          <xsl:value-of select="CommlVehSupplement/ZoneCombinationCd"></xsl:value-of>
        </xsl:when>
        <!-- Case 29405 End -->
        <xsl:when test="$CovCode='PREMOP'">
<!--Issue 59878 begins-->

			<xsl:choose>
				<xsl:when test="CommlCoverage[CoverageCd='PREMOP']/Option[OptionCd='com.cscDedTypeCd']/OptionValue">
					<xsl:value-of select="CommlCoverage[CoverageCd='PREMOP']/Option[OptionCd='com.cscDedTypeCd']/OptionValue"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
<!--Issue 59878 ends-->

        </xsl:when>
        <xsl:when test="$CovCode='LSDWKR'">
          <xsl:value-of select="CommlCoverage/Form/FormName"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYPOTX>
      <xsl:value-of select="$BYPOTX" />
    </BYPOTX>
    <xsl:variable name="BYPPTX">
      <xsl:choose>
        <xsl:when test="$CovCode='PREMOP'">
          <xsl:choose>
<!--Issue 59878 begins-->

				<xsl:when test="CommlCoverage[CoverageCd='PREMOP']/Deductible[DeductibleTypeCd='BodInj']/FormatCurrencyAmt/Amt">
					<xsl:value-of select="CommlCoverage[CoverageCd='PREMOP']/Deductible[DeductibleTypeCd='BodInj']/FormatCurrencyAmt/Amt"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
		<xsl:when test="$CovCode='EBL' or $CovCode='LIQ'">
			<xsl:value-of select="CommlCoverage/Deductible/FormatCurrencyAmt/Amt"/>
		</xsl:when>
        <xsl:when test="$CovCode='GKCOLL'">
			<xsl:value-of select="CommlCoverage/Deductible[DeductibleTypeCd='com.csc_DedAmtColl']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<xsl:when test="$CovCode='GKOTC'">
			<xsl:value-of select="CommlCoverage/Deductible[DeductibleTypeCd='com.csc_DedAmtOTC']/FormatCurrencyAmt/Amt"/>
<!--Issue 59878 ends-->

        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYPPTX>
      <xsl:value-of select="$BYPPTX" />
    </BYPPTX>
    <BYAEPC>
      <xsl:value-of select="com.csc_BaseRate" />
    </BYAEPC>
    <BYUSFT3>
      <xsl:value-of select="$PCONode/com.csc_BaseRate" />
    </BYUSFT3>
    <BYB5PC>
      <xsl:value-of select="com.csc_FinalRatePct" />
    </BYB5PC>
    <xsl:variable name="BYBKNB">
      <xsl:choose>
        <xsl:when test="$CovCode='UNDTNK'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_NumTanks']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="com.csc_NumVendors"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYBKNB>
      <xsl:value-of select="$BYBKNB" />
    </BYBKNB>
    <BYAGVA>
      <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYAGVA>
    <BYAHVA>
      <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='Aggregate']/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYAHVA>
		<!-- Issue 59878 Start -->
	<BYUSVA3>
		<xsl:choose>
			<xsl:when test="$CovCode='PROF' or $CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='UT' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerPerson']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<!--<xsl:otherwise>
					    <xsl:value-of select="translate(../../LiabilityInfo/CommlCoverage[CoverageCd='EAOCC']/Limit/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
					</xsl:otherwise>-->			<!--Issue 59878 - Commented this code-->
					<xsl:otherwise>0</xsl:otherwise>	<!--Issue 59878-->
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$CovCode='SUPPD'">
			    <xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='']/FormatCurrencyAmt/Amt"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='GKCOLL' or $CovCode='GKOTC'">
				<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_GarageLiabLmt']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<!-- UNI01126 Stop Gap - Start -->
			<xsl:when test="$CovCode='EMPLL'">
				<xsl:variable name="occlimit">
					<xsl:value-of select="substring(CommlCoverage/Option[OptionCd='com.csc_EmpLiabStopGapInd']/OptionValue,1,1)"/>
				</xsl:variable>
				<xsl:value-of select="$occlimit*100000">
				</xsl:value-of>
			</xsl:when>
			<!-- UNI01126 Stop Gap - - End -->
	  	</xsl:choose>	
	</BYUSVA3>
	<BYUSVA4>
		<xsl:choose>
			<xsl:when test="$CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='UT'">
						<!--<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_PerCommonCauseLmtAmt']/FormatCurrencyAmt/Amt,'$,,','')" />-->  <!--Issue 59878-->
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_PerCommonCauseLmt']/FormatCurrencyAmt/Amt,'$,,','')" />			<!--Issue 59878-->
					</xsl:when>
					<!--<xsl:otherwise>
					    <xsl:value-of select="translate(../../LiabilityInfo/CommlCoverage[CoverageCd='PRDCO']/Limit/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
					</xsl:otherwise>-->		<!--Issue 59878 - Commented this code-->
					<xsl:otherwise>0</xsl:otherwise>	<!--Issue 59878-->
				</xsl:choose>
			</xsl:when>
			<!-- UNI01126 Stop Gap - Start -->
			<xsl:when test="$CovCode='EMPLL'">
				<xsl:variable name="agglimit">
					<xsl:value-of select="substring(CommlCoverage/Option[OptionCd='com.csc_EmpLiabStopGapInd']/OptionValue,3,1)"/>
				</xsl:variable>
				<xsl:value-of select="$agglimit*100000"></xsl:value-of>
			</xsl:when>
			<!-- UNI01126 Stop Gap - - End -->
		</xsl:choose>	
	</BYUSVA4>
	<BYUSVA5>
		<xsl:choose>
			<xsl:when test="$CovCode='PROF' or $CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='UT' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='Aggregate']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<xsl:when test="$RateState='IL' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='Aggregate']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<!--<xsl:otherwise>
					    <xsl:value-of select="translate(../../LiabilityInfo/CommlCoverage[CoverageCd='GENAG']/Limit/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
					</xsl:otherwise>-->		<!--Issue 59878 - Commented this code-->
					<xsl:otherwise>0</xsl:otherwise>	<!--Issue 59878-->
				</xsl:choose>
			</xsl:when>
			<!-- UNI01126 Stop Gap - Start -->
			<xsl:when test="$CovCode='EMPLL'">
				<xsl:variable name="perslimit">
					<xsl:value-of select="substring(CommlCoverage/Option[OptionCd='com.csc_EmpLiabStopGapInd']/OptionValue,2,1)"/>
				</xsl:variable>
				<xsl:value-of select="$perslimit*100000"></xsl:value-of>
			</xsl:when>
			<!-- UNI01126 Stop Gap - - End -->
		</xsl:choose>	
	</BYUSVA5>
	<!--Issue 59878 start-->
	<BYCXVA>
		<xsl:choose>
			<xsl:when test="$CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='IL' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerPersonBI']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<xsl:otherwise>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>	
	</BYCXVA>
	<BYCYVA>
		<xsl:choose>
			<xsl:when test="$CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='IL' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerPersonPD']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<xsl:otherwise>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>	
	</BYCYVA>
	<BYCZVA>
		<xsl:choose>
			<xsl:when test="$CovCode='LIQ'">
				<xsl:choose>
					<xsl:when test="$RateState='IL' and $CovCode='LIQ'">
						<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_PerCommonCauseLmt']/FormatCurrencyAmt/Amt,'$,,','')" />
					</xsl:when>
					<xsl:otherwise>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>	
	</BYCZVA>
	<!--Issue 59878 end-->
	<BYUSIN30>
    	<xsl:choose>
			<xsl:when test="$CovCode='OCP' or $CovCode='PREMOP'">
    	  		<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_MoldInd']/OptionValue" />
    		</xsl:when>
    		<xsl:when test="$CovCode='AIBLT' and CommlCoverage/CoverageCd='BADDCA'">C</xsl:when>
    		<xsl:when test="$CovCode='AIBLT' and CommlCoverage/CoverageCd='BADDLA'">L</xsl:when>
    		<xsl:otherwise></xsl:otherwise>	
	  	</xsl:choose>	
    </BYUSIN30>
	<BYEICD>
		<xsl:choose>
			<xsl:when test="$CovCode='OCP'">
				<xsl:value-of select="TerritoryCd" />
			</xsl:when>
		</xsl:choose>	
	</BYEICD>
	<BYEGCD>
		<xsl:choose>
			<xsl:when test="$CovCode='PREMOP'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ProdRatingType']/OptionValue"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</BYEGCD>
    <BYEICD>
		<xsl:choose>
			<!-- UNI01126 Stop Gap - Start -->
			<xsl:when test="$CovCode='EMPLL'">
				<xsl:value-of select="TerritoryCd" />
			</xsl:when>
			<!-- UNI01126 Stop Gap - - End -->
		</xsl:choose>	
    </BYEICD>
	<!-- Issue 59878 End -->
  </xsl:template>
  <xsl:template name="BuildIMCCoverage">
    <xsl:param name="Location">0</xsl:param>
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
    <xsl:variable name="Unit" select="/ACORD/InsuranceSvcRq/*/CommlSubLocation[@LocationRef=$Location and @SubLocationRef=$SubLocation]"></xsl:variable>
    <BYAFNB>0</BYAFNB>
    <BYTYPE0ACT>
	<!-- 34771 start -->
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
	<!-- 34771 end -->
     </BYTYPE0ACT>
    <xsl:variable name="CovType">
      <xsl:choose>
        <!--
<xsl:when test="$CovCode='BOAT' or $CovCode='COMMLA' or $CovCode='COMMLO' or $CovCode='EQPDO' or 
                        $CovCode='EQPDP' or $CovCode='EQPDS' or $CovCode='RDOTV' or $CovCode='THEATR'">0</xsl:when>
-->
        <xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd='1'">0</xsl:when>
			<!-- Issue 59878 - Start -->
			<xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd=''and ($CovCode='COMMLC' or $CovCode='COMMLM' or $CovCode='EQUIP')"></xsl:when>
			<xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd=''"></xsl:when>		
			<!-- Issue 59878 - Start -->
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYD3ST>
      <xsl:value-of select="$CovType" />
    </BYD3ST>
    <BYA3VA>
      <xsl:value-of select="translate(CommlCoverage/CurrentTermAmt/Amt,'$,,','')" />
    </BYA3VA>
    <xsl:variable name="BYBRVA">
      <xsl:choose>
        <xsl:when test="$CovCode='BOAT'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='LEGLB']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="translate(DepositAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYBRVA>
      <xsl:value-of select="$BYBRVA" />
    </BYBRVA>
    <xsl:variable name="BYBSVA">
      <xsl:choose>
        <xsl:when test="$CovCode='BOAT'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='WCRFT']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EQPDP'">
          <xsl:value-of select="translate(CreditOrSurcharge[CreditSurchargeCd='APMP']/NumericValue/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYBSVA>
      <xsl:value-of select="$BYBSVA" />
    </BYBSVA>
    <BYBTVA>
      <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_PWD']/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYBTVA>
    <xsl:variable name="CovLevel">
      <xsl:choose>
			<!-- Issue 59878 Start -->
			<!-- Can set Insurance Line Coverage Indicator if location is zero -->
	        <!--<xsl:when test="$CovCode='COMMLC' or $CovCode='COMMLM' or $CovCode='EQUIP'">0I</xsl:when>-->
			<xsl:when test="$Location='0'">0I</xsl:when>
			<!-- Issue 59878 End -->
      </xsl:choose>
    </xsl:variable>
    <BYEETX>
      <xsl:value-of select="$CovLevel" />
    </BYEETX>
    <BYNDTX>
		<!-- Issue 59878 - Start -->
      <!--xsl:value-of select="substring-before(ClassCd,'-')" /-->
	  <xsl:value-of select="ClassCd" />
	  <!-- Issue 59878 - End -->
    </BYNDTX>
    <BYKKTX>
      <xsl:value-of select="ClassCdDesc" />
    </BYKKTX>
    <BYBCCD>
      <xsl:value-of select="$RateState" />
    </BYBCCD>
    <BYAGNB>
      <xsl:value-of select="TerritoryCd" />
    </BYAGNB>
    <xsl:variable name="BYA5VA">
      <xsl:choose>
        <xsl:when test="$CovCode='ACCTS' or $CovCode='VALPAP' or $CovCode='EQPDP'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='OFF']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
<!--Issue 59878 begins-->

	<xsl:when test="$CovCode='COMMLA'">
          <xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_SCH']/FormatCurrencyAmt/Amt"></xsl:value-of>
        </xsl:when>
<!--Issue 59878 ends-->

        <xsl:otherwise>
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA5VA>
      <xsl:value-of select="$BYA5VA" />
    </BYA5VA>
    <xsl:variable name="BYA9NB">
      <xsl:choose>
	  	<!-- Issue 59878 - start - Commented this code-->
        <!--<xsl:when test="$CovCode='RDOTV'">
          <xsl:value-of select="translate(CommlCoverage[CoverageCd='RDOTV']/Deductible[DeductibleTypeCd='com.csc_ExtraExpDed']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>-->
		<!-- Issue 59878 - end -->
		<!-- Issue 59878 - start -->
		<xsl:when test="$CovCode = 'BR'">
			<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<!-- Issue 59878 - end -->
		<!-- Issue 59878 - start -->
		<xsl:when test="$CovCode = 'RDOTV'">
			<xsl:value-of select="CommlCoverage/Deductible[DeductibleTypeCd='com.csc_XEXP']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<!-- Issue 59878 - end -->
			<!-- Issue 59878 - start -->
			<xsl:when test="$CovCode = 'BAILEE'">
 			<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<!-- Issue 59878 - end -->
        <xsl:otherwise>
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_SCH']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA9NB>
      <xsl:value-of select="$BYA9NB" />
    </BYA9NB>
	<!-- Issue 59878 - start -->
	<!--BYBVVA>
		<xsl:value-of select="com.csc_BaseRate"/>
	</BYBVVA-->
	<xsl:variable name="BYBVVA">
	<xsl:choose>
		<xsl:when test="$CovCode = 'CNTREQ' ">
 			<xsl:value-of select="com.csc_BaseRate" />
		</xsl:when>
	</xsl:choose>
   </xsl:variable>
    <BYBVVA><xsl:value-of select="$BYBVVA"/></BYBVVA>
	<!-- Issue 59878 - end -->
    <BYCZST>
      <xsl:value-of select="ReportingPeriodCd" />
    </BYCZST>
    <xsl:variable name="BYC0ST">
      <xsl:choose>
    		<!-- Issue 59878 Start -->
			<!-- Be sure a valid limit is present before setting the scheduled items switch -->
			<!-- <xsl:when test="count(CommlCoverage/Limit[LimitAppliesToCd='com.csc_SCH']) &gt; 0">Y</xsl:when> -->
			<xsl:when test="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_SCH']/FormatCurrencyAmt/Amt,'$,,','') &gt; 0">Y</xsl:when>
			<!-- Issue 59878 End -->
        <xsl:otherwise>N</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYC0ST>
      <xsl:value-of select="$BYC0ST" />
    </BYC0ST>
    <BYC1ST>
      <xsl:choose>
        <xsl:when test="CommlCoverage/Option[OptionCd='com.csc_Reporting']">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_Reporting']/OptionValue" />
        </xsl:when>
        <xsl:otherwise>N</xsl:otherwise>
      </xsl:choose>
    </BYC1ST>
    <BYIYTX>
		<!-- Issue 59878 - start -->
		<!--xsl:value-of select="com.csc_SignPositionCd"/-->
		<xsl:choose>
		<xsl:when test="$CovCode='PHYSUR'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_LmtPropCarriedInd']/OptionValue" />
		</xsl:when>
		<xsl:when test="$CovCode='SIGN'">
		<xsl:value-of select="com.csc_SignPositionCd" />
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
		<!-- Issue 59878 - end -->
    </BYIYTX>
    <BYIZTX>
      <xsl:value-of select="com.csc_ReceptacleClassCd" />
    </BYIZTX>
    <BYI1TX>
      <xsl:value-of select="com.csc_PremiumBaseCd" />
    </BYI1TX>
    <BYUSIN14>
      <xsl:value-of select="com.csc_PayMonthlyInd" />
    </BYUSIN14>
    <BYUSIN20>
      <xsl:value-of select="com.csc_AdjustmentPeriodCd" />
    </BYUSIN20>
	<!-- Issue 59878 - start -->
	<BYUSIN24>
	<xsl:choose>
		<xsl:when test="$CovCode = 'EQPDP'">
			<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_ContentsGroupCd']/OptionValue"></xsl:value-of>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</BYUSIN24>
	<!-- Issue 59878 - end -->
    <xsl:variable name="BYAKCD">
      <xsl:choose>
		<!-- Issue 59878 - start -->
			<!--xsl:when test="$CovCode='RDOTV'">
				<xsl:value-of select="com.csc_TypeTowerCd">
				</xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="CommlCoverage/CommlCoverageSupplement/CoinsurancePct">
				</xsl:value-of>
			</xsl:otherwise-->
    		<!--<xsl:when test="$CovCode='RDOTV' or $CovCode = 'INSTAL' or $CovCode = 'EDP' or $CovCode = 'CNTREQ' or $CovCode = 'EQPDP'">-->	<!--Issue 59878 - Added EQPDP, Removed RDOTV-->
			<xsl:when test="$CovCode = 'INSTAL' or $CovCode = 'EDP' or $CovCode = 'CNTREQ' or $CovCode = 'EQPDP'">	<!--Issue 59878 - Added EQPDP, Removed RDOTV-->
      		<xsl:value-of select="CommlCoverage/CommlCoverageSupplement/CoinsurancePct"></xsl:value-of>
   			</xsl:when>
			<xsl:when test="CommlCoverage/CommlCoverageSupplement/CoinsurancePct = ''">80</xsl:when>		<!--UNI00012-->
			<!--Issue 59878 start-->
			<xsl:when test="$CovCode='RDOTV'">
      		<xsl:value-of select="com.csc_TypeTowerCd"></xsl:value-of>
   			</xsl:when>
			<!--Issue 59878 end-->
    		<xsl:otherwise>
     		</xsl:otherwise>
			<!-- Issue 59878 - end -->
      </xsl:choose>
    </xsl:variable>
    <BYAKCD>
      <xsl:value-of select="$BYAKCD" />
    </BYAKCD>
    <BYALCD>
		<!-- Issue 59878 - start -->
		<!--xsl:value-of select="com.csc_ArtificalGeneratedCurrentCd"/-->
		<xsl:choose>
		<xsl:when test="$CovCode = 'PHYSUR'">
			<!--Begin CLP00009-->
			<!--<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.unitrin_ArtificialGenCurrDed']/OptionValue"></xsl:value-of>-->
			<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_ArtificialGenCurrDed']/OptionValue"></xsl:value-of>
			<!--End CLP00009-->
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="com.csc_ArtificalGeneratedCurrentCd"/></xsl:otherwise>
		</xsl:choose>  
		<!-- Issue 59878 - end -->   
    </BYALCD>
	<!-- Issue 59878 - start -->
	<!--BYAMCD>
		<xsl:value-of select="com.csc_CoverageTypeCd"/>
	</BYAMCD-->
	 <xsl:variable name="BYAMCD">
		<xsl:choose>
		<xsl:when test="$CovCode = 'COMMLC' or $CovCode = 'COMMLM'">
			<xsl:value-of select="com.csc_CoverageTypeCd"/>
		</xsl:when>
		<xsl:when test="$CovCode = 'PHYSUR'">
			<xsl:choose>
				<xsl:when test="CommlCoverage/Option[OptionCd = 'com.csc_VariousInd']/OptionValue = 'Y'">E</xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
			</xsl:choose> 
		</xsl:when>  
		<xsl:otherwise><xsl:value-of select="com.csc_CoverageTypeCd" /></xsl:otherwise>
		</xsl:choose>     
	</xsl:variable> 
	<BYAMCD><xsl:value-of select="$BYAMCD"/></BYAMCD>
	<!-- Issue 59878 - end -->
    <xsl:variable name="BYANCD">
      <xsl:choose>
        <xsl:when test="$CovCode='SIGN'">
			<!-- Issue 59878 - start -->
				<!--xsl:value-of select="CommlCoverage/Deductible/FormatPct">
				</xsl:value-of-->
				<xsl:value-of select="CommlCoverage/Deductible/FormatCurrencyAmt/Amt"></xsl:value-of>
				<!-- Issue 59878 - end -->
        </xsl:when>
        <xsl:when test="$CovCode='ACCTS'">
          <xsl:value-of select="com.csc_DuplicateRecordsCd"></xsl:value-of>
        </xsl:when>
			<!-- Issue 59878 - start -->
			<xsl:when test="$CovCode='MTC' or $CovCode = 'TRANS' or $CovCode = 'TRIP'">
      			<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_PropertyDescCd']/OptionValue"></xsl:value-of>
   			</xsl:when>
			<!-- Issue 59878 - end -->
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYANCD>
      <xsl:value-of select="$BYANCD" />
    </BYANCD>
    <BYPLTX>
	<!-- Issue 59878 - start -->
		<!--xsl:value-of select="PMACd"/-->
	<xsl:choose>
		<xsl:when test="$CovCode = 'COMMLC' or $CovCode = 'COMMLM' or $CovCode = 'CNTREQ' or $CovCode = 'INSTAL' or $CovCode = 'MISC' or $CovCode = 'MTC' or $CovCode = 'TRANS' or $CovCode = 'TRIP'">
  			<xsl:value-of select="PMACd" />
		</xsl:when>
	</xsl:choose>
	<!-- Issue 59878 - end -->
    </BYPLTX>
    <BYPPTX>
      <xsl:value-of select="translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYPPTX>
    <BYAEPC>
      <xsl:value-of select="com.csc_SpecificGroupIRate" />
    </BYAEPC>
    <xsl:variable name="BYAFPC">
      <xsl:choose>
        <xsl:when test="$CovCode='EQPDP'">
		<!--0-->			<!--Issue 59878 - Commented 0-->
		<xsl:value-of select="com.csc_BaseRate"></xsl:value-of> <!--Issue 59878-->
		</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="com.csc_BaseRate"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYAFPC>
      <xsl:value-of select="$BYAFPC" />
    </BYAFPC>
    <BYBKNB>
      <xsl:value-of select="Height/NumUnits" />
    </BYBKNB>
    <xsl:variable name="BYAGVA">
      <xsl:choose>
		<!-- Issue 59878 - start -->
		<xsl:when test="$CovCode='ACCTS' or $CovCode = 'BAILEE' or $CovCode = 'BR' or $CovCode = 'EQPDP'">
  			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
		</xsl:when>
		<!-- Issue 59878 - end -->
		<!-- Issue 59878 - start -->
		<xsl:when test="$CovCode='INSTAL'">
  			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_TEMPSTOR']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
		</xsl:when>
		<!-- Issue 59878 - end -->
		<!-- Issue 59878 - start -->
		<xsl:when test="$CovCode='TRANS'">
  			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_TOURVEH']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
		</xsl:when>
		<!-- Issue 59878 - end -->
        <xsl:when test="$CovCode='BOAT'">
          <xsl:value-of select="translate(com.csc_SpecificBoatCd,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EQPDO'">
          <xsl:value-of select="translate(CommlCoverage/Limit/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EQPDP'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='INSD']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='RDOTV'">
          <!--<xsl:value-of select="translate(CommlCoverage[CoverageCd='RDOTV']/Limit[LimitAppliesToCd='com.csc_ExtraExpLmt']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>-->	<!--Issue 59878-->
		  <xsl:value-of select="translate(CommlCoverage[CoverageCd='RDOTV']/Limit[LimitAppliesToCd='com.csc_XEXP']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>		  			<!--Issue 59878-->
        </xsl:when>
		<xsl:when test="TRIP">1</xsl:when> <!-- Issue 59878 -->
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYAGVA>
      <xsl:value-of select="$BYAGVA" />
    </BYAGVA>
    <xsl:variable name="BYAHVA">
      <xsl:choose>
		<!-- Issue 59878 - start -->
			<xsl:when test="$CovCode='TRIP'">
			<!--1-->		<!--Issue 59878 - Commented 1-->
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>	<!--Issue 59878-->
			</xsl:when>
			<!--xsl:when test="$CovCode='BAILEE' or $CovCode='BOAT' or $CovCode='BR'"-->
			<xsl:when test = "$CovCode = 'BAILEE' or $CovCode = 'EDP'">
			<!-- Issue 59878 - end -->
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='TRANS']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
		<!--xsl:when test="$CovCode='EQPDP'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='ACLOC']/FormatCurrencyAmt/Amt,'$,,','')">
			</xsl:value-of>
		</xsl:when>
		<xsl:when test="$CovCode='RDOTV'">
			<xsl:value-of select="translate(CommlCoverage[CoverageCd='RDOTV']/Limit[LimitAppliesToCd='com.csc_LossofIncLmt']/FormatCurrencyAmt/Amt,'$,,','')">
			</xsl:value-of>
		</xsl:when-->
		<!--Issue 59878 start-->
		<xsl:when test = "$CovCode = 'TRANS'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_CARRHIRE']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
		<!-- Issue 59878 - end -->
		<!--Issue 59878 start-->
		<xsl:when test = "$CovCode = 'TRIP'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
		<!-- Issue 59878 - end -->
		<!--Issue 59878 start-->
		<xsl:when test = "$CovCode = 'BR'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_TRANS']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
		<!-- Issue 59878 - end -->
		<xsl:otherwise>
			<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_VARIOUS']/FormatCurrencyAmt/Amt"/> <!-- Issue 59878 -->
		</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYAHVA>
      <xsl:value-of select="$BYAHVA" />
    </BYAHVA>
    <xsl:variable name="BYUSVA3">
	<!-- Issue 59878 - start -->
		<!--xsl:choose>
			<xsl:when test="$CovCode='ACCTS' or $CovCode='BR' or $CovCode='EQPDP'">
				<xsl:value-of select="translate(com.csc_AverageValues/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='BAILEE' or $CovCode='BOAT'">
				<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='ACLOC']/FormatCurrencyAmt/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='EDP'">
				<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_DataMedia']/FormatCurrencyAmt/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='RDOTV'">
				<xsl:value-of select="translate(CommlCoverage[CoverageCd='RDOTV']/Deductible[DeductibleTypeCd='com.csc_LossofIncDed']/FormatCurrencyAmt/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose-->
      <xsl:choose>
	<xsl:when test="$CovCode = 'TRIP'">
	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_OTHER']/FormatCurrencyAmt/Amt"/>
	</xsl:when>
	<!--Issue 59878 start-->
	<xsl:when test="$CovCode = 'INSTAL' or $CovCode = 'BR' or $CovCode = 'EQPDP'">
	<xsl:value-of select="com.csc_AverageValues"/>
	</xsl:when>
	<!--Issue 59878 end-->
	<!--Issue 59878 start-->
	<xsl:when test="$CovCode = 'EDP'">
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_DATAM']/FormatCurrencyAmt/Amt"/>
	</xsl:when>
	<!--Issue 59878 end-->
	<!--Issue 59878 start-->
	<xsl:when test="$CovCode = 'RDOTV'">
	<xsl:value-of select="CommlCoverage/Deductible[DeductibleTypeCd='com.csc_LOSSINC']/FormatCurrencyAmt/Amt"/>		
	</xsl:when>
	<!--Issue 59878 end-->
	<xsl:otherwise><xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_AcquiredAverage']/FormatCurrencyAmt/Amt"/></xsl:otherwise>
 	</xsl:choose>
	<!-- Issue 59878 - end -->
    </xsl:variable>
    <BYUSVA3>
      <xsl:value-of select="$BYUSVA3" />
    </BYUSVA3>
	<!-- Issue 59878 - start -->
	<!--xsl:variable name="BYUSVA4">
		<xsl:choose>
			<xsl:when test="$CovCode='BAILEE' or $CovCode='BOAT'">
				<xsl:value-of select="com.csc_PropertyAtOtherLocation">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='BR'">
				<xsl:value-of select="com.csc_TemporaryStorageCd">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='EDP'">
				<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_ExtraExp']/FormatCurrencyAmt/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:when test="$CovCode='EQPDP'">
				<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='TRANS']/FormatCurrencyAmt/Amt,'$,,','')">
				</xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable-->
	<xsl:variable name="BYUSVA4">
	<xsl:choose>
		<xsl:when test="$CovCode = 'BAILEE'">
		<!--<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='OFF']/FormatCurrencyAmt/Amt"/>-->  <!--Issue 59878 -->
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_OTLOC']/FormatCurrencyAmt/Amt"/>	 <!--Issue 59878 -->
		</xsl:when>
		<!--Issue 59878 start-->
		<xsl:when test="$CovCode = 'INSTAL'">
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_TRANS']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<!--Issue 59878 end-->
		<!--Issue 59878 start-->
		<xsl:when test="$CovCode = 'BR'">
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_TEMPSTOR']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<!--Issue 59878 end-->
		<!--Issue 59878 start-->
		<xsl:when test="$CovCode = 'EDP'">
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_XEXP']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<!--Issue 59878 end-->
		<xsl:otherwise>
  			<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='TRANS']/FormatCurrencyAmt/Amt"/>
  		</xsl:otherwise>
  	</xsl:choose>
	</xsl:variable>
	<!-- Issue 59878 - end -->
    <BYUSVA4>
      <xsl:value-of select="$BYUSVA4" />
    </BYUSVA4>
    <xsl:variable name="BYUSVA5">
<!--Issue 59878 begins-->

	<!--xsl:choose>
		<xsl:when test="$CovCode='ACCTS'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_BRNCH']/FormatCurrencyAmt/Amt,'$,,','')">
			</xsl:value-of>
		</xsl:when>
		<xsl:when test="$CovCode='EQPDP'">
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='OS']/FormatCurrencyAmt/Amt,'$,,','')">
			</xsl:value-of>
		</xsl:when>
		<xsl:otherwise-->
			<xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_UNSCH']/FormatCurrencyAmt/Amt,'$,,','')">
			</xsl:value-of>
		<!--/xsl:otherwise>
	</xsl:choose-->
<!--Issue 59878 ends-->

    </xsl:variable>
    <BYUSVA5>
      <xsl:value-of select="$BYUSVA5" />
    </BYUSVA5>
	<!-- Issue 59878 - start -->
	<BYCYVA><xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_Storage']/FormatCurrencyAmt/Amt"/></BYCYVA>
	<BYCXVA><xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_Processing']/FormatCurrencyAmt/Amt"/></BYCXVA>	
	<xsl:variable name="BYB2PC">
	<xsl:choose>
		<xsl:when test="$CovCode = 'CNTREQ'">
			<xsl:value-of select="com.csc_EmployeeToolsRate"/>
		</xsl:when> 
		<xsl:when test="$CovCode = 'RDOTV'">
			<xsl:value-of select="com.csc_LossOfIncomeRate"/>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYB2PC><xsl:value-of select="$BYB2PC"/></BYB2PC>
	<xsl:variable name="BYB3PC">
	<xsl:choose>
		<xsl:when test="$CovCode = 'CNTREQ' or $CovCode = 'EDP'">
			<xsl:value-of select="com.csc_IncomeCovRate"/>
		</xsl:when> 
		<xsl:when test="$CovCode = 'RDOTV'">
			<xsl:value-of select="com.csc_MobileEquipRate"/>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYB3PC><xsl:value-of select="$BYB3PC"/></BYB3PC>
	<xsl:variable name="BYB4PC">
	<xsl:choose>
		<xsl:when test="$CovCode = 'EDP'">
			<xsl:value-of select="com.csc_BaseRate"/>
		</xsl:when> 
		<xsl:when test="$CovCode = 'CNTREQ'">
			<xsl:value-of select="com.csc_EQLeasedRate"/>
		</xsl:when>
			<xsl:when test="$CovCode = 'RDOTV'">
			<xsl:value-of select="com.csc_EquipSoftwareRate"/>
		</xsl:when> 
		<xsl:when test="$CovCode = 'WHLIAB'">
			<xsl:value-of select="com.csc_StorageSoftCostRate"/>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<BYB4PC><xsl:value-of select="$BYB4PC"/></BYB4PC>
		<xsl:variable name="BYB5PC">
	  	<xsl:choose>
	    <xsl:when test="$CovCode='CNTREQ' or $CovCode = 'RDOTV' or $CovCode = 'WHLIAB'">
	      <xsl:value-of select="com.csc_RREIMRate"></xsl:value-of>
	        </xsl:when>
	    <xsl:otherwise></xsl:otherwise>
	  	</xsl:choose>
		</xsl:variable>
		<BYB5PC><xsl:value-of select="$BYB5PC" /></BYB5PC>	
		<xsl:variable name="BYALV0VAL">
	  	<xsl:choose>
	  	<xsl:when test="$CovCode = 'CNTREQ' or $CovCode = 'RDOTV' or $CovCode = 'EQPDP'">
	  	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='RREIM']/FormatCurrencyAmt/Amt"/>
	  	</xsl:when>
		<xsl:when test="$CovCode = 'MTC'">
	  	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_GROSS']/FormatCurrencyAmt/Amt"/>
	  	</xsl:when>
		<xsl:when test="$CovCode = 'WHLIAB'">
      	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_IncomeVehMobile']/FormatCurrencyAmt/Amt"/>
	  	</xsl:when>
	  	</xsl:choose>
   	 	</xsl:variable>
		<BYALV0VAL><xsl:value-of select="$BYALV0VAL" /></BYALV0VAL>	
		<xsl:variable name="BYALV1VAL">
		<xsl:choose>
		<xsl:when test="$CovCode = 'EDP' or $CovCode = 'TRANS'">
		<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_OTHER']/FormatCurrencyAmt/Amt"/>
		</xsl:when>
		<xsl:otherwise>
      	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_EmpTools']/FormatCurrencyAmt/Amt"/>
	  	</xsl:otherwise>
	  	</xsl:choose>
    	</xsl:variable>
		<BYALV1VAL><xsl:value-of select="$BYALV1VAL" /></BYALV1VAL>
		<xsl:variable name="BYALV2VAL">
      	<xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_IncomeVehMobile']/FormatCurrencyAmt/Amt"/>
    	</xsl:variable>
		<BYALV2VAL><xsl:value-of select="$BYALV2VAL" /></BYALV2VAL>		
		<xsl:variable name="BYALV3VAL">
		<xsl:choose>
	  	<xsl:when test="$CovCode='INSTAL'">
          <xsl:value-of select="com.csc_GrossInstallationReceipts"></xsl:value-of>
        </xsl:when>
		<xsl:otherwise><xsl:value-of select="CommlCoverage/Limit[LimitAppliesToCd='com.csc_MISC']/FormatCurrencyAmt/Amt"/></xsl:otherwise>
		</xsl:choose>
	 	</xsl:variable>
		<BYALV3VAL><xsl:value-of select="$BYALV3VAL" /></BYALV3VAL>	
		<xsl:variable name="BYAL1UCDE">
      	<xsl:choose>
        <xsl:when test="$CovCode='EDP'">
          <xsl:value-of select="com.csc_CoverageTypeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='RDOTV'">
			<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_TerrainHtCd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
		<BYAL1UCDE><xsl:value-of select="$BYAL1UCDE"/></BYAL1UCDE>
		<xsl:variable name="BYALDIPCT">
      	<xsl:choose>
        <xsl:when test="$CovCode='BAILEE'">
          <xsl:value-of select="com.csc_StorageSoftCostRate"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='BR'">
          <xsl:value-of select="com.csc_StorageSoftCostRate"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      	</xsl:choose>
    	</xsl:variable>
		<BYALDIPCT><xsl:value-of select="$BYALDIPCT"/></BYALDIPCT>
		<xsl:variable name="BYAL1WCDE">
      	<xsl:choose>
        <xsl:when test="$CovCode='CNTREQ'">
          <xsl:value-of select="com.csc_ValuationCd"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='EDP'">
          <xsl:value-of select="com.csc_ValuationCd"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='RDOTV'">
        	  <xsl:value-of select="com.csc_ValuationCd"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='INSTAL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_AverageLengthOfJob']/OptionValue"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='MTC'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_RefrigerationDed']/OptionValue"></xsl:value-of>
        </xsl:when>
		<xsl:when test="$CovCode='BR'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_WaitingPeriodCd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      	</xsl:choose>
    	</xsl:variable>
		<BYAL1WCDE>
		<xsl:value-of select="$BYAL1WCDE" />
	</BYAL1WCDE>
	<xsl:variable name="BYA5DT">
      <xsl:choose>
	<xsl:when test="$CovCode='TRIP'">1<xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_StartShipDt']/OptionValue,9,2)"/><xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_StartShipDt']/OptionValue,1,2)"/><xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_StartShipDt']/OptionValue,4,2)"/>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYA5DT><xsl:value-of select="$BYA5DT"/></BYA5DT>
	<xsl:variable name="BYA6DT">
	<xsl:choose>
	<xsl:when test="$CovCode='TRIP'">1<xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_EndShipDt']/OptionValue,9,2)"/><xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_EndShipDt']/OptionValue,1,2)"/><xsl:value-of select="substring(CommlCoverage/Option[OptionCd = 'com.csc_EndShipDt']/OptionValue,4,2)"/>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYA6DT><xsl:value-of select="$BYA6DT"/></BYA6DT>
	<xsl:variable name="BYAL1SCDE">
	<xsl:choose>
	<xsl:when test="$CovCode='RDOTV'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_LOINbrDaysCd']/OptionValue"></xsl:value-of>
    </xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYAL1SCDE><xsl:value-of select="$BYAL1SCDE"/></BYAL1SCDE>
	<xsl:variable name="BYAL1TCDE">
	<xsl:choose>
	<xsl:when test="$CovCode='RDOTV'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_LOINbrDaysCd']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYAL1TCDE><xsl:value-of select="$BYAL1TCDE"/></BYAL1TCDE>
	<xsl:variable name="BYAL1VCDE">
	<xsl:choose>
	<xsl:when test="$CovCode='RDOTV' or $CovCode = 'EDP' or $CovCode = 'TRANS' or $CovCode = 'TRIP'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_VariousCd']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYAL1VCDE><xsl:value-of select="$BYAL1VCDE"/></BYAL1VCDE>	
	<xsl:variable name="BYAL4ITXT">
	<xsl:choose>
	<xsl:when test="$CovCode='PHYSUR'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_ArtificialGenCurrInd']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYAL4ITXT><xsl:value-of select="$BYAL4ITXT"/></BYAL4ITXT>
	<xsl:variable name="BYAL4JTXT">
	</xsl:variable>
	<BYAL4JTXT><xsl:value-of select="$BYAL4JTXT"/></BYAL4JTXT>
	<xsl:variable name="BYAL4KTXT">
	<xsl:choose>
	<xsl:when test="$CovCode='EDP'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_WebSiteInterruptInd']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:when test="$CovCode='BAILEE' or $CovCode = 'FINART' or $CovCode = 'RDOTV' or $CovCode = 'WHLIAB' or $CovCode = 'TRANS'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_NamedPerilsInd']/OptionValue"></xsl:value-of>
	</xsl:when>
		<xsl:when test="$CovCode='MTC' or $CovCode = 'MISC' or $CovCode = 'TRIP'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_NamedPerilsInd']/OptionValue"/>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYAL4KTXT><xsl:value-of select="$BYAL4KTXT"/></BYAL4KTXT>
	<xsl:variable name="BYAL4LTXT">
	<xsl:choose>
	<xsl:when test="$CovCode='EDP' or $CovCode = 'BAILEE' or $CovCode = 'MTC'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_MISCInd']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:when test="$CovCode='FINART' or $CovCode = 'WHLIAB'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_VariousInd']/OptionValue" />
	</xsl:when>  
	<xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	<BYAL4LTXT><xsl:value-of select="$BYAL4LTXT"/></BYAL4LTXT>
	<xsl:variable name="BYALDCPCT">
	<xsl:choose>
	<xsl:when test="$CovCode='WHLIAB' or $CovCode = 'TRANS' or $CovCode='RDOTV' or $CovCode = 'FINART' or $CovCode = 'BAILEE'">
		<xsl:if test="CommlCoverage/Option[OptionCd = 'com.csc_NamedPerilsInd']/OptionValue = 'Y'">1</xsl:if>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYALDCPCT><xsl:value-of select="$BYALDCPCT"/></BYALDCPCT>
	<xsl:variable name="BYALDDPCT">
	<xsl:choose>
	<xsl:when test="$CovCode='WHLIAB' or $CovCode = 'FINART' or $CovCode = 'PHYSUR'">
		<xsl:if test="CommlCoverage/Option[OptionCd = 'com.csc_NamedPerilsInd']/OptionValue = 'Y'">1</xsl:if>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYALDDPCT><xsl:value-of select="$BYALDDPCT"/></BYALDDPCT>
	<xsl:variable name="BYIFNB">
		<xsl:choose>
	<xsl:when test="$CovCode='TRIP'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd = 'com.csc_TripDistance']/OptionValue"></xsl:value-of>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<BYIFNB><xsl:value-of select="$BYIFNB"/></BYIFNB>
	<!-- Issue 59878 - end -->
  </xsl:template>
  <xsl:template name="BuildCRCoverage">
    <xsl:param name="Location">0</xsl:param>
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
    <xsl:variable name="Unit" select="/ACORD/InsuranceSvcRq/*/CommlSubLocation[@LocationRef=$Location and @SubLocationRef=$SubLocation]"></xsl:variable>
    <BYAFNB>0</BYAFNB>
    <BYTYPE0ACT>
	<!-- 34771 start -->
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
	<!-- 34771 end -->
     </BYTYPE0ACT>
    <xsl:variable name="CovType">
      <xsl:choose>
        <xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd='1'">0</xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYD3ST>
      <xsl:value-of select="$CovType" />
    </BYD3ST>
    <BYA3VA>
      <xsl:value-of select="translate(CommlCoverage/CurrentTermAmt/Amt,'$,,','')" />
    </BYA3VA>
    <BYNDTX>
		<!-- 59878 --> <!--<xsl:value-of select="substring-before(ClassCd,'-')"/>-->
		<xsl:value-of select="ClassCd" />
    </BYNDTX>
    <BYKKTX>
      <xsl:value-of select="ClassCdDesc" />
    </BYKKTX>
    <BYBCCD>
      <xsl:value-of select="$RateState" />
    </BYBCCD>
    <BYAGNB>
      <xsl:value-of select="TerritoryCd" />
    </BYAGNB>
    <BYA5VA>
	  <!-- Issue 59878 - Start -->
      <!--xsl:value-of select="translate(CommlCoverage/Limit/FormatCurrencyAmt/Amt,'$,,','')" /-->
	  <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='']/FormatCurrencyAmt/Amt,'$,,','')" /> 
	  <!-- Issue 59878 - End -->
    </BYA5VA>
    <xsl:variable name="BYA9NB">
      <xsl:choose>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSTMS' or $CovCode='OUTSDE'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_MonSubLmtAmt']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='LESSE1'">
          <xsl:value-of select="translate(com.csc_ExcessCoverageAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA9NB>
      <xsl:value-of select="$BYA9NB" />
    </BYA9NB>
    <xsl:variable name="BYCZST">
      <xsl:choose>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSTMS' or $CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ChkRecReq']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='LESSE1'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclTrustDep']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="com.csc_CreditCardTypeCd"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYCZST>
		<xsl:value-of select="normalize-space($BYCZST)"/><!--Issue 59878 -->
    </BYCZST>
    <xsl:variable name="BYC0ST">
      <xsl:choose>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRBO'">
          <xsl:value-of select="com.csc_CoveredPropertyCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SAFE1A'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_SafeDepTransAppl']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYC0ST>
      <xsl:value-of select="$BYC0ST" />
    </BYC0ST>
	<BYCXVA>
	</BYCXVA>
	<!--59878-->
    <xsl:variable name="BYC1ST">
      <xsl:choose>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="com.csc_CardCoverageTypeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SAFE1B'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclMoney']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYC1ST>
      <xsl:value-of select="$BYC1ST" />
    </BYC1ST>
    <BYIYTX>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_BulkyProp']/OptionValue" />
    </BYIYTX>
    <BYI2TX>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_DecrLmtIns']/OptionValue" />
    </BYI2TX>
    <xsl:variable name="BYI3TX">
      <xsl:choose>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_IncrGstLmt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_IncRobbery']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYI3TX>
      <xsl:value-of select="$BYI3TX" />
    </BYI3TX>
    <xsl:variable name="BYI4TX">
      <xsl:choose>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_FoodDam']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='INSRBO' or $CovCode='OUTSDE'">
          <xsl:value-of select="com.csc_ScheduleChangeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SEC1A'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExInsDis']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYI4TX>
      <xsl:value-of select="$BYI4TX" />
    </BYI4TX>
    <xsl:variable name="BYI5TX">
      <xsl:choose>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="com.csc_WarehouseReceiptsCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_Laundry']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='INSRBO' or $CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_IncLmtSpecPerd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='LESSE1'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExcOverBlnkt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYI5TX>
      <xsl:value-of select="$BYI5TX" />
    </BYI5TX>
		<!--59878 starts-->
	<xsl:variable name="BYIFNB">
		<xsl:choose>
			<xsl:when test="$CovCode='GUEST2'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_RoomsOrApts']/OptionValue">
				</xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYIFNB>
		<xsl:value-of select="$BYIFNB"/>
	</BYIFNB>
	<!--59878 ends-->
    <xsl:variable name="BYUSIN13">
      <xsl:choose>
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="com.csc_ScheduleChangeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_LsdProp']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExtDefinition']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN13>
      <xsl:value-of select="$BYUSIN13" />
    </BYUSIN13>
    <xsl:variable name="BYUSIN14">
      <xsl:choose>
			<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>
			<!--59878-->
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_NonCompOff']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_GuestArticles']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='INSRBO'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExclDesigPremises']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN14>
      <xsl:value-of select="$BYUSIN14" />
    </BYUSIN14>
    <xsl:variable name="BYUSIN15">
      <xsl:choose>
			<!--59878 starts-->
			<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>
			<xsl:when test="$CovCode='INSOTH'">N</xsl:when>
			<xsl:when test="$CovCode='INSTMS'">N</xsl:when>
			<!--59878 ends-->
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_LsdWork']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='FRAUD' or $CovCode='INSOTH' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='INSRMS' or $CovCode='INSRBO' or $CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExclSpecProp']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN15>
      <xsl:value-of select="$BYUSIN15" />
    </BYUSIN15>
    <xsl:variable name="BYUSIN16">
      <xsl:choose>
			<!--59878 starts-->
			<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>
			<xsl:when test="$CovCode='INSOTH'">N</xsl:when>
			<!--59878 ends-->
        <xsl:when test="$CovCode='EMPDIS'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExcLmt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='FRAUD' or $CovCode='INSOTH' or $CovCode='INSRSB' or $CovCode='INSRBO' or $CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_IncrSpclLmt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN16>
      <xsl:value-of select="$BYUSIN16" />
    </BYUSIN16>
    <xsl:variable name="BYUSIN17">
      <xsl:choose>
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="com.csc_IncludeExcludePerson"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRSB' or $CovCode='INSRBO'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_AddPropOth']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_CarrierHire']/OptionValue"></xsl:value-of>
        </xsl:when>
		<!--Issue 59878 start-->
		<xsl:when test="$CovCode='INSRMS'">
		<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ForcedEntry']/OptionValue"></xsl:value-of>
		</xsl:when>
		<!--Issue 59878 end-->
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN17>
      <xsl:value-of select="$BYUSIN17" />
    </BYUSIN17>
    <xsl:variable name="BYUSIN18">
      <xsl:choose>
		<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>
		<!--59878 -->
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_DirTrust']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSRSB'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ForcedEntry']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSTMS' or $CovCode='OUTSDE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclSubLmt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN18>
      <xsl:value-of select="$BYUSIN18" />
    </BYUSIN18>
    <xsl:variable name="BYUSIN19">
      <xsl:choose>
        <xsl:when test="$CovCode='EMDISC' or $CovCode='EMPDIS'">
          <xsl:value-of select="com.csc_InstitutionTypeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="com.csc_ExcessCoverageCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='INSRBO' or $CovCode='SAFE1A' or $CovCode='SAFE1B'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_RedLmt']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN19>
      <xsl:value-of select="$BYUSIN19" />
    </BYUSIN19>
		<!--59878 starts
    <BYUSIN20>
      <xsl:value-of select="com.csc_CoalitionCd" />-->
	<xsl:variable name="BYUSIN20">
		<xsl:choose>
			<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>
			<xsl:otherwise>
		      <xsl:value-of select="com.csc_CoalitionCd" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYUSIN20>
		<xsl:value-of select="$BYUSIN20"/>
		<!--59878 ends-->
    </BYUSIN20>
    <xsl:variable name="BYUSIN21">
      <xsl:choose>
			<!--59878 starts-->
			<xsl:when test="$CovCode='EMPDIS'">N</xsl:when>	
			<xsl:when test="$CovCode='INSOTH'">N</xsl:when>
			<!--59878 ends-->
        <xsl:when test="$CovCode='EMDISC'">
          <xsl:value-of select="com.csc_ScheduleChangeCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EMPTPE' or $CovCode='EMPDIS' or $CovCode='EMPTPL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_FaithPerf']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH' or $CovCode='INSRBO'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_LmtSpecPrtn']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN21>
      <xsl:value-of select="$BYUSIN21" />
    </BYUSIN21>
    <xsl:variable name="BYUSIN22">
      <xsl:choose>
        <xsl:when test="$CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="com.csc_AllowableExcessLimitsCd"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSOTH'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclLossInFire']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN22>
      <xsl:value-of select="$BYUSIN22" />
    </BYUSIN22>
    <BYUSIN23>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_LmtOffEquip']/OptionValue" />
    </BYUSIN23>
    <BYUSIN24>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExtToFence']/OptionValue" />
    </BYUSIN24>
    <BYUSIN26>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ProtToWind']/OptionValue" />
    </BYUSIN26>
    <BYUSIN27>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclVand']/OptionValue" />
    </BYUSIN27>
    <xsl:variable name="BYUSIN28">
      <xsl:choose>
        <xsl:when test="$CovCode='EMPTPE' or $CovCode='EMPTPL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_TaxColl']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EXTORT'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclNamInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSRBO'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_InclJanitor']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN28>
      <xsl:value-of select="$BYUSIN28" />
    </BYUSIN28>
    <BYUSIN29>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExclPersProp']/OptionValue" />
    </BYUSIN29>
    <BYUSIN30>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_Binding']/OptionValue" />
    </BYUSIN30>
    <BYALCD>
      <xsl:value-of select="com.csc_StartTime" />
    </BYALCD>
    <BYPMTX>
		<!--59878 starts-->
		<!--<xsl:value-of select="com.csc_BusinessClassCd"/>-->
		<xsl:choose>
		<xsl:when test="$CovCode='INSRBO'"><xsl:value-of select="com.csc_BusinessClassCd" /></xsl:when>
		<xsl:when test="$CovCode='FORGRY'">NA</xsl:when>
		<xsl:otherwise><xsl:value-of select="com.csc_BusinessClassCd" /></xsl:otherwise>
	</xsl:choose>
	<!--59878 ends-->
    </BYPMTX>
    <BYPNTX>
      <xsl:value-of select="CommlCoverage/Form/FormNumber" />
    </BYPNTX>
    <BYPOTX>
      <xsl:value-of select="com.csc_EndTime" />
    </BYPOTX>
    <BYPPTX>
      <xsl:value-of select="translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYPPTX>
    <BYAEPC>
      <xsl:value-of select="com.csc_CoveredPct" />
    </BYAEPC>
    <BYBKNB>
      <xsl:value-of select="com.csc_NumAccounts" />
    </BYBKNB>
    <BYBLNB>
      <xsl:value-of select="com.csc_NumCardHolders" />
    </BYBLNB>
    <xsl:variable name="BYUSNB3">
      <xsl:choose>
        <xsl:when test="$CovCode='GUEST1' or $CovCode='GUEST2' or $CovCode='INSOTH' or $CovCode='INSRBO' or $CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS' or $CovCode='LESSE1' or $CovCode='LESSE2' or $CovCode='OUTSDE' or $CovCode='PAPER' or $CovCode='SAFE1A' or $CovCode='SAFE1B' or $CovCode='SEC1A' or $CovCode='SEC1B'">
          <xsl:value-of select="com.csc_NumPremises"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="com.csc_AddlPremises"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSNB3>
      <xsl:value-of select="$BYUSNB3" />
    </BYUSNB3>
    <BYUSNB4>
      <xsl:value-of select="NumEmployees" />
    </BYUSNB4>
    <BYUSNB5>
      <xsl:value-of select="com.csc_NumBoxes" />
    </BYUSNB5>
    <BYAGVA>
      <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_SecSubLmtAmt']/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYAGVA>
    <xsl:variable name="BYAHVA">
      <xsl:choose>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="translate(com.csc_WarehousePremiumAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerOcc']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSTMS' or $CovCode='OUTSDE'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_ChkSubLmtAmt']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYAHVA>
      <xsl:value-of select="$BYAHVA" />
    </BYAHVA>
    <xsl:variable name="BYUSVA3">
      <xsl:choose>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="translate(com.csc_CreditCardPremiumAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='GUEST2'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='PerPerson']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSVA3>
      <xsl:value-of select="$BYUSVA3" />
    </BYUSVA3>
    <xsl:variable name="BYUSVA5">
      <xsl:choose>
        <xsl:when test="$CovCode='FORGRY'">
          <xsl:value-of select="translate(com.csc_ForgeryPremiumAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='INSRMS' or $CovCode='INSRSB' or $CovCode='INSTMS'">
          <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='com.csc_DecrLmtAmt']/FormatCurrencyAmt/Amt,'$,,','')"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSVA5>
      <xsl:value-of select="$BYUSVA5" />
    </BYUSVA5>
	<xsl:variable name="BYA7DT">
		<xsl:choose>
			<xsl:when test="$CovCode='SAFE1A'">
				<xsl:call-template name="ConvertISODateToPTDate">
					<xsl:with-param name="Value" select="TransactionEffectiveDt"/>
				</xsl:call-template>
			</xsl:when>
        <xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
    <BYA7DT>
      <xsl:value-of select="$BYA7DT" />
    </BYA7DT>
  </xsl:template>
  <xsl:template name="BuildCFCoverage">
	<xsl:param name="Location">0</xsl:param>
	<xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
	<!--Issue 102807 starts-->
	<xsl:param name="AlarmClassCd"></xsl:param>		
    <xsl:param name="WatchmanCd"></xsl:param>	
    <xsl:param name="AlarmConnectionCd"></xsl:param>	
    <!--Issue 102807 ends-->
	<xsl:variable name="Unit" select="/ACORD/InsuranceSvcRq/*/CommlSubLocation[@LocationRef=$Location and @SubLocationRef=$SubLocation]"></xsl:variable>
	<BYAFNB>0</BYAFNB>
	<BYTYPE0ACT>
		<!-- 34771 start -->
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> -->
		<!-- 50764 -->
		<!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> -->
		<!-- 50764 -->
		<!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> -->
		<!-- 50764 -->
		<!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> -->
		<!-- 39039 -->
		<!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
		<!-- 34771 end -->
	</BYTYPE0ACT>
	<xsl:variable name="CovType">
		<xsl:choose>
			<!--
<xsl:when test="$CovCode='DEBRIS' or $CovCode='LOSS' or $CovCode='POLL'">0</xsl:when>
-->
			<xsl:when test="CommlCoverage/CurrentTermAmt/com.csc_OverrideInd='1'">0</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYD3ST>
		<xsl:value-of select="$CovType"/>
	</BYD3ST>
	<BYA3VA>
		<xsl:value-of select="translate(CommlCoverage/CurrentTermAmt/Amt,'$,,','')"/>
	</BYA3VA>
	<BYNDTX>
		<!-- 59878 <xsl:value-of select="substring-before(ClassCd,'-')"/>-->
		<xsl:value-of select="ClassCd" />
    </BYNDTX>
    <BYKKTX>
      <xsl:value-of select="com.csc_ClassCdDesc" />
    </BYKKTX>
    <BYBCCD>
      <xsl:value-of select="$RateState" />
    </BYBCCD>
    <BYAGNB>
      <xsl:value-of select="com.csc_TerritoryCd" />
    </BYAGNB>
    <BYBVVA>
			<!--xsl:value-of select="com.csc_SpecificGroupIRate"/-->
		  <!-- Issue 59878 - Start -->
	      <xsl:choose>
	        <xsl:when test="com.csc_SpecificGroupIRate=''">00000000</xsl:when>
    	    <xsl:otherwise><xsl:value-of select="concat(com.csc_SpecificGroupIRate,'')" /></xsl:otherwise>
	      </xsl:choose>
		  <!-- Issue 59878 - End -->
    </BYBVVA>
    <BYCZST>
		<!--59878	 starts-->
		<!--<xsl:value-of select="com.csc_BlanketTypeCd"/>-->
		<xsl:choose>
    		<xsl:when test="com.csc_BlanketTypeCd=''">N</xsl:when>
    		<xsl:otherwise><xsl:value-of select="normalize-space(concat(com.csc_BlanketTypeCd,''))" /></xsl:otherwise>
  		</xsl:choose>
		<!--59878 ends-->
    </BYCZST>
    <BYC0ST>
		<!-- Issue 59878 - start -->
		<xsl:choose>
			<xsl:when test="$RateState='MS'">N</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="com.csc_RiskTypeCd" />
			</xsl:otherwise>
		</xsl:choose>
		<!-- xsl:value-of select="com.csc_RiskTypeCd"/  -->					
		<!-- Issue 59878 End -->
    </BYC0ST>
    <BYC1ST>
      <xsl:value-of select="CommlCoverage/Option[OptionCd='THFT']/OptionValue" />
    </BYC1ST>
	<xsl:variable name="BYC2ST">
      <xsl:choose>
        <xsl:when test="$CovCode='BUSINC'">
				<xsl:choose>
    <!-- WAG000016- Issue 67224 Starts -->
    <!-- 
		 Since Caption on POINT is include Extra Expense while on Agency Link it is Exclude Extra Expense 
		 we need to send N when the check box is checked or otherwise 'Y'
	-->
        <xsl:when test="CommlCoverage/Option[OptionCd='com.csc_ExclEE']/OptionValue = 'N'">Y</xsl:when>
        <xsl:when test="CommlCoverage/Option[OptionCd='com.csc_ExclEE']/OptionValue = 'Y'">N</xsl:when>
        <xsl:when test="CommlCoverage/Option[OptionCd='com.csc_ExclEE']/OptionValue = ''">N</xsl:when>
        <!--<xsl:when test="SubjectInsuranceCd='BUSIN'">Y</xsl:when> -->
        <!--<xsl:when test="SubjectInsuranceCd='BUSEE'">N</xsl:when> -->
        <!--<xsl:otherwise>N</xsl:otherwise>-->	<!-- ICH00219 - AB -->
    <!-- WAG000016- Issue 67224 Ends -->
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="CommlCoverage/Limit/ValuationCd">
				</xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYC2ST>
		<xsl:value-of select="$BYC2ST"/>
	</BYC2ST>
    <xsl:variable name="BYIYTX">
		<xsl:choose>
			<xsl:when test="$CovCode='BR'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_BBC']/OptionValue">
				</xsl:value-of>
			</xsl:when>
			<!-- Issue 59878 - Start -->
        	<xsl:when test="$CovCode='ORD-B'">
          		<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_CombinedBandC']/OptionValue"></xsl:value-of>
        	</xsl:when>
			<!-- Issue 59878 - End -->
			<xsl:otherwise>
				<xsl:value-of select="CommlCoverage/Option[OptionCd='G']/OptionValue">
				</xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYIYTX>
		<xsl:value-of select="$BYIYTX"/>
	</BYIYTX>
	<xsl:variable name="BYIZTX">
		<xsl:choose>
			<xsl:when test="$CovCode='BUSINC'">
			<!-- Issue 59878 - Start -->
				<!--xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_MaxPeriod']/OptionValue">
				</xsl:value-of-->
				<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='BIMPI']/OptionValue,'')"></xsl:value-of>
				<!-- Issue 59878 - End -->
			</xsl:when>
			<!-- Issue 59878 - Start -->
	         <xsl:when test="$CovCode='BLDG'">
				<xsl:choose>
    		  		<xsl:when test="com.csc_OrdinanceOrLawCoverageAInd=''">N</xsl:when>
    	  			<xsl:otherwise><xsl:value-of select="concat(com.csc_OrdinanceOrLawCoverageAInd,'')" /></xsl:otherwise>
				</xsl:choose> 
    		</xsl:when>
			<xsl:when test="$CovCode='BR'">
      			<xsl:value-of select="com.csc_ReportingInd"></xsl:value-of>
    		</xsl:when>
			<!-- Issue 59878 - End -->
			<xsl:otherwise>
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_RA']/OptionValue">
				</xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYIZTX>
		<xsl:value-of select="$BYIZTX"/>
	</BYIZTX>
	<xsl:variable name="BYI0TX">
		<xsl:choose>
			<xsl:when test="count(CommlCoverage/Option[OptionCd='EQ' and OptionValue='Y']) &gt; 0">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYI0TX>
		<xsl:value-of select="$BYI0TX"/>
	</BYI0TX>
    <BYI1TX>
		<!-- Issue 59878 - Start -->
		<!--xsl:value-of select="com.csc_DependentPropertyConditionCd"/-->
		<xsl:choose>
	        <xsl:when test="$CovCode='BUSINC'">
			<xsl:choose>
			  <xsl:when test="com.csc_DependentPropertyConditionCd=''">A</xsl:when>
			  <xsl:otherwise><xsl:value-of select="concat(com.csc_DependentPropertyConditionCd,'')" /></xsl:otherwise>
			</xsl:choose>
	        </xsl:when>
	        <xsl:when test="$CovCode='BLDG'">
			<xsl:choose>
	        	  <xsl:when test="com.csc_OrdinanceOrLawCoverageAInd=''">N</xsl:when>
	        	  <xsl:otherwise><xsl:value-of select="concat(com.csc_OrdinanceOrLawCoverageAInd,'')" /></xsl:otherwise>

		</xsl:choose> 
        </xsl:when>
        <xsl:when test="$CovCode='MINE'">
          <xsl:value-of select="Option[OptionCd='com.csc_MineOccupancyInd']/OptionValue"></xsl:value-of>
        </xsl:when>
  	</xsl:choose>
	<!-- Issue 59878 - End -->
    </BYI1TX>
	<BYI2TX>
		<xsl:value-of select="com.csc_CoverageTypeCd"/>
    </BYI2TX>
	<xsl:variable name="BYI3TX">
		<xsl:choose>
			<xsl:when test="$CovCode='SPOIL'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_SPA']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_NYWHPD']/OptionValue"></xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYI3TX>
		<!-- Issue 59878 - start -->
	<xsl:choose>
				<xsl:when test="$RateState='MS'">N</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$BYI3TX"/>					
				</xsl:otherwise>
			</xsl:choose>
		<!-- xsl:value-of select="$BYI3TX"/ -->
		<!-- End Issue 59878 -->
	</BYI3TX>
	<BYI4TX>
      <xsl:value-of select="com.csc_UtilitiesCd" />
    </BYI4TX>
    <BYI5TX>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OPC']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYI5TX>
	<BYUSIN13>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TEPS']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN13>
	<BYUSIN14>	
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TEWS']/OptionValue,'')" />	<!-- Issue 59878 -->
    </BYUSIN14>
	<BYUSIN15>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TECS']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN15>
	<BYUSIN16>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TEOHP']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN16>
	<BYUSIN17>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TECL']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN17>
	<BYUSIN20>
	<!--Issue 102807 starts-->
	 <xsl:choose>
        <xsl:when test="$CovCode='PEAK'">
			  <xsl:value-of select="$WatchmanCd"></xsl:value-of>
		</xsl:when>
		 <xsl:otherwise>
		<xsl:value-of select="concat(com.csc_WatchmanCd,'')" />	<!-- Issue 59878 -->
        </xsl:otherwise>
      </xsl:choose>
		<!--Issue 102807 ends-->
	</BYUSIN20>
    <xsl:variable name="BYUSIN21">
      <xsl:choose>
        <xsl:when test="$CovCode='BUSINC'">
			<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_TEWP']/OptionValue,'')"></xsl:value-of>	<!-- Issue 59878 -->
        </xsl:when>
        <xsl:otherwise>
        <!--Issue 102807 starts-->
         <xsl:choose>
        <xsl:when test="$CovCode='PEAK'">
			<xsl:value-of select="$AlarmConnectionCd"></xsl:value-of>	<!-- Issue 59878 -->
			</xsl:when>
			 <xsl:otherwise>
			 	<xsl:value-of select="concat(com.csc_AlarmConnectionCd,'')"></xsl:value-of>
			  </xsl:otherwise>
			 </xsl:choose>
			 		<!--Issue 102807 ends-->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	<BYUSIN21>
		<xsl:value-of select="$BYUSIN21"/>
	</BYUSIN21>
	<BYUSIN22>
		<xsl:value-of select="CommlCoverage/Option[OptionCd='ALARM']/OptionValue"/>
	</BYUSIN22>
	<BYUSIN23>
		<!--xsl:value-of select="com.csc_AlarmClassCd"/-->
		<!--Issue 102807 starts-->
	 <xsl:choose>
        <xsl:when test="$CovCode='PEAK'">
			  <xsl:value-of select="$AlarmClassCd" />
		</xsl:when>
		 <xsl:otherwise>
		<xsl:value-of select="com.csc_AlarmClassCd"/>	<!-- Issue 59878 -->
        </xsl:otherwise>
      </xsl:choose>
		<!--Issue 102807 ends-->
	</BYUSIN23>
	<BYUSIN24>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OHCL']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN24>
	<BYUSIN25>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OPP']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN25>
	<BYUSIN26>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OHPT']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN26>
	<BYUSIN27>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OPW']/OptionValue,'')" />	<!-- Issue 59878 -->
	</BYUSIN27>
	<BYUSIN28>
		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='W']/OptionValue,'')" />	<!-- Issue 59878 -->
    </BYUSIN28>
    <xsl:variable name="BYUSIN29">
      <xsl:choose>
        <xsl:when test="$CovCode='BUSINC' or $CovCode='EE'">
          <xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_OLR']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='BR'">
          <xsl:value-of select="com.csc_InsuredTypeCd"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="com.csc_SpecialDiscountRatesCd"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYUSIN29>
		<!-- Start Issue 59878 -->
		<xsl:choose>
				<xsl:when test="$RateState='MS'">N</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$BYUSIN29" />
				</xsl:otherwise>
			</xsl:choose>
      		<!-- xsl:value-of select="$BYUSIN29" / -->
			<!-- End Issue 59878 -->
    </BYUSIN29>
    <xsl:variable name="BYUSIN30">
      <xsl:choose>
			<xsl:when test="$CovCode='MINE'">
				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_StructureType']/OptionValue" />	<!-- Issue 59878 -->
			</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="CommlCoverage/Option[OptionCd='V']/OptionValue"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	<BYUSIN30>
		<xsl:value-of select="$BYUSIN30"/>
	</BYUSIN30>
	<BYAKCD>
		<!-- Issue 59878 - Start -->
		<!--xsl:value-of select="CommlCoverage/CommlCoverageSupplement/CoinsurancePct"/-->
		<xsl:choose>
        <xsl:when test="$CovCode='FIRE-B'or $CovCode='FIRE-P'">80</xsl:when>
	    <xsl:otherwise>
			<xsl:value-of select="concat(CommlCoverage/CommlCoverageSupplement/CoinsurancePct,'')" />
		</xsl:otherwise>
		</xsl:choose>
		<!-- Issue 59878 - End -->
    </BYAKCD>
    <xsl:variable name="BYALCD">
		<xsl:choose>
			<!-- Issue 59878 - Start -->
			<!--xsl:when test="$CovCode='BUSINC'"-->
			<xsl:when test="$CovCode='BUSINC' or $CovCode='SPLCLS'">
			<!-- Issue 59878 - End -->
				<xsl:value-of select="com.csc_ExtendedPeriodCd">
				</xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<BYALCD>
		<xsl:value-of select="$BYALCD"/>
	</BYALCD>
	<BYAMCD>
		<!-- Issue 59878 - Start -->
		<!--xsl:value-of select="CommlCoverage/CoverageCd"/-->
		<xsl:value-of select="concat(com.csc_CoverageFormCd,'')" />
		<!-- Issue 59878 - End -->
	</BYAMCD>
    <xsl:variable name="BYANCD">
		<xsl:choose>
			<xsl:when test="$CovCode='BR'">
				<xsl:value-of select="translate(CommlCoverage/Deductible[DeductibleAppliesToCd='Theft']/FormatCurrencyAmt/Amt,'$,,','')"/>
			</xsl:when>
			<xsl:when test="$CovCode='BUSINC'">
				<!-- Issue 59878 - Start -->
				<!--xsl:value-of select="CommlCoverage/Option[OptionCd='BIOPE']/OptionValue"/-->
				<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='com.csc_OPE']/OptionValue,'')" />
				<!-- Issue 59878 - End -->
			</xsl:when>
			<xsl:when test="$CovCode='VACANT'">
				<xsl:value-of select="com.csc_PreviousOccupantVandalismCd"/>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
    <BYANCD>
      <xsl:value-of select="$BYANCD" />
    </BYANCD>
    <xsl:variable name="BYUSCD5">
      <xsl:choose>
        <xsl:when test="$CovCode='BUSINC'">
          <xsl:value-of select="com.csc_MonthlyLimitCd"></xsl:value-of>
			</xsl:when>
			<!-- Issue 59878 - Start -->
			<!--xsl:otherwise>
				<xsl:value-of select="CommlCoverage[CoverageCd='INFL']/Option/OptionValue">
				</xsl:value-of>
			</xsl:otherwise-->
    		<xsl:when test="$CovCode='BLDG'">
	    		<xsl:value-of select="concat(CommlCoverage/Option[OptionCd='INFL']/OptionValue,'')" />				<!-- ICH00018. eCase 35429. -->
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
			<!-- Issue 59878 - End -->
      </xsl:choose>
    </xsl:variable>
    <BYUSCD5>
      <xsl:value-of select="$BYUSCD5" />
    </BYUSCD5>
    <BYPLTX>
      <xsl:value-of select="com.csc_LossPaymentCd" />
    </BYPLTX>
    <xsl:variable name="BYPMTX">
      <xsl:choose>
        <xsl:when test="CommlCoverage/Option[OptionCd='com.csc_MA']/OptionValue='Y'">YES</xsl:when>
        <xsl:otherwise>NO</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYPMTX>
      <xsl:value-of select="$BYPMTX" />
    </BYPMTX>
    <xsl:variable name="BYPNTX">
      <xsl:choose>
        <xsl:when test="$CovCode='SPOIL'">
			<!-- Issue 59878 - Start -->
			<!--xsl:value-of select="CommlCoverage/CoverageCd"/-->
			<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_SpoilageCd']/OptionValue" />
			<!-- Issue 59878 - End -->
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="com.csc_GroupIIClassRateItemCd" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYPNTX>
		<!-- Issue 59878 - Start -->
		<xsl:choose>
				<xsl:when test="$CovCode = 'BLDG' or $CovCode = 'PEAK' or $CovCode = 'PERS' or $CovCode = 'PPO' or $CovCode = 'STOCK' or $CovCode = 'IMPBET'">N/A</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$BYPNTX" />
				</xsl:otherwise>
		</xsl:choose>
      	<!-- xsl:value-of select="$BYPNTX" / -->
		<!-- Issue 59878 - End -->
    </BYPNTX>
    <xsl:if test="$CovCode='SPOIL'">
      <BYPOTX>
        <xsl:value-of select="com.csc_SpoilageClassificationCd" />
      </BYPOTX>
    </xsl:if>
    <BYPPTX>
      <xsl:value-of select="translate(CommlCoverage/Deductible/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYPPTX>
    <BYUSFT4>
      <xsl:value-of select="com.csc_SpecificGroupIIRate" />
    </BYUSFT4>
	<!-- Issue 59878 - Start -->
	<BYUSNB5>
  		<xsl:choose>
    		<xsl:when test="$CovCode='MINE' and $RateState='IL'">
  				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_NumUnits']/OptionValue" />
    		</xsl:when>
    		<xsl:otherwise></xsl:otherwise>
  		</xsl:choose>
	</BYUSNB5>
    <BYC2ST>
		<xsl:choose>
    		<xsl:when test="$CovCode='MINE' and $RateState='IL'">
  				<xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_CondoLessors']/OptionValue" />
    		</xsl:when>
    		<xsl:otherwise></xsl:otherwise>
  		</xsl:choose>
	</BYC2ST>
	<!-- Issue 59878 - End -->
    <BYAGVA>
      <xsl:value-of select="translate(CommlCoverage/Limit/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYAGVA>
    <BYUSVA4>
      <xsl:value-of select="translate(CommlCoverage/Limit[LimitAppliesToCd='Theft']/FormatCurrencyAmt/Amt,'$,,','')" />
    </BYUSVA4>
    <xsl:variable name="BYA5DT">
      <xsl:choose>
        <xsl:when test="$CovCode='PEAK'">
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlCoverage/EffectiveDt" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA5DT>
      <xsl:value-of select="$BYA5DT" />
    </BYA5DT>
    <xsl:variable name="BYA6DT">
      <xsl:choose>
        <xsl:when test="$CovCode='PEAK'">
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlCoverage/ExpirationDt" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA6DT>
      <xsl:value-of select="$BYA6DT" />
    </BYA6DT>
    <xsl:variable name="BYA7DT">
      <xsl:choose>
        <xsl:when test="$CovCode='VACANT'">
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlCoverage/EffectiveDt" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA7DT>
      <xsl:value-of select="$BYA7DT" />
    </BYA7DT>
    <xsl:variable name="BYA8DT">
      <xsl:choose>
        <xsl:when test="$CovCode='VACANT'">
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlCoverage/ExpirationDt" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <BYA8DT>
      <xsl:value-of select="$BYA8DT" />
    </BYA8DT>
    <BYA9DT>
	<!-- Issue 59878 - Start -->
	 <xsl:choose>
		<xsl:when test="com.csc_SpecificRateDt=''">0000000</xsl:when>
		<xsl:otherwise>
		<!-- Issue 59878 - End -->
			<xsl:call-template name="ConvertISODateToPTDate">
				<xsl:with-param name="Value" select="com.csc_SpecificRateDt"/>
			</xsl:call-template>
		<!-- Issue 59878 - Start -->
		</xsl:otherwise>
	 </xsl:choose>
	 <!-- Issue 59878 - End -->
    </BYA9DT>
  </xsl:template>
  <xsl:template name="BuildBOCoverage">
    <xsl:param name="Location"></xsl:param>
    <xsl:param name="SubLocation"></xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
    <xsl:variable name="GlassSchedule" select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/GlassSignInfo[substring(@LocationRef,2,string-length(@LocationRef)-1)=$Location and substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)=$SubLocation]"></xsl:variable>
    <BYAFNB>0</BYAFNB>
    <BYTYPE0ACT>
	<!-- 34771 start -->
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
	<!-- 34771 end -->
     </BYTYPE0ACT>
    <xsl:if test="$Location='0' and $SubLocation='0'">
      <BYEETX>0I</BYEETX>
    </xsl:if>
    <BYNDTX>
      <xsl:value-of select="/*/CommlPropertyInfo[substring(@LocationRef,2,string-length(@LocationRef)-1)=$Location and substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)=$SubLocation]/ClassCd" />
    </BYNDTX>
    <BYBCCD>
	   <xsl:choose>
		<xsl:when test="$CovCode='EMPDIS' or $CovCode='HIRED' or $CovCode='NONOWN'"> 
				<xsl:value-of select="$RateState" />
		</xsl:when>
        <xsl:otherwise>
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[substring(@id,2,string-length(@id)-1)=$Location]/Addr/StateProvCd" />
		</xsl:otherwise>
      </xsl:choose>
      <!-- <xsl:value-of select="$RateState" /> -->
    </BYBCCD>
    <BYD3ST>
      <xsl:if test="CurrentTermAmt/com.csc_OverrideInd='Y'">0</xsl:if>
    </BYD3ST>
    <BYA3VA>
      <xsl:value-of select="translate(CurrentTermAmt/Amt,'$,,','')" />
    </BYA3VA>
    <BYCZST>
      <xsl:choose>
        <xsl:when test="$CovCode='LIQEXC'">
          <xsl:value-of select="Option[OptionCd='com.csc_LiqExclInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='MINE'">
          <xsl:value-of select="Option[OptionCd='com.csc_TypeStructureInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='MECH'">
          <xsl:value-of select="Option[OptionCd='com.csc_ACCoverageInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SPOIL' or $CovCode='ORDLAW'">
          <xsl:value-of select="Option[OptionCd='com.csc_TypeCoverageInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='TIME'">
          <xsl:value-of select="Option[OptionCd='com.csc_OHPT']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='Y2KLIA'">
          <xsl:value-of select="Option[OptionCd='com.csc_OptionsInd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYCZST>
    <BYC0ST>
      <xsl:choose>
        <xsl:when test="$CovCode='MECH'">
          <xsl:value-of select="Option[OptionCd='com.csc_BoilerPressureInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='Y2KLIA'">
          <xsl:value-of select="Option[OptionCd='com.csc_BodilyInjuryInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='EQPERS'">
          <xsl:value-of select="Option[OptionCd='com.csc_ContentRateGrpInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='ORDLAW'">
          <xsl:value-of select="Option[OptionCd='com.csc_EQCovInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SPOIL'">
          <xsl:value-of select="Option[OptionCd='com.csc_MaintAgreementInd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYC0ST>
    <BYC1ST>
      <xsl:choose>
        <xsl:when test="$CovCode='MECH'">
          <xsl:value-of select="Option[OptionCd='com.csc_PressureInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='LIQEXC'">
          <xsl:value-of select="Option[OptionCd='com.csc_LiquorScldInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='TIME'">
          <xsl:value-of select="Option[OptionCd='com.csc_OHCL']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='Y2KLIA'">
          <xsl:value-of select="Option[OptionCd='com.csc_PropertyDmgInd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYC1ST>
    <BYC2ST>
      <xsl:choose>
        <xsl:when test="$CovCode='Y2KLIA'">
          <xsl:value-of select="Option[OptionCd='com.csc_PersonalInjuryInd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='TIME'">
          <xsl:value-of select="Option[OptionCd='com.csc_OPW']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYC2ST>
    <BYIYTX>
      <xsl:choose>
        <xsl:when test="$CovCode='TIME'">
          <xsl:value-of select="Option[OptionCd='com.csc_OPC']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYIYTX>
    <BYIZTX>
      <xsl:choose>
        <xsl:when test="$CovCode='TIME'">
          <xsl:value-of select="Option[OptionCd='com.csc_OPP']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYIZTX>
    <BYI1TX>
      <xsl:choose>
        <xsl:when test="$CovCode='MINE'">
          <xsl:value-of select="Option[OptionCd='com.csc_MineOccupancyInd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYI1TX>
    <BYI2TX>
      <xsl:choose>
        <xsl:when test="$CovCode='EQBLDG' or $CovCode='EQPERS'">
          <xsl:value-of select="Option[OptionCd='com.csc_EQZoneInd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYI2TX>
    <BYAKCD>
      <xsl:choose>
        <xsl:when test="$CovCode='MINE'">
          <xsl:value-of select="Option[OptionCd='com.csc_MineCountyCd']/OptionValue"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$CovCode='SPOIL'">
          <xsl:value-of select="Option[OptionCd='com.csc_ClassTypeCd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYAKCD>
    <BYAMCD>
      <xsl:choose>
        <xsl:when test="$CovCode='BOILER'">
          <xsl:value-of select="Option[OptionCd='com.csc_BoilerClassCd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYAMCD>
    <BYANCD>
      <xsl:choose>
        <xsl:when test="$CovCode='EQBLDG' or $CovCode='EQPERS'">
          <xsl:value-of select="Option[OptionCd='com.csc_EQDedPct']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYANCD>
    <BYUSCD5>
      <xsl:choose>
        <xsl:when test="$CovCode='EQBLDG' or $CovCode='EQPERS'">
          <xsl:value-of select="Option[OptionCd='com.csc_EQBldgClassCd']/OptionValue"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </BYUSCD5>
    <BYAEPC>
      <xsl:choose>
        <xsl:when test="$CovCode='PRFBRB' or $CovCode='PRFBTY' or $CovCode='PRFCLR' or $CovCode='PRFVET'">
          <xsl:value-of select="Option[OptionCd='com.csc_NumPerson']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYAEPC>
    <BYBKNB>
      <xsl:choose>
        <xsl:when test="$CovCode='EMPDIS'">
          <xsl:value-of select="Option[OptionCd='com.csc_NumEmp']/OptionValue" />
        </xsl:when>
        <xsl:when test="$CovCode='BACKUP' or $CovCode='LEAD'">
          <xsl:value-of select="Option[OptionCd='com.csc_NumBldg']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYBKNB>
    <BYBLNB>
      <xsl:choose>
        <xsl:when test="$CovCode='EMPDIS'">
          <xsl:value-of select="Option[OptionCd='com.csc_NumLoc']/OptionValue" />
        </xsl:when>
        <xsl:when test="$CovCode='ADDMGR'">
          <xsl:value-of select="Option[OptionCd='com.csc_NumAddlInsured']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYBLNB>
    <BYAGVA>
      <xsl:choose>
        <xsl:when test="$CovCode='EMPDIS' or $CovCode='HIRED' or $CovCode='NONOWN' or $CovCode='EGLASS' or $CovCode='IGLASS' or $CovCode='SIGNS'">
          <xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" />
        </xsl:otherwise>
      </xsl:choose>
    </BYAGVA>
    <BYAHVA>
      <xsl:choose>
        <xsl:when test="$CovCode='CMPTR'">
          <xsl:value-of select="Option[OptionCd='com.csc_DataMedia']/OptionValue" />
        </xsl:when>
        <xsl:when test="$CovCode='EGLASS'">
          <xsl:for-each select="$GlassSchedule/GlassSignSchedule[ScheduleTypeCd='EGLASS' and GlassPositionAndUseInBldgCd='UP']">
            <xsl:value-of select="PlateSizeArea/NumUnits" />
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$CovCode='IGLASS'">
          <xsl:for-each select="$GlassSchedule/GlassSignSchedule[ScheduleTypeCd='IGLASS' and GlassPositionAndUseInBldgCd='UP']">
            <xsl:value-of select="PlateSizeArea/NumUnits" />
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$CovCode='ORDLAW'">
          <xsl:value-of select="Option[OptionCd='com.csc_Cov3Limit']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYAHVA>
    <BYUSVA3>
      <xsl:choose>
        <xsl:when test="$CovCode='EGLASS'">
          <xsl:for-each select="$GlassSchedule/GlassSignSchedule[ScheduleTypeCd='EGLASS' and GlassPositionAndUseInBldgCd='LO']">
            <xsl:value-of select="PlateSizeArea/NumUnits" />
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$CovCode='IGLASS'">
          <xsl:for-each select="$GlassSchedule/GlassSignSchedule[ScheduleTypeCd='IGLASS' and GlassPositionAndUseInBldgCd='LO']">
            <xsl:value-of select="PlateSizeArea/NumUnits" />
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$CovCode='ORDLAW'">
          <xsl:value-of select="Option[OptionCd='com.csc_Cov23Limit']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYUSVA3>
    <BYUSVA4>
      <xsl:choose>
        <xsl:when test="$CovCode='IGLASS'">
          <xsl:for-each select="$GlassSchedule/GlassSignSchedule[ScheduleTypeCd='IGLASS' and GlassPositionAndUseInBldgCd='IR']">
            <xsl:value-of select="PlateSizeArea/NumUnits" />
          </xsl:for-each>
        </xsl:when>
        <xsl:when test="$CovCode='ORDLAW'">
          <xsl:value-of select="Option[OptionCd='com.csc_Cov2Limit']/OptionValue" />
        </xsl:when>
      </xsl:choose>
    </BYUSVA4>
  </xsl:template>
  <xsl:template name="BuildCACoverage">
    <xsl:param name="Location"></xsl:param>
    <xsl:param name="SubLocation"></xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <xsl:param name="CovCode"></xsl:param>
	<!-- Issue 59878 - Start -->
	<xsl:param name="UnitNbr"></xsl:param>	
	<xsl:choose>
	  	<xsl:when test="$UnitNbr != '0'">
			<BYEETX></BYEETX>
		</xsl:when>
		<xsl:otherwise><BYEETX>0I</BYEETX></xsl:otherwise> 
	</xsl:choose>
	<!-- Issue 59878 - End -->
    <BYBCCD>
      <xsl:value-of select="$RateState" />
    </BYBCCD>
    <BYNDTX>
		<!-- Issue 67226 - START -->
		<xsl:choose>
			<xsl:when test="$CovCode = 'UMPD' and CoverageCd = 'UM'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh[substring(@id,2,string-length(@id)-1)=$UnitNbr]/CommlVehSupplement/PrimaryClassCd"/>
			</xsl:when>
			<xsl:otherwise>
		<!-- Issue 67226 - END -->
      <xsl:value-of select="../CommlVehSupplement/PrimaryClassCd" />
			</xsl:otherwise><!-- Issue 67226 -->
		</xsl:choose><!-- Issue 67226 -->
    </BYNDTX>
	<!-- Issue 67226 - START -->
	<BYKKTX>
		<xsl:choose>
			<xsl:when test="$CovCode = 'UMPD' and CoverageCd = 'UM'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh[substring(@id,2,string-length(@id)-1)=$UnitNbr]/CommlVehSupplement/com.csc_PrimaryClassCdDesc"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</BYKKTX>
	<!-- Issue 67226 - END -->
    <BYAGVA>
	<!-- Issue 67226 - START -->
		<xsl:choose>
<!--Issue 66352 begin		
			<xsl:when test="$CovCode = 'UMPD' and $RateState = 'MS' and CoverageCd = 'UM'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<xsl:when test="$CovCode = 'UMPD' and $RateState = 'OH' and CoverageCd = 'UM'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<xsl:when test="$CovCode = 'UMPD' and $RateState = 'TN' and CoverageCd = 'UM'">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt"/>
Issue 66352 end -->
<!--Issue 66352 begin -->		
<!--59878 DST CA#67 changed the condition from and to or -->		
  	<xsl:when test="($CovCode = 'UMPD' or CoverageCd = 'UM' 
  							and (($RateState = 'AR')
  								or ($RateState = 'CA')
  								or ($RateState = 'GA')
  								or ($RateState = 'IL')
  								or ($RateState = 'IN')
  								or ($RateState = 'LA')
  								or ($RateState = 'MS')
  								or ($RateState = 'NM')
  								or ($RateState = 'OH')
  								or ($RateState = 'OR')
  								or ($RateState = 'TN')
  								or ($RateState = 'TX')
  								or ($RateState = 'UT')))">
		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh[substring(@id,2,string-length(@id)-1)=$UnitNbr]/CommlVehSupplement/com.csc_VehicleUMPDLimitCd"/>
<!-- Issue 66352 End -->
			</xsl:when>
		<!-- Issue 59878 - Start -->
	  	<xsl:when test="$RateState = 'UT' and $CovCode = 'ADDPIP' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlCoverage/Option/OptionValue='a'">000005000</xsl:when>
	  	<xsl:when test="$RateState = 'UT' and $CovCode = 'ADDPIP' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlCoverage/Option/OptionValue='b'">000010000</xsl:when>
	  	<xsl:when test="$RateState = 'UT' and $CovCode = 'BRDPIP' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlCoverage/Option/OptionValue='a'">000005000</xsl:when>
	  	<xsl:when test="$RateState = 'UT' and $CovCode = 'BRDPIP' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlCoverage/Option/OptionValue='b'">000010000</xsl:when>
		<!-- Issue 59878 - End -->
			<xsl:otherwise>
	<!-- Issue 67226 - END -->
		<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
			</xsl:otherwise><!-- Issue 67226 -->
		</xsl:choose><!-- Issue 67226 -->
    </BYAGVA>
    <BYPPTX>
      <xsl:value-of select="Deductible/FormatCurrencyAmt/Amt" />
    </BYPPTX>
    <BYALCD>
      <!-- Issue 59878 - Start -->
	  <!--xsl:value-of select="Option[OptionCd='com.csc_CoverageType']/OptionValue" /-->
	  <xsl:choose>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'AR' and CoverageCd = 'UM'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'NM' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'OR' and CoverageCd = 'UM'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'IL' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'LA' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'TX' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'UT' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'GA' and CoverageCd = 'UM'">0</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'IN' and CoverageCd = 'UM'">300</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'OH' and CoverageCd = 'UM'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'MS' and CoverageCd = 'UM'">250</xsl:when> <!--issue 59878- changed deductible value from 200 to 250-->
	  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'TN' and CoverageCd = 'UM'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'AR' and CoverageCd = 'DUP'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'MS' and CoverageCd = 'DUP'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'OR' and CoverageCd = 'DUP'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'TN' and CoverageCd = 'DUP'">200</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'GA' and CoverageCd = 'DUP'">0</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'IN' and CoverageCd = 'DUP'">300</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'IL' and CoverageCd = 'DUP'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'LA' and CoverageCd = 'DUP'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'TX' and CoverageCd = 'DUP'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'NM' and CoverageCd = 'DUP'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'OH' and CoverageCd = 'DUP'">250</xsl:when>
	  	<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'UT' and CoverageCd = 'DUP'">250</xsl:when>
		<xsl:when test="CoverageCd = 'RCO'">COMP</xsl:when>
		<xsl:when test="CoverageCd = 'RSP'">SP</xsl:when>
		<xsl:when test="CoverageCd = 'RCL'">COLL</xsl:when>
		<xsl:otherwise><xsl:value-of select="Option[OptionCd='com.csc_CoverageType']/OptionValue" /></xsl:otherwise>
	  </xsl:choose>
	  <!-- Issue 59878 - End -->
    </BYALCD>
	<!-- Issue 67226 - Start -->
	<BYAMCD>
		<xsl:choose>
			<xsl:when test="$CovCode = 'DOCUMP' and $RateState = 'MS'">
				<xsl:variable name="DOCUMTYPE" select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMType']/OptionValue"/>
  		<xsl:choose>
  			<xsl:when test="normalize-space($DOCUMTYPE)='CSLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($DOCUMTYPE)='SLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($DOCUMTYPE)='SPLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($DOCUMTYPE)='CSLSTK'">PDSTK</xsl:when>
  			<xsl:when test="normalize-space($DOCUMTYPE)='SLSTK'">PDSTK</xsl:when>
  			<xsl:when test="normalize-space($DOCUMTYPE)='SPLSTK'">PDSTK</xsl:when>
			<xsl:otherwise></xsl:otherwise>
  		</xsl:choose>
	</xsl:when>
  	<xsl:when test="$CovCode = 'UMPD' and $RateState = 'MS'">
		<xsl:variable name="UMTYPE" select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMType']/OptionValue"/>
  		<xsl:choose>
  			<xsl:when test="normalize-space($UMTYPE)='CSLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($UMTYPE)='SLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($UMTYPE)='SPLNST'">PDNST</xsl:when>
  			<xsl:when test="normalize-space($UMTYPE)='CSLSTK'">PDSTK</xsl:when>
  			<xsl:when test="normalize-space($UMTYPE)='SLSTK'">PDSTK</xsl:when>
  			<xsl:when test="normalize-space($UMTYPE)='SPLSTK'">PDSTK</xsl:when>
			<xsl:otherwise></xsl:otherwise>
  		</xsl:choose>
	</xsl:when>
	<!--<xsl:when test="$CovCode = 'UN' and $RateState = 'AR'">
		<xsl:value-of select="Option[OptionCd='com.csc_UNType']/OptionValue"/>
	</xsl:when>-->
	<xsl:when test="$CovCode = 'WRKLSS' and $RateState = 'AR'">
        <xsl:value-of select="Option[OptionCd='com.csc_WorkLossExcl']/OptionValue" />
	</xsl:when>
	<xsl:otherwise/> 
  </xsl:choose>
	
 
	</BYAMCD>			
	<!-- Issue 67226 - End -->
    <BYAHVA>
      <xsl:value-of select="Option[OptionCd='com.csc_NumDays']/OptionValue" />
    </BYAHVA>
	<!--Issue 59878 start-->
	<!--Start ADDPIP-->
	<BYPMTX>
  		<xsl:choose> 
			<xsl:when test="$InsLine = 'CA' and $CovCode = 'ADDPIP'"> 
	  			<xsl:value-of select="../CommlCoverage[CoverageCd = 'ADDPIP']/Option/OptionValue"/> 	
  		  	</xsl:when> 
  		</xsl:choose> 
	</BYPMTX>
	<!--End ADDPIP-->
	<!--Start BRDPIP-->
	<BYBKNB>
	  <xsl:choose> 
		<xsl:when test="$InsLine = 'CA' and $CovCode = 'BRDPIP'"> 
		  <xsl:value-of select="../CommlCoverage[CoverageCd = 'BRDPIP']/Option/OptionValue"/> 	
        </xsl:when> 
        <xsl:when test="$InsLine = 'CA' and $CovCode = 'ADDPIP'"> 
		  <xsl:value-of select="../CommlCoverage[CoverageCd = 'BRDPIP']/Option/OptionValue"/> 	
        </xsl:when>         
	  </xsl:choose> 
	</BYBKNB>
	<!--End BRDPIP-->
	<!--Issue 59878 end-->
    <BYUSCD5>
	    <xsl:choose>
			<xsl:when test="$CovCode='UN' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']/Option[OptionCd='com.csc_UMSpecType']">
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']/Option[OptionCd='com.csc_UMSpecType']/OptionValue"/>
			</xsl:when>
			<xsl:when test="$CovCode='UN'">
				<xsl:value-of select="Limit[LimitAppliesToCd='com.csc_UNLimit']/FormatCurrencyAmt/Amt"></xsl:value-of>
			</xsl:when>
	    </xsl:choose>
    </BYUSCD5>

	<!-- Issue 66929 Begin -->
	<BYUSVA3>
		<xsl:choose>
        	<xsl:when test="$CovCode='UN' and $RateState='SD' and Option[OptionCd='com.csc_UNType']/OptionValue='SPLIT'">
				<xsl:value-of select="Limit[LimitAppliesToCd='UNDPD']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<!-- Issue 59878 - Start -->
   	  		<xsl:when test="$CovCode='UN' and $RateState='WA' and Option[OptionCd='com.csc_UNType']/OptionValue='CSL'">
				<xsl:value-of select="Limit[LimitAppliesToCd='UNDSP']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
   	  		<xsl:when test="$CovCode='UN' and $RateState='WA' and Option[OptionCd='com.csc_UNType']/OptionValue='SPLIT'">
				<xsl:value-of select="Option[OptionCd='UNDSP']/OptionValue"/>
			</xsl:when>
	  		<xsl:when test="$CovCode = 'UN' and Option[OptionCd='com.csc_UNType']/OptionValue!='CSL'">
				<xsl:value-of select="Limit[LimitAppliesToCd='UNDSP']/FormatCurrencyAmt/Amt"/>
			</xsl:when>
			<xsl:when test="$CovCode = 'SOUND'">
				<xsl:value-of select="Limit/FormatCurrencyAmt/Amt" />
			</xsl:when>
	  		<xsl:when test="$RateState = 'UT' and $CovCode = 'ADDPIP' and Option/OptionValue='a'">300</xsl:when>
	  		<xsl:when test="$RateState = 'UT' and $CovCode = 'ADDPIP' and Option/OptionValue='b'">350</xsl:when>
			<xsl:when test="$RateState = 'AR' and $CovCode = 'DTHBEN' and Option[OptionCd='com.csc_AccDeathBenefit']">00000005000</xsl:when>
		<!-- Issue 59878 - End -->
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</BYUSVA3>
	<!-- Issue 69929 End -->
    <BYUSVA4>
	<!-- Issue 67226 - START -->
	  <xsl:choose>
		<xsl:when test="$InsLine = 'CA' and $CovCode = 'DOCUMP' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd = 'DUP']/Option[OptionValue='Y'][OptionTypeCd='YNInd1']"><!-- Issue 59878 (log 237)  -->
			<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt"/>
	    </xsl:when>
		<xsl:when test="$InsLine = 'CA' and $CovCode = 'UMPD'">
			<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt"/>
	    </xsl:when>
		<xsl:when test="$InsLine = 'CA' and $CovCode = 'UNPD'">
			<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd = 'WA']/CommlCoverage[CoverageCd = 'UN']/Limit[LimitAppliesToCd='UNDPD']/FormatCurrencyAmt/Amt"/>
	    </xsl:when>
		<xsl:otherwise>
	<!-- Issue 67226 - END -->
      <xsl:value-of select="Option[OptionCd='com.csc_MaxAmtPerDay']/OptionValue" />
		</xsl:otherwise><!-- Issue 67226 -->
	  </xsl:choose><!-- Issue 67226 -->
    </BYUSVA4>
	<BYUSVA5>
	 <xsl:choose>
		<xsl:when test="$CovCode = 'UN' and Option[OptionCd='com.csc_UNType']/OptionValue!='CSL'">
			<xsl:value-of select="Limit[LimitAppliesToCd='UNDSG']/FormatCurrencyAmt/Amt"/>
		</xsl:when>	
		<xsl:otherwise>
		</xsl:otherwise>
	 </xsl:choose>
	</BYUSVA5>
	<!--<BYAENB>
		<xsl:choose>
			<xsl:when test="$InsLine = 'CA' and $CovCode = 'UNPD'">
				<xsl:value-of select="$UnitNbr"/>
		    </xsl:when>
		    <xsl:when test="$InsLine = 'CA' and $CovCode = 'UM'">
				<xsl:value-of select="$UnitNbr"/>
		    </xsl:when>
		<xsl:otherwise/>
		</xsl:choose>
	</BYAENB>-->
	<BYANCD>
	 <xsl:choose>	
		<xsl:when test="$CovCode = 'UN' and Option[OptionCd='com.csc_UNType']/OptionValue!='CSL'">
			<xsl:value-of select="Option[OptionCd='com.csc_UNType']/OptionValue" /> 
		</xsl:when>
		<xsl:otherwise>N</xsl:otherwise>	<!--Issue 59878-->
	 </xsl:choose>
	</BYANCD>
  </xsl:template>
  <xsl:template name="BuildTrailerCoverage">
    <xsl:param name="Location">0</xsl:param>
    <xsl:param name="SubLocation">0</xsl:param>
    <xsl:param name="InsLine"></xsl:param>
    <xsl:param name="RateState"></xsl:param>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ASBYCPL1</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ASBYCPL1__RECORD>
        <xsl:variable name="CoverageCd">
          <xsl:choose>
            <xsl:when test="CoverageCd='TCO'">TRLCOL</xsl:when>
            <xsl:when test="CoverageCd='TCP'">TRLCMP</xsl:when>
            <xsl:when test="CoverageCd='TSP'">TRLSP</xsl:when>
          </xsl:choose>
        </xsl:variable>
        <BYAACD>
          <xsl:value-of select="$LOC" />
        </BYAACD>
        <BYABCD>
          <xsl:value-of select="$MCO" />
        </BYABCD>
        <BYARTX>
          <xsl:value-of select="$SYM" />
        </BYARTX>
        <BYASTX>
          <xsl:value-of select="$POL" />
        </BYASTX>
        <BYADNB>
          <xsl:value-of select="$MOD" />
        </BYADNB>
        <BYAGTX>
          <xsl:value-of select="$InsLine" />
        </BYAGTX>
        <BYBRNB>
          <xsl:value-of select="$Location" />
        </BYBRNB>
        <BYEGNB>
          <xsl:value-of select="$SubLocation" />
        </BYEGNB>
        <BYANTX>
          <xsl:value-of select="$InsLine" />
        </BYANTX>
        <BYAENB>0</BYAENB>
        <BYAOTX>
          <xsl:value-of select="$CoverageCd" />
        </BYAOTX>
        <BYC0NB>0</BYC0NB>
        <BYC6ST>P</BYC6ST>
        <BYBCCD>
          <xsl:value-of select="$RateState" />
        </BYBCCD>
        <BYEETX>0I</BYEETX>
        <BYNDTX>
          <xsl:value-of select="../CommlVehSupplement/PrimaryClassCd" />
        </BYNDTX>
        <BYALCD>
          <xsl:value-of select="../RadiusCd" />
        </BYALCD>
        <BYPNTX>
          <xsl:value-of select="translate(Deductible/FormatCurrencyAmt/Amt,'$,,','')" />
        </BYPNTX>
        <BYPOTX>
          <!--<xsl:value-of select="/*/ZoneCombinationCd" />-->
	  <xsl:value-of select="../com.csc_NearZoneCd" />
        </BYPOTX>
        <BYPPTX>
          <xsl:value-of select="../FarthestTerminalZoneCd" />
        </BYPPTX>
        <BYBLNB>
          <xsl:value-of select="../NumTrailers" />
        </BYBLNB>
        <BYUSNB3>
          <xsl:value-of select="../NumDays" />
        </BYUSNB3>
        <BYUSVA3>
          <xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" />
        </BYUSVA3>
<!-- Issue '69073 begin -->
	<BYAL4LTXT><xsl:value-of select="CommlCoverage/Option[OptionCd='com.csc_ExclusionOption1']/OptionValue" /></BYAL4LTXT>
<!--Issue '69073 end -->			
      </ASBYCPL1__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->