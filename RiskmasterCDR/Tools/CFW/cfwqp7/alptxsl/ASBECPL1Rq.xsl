<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="BuildForms">
<xsl:param name="InsLine"/>
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBECPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBECPL1__RECORD>
<BEAACD><xsl:value-of select="$LOC"/></BEAACD>
<BEABCD><xsl:value-of select="$MCO"/></BEABCD>
<BEARTX><xsl:value-of select="$SYM"/></BEARTX>
<BEASTX><xsl:value-of select="$POL"/></BEASTX>
<BEADNB><xsl:value-of select="$MOD"/></BEADNB>
<BEAGTX><xsl:value-of select="$InsLine"/></BEAGTX>
<xsl:variable name="BEB7NB">
	<xsl:call-template name="FormatData">
	<xsl:with-param name="FieldName">$BEB7NB</xsl:with-param>      
	<xsl:with-param name="FieldLength">5</xsl:with-param>
	<xsl:with-param name="Value" select="IterationNumber"/>
	<xsl:with-param name="FieldType">N</xsl:with-param>
	</xsl:call-template>      
</xsl:variable>
<BEB7NB><xsl:value-of select="$BEB7NB"/></BEB7NB>
<BEAAST>P</BEAAST>
<BEANDT>0000000</BEANDT>
<BEB8NB><xsl:value-of select="FormNumber" /></BEB8NB>
<BEAMDT>
	<xsl:choose>
		<xsl:when test="substring(EditionDt,4,1) = '0'">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="substring(EditionDt,4,2)" />
	<xsl:value-of select="substring(EditionDt,1,2)" />01</BEAMDT>
<xsl:variable name="BEB9NB">
	<xsl:call-template name="FormatData">
	<xsl:with-param name="FieldName">$BEB9NB</xsl:with-param>      
	<xsl:with-param name="FieldLength">5</xsl:with-param>
	<xsl:with-param name="Value" select="substring(@id,2,string-length(@id)-1)"/>
	<xsl:with-param name="FieldType">N</xsl:with-param>
	</xsl:call-template>      
</xsl:variable>
<BEB9NB><xsl:value-of select="$BEB9NB"/></BEB9NB>
<BEDUST>6</BEDUST>
<BEADTX>CPP</BEADTX>
<xsl:variable name="BEBRNB">
	<xsl:call-template name="FormatData">
	<xsl:with-param name="FieldName">$BEBRNB</xsl:with-param>      
	<xsl:with-param name="FieldLength">5</xsl:with-param>
	<xsl:with-param name="Value" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
	<xsl:with-param name="FieldType">N</xsl:with-param>
	</xsl:call-template>      
</xsl:variable>
<BEBRNB><xsl:value-of select="$BEBRNB"/></BEBRNB>
<xsl:variable name="BEEGNB">
	<xsl:call-template name="FormatData">
	<xsl:with-param name="FieldName">$BEEGNB</xsl:with-param>      
	<xsl:with-param name="FieldLength">5</xsl:with-param>
	<xsl:with-param name="Value" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
	<xsl:with-param name="FieldType">N</xsl:with-param>
	</xsl:call-template>      
</xsl:variable>
<BEEGNB><xsl:value-of select="$BEEGNB"/></BEEGNB>
<FORMDESC><xsl:value-of select="FormName" /></FORMDESC> <!-- Issue 59878 -->
</ASBECPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->