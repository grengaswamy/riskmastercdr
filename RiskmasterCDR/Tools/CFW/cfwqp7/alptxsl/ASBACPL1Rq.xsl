<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/ACORD/InsuranceSvcRq/*" mode="ASBACPL1">
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBACPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBACPL1__RECORD>
<BAAACD><xsl:value-of select="$LOC"/></BAAACD>
<BAABCD><xsl:value-of select="$MCO"/></BAABCD>
<BAARTX><xsl:value-of select="$SYM"/></BAARTX>
<BAASTX><xsl:value-of select="$POL"/></BAASTX>
<BAADNB><xsl:value-of select="$MOD"/></BAADNB>
<BAC6ST>P</BAC6ST>
<BAAFNB>0</BAAFNB>
<!-- 34770 start --> 
<!--<BATYPE0ACT>NB</BATYPE0ACT>-->
<BATYPE0ACT>	
	<!--103409 starts-->
	<!--<xsl:choose>
	--><!--<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
	<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
	<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
	<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
	<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
	<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
</xsl:choose>-->
<xsl:call-template name="ConvertPolicyStatusCd">
	<xsl:with-param name="PolicyStatusCd">
		<xsl:value-of select="//PolicyStatusCd"/>
	</xsl:with-param>
</xsl:call-template>
<!--103409 Ends-->
</BATYPE0ACT>
<!-- 34770 end -->
<BAAUPC>9.99999</BAAUPC>
<!-- 36920 start -->
<xsl:choose>
<xsl:when test="$LOB='BOP'"> 
	<BACZST><xsl:value-of select="CommlPolicy/LOBSubCd"/></BACZST>
</xsl:when>
<!-- 59878 - Starts-->
<xsl:when test="$LOB='CPP'"> 
	<BACZST><xsl:value-of select="CommlPolicy/com.csc_ProgramCode"/></BACZST>
</xsl:when>
<!-- 59878 - ends-->
<xsl:otherwise> 
	<BACZST>S</BACZST>
</xsl:otherwise>
</xsl:choose>
<!-- 36920 end -->
<BAAKCD>
<xsl:choose>
<xsl:when test="CommlPolicy/CommlPolicySupplement/PolicyTypeCd and $LOB!='BOP'">
<xsl:value-of select="CommlPolicy/CommlPolicySupplement/PolicyTypeCd" />
</xsl:when>
<xsl:when test="$LOB='HP'">D</xsl:when>
<xsl:when test="$LOB='BOP'">BOP</xsl:when>
<!-- 39111 -->
<xsl:when test="$LOB='FP'">D</xsl:when>
<!-- 39111 -->
<!-- 59878 - Starts-->
<xsl:when test="$LOB='CPP'">														
	<xsl:value-of select="CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd" />	
</xsl:when>
<!-- 59878 - ends-->
<xsl:otherwise>MONO</xsl:otherwise>
</xsl:choose>
</BAAKCD>
<BAAEPC>1.000</BAAEPC>
<BAAFPC>1.000</BAAFPC>
<BAH5TX><xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/LegalEntityCd" /></BAH5TX>
<BAH6TX><xsl:value-of select="CommlPolicy/CommlPolicySupplement/OperationsDesc" /></BAH6TX>
<!-- 59878 starts-->
<xsl:if test="$LOB!='CPP'">
<BAI1TX>N</BAI1TX>
<BAI0TX>N</BAI0TX>
<BAIYTX></BAIYTX>
<BAAMCD></BAAMCD>
<BAA5DT></BAA5DT>
<BAALCD></BAALCD>
</xsl:if>
<!-- 59878 ends-->
<!--65774 Starts -->
<xsl:if test = "$LOB='HP'">
<!--<BAIGNB><xsl:value-of select = "/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/InsuredOrPrincipalInfo/CreditScore" /></BAIGNB>-->
<BAIGNB><xsl:value-of select = "normalize-space(/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/InsuredOrPrincipalInfo/CreditScore)"/></BAIGNB>
<BAIHNB><xsl:value-of select = "/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/InsuredOrPrincipalInfo/CreditScore" /></BAIHNB>
</xsl:if>
<xsl:if test = "$LOB='FP'">
<BAIGNB><xsl:value-of select = "normalize-space(/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/InsuredOrPrincipalInfo/CreditScore)"/></BAIGNB>
<BAIHNB><xsl:value-of select = "/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/InsuredOrPrincipalInfo/CreditScore" /></BAIHNB>
</xsl:if>	
<!--65774 Ends -->
</ASBACPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->