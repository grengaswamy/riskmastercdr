<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateUnitRec">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBJCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBJCPL1__RECORD>
            <BJAACD>
               <xsl:value-of select="$LOC"/>
            </BJAACD>
            <BJABCD>
               <xsl:value-of select="$MCO"/>
            </BJABCD>
            <BJARTX>
               <xsl:value-of select="$SYM"/>
            </BJARTX>
            <BJASTX>
               <xsl:value-of select="$POL"/>
            </BJASTX>
            <BJADNB>
               <xsl:value-of select="$MOD"/>
            </BJADNB>
            <BJAGTX>
			     <xsl:value-of select="$LOB"/>  
            </BJAGTX>
            <BJBRNB>00001</BJBRNB>
            <BJEGNB>00001</BJEGNB>
            <BJANTX>
			<!--<xsl:value-of select="$SYM"/>--> <!--issue # 40090-->
               <xsl:value-of select="$LOB"/>   <!--issue # 40090-->
            </BJANTX>
            <BJAENB>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BJAENB</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="ItemIdInfo/InsurerId"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>
            </BJAENB>
            <BJC6ST>P</BJC6ST>
            <BJTYPE0ACT>	
		<!-- 34771 start -->
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
		<!-- 34771 end -->
	    </BJTYPE0ACT>
            <BJBCCD>
               <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/>
            </BJBCCD>
            <BJAXNB>
		<xsl:value-of select="NumCylinders"/>
            </BJAXNB>
            <BJAGNB>
               <xsl:value-of select="TerritoryCd"/>
            </BJAGNB>
            <BJAHNB>
               <xsl:value-of select="ModelYear"/>
            </BJAHNB>
            <BJAXTX>
               <xsl:value-of select="concat(Manufacturer, ' ', Model)"/>
            </BJAXTX>
            <BJAYTX>
               <xsl:value-of select="VehIdentificationNumber"/>
            </BJAYTX>
            <BJAZTX>
               <xsl:value-of select="VehBodyTypeCd"/>
            </BJAZTX>
            <BJACST>
               <xsl:value-of select="VehPerformanceCd"/>
            </BJACST>
            <BJADST>
		<xsl:choose>
			<xsl:when test="string-length(com.csc_VehBodyTypeFreeformInd)='0'">
				<xsl:value-of select="string('S')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="com.csc_VehBodyTypeFreeformInd"/>
			</xsl:otherwise>
		</xsl:choose>        
            </BJADST>
            <BJAINB>
               <xsl:value-of select="CostNewAmt/Amt"/>
            </BJAINB>
            <BJAJNB>
               <xsl:value-of select="VehSymbolCd"/>
            </BJAJNB>
            <xsl:variable name="VehNbr" select="ItemIdInfo/InsurerId"/>
              <xsl:variable name="DriverNbr" select="substring(@RatedDriverRef,2,string-length(@RatedDriverRef)-1)"/>                         
            <xsl:choose>
              <xsl:when test="$DriverNbr">
            <BJAKNB><xsl:value-of select="$DriverNbr"/></BJAKNB>
            </xsl:when>
            <xsl:otherwise>
            <xsl:variable name="DriverVeh" select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@VehRef,2,string-length(@VehRef)-1)=$VehNbr) and (UsePct &gt; '50')]"/>
            <xsl:variable name="DriverRef" select="$DriverVeh/@DriverRef"/>
            <BJAKNB>
               <xsl:value-of select="substring($DriverRef,2,string-length($DriverRef)-1)"/>
            </BJAKNB>
            </xsl:otherwise>
            </xsl:choose>           
            <BJAMNB>
               <xsl:value-of select="DistanceOneWay/NumUnits"/>
            </BJAMNB>
            <BJANNB>
               <xsl:value-of select="NumDaysDrivenPerWeek"/>
            </BJANNB>
            <BJAONB>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BJAONB</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="EstimatedAnnualDistance/NumUnits"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
             </BJAONB>
             <BJAVCD>
               <xsl:value-of select="RateClassCd"/>
			 </BJAVCD>
					 <!--SYS/DST Issue # 347 Starts-->
					 <BJAWCD>
						 <xsl:choose>
							 <xsl:when test="count(/ACORD/InsuranceSvcRq/*/PersAutoLineBusiness/PersVeh) = 1">10</xsl:when>
							 <xsl:otherwise>20</xsl:otherwise>
						 </xsl:choose>
					 </BJAWCD>
					 <!--SYS/DST Issue # 347 Ends-->
            <BJA2TX>
               <xsl:value-of select="substring(VehIdentificationNumber,1,10)"/>
            </BJA2TX>
            <BJCXST>
               <xsl:value-of select="AirBagTypeCd"/>
            </BJCXST>
            <BJI6TX>
               <xsl:value-of select="AntiTheftDeviceCd"/>
            </BJI6TX>
            <BJCYST>
               <xsl:value-of select="AntiLockBrakeCd"/>
            </BJCYST>
            <BJC5ST>
               <xsl:value-of select="VehUseCd"/>
            </BJC5ST>
            <BJI4TX>
               <xsl:value-of select="DaytimeRunningLightInd"/>
            </BJI4TX>
            <!--begin 27834-->
             <xsl:if test="string-length(@RatedDriverRef) &gt; 0">
            <BJANCD><xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@VehRef,2,string-length(@VehRef)-1)=$VehNbr and substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr)]/UsePct"/></BJANCD>
            </xsl:if>
            <!--end 27834-->                         
         </ASBJCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->