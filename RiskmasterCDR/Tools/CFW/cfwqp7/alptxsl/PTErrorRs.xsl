<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="PointErrorsRs">
	    <xsl:choose>
          <!--  <xsl:when test="count(//ERRORREC__RECORD) > 0 or count(//ERROR__LST__ROW/ERROR__LST[ERROR__CODE != '']) > 0">-->
          <!-- 34771 start-->
			<xsl:when test="count(/*/ERRORREC__RECORD) > 0"> <!-- 62825 -->
           		<MsgStatus>
           			<MsgStatusCd><xsl:text>ERROR</xsl:text>	</MsgStatusCd>
           			<!-- Case 31594 Begin -->
           			<!--<MsgErrorCd></MsgErrorCd>-->
           			<xsl:variable name="ErrorCd" select="/com.csc_PolicySyncRs/ERROR__LST__ROW/ERROR__LST/fill_0"/>
           			<MsgErrorCd><xsl:value-of select="substring($ErrorCd, 2, 2)"/></MsgErrorCd>
           			<!-- Case 31594 End -->
           	    	<!--	<MsgStatusDesc>Error(s) occurred.</MsgStatusDesc>    	34771 added	-->
           	    	<MsgStatusDesc>Rating Error(s) occurred.</MsgStatusDesc>
 <!-- Fatal Errors -->
            		<xsl:for-each select="/*/ERROR__LST__ROW/ERROR__LST[ERROR__CODE != '']">
        		        <ExtendedStatus>
                            <ExtendedStatusCd/>
                            <ExtendedStatusDesc><xsl:value-of select="ERROR__MSG"/></ExtendedStatusDesc>
            		    </ExtendedStatus>
            		</xsl:for-each>
<!-- Rating Errors -->
<!-- Case 27591-->
            		<xsl:for-each select="/*/ERRORREC__RECORD">
        		         <ExtendedStatus>
                            <ExtendedStatusCd/>
                            <ExtendedStatusDesc><xsl:value-of select="ERRDESC"/></ExtendedStatusDesc>
            		   </ExtendedStatus>
        	    	</xsl:for-each>
        		</MsgStatus>
            </xsl:when>
            	<xsl:when test="count(//ERROR__LST__ROW/ERROR__LST[ERROR__CODE != '']) > 0"> 
			<MsgStatus>
           			<MsgStatusCd><xsl:text>ERROR</xsl:text>	</MsgStatusCd>
					<xsl:variable name="ErrorCd" select="/com.csc_PolicySyncRs/ERROR__LST__ROW/ERROR__LST/fill_0"/>
           			<MsgErrorCd><xsl:value-of select="substring($ErrorCd, 2, 2)"/></MsgErrorCd>
					<MsgStatusDesc>Error(s) occurred.</MsgStatusDesc>
					<xsl:for-each select="/*/ERROR__LST__ROW/ERROR__LST[ERROR__CODE != '']">
        		        <ExtendedStatus>
                            <ExtendedStatusCd><xsl:value-of select="ERROR__CODE"/></ExtendedStatusCd><!--Issue 99300 Started to fetch ERROR_CODE in ExtendedStatusCd -->
                            <ExtendedStatusDesc><xsl:value-of select="ERROR__MSG"/></ExtendedStatusDesc>
            		    </ExtendedStatus>
					</xsl:for-each>
			</MsgStatus>
			</xsl:when>
				<xsl:when test="count(//ERROR__LST__ROW/ERROR__LST[ERROR__MSG != '' and ERROR__SWITCH ='Y']) > 0"> 
				<MsgStatus>
           			<MsgStatusCd><xsl:text>ERROR</xsl:text>	</MsgStatusCd>
					<xsl:variable name="ErrorCd" select="/com.csc_PolicySyncRs/ERROR__SEVERITY"/>
           			<MsgErrorCd><xsl:value-of select="$ErrorCd"/></MsgErrorCd>
					<MsgStatusDesc>Error(s) occurred.</MsgStatusDesc>
					<xsl:for-each select="/*/ERROR__LST__ROW/ERROR__LST[ERROR__MSG != '']">
        		        <ExtendedStatus>
                            <ExtendedStatusCd/>
                            <ExtendedStatusDesc><xsl:value-of select="ERROR__MSG"/></ExtendedStatusDesc>
            		    </ExtendedStatus>
					</xsl:for-each>
			</MsgStatus>
				</xsl:when>
    		<xsl:otherwise>
        		<MsgStatus>
        		<!-- 55614
        		   <MsgStatusCd><xsl:text>INFO</xsl:text></MsgStatusCd>
        			<MsgErrorCd>0</MsgErrorCd>
        			<MsgStatusDesc>Success</MsgStatusDesc>
        		-->
        			<MsgStatusCd>Success</MsgStatusCd>
        			<MsgErrorCd>0</MsgErrorCd>
        			<MsgStatusDesc>Success</MsgStatusDesc>
        		</MsgStatus>
            </xsl:otherwise>
        </xsl:choose>   
	</xsl:template>
</xsl:stylesheet>

			<!--
            <xsl:when test="count(//ERRORREC__RECORD) > 0 or count(//ERROR__LST__ROW/ERROR__LST[ERROR__CODE != '']) > 0">
           		<MsgStatus>
           			<MsgStatusCd><xsl:text>ERROR</xsl:text>	</MsgStatusCd>
           			 Case 31594 Begin 
           			<MsgErrorCd></MsgErrorCd>
           			<xsl:variable name="ErrorCd" select="/com.csc_PolicySyncRs/ERROR__LST__ROW/ERROR__LST/fill_0"/>
           			<MsgErrorCd><xsl:value-of select="substring($ErrorCd, 2, 2)"/></MsgErrorCd>
           			 Case 31594 End 
           	    		<MsgStatusDesc>Error(s) occurred.</MsgStatusDesc>    	
			-->

			<!-- 34771 end -->