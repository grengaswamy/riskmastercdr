<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateISLPPP1400">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ISLPPP1400</RECORD__NAME>
         </RECORD__NAME__ROW>
          
         <ISLPPP1400__RECORD>
         	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
         	<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
         	<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
         	<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
         	<MODULE><xsl:value-of select="$MOD"/></MODULE>
         </ISLPPP1400__RECORD>
         
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
