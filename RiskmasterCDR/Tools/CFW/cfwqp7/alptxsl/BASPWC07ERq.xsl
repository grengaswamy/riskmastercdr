<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="CreateBASPWC07EDefaults">
	<BUS__OBJ__RECORD>
		<RECORD__NAME__ROW>
			<RECORD__NAME>BASPWC07E</RECORD__NAME>
		</RECORD__NAME__ROW>
		<BASPWC07E__RECORD>
			<WCSTATUS>P</WCSTATUS>
			<SYMBOL>
				<xsl:value-of select="$SYM"/>
			</SYMBOL>
			<POLICYNO>
				<xsl:value-of select="$POL"/>
			</POLICYNO>
			<MODULE>
				<xsl:value-of select="$MOD"/>
			</MODULE>
			<MASTERCO>
				<xsl:value-of select="$MCO"/>
			</MASTERCO>
			<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
			<STATE><xsl:value-of select="StateProvCd"/></STATE>
			<GRPCODE><xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_GroupProgram']/OtherId"/></GRPCODE>
			<RISKID><xsl:value-of select="NCCIIDNumber"/></RISKID>
			<EMODCONF></EMODCONF>
	</BASPWC07E__RECORD>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet> <!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->