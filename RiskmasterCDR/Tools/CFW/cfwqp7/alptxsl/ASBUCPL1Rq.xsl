<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="CreateLocation">
<xsl:param name="Location"/>
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBUCPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBUCPL1__RECORD>
<BUAACD><xsl:value-of select="$LOC"/></BUAACD>
<BUABCD><xsl:value-of select="$MCO"/></BUABCD>
<BUARTX><xsl:value-of select="$SYM"/></BUARTX>
<BUASTX><xsl:value-of select="$POL"/></BUASTX>
<BUADNB><xsl:value-of select="$MOD"/></BUADNB>
<BUBRNB><xsl:value-of select="$Location" /></BUBRNB>
<BUC6ST>P</BUC6ST>
<!-- 34770 start --> 
<!--<BUTYPE0ACT>NB</BUTYPE0ACT>-->
            <BUTYPE0ACT>
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
	    </BUTYPE0ACT>
<!-- 34770 end -->
<BUEFTX><xsl:value-of select="Addr/Addr1" /></BUEFTX>
<BUEGTX><xsl:value-of select="Addr/Addr2" /></BUEGTX>
<BUEITX><xsl:value-of select="Addr/City" /></BUEITX>
<BUEJTX><xsl:value-of select="Addr/StateProvCd" /></BUEJTX>
<BUAPNB><xsl:value-of select="Addr/PostalCode" /></BUAPNB>
</ASBUCPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

