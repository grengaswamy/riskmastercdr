<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 14734 
***********************************************************************************************
-->

  <xsl:include href="CommonFuncRq.xsl"/>
  <!-- Common Template Routines -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP0Rq.xsl"/> -->
  <xsl:include href="PMSP0000Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 0 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP1Rq.xsl"/> -->
  <xsl:include href="PMSP0200Rq.xsl"/>
  <xsl:include href="PMSPWC08Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 1 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP2Rq.xsl"/> -->
  <xsl:include href="PMSP1200Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 2 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP3Rq.xsl"/>-->
  <xsl:include href="PMSPWC04Rq.xsl"/>
  <xsl:include href="PMSPWC07Rq.xsl"/> 
  <xsl:include href="PMSPSA05Rq.xsl"/>
  <xsl:include href="PMSPSA06Rq.xsl"/>
  <xsl:include href="PMSPWC12Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 3 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP4Rq.xsl"/> -->
  <xsl:include href="PMSPSA15Rq.xsl"/>
  <xsl:include href="PMSPSA35Rq.xsl"/>
  <xsl:include href="PMSPSA36Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 4 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP5Rq.xsl"/> -->
  <xsl:include href="PMSPWC25Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 5 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP6Rq.xsl"/> -->
  <xsl:include href="PMSPWC02Rq.xsl"/>
  <xsl:include href="PMSPWC03Rq.xsl"/>
  <!-- 29653 End   -->
   <!-- WC PPP 6 record -->
  <!-- Case 39671 Start -->
  <xsl:include href="PTInfoRq.xsl"/>
  <!-- Case 39568 End   -->
  <!-- 57418 start -->
  <xsl:include href="BASP0200ERq.xsl"/>
  <!-- 57418 start -->
  <xsl:include href="BASPWC07ERq.xsl"/><!-- 94178 -->
  <xsl:include href="PMSPWC15Rq.xsl"/> <!-- 103186 -->

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
  <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
  <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/NAICCd"/>
  <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
  <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/CompanyProductCd"/>
  <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/PolicyNumber"/>
  <xsl:variable name="MOD">
    <xsl:call-template name="FormatData">
      <xsl:with-param name="FieldName">$MOD</xsl:with-param>
      <xsl:with-param name="FieldLength">2</xsl:with-param>
      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/PolicyVersion"/>
      <xsl:with-param name="FieldType">N</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/LOBCd"/>
  <!-- 31594 start -->
  <xsl:variable name="TYPEACT">
  	<xsl:choose>
		<!--103409 Starts-->
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">QQ</xsl:when>
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='R'">RB</xsl:when>	--><!--50764 -->

		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">QQ</xsl:when>
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='R'">RB</xsl:when>
		<!--103409 Ends-->
		<xsl:otherwise>EN</xsl:otherwise>
  	</xsl:choose>
  </xsl:variable> 
  <!-- 31594 end -->
  <xsl:template match="/">

    <xsl:element name="WorkCompPolicyQuoteInqRq">
      <!-- 39568 Start -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="CreatePTInfo"/> 
      <!-- 39568 End   -->
      <!-- WC PPP 0 record -->
      <!-- 29653 Start -->
      <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq"> -->
           <!--<xsl:call-template name="CreateISLPPP0"/> -->
      	   <!--<xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="PMSP0000"/> -->
           <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy" mode="PMSP0000"/> 
      <!--</xsl:for-each> -->
      <!-- 29653 End   -->
	   
	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->

      
      <!-- WC PPP 1 record -->
      <!-- 29653 Start -->
       <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq"> -->
        <!--<xsl:call-template name="CreateISLPPP1">
          <xsl:with-param name="Action">Quote</xsl:with-param>
        </xsl:call-template> -->
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="PMSP0200"/> 
        
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="PMSPWC02"/> 
       <!-- 64786 Start -->
        <!--<xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="PMSPWC08"/> -->
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="CreateEmpLimits"/>
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq" mode="CreateVolLimits"/>
       <!-- 64786 End -->
	   	 <!-- issue # 61710 and 111420 starts-->
	  <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/InsuredOrPrincipal">
	  	<xsl:if test="normalize-space(GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo[SupplementaryNameCd='DBA']/SupplementaryName) != ''">
			<xsl:variable name="LocationNbr"> 
				<xsl:choose>
				<xsl:when test="substring(@id, 2,string-length(@id)-1) = '0'">
					<xsl:value-of select="string('1')"/>  
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring(@id, 2,string-length(@id)-1)"/>  
				</xsl:otherwise> 
				</xsl:choose> 
			</xsl:variable>	
            <xsl:call-template name="CreateDBARecord">
	 			<xsl:with-param name="Location" select="$LocationNbr"/>
			</xsl:call-template>
       	</xsl:if>
      </xsl:for-each>
	<!-- issue # 61710 and 111420 Ends-->
       <!--</xsl:for-each>  -->
  	   <!-- 29653 End -->
      <!-- WC PPP 2 record : Additional Interests -->
      <!-- 29653 Start -->
      <!--<xsl:call-template name="CreateISLPPP2"/> -->
	  <!-- 103186 Added a call for CreatePMSPWC15 template -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location">
         <xsl:variable name="Location" select="substring(@id, 2,string-length(@id)-1)"/>
         <xsl:for-each select="AdditionalInterest">
            <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
            <xsl:call-template name="CreateAddlInterest">
                <xsl:with-param name="Location" select="$Location"/>    
	        </xsl:call-template>    
            </xsl:if>
			<xsl:if test="(substring(@id,1,1) = 'i')">
				<xsl:call-template name="CreatePMSPWC15"/>
			</xsl:if>
         </xsl:for-each>
      </xsl:for-each>
      <!-- 29653 End  -->
	  <!--103409 Starts-->
	   	<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompIndividuals">
       	<xsl:variable name="Location" select="substring-after(@id, 'l')"/>
       	<xsl:if test="string-length(NameInfo/com.csc_LongName)">
       		<xsl:call-template name="CreateWorkCompIndivudual">
       			<xsl:with-param name="Location" select="$Location"/>    
       		</xsl:call-template>    
       	</xsl:if>		
		<xsl:choose>
			<xsl:when test="com.csc_OtherIdentifier[OtherIdTypeCd = 'Deemed']"/>
			<xsl:otherwise>
				<xsl:call-template name="CreatePMSPWC15WorkCompIndividual"/>
			</xsl:otherwise>
		</xsl:choose>		
      </xsl:for-each>
	  <!--103409 Ends-->
      <!-- WC PPP 3 record : Locations -->
	  <!-- <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState"> Case 31594 -->
	  <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo"> <!--Case 31594 -->
      <!-- 29653 Start -->
        <!--<xsl:call-template name="CreateISLPPP3Defaults"/>-->
        <xsl:call-template name="CreatePMSPWC04Defaults"/>
        <xsl:call-template name="CreatePMSPWC07Defaults"/> 
        <xsl:call-template name="CreatePMSPSA05Defaults"/>
        <xsl:call-template name="CreatePMSPSA06Defaults"/>
        <xsl:call-template name="CreatePMSPWC12"/>
      <!-- 29653 End   -->
      </xsl:for-each>
      <!-- WC PPP 4 record : Class codes -->
      <!-- Start Case 33789 -->
      <!--xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass"-->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo">
		<xsl:for-each select="WorkCompRateClass">
		<!-- 29653 Start -->
			<!--<xsl:call-template name="CreateISLPPP4">
			  <xsl:with-param name="Action">Quote</xsl:with-param>
			</xsl:call-template> -->
			<xsl:call-template name="CreatePMSPSA15">
			  <xsl:with-param name="Action">Quote</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="CreatePMSPSA35">
			  <xsl:with-param name="Action">Quote</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="CreatePMSPSA36">
			  <xsl:with-param name="Action">Quote</xsl:with-param>
			</xsl:call-template>
			<!-- 29653 End -->
		</xsl:for-each>		
      </xsl:for-each>
      <!-- End Case 33789 -->
      <!-- WC PPP 5 record : Optional Mods -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge">
        <!-- The Experience mods are listed in this section, but should not be send as
        optional mods. -->
        <xsl:if test="(CreditSurchargeCd != 'EXP1') and (CreditSurchargeCd != 'EXP2') and (CreditSurchargeCd != 'ARAP') ">	<!-- Issue 62152 Add test for ARAP and not 0 -->
			<!-- Issue 94177 Removed NumericValue/FormatModFactor-->
        <!-- 29653 Start -->
          <!--<xsl:call-template name="CreateISLPPP5"/>-->
			<!-- Issue 94177 Starts-->
			<xsl:if test="(normalize-space(CreditSurchargeCd = 'RPAT') or (CreditSurchargeCd = 'ENDM'))">
				<xsl:if test ="(CreditSurchargeCd != 'EMOD')">
          <xsl:call-template name="CreatePMSPWC25"/>
				</xsl:if>					
			</xsl:if>
			<xsl:if test ="(NumericValue/FormatModFactor != '0')">
				<xsl:call-template name="CreatePMSPWC25"/>
			</xsl:if>
			<!-- Issue 94177 Ends-->
          <!-- 29653 End --> 
        </xsl:if>
      </xsl:for-each>
      <!-- Issue 104053 Starts -->
          <!-- WC PPP 6 record : Optional Forms -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy">
          <xsl:call-template name="CreatePMSPWC03">
            <xsl:with-param name="FormLevel">Policy</xsl:with-param>
          </xsl:call-template>
		  </xsl:for-each>
      <!-- Issue 104053 Ends -->
      <!-- WC PPP 6 record : Optional Forms -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState">
          <!-- 29653 Start -->
           <!--         <xsl:if test="string-length(com.csc_Form[1]/FormNumber) &gt; 0"> -->
          <!--<xsl:call-template name="CreateISLPPP6">
            <xsl:with-param name="FormLevel">State</xsl:with-param>
          </xsl:call-template> -->
		  <xsl:call-template name="CreatePMSPWC03">
            <xsl:with-param name="FormLevel">State</xsl:with-param>
          </xsl:call-template>
          <!-- 29653 End  -->
        <!--</xsl:if>-->
		<!-- 94178 Start -->
		<xsl:call-template name="CreateBASPWC07EDefaults"/>
		<!-- 94178 End -->
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario4" userelativepaths="yes" externalpreview="no" url="..\..\..\..\aaj ka log\322\82561284996212406250WCQuoteInqRq.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->