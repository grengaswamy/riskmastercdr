<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes" />
  <xsl:template name="CreatePropertySchedule">
    <xsl:if test="C4C0NB = '1'">
      <xsl:variable name="CoverageCd" select="C4AOTX" />
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>Y</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <!-- Issue 64220 Begin -->
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="format-number(sum(/*/ASC4CPL1__RECORD[C4C7ST!='Y' and C4AOTX=$CoverageCd]/C4BKVA), '###.00')" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <!-- Issue 64220 End -->
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="format-number(sum(/*/ASC4CPL1__RECORD[C4C7ST!='Y' and C4AOTX=$CoverageCd]/C4A3VA), '###.00')" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>Total Premium</ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <PropertySchedule>
      <xsl:attribute name="LocationRef">
         <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
      </xsl:attribute>
      <IsSummaryInd>N</IsSummaryInd>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="C4AOTX" />
        </CoverageCd>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="C4BKVA" />
            </Amt>
          </FormatCurrencyAmt>
        </Limit>
        <CurrentTermAmt>
          <Amt>
            <xsl:value-of select="C4A3VA" /> <!-- Issue 64220 -->
          </Amt>
        </CurrentTermAmt>
      </Coverage>
      <ItemDefinition>
        <ItemTypeCd>ScheduledProperty</ItemTypeCd>
        <ItemDesc>
          <!-- Issue 64220 Begin -->
          <xsl:choose>
            <xsl:when test="string-length(C4RPTX) &gt; 0">
              <xsl:value-of select="C4RPTX" />
            </xsl:when>
        	  <xsl:otherwise>
              <xsl:value-of select="C4IRTX" />
        	  </xsl:otherwise>
          </xsl:choose>
          <!-- Issue 64220 End -->
        </ItemDesc>
      </ItemDefinition>
    </PropertySchedule>
    <xsl:if test="normalize-space(C4BLVA) &gt; '0.00'">		<!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BLVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4BSVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RQTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BMVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BMVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4BTVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RRTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BNVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BNVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4AGVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RSTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BOVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BOVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4AHVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4H5TX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BPVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BPVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4CPVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4H6TX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BQVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BQVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4CQVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4IRTX" />
            <xsl:value-of select="C4IVTX" />
            <xsl:value-of select="C4RUTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4BMVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4BMVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4CRVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4ISTX" />
            <xsl:value-of select="C4IWTX" />
            <xsl:value-of select="C4B9CD" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4CNVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CNVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4CSVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4ITTX" />
            <xsl:value-of select="C4IXTX" />
            <xsl:value-of select="C4CACD" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="normalize-space(C4COVA) &gt; '0.00'"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4COVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
          <CurrentTermAmt>
            <Amt>
              <xsl:value-of select="C4CTVA" />
            </Amt>
          </CurrentTermAmt>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4IUTX" />
            <xsl:value-of select="C4RTTX" />
            <xsl:value-of select="C4CBCD" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(C4CACD)) &gt; 0"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CPVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RWTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(C4CBCD)) &gt; 0"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CQVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RXTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(C4CCCD)) &gt; 0"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CRVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RYTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(C4CDCD)) &gt; 0"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CSVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4RZTX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
    <xsl:if test="string-length(normalize-space(C4CECD)) &gt; 0"><!--Issue 64220 - Added normalize-space function-->
      <PropertySchedule>
        <xsl:attribute name="LocationRef">
           <!-- 55614 Begin <xsl:value-of select="concat('l',C4BRNB)" /> -->
          <xsl:value-of select="concat($KEY,'-l',C4BRNB)" />
          <!-- 55614 End -->
        </xsl:attribute>
        <IsSummaryInd>N</IsSummaryInd>
        <Coverage>
          <CoverageCd>
            <xsl:value-of select="C4AOTX" />
          </CoverageCd>
          <Limit>
            <FormatCurrencyAmt>
              <Amt>
                <xsl:value-of select="C4CTVA" />
              </Amt>
            </FormatCurrencyAmt>
          </Limit>
        </Coverage>
        <ItemDefinition>
          <ItemTypeCd>ScheduledProperty</ItemTypeCd>
          <ItemDesc>
            <xsl:value-of select="C4R0TX" />
          </ItemDesc>
        </ItemDefinition>
      </PropertySchedule>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
