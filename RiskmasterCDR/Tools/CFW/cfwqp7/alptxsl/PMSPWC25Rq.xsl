<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge (Mod) - 31594
-->
  <xsl:template name="CreatePMSPWC25">
    <xsl:variable name="TableName">PMSPWC25</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC25</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC25__RECORD>
		<WCSTATUS>P</WCSTATUS>		
		<SYMBOL>
          	<xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          	<xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          	<xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          	<xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          	<xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          	<xsl:value-of select="../StateProvCd"/>
        </STATE>
		<MODTYPE>
			<xsl:value-of select="CreditSurchargeCd"/>
		</MODTYPE>	
		<RATEOP>
			<xsl:value-of select="NumericValue/FormatInteger"/>
		</RATEOP>	
		<ENDEFFDATE>
			<xsl:call-template name="ConvertISODateToPTDate">
            	<xsl:with-param name="Value" select="*/ContractTerm/EffectiveDt"/>
            </xsl:call-template>
		</ENDEFFDATE>
		<OPRATESEQ>000</OPRATESEQ>
		<RATE>
			<!-- Issue # 94177 Start -->
			<xsl:choose>
				<xsl:when test ="(normalize-space(CreditSurchargeCd) = 'RPAT') or (normalize-space(CreditSurchargeCd) = 'ENDM')"> <!--Issue # 64227-->
				<!--<xsl:when test ="(normalize-space(CreditSurchargeCd = 'RPAT') or (CreditSurchargeCd = 'ENDM'))">--> <!--Issue # 64227-->
					<xsl:value-of select="string('0000')"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- Issue # 94177 End -->
			<xsl:if test="CreditSurchargeCd != 'INFF'">
        		<xsl:value-of select="NumericValue/FormatModFactor"/>
        	</xsl:if>
        	<xsl:if test="CreditSurchargeCd = 'INFF' 
        				  and string-length(NumericValue/FormatCurrencyAmt/Amt) = 0">
          		<xsl:value-of select="NumericValue/FormatModFactor"/>
          	</xsl:if>
					<!-- Issue # 94177 Start -->
				</xsl:otherwise>
			</xsl:choose>
			<!-- Issue # 94177 End -->
        </RATE>  	
		<RATE2>
			<!--<xsl:value-of select="NumericValue/com.csc_AnniversaryFactor"/>--><!--103409-->
			<xsl:value-of select="NumericValue/FormatPct"/><!--103409-->
		</RATE2>
		<FLATAMT>
			<xsl:call-template name="FormatData">
            	<xsl:with-param name="FieldName">FLATAMT</xsl:with-param>
            	<xsl:with-param name="FieldLength">12</xsl:with-param>
            	<xsl:with-param name="Value" select="NumericValue/FormatCurrencyAmt/Amt"/>
            	<xsl:with-param name="FieldType">N</xsl:with-param>
          	</xsl:call-template>
        </FLATAMT>  
		<DELPND>N</DELPND>	<!--** 65494 Added the N value -->
	  </PMSPWC25__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->
