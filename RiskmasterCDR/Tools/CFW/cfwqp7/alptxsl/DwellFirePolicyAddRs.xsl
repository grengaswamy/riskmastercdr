<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Point XML into ACORD XML to
return back to iSolutions   
E-Service case  
***********************************************************************************************-->
  <xsl:include href="CommonFuncRs.xsl" />
  <xsl:include href="PTSignOnRs.xsl" />
  <xsl:include href="PTErrorRs.xsl" />
  <xsl:include href="PTProducerRs.xsl" />
  <xsl:include href="PTInsuredOrPrincipalRs.xsl" />
  <xsl:include href="PTHPPersPolicyRs.xsl" />
  <xsl:include href="PTAdditionalInterestRs.xsl" />
  <xsl:include href="PTFormsRs.xsl" />
  <xsl:include href="PTCoverageRs.xsl" />
  <xsl:include href="PTDwellRs.xsl" />
  <xsl:include href="PTLocationRs.xsl" />
  <xsl:include href="PTSubLocationRs.xsl" />
  <xsl:include href="PTPropertyScheduleRs.xsl" /> <!-- Issue 64220 -->
  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes" />
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="BuildSignOn" />
      <InsuranceSvcRs>
        <RqUID />
        <DwellFirePolicyAddRs>
          <RqUID />
          <TransactionResponseDt />
          <TransactionEffectiveDt />
          <CurCd>USD</CurCd>
          <xsl:call-template name="PointErrorsRs" />
          <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer" />
          <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Person" />
          <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="PersPolicy" />
          <xsl:for-each select="/*/ASBUCPL1__RECORD">
            <xsl:call-template name="CreateLocation" />
          </xsl:for-each>
          <DwellFireLineBusiness>
            <LOBCd>FP</LOBCd>
            <xsl:for-each select="/*/ASBQCPL1__RECORD">
              <xsl:call-template name="CreateDwell" />
            </xsl:for-each>
            <xsl:for-each select="/*/ASC4CPL1__RECORD">
              <!-- Issue 64220 Begin -->
              <!--xsl:call-template name="CreateCoverage">
                <xsl:with-param name="LOB">HP</xsl:with-param>
              </xsl:call-template-->
              <!-- POINT does not offer a flag on the ASC4 to let us know when a coverage is 
                scheduled.  Therefore we must check the current coverage against the list of 
                those we know to be scheduled. -->
              <xsl:choose>                
                <xsl:when test="C4AOTX='CAMERA' or C4AOTX='COINS' or C4AOTX='F/A-1' or C4AOTX='FURS' or C4AOTX='GOLF' or C4AOTX='GUNS' or C4AOTX='JEWEL' or C4AOTX='MUSIC'">
                  <xsl:call-template name="CreatePropertySchedule" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="CreateCoverage">
                    <xsl:with-param name="LOB">FP</xsl:with-param>
                  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
              <!-- Issue 64220 Begin -->
            </xsl:for-each>
          </DwellFireLineBusiness>
        </DwellFirePolicyAddRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>
</xsl:stylesheet>
