<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template match="/*/PMSP0200__RECORD" mode="Producer">
    <Producer>
        <ItemIdInfo>
           <AgencyId><xsl:value-of select="FILLR1" />
              <xsl:value-of select="RPT0AGT0NR" />
              <xsl:value-of select="FILLR2" />
           </AgencyId>
        </ItemIdInfo>
		<!--Issue 102807 Starts-->
        <!--GeneralPartyInfo>
            <NameInfo>
                <CommlName>
                    <CommercialName/>
                    <SupplementaryNameInfo>
                        <SupplementaryNameCd/>
                        <SupplementaryName/>
                    </SupplementaryNameInfo>
                </CommlName>
                <LegalEntityCd/>
            </NameInfo>
            <Addr>
                <AddrTypeCd/>
                <Addr1/>
                <Addr2/>
                <Addr3/>
                <Addr4/> 
                <City/>
                <StateProvCd/>
                <PostalCode/>
                <CountryCd/>
            </Addr>
            <Communications>
                <PhoneInfo>
                    <PhoneTypeCd/>
                    <CommunicationUseCd/>
                    <PhoneNumber/>
                </PhoneInfo>
            </Communications>
        </GeneralPartyInfo-->
		<!--Issue 102807 ends-->
        <ProducerInfo>
           <ContractNumber><xsl:value-of select="../BASP0200E__RECORD/AGCYLNKUID"/></ContractNumber>
           <ProducerSubCode/>
        </ProducerInfo>
    </Producer>
    </xsl:template>
    
    <!-- 29653 Start -->
    <!--<xsl:template match="/*/ISLPPP1__RECORD" mode="WC"> -->
    <xsl:template match="/*/PMSP0200__RECORD" mode="WC">
    <!-- 29653 End   -->
    <Producer>
        <ItemIdInfo>
           <!-- 29653 Start -->
           <!--<AgencyId><xsl:value-of select="AGENTNO" /></AgencyId> -->
            <AgencyId><xsl:value-of select="FILLR1" />
              <xsl:value-of select="RPT0AGT0NR" />
              <xsl:value-of select="FILLR2" />
           <!-- 29653 End -->   
           </AgencyId>
        </ItemIdInfo>
        <GeneralPartyInfo>
            <NameInfo>
                <CommlName>
                    <CommercialName/>
                    <SupplementaryNameInfo>
                        <SupplementaryNameCd/>
                        <SupplementaryName/>
                    </SupplementaryNameInfo>
                </CommlName>
                <LegalEntityCd/>
            </NameInfo>
            <Addr>
                <AddrTypeCd/>
                <Addr1/>
                <Addr2/>
                <Addr3/>
                <Addr4/> 
                <City/>
                <StateProvCd/>
                <PostalCode/>
                <CountryCd/>
            </Addr>
            <Communications>
                <PhoneInfo>
                    <PhoneTypeCd/>
                    <CommunicationUseCd/>
                    <PhoneNumber/>
                </PhoneInfo>
            </Communications>
        </GeneralPartyInfo>
        <ProducerInfo>
           <ContractNumber/>
           <ProducerSubCode/>
        </ProducerInfo>
    </Producer>
    </xsl:template>
<!-- 55614 begin -->
<xsl:template match="/*/PMSPAGL0__RECORD" mode="Producer">
    <Producer>
        <ItemIdInfo>
           <AgencyId><xsl:value-of select="/*/PMSP0200__RECORD/FILLR1" />
              <xsl:value-of select="/*/PMSP0200__RECORD/RPT0AGT0NR" />
              <xsl:value-of select="/*/PMSP0200__RECORD/FILLR2" />
           </AgencyId>
           <SystemId><xsl:value-of select="AGNMAC"/></SystemId>
        </ItemIdInfo>
        <GeneralPartyInfo>
            <NameInfo>
                <CommlName>
                    <CommercialName><xsl:value-of select="AGNMNA"/></CommercialName>
                    <SupplementaryNameInfo>
                        <SupplementaryNameCd/>
                        <SupplementaryName/>
                    </SupplementaryNameInfo>
                </CommlName>
                <LegalEntityCd/>
            </NameInfo>
            <Addr>
                <AddrTypeCd/>
                <Addr1><xsl:value-of select="AGNMAD"/></Addr1>
                <Addr2/>
                <Addr3/>
                <Addr4/> 
                <City><xsl:value-of select="substring(AGNMCT,1,28)"/></City>
					<StateProvCd><xsl:value-of select="substring(AGNMCT,29,2)"/></StateProvCd>
					<PostalCode><xsl:value-of select="AGNMZI"/></PostalCode>
                <CountryCd/>
            </Addr>
            <Communications>
                <PhoneInfo>
                    <PhoneTypeCd/>
                    <CommunicationUseCd/>
                    <PhoneNumber><xsl:value-of select="AGNMTE"/></PhoneNumber>
                </PhoneInfo>
            </Communications>
        </GeneralPartyInfo>
        <ProducerInfo>
           <ContractNumber/>
           <ProducerSubCode><xsl:value-of select="../BASP0200E__RECORD/AGCYLNKUID"/></ProducerSubCode>
        </ProducerInfo>
    </Producer>
    </xsl:template>
<!-- 55614 end -->            
</xsl:stylesheet>        
