<?xml version='1.0'?>
<!--Created for Issue 103409-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template name="CreateWorkCompIndividuals">

	<xsl:variable name="Site" select="USE0LOC"/>
		<WorkCompIndividuals>
			<xsl:choose>
				<xsl:when test="normalize-space(DEEMSTAT)='D'">
					<xsl:attribute name="id">d<xsl:value-of select="number(DESC0SEQ)"/>l<xsl:value-of select="$Site"/></xsl:attribute>
				</xsl:when>
				<xsl:when test="normalize-space(DEEMSTAT)='N'">
					<xsl:attribute name="id">o<xsl:value-of select="number(DESC0SEQ)"/>l<xsl:value-of select="$Site"/></xsl:attribute>
				</xsl:when>
			</xsl:choose>			
			<IncludedExcludedCd>
				<xsl:value-of select="USE0CODE"/>
			</IncludedExcludedCd>
			<InclIndividualsEstAnnualRemunerationAmt>
				<Amt>
					<xsl:value-of select="OWNSALARY"/>
				</Amt>
			</InclIndividualsEstAnnualRemunerationAmt>
			<NameInfo>
				<PersonName>
				<Surname/>
				</PersonName>
				<TaxIdentity>
					<TaxIdTypeCd>SSN</TaxIdTypeCd>
					<TaxId>
						<xsl:value-of select="SSN"/>
					</TaxId>
				</TaxIdentity>
				<com.csc_LongName>
						<xsl:value-of select="DESC0LINE1"/>
				</com.csc_LongName>
			</NameInfo>			
			<xsl:choose>
				<xsl:when test="normalize-space(DEEMSTAT)='D'">
					<com.csc_OtherIdentifier>
						<OtherIdTypeCd>Deemed</OtherIdTypeCd>
						<OtherId>
							<xsl:value-of select="DEEMCODE"/>
						</OtherId>
					</com.csc_OtherIdentifier>
				</xsl:when>
				<xsl:when test="normalize-space(DEEMSTAT)='N'">					
				</xsl:when>
			</xsl:choose>			
			<OwnershipPct>
				<xsl:value-of select="OWNPERCENT"/>
			</OwnershipPct>
			<xsl:variable name = "SequenceNumber">
				<xsl:value-of select = "DESC0SEQ"/>
			</xsl:variable>
			<xsl:variable name = "ClassNum">
				<xsl:value-of select = "/*/PMSPWC15__RECORD[INTERESTSQ = $SequenceNumber and DROPIND != 'Y']/CLASSNUM"/>
			</xsl:variable>
			<xsl:variable name = "ClassCode">
				<xsl:choose>
					<xsl:when test="string-length($ClassNum)='3'">
						<xsl:value-of select="concat($ClassNum,'$$$')"/>
					</xsl:when> 
					<xsl:when test="string-length($ClassNum)='4'">
						<xsl:value-of select="concat($ClassNum,'$$')"/>
					</xsl:when> 
					<xsl:when test="string-length($ClassNum)='5'">
						<xsl:value-of select="concat($ClassNum,'$')"/>
					</xsl:when> 
					<xsl:otherwise>
						<xsl:value-of select="$ClassNum"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<RatingClassificationCd>				
				<xsl:value-of select = "concat($ClassCode,/*/PMSPWC15__RECORD[INTERESTSQ = $SequenceNumber and DROPIND != 'Y']/DESCSEQ)"/>
			</RatingClassificationCd>
			<TitleRelationshipCd>
				<xsl:value-of select = "OWNTITLE"/>
			</TitleRelationshipCd>
			<com.csc_EffectiveDt>
				<xsl:value-of select = "EFF0DATE"/>
			</com.csc_EffectiveDt>
			<com.csc_ExpirationDt>
				<xsl:value-of select = "EXP0DATE"/>
			</com.csc_ExpirationDt>
		   	<com.csc_ItemIdInfo>
			   	<OtherIdentifier>
			      	<OtherIdTypeCd>HostRowSeq</OtherIdTypeCd>
			      	<OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
			   	</OtherIdentifier>
			</com.csc_ItemIdInfo>
		</WorkCompIndividuals>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->