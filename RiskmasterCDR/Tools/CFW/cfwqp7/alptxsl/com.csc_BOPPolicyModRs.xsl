<?xml version="1.0"?>
<!--
*************************************************************************************************************
* 34771 - This is the master XSL for the BOP Amendment Mod Response message.
*************************************************************************************************************
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRs.xsl"/>
	<xsl:include href="PTErrorRs.xsl"/>
    <xsl:include href="PTSignOnRs.xsl"/>
    <xsl:include href="PTProducerRs.xsl"/>
    <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
    <xsl:include href="PTCommlPolicyRs.xsl"/>
    <xsl:include href="PTFormsRs.xsl"/>
    <xsl:include href="PTAdditionalInterestRs.xsl"/>
    <xsl:include href="PTLocationRs.xsl"/>
    <xsl:include href="PTSubLocationRs.xsl"/>
    <xsl:include href="PTCommlSubLocationRs.xsl"/>
    <xsl:include href="PTCommlPropertyRs.xsl"/>
    <xsl:include href="PTGeneralLiabilityRs.xsl"/>

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
	<xsl:variable name="LOB">BOP</xsl:variable> 	

    <xsl:template match="/">
        <ACORD>
            <xsl:call-template name="BuildSignOn"/>
            <InsuranceSvcRs>
    			<RqUID/>
                <com.csc_BOPPolicyModRs>
                    <RqUID/>
                    <TransactionRequestDt/>
                    <TransactionEffectiveDt/>
                    <CurCd/>
                    <CodeList/>
					<xsl:call-template name="PointErrorsRs"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
                    <xsl:for-each select="/*/ASBUCPL1__RECORD">
                        <xsl:call-template name="CreateLocation"/>
                    </xsl:for-each>
                    <BOPLineBusiness>
                        <LOBCd>BO</LOBCd>
                        <xsl:for-each select="/*/ASBBCPL1__RECORD">
							<xsl:call-template name="CreateCommlProperty">
								<xsl:with-param name="InsLine">BO</xsl:with-param> 
							</xsl:call-template> 
                        </xsl:for-each>
                    </BOPLineBusiness>
					<xsl:for-each select="/*/ASB5CPL1__RECORD">
						<xsl:call-template name="CreateBOCommlSubLocation"/> 
					</xsl:for-each>
                </com.csc_BOPPolicyModRs>
            </InsuranceSvcRs>
        </ACORD>
    </xsl:template>
 </xsl:stylesheet>
<!-- Stylesheet edited using Stylus Studio - (c)1998-2003. Sonic Software Corporation. All rights reserved. -->