<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="CoveredAutoSymbol" mode="ASCSCPL1">
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASCSCPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASCSCPL1__RECORD>
<CSAACD><xsl:value-of select="$LOC"/></CSAACD>
<CSABCD><xsl:value-of select="$MCO"/></CSABCD>
<CSARTX><xsl:value-of select="$SYM"/></CSARTX>
<CSASTX><xsl:value-of select="$POL"/></CSASTX>
<CSADNB><xsl:value-of select="$MOD"/></CSADNB>
<CSC6ST>V</CSC6ST>
<CSAFNB>0</CSAFNB>
<CSTYPE0ACT>
		<!-- 34771 start -->
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
		<!-- 34771 end -->
</CSTYPE0ACT>
<CSAUPC></CSAUPC>
<CSELCD>PIP</CSELCD>
<CSEMCD>MEDPAY</CSEMCD>
<CSENCD>ADDPIP</CSENCD>
<!--Issue 59878 begins-->

<xsl:variable name="CSEOCD">
	<xsl:for-each select="CommlCoverage[CoverageCd='UM']/Limit">
		<xsl:choose>
			<xsl:when test="LimitAppliesToCd='UMPD'">
				<xsl:variable name="Code">UMPD</xsl:variable>
			</xsl:when>			
		</xsl:choose>
	</xsl:for-each>	
</xsl:variable>
<CSEOCD>
	<xsl:choose>
		<xsl:when test="$CSEOCD = 'UMPD'">UMPD</xsl:when>	
		<xsl:otherwise>UM</xsl:otherwise>
	<!--<xsl:value-of select="$CSEOCD"/>-->
	</xsl:choose>
</CSEOCD>
<!--<CSEOCD>UM</CSEOCD>-->
<!--Issue 59878 ends-->

<CSEPCD>TOW</CSEPCD>
<CSEQCD>COMP</CSEQCD>
<CSERCD>SPEC</CSERCD>
<CSESCD>COLL</CSESCD>
<CSETCD>UN</CSETCD>
<CSEUCD>PPI</CSEUCD>
<CSEXCD>RENTAL</CSEXCD>	<!-- Issue 59878 -->
<CSJ3ST>GKCOMP</CSJ3ST>
<CSEVCD>GKCOL</CSEVCD>
<CSEWCD>GKCOLL</CSEWCD>
<xsl:for-each select="CoveredAutoSymbol/LiabilitySymbolCd">
<xsl:if test="position()='1'">
<CSUKTX><xsl:value-of select="." /></CSUKTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSULTX><xsl:value-of select="." /></CSULTX>
</xsl:if>
<xsl:if test="position()='3'">
<CSUMTX><xsl:value-of select="." /></CSUMTX>
</xsl:if>
<xsl:if test="position()='4'">
<CSUNTX><xsl:value-of select="." /></CSUNTX>
</xsl:if>
</xsl:for-each>
<!--Issue 59878 start-->
<xsl:for-each select="CoveredAutoSymbol/com.csc_RentalReimbursementSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--PIP-->
<CSU0TX><xsl:value-of select="." /></CSU0TX>
</xsl:if>
</xsl:for-each>
<!--Issue 59878 end-->
<xsl:for-each select="CoveredAutoSymbol/PIPNoFaultSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--PIP-->
<CSUOTX><xsl:value-of select="." /></CSUOTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSABTE><xsl:value-of select="." /></CSABTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/AutoMedicalPaymentsSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--MEDPAY-->
<CSUPTX><xsl:value-of select="." /></CSUPTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAATE><xsl:value-of select="." /></CSAATE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/APIPSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--ADDPIP-->
<CSUQTX><xsl:value-of select="." /></CSUQTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSADTE><xsl:value-of select="." /></CSADTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/UninsuredMotoristSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--UM -->
<CSURTX><xsl:value-of select="." /></CSURTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSACTE><xsl:value-of select="." /></CSACTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/TowingAndLaborSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--TOW -->
<CSUSTX><xsl:value-of select="." /></CSUSTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAFTE><xsl:value-of select="." /></CSAFTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/ComprehensiveSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--COMP -->
<CSUTTX><xsl:value-of select="." /></CSUTTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAETE><xsl:value-of select="." /></CSAETE>
</xsl:if>
</xsl:for-each>
<!--<xsl:for-each select="CoveredAutoSymbol/SpecifiedPerilsSymbolCd">--> <!--Issue 59878-->
<xsl:for-each select="CoveredAutoSymbol/com.csc_SpecifiedCauseOfLossSymbolCd"> <!--Issue 59878-->
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--SPEC -->
<CSUUTX><xsl:value-of select="." /></CSUUTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAHTE><xsl:value-of select="." /></CSAHTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/CollisionSymbolCd">
<xsl:if test="position()='1'and string-length(.) &gt; 0">
<!--COLL -->
<CSUVTX><xsl:value-of select="." /></CSUVTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAGTE><xsl:value-of select="." /></CSAGTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/UnderinsuredMotoristSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--UN -->
<CSUWTX><xsl:value-of select="." /></CSUWTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAJTE><xsl:value-of select="." /></CSAJTE>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="CoveredAutoSymbol/PropertyProtectionCoverageSymbolCd">
<xsl:if test="position()='1' and string-length(.) &gt; 0">
<!--PPI -->
<CSUXTX><xsl:value-of select="." /></CSUXTX>
</xsl:if>
<xsl:if test="position()='2'">
<CSAITE><xsl:value-of select="." /></CSAITE>
</xsl:if>
</xsl:for-each>
</ASCSCPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->