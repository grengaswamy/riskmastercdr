<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateCommlSubLocation">
     <xsl:variable name="Location"><xsl:value-of select="BVBRNB"/></xsl:variable>
     <xsl:variable name="SubLocation"><xsl:value-of select="BVEGNB"/></xsl:variable>
     <xsl:variable name="CFUnit" select="/*/ASB5CPL1__RECORD[B5BRNB=$Location and B5EGNB=$SubLocation and B5AGTX='CF']"/>
     <xsl:variable name="CRUnit" select="/*/ASB5CPL1__RECORD[B5BRNB=$Location and B5EGNB=$SubLocation and B5AGTX='CR' ]"/>
     <xsl:variable name="IMUnit" select="/*/ASB5CPL1__RECORD[B5BRNB=$Location and B5EGNB=$SubLocation and B5AGTX='IMC']"/>
     <CommlSubLocation>
        <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
        <xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
             <Construction>
               <ConstructionCd>
                 <xsl:choose>
                 <xsl:when test="CFUnit">
                 <xsl:value-of select="$CFUnit/B5HYTX"/>
                 </xsl:when>
                 <xsl:otherwise>
                 <xsl:value-of select="$IMUnit/B5HYTX"/>
                 </xsl:otherwise>
                 </xsl:choose>
                 </ConstructionCd>
               <InsurerConstructionClassCd/>
               <ConstructionBeganDt/>
               <ConstructionEstEndDt/>
               <ConstructionCostAmt>
                  <Amt/>
               </ConstructionCostAmt>
               <YearBuilt/>
               <FoundationCd/>
               <BldgCodeEffectivenessGradeCd><xsl:value-of select="$CFUnit/B5FJNB"/></BldgCodeEffectivenessGradeCd>
               <BldgEffectivenessGradeTypeCd/>
               <BldgArea>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </BldgArea>
               <NumStories><xsl:value-of select="$CFUnit/B5USNB4"/></NumStories>
               <NumUnits>
                  <xsl:choose>
                 <xsl:when test="$CFUnit">
                 <xsl:value-of select="$CFUnit/B5BKCD"/>
                 </xsl:when>
                 <xsl:otherwise>
                 <xsl:value-of select="$IMUnit/B5FLNB"/>
                 </xsl:otherwise>
                 </xsl:choose>
               </NumUnits>
            </Construction>
            <BldgProtection>
               <FireProtectionClassCd>
                  <xsl:choose>
                 <xsl:when test="$CFUnit">
                 <xsl:value-of select="$CFUnit/B5H1TX"/>
                 </xsl:when>
                 <xsl:otherwise>
                 <xsl:value-of select="$IMUnit/B5H1TX"/>
                 </xsl:otherwise>
                 </xsl:choose>
               </FireProtectionClassCd>
               <FireExtinguisherInd/>
               <ProtectionDeviceBurglarCd/>
               <ProtectionDeviceFireCd/>
               <ProtectionDeviceSmokeCd/>
               <ProtectionDeviceSprinklerCd/>
               <DoorLockCd/>
               <SprinkleredPct/>
            </BldgProtection>
            <!--Issu 102807 starts-->
            <!--BldgOccupancy>
               <NumUnitsOwnerOccupied/>
               <VacantUnoccupiedCd/>
               <OccupiedPct/>
               <AreaOccupied>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </AreaOccupied>
               <OccupancyDesc/>
               <NatureBusinessCd/>
               <OperationsDesc/>
            </BldgOccupancy-->
            <!--Issu 102807 starts-->
            <CrimeInfo>
               <AvgNumEmployees><xsl:value-of select="$CRUnit/B5USNB5"/></AvgNumEmployees>
               <DepositFrequencyCd/>
               <NightDepositInd/>
               <AnnualGrossReceiptsAmt>
                  <Amt/>
               </AnnualGrossReceiptsAmt>
               <PropertyDesc/>
               <MaxPropertyValueAmt>
                  <Amt/>
               </MaxPropertyValueAmt>
            </CrimeInfo>
            <!--Issu 102807 starts-->
            <!--CrimeMoneyAndSecurities>
               <MoneyAndSecuritiesCoverageTypeCd/>
               <MoneyAmt>
                  <Amt/>
               </MoneyAmt>
               <ChecksAmt>
                  <Amt/>
               </ChecksAmt>
               <PayrollAmt>
                  <Amt/>
               </PayrollAmt>
               <MoneyOvernightAmt>
                  <Amt/>
               </MoneyOvernightAmt>
            </CrimeMoneyAndSecurities-->
            <!--Issu 102807 ends-->
            <AlarmAndSecurity>
               <AlarmDescCd><xsl:value-of select="$CRUnit/B5IZTX"/></AlarmDescCd>
               <AlarmTypeCd><xsl:value-of select="$CRUnit/B5AKCD"/></AlarmTypeCd>
               <BulletResistingEnclosureTypeCd><xsl:value-of select="$CRUnit/B5IYTX"/></BulletResistingEnclosureTypeCd>
               <CentralStationInd><xsl:value-of select="$CRUnit/B5CZST"/></CentralStationInd>
               <ULCertificateExpirationDt/>
               <LineSecurityInd/>
               <LocalGongInd><xsl:value-of select="$CRUnit/B5C0ST"/></LocalGongInd>
               <MaintenanceContractInd/>
               <MonitoredSurveillanceInd/>
               <NumGuards><xsl:value-of select="$CRUnit/B5USNB4"/></NumGuards>
               <NumWatchPersons><xsl:value-of select="$CRUnit/B5USNB3"/></NumWatchPersons>
               <PoliceFireConnectionCd/>
               <ProtectionClassGradeCd><xsl:value-of select="$CRUnit/B5BLNB"/></ProtectionClassGradeCd>
               <ProtectionExtentCd/>
               <ULCertificateNumber/>
               <WatchPersonsFactorCd><xsl:value-of select="$CRUnit/B5C2ST"/></WatchPersonsFactorCd>
               <WithKeysInd><xsl:value-of select="$CRUnit/B5C1ST"/></WithKeysInd>
               <com.csc_OutsidePropertyProtectionCd><xsl:value-of select="$CRUnit/B5I0TX"/></com.csc_OutsidePropertyProtectionCd>
               <com.csc_OutsideStationInd><xsl:value-of select="$CRUnit/B5CZST"/></com.csc_OutsideStationInd>
            </AlarmAndSecurity>
            <!--Issu 102807 starts-->
            <!--SafeVaultCharacteristics>
               <ItemInfo>
                  <ItemIdInfo>
                     <InsurerId/>
                  </ItemIdInfo>
                  <ItemDefinition>
                     <ItemTypeCd/>
                     <Manufacturer/>
                     <Model/>
                     <SerialIdNumber/>
                     <ModelYear/>
                     <ItemDesc/>
                  </ItemDefinition>
               </ItemInfo>
               <LabelCd/>
               <SafeVaultClassCd/>
               <TimeLockInd/>
               <RelockingDeviceInd/>
               <Length>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Length>
               <Width>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Width>
               <Height>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Height>
               <NumOpenings/>
               <SafeVaultConstruction>
                  <ComponentTypeCd/>
                  <ConstructionCd/>
                  <Thickness>
                     <NumUnits/>
                     <UnitMeasurementCd/>
                  </Thickness>
               </SafeVaultConstruction>
            </SafeVaultCharacteristics-->
            <!--Issu 102807 ends-->
				<com.csc_SubLocationInfo>
					<com.csc_RatingPlanCd>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5O6TX"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5ALCD"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</com.csc_RatingPlanCd>
					<com.csc_ContentsRateGroupCd>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5PNTX"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5PNTX"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</com.csc_ContentsRateGroupCd>
					<com.csc_ContentsGroupCd>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5USIN24"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5USIN24"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</com.csc_ContentsGroupCd>
					<com.csc_ClassRateGroupCd><xsl:value-of select="$CFUnit/B5PQTX"/></com.csc_ClassRateGroupCd>
					<com.csc_GroupIIMultiplierCd><xsl:value-of select="$CFUnit/B5USCD5"/></com.csc_GroupIIMultiplierCd>
					<com.csc_GroupIISymbolCd><xsl:value-of select="$CFUnit/B5AKCD"/></com.csc_GroupIISymbolCd>
					<com.csc_RatingConstructionProtectionCd><xsl:value-of select="$CFUnit/B5PMTX"/></com.csc_RatingConstructionProtectionCd>
					<com.csc_SprinklerLeakageCd><xsl:value-of select="$CFUnit/B5C1ST"/></com.csc_SprinklerLeakageCd>
					<com.csc_IncidentalOfficeInd><xsl:value-of select="$CFUnit/B5USIN28"/></com.csc_IncidentalOfficeInd>
					<com.csc_IncidentalApartmentInd><xsl:value-of select="$CFUnit/B5USIN26"/></com.csc_IncidentalApartmentInd>
					<com.csc_NonHabitationalOccupancySymbolCd><xsl:value-of select="substring($CFUnit/B5KKTX,7,1)"/></com.csc_NonHabitationalOccupancySymbolCd>
					<com.csc_OccupancyClassSymbolCd><xsl:value-of select="substring($CFUnit/B5KKTX,1,6)"/></com.csc_OccupancyClassSymbolCd>
					<com.csc_ISODistrictNumber>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5FLNB"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5AKCD"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</com.csc_ISODistrictNumber>
					<com.csc_ISORiskId>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5ALCD"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5AMCD"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</com.csc_ISORiskId>
					<com.csc_WindHailDeductiblePct><xsl:value-of select="$CFUnit/B5B5PC"/></com.csc_WindHailDeductiblePct>
					<com.csc_ClassLimitAmt>
						<Amt><xsl:value-of select="$CFUnit/B5BLNB"/></Amt>
					</com.csc_ClassLimitAmt>
					<TaxCodeInfo>
					<TaxCd>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="substring($CFUnit/B5KKTX,8,6)"/>
                       </xsl:when>
                       <xsl:when test="$CRUnit and not($CFUnit)">
                       <xsl:value-of select="$CRUnit/B5BKCD"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5BKCD"/>
                        </xsl:otherwise>
                       </xsl:choose>
					</TaxCd>
					<TaxTypeCd>com.csc_COUNTY</TaxTypeCd>
					</TaxCodeInfo>
					<TaxCodeInfo>
					<TaxCd>
					   <xsl:choose>
                       <xsl:when test="$CFUnit">
                       <xsl:value-of select="$CFUnit/B5BJCD"/>
                       </xsl:when>
                       <xsl:when test="$CRUnit and not($CFUnit)">
                       <xsl:value-of select="$CRUnit/B5BJCD"/>
                       </xsl:when>
                       <xsl:otherwise>
                       <xsl:value-of select="$IMUnit/B5BJCD"/>
                       </xsl:otherwise>
                       </xsl:choose>
					</TaxCd>
					<TaxTypeCd>com.csc_CITY</TaxTypeCd>
					</TaxCodeInfo>		
					<com.csc_ReinforcedMasonaryInd><xsl:value-of select="$CFUnit/B5C0ST"/></com.csc_ReinforcedMasonaryInd>
					<com.csc_NonRatedSprinklerInd><xsl:value-of select="$CFUnit/B5USIN13"/></com.csc_NonRatedSprinklerInd>
					<com.csc_PartialSprinklerInd><xsl:value-of select="$CFUnit/B5C2ST"/></com.csc_PartialSprinklerInd>
					<com.csc_HighRiseInd><xsl:value-of select="$CFUnit/B5IYTX"/></com.csc_HighRiseInd>
					<com.csc_LightSteelConstructionInd><xsl:value-of select="$CFUnit/B5I1TX"/></com.csc_LightSteelConstructionInd>
					<com.csc_MoltenMaterialInd><xsl:value-of select="$CFUnit/B5IZTX"/></com.csc_MoltenMaterialInd>
					<com.csc_StormShuttersCd><xsl:value-of select="$CFUnit/B5USIN22"/></com.csc_StormShuttersCd>
					<com.csc_FireLegalAddlInsuredInd><xsl:value-of select="$CFUnit/B5I5TX"/></com.csc_FireLegalAddlInsuredInd>
					<com.csc_CondoAssocCoverageInd><xsl:value-of select="$CFUnit/B5USIN14"/></com.csc_CondoAssocCoverageInd>
					<com.csc_NewYorkFireFeeInd><xsl:value-of select="$CFUnit/B5CZST"/></com.csc_NewYorkFireFeeInd>
					<com.csc_RiskClassCd><xsl:value-of select="$IMUnit/B5ANCD"/></com.csc_RiskClassCd>
					<com.csc_EarthquakeInfo>
						<com.csc_SusceptibilityCd><xsl:value-of select="$CFUnit/B5I2TX"/></com.csc_SusceptibilityCd>
						<com.csc_BuildingClassCd><xsl:value-of select="$CFUnit/B5POTX"/></com.csc_BuildingClassCd>
						<com.csc_ContentsRateGradeCd><xsl:value-of select="$CFUnit/B5USIN27"/></com.csc_ContentsRateGradeCd>
						<com.csc_ZoneCd><xsl:value-of select="$CFUnit/B5ANCD"/></com.csc_ZoneCd>
						<com.csc_BuildingInConstructionInd><xsl:value-of select="$CFUnit/B5I4TX"/></com.csc_BuildingInConstructionInd>
						<com.csc_SprinklerLeakageInd><xsl:value-of select="$CFUnit/B5I3TX"/></com.csc_SprinklerLeakageInd>
						<CommlCoverage>
							<CoverageCd>EQ</CoverageCd>
							<CoverageDesc/>
							<Limit>
								<FormatCurrencyAmt>
									<Amt><xsl:value-of select="$CFUnit/B5USNB5"/></Amt>
								</FormatCurrencyAmt>
							</Limit>
							<Deductible>
								<FormatPct><xsl:value-of select="$CFUnit/B5PLTX"/></FormatPct>
								<DeductibleTypeCd/>
							</Deductible>
						</CommlCoverage>
					</com.csc_EarthquakeInfo>
				</com.csc_SubLocationInfo>
         </CommlSubLocation>
    </xsl:template>
    
<xsl:template name="CreateBOCommlSubLocation">
     	<!-- 34771 : start -->
	<!--
	 <xsl:variable name="Location"><xsl:value-of select="BVBRNB"/></xsl:variable>
     <xsl:variable name="SubLocation"><xsl:value-of select="BVEGNB"/></xsl:variable>	
     --> 
     <xsl:variable name="Location"><xsl:value-of select="B5BRNB"/></xsl:variable>
     <xsl:variable name="SubLocation"><xsl:value-of select="B5EGNB"/></xsl:variable>
     <!-- 34771 : END -->
     <CommlSubLocation>
        <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
        <xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
        <InterestCd><xsl:value-of select="B5CZST"/></InterestCd>					<!-- 34771 -->
             <Construction>
               <!--<ConstructionCd><xsl:value-of select="B5HYTX"/></ConstructionCd>		 34771 -->
               <ConstructionCd><xsl:value-of select="B5USCD5"/></ConstructionCd>	<!-- 34771 -->
                <InsurerConstructionClassCd/>
               <ConstructionBeganDt/>
               <ConstructionEstEndDt/>
               <ConstructionCostAmt>
                  <Amt/>
               </ConstructionCostAmt>
               <YearBuilt/>
               <FoundationCd/>
               <!-- 34771 : START -->
               <!--
               <BldgCodeEffectivenessGradeCd><xsl:value-of select="B5FJNB"/></BldgCodeEffectivenessGradeCd>
               <BldgEffectivenessGradeTypeCd/>
               -->
               <BldgCodeEffectivenessGradeCd><xsl:value-of select="B5PLTX"/></BldgCodeEffectivenessGradeCd>
               <BldgEffectivenessGradeTypeCd><xsl:value-of select="B5PMTX"/></BldgEffectivenessGradeTypeCd>
               <!-- 34771 : end -->
               <BldgArea>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </BldgArea>
               <NumStories><xsl:value-of select="B5USNB4"/></NumStories>
               <NumUnits><xsl:value-of select="B5BKCD"/></NumUnits>
            </Construction>
            <BldgProtection>
               <FireProtectionClassCd><xsl:value-of select="B5H1TX"/></FireProtectionClassCd>
               <FireExtinguisherInd/>
               <ProtectionDeviceBurglarCd><xsl:value-of select="B5I2TX"/></ProtectionDeviceBurglarCd>	<!-- 34771 -->
               <ProtectionDeviceBurglarCd/>
               <ProtectionDeviceFireCd/>
               <ProtectionDeviceSmokeCd/>
                <ProtectionDeviceSprinklerCd><xsl:value-of select="B5C1ST"/></ProtectionDeviceSprinklerCd> <!--34771 -->
               <DoorLockCd/>
               <SprinkleredPct/>
            </BldgProtection>
            <BldgOccupancy>
               <NumUnitsOwnerOccupied/>
               <VacantUnoccupiedCd/>
               <OccupiedPct/>
               <AreaOccupied>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </AreaOccupied>
               <OccupancyDesc/>
<!--               <NatureBusinessCd/> 34771 -->
                 <NatureBusinessCd><xsl:value-of select="B5KWTX"/></NatureBusinessCd>						<!-- 34771 -->
               <OperationsDesc/>
            </BldgOccupancy>
            <CrimeInfo>
               <AvgNumEmployees><xsl:value-of select="B5USNB5"/></AvgNumEmployees>
               <DepositFrequencyCd/>
               <NightDepositInd/>
               <AnnualGrossReceiptsAmt>
                <Amt><xsl:value-of select="B5USVA4"/></Amt>
               </AnnualGrossReceiptsAmt>
               <PropertyDesc/>
               <MaxPropertyValueAmt>
                  <Amt/>
               </MaxPropertyValueAmt>
            </CrimeInfo>
            <CrimeMoneyAndSecurities>
               <MoneyAndSecuritiesCoverageTypeCd/>
               <MoneyAmt>
                  <Amt/>
               </MoneyAmt>
               <ChecksAmt>
                  <Amt/>
               </ChecksAmt>
               <PayrollAmt>
                  <Amt/>
               </PayrollAmt>
               <MoneyOvernightAmt>
                  <Amt/>
               </MoneyOvernightAmt>
            </CrimeMoneyAndSecurities>
            <AlarmAndSecurity>
               <AlarmDescCd><xsl:value-of select="B5IZTX"/></AlarmDescCd>
               <AlarmTypeCd><xsl:value-of select="B5AKCD"/></AlarmTypeCd>
               <BulletResistingEnclosureTypeCd><xsl:value-of select="B5IYTX"/></BulletResistingEnclosureTypeCd>
               <CentralStationInd><xsl:value-of select="B5CZST"/></CentralStationInd>
               <ULCertificateExpirationDt/>
               <LineSecurityInd/>
               <LocalGongInd><xsl:value-of select="B5C0ST"/></LocalGongInd>
               <MaintenanceContractInd/>
               <MonitoredSurveillanceInd/>
               <NumGuards><xsl:value-of select="B5USNB4"/></NumGuards>
               <NumWatchPersons><xsl:value-of select="B5USNB3"/></NumWatchPersons>
               <PoliceFireConnectionCd/>
               <ProtectionClassGradeCd><xsl:value-of select="B5BLNB"/></ProtectionClassGradeCd>
               <ProtectionExtentCd/>
               <ULCertificateNumber/>
               <WatchPersonsFactorCd><xsl:value-of select="B5C2ST"/></WatchPersonsFactorCd>
               <WithKeysInd><xsl:value-of select="B5C1ST"/></WithKeysInd>
               <com.csc_OutsidePropertyProtectionCd><xsl:value-of select="B5I0TX"/></com.csc_OutsidePropertyProtectionCd>
               <com.csc_OutsideStationInd><xsl:value-of select="B5CZST"/></com.csc_OutsideStationInd>
            </AlarmAndSecurity>
            <SafeVaultCharacteristics>
               <ItemInfo>
                  <ItemIdInfo>
                     <InsurerId/>
                  </ItemIdInfo>
                  <ItemDefinition>
                     <ItemTypeCd/>
                     <Manufacturer/>
                     <Model/>
                     <SerialIdNumber/>
                     <ModelYear/>
                     <ItemDesc/>
                  </ItemDefinition>
               </ItemInfo>
               <LabelCd/>
               <SafeVaultClassCd/>
               <TimeLockInd/>
               <RelockingDeviceInd/>
               <Length>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Length>
               <Width>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Width>
               <Height>
                  <NumUnits/>
                  <UnitMeasurementCd/>
               </Height>
               <NumOpenings/>
               <SafeVaultConstruction>
                  <ComponentTypeCd/>
                  <ConstructionCd/>
                  <Thickness>
                     <NumUnits/>
                     <UnitMeasurementCd/>
                  </Thickness>
               </SafeVaultConstruction>
            </SafeVaultCharacteristics>
            	<com.csc_PropertyOwnedByOther><xsl:value-of select="B5AGVA"/></com.csc_PropertyOwnedByOther>	<!-- 34771 -->
			<com.csc_NumSwimmingPools><xsl:value-of select="B5BLNB"/></com.csc_NumSwimmingPools>			<!-- 34771 -->
				<com.csc_SubLocationInfo>
					<!-- 34771 : start -->
					<TerritoryCd><xsl:value-of select="B5AGNB"/></TerritoryCd>
					<com.csc_ContentsRateGroupCd><xsl:value-of select="B5AMCD"/></com.csc_ContentsRateGroupCd>
					<com.csc_ClassRateGroupCd><xsl:value-of select="B5PQTX"/></com.csc_ClassRateGroupCd>
					<StormShuttersCd><xsl:value-of select="B5IZTX"/></StormShuttersCd>
					<com.csc_WindHailDeductiblePct><xsl:value-of select="B5IYTX"/></com.csc_WindHailDeductiblePct> <!--37441-->
					<!-- 34771 : end -->
					<!-- 34771 : start - Commented out unnecessary fields -->
					<!--
					<com.csc_RatingPlanCd><xsl:value-of select="B5O6TX"/></com.csc_RatingPlanCd>
					<com.csc_ContentsRateGroupCd><xsl:value-of select="B5PNTX"/></com.csc_ContentsRateGroupCd>
					<com.csc_ContentsGroupCd><xsl:value-of select="B5USIN24"/></com.csc_ContentsGroupCd>
					<com.csc_ClassRateGroupCd><xsl:value-of select="B5PQTX"/></com.csc_ClassRateGroupCd>
					<com.csc_GroupIIMultiplierCd><xsl:value-of select="B5USCD5"/></com.csc_GroupIIMultiplierCd>
					<com.csc_GroupIISymbolCd><xsl:value-of select="B5AKCD"/></com.csc_GroupIISymbolCd>
					<com.csc_RatingConstructionProtectionCd><xsl:value-of select="B5PMTX"/></com.csc_RatingConstructionProtectionCd>
					<com.csc_SprinklerLeakageCd><xsl:value-of select="B5C1ST"/></com.csc_SprinklerLeakageCd>
					<com.csc_IncidentalOfficeInd><xsl:value-of select="B5USIN28"/></com.csc_IncidentalOfficeInd>
					<com.csc_IncidentalApartmentInd><xsl:value-of select="B5USIN26"/></com.csc_IncidentalApartmentInd>
					<com.csc_NonHabitationalOccupancySymbolCd></com.csc_NonHabitationalOccupancySymbolCd>
					<com.csc_OccupancyClassSymbolCd></com.csc_OccupancyClassSymbolCd>
					<com.csc_ISODistrictNumber><xsl:value-of select="B5FLNB"/></com.csc_ISODistrictNumber>
					<com.csc_ISORiskId><xsl:value-of select="B5ALCD"/></com.csc_ISORiskId>
					<com.csc_WindHailDeductiblePct><xsl:value-of select="B5B5PC"/></com.csc_WindHailDeductiblePct>
					<com.csc_ClassLimitAmt>
						<Amt><xsl:value-of select="B5BLNB"/></Amt>
					</com.csc_ClassLimitAmt>
					<TaxCodeInfo>
					   <TaxCd><xsl:value-of select="B5BKCD"/></TaxCd>
					   <TaxTypeCd>com.csc_COUNTY</TaxTypeCd>
					</TaxCodeInfo>
					<TaxCodeInfo>
					   <TaxCd><xsl:value-of select="B5BJCD"/></TaxCd>
					   <TaxTypeCd>com.csc_CITY</TaxTypeCd>
					</TaxCodeInfo>		
					<com.csc_ReinforcedMasonaryInd><xsl:value-of select="B5C0ST"/></com.csc_ReinforcedMasonaryInd>
					<com.csc_NonRatedSprinklerInd><xsl:value-of select="B5USIN13"/></com.csc_NonRatedSprinklerInd>
					<com.csc_PartialSprinklerInd><xsl:value-of select="B5C2ST"/></com.csc_PartialSprinklerInd>
					<com.csc_HighRiseInd><xsl:value-of select="B5IYTX"/></com.csc_HighRiseInd>
					<com.csc_LightSteelConstructionInd><xsl:value-of select="B5I1TX"/></com.csc_LightSteelConstructionInd>
					<com.csc_MoltenMaterialInd><xsl:value-of select="B5IZTX"/></com.csc_MoltenMaterialInd>
					<com.csc_StormShuttersCd><xsl:value-of select="B5USIN22"/></com.csc_StormShuttersCd>
					<com.csc_FireLegalAddlInsuredInd><xsl:value-of select="B5I5TX"/></com.csc_FireLegalAddlInsuredInd>
					<com.csc_CondoAssocCoverageInd><xsl:value-of select="B5USIN14"/></com.csc_CondoAssocCoverageInd>
					<com.csc_NewYorkFireFeeInd><xsl:value-of select="B5CZST"/></com.csc_NewYorkFireFeeInd>
					<com.csc_RiskClassCd><xsl:value-of select="B5ANCD"/></com.csc_RiskClassCd>
					<com.csc_EarthquakeInfo>
						<com.csc_SusceptibilityCd><xsl:value-of select="B5I2TX"/></com.csc_SusceptibilityCd>
						<com.csc_BuildingClassCd><xsl:value-of select="B5POTX"/></com.csc_BuildingClassCd>
						<com.csc_ContentsRateGradeCd><xsl:value-of select="B5USIN27"/></com.csc_ContentsRateGradeCd>
						<com.csc_ZoneCd><xsl:value-of select="B5ANCD"/></com.csc_ZoneCd>
						<com.csc_BuildingInConstructionInd><xsl:value-of select="B5I4TX"/></com.csc_BuildingInConstructionInd>
						<com.csc_SprinklerLeakageInd><xsl:value-of select="B5I3TX"/></com.csc_SprinklerLeakageInd>
						<CommlCoverage>
							<CoverageCd>EQ</CoverageCd>
							<CoverageDesc/>
							<Limit>
								<FormatCurrencyAmt>
									<Amt><xsl:value-of select="B5USNB5"/></Amt>
								</FormatCurrencyAmt>
							</Limit>
							<Deductible>
								<FormatPct><xsl:value-of select="B5PLTX"/></FormatPct>
								<DeductibleTypeCd/>
							</Deductible>
						</CommlCoverage>
					</com.csc_EarthquakeInfo>-->
					<!-- 34771 : end -->
				</com.csc_SubLocationInfo>
         </CommlSubLocation>
    </xsl:template>    
</xsl:stylesheet>

