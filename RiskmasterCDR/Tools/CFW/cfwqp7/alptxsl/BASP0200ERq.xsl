
<!-- Created for 50768 - EIG Virtual services -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ACORD/InsuranceSvcRq/*/*" mode="BASP0200E">
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>BASP0200E</RECORD__NAME>
			</RECORD__NAME__ROW>
			<BASP0200E__RECORD>
				<TRANS0STAT>V</TRANS0STAT>
				<ID02>02</ID02>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<GROUPCODE>
					<xsl:value-of select="GroupId"></xsl:value-of>
				</GROUPCODE>
				<!--UNDWCODE><xsl:value-of select="UnderwriterCd"></xsl:value-of></UNDWCODE--> <!-- 73400 --><!-- 103409 - Commented -->
				<UNDWCODE><xsl:value-of select="com.csc_Underwriter"></xsl:value-of></UNDWCODE> <!-- 103409 -->
				<VALUEUP>000</VALUEUP>
				<RETROPREM>00000000000</RETROPREM>
				<DIVIDEND>00000000000</DIVIDEND>
				<DEFERPREM>00000000000</DEFERPREM>
				<USRTXT1/>
				<USRTXT2/>
				<USRTXT3/>
				<USRTXT4/>
				<USRTXT5/>
				<USRTXT6/>
				<USRTXT7/>
				<USRTXT8/>
				<USRTXT9/>
				<USRTXT10/>
				<USRTXT11/>
				<USRTXT12/>
				<USRTXT13/>
				<USRTXT14/>
				<USRNBR1>00000</USRNBR1>
				<USRNBR2>00000</USRNBR2>
				<USRNBR3>0000000000</USRNBR3>
				<USRNBR4>0000000000</USRNBR4>
				<USRNBR5>0000000000</USRNBR5>
				<USRNBR6>0000000000</USRNBR6>
				<USRDLR1>00000000000</USRDLR1>
				<USRDLR2>00000000000</USRDLR2>
				<USRDLR3>00000000000</USRDLR3>
				<USRDLR4>00000000000</USRDLR4>
				<USRIND1/>
				<USRIND2/>
				<USRIND3/>
				<USRIND4/>
				<USRIND5/>
				<USRIND6/>
				<USRIND7/>
				<USRIND8/>
				<USRIND9/>
				<USRIND10/>
				<RECUSER/>
				<DEFERPER>000000000</DEFERPER>
				<AGCYLNKUID>
						<xsl:value-of select="../Producer/ProducerInfo/ContractNumber"></xsl:value-of> <!--57418-->
			   </AGCYLNKUID>
			   <!--98500 starts-->
			   <EMAILADDR1>
						<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'PrimaryContact']/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
						<!--<xsl:value-of select="../CommlPolicy/ContactInfo/ContactEmailAddressTxt"></xsl:value-of>-->
			   </EMAILADDR1>
			   <CONTACT1>
						<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'PrimaryContact']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
						<!--<xsl:value-of select="../CommlPolicy/ContactInfo/ContactNm"></xsl:value-of>-->
			   </CONTACT1>
			   <PHONE1>
						<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'PrimaryContact']/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"/>
				 </PHONE1>
			   <EMAILADDR2>
						<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'SecondaryContact']/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
						<!--<xsl:value-of select="../CommlPolicy/ContactInfo/ContactEmailAddressTxt"></xsl:value-of>-->
			   </EMAILADDR2>
			   <CONTACT2>
								<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'SecondaryContact']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
								<!--<xsl:value-of select="../CommlPolicy/ContactInfo/ContactNm"></xsl:value-of>-->
			   </CONTACT2>
			   <PHONE2>
						<xsl:value-of select="../InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd = 'SecondaryContact']/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"/>
				 </PHONE2>
			   
			   <PHONE>
					<xsl:value-of select="../InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber"></xsl:value-of><!--95024 fetching phone number from InsuredorPrincipal instead from contactinfo-->
			   </PHONE>
			   <!--98500 ends-->
				<AUTOREIND></AUTOREIND>
				<ORIGUNDWCD></ORIGUNDWCD>
				<GOVNCLASS></GOVNCLASS>
				<GOVNCLASSD></GOVNCLASSD>
				<SEQUENCENO></SEQUENCENO>
				<RISKMGMT></RISKMGMT>
				<CONTRACTOR></CONTRACTOR>
				<PAYROLLCO></PAYROLLCO>
				<CARRWENTCD></CARRWENTCD>
				<CARRWENTNM></CARRWENTNM>
				<PRIORCARCD></PRIORCARCD>
				<PRIORCARNM></PRIORCARNM>
			</BASP0200E__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->