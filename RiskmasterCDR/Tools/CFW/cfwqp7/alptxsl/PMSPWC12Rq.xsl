<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
  <xsl:template name="CreatePMSPWC12">
    <xsl:variable name="TableName">PMSPWC12</xsl:variable>
    <xsl:variable name="LocationId" select="@id"/>
    <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)"/>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC12</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC12__RECORD>
      	<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
           <xsl:value-of select="../StateProvCd"/> 
        </STATE>
		<EMODINDCTR>D</EMODINDCTR>
		<ARAPINDCTR>D</ARAPINDCTR>
	</PMSPWC12__RECORD>      
    </BUS__OBJ__RECORD>
  </xsl:template>

  <!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
  
<!-- Case 31594 begin - Modified the template to build one WC12 segement for each location -->
<xsl:template name="CreatePMSPWC12Defaults">
    <xsl:variable name="TableName">PMSPWC12</xsl:variable>
	<xsl:variable name="LocationNbr" select="@LocationRef"/>
	<xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
	<!--<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
	<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd"/>						<!--103409-->
	<xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC12</RECORD__NAME>
      </RECORD__NAME__ROW>
       <PMSPWC12__RECORD>
      	<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="Addr/StateProvCd"/>
        </STATE>
		<EMODINDCTR>D</EMODINDCTR>
		<ARAPINDCTR>D</ARAPINDCTR>
	   </PMSPWC12__RECORD>      
    </BUS__OBJ__RECORD>
    </xsl:if>
  </xsl:template>
  <!-- Case 31594 end -->


</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->