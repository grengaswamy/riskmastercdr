<?xml version="1.0"?>
<!-- 50764 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="BuildCancel02PTSegmentRq">
	<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/com.csc_AmendmentMode"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP0200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP0200__RECORD>
				<TRANS0STAT>P</TRANS0STAT>
				<ID02>02</ID02>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<EFF0DT>
					<xsl:call-template name="ConvertISODateToPTDate">
						<xsl:with-param name="Value" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ContractTerm/EffectiveDt"/>
					</xsl:call-template>
				</EFF0DT>
				<EXP0DT>
					<xsl:call-template name="ConvertISODateToPTDate">
						<xsl:with-param name="Value" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ContractTerm/ExpirationDt"/>
					</xsl:call-template>
				</EXP0DT>
				<COMPANY0NO>
					<xsl:value-of select="$PCO"/>
				</COMPANY0NO>
				<RISK0STATE>
					<!--<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
						<xsl:with-param name="Value" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ControllingStateProvCd"/>
					</xsl:call-template>-->
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/ControllingStateProvCd"/>
				</RISK0STATE>
				<BRANCH>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_Branch"/>
				</BRANCH>
				<PROFIT0CTR>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ProfCnt"/>
				</PROFIT0CTR>
				<FILLR1>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/Producer/ItemIdInfo/AgencyId,1,3)"/>
				</FILLR1>
				<RPT0AGT0NR>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/Producer/ItemIdInfo/AgencyId,4,1)"/>
				</RPT0AGT0NR>
				<FILLR2>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/Producer/ItemIdInfo/AgencyId,5,3)"/>
				</FILLR2>
				<ENTER0DATE>
					<xsl:call-template name="ConvertISODateToPTDate">
						<xsl:with-param name="Value" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/TransactionRequestDt"/>
					</xsl:call-template>
				</ENTER0DATE>
				<TOT0AG0PRM>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CurrentTermAmt/Amt"/>
				</TOT0AG0PRM>
				<LINE0BUS>
					<xsl:choose>
						<xsl:when test="$LOB='CA'">CPP</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$LOB"/>
						</xsl:otherwise>
					</xsl:choose>
				</LINE0BUS>

				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_IssueCd">
					<ISSUE0CODE>
						<xsl:value-of select="."/>
					</ISSUE0CODE>
				</xsl:for-each>
				<COMP0LINE>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_CoLine"/>
				</COMP0LINE>
				<PAY0CODE>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
				</PAY0CODE>
				<MODE0CODE>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
				</MODE0CODE>
				<AUDIT0CODE>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_SubjectToAuditCd"/>
				</AUDIT0CODE>
				<KIND0CODE>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_AuditType"/>
				</KIND0CODE>
				<SORT0NAME>
					<xsl:choose>
						<xsl:when test="$LOB='CPP' or $LOB='BOP'">
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname,1,4)"/>
						</xsl:otherwise>
					</xsl:choose>
				</SORT0NAME>
				<PROD0CODE>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ProdCde"/>
				</PROD0CODE>
				<RENEW0PAY>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PaymentOption/com.csc_RenewalPaymentPlanCd,1,1)"/>
				</RENEW0PAY>
				<RENEW0MODE>
					<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PaymentOption/com.csc_RenewalPaymentPlanCd,2,1)"/>
				</RENEW0MODE>
				<xsl:choose>
					<xsl:when test="$TYPEACT='NR'">
						<RENEWAL0CD>7</RENEWAL0CD>
					</xsl:when>
					<xsl:otherwise>
						<RENEWAL0CD>9</RENEWAL0CD>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:if test="string-length(normalize-space(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonCd)) &gt; 0">
					<RSN0AM01ST>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonCd,1,1)"/>
					</RSN0AM01ST>
					<RSN0AM02ND>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonCd,2,1)"/>
					</RSN0AM02ND>
					<RSN0AM03RD>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonCd,3,1)"/>
					</RSN0AM03RD>

				</xsl:if>

				<ORI0INCEPT>
					<xsl:choose>
						<xsl:when test="string-length(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_OrigDate) &gt; 0">
							<xsl:value-of select="number(substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_OrigDate,1,1)) - 1"/>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_OrigDate,3,2)"/>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_OrigDate,6,2)"/>
						</xsl:when>
						<xsl:otherwise>10306</xsl:otherwise>
					</xsl:choose>
				</ORI0INCEPT>

				<ZIP0POST>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
				</ZIP0POST>
				<!--<xsl:choose>
					<xsl:when test="$LOB='CPP' or $LOB='BOP' or $LOB='CA'">-->
						<ADD0LINE01>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
						</ADD0LINE01>
						<ADD0LINE02>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
						</ADD0LINE02>
						<ADD0LINE03>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
						</ADD0LINE03>
						<ADD0LINE04>
							<xsl:value-of select="concat(substring(concat(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/City,'                            '), 1, 28),ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd)"/>
						</ADD0LINE04>
				<!--	</xsl:when>
					<xsl:otherwise>
						<ADD0LINE01>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
						</ADD0LINE01>
						<xsl:variable name="AddlInsured" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
						<ADD0LINE02>
							<xsl:choose>
								<xsl:when test="string-length($AddlInsured)">
									<xsl:value-of select="$AddlInsured"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>
								</xsl:otherwise>
							</xsl:choose>
						</ADD0LINE02>
						<ADD0LINE03>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
						</ADD0LINE03>
						<ADD0LINE04>
							<xsl:value-of select="substring(concat(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/City, '                                                                                                    '), 1, 28)"/>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
						</ADD0LINE04>
					</xsl:otherwise>
				</xsl:choose>-->
				<FILLR5>0</FILLR5>
				<xsl:choose>
					<xsl:when test="$TYPEACT='NR'">
						<TYPE0ACT>NR</TYPE0ACT>
					</xsl:when>
					<xsl:otherwise>
						<TYPE0ACT>CN</TYPE0ACT>
					</xsl:otherwise>
				</xsl:choose>
				<CANCELDATE>
					<xsl:call-template name="ConvertISODateToPTDate">
						<xsl:with-param name="Value" select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/TransactionEffectiveDt"/>
					</xsl:call-template>
				</CANCELDATE>
				<AMEND0NUM>
					<xsl:choose>
						<xsl:when test="string-length(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_AmendNum) = 1">
							<xsl:value-of select="concat('0',ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_AmendNum)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_AmendNum"/>
						</xsl:otherwise>
					</xsl:choose>
				</AMEND0NUM>
				<REVIEW0CD>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReviewCd"/>
				</REVIEW0CD>
				<MVR0RPT0YR>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_MvrRptYr"/>
				</MVR0RPT0YR>
				<RISK0GRADE>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_RiskGd"/>
				</RISK0GRADE>
				<RISK0UNDWR>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_RskUndw"/>
				</RISK0UNDWR>
				<RN0POL0SYM>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_RenSym"/>
				</RN0POL0SYM>
				<RN0POL0NUM>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_RenPolN"/>
				</RN0POL0NUM>
				<CUST0NO>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_CustNum"/>
				</CUST0NO>
				<SPEC0USE0A>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_SpecA"/>
				</SPEC0USE0A>
				<SPEC0USE0B>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_SpecB"/>
				</SPEC0USE0B>
				<SPEC0USE0C>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_SpecC"/>
				</SPEC0USE0C>
				<HIST0OPT>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_History"/>
				</HIST0OPT>
				<FIN0AUD0IN>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_FnlAud"/>
				</FIN0AUD0IN>
				<EXC0CLAIM>
					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_EXCCLM"/>
				</EXC0CLAIM>
				<MRSUID>A</MRSUID>
				<INSTAL0TRM>
					<xsl:choose>
						<xsl:when test="string-length(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_InstlTrm) = 2">
							<xsl:value-of select="concat('0',ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_InstlTrm)"/>
						</xsl:when>
						<xsl:when test="string-length(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_InstlTrm) = 1">
							<xsl:value-of select="concat('00',ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_InstlTrm)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_InstlTrm"/>
						</xsl:otherwise>
					</xsl:choose>
				</INSTAL0TRM>
				<NMB0INSTAL>
					<xsl:choose>
						<xsl:when test="string-length(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_NumInstl) = 1">
							<xsl:value-of select="concat('0',ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_NumInstl)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_NumInstl"/>
						</xsl:otherwise>
					</xsl:choose>
				</NMB0INSTAL>
			</PMSP0200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->