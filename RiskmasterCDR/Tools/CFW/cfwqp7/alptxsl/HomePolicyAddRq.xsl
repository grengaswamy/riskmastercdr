<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:include href="CommonFuncRq.xsl" />
  <!-- Common Formatting Routines -->
  <xsl:include href="PMSP0000Rq.xsl" />
  <!-- Activity Record -->
  <xsl:include href="PMSP0200Rq.xsl" />
  <!-- Basic Contract Record -->
  <xsl:include href="PMSP1200Rq.xsl" />
  <!-- Additional Interest Record -->
  <xsl:include href="ASBQCPL1Rq.xsl" />
  <!-- Personal Property Unit Record     -->
  <xsl:include href="ASC4CPL1Rq.xsl" />
  <!-- Personal Property Coverage Record -->
  <xsl:include href="ASBUCPL1Rq.xsl" />
  <!-- Location Record -->
  <xsl:include href="ASBACPL1Rq.xsl" />
  <!-- Policy Level Record -->
  <!-- Case 39568 Start -->
  <xsl:include href="PTInfoRq.xsl" />
  <!-- Case 39568 End   -->
  <xsl:include href="BASP0200ERq.xsl" />
  <!-- 57418 start -->
  <!-- Start Case 27708 - Add client files Jeff Simmons -->
  <xsl:include href="BASCLT0100Rq.xsl" />
  <!-- Client Name                                    -->
  <xsl:include href="BASCLT0300Rq.xsl" />
  <xsl:include href="BASCLT1400Rq.xsl" />
  <xsl:include href="BASCLT1500Rq.xsl" />
  <!-- End   Case 27708 - Add client files Jeff Simmons -->
  <xsl:include href="BASORDP001Rq.xsl" />
  <!-- 87428-->
  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes" />
  <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/com.csc_CompanyPolicyProcessingId" />
  <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/NAICCd" />
  <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/com.csc_InsuranceLineIssuingCompany" />
  <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/CompanyProductCd" />
  <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/PolicyNumber" />
  <!-- 29653 start -->
  <xsl:variable name="TYPEACT">
    <xsl:choose>
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">NB</xsl:when>-->		<!--103409-->
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">NB</xsl:when>		<!--103409-->
      <xsl:otherwise>EN</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <!-- 29653 end -->
  <xsl:variable name="MOD">
    <xsl:call-template name="FormatData">
      <xsl:with-param name="FieldName">$MOD</xsl:with-param>
      <xsl:with-param name="FieldLength">2</xsl:with-param>
      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/PolicyVersion" />
      <xsl:with-param name="FieldType">N</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy/LOBCd" />
  <xsl:template match="/">
    <xsl:element name="HomePolicyAddRq">
      <!-- 39568 Start -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/HomePolicyAddRq" mode="CreatePTInfo" />
      <!-- 39568 End   -->
      <!-- Build BASP0200E record - 57418 start-->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy" mode="BASP0200E" />
      <!-- 57418 end -->
      <!-- Build Policy Activity Record -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/HomePolicyAddRq/PersPolicy" mode="PMSP0000" />
      <!-- Build Policy Basic Contract Record -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/HomePolicyAddRq" mode="PMSP0200" />
      <!-- Build Policy Level Record -->
      <xsl:apply-templates select="/ACORD/InsuranceSvcRq/HomePolicyAddRq" mode="ASBACPL1" />
      <!-- Build Location and Location Level Additional Interest Records - PMSP1200-->
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/Location">
        <xsl:variable name="Location" select="substring(@id, 2,string-length(@id)-1)" />
        <!-- Location Record               
               <xsl:call-template name="CreateLocation">
                     <xsl:with-param name="Location" select="$Location"/>    
                   </xsl:call-template>
-->
        <!-- Additional Interest Record -->
        <xsl:for-each select="AdditionalInterest">
          <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
            <xsl:call-template name="CreateAddlInterest">
              <xsl:with-param name="Location" select="$Location" />
            </xsl:call-template>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
		<!--Issue 65778 Start-->
		<!--Build PMSP1200 for second named insured-->
		<xsl:if test="string-length(/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/GeneralPartyInfo/NameInfo/com.csc_LongName)">
			<xsl:call-template name="CreateSecondNamedInsured">
			</xsl:call-template>
		</xsl:if>
		<!--Issue 65778 End-->
      <!-- Build Policy Unit Record -->
      <xsl:apply-templates select="/ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/Dwell" mode="ASBQCPL1" />
      <!-- Build Policy Coverage Records - ASC4CPL1-->
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/Coverage">
        <xsl:call-template name="CreateExtendedCoverages" />
      </xsl:for-each>
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/PropertySchedule">
        <xsl:if test="/ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/PropertySchedule/Coverage/CoverageCd!=''"> <!-- Issue 64220 -->
          <xsl:call-template name="CreateScheduledProperty" />
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/Watercraft">
        <!--start issue 39335-->
        <!--<xsl:if test="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/HomeLineBusiness/Watercraft/Coverage/CoverageCd!=''"> -->
        <xsl:if test="/ACORD/InsuranceSvcRq/HomePolicyAddRq/HomeLineBusiness/Watercraft/Coverage/CoverageCd!=''">
          <!--end issue 39335-->
          <xsl:call-template name="CreateWaterCraftLiability" />
        </xsl:if>
      </xsl:for-each>
      <!-- Start Case 27708 - Add client files Jeff Simmons -->
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq">
        <xsl:call-template name="CreateBASCLT0100" />
        <xsl:call-template name="CreateBASCLT0300" />
        <xsl:call-template name="CreateBASCLT1400" />
      </xsl:for-each>
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/Location">
        <xsl:variable name="Location" select="substring(@id, 2,string-length(@id)-1)" />
        <xsl:for-each select="AdditionalInterest">
          <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
            <xsl:call-template name="CreateBASCLT1500">
              <xsl:with-param name="Location" select="$Location" />
            </xsl:call-template>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
      <!-- End   Case 27708 - Add client files Jeff Simmons -->
      <!-- 87428 begin-->
      <xsl:for-each select="ACORD/InsuranceSvcRq/HomePolicyAddRq/com.csc_TPOInfo">
        <xsl:call-template name="CreateBASORDP001" />
      </xsl:for-each>
      <!-- 87428 end-->
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
