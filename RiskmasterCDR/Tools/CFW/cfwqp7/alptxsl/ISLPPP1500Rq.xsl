<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateISLPPP1500">
	<xsl:variable name="IntNum" select="substring(@id, 2,string-length(@id)-1)"/>
	<xsl:variable name="IntType" select="AdditionalInterestInfo/NatureInterestCd"/>
	<xsl:variable name="IntSeq" select="count(../AdditionalInterest[substring(@id,2,string-length(@id)-1) &lt; $IntNum]/AdditionalInterestInfo[NatureInterestCd = $IntType])"/>
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ISLPPP1500</RECORD__NAME>
         </RECORD__NAME__ROW>
          
         <ISLPPP1500__RECORD>
           	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
         	<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
         	<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
         	<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
         	<MODULE><xsl:value-of select="$MOD"/></MODULE>
           	<USE0CODE><xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/></USE0CODE>
            <USE0LOC>
              <xsl:call-template name="FormatData">
                  <xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
                  <xsl:with-param name="FieldLength">5</xsl:with-param>
                  <xsl:with-param name="Value" select="substring(../@id,2,string-length(../@id)-1)"/>
                  <xsl:with-param name="FieldType">N</xsl:with-param>
              </xsl:call-template>
            </USE0LOC>
	    <DESC0SEQ>
		<xsl:choose>
			<xsl:when test="$TYPEACT='EN'">
				<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostId']/OtherId"/> 
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="$IntSeq"/></xsl:otherwise>
		</xsl:choose>
	    </DESC0SEQ>
         </ISLPPP1500__RECORD>
         
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
