<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template match="/*/ASCSCPL1__RECORD">
            <CoveredAutoSymbol>
               <APIPSymbolCd><xsl:value-of select="CSUQTX"/></APIPSymbolCd>
               <xsl:if test="string-length(CSADTE) > 0">
               <APIPSymbolCd><xsl:value-of select="CSADTE"/></APIPSymbolCd>
               </xsl:if>                              
               <AutoMedicalPaymentsSymbolCd><xsl:value-of select="CSUPTX"/></AutoMedicalPaymentsSymbolCd>
               <xsl:if test="string-length(CSAATE) > 0">
               <AutoMedicalPaymentsSymbolCd><xsl:value-of select="CSAATE"/></AutoMedicalPaymentsSymbolCd>
               </xsl:if>                              
               <CollisionSymbolCd><xsl:value-of select="CSUVTX"/></CollisionSymbolCd>
               <xsl:if test="string-length(CSAGTE) > 0">
               <CollisionSymbolCd><xsl:value-of select="CSAGTE"/></CollisionSymbolCd>
               </xsl:if>                              
               <ComprehensiveSymbolCd><xsl:value-of select="CSUTTX"/></ComprehensiveSymbolCd>
               <xsl:if test="string-length(CSAETE) > 0">
               <ComprehensiveSymbolCd><xsl:value-of select="CSAETE"/></ComprehensiveSymbolCd>
               </xsl:if>                              
               <LiabilitySymbolCd><xsl:value-of select="CSUKTX"/></LiabilitySymbolCd>
               <xsl:if test="string-length(CSULTX) > 0">
               <LiabilitySymbolCd><xsl:value-of select="CSULTX"/></LiabilitySymbolCd>
               </xsl:if>               
               <xsl:if test="string-length(CSUMTX) > 0">
               <LiabilitySymbolCd><xsl:value-of select="CSUMTX"/></LiabilitySymbolCd>
               </xsl:if>               
               <xsl:if test="string-length(CSUNTX) > 0">
               <LiabilitySymbolCd><xsl:value-of select="CSUNTX"/></LiabilitySymbolCd>
               </xsl:if>
               <PIPNoFaultSymbolCd><xsl:value-of select="CSUOTX"/></PIPNoFaultSymbolCd>
               <xsl:if test="string-length(CSABTE) > 0">
               <PIPNoFaultSymbolCd><xsl:value-of select="CSABTE"/></PIPNoFaultSymbolCd>
               </xsl:if>                              
               <PropertyProtectionCoverageSymbolCd><xsl:value-of select="CSUXTX"/></PropertyProtectionCoverageSymbolCd>
               <xsl:if test="string-length(CSAITE) > 0">
               <PropertyProtectionCoverageSymbolCd><xsl:value-of select="CSAITE"/></PropertyProtectionCoverageSymbolCd>
               </xsl:if>                              
               <SpecifiedPerilsSymbolCd><xsl:value-of select="CSUUTX"/></SpecifiedPerilsSymbolCd>
               <xsl:if test="string-length(CSAHTE) > 0">
               <SpecifiedPerilsSymbolCd><xsl:value-of select="CSAHTE"/></SpecifiedPerilsSymbolCd>
               </xsl:if>                              
               <TowingAndLaborSymbolCd><xsl:value-of select="CSUSTX"/></TowingAndLaborSymbolCd>
               <xsl:if test="string-length(CSAFTE) > 0">
               <TowingAndLaborSymbolCd><xsl:value-of select="CSAFTE"/></TowingAndLaborSymbolCd>
               </xsl:if>                              
               <UnderinsuredMotoristSymbolCd><xsl:value-of select="CSUWTX"/></UnderinsuredMotoristSymbolCd>
               <xsl:if test="string-length(CSAJTE) > 0">
               <UnderinsuredMotoristSymbolCd><xsl:value-of select="CSAJTE"/></UnderinsuredMotoristSymbolCd>
               </xsl:if>                              
               <UninsuredMotoristSymbolCd><xsl:value-of select="CSURTX"/></UninsuredMotoristSymbolCd>
               <xsl:if test="string-length(CSACTE) > 0">
               <UninsuredMotoristSymbolCd><xsl:value-of select="CSACTE"/></UninsuredMotoristSymbolCd>
               </xsl:if>                              
            </CoveredAutoSymbol>
	</xsl:template>
</xsl:stylesheet>

