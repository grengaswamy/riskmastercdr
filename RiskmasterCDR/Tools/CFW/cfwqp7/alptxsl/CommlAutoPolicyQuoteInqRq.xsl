<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Formatting Routines -->
    <xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->
    <xsl:include href="PMSP0200Rq.xsl"/>  <!-- Basic Contract Record -->
    <xsl:include href="PMSP1200Rq.xsl"/>  <!-- Additional Interest Record -->
    <xsl:include href="ASBACPL1Rq.xsl"/>  <!-- Policy Level Record -->
    <xsl:include href="ASBBCPL1Rq.xsl"/>  <!-- Insurance Line Record -->
    <xsl:include href="ASBCCPL1Rq.xsl"/>  <!-- Product Record -->
    <xsl:include href="ASBUCPL1Rq.xsl"/>  <!-- Location Record -->
    <xsl:include href="ASBYCPL1Rq.xsl"/>  <!-- Coverage Record -->
    <xsl:include href="ASCSCPL1Rq.xsl"/>  <!-- Coverage Symbols Record -->
    <xsl:include href="ASB5CPL1Rq.xsl"/>  <!-- Unit Record -->
    <xsl:include href="ALB5CPL1Rq.xsl"/>  <!-- Unit Record (Add'l Info) -->
    <!-- Case 39568 Start -->
    <xsl:include href="PTInfoRq.xsl"/>    <!-- Host Information Record -->
    <!-- case 39568 End   -->
   <xsl:include href="BASP0200ERq.xsl"/> <!-- 57418 start -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/PolicyNumber"/>
    <!-- 29653 start -->
    <xsl:variable name="TYPEACT">
  	<xsl:choose>
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">NB</xsl:when>-->		<!--103409-->
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">NB</xsl:when>		<!--103409-->
		<xsl:otherwise>EN</xsl:otherwise>
  	</xsl:choose>
    </xsl:variable> 
    <!-- 29653 end -->

    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	  	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/LOBCd"/>   

    <xsl:template match="/">
        <xsl:element name="CommlAutoPolicyQuoteInqRq">
             <!-- 39568 Start -->
             <xsl:apply-templates select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq" mode="CreatePTInfo"/> 
             <!-- 39568 End   -->
	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->             
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy" mode="PMSP0000"/>
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq" mode="PMSP0200"/>
<!-- Build Policy Level Additional Interest Records -->
            <xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/AdditionalInterest">
                <xsl:call-template name="CreateAddlInterest"/>
            </xsl:for-each>
<!-- Build Location Records -->           
            <xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/Location">
                <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>
               <xsl:call-template name="CreateLocation">
                     <xsl:with-param name="Location" select="$Location"/>    
                </xsl:call-template>
<!-- Build Additional Interests Attached to this Location -->
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="Location" select="$Location"/>    
                        </xsl:call-template>
                   </xsl:if>                          
                </xsl:for-each>
            </xsl:for-each>
<!-- Build Policy Level Record -->
            <xsl:apply-templates select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq" mode="ASBACPL1"/>
            
            <xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlAutoLineBusiness">
               <xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/ControllingStateProvCd"/>
<!-- Build Product Record -->
                    <xsl:call-template name="BuildProductRecord">
                         <xsl:with-param name="InsLine">CA</xsl:with-param>
                        <xsl:with-param name="RateState" select="$PrimaryState"/>
                    </xsl:call-template>

<!-- Build Trailer Interchange ASBY Record  -->           
                 <xsl:for-each select="TruckersSupplement/TruckersTrailerInterchange/CommlCoverage">
				 	<xsl:if test="CoverageCd!=''">
                    <xsl:call-template name="BuildTrailerCoverage">
                       <xsl:with-param name="InsLine">CA</xsl:with-param>
                       <xsl:with-param name="RateState" select="$PrimaryState"/>
                    </xsl:call-template>
					</xsl:if>
                 </xsl:for-each>                      
            </xsl:for-each>
            <xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState">
               <xsl:variable name="RateState" select="StateProvCd"/>
 <!-- Build Insurance Line Information Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine">CA</xsl:with-param>
				    <xsl:with-param name="RateState" select="$RateState"></xsl:with-param>	      
                </xsl:call-template>
 <!-- Build Unit Records   -->
               <xsl:for-each select="CommlVeh">
                  <xsl:variable name="Node" select="."/>
                  <xsl:call-template name="BuildUnitRecord">
                     <xsl:with-param name="Location">0</xsl:with-param>
                     <xsl:with-param name="SubLocation">0</xsl:with-param>
                     <xsl:with-param name="RateState" select="$RateState"/>
                     <xsl:with-param name="InsLine">CA</xsl:with-param>
                     <xsl:with-param name="Product">CA</xsl:with-param>
                     <xsl:with-param name="Node" select="$Node"/>                
                  </xsl:call-template>
                <xsl:call-template name="BuildALB5CPL1">
                     <xsl:with-param name="Location">0</xsl:with-param>
                     <xsl:with-param name="SubLocation">0</xsl:with-param>
                     <xsl:with-param name="InsLine">CA</xsl:with-param>
                     <xsl:with-param name="Node" select="$Node"/>
                 </xsl:call-template>
<!--	 Build Additional Interests Attached to this Vehicle -->
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="Location">0</xsl:with-param>
                            <xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>    
                        </xsl:call-template>
                   </xsl:if>                          
                </xsl:for-each>
<!-- Build Coverage Records -->
                 <xsl:for-each select="CommlCoverage">
				 <xsl:if test="com.csc_CoverageTypeCd!='G'"> 
                    <xsl:call-template name="BuildCoverage">
                       <xsl:with-param name="InsLine">CA</xsl:with-param>
                       <xsl:with-param name="RateState" select="$RateState"/>
                    </xsl:call-template>
				 </xsl:if>
                 </xsl:for-each>
               </xsl:for-each>
            </xsl:for-each>              

<!-- Build Coverage Symbols Record -->
		<xsl:for-each select="ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlAutoLineBusiness">
            <xsl:call-template name="CoveredAutoSymbol"/>
		</xsl:for-each>

        </xsl:element>
    </xsl:template>      
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->