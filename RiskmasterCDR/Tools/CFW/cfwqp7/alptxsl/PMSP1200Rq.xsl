
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="CreateAddlInterest">
		<xsl:param name="Location">0</xsl:param>
		<xsl:param name="SubLocation">0</xsl:param>
		<xsl:param name="UnitNbr">0</xsl:param>
		<!--27083-->
		<xsl:variable name="UnitSeq" select="'0'"/>
		<!--27083-->
		<!-- 29653 Start -->
		<xsl:variable name="IntNum" select="substring(@id, 2,string-length(@id)-1)"/>
		<xsl:variable name="IntType" select="AdditionalInterestInfo/NatureInterestCd"/>
		<xsl:variable name="IntSeq" select="count(../AdditionalInterest[substring(@id,2,string-length(@id)-1) &lt; $IntNum]/AdditionalInterestInfo[NatureInterestCd = $IntType])"/>
		<!-- 29653 End -->
		<xsl:variable name="IntLevel" select="substring(@id,1,1)"/>
		<!-- Issue 50771 -->
		<xsl:variable name="PolEffDt">
			<xsl:choose>
				<xsl:when test="$LOB = 'PA' or $LOB = 'APV' or substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or substring($LOB,1,2) = 'FP'"> <!-- 39111 Add FP -->
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="PolExpDt">
			<xsl:choose>
				<xsl:when test="$LOB = 'PA' or $LOB = 'APV'or substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or substring($LOB,1,2) = 'FP'"> <!-- 39111 Add FP -->
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200__RECORD>
				<TRANS0STAT>V</TRANS0STAT>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<ID12>12</ID12>
				<USE0CODE>
					<xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
				</USE0CODE>
				<!-- 27083
             <USE0LOC>
               <xsl:value-of select="$Location"/>
            </USE0LOC>
            <DESC0SEQ>
               <xsl:value-of select="position()"/>
            </DESC0SEQ>-->
				<!--27083 begin-->
				<xsl:choose>
					<xsl:when test="$UnitNbr &gt; '0'">
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$UnitNbr"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
						<!--issue # 50771 Starts-->
						<!--  <DESC0SEQ><xsl:value-of select="position()"/></DESC0SEQ>-->
						<xsl:choose>
							<xsl:when test="$IntLevel = 'd'">
								<!-- issue # 50774 starts-->
								<!--<DESC0SEQ>
									<xsl:value-of select="$IntNum"/>
								</DESC0SEQ>
							</xsl:when>
							<xsl:otherwise>
								<DESC0SEQ>
									<xsl:value-of select="position()"/>
								</DESC0SEQ>
								</xsl:otherwise>-->
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="$IntNum"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							</xsl:when>
							<xsl:otherwise>
							<!-- Issue 79346 start
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="position()"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							-->
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='InterestTypeSeqNbr']/OtherId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							<!-- Issue 79346 end -->
							</xsl:otherwise>
							<!-- issue # 50774 ends-->
						</xsl:choose>
						<!--issue # 50771 Ends-->
					</xsl:when>
					<xsl:otherwise>
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$Location"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
						<xsl:choose>
							<!-- issue # 50774 starts-->
							<!--<xsl:when test="$LOB = 'BOP'">
						<DESC0SEQ>
							<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[@OtherIdTypeCd='InterestTypeSeqNbr']/@OtherId"/>
						</DESC0SEQ>
					</xsl:when>
					
					 <xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
						<DESC0SEQ>
						<xsl:choose>
							<xsl:when test="$IntLevel = 'd'">
								<xsl:value-of select="$IntNum"/>
						    </xsl:when> 
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="$TYPEACT='EN'">
										<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostId']/OtherId"/> 
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$IntSeq"/>
									</xsl:otherwise>	
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						</DESC0SEQ>
					</xsl:when>
				  
                  <xsl:when test="$SubLocation = '0'">
                        <DESC0SEQ><xsl:value-of select="position()"/></DESC0SEQ>
                  </xsl:when>
                  <xsl:otherwise>
                       <DESC0SEQ><xsl:value-of select="$SubLocation"/></DESC0SEQ>
                  </xsl:otherwise>-->
						<!-- Issue 59878 Start -->
							<xsl:when test="$LOB = 'CPP'">
            					<DESC0SEQ>
               						<xsl:value-of select="position()"/>
            					</DESC0SEQ>
							</xsl:when>
						<!-- Issue 59878 End -->
							<xsl:when test="$LOB = 'BOP'">
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[@OtherIdTypeCd='InterestTypeSeqNbr']/@OtherId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							</xsl:when>

							<xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
								<DESC0SEQ>
									<xsl:choose>
										<xsl:when test="$IntLevel = 'd' or $IntLevel = 'i'">
											<xsl:call-template name="FormatData">
												<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
												<xsl:with-param name="FieldLength">5</xsl:with-param>
												<xsl:with-param name="Precision">0</xsl:with-param>
												<xsl:with-param name="Value" select="$IntNum"/>
												<xsl:with-param name="FieldType">N</xsl:with-param>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="FormatData">
												<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
												<xsl:with-param name="FieldLength">5</xsl:with-param>
												<xsl:with-param name="Precision">0</xsl:with-param>
												<!-- Issue 79346 start -->
												<!--<xsl:with-param name="Value" select="number($IntNum)-1"/>-->
												<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='InterestTypeSeqNbr']/OtherId"/>
												<!-- Issue 79346 end -->
												<xsl:with-param name="FieldType">N</xsl:with-param>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</DESC0SEQ>
							</xsl:when>

							<xsl:when test="$SubLocation = '0'">
							<!-- 79346 start -->
							<!--
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="position()"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							-->
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='InterestTypeSeqNbr']/OtherId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							<!-- Issue 79346 end -->
							</xsl:when>
							<xsl:otherwise>
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value" select="$SubLocation"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<!-- issue # 50774 starts-->
				<TYPE0ACT>
				<!--103409 starts-->
				<!--<xsl:choose>
				<xsl:when test="com.csc_AmendmentMode = 'N'">NB</xsl:when>
				<xsl:when test="com.csc_AmendmentMode = 'A'">EN</xsl:when>
				<xsl:when test="com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
			      <xsl:otherwise>NB</xsl:otherwise>
				</xsl:choose>-->
				
				<xsl:call-template name="ConvertPolicyStatusCd">
					<xsl:with-param name="PolicyStatusCd">
						<xsl:value-of select="//PolicyStatusCd"/>
					</xsl:with-param>
				</xsl:call-template>
				<!--103409 Ends-->
				</TYPE0ACT>
				<!--27083 end -->
				<DZIP0CODE>
					<xsl:value-of select="GeneralPartyInfo/Addr/PostalCode"/>
				</DZIP0CODE>
				<DESC0LINE1>
					<xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
				</DESC0LINE1>
				<!--Issue 39380 begin - switched Addr1 & Addr2 -->
				<!--<DESC0LINE2><xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/></DESC0LINE2>-->
				<!--<DESC0LINE3><xsl:value-of select="GeneralPartyInfo/Addr/Addr2"/></DESC0LINE3>-->
				<DESC0LINE2>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr2"/>
				</DESC0LINE2>
				<DESC0LINE3>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
				</DESC0LINE3>
				<!--Issue 39380 end -->
				<DESC0LINE4>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0LINE4</xsl:with-param>
						<xsl:with-param name="FieldLength">28</xsl:with-param>
						<xsl:with-param name="Value" select="GeneralPartyInfo/Addr/City"/>
						<xsl:with-param name="FieldType">A</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
				</DESC0LINE4>
				<DESC0LINE5>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr3"/>
				</DESC0LINE5>
				<!-- Issue 59878 Start -->
				<!--xsl:if test="$LOB!= 'CPP'"--> <!-- DST CPP rating error-->
				<!-- Issue 50771 Begin -->
				<EFF0DATE>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">
							<xsl:value-of select="substring(AdditionalInterestInfo/com.csc_InterestStartDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(AdditionalInterestInfo/com.csc_InterestStartDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(AdditionalInterestInfo/com.csc_InterestStartDt,9,2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring($PolEffDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,9,2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</EFF0DATE>
				<EXP0DATE>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">
							<xsl:value-of select="substring(AdditionalInterestInfo/InterestEndDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(AdditionalInterestInfo/InterestEndDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(AdditionalInterestInfo/InterestEndDt,9,2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring($PolExpDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolExpDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolExpDt,9,2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</EXP0DATE>
				<!-- Issue 50771 End -->
				<!-- Issue 50698 Begin -->
				<OWNPERCENT>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNPERCENT</xsl:with-param>
						<xsl:with-param name="FieldLength">6</xsl:with-param>
						<xsl:with-param name="Value" select="AdditionalInterestInfo/com.csc_InterestPercent/NumericValue/FormatInteger"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNPERCENT>
				<OWNSALARY>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNSALARY</xsl:with-param>
						<xsl:with-param name="FieldLength">9</xsl:with-param>
						<xsl:with-param name="Value" select="AdditionalInterestInfo/ActualRemunerationAmt/Amt"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNSALARY>
				<OWNTITLE>
					<xsl:value-of select="AdditionalInterestInfo/InterestRank"/>
				</OWNTITLE>
				<!-- Issue 50698 End -->
				<!-- Issue 50771 Begin -->
				<DEEMCODE>
					<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DeemedCd']/OtherId"/>
				</DEEMCODE>
				<DEEMSTAT>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">D</xsl:when>
						<xsl:when test="$IntLevel = 'i'">N</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</DEEMSTAT>
				<!-- Issue 50771 End -->
				<!-- Issue 50698 Begin -->
				<xsl:choose>
					<xsl:when test="(GeneralPartyInfo/NameInfo/TaxIdentity/TaxIdTypeCd='UNEMPLOYMENT NUMBER') or (GeneralPartyInfo/NameInfo/TaxIdentity/TaxIdTypeCd='FEIN')">
						<SSN>
							<!--xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxCd"/-->		<!-- 103409 -->
							<xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxId"/>	<!-- 103409 -->
						</SSN>
						<!-- Issue 50698 End -->
						<!-- 39421 - start -->
						<TAXID>
							<!-- xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='UNEMPLOYMENT NUMBER']/TaxCd"/-->	<!-- 103409 -->
							<xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='UNEMPLOYMENT NUMBER']/TaxId"/>	<!-- 103409 -->
						</TAXID>
					</xsl:when>
					<xsl:otherwise>
						<SSN>
							<xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
						</SSN>
					</xsl:otherwise>
				</xsl:choose>
				<!-- 39421 - start -->

				<!-- Issue 50771 Begin -->
				<FLAG>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'a'">N</xsl:when>
						<xsl:when test="$IntLevel = 'c'">N</xsl:when>
						<xsl:otherwise>Y</xsl:otherwise>
					</xsl:choose>
				</FLAG>
				<!-- Issue 50771 End -->
				<!--/xsl:if--><!-- DST CPP Rating error removed the conditional check-->
				<!-- Issue 59878 End -->
				<!-- Issue 105522 start -->
				<!--Changes made for CPP C.0 starts-->
				<!--xsl:choose>
					<xsl:when test="$LOB != 'CPP'">
						<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>							
						</LOANNUM>
					</xsl:when>
				</xsl:choose-->
				<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>							
				</LOANNUM>
				<!--Changes made for CPP C.0 ends-->
				<!-- Issue 105522 End -->								
			</PMSP1200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
	<!--Issue 65778 Start-->
	<!--The following template creates an additional interest record for Co-Applicant for HO and FP-->
	<xsl:template name="CreateSecondNamedInsured">
		<xsl:param name="Location">1</xsl:param>
		<xsl:param name="UnitNbr">1</xsl:param>
		<xsl:variable name="UnitSeq" select="'0'"/>
		<xsl:variable name="IntType" select="'NI'"/>
		<xsl:variable name="PolEffDt">
			<xsl:choose>
				<xsl:when test="substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or $LOB = 'FP'">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="PolExpDt">
			<xsl:choose>
				<xsl:when test="substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or $LOB = 'FP'">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200__RECORD>
				<TRANS0STAT>V</TRANS0STAT>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<ID12>12</ID12>
				<USE0CODE>NI</USE0CODE>
				<xsl:choose>
					<xsl:when test="$UnitNbr &gt; '0'">
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$UnitNbr"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
					</xsl:when>
					<xsl:otherwise>
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$Location"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
					</xsl:otherwise>
				</xsl:choose>
				<DESC0SEQ>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="1"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</DESC0SEQ>
				<TYPE0ACT>
					<xsl:choose>
						<xsl:when test="com.csc_AmendmentMode = 'N'">NB</xsl:when>
						<xsl:when test="com.csc_AmendmentMode = 'A'">EN</xsl:when>
						<xsl:when test="com.csc_AmendmentMode = 'R'">RB</xsl:when>
						<xsl:when test="com.csc_AmendmentMode = 'NR'">NR</xsl:when>
						<xsl:when test="com.csc_AmendmentMode = 'CN'">CN</xsl:when>
						<xsl:when test="com.csc_AmendmentMode = 'RI'">RI</xsl:when>
						<xsl:otherwise>NB</xsl:otherwise>
					</xsl:choose>
				</TYPE0ACT>
				<DZIP0CODE>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/GeneralPartyInfo/Addr/PostalCode"/>
				</DZIP0CODE>
				<DESC0LINE1>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
				</DESC0LINE1>
				<DESC0LINE2>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/GeneralPartyInfo/Addr/Addr2"/>
				</DESC0LINE2>
				<DESC0LINE3>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/GeneralPartyInfo/Addr/Addr1"/>
				</DESC0LINE3>
				<DESC0LINE4>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0LINE4</xsl:with-param>
						<xsl:with-param name="FieldLength">28</xsl:with-param>
						<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/GeneralPartyInfo/Addr/City"/>
						<xsl:with-param name="FieldType">A</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/GeneralPartyInfo/Addr/StateProvCd"/>
				</DESC0LINE4>
				<DESC0LINE5></DESC0LINE5>
				<EFF0DATE>
							<xsl:value-of select="substring($PolEffDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,9,2)"/>
				</EFF0DATE>
				<EXP0DATE>
							<xsl:value-of select="substring($PolExpDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolExpDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolExpDt,9,2)"/>
				</EXP0DATE>
				<OWNPERCENT>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNPERCENT</xsl:with-param>
						<xsl:with-param name="FieldLength">6</xsl:with-param>
						<xsl:with-param name="Value" select="0"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNPERCENT>
				<OWNSALARY>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNSALARY</xsl:with-param>
						<xsl:with-param name="FieldLength">9</xsl:with-param>
						<xsl:with-param name="Value" select="0"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNSALARY>
				<OWNTITLE></OWNTITLE>
				<DEEMCODE></DEEMCODE>
				<DEEMSTAT></DEEMSTAT>
				<SSN></SSN>
				<TAXID></TAXID>
				<FLAG>N</FLAG>
				<!-- Issue 105522 start -->
				<!--Changes made for CPP C.0 starts-->
				<!--xsl:choose>
					<xsl:when test="$LOB != 'CPP'">
						<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>					
						</LOANNUM>
					</xsl:when>
				</xsl:choose-->
				<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>							
				</LOANNUM>
				<!--Changes made for CPP C.0 ends-->
				<!-- Issue 105522 End -->				
			</PMSP1200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
	<!--Issue 65778 End-->
	<!--Issue 103409 Starts-->
	<xsl:template name="CreateWorkCompIndivudual">
		<xsl:param name="Location">0</xsl:param>
		<xsl:param name="SubLocation">0</xsl:param>
		<xsl:param name="UnitNbr">0</xsl:param>
		<xsl:variable name="UnitSeq" select="'0'"/>	
		<xsl:variable name="IntNum">
			<xsl:choose>
				<xsl:when test="com.csc_OtherIdentifier[OtherIdTypeCd='Deemed']">
					<xsl:value-of select="substring-after(substring-before(@id, 'l'), 'd')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring-after(substring-before(@id, 'l'), 'o')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="IntType"><xsl:value-of select="IncludedExcludedCd"/></xsl:variable>
		<xsl:variable name="IntSeq" select="($IntNum - 1)"/>
		<xsl:variable name="IntLevel">
			<xsl:if test="com.csc_OtherIdentifier[OtherIdTypeCd='Deemed']">d</xsl:if>
		</xsl:variable>
		<xsl:variable name="PolEffDt">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
		</xsl:variable>
		<xsl:variable name="PolExpDt">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
		</xsl:variable>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200__RECORD>
				<TRANS0STAT>V</TRANS0STAT>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<ID12>12</ID12>
				<USE0CODE>
					<xsl:value-of select="$IntType"/>
				</USE0CODE>
				<USE0LOC>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$Location"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</USE0LOC>
				<DESC0SEQ>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$IntNum"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</DESC0SEQ>							
				<TYPE0ACT>
					<xsl:call-template name="ConvertPolicyStatusCd">
						<xsl:with-param name="PolicyStatusCd">
							<xsl:value-of select="//PolicyStatusCd"/>
						</xsl:with-param>
					</xsl:call-template>				
				</TYPE0ACT>
				<DZIP0CODE>
					<xsl:value-of select="NameInfo/TaxIdentity/TaxIdTypeCd['SSN']/TaxId"/>
				</DZIP0CODE>
				<DESC0LINE1>
					<xsl:value-of select="NameInfo/com.csc_LongName"/>
				</DESC0LINE1>
				<DESC0LINE2>				
				</DESC0LINE2>
				<DESC0LINE3>					
				</DESC0LINE3>				
				<DESC0LINE4>
					<!--<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0LINE4</xsl:with-param>
						<xsl:with-param name="FieldLength">28</xsl:with-param>
						<xsl:with-param name="Value" select="GeneralPartyInfo/Addr/City"/>
						<xsl:with-param name="FieldType">A</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>-->
				</DESC0LINE4>
				<DESC0LINE5>
					<!--<xsl:value-of select="GeneralPartyInfo/Addr/Addr3"/>-->
				</DESC0LINE5>
				<EFF0DATE>
				<xsl:if test="$LOB != 'CPP'">
				<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">
							<xsl:value-of select="substring(com.csc_EffectiveDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(com.csc_EffectiveDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(com.csc_EffectiveDt,9,2)"/>		
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring($PolEffDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring($PolEffDt,9,2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				</EFF0DATE>
				<EXP0DATE>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">
							<xsl:value-of select="substring(com.csc_ExpirationDt,1,4)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(com.csc_ExpirationDt,6,2)"/>
							<xsl:value-of select="string('-')"/>
							<xsl:value-of select="substring(com.csc_ExpirationDt,9,2)"/>	
						</xsl:when>
						<xsl:otherwise>
					<xsl:value-of select="substring($PolExpDt,1,4)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolExpDt,6,2)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolExpDt,9,2)"/>
						</xsl:otherwise>
					</xsl:choose>
				</EXP0DATE>			
			<!--	<xsl:value-of select="OwnershipPct"/>-->
				<OWNPERCENT>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNPERCENT</xsl:with-param>
						<xsl:with-param name="FieldLength">6</xsl:with-param>
						<xsl:with-param name="Value" select="OwnershipPct"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNPERCENT>
				<OWNSALARY>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNSALARY</xsl:with-param>
						<xsl:with-param name="FieldLength">9</xsl:with-param>
						<xsl:with-param name="Value" select="InclIndividualsEstAnnualRemunerationAmt/Amt"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNSALARY>
				<OWNTITLE>
					<xsl:value-of select="TitleRelationshipCd"/>
				</OWNTITLE>
				<DEEMCODE>
					<xsl:value-of select="com.csc_OtherIdentifier[OtherIdTypeCd='Deemed']/OtherId"/>
				</DEEMCODE>
				<DEEMSTAT>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'd'">D</xsl:when>
						<xsl:when test="$IntLevel = 'i'">N</xsl:when>
						<xsl:otherwise>N</xsl:otherwise>
					</xsl:choose>
				</DEEMSTAT>
				<SSN>
					<xsl:value-of select="NameInfo/TaxIdentity[TaxIdTypeCd = 'SSN']/TaxId"/>					
				</SSN>				
				<FLAG>
					<xsl:choose>
						<xsl:when test="$IntLevel = 'a'">N</xsl:when>
						<xsl:when test="$IntLevel = 'c'">N</xsl:when>
						<xsl:otherwise>Y</xsl:otherwise>
					</xsl:choose>
				</FLAG>			
				<!-- Issue 105522 start -->
					<!--Changes made for CPP C.0 starts-->
				<!--xsl:choose>
					<xsl:when test="$LOB != 'CPP'">
						<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>							
						</LOANNUM>
					</xsl:when>
				</xsl:choose-->
				<LOANNUM>							
							<xsl:value-of select="AdditionalInterestInfo/AccountNumberId"/>							
				</LOANNUM>
				<!--Changes made for CPP C.0 ends-->
				<!-- Issue 105522 End -->
			</PMSP1200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
	<!--Issue 103409 Ends-->
	<!--issue # 61710 Issue # 111420 starts-->
		<xsl:template name="CreateDBARecord">
		<xsl:param name="Location">0</xsl:param>
		<xsl:param name="SubLocation">0</xsl:param>
		<xsl:param name="UnitNbr">0</xsl:param>
		<xsl:variable name="UnitSeq" select="'0'"/>

		<xsl:variable name="IntNum" select="substring(@id, 2,string-length(@id)-1)"/>
		<xsl:variable name="IntType" select="AdditionalInterestInfo/NatureInterestCd"/>
		<xsl:variable name="IntSeq" select="count(../AdditionalInterest[substring(@id,2,string-length(@id)-1) &lt; $IntNum]/AdditionalInterestInfo[NatureInterestCd = $IntType])"/>
		<xsl:variable name="IntLevel" select="substring(@id,1,1)"/>
		
		<xsl:variable name="PolEffDt">
			<xsl:choose>
				<xsl:when test="$LOB = 'PA' or $LOB = 'APV' or substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or $LOB = 'FP'">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="PolExpDt">
			<xsl:choose>
				<xsl:when test="$LOB = 'PA' or $LOB = 'APV'or substring($LOB,1,2) = 'HO' or substring($LOB,1,2) = 'HP' or $LOB = 'FP'">
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200__RECORD>
				<TRANS0STAT>V</TRANS0STAT>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<ID12>12</ID12>
				<USE0CODE>DA</USE0CODE>
				<xsl:choose>
					<xsl:when test="$UnitNbr &gt; '0'">
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$UnitNbr"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
						
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value">0</xsl:with-param>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
					</xsl:when>
					<xsl:otherwise>
						<USE0LOC>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Precision">0</xsl:with-param>
								<xsl:with-param name="Value" select="$Location"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</USE0LOC>
						<xsl:choose>

							<xsl:when test="$LOB = 'CPP'">
            					<DESC0SEQ>
               						<xsl:value-of select="position()"/>
            					</DESC0SEQ>
							</xsl:when>
							<xsl:when test="$LOB = 'BOP'">
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value">0</xsl:with-param>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							</xsl:when>

							<xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
								<DESC0SEQ>
							
											<xsl:call-template name="FormatData">
												<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
												<xsl:with-param name="FieldLength">5</xsl:with-param>
												<xsl:with-param name="Precision">0</xsl:with-param>
												<xsl:with-param name="Value">0</xsl:with-param>
												<xsl:with-param name="FieldType">N</xsl:with-param>
											</xsl:call-template>
								</DESC0SEQ>
							</xsl:when>

							<xsl:otherwise>
								<DESC0SEQ>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Precision">0</xsl:with-param>
										<xsl:with-param name="Value">0</xsl:with-param>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</DESC0SEQ>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>

				<TYPE0ACT>
				<xsl:call-template name="ConvertPolicyStatusCd">
					<xsl:with-param name="PolicyStatusCd">
						<xsl:value-of select="//PolicyStatusCd"/>
					</xsl:with-param>
				</xsl:call-template>
				</TYPE0ACT>

				<DZIP0CODE>
					<xsl:value-of select="GeneralPartyInfo/Addr/PostalCode"/>
				</DZIP0CODE>
				<DESC0LINE1>
					<xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
				</DESC0LINE1>
				<DESC0LINE2>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr2"/>
				</DESC0LINE2>
				<DESC0LINE3>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
				</DESC0LINE3>
				<DESC0LINE4>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0LINE4</xsl:with-param>
						<xsl:with-param name="FieldLength">28</xsl:with-param>
						<xsl:with-param name="Value" select="GeneralPartyInfo/Addr/City"/>
						<xsl:with-param name="FieldType">A</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
				</DESC0LINE4>
				<DESC0LINE5>
					<xsl:value-of select="GeneralPartyInfo/Addr/Addr3"/>
				</DESC0LINE5>
				<xsl:if test="$LOB != 'CPP'">
				<EFF0DATE>
					<xsl:value-of select="substring($PolEffDt,1,4)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolEffDt,6,2)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolEffDt,9,2)"/>
				</EFF0DATE>
				<EXP0DATE>
					<xsl:value-of select="substring($PolExpDt,1,4)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolExpDt,6,2)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($PolExpDt,9,2)"/>
				</EXP0DATE>
				<OWNPERCENT>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNPERCENT</xsl:with-param>
						<xsl:with-param name="FieldLength">6</xsl:with-param>
						<xsl:with-param name="Value">0</xsl:with-param>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNPERCENT>
				<OWNSALARY>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">OWNSALARY</xsl:with-param>
						<xsl:with-param name="FieldLength">9</xsl:with-param>
						<xsl:with-param name="Value">0</xsl:with-param>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</OWNSALARY>
					<SSN/>
				<FLAG>N</FLAG>
			</xsl:if>
			</PMSP1200__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->
