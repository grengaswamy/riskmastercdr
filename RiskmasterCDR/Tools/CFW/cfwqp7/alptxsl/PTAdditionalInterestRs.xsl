<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateAdditionalInterest">
     <xsl:param name="LOBCd"/> <!-- Issue 33385 FD06 Integration -->
        <AdditionalInterest>
            <GeneralPartyInfo>
                <NameInfo>
                    <CommlName>
                        <CommercialName><xsl:value-of select="DESC0LINE1"/></CommercialName>
                        <SupplementaryNameInfo>
                            <SupplementaryNameCd/>
                            <SupplementaryName/>
                        </SupplementaryNameInfo>
                    </CommlName>
					<!-- 103409 start -->
					<xsl:if test="/*/PMSPWC03__RECORD/SYMBOL='WC'">
                    <TaxIdentity>
						<TaxIdTypeCd>FEIN</TaxIdTypeCd>
						<TaxId><xsl:value-of select="SSN"/></TaxId>						
					</TaxIdentity>
					<TaxIdentity>
						<TaxIdTypeCd>UNEMPLOYMENT NUMBER</TaxIdTypeCd>
						<TaxId><xsl:value-of select="TAXID"/></TaxId>						
                    </TaxIdentity>
					</xsl:if>
                    <!-- 103409 end -->
                </NameInfo>
                <Addr>
             
                    <AddrTypeCd>MailingAddress</AddrTypeCd>
                    <!-- Issue 33385 FD06 Integration Start-->
                    <xsl:choose>
                    <xsl:when test="$LOBCd='APV'">
                    <Addr1><xsl:value-of select="DESC0LINE3"/></Addr1>
                    <Addr2><xsl:value-of select="DESC0LINE2"/></Addr2> 
                    <!--<Addr3/>-->
                    </xsl:when>
					<!-- 34771 : START -->
			       <!-- <xsl:when test="substring($LOBCd,1,2)='BO'">
					<Addr1><xsl:value-of select="DESC0LINE2"/></Addr1>
					<Addr2><xsl:value-of select="DESC0LINE3"/></Addr2>
					<Addr3><xsl:value-of select="DESC0LINE5"/></Addr3>
				    </xsl:when>-->
                    <!-- 34771 end-->

                    <xsl:otherwise> 
                    <!-- Issue 33385 FD06 Integration End-->
                   <Addr2><xsl:value-of select="DESC0LINE2"/></Addr2>
                   <Addr3><xsl:value-of select="DESC0LINE3"/></Addr3>
                    </xsl:otherwise><!--Issue 33385 FD06 Integration-->       
					</xsl:choose> <!--Issue 33385 FD06 Integration-->
					<Addr4><xsl:value-of select="DESC0LINE5"/></Addr4> 
                    <City><xsl:value-of select="substring(DESC0LINE4,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(DESC0LINE4,29,2)"/></StateProvCd> 
                    <PostalCode><xsl:value-of select="DZIP0CODE"/></PostalCode>
                    <CountryCd>USA</CountryCd>
                </Addr>
            </GeneralPartyInfo>
            <AdditionalInterestInfo>
                <NatureInterestCd><xsl:value-of select="USE0CODE"/></NatureInterestCd>
			<!-- issue 105522 start-->
				<!--AccountNumberId/-->
				<AccountNumberId>
					<xsl:value-of select="LOANNUM"/>
				</AccountNumberId>
			<!-- issue 105522 end -->
            </AdditionalInterestInfo>
			<!-- Start Issue 53911 -->
	         <!--<xsl:choose>	-->											<!-- 79346 -->
			 <!--<xsl:when test="/*/PMSP0200__RECORD/LINE0BUS = 'BOP'"> -->	<!-- 79346 -->
					<com.csc_ItemIdInfo>
					   <OtherIdentifier>
					      <OtherIdTypeCd>InterestTypeSeqNbr</OtherIdTypeCd>
					      <OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
					   </OtherIdentifier>
					</com.csc_ItemIdInfo>
			<!--</xsl:when>	-->												<!-- 79346 -->							
			<!--</xsl:choose>-->											<!-- 79346 -->
			<!-- End Issue 53911 -->									
        </AdditionalInterest>
    </xsl:template><!-- Issue 33385 FD06 Integration -->
    
    <xsl:template name="CreateWCAdditionalInterest">
        <AdditionalInterest>
            <GeneralPartyInfo>
                <NameInfo>
                    <CommlName>
                        <CommercialName><xsl:value-of select="DESC0LINE1"/></CommercialName>
                        <SupplementaryNameInfo>
                            <SupplementaryNameCd/>
                            <SupplementaryName/>
                        </SupplementaryNameInfo>
                    </CommlName>
                    <!-- 39421 start -->
                    <TaxIdentity>
						<TaxIdTypeCd>FEIN</TaxIdTypeCd>
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="SSN"/></TaxCd>-->
						<TaxId><xsl:value-of select="SSN"/></TaxId>
						<!-- 55614 End -->
					</TaxIdentity>
					<TaxIdentity>
						<TaxIdTypeCd>UNEMPLOYMENT NUMBER</TaxIdTypeCd>	<!--Removed space after NUMBER Issue 98627-->
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="TAXID"/></TaxCd>-->
						<TaxId><xsl:value-of select="TAXID"/></TaxId>
						<!-- 55614 End -->
                    </TaxIdentity>
                    <!-- 39421 end -->
                </NameInfo>
                <Addr>
                    <AddrTypeCd>MailingAddress</AddrTypeCd>
		   <!-- Case 39570: Resequenced the address fields from Addr2 through 4, to Addr1 through 3 -->
                    <Addr1><xsl:value-of select="DESC0LINE2"/></Addr1>
                    <Addr2><xsl:value-of select="DESC0LINE3"/></Addr2>
                    <Addr3><xsl:value-of select="DESC0LINE4"/></Addr3>
                    <City><xsl:value-of select="substring(DESC0LINE4,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(DESC0LINE4,29,2)"/></StateProvCd>                    
                    <PostalCode><xsl:value-of select="DZIP0CODE"/></PostalCode>
                    <CountryCd>USA</CountryCd>
                </Addr>
            </GeneralPartyInfo>
            <AdditionalInterestInfo>
                <NatureInterestCd><xsl:value-of select="USE0CODE"/></NatureInterestCd>
                <AccountNumberId/>
            </AdditionalInterestInfo>
			<!-- 79346 start-->
			<com.csc_ItemIdInfo>
			    <OtherIdentifier>
				      <OtherIdTypeCd>InterestTypeSeqNbr</OtherIdTypeCd>
				      <OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>
			<!-- 79346 end-->
        </AdditionalInterest>
    </xsl:template><!-- Issue 33385 FD06 Integration -->

<!-- Case 31594 Begin -->
	<xsl:template name="CreateWorkCompAdditionalInterest">
	<!-- Issue 50698 Begin -->
	<xsl:variable name="IntType">
		<xsl:choose>
			<xsl:when test="normalize-space(DEEMSTAT)='D'">
				<xsl:value-of select="string('d')"/>
			</xsl:when>
			<xsl:when test="normalize-space(DEEMSTAT)='N'">
				<xsl:value-of select="string('i')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="IntNbr" select="position()"/>
	<!-- Issue 50698 End -->
	<!-- 50771 start -->
	<xsl:choose>
	<xsl:when test="normalize-space(DEEMSTAT)='D' or normalize-space(DEEMSTAT)='N'">	<!-- Issue 50698 added check for N -->
		<AdditionalInterest>
	    <!-- 55614 Begin -->
	      <!--<xsl:attribute name="id"><xsl:value-of select="concat($IntType,$IntNbr)"/></xsl:attribute>
        	<ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
				</OtherIdentifier>
			</ItemIdInfo> -->
			     <xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-',$IntType,$IntNbr)"/></xsl:attribute>
			 <!--   55614 End -->
            <GeneralPartyInfo>
                <NameInfo>
                    <CommlName>
                        <CommercialName><xsl:value-of select="DESC0LINE1"/></CommercialName>
                        <SupplementaryNameInfo>
                            <SupplementaryNameCd>DBA</SupplementaryNameCd>
														<!-- 111420 sys dst defect log 322 starts-->
                            <!--<SupplementaryName><xsl:value-of select="DESC0LINE2"/></SupplementaryName>-->
													<!--<SupplementaryName>
														<xsl:value-of select="DESC0LINE1"/>
													</SupplementaryName>-->
													  <!-- 111420 sys dst defect log 322 starts-->
                        </SupplementaryNameInfo>
                    </CommlName>
		    <TaxIdentity>
			<TaxIdTypeCd>SSN</TaxIdTypeCd>
			<TaxId>	<xsl:value-of select="SSN"/></TaxId>
		    </TaxIdentity>
					<!-- 39421 start -->
                    <TaxIdentity>
						<TaxIdTypeCd>FEIN</TaxIdTypeCd>
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="SSN"/></TaxCd>-->
						<TaxId><xsl:value-of select="SSN"/></TaxId>
						<!-- 55614 End -->
					</TaxIdentity>
					<TaxIdentity>
						<TaxIdTypeCd>UNEMPLOYMENT NUMBER</TaxIdTypeCd>	<!--Removed space after NUMBER Issue 98627-->
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="TAXID"/></TaxCd>-->
						<TaxId><xsl:value-of select="TAXID"/></TaxId>
						<!-- 55614 End -->
                    </TaxIdentity>
                    <!-- 39421 end -->
                </NameInfo>
                <Addr>
                    <AddrTypeCd>MailingAddress</AddrTypeCd>
                    <Addr1><xsl:value-of select="DESC0LINE3"/></Addr1>	
		    						<Addr2><xsl:value-of select="DESC0LINE2"/></Addr2> <!-- 111420 sys dst defect log 322-->				
                    <Addr3></Addr3>
                    <Addr4></Addr4>
                    <City><xsl:value-of select="substring(DESC0LINE4,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(DESC0LINE4,29,2)"/></StateProvCd>                    
                    <PostalCode><xsl:value-of select="DZIP0CODE"/></PostalCode>
                    <CountryCd>USA</CountryCd>
                </Addr>
            </GeneralPartyInfo>
			<AdditionalInterestInfo>
		  <NatureInterestCd><xsl:value-of select="USE0CODE"/></NatureInterestCd>	<!-- Issue 50698 -->
                  <InterestRank><xsl:value-of select="OWNTITLE"/></InterestRank>
				  <!-- 103186 Start-->
				  <xsl:variable name = "SequenceNumber">
					<xsl:value-of select = "DESC0SEQ"/>
				  </xsl:variable>
				  <xsl:variable name = "ClassNum">
					<xsl:value-of select = "/*/PMSPWC15__RECORD[INTERESTSQ = $SequenceNumber]/CLASSNUM"/>
				  </xsl:variable>
				   <xsl:variable name = "ClassCode">
					<xsl:choose>
					  <xsl:when test="string-length($ClassNum)='3'">
					  	<xsl:value-of select="concat($ClassNum,'$$$')"/>
					  </xsl:when> 
					  <xsl:when test="string-length($ClassNum)='4'">
					  	<xsl:value-of select="concat($ClassNum,'$$')"/>
					  </xsl:when> 
					  <xsl:when test="string-length($ClassNum)='5'">
					  	<xsl:value-of select="concat($ClassNum,'$')"/>
					  </xsl:when> 
					  <xsl:otherwise>
					  	<xsl:value-of select="$ClassNum"/>
					  </xsl:otherwise>
					</xsl:choose>
				  </xsl:variable>
				  <com.csc_RatingClassificationCd><xsl:value-of select = "concat($ClassCode,/*/PMSPWC15__RECORD[INTERESTSQ = $SequenceNumber]/DESCSEQ)"/></com.csc_RatingClassificationCd> <!-- Issue 103186 -->
				  <!-- 103186 End-->
                  <com.csc_InterestPercent>
                     <NumericValue>
                        <FormatInteger><xsl:value-of select="OWNPERCENT"/></FormatInteger>
                     </NumericValue>
                  </com.csc_InterestPercent>
                  <ActualRemunerationAmt>
                     <Amt><xsl:value-of select="OWNSALARY"/></Amt>
                  </ActualRemunerationAmt>
                  <AccountNumberId/>
                  <com.csc_InterestStartDt><xsl:value-of select="EFF0DATE"/></com.csc_InterestStartDt>
                  <InterestEndDt><xsl:value-of select="EXP0DATE"/></InterestEndDt>
               </AdditionalInterestInfo>
			   <com.csc_ItemIdInfo>
			   <!-- 55614 Begin -->
			     <OtherIdentifier>
			   	<!--<OtherIdTypeCd></OtherIdTypeCd>
						<OtherId><xsl:if test="normalize-space(DEEMSTAT)='D'"><xsl:value-of select="DEEMCODE"/></xsl:if></OtherId>-->
					    <OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					    <OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
				       </OtherIdentifier>
			   		<OtherIdentifier>
						<OtherIdTypeCd>Deemed</OtherIdTypeCd>
						<OtherId><xsl:if test="normalize-space(DEEMSTAT)='D'"><xsl:value-of select="DEEMCODE"/></xsl:if></OtherId>
						<!-- 55614 End -->
					</OtherIdentifier>
			   </com.csc_ItemIdInfo>
        </AdditionalInterest>
	</xsl:when>
	<!-- 50771 end -->
	<xsl:otherwise>				<!-- 50771 -->
		<AdditionalInterest>	<!-- 50771 -->
	    <!-- 55614 Begin -->
	       <!-- <xsl:attribute name="id"><xsl:value-of select="concat(string('c'),position())"/></xsl:attribute>
        	<ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
				</OtherIdentifier>
			</ItemIdInfo>-->
			<xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-c',position())"/></xsl:attribute>
			  <!-- 55614 End -->
            <GeneralPartyInfo>
                <NameInfo>
                    <CommlName>
                        <CommercialName><xsl:value-of select="DESC0LINE1"/></CommercialName>
                        <SupplementaryNameInfo>
                            <SupplementaryNameCd>DBA</SupplementaryNameCd>
													<!-- 111420 sys dst log 322 starts-->
													<!--<SupplementaryName><xsl:value-of select="DESC0LINE2"/></SupplementaryName>-->
													<!--<SupplementaryName>
														<xsl:value-of select="DESC0LINE1"/>
													</SupplementaryName>-->
													<!--111420 sys dst log 322 ends-->
                        </SupplementaryNameInfo>
                    </CommlName>
                    <!-- 39421 start -->
                    <TaxIdentity>
						<TaxIdTypeCd>FEIN</TaxIdTypeCd>
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="SSN"/></TaxCd>-->
						<TaxId><xsl:value-of select="SSN"/></TaxId>
						<!-- 55614 End -->
					</TaxIdentity>
					<TaxIdentity>
						<TaxIdTypeCd>UNEMPLOYMENT NUMBER</TaxIdTypeCd>	<!--Removed space after NUMBER Issue 98627-->
						<!-- 55614 Begin -->
						<!--<TaxCd><xsl:value-of select="TAXID"/></TaxCd>-->
						<TaxId><xsl:value-of select="TAXID"/></TaxId>
						<!-- 55614 End -->
                    </TaxIdentity>
                    <!-- 39421 end -->
                </NameInfo>
                <Addr>
                    <AddrTypeCd>MailingAddress</AddrTypeCd>
                    <!-- Case 39570 <Addr1></Addr1>
                    <Addr2><xsl:value-of select="DESC0LINE3"/></Addr2>	-->
                    <Addr1><xsl:value-of select="DESC0LINE3"/></Addr1>	<!-- Case 39570 -->
		    						<Addr2><xsl:value-of select="DESC0LINE2"/></Addr2>	<!-- 111420 sys dst log 322 Added <xsl:value-of select="DESC0LINE2"/>-->		<!-- Case 39570 -->
                    <Addr3></Addr3>
                    <Addr4></Addr4>
                    <City><xsl:value-of select="substring(DESC0LINE4,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(DESC0LINE4,29,2)"/></StateProvCd>                    
                    <PostalCode><xsl:value-of select="DZIP0CODE"/></PostalCode>
                    <CountryCd>USA</CountryCd>
                </Addr>
            </GeneralPartyInfo>
            <AdditionalInterestInfo>
                <NatureInterestCd><xsl:value-of select="USE0CODE"/></NatureInterestCd>
                <AccountNumberId/>
            </AdditionalInterestInfo>
			<!-- 79346 start-->
			<com.csc_ItemIdInfo>
			    <OtherIdentifier>
				      <OtherIdTypeCd>InterestTypeSeqNbr</OtherIdTypeCd>
				      <OtherId><xsl:value-of select="DESC0SEQ"/></OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>
			<!-- 79346 end-->
        </AdditionalInterest>
	</xsl:otherwise>	<!-- 50771 -->
	</xsl:choose>		<!-- 50771 -->
	
    </xsl:template>
<!-- Case 31594 End -->
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->