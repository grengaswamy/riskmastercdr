<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRs.xsl"/>
	 <xsl:include href="PTErrorRs.xsl"/>
    <xsl:include href="PTSignOnRs.xsl"/>
    <xsl:include href="PTProducerRs.xsl"/>
    <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
    <xsl:include href="PTCommlPolicyRs.xsl"/>
    <xsl:include href="PTFormsRs.xsl"/>
   
    <xsl:include href="PTAdditionalInterestRs.xsl"/>
    <xsl:include href="PTLocationRs.xsl"/>
    <xsl:include href="PTSubLocationRs.xsl"/>
    <xsl:include href="PTCommlSubLocationRs.xsl"/>
    <xsl:include href="PTCommlPropertyRs.xsl"/>
    <xsl:include href="PTGeneralLiabilityRs.xsl"/>
    <xsl:include href="PTInlandMarineRs.xsl"/>
    <xsl:include href="PTCommlCrimeRs.xsl"/>
<!--    <xsl:include href="PTFormsRs.xsl"/>-->
<!-- Issue 69110 - Start -->
    <xsl:include href="PTCommlRateStateRs.xsl"/>
    <xsl:include href="PTCommlCCRateStateRs.xsl"/>
	<xsl:include href="PTTruckersSupplementRs.xsl"/> 
	<xsl:include href="PTCommlCoverageRs.xsl"/>
	<xsl:include href="PTCommlVehRs.xsl"/>
	<!--xsl:include href="PTCPPFormsRs.xsl"/-->
<!-- Issue 69110 - Start -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:key name="StateKey" match="ASB5CPL1__RECORD" use="B5BCCD"/>   <!-- Unit/State Key -->    <!-- Issue 69110 -->
	<xsl:key name="VehKey" match="ASBYCPL1__RECORD" use="BYAENB"/>   <!-- Coverage/Vehicle Key --> <!-- Issue 69110 -->
    <xsl:key name="ILKey" match="ASBYCPL1__RECORD" use="BYAGTX"/>   <!-- Coverage/Insurance Line Key --> <!-- Issue 69110 -->

    <xsl:template match="/">
        <ACORD>
            <xsl:call-template name="BuildSignOn"/>
            <InsuranceSvcRs>
                <RqUID/>
                <com.csc_CommlPkgPolicyQuoteInqRs>
                    <RqUID/>
                    <TransactionResponseDt/>
                    <CurCd>USD</CurCd>
    			    <xsl:call-template name="PointErrorsRs"/>
                    <xsl:apply-templates select="com.csc_CommlPkgPolicyQuoteInqRs/PMSP0200__RECORD" mode="Producer"/>
                    <xsl:apply-templates select="com.csc_CommlPkgPolicyQuoteInqRs/PMSP0200__RECORD" mode="Business"/>
                    <xsl:apply-templates select="com.csc_CommlPkgPolicyQuoteInqRs/PMSP0200__RECORD" mode="CommlPolicy"/>
                    <xsl:for-each select="com.csc_CommlPkgPolicyQuoteInqRs/ASBUCPL1__RECORD">
                        <xsl:call-template name="CreateLocation"/>
                    </xsl:for-each>
                    <xsl:for-each select="com.csc_CommlPkgPolicyQuoteInqRs/ASBVCPL1__RECORD">
                            <xsl:call-template name="CreateCommlSubLocation"/>
                    </xsl:for-each>                     
                        <xsl:for-each select="com.csc_CommlPkgPolicyQuoteInqRs/ASBBCPL1__RECORD">
                            <xsl:variable name="InsLine" select="BBAGTX"/>
                            <xsl:choose>
                                <xsl:when test="$InsLine='CF'">
                                    <xsl:call-template name="CreateCommlProperty">
                                    <xsl:with-param name="InsLine">CF</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="$InsLine='GL'">
                                    <xsl:call-template name="CreateGeneralLiability"/>
                                </xsl:when>
                                <xsl:when test="$InsLine='CR'">
                                    <xsl:call-template name="CreateCommlCrime"/>
                                </xsl:when>
                                <xsl:when test="$InsLine='IMC'">
                                    <xsl:call-template name="CreateInlandMarine"/>
                                </xsl:when>
                                <xsl:otherwise/>
                            </xsl:choose>
                        </xsl:for-each>
				<!-- Issue 69110 Begin - Handle Commercial Automobile -->
				<CommlAutoLineBusiness>
                     <LOBCd>CA</LOBCd>
                     <!-- Issue 59878 start -->
                      	<!--<xsl:apply-templates select="/*[1]/ASCSCPL1__RECORD"/>
	                     <xsl:choose>
	                     <xsl:when test="com.csc_CommlPkgPolicyQuoteInqRs/ASB5CPL1__RECORD/B5AENB > 0">
							 <xsl:for-each select="com.csc_CommlPkgPolicyQuoteInqRs/ASB5CPL1__RECORD[count(. | key('StateKey', B5BCCD)[1])=1]">
								<xsl:sort select="B5BCCD"/>
								<xsl:call-template name="CreateCommlRateState"/>
        	                </xsl:for-each>
        	              </xsl:when>
        	             <xsl:otherwise>
        	               <xsl:call-template name="CreateCommlCCRateState"/>
        	             </xsl:otherwise>
        	            </xsl:choose>-->
        	            <xsl:for-each select="/*/ASB5CPL1__RECORD[B5ANTX='CA']">
        	                <xsl:sort select="B5BCCD"/>
        	                <xsl:variable name="State" select="B5BCCD"/>
         	             <xsl:if test="not(preceding-sibling::ASB5CPL1__RECORD[B5BCCD=$State])">
								    <xsl:call-template name="CreateCommlRateState"/>
								 </xsl:if>
        	             </xsl:for-each>
        	            <!-- Issue 59878 end -->
                           <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='CTCOMP']">
                           <xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
                           <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='CTLIAB']">
                           <xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
						   <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='TAXES']">
						   <xsl:if test="BYAENB ='0'">
                           <xsl:call-template name="CreateCommlCoverage"/>
						   </xsl:if>
						   </xsl:for-each>
						   <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='HIRREN']">
						   <xsl:if test="BYAENB ='0'">
                           <xsl:call-template name="CreateCommlCoverage"/>
						   </xsl:if>
						   </xsl:for-each>
                           <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='CAEXP']">
                           		<xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
						   <!--Issue 59878 start-->
						   <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='MINPRM']">
                           		<xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
						   <!--Issue 59878 End-->
                       	<xsl:call-template name="CreateTrailerInterchange"/>
<!-- Build Forms -->
		        <!--<xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CA']">-->  <!--59878-->
		        <!--Issue 102807 commented out startrs-->
				<!--xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CA']">		
  				<xsl:call-template name="BuildCPPForm" />
   			</xsl:for-each-->
   			<!--Issue 102807 commented out ends-->
	            </CommlAutoLineBusiness>
                </com.csc_CommlPkgPolicyQuoteInqRs>
            </InsuranceSvcRs>
        </ACORD>
    </xsl:template>
 </xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\Documents and Settings\snema\Desktop\test.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->