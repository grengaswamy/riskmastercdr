<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Point XML into ACORD XML to
return back to iSolutions 
Created by E-Service case 31594  
***********************************************************************************************-->
	<xsl:include href="CommonFuncRs.xsl"/>
	<xsl:include href="PTSignOnRs.xsl"/>
	<xsl:include href="PTErrorRs.xsl"/>
	<xsl:include href="PTProducerRs.xsl"/>
	<xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
	<xsl:include href="PTAdditionalInterestRs.xsl"/>
	<xsl:include href="PTFormsRs.xsl"/>
	<xsl:include href="PTPersPolicyRs.xsl"/><!-- 33385 -->
	<xsl:include href="PTPersDriverRs.xsl"/>
	<xsl:include href="PTPersVehRs.xsl"/>
	<xsl:include href="PTAccidentViolationRs.xsl"/><!-- 33385 -->
	<xsl:include href="PTDriverVehRs.xsl"/><!-- 33385 -->
	<xsl:include href="PTCoverageRs.xsl"/>
	<xsl:include href="PTCommlPolicyRs.xsl"/>
	<xsl:include href="PTCommlCoverageRs.xsl"/><!-- <xsl:include href="PTWCLocationRs.xsl"/>--><!-- 34771 -->
	<xsl:include href="PTWorkCompRateStateRs.xsl"/>
	<xsl:include href="PTWorkCompLocInfoRs.xsl"/><!-- 34771 -->
	<xsl:include href="PTHPPersPolicyRs.xsl"/><!-- 34770 -->
	<xsl:include href="PTDwellRs.xsl"/><!-- 34770 -->
	<xsl:include href="PTSubLocationRs.xsl"/><!-- 34770 --><!-- 34771 -->
	<xsl:include href="PTLocationRs.xsl"/><!-- 34771 -->
	<xsl:include href="PTCommlSubLocationRs.xsl"/><!-- 34771 -->
	<xsl:include href="PTCommlPropertyRs.xsl"/><!-- 34771 -->
	<xsl:include href="PTModFactorsRs.xsl"/><!-- 34771 -->
	<xsl:include href="PTPropertyScheduleRs.xsl"/><!-- 55614 -->
	<xsl:include href="PTWCIndividualsRs.xsl"/>		<!--103409-->
	
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<ACORD>
			<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<com.csc_PolicySyncRs>
					<RqUID/>
					<TransactionResponseDt/>
					<TransactionEffectiveDt>
						<xsl:if test="/*/PMSP0200A__RECORD/EFFECTDATE">
							<xsl:call-template name="ConvertPTDateToISODate">
								<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
							</xsl:call-template>
						</xsl:if>
					</TransactionEffectiveDt>
					<CurCd>USD</CurCd>
					<xsl:call-template name="PointErrorsRs"/>
					<!-- 55614 begin -->
					<!--<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>-->
					<xsl:apply-templates select="/*/PMSPAGL0__RECORD" mode="Producer"/>
					<!-- 55614 end -->
					<xsl:variable name="LOB" select="/*/PMSP0200__RECORD/LINE0BUS"/>
					<xsl:choose><!--Case 34770 start -->
						<xsl:when test="$LOB='HP' or $LOB='FP'">
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Person"/>
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="PersPolicy"/>
							<!-- 55614 begin -->
				<!--	
                 <Location>									  
					   <xsl:variable name="add1" select="/*/ASBQCPL1__RECORD/BQDCNB"/>
					   <xsl:variable name="add2" select="/*/ASBQCPL1__RECORD/BQDDNB"/>
					   <xsl:variable name="PostCode" select="/*/ASBQCPL1__RECORD/BQAPNB"/>
					   <xsl:variable name="State" select="/*/ASBQCPL1__RECORD/BQBCCD"/>
					   <xsl:variable name="County" select="/*/ASBQCPL1__RECORD/BQH2TX"/>
					   <xsl:if test="$add1!='' and ($add2!='' or $PostCode!='' or $State!='' or $County!='' )">												  			 					 					  
					   <Addr>
						 <Addr1>						
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQDCNB"/>
						 </Addr1>
						 <Addr2>
						<xsl:if test="/*/ASBQCPL1__RECORD/BQDENB !='' "> 
								<xsl:value-of select="/*/ASBQCPL1__RECORD/BQDDNB"/>						
						 </xsl:if>
						 </Addr2>
						 <City>							
							<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,1,28)"/>
						</City>
						 <PostalCode>							
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQAPNB"/>	
						</PostalCode>
						<StateProvCd>								
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQBCCD"/>	                 	
						</StateProvCd>
						<CountyCd> <xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/>	</CountyCd>
							<County>
									<xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/>	
							</County>
						</Addr>
						</xsl:if>
						<xsl:if test="$add1='0' or $add2='0' or $PostCode='0' or $State='0' or $County='0' or ($add1='')">
						<Addr>								          																																																				
							<Addr1>
							<xsl:value-of select="/*/PMSP0200__RECORD/ADD0LINE03"/>						
							</Addr1>
							<Addr2>						
							<xsl:value-of select="/*/PMSP0200__RECORD/ADD0LINE02"/>	
							</Addr2>							
							<PostalCode>
								<xsl:value-of select="/*/PMSP0200__RECORD/ZIP0POST"/>						
							</PostalCode>						
							<City>							
							<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,1,28)"/>
							</City>
							<StateProvCd>							
								<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,29,2)"/>                   	
							</StateProvCd>
							<CountyCd/>
							<County>
									<xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/>
							</County>	
							</Addr>
					</xsl:if>
					<xsl:for-each select="//PMSP1200__RECORD[USE0CODE!='LA']">
					<xsl:call-template name="CreateAdditionalInterest"/>	
					</xsl:for-each>														 
				   </Location>
				   <HomeLineBusiness>
	                    <LOBCd>HP</LOBCd>
	                   <xsl:for-each select="/*/ASBQCPL1__RECORD">
	                       <xsl:call-template name="CreateDwell"/>
	                   </xsl:for-each>
					   <xsl:for-each select="/*/ASC4CPL1__RECORD[C4C7ST!='Y']">
	                
						   <xsl:call-template name="CreateCoverage">
	                          <xsl:with-param name="LOB">HP</xsl:with-param>
	                       </xsl:call-template>
	                    </xsl:for-each>
								
            			<QuestionAnswer>
       	      			   <QuestionCd/>
						   <YesNoCd/>
           				   <Explanation/>
           				</QuestionAnswer>
	                </HomeLineBusiness>
	                	<com.csc_LastActivityInfo>
					  	 	<com.csc_LastActivityTypeCd /> 
  							<com.csc_LastActivityDt>
							<xsl:choose>
							<xsl:when test="/*/PMSP0200A__RECORD/EFFECTDATE">
								<xsl:call-template name="ConvertPTDateToISODate">
                	       			<xsl:with-param name="Value"  select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
                    			</xsl:call-template>
							</xsl:when>
							</xsl:choose>
  							</com.csc_LastActivityDt>
  					 </com.csc_LastActivityInfo>
					<PolicySummaryInfo>Insert Amendment Twin</PolicySummaryInfo>
				  </xsl:when>
				Case 34770 end 
-->
							<!--113387 starts-->
							<!--xsl:choose>
							<xsl:when test="/*/PMSP1200__RECORD[USE0CODE='LA']">
								<xsl:call-template name="CreateLocationFrom12LA"/>
							</xsl:when>	
							<xsl:otherwise>	
							   <xsl:call-template name="CreateHOLocation"/>
							</xsl:otherwise>
						</xsl:choose-->
							<xsl:call-template name="CreateHOLocation"/>
							<!--113387 ends-->				
							<xsl:if test="$LOB='HP'">
								<HomeLineBusiness>
									<LOBCd>HP</LOBCd>
									<xsl:for-each select="/*/ASBQCPL1__RECORD">
										<xsl:call-template name="CreateDwell">
											<xsl:with-param name="LOB">HP</xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
									 <xsl:for-each select="/*/ASC4CPL1__RECORD[C4C7ST!='Y']">
					           <xsl:if test="C4BTCD != '1'">
							   <!--Issue 64220 starts-->
							   <xsl:variable name="CoverageCode" select="normalize-space(C4AOTX)" /> <!--Issue 64220--> 
							   	<xsl:choose>
										<xsl:when test="$CoverageCode!='CAMERA' and $CoverageCode!='GOLF' and $CoverageCode!='JEWEL' and $CoverageCode!='MUSIC' and $CoverageCode!='COINS' and $CoverageCode!='GUNS' and $CoverageCode!='F/A-1' and $CoverageCode!='FURS'">
											<xsl:call-template name="CreateCoverage">
	                          					<xsl:with-param name="LOB">HP</xsl:with-param>
	                       					</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="CreatePropertySchedule"/>
										</xsl:otherwise>
								</xsl:choose>
						        <!-- <xsl:call-template name="CreateCoverage">
	                          <xsl:with-param name="LOB">HP</xsl:with-param>
	                       </xsl:call-template>-->
						   <!--Issue 64220 ends-->
	                       </xsl:if>
	                    </xsl:for-each>
	                    <xsl:for-each select="/*/ASC4CPL1__RECORD[C4C7ST!='Y']">
	                      <xsl:if test="C4BTCD = '1'">
						      <xsl:call-template name="CreatePropertySchedule"/>
	                       </xsl:if>
	                    </xsl:for-each>
								</HomeLineBusiness>
							</xsl:if>
							<xsl:if test="$LOB='FP'">
								<DwellFireLineBusiness>
									<LOBCd>FP</LOBCd>
									<xsl:for-each select="/*/ASC4CPL1__RECORD[C4C7ST!='Y']">
										<xsl:call-template name="CreateCoverage">
											<xsl:with-param name="LOB">FP</xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
									<xsl:for-each select="/*/ASBQCPL1__RECORD">
										<xsl:call-template name="CreateDwell">
											<xsl:with-param name="LOB">FP</xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
								</DwellFireLineBusiness>
							</xsl:if>
							<PolicySummaryInfo>
								<FullTermAmt>
									<Amt><xsl:value-of select="/*/ASBQCPL1__RECORD/BQA3VA"/></Amt>
								</FullTermAmt>
								<PolicyStatusCd><xsl:value-of select="/*/PMSP0200__RECORD/TYPE0ACT"/></PolicyStatusCd>
								<com.csc_LastAcyDt>
								    <xsl:if test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:if>
								</com.csc_LastAcyDt>
								<com.csc_TransAmt>
								   <Amt><xsl:value-of select="/*/PMSP0200A__RECORD/TOT0AG0PRM"/></Amt>
								</com.csc_TransAmt>
								<com.csc_ReasonAmendedCd><xsl:value-of select="/*/PMSP0200A__RECORD/REASAMNDCD"/></com.csc_ReasonAmendedCd>
							</PolicySummaryInfo>
						</xsl:when>
						<!-- 55614 end -->
						
						<xsl:when test="$LOB='WCV' or $LOB='WCA'">
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
							<xsl:variable name="ValidLocation" select="/*/PMSPWC04__RECORD[DROPIND = 'N' or string-length(DROPIND)=0]"/>
							<xsl:for-each select="$ValidLocation">
								<xsl:sort select="SITE"/>
								<xsl:call-template name="WCLocationInfo"/>
							</xsl:for-each>
							<!--98500 Begin -->
							<xsl:if test="/*/BASP0200E__RECORD">
								<xsl:call-template name="CreateContactInfo"/>
							</xsl:if>
							<!--98500 End -->
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
							<xsl:for-each select="$ValidLocation">
								<xsl:sort select="SITE"/>
								<xsl:call-template name="CreateWCLocation"/>
							</xsl:for-each>
							<WorkCompLineBusiness>
								<LOBCd>WC</LOBCd>
								<!-- 55614 Begin
								<WorkCompAssignedRisk>
									<ApplicantsStatementDesc/>
									<NumInsurersDeclined/>
								</WorkCompAssignedRisk>
								55614 End -->
								<!--103409  sys/dst log 360 starts-->
								<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = 'D' and USE0CODE='IO']">
									<xsl:sort select="number(DESC0SEQ)"/>
					            	<xsl:call-template name="CreateWorkCompIndividuals"/>
					          	</xsl:for-each>
								<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = 'D'and USE0CODE='EO']">
									<xsl:sort select="number(DESC0SEQ)"/>
					            	<xsl:call-template name="CreateWorkCompIndividuals"/>
					          	</xsl:for-each>
								<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT!='D' and DEEMSTAT = 'N']">
									<xsl:sort select="number(DESC0SEQ)"/>
					            	<xsl:call-template name="CreateWorkCompIndividuals"/>
					          	</xsl:for-each>
								<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = 'D'and USE0CODE!='EO' and USE0CODE!='IO']">
									<xsl:sort select="number(DESC0SEQ)"/>
					            	<xsl:call-template name="CreateWorkCompIndividuals"/>
					          	</xsl:for-each>
								
								<!--103409 Ends  sys/dst log 360 ends-->
								<xsl:for-each select="/com.csc_PolicySyncRs/PMSPWC07__RECORD">
									<xsl:variable name="State" select="STATE"/>
									<!-- 101660 Begin -->
									<!--<xsl:variable name="NumOfLocs" select="count(/com.csc_PolicySyncRs/PMSPWC04__RECORD[STATE=$State and DROPIND!='Y'])"/>
									<xsl:if test="$NumOfLocs != 0">
										<xsl:call-template name="CreateWCRateState"/>
									</xsl:if>-->
										<!-- 106056 Begin -->
									<!--<xsl:call-template name="CreateWCRateState"/>-->
									<xsl:variable name="NumOfLocs" select="count(/com.csc_PolicySyncRs/PMSPWC04__RECORD[STATE=$State and DROPIND!='Y'])"/>
									<xsl:if test="$NumOfLocs != 0">
										<xsl:call-template name="CreateWCRateState"/>
									</xsl:if>
									<!-- 106056 End -->
									<!-- 101660 End -->
								</xsl:for-each>
								<xsl:apply-templates select="/*/PMSPWC02__RECORD" mode="CreateWCCommlCoverage"/>
								<QuestionAnswer>
									<QuestionCd/>
									<YesNoCd/>
									<Explanation/>
								</QuestionAnswer>
							</WorkCompLineBusiness>
							<!-- 55614 Begin							
							<RemarkText IdRef="policy">Work Comp XML Sync</RemarkText>
							<com.csc_LastActivityInfo>
								<com.csc_LastActivityTypeCd/>--> <!-- 29653 Start --><!--<com.csc_LastActivityDt> --><!--	<xsl:call-template name="ConvertPTDateToISODate"> --><!--<xsl:with-param name="Value"  select="/com.csc_PolicySyncRs/PMSP0200A__RECORD/EFFECTDATE"/> --><!--</xsl:call-template> --><!--</com.csc_LastActivityDt> -->
							<!--	<com.csc_LastActivityDt>
									<xsl:choose>
										<xsl:when test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:when>
									</xsl:choose>
								</com.csc_LastActivityDt>--><!-- 29653 End -->
						<!--	</com.csc_LastActivityInfo>
							<PolicySummaryInfo>Insert Amendment Twin</PolicySummaryInfo>-->
							<PolicySummaryInfo>
								<FullTermAmt>
									<Amt><xsl:value-of select="/*/PMSPWC02__RECORD/POLPREM"/></Amt>
								</FullTermAmt>
								<PolicyStatusCd><xsl:value-of select="/*/PMSP0200__RECORD/TYPE0ACT"/></PolicyStatusCd>
								<com.csc_LastAcyDt>
								    <xsl:if test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:if>
								</com.csc_LastAcyDt>
								<com.csc_TransAmt>
								    <Amt><xsl:value-of select="/*/PMSP0200A__RECORD/TOT0AG0PRM"/></Amt>
								</com.csc_TransAmt>								
								<com.csc_ReasonAmendedCd><xsl:value-of select="/*/PMSP0200A__RECORD/REASAMNDCD"/></com.csc_ReasonAmendedCd>
							</PolicySummaryInfo>
							<!-- 55614 end -->
						</xsl:when>
						<xsl:when test="$LOB='APV'">
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Person"/><!-- 33385 -->
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="PersPolicyPA"/><!-- 33385 -->
							<PersAutoLineBusiness>
								<LOBCd>APV</LOBCd>
								<xsl:for-each select="/*/ASBLCPL1__RECORD[BLC7ST != 'Y']"><!-- 33385 -->
									<xsl:call-template name="CreatePersDriver"/>
								</xsl:for-each>
								<xsl:for-each select="/*/ASBJCPL1__RECORD[BJC7ST != 'Y']"><!-- 33385 -->
									<xsl:call-template name="CreatePersVeh"/>
								</xsl:for-each><!-- 33385 Begin -->
								<com.csc_NextDriverAssignedId>
									<xsl:for-each select="/*/ASBLCPL1__RECORD">
										<xsl:sort select="BLA0NB"/>
										<xsl:if test="position() = count(//ASBLCPL1__RECORD)">
											<xsl:value-of select="BLA0NB+2"/>
										</xsl:if>
									</xsl:for-each>
								</com.csc_NextDriverAssignedId>
								<com.csc_NextVehicleAssignedId>
									<xsl:for-each select="/*/ASBJCPL1__RECORD">
										<xsl:sort select="BJAENB"/>
										<xsl:if test="position() = count(//ASBJCPL1__RECORD)">
											<xsl:value-of select="BJAENB+2"/>
										</xsl:if>
									</xsl:for-each>
								</com.csc_NextVehicleAssignedId><!-- 33385 End -->
							</PersAutoLineBusiness><!-- 33385 Begin --><!-- 29653 Start --><!--	 <com.csc_LastActivityInfo> --><!--	  	 <com.csc_LastActivityTypeCd />  --><!--		 <com.csc_LastActivityDt> --><!--		 <xsl:call-template name="ConvertPTDateToISODate"> --><!-- <xsl:with-param name="Value"  select="/com.csc_PolicySyncRs/PMSP0200A__RECORD/EFFECTDATE"/> --><!-- </xsl:call-template> --><!--		</com.csc_LastActivityDt> --><!--	   </com.csc_LastActivityInfo> -->
							<!-- 55614 begin
							<com.csc_LastActivityDt>
								<xsl:choose>
									<xsl:when test="/*/PMSP0200A__RECORD/EFFECTDATE">
										<xsl:call-template name="ConvertPTDateToISODate">
											<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
										</xsl:call-template>
									</xsl:when>
								</xsl:choose>
							</com.csc_LastActivityDt>  --> <!--29653 End --><!-- 33385 End -->
							<PolicySummaryInfo>
								<FullTermAmt>
									<Amt><xsl:value-of select="/*/PMSP0200__RECORD/TOT0AG0PRM"/></Amt>
								</FullTermAmt>
								<PolicyStatusCd><xsl:value-of select="/*/PMSP0200__RECORD/TYPE0ACT"/></PolicyStatusCd>
								<com.csc_LastAcyDt>
								    <xsl:if test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:if>
								</com.csc_LastAcyDt>
								<com.csc_TransAmt>
								   <Amt><xsl:value-of select="/*/PMSP0200A__RECORD/TOT0AG0PRM"/></Amt>
								</com.csc_TransAmt>
								<com.csc_ReasonAmendedCd><xsl:value-of select="/*/PMSP0200A__RECORD/REASAMNDCD"/></com.csc_ReasonAmendedCd>
							</PolicySummaryInfo>
							<!-- 55614 end -->
						</xsl:when><!--Case 34771 start -->
						<xsl:when test="$LOB='BOP'">
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
							<xsl:for-each select="/*/ASBUCPL1__RECORD[BUC7ST!='Y']">
								<xsl:call-template name="CreateLocation"/>
							</xsl:for-each>
							<BOPLineBusiness>
								<LOBCd>BO</LOBCd>
								<xsl:for-each select="/*/ASBBCPL1__RECORD">
									<xsl:call-template name="CreateCommlProperty">
										<xsl:with-param name="InsLine">BO</xsl:with-param>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:for-each select="/*/ASBCCPL1__RECORD[BCC7ST!='Y']">
									<com.csc_ModificationFactors>
										<StateProvCd>
											<xsl:value-of select="BCADCD"/>
										</StateProvCd>
										<xsl:call-template name="CreateModFactors">
											<xsl:with-param name="StateProvCd">
												<xsl:value-of select="BCADCD"/>
											</xsl:with-param>
										</xsl:call-template>
									</com.csc_ModificationFactors>
								</xsl:for-each>
							</BOPLineBusiness>
							<xsl:for-each select="/*/ASB5CPL1__RECORD[B5C7ST!='Y']">
								<xsl:call-template name="CreateBOCommlSubLocation"/>
							</xsl:for-each>
							<!-- 55614 begin 
							<com.csc_LastActivityInfo>
								<com.csc_LastActivityTypeCd/>
								<com.csc_LastActivityDt>
									<xsl:choose>
										<xsl:when test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:when>
									</xsl:choose>
								</com.csc_LastActivityDt>
							</com.csc_LastActivityInfo>
							<PolicySummaryInfo>Insert Amendment Twin</PolicySummaryInfo>-->
							<PolicySummaryInfo>
								<FullTermAmt>
									<Amt><xsl:value-of select="/*/ASBACPL1__RECORD/BAA3VA"/></Amt>
								</FullTermAmt>
								<PolicyStatusCd><xsl:value-of select="/*/PMSP0200__RECORD/TYPE0ACT"/></PolicyStatusCd>
								<com.csc_LastAcyDt>
								    <xsl:if test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:if>
								</com.csc_LastAcyDt>
								<com.csc_TransAmt>
								   <Amt><xsl:value-of select="/*/PMSP0200A__RECORD/TOT0AG0PRM"/></Amt>
								</com.csc_TransAmt>
								<com.csc_ReasonAmendedCd><xsl:value-of select="/*/PMSP0200A__RECORD/REASAMNDCD"/></com.csc_ReasonAmendedCd>
							</PolicySummaryInfo>
							<!-- 55614 end -->
						</xsl:when><!--Case 34771 end -->
					</xsl:choose>
				</com.csc_PolicySyncRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet>