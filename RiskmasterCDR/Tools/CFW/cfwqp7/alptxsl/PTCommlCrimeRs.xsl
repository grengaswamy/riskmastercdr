<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateCommlCrime">
        <com.csc_CrimeLineBusiness>
            <LOBCd>CRIME</LOBCd>
            <CurrentTermAmt>
                <Amt><xsl:value-of select="BBA3VA"/></Amt>
            </CurrentTermAmt>
            <com.csc_CommlCrimeInfo>
<!-- Build Comml Crime Classification Aggregates -->
                            <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAGTX='CR']"> 
<!-- Ignore Coverage Minimum Premiums. They are generated below. -->
<xsl:sort select="BYBRNB"/><!--Issue 102807 -->
                                <xsl:variable name="IsMinPremCov">
                                    <xsl:call-template name="MinPremLookup">
                                        <xsl:with-param name="InsLine">CR</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:if test="$IsMinPremCov='N'">
        <xsl:variable name="Location" select="BYBRNB"/>
        <xsl:variable name="SubLocation" select="BYEGNB"/>
        <xsl:variable name="CovCode" select="normalize-space(BYAOTX)"/>
        <xsl:variable name="ACORDCovCode">
           <xsl:call-template name="TranslateCoverageCode">
              <xsl:with-param name="InsLine">CR</xsl:with-param>
              <xsl:with-param name="CovCode" select="$CovCode"/>
           </xsl:call-template>
        </xsl:variable>                
        <!--Issue 102807 starts-->
            <xsl:variable name="GetForm">
            <xsl:if test="not(preceding-sibling::ASBYCPL1__RECORD[BYBRNB=$Location and BYAGTX='CR'])">
                   <xsl:text>1</xsl:text>
           </xsl:if>
           </xsl:variable>    
           <!--Issue 102807 ends-->
        <com.csc_CommlCrimeClassification>
            <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
            <xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
		    <ClassCd><xsl:value-of select="BYNDTX"/></ClassCd>
		    <ClassCdDesc/>
		    <PMACd><xsl:value-of select="BYAMCD"/></PMACd>
		    <TerritoryCd><xsl:value-of select="BYAGNB"/></TerritoryCd>
            <CommlCoverage>
                <CoverageCd><xsl:value-of select="$ACORDCovCode"/></CoverageCd>
                <CoverageDesc/>
				<!--59878 start--> <IterationNumber><xsl:value-of select="BYC0NB"/></IterationNumber>
				<!--59878 ends-->
                <Limit>
                   <FormatCurrencyAmt>
                      <Amt><xsl:value-of select="BYA5VA"/></Amt>
                   </FormatCurrencyAmt>
                   <LimitAppliesToCd/>
                </Limit>
    			<Deductible>
    			    <FormatCurrencyAmt>
    				    <Amt><xsl:value-of select="BYPPTX"/></Amt>
    			    </FormatCurrencyAmt>
    			    <DeductibleTypeCd/>
    			</Deductible>
                <CurrentTermAmt>
                    <Amt><xsl:value-of select="BYA3VA"/></Amt>
 				    <com.csc_OverrideInd>
				        <xsl:choose>
				            <xsl:when test="BYD3ST='0'">1</xsl:when>
				            <xsl:otherwise>0</xsl:otherwise>
				        </xsl:choose>
				    </com.csc_OverrideInd>
                </CurrentTermAmt>
<!-- Generate the Coverage Minimum Premium aggregate if applicable -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">CR</xsl:with-param>
			    <xsl:with-param name="Location" select="$Location"/>
			    <xsl:with-param name="SubLocation" select="$SubLocation"/>
			    <xsl:with-param name="CovCode" select="$CovCode"/>
			</xsl:call-template>
            </CommlCoverage>			
			<!--Issue 102807 starts-->
	     		<xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CR' and BEBRNB =$Location]">
        	               <xsl:if test="$GetForm = '1'">
								    <xsl:call-template name="BuildCPPForm"/>
	    					 </xsl:if>
              </xsl:for-each>
		     <!--Issue 102807 ends-->
        </com.csc_CommlCrimeClassification>
       </xsl:if>
      </xsl:for-each>
	  	<!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CR'and BEBRNB='0']">
	  
		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	
            </com.csc_CommlCrimeInfo>
 <!-- Now generate the Insurance Line Minimum Premium aggregate -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">CR</xsl:with-param>
			    <xsl:with-param name="CovCode">INSLIN</xsl:with-param>
			</xsl:call-template>
<!-- Build Modification Factors -->
            <xsl:for-each select="/*/ASBCCPL1__RECORD[BCAGTX='CR']">
    		    <com.csc_ModificationFactors>
        			<StateProvCd><xsl:value-of select="BCADCD"/></StateProvCd>
<!-- Schedule Modifier -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">SCH</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCBLPC"/></xsl:with-param>
        			</xsl:call-template>
<!-- IRPM Modifier -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">IRPM</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCANPC"/></xsl:with-param>
        			</xsl:call-template>
    		    </com.csc_ModificationFactors>
    		</xsl:for-each>
<!-- Build Forms --><!-- ICH00284B -->
	        <!--<xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CR']">-->		<!--UNI00284B--><!--Issue 102807 commented out-->
			<!--<xsl:call-template name="BuildCPPForm" />--><!--Issue 102807 commented out-->
		<!--/xsl:for-each-->
        </com.csc_CrimeLineBusiness>
    </xsl:template>

</xsl:stylesheet>

