<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateViolationRec">
       <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBMCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBMCPL1__RECORD>
            <BMAACD>
               <xsl:value-of select="$LOC"/>
            </BMAACD>
            <BMABCD>
               <xsl:value-of select="$MCO"/>
            </BMABCD>
            <BMARTX>
               <xsl:value-of select="$SYM"/>
            </BMARTX>
            <BMASTX>
               <xsl:value-of select="$POL"/>
            </BMASTX>
            <BMADNB>
               <xsl:value-of select="$MOD"/>
            </BMADNB>
            <BMAGTX>
               <xsl:value-of select="$LOB"/>
            </BMAGTX>
            <BMBRNB>00001</BMBRNB>
            <BMEGNB>00001</BMEGNB>
            <BMANTX>
			  <!--<xsl:value-of select="$SYM"/>--> <!--issue # 40090-->
			     <xsl:value-of select="$LOB"/>  <!--issue # 40090-->
            </BMANTX>
            <BMA0NB>
	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">BMA0NB</xsl:with-param>      
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@DriverRef,2,string-length(@DriverRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BMA0NB>
            <BMBKTX>
               <xsl:value-of select="AccidentViolationCd"/>
            </BMBKTX>
<!-- begin 27834            
            <BMEYNB>1</BMEYNB>
-->
   <xsl:choose>
       <xsl:when test="string-length(@id) &gt; 0">
             <BMEYNB><xsl:value-of select="substring(@id,2,string-length(@id)-1)"/></BMEYNB>
       </xsl:when>
       <xsl:otherwise>
            <BMEYNB><xsl:value-of select="position()"/></BMEYNB>
       </xsl:otherwise>
   </xsl:choose>
<!-- end 27834 -->                
            <BMC6ST>P</BMC6ST>
            <BMANDT>0000000</BMANDT>
            <BMAFNB>00</BMAFNB>
            <BMALDT>0000000</BMALDT>
            <BMAFDT>0000000</BMAFDT>
             <BMTYPE0ACT>
		<!-- 34771 start -->
		 <!--103409 starts-->
		 <!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		 <xsl:call-template name="ConvertPolicyStatusCd">
			 <xsl:with-param name="PolicyStatusCd">
				 <xsl:value-of select="//PolicyStatusCd"/>
			 </xsl:with-param>
		 </xsl:call-template>
		 <!--103409 Ends-->
				 
		<!-- 34771 end -->
            </BMTYPE0ACT>

            <BMAIDT>
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="AccidentViolationDt"/>
               </xsl:call-template>
            </BMAIDT>
         </ASBMCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
