<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateInlandMarine">
        <com.csc_CommlInlandMarineLineBusiness>
            <LOBCd>INMRC</LOBCd>
            <CurrentTermAmt>
                <Amt><xsl:value-of select="BBA3VA"/></Amt>
            </CurrentTermAmt>
            <com.csc_CommlInlandMarineInfo>
<!-- Build Comml Crime Classification Aggregates -->
			<!-- Issue 59878 - Start -->
            <!--xsl:for-each select="/*/ASBYCPL1__RECORD[BYAGTX='IMC']"-->                
			<xsl:for-each select="key('ILKey', 'IMC')">
			<!-- Issue 59878 - End -->
<!-- Ignore Coverage Minimum Premiums. They are generated below. -->
 <xsl:sort select="BYBRNB"/><!--Issue 102807 -->
                    <xsl:variable name="IsMinPremCov">
                        <xsl:call-template name="MinPremLookup">
                            <xsl:with-param name="InsLine">IMC</xsl:with-param>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:if test="$IsMinPremCov='N'">
        <xsl:variable name="Location" select="BYBRNB"/>
        <xsl:variable name="SubLocation" select="BYEGNB"/>
        <xsl:variable name="CovCode" select="normalize-space(BYAOTX)"/>
        <xsl:variable name="ACORDCovCode">
            <xsl:call-template name="TranslateCoverageCode">
                <xsl:with-param name="InsLine">IMC</xsl:with-param>
                <xsl:with-param name="CovCode" select="$CovCode"/>
            </xsl:call-template>
        </xsl:variable>        
        <!--Issue 102807 starts-->
            <xsl:variable name="GetForm">
            <xsl:if test="not(preceding-sibling::ASBYCPL1__RECORD[BYBRNB=$Location and BYAGTX='IM'])">
                   <xsl:text>1</xsl:text>
           </xsl:if>
           </xsl:variable>    
           <!--Issue 102807 ends-->       
        <com.csc_CommlInlandMarineClassification>
            <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
            <xsl:if test="$SubLocation > 0">
            <xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
            </xsl:if>
            <TerritoryCd><xsl:value-of select="BYAGNB"/></TerritoryCd>        
            <PMACd><xsl:value-of select="BYPLTX"/></PMACd> 
            <ClassCd><xsl:value-of select="BYNDTX"/></ClassCd> 
            <ClassCdDesc><xsl:value-of select="BYKKTX"/></ClassCdDesc> 
            <com.csc_CoverageTypeCd><xsl:value-of select="BYAMCD"/></com.csc_CoverageTypeCd> 
            <com.csc_BaseRate><xsl:value-of select="BYAFPC"/></com.csc_BaseRate> 
            <CommlCoverage>
                <CoverageCd><xsl:value-of select="$ACORDCovCode"/></CoverageCd>
                <CoverageDesc/>
				<!-- Issue 59878 - Start -->
				<IterationNumber><xsl:value-of select="BYC0NB"/></IterationNumber>
				<!-- Issue 59878 - End -->
                <xsl:if test="BYA9NB != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt>
                              <xsl:value-of select="BYA9NB"/>
                          </Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>com.csc_SCH</LimitAppliesToCd>
                    </Limit>
                </xsl:if>
                <xsl:if test="BYUSVA5 != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt>
                              <xsl:value-of select="BYUSVA5"/>
                          </Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>com.csc_UNSCH</LimitAppliesToCd>
                    </Limit>
                </xsl:if>                                
                <xsl:if test="BYAGVA != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt><xsl:value-of select="BYAGVA"/></Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>PREM</LimitAppliesToCd>
                    </Limit>
                </xsl:if>
				<!-- Issue 59878 - Start -->
				<xsl:choose>
					<xsl:when test="normalize-space($ACORDCovCode)='TRANS'">
						<xsl:if test="BYALV1VAL != '0.00'">
		                    <Limit>
		                       <FormatCurrencyAmt>
		                          <Amt>
		                              <xsl:value-of select="BYALV1VAL"/>
		                          </Amt>
		                       </FormatCurrencyAmt>
		                       <LimitAppliesToCd/>
		                    </Limit>
		                </xsl:if>
					</xsl:when>
					<xsl:when test="normalize-space($ACORDCovCode)='TRIP'">
						<xsl:if test="BYUSVA3 != '0.00'">
		                    <Limit>
		                       <FormatCurrencyAmt>
		                          <Amt>
		                              <xsl:value-of select="BYUSVA3"/>
		                          </Amt>
		                       </FormatCurrencyAmt>
		                       <LimitAppliesToCd/>
		                    </Limit>
		                </xsl:if>
					</xsl:when>
					<xsl:otherwise>
				<!-- Issue 59878 - End -->
                <xsl:if test="BYA5VA != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt>
                              <xsl:value-of select="BYA5VA"/>
                          </Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>OFF</LimitAppliesToCd>
                    </Limit>
                </xsl:if>
				<!-- Issue 59878 - Start -->
				</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="BYALV0VAL != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt>
                              <xsl:value-of select="BYALV0VAL"/>
                          </Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>OFF</LimitAppliesToCd>
                    </Limit>
                </xsl:if>
				<xsl:if test="BYALV1VAL != '0.00'">
                    <Limit>
                       <FormatCurrencyAmt>
                          <Amt>
                              <xsl:value-of select="BYALV1VAL"/>
                          </Amt>
                       </FormatCurrencyAmt>
                       <LimitAppliesToCd>OFF</LimitAppliesToCd>
                    </Limit>
                </xsl:if>
				<!-- Issue 59878 - End -->
    			<Deductible>
   				    <xsl:choose>
    				       <xsl:when test="normalize-space($ACORDCovCode)='SIGN'">
    				 <FormatPct><xsl:value-of select="BYANCD"/>%</FormatPct>      
     				       </xsl:when>
    				       <xsl:otherwise>
    			    <FormatCurrencyAmt>
    				    <Amt><xsl:value-of select="BYPPTX"/></Amt>
     			    </FormatCurrencyAmt>   				    
                           </xsl:otherwise>
    				    </xsl:choose>    			
     			    <DeductibleTypeCd/>
    			</Deductible>
                <CurrentTermAmt>
                    <Amt><xsl:value-of select="BYA3VA"/></Amt>
				    <com.csc_OverrideInd>
				        <xsl:choose>
				            <xsl:when test="BYD3ST='0'">1</xsl:when>
				            <xsl:otherwise>0</xsl:otherwise>
				        </xsl:choose>
				    </com.csc_OverrideInd>
                </CurrentTermAmt>
            </CommlCoverage>
<!-- Generate the Coverage Minimum Premium aggregate if applicable -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">IMC</xsl:with-param>
			    <xsl:with-param name="Location" select="$Location"/>
			    <xsl:with-param name="SubLocation" select="$SubLocation"/>
			    <xsl:with-param name="CovCode" select="$CovCode"/>
			</xsl:call-template>
				<!--Issue 102807 starts-->
	     		<xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='IMC' and BEBRNB =$Location]">
        	               <xsl:if test="$GetForm = '1'">
								    <xsl:call-template name="BuildCPPForm"/>
	    					 </xsl:if>
              </xsl:for-each>
		     <!--Issue 102807 ends-->
        </com.csc_CommlInlandMarineClassification>
                    </xsl:if>
                </xsl:for-each>
                   <!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='IMC'and BEBRNB='0']">
	  		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	
         </com.csc_CommlInlandMarineInfo>                                 
<!-- Now generate the Insurance Line Minimum Premium aggregate -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">IMC</xsl:with-param>
			    <xsl:with-param name="CovCode">INSLIN</xsl:with-param>
			</xsl:call-template>
<!-- Build Modification Factors -->
            <xsl:for-each select="/*/ASBCCPL1__RECORD[BCAGTX='IMC']">
    		    <com.csc_ModificationFactors>
        			<StateProvCd><xsl:value-of select="BCADCD"/></StateProvCd>
<!-- IRPM Modifier -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">IRPM</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCANPC"/></xsl:with-param>
        			</xsl:call-template>
<!-- Rate Mod Factor -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">RDF</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCALPC"/></xsl:with-param>
        			</xsl:call-template>
    		    </com.csc_ModificationFactors>
    		</xsl:for-each>
<!-- Build Forms --><!-- ICH00284B -->
	<!--Issue 102807 commented out startrs-->
	        <!--xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='IMC']">		
			<xsl:call-template name="BuildCPPForm" />
		</xsl:for-each-->
		<!--Issue 102807 commented out end-->
        </com.csc_CommlInlandMarineLineBusiness>
    </xsl:template>

</xsl:stylesheet>

