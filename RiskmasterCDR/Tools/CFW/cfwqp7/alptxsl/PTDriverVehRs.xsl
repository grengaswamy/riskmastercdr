<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreateDriverVeh">
	     <xsl:variable name="DriverNbr" select="BLA0NB"/>
	     <xsl:variable name="UsePct" select="/*/ASBJCPL1__RECORD[BJAKNB=$DriverNbr]/BJANCD"/>
         <xsl:if test="BLBFTX &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v',BLBFTX)"/></xsl:attribute>
                <xsl:variable name="VehRef"><xsl:value-of select="BLBFTX"/></xsl:variable>
				<UsePct>100</UsePct>
            </DriverVeh>
         </xsl:if>
         <xsl:if test="BLBGTX &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v', BLBGTX)"/></xsl:attribute>                            
                <xsl:variable name="VehRef"><xsl:value-of select="BLBGTX"/></xsl:variable>
               <UsePct>100</UsePct>                               
            </DriverVeh>
         </xsl:if>
         <xsl:if test="BLBJTX &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v', BLBJTX)"/></xsl:attribute>                            
                <xsl:variable name="VehRef"><xsl:value-of select="BLBJTX"/></xsl:variable>
                <UsePct>100</UsePct>
            </DriverVeh>
         </xsl:if>
         <xsl:if test="BLBHTX &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v', BLBHTX)"/></xsl:attribute>
                <xsl:variable name="VehRef"><xsl:value-of select="BLBHTX"/></xsl:variable>
                <UsePct>50</UsePct>
            </DriverVeh>
         </xsl:if>
         <xsl:if test="BLBITX &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v', BLBITX)"/></xsl:attribute>                            
                <xsl:variable name="VehRef"><xsl:value-of select="BLBITX"/></xsl:variable>
                <UsePct>50</UsePct>
            </DriverVeh>
         </xsl:if>
         <xsl:if test="BLA1NB &gt; '0'">
            <DriverVeh>
                <xsl:attribute name="DriverRef"><xsl:value-of select="concat('d', BLA0NB)"/></xsl:attribute>
                <xsl:attribute name="VehRef"><xsl:value-of select="concat('v', BLA1NB)"/></xsl:attribute>                            
                <xsl:variable name="VehRef"><xsl:value-of select="BLA1NB"/></xsl:variable>
                <UsePct>50</UsePct>
            </DriverVeh>
         </xsl:if>                        	                                   	                                   	
	</xsl:template>	
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->