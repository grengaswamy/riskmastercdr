<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreatePersDriver">
		<PersDriver>
		    <xsl:variable name="DriverNbr" select="BLA0NB"/>
                <xsl:attribute name="id"><xsl:value-of select="concat('d',$DriverNbr)"/></xsl:attribute>
         	    <ItemIdInfo>
         	       <InsurerId><xsl:value-of select="$DriverNbr"/></InsurerId>
        	    </ItemIdInfo>
	       <GeneralPartyInfo>
                  <NameInfo>
	             <PersonName>
                        <Surname>
                            <xsl:value-of select="BLBCTX"/>
                        </Surname>
                        <GivenName>
                            <xsl:value-of select="BLI7TX"/>
                        </GivenName>
		        <OtherGivenName>
                            <xsl:value-of select="BLI8TX"/>
		        </OtherGivenName>
		        <TitlePrefix/>
		        <!--start case 34296-->
		        <!--NameSuffix/-->
		        <NameSuffix><xsl:value-of select="BLI9TX"/></NameSuffix>
		        <!--end case 34296-->
                     </PersonName>
                     <TaxIdentity>
                        <TaxIdTypeCd>SSN</TaxIdTypeCd> 
                        <TaxId/>
                     </TaxIdentity>
                  </NameInfo>
	       </GeneralPartyInfo>
               <DriverInfo>
                  <PersonInfo>
                     <GenderCd>
                            <xsl:value-of select="BLA7ST"/>
                     </GenderCd>
                     <BirthDt>
                         <xsl:call-template name="ConvertPTDateToISODate">
                         <xsl:with-param name="Value"  select="BLAGDT"/>
                        </xsl:call-template>
                     </BirthDt>
                     <MaritalStatusCd>
                            <xsl:value-of select="BLA8ST"/>
                     </MaritalStatusCd>
                  </PersonInfo>
                  <DriversLicense>
                     <LicensedDt>
                         <xsl:call-template name="ConvertPTDateToISODate">
                         <xsl:with-param name="Value"  select="BLAHDT"/>
                        </xsl:call-template>
                     </LicensedDt>
                     <DriversLicenseNumber>
                            <xsl:value-of select="BLBDTX"/>
                     </DriversLicenseNumber>
                     <StateProvCd>
                            <xsl:value-of select="BLBETX"/>
                     </StateProvCd>
                  </DriversLicense>
               </DriverInfo>
               <PersDriverInfo>
                  <DefensiveDriverDt/>
                  <DistantStudentInd>
                            <xsl:value-of select="BLNATX"/>
                  </DistantStudentInd> 
                  <DriverRelationshipToApplicantCd>
                            <xsl:value-of select="BLA9ST"/>
                  </DriverRelationshipToApplicantCd>
                  <DriverTrainingInd>
                            <xsl:value-of select="BLM7TX"/>
                  </DriverTrainingInd>
                  <GoodDriverInd>
                            <xsl:value-of select="BLM6TX"/>
                  </GoodDriverInd>
                  <GoodStudentCd>
                            <xsl:value-of select="BLM8TX"/>
                  </GoodStudentCd>
               </PersDriverInfo>
 		</PersDriver>
	</xsl:template>
</xsl:stylesheet>

