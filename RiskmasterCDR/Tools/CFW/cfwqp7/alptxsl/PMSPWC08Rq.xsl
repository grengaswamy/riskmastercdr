<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Add)
-->
<!-- ???? Start -->
<!--<xsl:template match="ACORD/InsuranceSvcRq/*" mode="PMSPWC08"> -->
<xsl:template match="ACORD/InsuranceSvcRq/*" mode="CreateEmpLimits">
<!-- ??? End -->
   <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC08</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC08__RECORD>
	 <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
       	<BIACCIDENT>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIACCIDENT>
		<BIDISEMP>
			 <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISEMP>
		<BIDISPOL>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISPOL>
		<ENDEFFDATE>
			<xsl:call-template name="ConvertISODateToPTDate">
				<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
			</xsl:call-template>
		</ENDEFFDATE>
		<EMPLVCIND>E</EMPLVCIND>
	</PMSPWC08__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
  
  
  <!-- ??? start -->
  <xsl:template match="ACORD/InsuranceSvcRq/*" mode="CreateVolLimits">
   <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC08</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC08__RECORD>
	 <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
       	<BIACCIDENT>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIACCIDENT>
		<BIDISEMP>
			 <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISEMP>
		<BIDISPOL>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISPOL>
		<ENDEFFDATE>
			<xsl:call-template name="ConvertISODateToPTDate">
				<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
			</xsl:call-template>
		</ENDEFFDATE>
		<EMPLVCIND>V</EMPLVCIND>
	</PMSPWC08__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
  <!-- ??? End -->  
  
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->