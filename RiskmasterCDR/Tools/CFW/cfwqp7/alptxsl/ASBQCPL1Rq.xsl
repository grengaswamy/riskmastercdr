<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="ACORD/InsuranceSvcRq/*/HomeLineBusiness/Dwell" mode="ASBQCPL1">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBQCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBQCPL1__RECORD>
             <BQAACD>
               <xsl:value-of select="$LOC"/>
            </BQAACD>
            <BQABCD>
               <xsl:value-of select="$MCO"/>
            </BQABCD>
            <BQARTX>
               <xsl:value-of select="$SYM"/>
            </BQARTX>
            <BQASTX>
               <xsl:value-of select="$POL"/>
            </BQASTX>
            <BQADNB>
               <xsl:value-of select="$MOD"/>
            </BQADNB>
            <BQAGTX>
               <xsl:value-of select="$LOB"/>
            </BQAGTX>
            <BQBRNB>00001</BQBRNB>
            <BQEGNB>00001</BQEGNB>
            <BQANTX>HP</BQANTX>
            <BQAENB>00001</BQAENB>
            <BQC6ST>P</BQC6ST>
            <BQANDT>0000000</BQANDT>
            <BQAFNB>00</BQAFNB>
            <BQALDT>0000000</BQALDT>
            <BQAFDT>0000000</BQAFDT>
			<!-- 34770 start -->
			<!--<BQTYPE0ACT>NB</BQTYPE0ACT>-->
	            <BQTYPE0ACT>
		<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> <!-- 50764 -->
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> <!-- 50764 -->
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> <!-- 50764 -->
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> <!-- 39039 -->
		</xsl:choose>
            </BQTYPE0ACT>
			<!-- 34770 end -->
            <BQBCCD>
               <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/>
            </BQBCCD>
            <BQAGNB>
               <xsl:value-of select="DwellRating/TerritoryCd"/>
            </BQAGNB>
            <BQHXTX>
               <xsl:value-of select="PolicyTypeCd"/>
            </BQHXTX>
            <BQHYTX>
               <xsl:value-of select="Construction/ConstructionCd"/>
            </BQHYTX>
            <BQHZTX>
               <xsl:value-of select="Coverage[CoverageCd = 'FVREP']/Option/OptionValue"/>
            </BQHZTX>
            <BQH0TX>
               <xsl:value-of select="DwellOccupancy/OccupancyTypeCd"/>
            </BQH0TX>
            <BQH1TX>
               <xsl:value-of select="BldgProtection/FireProtectionClassCd"/>
            </BQH1TX>
			<BQBICD>N</BQBICD>
			<BQBJCD>N</BQBJCD>
			<BQAPNB>
				<!--issue # 34770 Starts-->
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/PostalCode"/>
				<!--issue # 34770 Ends-->
			</BQAPNB>
			<BQBLCD>
               <xsl:value-of select="BldgProtection/ProtectionDeviceBurglarCd"/>
            </BQBLCD>
			<!-- Case 36997, 29405 start-->
			<!--<BQBNCD>N</BQBNCD>-->
			<BQBNCD>
				<xsl:choose>
				<xsl:when test="DwellOccupancy/NumApartments='1' or DwellOccupancy/NumApartments='2'">1</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments='3' or DwellOccupancy/NumApartments='4'">3</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments='5' or DwellOccupancy/NumApartments='6' or DwellOccupancy/NumApartments='7' or DwellOccupancy/NumApartments='8'">5</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments >= '9'">9</xsl:when>
				<xsl:otherwise>N</xsl:otherwise> 
				</xsl:choose>				
			</BQBNCD>		 
			<!-- Case 36997, 29405 end-->
			<!-- Case 36995, 29405 start -->		 
			<!--<BQBOCD>N</BQBOCD>-->
			<BQBOCD>
				<xsl:value-of select="Coverage[CoverageCd = 'com.csc_TheftDed']/Option/OptionValue"/>
			</BQBOCD>
			<!-- Case 36995, 29405 end -->
			<!-- Case 36996, 29405 start -->
			<BQAEDT>
				<xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="PurchaseDt"/>
               </xsl:call-template>				
			</BQAEDT>
			<!-- Case 36996, 29405 end -->
			<!-- Case 36998, 29405 start -->
			<BQH2TX>
			  <!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/Addr/County"/>--> <!--case 34770-->
			  <xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/County"/><!-- cASE 34770 MODIFIED xxxx-->
			</BQH2TX>
			<!-- Case 36998, 29405 end -->
            <BQBPCD>
              	<xsl:value-of select="Coverage[CoverageCd = 'WINDX']/Option/OptionValue"/>
            </BQBPCD>
			<BQBQCD>4</BQBQCD>            
            <BQBRCD>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Deductible/DeductibleTypeCd"/>
            </BQBRCD>             
			<BQBSCD>N</BQBSCD>
             <BQJBTX>
               <xsl:value-of select="Coverage[CoverageCd = 'com.csc_VALUP']/Option/OptionValue"/>
            </BQJBTX>  
			<BQJCTX>N</BQJCTX>                       
			<!--Start Case 37041 -->
			<!--<BQDBNB>N</BQDBNB>-->
			<BQDBNB>				
			 	<xsl:value-of select="Construction/RoofingMaterial/RoofMaterialCd"/><!-- fd02 IT IS COMMENTED BECUSE OF RATING ERROR-->	
			</BQDBNB>		 
			<!--Ends Case 37041-->
			<BQDCNB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/Addr1"/></BQDCNB>
			 <!--63722 Issue 431 begin-->
			 <BQDDNB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/City"/></BQDDNB> <!--issue # 34770-->
			 <BQDENB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/Addr2"/></BQDENB> <!--issue # 49441-->
			 <!--63722 Issue 431 end-->
            <BQDGNB>
               <xsl:value-of select="DwellInspectionValuation/NumFamilies"/>
            </BQDGNB>
            <BQDHNB>
               <xsl:value-of select="BldgProtection/DistanceToHydrant/NumUnits"/>
            </BQDHNB>
            <BQDINB>
               <xsl:value-of select="BldgProtection/DistanceToFireStation/NumUnits"/>
            </BQDINB>            
            <BQDLNB>
               <xsl:value-of select="Construction/YearBuilt"/>
            </BQDLNB>
            <xsl:variable name="INFGD" select="Coverage[CoverageCd = 'INFGD']/Option/OptionValue"/>
             <xsl:if test = "string-length($INFGD)">                              
             <BQAWPC>
                <xsl:value-of select="concat($INFGD, '.00000')"/>
            </BQAWPC>
            </xsl:if>                          
            <BQA6VA>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Limit/FormatCurrencyAmt/Amt"/>
            </BQA6VA>
            <BQA8VA>
               <xsl:value-of select="Coverage[CoverageCd='OS']/Limit/FormatCurrencyAmt/Amt"/>
            </BQA8VA>
            <BQBAVA>
               <xsl:value-of select="Coverage[CoverageCd='PP']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBAVA>
            <BQBCVA>
               <xsl:value-of select="Coverage[CoverageCd='LOU']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBCVA>
            <BQBEVA>
               <xsl:value-of select="Coverage[CoverageCd='PL']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBEVA>
            <BQBGVA>
               <xsl:value-of select="Coverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBGVA>
            <BQA9NB>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Deductible/FormatCurrencyAmt/Amt"/>
            </BQA9NB>
			<!-- Case start XXXX for FD02 34770 -->		 
			 <!--<BQCZST>N</BQCZST>-->
			 <BQCZST>			 
				<xsl:choose>
				<xsl:when test="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue='1' ">1</xsl:when>
				<xsl:when test="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue='2' ">2</xsl:when>
				<xsl:when test="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue='3' ">5</xsl:when>				
				<xsl:otherwise>N</xsl:otherwise> 
				</xsl:choose>						
			 <!--<xsl:value-of select="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue"/> 20may-->
			 </BQCZST>
			 <!-- Case Ends XXXX for FD02 34770 -->
			<BQC0ST>N</BQC0ST>
			<BQC1ST>N</BQC1ST>
			 <!--Issue 65776 Starts-->
			 <BQAEPC>
		          <xsl:value-of select="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue"/>
		     </BQAEPC>  
		    <!--Issue 65776 Ends-->
			<BQC2ST>N</BQC2ST>
			<BQIYTX>N</BQIYTX>
			<BQIZTX>N</BQIZTX>
			<BQI0TX>N</BQI0TX>
			<BQI1TX>N</BQI1TX>
            <BQAKCD>N</BQAKCD>
			<BQAMCD>N</BQAMCD>
			<BQBLNB>1</BQBLNB>
			<BQJ1TX>H</BQJ1TX>
            <BQBUVA>
               <xsl:value-of select="PurchasePriceAmt/Amt"/>
            </BQBUVA>
            <BQM1TX>
               <xsl:value-of select="AreaTypeSurroundingsCd"/>
            </BQM1TX>            
         </ASBQCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
   <xsl:template match="ACORD/InsuranceSvcRq/*/DwellFireLineBusiness/Dwell" mode="ASBQCPL1">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBQCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBQCPL1__RECORD>
             <BQAACD>
               <xsl:value-of select="$LOC"/>
            </BQAACD>
            <BQABCD>
               <xsl:value-of select="$MCO"/>
            </BQABCD>
            <BQARTX>
               <xsl:value-of select="$SYM"/>
            </BQARTX>
            <BQASTX>
               <xsl:value-of select="$POL"/>
            </BQASTX>
            <BQADNB>
               <xsl:value-of select="$MOD"/>
            </BQADNB>
            <BQAGTX>
               <xsl:value-of select="$LOB"/>
            </BQAGTX>
            <BQBRNB>00001</BQBRNB>
            <BQEGNB>00001</BQEGNB>
			<!-- 39111 Start -->
            <BQANTX>
				DP-<xsl:value-of select="PolicyTypeCd"/>
            </BQANTX>
			<!-- 39111 End -->
            <BQAENB>00001</BQAENB>
            <BQC6ST>P</BQC6ST>
            <BQANDT>0000000</BQANDT>
            <BQAFNB>00</BQAFNB>
            <BQALDT>0000000</BQALDT>
            <BQAFDT>0000000</BQAFDT>
			<!-- 34770 start -->
			<!--<BQTYPE0ACT>NB</BQTYPE0ACT>-->
	            <BQTYPE0ACT>
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
            </BQTYPE0ACT>
			<!-- 34770 end -->
            <BQBCCD>
               <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/>
            </BQBCCD>
            <BQAGNB>
               <xsl:value-of select="DwellRating/TerritoryCd"/>
            </BQAGNB>
            <BQHXTX>
               <xsl:value-of select="PolicyTypeCd"/>
            </BQHXTX>
			<!-- 99623 starts -->
            <!--<BQANCD>0100</BQANCD>-->
			<BQANCD>
				<xsl:choose>
					<xsl:when test="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/Addr/County ='Gary'">1100
					</xsl:when>
					<xsl:otherwise>0100</xsl:otherwise>
				</xsl:choose>
			</BQANCD>
			<!-- 99623 ends -->
            <BQHYTX>
               <xsl:value-of select="Construction/ConstructionCd"/>
            </BQHYTX>
            <BQHZTX>
               <xsl:value-of select="Coverage[CoverageCd = 'FVREP']/Option/OptionValue"/>
            </BQHZTX>
            <BQH0TX>
               <xsl:value-of select="DwellOccupancy/OccupancyTypeCd"/>
            </BQH0TX>
            <BQH1TX>
               <xsl:value-of select="BldgProtection/FireProtectionClassCd"/>
            </BQH1TX>
			<BQBICD>N</BQBICD>
			<BQBJCD>N</BQBJCD>
			<BQAPNB>
				<!--issue # 34770 Starts-->
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/PostalCode"/>
				<!--issue # 34770 Ends-->
			</BQAPNB>
			<BQBLCD>
               <xsl:value-of select="BldgProtection/ProtectionDeviceBurglarCd"/>
            </BQBLCD>
			<!-- Case 36997, 29405 start-->
			<!--<BQBNCD>N</BQBNCD>-->
			<BQBNCD>
				<xsl:choose>
				<xsl:when test="DwellOccupancy/NumApartments='1' or DwellOccupancy/NumApartments='2'">1</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments='3' or DwellOccupancy/NumApartments='4'">3</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments='5' or DwellOccupancy/NumApartments='6' or DwellOccupancy/NumApartments='7' or DwellOccupancy/NumApartments='8'">5</xsl:when>
				<xsl:when test="DwellOccupancy/NumApartments >= '9'">9</xsl:when>
				<xsl:otherwise>N</xsl:otherwise> 
				</xsl:choose>				
			</BQBNCD>		 
			<!-- Case 36997, 29405 end-->
			<!-- Case 36995, 29405 start -->		 
			<!--<BQBOCD>N</BQBOCD>-->
			<BQBOCD>
				<xsl:value-of select="Coverage[CoverageCd = 'com.csc_TheftDed']/Option/OptionValue"/>
			</BQBOCD>
			<!-- Case 36995, 29405 end -->
			<!-- Case 36996, 29405 start -->
			<BQAEDT>
				<xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="PurchaseDt"/>
               </xsl:call-template>				
			</BQAEDT>
			<!-- Case 36996, 29405 end -->
			<!-- Case 36998, 29405 start -->
			<BQH2TX>
			  <!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/Addr/County"/>--> <!--case 34770-->
			  <xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/County"/><!-- cASE 34770 MODIFIED xxxx-->
			</BQH2TX>
			<!-- Case 36998, 29405 end -->
            <BQBPCD>
              	<xsl:value-of select="Coverage[CoverageCd = 'WINDX']/Option/OptionValue"/>
            </BQBPCD>
			<BQBQCD>N</BQBQCD>
			<BQM3TX>
				<xsl:value-of select="Coverage[CoverageCd = 'VMM']/Option/OptionValue"/>
			</BQM3TX>
            <BQBRCD>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Deductible/DeductibleTypeCd"/>
            </BQBRCD>             
			<BQBSCD>N</BQBSCD>
             <BQJBTX>
               <xsl:value-of select="Coverage[CoverageCd = 'com.csc_VALUP']/Option/OptionValue"/>
            </BQJBTX>  
			<BQJCTX>N</BQJCTX>                       
			<!--Start Case 37041 -->
			<!--<BQDBNB>N</BQDBNB>-->
			<BQDBNB>				
			 	<xsl:value-of select="Construction/RoofingMaterial/RoofMaterialCd"/><!-- fd02 IT IS COMMENTED BECUSE OF RATING ERROR-->	
			</BQDBNB>		 
			<!--Ends Case 37041-->
			<BQDCNB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/Addr1"/></BQDCNB>
			 <!--63722 Issue 431 begin-->
			<BQDDNB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/City"/></BQDDNB> <!--issue # 34770-->
			<BQDENB><xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location/Addr/Addr2"/></BQDENB> <!--issue # 49441-->
			 <!--63722 Issue 431 end-->
            <BQDGNB>
               <xsl:value-of select="DwellInspectionValuation/NumFamilies"/>
            </BQDGNB>
            <BQDHNB>
               <xsl:value-of select="BldgProtection/DistanceToHydrant/NumUnits"/>
            </BQDHNB>
            <BQDINB>
               <xsl:value-of select="BldgProtection/DistanceToFireStation/NumUnits"/>
            </BQDINB>            
            <BQDLNB>
               <xsl:value-of select="Construction/YearBuilt"/>
            </BQDLNB>
            <xsl:variable name="INFGD" select="Coverage[CoverageCd = 'INFGD']/Option/OptionValue"/>
             <xsl:if test = "string-length($INFGD)">                              
             <BQAWPC>
                <xsl:value-of select="concat($INFGD, '.00000')"/>
            </BQAWPC>
            </xsl:if>                          
            <BQA6VA>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Limit/FormatCurrencyAmt/Amt"/> <!-- Coverage A -->
            </BQA6VA>
            <BQA8VA>
               <xsl:value-of select="Coverage[CoverageCd='PP']/Limit/FormatCurrencyAmt/Amt"/> <!-- Coverage C -->
            </BQA8VA>
            <BQBAVA>
               <xsl:value-of select="Coverage[CoverageCd='PL']/Limit/FormatCurrencyAmt/Amt"/> <!-- Coverage D -->
            </BQBAVA>
            <BQBCVA>
               <xsl:value-of select="Coverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt"/> <!-- Coverage F -->
            </BQBCVA>
            <!--
            <BQBEVA>
               <xsl:value-of select="Coverage[CoverageCd='PL']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBEVA>
            <BQBGVA>
               <xsl:value-of select="Coverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt"/>
            </BQBGVA>
			-->
            <BQA9NB>
               <xsl:value-of select="Coverage[CoverageCd='DWELL']/Deductible/FormatCurrencyAmt/Amt"/>
            </BQA9NB>
			<!-- Case start XXXX for FD02 34770 -->
			<!--99623 Starts--><!--sys/dst log 349 starts-->
			 <BQCZST>
				<xsl:value-of select="Coverage[CoverageCd = 'HP031']/Option/OptionValue"/>
			 </BQCZST>
			 <!--<BQCZST>Y</BQCZST>-->
			 <!--99623 Ends--><!--sys/dst log 349 ends-->
			 <!-- Case Ends XXXX for FD02 34770 -->
			<!-- Issue 39111Start -->
			<BQC0ST><xsl:value-of select="Construction/NumStories"/></BQC0ST>
			<!-- Issue 39111 End -->
			<BQC1ST>N</BQC1ST>
			 <!--Issue 65776 Starts-->
			 <BQAEPC>
				 <xsl:value-of select="Coverage[CoverageCd = 'WNDSD']/Option/OptionValue"/>
			 </BQAEPC>
			 <!--Issue 65776 Ends-->
			 <!-- Issue 39111 Start -->
			<BQC2ST>
				<xsl:value-of select="Construction/INsurerConstructionClassCd"/>
			</BQC2ST>
			 <!-- Issue 39111 End -->
			<BQIYTX>0</BQIYTX>
			<BQIZTX>N</BQIZTX>
			<BQI0TX>
				<xsl:value-of select="Coverage[CoverageCd = 'DP0008']/Option/OptionValue"/>
			</BQI0TX>
			<BQI1TX>N</BQI1TX>
            <BQAKCD></BQAKCD> <!-- 39111 Remove N -->
            <BQAMCD>DP-<xsl:value-of select="PolicyTypeCd"/></BQAMCD>
			<BQALCD>DP-<xsl:value-of select="PolicyTypeCd"/></BQALCD>
			<BQBLNB>1</BQBLNB>
			<BQJ1TX>H</BQJ1TX>
            <BQBUVA>
               <xsl:value-of select="PurchasePriceAmt/Amt"/>
            </BQBUVA>
            <BQM1TX>
               <xsl:value-of select="AreaTypeSurroundingsCd"/>
            </BQM1TX>            
         </ASBQCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->