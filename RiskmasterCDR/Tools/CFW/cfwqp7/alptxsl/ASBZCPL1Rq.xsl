<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="BuildCoverageSupplement">
<xsl:param name="Location">0</xsl:param>
<xsl:param name="SubLocation">0</xsl:param> 
<xsl:param name="Position">1</xsl:param>
<xsl:param name="InsLine">IM</xsl:param> 						<!-- Issue 59878 -->
<xsl:variable name="CovCd" select="../CoverageCd"/>	<!-- Issue 59878 -->
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>ASBZCPL1</RECORD__NAME>
</RECORD__NAME__ROW>
<ASBZCPL1__RECORD>
<BZAACD><xsl:value-of select="$LOC"/></BZAACD>
<BZABCD><xsl:value-of select="$MCO"/></BZABCD>
<BZARTX><xsl:value-of select="$SYM"/></BZARTX>
<BZASTX><xsl:value-of select="$POL"/></BZASTX>
<BZADNB><xsl:value-of select="$MOD"/></BZADNB>
<BZAGTX><xsl:value-of select="$InsLine"/></BZAGTX>
<BZBRNB><xsl:value-of select="$Location"/></BZBRNB>
<BZEGNB><xsl:value-of select="$SubLocation"/></BZEGNB>
<BZANTX>
	<xsl:choose> <!-- Issue 59878 -->
		<xsl:when test="$InsLine='IMC'"><xsl:value-of select="$InsLine"/></xsl:when>		<!-- Issue 59878 -->
		<xsl:otherwise><xsl:value-of select="$LOB"/></xsl:otherwise>
	</xsl:choose> <!-- Issue 59878 -->
</BZANTX>
<BZAENB>0</BZAENB>
<BZAOTX><xsl:value-of select="../CoverageCd"/></BZAOTX>
<BZC0NB>
	<xsl:choose>
	<xsl:when test="$Location='0' and $SubLocation='0'"> 
		<xsl:value-of select="../IterationNumber"/>
	</xsl:when>

	<xsl:otherwise><xsl:value-of select="../IterationNumber"/></xsl:otherwise><!-- Issue 59878 -->
	</xsl:choose>
</BZC0NB>
<BZPBTX>
	<xsl:choose>
		<xsl:when test="$InsLine='BOP'"><xsl:value-of select="../CoverageCd"/></xsl:when>
		<xsl:when test="$InsLine='IMC'"><xsl:value-of select="../CoverageCd"/></xsl:when>		<!-- Issue 59878 -->
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
</BZPBTX>
<!-- 34771 start -->
 <BZTYPE0ACT>
	<!--103409 starts-->
	<!--<xsl:choose>
		<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
		<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
		<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
	</xsl:choose>-->

	<xsl:call-template name="ConvertPolicyStatusCd">
		<xsl:with-param name="PolicyStatusCd">
			<xsl:value-of select="//PolicyStatusCd"/>
		</xsl:with-param>
	</xsl:call-template>
	<!--103409 Ends-->
 </BZTYPE0ACT>
<!-- 34771 end -->
<!-- Issue 59878 - Start -->
<!--59878 DST IM#3 starts-->
<!--BZDZST>
<xsl:choose>																																									
	 <xsl:when test="$InsLine='IMC'">S</xsl:when>
 </xsl:choose>	
</BZDZST-->
<!--BZD2ST>
<xsl:choose>																																									
	 <xsl:when test="$InsLine='IMC'">A</xsl:when>
 </xsl:choose>	 
</BZD2ST-->
<BZDZST></BZDZST>
<BZD0ST></BZD0ST>
<BZD2ST></BZD2ST>
 <!--59878 DST IM#3 ends--> 
<BZD3ST>
<xsl:choose>																																									
	 <xsl:when test="$InsLine='IMC'">A</xsl:when>
 </xsl:choose>	 
</BZD3ST>
<BZITEM>
  <xsl:choose>																																									
	 <xsl:when test="$InsLine='IMC'"> 																													
	 	   <xsl:choose>																																							  
	     	<xsl:when test="$CovCd='BOAT'"><xsl:value-of select="com.csc_ItemIdInfo"/></xsl:when> 		   						
			<xsl:otherwise><xsl:value-of select="ItemIdInfo"/></xsl:otherwise>														   
	   </xsl:choose>																																							   
    </xsl:when>																																										
    <xsl:otherwise>   																																					
	   <xsl:value-of select="substring-before(($Position div 4),'.') + 1"/>
    </xsl:otherwise>																																							
  </xsl:choose>																																									
</BZITEM>
<!-- Issue 59878 - End -->
<BZC6ST>P</BZC6ST>
<!-- Issue 59878 - Start -->
<xsl:choose>																																									
  <xsl:when test="$InsLine='IMC'"> 		  																											
	<BZPCTX><xsl:value-of select="ItemDefinition/ItemDesc"/></BZPCTX>		
	<BZPDTX><xsl:value-of select="ItemDefinition/com.csc_ItemDesc2"/></BZPDTX>	
	<BZR1TX><xsl:value-of select="ItemDefinition/com.csc_ItemDesc3"/></BZR1TX>	
	<BZR2TX><xsl:value-of select="ItemDefinition/com.csc_ItemDesc4"/></BZR2TX>	
  </xsl:when>																																									
  <xsl:otherwise>   		
 <!-- Issue 59878 - End -->																																				
<BZPCTX><xsl:value-of select="substring-after(com.csc_Description,'_')"/></BZPCTX>
<xsl:for-each select="following-sibling::CommlCoverageSupplement"> 
<xsl:if test="(position() mod 4 = '1') and (position()&lt;($Position + 4))">
<BZPDTX><xsl:value-of select="substring-after(com.csc_Description,'_')"/></BZPDTX>
</xsl:if>
<xsl:if test="position() mod 4 = '2' and (position()&lt;($Position + 4))">
<BZR1TX><xsl:value-of select="substring-after(com.csc_Description,'_')"/></BZR1TX>
</xsl:if>
<xsl:if test="position() mod 4 = '3' and (position()&lt;($Position + 4))">
<BZR2TX><xsl:value-of select="substring-after(com.csc_Description,'_')"/></BZR2TX>
</xsl:if>
</xsl:for-each>
  </xsl:otherwise>																																							<!-- ICH00292 - JT -->
</xsl:choose>																																									<!-- ICH00292 - JT -->    
<!-- Issue 59878 -->
<BZAGVA>
  <xsl:choose>																		
	 <xsl:when test="$InsLine='IMC'">
	   <xsl:choose>
	     	<xsl:when test="$CovCd='BOAT'"><xsl:value-of select="CostNewAmt/Amt"/></xsl:when> 							
			<xsl:otherwise><xsl:value-of select="ItemValueAmt/Amt"/></xsl:otherwise>
	   </xsl:choose>
    </xsl:when>																		
    <xsl:otherwise/>																
  </xsl:choose>																		
</BZAGVA>
<BZCZST>
 <xsl:choose>																		
	 <xsl:when test="$InsLine='IMC'">
      <xsl:choose>
	     	<xsl:when test="$CovCd='BOAT'"><xsl:value-of select="WaterUnitTypeCd"/></xsl:when> 							
			<xsl:otherwise/>
	   </xsl:choose>
    </xsl:when>																		
    <xsl:otherwise/>																
  </xsl:choose>																		
</BZCZST>
<BZACPC>
  <xsl:choose>																		
	 <xsl:when test="$InsLine='IMC'">
	   <xsl:choose>
	     	<xsl:when test="$CovCd='MISC'"><xsl:value-of select="com.csc_Rate"/></xsl:when> 							
			<xsl:otherwise/>
	   </xsl:choose>
    </xsl:when>																		
    <xsl:otherwise/>																
  </xsl:choose>																		
</BZACPC>
<BZAHVA>
  <xsl:choose>																		
	 <xsl:when test="$InsLine='IMC'">
	   <xsl:choose>
	     	<xsl:when test="$CovCd='RDOTV'"><xsl:value-of select="com.csc_StudioEquipLmt"/></xsl:when> 							
			<xsl:otherwise/>
	   </xsl:choose>
    </xsl:when>																		
    <xsl:otherwise/>																
  </xsl:choose>																		
</BZAHVA>
<BZUSVA3>
  <xsl:choose>																		
	 <xsl:when test="$InsLine='IMC'">
	   <xsl:choose>
	     	<xsl:when test="$CovCd='RDOTV'"><xsl:value-of select="com.csc_OtherPropLmt"/></xsl:when> 							
			<xsl:otherwise/>
	   </xsl:choose>
    </xsl:when>																		
    <xsl:otherwise/>																
  </xsl:choose>																		
</BZUSVA3>
<!-- Issue 59878 -->
</ASBZCPL1__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->