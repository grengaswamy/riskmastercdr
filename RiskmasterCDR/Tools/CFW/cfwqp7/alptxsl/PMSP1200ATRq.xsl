
<!-- PMSP1200AT is created only for BOP AddInterest -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Issue 59878 - Start -->
   <xsl:template name="CreateNIInfo">
      <xsl:param name="UnitNbr">0</xsl:param>    
        <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>PMSP1200AT</RECORD__NAME>
         </RECORD__NAME__ROW>
         <PMSP1200AT__RECORD>
		 	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
			<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
			<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
			<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
            <MODULE><xsl:value-of select="$MOD"/></MODULE>
			<USE0CODE><xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/></USE0CODE>
			<INSLINE>CA</INSLINE>
			<RLOCNUM>00000</RLOCNUM>
			<RSUBLOCNUM>00000</RSUBLOCNUM>
			<PRODUCT></PRODUCT>
			<UNITNUM><xsl:value-of select="$UnitNbr"/></UNITNUM>
			<USE0LOC></USE0LOC>
			<DESC0SEQ></DESC0SEQ>
			<DELETE0IND></DELETE0IND>
					 <!--BOP rating error with Additional Interest starts-->
					 <xsl:variable name="EffectiveDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
					 <xsl:variable name="ExpirationDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
					 <ITEMSEQ>00000</ITEMSEQ>
					 <COVSEQ>00000</COVSEQ>
					 <EFFDATE>
						 <xsl:value-of select="substring($EffectiveDate,1,4)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($EffectiveDate,6,2)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($EffectiveDate,9,2)"/>
					 </EFFDATE>
					 <EXPDATE>
						 <xsl:value-of select="substring($ExpirationDate,1,4)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($ExpirationDate,6,2)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($ExpirationDate,9,2)"/>
					 </EXPDATE>
					 <!--BOP rating error with Additional Interest ends-->
          </PMSP1200AT__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>

   <xsl:template name="CreateAddlInterest12AT">
      <xsl:param name="UnitNbr">0</xsl:param>    
      <xsl:param name="SubLocation">0</xsl:param>    
      <xsl:param name="Location">0</xsl:param>  
      <xsl:param name="InsLine"></xsl:param>  
      <xsl:param name="InterestType"></xsl:param>
      <xsl:param name="AISequence"></xsl:param>
  
	  
        <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>PMSP1200AT</RECORD__NAME>
         </RECORD__NAME__ROW>
         <PMSP1200AT__RECORD>
		 	<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
			<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
			<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
			<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
            		<MODULE><xsl:value-of select="$MOD"/></MODULE>
			<USE0CODE><xsl:value-of select="$InterestType"/></USE0CODE>
			<INSLINE><xsl:value-of select="$InsLine"/></INSLINE>
			<RLOCNUM><xsl:value-of select="$Location"/></RLOCNUM>
			<RSUBLOCNUM><xsl:value-of select="$SubLocation"/></RSUBLOCNUM>
			<PRODUCT></PRODUCT>
			<UNITNUM><xsl:value-of select="$UnitNbr"/></UNITNUM>
			<USE0LOC></USE0LOC>
			<DESC0SEQ><xsl:value-of select="$AISequence" /></DESC0SEQ>
			<DELETE0IND></DELETE0IND>
					 <!--BOP rating error with Additional Interest starts-->
					 <xsl:variable name="EffectiveDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
					 <xsl:variable name="ExpirationDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
					 <ITEMSEQ>00000</ITEMSEQ>
					 <COVSEQ>00000</COVSEQ>
					 <EFFDATE>
						 <xsl:value-of select="substring($EffectiveDate,1,4)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($EffectiveDate,6,2)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($EffectiveDate,9,2)"/>
					 </EFFDATE>
					 <EXPDATE>
						 <xsl:value-of select="substring($ExpirationDate,1,4)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($ExpirationDate,6,2)"/>
						 <xsl:value-of select="string('-')"/>
						 <xsl:value-of select="substring($ExpirationDate,9,2)"/>
					 </EXPDATE>
					 <!--BOP rating error with Additional Interest ends-->
          </PMSP1200AT__RECORD>
      </BUS__OBJ__RECORD>
    </xsl:template>
	<!-- Issue 59878 - End -->
	<xsl:template name="CreateBOPAddlInterest1200AT">
		<xsl:param name="Location">0</xsl:param>
		<xsl:param name="SubLocation">0</xsl:param>
		<xsl:param name="UnitNbr">0</xsl:param>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMSP1200AT</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMSP1200AT__RECORD>
				<LOCATION>
					<xsl:value-of select="$LOC"/>
				</LOCATION>
				<MASTER0CO>
					<xsl:value-of select="$MCO"/>
				</MASTER0CO>
				<SYMBOL>
					<xsl:value-of select="$SYM"/>
				</SYMBOL>
				<POLICY0NUM>
					<xsl:value-of select="$POL"/>
				</POLICY0NUM>
				<MODULE>
					<xsl:value-of select="$MOD"/>
				</MODULE>
				<USE0CODE>
					<xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
				</USE0CODE>
				<INSLINE>
					<xsl:value-of select="string('BOP')"/>
				</INSLINE>
				<RLOCNUM>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">RLOCNUM</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$Location"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</RLOCNUM>
				<RSUBLOCNUM>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">RSUBLOCNUM</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$SubLocation"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</RSUBLOCNUM>
				<PRODUCT>
					<xsl:value-of select="string('BOP')"/>
				</PRODUCT>
				<UNITNUM>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">UNITNUM</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$Location"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</UNITNUM>
				<USE0LOC>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="$Location"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</USE0LOC>
				<DESC0SEQ>
				<!-- issue # 50774 starts-->
					<!--<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[@OtherIdTypeCd='InterestTypeSeqNbr']/@OtherId"/>-->				 
					<xsl:call-template name="FormatData">
	   	     	    	<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>      
		            	<xsl:with-param name="FieldLength">5</xsl:with-param>
		            	<xsl:with-param name="Precision">0</xsl:with-param>		      
		            	<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[@OtherIdTypeCd='InterestTypeSeqNbr']/@OtherId"/>
		            	<xsl:with-param name="FieldType">N</xsl:with-param>
	             	</xsl:call-template>
					<!-- issue # 50774 ends-->
				</DESC0SEQ>
				<DELETE0IND>
					<xsl:value-of select="string(' ')"/>
				</DELETE0IND>
				<!--BOP rating error with Additional Interest starts-->
				<xsl:variable name="EffectiveDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
				<xsl:variable name="ExpirationDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
				<ITEMSEQ>00000</ITEMSEQ>
				<COVSEQ>00000</COVSEQ>
				<EFFDATE>
					<xsl:value-of select="substring($EffectiveDate,1,4)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($EffectiveDate,6,2)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($EffectiveDate,9,2)"/>
				</EFFDATE>
				<EXPDATE>
					<xsl:value-of select="substring($ExpirationDate,1,4)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($ExpirationDate,6,2)"/>
					<xsl:value-of select="string('-')"/>
					<xsl:value-of select="substring($ExpirationDate,9,2)"/>
				</EXPDATE>
				<!--BOP rating error with Additional Interest ends-->
			</PMSP1200AT__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->