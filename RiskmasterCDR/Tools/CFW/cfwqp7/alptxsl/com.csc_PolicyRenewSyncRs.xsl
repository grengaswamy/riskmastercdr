<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Point XML into ACORD XML to
return back to iSolutions 
Created by E-Service case 31594  
***********************************************************************************************-->

	<xsl:include href="CommonFuncRs.xsl"/>
	<xsl:include href="PTSignOnRs.xsl"/>
	<xsl:include href="PTErrorRs.xsl"/>
	<xsl:include href="PTProducerRs.xsl"/>
	<xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
	<xsl:include href="PTAdditionalInterestRs.xsl"/>
	<xsl:include href="PTFormsRs.xsl"/>
	<xsl:include href="PTPersPolicyRs.xsl"/>
	<xsl:include href="PTPersDriverRs.xsl"/>
	<xsl:include href="PTPersVehRs.xsl"/>
	<xsl:include href="PTAccidentViolationRs.xsl"/>
	<xsl:include href="PTDriverVehRs.xsl"/>
	<xsl:include href="PTCoverageRs.xsl"/>
	<xsl:include href="PTCommlPolicyRs.xsl"/>
	<xsl:include href="PTCommlCoverageRs.xsl"/>

	<xsl:include href="PTWorkCompRateStateRs.xsl"/>
	<xsl:include href="PTWorkCompLocInfoRs.xsl"/>
	<xsl:include href="PTHPPersPolicyRs.xsl"/>
	<xsl:include href="PTDwellRs.xsl"/>
	<xsl:include href="PTSubLocationRs.xsl"/>
	<xsl:include href="PTLocationRs.xsl"/>
	<xsl:include href="PTCommlSubLocationRs.xsl"/>
	<xsl:include href="PTCommlPropertyRs.xsl"/>
	<xsl:include href="PTModFactorsRs.xsl"/>

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<ACORD>
			<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<com.csc_PolicyRenewSyncRs>
					<RqUID/>
					<TransactionResponseDt/>
					<TransactionEffectiveDt/>
					<CurCd>USD</CurCd>
					<xsl:call-template name="PointErrorsRs"/>
					<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>
					<xsl:variable name="LOB" select="/*/PMSP0200__RECORD/LINE0BUS"/>
					<xsl:choose>
						<xsl:when test="$LOB='WCV' or $LOB='WCA'">
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
							<xsl:variable name="ValidLocation" select="/*/PMSPWC04__RECORD[DROPIND = 'N' or string-length(DROPIND)=0]"/>
							<xsl:for-each select="$ValidLocation">
								<xsl:sort select="SITE"/>
								<xsl:call-template name="WCLocationInfo"/>
							</xsl:for-each>
							<xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
							<xsl:for-each select="$ValidLocation">
								<xsl:sort select="SITE"/>
								<xsl:call-template name="CreateWCLocation"/>
							</xsl:for-each>

							<WorkCompLineBusiness>
								<LOBCd>WC</LOBCd>
								<WorkCompAssignedRisk>
									<ApplicantsStatementDesc/>
									<NumInsurersDeclined/>
								</WorkCompAssignedRisk>

								<xsl:for-each select="/com.csc_PolicyRenewSyncRs/PMSPWC07__RECORD">
									<xsl:variable name="State" select="STATE"/>
									<xsl:variable name="NumOfLocs" select="count(/com.csc_PolicyRenewSyncRs/PMSPWC04__RECORD[STATE=$State and DROPIND!='Y'])"/>
									<xsl:if test="$NumOfLocs != 0">
										<xsl:call-template name="CreateWCRateState"/>
									</xsl:if>
								</xsl:for-each>

								<xsl:apply-templates select="/*/PMSPWC02__RECORD" mode="CreateWCCommlCoverage"/>
								<QuestionAnswer>
									<QuestionCd/>
									<YesNoCd/>
									<Explanation/>
								</QuestionAnswer>
							</WorkCompLineBusiness>
							<!--<RemarkText IdRef="policy">Work Comp XML Sync</RemarkText>-->		<!--103409-->
							<RemarkText>Work Comp XML Sync</RemarkText>							<!--103409-->
							<com.csc_LastActivityInfo>
								<com.csc_LastActivityTypeCd/>
								<com.csc_LastActivityDt>
									<xsl:choose>
										<xsl:when test="/*/PMSP0200A__RECORD/EFFECTDATE">
											<xsl:call-template name="ConvertPTDateToISODate">
												<xsl:with-param name="Value" select="/*/PMSP0200A__RECORD/EFFECTDATE"/>
											</xsl:call-template>
										</xsl:when>
									</xsl:choose>
								</com.csc_LastActivityDt>
							</com.csc_LastActivityInfo>
							<PolicySummaryInfo>Insert Amendment Twin</PolicySummaryInfo>
						</xsl:when>
					</xsl:choose>
				</com.csc_PolicyRenewSyncRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->