<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateBASCLT0300">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>BASCLT0300</RECORD__NAME>
         </RECORD__NAME__ROW>
                 
         
         <BASCLT0300__RECORD>
           	<ADDRLN1><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/></ADDRLN1>
         	<ADDRLN2><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/></ADDRLN2>
         	<ADDRLN3><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr3"/></ADDRLN3>
         	<ADDRLN4><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr4"/></ADDRLN4>
           	<CITY><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/City"/></CITY>
         	<STATE><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/></STATE>
         	<ZIPCODE><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/></ZIPCODE>
         	<COUNTRY><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/CountryCd"/></COUNTRY>
         	<COUNTY><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/County"/></COUNTY>
         	<EFFDATE><xsl:value-of select="InsuredOrPrincipal/ContractTerm/EffectiveDt"/></EFFDATE>
         </BASCLT0300__RECORD>
      
        
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
