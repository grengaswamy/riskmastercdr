<!-- Created for Case 34771 : BOP Amendments -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:param name="StateProvCd"/>
    <xsl:template name="CreateModFactors">			
			<CreditOrSurcharge>
			<CreditSurchargeDt></CreditSurchargeDt>
			<CreditSurchargeCd>EXN</CreditSurchargeCd>
			<NumericValue>
			<FormatModFactor><xsl:choose><xsl:when test="normalize-space(BCBGPC)=''">0</xsl:when>
									<xsl:otherwise><xsl:value-of select="BCBGPC"/></xsl:otherwise>
									</xsl:choose>
									</FormatModFactor>
			</NumericValue>
			</CreditOrSurcharge>
			<CreditOrSurcharge>
			<CreditSurchargeDt></CreditSurchargeDt>
			<CreditSurchargeCd>IRPM</CreditSurchargeCd>
			<NumericValue>
			<FormatModFactor><xsl:choose><xsl:when test="normalize-space(BCANPC)=''">0</xsl:when>
									<xsl:otherwise><xsl:value-of select="BCANPC"/></xsl:otherwise>
									</xsl:choose></FormatModFactor>
			</NumericValue>
			</CreditOrSurcharge>
			<CreditOrSurcharge>
			<CreditSurchargeDt></CreditSurchargeDt>
			<CreditSurchargeCd>SCH</CreditSurchargeCd>
			<NumericValue>
			<FormatModFactor><xsl:choose><xsl:when test="normalize-space(BCBLPC)=''">0</xsl:when>
									<xsl:otherwise><xsl:value-of select="BCBLPC"/></xsl:otherwise>
									</xsl:choose></FormatModFactor>
			</NumericValue>
			</CreditOrSurcharge>
		</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->