<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="CreateBASORDP001">
<BUS__OBJ__RECORD>
<RECORD__NAME__ROW>
<RECORD__NAME>BASORDP001</RECORD__NAME>
</RECORD__NAME__ROW>
<BASORDP001__RECORD>
		<LOCATION><xsl:value-of select="$LOC"/></LOCATION>
		<MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
		<SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
		<POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
		<MODULE><xsl:value-of select="$MOD"/></MODULE>
		<INSLINE><xsl:value-of select="../PersPolicy/LOBCd"/></INSLINE>
		<RSKLOCNUM>00000</RSKLOCNUM>
		<SUBLOCNUM>00000</SUBLOCNUM>
		<PRODUCT><xsl:value-of select="../PersPolicy/LOBCd"/></PRODUCT>
		<UNITNUM>00000</UNITNUM>
		<ORDDRVID>
		<xsl:variable name="LicenseNbr" select="com.csc_PolicyKey"/>
		<xsl:choose>
		<xsl:when test="com.csc_Type='MVR'">0000<xsl:value-of select="../PersAutoLineBusiness/PersDriver[DriverInfo/DriversLicense/DriversLicenseNumber=
		$LicenseNbr]/ItemIdInfo/InsurerId"/>
		</xsl:when>
		<xsl:otherwise>00000</xsl:otherwise>
		</xsl:choose>
		</ORDDRVID>
		<ORDENTITY></ORDENTITY>
		<ORDENTDESC></ORDENTDESC>
		<ORDREPTYP><xsl:value-of select="com.csc_Type"/></ORDREPTYP>
		<ORDGUID><xsl:value-of select="RqUID"/></ORDGUID>
		<RECTIME><xsl:value-of select="substring(../TransactionRequestDt,12,8)"/></RECTIME>
		<RECDATE><xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="../TransactionRequestDt"/>
               </xsl:call-template>
      </RECDATE>
		<RECUSER><xsl:value-of select="../Producer/ProducerInfo/ContractNumber"/></RECUSER>
</BASORDP001__RECORD>
</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>

