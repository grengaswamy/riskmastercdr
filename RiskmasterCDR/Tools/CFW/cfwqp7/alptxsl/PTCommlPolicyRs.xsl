<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
      <!-- 29653 STart -->
      <!-- <xsl:template match="/*/ISLPPP1__RECORD" mode="WCPolicy"> -->
      <xsl:template match="/*/PMSP0200__RECORD" mode="WCPolicy">
	  <xsl:variable name="ActivityType" select="/*/PMSP0000__RECORD/TYPE0ACT"/><!--99306 -->
      <!-- 29653 STart -->
        <CommlPolicy>
            <PolicyNumber><xsl:value-of select="POLICY0NUM"/></PolicyNumber>
            <PolicyVersion><xsl:value-of select="MODULE"/></PolicyVersion>
            <CompanyProductCd><xsl:value-of select="SYMBOL"/></CompanyProductCd>
<!-- Case 39568 Start -->
<!--        <LOBCd>APV</LOBCd> -->
	    <LOBCd>WCV</LOBCd>
<!-- Case 39568 End  -->
            <NAICCd><xsl:value-of select="MASTER0CO"/></NAICCd>
            <ControllingStateProvCd><xsl:value-of select="RISK0STATE"/></ControllingStateProvCd>
            <ContractTerm> 
                <EffectiveDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="EFF0DATE"/>
                    </xsl:call-template>
                </EffectiveDt> 
                <ExpirationDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="EXP0DATE"/>
                    </xsl:call-template>
                </ExpirationDt>
                <!-- 55614 Begin -->
                <DurationPeriod>
					   <NumUnits><xsl:value-of select="INSTAL0TRM"/></NumUnits>
				    </DurationPeriod>
				    <!-- 55614 End -->
            </ContractTerm>
            <BillingMethodCd/>
            <CurrentTermAmt>
			<!-- 99306 Start -->
				<xsl:choose>
						<xsl:when test="$ActivityType ='EN'">
					 		<Amt><xsl:value-of select="/*/PMSP0400__RECORD/NEWWRTPRM"/></Amt>
						</xsl:when>
						<xsl:otherwise>
							<Amt><xsl:value-of select="TOT0AG0PRM"/></Amt>
						</xsl:otherwise>
				</xsl:choose>
			<!-- 99306 End -->
            </CurrentTermAmt>
            <OtherInsuranceWithCompanyCd/>
<!--            
            <xsl:for-each select="/*/ASBECPL1__RECORD">
                <xsl:call-template name="BuildForm"/>
            </xsl:for-each>
-->            
    <!--Issue 102807 starts-->    
	<!--OtherOrPriorPolicy>
                <PolicyCd/> 
                <PolicyNumber/>
                <LOBCd/> 
                <InsurerName/>
                <ContractTerm> 
                    <EffectiveDt/>
                    <ExpirationDt/>
                </ContractTerm>
                <PolicyAmt>
                    <Amt/>
                </PolicyAmt>
            </OtherOrPriorPolicy-->  
    <!--Issue 102807 ends--> 			
            <PaymentOption>
      <!-- 66613 Start -->
      <!--<PaymentPlanCd/>
                <MethodPaymentCd/>
                <DepositAmt>
                    <Amt/>
                </DepositAmt>
                 <com.csc_BillingPlanCd><xsl:value-of select="KIND0CODE"/></com.csc_BillingPlanCd>
             -->
             <!--97658 start-->
             <PaymentPlanCd><xsl:value-of select="PAY0CODE"/><xsl:value-of select="MODE0CODE"/></PaymentPlanCd>
             <!--97658 end-->
             <xsl:if test= "/*/ISLP3400__RECORD">               
               <DepositAmt>
						<Amt><xsl:value-of select="/*/ISLP3400__RECORD/DOWNPYMT"/></Amt>
					</DepositAmt>
					<DownPaymentPct><xsl:value-of select="/*/ISLP3400__RECORD/DOWNPERC"/></DownPaymentPct>
					<xsl:call-template name="CreateInstallments">
					   <xsl:with-param name = "i" select="1"/>
					   <xsl:with-param name = "Pos" select="11"/>
					   <xsl:with-param name = "n" select="/*/ISLP3400__RECORD/NUMINST"/> 
					</xsl:call-template>
					<NumPayments><xsl:value-of select="/*/ISLP3400__RECORD/NUMINST"/></NumPayments>
					</xsl:if>
					<com.csc_BillingPlanCd><xsl:value-of select="PAY0CODE"/><xsl:value-of select="MODE0CODE"/></com.csc_BillingPlanCd>
					<xsl:if test= "/*/ISLP3400__RECORD">
					<com.csc_AccountBalanceAmt>
					   <Amt><xsl:value-of select="/*/ISLP3400__RECORD/TOTBALANCE"/></Amt>
					</com.csc_AccountBalanceAmt>
					</xsl:if>
		<!-- 66613 End   -->			
             </PaymentOption>
            <!-- 29653 Start -->
            <!--<xsl:for-each select="/*/ISLPPP2__RECORD[LOCATION='00000']"> -->
            <xsl:for-each select="/*/PMSP1200__RECORD[LOCATION='00000']">
            <!-- 29653 End   -->
                <xsl:call-template name="CreateAdditionalInterest">
                </xsl:call-template>
            </xsl:for-each>
					<!--113617 start-->
					<AccountNumberId>
						<xsl:value-of select="/*/PMSP0200__RECORD/CUST0NO"/>
					</AccountNumberId>
					<!--<AccountNumberId/>-->
					<!--113617 end-->
					<CommlPolicySupplement>
               <AuditFrequencyCd></AuditFrequencyCd>
               <com.csc_AuditTypeCd></com.csc_AuditTypeCd>
            </CommlPolicySupplement>
            <com.csc_CompanyPolicyProcessingId><xsl:value-of select="LOCATION"/></com.csc_CompanyPolicyProcessingId>
            <com.csc_InsuranceLineIssuingCompany><xsl:value-of select="COMPANY0NO"/></com.csc_InsuranceLineIssuingCompany>
            <com.csc_PolicyTermMonths><xsl:value-of select="INSTAL0TRM"/></com.csc_PolicyTermMonths>
        </CommlPolicy>
    </xsl:template>

<!-- Case 31594 Begin -->
	<xsl:template match="/*/PMSP0200__RECORD" mode="CommlPolicy">
	<xsl:variable name="ActivityType" select="/*/PMSP0000__RECORD/TYPE0ACT"/><!--Case 34771 -->
		<CommlPolicy>
			<!-- SYS DST Log #36 starts -->
			<xsl:variable name="AccountNumberId" select="CUST0NO"/>
			<!-- SYS DST Log #36 Ends -->
			<!--Case 34771 -->
			<PolicyNumber><xsl:value-of select="POLICY0NUM"/></PolicyNumber>
            <PolicyVersion><xsl:value-of select="MODULE"/></PolicyVersion>
            <CompanyProductCd><xsl:value-of select="SYMBOL"/></CompanyProductCd>
			<!--Issue 59878 start-->
			<xsl:variable name="LOBCd" select="LINE0BUS"/>
			<!--Issue 59878 end-->
            <LOBCd><xsl:value-of select="LINE0BUS"/></LOBCd>
			<LOBSubCd><xsl:value-of select="/*/ASBACPL1__RECORD/BACZST"/></LOBSubCd>
            <NAICCd><xsl:value-of select="MASTER0CO"/></NAICCd>
            <ControllingStateProvCd>
				<xsl:call-template name="ConvertNumericStateCdToAlphaStateCd">
					<xsl:with-param name="Value"  select="RISK0STATE"/>
				</xsl:call-template>
            </ControllingStateProvCd>
            <ContractTerm> 
                <EffectiveDt>
					<xsl:call-template name="ConvertPTDateToISODate">
						<xsl:with-param name="Value"  select="EFF0DT"/>
                    </xsl:call-template>
                </EffectiveDt> 
                <ExpirationDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
						<xsl:with-param name="Value"  select="EXP0DT"/>
                    </xsl:call-template>
                </ExpirationDt>
                <!-- 55614 Begin -->
                <DurationPeriod>
					   <NumUnits><xsl:value-of select="INSTAL0TRM"/></NumUnits>
				    </DurationPeriod>
				    <!-- 55614 End -->
            </ContractTerm>
            <BillingMethodCd/>
            <CurrentTermAmt>
			<!-- 34771 start -->
                <!--<Amt><xsl:value-of select="TOT0AG0PRM"/></Amt>-->
				<Amt>
					<xsl:choose>
						<xsl:when test="$ActivityType ='EN'">
					 		<xsl:value-of select="/*/PMSP0400__RECORD/NEWWRTPRM"/> 
						</xsl:when>
						<xsl:otherwise>
					 			<!-- Issue 59878 - Start -->
							<xsl:choose>
								<xsl:when test="LINE0BUS = 'CPP'">
									<xsl:value-of select="//ASBACPL1__RECORD/BAA3VA"/>
								</xsl:when>
								<xsl:otherwise>
								<!-- Issue 59878 - End -->
									<xsl:value-of select="TOT0AG0PRM"/>  
								<!-- Issue 59878 - Start -->
								</xsl:otherwise>
							</xsl:choose>
							<!-- Issue 59878 - End --> 
						</xsl:otherwise>
					</xsl:choose>
				</Amt>
			<!-- 34771 end -->
            </CurrentTermAmt>
          <!-- 55614 begin-->  
          <!-- <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='*AL']">-->
          <!-- ICH00284B -->
          <!--      <xsl:call-template name="BuildForm"/>
            </xsl:for-each> -->
			<!-- 50768 start -->
			<!-- <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CA']">
                <xsl:call-template name="BuildForm"/>
            </xsl:for-each> -->
			<!-- 50768 end -->
			<!-- 55614 end -->
			<!-- 50768 start -->
			<GroupId><xsl:value-of select="/*/BASP0200E__RECORD/GROUPCODE"/></GroupId>
			<!-- 50768 end -->
            <OtherInsuranceWithCompanyCd/>
            <!-- 55614 begin-->  
            <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='*AL']"><!-- ICH00284B -->
                <xsl:call-template name="BuildForm"/>
            </xsl:for-each>
            <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CA']">
                <xsl:call-template name="BuildForm"/>
            </xsl:for-each>
            <!-- 55614 end -->
    <!--Issue 102807 starts-->         
	<!--OtherOrPriorPolicy>
                <PolicyCd/> 
                <PolicyNumber/>
                <LOBCd/> 
                <InsurerName/>
                <ContractTerm> 
					<EffectiveDt/>
                    <ExpirationDt/>
                </ContractTerm>
                <PolicyAmt>
                    <Amt/>
                </PolicyAmt>
            </OtherOrPriorPolicy-->
    <!--Issue 102807 ends--> 
            <PaymentOption>
 <!-- 66613 Start -->
      <!--<PaymentPlanCd/>
                <MethodPaymentCd/>
                <DepositAmt>
                    <Amt/>
                </DepositAmt>
                 <com.csc_BillingPlanCd><xsl:value-of select="KIND0CODE"/></com.csc_BillingPlanCd>
             -->
             <!--97658 start-->
             <PaymentPlanCd><xsl:value-of select="PAY0CODE"/><xsl:value-of select="MODE0CODE"/></PaymentPlanCd>
             <!--97658 end-->
             <xsl:if test= "/*/ISLP3400__RECORD">               
               <DepositAmt>
						<Amt><xsl:value-of select="/*/ISLP3400__RECORD/DOWNPYMT"/></Amt>
					</DepositAmt>
					<DownPaymentPct><xsl:value-of select="/*/ISLP3400__RECORD/DOWNPERC"/></DownPaymentPct>
					<xsl:call-template name="CreateInstallments">
					   <xsl:with-param name = "i" select="1"/>
					   <xsl:with-param name = "Pos" select="11"/>
					   <xsl:with-param name = "n" select="/*/ISLP3400__RECORD/NUMINST"/> 
					</xsl:call-template>
					<NumPayments><xsl:value-of select="/*/ISLP3400__RECORD/NUMINST"/></NumPayments>
					</xsl:if>
					<!--<com.csc_BillingPlanCd><xsl:value-of select="PAY0CODE"/><xsl:value-of select="MODE0CODE"/></com.csc_BillingPlanCd>   55614 -->
					<xsl:if test= "/*/ISLP3400__RECORD">
					<com.csc_AccountBalanceAmt>
					   <Amt><xsl:value-of select="/*/ISLP3400__RECORD/TOTBALANCE"/></Amt>
					</com.csc_AccountBalanceAmt>
					</xsl:if>
		<!-- 66613 End   -->			
            </PaymentOption>
            <xsl:for-each select="/*/PMSP1200__RECORD[USE0LOC='00000']">
                <xsl:call-template name="CreateWorkCompAdditionalInterest">
                </xsl:call-template>
            </xsl:for-each>
			<!-- SYS DST Log #36 starts -->
			<AccountNumberId>
				<xsl:value-of select="$AccountNumberId"/>
			</AccountNumberId>
			<!--<AccountNumberId/>-->
			<!-- SYS DST Log #36 ends -->            
			<CommlPolicySupplement>
               <ReportingPeriodCd />
               <OperationsDesc><xsl:value-of select="/*/ASBACPL1__RECORD/BAH6TX"/></OperationsDesc>
               <!-- 34771 : start -->
               <xsl:if test="/*/ASBBCPL1__RECORD/BBIYTX != ''">
			   			<xsl:if test="normalize-space($LOBCd) !='CPP'"> <!-- Issue 59878 added condition -->
			         		<AuditFrequencyCd><xsl:value-of select="/*/ASBBCPL1__RECORD/BBIYTX"/></AuditFrequencyCd>
						</xsl:if> <!-- Issue 59878 added condition -->
               </xsl:if>
               <xsl:if test="/*/ASBBCPL1__RECORD/BBIZTX != ''">
			   <!--Issue 59878 start-->			   
			   <xsl:choose>
					<xsl:when test="normalize-space($LOBCd) ='CPP'">
				 		<com.csc_AuditTypeCd>
							<xsl:value-of select="/*/ASBACPL1__RECORD/BAAKCD"/>
						</com.csc_AuditTypeCd>
					</xsl:when>
					<xsl:otherwise>
						<com.csc_AuditTypeCd><xsl:value-of select="/*/ASBBCPL1__RECORD/BBIZTX"/></com.csc_AuditTypeCd>
					</xsl:otherwise>
				</xsl:choose>
				<!--Issue 59878 end-->
				<!--<com.csc_AuditTypeCd><xsl:value-of select="/*/ASBBCPL1__RECORD/BBIZTX"/></com.csc_AuditTypeCd>--> <!--Issue 59878 - Commented this and moved to <xsl:otherwise>-->
               </xsl:if>
               <!--<PolicyTypeCd><xsl:value-of select="/*/ASBCPL1__RECORD/BAAKCD"/></PolicyTypeCd>-->
			   <PolicyTypeCd><xsl:value-of select="/*/ASBBCPL1__RECORD/BBAKCD"/></PolicyTypeCd>
			   <!-- 34771 : end -->
			   <xsl:if test="/*/PMSP0200__RECORD/AUDIT0CODE != ''">
				<AuditFrequencyCd>
					<xsl:value-of select="/*/PMSP0200__RECORD/AUDIT0CODE"/>
				</AuditFrequencyCd>
			    </xsl:if>
			    <!-- 55614 begin-->
		       <CreditOrSurcharge>
			       <CreditSurchargeCd>APMP</CreditSurchargeCd>
			       <NumericValue>
			          <FormatCurrencyAmt>
				          <Amt><xsl:value-of select="/*/PMSPWC02__RECORD/MINPREM"/></Amt>
			          </FormatCurrencyAmt>
			       </NumericValue>
		       </CreditOrSurcharge>
		       <CreditOrSurcharge>
			       <CreditSurchargeCd>DIVID</CreditSurchargeCd>
			       <SecondaryCd><xsl:value-of select="/*/BPOP0200E__RECORD/DIVCODE"/></SecondaryCd>
		       </CreditOrSurcharge>
		       <!-- 55614 end-->
			    <xsl:if test="/*/PMSPWC04__RECORD[AUDTYPE!='']/AUDTYPE != ''">
				<com.csc_AuditTypeCd>
					<xsl:value-of select="/*/PMSPWC04__RECORD[AUDTYPE!='']/AUDTYPE"/>
				</com.csc_AuditTypeCd>
				</xsl:if>
            </CommlPolicySupplement>
			<!--SYS/DST WC Endorsement Issue--><!--added WCA for issue 111510 sys/dst log 344-->
			<xsl:if test="LINE0BUS = 'WCV' or LINE0BUS = 'WC' or LINE0BUS = 'BO' or LINE0BUS = 'BOP' or LINE0BUS = 'WCA'">
            <com.csc_CompanyPolicyProcessingId><xsl:value-of select="LOCATION"/></com.csc_CompanyPolicyProcessingId>
            <com.csc_InsuranceLineIssuingCompany><xsl:value-of select="COMPANY0NO"/></com.csc_InsuranceLineIssuingCompany>
            <com.csc_PolicyTermMonths><xsl:value-of select="INSTAL0TRM"/></com.csc_PolicyTermMonths>
            </xsl:if>
			<!--SYS/DST WC Endorsement Issue-->
            <!-- 55614 Begin -->
            <!-- <com.csc_NextLocationAssignedId>
            	<xsl:for-each select="/*/PMSPWC04__RECORD">
            		<xsl:sort select="SITE"/>
            		<xsl:variable name="Loc" select="SITE"/>
            		<xsl:if test="position() = count(//PMSPWC04__RECORD)">
						<xsl:value-of select="SITE + 1"/>
					</xsl:if>
				</xsl:for-each>
			</com.csc_NextLocationAssignedId>-->
			<!-- 34771 start -->
			<!-- <com.csc_ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_NextAvailableLocationNbr</OtherIdTypeCd>
					<OtherId><xsl:value-of select="count(//ASBUCPL1__RECORD)+1"/></OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>-->
			<!-- 34771 end -->
			<!-- issue 104053 Starts -->
			<xsl:for-each select="/*/PMSPWC03__RECORD[STATE='00']">
				<xsl:call-template name="CreateOptionalForms"/>
			</xsl:for-each>
			<!-- issue 104053 Ends -->
			<com.csc_ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_NextAvailableLocationNbr</OtherIdTypeCd>
					<OtherId>
					 <xsl:choose>
                    <xsl:when test="/*/PMSPWC04__RECORD">
                       <xsl:for-each select="/*/PMSPWC04__RECORD">
            		     <xsl:sort select="SITE"/>
            		     <xsl:variable name="Loc" select="SITE"/>
            		     <xsl:if test="position() = count(//PMSPWC04__RECORD)">
						     <xsl:value-of select="SITE + 1"/>
					        </xsl:if>
				           </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise> 
					        <xsl:value-of select="count(//ASBUCPL1__RECORD)+1"/>
                    </xsl:otherwise>      
					</xsl:choose>
					</OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>
			<!-- 55614 End -->
        </CommlPolicy>
    </xsl:template>
<!-- Case 31594 End -->
       <!-- 66613 Start -->    
    <xsl:template name="CreateInstallments">
					   <xsl:param name = "i"/>
					   <xsl:param name = "Pos"/>
					   <xsl:param name = "n"/>
					<InstallmentInfo>
					   <InstallmentAmt>
						   <Amt><xsl:value-of select="/*/ISLP3400__RECORD/*[$Pos +1]"/></Amt>
					   </InstallmentAmt>
					   <InstallmentDueDt>
					   <xsl:variable name = "Dt" select="/*/ISLP3400__RECORD/*[$Pos]"/>
					      <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="$Dt"/>
                     </xsl:call-template>
 					   </InstallmentDueDt>
					   <InstallmentNumber><xsl:value-of select="$i"/></InstallmentNumber>
					</InstallmentInfo>
					<xsl:if test = "$i &lt; $n">
					<xsl:call-template name="CreateInstallments">
					   <xsl:with-param name = "i" select="$i+1"/>
					   <xsl:with-param name = "Pos" select="11+($i*2)"/>
					   <xsl:with-param name = "n" select="/*/ISLP3400__RECORD/NUMINST"/> 
					</xsl:call-template>
					</xsl:if>
	</xsl:template>
 <!-- 66613 End   -->   
</xsl:stylesheet>
