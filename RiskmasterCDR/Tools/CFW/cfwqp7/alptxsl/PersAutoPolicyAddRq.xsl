<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Point XML before sending to the the Point system   
E-Service case 19453 
***********************************************************************************************-->

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Template Templates -->
    <xsl:include href="PMSP0000Rq.xsl"/>    <!-- Activity Record                   -->
    <xsl:include href="PMSP0200Rq.xsl"/>    <!-- Basic Contract Record             -->
    <xsl:include href="PMSP1200Rq.xsl"/>    <!-- Additional Interest Record        -->
    <xsl:include href="ASBJCPL0Rq.xsl"/> 	<!-- Personal Auto Unit Record     -->
    <xsl:include href="ASBKCPL0Rq.xsl"/> 	<!-- Personal Auto Coverage Record -->
    <xsl:include href="ASBLCPL0Rq.xsl"/>    <!-- Policy Auto Driver Record -->
    <xsl:include href="ASBMCPL0Rq.xsl"/>    <!-- Policy Auto Driver Violations -->
    <xsl:include href="ASBACPL1Rq.xsl"/>  <!-- Policy Level Record  issue # 73609 -->
    <!-- Start Case 27708 - Add client files Jeff Simmons --> 
    <xsl:include href="BASCLT0100Rq.xsl"/>  <!-- Client Name                                    -->
    <xsl:include href="BASCLT0300Rq.xsl"/>  
    <xsl:include href="BASCLT1400Rq.xsl"/>  
    <xsl:include href="BASCLT1500Rq.xsl"/>  
    <!-- End   Case 27708 - Add client files Jeff Simmons --> 
    <!-- Case 39568 Start -->
    <xsl:include href="PTInfoRq.xsl"/>
    <!-- Case 39568 End   -->
    <xsl:include href="BASP0200ERq.xsl"/> <!-- 57418 start -->
     <xsl:include href="BASORDP001Rq.xsl"/>          <!-- 87428-->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/PolicyNumber"/>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/LOBCd"/>    
     <!-- 33385 start -->
  <xsl:variable name="TYPEACT">NB</xsl:variable> 
  <!-- 33385 end -->
    <xsl:template match="/">

        <xsl:element name="PersAutoPolicyAddRq">
             <!-- 39568 Start -->
             <xsl:apply-templates select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq" mode="CreatePTInfo"/> 
             <!-- 39568 End   -->
	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->             
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy" mode="PMSP0000"/>
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq" mode="PMSP0200"/>
            <!--issue # 73609  starts-->
 <!-- Build Policy Level Record -->
            <xsl:apply-templates select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq" mode="ASBACPL1"/>
         <!--issue # 73609  Ends-->   

<!-- Build Policy Level Additional Interest Records - PMSP1200 -->
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/AdditionalInterest">
       	         <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                      <xsl:call-template name="CreateAddlInterest"/>
                </xsl:if>    
            </xsl:for-each>
<!-- Build Driver Records - ASBLCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersAutoLineBusiness/PersDriver">
                <xsl:call-template name="CreateDriverRec"/>
            </xsl:for-each>                
<!-- Build Violation Records - ASBMCPL0-->
			<xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/AccidentViolation">
	     		 <xsl:if test="string-length(AccidentViolationCd) > 0">	<!--Issue 39428--><!-- Case 33385 -->
               		 <xsl:call-template name="CreateViolationRec"/>
               	 </xsl:if>																<!-- Case 33385 -->
            </xsl:for-each>                            
<!-- Build Vehicle Unit Records - ASBJCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersAutoLineBusiness/PersVeh">
                <xsl:call-template name="CreateUnitRec"/>
<!-- Build Vehicle Level Additional Interest Records - PMSP1200-->
                <xsl:for-each select="AdditionalInterest">
                       <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                         <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>     <!--27083-->
	                     </xsl:call-template>  
                       </xsl:if>    
                </xsl:for-each>
           </xsl:for-each>                                              
<!-- Build Vehicle Coverage Records - ASBKCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersAutoLineBusiness/PersVeh/Coverage">
                <xsl:call-template name="CreateCoverageRec"/>
            </xsl:for-each> 
            
            <!-- Start Case 27708 - Add client files Jeff Simmons --> 
            <xsl:for-each select="/ACORD/InsuranceSvcRq/PersAutoPolicyAddRq"> 
                <xsl:call-template name="CreateBASCLT0100"></xsl:call-template>   
                <xsl:call-template name="CreateBASCLT0300"></xsl:call-template>    
                <xsl:call-template name="CreateBASCLT1400"></xsl:call-template>   
            </xsl:for-each>   
            
            
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersPolicy/AdditionalInterest">
       	         <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                      <xsl:call-template name="CreateBASCLT1500"/>
                 </xsl:if>    
            </xsl:for-each>
            
            
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/PersAutoLineBusiness/PersVeh">
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                      <xsl:call-template name="CreateBASCLT1500">                            
       	                 <xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>   
       	              </xsl:call-template>                                                   
                   </xsl:if>    
                </xsl:for-each>
            </xsl:for-each>                                   
            <!-- End  Case 27708 - Add client files Jeff Simmons -->   
             
            <!-- 87428 begin--> 
            <xsl:for-each select="ACORD/InsuranceSvcRq/PersAutoPolicyAddRq/com.csc_TPOInfo">
               	 	<xsl:call-template name="CreateBASORDP001"/>
            </xsl:for-each>
            <!-- 87428 end-->        
                
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>