<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 14734 
***********************************************************************************************
-->
  <xsl:include href="CommonFuncRs.xsl"/>
  <xsl:include href="PTSignOnRs.xsl"/>
  <xsl:include href="PTErrorRs.xsl"/>
  <xsl:include href="PTProducerRs.xsl"/>
  <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
  <xsl:include href="PTCommlPolicyRs.xsl"/>
  <xsl:include href="PTWCLocationRs.xsl"/>
  <xsl:include href="PTAdditionalInterestRs.xsl"/>
  <xsl:include href="PTWorkCompRateStateRs.xsl"/>
  <xsl:include href="PTWorkCompLocInfoRs.xsl"/>
  <xsl:include href="PTCommlCoverageRs.xsl"/>
  <xsl:include href="PTFormsRs.xsl"/>
  <xsl:include href="PTWCIndividualsRs.xsl"/>		<!--103409-->

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="BuildSignOn"/>
      <InsuranceSvcRs>
        <RqUID/>
        <WorkCompPolicyQuoteInqRs>
          <RqUID/>
          <TransactionResponseDt/>
          <TransactionEffectiveDt/>
          <CurCd>USD</CurCd>
          <xsl:call-template name="PointErrorsRs"/>
 	  	  <!-- 29653 Start -->
          <!--  <xsl:apply-templates select="//ISLPPP1__RECORD" mode="WC"/>
          <xsl:apply-templates select="//ISLPPP1__RECORD" mode="WClnsured"/>
          <xsl:apply-templates select="//ISLPPP1__RECORD" mode="WCPolicy"/> -->
	  	  <xsl:apply-templates select="//PMSP0200__RECORD" mode="WC"/>
          <xsl:apply-templates select="//PMSP0200__RECORD" mode="WClnsured"/>
          <xsl:apply-templates select="//PMSP0200__RECORD" mode="WCPolicy"/>
	  	  <!-- 29653 End   -->
          <!-- 29653 Start -->
          <!-- <xsl:for-each select="//ISLPPP3__RECORD"> -->
          <xsl:for-each select="//PMSPWC04__RECORD">
          <!-- 29653 End   -->
            <xsl:call-template name="CreateLocation"/>
          </xsl:for-each>
          <WorkCompLineBusiness>
            <LOBCd>WC</LOBCd>
            <WorkCompAssignedRisk>
              <ApplicantsStatementDesc/>
              <NumInsurersDeclined/>
            </WorkCompAssignedRisk>
	        <!-- 29653 Start -->           
			<!--103409 Starts-->
			<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = 'D' or DEEMSTAT = 'N']">
				<xsl:sort select="number(DESC0SEQ)"/>
            	<xsl:call-template name="CreateWorkCompIndividuals"/>
          	</xsl:for-each>
			<!--103409 Ends-->
	        <!-- <xsl:for-each select="/WorkCompPolicyQuoteInqRs/ISLPPP4__RECORD"> -->
	        <xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMSPSA15__RECORD">
	        <!--<xsl:sort select="STATE"/> -->
              <xsl:sort select="UNITNO"/>
              <!-- 29653 End   -->
              <xsl:variable name="State" select="STATE"/>
              <!-- 29653 Start -->
              <!-- <xsl:if test="not(preceding-sibling::ISLPPP4__RECORD[STATE=$State])"> -->
              <xsl:if test="not(preceding-sibling::PMSPSA15__RECORD[STATE=$State])">
              <!-- 29653 End   -->
                <WorkCompRateState>
                  <StateProvCd>
                    <xsl:value-of select="$State"/>
                  </StateProvCd>
                  <AnniversaryRatingDt/>
                  <NCCIIDNumber/>
                  <!-- 29653 Start -->
                  <!--  <xsl:for-each select="//ISLPPP5__RECORD[STATE=$State]"> -->
                  <xsl:for-each select="//PMSPWC25__RECORD[STATE=$State]">
                  <!-- 29653 End  -->
                    <CreditOrSurcharge>
                      <CreditSurchargeCd>
                        <!-- 29653 Start -->
                        <!--<xsl:value-of select="MODIDENT"/> -->
                        <xsl:value-of select="MODTYPE"/>
                        <!-- 29653 End   -->
                      </CreditSurchargeCd>
                      <NumericValue>
                        <FormatCurrencyAmt>
                          <Amt>
                            <xsl:value-of select="FLATAMT"/>
                          </Amt>
                        </FormatCurrencyAmt>
                      </NumericValue>
                    </CreditOrSurcharge>
                  </xsl:for-each>
		          <!-- 29653 Start -->
		          <!--- <xsl:for-each select="/WorkCompPolicyQuoteInqRs/ISLPPP4__RECORD[STATE=$State]"> -->
                  <!--  <xsl:call-template name="CreateWorkCompLocInfo"/> -->
                  <!-- </xsl:for-each> -->
                  <!-- 29653 End   -->
                                   
                  <!-- 29653 Start -->
                  
				                    
                  <xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMSPSA15__RECORD[STATE=$State]">
              	     <xsl:variable name="UnitNo" select="UNITNO"/>
                     <xsl:variable name="CovSeq" select="COVSEQ"/>
                     <xsl:variable name="TransSeq" select="TRANSSEQ"/>
					 <xsl:variable name="Classnum" select="CLASSNUM"/>
 	     		     <xsl:variable name="Bureau" select="/WorkCompPolicyQuoteInqRs/PMSPSA35__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/BUREAU34"/>
			         <xsl:variable name="Spec50" select="/WorkCompPolicyQuoteInqRs/PMSPSA36__RECORD[UNITNO=$UnitNo and COVSEQ=$CovSeq and TRANSSEQ=$TransSeq]/SPEC50"/>  
		                   
			  	     <xsl:choose>
                         <xsl:when  test="$Classnum='XXXA'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				            </xsl:call-template>	
			             </xsl:when>
			
			             <xsl:when  test="$Classnum='XXXM'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				            </xsl:call-template>	
			    		</xsl:when>

			            <xsl:when  test="$Classnum='XXXS'">
			                <xsl:variable name="Rate">000.000</xsl:variable> 
                            <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:when  test="$Classnum='XXXP'">
			               <xsl:variable name="Rate">000.000</xsl:variable>  
                           <xsl:call-template name="CreateWorkCompLocInfo">
				                <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>	
						</xsl:when>
			
			            <xsl:otherwise>
				           <xsl:variable name="Rate"> 
						   <xsl:call-template name="CreateWCRate"> 
		                       <xsl:with-param name="Bureau" select="$Bureau"/>
		                       <xsl:with-param name="Spec50" select="$Spec50"/>
        		           </xsl:call-template>
                           </xsl:variable> 
						                           
				           <xsl:call-template name="CreateWorkCompLocInfo">
				              <xsl:with-param name="Rate" select="$Rate"/>
				           </xsl:call-template>					   
					</xsl:otherwise>
			</xsl:choose>
						   
						   
						   
				  </xsl:for-each>
                  <!-- 29653 End -->
                  
                  
                </WorkCompRateState>
              </xsl:if>
            </xsl:for-each>

            <!-- 29653 Start -->
	        <!--<xsl:apply-templates select="//ISLPPP1__RECORD" mode="WCCoverage"/> -->
            <xsl:apply-templates select="//PMSPWC02__RECORD" mode="WCCoverage"/>
 	        <!-- 29653 End   -->
            <QuestionAnswer>
              <QuestionCd/>
              <YesNoCd/>
              <Explanation/>
            </QuestionAnswer>
          </WorkCompLineBusiness>
			<!--<RemarkText IdRef="policy">Work Comp XML Quote</RemarkText>-->	<!--103409-->
			<RemarkText>Work Comp XML Quote</RemarkText>			<!--103409-->
        </WorkCompPolicyQuoteInqRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\Documents and Settings\snema\Desktop\test.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->