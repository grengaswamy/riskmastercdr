<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
  <xsl:template name="CreatePMSPWC04">
    <xsl:variable name="TableName">PMSPWC04</xsl:variable>
	<!-- 62413 start -->
	<!--<xsl:variable name="LocationId" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location/@id"/>-->
	<!--<xsl:variable name="LocationId" select="/ACORD/InsuranceSvcRq/*/Location/@id"/>--> <!--73232 -->
	<xsl:variable name="LocationId" select="@id"/> <!--73232 -->
	<!-- 62413 end -->
    <xsl:variable name="LocationNbr" select="@LocationRef"/>
    <xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
    <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)"/>
	<!-- Issue 62413 Begin -->
	<xsl:variable name="FEIN">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationId]/TaxCodeInfo">
			<xsl:if test="TaxTypeCd='FEIN'">
				<xsl:value-of select="TaxCd"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="UNEMPNO">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationId]/TaxCodeInfo">
			<xsl:if test="TaxTypeCd='UNEMPLOYMENT NUMBER'">
				<xsl:value-of select="TaxCd"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<!-- Issue 62413 End -->
      <BUS__OBJ__RECORD>
      	<RECORD__NAME__ROW>
        	<RECORD__NAME>PMSPWC04</RECORD__NAME>
      	</RECORD__NAME__ROW>
      	<PMSPWC04__RECORD>
      		<SYMBOL>
      			<xsl:value-of select="$SYM"/>
      		</SYMBOL>
      	    <POLICYNO>
          		<xsl:value-of select="$POL"/>
        	</POLICYNO>
        	<MODULE>
          		<xsl:value-of select="$MOD"/>
        	</MODULE>
        	<MASTERCO>
          		<xsl:value-of select="$MCO"/>
        	</MASTERCO>
        	<LOCATION>
          		<xsl:value-of select="$LOC"/>
        	</LOCATION>
        	<STATE>
          		<xsl:value-of select="Addr/StateProvCd"/>
        	</STATE>
      	 	<SITE>
          		<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">SITE</xsl:with-param>
            		<xsl:with-param name="FieldLength">5</xsl:with-param>
            		<xsl:with-param name="Value" select="$LocationNumber"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
        	</SITE>	
        	<NAME>
          		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>							
        	</NAME>
        	<ADDRESS1>
                        <!-- Issue 50396 xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/ -->
           	</ADDRESS1>
			<ADDRESS2>
				
          		<xsl:value-of select="Addr/Addr1"/>
			</ADDRESS2>
			<CITYST>
			 <!-- 29653 Start --> 
			 <!--<xsl:value-of select="Addr/City"/> -->
			  <xsl:value-of select="substring(concat(Addr/City, '                                                                                                    '), 1, 28)"/>
              <xsl:value-of select="Addr/StateProvCd"/> 
             <!-- 29653 End --> 
      	     </CITYST>
			<ZIP>
          		<xsl:value-of select="Addr/PostalCode"/>
        	</ZIP>
			<CONTACT></CONTACT>
			<PHONE></PHONE>
			<TAXLOC></TAXLOC>
			<AUDITOR></AUDITOR>
        	<AUDTYPE>
        		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd" /> 
        	</AUDTYPE>
        	<NXTAUDEXP></NXTAUDEXP>
        	<AUDBASIS>P</AUDBASIS>
        	<ENDEFFDATE></ENDEFFDATE>
         	<PDTABLE>
          		<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
            		<xsl:with-param name="FieldLength">2</xsl:with-param>
            		<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
        	</PDTABLE>
        	<PDSLIND>
          		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/>
        	</PDSLIND>
		
			<PDOVRPCT>000000</PDOVRPCT> <!--Issue #57418-->
			<PDEFFDATE></PDEFFDATE>
			<FEIN><xsl:value-of select="$FEIN"/></FEIN> <!-- 62413 -->
			<LEGALID></LEGALID>
			<SIC></SIC>

			<NUMEMPL>
 				<xsl:call-template name="FormatData">
  					<xsl:with-param name="FieldName">NUMEMPL</xsl:with-param> 
  					<xsl:with-param name="FieldLength">6</xsl:with-param> 
					<!--73232 start-->
 					<!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/WorkCompLocInfo/NumEmployees" /> -->
					<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo[@LocationRef=$LocationId]/NumEmployees" /> 
					<!--73232 end-->
					<xsl:with-param name="FieldType">N</xsl:with-param> 
  					</xsl:call-template>
			</NUMEMPL>
			<UNEMPNO><xsl:value-of select="$UNEMPNO"/></UNEMPNO>
			<DROPIND></DROPIND>
      </PMSPWC04__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>

  <!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
  
<!-- Case 31594 begin - Modified the template to build one WC04 segement for each location -->
<xsl:template name="CreatePMSPWC04Defaults">
       <xsl:variable name="TableName">PMSPWC04</xsl:variable>
	<xsl:variable name="LocationNbr" select="@LocationRef"/>
	<xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
	<!--<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode"/>-->	<!--103409-->
	<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd"/>			<!--103409-->
      <!-- 29653 Start -->
      <xsl:variable name="LocationNumber" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
      <!-- 29653 End   -->
	<!-- Issue 62413 Begin -->
	<xsl:variable name="LocationId" select="/ACORD/InsuranceSvcRq/*/Location/@id"/>
	<!-- Issue 62413 End -->
    <!-- Issue 62413 Begin -->
	<xsl:variable name="FEIN">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/TaxCodeInfo">
			<xsl:if test="TaxTypeCd='FEIN'">
				<xsl:value-of select="TaxCd"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="UNEMPNO">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/TaxCodeInfo">
			<xsl:if test="TaxTypeCd='UNEMPLOYMENT NUMBER'">
				<xsl:value-of select="TaxCd"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<!-- Issue 62413 End -->
	<xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
    <BUS__OBJ__RECORD>
      	<RECORD__NAME__ROW>
        	<RECORD__NAME>PMSPWC04</RECORD__NAME>
      	</RECORD__NAME__ROW>
        <PMSPWC04__RECORD>
      		<SYMBOL>
      			<xsl:value-of select="$SYM"/>
      		</SYMBOL>
      	    <POLICYNO>
          		<xsl:value-of select="$POL"/>
        	</POLICYNO>
        	<MODULE>
          		<xsl:value-of select="$MOD"/>
        	</MODULE>
        	<MASTERCO>
          		<xsl:value-of select="$MCO"/>
        	</MASTERCO>
        	<LOCATION>
          		<xsl:value-of select="$LOC"/>
        	</LOCATION>
        	<STATE>
                   <xsl:value-of select="../StateProvCd"/>                 
           	</STATE>
      	 	<SITE>
          	  	<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">SITE</xsl:with-param>
            		<xsl:with-param name="FieldLength">5</xsl:with-param>
            		<xsl:with-param name="Value" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
        	</SITE>	
        	<NAME>
            <!-- 29653 Start -->
            <!--     		<xsl:variable name="LocationNumber" select="substring(/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef,2)"/> -->
            <!--    		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal -->
            <!--    		 					[substring(@id,2)=$LocationNumber] -->
            <!--    							/GeneralPartyInfo/NameInfo/com.csc_LongName"/>	 -->			
   		<!--/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>								-->
       <!--     <xsl:choose>
		<xsl:when test="$TYPEACT='EN'"> -->
		     <!--<xsl:variable name="LocationNumb" select="substring(/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef,2)"/> -->
        <!--		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal
        		 					[substring(@id,2)=$LocationNumber]
        							/GeneralPartyInfo/NameInfo/com.csc_LongName"/>	 -->							
        							<!--/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>								-->
		<!--</xsl:when>
		<xsl:otherwise> -->
		         <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/> 							
		        <!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/> -->							
		  	<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/> -->
		<!--</xsl:otherwise> -->
		<!--</xsl:choose> -->
            <!-- 29653 End -->
           	</NAME>
           	<xsl:choose>
				<xsl:when test="$LocationPath/Addr1!=''">
        			<ADDRESS1>
        				<xsl:variable name="NameRef" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef"/>
        				<!-- Issue 50396 xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[@id=$NameRef]/GeneralPartyInfo/
        										NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/ -->
        			</ADDRESS1>
        			<ADDRESS2><xsl:value-of select="$LocationPath/Addr1"/></ADDRESS2> 
        			<CITYST>
        				<xsl:value-of select="substring(concat($LocationPath/City, '                                                                                                    '), 1, 28)"/>
						<xsl:value-of select="$LocationPath/StateProvCd"/><!--112776-->
           			</CITYST>
        			<ZIP><xsl:value-of select="$LocationPath/PostalCode"/></ZIP>
				</xsl:when>
				<xsl:otherwise>
					<ADDRESS1>Generated Optional 1</ADDRESS1>
        			<ADDRESS2>Generated Address 1</ADDRESS2>
        			<CITYST>
          				<xsl:value-of select="substring(concat('Generated City', '                                                                                                    '), 1, 28)"/>
          			</CITYST>
        			<ZIP>12345</ZIP>
				</xsl:otherwise>
			</xsl:choose>
     		<CONTACT></CONTACT>
			<PHONE></PHONE>
			<TAXLOC></TAXLOC>
			<AUDITOR></AUDITOR>
        	<AUDTYPE>
        		<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd" /> 
  			</AUDTYPE>
        	<NXTAUDEXP></NXTAUDEXP>
        	<AUDBASIS>P</AUDBASIS>
        	<ENDEFFDATE></ENDEFFDATE>
          	<PDTABLE>
          		<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
            		<xsl:with-param name="FieldLength">2</xsl:with-param>
            		<xsl:with-param name="Value" select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
        	</PDTABLE>
        	<PDSLIND>
          		<xsl:value-of select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/>
        	</PDSLIND>
			<PDOVRPCT></PDOVRPCT>
			<PDEFFDATE></PDEFFDATE>
			<FEIN><xsl:value-of select="$FEIN"/></FEIN> <!-- 62413 -->
			<LEGALID></LEGALID>
			<SIC></SIC>
			<NUMEMPL> 
				<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">NUMEMPL</xsl:with-param>
            		<xsl:with-param name="FieldLength">6</xsl:with-param>
            		<xsl:with-param name="Value" select="NumEmployees"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template></NUMEMPL>
			<UNEMPNO><xsl:value-of select="$UNEMPNO"/></UNEMPNO><!-- 62413 -->
			<DROPIND></DROPIND>
      </PMSPWC04__RECORD>
    </BUS__OBJ__RECORD>
    </xsl:if>
  </xsl:template>
  <!-- Case 31594 end -->


</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->