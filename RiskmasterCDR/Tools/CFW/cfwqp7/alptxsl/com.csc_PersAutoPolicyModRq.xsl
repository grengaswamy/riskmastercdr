<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Point XML before sending to the the Point system   
E-Service case 33385 
***********************************************************************************************-->

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Template Templates -->
    <xsl:include href="PMSP0000Rq.xsl"/>    <!-- Activity Record                   -->
    <xsl:include href="PMSP0200Rq.xsl"/>    <!-- Basic Contract Record             -->
    <xsl:include href="PMSP1200Rq.xsl"/>    <!-- Additional Interest Record        -->
    <xsl:include href="ASBJCPL0Rq.xsl"/> 	<!-- Personal Auto Unit Record     -->
    <xsl:include href="ASBACPL1Rq.xsl"/>  <!-- Policy Level Record -->
    <xsl:include href="ASBKCPL0Rq.xsl"/> 	<!-- Personal Auto Coverage Record -->
    <xsl:include href="ASBLCPL0Rq.xsl"/>    <!-- Policy Auto Driver Record -->
    <xsl:include href="ASBMCPL0Rq.xsl"/>    <!-- Policy Auto Driver Violations -->
    <!-- Case 39568 Start -->
    <xsl:include href="PTInfoRq.xsl"/>
    <!-- Case 39568 End   -->
   <xsl:include href="BASP0200ERq.xsl"/> <!-- 57418 start -->    

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <!--<xsl:output encoding="UTF-8" version="1.0" method="xml" indent="yes"/>-->
    
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/PolicyNumber"/>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/LOBCd"/>    
	<xsl:variable name="TYPEACT">EN</xsl:variable> <!-- 33385 -->
    <xsl:template match="/">

        <xsl:element name="com.csc_PersAutoPolicyModRq">
        	 <!-- 39568 Start -->
             <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq" mode="CreatePTInfo"/> 
             <!-- 39568 End   -->
	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->             
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy" mode="PMSP0000"/>
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq" mode="PMSP0200"/>
 <!-- Build Policy Level Record -->
            <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq" mode="ASBACPL1"/>
<!-- Build Policy Level Additional Interest Records - PMSP1200 -->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/AdditionalInterest">
                 <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                      <xsl:call-template name="CreateAddlInterest"/>
                 </xsl:if>    
            </xsl:for-each>
<!-- Build Driver Records - ASBLCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersAutoLineBusiness/PersDriver">
                <xsl:call-template name="CreateDriverRec"/>
            </xsl:for-each>                
<!-- Build Violation Records - ASBMCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/AccidentViolation">
	      <xsl:if test="string-length(AccidentViolationCd) > 0">
	      <!--<xsl:if test="string-length(ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/AccidentViolation/AccidentViolationCd) > 0">	Case 33385 -->
                <xsl:call-template name="CreateViolationRec"/>
	      </xsl:if>																<!-- Case 33385 -->
            </xsl:for-each>                            
<!-- Build Vehicle Unit Records - ASBJCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersAutoLineBusiness/PersVeh">
                <xsl:call-template name="CreateUnitRec"/>
<!-- Build Vehicle Level Additional Interest Records - PMSP1200-->
                <xsl:for-each select="AdditionalInterest">
                      <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                         <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>     <!--27083-->
	                   </xsl:call-template>            
                      </xsl:if>    
                </xsl:for-each>
           </xsl:for-each>                                              
<!-- Build Vehicle Coverage Records - ASBKCPL0-->
            <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersAutoLineBusiness/PersVeh/Coverage">
                <xsl:call-template name="CreateCoverageRec"/>
            </xsl:for-each>     
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->