<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
<!--02 Segment of 901 file-->
<xsl:include href="CommonFuncRq.xsl"/>  <!-- common routine -->
<xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->
<xsl:include href="BuildCancel02PTSegmentRq.xsl"/>  <!-- Basic Contract Record -->
<xsl:include href="BuildCancel12PTSegmentRq.xsl"/>	<!-- 1200 records -->

    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM">
        <xsl:choose>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd)='TP'">H67</xsl:when>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd)='THP'">H67</xsl:when>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd)='THX'">H67</xsl:when> 
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd)='TX'">H67</xsl:when>          
            <xsl:otherwise><xsl:value-of select="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="POL">
	  	    <xsl:call-template name="FormatData">
	   	  		<xsl:with-param name="FieldName">$POL</xsl:with-param>      
		     		 <xsl:with-param name="FieldLength">7</xsl:with-param>
		     		 <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PolicyNumber"/>
		      		<xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template> 
	</xsl:variable> 
	<xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	  	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>
    <!-- ICH <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/LOBCd"/>   --> 
    <xsl:variable name="LOB">
    <xsl:choose>
       <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/LOBCd)= 'TP'">THP</xsl:when>
       <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/LOBCd)= 'TX'">THX</xsl:when>
       <xsl:otherwise>  
   
       <xsl:value-of select="/ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/LOBCd"/>
       </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <xsl:variable name="TYPEACT">
  		<xsl:choose>
			<!--103409 starts-->
			<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/com.csc_AmendmentMode='N'">QQ</xsl:when>
			<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/com.csc_AmendmentMode='R'">RB</xsl:when>	
			<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/com.csc_AmendmentMode='NR'">NR</xsl:when>-->

			<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/PolicyStatusCd='N'">QQ</xsl:when>
			<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/PolicyStatusCd='R'">RB</xsl:when>
			<xsl:when test="/ACORD/InsuranceSvcRq/*/com.csc_PartialPolicy/PolicyStatusCd='NR'">NR</xsl:when>
			<!--103409 Ends-->
			<xsl:otherwise>EN</xsl:otherwise>
  		</xsl:choose>
  	</xsl:variable> 
  
<xsl:template match="/">
<com.csc_CancelNonRenewalSubmitRq>
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy" mode="PMSP0000">
               <xsl:with-param name="TransactionType" select="'CN'"/>
            </xsl:apply-templates>
<!--Build the XSL record for PMS0200 Cancellation Segment-->
	<xsl:call-template name ="BuildCancel02PTSegmentRq" />
	<xsl:call-template name ="BuildCancel12PTSegmentRq" />
</com.csc_CancelNonRenewalSubmitRq>
</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->