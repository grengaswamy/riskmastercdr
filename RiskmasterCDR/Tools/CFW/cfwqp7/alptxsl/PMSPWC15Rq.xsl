<!-- Added for Issue 103186-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="CreatePMSPWC15">
		<BUS__OBJ__RECORD>
      		<RECORD__NAME__ROW>
        		<RECORD__NAME>PMSPWC15</RECORD__NAME>
      		</RECORD__NAME__ROW>
			<PMSPWC15__RECORD>
				<SYMBOL>
		          <xsl:value-of select="$SYM"/>
		        </SYMBOL>
		        <POLICYNO>
		          <xsl:value-of select="$POL"/>
		        </POLICYNO>
		        <MODULE>
		          <xsl:value-of select="$MOD"/>
		        </MODULE>
		        <MASTERCO>
		          <xsl:value-of select="$MCO"/>
		        </MASTERCO>
		        <LOCATION>
		          <xsl:value-of select="$LOC"/>
		        </LOCATION>
		        <UNITNO>
					<xsl:call-template name="FormatData">
		            	<xsl:with-param name="FieldName">UNITNO</xsl:with-param>
		            	<xsl:with-param name="FieldLength">5</xsl:with-param>
		            	<xsl:with-param name="Value" select="substring(@id,3,string-length(../@id)-1)"/>
		            	<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</UNITNO>
				<EXPSRTYPE>E</EXPSRTYPE>
				<CLASSNUM>
                	 <xsl:value-of select="substring(AdditionalInterestInfo/com.csc_RatingClassificationCd,1,6)"/>
        		</CLASSNUM>
				<DESCSEQ>
					 <xsl:value-of select="substring(AdditionalInterestInfo/com.csc_RatingClassificationCd,8,2)"/>	
				</DESCSEQ>
				<USE0CODE>
				     <xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
				</USE0CODE>
				<INTERESTSQ>
				     <xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='InterestTypeSeqNbr']/OtherId"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					 </xsl:call-template>
				</INTERESTSQ>
				<WCSTATUS>P</WCSTATUS>
				<EXPOSURE>
					<xsl:value-of select="AdditionalInterestInfo/ActualRemunerationAmt/Amt"/>
				</EXPOSURE>
				<RATE>0</RATE>
				<EXPCOVCODE></EXPCOVCODE>
			</PMSPWC15__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
	<!--Issue 103409 Starts-->
	<xsl:template name="CreatePMSPWC15WorkCompIndividual">
		<BUS__OBJ__RECORD>
      		<RECORD__NAME__ROW>
        		<RECORD__NAME>PMSPWC15</RECORD__NAME>
      		</RECORD__NAME__ROW>
			<PMSPWC15__RECORD>
				<SYMBOL>
		          <xsl:value-of select="$SYM"/>
		        </SYMBOL>
		        <POLICYNO>
		          <xsl:value-of select="$POL"/>
		        </POLICYNO>
		        <MODULE>
		          <xsl:value-of select="$MOD"/>
		        </MODULE>
		        <MASTERCO>
		          <xsl:value-of select="$MCO"/>
		        </MASTERCO>
		        <LOCATION>
		          <xsl:value-of select="$LOC"/>
		        </LOCATION>
		        <UNITNO>
					<xsl:call-template name="FormatData">
		            	<xsl:with-param name="FieldName">UNITNO</xsl:with-param>
		            	<xsl:with-param name="FieldLength">5</xsl:with-param>
		            	<xsl:with-param name="Value" select="substring(@id,4,string-length(@id))"/>
		            	<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</UNITNO>
				<EXPSRTYPE>E</EXPSRTYPE>
				<CLASSNUM>
                	 <xsl:value-of select="substring(RatingClassificationCd,1,6)"/>
        		</CLASSNUM>
				<DESCSEQ>
					 <xsl:value-of select="substring(RatingClassificationCd,7,2)"/>	
				</DESCSEQ>
				<!--<USE0CODE>
				     <xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>IO
				</USE0CODE>-->
				<USE0CODE><xsl:value-of select="IncludedExcludedCd"/></USE0CODE>
				<INTERESTSQ>
				     <xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">DESC0SEQ</xsl:with-param>
						<xsl:with-param name="FieldLength">5</xsl:with-param>
						<xsl:with-param name="Precision">0</xsl:with-param>
						<!--<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='InterestTypeSeqNbr']/OtherId"/>-->
						<xsl:with-param name="Value" select="substring(@id, 2, 1)"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					 </xsl:call-template>
				</INTERESTSQ>
				<WCSTATUS>P</WCSTATUS>
				<EXPOSURE>
					<xsl:value-of select="InclIndividualsEstAnnualRemunerationAmt/Amt"/>
				</EXPOSURE>
				<RATE>0</RATE>
				<EXPCOVCODE></EXPCOVCODE>
			</PMSPWC15__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
	<!--Issue 103409 Ends-->
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->