<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:include href="CommonFuncRs.xsl" /> 
   <xsl:include href="PTErrorRs.xsl" />
   <xsl:include href="PTSignOnRs.xsl"/>
	<xsl:template match="/">
		<ACORD>
		<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<com.csc_CancelNonRenewalSubmitRs>
					<TransactionEffectiveDt>
						<xsl:call-template name="ConvertPTDateToISODate">
                                                     <xsl:with-param name="Value" select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/EFF0DT" /> 
                                                </xsl:call-template>

					</TransactionEffectiveDt>
					<xsl:call-template name="PointErrorsRs" />  <!-- Tracey Scott 12/10/2004 -->
					<com.csc_PartialPolicy>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/POLICY0NUM">
							<PolicyNumber>
								<xsl:value-of select="."/>
							</PolicyNumber>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/MODULE">
							<PolicyVersion>
								<xsl:value-of select="."/>
							</PolicyVersion>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/SYMBOL">
							<CompanyProductCd>
								<xsl:value-of select="."/>
							</CompanyProductCd>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/MASTER0CO">
							<NAICCd>
								<xsl:value-of select="."/>
							</NAICCd>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/LOCATION">
							<com.csc_CompanyPolicyProcessingId>
								<xsl:value-of select="."/>
							</com.csc_CompanyPolicyProcessingId>
						</xsl:for-each>
					</com.csc_PartialPolicy>
					<com.csc_CancelNonRenewalInfo>
						
							<com.csc_ReasonCd>
								<xsl:value-of select="concat(com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/RSN0AM01ST,com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/RSN0AM02ND,com.csc_CancelNonRenewalSubmitRs/PMSP0200__RECORD/RSN0AM03RD)"/>
							</com.csc_ReasonCd>
					
					</com.csc_CancelNonRenewalInfo>
				</com.csc_CancelNonRenewalSubmitRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->