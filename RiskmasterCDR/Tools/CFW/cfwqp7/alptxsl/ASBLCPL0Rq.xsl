<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateDriverRec">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBLCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBLCPL1__RECORD>
            <BLAACD>
               <xsl:value-of select="$LOC"/>
            </BLAACD>
            <BLABCD>
               <xsl:value-of select="$MCO"/>
            </BLABCD>
            <BLARTX>
               <xsl:value-of select="$SYM"/>
            </BLARTX>
            <BLASTX>
               <xsl:value-of select="$POL"/>
            </BLASTX>
            <BLADNB>
               <xsl:value-of select="$MOD"/>
            </BLADNB>
            <BLAGTX>
               <xsl:value-of select="$LOB"/>
            </BLAGTX>
            <BLBRNB>00001</BLBRNB>
            <BLEGNB>00001</BLEGNB>
            <BLANTX>
              <!--<xsl:value-of select="$SYM"/>--> <!--issue # 40090-->
			   <xsl:value-of select="$LOB"/>  <!--issue # 40090-->
            </BLANTX>
            <BLA0NB>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLA0NB</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="ItemIdInfo/InsurerId"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>
            </BLA0NB>
            <BLC6ST>P</BLC6ST>
            <BLANDT>0000000</BLANDT>
            <BLAFNB>00</BLAFNB>
            <BLALDT>0000000</BLALDT>
            <BLAFDT>0000000</BLAFDT>
            <BLTYPE0ACT>
		<!-- 34771 start -->
		<!--103409 starts-->
		<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->
				
		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
		<!-- 34771 end -->
            </BLTYPE0ACT>
            <BLBCTX>
               <xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/Surname"/>
            </BLBCTX>
            <BLI7TX>
               <xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/GivenName"/>
            </BLI7TX>
            <BLI8TX>
               <xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/OtherGivenName"/>
            </BLI8TX>
            <BLI9TX>
               <xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/NameSuffix"/>
            </BLI9TX>
            <BLBDTX>
               <xsl:value-of select="DriverInfo/DriversLicense/DriversLicenseNumber"/>
            </BLBDTX>
            <BLAGDT>
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="DriverInfo/PersonInfo/BirthDt"/>
               </xsl:call-template>
            </BLAGDT>
            <BLBETX>
               <xsl:value-of select="DriverInfo/DriversLicense/StateProvCd"/>
            </BLBETX>
            <BLA7ST>
               <xsl:value-of select="DriverInfo/PersonInfo/GenderCd"/>
            </BLA7ST>
            <BLA8ST>
               <xsl:value-of select="DriverInfo/PersonInfo/MaritalStatusCd"/>
            </BLA8ST>
            <BLA9ST>
               <xsl:value-of select="PersDriverInfo/DriverRelationshipToApplicantCd"/>
            </BLA9ST>
            <xsl:variable name="DriverNbr" select="ItemIdInfo/InsurerId"/>
             <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and (UsePct &gt; '50')]">-->		<!--104527-->
			 <xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and (UsePct &gt; '50') and string-length(@id) = 0]">	<!--104527-->
             <xsl:if test="position()='1'">
            <BLBFTX>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLBFTX</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLBFTX>                
                </xsl:if>
             <xsl:if test="position()='2'">
            <BLBGTX>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLBGTX</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLBGTX>                
                </xsl:if>
             <xsl:if test="position()='3'">
            <BLBJTX>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLBJTX</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLBJTX>                
                </xsl:if>                                                
           </xsl:for-each>
             <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and ((UsePct = '50') or (UsePct &lt; '50'))]">-->	<!--104527-->
			 <xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and ((UsePct = '50') or (UsePct &lt; '50')) and string-length(@id) = 0]">	<!--104527-->
             <xsl:if test="position()='1'">
            <BLBHTX>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLBHTX</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLBHTX>                
                </xsl:if>
             <xsl:if test="position()='2'">
            <BLBITX>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLBITX</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLBITX>                
                </xsl:if>
             <xsl:if test="position()='3'">
            <BLA1NB>
	         <xsl:call-template name="FormatData">
	         	<xsl:with-param name="FieldName">BLA1NB</xsl:with-param>
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="substring(@VehRef,2,string-length(@VehRef)-1)"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
            </BLA1NB>                
                </xsl:if>                                                
           </xsl:for-each>          
            <BLAHDT>
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="DriverInfo/DriversLicense/LicensedDt"/>
               </xsl:call-template>
            </BLAHDT>
            <xsl:choose>
               <xsl:when test="ACORD/InsuranceSvcRq/*/PersAutoLineBusiness/PersVeh/Coverage[CoverageCd='INEXP']">
                  <BLM5TX>1</BLM5TX>
               </xsl:when>
               <xsl:otherwise>
                  <BLM5TX>0</BLM5TX>
               </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
               <xsl:when test="string-length(PersDriverInfo/DefensiveDriverDt)">
                  <BLM6TX>1</BLM6TX>
               </xsl:when>
               <xsl:otherwise>
                  <BLM6TX>0</BLM6TX>
               </xsl:otherwise>
            </xsl:choose>
           <BLM6TX>
               <xsl:value-of select="PersDriverInfo/GoodDriverInd"/>
            </BLM6TX>            
            <BLM7TX>
               <xsl:value-of select="PersDriverInfo/DriverTrainingInd"/>
            </BLM7TX>
            <BLM8TX>
               <xsl:value-of select="PersDriverInfo/GoodStudentCd"/>
            </BLM8TX>
            <BLNATX>
               <xsl:value-of select="PersDriverInfo/DistantStudentInd"/>
            </BLNATX>
			<!--65774 Starts -->
			<xsl:if test = "PersDriverInfo/DriverRelationshipToApplicantCd = 'I'">
			<BLBLNB>
				<xsl:value-of select = "/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI']/InsuredOrPrincipalInfo/CreditScore"/>
			</BLBLNB>
			</xsl:if>
			<!--65774 Ends -->
         </ASBLCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
