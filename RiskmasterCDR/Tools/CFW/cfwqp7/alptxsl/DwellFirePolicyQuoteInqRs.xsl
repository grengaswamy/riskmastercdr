<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Point XML into ACORD XML to
return back to iSolutions   
E-Service case  
***********************************************************************************************-->

   <xsl:include href="CommonFuncRs.xsl"/>   
   <xsl:include href="PTSignOnRs.xsl"/>
   <xsl:include href="PTErrorRs.xsl"/>  
   <xsl:include href="PTProducerRs.xsl"/>
   <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
   <xsl:include href="PTHPPersPolicyRs.xsl"/>
   <xsl:include href="PTAdditionalInterestRs.xsl"/>
   <xsl:include href="PTFormsRs.xsl"/>
   <xsl:include href="PTCoverageRs.xsl"/>
   <xsl:include href="PTDwellRs.xsl"/>
   <xsl:include href="PTLocationRs.xsl"/>
   <xsl:include href="PTSubLocationRs.xsl"/>

   <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
   <xsl:template match="/">
      <ACORD>
         <xsl:call-template name="BuildSignOn"/>
         <InsuranceSvcRs>
              <RqUID/>
              <DwellFirePolicyQuoteInqRs>
                 <RqUID/>
                 <TransactionResponseDt/>
                 <TransactionEffectiveDt/>
                 <CurCd>USD</CurCd>
    	           <xsl:call-template name="PointErrorsRs"/>
                 <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>
                 <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Person"/>
                 <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="PersPolicy"/>
                 
                    <xsl:for-each select="/*/ASBUCPL1__RECORD">
                       <xsl:call-template name="CreateLocation"/>
                    </xsl:for-each>                
                 <DwellFireLineBusiness>
                    <LOBCd>FP</LOBCd>
                   <xsl:for-each select="/*/ASBQCPL1__RECORD">
                       <xsl:call-template name="CreateDwell">
					   		<xsl:with-param name="LOB">FP</xsl:with-param>
					   </xsl:call-template>
					</xsl:for-each>
                   <xsl:for-each select="/*/ASC4CPL1__RECORD">
                       <xsl:call-template name="CreateCoverage">
                          <xsl:with-param name="LOB">FP</xsl:with-param>
                       </xsl:call-template>
                    </xsl:for-each>
                </DwellFireLineBusiness>
             </DwellFirePolicyQuoteInqRs>
         </InsuranceSvcRs>
      </ACORD>
   </xsl:template>
</xsl:stylesheet>
