<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:include href="BOS2ErrorRs.xsl"/>
	<xsl:include href="CommonFuncRs.xsl"/>
	<xsl:include href="BOS2SignOnRs.xsl"/>
	<xsl:include href="BOS2Pinfo53RS.xsl"/>
	<xsl:include href="BOS2LocationSegRS.xsl"/>
	<xsl:include href="BOS2BuildingSegRS.xsl"/>
	<xsl:include href="BOS2CoveragePremiumRS.xsl"/>

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<xsl:variable name="TranEffYear" select="substring(concat('0000',BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranEffMonth" select="substring(concat('00',BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranEffDay" select="substring(concat('00',BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA), string-length(BOPPolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranEffDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>
		<ACORD>
			<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<BOPPolicyAddRs>
					<RqUID/>
					<TransactionRequestDt>
						<xsl:value-of select="$TranEffDate"/>
					</TransactionRequestDt>
					<CurCd>USD</CurCd>
					<xsl:call-template name="SeriesIIErrorsRs"/>
					<xsl:apply-templates mode="Poducer" select="/*/POLICY__INFORMATION__SEG"/>
					<xsl:apply-templates mode="Person" select="/*/POLICY__INFORMATION__SEG"/>
					<!-- Create Policy Information -->
					<xsl:apply-templates mode="Policy" select="/*/POLICY__INFORMATION__SEG"/>
					<!-- Generate Locations -->
					<xsl:call-template name="S2LocationRS"/>
					<BOPLineBusiness>
						<LOBCd>BO</LOBCd>
						<!-- Process Premium amount here -->
						<xsl:call-template name="CovPremiumAmt"/>
					</BOPLineBusiness>
					<!-- Generate SubLocations -->
					<xsl:call-template name="S2SubLocationRS"/>
				</BOPPolicyAddRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="8.077766832400003008BORSInternalS2_03.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->