<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
 <!--xsl:include href="SavErrorRs.xsl"/-->
 <xsl:output method="xml" indent="yes"/>
 
  <xsl:template match="/">
     <xsl:apply-templates select="/com.csc_PolicyListInquireRs"/>
  </xsl:template>

 <xsl:template match="/com.csc_PolicyListInquireRs">
	


<ACORD>
	<SignonRs>
		<Status>
			<StatusCd/>
			<StatusDesc/>
		</Status>
		<SignonRoleCd/>
		<CustId>
			<SPName/>
			<CustLoginId/>
		</CustId>
		<GenSessKey/>
		<ClientDt/>
		<CustLangPref/>
		<ClientApp>
			<Org/>
			<Name/>
			<Version/>
		</ClientApp>
		<ServerDt/>
		<Language/>
	</SignonRs>
	<InsuranceSvcRs>
		<Status>
			<StatusCode/>
			<StatusDesc/>
			<com.csc_ServerStatusCode/>
			<com.csc_Severity/>
		</Status>
		<RqUID/>
		<com.csc_PolicyListInquireRs>
			<RqUID/>
			<TransactionResponseDt/>
			<TransactionEffectiveDt><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></TransactionEffectiveDt>
			<CurCd/>
			<MsgStatus>
				<MsgStatusCd/>
				<MsgStatusDesc/>
				<ExtendedStatus>
					<ExtendedStatusCd/>
					<ExtendedStatusDesc/>
					<com.csc_StatusReference/>
				</ExtendedStatus>
			</MsgStatus>
			<Producer>
				<ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd com.csc_colind="Required">S2Agency</OtherIdTypeCd>
						<OtherId/>
					</OtherIdentifier>
					
				</ItemIdInfo>
				<GeneralPartyInfo>
					<NameInfo>
						<PersonName>
							<Surname><xsl:value-of select= "translate((normalize-space(concat(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/fill_0, /com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER, /com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/fill_1))),' ','')"/></Surname>
<!-- Code change for Producer Name			<GivenName/>   -->
							<ProducerName>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__PRODUCER__CODE" />
							</ProducerName>
							<OtherGivenName/>
							<TitlePrefix/>
							<NameSuffix/>
						</PersonName>
						<LegalEntityCd/>
						<NonTaxIdentity>
							<NonTaxIdTypeCd/>
							<NonTaxId/>
						</NonTaxIdentity>
						<com.csc_LongName com.csc_colind="ReadOnly"><xsl:value-of select= "concat(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/fill_0, /com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER, /com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/fill_1)"/></com.csc_LongName>
					</NameInfo>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
				</GeneralPartyInfo>
				<ProducerInfo>
					<ContractNumber/>
					<ProducerSubCode/>
				</ProducerInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
			</Producer>
			<PersPolicy>
				<PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
				<CompanyProductCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS" /></CompanyProductCd>
				<LOBCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
				<NAICCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__MASTER__CO__NUMBER" /></NAICCd>
				<ControllingStateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></ControllingStateProvCd>
				<TotalAgenPrm><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></TotalAgenPrm>
				<PolicyStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__POLICY__STATUS__ON__PIF" /></PolicyStatusCd>	
				<PayServiceCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__PAY__SERVICE__CODE" /></PayServiceCd>
				<RenewalCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__UNDERWRITING__CODE/PIF__RENEWAL__CODE" /></RenewalCd>
				<AuditCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE" /></AuditCd>	
				<IssueCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ISSUE__CODE" /></IssueCd>	
				<ContractTerm>
					<EffectiveDt><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></EffectiveDt>
					<ExpirationDt><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EXPIRATION__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EXPIRATION__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EXPIRATION__DATE, 7, 2))"/></ExpirationDt>
				<PolicyEnteredDt><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ENTERED__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ENTERED__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ENTERED__DATE, 7, 2))"/></PolicyEnteredDt>
				</ContractTerm>
				<CurrentTermAmt>
					<Amt/>
				</CurrentTermAmt>
				<OriginalInceptionDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ORIGINAL__INCEPT" /></OriginalInceptionDt>
				<PayorCd/>
				<RateEffectiveDt com.csc_colind="Required"><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></RateEffectiveDt>
				<RenewalPayorCd/>
				<RenewalTerm>
					<DurationPeriod>
						<NumUnits/>
					</DurationPeriod>
				</RenewalTerm>
				<Form>
					<FormNumber/>
					<EditionDt/>
					<FormSequencingNumber/>
					<com.csc_FormId/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</Form>
				<Form>
					<FormNumber/>
					<EditionDt/>
					<FormSequencingNumber/>
					<com.csc_FormId/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</Form>
				<QuoteInfo>
					<CompanysQuoteNumber/>
					<IterationNumber/>
				</QuoteInfo>
				<com.csc_BranchCd>
						<xsl:choose>
							<xsl:when test="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH='00'">Master Office</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH" />
							</xsl:otherwise>
						</xsl:choose>
				</com.csc_BranchCd>
				<com.csc_RenewalStatusCd com.csc_colind="Required">1</com.csc_RenewalStatusCd>
				<com.csc_StandardBaseTierInd/>
				<com.csc_AssignedPolicyNumberInd/>
				<com.csc_BranchInfo>
					<xsl:choose>
							<xsl:when test="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH='00'">Master Office</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH" />
							</xsl:otherwise>
						</xsl:choose>
				</com.csc_BranchInfo>
				<com.csc_ApplicationReceivedDt><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></com.csc_ApplicationReceivedDt>
				<com.csc_PolicyWrittenDt com.csc_colind="ReadOnly"><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></com.csc_PolicyWrittenDt>
				<PersApplicationInfo>
					<InsuredOrPrincipal>
						<InsuredOrPrincipalInfo>
							<InsuredOrPrincipalRoleCd/>
							<PersonInfo>
								<GenderCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__SEX" /></GenderCd>
								<BirthDt>
									<xsl:value-of select= "concat(/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__YEAR, '-', /com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__MONTH, '-', /com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__DAY)"/>
								</BirthDt>
								<MaritalStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__MARITAL__STATUS" /></MaritalStatusCd>
								<OccupationDesc/>
								<OccupationClassCd/>
								<MiscParty>
									<GeneralPartyInfo>
										<NameInfo>
											<PersonName>
												<Surname/>
												<GivenName><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></GivenName>
												<OtherGivenName/>
												<TitlePrefix/>
												<NameSuffix/>
											</PersonName>
											<LegalEntityCd/>
											<NonTaxIdentity>
												<NonTaxIdTypeCd/>
												<NonTaxId/>
											</NonTaxIdentity>
											<com.csc_LongName com.csc_colind="ReadOnly"><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></com.csc_LongName>
										</NameInfo>
										<Addr>
											<AddrTypeCd/>
											<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
											<Addr2/>
											<Addr3/>
											<Addr4/>
											<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
											<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
											<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" />-</PostalCode>
											<Country>United States of America</Country>
											<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
											<com.csc_NumYrsAtAddr/>
										</Addr>
										<com.csc_ItemIdInfo>
											<OtherIdentifier>
												<OtherIdTypeCd/>
												<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
											</OtherIdentifier>
											<OtherIdentifier>
												<OtherIdTypeCd/>
												<OtherId/>
											</OtherIdentifier>
										</com.csc_ItemIdInfo>
									</GeneralPartyInfo>
								</MiscParty>
								<com.csc_TaxIdentity>
									<TaxIdTypeCd/>
									<TaxId/>
								</com.csc_TaxIdentity>
								<com.csc_ItemIdInfo>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
								</com.csc_ItemIdInfo>
								<com.csc_ActionInfo>
									<ActionCd/>
									<com.csc_ReasonCd/>
								</com.csc_ActionInfo>
								<com.csc_DriversLicense>
									<DriversLicenseNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__NUMBER" /></DriversLicenseNumber>
									<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__STATE" /></StateProvCd>
								</com.csc_DriversLicense>
								<com.csc_StatusCd/>
							</PersonInfo>
						</InsuredOrPrincipalInfo>
					</InsuredOrPrincipal>
				</PersApplicationInfo>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">100</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">100</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">ITN</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<DriverVeh DriverRef="N1112E" VehRef="N11C5F">
					<UsePct/>
					<com.csc_DriverUseCd com.csc_colind="Required">PR</com.csc_DriverUseCd>
					<com.csc_AttendSchoolInd/>
					<com.csc_PreassignedToVehicleInd/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_GeneralPartyInfo>
						<com.csc_ItemIdInfo>
							<OtherIdentifier>
								<OtherIdTypeCd/>
								<OtherId/>
							</OtherIdentifier>
						</com.csc_ItemIdInfo>
					</com.csc_GeneralPartyInfo>
				</DriverVeh>
				<com.csc_ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
				<com.csc_DataAffectedCd/>
				<com.csc_ActivityDescCd/>
				<com.csc_PayAgentTypeCd/>
			</PersPolicy>
			<PersAutoLineBusiness>
				<LOBCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
				<xsl:for-each select="DRIVER__SEG">
				<PersDriver>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd>CompId</OtherIdTypeCd>
							<OtherId>
								<xsl:value-of select="DRIVER__ID" />
							</OtherId>
						</OtherIdentifier>
					</ItemIdInfo>
					<GeneralPartyInfo>
						<NameInfo>
							<PersonName>
								<Surname/>
								<GivenName><xsl:value-of select="DRIVER__NAME" /></GivenName>
								<OtherGivenName/>
								<TitlePrefix/>
								<NameSuffix/>
							</PersonName>
						</NameInfo>
						<Addr>
							<AddrTypeCd/>
							<Addr1/>
							<Addr2/>
							<Addr3/>
							<Addr4/>
							<City/>
							<StateProvCd/>
							<PostalCode/>
							<Country/>
							<County/>
						</Addr>
						<Communications>
							<PhoneInfo>
								<PhoneTypeCd/>
								<PhoneNumber/>
								<PhoneNumber/>
							</PhoneInfo>
						</Communications>
					</GeneralPartyInfo>
					<DriverInfo>
						<PersonInfo>
							<GenderCd><xsl:value-of select="DRIVER__SEX" /></GenderCd>
							<BirthDt>
								<xsl:value-of select= "concat(DRIVER__DATE__BIRTH/DOB__YEAR, '-', DRIVER__DATE__BIRTH/DOB__MONTH, '-', DRIVER__DATE__BIRTH/DOB__DAY)"/>
							</BirthDt>	
							<DriverAge><xsl:value-of select="DRIVER__AGE" /></DriverAge>
		 					<MaritalStatusCd><xsl:value-of select="DRIVER__MARITAL__STATUS" /></MaritalStatusCd>
							<DriverIdentification><xsl:value-of select="DRIVER__ID" /></DriverIdentification>
							<DriverKey><xsl:value-of select="DRIVER__KEY" /></DriverKey>
							<OccupationClassCd/>
						</PersonInfo>
						<DriversLicense>
							<DriversLicenseNumber><xsl:value-of select="DRIVER__LICENSE__NUMBER" /></DriversLicenseNumber>
							<StateProvCd><xsl:value-of select="DRIVER__LICENSE__STATE" /></StateProvCd>
							<LicenseDate>
								<xsl:value-of select="concat(DRIVER__LICENSE__DATE/DLD__YEAR,'-',DRIVER__LICENSE__DATE/DLD__MONTH,'-',DRIVER__LICENSE__DATE/DLD__DAY)" />
							</LicenseDate>
						</DriversLicense>


					</DriverInfo>
					<PersDriverInfo>
						<DriverRelationshipToApplicantCd><xsl:value-of select="DRIVER__RELATION__TO__INDS" /></DriverRelationshipToApplicantCd>
						<DriverAttribute/>
						<DriverTrainingInd/>
						<DriverTypeCd/>
						<GoodStudentCd/>
					</PersDriverInfo>
				</PersDriver>
				</xsl:for-each>
				<PersVeh id="N11C5F">
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd>CompId</OtherIdTypeCd>
							<OtherId>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__KEY/USVD__UNIT__NUMBER/USVD__NUNIT__LST__DIGIT" />
							</OtherId>							
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Manufacturer/>
					<Model><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__MAKE__DESC" /></Model>
					<VehTypeCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__TYPE__VEHICLE" /></VehTypeCd>
					<ModelYear com.csc_colind="Required"><xsl:value-of select="concat(/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__YEAR__MAKE/USVD__YEAR__CC,/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__YEAR__MAKE/USVD__YEAR__YY)" /></ModelYear>
					<VehBodyTypeCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__BODY" /></VehBodyTypeCd>
					<VehClassCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__VEH__CLASS__CODE" /></VehClassCd>
					<CostNewAmt>
						<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__COST__NEW" /></Amt>
					</CostNewAmt>
					<FullTermAmt>
						<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></Amt>
					</FullTermAmt>
					<TerritoryCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__TERRITORY" /></TerritoryCd>
					<VehIdentificationNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__VIN" /></VehIdentificationNumber>
					<VehSymbolCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__SYMBOL" /></VehSymbolCd>
					<com.csc_VehTypeCd com.csc_colind="Required"><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__TYPE__VEHICLE" /></com.csc_VehTypeCd>
					<VehiclePlateNUmber><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__PL__NUM__TYPE/USVD__PLATE__NUMBER" /></VehiclePlateNUmber>
					<DateLastChange>
						<YearLastChange>
							<YearChangeCC><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__YEAR/USVD__DATE__LAST__CHANGE__CC" /></YearChangeCC>
							<YearChangeYY><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__YEAR/USVD__DATE__LAST__CHANGE__YY" /></YearChangeYY>
						</YearLastChange>	
						<MonthChange><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__MONTH" /></MonthChange>
						<DayChange><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__DAY" /></DayChange>	
					</DateLastChange>					
					<com.csc_AdrSeqNbr/>
					<com.csc_StatedAmt>
						<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__STATED__AMOUNT" /></Amt>
					</com.csc_StatedAmt>
					<com.csc_CountyCd com.csc_colind="Required">051</com.csc_CountyCd>
					<AntiLockBrakeCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__ANTILOCK__DISC" /></AntiLockBrakeCd>
					<MultiCarDiscountInd/>
					<AntiTheftDeviceCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__ANTILOCK__DISC" /></AntiTheftDeviceCd>
					<RateClassCd/>
					<VehPerformanceCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__PERFORMANCE" /></VehPerformanceCd>
					<VehUseCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__USE__CODE" /></VehUseCd>
					<SeatBeltTypeCd/>
					<AirBagTypeCd/>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA1/VCR__COVERAGE__CODE1" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA1/VCR__COVERAGE__LIMIT1" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA1/VCR__COVERAGE__PREMIUM1" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA2/VCR__COVERAGE__CODE2" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA2/VCR__COVERAGE__LIMIT2" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA2/VCR__COVERAGE__PREMIUM2" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA3/VCR__COVERAGE__CODE3" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA3/VCR__COVERAGE__LIMIT3" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA3/VCR__COVERAGE__PREMIUM3" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA4/VCR__COVERAGE__CODE4" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA4/VCR__COVERAGE__LIMIT4" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA4/VCR__COVERAGE__PREMIUM4" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA5/VCR__COVERAGE__CODE5" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA5/VCR__COVERAGE__LIMIT5" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA5/VCR__COVERAGE__PREMIUM5" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA6/VCR__COVERAGE__CODE6" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA6/VCR__COVERAGE__LIMIT6" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA6/VCR__COVERAGE__PREMIUM6" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA7/VCR__COVERAGE__CODE7" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA7/VCR__COVERAGE__LIMIT7" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA7/VCR__COVERAGE__PREMIUM7" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA8/VCR__COVERAGE__CODE8" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA8/VCR__COVERAGE__LIMIT8" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA8/VCR__COVERAGE__PREMIUM8" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA9/VCR__COVERAGE__CODE9" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA9/VCR__COVERAGE__LIMIT9" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA9/VCR__COVERAGE__PREMIUM9" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA10/VCR__COVERAGE__CODE10" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA10/VCR__COVERAGE__LIMIT10" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA10/VCR__COVERAGE__PREMIUM10" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA11/VCR__COVERAGE__CODE11" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA11/VCR__COVERAGE__LIMIT11" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA11/VCR__COVERAGE__PREMIUM11" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA12/VCR__COVERAGE__CODE12" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA12/VCR__COVERAGE__LIMIT12" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA12/VCR__COVERAGE__PREMIUM12" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA13/VCR__COVERAGE__CODE13" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA13/VCR__COVERAGE__LIMIT13" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA13/VCR__COVERAGE__PREMIUM13" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd>
							<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA14/VCR__COVERAGE__CODE14" />
						</CoverageCd>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA14/VCR__COVERAGE__LIMIT14" />
								</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<CurrentTermAmt>
							<Amt>
								<xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__COVERAGE__SEG/SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA14/VCR__COVERAGE__PREMIUM14" />
							</Amt>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>					
					<com.csc_Form>
						<FormNumber/>
						<EditionDt/>
						<com.csc_SequenceNumber/>
						<com.csc_FormId com.csc_colind="Required">AU621</com.csc_FormId>
						<com.csc_OverrideInd/>
					</com.csc_Form>
					<com.csc_TransAmt>
						<Amt/>
					</com.csc_TransAmt>
				</PersVeh>
				<com.csc_Location>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Addr>
						<AddrTypeCd com.csc_colind="Required">PLG</AddrTypeCd>
						<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
						<Addr2/>
						<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
						<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
						<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" /></PostalCode>
						<Country/>
						<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
					</Addr>
					<com.csc_AdrSeqNbr/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</com.csc_Location>
				<com.csc_Location>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select= "concat(substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 1, 4), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 5, 2), '-', substring(/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE, 7, 2))"/></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Addr>
						<AddrTypeCd com.csc_colind="Required">PLG</AddrTypeCd>
						<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
						<Addr2/>
						<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
						<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
						<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" />-</PostalCode>
						<Country/>
						<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
					</Addr>
					<com.csc_AdrSeqNbr/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</com.csc_Location>
			</PersAutoLineBusiness>
			<PolicySummaryInfo>
				<PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
				<FullTermAmt>
					<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></Amt>
				</FullTermAmt>
				<PolicyStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__POLICY__STATUS__ON__PIF" /></PolicyStatusCd>
				<com.csc_TransAmt>
					<Amt/>
				</com.csc_TransAmt>
			</PolicySummaryInfo>
			<com.csc_BillingInfo>
				<com.csc_BillingAccountNumber/>
				<com.csc_BillingAccountId/>
				<com.csc_PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></com.csc_PolicyNumber>
				<PersonName>
					<Surname/>
					<GivenName><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></GivenName>
					<OtherGivenName/>
					<TitlePrefix/>
					<NameSuffix/>
				</PersonName>
			</com.csc_BillingInfo>
			<com.csc_ClaimInfo/>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
		</com.csc_PolicyListInquireRs>
	</InsuranceSvcRs>
</ACORD>
</xsl:template>
</xsl:stylesheet>
