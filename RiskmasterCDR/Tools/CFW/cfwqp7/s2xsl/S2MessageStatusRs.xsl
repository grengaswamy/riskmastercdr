<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series2 rating errors into ACORD XML
to return back to iSolutions.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template name="BuildMessageStatus">
	 <xsl:param name="MessageType" />
		<!--Modified for Issue 35934
		<xsl:if test="count(//ERROR__DESC__SEG) &gt; 0"> -->
		<xsl:choose>
			<xsl:when test="count(//ERROR__DESC__SEG) &gt; 0">
				<MsgStatus>
					<!--Modified for Issue 35934<MsgStatusCd>ERROR</MsgStatusCd>-->
					<MsgStatusCd>1</MsgStatusCd>
					<!--Modified for Issue 35934-->
					<MsgErrorCd>1000</MsgErrorCd>
					<!-- Case 34768: Set error code to 1000 if errors exist. -->
					<!-- <MsgStatusDesc>Error(s) occurred.</MsgStatusDesc>	Case 34768 - Comment out -->
					<!-- Case 34768 Begin -->
					<MsgStatusDesc>
						<xsl:choose>
							<xsl:when test="string-length($MessageType) = 0">Error(s) occurred.</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat($MessageType, ' ERROR(S) OCCURRED')"/>
							</xsl:otherwise>
						</xsl:choose>
					</MsgStatusDesc>
					<!-- Case 34768 End -->
					<xsl:for-each select="//ERROR__DESC__SEG">
						<ExtendedStatus>
							<ExtendedStatusCd>
								<xsl:value-of select="./ERR__SEGMENT__DATA/ERR__ERROR__CODE"/>
							</ExtendedStatusCd>
							<ExtendedStatusDesc>
								<!-- Case 39354 Begin -->
								<xsl:variable name="ErrMessage">
									<xsl:value-of select="substring(./ERR__SEGMENT__DATA/ERR__ERROR__DESC,3,48)"/>
								</xsl:variable>
								<!-- xsl:value-of select="concat(./ERR__SEGMENT__DATA/ERR__ERROR__DESC,./ERR__SEGMENT__DATA/ERR__XML__ERROR__DESC)"/ -->
								<xsl:value-of select="concat($ErrMessage,' ',./ERR__SEGMENT__DATA/ERR__ERROR__STATE,' ',./ERR__SEGMENT__DATA/ERR__ERROR__SEG__DATA)"/>
							</ExtendedStatusDesc>
						</ExtendedStatus>
					</xsl:for-each>
				</MsgStatus>
				<!--Added MsgStatus for Issue 35934-->
				<!--	</xsl:if>  -->
			</xsl:when>
			<xsl:otherwise>
				<MsgStatus>
					<MsgStatusCd>0</MsgStatusCd>
					<MsgStatusDesc>
						<xsl:value-of select="//POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__DESC"/>
					</MsgStatusDesc>
				</MsgStatus>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->