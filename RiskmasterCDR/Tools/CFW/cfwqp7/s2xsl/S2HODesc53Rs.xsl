<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service iSSUE # 35270 
***********************************************************************************************
-->
	<xsl:template name="S2HODescRsTemplate">
		<!--<xsl:for-each select="HomePolicyQuoteInqRs/DESCRIPTION__FULL__SEG">-->
			<xsl:if test="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE !='LA'">
				<AdditionalInterest>
					<GeneralPartyInfo>
						<NameInfo>
							<CommlName>
								<CommercialName>
									<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1"/>
								</CommercialName>
								<SupplementaryNameInfo>
									<SupplementaryNameCd/>
									<SupplementaryName/>
								</SupplementaryNameInfo>
							</CommlName>
						</NameInfo>
						<Addr>
							<Addr1>
								<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__2"/>
							</Addr1>
							<Addr2>
								<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3"/>
							</Addr2>
							<xsl:choose>
								<xsl:when test="string-length(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4) &gt; 0">
									<xsl:variable name="Addr4City">
										<xsl:value-of select="substring-before(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,',')"/>
									</xsl:variable>
									<xsl:variable name="Addr4State">
										<xsl:value-of select="substring-after(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,', ')"/>
									</xsl:variable>
									<xsl:choose>
										<xsl:when test="string-length($Addr4City) &gt; 0">
											<City>
												<xsl:value-of select="$Addr4City"/>
											</City>
											<StateProvCd>
												<xsl:value-of select="$Addr4State"/>
											</StateProvCd>
										</xsl:when>
										<xsl:when test="string-length(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4) &gt; 2">
											<!-- No comma between city and state. -->
											<City>
												<xsl:value-of select="substring-before(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,' ')"/>
											</City>
											<StateProvCd>
												<xsl:value-of select="substring-after(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,' ')"/>
											</StateProvCd>
										</xsl:when>
										<xsl:otherwise>
											<!-- If Address line 4 is not empty, and there is no comma seperating the city and state, then the state could be on line 4 by itself. -->
											<City>
												<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3"/>
											</City>
											<StateProvCd>
												<xsl:value-of select="substring(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4, 1, 2)"/>
											</StateProvCd>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="Addr3City">
										<xsl:value-of select="substring-before(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3,', ')"/>
									</xsl:variable>
									<xsl:variable name="Addr3State">
										<xsl:value-of select="substring-after(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3,', ')"/>
									</xsl:variable>
									<xsl:choose>
										<xsl:when test="string-length($Addr3City) &gt; 0">
											<City>
												<xsl:value-of select="$Addr3City"/>
											</City>
											<StateProvCd>
												<xsl:value-of select="$Addr3State"/>
											</StateProvCd>
										</xsl:when>
										<xsl:otherwise>
											<!-- There is no comma seperating the city and state. -->
											<City>
												<xsl:value-of select="substring-before(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3, ' ')"/>
											</City>
											<StateProvCd>
												<xsl:value-of select="substring-after(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3, ' ')"/>
											</StateProvCd>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
							<PostalCode>
								<xsl:value-of select="substring(DESCRIPTION__INFORMATION/DESCRIPTION__ZIP__CODE,2,5)"/>
							</PostalCode>
						</Addr>
					</GeneralPartyInfo>
					<AdditionalInterestInfo>
						<NatureInterestCd>
							<xsl:value-of select="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE"/>
						</NatureInterestCd>
						<AccountNumberId>
							<xsl:value-of select="DESCRIPTION__LOAN__NUMBER"/>
						</AccountNumberId>
					</AdditionalInterestInfo>
				</AdditionalInterest>
			</xsl:if>
		<!--</xsl:for-each>-->
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="CFWintermediat Response File.xml" userelativepaths="yes" externalpreview="no" url="CFWintermediat Response File.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="CFWintermediat Response File.xml" srcSchemaRoot="HomePolicyQuoteInqRs" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->