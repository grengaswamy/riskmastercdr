<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 34768 
***********************************************************************************************
-->
  <xsl:include href="S2CommonFuncRs.xsl"/>
  <xsl:include href="S2SignOnRs.xsl"/>
  <xsl:include href="S2ErrorRs.xsl"/>
  <xsl:include href="S2ProducerRs.xsl"/>
  <xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
  <xsl:include href="S2CommlPolicyRs.xsl"/>
  <xsl:include href="S2LocationRs.xsl"/>
  <xsl:include href="S2AdditionalInterestRs.xsl"/>
  <xsl:include href="S2WCWorkSTATERs.xsl"/>
  <xsl:include href="S2WCCommlCoverage12GPRs.xsl"/>
  <xsl:include href="S2WorkCompLocInfoRs.xsl"/>
  <xsl:include href="S2MessageStatusRs.xsl"/>
  <xsl:include href="S2CreditSurchargeRs.xsl"/>

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

  <xsl:template match="/">
    <xsl:variable name="PolicyMode" select="/*/POLICY__INFORMATION__SEG/PIF__ISSUE__CODE"/>
    <ACORD>
      <xsl:call-template name="BuildSignOn"/>
      <InsuranceSvcRs>
        <RqUID/>
        <WorkCompPolicyAddRs>
          <RqUID/>
          <TransactionResponseDt/>
          <TransactionEffectiveDt/>
          <CurCd>USD</CurCd>
	  <xsl:call-template name="BuildMessageStatus">			<!-- Case 34768 -->
		<xsl:with-param name="MessageType">			<!-- Case 34768 -->
			<xsl:value-of select="string('RATING')"/>	<!-- Case 34768 -->
		</xsl:with-param>					<!-- Case 34768 -->
	  </xsl:call-template>						<!-- Case 34768 -->
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WC"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Person"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WCPolicy"/>
		<!--Create Location -->
	  <xsl:call-template name="CreateLocationFrom43SEG">
		<xsl:with-param name="LOB" select="WCP"/>
	  </xsl:call-template>
	  <WorkCompLineBusiness>
	  	<LOBCd>WCP</LOBCd>
		<xsl:for-each select="/WorkCompPolicyAddRs/PMDI4W1__STATE__RATING__REC">
			<xsl:variable name="State" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
			<xsl:variable name="StateUnmodPrem" select="PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__UNMOD__STATE__PREM"/>
			<xsl:variable name="StateTermPrem" select="PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__STATE__TERM__PREM"/>
			<xsl:choose>
				<xsl:when test="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE = '0000'">
				<WorkCompRateState>
					<StateProvCd>
						<xsl:call-template name="BuildStateProvCd">
							<xsl:with-param name="StateCd" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
						</xsl:call-template>
					</StateProvCd>
					<AnniversaryRatingDt/>
					<NCCIIDNumber/>

				<!-- Do State Class Codes -->
					<xsl:for-each select="/WorkCompPolicyAddRs/PMDU4W1__CLASS__RATING__SEG[PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__WC__RATING__STATE=$State]">
						<xsl:call-template name="CreateWorkCompLocInfo">
							<xsl:with-param name="StateCd" select="$State"/>
						</xsl:call-template>
					</xsl:for-each>

				<!-- Do State Totals -->
				<xsl:if test="$PolicyMode='N'">
					<xsl:call-template name="CreateWorkCompLocInfoTotals">
						<xsl:with-param name="StateCd" select="$State"/>
						<xsl:with-param name="StateUPrem" select="$StateUnmodPrem"/>
						<xsl:with-param name="StateTPrem" select="$StateTermPrem"/>
					</xsl:call-template>
				</xsl:if>
				</WorkCompRateState>

				<!-- Generated State Modifier Class Codes for Amendments -->
				<xsl:if test="$PolicyMode='A'">
					<xsl:call-template name="CreateWorkCompLocInfoState">
						<xsl:with-param name="StateCd" select="$State"/>
					</xsl:call-template>
				</xsl:if>

				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:for-each>

				<!-- Do Policy Totals -->
		<xsl:if test="$PolicyMode='N'">
			<xsl:call-template name="CreateWorkCompLocInfoPolicy"/>
		</xsl:if>

		<xsl:apply-templates select="CreateCommlCoverage"/>
		<QuestionAnswer>
			<QuestionCd/>
			<YesNoCd/>
			<Explanation/>
		</QuestionAnswer>
	  </WorkCompLineBusiness>
          <RemarkText IdRef="policy">Work Comp XML Issue</RemarkText>
        </WorkCompPolicyAddRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>

</xsl:stylesheet>