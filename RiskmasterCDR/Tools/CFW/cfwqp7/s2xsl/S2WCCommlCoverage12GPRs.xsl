<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template name="CreateCommlCoverage">
		<CommlCoverage>
		   <CoverageCd>WCEL</CoverageCd>             <!-- 55614 -->
			<Limit>
				<!-- <CoverageCd>WCEL</CoverageCd>  55614 -->
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__STD/PMDL4W1__COV__II__LMTS__STD__EACH"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>EachClaim</LimitAppliesToCd>
			</Limit>
			<!--<TerritoryCd>
				<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__LATEST__COVII__IDS/PMDL4W1__CURR__COVII__STD__ID"/>
			</TerritoryCd>-->
			<Limit>
				<!-- <CoverageCd>WCEL</CoverageCd>  55614 -->
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__STD/PMDL4W1__COV__II__LMTS__STD__EACH"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
			</Limit>
			<!--<TerritoryCd>
				<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__LATEST__COVII__IDS/PMDL4W1__CURR__COVII__STD__ID"/>
			</TerritoryCd>-->
			<Limit>
				<!-- <CoverageCd>WCEL</CoverageCd>  55614 -->
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__STD/PMDL4W1__COV__II__LMTS__STD__POL"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
			</Limit>
			<!--<TerritoryCd>
				<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__LATEST__COVII__IDS/PMDL4W1__CURR__COVII__STD__ID"/>
			</TerritoryCd>-->
                        <xsl:for-each select="/*/DESCRIPTION__FULL__SEG">
                        <xsl:if test="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE='GP'">
				<Option>
					<OptionTypeCd>
						<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__DESC"/>
					</OptionTypeCd>
					<OptionCd>
						<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__CODE"/>
					</OptionCd>
					<OptionValue>
						<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESC__NEW__GROUP__DISC__PERCENT"/>
					</OptionValue>
				</Option>
                        </xsl:if>
                        </xsl:for-each>
           <TerritoryCd>
				<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__LATEST__COVII__IDS/PMDL4W1__CURR__COVII__STD__ID"/>
			</TerritoryCd>
		</CommlCoverage>
		<!-- 55614 Begin -->
		<CommlCoverage>
		   <CoverageCd>VOL</CoverageCd>            
			<Limit>
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__VC/PMDL4W1__COV__II__LMTS__VC__EACH"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>EachClaim</LimitAppliesToCd>
			</Limit>
			<Limit>
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__VC/PMDL4W1__COV__II__LMTS__VC__EACH"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>EachEmployee</LimitAppliesToCd>
			</Limit>
			<Limit>
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__COVERAGE__II__LMTS__VC/PMDL4W1__COV__II__LMTS__VC__POL"/>
					</Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd>PolicyLimit</LimitAppliesToCd>
			</Limit>
			<TerritoryCd>
				<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__LATEST__COVII__IDS/PMDL4W1__CURR__COVII__VOL__ID"/>
			</TerritoryCd>
		</CommlCoverage>
		<!-- 55614 End -->
	</xsl:template>
</xsl:stylesheet>