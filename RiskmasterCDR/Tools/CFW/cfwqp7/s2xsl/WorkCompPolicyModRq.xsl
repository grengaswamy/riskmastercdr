<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="xalan://java.util.GregorianCalendar">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service case 34768 
***********************************************************************************************
-->

	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2AuditScheduleSegRq.xsl"/>
	<xsl:include href="S2ClassRatingSegRq.xsl"/>
	<xsl:include href="S2PolicyRatingRecRq.xsl"/>
	<xsl:include href="S2LocationSegRq.xsl"/>
	<xsl:include href="S2AdditionalIntSegRq.xsl"/>
	<xsl:include href="S2StateRatingRecRq.xsl"/>
	<xsl:include href="S2Pinfo53.xsl"/>
	<xsl:include href="S2EPF00.xsl"/>

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<!--103409 Starts-->
	<!--<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	--><!--Issue # 40090 start --><!--
	--><!--<xsl:variable name="SYM" select="string('WCP')"/>--><!--
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/CompanyProductCd"/>
	--><!--Issue # 40090 End --><!--
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/PolicyNumber"/>-->

	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/CompanyProductCd"/>
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/PolicyNumber"/>
	<xsl:variable name="MOD">
	<!--103409 Ends-->
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<!--<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/PolicyVersion"/>-->			<!--103409-->
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/PolicyVersion"/>			<!--103409-->
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>            
	</xsl:variable>    
	<!--<xsl:variable name="LOB" select="string('WCP')"/>-->
	<!--<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/LOBCd"/>-->	<!--103409-->
	<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/LOBCd"/>	<!--103409-->
	<!--Issue # 40090 End -->
	<!--103409 Starts-->
	<!--<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/TransactionRequestDt"/>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/ExpirationDt"/>-->

	<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/TransactionRequestDt"/>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/CommlPolicy/ContractTerm/ExpirationDt"/>
	<!--103409 Ends-->

	<xsl:variable name="tmp" select="date:new()"/>		
	<xsl:variable name="month" select="substring(concat('0',date:get($tmp, 2) + 1),string-length(date:get($tmp, 2) + 1))" />
	<xsl:variable name="day" select="substring(concat('0',date:get($tmp, 5)),string-length(date:get($tmp, 5)))" />
	<xsl:variable name="year" select="date:get($tmp, 1)" />
	<xsl:variable name="ActDate">
		<xsl:value-of select="$year"/>/<xsl:value-of select="$month"/>/<xsl:value-of select="$day"/>
	</xsl:variable>

	<xsl:template match="/">

		<!--<xsl:element name="com.csc_WorkCompPolicyModRq">-->	<!--103409-->
		<xsl:element name="WorkCompPolicyModRq">	<!--103409-->
			<!-- First, create the 00 Segment -->
			<xsl:call-template name="S2EPF00Template">
				<xsl:with-param name="ProcessingType">EN</xsl:with-param>
				<xsl:with-param name="PolicyMode">A</xsl:with-param>
			</xsl:call-template>

			<!-- Create 02 Segment -->
			<xsl:call-template name="S2PinfoTemplate">   
				<xsl:with-param name="ProcessingType">EN</xsl:with-param>
				<xsl:with-param name="PolicyMode">A</xsl:with-param>
			</xsl:call-template>

			<!-- Create 43JJ (Location) Segments -->
			<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location">-->		<!--103409-->
			<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/Location">	<!--103409-->
				<xsl:call-template name="S2LocationSegTemplate">
					<xsl:with-param name="TYPEACT">EN</xsl:with-param>
				</xsl:call-template>
			</xsl:for-each>

			<!-- Create 43LD (Audit Schedule) Segments -->
				<xsl:call-template name="S2AuditScheduleSegTemplate">
					<xsl:with-param name="TYPEACT">Q</xsl:with-param>
				</xsl:call-template>

			<!-- Create 43LJ (Additional Interest) Segments -->
			<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location/AdditionalInterest">-->	<!--103409-->
			<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/Location/AdditionalInterest">		<!--103409-->
				<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
					<xsl:call-template name="S2AdditionalIntSegTemplate"/>
				</xsl:if>
			</xsl:for-each>

			<!-- Create 43LX (Policy Rating) Segment -->
			<xsl:variable name="ContState" select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/ControllingStateProvCd,1,2)"/>
			<xsl:variable name="AnnDate" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[StateProvCd = $ContState]/AnniversaryRatingDt"/>
			<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness">--><!--103409-->
			<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/WorkCompLineBusiness">				<!--103409-->
				<xsl:call-template name="S2PolicyRatingRecTemplate"> 
					<xsl:with-param name="AnnDt" select="$AnnDate"/>  
				</xsl:call-template>
			</xsl:for-each>

			<!-- Create 43IX (State) NONPREM Segments for each state -->
			<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState">-->	<!--103409-->
			<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState">				<!--103409-->
				<xsl:variable name="StateCode" select="StateProvCd"/>
				<xsl:call-template name="S2StateRatingRecTemplate">
					<xsl:with-param name="StateRecType">NONPREM</xsl:with-param>
					<xsl:with-param name="StateCd" select="$StateCode"/>
				</xsl:call-template>

				<!-- Create 43IX (State) Optional Modifier Segments for each state -->
				<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState[StateProvCd=$StateCode]/CreditOrSurcharge">-->		<!--103409-->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState[StateProvCd=$StateCode]/CreditOrSurcharge">				<!--103409-->
					<xsl:if test="(NumericValue/FormatInteger != 'G') and (string-length(CreditSurchargeCd) &gt; 0)">
						<xsl:call-template name="S2StateRatingRecTemplate">
							<xsl:with-param name="StateRecType">OPTMOD</xsl:with-param>
							<xsl:with-param name="StateCd" select="$StateCode"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

			<!-- Create 43UX (Class Codes) Segments -->
			<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo">-->		<!--103409-->
			<xsl:for-each select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo">			<!--103409-->
				<xsl:for-each select="WorkCompRateClass">
					<xsl:call-template name="S2ClassRatingSegTemplate">
					</xsl:call-template>
				</xsl:for-each>		
			</xsl:for-each>

		</xsl:element>			
	</xsl:template>

</xsl:stylesheet>