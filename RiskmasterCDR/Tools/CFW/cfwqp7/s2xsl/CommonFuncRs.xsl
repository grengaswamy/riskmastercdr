<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <!-- 55614 begin -->
    <xsl:variable name="KEY">
  	        <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL"/>
	   	  <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER"/>
	   	  <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__MODULE"/>
   </xsl:variable>        
   <!-- 55614 end -->
	<xsl:template name="ConvertNumericStateCdToAlphaStateCd">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='01'">AL</xsl:when>
			<xsl:when test="$Value='02'">AZ</xsl:when>
			<xsl:when test="$Value='03'">AR</xsl:when>
			<xsl:when test="$Value='04'">CA</xsl:when>
			<xsl:when test="$Value='05'">CO</xsl:when>
			<xsl:when test="$Value='06'">CT</xsl:when>
			<xsl:when test="$Value='07'">DE</xsl:when>
			<xsl:when test="$Value='08'">DC</xsl:when>
			<xsl:when test="$Value='09'">FL</xsl:when>
			<xsl:when test="$Value='10'">GA</xsl:when>
			<xsl:when test="$Value='11'">ID</xsl:when>
			<xsl:when test="$Value='12'">IL</xsl:when>
			<xsl:when test="$Value='13'">IN</xsl:when>
			<xsl:when test="$Value='14'">IA</xsl:when>
			<xsl:when test="$Value='15'">KS</xsl:when>
			<xsl:when test="$Value='16'">KY</xsl:when>
			<xsl:when test="$Value='17'">LA</xsl:when>
			<xsl:when test="$Value='18'">ME</xsl:when>
			<xsl:when test="$Value='19'">MD</xsl:when>
			<xsl:when test="$Value='20'">MA</xsl:when>
			<xsl:when test="$Value='21'">MI</xsl:when>
			<xsl:when test="$Value='22'">MN</xsl:when>
			<xsl:when test="$Value='23'">MS</xsl:when>
			<xsl:when test="$Value='24'">MO</xsl:when>
			<xsl:when test="$Value='25'">MT</xsl:when>
			<xsl:when test="$Value='26'">NE</xsl:when>
			<xsl:when test="$Value='27'">NV</xsl:when>
			<xsl:when test="$Value='28'">NH</xsl:when>
			<xsl:when test="$Value='29'">NJ</xsl:when>
			<xsl:when test="$Value='30'">NM</xsl:when>
			<xsl:when test="$Value='31'">NY</xsl:when>
			<xsl:when test="$Value='32'">NC</xsl:when>
			<xsl:when test="$Value='33'">ND</xsl:when>
			<xsl:when test="$Value='34'">OH</xsl:when>
			<xsl:when test="$Value='35'">OK</xsl:when>
			<xsl:when test="$Value='36'">OR</xsl:when>
			<xsl:when test="$Value='37'">PA</xsl:when>
			<xsl:when test="$Value='38'">RI</xsl:when>
			<xsl:when test="$Value='39'">SC</xsl:when>
			<xsl:when test="$Value='40'">SD</xsl:when>
			<xsl:when test="$Value='41'">TN</xsl:when>
			<xsl:when test="$Value='42'">TX</xsl:when>
			<xsl:when test="$Value='43'">UT</xsl:when>
			<xsl:when test="$Value='44'">VT</xsl:when>
			<xsl:when test="$Value='45'">VA</xsl:when>
			<xsl:when test="$Value='46'">WA</xsl:when>
			<xsl:when test="$Value='47'">WV</xsl:when>
			<xsl:when test="$Value='48'">WI</xsl:when>
			<xsl:when test="$Value='49'">WY</xsl:when>
			<xsl:when test="$Value='50'">HI</xsl:when>
			<xsl:when test="$Value='51'">AK</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--  Format an eight digit S2 date (i.e. 20011231) to an ISO date (i.e. 2001-12-31) -->
	<xsl:template name="ConvertS2DateToISODate">
		<xsl:param name="Value"/>
		<!--  Year -->
		<xsl:value-of select="concat(substring($Value,1,4),'/')"/>
		<!--  Month -->
		<xsl:value-of select="concat(substring($Value,5,2),'/')"/>
		<!--  Day -->
		<xsl:value-of select="substring($Value,7,2)"/>
	</xsl:template>

	<!-- Start Issue 35271 -->
	<xsl:template name="ConvertSubjectOfInsToCovCode">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='106'">BOILER</xsl:when>
			<xsl:when test="$Value='116'">CONDO</xsl:when>
			<xsl:when test="$Value='113'">MONEY</xsl:when>
			<xsl:when test="$Value='120'">PRFCMT</xsl:when>
			<xsl:when test="$Value='108'">ACCTS</xsl:when>
			<xsl:when test="$Value='114'">ADDMGR</xsl:when>
			<xsl:when test="$Value='102'">BURG</xsl:when>
			<xsl:when test="$Value='117'">CMPTR</xsl:when>
			<xsl:when test="$Value='103'">EGLASS</xsl:when>
			<xsl:when test="$Value='121'">IGLASS</xsl:when>
			<xsl:when test="$Value='115'">LOSS</xsl:when>
			<xsl:when test="$Value='119'">MECH</xsl:when>
			<xsl:when test="$Value='104'">SIGNS</xsl:when>
			<xsl:when test="$Value='118'">SPOIL</xsl:when>
			<xsl:when test="$Value='109'">VALPAP</xsl:when>
			<xsl:when test="$Value='101'">BPP</xsl:when>
			<xsl:when test="$Value='100'">BLDG</xsl:when>
			<xsl:when test="$Value='105'">EMPDIS</xsl:when>
			<xsl:when test="$Value='110'">NONOWN</xsl:when>
			<xsl:when test="$Value='111'">HIRED</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="000"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

<!--	<xsl:template name="ConvertCoverageEndorse">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='BOILER'">BV</xsl:when>
			<xsl:when test="$Value='CONDO'">CM</xsl:when>
			<xsl:when test="$Value='MONEY'">MS</xsl:when>
			<xsl:when test="$Value='PRFCMT'">FD</xsl:when>
			<xsl:when test="$Value='ACCTS'">AR</xsl:when>
			<xsl:when test="$Value='ADDMGR'">AI</xsl:when>
			<xsl:when test="$Value='BURG'">BR</xsl:when>
			<xsl:when test="$Value='CMPTR'">EM</xsl:when>
			<xsl:when test="$Value='EGLASS'">EG</xsl:when>
			<xsl:when test="$Value='IGLASS'">IG</xsl:when>
			<xsl:when test="$Value='LOSS'">CL</xsl:when>
			<xsl:when test="$Value='MECH'">MB</xsl:when>
			<xsl:when test="$Value='SIGNS'">OS</xsl:when>
			<xsl:when test="$Value='SPOIL'">SC</xsl:when>
			<xsl:when test="$Value='VALPAP'">VP</xsl:when>
			<xsl:when test="$Value='FLL'">FL</xsl:when>
			<xsl:when test="$Value='BPP'">PP</xsl:when>
			<xsl:when test="$Value='BLDG'">BD</xsl:when>
			<xsl:when test="$Value='EMPDIS'">ED</xsl:when>
			<xsl:when test="$Value='NONOWN'">NO</xsl:when>
			<xsl:when test="$Value='HIRED'">HA</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="000"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>-->
	<!-- End Issue 35271 -->

</xsl:stylesheet>