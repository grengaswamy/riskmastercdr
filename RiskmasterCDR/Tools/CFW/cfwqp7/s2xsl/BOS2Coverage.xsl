<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="BOS2CoveragePremium.xsl"/>
	<!-- Premium Information (pmduyb1.cbl) -->

	<xsl:template name="S2CoverageTemplate01" match="/">
		<!-- For Optional Coverages -->
		<xsl:param name="RefLocId"/>
		<xsl:param name="RefSubLocId"/>
		<xsl:param name="LocId"/>
		<xsl:param name="SubLocId"/>

		<xsl:for-each select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and normalize-space(SubjectInsuranceCd)='']/CommlCoverage[normalize-space(CoverageCd) != '']">
			<BUS__OBJ__RECORD>
				<PMDUXB1__UNIT__GRP__RATING__SEG>
					<PMDUXB1__SEGMENT__KEY>
						<PMDUXB1__REC__LLBB/>
						<PMDUXB1__SEGMENT__ID>43</PMDUXB1__SEGMENT__ID>
						<PMDUXB1__SEGMENT__STATUS>A</PMDUXB1__SEGMENT__STATUS>
						<PMDUXB1__TRANSACTION__DATE>
							<PMDUXB1__YEAR__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,1,4)"/>
							</PMDUXB1__YEAR__TRANSACTION>
							<PMDUXB1__MONTH__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,6,2)"/>
							</PMDUXB1__MONTH__TRANSACTION>
							<PMDUXB1__DAY__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,9,2)"/>
							</PMDUXB1__DAY__TRANSACTION>
						</PMDUXB1__TRANSACTION__DATE>
						<PMDUXB1__SEGMENT__ID__KEY>
							<PMDUXB1__SEGMENT__LEVEL__CODE>U</PMDUXB1__SEGMENT__LEVEL__CODE>
							<PMDUXB1__SEGMENT__PART__CODE>X</PMDUXB1__SEGMENT__PART__CODE>
							<PMDUXB1__SUB__PART__CODE>1</PMDUXB1__SUB__PART__CODE>
							<PMDUXB1__INSURANCE__LINE>BP</PMDUXB1__INSURANCE__LINE>
						</PMDUXB1__SEGMENT__ID__KEY>
						<PMDUXB1__LEVEL__KEY>
							<PMDUXB1__LOCATION__NUMBER__A>
								<PMDUXB1__LOCATION__NUMBER>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">SITE</xsl:with-param>
										<xsl:with-param name="FieldLength">4</xsl:with-param>
										<xsl:with-param name="Value" select="$LocId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</PMDUXB1__LOCATION__NUMBER>
							</PMDUXB1__LOCATION__NUMBER__A>
							<PMDUXB1__SUB__LOCATION__NUMBER__A>
								<PMDUXB1__SUB__LOCATION__NUMBER>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">SITE</xsl:with-param>
										<xsl:with-param name="FieldLength">3</xsl:with-param>
										<xsl:with-param name="Value" select="$SubLocId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</PMDUXB1__SUB__LOCATION__NUMBER>
							</PMDUXB1__SUB__LOCATION__NUMBER__A>
							<PMDUXB1__RISK__UNIT__GROUP__KEY>
								<PMDUXB1__PMS__DEF__SUBJ__OF__INS>
									<PMDUXB1__SUBJ__OF__INS>
										<xsl:call-template name="ConvertSubjectOfInsToNumeric">
											<xsl:with-param name="Value" select="CoverageCd"/>
										</xsl:call-template>
									</PMDUXB1__SUBJ__OF__INS>
								</PMDUXB1__PMS__DEF__SUBJ__OF__INS>
								<PMDUXB1__SEQUENCE__RISK__UNIT>
									<xsl:value-of select="string('   ')"/>
								</PMDUXB1__SEQUENCE__RISK__UNIT>
							</PMDUXB1__RISK__UNIT__GROUP__KEY>
							<PMDUXB1__RISK__UNIT>
								<xsl:value-of select="string('      ')"/>
							</PMDUXB1__RISK__UNIT>
						</PMDUXB1__LEVEL__KEY>
						<PMDUXB1__ITEM__EFFECTIVE__DATE>
							<PMDUXB1__YEAR__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,1,4)"/>
							</PMDUXB1__YEAR__ITEM__EFFECTIVE>
							<PMDUXB1__MONTH__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,6,2)"/>
							</PMDUXB1__MONTH__ITEM__EFFECTIVE>
							<PMDUXB1__DAY__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</PMDUXB1__DAY__ITEM__EFFECTIVE>
						</PMDUXB1__ITEM__EFFECTIVE__DATE>
						<PMDUXB1__VARIABLE__KEY>
							<fill_0>
								<xsl:value-of select="string('      ')"/>
							</fill_0>
						</PMDUXB1__VARIABLE__KEY>
						<PMDUXB1__PROCESS__DATE>
							<PMDUXB1__YEAR__PROCESS>
								<xsl:value-of select="string('0000')"/>
							</PMDUXB1__YEAR__PROCESS>
							<PMDUXB1__MONTH__PROCESS>
								<xsl:value-of select="string('00')"/>
							</PMDUXB1__MONTH__PROCESS>
							<PMDUXB1__DAY__PROCESS>
								<xsl:value-of select="string('00')"/>
							</PMDUXB1__DAY__PROCESS>
						</PMDUXB1__PROCESS__DATE>
					</PMDUXB1__SEGMENT__KEY>
					<PMDUXB1__SEGMENT__DATA>
						<PMDUXB1__TEXAS__IND>
							<xsl:value-of select="string('  ')"/>
						</PMDUXB1__TEXAS__IND>
						<PMDUXB1__COV__ENDORE>
							<xsl:call-template name="ConvertCoverageEndorse">
								<xsl:with-param name="Value" select="CoverageCd"/>
							</xsl:call-template>
						</PMDUXB1__COV__ENDORE>
						<PMDUXB1__COMMON__DATA>
							<PMDUXB1__LIMITA__A>
								<PMDUXB1__LIMITA>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__LIMITA>
							</PMDUXB1__LIMITA__A>
							<PMDUXB1__AF__LIMITA__A>
								<PMDUXB1__AF__LIMITA>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__AF__LIMITA>
							</PMDUXB1__AF__LIMITA__A>
							<PMDUXB1__DEDUCTIBLE__A>
								<PMDUXB1__DEDUCTIBLE>
									<xsl:value-of select="../../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt"/>
								</PMDUXB1__DEDUCTIBLE>
							</PMDUXB1__DEDUCTIBLE__A>
						</PMDUXB1__COMMON__DATA>
						<PMDUXB1__GLASS__SQ__FT__A>
							<xsl:choose>
								<xsl:when test="CoverageCd='EGLASS'">
									<xsl:variable name="tCovCd" select="CoverageCd"/>
									<xsl:choose>
										<xsl:when test="normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits)!='' and normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='LO']/PlateSizeArea/NumUnits)!=''">
											<PMDUXB1__GLASS__SQ__FT>
												<xsl:value-of select="number(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits)+number(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='LO']/PlateSizeArea/NumUnits)"/>
											</PMDUXB1__GLASS__SQ__FT>
										</xsl:when>
										<xsl:when test="normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits)!=''">
											<PMDUXB1__GLASS__SQ__FT>
												<xsl:value-of select="../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits"/>
											</PMDUXB1__GLASS__SQ__FT>
										</xsl:when>
										<xsl:when test="normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='LO']/PlateSizeArea/NumUnits)!=''">
											<PMDUXB1__GLASS__SQ__FT>
												<xsl:value-of select="../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$tCovCd and GlassPositionAndUseInBldgCd='LO']/PlateSizeArea/NumUnits"/>
											</PMDUXB1__GLASS__SQ__FT>
										</xsl:when>
										<xsl:otherwise>
											<PMDUXB1__GLASS__SQ__FT__X>
												<xsl:value-of select="string('      ')"/>
											</PMDUXB1__GLASS__SQ__FT__X>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<PMDUXB1__GLASS__SQ__FT__X>
										<xsl:value-of select="string('      ')"/>
									</PMDUXB1__GLASS__SQ__FT__X>
								</xsl:otherwise>
							</xsl:choose>
						</PMDUXB1__GLASS__SQ__FT__A>
						<PMDUXB1__MONEY__SEC__DATA>
							<PMDUXB1__MONEY__SEC__LIMITB__A>
								<PMDUXB1__MONEY__SEC__LIMITB>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__MONEY__SEC__LIMITB>
							</PMDUXB1__MONEY__SEC__LIMITB__A>
						</PMDUXB1__MONEY__SEC__DATA>
						<PMDUXB1__EMPL__DISH__DATA>
							<PMDUXB1__NUMB__EMPLS__A>
								<xsl:choose>
									<xsl:when test="CoverageCd='PRFCMT' and normalize-space(Option[OptionCd='com.csc_NumPerson']/OptionValue)!=''">
										<PMDUXB1__NUMB__EMPLS>
											<xsl:value-of select="Option[OptionCd='com.csc_NumPerson']/OptionValue"/>
										</PMDUXB1__NUMB__EMPLS>
									</xsl:when>
									<xsl:when test="CoverageCd='EMPDIS' and normalize-space(Option[OptionCd='com.csc_NumEmp']/OptionValue)!=''">
										<PMDUXB1__NUMB__EMPLS>
											<xsl:value-of select="Option[OptionCd='com.csc_NumEmp']/OptionValue"/>
										</PMDUXB1__NUMB__EMPLS>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__NUMB__EMPLS__X>
											<xsl:value-of select="string('     ')"/>
										</PMDUXB1__NUMB__EMPLS__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__NUMB__EMPLS__A>
						</PMDUXB1__EMPL__DISH__DATA>
						<PMDUXB1__MECH__BREAK__DATA>
							<PMDUXB1__LOCATION__OF__VESSELS>
								<PMDUXB1__LOC__VESSELS>
									<xsl:choose>
										<xsl:when test="CoverageCd='BOILER'">
											<xsl:choose>
												<xsl:when test="Option[OptionCd='com.csc_BoilerPremisesInd']/OptionValue='Y'">
													<xsl:value-of select="1"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="2"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:when test="CoverageCd='MECH'">
											<xsl:choose>
												<xsl:when test="Option[OptionCd='com.csc_MECH_ACPremisesInd']/OptionValue='Y'">
													<xsl:value-of select="1"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="2"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="string(' ')"/>
										</xsl:otherwise>
									</xsl:choose>
								</PMDUXB1__LOC__VESSELS>
							</PMDUXB1__LOCATION__OF__VESSELS>
							<PMDUXB1__AIR__CONDITIONING__EQUIP>
								<PMDUXB1__AIR__COND__EQUIP>
									<xsl:choose>
										<xsl:when test="CoverageCd='MECH'">
											<xsl:value-of select="Option[OptionCd='com.csc_ACCoverageInd']/OptionValue"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="string(' ')"/>
										</xsl:otherwise>
									</xsl:choose>
								</PMDUXB1__AIR__COND__EQUIP>
							</PMDUXB1__AIR__CONDITIONING__EQUIP>
						</PMDUXB1__MECH__BREAK__DATA>
						<PMDUXB1__SW__POOL__NAI__A>
							<xsl:choose>
								<xsl:when test="CoverageCd='EMPDIS'">
									<PMDUXB1__SW__POOL__NAI>
										<xsl:value-of select="Option[OptionCd='com.csc_NumLoc']/OptionValue"/>
									</PMDUXB1__SW__POOL__NAI>
								</xsl:when>
								<xsl:when test="CoverageCd='ADDMGR'">
									<PMDUXB1__SW__POOL__NAI>
										<xsl:value-of select="Option[OptionCd='com.csc_NumAddlInsured']/OptionValue"/>
									</PMDUXB1__SW__POOL__NAI>
								</xsl:when>
								<xsl:otherwise>
									<PMDUXB1__SW__POOL__NAI__X>
										<xsl:value-of select="string('     ')"/>
									</PMDUXB1__SW__POOL__NAI__X>
								</xsl:otherwise>
							</xsl:choose>
						</PMDUXB1__SW__POOL__NAI__A>
						<PMDUXB1__CO__DEV__A>
							<PMDUXB1__CO__DEV>
								<xsl:value-of select="string('000000')"/>
							</PMDUXB1__CO__DEV>
						</PMDUXB1__CO__DEV__A>
						<PMDUXB1__OVERRIDE__IND>
							<xsl:value-of select="CurrentTermAmt/com.csc_OverrideInd"/>
						</PMDUXB1__OVERRIDE__IND>
						<PMDUXB1__DEC__CHANGE__FLAG>
							<xsl:value-of select="string(' ')"/>
						</PMDUXB1__DEC__CHANGE__FLAG>
						<PMDUXB1__SPOIL__COV__DATA>
							<PMDUXB1__TYPE__LOSS__A>
								<xsl:choose>
									<xsl:when test="CoverageCd='SPOIL'">
										<xsl:choose>
											<xsl:when test="Option[OptionCd='com.csc_TypeCoverageInd']/OptionValue='B'">
												<PMDUXB1__TYPE__LOSS>
													<xsl:value-of select="1"/>
												</PMDUXB1__TYPE__LOSS>
											</xsl:when>
											<xsl:when test="Option[OptionCd='com.csc_TypeCoverageInd']/OptionValue='P'">
												<PMDUXB1__TYPE__LOSS>
													<xsl:value-of select="2"/>
												</PMDUXB1__TYPE__LOSS>
											</xsl:when>
											<xsl:otherwise>
												<PMDUXB1__TYPE__LOSS>
													<xsl:value-of select="3"/>
												</PMDUXB1__TYPE__LOSS>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__TYPE__LOSS__X>
											<xsl:value-of select="string(' ')"/>
										</PMDUXB1__TYPE__LOSS__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__TYPE__LOSS__A>
							<PMDUXB1__REF__MAIN>
								<xsl:choose>
									<xsl:when test="CoverageCd='SPOIL'">
										<xsl:value-of select="Option[OptionCd='com.csc_MaintAgreementInd']/OptionValue"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="string(' ')"/>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__REF__MAIN>
						</PMDUXB1__SPOIL__COV__DATA>
						<PMDUXB1__DED__CDE>
							<xsl:value-of select="string('   ')"/>
						</PMDUXB1__DED__CDE>
						<PMDUXB1__OTHER__DEDUCT__A>
							<PMDUXB1__OTHER__DEDUCT__X>
								<xsl:value-of select="string('       ')"/>
							</PMDUXB1__OTHER__DEDUCT__X>
						</PMDUXB1__OTHER__DEDUCT__A>
						<PMDUXB1__COMMON__DATA__NEW>
							<PMDUXB1__LIMITA__EXP__A>
								<xsl:choose>
									<!--<xsl:when test="CoverageCd='CONDO' or CoverageCd='PRFCMT' or CoverageCd='ACCTS' or CoverageCd='LOSS' or CoverageCd='MECH' or CoverageCd='SIGNS' or CoverageCd='VALPAP' or CoverageCd='EMPDIS' or CoverageCd='HIRED' or CoverageCd='NONOWN' or CoverageCd='CMPTR' or CoverageCd='MONEY' or CoverageCd='SPOIL' or CoverageCd='BURG'">-->
									<xsl:when test="CoverageCd='CONDO' or CoverageCd='PRFCMT' or CoverageCd='ACCTS' or CoverageCd='LOSS' or CoverageCd='MECH' or CoverageCd='SIGNS' or CoverageCd='VALPAP' or CoverageCd='EMPDIS' or CoverageCd='CMPTR' or CoverageCd='MONEY' or CoverageCd='SPOIL' ">
										<xsl:choose>
											<xsl:when test="normalize-space(Limit/FormatCurrencyAmt/Amt)!=''">
												<PMDUXB1__LIMITA__EXP>
													<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
												</PMDUXB1__LIMITA__EXP>
											</xsl:when>
											<xsl:otherwise>
												<PMDUXB1__LIMITA__EXP__X>
													<xsl:value-of select="string('         ')"/>
												</PMDUXB1__LIMITA__EXP__X>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="CoverageCd='EGLASS' or CoverageCd='IGLASS'">
										<xsl:variable name="CovCd" select="CoverageCd"/>
										<xsl:choose>
											<xsl:when test="normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$CovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits)!=''">
												<PMDUXB1__LIMITA__EXP>
													<xsl:value-of select="../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$CovCd and GlassPositionAndUseInBldgCd='UP']/PlateSizeArea/NumUnits"/>
												</PMDUXB1__LIMITA__EXP>
											</xsl:when>
											<xsl:otherwise>
												<PMDUXB1__LIMITA__EXP__X>
													<xsl:value-of select="string('         ')"/>
												</PMDUXB1__LIMITA__EXP__X>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__LIMITA__EXP__X>
											<xsl:value-of select="string('         ')"/>
										</PMDUXB1__LIMITA__EXP__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__LIMITA__EXP__A>
							<PMDUXB1__DEDUCTIBLE__EXP__A>
								<xsl:choose>
									<xsl:when test="normalize-space(../../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt)!=''">
										<PMDUXB1__DEDUCTIBLE__EXP>
											<xsl:value-of select="../../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt"/>
										</PMDUXB1__DEDUCTIBLE__EXP>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__DEDUCTIBLE__EXP__X>
											<xsl:value-of select="string('      ')"/>
										</PMDUXB1__DEDUCTIBLE__EXP__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__DEDUCTIBLE__EXP__A>
						</PMDUXB1__COMMON__DATA__NEW>
						<PMDUXB1__MONEY__SEC__DATA__NEW>
							<PMDUXB1__MONEY__SEC__LIMITB__EXP__A>
								<xsl:choose>
									<xsl:when test="CoverageCd='MONEY'">
										<PMDUXB1__MONEY__SEC__LIMITB__EXP>
											<xsl:value-of select="Option[OptionCd='com.csc_MoneySecondLimit']/OptionValue"/>
										</PMDUXB1__MONEY__SEC__LIMITB__EXP>
									</xsl:when>
									<xsl:when test="CoverageCd='CMPTR'">
										<PMDUXB1__MONEY__SEC__LIMITB__EXP>
											<xsl:value-of select="Option[OptionCd='com.csc_DataMedia']/OptionValue"/>
										</PMDUXB1__MONEY__SEC__LIMITB__EXP>
									</xsl:when>
									<xsl:when test="CoverageCd='EGLASS' or CoverageCd='IGLASS'">
										<xsl:variable name="CovCd2" select="CoverageCd"/>
										<PMDUXB1__MONEY__SEC__LIMITB__EXP>
											<xsl:value-of select="../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd=$CovCd2 and GlassPositionAndUseInBldgCd='LO']/PlateSizeArea/NumUnits"/>
										</PMDUXB1__MONEY__SEC__LIMITB__EXP>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__MONEY__SEC__LIMITB__EXP__X>
											<xsl:value-of select="string('         ')"/>
										</PMDUXB1__MONEY__SEC__LIMITB__EXP__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__MONEY__SEC__LIMITB__EXP__A>
						</PMDUXB1__MONEY__SEC__DATA__NEW>
						<PMDUXB1__IRREG__SHAPE__GLS__A>
							<xsl:choose>
								<xsl:when test="CoverageCd='IGLASS' and normalize-space(../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd='IGLASS' and GlassPositionAndUseInBldgCd='IR']/PlateSizeArea/NumUnits)!=''">
									<PMDUXB1__IRREG__SHAPE__GLS>
										<xsl:value-of select="../../GlassSignInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/GlassSignSchedule[ScheduleTypeCd='IGLASS' and GlassPositionAndUseInBldgCd='IR']/PlateSizeArea/NumUnits"/>
									</PMDUXB1__IRREG__SHAPE__GLS>
								</xsl:when>
								<xsl:otherwise>
									<PMDUXB1__IRREG__SHAPE__GLS__X>
										<xsl:value-of select="string('      ')"/>
									</PMDUXB1__IRREG__SHAPE__GLS__X>
								</xsl:otherwise>
							</xsl:choose>
						</PMDUXB1__IRREG__SHAPE__GLS__A>
						<PMDUXB1__PMS__FUTURE__USE>
							<xsl:value-of select="string('                                                                                                                                                                ')"/>
						</PMDUXB1__PMS__FUTURE__USE>
						<PMDUXB1__CUST__FUTURE__USE>
							<xsl:value-of select="string('                              ')"/>
						</PMDUXB1__CUST__FUTURE__USE>
						<PMDUXB1__YR2000__CUST__USE>
							<xsl:value-of select="string('                                                                                                    ')"/>
						</PMDUXB1__YR2000__CUST__USE>
					</PMDUXB1__SEGMENT__DATA>
				</PMDUXB1__UNIT__GRP__RATING__SEG>
			</BUS__OBJ__RECORD>
			<!-- Optional Coverage Premium Segment -->
			<xsl:if test="CurrentTermAmt/com.csc_OverrideInd='Y'">
				<xsl:call-template name="S2CovPremTemplate01">
					<xsl:with-param name="RefLocId" select="$RefLocId"/>
					<xsl:with-param name="RefSubLocId" select="$RefSubLocId"/>
					<xsl:with-param name="LocId" select="$LocId"/>
					<xsl:with-param name="SubLocId" select="$SubLocId"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="S2CoverageTemplate02" match="/">
		<!-- For Building & Property Coverages -->
		<xsl:param name="RefLocId"/>
		<xsl:param name="RefSubLocId"/>
		<xsl:param name="LocId"/>
		<xsl:param name="SubLocId"/>

		<xsl:for-each select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId][SubjectInsuranceCd='BLDG' or SubjectInsuranceCd='BPP']">
			<BUS__OBJ__RECORD>
				<PMDUXB1__UNIT__GRP__RATING__SEG>
					<PMDUXB1__SEGMENT__KEY>
						<PMDUXB1__REC__LLBB/>
						<PMDUXB1__SEGMENT__ID>43</PMDUXB1__SEGMENT__ID>
						<PMDUXB1__SEGMENT__STATUS>A</PMDUXB1__SEGMENT__STATUS>
						<PMDUXB1__TRANSACTION__DATE>
							<PMDUXB1__YEAR__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,1,4)"/>
							</PMDUXB1__YEAR__TRANSACTION>
							<PMDUXB1__MONTH__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,6,2)"/>
							</PMDUXB1__MONTH__TRANSACTION>
							<PMDUXB1__DAY__TRANSACTION>
								<xsl:value-of select="substring($TransEffDt,9,2)"/>
							</PMDUXB1__DAY__TRANSACTION>
						</PMDUXB1__TRANSACTION__DATE>
						<PMDUXB1__SEGMENT__ID__KEY>
							<PMDUXB1__SEGMENT__LEVEL__CODE>U</PMDUXB1__SEGMENT__LEVEL__CODE>
							<PMDUXB1__SEGMENT__PART__CODE>X</PMDUXB1__SEGMENT__PART__CODE>
							<PMDUXB1__SUB__PART__CODE>1</PMDUXB1__SUB__PART__CODE>
							<PMDUXB1__INSURANCE__LINE>BP</PMDUXB1__INSURANCE__LINE>
						</PMDUXB1__SEGMENT__ID__KEY>
						<PMDUXB1__LEVEL__KEY>
							<PMDUXB1__LOCATION__NUMBER__A>
								<PMDUXB1__LOCATION__NUMBER>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">SITE</xsl:with-param>
										<xsl:with-param name="FieldLength">4</xsl:with-param>
										<xsl:with-param name="Value" select="$LocId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</PMDUXB1__LOCATION__NUMBER>
							</PMDUXB1__LOCATION__NUMBER__A>
							<PMDUXB1__SUB__LOCATION__NUMBER__A>
								<PMDUXB1__SUB__LOCATION__NUMBER>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">SITE</xsl:with-param>
										<xsl:with-param name="FieldLength">3</xsl:with-param>
										<xsl:with-param name="Value" select="$SubLocId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</PMDUXB1__SUB__LOCATION__NUMBER>
							</PMDUXB1__SUB__LOCATION__NUMBER__A>
							<PMDUXB1__RISK__UNIT__GROUP__KEY>
								<PMDUXB1__PMS__DEF__SUBJ__OF__INS>
									<PMDUXB1__SUBJ__OF__INS>
										<xsl:call-template name="ConvertSubjectOfInsToNumeric">
											<xsl:with-param name="Value" select="SubjectInsuranceCd"/>
										</xsl:call-template>
									</PMDUXB1__SUBJ__OF__INS>
								</PMDUXB1__PMS__DEF__SUBJ__OF__INS>
								<PMDUXB1__SEQUENCE__RISK__UNIT>
									<xsl:value-of select="string('   ')"/>
								</PMDUXB1__SEQUENCE__RISK__UNIT>
							</PMDUXB1__RISK__UNIT__GROUP__KEY>
							<PMDUXB1__RISK__UNIT>
								<xsl:value-of select="string('      ')"/>
							</PMDUXB1__RISK__UNIT>
						</PMDUXB1__LEVEL__KEY>
						<PMDUXB1__ITEM__EFFECTIVE__DATE>
							<PMDUXB1__YEAR__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,1,4)"/>
							</PMDUXB1__YEAR__ITEM__EFFECTIVE>
							<PMDUXB1__MONTH__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,6,2)"/>
							</PMDUXB1__MONTH__ITEM__EFFECTIVE>
							<PMDUXB1__DAY__ITEM__EFFECTIVE>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</PMDUXB1__DAY__ITEM__EFFECTIVE>
						</PMDUXB1__ITEM__EFFECTIVE__DATE>
						<PMDUXB1__VARIABLE__KEY>
							<fill_0>
								<xsl:value-of select="string('      ')"/>
							</fill_0>
						</PMDUXB1__VARIABLE__KEY>
						<PMDUXB1__PROCESS__DATE>
							<PMDUXB1__YEAR__PROCESS>
								<xsl:value-of select="string('0000')"/>
							</PMDUXB1__YEAR__PROCESS>
							<PMDUXB1__MONTH__PROCESS>
								<xsl:value-of select="string('00')"/>
							</PMDUXB1__MONTH__PROCESS>
							<PMDUXB1__DAY__PROCESS>
								<xsl:value-of select="string('00')"/>
							</PMDUXB1__DAY__PROCESS>
						</PMDUXB1__PROCESS__DATE>
					</PMDUXB1__SEGMENT__KEY>
					<PMDUXB1__SEGMENT__DATA>
						<PMDUXB1__TEXAS__IND>
							<xsl:value-of select="string('  ')"/>
						</PMDUXB1__TEXAS__IND>
						<PMDUXB1__COV__ENDORE>
							<xsl:call-template name="ConvertCoverageEndorse">
								<xsl:with-param name="Value" select="SubjectInsuranceCd"/>
							</xsl:call-template>
						</PMDUXB1__COV__ENDORE>
						<PMDUXB1__COMMON__DATA>
							<PMDUXB1__LIMITA__A>
								<PMDUXB1__LIMITA>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__LIMITA>
							</PMDUXB1__LIMITA__A>
							<PMDUXB1__AF__LIMITA__A>
								<PMDUXB1__AF__LIMITA>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__AF__LIMITA>
							</PMDUXB1__AF__LIMITA__A>
							<PMDUXB1__DEDUCTIBLE__A>
								<PMDUXB1__DEDUCTIBLE>
									<xsl:value-of select="../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt"/>
								</PMDUXB1__DEDUCTIBLE>
							</PMDUXB1__DEDUCTIBLE__A>
						</PMDUXB1__COMMON__DATA>
						<PMDUXB1__GLASS__SQ__FT__A>
							<PMDUXB1__GLASS__SQ__FT>
								<xsl:value-of select="string('      ')"/>
							</PMDUXB1__GLASS__SQ__FT>
						</PMDUXB1__GLASS__SQ__FT__A>
						<PMDUXB1__MONEY__SEC__DATA>
							<PMDUXB1__MONEY__SEC__LIMITB__A>
								<PMDUXB1__MONEY__SEC__LIMITB>
									<xsl:value-of select="string('000000000')"/>
								</PMDUXB1__MONEY__SEC__LIMITB>
							</PMDUXB1__MONEY__SEC__LIMITB__A>
						</PMDUXB1__MONEY__SEC__DATA>
						<PMDUXB1__EMPL__DISH__DATA>
							<PMDUXB1__NUMB__EMPLS__A>
								<PMDUXB1__NUMB__EMPLS__X>
									<xsl:value-of select="string('     ')"/>
								</PMDUXB1__NUMB__EMPLS__X>
							</PMDUXB1__NUMB__EMPLS__A>
						</PMDUXB1__EMPL__DISH__DATA>
						<PMDUXB1__MECH__BREAK__DATA>
							<PMDUXB1__LOCATION__OF__VESSELS>
								<PMDUXB1__LOC__VESSELS>
									<xsl:value-of select="string(' ')"/>
								</PMDUXB1__LOC__VESSELS>
							</PMDUXB1__LOCATION__OF__VESSELS>
							<PMDUXB1__AIR__CONDITIONING__EQUIP>
								<PMDUXB1__AIR__COND__EQUIP>
									<xsl:value-of select="string(' ')"/>
								</PMDUXB1__AIR__COND__EQUIP>
							</PMDUXB1__AIR__CONDITIONING__EQUIP>
						</PMDUXB1__MECH__BREAK__DATA>
						<PMDUXB1__SW__POOL__NAI__A>
							<PMDUXB1__SW__POOL__NAI__X>
								<xsl:value-of select="string('     ')"/>
							</PMDUXB1__SW__POOL__NAI__X>
						</PMDUXB1__SW__POOL__NAI__A>
						<PMDUXB1__CO__DEV__A>
							<PMDUXB1__CO__DEV>
								<xsl:value-of select="string('000000')"/>
							</PMDUXB1__CO__DEV>
						</PMDUXB1__CO__DEV__A>
						<PMDUXB1__OVERRIDE__IND>
							<xsl:value-of select="CommlCoverage[normalize-space(CoverageCd)='']/CurrentTermAmt/com.csc_OverrideInd"/>
						</PMDUXB1__OVERRIDE__IND>
						<PMDUXB1__DEC__CHANGE__FLAG>
							<xsl:value-of select="string(' ')"/>
						</PMDUXB1__DEC__CHANGE__FLAG>
						<PMDUXB1__SPOIL__COV__DATA>
							<PMDUXB1__TYPE__LOSS__A>
								<PMDUXB1__TYPE__LOSS__X>
									<xsl:value-of select="string(' ')"/>
								</PMDUXB1__TYPE__LOSS__X>
							</PMDUXB1__TYPE__LOSS__A>
							<PMDUXB1__REF__MAIN>
								<xsl:value-of select="string(' ')"/>
							</PMDUXB1__REF__MAIN>
						</PMDUXB1__SPOIL__COV__DATA>
						<PMDUXB1__DED__CDE>
							<xsl:value-of select="string('   ')"/>
						</PMDUXB1__DED__CDE>
						<PMDUXB1__OTHER__DEDUCT__A>
							<PMDUXB1__OTHER__DEDUCT__X>
								<xsl:value-of select="string('       ')"/>
							</PMDUXB1__OTHER__DEDUCT__X>
						</PMDUXB1__OTHER__DEDUCT__A>
						<PMDUXB1__COMMON__DATA__NEW>
							<PMDUXB1__LIMITA__EXP__A>
								<xsl:choose>
									<xsl:when test="normalize-space(CommlCoverage[normalize-space(CoverageCd)='']/Limit/FormatCurrencyAmt/Amt)!=''">
										<PMDUXB1__LIMITA__EXP>
											<xsl:value-of select="CommlCoverage[normalize-space(CoverageCd)='']/Limit/FormatCurrencyAmt/Amt"/>
										</PMDUXB1__LIMITA__EXP>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__LIMITA__EXP__X>
											<xsl:value-of select="string('         ')"/>
										</PMDUXB1__LIMITA__EXP__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__LIMITA__EXP__A>
							<PMDUXB1__DEDUCTIBLE__EXP__A>
								<xsl:choose>
									<xsl:when test="normalize-space(../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt)!=''">
										<PMDUXB1__DEDUCTIBLE__EXP>
											<xsl:value-of select="../CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd='BPP']/CommlCoverage[normalize-space(CoverageCd)='']/Deductible/FormatCurrencyAmt/Amt"/>
										</PMDUXB1__DEDUCTIBLE__EXP>
									</xsl:when>
									<xsl:otherwise>
										<PMDUXB1__DEDUCTIBLE__EXP__X>
											<xsl:value-of select="string('      ')"/>
										</PMDUXB1__DEDUCTIBLE__EXP__X>
									</xsl:otherwise>
								</xsl:choose>
							</PMDUXB1__DEDUCTIBLE__EXP__A>
						</PMDUXB1__COMMON__DATA__NEW>
						<PMDUXB1__MONEY__SEC__DATA__NEW>
							<PMDUXB1__MONEY__SEC__LIMITB__EXP__A>
								<PMDUXB1__MONEY__SEC__LIMITB__EXP__X>
									<xsl:value-of select="string('         ')"/>
								</PMDUXB1__MONEY__SEC__LIMITB__EXP__X>
							</PMDUXB1__MONEY__SEC__LIMITB__EXP__A>
						</PMDUXB1__MONEY__SEC__DATA__NEW>
						<PMDUXB1__IRREG__SHAPE__GLS__A>
							<PMDUXB1__IRREG__SHAPE__GLS__X>
								<xsl:value-of select="string('      ')"/>
							</PMDUXB1__IRREG__SHAPE__GLS__X>
						</PMDUXB1__IRREG__SHAPE__GLS__A>
						<PMDUXB1__PMS__FUTURE__USE>
							<xsl:value-of select="string('                                                                                                                                                                ')"/>
						</PMDUXB1__PMS__FUTURE__USE>
						<PMDUXB1__CUST__FUTURE__USE>
							<xsl:value-of select="string('                              ')"/>
						</PMDUXB1__CUST__FUTURE__USE>
						<PMDUXB1__YR2000__CUST__USE>
							<xsl:value-of select="string('                                                                                                    ')"/>
						</PMDUXB1__YR2000__CUST__USE>
					</PMDUXB1__SEGMENT__DATA>
				</PMDUXB1__UNIT__GRP__RATING__SEG>
			</BUS__OBJ__RECORD>
			<!-- Building and Property Coverage Premium Segment -->
			<xsl:if test="normalize-space(CommlCoverage/CoverageCd)='' and CommlCoverage/CurrentTermAmt/com.csc_OverrideInd='Y'">
				<xsl:call-template name="S2CovPremTemplate02">
					<xsl:with-param name="RefLocId" select="$RefLocId"/>
					<xsl:with-param name="RefSubLocId" select="$RefSubLocId"/>
					<xsl:with-param name="LocId" select="$LocId"/>
					<xsl:with-param name="SubLocId" select="$SubLocId"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="BORQ_S2_ACORD.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->