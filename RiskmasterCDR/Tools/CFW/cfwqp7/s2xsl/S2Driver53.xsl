<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template name="S2Driver53Template">
		<xsl:variable name="attribString"/>

		<xsl:for-each select="//PersAutoLineBusiness/PersDriver">
			<BUS__OBJ__RECORD>
				<RECORD__NAME__ROW>
					<RECORD__NAME>DRIVER__SEG</RECORD__NAME>
				</RECORD__NAME__ROW>
				<DRIVER__SEG>
					<DRIVER__REC__LLBB>
						<DIRVER__REC__LENGTH/>
						<DRIVER__ACTION__CODE/>
						<DRIVER__FILE__ID/>
					</DRIVER__REC__LLBB>
					<DRIVER__KEY>25</DRIVER__KEY>

					<xsl:variable name="DriverID" select="concat('0',substring-after(@id, 'd'))"/>
					<DRIVER__ID>
						<xsl:value-of select="substring($DriverID, string-length($DriverID) - 1, 2)"/>
					</DRIVER__ID>
					<DRIVER__NAME>
						<xsl:value-of select="concat(GeneralPartyInfo/NameInfo/PersonName/GivenName,' ',GeneralPartyInfo/NameInfo/PersonName/Surname)"/>
					</DRIVER__NAME>
					<DRIVER__LICENSE__NUMBER>
						<xsl:value-of select="substring(DriverInfo/DriversLicense/DriversLicenseNumber,1,19)"/>
					</DRIVER__LICENSE__NUMBER>
					<DRIVER__DATE__BIRTH>
						<DOB__YEAR>
							<xsl:value-of select="substring(DriverInfo/PersonInfo/BirthDt,1,4)"/>
						</DOB__YEAR>
						<DOB__MONTH>
							<xsl:value-of select="substring(DriverInfo/PersonInfo/BirthDt,6,2)"/>
						</DOB__MONTH>
						<DOB__DAY>
							<xsl:value-of select="substring(DriverInfo/PersonInfo/BirthDt,9,2)"/>
						</DOB__DAY>
					</DRIVER__DATE__BIRTH>

					<DRIVER__LICENSE__STATE>
						<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
							<xsl:with-param name="Value" select="DriverInfo/DriversLicense/StateProvCd"/>
						</xsl:call-template>
					</DRIVER__LICENSE__STATE>
					<DRIVER__SEX>
						<xsl:value-of select="DriverInfo/PersonInfo/GenderCd"/>
					</DRIVER__SEX>
					<DRIVER__MARITAL__STATUS>
						<xsl:value-of select="DriverInfo/PersonInfo/MaritalStatusCd"/>
					</DRIVER__MARITAL__STATUS>
					<DRIVER__RELATION__TO__INDS>
						<xsl:choose>
							<xsl:when test="PersDriverInfo/DriverRelationshipToApplicantCd='CH'">C</xsl:when>
							<xsl:when test="PersDriverInfo/DriverRelationshipToApplicantCd='IN'">I</xsl:when>
							<xsl:when test="PersDriverInfo/DriverRelationshipToApplicantCd='OT'">O</xsl:when>
							<xsl:when test="PersDriverInfo/DriverRelationshipToApplicantCd='RE'">R</xsl:when>
							<xsl:when test="PersDriverInfo/DriverRelationshipToApplicantCd='SP'">S</xsl:when>
							<xsl:when test="normalize-space(PersDriverInfo/DriverRelationshipToApplicantCd)='C' or 'I' or 'O' or 'R' or 'S'">
								<xsl:value-of select="normalize-space(PersDriverInfo/DriverRelationshipToApplicantCd)"/>
							</xsl:when>
							<xsl:otherwise>I</xsl:otherwise>
						</xsl:choose>
					</DRIVER__RELATION__TO__INDS>
					<xsl:variable name="DriverNbr" select="substring($DriverID, string-length($DriverID), 1)"/>
					<DRIVER__PRIN__OPERATOR>
						<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and (UsePct &gt; '50')]">
							<PO__VEHICLE__NO>
							  <xsl:attribute name="index"><xsl:value-of select="position() - 1"/></xsl:attribute>
								<xsl:value-of select="substring(@VehRef,string-length(@VehRef),1)"/>
							</PO__VEHICLE__NO>
						</xsl:for-each>
					</DRIVER__PRIN__OPERATOR>
					<DRIVER__PT__OPERATOR>

						<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/DriverVeh[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr) and (UsePct &lt; '51')]">
							<PT__VEHICLE__NO>
                            <xsl:attribute name="index"><xsl:value-of select="position() - 1"/></xsl:attribute>
								<xsl:value-of select="substring(@VehRef,string-length(@VehRef),1)"/>
							</PT__VEHICLE__NO>
						</xsl:for-each>
					</DRIVER__PT__OPERATOR>
					<DRIVER__AGE/>
					<DRIVER__SPUSE/>
					<DRIVER__CF__ATTRIBUTE/>
					<DRIVER__ATTRIBUTE__AREA>
						<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersPolicy/AccidentViolation[(substring(@DriverRef,2,string-length(@DriverRef)-1)=$DriverNbr)]">
							<DRIV__ATTR>
								<DRIV__ATTR__VALUE>
									<xsl:value-of select="AccidentViolationCd"/>
								</DRIV__ATTR__VALUE>
							</DRIV__ATTR>
							<DRIVER__ATTRB__DATE>
								<ATTRIBUTE__DATE>
									<ATTRIBUTE__YEAR>
										<xsl:value-of select="substring(AccidentViolationDt,1,4)"/>
									</ATTRIBUTE__YEAR>
									<ATTRIBUTE__MONTH>
										<xsl:value-of select="substring(AccidentViolationDt,6,2)"/>
									</ATTRIBUTE__MONTH>
									<ATTRIBUTE__DAY>
										<xsl:value-of select="substring(AccidentViolationDt,9,2)"/>
									</ATTRIBUTE__DAY>
								</ATTRIBUTE__DATE>
							</DRIVER__ATTRB__DATE>
						</xsl:for-each>
					</DRIVER__ATTRIBUTE__AREA>
					<DRIVER__AAA__IND/>
					<DRIVER__LICENSE__DATE>
						<DLD__YEAR>
							<xsl:value-of select="substring(DriverInfo/DriversLicense/LicensedDt,1,4)"/>
						</DLD__YEAR>
						<DLD__MONTH>
							<xsl:value-of select="substring(DriverInfo/DriversLicense/LicensedDt,6,2)"/>
						</DLD__MONTH>
						<DLD__DAY>
							<xsl:value-of select="substring(DriverInfo/DriversLicense/LicensedDt,9,2)"/>
						</DLD__DAY>
					</DRIVER__LICENSE__DATE>
					<DRIVER__NUM__YEARS__LICENSED/>
					<DRIVER__MAJOR__CONVICTIONS__X>
						<DRIVER__MAJOR__CONVICTIONS/>
					</DRIVER__MAJOR__CONVICTIONS__X>
					<DRIVER__MINOR__CONVICTIONS_X>
						<DRIVER__MINOR__CONVICTIONS/>
					</DRIVER__MINOR__CONVICTIONS_X>
					<DRIVER__BI__ACCIDENTS_X>
						<DRIVER__BI__ACCIDENTS/>
					</DRIVER__BI__ACCIDENTS_X>
					<DRIVER__PD__ACCIDENTS_X>
						<DRIVER__PD__ACCIDENTS/>
					</DRIVER__PD__ACCIDENTS_X>
					<DRIVER__PMS__FUTURE__USE/>
					<DRIVER__CUST__FUTURE__USE/>
					<DRIVER__YR2000__CUST__USE/>
					<DRIVER__DUP__KEY__SEQ__NUM/>
				</DRIVER__SEG>
			</BUS__OBJ__RECORD>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->