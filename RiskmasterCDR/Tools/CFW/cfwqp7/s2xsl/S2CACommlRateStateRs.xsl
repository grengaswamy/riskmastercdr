<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template name="CreateCommlRateState">
		<xsl:for-each select="/*/VEHICLE__DESCRIPTION__SEG[USVD__MAKE__DESC != 'DOC' and USVD__MAKE__DESC != 'HIRED AUTO' and USVD__MAKE__DESC != 'NON-OWNED']">
			<xsl:variable name="RateState">
				<xsl:value-of select="./USVD__STATE"/>
			</xsl:variable>
			<xsl:variable name="ThisUnit">
				<xsl:value-of select="./USVD__KEY/USVD__UNIT__NUMBER"/>
			</xsl:variable>
			<xsl:variable name="ProcessState">
				<xsl:choose>
						<!-- If there were no preceding vehicles for this state, then the state needs to be processed -->
					<xsl:when test="count(//VEHICLE__DESCRIPTION__SEG[USVD__STATE = $RateState and USVD__KEY/USVD__UNIT__NUMBER &lt; $ThisUnit]) = '0'">
						<xsl:value-of select="string('Y')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="string('N')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:if test="$ProcessState = 'Y'">
				<!-- First find state level coverages. For S2 these are the last vehcicle records. -->
			<CommlRateState>
				<StateProvCd>
					<xsl:call-template name="BuildStateProvCd">
						<xsl:with-param name="StateCd" select="./USVD__STATE"/>
					</xsl:call-template>
				</StateProvCd>

				<xsl:for-each select="//VEHICLE__DESCRIPTION__SEG[USVD__STATE = $RateState]">
					<xsl:variable name="VehNo">
						<xsl:choose>
							<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 2) = '00'">
								<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 3, 1)"/>
							</xsl:when>
							<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 1) = '0'">
								<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 2, 2)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="./USVD__KEY/USVD__UNIT__NUMBER"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:if test="USVD__MAKE__DESC = 'HIRED AUTO'">
						<xsl:variable name="CovPrem">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VEHICLE__COVERAGE__CONTROL/VEHICLE__TOTAL__PREMIUM"/>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="CovCd">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA[@index=0]/VCR__COVERAGE__CODE"/>
							</xsl:for-each>
						</xsl:variable>

						<CommlAutoHiredInfo>
							<HiredLiabilityCostAmt>
								<Amt>
									<xsl:value-of select="./USVD__STATED__AMOUNT"/>
								</Amt>
							</HiredLiabilityCostAmt>
							<CommlCoverage>
								<!-- CoverageCd><xsl:value-of select="USVD__VEH__CLASS__CODE"/></CoverageCd -->
								<CoverageCd><xsl:value-of select="concat(USVD__VEH__CLASS__CODE, $CovCd)"/></CoverageCd>
								<CoverageDesc>
									<xsl:value-of select="USVD__MAKE__DESC"/>
								</CoverageDesc>
								<Limit>
									<FormatCurrencyAmt>
										<Amt></Amt>
									</FormatCurrencyAmt>
								</Limit>
								<Deductible>
									<FormatCurrencyAmt>
										<Amt></Amt>
									</FormatCurrencyAmt>
									<DeductibleTypeCd>FL</DeductibleTypeCd>
								</Deductible>
								<CurrentTermAmt>
									<Amt><xsl:value-of select="$CovPrem"/></Amt>
								</CurrentTermAmt>
							</CommlCoverage>
						</CommlAutoHiredInfo>
					</xsl:if>

					<xsl:if test="USVD__MAKE__DESC = 'NON-OWNED'">
						<xsl:variable name="CovPrem">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VEHICLE__COVERAGE__CONTROL/VEHICLE__TOTAL__PREMIUM"/>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="CovCd">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA[@index=0]/VCR__COVERAGE__CODE"/>
							</xsl:for-each>
						</xsl:variable>

						<CommlAutoNonOwnedInfo>
							<NonOwnedInfo>
								<NonOwnedGroupTypeCd/>
								<NumNonOwned><xsl:value-of select="USVD__CUBIC__CM"/></NumNonOwned>
								<CommlCoverage>
								<CoverageCd><xsl:value-of select="concat(USVD__VEH__CLASS__CODE, $CovCd)"/></CoverageCd>
								<CoverageDesc>
									<xsl:value-of select="USVD__MAKE__DESC"/>
								</CoverageDesc>
									<Limit>
										<FormatCurrencyAmt>
											<Amt></Amt>
										</FormatCurrencyAmt>
									</Limit>
									<Deductible>
										<FormatCurrencyAmt>
											<Amt></Amt>
										</FormatCurrencyAmt>
										<DeductibleTypeCd>FL</DeductibleTypeCd>
									</Deductible>
									<CurrentTermAmt>
										<Amt><xsl:value-of select="$CovPrem"/></Amt>
									</CurrentTermAmt>
								</CommlCoverage>
							</NonOwnedInfo>
						</CommlAutoNonOwnedInfo>
					</xsl:if>

					<xsl:if test="USVD__MAKE__DESC = 'DOC'">
						<xsl:variable name="CovPrem">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VEHICLE__COVERAGE__CONTROL/VEHICLE__TOTAL__PREMIUM"/>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="CovCd">
							<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER = $VehNo]">
								<xsl:value-of select="SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA[@index=0]/VCR__COVERAGE__CODE"/>
							</xsl:for-each>
						</xsl:variable>

						<CommlAutoDriveOtherCarInfo>
							<NumIndividualsCovered><xsl:value-of select="USVD__SYMBOL"/></NumIndividualsCovered>
							<CommlCoverage>
								<CoverageCd><xsl:value-of select="concat(USVD__VEH__CLASS__CODE, $CovCd)"/></CoverageCd>
								<CoverageDesc>
									<xsl:value-of select="USVD__MAKE__DESC"/>
								</CoverageDesc>
								<Limit>
									<FormatCurrencyAmt>
										<Amt></Amt>
									</FormatCurrencyAmt>
								</Limit>
								<Deductible>
									<FormatCurrencyAmt>
										<Amt></Amt>
									</FormatCurrencyAmt>
									<DeductibleTypeCd>FL</DeductibleTypeCd>
								</Deductible>
								<Option>
									<OptionTypeCd>YNInd1</OptionTypeCd>
									<OptionCd/>
									<OptionValue>Y</OptionValue>
								</Option>
								<CurrentTermAmt>
									<Amt>
										<xsl:value-of select="$CovPrem"/>
									</Amt>
								</CurrentTermAmt>
							</CommlCoverage>
						</CommlAutoDriveOtherCarInfo>
					</xsl:if>
				</xsl:for-each>

				<!-- Next process each vehicle for this state -->
				<xsl:for-each select="/*/VEHICLE__DESCRIPTION__SEG[USVD__STATE = $RateState]">
					<xsl:if test="USVD__MAKE__DESC != 'DOC' and USVD__MAKE__DESC != 'HIRED AUTO' and USVD__MAKE__DESC != 'NON-OWNED'">
						<xsl:call-template name="CreateCommlVeh"/>
					</xsl:if>
				</xsl:for-each>

			</CommlRateState>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>

