<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template name="CreateCommlVeh">

		<CommlVeh>
			<xsl:variable name="VehNbr">
				<xsl:choose>
					<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 2) = '00'">
						<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 3, 1)"/>
					</xsl:when>
					<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 1) = '0'">
						<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 2, 2)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="./USVD__KEY/USVD__UNIT__NUMBER"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="id">
				<xsl:value-of select="concat('v',$VehNbr - 9)"/>
			</xsl:attribute>

			<ItemIdInfo>
				<InsurerId>
					<xsl:value-of select="$VehNbr - 9" />
				</InsurerId>
			</ItemIdInfo>
			<Manufacturer>
				<xsl:value-of select="substring-before(concat(./USVD__MAKE__DESC,' '),' ')" />
			</Manufacturer>
			<Model>
				<xsl:value-of select="substring-after(./USVD__MAKE__DESC, ' ')" />
			</Model>
			<ModelYear>
				<xsl:value-of select="concat(./USVD__YEAR__MAKE/USVD__YEAR__CC,./USVD__YEAR__MAKE/USVD__YEAR__YY)"/>
			</ModelYear>
			<VehBodyTypeCd>
				<xsl:value-of select="./USVD__TYPE__VEHICLE"/>
			</VehBodyTypeCd>
			<CostNewAmt>
				<Amt>
					<xsl:choose>
						<xsl:when test="string-length(./USVD__COST__NEW)='0'">0</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="./USVD__COST__NEW"/>
						</xsl:otherwise>
					</xsl:choose>
				</Amt>
			</CostNewAmt>
			<com.csc_StatedAmt>
				<Amt>
					<xsl:choose>
						<xsl:when test="string-length(./USVD__STATED__AMOUNT)='0'">0</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="./USVD__STATED__AMOUNT"/>
						</xsl:otherwise>
					</xsl:choose>
				</Amt>         
			</com.csc_StatedAmt>
			<NumDaysDrivenPerWeek/>
			<EstimatedAnnualDistance>
				<NumUnits>
					<xsl:value-of select="./USVD__ANNUAL__MILES"/>
				</NumUnits>
				<UnitMeasurementCd>MI</UnitMeasurementCd>
			</EstimatedAnnualDistance>
			<TerritoryCd>
				<xsl:value-of select="./USVD__TERRITORY"/>
			</TerritoryCd>
			<VehIdentificationNumber>
				<xsl:value-of select="./USVD__SERIAL__NUMBER"/>
			</VehIdentificationNumber>
			<VehSymbolCd>
				<xsl:value-of select="./USVD__SYMBOL"/>
			</VehSymbolCd>

				<!-- Find Additional Interests for this vehicle -->
			<xsl:variable name="PASS_LOB">ACV</xsl:variable>
			<xsl:call-template name="CreateAdditionalInterestFrom12SEG">
				<xsl:with-param name="PASS_LOB" select="$PASS_LOB"/>
				<xsl:with-param name="UseCode"/>
				<xsl:with-param name="UnitNo" select="$VehNbr"/>
			</xsl:call-template>

			<CommlVehSupplement>
				<FarZoneCd>	<!-- For Trailer -->
				</FarZoneCd>
				<NearZoneCd>	<!-- For Trailer -->
				</NearZoneCd>
				<PrimaryClassCd>
					<xsl:value-of select="./USVD__VEH__CLASS__CODE" />
				</PrimaryClassCd>
				<UseLeasedVehDesc>
				</UseLeasedVehDesc>
			</CommlVehSupplement>

				<!-- Find Vehicle Coverage for this vehicle -->
			<xsl:for-each select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER=$VehNbr]">
				<xsl:call-template name="CreateCACommlCoverage"></xsl:call-template>
			</xsl:for-each>
		</CommlVeh>

	</xsl:template>
</xsl:stylesheet>

