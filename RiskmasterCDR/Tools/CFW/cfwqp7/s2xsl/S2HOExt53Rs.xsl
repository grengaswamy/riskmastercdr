<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="S2HOPrem53Rs.xsl"/>
	<!--<xsl:include href="CommonTplRq.xsl"/> -->
	<xsl:template name="S2HOExtCovDetS2RsTemplate" match="/">
		<xsl:variable name="ProtectiveDeviceCode">
			<xsl:choose>
				<xsl:when test="*/PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE='H216'">
					<xsl:value-of select="substring(substring-after(*/PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'TYPE='),1,1)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="string('0')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="ReplacementCostExists">
			<xsl:choose>
				<xsl:when test="*/PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE='H290'">
					<xsl:value-of select="string('1')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="string('0')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="LiabilityCovCodeExist">
			<xsl:choose>
				<xsl:when test="*/HOMEOWNERS__PREMIUM__SEG/HOMEOWNERS__TABLE__1/MISCELLANEOUS__PREMIUMS[@index ='0']/HOMEOWNERS__PREMIUM__FORM = '005'">
					<xsl:value-of select="string('1')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="string('0')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="ExtCovH035CounterTemp">
			<xsl:for-each select="*/PROPERTY__EXTENSION__SEG">
				<xsl:if test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H035'">
					<xsl:value-of select="position()"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>

		<xsl:variable name="ExtCovH035Counter">
			<xsl:choose>
				<xsl:when test="$ExtCovH035CounterTemp=''">
					<xsl:choose>
						<xsl:when test="$ProtectiveDeviceCode ='0'">
							<xsl:value-of select="string('0')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('1')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$ExtCovH035CounterTemp"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="ExtCovH041CounterTemp">
			<xsl:for-each select="*/PROPERTY__EXTENSION__SEG">
				<xsl:if test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H041'">
					<xsl:value-of select="string(number(position())-number($ExtCovH035Counter))"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="CountExtCov" select="count(*/PROPERTY__EXTENSION__SEG)" /> 

		<xsl:variable name="ExtCovH041Counter">
			<xsl:choose>
				<xsl:when test="$ExtCovH041CounterTemp=''">
					<xsl:value-of select="string('0')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($ExtCovH041CounterTemp)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:for-each select="*/PROPERTY__EXTENSION__SEG">

			<xsl:choose>
				<xsl:when test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H216'"/>
				<xsl:when test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H290'"/>
			<!--	<xsl:when test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'HWCR'">
					<Watercraft>
						<xsl:attribute name="id">
							<xsl:value-of select="concat('wc', number(PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__SEQUENCE__NUMBER)+1)"/>
						</xsl:attribute>
						<xsl:attribute name="LocationRef">
							<xsl:value-of select="concat('l','1')"/>
						</xsl:attribute>
						<WaterUnitTypeCd>
							<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'SAIL='),1,1)"/>
						</WaterUnitTypeCd>
						<ItemDefinition>
							<ItemTypeCd/>
							<Manufacturer/>
							<Model/>
							<ModelYear/>
							<ItemDesc>
								<xsl:if test="normalize-space(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__3) = ''">
									<xsl:value-of select="PER__DESC__LINE__2"/>
								</xsl:if>
								<xsl:if test="normalize-space(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__3) != ''">
									<xsl:value-of select="concat(PER__DESC__LINE__2,PER__DESC__LINE__3)"/>
								</xsl:if>
							</ItemDesc>
						</ItemDefinition>
						<Length>
							<NumUnits>
								<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'LGTH='),1,2)"/>
							</NumUnits>
							<UnitMeasurementCd/>
						</Length>
						<Speed>
							<NumUnits>
								<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'MPHR='),1,2)"/>
							</NumUnits>
							<UnitMeasurementCd/>
						</Speed>
						<Horsepower>
							<NumUnits>
								<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'HP#1='),1,3)"/>
							</NumUnits>
							<UnitMeasurementCd/>
						</Horsepower>
						<PropulsionTypeCd/>
						<Coverage>
							<CoverageCd>HWCR</CoverageCd>
							<Limit>
								<FormatCurrencyAmt>
									<Amt/>
								</FormatCurrencyAmt>
							</Limit>
							<CurrentTermAmt>
								<Amt>
									<xsl:variable name="FormCd">
										<xsl:call-template name="ConvertHOSIIExtCovCdtoPremiumForm">
											<xsl:with-param name="Value" select="'HWCR'"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:call-template name="S2HOPremRsTemplate">
										<xsl:with-param name="FormCode" select="$FormCd"/>
									</xsl:call-template>
								</Amt>
							</CurrentTermAmt>
							<TerritoryCd>
								<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'TERR='),1,1)"/>
							</TerritoryCd>
							<Form>
								<FormNumber/>
								<FormName/>
								<EditionDt/>
							</Form>
						</Coverage>
						<com.csc_NavigationalPeriod>
							<NumUnits>
								<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'NAVE='),1,2)"/>
							</NumUnits>
							<UnitMeasurementCd/>
						</com.csc_NavigationalPeriod>
					</Watercraft>
					<xsl:if test="string-length(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'HP#2=')) &gt; 0">
						<WatercraftAccessory>
							<xsl:attribute name="WatercraftRef">
								<xsl:value-of select="concat('wc', number(PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__SEQUENCE__NUMBER)+1)"/>
							</xsl:attribute>
							<xsl:attribute name="LocationRef">
								<xsl:value-of select="concat('l','1')"/>
							</xsl:attribute>
							<WaterUnitTypeCd/>
							<ItemDefinition>
								<ItemTypeCd/>
							</ItemDefinition>
							<Horsepower>
								<NumUnits>
									<xsl:value-of select="substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'HP#2='),1,3)"/>
								</NumUnits>
								<UnitMeasurementCd/>
							</Horsepower>
						</WatercraftAccessory>
					</xsl:if>
				</xsl:when>-->
				<xsl:otherwise>
					<!--<xsl:if test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE != 'HWCR'">-->
					<Coverage>
						<xsl:if test="PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__INLAND__MARINE__CODE != 'I'">
							<CoverageCd>
								<xsl:value-of select="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE"/>
							</CoverageCd>
							<IterationNumber>
								<xsl:value-of select="number(PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__SEQUENCE__NUMBER)+1"/>
							</IterationNumber>
						</xsl:if>
						<xsl:if test="PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__INLAND__MARINE__CODE = 'I' and PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__IM__TYPE !=''">
							<CoverageCd>
								<xsl:value-of select="concat(PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__INLAND__MARINE__CODE,PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__IM__TYPE,'#')"/>
							</CoverageCd>
							<IterationNumber>
								<xsl:value-of select="number(substring(PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__IM__OCCURANCE,2,1))+1"/>
							</IterationNumber>
						</xsl:if>
						<CoverageDesc>
							<xsl:choose>
								<xsl:when test="PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__3 !=''">
									<xsl:value-of select="concat(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__2,PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__3)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="normalize-space(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__2)"/>
								</xsl:otherwise>
							</xsl:choose>
						</CoverageDesc>
						<xsl:choose>
							<xsl:when test="string-length(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1) &gt; 0">
								<Limit>
									<FormatCurrencyAmt>
										<xsl:choose>
											<xsl:when test="contains(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'$AMT=')">
												<Amt>
													<xsl:value-of select="concat(substring(substring-after(PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'$AMT='),1,5),'.00')"/>
												</Amt>
											</xsl:when>
											<xsl:otherwise>
												<Amt>
													<xsl:value-of select="string('')"/>
												</Amt>
											</xsl:otherwise>
										</xsl:choose>
									</FormatCurrencyAmt>
								</Limit>
							</xsl:when>
							<xsl:otherwise>
								<Limit>
									<FormatCurrencyAmt>
										<Amt>
											<xsl:value-of select="string('')"/>
										</Amt>
									</FormatCurrencyAmt>
								</Limit>
							</xsl:otherwise>
						</xsl:choose>
						<CurrentTermAmt>
							<Amt>
								<xsl:variable name="ExtCovcode">
									<xsl:choose>
										<xsl:when test="substring(PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE,1,1) = 'I'">
											<xsl:value-of select="concat(PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__INLAND__MARINE__CODE,PROPERTY__EXTENSION__DATA/PER__INLAND__MARINE__USE/PER__IM__TYPE,'#')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="FormCd">
									<xsl:call-template name="ConvertHOSIIExtCovCdtoPremiumForm">
										<xsl:with-param name="Value" select="$ExtCovcode"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="IndexPosition">
									<xsl:choose>
										<xsl:when test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H035'">
											<xsl:choose>
												<xsl:when test="$ProtectiveDeviceCode ='0'">
													<xsl:value-of select="position()-1-number($ReplacementCostExists)+number($LiabilityCovCodeExist)"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="position()-2-number($ReplacementCostExists)+number($LiabilityCovCodeExist)"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:when test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'HWCR'">
											<xsl:variable name="SeqNumber" select="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__SEQUENCE__NUMBER"/>
											<xsl:choose>
												<xsl:when test="$ProtectiveDeviceCode ='0'">
													<xsl:value-of select="($CountExtCov)-1-($SeqNumber)-number($ReplacementCostExists)-$ExtCovH041Counter+number($LiabilityCovCodeExist)"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="($CountExtCov)-2-($SeqNumber)-number($ReplacementCostExists)-$ExtCovH041Counter+number($LiabilityCovCodeExist)"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="$ProtectiveDeviceCode ='0'">
													<xsl:value-of select="position()-1-number($ReplacementCostExists)-$ExtCovH041Counter+number($LiabilityCovCodeExist)"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="position()-2-number($ReplacementCostExists)-$ExtCovH041Counter+number($LiabilityCovCodeExist)"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<!--<xsl:variable name="IndexPosition" select="position()-2"/>-->
								<xsl:call-template name="S2HOPremRsTemplate">
									<xsl:with-param name="FormCode" select="$FormCd"/>
									<xsl:with-param name="IndexPosition" select="$IndexPosition"/>
									<xsl:with-param name="SequenceNumber" select="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__SEQUENCE__NUMBER"/>
								</xsl:call-template>
								<xsl:if test="PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE = 'H041'">
									<xsl:value-of select="string('0.00')"/>
								</xsl:if>
							</Amt>
						</CurrentTermAmt>
					</Coverage>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
  <!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->