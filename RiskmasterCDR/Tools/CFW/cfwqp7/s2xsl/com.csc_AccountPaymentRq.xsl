<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
<!--02 Segment of 901 file-->
<xsl:include href="BuildInstPmt02SegmentRq.xsl"/>

<xsl:template match="/">
<com.csc_AccountPaymentRq>
 <!--Build the XSL record for 02 Installment Payment Segment-->
	<xsl:call-template name ="BuildInstPmt02SegmentRq" />
</com.csc_AccountPaymentRq>
</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->