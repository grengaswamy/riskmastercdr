<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="CommandAreaRecord">
<!--	<xsl:template match="/"> -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__SYNC__REQ</RECORD__NAME>
			</RECORD__NAME__ROW>
			<POLICY__SYNC__REQ>
				<COMMAREA__ACCOUNTING>
				<!--Issue # 34770 Begin-->
				<!--<C__REQUEST__TYPE>PAUT</C__REQUEST__TYPE>-->
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/CompanyProductCd">
						<C__REQUEST__TYPE>
							<xsl:choose>
								<!--issue # 40090 starts-->
								<xsl:when test="../LOBCd='APV'">PAUT</xsl:when>
								<xsl:when test="../LOBCd='ACV'">PAUT</xsl:when>
								<xsl:when test="../LOBCd='AFV'">PAUT</xsl:when>
								<xsl:when test="../LOBCd='APT'">PAUT</xsl:when>
								<!--issue # 40090 Ends-->
								<xsl:when test=".='AUTOP'">PAUT</xsl:when>
								<xsl:when test=".='AUTOT'">PAUT</xsl:when>
								<xsl:when test=".='APT'">PAUT</xsl:when>
								<xsl:when test=".='APV'">PAUT</xsl:when>
								<xsl:when test=".='ACV'">PAUT</xsl:when>
								<xsl:when test=".='AFV'">PAUT</xsl:when>
								<xsl:otherwise>PSYN</xsl:otherwise>
							</xsl:choose>
						</C__REQUEST__TYPE>
					</xsl:for-each>
					<!--Issue # 34770 End-->
					<C__REQUEST__ID/>
					<C__MORE__INDICATOR/>
					<C__ERROR__CODE/>
					<C__USER__NAME>S2</C__USER__NAME>
					<C__POLICY__KEY>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/com.csc_CompanyPolicyProcessingId">
							<C__LOCATION>
								<!-- Case 34768 Begin -->
								<!-- <xsl:value-of select="."/> -->
								<xsl:call-template name="FormatData">
       								<xsl:with-param name="FieldName">C__LOCATION</xsl:with-param>
    								<xsl:with-param name="FieldLength">2</xsl:with-param>
		      						<xsl:with-param name="Value" select="."/>
		     						<xsl:with-param name="FieldType">N</xsl:with-param>
	        					</xsl:call-template>
								<!-- Case 34768 End -->
							</C__LOCATION>
						</xsl:for-each>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/PolicyNumber">
							<C__POLICY>
								<xsl:value-of select="."/>
							</C__POLICY>
						</xsl:for-each>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/CompanyProductCd">
							<C__SYMBOL>
							<!-- issue # 40090 start-->
							<xsl:choose>
							<xsl:when test=". !=''">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
							<!-- issue # 40090 end-->
								<xsl:choose>
									<xsl:when test=".='AUTOP'">APV</xsl:when>
									<xsl:when test=".='AUTOT'">ATV</xsl:when>
									<xsl:when test=".='APT'">APT</xsl:when>
									<xsl:when test=".='ACV'">ACV</xsl:when>
									<xsl:when test=".='AFV'">AFV</xsl:when>
									<xsl:when test=".='HO'">HO</xsl:when>
									<xsl:when test=".='HP'">HP</xsl:when>
									<xsl:when test=".='BOP'">BOP</xsl:when>
									<xsl:when test=".='BO'">BOP</xsl:when>
									<xsl:when test=".='WC'">WCP</xsl:when>
									<xsl:when test=".='WC '">WCP</xsl:when>
									<xsl:when test=".='WCV'">WCV</xsl:when>
									<xsl:when test=".='WCA'">WCA</xsl:when>
									<xsl:when test=".='WCP'">WCP</xsl:when>
									<xsl:when test=".='CPP'">CPP</xsl:when>
									<xsl:otherwise>APV</xsl:otherwise>
								</xsl:choose>
								<!-- issue # 40090 starts-->
								</xsl:otherwise>
								</xsl:choose>
								<!-- issue # 40090 end-->
							</C__SYMBOL>
						</xsl:for-each>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/NAICCd">
							<C__MASTER__COMPANY>
								<xsl:value-of select="."/>
							</C__MASTER__COMPANY>
						</xsl:for-each>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/PolicyVersion">
							<C__MODULE>
								<xsl:call-template name="FormatData">
       								<xsl:with-param name="FieldName">C__MODULE</xsl:with-param>
    								<xsl:with-param name="FieldLength">2</xsl:with-param>
		      						<xsl:with-param name="Value" select="."/>
		     						<xsl:with-param name="FieldType">N</xsl:with-param>
	        					</xsl:call-template>
							</C__MODULE>
						</xsl:for-each>
						<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/com.csc_InsuranceLineIssuingCompany">
							<C__PIF__COMPANY__NUMBER>
								<xsl:call-template name="FormatData">
       								<xsl:with-param name="FieldName">C__PIF__COMPANY__NUMBER</xsl:with-param>
    								<xsl:with-param name="FieldLength">2</xsl:with-param>
		      						<xsl:with-param name="Value" select="."/>
		     						<xsl:with-param name="FieldType">N</xsl:with-param>
		        				</xsl:call-template>
							</C__PIF__COMPANY__NUMBER>
						</xsl:for-each>  
					</C__POLICY__KEY>
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/RqUID">
						<C__RQID1>
							<xsl:value-of select="."/>
						</C__RQID1>
					</xsl:for-each>
				</COMMAREA__ACCOUNTING>
			</POLICY__SYNC__REQ>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>