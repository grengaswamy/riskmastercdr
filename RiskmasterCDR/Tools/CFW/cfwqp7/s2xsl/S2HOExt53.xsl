<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--<xsl:template name="S2HOExtCovDetTemplate" match="/">-->
	<xsl:template name="S2HOExtCovDetTemplate">
		<xsl:for-each select="ACORD/InsuranceSvcRq/*/HomeLineBusiness/Coverage">
			<xsl:if test="CoverageCd != ''">
				<xsl:variable name="CovDesc" select="CoverageDesc"/>
				<xsl:variable name="CovCode" select="substring(CoverageCd,1,4)"/>
				<BUS__OBJ__RECORD>
					<RECORD__NAME__ROW>
						<RECORD__NAME>PROPERTY__EXTENSION__SEG</RECORD__NAME>
					</RECORD__NAME__ROW>
					<PROPERTY__EXTENSION__SEG>
						<PER__REC__LLBB>
							<PER__REC__LENGTH/>
							<PER__ACTION__CODE/>
							<PER__FILE__ID/>
						</PER__REC__LLBB>
						<PER__ID>30</PER__ID>
						<xsl:choose>
							<xsl:when test="substring(CoverageCd,1,1)='I'">
								<PER__INLAND__MARINE__USE>
									<PER__INLAND__MARINE__CODE>
										<xsl:value-of select="string('I')"/>
									</PER__INLAND__MARINE__CODE>
									<PER__IM__TYPE>
										<xsl:value-of select="substring(CoverageCd,2,2)"/>
									</PER__IM__TYPE>
									<PER__IM__OCCURANCE>
										<xsl:variable name="IterationNumber" select="IterationNumber"/>
										<xsl:choose>
											<xsl:when test="string-length($IterationNumber) &gt; 1">
												<xsl:value-of select="substring(concat('00',number($IterationNumber)-1),number(string-length($IterationNumber)),2)"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="substring(concat('00',number($IterationNumber)-1),1+number(string-length($IterationNumber)),2)"/>
											</xsl:otherwise>
										</xsl:choose>
									</PER__IM__OCCURANCE>
								</PER__INLAND__MARINE__USE>
							</xsl:when>
							<xsl:otherwise>
								<PER__USE__SEQUENCE>
									<PER__USE>
										<xsl:value-of select="CoverageCd"/>
									</PER__USE>
									<PER__SEQUENCE__NUMBER>
										<xsl:variable name="IterationNumber" select="IterationNumber"/>
										<xsl:value-of select="number($IterationNumber)-1"/>
									</PER__SEQUENCE__NUMBER>
								</PER__USE__SEQUENCE>
							</xsl:otherwise>
						</xsl:choose>
						<PER__AMENDMENT__NUMBER>
							<xsl:value-of select="string('0')"/>
						</PER__AMENDMENT__NUMBER>
						<PER__ENTERED__DATE>
							<xsl:variable name="TransEffDt" select="../../TransactionRequestDt"/>
							<xsl:value-of select="concat(substring($TransEffDt,1,4),substring($TransEffDt,6,2),substring($TransEffDt,9,2)) "/>
						</PER__ENTERED__DATE>
						<PER__PARAMETERS>
							<PER__DESC__LINE__1>
								<xsl:choose>
									<xsl:when test="$CovCode ='H035'"><xsl:if test="Limit/FormatCurrencyAmt/Amt != ''"><xsl:variable name="LimitAmt" select="Limit/FormatCurrencyAmt/Amt"/><xsl:value-of select="concat('$AMT=',substring(concat('00000',$LimitAmt),number(string-length($LimitAmt))+1,5))"/></xsl:if><xsl:for-each select="Option"><xsl:choose><xsl:when test="OptionCd ='COVG'"><xsl:if test="OptionValue = '3' or OptionValue ='5'"><xsl:value-of select="concat(',',OptionCd,'=S')"/></xsl:if><xsl:if test="OptionValue = '1' or OptionValue ='2' or OptionValue ='4' or OptionValue ='6'"><xsl:value-of select="concat(',',OptionCd,'=B')"/></xsl:if></xsl:when><xsl:otherwise><xsl:value-of select="concat(',',OptionCd,'=',OptionValue)"/></xsl:otherwise></xsl:choose></xsl:for-each></xsl:when>
									<xsl:otherwise><xsl:if test="Limit/FormatCurrencyAmt/Amt != ''"><xsl:variable name="LimitAmt" select="Limit/FormatCurrencyAmt/Amt"/><xsl:value-of select="concat('$AMT=',substring(concat('00000',$LimitAmt),number(string-length($LimitAmt))+1,5))"/></xsl:if><xsl:for-each select="Option"><xsl:if test="OptionCd !='' and OptionValue != ''"><xsl:choose><xsl:when test="position () &gt;1"><xsl:choose><xsl:when test="OptionCd ='#EMP'"><xsl:value-of select="concat(',',OptionCd,'=',substring(concat('00',OptionValue),number(string-length(OptionValue))+1,2))"/></xsl:when><xsl:otherwise><xsl:value-of select="concat(',',OptionCd,'=',OptionValue)"/></xsl:otherwise></xsl:choose></xsl:when><xsl:when test="../Limit/FormatCurrencyAmt/Amt != ''"><xsl:choose><xsl:when test="OptionCd ='#EMP'"><xsl:value-of select="concat(',',OptionCd,'=',substring(concat('00',OptionValue),number(string-length(OptionValue))+1,2))"/></xsl:when><xsl:otherwise><xsl:value-of select="concat(',',OptionCd,'=',OptionValue)"/></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><xsl:choose><xsl:when test="OptionCd ='#EMP'"><xsl:value-of select="concat(OptionCd,'=',substring(concat('00',OptionValue),number(string-length(OptionValue))+1,2))"/></xsl:when><xsl:otherwise><xsl:value-of select="concat(OptionCd,'=',OptionValue)"/></xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:if></xsl:for-each></xsl:otherwise>
								</xsl:choose>
							</PER__DESC__LINE__1>
							<PER__DESC__LINE__2>
								<xsl:if test="string-length ($CovDesc) &gt; 70">
									<xsl:value-of select="substring($CovDesc,1,70)"/>
								</xsl:if>
								<xsl:if test="string-length($CovDesc) &lt; 70">
									<xsl:value-of select="$CovDesc"/>
								</xsl:if>
							</PER__DESC__LINE__2>
							<PER__SHORT__DESC__LINE__3>
								<PER__DESC__LINE__FIRST__60>
									<xsl:if test="string-length($CovDesc) &gt; 70">
										<xsl:value-of select="substring($CovDesc,71,70)"/>
									</xsl:if>
								</PER__DESC__LINE__FIRST__60>
							</PER__SHORT__DESC__LINE__3>
							<PER__STATUS>
								<xsl:value-of select="string('C')"/>
							</PER__STATUS>
							<PER__ORIGINAL__RATE__BOOK/>
							<PER__RATE__BOOK/>
							<PER__ENDORSEMENT__PREM>
								<xsl:value-of select="string('0000')"/>
							</PER__ENDORSEMENT__PREM>
							<PER__H390__FIRE__RB/>
							<PER__H390__EC__RB/>
							<PER__H390__VMM__RB/>
							<fill_3/>
						</PER__PARAMETERS>
						<PER__PMS__FUTURE__USE/>
						<PER__ACA__IDENTIFIER__SAVE2>
							<PER__ACA__FIXED__IDENTIFIER__2>
								<xsl:value-of select="string('0000')"/>
							</PER__ACA__FIXED__IDENTIFIER__2>
						</PER__ACA__IDENTIFIER__SAVE2>
						<PER__CUST__FUTURE__USE/>
						<PER__YR2000__CUST__USE/>
						<PER__DUPLICATE__KEY__SEQUENCE>
							<xsl:value-of select="string('000')"/>
						</PER__DUPLICATE__KEY__SEQUENCE>
					</PROPERTY__EXTENSION__SEG>
				</BUS__OBJ__RECORD>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="S2HOExtCovWaterCraftTemplate" match="/">
		<xsl:for-each select="ACORD/InsuranceSvcRq/*/HomeLineBusiness/Watercraft">
			<xsl:if test="WaterUnitTypeCd != '' ">
				<xsl:variable name="CovDesc" select="CoverageDesc"/>
				<BUS__OBJ__RECORD>
					<RECORD__NAME__ROW>
						<RECORD__NAME>PROPERTY__EXTENSION__SEG</RECORD__NAME>
					</RECORD__NAME__ROW>
					<PROPERTY__EXTENSION__SEG>
						<PER__REC__LLBB>
							<PER__REC__LENGTH/>
							<PER__ACTION__CODE/>
							<PER__FILE__ID/>
						</PER__REC__LLBB>
						<PER__ID>30</PER__ID>
						<PER__USE__SEQUENCE>
							<PER__USE>
								<xsl:value-of select="Coverage/CoverageCd"/>
							</PER__USE>
							<PER__SEQUENCE__NUMBER>
								<xsl:variable name="IterationNumber" select="substring(@id,3,1)"/>
								<xsl:value-of select="number($IterationNumber)-1"/>
							</PER__SEQUENCE__NUMBER>
						</PER__USE__SEQUENCE>
						<PER__AMENDMENT__NUMBER>
							<xsl:value-of select="string('0')"/>
						</PER__AMENDMENT__NUMBER>
						<PER__ENTERED__DATE>
							<xsl:variable name="TransEffDt" select="../../TransactionRequestDt"/>
							<xsl:value-of select="concat(substring($TransEffDt,1,4),substring($TransEffDt,6,2),substring($TransEffDt,9,2)) "/>
						</PER__ENTERED__DATE>
						<PER__PARAMETERS>
							<PER__DESC__LINE__1>
								<xsl:variable name="WaterCraftId" select="@id"/>
								<xsl:choose><xsl:when test="WaterUnitTypeCd='N'"><xsl:if test="Length/NumUnits !=''"><xsl:value-of select="concat('LGTH=',substring(concat('00',Length/NumUnits),number(string-length(Length/NumUnits))+1,2 ))"/></xsl:if><xsl:choose><xsl:when test="Horsepower/NumUnits !=''"><xsl:value-of select="concat(',HP#1=',substring(concat('000',Horsepower/NumUnits),number(string-length(Horsepower/NumUnits))+1,3))"/><xsl:if test="../WatercraftAccessory[@WatercraftRef=$WaterCraftId]/Horsepower/NumUnits !=''"><xsl:value-of select="concat(',HP#2=',substring(concat('000',../WatercraftAccessory[@WatercraftRef=$WaterCraftId]/Horsepower/NumUnits),number(string-length(../WatercraftAccessory[@WatercraftRef=$WaterCraftId]/Horsepower/NumUnits))+1,3))"/></xsl:if></xsl:when><xsl:otherwise><xsl:value-of select="concat(',SAIL=',WaterUnitTypeCd)"/></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><xsl:value-of select="concat('SAIL=',WaterUnitTypeCd)"/></xsl:otherwise></xsl:choose><xsl:if test="com.csc_NavigationalPeriod/NumUnits !=''"><xsl:value-of select="concat(',NAVE=',substring(concat('00',com.csc_NavigationalPeriod/NumUnits),1+number(string-length(com.csc_NavigationalPeriod/NumUnits)) ,2))"/></xsl:if><xsl:if test="Coverage/TerritoryCd !=''"><xsl:value-of select="concat(',TERR=',substring(concat('00',Coverage/TerritoryCd),1+number(string-length(Coverage/TerritoryCd)),2))"/></xsl:if>
							</PER__DESC__LINE__1>
							<PER__DESC__LINE__2>
								<xsl:if test="string-length(ItemDefinition/ItemDesc)&gt; 70">
									<xsl:value-of select="substring(ItemDefinition/ItemDesc,1,70)"/>
								</xsl:if>
								<xsl:if test="string-length(ItemDefinition/ItemDesc) &lt; 70">
									<xsl:value-of select="ItemDefinition/ItemDesc"/>
								</xsl:if>
							</PER__DESC__LINE__2>
							<PER__SHORT__DESC__LINE__3>
								<PER__DESC__LINE__FIRST__60>
									<xsl:if test="string-length(ItemDefinition/ItemDesc) &gt; 70">
										<xsl:value-of select="substring(ItemDefinition/ItemDesc,71,70)"/>
									</xsl:if>
								</PER__DESC__LINE__FIRST__60>
							</PER__SHORT__DESC__LINE__3>
							<PER__STATUS>
								<xsl:value-of select="string('C')"/>
							</PER__STATUS>
							<PER__ORIGINAL__RATE__BOOK/>
							<PER__RATE__BOOK/>
							<PER__ENDORSEMENT__PREM>
								<xsl:value-of select="string('0000')"/>
							</PER__ENDORSEMENT__PREM>
							<PER__H390__FIRE__RB/>
							<PER__H390__EC__RB/>
							<PER__H390__VMM__RB/>
							<fill_3/>
						</PER__PARAMETERS>
						<PER__PMS__FUTURE__USE/>
						<PER__ACA__IDENTIFIER__SAVE2>
							<PER__ACA__FIXED__IDENTIFIER__2>
								<xsl:value-of select="string('0000')"/>
							</PER__ACA__FIXED__IDENTIFIER__2>
						</PER__ACA__IDENTIFIER__SAVE2>
						<PER__CUST__FUTURE__USE/>
						<PER__YR2000__CUST__USE/>
						<PER__DUPLICATE__KEY__SEQUENCE>
							<xsl:value-of select="string('000')"/>
						</PER__DUPLICATE__KEY__SEQUENCE>
					</PROPERTY__EXTENSION__SEG>
				</BUS__OBJ__RECORD>
			</xsl:if>
		</xsl:for-each>
		</xsl:template>
		<xsl:template name="S2HOProtectiveDeviceTemplate" match="/">
		<xsl:if test="normalize-space(ACORD/InsuranceSvcRq/*/HomeLineBusiness/Dwell/BldgProtection/ProtectionDeviceBurglarCd) != ''">
			<BUS__OBJ__RECORD>
				<RECORD__NAME__ROW>
					<RECORD__NAME>PROPERTY__EXTENSION__SEG</RECORD__NAME>
				</RECORD__NAME__ROW>
				<PROPERTY__EXTENSION__SEG>
					<PER__REC__LLBB>
						<PER__REC__LENGTH/>
						<PER__ACTION__CODE/>
						<PER__FILE__ID/>
					</PER__REC__LLBB>
					<PER__ID>30</PER__ID>
					<PER__USE__SEQUENCE>
						<PER__USE>
							<xsl:text>H216</xsl:text>
						</PER__USE>
						<PER__SEQUENCE__NUMBER>
							<xsl:text>0</xsl:text>
						</PER__SEQUENCE__NUMBER>
					</PER__USE__SEQUENCE>
					<PER__AMENDMENT__NUMBER>
						<xsl:value-of select="string('0')"/>
					</PER__AMENDMENT__NUMBER>
					<PER__ENTERED__DATE>
						<xsl:variable name="TransEffDt" select="ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/TransactionRequestDt"/>
						<xsl:value-of select="concat(substring($TransEffDt,1,4),substring($TransEffDt,6,2),substring($TransEffDt,9,2)) "/>
					</PER__ENTERED__DATE>
					<PER__PARAMETERS>
						<PER__DESC__LINE__1>
							<xsl:value-of select="concat('DWL#=0,TYPE=',normalize-space(ACORD/InsuranceSvcRq/*/HomeLineBusiness/Dwell/BldgProtection/ProtectionDeviceBurglarCd))"/>
						</PER__DESC__LINE__1>
						<PER__DESC__LINE__2/>
						<PER__SHORT__DESC__LINE__3>
							<PER__DESC__LINE__FIRST__60>
							</PER__DESC__LINE__FIRST__60>
						</PER__SHORT__DESC__LINE__3>
						<PER__STATUS>
							<xsl:value-of select="string('C')"/>
						</PER__STATUS>
						<PER__ORIGINAL__RATE__BOOK/>
						<PER__RATE__BOOK/>
						<PER__ENDORSEMENT__PREM>
							<xsl:value-of select="string('0000')"/>
						</PER__ENDORSEMENT__PREM>
						<PER__H390__FIRE__RB/>
						<PER__H390__EC__RB/>
						<PER__H390__VMM__RB/>
						<fill_3/>
					</PER__PARAMETERS>
					<PER__PMS__FUTURE__USE/>
					<PER__ACA__IDENTIFIER__SAVE2>
						<PER__ACA__FIXED__IDENTIFIER__2>
							<xsl:value-of select="string('0000')"/>
						</PER__ACA__FIXED__IDENTIFIER__2>
					</PER__ACA__IDENTIFIER__SAVE2>
					<PER__CUST__FUTURE__USE/>
					<PER__YR2000__CUST__USE/>
					<PER__DUPLICATE__KEY__SEQUENCE>
						<xsl:value-of select="string('000')"/>
					</PER__DUPLICATE__KEY__SEQUENCE>
				</PROPERTY__EXTENSION__SEG>
			</BUS__OBJ__RECORD>
		</xsl:if>
	</xsl:template>
	<xsl:template name="S2HOReplacementCostTemplate" match="/">
			<BUS__OBJ__RECORD>
				<RECORD__NAME__ROW>
					<RECORD__NAME>PROPERTY__EXTENSION__SEG</RECORD__NAME>
				</RECORD__NAME__ROW>
				<PROPERTY__EXTENSION__SEG>
					<PER__REC__LLBB>
						<PER__REC__LENGTH/>
						<PER__ACTION__CODE/>
						<PER__FILE__ID/>
					</PER__REC__LLBB>
					<PER__ID>30</PER__ID>
					<PER__USE__SEQUENCE>
						<PER__USE>
							<xsl:text>H290</xsl:text>
						</PER__USE>
						<PER__SEQUENCE__NUMBER>
							<xsl:text>0</xsl:text>
						</PER__SEQUENCE__NUMBER>
					</PER__USE__SEQUENCE>
					<PER__AMENDMENT__NUMBER>
						<xsl:value-of select="string('0')"/>
					</PER__AMENDMENT__NUMBER>
					<PER__ENTERED__DATE>
						<xsl:variable name="TransEffDt" select="ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/TransactionRequestDt"/>
						<xsl:value-of select="concat(substring($TransEffDt,1,4),substring($TransEffDt,6,2),substring($TransEffDt,9,2)) "/>
					</PER__ENTERED__DATE>
					<PER__PARAMETERS>
						<PER__DESC__LINE__1>
							<xsl:value-of select="string('DWL#=0')"/>
						</PER__DESC__LINE__1>
						<PER__DESC__LINE__2/>
						<PER__SHORT__DESC__LINE__3>
							<PER__DESC__LINE__FIRST__60>
							</PER__DESC__LINE__FIRST__60>
						</PER__SHORT__DESC__LINE__3>
						<PER__STATUS>
							<xsl:value-of select="string('C')"/>
						</PER__STATUS>
						<PER__ORIGINAL__RATE__BOOK/>
						<PER__RATE__BOOK/>
						<PER__ENDORSEMENT__PREM>
							<xsl:value-of select="string('0000')"/>
						</PER__ENDORSEMENT__PREM>
						<PER__H390__FIRE__RB/>
						<PER__H390__EC__RB/>
						<PER__H390__VMM__RB/>
						<fill_3/>
					</PER__PARAMETERS>
					<PER__PMS__FUTURE__USE/>
					<PER__ACA__IDENTIFIER__SAVE2>
						<PER__ACA__FIXED__IDENTIFIER__2>
							<xsl:value-of select="string('0000')"/>
						</PER__ACA__FIXED__IDENTIFIER__2>
					</PER__ACA__IDENTIFIER__SAVE2>
					<PER__CUST__FUTURE__USE/>
					<PER__YR2000__CUST__USE/>
					<PER__DUPLICATE__KEY__SEQUENCE>
						<xsl:value-of select="string('000')"/>
					</PER__DUPLICATE__KEY__SEQUENCE>
				</PROPERTY__EXTENSION__SEG>
			</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>