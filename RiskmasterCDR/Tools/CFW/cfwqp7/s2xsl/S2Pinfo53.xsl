<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2PinfoTemplate">
		<xsl:param name="ProcessingType"/>
		<xsl:param name="PolicyMode"/>
		<!-- Case 34768 -->
		<!-- Case 34768 Begin -->
		<xsl:variable name="PolicyModule">
			<xsl:choose>
				<xsl:when test="$PolicyMode = 'N'">00</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$MOD"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Case 34768 End -->
		<!-- <xsl:template match="/"> -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__INFORMATION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<POLICY__INFORMATION__SEG>
				<PIF__REC__LLBB/>
				<PIF__ID>02</PIF__ID>
				<PIF__KEY>
					<PIF__SYMBOL>
						<xsl:value-of select="$SYM"/>
					</PIF__SYMBOL>
					<PIF__POLICY__NUMBER>
						<xsl:value-of select="$POL"/>
					</PIF__POLICY__NUMBER>
					<PIF__MODULE>
						<xsl:call-template name="FormatData">
							<xsl:with-param name="FieldName">PIF__MODULE</xsl:with-param>
							<xsl:with-param name="FieldLength">2</xsl:with-param>
							<!-- <xsl:with-param name="Value" select="$MOD"/>		Case 34768 -->
							<xsl:with-param name="Value" select="$PolicyModule"/>
							<!--	Case 34768 -->
							<xsl:with-param name="FieldType">N</xsl:with-param>
						</xsl:call-template>
					</PIF__MODULE>
				</PIF__KEY>
				<!--Issue 103869 Starts-->
				<!--<PIF__MASTER__CO__NUMBER>
					<xsl:value-of select="$MCO"/>
				</PIF__MASTER__CO__NUMBER>-->
				<PIF__MASTER__CO__NUMBER__A>
					<xsl:value-of select="$MCO"/>
				</PIF__MASTER__CO__NUMBER__A>
				<!--Issue 103869 Ends-->
				<PIF__LOCATION>
					<xsl:value-of select="$LOC"/>
				</PIF__LOCATION>
				<PIF__AMEND__NUMBER>
					<xsl:value-of select="string('00')"/>
				</PIF__AMEND__NUMBER>
				<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/*/TransactionEffectiveDt"/>
				<!-- Case 34768 Begin -->
				<!-- <xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/> -->
				<xsl:variable name="EffDt">
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Case 34768 End -->
				<!--  Always move the ContractTerm/EffectiveDt to PIF__EFFECTIVE_DATE.  This will be used for 
server rating.  Overlay that date with TransactionEffectiveDt if the message is for issue 
(Processing in series II batch cycle.  This will be achieved by the presents of the "ChangeStatus"
element.  If that element is present, then the policy is being issued, otherwise it is being
quoted using server rating.  -->
				<PIF__EFFECTIVE__DATE>
					<xsl:choose>
						<xsl:when test="$ProcessingType = 'Q'or $ProcessingType = 'I'">
							<!-- Case 34768: added test for 'I'. -->
							<PIF__EFF__DATE__YR__MO>
								<PIF__EFF__YR>
									<xsl:value-of select="substring($EffDt,1,4)"/>
								</PIF__EFF__YR>
								<PIF__EFF__MO>
									<xsl:value-of select="substring($EffDt,6,2)"/>
								</PIF__EFF__MO>
							</PIF__EFF__DATE__YR__MO>
							<PIF__EFF__DA>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</PIF__EFF__DA>
						</xsl:when>
						<!-- Overlay the policy effective date with TransactionEffectiveDt for issuing the policy.  -->
						<xsl:when test="$ProcessingType = 'EN'">
							<!-- Case 34768: changed test from 'I' to 'EN'. -->
							<PIF__EFF__DATE__YR__MO>
								<PIF__EFF__YR>
									<xsl:value-of select="substring($TransEffDt,1,4)"/>
								</PIF__EFF__YR>
								<PIF__EFF__MO>
									<xsl:value-of select="substring($TransEffDt,6,2)"/>
								</PIF__EFF__MO>
							</PIF__EFF__DATE__YR__MO>
							<PIF__EFF__DA>
								<xsl:value-of select="substring($TransEffDt,9,2)"/>
							</PIF__EFF__DA>
						</xsl:when>
						<!-- Case 34768 Begin -->
						<xsl:otherwise>
							<PIF__EFF__DATE__YR__MO>
								<PIF__EFF__YR>
									<xsl:value-of select="substring($EffDt,1,4)"/>
								</PIF__EFF__YR>
								<PIF__EFF__MO>
									<xsl:value-of select="substring($EffDt,6,2)"/>
								</PIF__EFF__MO>
							</PIF__EFF__DATE__YR__MO>
							<PIF__EFF__DA>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</PIF__EFF__DA>
						</xsl:otherwise>
						<!-- Case 34768 End -->
					</xsl:choose>
				</PIF__EFFECTIVE__DATE>
				<!-- Case 34768 Begin: need to check LOB to determine where EXP Date is coming from.  -->
				<!-- <xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/> -->
				<xsl:variable name="ExpDt">
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Case 34768 End.  -->
				<PIF__EXPIRATION__DATE>
					<PIF__EXP__DATE__YR__MO>
						<PIF__EXP__YR>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PIF__EXP__YR>
						<PIF__EXP__MO>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PIF__EXP__MO>
					</PIF__EXP__DATE__YR__MO>
					<PIF__EXP__DA>
						<xsl:value-of select="substring($ExpDt,9,2)"/>
					</PIF__EXP__DA>
				</PIF__EXPIRATION__DATE>
				<PIF__INSTALLMENT__TERM>
					<!-- Case 34768 Begin -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_PolicyTermMonths"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- Case 34768 End -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_PolicyTermMonths"/>
						</xsl:otherwise>
						<!-- Case 34768 -->
					</xsl:choose>
					<!-- Case 34768 -->
				</PIF__INSTALLMENT__TERM>
				<PIF__NUMBER__INSTALLMENTS>
					<xsl:value-of select="string('01')"/>
				</PIF__NUMBER__INSTALLMENTS>
				<PIF__RISK__STATE__PROV>
					<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
						<!-- Case 34768 Begin -->
						<!-- <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/> -->
						<xsl:with-param name="Value">
							<xsl:choose>
								<xsl:when test="$LOB = 'WCP' or $LOB = 'CA' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
									<!-- xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ControllingStateProvCd"/ -->
									<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/ControllingStateProvCd,1, 2)"/>
								</xsl:when>
								<xsl:otherwise>
									<!-- xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/ -->
									 <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd,1, 2)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<!-- Case 34768 End -->
					</xsl:call-template>
				</PIF__RISK__STATE__PROV>
				<PIF__COMPANY__NUMBER>
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP'">
							<xsl:value-of select="$PCO"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$MCO"/>
						</xsl:otherwise>
					</xsl:choose>
				</PIF__COMPANY__NUMBER>
				<PIF__BRANCH>
					<xsl:value-of select="string('00')"/>
				</PIF__BRANCH>
				<PIF__PROFIT__CENTER>
					<xsl:value-of select="string('000')"/>
				</PIF__PROFIT__CENTER>
				<PIF__AGENCY__NUMBER>
					<xsl:value-of select="normalize-space(/ACORD/InsuranceSvcRq/*/Producer/ItemIdInfo/AgencyId)"/>
					<!--<fill_3>
						<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/Producer/ItemIdInfo/AgencyId,1,3)"/>
					</fill_3>
					<PIF__REPORTING__AGT__NR>
						<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/Producer/ItemIdInfo/AgencyId,4,1)"/>
					</PIF__REPORTING__AGT__NR>
					<fill_4>
						<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/Producer/ItemIdInfo/AgencyId,5,3)"/>
					</fill_4>-->
				</PIF__AGENCY__NUMBER>
				<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/*/TransactionRequestDt"/>
				<PIF__ENTERED__DATE>
					<xsl:value-of select="concat(substring($TransRqDt,1,4),substring($TransRqDt,6,2),substring($TransRqDt,9,2))"/>
				</PIF__ENTERED__DATE>
				<PIF__TOTAL__AGEN__PREM>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/CurrentTermAmt/Amt"/> -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CurrentTermAmt/Amt"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/CurrentTermAmt/Amt"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__TOTAL__AGEN__PREM>
				<PIF__LINE__BUSINESS>
					<xsl:value-of select="$LOB"/>
				</PIF__LINE__BUSINESS>
				<PIF__ISSUE__CODE>
					<!-- Case 34768 Begin -->
					<!--<xsl:choose>
						<xsl:when test="$ProcessingType='Q'">
							<xsl:value-of select="string('N')"/>
						</xsl:when>
						<xsl:when test="$ProcessingType='I'">
							<xsl:value-of select="string('A')"/>
						</xsl:when>
					</xsl:choose>-->
					<xsl:value-of select="$PolicyMode"/>
					<!-- Case 34768 End -->
				</PIF__ISSUE__CODE>
				<!-- Case 34768 Begin -->
				<!-- <PIF__COMPANY__LINE>6</PIF__COMPANY__LINE> -->
				<PIF__COMPANY__LINE>
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP'">
							<xsl:value-of select="string('5')"/>
						</xsl:when>
						<!-- Issue 80442 Begin -->
						<xsl:when test="$LOB = 'ACV' or $LOB = 'AFV'">
							<xsl:value-of select="string('7')"/>
						</xsl:when>
						<!-- Issue 80442 End -->
						<xsl:otherwise>
							<xsl:value-of select="string('6')"/>
						</xsl:otherwise>
					</xsl:choose>
				</PIF__COMPANY__LINE>
				<!-- Case 34768 End -->
				<PIF__PAY__SERVICE__CODE>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/> -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__PAY__SERVICE__CODE>
				<PIF__MODE__CODE>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/>	-->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__MODE__CODE>
				<!-- Case 34768 Begin -->
				<!-- <PIF__AUDIT__CODE>N</PIF__AUDIT__CODE> -->
				<PIF__AUDIT__CODE>
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('N')"/>
						</xsl:otherwise>
					</xsl:choose>
				</PIF__AUDIT__CODE>
				<!-- Case 34768 End -->
				<PIF__KIND__CODE>D</PIF__KIND__CODE>
				<PIF__VARIATION__CODE>
					<xsl:value-of select="string('0')"/>
				</PIF__VARIATION__CODE>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal">
					<xsl:if test="InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI'">
						<PIF__SORT__NAME>
							<!-- Case 34768 Begin -->
							<xsl:choose>
								<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
									<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
								</xsl:when>
								<xsl:otherwise>
									<!-- Case 34768 End -->
									<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/PersonName/Surname,1,4)"/>
								</xsl:otherwise>
								<!-- Case 34768 -->
							</xsl:choose>
							<!-- Case 34768 -->
						</PIF__SORT__NAME>
					</xsl:if>
				</xsl:for-each>
				<PIF__PRODUCER__CODE>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Producer/ProducerInfo/ProducerSubCode"/>
				</PIF__PRODUCER__CODE>
				<PIF__UNDERWRITING__CODE>
					<PIF__REVIEW__CODE>N</PIF__REVIEW__CODE>
					<PIF__MVR__REPORT__YEAR>N</PIF__MVR__REPORT__YEAR>
					<PIF__RISK__GRADE__GUIDE>5</PIF__RISK__GRADE__GUIDE>
					<PIF__RISK__GRADE__UNDWR>5</PIF__RISK__GRADE__UNDWR>
					<PIF__RENEWAL__CODE>1</PIF__RENEWAL__CODE>
				</PIF__UNDERWRITING__CODE>
				<PIF__REASON__AMENDED>
					<!-- Case 34768 Begin -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' and $ProcessingType = 'EN'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/ModInfo/ChangeDesc"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/ChangeStatus/ChangeDesc"/>
						</xsl:otherwise>
					</xsl:choose>
					<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/ChangeStatus/ChangeDesc"/>-->
					<!-- Case 34768 End -->
				</PIF__REASON__AMENDED>
				<PIF__RENEW__PAY__CODE>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/> -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__RENEW__PAY__CODE>
				<PIF__RENEW__MODE__CODE>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/> -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__RENEW__MODE__CODE>
				<PIF__RENEW__POLICY__SYMBOL>
					<xsl:value-of select="string('   ')"/>
				</PIF__RENEW__POLICY__SYMBOL>
				<PIF__RENEW__POLICY__NUMBER>
					<xsl:value-of select="string('       ')"/>
				</PIF__RENEW__POLICY__NUMBER>
				<PIF__ORIGINAL__INCEPT>
					<xsl:value-of select="string('      ')"/>
				</PIF__ORIGINAL__INCEPT>
				<PIF__CUSTOMER__NUMBER>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/AccountNumberId"/> -->
					<xsl:choose>
						<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/AccountNumberId"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/AccountNumberId"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Case 34768 End -->
				</PIF__CUSTOMER__NUMBER>
				<PIF__SPECIAL__USE__A/>
				<PIF__SPECIAL__USE__B/>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal">
					<xsl:if test="InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI'">
						<PIF__ZIP__POSTAL__CODE>
							<xsl:value-of select="substring(concat(' ',GeneralPartyInfo/Addr/PostalCode),1,6)"/>
						</PIF__ZIP__POSTAL__CODE>
						<PIF__ADDRESS__LINE__1>
							<!-- Case 34768 Begin -->
							<xsl:choose>
								<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
									<xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
								</xsl:when>
								<xsl:when test="string-length(GeneralPartyInfo/NameInfo/com.csc_LongName) &gt; 0">
									<xsl:value-of select="GeneralPartyInfo/NameInfo/com.csc_LongName"/>
								</xsl:when>
								<xsl:otherwise>
									<!-- Case 34768 End -->
									<!--xsl:value-of select="concat(GeneralPartyInfo/NameInfo/PersonName/GivenName,' ',GeneralPartyInfo/NameInfo/PersonName/SurName)"/-->			<!-- 103409 -->
									<xsl:value-of select="concat(GeneralPartyInfo/NameInfo/PersonName/GivenName,' ',GeneralPartyInfo/NameInfo/PersonName/Surname)"/>		<!-- 103409 -->
								</xsl:otherwise>
							</xsl:choose>
						</PIF__ADDRESS__LINE__1>
						<PIF__ADDRESS__LINE__2>
							<xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
						</PIF__ADDRESS__LINE__2>
						<PIF__ADDRESS__LINE__3>
							<xsl:choose>
								<!-- Case 34768 End -->
								<xsl:when test="GeneralPartyInfo/Addr/Addr2=''">
									<xsl:choose>
										<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
											<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,', ',GeneralPartyInfo/Addr/StateProvCd)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,',',GeneralPartyInfo/Addr/StateProvCd)"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<!--<xsl:when test="GeneralPartyInfo/Addr/Addr2=''"><xsl:value-of select="concat(GeneralPartyInfo/Addr/City,' ',GeneralPartyInfo/Addr/StateProvCd)"/></xsl:when>-->
								<!-- Case 34768 End -->
								<xsl:otherwise>
									<xsl:value-of select="GeneralPartyInfo/Addr/Addr2"/>
								</xsl:otherwise>
							</xsl:choose>
						</PIF__ADDRESS__LINE__3>
						<PIF__ADDRESS__LINE__4>
							<!--<xsl:value-of select="concat(substring(GeneralPartyInfo/Addr/City,1,27),' ',substring(GeneralPartyInfo/Addr/StateProvCd,29,2))"/>  -->
							<xsl:choose>
								<xsl:when test="GeneralPartyInfo/Addr/Addr2=''">
								</xsl:when>
								<xsl:otherwise>
									<!-- Case 34768 Begin -->
									<xsl:choose>
										<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
											<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,', ',GeneralPartyInfo/Addr/StateProvCd)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,',',GeneralPartyInfo/Addr/StateProvCd)"/>
										</xsl:otherwise>
									</xsl:choose>
									<!--<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,' ',GeneralPartyInfo/Addr/StateProvCd)"/>-->
									<!-- Case 34768 End -->
								</xsl:otherwise>
							</xsl:choose>
						</PIF__ADDRESS__LINE__4>
					</xsl:if>
				</xsl:for-each>
				<PIF__HISTORY__OPTION/>
				<PIF__FINAL__AUDIT__IND/>
				<!-- Case 35934 - Start -->
				<PIF__SPECIAL__AUTO__CEDED>
					<PIF__SPECIAL__USE__POS__1/>
					<PIF__SPECIAL__USE__POS__2/>
				</PIF__SPECIAL__AUTO__CEDED>
				<!-- Case 35934 - End -->
				<PIF__EXCESS__CLAIM__IND/>
				<PIF__PMS__TYPE__LEVEL>
					<xsl:value-of select="string('A')"/>
				</PIF__PMS__TYPE__LEVEL>
				<PIF__ANNIVERSARY__RERATE/>
				<!-- Case 34768 Begin -->
				<!-- <PIF__NAME__PRINT__OPTION>
						<xsl:value-of select="string('0')"/>
					</PIF__NAME__PRINT__OPTION> -->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal">
					<xsl:if test="InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI'">
						<PIF__NAME__PRINT__OPTION>
							<xsl:choose>
								<xsl:when test="$LOB = 'WCP' or $LOB = 'ACV' or $LOB = 'AFV'">	<!-- Issue 80442 Added check for ACV and AFV -->
									<xsl:value-of select="GeneralPartyInfo/NameInfo/LegalEntityCd"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('0')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PIF__NAME__PRINT__OPTION>
					</xsl:if>
				</xsl:for-each>
				<!-- Case 34768 End -->
				<PIF__LEGAL__ENTITY/>
				<PIF__REINS__PROCESS__IND/>
				<PIF__POLICY__STATUS__ON__PIF/>
				<PIF__ACA__POLICY__CODE>
					<xsl:value-of select="string('N')"/>
				</PIF__ACA__POLICY__CODE>
				<PIF__ACA__DESTINATION/>
				<PIF__PC__ADDRESS__AREA/>
				<PIF__PC__SEQUENCE/>
				<PIF__UK__POSTAL__CODE/>
				<!-- Case 35934 - Start -->
				<PIF__MOD__NUMBER>
					<PIF__RENEW__MOD/>
					<PIF__RENEW__MCO/>
					<PIF__RENEW__LOC/>
					<fill_8/>
				</PIF__MOD__NUMBER>
				<!-- Case 35934 - End -->
				<PIF__POLICY__START__TIME__A>
					<PIF__POLICY__START__TIME/>
				</PIF__POLICY__START__TIME__A>
				<PIF__INSURED__OCCUPATION__CODE/>
				<PIF__INSURED__OCCUPATION__CODE__X/>
				<!-- Case 35934 - Start -->
				<PIF__USA__INDICATORS>
					<PIF__WCP__CONVERSION__INDICATOR/>
					<fill_9/>
				</PIF__USA__INDICATORS>
				<PIF__DEC__CHANGE__SW/>
				<PIF__PMS__FUTURE__USE/>
				<PIF__ISOL__APP__ID>
					<!-- Case 34768 Begin -->
					<!-- <xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/@id"/> -->
					<!-- Case 39354 <xsl:choose>
							<xsl:when test="$LOB = 'WCP'"> -->
					<xsl:if test="string-length(/ACORD/InsuranceSvcRq/*/CommlPolicy)">
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/@id"/>
					</xsl:if>
					<!-- Case 39354 <xsl:otherwise> -->
					<xsl:if test="string-length(/ACORD/InsuranceSvcRq/*/PersPolicy)">
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/@id"/>
					</xsl:if>
					<!-- Case 39354</xsl:otherwise>
						</xsl:choose> -->
					<!-- Case 34768 End -->
				</PIF__ISOL__APP__ID>
				<PIF__CUST__FUTURE__USE/>
				<PIF__YR2000__CUST__USE/>
			</POLICY__INFORMATION__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->