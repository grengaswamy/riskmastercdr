<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series2 XML into ACORD XML
to return back to iSolutions   
E-Service case 34768 
***********************************************************************************************
-->
  <xsl:include href="S2CommonFuncRs.xsl"/>
  <xsl:include href="S2SignOnRs.xsl"/>
  <xsl:include href="S2MessageStatusRs.xsl"/>
  <xsl:include href="S2ProducerRs.xsl"/>
  <xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
  <xsl:include href="S2CommlPolicyRs.xsl"/>
  <xsl:include href="S2LocationRs.xsl"/>
  <xsl:include href="S2AdditionalInterestRs.xsl"/>
  <xsl:include href="S2WCWorkSTATERs.xsl"/>
  <xsl:include href="S2WCCommlCoverage12GPRs.xsl"/>
  <xsl:include href="S2WorkCompLocInfoRs.xsl"/>
  <xsl:include href="S2CreditSurchargeRs.xsl"/>

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="BuildSignOn"/>
      <InsuranceSvcRs>
        <RqUID/>
        <com.csc_WorkCompPolicyModRs>
          <RqUID/>
          <TransactionResponseDt/>
          <TransactionEffectiveDt/>
          <CurCd>USD</CurCd>
          <xsl:call-template name="BuildMessageStatus"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WC"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Person"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WCPolicy"/>
		<!--Create Location -->
	  <xsl:call-template name="CreateLocationFrom43SEG">
		<xsl:with-param name="LOB" select="WCP"/>
	  </xsl:call-template>
	  <WorkCompLineBusiness>
	  	<LOBCd>WCP</LOBCd>
		<xsl:for-each select="/com.csc_WorkCompPolicyModRs/PMDI4W1__STATE__RATING__REC">
			<xsl:variable name="State" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
			<xsl:if test="not(preceding-sibling::PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$State])">
				<WorkCompRateState>
					<StateProvCd>
						<xsl:call-template name="BuildStateProvCd">
							<xsl:with-param name="StateCd" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
						</xsl:call-template>
					</StateProvCd>
					<AnniversaryRatingDt/>
					<NCCIIDNumber/>
					<xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$State]">
						<xsl:if test="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE !='0000'">
							<CreditOrSurcharge>
								<CreditSurchargeCd>
									<xsl:value-of select="concat(PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE, PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__SEQ)"/>
								</CreditSurchargeCd>
								<NumericValue>
									<FormatInteger>
										<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__GENERATED__SEG__IND"/>
									</FormatInteger>
									<FormatModFactor>
										<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE"/>
									</FormatModFactor>
									<FormatCurrencyAmt>
										<xsl:choose>
											<xsl:when test="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE &gt; '0'">
												<Amt/>
											</xsl:when>
											<xsl:otherwise>
												<Amt>
													<xsl:value-of select="concat(PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM,'.00')"/>
												</Amt>
											</xsl:otherwise>
										</xsl:choose>
									</FormatCurrencyAmt>
								</NumericValue>
								<com.csc_ModifierDesc>
									<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__DESC"/>
								</com.csc_ModifierDesc>
							</CreditOrSurcharge>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="/com.csc_WorkCompPolicyModRs/PMD4J__NAME__ADDR__SEG[PMD4J__SEGMENT__DATA/PMD4J__LOCATION__STATE=$State]">
						<xsl:call-template name="CreateWorkCompLocInfo">
							<xsl:with-param name="SiteNum" select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER__N"/>
							<xsl:with-param name="StateCd" select="PMD4J__SEGMENT__DATA/PMD4J__LOCATION__STATE"/>
						</xsl:call-template>
					</xsl:for-each>
				</WorkCompRateState>
			</xsl:if>
		</xsl:for-each>

		<xsl:apply-templates select="CreateCommlCoverage"/>
		<QuestionAnswer>
			<QuestionCd/>
			<YesNoCd/>
			<Explanation/>
		</QuestionAnswer>
	  </WorkCompLineBusiness>
          <RemarkText IdRef="policy">Work Comp XML Issue</RemarkText>
        </com.csc_WorkCompPolicyModRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>

</xsl:stylesheet>