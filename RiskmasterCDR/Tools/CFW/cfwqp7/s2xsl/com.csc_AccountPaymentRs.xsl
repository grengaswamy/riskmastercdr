<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<ACORD>
			<InsuranceSvcRs>
				<com.csc_AccountPaymentRs>
					<TransactionEffectiveDt>
						<xsl:value-of select="concat(com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR,'-',com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO,'-',com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA)"/>
						</TransactionEffectiveDt>
					<MsgStatus>
						<MsgStatusCd>00</MsgStatusCd>
							<MsgStatusDesc>
								<xsl:value-of select="com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__DESC"/>
							</MsgStatusDesc>
					</MsgStatus>
					<com.csc_AccountPaymentInfo>
						<xsl:for-each select="com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER">
							<PolicyNumber>
								<xsl:value-of select="."/>
							</PolicyNumber>
						</xsl:for-each>
						<xsl:for-each select="com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__SPECIAL__USE__A">
							<MethodPaymentCd>
								<xsl:value-of select="."/>
							</MethodPaymentCd>
						</xsl:for-each>
						<com.csc_PaymentAmt>
							<xsl:for-each select="com.csc_AccountPaymentRs/POLICY__INFORMATION__SEG/PIF__SPECIAL__USE__B">
								<Amt>
									<xsl:value-of select="."/>
								</Amt>
							</xsl:for-each>
						</com.csc_PaymentAmt>
					</com.csc_AccountPaymentInfo>
				</com.csc_AccountPaymentRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Pinfo53Record.xml" userelativepaths="yes" externalpreview="no" url="file://d:\CSC's iSolutions\Development 41\Installment Payment Components\Stub XML\Pinfo53Record.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="file://d:\CSC's iSolutions\Development 41\Installment Payment Components\Stub XML\Pinfo53Record.xml" srcSchemaRoot="com.csc_AccountPaymentRs" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="file://d:\CSC's iSolutions\Development 41\Installment Payment Components\XML Message\com.csc_AccountPaymentRs.xml" destSchemaRoot="ACORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->