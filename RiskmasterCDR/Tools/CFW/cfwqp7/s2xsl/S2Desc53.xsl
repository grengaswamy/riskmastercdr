<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2Desc53Template">

		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/PersAutoLineBusiness/PersVeh/AdditionalInterest">
			<xsl:if test="(string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName) &gt; '0') or (string-length(GeneralPartyInfo/NameInfo/PersonName/GivenName) &gt; '0')">
				<BUS__OBJ__RECORD>
					<RECORD__NAME__ROW>
						<RECORD__NAME>DESCRIPTION__FULL__SEG</RECORD__NAME>
					</RECORD__NAME__ROW>
					<DESCRIPTION__FULL__SEG>
						<DESCRIPTION__SEG>
							<DESCRIPTION__REC__LLBB>
								<DESCRIPTION__REC__LENGTH/>
								<DESCRIPTION__ACTION__CODE/>
								<DESCRIPTION__FILE__ID/>
							</DESCRIPTION__REC__LLBB>
						</DESCRIPTION__SEG>
						<DESCRIPTION__ID>12</DESCRIPTION__ID>
						<DESCRIPTION__KEY>
							<DESCRIPTION__USE>
								<USE__CODE>
									<xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
								</USE__CODE>
								<USE__LOCATION>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE__LOCATION</xsl:with-param>
										<xsl:with-param name="FieldLength">3</xsl:with-param>
										<xsl:with-param name="Value" select="parent::PersVeh/ItemIdInfo/InsurerId"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</USE__LOCATION>
							</DESCRIPTION__USE>
							<DESCRIPTION__SEQUENCE>0</DESCRIPTION__SEQUENCE>
						</DESCRIPTION__KEY>
						<DESCRIPTION__INFORMATION>
							<DESCRIPTION__SORT__NAME>
								<xsl:choose>
									<xsl:when test="string-length(GeneralPartyInfo/NameInfo/PersonName/GivenName) &gt; '0'">
										<xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/GivenName"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
									</xsl:otherwise>
								</xsl:choose>
							</DESCRIPTION__SORT__NAME>
							<DESCRIPTION__ZIP__FILLER>0</DESCRIPTION__ZIP__FILLER>
							<DESCRIPTION__ZIP__CODE>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/PostalCode,1,5)"/>
							</DESCRIPTION__ZIP__CODE>
							<DESCRIPTION__LINE__1>
								<xsl:choose>
									<xsl:when test="string-length(GeneralPartyInfo/NameInfo/PersonName/GivenName) &gt; '0'">
										<xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/GivenName"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,30)"/>
									</xsl:otherwise>
								</xsl:choose>
							</DESCRIPTION__LINE__1>
							<DESCRIPTION__LINE__2>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr1,1,30)"/>
							</DESCRIPTION__LINE__2>
							<DESCRIPTION__LINE__3>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr2,1,30)"/>
							</DESCRIPTION__LINE__3>
							<DESCRIPTION__LINE__4>
								<xsl:value-of select="concat(GeneralPartyInfo/Addr/City, ',')"/>
								<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
							</DESCRIPTION__LINE__4>
						</DESCRIPTION__INFORMATION>
						<DESCRIPTION__NAME__CODE/>
						<DESC__UK__POSTAL__CODE/>
						<DESC__MISC__ADJ__ZIP__CODE/>
						<DESC__MISC__ADJ__TAX__ID/>
						<DESC__PMS__FUTURE__USE/>
						<DESCRIPTION__LOAN__NUMBER/>
						<DESC__ACA__IDENTIFIER__SAVE/>
						<DESC__CUST__FUTURE__USE/>
						<DESC__YR2000__CUST__USE/>
						<DESC__DUP__KEY__SEQ__NUM/>
					</DESCRIPTION__FULL__SEG>
				</BUS__OBJ__RECORD>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 80442 CA Additional Interests Begin -->
		<xsl:for-each select="ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/AdditionalInterest">
			<xsl:if test="(string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName) &gt; '0')">
				<xsl:variable name="IntType" select="AdditionalInterestInfo/NatureInterestCd"/>
				<xsl:variable name="ThisVeh" select="../@id"/>
				<xsl:variable name="ThisInt" select="position()"/>
				<BUS__OBJ__RECORD>
					<RECORD__NAME__ROW>
						<RECORD__NAME>DESCRIPTION__FULL__SEG</RECORD__NAME>
					</RECORD__NAME__ROW>
					<DESCRIPTION__FULL__SEG>
						<DESCRIPTION__SEG>
							<DESCRIPTION__REC__LLBB>
								<DESCRIPTION__REC__LENGTH/>
								<DESCRIPTION__ACTION__CODE/>
								<DESCRIPTION__FILE__ID/>
							</DESCRIPTION__REC__LLBB>
						</DESCRIPTION__SEG>
						<DESCRIPTION__ID>12</DESCRIPTION__ID>
						<DESCRIPTION__KEY>
							<DESCRIPTION__USE>
								<USE__CODE>
									<xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
								</USE__CODE>
								<USE__LOCATION>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">USE__LOCATION</xsl:with-param>
										<xsl:with-param name="FieldLength">3</xsl:with-param>
										<xsl:with-param name="Value" select="parent::CommlVeh/ItemIdInfo/InsurerId + 9"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
									</xsl:call-template>
								</USE__LOCATION>
							</DESCRIPTION__USE>
							<DESCRIPTION__SEQUENCE>0</DESCRIPTION__SEQUENCE>
						</DESCRIPTION__KEY>
						<DESCRIPTION__INFORMATION>
							<DESCRIPTION__SORT__NAME>
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
							</DESCRIPTION__SORT__NAME>
							<DESCRIPTION__ZIP__FILLER>0</DESCRIPTION__ZIP__FILLER>
							<DESCRIPTION__ZIP__CODE>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/PostalCode,1,5)"/>
							</DESCRIPTION__ZIP__CODE>
							<DESCRIPTION__LINE__1>
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,30)"/>
							</DESCRIPTION__LINE__1>
							<DESCRIPTION__LINE__2>
								<xsl:choose>
									<xsl:when test="string-length(GeneralPartyInfo/Addr/Addr1) &gt; '0'">
										<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr1,1,30)"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr2,1,30)"/>
									</xsl:otherwise>
								</xsl:choose>
							</DESCRIPTION__LINE__2>
							<DESCRIPTION__LINE__3>
								<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1) &gt; '0'">
									<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr2,1,30)"/>
								</xsl:if>
							</DESCRIPTION__LINE__3>
							<DESCRIPTION__LINE__4>
								<xsl:value-of select="concat(GeneralPartyInfo/Addr/City, ', ')"/>
								<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
							</DESCRIPTION__LINE__4>
						</DESCRIPTION__INFORMATION>
						<DESCRIPTION__NAME__CODE/>
						<DESC__UK__POSTAL__CODE/>
						<DESC__MISC__ADJ__ZIP__CODE/>
						<DESC__MISC__ADJ__TAX__ID/>
						<DESC__PMS__FUTURE__USE/>
						<DESCRIPTION__LOAN__NUMBER/>
						<DESC__ACA__IDENTIFIER__SAVE/>
						<DESC__CUST__FUTURE__USE/>
						<DESC__YR2000__CUST__USE/>
						<DESC__DUP__KEY__SEQ__NUM/>
					</DESCRIPTION__FULL__SEG>
				</BUS__OBJ__RECORD>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 80442 CA Additional Interests End -->
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->