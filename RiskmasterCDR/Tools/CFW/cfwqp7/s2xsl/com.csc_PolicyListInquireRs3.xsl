<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
 <!--xsl:include href="SavErrorRs.xsl"/-->
 <xsl:output method="xml" indent="yes"/>
 
  <xsl:template match="/">
     <xsl:apply-templates select="/com.csc_PolicyListInquireRs"/>
  </xsl:template>

 <xsl:template match="/com.csc_PolicyListInquireRs">
	


<ACORD>
	<SignonRs>
		<Status>
			<StatusCd/>
			<StatusDesc/>
		</Status>
		<SignonRoleCd/>
		<CustId>
			<SPName/>
			<CustLoginId/>
		</CustId>
		<GenSessKey/>
		<ClientDt/>
		<CustLangPref/>
		<ClientApp>
			<Org/>
			<Name/>
			<Version/>
		</ClientApp>
		<ServerDt/>
		<Language/>
	</SignonRs>
	<InsuranceSvcRs>
		<Status>
			<StatusCode/>
			<StatusDesc/>
			<com.csc_ServerStatusCode/>
			<com.csc_Severity/>
		</Status>
		<RqUID/>
		<com.csc_PolicyListInquireRs>
			<RqUID/>
			<TransactionResponseDt/>
			<TransactionEffectiveDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></TransactionEffectiveDt>
			<CurCd/>
			<MsgStatus>
				<MsgStatusCd/>
				<MsgStatusDesc/>
				<ExtendedStatus>
					<ExtendedStatusCd/>
					<ExtendedStatusDesc/>
					<com.csc_StatusReference/>
				</ExtendedStatus>
			</MsgStatus>
			<Producer>
				<ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd com.csc_colind="Required">S2Agency</OtherIdTypeCd>
						<OtherId/>
					</OtherIdentifier>
					
				</ItemIdInfo>
				<GeneralPartyInfo>
					<NameInfo>
						<PersonName>
							<Surname><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER" /></Surname>
							<GivenName/>
							<OtherGivenName/>
							<TitlePrefix/>
							<NameSuffix/>
						</PersonName>
						<LegalEntityCd/>
						<NonTaxIdentity>
							<NonTaxIdTypeCd/>
							<NonTaxId/>
						</NonTaxIdentity>
						<com.csc_LongName com.csc_colind="ReadOnly"><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER" /></com.csc_LongName>
					</NameInfo>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
				</GeneralPartyInfo>
				<ProducerInfo>
					<ContractNumber/>
					<ProducerSubCode/>
				</ProducerInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
			</Producer>
			<PersPolicy>
				<PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
				<CompanyProductCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS" /></CompanyProductCd>
				<LOBCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
				<NAICCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__MASTER__CO__NUMBER" /></NAICCd>
				<ControllingStateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></ControllingStateProvCd>
				<TotalAgenPrm><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></TotalAgenPrm>
				<PolicyStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__POLICY__STATUS__ON__PIF" /></PolicyStatusCd>	
				<ContractTerm>
					<EffectiveDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></EffectiveDt>
					<ExpirationDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EXPIRATION__DATE" /></ExpirationDt>
				</ContractTerm>
				<CurrentTermAmt>
					<Amt/>
				</CurrentTermAmt>
				<OriginalInceptionDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ORIGINAL__INCEPT" /></OriginalInceptionDt>
				<PayorCd/>
				<RateEffectiveDt com.csc_colind="Required"><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></RateEffectiveDt>
				<RenewalPayorCd/>
				<RenewalTerm>
					<DurationPeriod>
						<NumUnits/>
					</DurationPeriod>
				</RenewalTerm>
				<Form>
					<FormNumber/>
					<EditionDt/>
					<FormSequencingNumber/>
					<com.csc_FormId/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</Form>
				<Form>
					<FormNumber/>
					<EditionDt/>
					<FormSequencingNumber/>
					<com.csc_FormId/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</Form>
				<QuoteInfo>
					<CompanysQuoteNumber/>
					<IterationNumber/>
				</QuoteInfo>
				<com.csc_BranchCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH" /></com.csc_BranchCd>
				<com.csc_RenewalStatusCd com.csc_colind="Required">1</com.csc_RenewalStatusCd>
				<com.csc_StandardBaseTierInd/>
				<com.csc_AssignedPolicyNumberInd/>
				<com.csc_BranchInfo>
					<com.csc_BranchCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__BRANCH" /></com.csc_BranchCd>
				</com.csc_BranchInfo>
				<com.csc_ApplicationReceivedDt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></com.csc_ApplicationReceivedDt>
				<com.csc_PolicyWrittenDt com.csc_colind="ReadOnly"><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></com.csc_PolicyWrittenDt>
				<PersApplicationInfo>
					<InsuredOrPrincipal>
						<InsuredOrPrincipalInfo>
							<InsuredOrPrincipalRoleCd/>
							<PersonInfo>
								<GenderCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__SEX" /></GenderCd>
								<BirthDt>
									<BirthYear><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__YEAR" /></BirthYear>
									<BirthMonth><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__MONTH" /></BirthMonth>
									<BirthDay><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__DAY" /></BirthDay>
								</BirthDt>
								<MaritalStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__MARITAL__STATUS" /></MaritalStatusCd>
								<OccupationDesc/>
								<OccupationClassCd/>
								<MiscParty>
									<GeneralPartyInfo>
										<NameInfo>
											<PersonName>
												<Surname/>
												<GivenName><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></GivenName>
												<OtherGivenName/>
												<TitlePrefix/>
												<NameSuffix/>
											</PersonName>
											<LegalEntityCd/>
											<NonTaxIdentity>
												<NonTaxIdTypeCd/>
												<NonTaxId/>
											</NonTaxIdentity>
											<com.csc_LongName com.csc_colind="ReadOnly"><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></com.csc_LongName>
										</NameInfo>
										<Addr>
											<AddrTypeCd/>
											<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
											<Addr2/>
											<Addr3/>
											<Addr4/>
											<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
											<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
											<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" />-</PostalCode>
											<Country>United States of America</Country>
											<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
											<com.csc_NumYrsAtAddr/>
										</Addr>
										<com.csc_ItemIdInfo>
											<OtherIdentifier>
												<OtherIdTypeCd/>
												<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
											</OtherIdentifier>
											<OtherIdentifier>
												<OtherIdTypeCd/>
												<OtherId/>
											</OtherIdentifier>
										</com.csc_ItemIdInfo>
									</GeneralPartyInfo>
								</MiscParty>
								<com.csc_TaxIdentity>
									<TaxIdTypeCd/>
									<TaxId/>
								</com.csc_TaxIdentity>
								<com.csc_ItemIdInfo>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
									<OtherIdentifier>
										<OtherIdTypeCd/>
										<OtherId/>
									</OtherIdentifier>
								</com.csc_ItemIdInfo>
								<com.csc_ActionInfo>
									<ActionCd/>
									<com.csc_ReasonCd/>
								</com.csc_ActionInfo>
								<com.csc_DriversLicense>
									<DriversLicenseNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__NUMBER" /></DriversLicenseNumber>
									<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__STATE" /></StateProvCd>
								</com.csc_DriversLicense>
								<com.csc_StatusCd/>
							</PersonInfo>
						</InsuredOrPrincipalInfo>
					</InsuredOrPrincipal>
				</PersApplicationInfo>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">100</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">100</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<AccidentViolation>
					<AccidentViolationCd com.csc_colind="Required">ITN</AccidentViolationCd>
					<AccidentViolationDt/>
					<NumSurchargePoints/>
					<com.csc_MVRSourceTypeCd/>
					<com.csc_AccCredAppliesInd/>
					<com.csc_OverrideInd/>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_SequenceNbr/>
				</AccidentViolation>
				<DriverVeh DriverRef="N1112E" VehRef="N11C5F">
					<UsePct/>
					<com.csc_DriverUseCd com.csc_colind="Required">PR</com.csc_DriverUseCd>
					<com.csc_AttendSchoolInd/>
					<com.csc_PreassignedToVehicleInd/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
					<com.csc_ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_GeneralPartyInfo>
						<com.csc_ItemIdInfo>
							<OtherIdentifier>
								<OtherIdTypeCd/>
								<OtherId/>
							</OtherIdentifier>
						</com.csc_ItemIdInfo>
					</com.csc_GeneralPartyInfo>
				</DriverVeh>
				<com.csc_ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
				<com.csc_DataAffectedCd/>
				<com.csc_ActivityDescCd/>
				<com.csc_PayAgentTypeCd/>
			</PersPolicy>
			<PersAutoLineBusiness>
				<LOBCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
				<PersDriver id="N1112E">
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<GeneralPartyInfo>
						<NameInfo>
							<PersonName>
								<Surname/>
								<GivenName><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></GivenName>
								<OtherGivenName/>
								<TitlePrefix/>
								<NameSuffix/>
							</PersonName>
						</NameInfo>
						<Addr>
							<AddrTypeCd/>
							<Addr1/>
							<Addr2/>
							<Addr3/>
							<Addr4/>
							<City/>
							<StateProvCd/>
							<PostalCode/>
							<Country/>
							<County/>
						</Addr>
						<Communications>
							<PhoneInfo>
								<PhoneTypeCd/>
								<PhoneNumber/>
								<PhoneNumber/>
							</PhoneInfo>
						</Communications>
					</GeneralPartyInfo>
					<DriverInfo>
						<PersonInfo>
							<GenderCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__SEX" /></GenderCd>
							<BirthDt>
								<BirthYear><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__YEAR" /></BirthYear>
								<BirthMonth><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__MONTH" /></BirthMonth>
								<BirthDay><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__DATE__BIRTH/DOB__DAY" /></BirthDay>
							</BirthDt>	
							<DriverAge><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__AGE" /></DriverAge>
		 					<MaritalStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__MARITAL__STATUS" /></MaritalStatusCd>
							<DriverIdentification><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__ID" /></DriverIdentification>
							<DriverKey><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__KEY" /></DriverKey>
							<OccupationClassCd/>
						</PersonInfo>
						<DriversLicense>
							<DriversLicenseNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__NUMBER" /></DriversLicenseNumber>
							<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__STATE" /></StateProvCd>
							<LicenseDate>
								<DldYear><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__DATE/DLD__YEAR" /></DldYear>
								<DldMonth><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__DATE/DLD__MONTH" /></DldMonth>
								<DldDay><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__LICENSE__DATE/DLD__DAY" /></DldDay>
							</LicenseDate>
						</DriversLicense>


					</DriverInfo>
					<PersDriverInfo>
						<DriverRelationshipToApplicantCd><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__RELATION__TO__INDS" /></DriverRelationshipToApplicantCd>
						<DriverAttribute><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__ATTRIBUTE__AREA" /></DriverAttribute>
						<DriverTrainingInd/>
						<DriverTypeCd/>
						<GoodStudentCd/>
					</PersDriverInfo>
				</PersDriver>
				<PersVeh id="N11C5F">
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Manufacturer/>
					<Model><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__MAKE__DESC" /></Model>
					<ModelYear com.csc_colind="Required"><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__YEAR__MAKE" /></ModelYear>
					<VehBodyTypeCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__BODY" /></VehBodyTypeCd>
					<VehClassCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__VEH__CLASS__CODE" /></VehClassCd>
					<CostNewAmt>
						<Amt/>
					</CostNewAmt>
					<FullTermAmt>
						<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></Amt>
					</FullTermAmt>
					<TerritoryCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__TERRITORY" /></TerritoryCd>
					<VehIdentificationNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__VIN" /></VehIdentificationNumber>
					<VehSymbolCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__SYMBOL" /></VehSymbolCd>
					<com.csc_VehTypeCd com.csc_colind="Required"><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__TYPE__VEHICLE" /></com.csc_VehTypeCd>
					<VehiclePlateNUmber><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__PL__NUM__TYPE/USVD__PLATE__NUMBER" /></VehiclePlateNUmber>
					<DateLastChange>
						<YearLastChange>
							<YearChangeCC><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__YEAR/USVD__DATE__LAST__CHANGE__CC" /></YearChangeCC>
							<YearChangeYY><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__YEAR/USVD__DATE__LAST__CHANGE__YY" /></YearChangeYY>
						</YearLastChange>	
						<MonthChange><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__MONTH" /></MonthChange>
						<DayChange><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__CONTROL/USVD__DATE__LAST__CHANGE/USVD__DAY" /></DayChange>	
					</DateLastChange>					
					<com.csc_AdrSeqNbr/>
					<com.csc_StatedAmt>
						<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__STATED__AMOUNT" /></Amt>
					</com.csc_StatedAmt>
					<com.csc_CountyCd com.csc_colind="Required">051</com.csc_CountyCd>
					<AntiLockBrakeCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__ANTILOCK__DISC" /></AntiLockBrakeCd>
					<MultiCarDiscountInd/>
					<AntiTheftDeviceCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__ANTILOCK__DISC" /></AntiTheftDeviceCd>
					<RateClassCd/>
					<VehPerformanceCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__PERFORMANCE" /></VehPerformanceCd>
					<VehUseCd><xsl:value-of select="/com.csc_PolicyListInquireRs/VEHICLE__DESCRIPTION__SEG/USVD__USE__CODE" /></VehUseCd>
					<SeatBeltTypeCd/>
					<AirBagTypeCd/>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<Coverage>
						<CoverageCd/>
						<CoverageDesc/>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<LimitAppliesToCd/>
						</Limit>
						<Deductible>
							<FormatCurrencyAmt>
								<Amt/>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
						<CurrentTermAmt>
							<Amt/>
							<com.csc_OverrideInd/>
							<com.csc_ItemIdInfo/>
						</CurrentTermAmt>
						<EffectiveDt/>
						<com.csc_NetChangeAmt>
							<Amt/>
							<com.csc_OverrideInd/>
						</com.csc_NetChangeAmt>
					</Coverage>
					<com.csc_Form>
						<FormNumber/>
						<EditionDt/>
						<com.csc_SequenceNumber/>
						<com.csc_FormId com.csc_colind="Required">AU621</com.csc_FormId>
						<com.csc_OverrideInd/>
					</com.csc_Form>
					<com.csc_TransAmt>
						<Amt/>
					</com.csc_TransAmt>
				</PersVeh>
				<com.csc_Location>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Addr>
						<AddrTypeCd com.csc_colind="Required">PLG</AddrTypeCd>
						<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
						<Addr2/>
						<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
						<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
						<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" /></PostalCode>
						<Country/>
						<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
					</Addr>
					<com.csc_AdrSeqNbr/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</com.csc_Location>
				<com.csc_Location>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></OtherId>
						</OtherIdentifier>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<Addr>
						<AddrTypeCd com.csc_colind="Required">PLG</AddrTypeCd>
						<Addr1><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B" /></Addr1>
						<Addr2/>
						<City><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></City>
						<StateProvCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></StateProvCd>
						<PostalCode><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ZIP__POSTAL__CODE/PIF__ZIP__5__DIGIT__CODE" />-</PostalCode>
						<Country/>
						<County><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></County>
					</Addr>
					<com.csc_AdrSeqNbr/>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</com.csc_Location>
			</PersAutoLineBusiness>
			<PolicySummaryInfo>
				<PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
				<FullTermAmt>
					<Amt><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></Amt>
				</FullTermAmt>
				<PolicyStatusCd><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__POLICY__STATUS__ON__PIF" /></PolicyStatusCd>
				<com.csc_TransAmt>
					<Amt/>
				</com.csc_TransAmt>
			</PolicySummaryInfo>
			<com.csc_BillingInfo>
				<com.csc_BillingAccountNumber/>
				<com.csc_BillingAccountId/>
				<com.csc_PolicyNumber><xsl:value-of select="/com.csc_PolicyListInquireRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></com.csc_PolicyNumber>
				<PersonName>
					<Surname/>
					<GivenName><xsl:value-of select="/com.csc_PolicyListInquireRs/DRIVER__SEG/DRIVER__NAME" /></GivenName>
					<OtherGivenName/>
					<TitlePrefix/>
					<NameSuffix/>
				</PersonName>
			</com.csc_BillingInfo>
			<com.csc_ClaimInfo/>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
			<com.csc_AvailableFunctions>
				<com.csc_FunctionName/>
			</com.csc_AvailableFunctions>
		</com.csc_PolicyListInquireRs>
	</InsuranceSvcRs>
</ACORD>
</xsl:template>
</xsl:stylesheet>
