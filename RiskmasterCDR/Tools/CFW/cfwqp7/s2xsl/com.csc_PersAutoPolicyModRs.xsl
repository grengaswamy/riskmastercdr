<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<!--Common Include Files-->
	<xsl:include href="S2CommonFuncRs.xsl"/>
	<xsl:include href="S2SignOnRs.xsl"/>
	<xsl:include href="S2ProducerRs.xsl"/>
	<xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
	<xsl:include href="S2AdditionalInterestRs.xsl"/>
	<xsl:include href="S2CreditSurchargeRs.xsl"/>
	<xsl:include href="S2FormsRs.xsl"/>
	<xsl:include href="S2AccidentViolationRs.xsl"/>
	<xsl:include href="S2PACoverageRs.xsl"/>
	<xsl:include href="S2PersPolicyRs.xsl"/>
	<xsl:include href="S2MessageStatusRs.xsl"/>
	<!-- FD15 -->

	<!--Personal Auto Include Files-->
	<xsl:include href="S2PADriverVehRs.xsl"/>
	<xsl:include href="S2PAPersDriverRs.xsl"/>
	<xsl:include href="S2PAPersVehRs.xsl"/>

	<xsl:template match="/">
		<ACORD>
			<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<com.csc_PersAutoPolicyModRs>
					<!--Modified RqUID tag for Issue 35934 Start-->
					<RqUID>
						<xsl:value-of select="com.csc_PersAutoPolicyModRs/POLICY__INFORMATION__SEG/PIF__ISOL__APP__ID"/>
					</RqUID>
					<TransactionResponseDt/>
					<TransactionEffectiveDt/>
					<CurCd>USD</CurCd>					
					<xsl:call-template name="BuildMessageStatus"/>
					<!-- FD15 -->
					<!-- Determine the Line of Business for Response-->
					<xsl:for-each select="com.csc_PersAutoPolicyModRs/POLICY__INFORMATION__SEG">
						<xsl:variable name="LOB" select="PIF__LINE__BUSINESS"/>

						<xsl:if test="$LOB='APV' or $LOB='ATV'">
							<!-- PA -->
							<!--com.csc_PersAutoPolicy-->
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Producer"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Person"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="PersPolicy">
								<xsl:with-param name="LOB" select="$LOB"/>
							</xsl:apply-templates>
							<PersAutoLineBusiness>
								<!--Modified for Issue 35934<LOBCd/>-->
								<LOBCd>
									<xsl:value-of select="PIF__LINE__BUSINESS"/>
								</LOBCd>
								<xsl:call-template name="CreatePersDriver"/>
								<xsl:call-template name="CreatePersVeh"/>
								<!--Added for Issue 35934 Start-->
								<QuestionAnswer>
									<QuestionCd/>
									<YesNoCd/>
									<Explanation/>
									<com.csc_Question/>
								</QuestionAnswer>
								<!--Added for Issue 35934 End-->
							</PersAutoLineBusiness>
							<!--/com.csc_PersAutoPolicy-->
						</xsl:if>
					</xsl:for-each>
				</com.csc_PersAutoPolicyModRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->