<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Person">
		<xsl:variable name="LOB" select="PIF__LINE__BUSINESS"/> <!-- Issue 39354 -->
		<InsuredOrPrincipal>
			<ItemIdInfo>
				<AgencyId/>
				<InsurerId/>
				<SystemId/>
				<OtherIdentifier>
					<OtherIdTypeCd/>
					<OtherId/>
				</OtherIdentifier>
			</ItemIdInfo>
			<GeneralPartyInfo>
				<NameInfo>
					<PersonName>
						<Surname>
							<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
						</Surname>
						<!--Issue 35934 <GivenName/>-->
						<GivenName>
							<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
						</GivenName>
						<OtherGivenName/>
						<TitlePrefix/>
						<NameSuffix/>
					</PersonName>
					<xsl:choose>
						<xsl:when test="PIF__VARIATION__CODE='A'">
							<LegalEntityCd>AS</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='C'">
							<LegalEntityCd>CP</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='H'">
							<LegalEntityCd>CH</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='I'">
							<LegalEntityCd>IN</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='J'">
							<LegalEntityCd>JV</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='N'">
							<LegalEntityCd>NA</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='O'">
							<LegalEntityCd>OT</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='P'">
							<LegalEntityCd>PA</LegalEntityCd>
						</xsl:when>
						<xsl:when test="PIF__VARIATION__CODE='T'">
							<LegalEntityCd>HP</LegalEntityCd>
						</xsl:when>
					</xsl:choose>
					<com.csc_LongName>
						<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
					</com.csc_LongName>
					<com.csc_OtherIdentifier/>
				</NameInfo>
				<Addr>
					<!--Issue 35934
					<AddrTypeCd/>-->
					<AddrTypeCd>StreetAddress</AddrTypeCd>
					<Addr1>
						<!-- Issue 39354 Begin -->
						<!-- xsl:value-of select="concat(PIF__ADDRESS__LINE__2/PIF__INSURED__NAME__CONT,PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B)"/ -->
						<xsl:choose>
							<xsl:when test="$LOB = 'APV' or $LOB = 'ATV'">
								<xsl:value-of select="string(' ')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="PIF__ADDRESS__LINE__2"/>
							</xsl:otherwise>
						</xsl:choose>
						<!-- Issue 39354 End -->
						<!--<xsl:value-of select="PIF__ADDRESS__LINE__3"/> Modified for Issue 35934-->
					</Addr1>
					<Addr2>
						<!-- Issue 39354 Begin -->
						<!-- xsl:value-of select="PIF__ADDRESS__LINE__3"/ -->
						<!-- xsl:value-of select="concat(PIF__ADDRESS__LINE__2/PIF__INSURED__NAME__CONT,PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B)"/ -->
						<xsl:choose>
							<xsl:when test="$LOB = 'APV' or $LOB = 'ATV'">
								<xsl:value-of select="PIF__ADDRESS__LINE__2"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="PIF__ADDRESS__LINE__3"/>
							</xsl:otherwise>
						</xsl:choose>
						<!-- Issue 39354 End -->
					</Addr2>
					<Addr3/>
					<Addr4/>
					<xsl:choose>
						<xsl:when test="PIF__ADDRESS__LINE__4 != ''">
							<City>
								<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__4,',')"/>
							</City>
							<StateProvCd>
								<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__4,',')"/>
							</StateProvCd>
						</xsl:when>
						<xsl:when test="PIF__ADDRESS__LINE__3 != '' and PIF__ADDRESS__LINE__4 = ''">
							<City>
								<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__3,',')"/>
							</City>
							<StateProvCd>
								<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__3,',')"/>
							</StateProvCd>
						</xsl:when>
						<!--Added for Issue 35934 Start-->
						<xsl:otherwise>
							<City/>
							<StateProvCd/>
						</xsl:otherwise>
						<!--Added for Issue 35934 End-->
					</xsl:choose>
					<StateProv/>
					<PostalCode>
						<xsl:variable name="PostCd">
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)= '5'">
								<!-- <xsl:value-of select="concat(PIF__ZIP__POSTAL__CODE,string('-0000'))"/> -->
								<xsl:value-of select="PIF__ZIP__POSTAL__CODE"/>
							</xsl:if>
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)!= '5'">
								<!-- <xsl:value-of select="concat(substring(PIF__ZIP__POSTAL__CODE,2,5),string('-0000'))"/> -->
								<xsl:value-of select="substring(PIF__ZIP__POSTAL__CODE,2,5)"/>
							</xsl:if>
						</xsl:variable>
						<xsl:value-of select="$PostCd"/>
						<xsl:value-of select="string('-0000')"/>
					</PostalCode>
					<!--Modifiedfor Issue 35934 Start-->
					<CountryCd>USA</CountryCd>
					<!--<CountryCd/>
					<Country/>-->
					<!--Modifiedfor Issue 35934 End-->
					<County/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber>
							<xsl:value-of select="PIF__CUST__PHONE"/>
						</PhoneNumber>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<InsuredOrPrincipalInfo>
				<!--Modified for Issue 35934-->
				<!--				<InsuredOrPrincipalRoleCd/>-->
				<InsuredOrPrincipalRoleCd>FNI</InsuredOrPrincipalRoleCd>
				<PersonInfo>
					<GenderCd/>
					<BirthDt/>
					<MaritalStatusCd/>
					<OccupationDesc/>
					<OccupationClassCd/>
					<LengthTimeEmployed>
						<NumUnits/>
						<UnitMeasurementCd/>
					</LengthTimeEmployed>
					<LengthTimeCurrentOccupation>
						<NumUnits/>
						<UnitMeasurementCd/>
					</LengthTimeCurrentOccupation>
					<LengthTimeWithPreviousEmployer>
						<NumUnits/>
						<UnitMeasurementCd/>
					</LengthTimeWithPreviousEmployer>
				</PersonInfo>
			</InsuredOrPrincipalInfo>
			<LengthTimeKnownByAgentBroker>
				<NumUnits/>
				<UnitMeasurementCd/>
			</LengthTimeKnownByAgentBroker>
		</InsuredOrPrincipal>
	</xsl:template>
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Business">
		<xsl:param name="LOB"/>
		<InsuredOrPrincipal>
			<ItemIdInfo>
				<AgencyId/>
				<InsurerId/>
				<SystemId/>
				<OtherIdentifier>
					<OtherIdTypeCd/>
					<OtherId/>
				</OtherIdentifier>
			</ItemIdInfo>
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName>
							<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
						</CommercialName>
						<IndexName/>
						<SupplementaryNameInfo>
							<xsl:choose>
								<xsl:when test="PIF__ADDRESS__LINE__4 != '' and PIF__ADDRESS__LINE__2/PIF__INSURED__NAME__CONT != '*'">
									<SupplementaryNameCd>DBA</SupplementaryNameCd>
									<SupplementaryName>
										<xsl:value-of select="concat(PIF__ADDRESS__LINE__2/PIF__INSURED__NAME__CONT,PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B)"/>
									</SupplementaryName>
								</xsl:when>
								<xsl:otherwise>
									<SupplementaryNameCd/>
									<SupplementaryName/>
								</xsl:otherwise>
							</xsl:choose>
						</SupplementaryNameInfo>
					</CommlName>
					<xsl:choose>
						<xsl:when test="$LOB='WCP'">
							<LegalEntityCd>
								<xsl:value-of select="PIF__NAME__PRINT__OPTION"/>
							</LegalEntityCd>
						</xsl:when>
						<xsl:otherwise>
							<LegalEntityCd>
								<xsl:value-of select="PIF__VARIATION__CODE"/>
							</LegalEntityCd>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$LOB='WCP'">
						<xsl:if test="normalize-space(//PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__FED__EMP__ID__NUMBER)!=''">
							<TaxIdentity>
								<TaxIdTypeCd>FEIN</TaxIdTypeCd>
								<TaxId>
									<xsl:value-of select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__FED__EMP__ID__NUMBER"/>
								</TaxId>
							</TaxIdentity>
						</xsl:if>

						<xsl:for-each select="/*/DESCRIPTION__FULL__SEG">
							<xsl:if test="normalize-space(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1__SPLIT__OUT/DESCRIPTION__FED__EMP__NUM)!='' and DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE='FI'">
								<TaxIdentity>
									<TaxIdTypeCd>SSN</TaxIdTypeCd>
									<TaxId>
										<xsl:value-of select="normalize-space(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1__SPLIT__OUT/DESCRIPTION__FED__EMP__NUM)"/>
									</TaxId>
								</TaxIdentity>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					<com.csc_LongName>
						<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
					</com.csc_LongName>
					<com.csc_OtherIdentifier/>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<xsl:choose>
						<xsl:when test="PIF__ADDRESS__LINE__4 != ''">
							<Addr1>
								<xsl:value-of select="PIF__ADDRESS__LINE__3"/>
							</Addr1>
							<Addr2>
								<xsl:value-of select="PIF__ADDRESS__LINE__4"/>
							</Addr2>
							<!-- Issue 35934 	<Addr2/> -->
						</xsl:when>
						<xsl:otherwise>
							<Addr1>
								<!-- Issue 39354 Begin -->
								<!-- xsl:value-of select="concat(PIF__ADDRESS__LINE__2/PIF__INSURED__NAME__CONT,PIF__ADDRESS__LINE__2/PIF__ADDRESS__LINE__2__B)"/ -->
								<xsl:value-of select="PIF__ADDRESS__LINE__2"/>
								<!-- Issue 39354 End -->
							</Addr1>
							<!-- Issue 35934 -->
							<Addr2>
								<xsl:value-of select="PIF__ADDRESS__LINE__3"/>
							</Addr2>
							<!-- Issue 35934 	<Addr2/> -->
							<!-- <Addr2/>  55614 -->
						</xsl:otherwise>
					</xsl:choose>
					<Addr3/>
					<Addr4/>
					<xsl:choose>
						<xsl:when test="PIF__ADDRESS__LINE__4 != ''">
							<!-- Case 39354 Begin  City can have spaces in its name -->
							<xsl:variable name="State4">
								<!--<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__4,',')"/>-->	<!-- issue #99268 -->
								<xsl:value-of select="normalize-space(substring-after(PIF__ADDRESS__LINE__4,','))"/> <!-- issue #99268 -->
							</xsl:variable>
							<xsl:variable name="City4">
								<xsl:choose>
									<xsl:when test="string-length($State4) = '2'">
										<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__4,',')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(PIF__ADDRESS__LINE__4,1, string-length(PIF__ADDRESS__LINE__4) - 3)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<!-- Case 39354 End -->
							<City>
								<!-- Case 39354 Begin -->
								<!-- xsl:value-of select="substring-before(PIF__ADDRESS__LINE__4,',')"/ -->
								<xsl:value-of select="$City4"/>
								<!-- Case 39354 End -->
							</City>
							<StateProvCd>
								<!-- Case 39354 Begin -->
								<!-- xsl:value-of select="normalize-space(substring-after(PIF__ADDRESS__LINE__4,', '))"/ -->
								<xsl:choose>
									<xsl:when test="string-length($State4) = '2'">
										<xsl:value-of select="$State4"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(PIF__ADDRESS__LINE__4,string-length($City4) + 1, 2)"/>
									</xsl:otherwise>
								</xsl:choose>
								<!-- Case 39354 End -->
							</StateProvCd>
						</xsl:when>
						<xsl:otherwise>
							<!-- Case 39354 Begin  City can have spaces in its name -->
							<xsl:variable name="State3">
								<!--<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__3,',')"/>--> <!-- issue #99268 -->
								<xsl:value-of select="normalize-space(substring-after(PIF__ADDRESS__LINE__3,','))"/> <!-- issue #99268 -->
							</xsl:variable>
							<xsl:variable name="City3">
								<xsl:choose>
									<xsl:when test="string-length($State3) = '2'">
										<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__3,',')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(PIF__ADDRESS__LINE__3,1, string-length(PIF__ADDRESS__LINE__3) - 3)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<!-- Case 39354 End -->
							<City>
								<!-- Case 39354 Begin -->
								<!-- xsl:value-of select="substring-before(PIF__ADDRESS__LINE__3,',')"/ -->
								<xsl:value-of select="$City3"/>
								<!-- Case 39354 End -->
							</City>
							<StateProvCd>
								<!-- Case 39354 Begin -->
								<!-- xsl:value-of select="normalize-space(substring-after(PIF__ADDRESS__LINE__3,', '))"/ -->
								<xsl:choose>
									<xsl:when test="string-length($State3) = '2'">
										<xsl:value-of select="$State3"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring(PIF__ADDRESS__LINE__3,string-length($City3) + 2, 2)"/>
									</xsl:otherwise>
								</xsl:choose>
								<!-- Case 39354 End -->
							</StateProvCd>
						</xsl:otherwise>
					</xsl:choose>
					<StateProv/>
					<PostalCode>
						<xsl:variable name="PostCd">
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)= '5'">
								<xsl:value-of select="PIF__ZIP__POSTAL__CODE"/>
							</xsl:if>
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)!= '5'">
								<xsl:value-of select="substring(PIF__ZIP__POSTAL__CODE,2,5)"/>
							</xsl:if>
						</xsl:variable>
						<xsl:value-of select="$PostCd"/>
						<!--<xsl:value-of select="string('-0000')"/>-->	<!-- issue #99268 -->
						<xsl:value-of select="string('-9999')"/>	<!-- issue #99268 -->
					</PostalCode>
					<CountryCd/>
					<Country/>
					<County/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber>
							<xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__CUST__PHONE"/>
						</PhoneNumber>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<InsuredOrPrincipalInfo>
				<InsuredOrPrincipalRoleCd>NI</InsuredOrPrincipalRoleCd>
				<PersonInfo>
					<GenderCd/>
					<BirthDt/>
					<MaritalStatusCd/>
					<OccupationDesc/>
					<OccupationClassCd/>
					<LengthTimeEmployed>
						<NumUnits/>
						<UnitMeasurementCd/>
					</LengthTimeEmployed>
					<LengthTimeCurrentOccupation>
						<NumUnits>
							<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__BUSINESS__EXPERIENCE"/>
						</NumUnits>
						<xsl:if test="normalize-space(//UNDGI__SEGMENT/UNDGI__BUSINESS__EXPERIENCE) != ''">
							<!-- test added 10/07/03 -->
							<UnitMeasurementCd>Years</UnitMeasurementCd>
						</xsl:if>
					</LengthTimeCurrentOccupation>
					<LengthTimeWithPreviousEmployer>
						<NumUnits/>
						<UnitMeasurementCd/>
					</LengthTimeWithPreviousEmployer>
				</PersonInfo>
			</InsuredOrPrincipalInfo>
			<LengthTimeKnownByAgentBroker>
				<NumUnits/>
				<UnitMeasurementCd/>
			</LengthTimeKnownByAgentBroker>
		</InsuredOrPrincipal>
	</xsl:template>
</xsl:stylesheet>