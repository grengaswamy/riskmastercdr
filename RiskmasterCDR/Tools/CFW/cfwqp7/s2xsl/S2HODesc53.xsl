<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2HODescTemplate">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location/AdditionalInterest">
			<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
				<BUS__OBJ__RECORD>
					<RECORD__NAME__ROW>
						<RECORD__NAME>DESCRIPTION__FULL__SEG</RECORD__NAME>
					</RECORD__NAME__ROW>
					<DESCRIPTION__FULL__SEG>
						<DESCRIPTION__SEG>
							<DESCRIPTION__REC__LLBB>
								<DESCRIPTION__REC__LENGTH/>
								<DESCRIPTION__ACTION__CODE/>
								<DESCRIPTION__FILE__ID/>
							</DESCRIPTION__REC__LLBB>
						</DESCRIPTION__SEG>
						<DESCRIPTION__ID>12</DESCRIPTION__ID>
						<DESCRIPTION__KEY>
							<DESCRIPTION__USE>
								<USE__CODE>
									<xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
								</USE__CODE>
								<UNIT__NUMBER><xsl:value-of select="string('000')"/></UNIT__NUMBER>
							</DESCRIPTION__USE>
							<DESCRIPTION__SEQUENCE>0</DESCRIPTION__SEQUENCE>
						</DESCRIPTION__KEY>
						<DESCRIPTION__INFORMATION>
							<DESCRIPTION__SORT__NAME>
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
							</DESCRIPTION__SORT__NAME>
							<DESCRIPTION__ZIP__FILLER>0</DESCRIPTION__ZIP__FILLER>
							<DESCRIPTION__ZIP__CODE>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/PostalCode,1,5)"/>
							</DESCRIPTION__ZIP__CODE>
							<DESCRIPTION__LINE__1>
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,30)"/>
							</DESCRIPTION__LINE__1>
							<DESCRIPTION__LINE__2>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr1,1,30)"/>
							</DESCRIPTION__LINE__2>
							<DESCRIPTION__LINE__3>
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr2,1,30)"/>
							</DESCRIPTION__LINE__3>
							<DESCRIPTION__LINE__4>
								<xsl:if test="string-length(GeneralPartyInfo/Addr/City) &lt; 27">
									<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,', ')"/>
									<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
								</xsl:if>
								<xsl:if test="string-length(GeneralPartyInfo/Addr/City) &gt; 26">
									<xsl:value-of select="concat(substring(GeneralPartyInfo/Addr/City,1,26),', ')"/>
									<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
								</xsl:if>
							</DESCRIPTION__LINE__4>
						</DESCRIPTION__INFORMATION>
						<DESCRIPTION__NAME__CODE/>
						<DESC__UK__POSTAL__CODE/>
						<DESC__MISC__ADJ__ZIP__CODE/>
						<DESC__MISC__ADJ__TAX__ID/>
						<DESC__PMS__FUTURE__USE/>
						<DESCRIPTION__LOAN__NUMBER>
							<xsl:variable name="LoanAccNumber" select="AdditionalInterestInfo/AccountNumberId"/>
							<xsl:value-of select="substring(concat('0000000000',$LoanAccNumber),1+string-length($LoanAccNumber),10)"/>
						</DESCRIPTION__LOAN__NUMBER>
						<DESC__ACA__IDENTIFIER__SAVE/>
						<DESC__CUST__FUTURE__USE/>
						<DESC__YR2000__CUST__USE/>
						<DESC__DUP__KEY__SEQ__NUM/>
					</DESCRIPTION__FULL__SEG>
				</BUS__OBJ__RECORD>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="WaterCraft.xml" userelativepaths="yes" externalpreview="no" url="WaterCraft.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\WaterCraft.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\desc53.xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->