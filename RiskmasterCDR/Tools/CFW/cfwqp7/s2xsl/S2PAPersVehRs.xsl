<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template name="CreatePersVeh">
		<xsl:for-each select="/*/VEHICLE__DESCRIPTION__SEG">
			<PersVeh>
				<xsl:variable name="UnitNo">
					<!--Modified for Issue 35934-->
					<!--<xsl:value-of select="concat(./USVD__KEY/USVD__UNIT__NUMBER/fill_0,./USVD__KEY/USVD__UNIT__NUMBER/USVD__NUNIT__LST__DIGIT)"/>-->
					<!-- Issue 39354 Begin -->
					<!-- xsl:value-of select="./USVD__KEY/USVD__UNIT__NUMBER/USVD__NUNIT__LST__DIGIT"/ -->
					<xsl:choose>
						<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 2) = '00'">
							<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 3, 1)"/>
						</xsl:when>
						<xsl:when test="substring(./USVD__KEY/USVD__UNIT__NUMBER, 1, 1) = '0'">
							<xsl:value-of select="substring(./USVD__KEY/USVD__UNIT__NUMBER, 2, 2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="./USVD__KEY/USVD__UNIT__NUMBER"/>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 39354 End -->
				</xsl:variable>
				<xsl:attribute name="id">
					<xsl:value-of select="concat('v',$UnitNo)"/>
				</xsl:attribute>
				<ItemIdInfo>
					<AgencyId/>
					<InsurerId>
						<xsl:value-of select="$UnitNo"/>
					</InsurerId>
					<SystemId/>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</ItemIdInfo>
				<Manufacturer>
					<xsl:value-of select="substring-before(./USVD__MAKE__DESC,' ')"/>
				</Manufacturer>
				<Model>
					<xsl:value-of select="substring-after(./USVD__MAKE__DESC,' ')"/>
				</Model>
				<ModelYear>
					<xsl:value-of select="concat(./USVD__YEAR__MAKE/USVD__YEAR__CC,./USVD__YEAR__MAKE/USVD__YEAR__YY)"/>
				</ModelYear>
				<VehBodyTypeCd>
					<xsl:value-of select="./USVD__TYPE__VEHICLE"/>
				</VehBodyTypeCd>
				<CostNewAmt>
					<Amt>
						<xsl:choose>
							<xsl:when test="string-length(./USVD__COST__NEW)='0'">0</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="./USVD__COST__NEW"/>
							</xsl:otherwise>
						</xsl:choose>
					</Amt>
				</CostNewAmt>
				<NumDaysDrivenPerWeek>
					<xsl:value-of select="normalize-space(./USVD__DAYS__PER__WEEK)"/>
				</NumDaysDrivenPerWeek>
				<EstimatedAnnualDistance>
					<NumUnits>
						<xsl:value-of select="./USVD__ANNUAL__MILES"/>
					</NumUnits>
					<UnitMeasurementCd>MI</UnitMeasurementCd>
				</EstimatedAnnualDistance>
				<FullTermAmt>
					<Amt/>
					<CurCd/>
				</FullTermAmt>
				<Displacement>
					<NumUnits/>
					<UnitMeasurementCd/>
				</Displacement>
				<LeasedVehInd/>
				<LeasedDt/>
				<LicensePlateEffectiveDt/>
				<LicensePlateExpirationDt/>
				<LicensePlateNumber/>
				<NumCylinders/>
				<PurchaseDt/>
				<RegistrationStateProvCd>
					<xsl:call-template name="BuildStateProvCd">
						<xsl:with-param name="StateCd" select="./USVD__STATE"/>
					</xsl:call-template>
				</RegistrationStateProvCd>
				<TerritoryCd>
					<xsl:value-of select="./USVD__TERRITORY"/>
				</TerritoryCd>
				<VehIdentificationNumber>
					<xsl:value-of select="./USVD__SERIAL__NUMBER"/>
				</VehIdentificationNumber>
				<VehSymbolCd>
					<xsl:value-of select="./USVD__SYMBOL"/>
				</VehSymbolCd>
				<xsl:variable name="PASS_LOB">APV</xsl:variable>
				<xsl:call-template name="CreateAdditionalInterestFrom12SEG">
					<xsl:with-param name="PASS_LOB" select="$PASS_LOB"/>
					<xsl:with-param name="UseCode"/>
					<xsl:with-param name="UnitNo" select="$UnitNo"/>
				</xsl:call-template>
				<AlteredInd/>
				<AntiLockBrakeCd>
					<xsl:value-of select="./USVD__ANTILOCK__DISC"/>
				</AntiLockBrakeCd>
				<AntiTheftDeviceCreditPct/>
				<BumperDiscountInd/>
				<CarpoolInd/>
				<DamageabilityCd/>
				<DaytimeRunningLightInd>
					<xsl:value-of select="./USVD__DAY__LIGHTS"/>
				</DaytimeRunningLightInd>
				<EngineTypeCd/>
				<ExistingUnrepairedDamageInd/>
				<GaragingCd/>
				<Length>
					<NumUnits/>
					<UnitMeasurementCd/>
				</Length>
				<MaximumSpeed>
					<NumUnits/>
					<UnitMeasurementCd/>
				</MaximumSpeed>
				<DistanceOneWay>
					<NumUnits>
						<xsl:value-of select="./USVD__MILES__TO__WORK"/>
					</NumUnits>
					<UnitMeasurementCd/>
				</DistanceOneWay>
				<MultiCarDiscountInd>
					<xsl:value-of select="./USVD__MULTICAR__OVRRD"/>
				</MultiCarDiscountInd>
				<NewVehInd/>
				<NonOwnedVehInd/>
				<NumAxles/>
				<LengthTimePerMonth>
					<NumUnits/>
					<UnitMeasurementCd/>
				</LengthTimePerMonth>
				<NumYouthfulOperators/>
				<OdometerReading>
					<NumUnits/>
					<UnitMeasurementCd/>
				</OdometerReading>
				<PhysicalDamageRateClassCd/>
				<PricingCd/>
				<AntiTheftDeviceCd>
					<xsl:value-of select="./USVD__ANTITHEFT__DEVICE"/>
				</AntiTheftDeviceCd>
				<PrincipalOperatorInd/>
				<RateClassCd/>
				<ResidualMarketFacilityInd/>
				<SeenCarInd/>
				<TerritoryCodeCommutingDestinationCd/>
				<TractionControlInd/>
				<RegisteredVehInd/>
				<AlterationsAmt>
					<Amt/>
					<CurCd/>
				</AlterationsAmt>
				<VehInspectionStatusCd/>
				<VehPerformanceCd>
					<xsl:value-of select="./USVD__PERFORMANCE"/>
				</VehPerformanceCd>
				<VehSalvageTitleNumber/>
				<VehUseCd>
					<xsl:value-of select="./USVD__USE__CODE"/>
				</VehUseCd>
				<FourWheelDriveInd/>
				<!--Removed for Issue 35934
				<QuestionAnswer>
					<QuestionCd/>
					<YesNoCd/>
					<Explanation/>
					<com.csc_Question/>
				</QuestionAnswer>-->
				<!--<xsl:if test="./USVD__PASSIVE__RESTRAINT='D' or ./USVD__PASSIVE__RESTRAINT='Y' or ./USVD__PASSIVE__RESTRAINT='A' or ./USVD__PASSIVE__RESTRAINT='N'">
					<SeatBeltTypeCd>
						<xsl:value-of select="./USVD__PASSIVE__RESTRAINT"/>
					</SeatBeltTypeCd>
				</xsl:if>
				<xsl:if test="./USVD__PASSIVE__RESTRAINT='D' or ./USVD__PASSIVE__RESTRAINT='B' or ./USVD__PASSIVE__RESTRAINT='C' or ./USVD__PASSIVE__RESTRAINT='N'">-->
				<AirBagTypeCd>
					<xsl:value-of select="./USVD__PASSIVE__RESTRAINT"/>
				</AirBagTypeCd>
				<!--</xsl:if>-->
				<xsl:apply-templates select="/*/VEHICLE__COVERAGE__SEG[VCR__KEY/VCR__UNIT__NUMBER=$UnitNo]" mode="CreateCoverage"/>				
				<com.csc_StatedAmt>
					<Amt>
						<xsl:value-of select="./USVD__STATED__AMOUNT__NUM"/>
					</Amt>
					<CurCd/>
				</com.csc_StatedAmt>
				<NumCylinders>
					<xsl:value-of select="./USVD__CUBIC__CM"/>
				</NumCylinders>
				<com.csc_TaxCodeInfo>
					<TaxCd>
						<xsl:value-of select="./USVD__TAX__LOCATION"/>
					</TaxCd>
				</com.csc_TaxCodeInfo>								
			</PersVeh>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\InternalXML_Rs.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->