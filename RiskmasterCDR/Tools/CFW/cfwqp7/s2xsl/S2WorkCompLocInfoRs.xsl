<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform Series II records into internal XML
prior to posting data to the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template name="CreateWorkCompLocInfo">
		<xsl:param name="StateCd"/>

				<WorkCompLocInfo>
					<xsl:attribute name="LocationRef">
						<!-- <xsl:value-of select="concat('l',$StateCd)"/> -->
						<xsl:value-of select="concat('l',PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__LOCATION__NUMBER)"/>
					</xsl:attribute>
					<WorkCompRateClass>
						<ActualRemunerationAmt>
							<Amt>
								<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__CLASS__TERM__PREM"/>
							</Amt>
						</ActualRemunerationAmt>
						<NumEmployees/>
						<NumEmployeesFullTime/>
						<NumEmployeesPartTime/>
						<Rate>
							<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__RATE"/>
						</Rate>
						<RatingClassificationCd>
							<xsl:value-of select="PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__RISK__UNIT/PMDU4W1__CLASS__CODE"/>
							<xsl:value-of select="PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__RISK__UNIT/PMDU4W1__CLASS__CODE__SEQ"/>
						</RatingClassificationCd>
						<Exposure>
							<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__PREMIUM__BASIS__AMT"/>
						</Exposure>
						<ItemIdInfo>
							<OtherIdentifier>
								<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
								<OtherId/>
							</OtherIdentifier>
						</ItemIdInfo>
					</WorkCompRateClass>
				</WorkCompLocInfo>

	</xsl:template>

<!-- State level premium totals -->
	<xsl:template name="CreateWorkCompLocInfoTotals">
		<xsl:param name="StateCd"/>
		<xsl:param name="StateUPrem"/>
		<xsl:param name="StateTPrem"/>


		<WorkCompLocInfo>
			<xsl:attribute name="LocationRef">
				<xsl:value-of select="concat('l',$StateCd)"/>
			</xsl:attribute>
			<WorkCompRateClass>
				<ActualRemunerationAmt>
					<Amt>
						<xsl:value-of select="$StateUPrem"/>
					</Amt>
				</ActualRemunerationAmt>
				<NumEmployees/>
				<NumEmployeesFullTime/>
				<NumEmployeesPartTime/>
				<Rate>
					<xsl:value-of select="string('0000')"/>
				</Rate>
				<RatingClassificationCd>
					<xsl:value-of select="string('XXXM')"/>
				</RatingClassificationCd>
				<Exposure>
					<xsl:value-of select="string('0000')"/>
				</Exposure>
				<ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
						<OtherId/>
					</OtherIdentifier>
				</ItemIdInfo>
			</WorkCompRateClass>
		</WorkCompLocInfo>

		<!-- xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$StateCd]" -->
		<xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$StateCd]">
			<xsl:variable name="ClassCode" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
			<xsl:variable name="ClassOrder" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE"/>
			<xsl:choose>
				<xsl:when test="$ClassCode != '0063' and $ClassCode !='0900' and $ClassCode != '    ' and $ClassOrder != '0000' and $ClassOrder != '0250'">
					<WorkCompLocInfo>
						<xsl:attribute name="LocationRef">
							<xsl:value-of select="concat('l',$StateCd)"/>
						</xsl:attribute>
						<WorkCompRateClass>
							<ActualRemunerationAmt>
								<Amt>
									<xsl:choose>
										<xsl:when test="$ClassCode='9880'">
											<xsl:value-of select="string('-')"/>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:otherwise>
									</xsl:choose>
								</Amt>
							</ActualRemunerationAmt>
							<NumEmployees/>
							<NumEmployeesFullTime/>
							<NumEmployeesPartTime/>
							<Rate>
								<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE"/>
							</Rate>
							<RatingClassificationCd>
								<xsl:value-of select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
								<xsl:value-of select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__SEQ"/>
							</RatingClassificationCd>
							<Exposure>
								<xsl:value-of select="string('0000')"/>
							</Exposure>
							<ItemIdInfo>
								<OtherIdentifier>
									<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
									<OtherId/>
								</OtherIdentifier>
							</ItemIdInfo>
						</WorkCompRateClass>
					</WorkCompLocInfo>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:for-each>

		<WorkCompLocInfo>
			<xsl:attribute name="LocationRef">
				<xsl:value-of select="concat('l',$StateCd)"/>
			</xsl:attribute>
			<WorkCompRateClass>
				<ActualRemunerationAmt>
					<Amt>
						<xsl:value-of select="$StateTPrem"/>
					</Amt>
				</ActualRemunerationAmt>
				<NumEmployees/>
				<NumEmployeesFullTime/>
				<NumEmployeesPartTime/>
				<Rate>
					<xsl:value-of select="string('0000')"/>
				</Rate>
				<RatingClassificationCd>
					<xsl:value-of select="string('XXXS')"/>
				</RatingClassificationCd>
				<Exposure>
					<xsl:value-of select="string('0000')"/>
				</Exposure>
				<ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
						<OtherId/>
					</OtherIdentifier>
				</ItemIdInfo>
			</WorkCompRateClass>
		</WorkCompLocInfo>

	</xsl:template>

<!-- Total Policy premium -->
	<xsl:template name="CreateWorkCompLocInfoPolicy">

		<!-- xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMDI4W1__STATE__RATING__REC" -->
		<xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC">
			<xsl:sort select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
			<xsl:variable name="ClassCode" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
			<xsl:variable name="ClassDesc" select="substring(PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__DESC, 1, 3)"/>
			<xsl:choose>
				<!-- Case 39354 Begin -->
				<!-- xsl:when test="$ClassCode = '0063' or $ClassCode ='0900' or $ClassDesc = 'TAX'" -->
				<xsl:when test="$ClassCode = '0063' or $ClassCode ='0900'">
				<!-- Case 39354 End -->
				<WorkCompRateState>
					<StateProvCd>
						<xsl:call-template name="BuildStateProvCd">
							<xsl:with-param name="StateCd" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
						</xsl:call-template>
					</StateProvCd>
					<AnniversaryRatingDt/>
					<NCCIIDNumber/>
					<WorkCompLocInfo>
						<xsl:attribute name="LocationRef">
							<xsl:value-of select="concat('l',string('999'))"/>
						</xsl:attribute>
						<WorkCompRateClass>
							<ActualRemunerationAmt>
								<Amt>
									<xsl:choose>
										<xsl:when test="$ClassCode='0063'">
											<xsl:value-of select="string('-')"/>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:otherwise>
									</xsl:choose>
								</Amt>
							</ActualRemunerationAmt>
							<NumEmployees/>
							<NumEmployeesFullTime/>
							<NumEmployeesPartTime/>
							<Rate/>
							<RatingClassificationCd>
								<xsl:choose>
									<xsl:when test="$ClassCode = '0063'">XXPD</xsl:when>
									<xsl:when test="$ClassCode = '0900'">XXEC</xsl:when>
									<xsl:when test="$ClassDesc = 'TAX'">XXTX</xsl:when>
								</xsl:choose>
							</RatingClassificationCd>
							<Exposure>
								<xsl:value-of select="string('0000')"/>
							</Exposure>
							<ItemIdInfo>
								<OtherIdentifier>
									<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
									<OtherId/>
								</OtherIdentifier>
							</ItemIdInfo>
						</WorkCompRateClass>
					</WorkCompLocInfo>
				</WorkCompRateState>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:for-each>

		<!-- xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMDGS1__PREMIUM__SUMMARY__SEG" -->
		<xsl:for-each select="/*/PMDGS1__PREMIUM__SUMMARY__SEG">
		<WorkCompRateState>
			<StateProvCd/>
			<AnniversaryRatingDt/>
			<NCCIIDNumber/>
			<WorkCompLocInfo>
				<xsl:attribute name="LocationRef">
					<xsl:value-of select="concat('l',string('999'))"/>
				</xsl:attribute>
				<WorkCompRateClass>
					<ActualRemunerationAmt>
						<Amt>
							<xsl:value-of select="PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/>
						</Amt>
					</ActualRemunerationAmt>
					<NumEmployees/>
					<NumEmployeesFullTime/>
					<NumEmployeesPartTime/>
					<Rate>
						<xsl:value-of select="string('0000')"/>
					</Rate>
					<RatingClassificationCd>
						<xsl:value-of select="string('XXXP')"/>
					</RatingClassificationCd>
					<Exposure>
						<xsl:value-of select="string('0000')"/>
					</Exposure>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
				</WorkCompRateClass>
			</WorkCompLocInfo>
		</WorkCompRateState>

			<xsl:if test="PMDGS1__SEGMENT__DATA/PMDGS1__INS__LINE__TOTAL__PREM/PMDGS1__TAX__ASSESS__CHARGE &gt; '0'">
			<WorkCompRateState>
				<StateProvCd/>
				<AnniversaryRatingDt/>
				<NCCIIDNumber/>
				<WorkCompLocInfo>
					<xsl:attribute name="LocationRef">
					<xsl:value-of select="concat('l',string('999'))"/>
					</xsl:attribute>
					<WorkCompRateClass>
						<ActualRemunerationAmt>
							<Amt>
								<xsl:value-of select="PMDGS1__SEGMENT__DATA/PMDGS1__INS__LINE__TOTAL__PREM/PMDGS1__TAX__ASSESS__CHARGE"/>
							</Amt>
						</ActualRemunerationAmt>
						<NumEmployees/>
						<NumEmployeesFullTime/>
						<NumEmployeesPartTime/>
						<Rate>
							<xsl:value-of select="string('0000')"/>
						</Rate>
						<RatingClassificationCd>
							<xsl:value-of select="string('XXTX')"/>
						</RatingClassificationCd>
						<Exposure>
							<xsl:value-of select="string('0000')"/>
						</Exposure>
						<ItemIdInfo>
							<OtherIdentifier>
								<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
								<OtherId/>
							</OtherIdentifier>
						</ItemIdInfo>
					</WorkCompRateClass>
				</WorkCompLocInfo>
			</WorkCompRateState>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>

<!-- Generated State Class Codes for Amendments -->
	<xsl:template name="CreateWorkCompLocInfoState">
		<xsl:param name="StateCd"/>
		<!-- xsl:for-each select="/WorkCompPolicyQuoteInqRs/PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$StateCd]" -->
		<xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC[PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$StateCd]">
			<xsl:sort select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
			<xsl:variable name="ClassCode" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
			<xsl:variable name="ClassOrder" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE"/>
			<xsl:variable name="GenSegInd" select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__GENERATED__SEG__IND"/>
			<xsl:choose>
				<xsl:when test="$GenSegInd = 'G' and $ClassCode != '0000'">
				<WorkCompRateState>
					<StateProvCd>
						<xsl:call-template name="BuildStateProvCd">
							<xsl:with-param name="StateCd" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
						</xsl:call-template>
					</StateProvCd>
					<AnniversaryRatingDt/>
					<NCCIIDNumber/>
					<WorkCompLocInfo>
						<xsl:attribute name="LocationRef">
							<xsl:value-of select="concat('l',$StateCd)"/>
						</xsl:attribute>
						<WorkCompRateClass>
							<ActualRemunerationAmt>
								<Amt>
									<xsl:choose>
										<xsl:when test="$ClassCode='9880'">
											<xsl:value-of select="string('-')"/>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM"/>
										</xsl:otherwise>
									</xsl:choose>
								</Amt>
							</ActualRemunerationAmt>
							<NumEmployees/>
							<NumEmployeesFullTime/>
							<NumEmployeesPartTime/>
							<Rate>
								<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE"/>
							</Rate>
							<RatingClassificationCd>
								<xsl:value-of select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__CODE"/>
								<xsl:value-of select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT/PMDI4W1__REPORTING__CLASS__SEQ"/>
							</RatingClassificationCd>
							<Exposure>
								<xsl:value-of select="string('0000')"/>
							</Exposure>
							<ItemIdInfo>
								<OtherIdentifier>
									<OtherIdTypeCd>PremiumId</OtherIdTypeCd>
									<OtherId/>
								</OtherIdentifier>
							</ItemIdInfo>
						</WorkCompRateClass>
					</WorkCompLocInfo>
				</WorkCompRateState>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>