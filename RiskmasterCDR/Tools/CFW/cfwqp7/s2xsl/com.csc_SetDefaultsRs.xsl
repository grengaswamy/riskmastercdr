<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
 <!--xsl:include href="SavErrorRs.xsl"/-->
 <xsl:output method="xml" indent="yes"/>
 
  <xsl:template match="/">
     <xsl:apply-templates select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG"/>
  </xsl:template>

 <xsl:template match="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG">
	<ACORD>
		<SignonRs>
			<Status>
				<StatusCd/>
				<StatusDesc/>
			</Status>
			<SignonRoleCd/>
			<CustId>
				<SPName/>
				<CustLoginId/>
			</CustId>
			<GenSessKey/>
			<ClientDt/>
			<CustLangPref/>
			<ClientApp>
				<Org/>
				<Name/>
				<Version/>
			</ClientApp>
			<ServerDt/>
			<Language/>
		</SignonRs>
		<InsuranceSvcRs>
			<Status>
				<StatusCd/>
				<StatusDesc/>
			</Status>
			<RqUID/>
			<com.csc_SetDefaultsRs>
				<RqUID/>
				<TransactionResponseDt/>
				<TransactionEffectiveDt/></TransactionEffectiveDt>
				<CurCd/>
				<Producer>
					<ItemIdInfo>
						<OtherIdentifier>
							<OtherIdTypeCd com.csc_colind="Required">S2Agency</OtherIdTypeCd>
							<OtherId/>
						</OtherIdentifier>
					</ItemIdInfo>
					<ProducerInfo>
						<ContractNumber><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER/PIF__REPORTING__AGT__NR" /></ContractNumber>
						<ProducerSubCode/>
					</ProducerInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</Producer>
				<PersPolicy>
					<PolicyNumber><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
					<CompanyProductCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS" /></CompanyProductCd>
					<LOBCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
					<NAICCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__MASTER__CO__NUMBER" /></NAICCd>
					
					<ControllingStateProvCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__RISK__STATE__PROV" /></ControllingStateProvCd>

					<TotalAgenPrm><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__TOTAL__AGEN__PREM" /></TotalAgenPrm>
					<SortName><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__SORT__NAME" /></SortName>
					<ContractTerm>
						<EffectiveDt><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></EffectiveDt>
						<ExpirationDt><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__EXPIRATION__DATE" /></ExpirationDt>
					</ContractTerm>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
					<OriginalInceptionDt><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ORIGINAL__INCEPT" /></OriginalInceptionDt>
					<PayorCd/>
					<RateEffectiveDt com.csc_colind="Required"><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE" /></RateEffectiveDt>
					<RenewalPayorCd/>
					<RenewalTerm>
						<DurationPeriod>
							<NumUnits></NumUnits>
						</DurationPeriod>
					</RenewalTerm>
					<QuoteInfo>
						<CompanysQuoteNumber></CompanysQuoteNumber>
						<IterationNumber></IterationNumber>
					</QuoteInfo>
					<com.csc_BranchInfo>
					<com.csc_BranchCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__BRANCH" /></com.csc_BranchCd>
					</com.csc_BranchInfo>
					<com.csc_ApplicationReceivedDt></com.csc_ApplicationReceivedDt>
					<com.csc_PolicyWrittenDt com.csc_colind="ReadOnly"><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ENTERED__DATE" /></com.csc_PolicyWrittenDt>
					<com.csc_Underwriter/>
					<com.csc_UnderwritingDecision></com.csc_UnderwritingDecision>
					<PersApplicationInfo></PersApplicationInfo>
					<com.csc_ActionInfo></com.csc_ActionInfo>
					<com.csc_ActivityDescCd></com.csc_ActivityDescCd>
					<com.csc_PayAgentTypeCd></com.csc_PayAgentTypeCd>
				</PersPolicy>
				<PersAutoLineBusiness>
					<LOBCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL" /></LOBCd>
					<com.csc_Location>
						<Addr>
							<AddrTypeCd com.csc_colind="Required"></AddrTypeCd>
							<Addr1><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__1" /></Addr1>
							<Addr2><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2" /></Addr2>
							<Addr3><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__2__B" /></Addr3>
							<Addr4><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__ADDRESS__LINE__3" /></Addr4>
							<City></City>
							<StateProvCd></StateProvCd>
							<PostalCode></PostalCode>
							<Country/>
							<County></County>
						</Addr>
						<com.csc_AdrSeqNbr></com.csc_AdrSeqNbr>
						<com.csc_ActionInfo>
							<ActionCd/>
							<com.csc_ReasonCd/>
						</com.csc_ActionInfo>
					</com.csc_Location>
				</PersAutoLineBusiness>
				<PolicySummaryInfo>
					<PolicyNumber><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER" /></PolicyNumber>
					<FullTermAmt>
						<Amt/>
					</FullTermAmt>
					<PolicyStatusCd><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__STATUS__ON__PIF" /></PolicyStatusCd>
					<ErrorMessage><xsl:value-of select="/com.csc_SetDefaultsRs/POLICY__INFORMATION__SEG/PIF__PMS__FUTURE__USE" /></ErrorMessage>
					<com.csc_TransAmt>
						<Amt/>
					</com.csc_TransAmt>
				</PolicySummaryInfo>
			</com.csc_SetDefaultsRs>
		</InsuranceSvcRs>
	</ACORD>
</xsl:template>
</xsl:stylesheet>
