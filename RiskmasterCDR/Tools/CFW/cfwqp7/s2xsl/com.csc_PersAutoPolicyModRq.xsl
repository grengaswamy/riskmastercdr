<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service case 19455 
***********************************************************************************************-->

	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2EPF00.xsl"/>
	<xsl:include href="S2Vehcov53.xsl"/>
	<xsl:include href="S2Vehdes53.xsl"/>
	<xsl:include href="S2Vehmerge53.xsl"/>	
	<xsl:include href="S2Driver53.xsl"/>
	<xsl:include href="S2Pinfo53.xsl"/>
	<xsl:include href="S2Desc53.xsl"/>
	<xsl:include href="S2CACommonCoveragesRq.xsl"/><!-- Issue 80442 -->
	
    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="VehCount" select="count(//ACORD/InsuranceSvcRq/*/PersAutoLineBusiness/PersVeh)"/><!-- Issue 80442 -->
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/PolicyNumber"/>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/LOBCd"/>
    <xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/TransactionEffectiveDt"/>
    <xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/TransactionRequestDt"/>
    <xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/com.csc_PersAutoPolicyModRq/PersPolicy/ContractTerm/EffectiveDt"/>
          	
	<xsl:template match="/">
        <xsl:element name="com.csc_PersAutoPolicyModRq">
			<xsl:call-template name="S2EPF00Template">
				<xsl:with-param name="ProcessingType">EN</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/PolicyStatusCd"/>				<!--103409-->
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="S2PinfoTemplate">
				<xsl:with-param name="ProcessingType">EN</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/PolicyStatusCd"/>				<!--103409-->
				</xsl:with-param>
			</xsl:call-template>
		<xsl:call-template name="S2Desc53Template"/>   			
		<xsl:call-template name="S2Driver53Template"/>   
		<xsl:call-template name="S2Vehmerge53Template"/>   
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>