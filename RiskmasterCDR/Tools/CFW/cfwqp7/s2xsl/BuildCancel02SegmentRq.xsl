<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="BuildCancel02SegmentRq">
	<!-- <xsl:template match="/"> -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__INFORMATION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<POLICY__INFORMATION__SEG>
				<PIF__REC__LLBB/>
				<PIF__ID>02</PIF__ID>
				<PIF__KEY>
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/CompanyProductCd">
						<PIF__SYMBOL>
							<xsl:value-of select="."/>
						</PIF__SYMBOL>
					</xsl:for-each>
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PolicyNumber">
						<PIF__POLICY__NUMBER>
							<xsl:value-of select="."/>
						</PIF__POLICY__NUMBER>
					</xsl:for-each>
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/PolicyVersion">
						<PIF__MODULE>
							<xsl:value-of select="."/>
						</PIF__MODULE>
					</xsl:for-each>
				</PIF__KEY>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/NAICCd">
					<PIF__MASTER__CO__NUMBER>
						<xsl:value-of select="."/>
					</PIF__MASTER__CO__NUMBER>
				</xsl:for-each>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_PartialPolicy/com.csc_CompanyPolicyProcessingId">
					<PIF__LOCATION>
						<xsl:value-of select="."/>
					</PIF__LOCATION>
				</xsl:for-each>
				<PIF__EFFECTIVE__DATE>
					<PIF__EFF__DATE__YR__MO>
						<PIF__EFF__YR>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/TransactionEffectiveDt,1,4)"/>
						</PIF__EFF__YR>
						<PIF__EFF__MO>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/TransactionEffectiveDt,6,2)"/>
						</PIF__EFF__MO>
					</PIF__EFF__DATE__YR__MO>
					<PIF__EFF__DA>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/TransactionEffectiveDt,9,2)"/>
					</PIF__EFF__DA>
				</PIF__EFFECTIVE__DATE>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_MethodCd">
					<PIF__ISSUE__CODE>
						<xsl:value-of select="."/>
					</PIF__ISSUE__CODE>
				</xsl:for-each>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CancelNonRenewalSubmitRq/com.csc_CancelNonRenewalInfo/com.csc_ReasonCd">
					<PIF__REASON__AMENDED>
						<xsl:value-of select="."/>
					</PIF__REASON__AMENDED>
				</xsl:for-each>
			</POLICY__INFORMATION__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="com.csc_CancelNonRenewalSubmitRq.xml" userelativepaths="yes" externalpreview="no" url="..\ACORD XML Messages\com.csc_CancelNonRenewalSubmitRq.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\ACORD XML Messages\com.csc_CancelNonRenewalSubmitRq.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\Stub XML\Pinfo53Record.xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->