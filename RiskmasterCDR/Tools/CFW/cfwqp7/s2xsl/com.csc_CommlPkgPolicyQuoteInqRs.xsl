<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform Series II records into internal XML
prior to posting data to the iSolutions database.
E-Service case .NET 
***********************************************************************************************
-->
  <xsl:include href="S2CommonFuncRs.xsl"/>
  <xsl:include href="S2SignOnRs.xsl"/>
  <xsl:include href="S2ErrorRs.xsl"/>
  <xsl:include href="S2ProducerRs.xsl"/>
  <xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
  <xsl:include href="S2MessageStatusRs.xsl"/>

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

  <xsl:template match="/">

    <ACORD>
      <xsl:call-template name="BuildSignOn"/>
      <InsuranceSvcRs>
        <RqUID/>
        <com.csc_CommlPkgPolicyQuoteInqRs>
          <RqUID/>
          <TransactionResponseDt/>
          <TransactionEffectiveDt/>
          <CurCd>USD</CurCd>
	  <xsl:call-template name="BuildMessageStatus">
		<xsl:with-param name="MessageType">
			<xsl:value-of select="string('RATING')"/>
		</xsl:with-param>
	  </xsl:call-template>
	<!-- 
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WC"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Person"/>
	  <xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="WCPolicy"/>
	-->
		<QuestionAnswer>
			<QuestionCd/>
			<YesNoCd/>
			<Explanation/>
		</QuestionAnswer>
          <RemarkText IdRef="policy">Comml Pkg Policy XML Quote</RemarkText>
        </com.csc_CommlPkgPolicyQuoteInqRs>
      </InsuranceSvcRs>
    </ACORD>
  </xsl:template>

</xsl:stylesheet>