<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Producer">
		<Producer>
			<ItemIdInfo>
				<AgencyId>
					<xsl:value-of select="PIF__AGENCY__NUMBER"/>
				</AgencyId>
				<InsurerId/>
				<SystemId/>
				<OtherIdentifier>
					<OtherIdTypeCd com.csc_colind="Required">S2Agency</OtherIdTypeCd>
					<OtherId/>
				</OtherIdentifier>
			</ItemIdInfo>
			<!--Modified for Issue 35934 Start-->
			<!-- 55614 Begin -->
			<!--<ProducerInfo>
				<ContractNumber>-->
					<!-- xsl:value-of select="concat(PIF__AGENCY__NUMBER/fill_3,PIF__AGENCY__NUMBER/PIF__REPORTING__AGT__NR,PIF__AGENCY__NUMBER/fill_4)"/ -->
					<!--<xsl:value-of select="PIF__AGENCY__NUMBER"/>
				</ContractNumber>
			</ProducerInfo>-->
			<!-- 55614 End -->
			<!--Modified for Issue 35934 End-->
			<GeneralPartyInfo>
				<NameInfo>
				   <!-- 55614 Begin --> 
					<!--<PersonName>
						<Surname>-->
							<!-- xsl:value-of select="/com.csc_PolicySyncRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER"/ -->
							<!--<xsl:value-of select="PIF__AGENCY__NUMBER"/>
						</Surname>
						<GivenName/>
						<OtherGivenName/>
						<TitlePrefix/>
						<NameSuffix/>
					</PersonName>-->
					<!-- 55614 End -->
					<CommlName>
						<CommercialName>CSC</CommercialName>
						<IndexName/>
						<SupplementaryNameInfo>
							<SupplementaryNameCd/>
							<SupplementaryName/>
						</SupplementaryNameInfo>
					</CommlName>
					<LegalEntityCd/>
					<NonTaxIdentity>
						<NonTaxIdTypeCd/>
						<NonTaxId/>
					</NonTaxIdentity>
					<com.csc_LongName com.csc_colind="ReadOnly">
						<!-- xsl:value-of select="/com.csc_PolicySyncRs/POLICY__INFORMATION__SEG/PIF__AGENCY__NUMBER"/ -->
						<xsl:value-of select="PIF__AGENCY__NUMBER"/>
					</com.csc_LongName>
					<com.csc_OtherIdentifier/>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<Addr1/>
					<Addr2/>
					<Addr3/>
					<Addr4/>
					<City/>
					<StateProvCd>
						<xsl:call-template name="BuildStateProvCd">
							<xsl:with-param name="StateCd" select="PIF__RISK__STATE__PROV"/>
						</xsl:call-template>
					</StateProvCd>
					<!-- 55614 Begin -->
					<!--<StateProv/>
					<PostalCode/>
					<CountryCd/>
					<Country/>
					<County/>
					<com.csc_AddrTypeInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<EffectiveDt/>
						<ExpirationDt/>
						<AddrTypeCd/>
						<com.csc_NumberYearsAtAddr/>
						<ActionCd/>
					</com.csc_AddrTypeInfo>
					<com.csc_EffectiveDt/>
					<com.csc_ExpirationDt/>
					<com.csc_OtherIdentifier/>
					<com.csc_ActionCd/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
						<EmailAddr/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</PhoneInfo>
					<EmailInfo>
						<CommunicationUseCd/>
						<EmailAddr/>
						<com.csc_EmailTypeCd/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</EmailInfo>
				</Communications>
				<com.csc_ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>-->
				</Addr>
				<!-- 55614 End -->
			</GeneralPartyInfo>
			<ProducerInfo>
				<ContractNumber><xsl:value-of select="PIF__AGENCY__NUMBER"/></ContractNumber>           <!-- 55614 -->
				<ProducerSubCode>
					<xsl:value-of select="PIF__PRODUCER__CODE"/>
				</ProducerSubCode>
				<!-- 55614 Begin -->
				<!--<PlacingOffice/>
				<NonResLicense>
					<NonResProducerLicenseNumber/>
					<NonResProducerLicenseExpirationDt/>
					<StateProvCd/>
				</NonResLicense>
				<ResProducerLicenseNumber/>
				<ResProducerLicenseExpirationDt/>
				<ProducerRoleCd/>
			</ProducerInfo>
			<com.csc_ActionInfo>
				<ActionCd/>
				<com.csc_ReasonCd/>
			</com.csc_ActionInfo>-->
			</ProducerInfo>
			<!-- 55614 End -->
		</Producer>
	</xsl:template>
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="WC">
		<Producer>
			<ItemIdInfo>
				<AgencyId>
					<xsl:value-of select="PIF__AGENCY__NUMBER"/>
				</AgencyId>
				<InsurerId/>
				<SystemId/>
				<OtherIdentifier>
					<OtherIdTypeCd com.csc_colind="Required">S2Agency</OtherIdTypeCd>
					<OtherId/>
				</OtherIdentifier>
			</ItemIdInfo>
			<GeneralPartyInfo>
				<NameInfo>
					<PersonName>
						<Surname/>
						<GivenName/>
						<OtherGivenName/>
						<TitlePrefix/>
						<NameSuffix/>
					</PersonName>
					<CommlName>
						<CommercialName/>
						<IndexName/>
						<SupplementaryNameInfo>
							<SupplementaryNameCd/>
							<SupplementaryName/>
						</SupplementaryNameInfo>
					</CommlName>
					<LegalEntityCd/>
					<NonTaxIdentity>
						<NonTaxIdTypeCd/>
						<NonTaxId/>
					</NonTaxIdentity>
					<com.csc_LongName/>
					<com.csc_OtherIdentifier/>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<Addr1/>
					<Addr2/>
					<Addr3/>
					<Addr4/>
					<City/>
					<StateProvCd/>
					<StateProv/>
					<PostalCode/>
					<CountryCd/>
					<Country/>
					<County/>
					<com.csc_AddrTypeInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<EffectiveDt/>
						<ExpirationDt/>
						<AddrTypeCd/>
						<com.csc_NumberYearsAtAddr/>
						<ActionCd/>
					</com.csc_AddrTypeInfo>
					<com.csc_EffectiveDt/>
					<com.csc_ExpirationDt/>
					<com.csc_OtherIdentifier/>
					<com.csc_ActionCd/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
						<EmailAddr/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</PhoneInfo>
					<EmailInfo>
						<CommunicationUseCd/>
						<EmailAddr/>
						<com.csc_EmailTypeCd/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</EmailInfo>
				</Communications>
				<com.csc_ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
			</GeneralPartyInfo>
			<ProducerInfo>
				<ContractNumber/>
				<ProducerSubCode/>
				<PlacingOffice/>
				<NonResLicense>
					<NonResProducerLicenseNumber/>
					<NonResProducerLicenseExpirationDt/>
					<StateProvCd/>
				</NonResLicense>
				<ResProducerLicenseNumber/>
				<ResProducerLicenseExpirationDt/>
				<ProducerRoleCd/>
			</ProducerInfo>
			<com.csc_ActionInfo>
				<ActionCd/>
				<com.csc_ReasonCd/>
			</com.csc_ActionInfo>
		</Producer>
	</xsl:template>
</xsl:stylesheet>