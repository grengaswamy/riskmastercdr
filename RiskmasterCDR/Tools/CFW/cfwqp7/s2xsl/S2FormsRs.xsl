<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:variable name="pF" select="'*'"/>
	<xsl:template match="/*/COMPUTER__ISSUE__FORMS__SEG" mode="BuildForm">
		<xsl:call-template name="ExtractFormEntries">
			<xsl:with-param name="Entries" select="FORMS__SECTIONS/FORMS__ENTRIES"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="ExtractFormEntries">
		<xsl:param name="Entries"/>
		<xsl:variable name="FormString">
			<xsl:if test="string-length($Entries) &gt; 0">
				<xsl:choose>
					<xsl:when test="string-length($Entries) = '276'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12),$pF,substring($Entries,217,12),$pF,substring($Entries,229,12),$pF,substring($Entries,241,12),$pF,substring($Entries,253,12),$pF,substring($Entries,265,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '264'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12),$pF,substring($Entries,217,12),$pF,substring($Entries,229,12),$pF,substring($Entries,241,12),$pF,substring($Entries,253,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '252'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12),$pF,substring($Entries,217,12),$pF,substring($Entries,229,12),$pF,substring($Entries,241,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '240'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12),$pF,substring($Entries,217,12),$pF,substring($Entries,229,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '228'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12),$pF,substring($Entries,217,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '216'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12),$pF,substring($Entries,205,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '204'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12),$pF,substring($Entries,193,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '192'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12),$pF,substring($Entries,181,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '180'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12),$pF,substring($Entries,169,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '168'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12),$pF,substring($Entries,157,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '156'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12),$pF,substring($Entries,145,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '144'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12),$pF,substring($Entries,133,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '132'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12),$pF,substring($Entries,121,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '120'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12),$pF,substring($Entries,109,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '108'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12),$pF,substring($Entries,97,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '96'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12),$pF,substring($Entries,85,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '84'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12),$pF,substring($Entries,73,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '72'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12),$pF,substring($Entries,61,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '60'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12),$pF,substring($Entries,49,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '48'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12),$pF,substring($Entries,37,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '36'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12),$pF,substring($Entries,25,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '24'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pF,substring($Entries,13,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '12'">
						<xsl:value-of select="substring($Entries,1,12)"/>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($FormString) &gt; 12">
				<xsl:call-template name="AssignValue">
					<xsl:with-param name="str1" select="substring-before($FormString,$pF)"/>
					<xsl:with-param name="str2" select="substring-after($FormString,$pF)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length($FormString) = 0">
				<xsl:call-template name="AssignValue">
					<xsl:with-param name="str1"/>
					<xsl:with-param name="str2"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="AssignValue">
					<xsl:with-param name="str1" select="$FormString"/>
						</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="AssignValue">
		<xsl:param name="str1"/>
		<xsl:param name="str2"/>
		<xsl:if test="string-length($str1) &gt; 0">
		<Form>
			<xsl:attribute name="id">
				<xsl:value-of select="./FORMS__ID"/>
			</xsl:attribute>
			<xsl:attribute name="VehRef">
				<xsl:value-of select="./FORMS__UNIT"/>
			</xsl:attribute>
			<FormName>
				<xsl:value-of select="substring($str1,1,7)"/>
			</FormName>
			<EditionDt>
				<xsl:value-of select="substring($str1,8,4)"/>
			</EditionDt>
		</Form>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="string-length(normalize-space($str2)) &gt; 12">
				<xsl:call-template name="AssignValue">
					<xsl:with-param name="str1" select="substring-before($str2,$pF)"/>
					<xsl:with-param name="str2" select="substring-after($str2,$pF)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length(normalize-space($str2)) = 12">
				<xsl:call-template name="AssignValue">
					<xsl:with-param name="str1" select="$str2"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->