<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="xalan://java.util.GregorianCalendar">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service case .NET 
***********************************************************************************************
-->

	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2Vehcov53.xsl"/>
	<xsl:include href="S2Vehdes53.xsl"/>
	<xsl:include href="S2Vehmerge53.xsl"/>	
	<xsl:include href="S2Driver53.xsl"/>
	<xsl:include href="S2Pinfo53.xsl"/>
	<xsl:include href="S2Desc53.xsl"/>
	<xsl:include href="S2EPF00.xsl"/>
	<xsl:include href="S2CACommonCoveragesRq.xsl"/><!-- Issue 80442 -->

     <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/CompanyProductCd"/>
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/PolicyNumber"/>
	<xsl:variable name="MOD">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/PolicyVersion"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>            
	</xsl:variable>
	<!--<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/com.csc_AmendmentMode"/> --><!-- Case 34768 -->	<!--103409-->
	<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/PolicyStatusCd"/> <!--103409-->
	<!-- Issue 80442 Begin 
	<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/LOBCd"/ -->
	<xsl:variable name="VehCount" select="count(//ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh)"/>
	<xsl:variable name="LOB">
		<xsl:choose>
			<!-- If there are more than 4 vehicles, the LOB becomes AFV for fleet -->
			<xsl:when test="$VehCount &gt; 4">AFV</xsl:when>
			<xsl:otherwise>ACV</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Issue 80442 End -->
	<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/TransactionEffectiveDt"/>
	<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/TransactionRequestDt"/>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/CommlAutoPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>

	<xsl:template match="/">

		<xsl:element name="CommlAutoPolicyQuoteInqRq">
			<xsl:call-template name="S2PinfoTemplate">   
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<xsl:value-of select="$PolicyMode"/>
				</xsl:with-param>
			</xsl:call-template>
		<!-- Issue 80442 Uncomment the call to S2Desc53Template -->
			<xsl:call-template name="S2Desc53Template"/>   			
		<!--	<xsl:call-template name="S2Driver53Template"/>   -->
			<xsl:call-template name="S2Vehmerge53Template"/>   
		<!-- Issue 80442 Uncomment the call to S2Vehmerge53Template -->
			<xsl:call-template name="S2EPF00Template">  
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<xsl:value-of select="$PolicyMode"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>			

	</xsl:template>

</xsl:stylesheet>