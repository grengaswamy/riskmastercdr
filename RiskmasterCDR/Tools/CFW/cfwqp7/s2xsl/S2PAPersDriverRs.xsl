<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template name="CreatePersDriver">
		<xsl:for-each select="/*/DRIVER__SEG">
			<PersDriver>
				<xsl:variable name="DriverNbr" select="./DRIVER__ID"/>
				<xsl:attribute name="id">
					<xsl:value-of select="concat('d',$DriverNbr)"/>
				</xsl:attribute>
				<ItemIdInfo>
					<AgencyId/>
					<InsurerId>
						<xsl:value-of select="./DRIVER__ID"/>
					</InsurerId>
					<SystemId/>
				</ItemIdInfo>
				<GeneralPartyInfo>
					<NameInfo>
						<PersonName>
							<Surname>
								<!--Modified for Issue 35934<xsl:value-of select="normalize-space(./DRIVER__NAME__LAST)"/>-->
								<xsl:value-of select="substring-after(./DRIVER__NAME,' ')"/>
							</Surname>
							<GivenName>
								<!--Modified for Issue 35934<xsl:value-of select="normalize-space(./DRIVER__NAME__FIRST)"/>-->
								<xsl:value-of select="substring-before(./DRIVER__NAME,' ')"/>
							</GivenName>
							<OtherGivenName>
								<xsl:value-of select="normalize-space(./DRIVER__NAME__MIDDLE)"/>
							</OtherGivenName>
							<NameSuffix>
								<xsl:value-of select="normalize-space(./DRIVER__NAME__SUFFIX)"/>
							</NameSuffix>
						</PersonName>
						<LegalEntityCd/>
						<TaxIdentity>
							<TaxIdType>SSN</TaxIdType>
							<TaxId>
								<xsl:value-of select="./DRIVER__SSN"/>
							</TaxId>
						</TaxIdentity>
						<com.csc_LongName/>
						<com.csc_OtherIdentifier/>
					</NameInfo>
					<Addr>
						<AddrTypeCd/>
						<Addr1/>
						<Addr2/>
						<Addr3/>
						<Addr4/>
						<City/>
						<StateProvCd/>
						<StateProv/>
						<PostalCode/>
						<CountryCd/>
						<Country/>
						<County/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</Addr>
					<Communications>
						<PhoneInfo>
							<PhoneTypeCd/>
							<CommunicationUseCd/>
							<PhoneNumber/>
							<EmailAddr/>
							<com.csc_EffectiveDt/>
							<com.csc_ExpirationDt/>
							<com.csc_OtherIdentifier/>
							<com.csc_ActionCd/>
						</PhoneInfo>
					</Communications>
				</GeneralPartyInfo>
				<DriverInfo>
					<PersonInfo>
						<GenderCd>
							<xsl:value-of select="./DRIVER__SEX"/>
						</GenderCd>
						<BirthDt>
							<xsl:value-of select="concat(./DRIVER__DATE__BIRTH/DOB__YEAR,'/',./DRIVER__DATE__BIRTH/DOB__MONTH,'/',./DRIVER__DATE__BIRTH/DOB__DAY)"/>
						</BirthDt>
						<MaritalStatusCd>
							<xsl:value-of select="./DRIVER__MARITAL__STATUS"/>
						</MaritalStatusCd>
						<OccupationDesc/>
						<OccupationClassCd/>
						<LengthTimeEmployed>
							<NumUnits/>
							<UnitMeasurementCd/>
						</LengthTimeEmployed>
						<LengthTimeCurrentOccupation>
							<NumUnits/>
							<UnitMeasurementCd/>
						</LengthTimeCurrentOccupation>
						<LengthTimeWithPreviousEmployer>
							<NumUnits/>
							<UnitMeasurementCd/>
						</LengthTimeWithPreviousEmployer>
						<com.csc_NumberDependents/>
						<com.csc_YearHired/>
						<com.csc_OccupationDesc/>
						<com.csc_Position/>
						<com.csc_SalaryRangeCd/>
						<com.csc_CitizenCountryCd/>
						<com.csc_NonCitizenArrivalDt/>
						<com.csc_DeathDt/>
						<com.csc_EducationLevel/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</PersonInfo>
					<!--Modified for Issue 35934 Start
					<com.csc_License>-->
					<DriversLicense>
						<LicensedDt>
							<xsl:value-of select="concat(./DRIVER__LICENSE__DATE/DLD__YEAR,'/', ./DRIVER__LICENSE__DATE/DLD__MONTH,'/',./DRIVER__LICENSE__DATE/DLD__DAY)"/>
						</LicensedDt>
						<DriversLicenseNumber>
							<!--<com.csc_LicensePermitNumber>-->
							<xsl:value-of select="./DRIVER__LICENSE__NUMBER"/>
							<!--</com.csc_LicensePermitNumber>-->
						</DriversLicenseNumber>
						<StateProvCd>
							<xsl:call-template name="BuildStateProvCd">
								<xsl:with-param name="StateCd" select="./DRIVER__LICENSE__STATE"/>
							</xsl:call-template>
						</StateProvCd>
						<!--</com.csc_License>-->
					</DriversLicense>
					<!--Modified for Issue 35934 End-->
				</DriverInfo>
				<PersDriverInfo>
					<xsl:variable name="UnitNo">
						<xsl:value-of select="concat(../VEHICLE__DESCRIPTION__SEG/USVD__KEY/USVD__UNIT__NUMBER/fill_0,../VEHICLE__DESCRIPTION__SEG/USVD__KEY/USVD__UNIT__NUMBER/USVD__NUNIT__LST__DIGIT)"/>
					</xsl:variable>
					<!--<xsl:attribute name="VehPrincipallyDrivenRef">
						<xsl:value-of select="concat('v',$UnitNo)"/>
					</xsl:attribute>-->
					<AssignedRiskReasonCd/>
					<DriverViolationInformationCd/>
					<DefensiveDriverDt/>
					<DefensiveDriverExpirationDt/>
					<DefensiveDriverCd/>
					<!--<xsl:for-each select="./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES[@index]">
                    	<xsl:if test="./DRIV__ATTR/DRIV__ATTR__VALUE='SAC '">                               
							<DistantStudentInd>Y</DistantStudentInd>
                    	</xsl:if>
                    </xsl:for-each> xml image modified -->
					<xsl:if test="contains(./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES, 'SAC ')">
						<DistantStudentInd>Y</DistantStudentInd>
					</xsl:if>
					<DriverAssignedRiskInd/>
					<DriverRelationshipToApplicantCd>
						<xsl:value-of select="./DRIVER__RELATION__TO__INDS"/>
					</DriverRelationshipToApplicantCd>
					<!--<xsl:for-each select="./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES[@index]">
                    	<xsl:if test="./DRIV__ATTR/DRIV__ATTR__VALUE='PC00'"> 
							<DriverTrainingCompletionDt>
                            	<xsl:value-of select="./DRIVER__ATTRB__DATE"/>
                            </DriverTrainingCompletionDt>
							<DriverTrainingInd>Y</DriverTrainingInd>
                        </xsl:if>
                    </xsl:for-each>   xml image modified -->
					<xsl:if test="contains(./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES, 'PC00')">
						<DriverTrainingCompletionDt>
							<xsl:value-of select="substring(substring-after(./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES, 'PC00'), 1, 8)"/>
						</DriverTrainingCompletionDt>
						<DriverTrainingInd>Y</DriverTrainingInd>
					</xsl:if>
					<DriverTypeCd/>
					<GoodDriverInd>
						<xsl:value-of select="./DRIVER__AAA__IND"/>
					</GoodDriverInd>
					<!--<xsl:for-each select="./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES[@index]">
                        <xsl:if test="./DRIV__ATTR/DRIV__ATTR__VALUE='GST '"> 
							<GoodStudentDt>
                            	<xsl:value-of select="./DRIVER__ATTRB__DATE"/> 
                            </GoodStudentDt>
							<GoodStudentCd>Y</GoodStudentCd>
                        </xsl:if>     
                    </xsl:for-each>   xml image modified -->
					<xsl:if test="contains(./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES, 'GST ')">
						<GoodStudentDt>
							<xsl:value-of select="substring(substring-after(./DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES, 'GST '), 1, 8)"/>
						</GoodStudentDt>
						<GoodStudentCd>Y</GoodStudentCd>
					</xsl:if>
					<MatureDriverDt/>
					<MatureDriverExpirationDt/>
					<MatureDriverInd/>
					<ReinstatementDt/>
					<ResidentCustodyInd/>
					<RestrictedInd/>
					<SuspensionRevocationDt/>
					<SuspensionRevocationReasonCd/>
				</PersDriverInfo>
			</PersDriver>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->