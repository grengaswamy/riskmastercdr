<?xml version="1.0"?>
<!--
**********************************************************************************************
This XSL stylesheet is used by Communications Framework to transform XML from ACORD business
messages into POINT specific data records.
E-Service case XXXXX
**********************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="ConvertISODateToPTDate">
         <xsl:param name="Value"/>
        <xsl:variable name="ModifiedDateValue">
            <xsl:choose>
                <xsl:when test="translate(string($Value),' /-.','')=''">1900-00-00</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Value"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

<!--  Format an ISO date (i.e. 2001-12-31) to a 7 digit POINT date (i.e. 1123101) -->
        <xsl:choose>
            <xsl:when test="substring($Value,1,2)='20'">
                <xsl:value-of select="concat('1',substring($ModifiedDateValue,3,2))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('0',substring($ModifiedDateValue,3,2))"/>
            </xsl:otherwise>
        </xsl:choose>
<!--  Month -->
        <xsl:value-of select="substring($ModifiedDateValue,6,2)"/>
<!--  Day -->
        <xsl:value-of select="substring($ModifiedDateValue,9,2)"/>
    </xsl:template>
    
    <xsl:template name="FormatData">
        <xsl:param name="FieldName"/>
        <xsl:param name="Value"/>
        <xsl:param name="FieldType"/>
        <xsl:param name="FieldLength"/>
        <xsl:param name="Precision">0</xsl:param>
        <xsl:param name="NumberMask">No Mask</xsl:param>

        <xsl:choose>
            <xsl:when test="$FieldType='N'">
                <xsl:variable name="FormatMask">
                    <xsl:choose>
                        <xsl:when test="$NumberMask != 'No Mask'">
                            <xsl:value-of select="$NumberMask"/>
                        </xsl:when>
                        <xsl:when test="$Precision > 0">
                            <xsl:value-of select="concat(substring('00000000000',1,$FieldLength - $Precision),'.',substring('00000000000',1,$Precision))"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring('00000000000',1,$FieldLength)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="NewValue">
                    <xsl:choose>
                        <xsl:when test="string($Value)=''">0</xsl:when>
                        <xsl:otherwise>
                             <xsl:value-of select="number(translate($Value, ' $,', ''))"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:if test="string($NewValue)='NaN'">
                    <xsl:message terminate="yes"><xsl:value-of select="$FieldName"/> should be numeric. The value is '<xsl:value-of select="$Value"/>'
The context is: <xsl:value-of select="name()"/>
                    </xsl:message>
                </xsl:if>
                <xsl:value-of select="translate(format-number($NewValue,$FormatMask),'.','')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="ModifiedStringValue">
                    <xsl:choose>
                        <xsl:when test="$Value=''"> </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$Value"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of select="substring(concat($ModifiedStringValue, '                                                                                                    '), 1, $FieldLength)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="ConvertAlphaStateCdToNumericStateCd">
        <xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='AL'">01</xsl:when>
			<xsl:when test="$Value='AZ'">02</xsl:when>
			<xsl:when test="$Value='AR'">03</xsl:when>
			<xsl:when test="$Value='CA'">04</xsl:when>
			<xsl:when test="$Value='CO'">05</xsl:when>
			<xsl:when test="$Value='CT'">06</xsl:when>
			<xsl:when test="$Value='DE'">07</xsl:when>
			<xsl:when test="$Value='DC'">08</xsl:when>
			<xsl:when test="$Value='FL'">09</xsl:when>
			<xsl:when test="$Value='GA'">10</xsl:when>
			<xsl:when test="$Value='ID'">11</xsl:when>
			<xsl:when test="$Value='IL'">12</xsl:when>
			<xsl:when test="$Value='IN'">13</xsl:when>
			<xsl:when test="$Value='IA'">14</xsl:when>
			<xsl:when test="$Value='KS'">15</xsl:when>
			<xsl:when test="$Value='KY'">16</xsl:when>
			<xsl:when test="$Value='LA'">17</xsl:when>
			<xsl:when test="$Value='ME'">18</xsl:when>
			<xsl:when test="$Value='MD'">19</xsl:when>
			<xsl:when test="$Value='MA'">20</xsl:when>
			<xsl:when test="$Value='MI'">21</xsl:when>
			<xsl:when test="$Value='MN'">22</xsl:when>
			<xsl:when test="$Value='MS'">23</xsl:when>
			<xsl:when test="$Value='MO'">24</xsl:when>
			<xsl:when test="$Value='MT'">25</xsl:when>
			<xsl:when test="$Value='NE'">26</xsl:when>
			<xsl:when test="$Value='NV'">27</xsl:when>
			<xsl:when test="$Value='NH'">28</xsl:when>
			<xsl:when test="$Value='NJ'">29</xsl:when>
			<xsl:when test="$Value='NM'">30</xsl:when>
			<xsl:when test="$Value='NY'">31</xsl:when>
			<xsl:when test="$Value='NC'">32</xsl:when>
			<xsl:when test="$Value='ND'">33</xsl:when>
			<xsl:when test="$Value='OH'">34</xsl:when>
			<xsl:when test="$Value='OK'">35</xsl:when>
			<xsl:when test="$Value='OR'">36</xsl:when>
			<xsl:when test="$Value='PA'">37</xsl:when>
			<xsl:when test="$Value='RI'">38</xsl:when>
			<xsl:when test="$Value='SC'">39</xsl:when>
			<xsl:when test="$Value='SD'">40</xsl:when>
			<xsl:when test="$Value='TN'">41</xsl:when>
			<xsl:when test="$Value='TX'">42</xsl:when>
			<xsl:when test="$Value='UT'">43</xsl:when>
			<xsl:when test="$Value='VT'">44</xsl:when>
			<xsl:when test="$Value='VA'">45</xsl:when>
			<xsl:when test="$Value='WA'">46</xsl:when>
			<xsl:when test="$Value='WV'">47</xsl:when>
			<xsl:when test="$Value='WI'">48</xsl:when>
			<xsl:when test="$Value='WY'">49</xsl:when>
			<xsl:when test="$Value='HI'">50</xsl:when>
			<xsl:when test="$Value='AK'">51</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	    
<xsl:template name="ConvertHOSIIExtCovCdtoPremiumForm">
<xsl:param name="Value"/>
<xsl:choose>
			<xsl:when test="$Value='H313'"><xsl:text>313</xsl:text></xsl:when>
			<xsl:when test="$Value='H035'"><xsl:text>035</xsl:text></xsl:when>
			<xsl:when test="$Value='H041'"><xsl:text>041</xsl:text></xsl:when>
			<xsl:when test="$Value='H048'"><xsl:text>048</xsl:text></xsl:when>
			<xsl:when test="$Value='H053'"><xsl:text>053</xsl:text></xsl:when>
			<xsl:when test="$Value='HCCR'"><xsl:text>CCR</xsl:text></xsl:when>
			<xsl:when test="$Value='H071'"><xsl:text>071</xsl:text></xsl:when>
			<xsl:when test="$Value='HWCR'"><xsl:text>075</xsl:text></xsl:when>
			<xsl:when test="$Value='HOBM'"><xsl:text>011</xsl:text></xsl:when>
			<xsl:when test="$Value='H314'"><xsl:text>314</xsl:text></xsl:when>
			<xsl:when test="$Value='H315'"><xsl:text>315</xsl:text></xsl:when>
			<xsl:when test="$Value='H099'"><xsl:text>099</xsl:text></xsl:when>
			<xsl:when test="$Value='IJE#'"><xsl:text>801</xsl:text></xsl:when>
			<xsl:when test="$Value='IFU#'"><xsl:text>802</xsl:text></xsl:when>
			<xsl:when test="$Value='IFA#'"><xsl:text>803</xsl:text></xsl:when>
			<xsl:when test="$Value='IFF#'"><xsl:text>FF0</xsl:text></xsl:when>
			<xsl:when test="$Value='ICA#'"><xsl:text>805</xsl:text></xsl:when>
			<xsl:when test="$Value='ICO#'"><xsl:text>806</xsl:text></xsl:when>
			<xsl:when test="$Value='IGE#'"><xsl:text>808</xsl:text></xsl:when>
			<xsl:when test="$Value='IMI#'"><xsl:text>809</xsl:text></xsl:when>
			<xsl:when test="$Value='IGU#'"><xsl:text>818</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Value"/>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>
</xsl:stylesheet>  

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\WaterCraft.xml" htmlbaseurl="" outputurl="..\ImageHo.xml" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->