<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- 55614 begin -->
    <xsl:variable name="KEY">
  	        <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL"/>
	   	  <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER"/>
	   	  <xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__KEY/PIF__MODULE"/>
   </xsl:variable>        
   <!-- 55614 end -->
	<xsl:template name="BuildStateProvCd">
		<xsl:param name="StateCd"/>
		<xsl:choose>
			<xsl:when test="$StateCd='01'">AL</xsl:when>
			<xsl:when test="$StateCd='02'">AZ</xsl:when>
			<xsl:when test="$StateCd='03'">AR</xsl:when>
			<xsl:when test="$StateCd='04'">CA</xsl:when>
			<xsl:when test="$StateCd='05'">CO</xsl:when>
			<xsl:when test="$StateCd='06'">CT</xsl:when>
			<xsl:when test="$StateCd='07'">DE</xsl:when>
			<xsl:when test="$StateCd='08'">DC</xsl:when>
			<xsl:when test="$StateCd='09'">FL</xsl:when>
			<xsl:when test="$StateCd='10'">GA</xsl:when>
			<xsl:when test="$StateCd='11'">ID</xsl:when>
			<xsl:when test="$StateCd='12'">IL</xsl:when>
			<xsl:when test="$StateCd='13'">IN</xsl:when>
			<xsl:when test="$StateCd='14'">IA</xsl:when>
			<xsl:when test="$StateCd='15'">KS</xsl:when>
			<xsl:when test="$StateCd='16'">KY</xsl:when>
			<xsl:when test="$StateCd='17'">LA</xsl:when>
			<xsl:when test="$StateCd='18'">ME</xsl:when>
			<xsl:when test="$StateCd='19'">MD</xsl:when>
			<xsl:when test="$StateCd='20'">MA</xsl:when>
			<xsl:when test="$StateCd='21'">MI</xsl:when>
			<xsl:when test="$StateCd='22'">MN</xsl:when>
			<xsl:when test="$StateCd='23'">MS</xsl:when>
			<xsl:when test="$StateCd='24'">MO</xsl:when>
			<xsl:when test="$StateCd='25'">MT</xsl:when>
			<xsl:when test="$StateCd='26'">NE</xsl:when>
			<xsl:when test="$StateCd='27'">NV</xsl:when>
			<xsl:when test="$StateCd='28'">NH</xsl:when>
			<xsl:when test="$StateCd='29'">NJ</xsl:when>
			<xsl:when test="$StateCd='30'">NM</xsl:when>
			<xsl:when test="$StateCd='31'">NY</xsl:when>
			<xsl:when test="$StateCd='32'">NC</xsl:when>
			<xsl:when test="$StateCd='33'">ND</xsl:when>
			<xsl:when test="$StateCd='34'">OH</xsl:when>
			<xsl:when test="$StateCd='35'">OK</xsl:when>
			<xsl:when test="$StateCd='36'">OR</xsl:when>
			<xsl:when test="$StateCd='37'">PA</xsl:when>
			<xsl:when test="$StateCd='38'">RI</xsl:when>
			<xsl:when test="$StateCd='39'">SC</xsl:when>
			<xsl:when test="$StateCd='40'">SD</xsl:when>
			<xsl:when test="$StateCd='41'">TN</xsl:when>
			<xsl:when test="$StateCd='42'">TX</xsl:when>
			<xsl:when test="$StateCd='43'">UT</xsl:when>
			<xsl:when test="$StateCd='44'">VT</xsl:when>
			<xsl:when test="$StateCd='45'">VA</xsl:when>
			<xsl:when test="$StateCd='46'">WA</xsl:when>
			<xsl:when test="$StateCd='47'">WV</xsl:when>
			<xsl:when test="$StateCd='48'">WI</xsl:when>
			<xsl:when test="$StateCd='49'">WY</xsl:when>
			<xsl:when test="$StateCd='50'">HI</xsl:when>
			<xsl:when test="$StateCd='51'">AK</xsl:when>
		</xsl:choose>
	</xsl:template>
        <!-- Added RetrieveBOPCovCd template 10/20/03  -->
        <xsl:template name="RetrieveBOPCovCd">
           <xsl:param name="RXRiskUnitGroup"/>
                <xsl:choose>
                        <xsl:when test="$RXRiskUnitGroup='400'"><CoverageCd>BC</CoverageCd></xsl:when>  
                        <xsl:when test="$RXRiskUnitGroup='402'"><CoverageCd>CE</CoverageCd></xsl:when> 
                        <xsl:when test="$RXRiskUnitGroup='404'"><CoverageCd>AR</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='409'"><CoverageCd>FA</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='412'"><CoverageCd>IN</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='421'"><CoverageCd>TF</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='430'"><CoverageCd>PS</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='438'"><CoverageCd>VP</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='460'"><CoverageCd>CA</CoverageCd></xsl:when>
                        <xsl:when test="$RXRiskUnitGroup='706'"><CoverageCd>FG</CoverageCd></xsl:when> 
                        <xsl:when test="$RXRiskUnitGroup='415'"><CoverageCd>SP</CoverageCd></xsl:when> 
                        <xsl:otherwise/>   
                </xsl:choose>
        </xsl:template>
		<xsl:template name="TranslateCoverageDesc">
		<xsl:param name="CoverageCd"/>
		<xsl:choose>
			<xsl:when test="$CoverageCd='02'">BI</xsl:when>
			<xsl:when test="$CoverageCd='05'">PD</xsl:when>
			<xsl:when test="$CoverageCd='07'">MP</xsl:when>
			<xsl:when test="$CoverageCd='46'">CMP</xsl:when>
			<xsl:when test="$CoverageCd='47'">COL</xsl:when>
			<xsl:otherwise><xsl:value-of select="$CoverageCd"/></xsl:otherwise>   
		</xsl:choose>
	</xsl:template>

	<!-- Case 34768 Begin -->
	<!--  Format an eight digit S2 date (i.e. 20011231) to an ISO date (i.e. 2001-12-31) -->
	<xsl:template name="ConvertS2DateToISODate">
		<xsl:param name="Value"/>
  
		<!-- <xsl:variable name="HoldDateValue">
			<xsl:value-of select="$Value"/>
		</xsl:variable> -->

				<!--  Year -->
       	        <xsl:value-of select="concat(substring($Value,1,4),'/')"/>
				<!--  Month -->
        	<xsl:value-of select="concat(substring($Value,5,2),'/')"/>
				<!--  Day -->
        	<xsl:value-of select="substring($Value,7,2)"/>        
	</xsl:template>
	<!-- Case 34768 End -->

</xsl:stylesheet>


<!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->