<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="xalan://java.util.GregorianCalendar">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service case .NET 
***********************************************************************************************
-->

	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2Pinfo53.xsl"/>
	<xsl:include href="S2EPF00.xsl"/>

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="string('CPP')"/>
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/PolicyNumber"/>
	<xsl:variable name="MOD">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/PolicyVersion"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>            
	</xsl:variable>
	<!--<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
	<xsl:variable name="PolicyMode" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/PolicyStatusCd"/>		<!--103409-->
	<xsl:variable name="LOB" select="string('CPP')"/>
	<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/TransactionEffectiveDt"/>
	<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/TransactionRequestDt"/>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/ContractTerm/ExpirationDt"/>

	<xsl:variable name="tmp" select="date:new()"/>		
	<xsl:variable name="month" select="substring(concat('0',date:get($tmp, 2) + 1),string-length(date:get($tmp, 2) + 1))" />
	<xsl:variable name="day" select="substring(concat('0',date:get($tmp, 5)),string-length(date:get($tmp, 5)))" />
	<xsl:variable name="year" select="date:get($tmp, 1)" />
	<xsl:variable name="ActDate">
		<xsl:value-of select="$year"/>/<xsl:value-of select="$month"/>/<xsl:value-of select="$day"/>
	</xsl:variable>

	<xsl:template match="/">

		<xsl:element name="com.csc_CommlPkgPolicyQuoteInqRq">
			<!-- Create 02 Segment -->
			<xsl:call-template name="S2PinfoTemplate">   
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<xsl:value-of select="$PolicyMode"/>
				</xsl:with-param>
			</xsl:call-template>

			<!-- Lastly, create the 00 Segment -->
			<xsl:call-template name="S2EPF00Template">  
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
				<xsl:with-param name="PolicyMode">
					<xsl:value-of select="$PolicyMode"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>			
	</xsl:template>

</xsl:stylesheet>