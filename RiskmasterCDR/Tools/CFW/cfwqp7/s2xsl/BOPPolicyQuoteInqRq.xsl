<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service case 35271
***********************************************************************************************-->
	<xsl:include href="CommonFuncRq.xsl"/>
	<!-- Common functions -->
	<xsl:include href="BOS2Pinfo53.xsl"/>
	<!-- Policy Information (Pinfo53.cbl) -->
	<xsl:include href="BOS2LocationSeg.xsl"/>
	<!-- Location Information (pmd4j.cbl) -->
	<xsl:include href="BOS2BuildingSeg.xsl"/>
	<!-- Building Information (pmdnxb1.cbl) -->
	<xsl:include href="BOS2AddIntSeg.xsl"/>
	<!-- Additional Interest Information (pmd4j.cbl) -->
	<xsl:include href="BOS2Coverage.xsl"/>
	<!-- Coverage Information (pmduxb1.cbl) -->

	<xsl:include href="S2EPF00.xsl"/>
	<!-- Segment 00 -->

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CompanyProductCd"/>
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyNumber"/>
	<xsl:variable name="MOD">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyVersion"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/LOBCd"/>
	<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="TransRqDt">1900/01/01</xsl:variable>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/ExpirationDt"/>

	<xsl:template match="/">
		<xsl:element name="BOPolicyQuoteInqRq">
			<xsl:call-template name="S2PinfoTemplate">
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
			</xsl:call-template>

			<xsl:for-each select="ACORD/InsuranceSvcRq/*/Location">

				<xsl:variable name="RefLocId" select="@id"/>

				<!-- Build Location Records -->
				<xsl:call-template name="S2LocationTemplate"/>

				<!-- Build SubLocation Records -->
				<xsl:for-each select="SubLocation">
					<xsl:variable name="RefSubLocId" select="@id"/>
					<xsl:call-template name="S2BldgTemplate"/>

					<!-- Build Additional Interests Attached to SubLocations -->
					<xsl:for-each select="AdditionalInterest">
						<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
							<xsl:call-template name="S2AddIntTemplate"/>
						</xsl:if>
					</xsl:for-each>

					<!-- Build Coverage Segments -->
					<!-- Building and Property Coverage Segment -->
					<xsl:call-template name="S2CoverageTemplate02">
						<xsl:with-param name="RefLocId" select="../@id"/>
						<xsl:with-param name="RefSubLocId" select="@id"/>
						<xsl:with-param name="LocId" select="../ItemIdInfo/InsurerId"/>
						<xsl:with-param name="SubLocId" select="ItemIdInfo/InsurerId"/>
					</xsl:call-template>

					<!-- Build Optional Coverage Segment -->
					<xsl:call-template name="S2CoverageTemplate01">
						<xsl:with-param name="RefLocId" select="../@id"/>
						<xsl:with-param name="RefSubLocId" select="@id"/>
						<xsl:with-param name="LocId" select="../ItemIdInfo/InsurerId"/>
						<xsl:with-param name="SubLocId" select="ItemIdInfo/InsurerId"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>

			<xsl:call-template name="S2EPF00Template">
				<xsl:with-param name="ProcessingType">Q</xsl:with-param>
				<xsl:with-param name="PolicyMode">N</xsl:with-param>	<!-- New Business -->
			</xsl:call-template>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="BORQ_S2_ACORD.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->