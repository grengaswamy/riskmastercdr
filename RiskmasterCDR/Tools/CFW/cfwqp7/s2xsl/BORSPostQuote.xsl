<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
***********************************************************************************************
	This XSL stylesheet is used by Generic rating to generate the Stored procedures
	to update the iSolutions database with policy premium data.
	E-Service case 29405 
***********************************************************************************************
-->

	<xsl:template match="/">
		<!-- Clear out the old rate information -->EXEC BODeleteRating @AppId='!APPID!';
		EXEC SHDeleteRatingErrors @AppId='!APPID!';

		<!-- Added if condition for PT Issue 35271 -->
		<xsl:if test="ACORD/SignonRs/ClientApp/Name='PT'">
		<!-- First update the Total Premium for PT policy -->
		    EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='0',
					@BuildingNbr='0',
					@CoverageCd='0',
					@CoverageSeqNbr='0',
					@TotalExposureAmt='0',
					@PremiumAmt='<xsl:value-of select="/ACORD/InsuranceSvcRs/*/CommlPolicy/CurrentTermAmt/Amt"/>',
					@DeductibleAmt='0';
		</xsl:if>

		<!-- Start Issue 35271 -->
		<xsl:if test="ACORD/SignonRs/ClientApp/Name='S2'">

			<xsl:variable name="TotalAmt">
				<xsl:choose>
					<xsl:when test="normalize-space(/ACORD/InsuranceSvcRs/*/CommlPolicy/CurrentTermAmt/Amt)=''">
						<xsl:value-of select="number('0')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="number(/ACORD/InsuranceSvcRs/*/CommlPolicy/CurrentTermAmt/Amt)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
		
			<xsl:variable name="TaxAmt">
				<xsl:choose>
					<xsl:when test="normalize-space(/ACORD/InsuranceSvcRs/*/CommlPolicy/CommlPolicySupplement/CreditOrSurcharge/NumericValue/FormatCurrencyAmt/Amt)=''">
						<xsl:value-of select="number('0')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="number(/ACORD/InsuranceSvcRs/*/CommlPolicy/CommlPolicySupplement/CreditOrSurcharge/NumericValue/FormatCurrencyAmt/Amt)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
		
			<xsl:variable name="PolicyAmt" select="number($TotalAmt)+number($TaxAmt)"/>

			<!-- First update the Total Premium for S2 policy -->
			EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='0',
					@BuildingNbr='0',
					@CoverageCd='0',
					@CoverageSeqNbr='0',
					@TotalExposureAmt='0',
					@PremiumAmt='<xsl:value-of select="$PolicyAmt"/>',
					@DeductibleAmt='0';

			<!-- Update Tax and surcharge amount for S2 -->
		    EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='0',
					@BuildingNbr='0',
					@CoverageCd='TAX',
					@CoverageSeqNbr='0',
					@TotalExposureAmt='0',
					@PremiumAmt='<xsl:value-of select="$TaxAmt"/>',
					@DeductibleAmt='0';

			<!-- Update the total premium for the policy in SHPlanInfo table -->
	    	EXEC SHUpdateTotalPremium @AppId='!APPID!',
	        @Premium='<xsl:value-of select="round($PolicyAmt)"/>'

		</xsl:if>
		<!-- End Issue 35271 -->

		<!-- Update the premium information for each location-building --><xsl:for-each select="/ACORD/InsuranceSvcRs/*/BOPLineBusiness/PropertyInfo/CommlPropertyInfo"><xsl:variable name="Location" select="@LocationRef"/><xsl:variable name="SubLocation" select="@SubLocationRef"/>
	    EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='<xsl:value-of select="substring(@LocationRef,2,1)"/>',
					@BuildingNbr='<xsl:value-of select="substring(@SubLocationRef,2,1)"/>',
					@CoverageCd='0',
					@CoverageSeqNbr='0',
					@TotalExposureAmt='0',
					@PremiumAmt='<xsl:value-of select="sum(//CommlPropertyInfo[@LocationRef=$Location and @SubLocationRef=$SubLocation]/CommlCoverage/CurrentTermAmt/Amt)"/>',
					@DeductibleAmt='0';</xsl:for-each>
		<!-- Update the premium information for Insurance Line Coverages --><xsl:for-each select="/ACORD/InsuranceSvcRs/*/BOPLineBusiness/PropertyInfo/CommlCoverage"><xsl:if test="CoverageCd!=''">
	   	EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='0',
					@BuildingNbr='0',
					@CoverageCd='<xsl:value-of select="CoverageCd"/>',
					@CoverageSeqNbr='<xsl:value-of select="IterationNumber"/>',
					@TotalExposureAmt='<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>',
					@PremiumAmt='<xsl:value-of select="CurrentTermAmt/Amt"/>',
					@DeductibleAmt='0';</xsl:if></xsl:for-each>
		<!-- Update the premium information for Optional Coverage --><xsl:for-each select="/ACORD/InsuranceSvcRs/*/BOPLineBusiness/PropertyInfo/CommlPropertyInfo/CommlCoverage"><xsl:if test="CoverageCd!=''"><xsl:variable name="DeductibleAmt" select="Deductible/FormatCurrencyAmt/Amt"/>
	  	EXEC BOInsertRating @AppId='!APPID!',
					@InsuranceLineCd='BOP',
					@LocationNbr='<xsl:value-of select="substring(parent::CommlPropertyInfo/@LocationRef,2,1)"/>',
					@BuildingNbr='<xsl:value-of select="substring(parent::CommlPropertyInfo/@SubLocationRef,2,1)"/>',
					@CoverageCd='<xsl:value-of select="translate(CoverageCd,'&amp;','')"/>',
					@CoverageSeqNbr='1',
					@TotalExposureAmt='<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>',
					@PremiumAmt='<xsl:value-of select="CurrentTermAmt/Amt"/>',
					@DeductibleAmt='<xsl:choose><xsl:when test="normalize-space($DeductibleAmt)=''">0</xsl:when><xsl:otherwise><xsl:value-of select="$DeductibleAmt"/></xsl:otherwise></xsl:choose>'</xsl:if></xsl:for-each>
		
		<!-- Added if condition for PT Issue 35271 -->
		<xsl:if test="ACORD/SignonRs/ClientApp/Name='PT'">
			<!-- Update the total premium for the policy in SHPlanInfo table -->
	    	EXEC SHUpdateTotalPremium @AppId='!APPID!',
	        @Premium='<xsl:value-of select="/ACORD/InsuranceSvcRs/*/CommlPolicy/CurrentTermAmt/Amt"/>'
		</xsl:if>

		<!-- Tag policy as Quoted -->
		EXEC SHSetQuoted @AppId='!APPID!'</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="96265128824000003136BORSQuoteS2.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->