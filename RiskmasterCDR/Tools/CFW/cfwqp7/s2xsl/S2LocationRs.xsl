<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template name="CreateLocationFrom12SEG">
		<xsl:for-each select="/*/DESCRIPTION__FULL__SEG">
			<xsl:if test="DESCRIPTION__FULL__SEG/DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE='LA'">
				<Location>
					<Addr>
						<Addr1>
							<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1"/>
						</Addr1>
						<Addr2>
							<xsl:value-of select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__2"/>
						</Addr2>
                                                
                                                <City>
                                                      <xsl:value-of select="DESCRIPTION__INFORMATION/DESC__CITY"/> 
                                                </City>
                                                <StateProvCd>
                                                      <xsl:value-of select="DESCRIPTION__INFORMATION/DESC__STATE"/> 
                                                </StateProvCd>
						<PostalCode>
							<xsl:choose>
								<xsl:when test="string-length(DESC__ZIP__PLUS4) &gt; 0">
									<xsl:value-of select="concat(concat(substring(DESCRIPTION__INFORMATION/DESCRIPTION__ZIP__CODE,2,5),'-'),DESC__ZIP__PLUS4)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="substring(DESCRIPTION__INFORMATION/DESCRIPTION__ZIP__CODE,2,5)"/>
								</xsl:otherwise>
							</xsl:choose>
						</PostalCode>
					</Addr>
                                        <TaxCodeInfo>
                                            <TaxCd>
                                                 <xsl:value-of select="/*/HRR__INFO/HRR__TAX__LOCATION"/> 
                                            </TaxCd>
                                        </TaxCodeInfo>                 
				</Location>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="CreateLocationFrom43SEG">
		<xsl:param name="LOB"/>
		<xsl:for-each select="/*/PMD4J__NAME__ADDR__SEG">
			<xsl:sort select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER"/>
			<xsl:if test="PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__LEVEL__CODE='J' and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__PART__CODE='J'                                                       and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SUB__PART__CODE='1'">
				<Location>
					<xsl:attribute name="id">
					   <!-- 55614 Begin -->
						<!--<xsl:value-of select="concat('L',substring(PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER,3,2))"/> -->
						<xsl:value-of select="concat($KEY,'-l',number(PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER))"/>
						<!-- 55614 End -->
					</xsl:attribute>
					<xsl:variable name="LJ-LOCNUM" select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER"/>
					<ItemIdInfo>
						<InsurerId>
							<xsl:value-of select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER"/>
						</InsurerId>
						<OtherIdentifier>
							<OtherId>
								<xsl:value-of select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER"/>
							</OtherId>
						</OtherIdentifier>
					</ItemIdInfo>
					<Addr>
						<Addr1>
							<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__1"/>
						</Addr1>
						<Addr2>
							<xsl:value-of select="concat(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__2/PMD4J__ADDR__LIN__2__POS__1, PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__2/PMD4J__ADDR__LIN__2__POS__2__30)"/>
						</Addr2>
						<xsl:choose>
							<xsl:when test="string-length(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4) &gt; 0">
								<xsl:variable name="Addr4City">
									<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,',')"/>
								</xsl:variable>
								<xsl:variable name="Addr4State">
									<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,', ')"/>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="string-length($Addr4City) &gt; 0">
										<City>
											<xsl:value-of select="$Addr4City"/>
										</City>
										<StateProvCd>
											<xsl:value-of select="$Addr4State"/>
										</StateProvCd>
									</xsl:when>
									<xsl:when test="string-length(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4) &gt; 2"> <!-- No comma between city and state. -->
										<City>
											<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,' ')"/>
										</City>
										<StateProvCd>
											<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,' ')"/>
										</StateProvCd>
									</xsl:when>
									<xsl:otherwise> <!-- If Address line 4 is not empty, and there is no comma seperating the city and state, then the state could be on line 4 by itself. -->
										<City>
											<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3"/>
										</City>
										<StateProvCd>
											<xsl:value-of select="substring(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4, 1, 2)"/>
										</StateProvCd>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="Addr3City">
									<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3,',')"/>
								</xsl:variable>
								<xsl:variable name="Addr3State">
									<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3,', ')"/>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="string-length($Addr3City) &gt; 0">
										<City>
											<xsl:value-of select="$Addr3City"/>
										</City>
										<StateProvCd>
											<xsl:value-of select="$Addr3State"/>
										</StateProvCd>
									</xsl:when>
									<xsl:otherwise> <!-- There is no comma seperating the city and state. -->
										<City>
											<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3, ' ')"/>
										</City>
										<StateProvCd>
											<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3, ' ')"/>
										</StateProvCd>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<PostalCode>
							<xsl:value-of select="concat(concat(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__BASIC, PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__DASH), PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__EXPANDED__A)"/>
						</PostalCode>
						<County>
							<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__COUNTY"/>
						</County>
					</Addr>  

                                        <TaxCodeInfo>
                                             <TaxCd>
                                               <xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__SORT__NAME__AREA/PMD4J__TAX__LOC"/> 
                                             </TaxCd>                                           
                                        </TaxCodeInfo> 
                                        <!-- call to additional interest stylesheet -->
                                <xsl:call-template name="CreateAdditionalInterestFrom43SEG">  
					<xsl:with-param name="LOB" select="$LOB"/>
					<xsl:with-param name="LJ-LOCNUM" select="$LJ-LOCNUM"/>
				</xsl:call-template>
				</Location>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>