<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:variable name="pC" select="'*'"/>
	<!-- Template for Vehicle Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateCoverage">
		<xsl:variable name="Cov" select="position()"/>
		<xsl:variable name="Coverage" select="SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA"/>
		<xsl:for-each select="../VEHICLE__DESCRIPTION__SEG">
			<xsl:if test="position() = $Cov">
				<xsl:call-template name="ExtractCoverage">
					<xsl:with-param name="CovData" select="$Coverage"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="ExtractCoverage">
		<xsl:param name="CovData"/>
		<xsl:variable name="CovString">
			<xsl:if test="string-length($CovData) &gt; 0">
				<xsl:choose>
					<xsl:when test="string-length($CovData) = '238'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17),$pC,substring($CovData,154,17),$pC,substring($CovData,171,17),$pC,substring($CovData,188,17),$pC,substring($CovData,205,17),$pC,substring($CovData,222,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '221'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17),$pC,substring($CovData,154,17),$pC,substring($CovData,171,17),$pC,substring($CovData,188,17),$pC,substring($CovData,205,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '204'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17),$pC,substring($CovData,154,17),$pC,substring($CovData,171,17),$pC,substring($CovData,188,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '187'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17),$pC,substring($CovData,154,17),$pC,substring($CovData,171,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '170'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17),$pC,substring($CovData,154,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '153'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17),$pC,substring($CovData,137,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '136'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17),$pC,substring($CovData,120,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '119'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17),$pC,substring($CovData,103,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '102'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17),$pC,substring($CovData,86,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '85'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17),$pC,substring($CovData,69,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '68'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17),$pC,substring($CovData,52,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '51'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17),$pC,substring($CovData,35,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '34'">
						<xsl:value-of select="concat(substring($CovData,1,17),$pC,substring($CovData,18,17))"/>
					</xsl:when>
					<xsl:when test="string-length($CovData) = '17'">
						<xsl:value-of select="substring($CovData,1,17)"/>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($CovString) &gt; 17">
				<xsl:call-template name="AssignValues">
					<xsl:with-param name="str1" select="substring-before($CovString,$pC)"/>
					<xsl:with-param name="str2" select="substring-after($CovString,$pC)"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length($CovString) = 0">
				<xsl:call-template name="AssignValues">
					<xsl:with-param name="str1"/>
					<xsl:with-param name="str2"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="AssignValues">
					<xsl:with-param name="str1" select="$CovString"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="AssignValues">
		<xsl:param name="str1"/>
		<xsl:param name="str2"/>
		<xsl:param name="lob"/>
		<xsl:if test="$lob='APV'">
			<xsl:if test="string-length($str1) &gt; 0">
				<Coverage>
					<CoverageCd>
						<xsl:value-of select="substring($str1,1,2)"/>
					</CoverageCd>
					<CoverageDesc/>
					<xsl:if test="(substring($str1,1,2) &gt; 0 and substring($str1,1,2) &lt; 36) or (substring($str1,1,2) &gt; 36 and substring($str1,1,2) &lt; 44) or substring($str1,1,2)= 45">
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="normalize-space(substring($str1,3,7))"/>
								</Amt>
							</FormatCurrencyAmt>
						</Limit>
					</xsl:if>
					<xsl:if test="substring($str1,1,2)= 36 or substring($str1,1,2)= 44 or substring($str1,1,2) = 46 or (substring($str1,1,2) &gt; 46 and substring($str1,1,2) &lt; 50)">
						<Deductible>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="normalize-space(substring($str1,3,7))"/>
								</Amt>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
					</xsl:if>
					<xsl:if test="normalize-space(substring($str1,1,2)) = 'Y'">
						<Option>
							<OptionTypeCd>YNInd1</OptionTypeCd>
							<OptionCd/>
							<OptionValue>1</OptionValue>
						</Option>
					</xsl:if>
					<xsl:if test="normalize-space(substring($str1,1,2)) = 'N'">
						<Option>
							<OptionTypeCd>YNInd1</OptionTypeCd>
							<OptionCd/>
							<OptionValue>0</OptionValue>
						</Option>
					</xsl:if>
					<CurrentTermAmt>
						<Amt>
							<xsl:variable name="TermAmt">
								<xsl:choose>
									<xsl:when test="contains(normalize-space(substring($str1,10,7)),'{')">
										<xsl:value-of select="translate(substring($str1,10,7),'{','0')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="normalize-space(substring($str1,10,7))"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:value-of select="$TermAmt"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="string-length($str2) &gt; 17">
					<xsl:call-template name="AssignValues">
						<xsl:with-param name="str1" select="substring-before($str2,$pC)"/>
						<xsl:with-param name="str2" select="substring-after($str2,$pC)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="string-length($str2) = 17">
					<xsl:call-template name="AssignValues">
						<xsl:with-param name="str1" select="$str2"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$lob = 'ACV'">
			<xsl:if test="string-length($str1) &gt; 0">
				<CommlCoverage>
					<CoverageCd>
						<xsl:value-of select="substring($str1,1,2)"/>
					</CoverageCd>
					<CoverageDesc/>
					<xsl:if test="(substring($str1,1,2) &gt; 0 and substring($str1,1,2) &lt; 36) or (substring($str1,1,2) &gt; 36 and substring($str1,1,2) &lt; 44) or substring($str1,1,2)= 45">
						<Limit>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="normalize-space(substring($str1,3,7))"/>
								</Amt>
							</FormatCurrencyAmt>
						</Limit>
					</xsl:if>
					<xsl:if test="substring($str1,1,2)= 36 or substring($str1,1,2)= 44 or substring($str1,1,2) = 46 or (substring($str1,1,2) &gt; 46 and substring($str1,1,2) &lt; 50)">
						<Deductible>
							<FormatCurrencyAmt>
								<Amt>
									<xsl:value-of select="normalize-space(substring($str1,3,7))"/>
								</Amt>
							</FormatCurrencyAmt>
							<DeductibleTypeCd/>
						</Deductible>
					</xsl:if>
					<xsl:if test="normalize-space(substring($str1,1,2)) = 'Y'">
						<Option>
							<OptionTypeCd>YNInd1</OptionTypeCd>
							<OptionCd/>
							<OptionValue>1</OptionValue>
						</Option>
					</xsl:if>
					<xsl:if test="normalize-space(substring($str1,1,2)) = 'N'">
						<Option>
							<OptionTypeCd>YNInd1</OptionTypeCd>
							<OptionCd/>
							<OptionValue>0</OptionValue>
						</Option>
					</xsl:if>
					<CurrentTermAmt>
						<Amt>
							<xsl:variable name="TermAmt">
								<xsl:choose>
									<xsl:when test="contains(normalize-space(substring($str1,10,7)),'{')">
										<xsl:value-of select="translate(substring($str1,10,7),'{','0')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="normalize-space(substring($str1,10,7))"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:value-of select="$TermAmt"/>
						</Amt>
					</CurrentTermAmt>
				</CommlCoverage>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="string-length($str2) &gt; 17">
					<xsl:call-template name="AssignValues">
						<xsl:with-param name="str1" select="substring-before($str2,$pC)"/>
						<xsl:with-param name="str2" select="substring-after($str2,$pC)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="string-length($str2) = 17">
					<xsl:call-template name="AssignValues">
						<xsl:with-param name="str1" select="$str2"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template match="/*/DESCRIPTION__FULL_SEG">
		<xsl:param name="LOB"/>
		<xsl:if test="$LOB='ACV' or $LOB='BOP' or $LOB='WCP'">
			<CommlCoverage>
				<CoverageCd>
					<xsl:value-of select="substring($str1,1,2)"/>
				</CoverageCd>
				<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__CLASS__TERM__PREM"/>
					</Amt>
				</CurrentTermAmt>
			</CommlCoverage>
		</xsl:if>
	</xsl:template>

	<!-- Template for BA Hired Auto Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateHACoverage">
		<xsl:param name="HACd"/>
		<CommlCoverage>
			<CoverageCd/>
			<IterationNumber/>
			<CoverageDesc/>
			<xsl:choose>
				<xsl:when test="substring($HACd,1,2) ='66'">
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="normalize-space(substring(./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA,3,7))"/>
							</Amt>
							<CurCd/>
						</FormatCurrencyAmt>
					</Limit>
				</xsl:when>
				<xsl:when test="substring($HACd,1,2) ='99'">
					<Deductible>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="normalize-space(substring(./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA,3,7))"/>
							</Amt>
							<CurCd/>
						</FormatCurrencyAmt>
					</Deductible>
				</xsl:when>
			</xsl:choose>
		</CommlCoverage>
	</xsl:template>

	<!-- Template for BA NonOwned Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateNOCoverage">
		<xsl:param name="NOCd"/>
		<CommlCoverage>
			<CoverageCd/>
			<IterationNumber/>
			<CoverageDesc/>
			<Limit>
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="normalize-space(substring(./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA,3,7))"/>
					</Amt>
					<CurCd/>
				</FormatCurrencyAmt>
			</Limit>
		</CommlCoverage>
	</xsl:template>

	<!-- Template for BA Drive Other Car Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateDOCoverage">
		<xsl:param name="DOCd"/>
		<CommlCoverage>
			<CoverageCd/>
			<IterationNumber/>
			<CoverageDesc/>
			<Limit>
				<FormatCurrencyAmt>
					<Amt>
						<xsl:value-of select="normalize-space(substring(./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA,3,7))"/>
					</Amt>
					<CurCd/>
				</FormatCurrencyAmt>
			</Limit>
		</CommlCoverage>
	</xsl:template>

	<!-- Template for BA Business Auto Plus Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateBPCoverage">
		<xsl:param name="BPCd"/>
		<CommlCoverage>
			<CoverageCd/>
			<IterationNumber/>
			<CoverageDesc/>
			<Option>
				<OptionTypeCd/>
				<OptionCd/>
				<OptionValue>
					<xsl:value-of select="normalize-space(substring(./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES/VCR__COVERAGE__DATA,3,7))"/>
				</OptionValue>
			</Option>
		</CommlCoverage>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->