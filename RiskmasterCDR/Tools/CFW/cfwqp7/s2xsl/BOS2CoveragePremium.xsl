<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2CovPremTemplate01" match="/">
		<!-- For Optional Coverages -->
		<xsl:param name="RefLocId">
		</xsl:param>
		<xsl:param name="RefSubLocId">
		</xsl:param>
		<xsl:param name="LocId">
		</xsl:param>
		<xsl:param name="SubLocId">
		</xsl:param>
		<BUS__OBJ__RECORD>
			<PMDUYB1__RATE__OVERRIDE__SEG>
				<PMDUYB1__SEGMENT__KEY>
					<PMDUYB1__REC__LLBB/>
					<PMDUYB1__SEGMENT__ID>43</PMDUYB1__SEGMENT__ID>
					<PMDUYB1__SEGMENT__STATUS>A</PMDUYB1__SEGMENT__STATUS>
					<PMDUYB1__TRANSACTION__DATE>
						<PMDUYB1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,1,4)"/>
						</PMDUYB1__YEAR__TRANSACTION>
						<PMDUYB1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,6,2)"/>
						</PMDUYB1__MONTH__TRANSACTION>
						<PMDUYB1__DAY__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,9,2)"/>
						</PMDUYB1__DAY__TRANSACTION>
					</PMDUYB1__TRANSACTION__DATE>
					<PMDUYB1__SEGMENT__ID__KEY>
						<PMDUYB1__SEGMENT__LEVEL__CODE>U</PMDUYB1__SEGMENT__LEVEL__CODE>
						<PMDUYB1__SEGMENT__PART__CODE>Y</PMDUYB1__SEGMENT__PART__CODE>
						<PMDUYB1__SUB__PART__CODE>1</PMDUYB1__SUB__PART__CODE>
						<PMDUYB1__INSURANCE__LINE>BP</PMDUYB1__INSURANCE__LINE>
					</PMDUYB1__SEGMENT__ID__KEY>
					<PMDUYB1__LEVEL__KEY>
						<PMDUYB1__LOCATION__NUMBER__A>
							<PMDUYB1__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">4</xsl:with-param>
									<xsl:with-param name="Value" select="$LocId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDUYB1__LOCATION__NUMBER>
						</PMDUYB1__LOCATION__NUMBER__A>
						<PMDUYB1__SUB__LOCATION__NUMBER__A>
							<PMDUYB1__SUB__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="$SubLocId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDUYB1__SUB__LOCATION__NUMBER>
						</PMDUYB1__SUB__LOCATION__NUMBER__A>
						<PMDUYB1__RISK__UNIT__GROUP__KEY>
							<PMDUYB1__PMS__DEF__SUBJ__OF__INS>
								<PMDUYB1__SUBJ__OF__INS>
									<xsl:call-template name="ConvertSubjectOfInsToNumeric">
										<xsl:with-param name="Value" select="CoverageCd"/>
									</xsl:call-template>
								</PMDUYB1__SUBJ__OF__INS>
							</PMDUYB1__PMS__DEF__SUBJ__OF__INS>
							<PMDUYB1__SEQUENCE__RISK__UNIT>
								<xsl:value-of select="string('   ')"/>
							</PMDUYB1__SEQUENCE__RISK__UNIT>
						</PMDUYB1__RISK__UNIT__GROUP__KEY>
						<PMDUYB1__RISK__UNIT>
							<xsl:value-of select="string('      ')"/>
						</PMDUYB1__RISK__UNIT>
					</PMDUYB1__LEVEL__KEY>
					<PMDUYB1__ITEM__EFFECTIVE__DATE>
						<PMDUYB1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDUYB1__YEAR__ITEM__EFFECTIVE>
						<PMDUYB1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDUYB1__MONTH__ITEM__EFFECTIVE>
						<PMDUYB1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDUYB1__DAY__ITEM__EFFECTIVE>
					</PMDUYB1__ITEM__EFFECTIVE__DATE>
					<PMDUYB1__VARIABLE__KEY>
						<fill_0>
							<xsl:value-of select="string('      ')"/>
						</fill_0>
					</PMDUYB1__VARIABLE__KEY>
					<PMDUYB1__PROCESS__DATE>
						<PMDUYB1__YEAR__PROCESS>
							<xsl:value-of select="string('0000')"/>
						</PMDUYB1__YEAR__PROCESS>
						<PMDUYB1__MONTH__PROCESS>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__MONTH__PROCESS>
						<PMDUYB1__DAY__PROCESS>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__DAY__PROCESS>
					</PMDUYB1__PROCESS__DATE>
				</PMDUYB1__SEGMENT__KEY>
				<PMDUYB1__SEGMENT__DATA>
					<PMDUYB1__TEXAS__IND>
						<xsl:value-of select="string('  ')"/>
					</PMDUYB1__TEXAS__IND>
					<PMDUYB1__RATE__BOOK__ID>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RATE__BOOK__ID>
					<PMDUYB1__EXPIRATION__DATE>
						<PMDUYB1__EXP__DATE__YR__MO>
							<PMDUYB1__EXP__YR>
								<xsl:value-of select="string('0000')"/>
							</PMDUYB1__EXP__YR>
							<PMDUYB1__EXP__MO>
								<xsl:value-of select="string('00')"/>
							</PMDUYB1__EXP__MO>
						</PMDUYB1__EXP__DATE__YR__MO>
						<PMDUYB1__EXP__DA>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__EXP__DA>
					</PMDUYB1__EXPIRATION__DATE>
					<PMDUYB1__COV__ENDORE>
						<xsl:call-template name="ConvertCoverageEndorse">
							<xsl:with-param name="Value" select="CoverageCd"/>
						</xsl:call-template>
					</PMDUYB1__COV__ENDORE>
					<PMDUYB1__COMMON__DATA>
						<PMDUYB1__AF__MG__IND>
							<xsl:value-of select="string('X')"/>
						</PMDUYB1__AF__MG__IND>
						<PMDUYB1__AF__FCTR__A>
							<PMDUYB1__AF__FCTR>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__AF__FCTR>
						</PMDUYB1__AF__FCTR__A>
						<PMDUYB1__AF__PREM__A>
							<PMDUYB1__AF__PREM>
								<xsl:value-of select="string('000000000')"/>
							</PMDUYB1__AF__PREM>
						</PMDUYB1__AF__PREM__A>
						<PMDUYB1__BASE__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__BASE__RATE__MG__IND>
						<PMDUYB1__BASE__RATE__A>
							<PMDUYB1__BASE__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__BASE__RATE>
						</PMDUYB1__BASE__RATE__A>
						<PMDUYB1__STATE__MULT__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__STATE__MULT__MG__IND>
						<PMDUYB1__STATE__MULT__A>
							<PMDUYB1__STATE__MULT>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__STATE__MULT>
						</PMDUYB1__STATE__MULT__A>
						<PMDUYB1__DED__FCTR__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__DED__FCTR__MG__IND>
						<PMDUYB1__DED__FCTR__A>
							<PMDUYB1__DED__FCTR>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__DED__FCTR>
						</PMDUYB1__DED__FCTR__A>
						<PMDUYB1__LIAB__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__LIAB__RATE__MG__IND>
						<PMDUYB1__LIAB__RATE__A>
							<PMDUYB1__LIAB__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__LIAB__RATE>
						</PMDUYB1__LIAB__RATE__A>
						<PMDUYB1__BASE__PREM__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__BASE__PREM__MG__IND>
						<PMDUYB1__BASE__PREM__A>
							<PMDUYB1__BASE__PREM>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__BASE__PREM>
						</PMDUYB1__BASE__PREM__A>
						<PMDUYB1__MASS__TEN__RELOC__RATE__A>
							<PMDUYB1__MASS__TEN__RELOC__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__MASS__TEN__RELOC__RATE>
						</PMDUYB1__MASS__TEN__RELOC__RATE__A>
					</PMDUYB1__COMMON__DATA>
					<PMDUYB1__BLDG__DATA>
						<PMDUYB1__SPL__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__SPL__RATE__MG__IND>
						<PMDUYB1__SPL__RATE__A>
							<PMDUYB1__SPL__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__SPL__RATE>
						</PMDUYB1__SPL__RATE__A>
						<PMDUYB1__FIRE__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__FIRE__RATE__MG__IND>
						<PMDUYB1__FIRE__RATE__A>
							<PMDUYB1__FIRE__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__FIRE__RATE>
						</PMDUYB1__FIRE__RATE__A>
						<PMDUYB1__ACV__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__ACV__RATE__MG__IND>
						<PMDUYB1__ACV__RATE__A>
							<PMDUYB1__ACV__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__ACV__RATE>
						</PMDUYB1__ACV__RATE__A>
					</PMDUYB1__BLDG__DATA>
					<PMDUYB1__PROP__DATA>
						<PMDUYB1__SPL__ADD__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__SPL__ADD__MG__IND>
						<PMDUYB1__SPL__ADD__A>
							<PMDUYB1__SPL__ADD>
								<xsl:value-of select="string('00000')"/>
							</PMDUYB1__SPL__ADD>
						</PMDUYB1__SPL__ADD__A>
					</PMDUYB1__PROP__DATA>
					<PMDUYB1__EMPL__DATA>
						<PMDUYB1__EMPL__PREM1__A>
							<PMDUYB1__EMPL__PREM1>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__EMPL__PREM1>
						</PMDUYB1__EMPL__PREM1__A>
						<PMDUYB1__EMPL__PREM2__A>
							<PMDUYB1__EMPL__PREM2>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__EMPL__PREM2>
						</PMDUYB1__EMPL__PREM2__A>
					</PMDUYB1__EMPL__DATA>
					<PMDUYB1__RMF__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RMF__RATE__MG__IND>
					<PMDUYB1__RMF__RATE__A>
						<PMDUYB1__RMF__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__RMF__RATE>
					</PMDUYB1__RMF__RATE__A>
					<PMDUYB1__CO__DEV__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__CO__DEV__RATE__MG__IND>
					<PMDUYB1__CO__DEV__RATE__A>
						<PMDUYB1__CO__DEV__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__CO__DEV__RATE>
					</PMDUYB1__CO__DEV__RATE__A>
					<PMDUYB1__MOD__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__MOD__RATE__MG__IND>
					<PMDUYB1__MOD__RATE__A>
						<PMDUYB1__MOD__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__MOD__RATE>
					</PMDUYB1__MOD__RATE__A>
					<PMDUYB1__FLATE__RATE__A>
						<PMDUYB1__FLAT__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__FLAT__RATE>
					</PMDUYB1__FLATE__RATE__A>
					<PMDUYB1__FINAL__PREM__MG__IND>M</PMDUYB1__FINAL__PREM__MG__IND>
					<PMDUYB1__FINAL__PREM__A>
						<PMDUYB1__FINAL__PREM>
							<xsl:value-of select="CurrentTermAmt/Amt"/>
						</PMDUYB1__FINAL__PREM>
					</PMDUYB1__FINAL__PREM__A>
					<PMDUYB1__RDF__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RDF__MG__IND>
					<PMDUYB1__RDF__A>
						<PMDUYB1__RDF>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__RDF>
					</PMDUYB1__RDF__A>
					<PMDUYB1__TERR__MULT__A>
						<PMDUYB1__TERR__MULT>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__TERR__MULT>
					</PMDUYB1__TERR__MULT__A>
					<PMDUYB1__PREM__FCTR__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__PREM__FCTR__MG__IND>
					<PMDUYB1__CALC__PREM__FCTR__A>
						<PMDUYB1__CALC__PREM__FCTR>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__CALC__PREM__FCTR>
					</PMDUYB1__CALC__PREM__FCTR__A>
					<PMDUYB1__LESS__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__LESS__RATE__MG__IND>
					<PMDUYB1__LESS__RATE__A>
						<PMDUYB1__LESS__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__LESS__RATE>
					</PMDUYB1__LESS__RATE__A>
					<PMDUYB1__FIRE__PREM__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__FIRE__PREM__MG__IND>
					<PMDUYB1__FIRE__PREM__A>
						<PMDUYB1__FIRE__PREM>
							<xsl:value-of select="string('00000000')"/>
						</PMDUYB1__FIRE__PREM>
					</PMDUYB1__FIRE__PREM__A>
					<PMDUYB1__DEC__CHANGE__FLAG>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__DEC__CHANGE__FLAG>
					<PMDUYB1__WH__FACTOR>
						<xsl:value-of select="string('000000')"/>
					</PMDUYB1__WH__FACTOR>
					<PMDUYB1__SUBTOTAL__PREM>
						<xsl:value-of select="string('00000000')"/>
					</PMDUYB1__SUBTOTAL__PREM>
					<PMDUYB1__PROP__DATA__EXP>
						<PMDUYB1__SPL__ADD__EXP__A>
							<PMDUYB1__SPL__ADD__EXP>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__SPL__ADD__EXP>
						</PMDUYB1__SPL__ADD__EXP__A>
					</PMDUYB1__PROP__DATA__EXP>
					<PMDUYB1__SPRINKLER__FACTOR>
						<xsl:value-of select="string('000000')"/>
					</PMDUYB1__SPRINKLER__FACTOR>
					<PMDUYB1__ALL__FLRS__FACT>
						<xsl:value-of select="string('0000')"/>
					</PMDUYB1__ALL__FLRS__FACT>
					<PMDUYB1__CML91248__DATE__SW>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__CML91248__DATE__SW>
					<PMDUYB1__PMS__FUTURE__USE>
						<xsl:value-of select="string('                                             ')"/>
					</PMDUYB1__PMS__FUTURE__USE>
					<PMDUYB1__CUST__FUTURE__USE>
						<xsl:value-of select="string('                               ')"/>
					</PMDUYB1__CUST__FUTURE__USE>
					<PMDUYB1__YR2000__CUST__USE>
						<xsl:value-of select="string('                                                                                                    ')"/>
					</PMDUYB1__YR2000__CUST__USE>
				</PMDUYB1__SEGMENT__DATA>
			</PMDUYB1__RATE__OVERRIDE__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>

	<xsl:template name="S2CovPremTemplate02" match="/">
		<!-- For Building & Property Coverages -->
		<xsl:param name="RefLocId">
		</xsl:param>
		<xsl:param name="RefSubLocId">
		</xsl:param>
		<xsl:param name="LocId">
		</xsl:param>
		<xsl:param name="SubLocId">
		</xsl:param>

		<BUS__OBJ__RECORD>
			<PMDUYB1__RATE__OVERRIDE__SEG>
				<PMDUYB1__SEGMENT__KEY>
					<PMDUYB1__REC__LLBB/>
					<PMDUYB1__SEGMENT__ID>43</PMDUYB1__SEGMENT__ID>
					<PMDUYB1__SEGMENT__STATUS>A</PMDUYB1__SEGMENT__STATUS>
					<PMDUYB1__TRANSACTION__DATE>
						<PMDUYB1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,1,4)"/>
						</PMDUYB1__YEAR__TRANSACTION>
						<PMDUYB1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,6,2)"/>
						</PMDUYB1__MONTH__TRANSACTION>
						<PMDUYB1__DAY__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,9,2)"/>
						</PMDUYB1__DAY__TRANSACTION>
					</PMDUYB1__TRANSACTION__DATE>
					<PMDUYB1__SEGMENT__ID__KEY>
						<PMDUYB1__SEGMENT__LEVEL__CODE>U</PMDUYB1__SEGMENT__LEVEL__CODE>
						<PMDUYB1__SEGMENT__PART__CODE>Y</PMDUYB1__SEGMENT__PART__CODE>
						<PMDUYB1__SUB__PART__CODE>1</PMDUYB1__SUB__PART__CODE>
						<PMDUYB1__INSURANCE__LINE>BP</PMDUYB1__INSURANCE__LINE>
					</PMDUYB1__SEGMENT__ID__KEY>
					<PMDUYB1__LEVEL__KEY>
						<PMDUYB1__LOCATION__NUMBER__A>
							<PMDUYB1__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">4</xsl:with-param>
									<xsl:with-param name="Value" select="$LocId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDUYB1__LOCATION__NUMBER>
						</PMDUYB1__LOCATION__NUMBER__A>
						<PMDUYB1__SUB__LOCATION__NUMBER__A>
							<PMDUYB1__SUB__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="$SubLocId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDUYB1__SUB__LOCATION__NUMBER>
						</PMDUYB1__SUB__LOCATION__NUMBER__A>
						<PMDUYB1__RISK__UNIT__GROUP__KEY>
							<PMDUYB1__PMS__DEF__SUBJ__OF__INS>
								<PMDUYB1__SUBJ__OF__INS>
									<xsl:call-template name="ConvertSubjectOfInsToNumeric">
										<xsl:with-param name="Value" select="SubjectInsuranceCd"/>
									</xsl:call-template>
								</PMDUYB1__SUBJ__OF__INS>
							</PMDUYB1__PMS__DEF__SUBJ__OF__INS>
							<PMDUYB1__SEQUENCE__RISK__UNIT>
								<xsl:value-of select="string('   ')"/>
							</PMDUYB1__SEQUENCE__RISK__UNIT>
						</PMDUYB1__RISK__UNIT__GROUP__KEY>
						<PMDUYB1__RISK__UNIT>
							<xsl:value-of select="string('      ')"/>
						</PMDUYB1__RISK__UNIT>
					</PMDUYB1__LEVEL__KEY>
					<PMDUYB1__ITEM__EFFECTIVE__DATE>
						<PMDUYB1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDUYB1__YEAR__ITEM__EFFECTIVE>
						<PMDUYB1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDUYB1__MONTH__ITEM__EFFECTIVE>
						<PMDUYB1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDUYB1__DAY__ITEM__EFFECTIVE>
					</PMDUYB1__ITEM__EFFECTIVE__DATE>
					<PMDUYB1__VARIABLE__KEY>
						<fill_0>
							<xsl:value-of select="string('      ')"/>
						</fill_0>
					</PMDUYB1__VARIABLE__KEY>
					<PMDUYB1__PROCESS__DATE>
						<PMDUYB1__YEAR__PROCESS>
							<xsl:value-of select="string('0000')"/>
						</PMDUYB1__YEAR__PROCESS>
						<PMDUYB1__MONTH__PROCESS>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__MONTH__PROCESS>
						<PMDUYB1__DAY__PROCESS>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__DAY__PROCESS>
					</PMDUYB1__PROCESS__DATE>
				</PMDUYB1__SEGMENT__KEY>
				<PMDUYB1__SEGMENT__DATA>
					<PMDUYB1__TEXAS__IND>
						<xsl:value-of select="string('  ')"/>
					</PMDUYB1__TEXAS__IND>
					<PMDUYB1__RATE__BOOK__ID>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RATE__BOOK__ID>
					<PMDUYB1__EXPIRATION__DATE>
						<PMDUYB1__EXP__DATE__YR__MO>
							<PMDUYB1__EXP__YR>
								<xsl:value-of select="string('0000')"/>
							</PMDUYB1__EXP__YR>
							<PMDUYB1__EXP__MO>
								<xsl:value-of select="string('00')"/>
							</PMDUYB1__EXP__MO>
						</PMDUYB1__EXP__DATE__YR__MO>
						<PMDUYB1__EXP__DA>
							<xsl:value-of select="string('00')"/>
						</PMDUYB1__EXP__DA>
					</PMDUYB1__EXPIRATION__DATE>
					<PMDUYB1__COV__ENDORE>
						<xsl:call-template name="ConvertCoverageEndorse">
							<xsl:with-param name="Value" select="SubjectInsuranceCd"/>
						</xsl:call-template>
					</PMDUYB1__COV__ENDORE>
					<PMDUYB1__COMMON__DATA>
						<PMDUYB1__AF__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__AF__MG__IND>
						<PMDUYB1__AF__FCTR__A>
							<PMDUYB1__AF__FCTR>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__AF__FCTR>
						</PMDUYB1__AF__FCTR__A>
						<PMDUYB1__AF__PREM__A>
							<PMDUYB1__AF__PREM>
								<xsl:value-of select="string('000000000')"/>
							</PMDUYB1__AF__PREM>
						</PMDUYB1__AF__PREM__A>
						<PMDUYB1__BASE__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__BASE__RATE__MG__IND>
						<PMDUYB1__BASE__RATE__A>
							<PMDUYB1__BASE__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__BASE__RATE>
						</PMDUYB1__BASE__RATE__A>
						<PMDUYB1__STATE__MULT__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__STATE__MULT__MG__IND>
						<PMDUYB1__STATE__MULT__A>
							<PMDUYB1__STATE__MULT>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__STATE__MULT>
						</PMDUYB1__STATE__MULT__A>
						<PMDUYB1__DED__FCTR__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__DED__FCTR__MG__IND>
						<PMDUYB1__DED__FCTR__A>
							<PMDUYB1__DED__FCTR>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__DED__FCTR>
						</PMDUYB1__DED__FCTR__A>
						<PMDUYB1__LIAB__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__LIAB__RATE__MG__IND>
						<PMDUYB1__LIAB__RATE__A>
							<PMDUYB1__LIAB__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__LIAB__RATE>
						</PMDUYB1__LIAB__RATE__A>
						<PMDUYB1__BASE__PREM__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__BASE__PREM__MG__IND>
						<PMDUYB1__BASE__PREM__A>
							<PMDUYB1__BASE__PREM>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__BASE__PREM>
						</PMDUYB1__BASE__PREM__A>
						<PMDUYB1__MASS__TEN__RELOC__RATE__A>
							<PMDUYB1__MASS__TEN__RELOC__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__MASS__TEN__RELOC__RATE>
						</PMDUYB1__MASS__TEN__RELOC__RATE__A>
					</PMDUYB1__COMMON__DATA>
					<PMDUYB1__BLDG__DATA>
						<PMDUYB1__SPL__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__SPL__RATE__MG__IND>
						<PMDUYB1__SPL__RATE__A>
							<PMDUYB1__SPL__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__SPL__RATE>
						</PMDUYB1__SPL__RATE__A>
						<PMDUYB1__FIRE__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__FIRE__RATE__MG__IND>
						<PMDUYB1__FIRE__RATE__A>
							<PMDUYB1__FIRE__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__FIRE__RATE>
						</PMDUYB1__FIRE__RATE__A>
						<PMDUYB1__ACV__RATE__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__ACV__RATE__MG__IND>
						<PMDUYB1__ACV__RATE__A>
							<PMDUYB1__ACV__RATE>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__ACV__RATE>
						</PMDUYB1__ACV__RATE__A>
					</PMDUYB1__BLDG__DATA>
					<PMDUYB1__PROP__DATA>
						<PMDUYB1__SPL__ADD__MG__IND>
							<xsl:value-of select="string(' ')"/>
						</PMDUYB1__SPL__ADD__MG__IND>
						<PMDUYB1__SPL__ADD__A>
							<PMDUYB1__SPL__ADD>
								<xsl:value-of select="string('00000')"/>
							</PMDUYB1__SPL__ADD>
						</PMDUYB1__SPL__ADD__A>
					</PMDUYB1__PROP__DATA>
					<PMDUYB1__EMPL__DATA>
						<PMDUYB1__EMPL__PREM1__A>
							<PMDUYB1__EMPL__PREM1>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__EMPL__PREM1>
						</PMDUYB1__EMPL__PREM1__A>
						<PMDUYB1__EMPL__PREM2__A>
							<PMDUYB1__EMPL__PREM2>
								<xsl:value-of select="string('00000000')"/>
							</PMDUYB1__EMPL__PREM2>
						</PMDUYB1__EMPL__PREM2__A>
					</PMDUYB1__EMPL__DATA>
					<PMDUYB1__RMF__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RMF__RATE__MG__IND>
					<PMDUYB1__RMF__RATE__A>
						<PMDUYB1__RMF__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__RMF__RATE>
					</PMDUYB1__RMF__RATE__A>
					<PMDUYB1__CO__DEV__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__CO__DEV__RATE__MG__IND>
					<PMDUYB1__CO__DEV__RATE__A>
						<PMDUYB1__CO__DEV__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__CO__DEV__RATE>
					</PMDUYB1__CO__DEV__RATE__A>
					<PMDUYB1__MOD__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__MOD__RATE__MG__IND>
					<PMDUYB1__MOD__RATE__A>
						<PMDUYB1__MOD__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__MOD__RATE>
					</PMDUYB1__MOD__RATE__A>
					<PMDUYB1__FLATE__RATE__A>
						<PMDUYB1__FLAT__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__FLAT__RATE>
					</PMDUYB1__FLATE__RATE__A>
					<PMDUYB1__FINAL__PREM__MG__IND>M</PMDUYB1__FINAL__PREM__MG__IND>
					<PMDUYB1__FINAL__PREM__A>
						<PMDUYB1__FINAL__PREM>
							<xsl:value-of select="CommlCoverage[normalize-space(CoverageCd)='']/CurrentTermAmt/Amt"/>
						</PMDUYB1__FINAL__PREM>
					</PMDUYB1__FINAL__PREM__A>
					<PMDUYB1__RDF__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__RDF__MG__IND>
					<PMDUYB1__RDF__A>
						<PMDUYB1__RDF>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__RDF>
					</PMDUYB1__RDF__A>
					<PMDUYB1__TERR__MULT__A>
						<PMDUYB1__TERR__MULT>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__TERR__MULT>
					</PMDUYB1__TERR__MULT__A>
					<PMDUYB1__PREM__FCTR__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__PREM__FCTR__MG__IND>
					<PMDUYB1__CALC__PREM__FCTR__A>
						<PMDUYB1__CALC__PREM__FCTR>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__CALC__PREM__FCTR>
					</PMDUYB1__CALC__PREM__FCTR__A>
					<PMDUYB1__LESS__RATE__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__LESS__RATE__MG__IND>
					<PMDUYB1__LESS__RATE__A>
						<PMDUYB1__LESS__RATE>
							<xsl:value-of select="string('000000')"/>
						</PMDUYB1__LESS__RATE>
					</PMDUYB1__LESS__RATE__A>
					<PMDUYB1__FIRE__PREM__MG__IND>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__FIRE__PREM__MG__IND>
					<PMDUYB1__FIRE__PREM__A>
						<PMDUYB1__FIRE__PREM>
							<xsl:value-of select="string('00000000')"/>
						</PMDUYB1__FIRE__PREM>
					</PMDUYB1__FIRE__PREM__A>
					<PMDUYB1__DEC__CHANGE__FLAG>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__DEC__CHANGE__FLAG>
					<PMDUYB1__WH__FACTOR>
						<xsl:value-of select="string('000000')"/>
					</PMDUYB1__WH__FACTOR>
					<PMDUYB1__SUBTOTAL__PREM>
						<xsl:value-of select="string('00000000')"/>
					</PMDUYB1__SUBTOTAL__PREM>
					<PMDUYB1__PROP__DATA__EXP>
						<PMDUYB1__SPL__ADD__EXP__A>
							<PMDUYB1__SPL__ADD__EXP>
								<xsl:value-of select="string('000000')"/>
							</PMDUYB1__SPL__ADD__EXP>
						</PMDUYB1__SPL__ADD__EXP__A>
					</PMDUYB1__PROP__DATA__EXP>
					<PMDUYB1__SPRINKLER__FACTOR>
						<xsl:value-of select="string('000000')"/>
					</PMDUYB1__SPRINKLER__FACTOR>
					<PMDUYB1__ALL__FLRS__FACT>
						<xsl:value-of select="string('0000')"/>
					</PMDUYB1__ALL__FLRS__FACT>
					<PMDUYB1__CML91248__DATE__SW>
						<xsl:value-of select="string(' ')"/>
					</PMDUYB1__CML91248__DATE__SW>
					<PMDUYB1__PMS__FUTURE__USE>
						<xsl:value-of select="string('                                             ')"/>
					</PMDUYB1__PMS__FUTURE__USE>
					<PMDUYB1__CUST__FUTURE__USE>
						<xsl:value-of select="string('                               ')"/>
					</PMDUYB1__CUST__FUTURE__USE>
					<PMDUYB1__YR2000__CUST__USE>
						<xsl:value-of select="string('                                                                                                    ')"/>
					</PMDUYB1__YR2000__CUST__USE>
				</PMDUYB1__SEGMENT__DATA>
			</PMDUYB1__RATE__OVERRIDE__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="BORQ_S2_ACORD.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->