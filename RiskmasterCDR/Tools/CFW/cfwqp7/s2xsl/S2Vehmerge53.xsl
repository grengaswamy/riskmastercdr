<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" indent="yes"/>
<xsl:template name="S2Vehmerge53Template">

<xsl:for-each select="//PersAutoLineBusiness/PersVeh">	
	<xsl:variable name="VehicleNumber" select="substring(@id,2,string-length(@id)-1)"/>
	<xsl:value-of select="$VehicleNumber"/>
	<xsl:call-template name="S2Vehdes53Template">  
		<xsl:with-param name="VehicleNumber" select="$VehicleNumber"/>
	</xsl:call-template>

	<xsl:call-template name="S2Vehcov53Template">   
		<xsl:with-param name="VehicleNumber" select="$VehicleNumber"/>
	</xsl:call-template>
</xsl:for-each>

<!-- Issue 80442 Begin -->
<xsl:for-each select="//CommlAutoLineBusiness/CommlRateState/CommlVeh">	
	<xsl:variable name="VehicleNumber" select="substring(@id,2,string-length(@id)-1) + 9"/>
	<xsl:value-of select="$VehicleNumber"/>
	<xsl:call-template name="S2Vehdes53Template">  
		<xsl:with-param name="VehicleNumber" select="$VehicleNumber"/>
	</xsl:call-template>

	<xsl:call-template name="S2Vehcov53Template">   
		<xsl:with-param name="VehicleNumber" select="$VehicleNumber"/>
	</xsl:call-template>
</xsl:for-each>

<xsl:if test="$LOB = 'ACV' or $LOB = 'AFV'">
	<xsl:call-template name="S2CACommonCoverages">
		<xsl:with-param name="VehCount" select="$VehCount"/>
	</xsl:call-template>
</xsl:if>
<!-- Issue 80442 End -->
</xsl:template>
</xsl:stylesheet>
