<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->

	<xsl:template name="SeriesIIErrorsRs">
		<xsl:for-each select="/*/POLICY__INFORMATION__SEG/PIF__ISOL__STATUS">
			<xsl:choose>
				<xsl:when test="count(//ERROR__DESC__SEG) &gt; 0">
					<!--<xsl:when test="count(//ERROR__DESC__SEG) &lt; 0">-->
					<MsgStatus>
						<MsgStatusCd>1</MsgStatusCd>
						<MsgErrorCd>
						</MsgErrorCd>
						<MsgStatusDesc>RATING ERROR(s).</MsgStatusDesc>
						<xsl:for-each select="/*/ERROR__DESC__SEG">
							<ExtendedStatus>
								<ExtendedStatusCd>
									<xsl:value-of select="./ERR__SEGMENT__DATA/ERR__ERROR__CODE"/>
								</ExtendedStatusCd>
								<ExtendedStatusDesc>
									<xsl:value-of select="concat(./ERR__SEGMENT__DATA/ERR__ERROR__DESC,./ERR__SEGMENT__DATA/ERR__XML__ERROR__DESC)"/>
								</ExtendedStatusDesc>
							</ExtendedStatus>
						</xsl:for-each>
					</MsgStatus>
				</xsl:when>
				<xsl:otherwise>
					<MsgStatus>
						<MsgStatusCd>
							<xsl:text>INFO</xsl:text>
						</MsgStatusCd>
						<MsgErrorCd>0</MsgErrorCd>
						<MsgStatusDesc>Success</MsgStatusDesc>
					</MsgStatus>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->