<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template name="CreateWorkCompRateState">


                <xsl:variable name="AnniversaryDt" select="/*/PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__ANN__RATE__DTE__MMDD"/> 

                <xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC">
                        <xsl:variable name="CurState" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE"/>
                        <xsl:variable name="NumEmployee" select="PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__DIST__OF__COL__DATA/PMDI4W1__A__DC__NUMBER__OF__EMPL"/>
			<xsl:variable name="SegStatus" select="PMDI4W1__STATE__RATING__REC/PMDI4W1__SEGMENT__KEY/PMDI4W1__SEGMENT__STATUS"/>
			<xsl:variable name="BureauId" select="PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__INTRASTATE__ID__NUM"/>

                <xsl:if test="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE ='0000'">
		<WorkCompRateState>
			<StateProvCd>
				<xsl:call-template name="BuildStateProvCd">
					<xsl:with-param name="StateCd" select="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE "/>
				</xsl:call-template>
			</StateProvCd>
			<AnniversaryRatingDt>
				<xsl:value-of select="$AnniversaryDt"/>
			</AnniversaryRatingDt>
			<NCCIIDNumber><xsl:value-of select="$BureauId"/></NCCIIDNumber>   <!-- 55614 -->
			<ParticipatingPlanDescCd>
				<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__DIVIDEND__PLAN__IND"/>
			</ParticipatingPlanDescCd>

			<xsl:for-each select="/*/PMDI4W1__STATE__RATING__REC">
			   <xsl:if test="PMDI4W1__SEGMENT__KEY/PMDI4W1__SEGMENT__STATUS != 'D'">
				<xsl:choose>
					<xsl:when test="PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__WC__RATING__STATE=$CurState and PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE !='0000'">
						<CreditOrSurcharge>
							<CreditSurchargeCd>
								<xsl:value-of select="concat(PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__CODE, PMDI4W1__SEGMENT__KEY/PMDI4W1__LEVEL__KEY/PMDI4W1__RISK__UNIT__GROUP/PMDI4W1__CLASS__ORDER__SEQ)"/>
							</CreditSurchargeCd>
                    <!-- 55614 Begin -->
							<!--<NumericValue>
								<FormatInteger>
									<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__GENERATED__SEG__IND"/>
								</FormatInteger>
								<FormatModFactor>
									<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE"/>
								</FormatModFactor>
									<FormatCurrencyAmt>
										<xsl:choose>
											<xsl:when test="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE &gt; '0'">
												<Amt/>
											</xsl:when>
											<xsl:otherwise>
												<Amt>
													<xsl:value-of select="concat(PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM,'.00')"/>
												</Amt>
											</xsl:otherwise>
										</xsl:choose>
									</FormatCurrencyAmt>
							</NumericValue>
							<com.csc_ModifierDesc>
								<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__DESC"/>
							</com.csc_ModifierDesc>-->
							<NumericValue>
								<FormatModFactor>
									<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE"/>
								</FormatModFactor>
							</NumericValue>
							<NumericValue>
									<FormatCurrencyAmt>
										<xsl:choose>
											<xsl:when test="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__RATE &gt; '0'">
												<Amt/>
											</xsl:when>
											<xsl:otherwise>
												<Amt>
													<xsl:value-of select="concat(PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__PREMIUM,'.00')"/>
												</Amt>
											</xsl:otherwise>
										</xsl:choose>
									</FormatCurrencyAmt>
							</NumericValue>
							<NumericValue>
								<FormatInteger>
									<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__GENERATED__SEG__IND"/>
								</FormatInteger>
							</NumericValue>
							<SecondaryCd>
								<xsl:value-of select="PMDI4W1__SEGMENT__DATA/PMDI4W1__RATING__MODIFIER__DATA/PMDI4W1__MODIFIER__DESC"/>
							</SecondaryCd>
							<!-- 55614 End -->
						</CreditOrSurcharge>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			   </xsl:if>
			</xsl:for-each>

         <!-- 101660 Begin -->
			<!--<xsl:for-each select="//PMDU4W1__CLASS__RATING__SEG">
	                        <xsl:variable name="UXJJ-LOC" select="PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__LOCATION__NUMBER"/>  
			<xsl:choose>
                            <xsl:when test="PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__WC__RATING__STATE=$CurState">-->
           <xsl:for-each select="/*/PMD4J__NAME__ADDR__SEG[PMD4J__SEGMENT__DATA/PMD4J__LOCATION__STATE=$CurState]">
               <xsl:variable name="UXJJ-LOC" select="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER"/>            
          <!-- 101660 End -->
			<WorkCompLocInfo>
				<xsl:attribute name="LocationRef">
					<!--<xsl:value-of select="concat('L',substring(PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__LOCATION__NUMBER,4,1))"/>-->
			   <!-- 101660 Begin -->		
					<!--<xsl:value-of select="concat('L',PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__LOCATION__NUMBER__N)"/>-->
					<!-- 55614 Begin -->
					<!--<xsl:value-of select="concat('L',PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER__N)"/>-->
					<xsl:value-of select="concat($KEY, '-l',number(PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER))"/>
			   <!-- 55614 End -->
			   <!-- 101660 End -->		
				</xsl:attribute>
				<NumEmployees>
					<xsl:value-of select="$NumEmployee"/>
				</NumEmployees>
             <xsl:for-each select="/*/PMDU4W1__CLASS__RATING__SEG[PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__LOCATION__NUMBER=$UXJJ-LOC]">         <!-- 101660 -->
							<WorkCompRateClass>
								<ActualRemunerationAmt>
									<Amt>
										<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__PREMIUM__BASIS__AMT"/>
									</Amt>
								</ActualRemunerationAmt>
                                                                <NumEmployeesFullTime/>
                                                                <NumEmployeesPartTime/>
								<Rate/>
								<PremiumBasisCd>
									<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__PREMIUM__BASIS__TYP"/>
								</PremiumBasisCd>
								<RatingClassificationCd>
									<xsl:value-of select="concat(PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__RISK__UNIT/PMDU4W1__CLASS__CODE, PMDU4W1__SEGMENT__KEY/PMDU4W1__LEVEL__KEY/PMDU4W1__RISK__UNIT/PMDU4W1__CLASS__CODE__SEQ)"/>
								</RatingClassificationCd>
								<Exposure>
									<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__PREMIUM__BASIS__AMT"/>
								</Exposure>
								<RatingClassificationDesc>
									<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__CLASS__CODE__DESCRIPTION"/>
								</RatingClassificationDesc>
								<RatingClassificationDescCd>
									<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__CLASS__CODE__SUFFIX/PMDU4W1__CLASS__CODE__SUFFIX__1"/>
								</RatingClassificationDescCd>
								<!-- RatingClassificationDescCd>
									<xsl:value-of select="PMDU4W1__SEGMENT__DATA/PMDU4W1__CLASS__CODE__SUFFIX"/>
								</RatingClassificationDescCd -->
								<!-- 55614 Begin -->
								<!--<ItemIdInfo> -->
								<com.csc_ItemIdInfo>
								<!-- 55614 End -->
									<OtherIdentifier>
										<OtherIdTypeCd>HostID</OtherIdTypeCd>
										<OtherId>
											<xsl:number value="position()"/>
										</OtherId>
									</OtherIdentifier>
									<!-- 55614 Begin -->
								<!-- </ItemIdInfo> -->
								</com.csc_ItemIdInfo>
								<!-- 55614 End -->
							</WorkCompRateClass>
							<!-- 101660 Begin -->
			<!--</WorkCompLocInfo>
                        </xsl:when>
			<xsl:otherwise/>
			</xsl:choose>
			</xsl:for-each>-->
		        </xsl:for-each>         
			</WorkCompLocInfo>
		</xsl:for-each>
			 <!-- 101660 End -->
                        <!-- com.csc_RatingBureauId -->
         <!-- 55614 Begin -->                
			<!--<NCCIIDNumber>
				<xsl:value-of select="$BureauId"/>
			</NCCIIDNumber>-->
			<!-- /com.csc_RatingBureauId -->
			<!--<com.csc_SuperiorLossInd>
				<xsl:value-of select="substring(PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__NON__PREM__INFO,7,1)"/>
			</com.csc_SuperiorLossInd>
			<com.csc_DCBenefitsInd>
				<xsl:value-of select="substring(PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__NON__PREM__INFO,14,1)"/>
			</com.csc_DCBenefitsInd>-->
			<com.csc_ItemIdInfo>
					<OtherIdentifier>
					   <OtherIdTypeCd>SuperiorLossInd</OtherIdTypeCd>
					   <OtherId><xsl:value-of select="substring(PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__NON__PREM__INFO,7,1)"/></OtherId>
					</OtherIdentifier>
					<OtherIdentifier>
					   <OtherIdTypeCd>DCBenefitsInd</OtherIdTypeCd>
					   <OtherId><xsl:value-of select="substring(PMDI4W1__SEGMENT__DATA/PMDI4W1__NON__STAT__DATA/PMDI4W1__NON__PREM__INFO,14,1)"/></OtherId>
					</OtherIdentifier>
		  </com.csc_ItemIdInfo>		  
		  <!-- 55614 End -->
		</WorkCompRateState>
                </xsl:if>
  </xsl:for-each>
	</xsl:template>
</xsl:stylesheet>