<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="Pinfo53Rq"> 
    <xsl:param name="ProcessingType"/> 	
	<!-- <xsl:template match="/"> -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__INFORMATION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
				<POLICY__INFORMATION__SEG>
					<PIF__REC__LLBB/>
					<PIF__ID>02</PIF__ID>
					<PIF__KEY>
							<PIF__SYMBOL>
		               					<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/CompanyProductCd"/>					
							</PIF__SYMBOL>
							<PIF__POLICY__NUMBER>
								<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/PolicyNumber"/>
							</PIF__POLICY__NUMBER>
							<PIF__MODULE>
								<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/PolicyVersion"/>
	        					</PIF__MODULE>
					</PIF__KEY>
						<PIF__MASTER__CO__NUMBER>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/NAICCd"/>
						</PIF__MASTER__CO__NUMBER>
						<PIF__LOCATION>
							<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/com.csc_CompanyPolicyProcessingId"/>
						</PIF__LOCATION>
					<PIF__AMEND__NUMBER/>
						
					<PIF__EFFECTIVE__DATE/>
					<PIF__EXPIRATION__DATE/>
					<PIF__INSTALLMENT__TERM/>
					<PIF__NUMBER__INSTALLMENTS/>
					<PIF__RISK__STATE__PROV/>
					<PIF__COMPANY__NUMBER/>
					<PIF__BRANCH/>
					<PIF__PROFIT__CENTER/>
					<PIF__AGENCY__NUMBER/>
					<PIF__ENTERED__DATE/>
					<PIF__TOTAL__AGEN__PREM/>
               				<PIF__LINE__BUSINESS/>
               				<PIF__ISSUE__CODE/>
					<PIF__COMPANY__LINE>6</PIF__COMPANY__LINE>
					<PIF__PAY__SERVICE__CODE/>
				        <PIF__MODE__CODE/>
				        <PIF__AUDIT__CODE>N</PIF__AUDIT__CODE>					
					<PIF__KIND__CODE>D</PIF__KIND__CODE>
					<PIF__VARIATION__CODE/>
							<PIF__SORT__NAME/>
						<PIF__PRODUCER__CODE/>
					<PIF__UNDERWRITING__CODE>
						<PIF__REVIEW__CODE>N</PIF__REVIEW__CODE>
						<PIF__MVR__REPORT__YEAR>N</PIF__MVR__REPORT__YEAR>
						<PIF__RISK__GRADE__GUIDE>5</PIF__RISK__GRADE__GUIDE>
						<PIF__RISK__GRADE__UNDWR>5</PIF__RISK__GRADE__UNDWR>
						<PIF__RENEWAL__CODE>1</PIF__RENEWAL__CODE>
					</PIF__UNDERWRITING__CODE>
					<PIF__REASON__AMENDED/>
					<PIF__RENEW__PAY__CODE/>
				        <PIF__RENEW__MODE__CODE/>
       				        <PIF__RENEW__POLICY__SYMBOL>
	               		<xsl:value-of select="string('   ')"/>					
					</PIF__RENEW__POLICY__SYMBOL>
					<PIF__RENEW__POLICY__NUMBER>
						<xsl:value-of select="string('       ')"/>
					</PIF__RENEW__POLICY__NUMBER>
					<PIF__ORIGINAL__INCEPT>
	               		<xsl:value-of select="string('      ')"/>					
					</PIF__ORIGINAL__INCEPT>
					<PIF__CUSTOMER__NUMBER/>
				        <PIF__SPECIAL__USE__A/>
					<PIF__SPECIAL__USE__B/>
	   					<PIF__ZIP__POSTAL__CODE>
<xsl:value-of select="ACORD/InsuranceSvcRq/com.csc_PolicySyncRq/ZipCode"/>
						</PIF__ZIP__POSTAL__CODE>
							<PIF__ADDRESS__LINE__1/>
							<PIF__ADDRESS__LINE__2/>
							<PIF__ADDRESS__LINE__3/>
							<PIF__ADDRESS__LINE__4/>
						<PIF__HISTORY__OPTION/>
					<PIF__FINAL__AUDIT__IND/>
					<PIF__SPECIAL__AUTO__CEDED/>
					<PIF__EXCESS__CLAIM__IND/>
					<PIF__PMS__TYPE__LEVEL>
						<xsl:value-of select="string('A')"/>
					</PIF__PMS__TYPE__LEVEL>
					<PIF__ANNIVERSARY__RERATE/>
					<PIF__NAME__PRINT__OPTION>
						<xsl:value-of select="string('0')"/>
					</PIF__NAME__PRINT__OPTION>
					<PIF__LEGAL__ENTITY/>
					<PIF__REINS__PROCESS__IND/>
					<PIF__POLICY__STATUS__ON__PIF/>
					<PIF__ACA__POLICY__CODE>
						<xsl:value-of select="string('N')"/>
					</PIF__ACA__POLICY__CODE>
					<PIF__ACA__DESTINATION/>
					<PIF__PC__ADDRESS__AREA/>
					<PIF__PC__SEQUENCE/>
					<PIF__UK__POSTAL__CODE/>
					<PIF__MOD__NUMBER/>
					<PIF__POLICY__START__TIME__A/>
					<PIF__INSURED__OCCUPATION__CODE/>
					<PIF__INSURED__OCCUPATION__CODE__X/>
					<PIF__USA__INDICATORS/>
					<PIF__DEC__CHANGE__SW/>
					<PIF__PMS__FUTURE__USE/>
					<PIF__ISOL__APP__ID/>
					<PIF__CUST__FUTURE__USE/>
					<PIF__YR2000__CUST__USE/>
				</POLICY__INFORMATION__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylesheet edited using Stylus Studio - (c)1998-2002 eXcelon Corp. -->