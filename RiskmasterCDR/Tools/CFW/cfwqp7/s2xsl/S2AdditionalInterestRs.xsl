<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:template name="CreateAdditionalInterestFrom12SEG">
		<!--Modified for Issue 35934
		<xsl:param name="PASS_LOB">Ignore</xsl:param>
		<xsl:param name="UseCode">Ignore</xsl:param>
		<xsl:param name="UnitNo">000</xsl:param>-->
		<xsl:param name="PASS_LOB"/>
		<xsl:param name="UseCode"/>
		<xsl:param name="UnitNo"/>
		<xsl:for-each select="//DESCRIPTION__FULL__SEG[DESCRIPTION__KEY/DESCRIPTION__USE/UNIT__NUMBER =$UnitNo]">
			<xsl:variable name="Code" select="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE"/>
			<xsl:variable name="Location" select="number(DESCRIPTION__KEY/DESCRIPTION__USE/USE__LOCATION)"/>
			<xsl:variable name="Zip" select="DESCRIPTION__INFORMATION/DESCRIPTION__ZIP__CODE"/>
			<xsl:variable name="Line1" select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1"/>
			<xsl:variable name="Line2" select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__2"/>
			<xsl:variable name="Line3" select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3"/>
			<xsl:variable name="Line4" select="DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4"/>
			<xsl:variable name="Loan" select="DESCRIPTION_LOAN_NUMBER"/>
			<xsl:variable name="Cty" select="substring-before(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,',')"/>
			<xsl:variable name="St" select="substring-after(DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4,',')"/>
			<xsl:variable name="ZPlus" select="DESC-ZIP-PLUS4"/>

			<!-- Added 10/07/03 to record the usecode for policy level info on 12 segs -->
			<!--Modified for Issue 35934 <xsl:if test="$UnitNo='000'">-->
			<xsl:if test="$UnitNo='0'">
				<AdditionalInterest>
					<AdditionalInterestInfo>
						<NatureInterestCd>
							<xsl:value-of select="$Code"/>
						</NatureInterestCd>
					</AdditionalInterestInfo>
				</AdditionalInterest>
			</xsl:if>
			<xsl:if test="$Code ='LP'">
				<xsl:call-template name="BuildInterest">
					<xsl:with-param name="UseCode" select="$Code"/>
					<xsl:with-param name="UseLocation" select="$Location"/>
					<xsl:with-param name="ZipCode" select="$Zip"/>
					<xsl:with-param name="DescLine1" select="$Line1"/>
					<xsl:with-param name="DescLine2" select="$Line2"/>
					<xsl:with-param name="DescLine3" select="$Line3"/>
					<xsl:with-param name="DescLine4" select="$Line4"/>
					<xsl:with-param name="LoanNumber" select="$Loan"/>
					<xsl:with-param name="City" select="$Cty"/>
					<xsl:with-param name="State" select="$St"/>
					<xsl:with-param name="ZipPlus" select="$ZPlus"/>
					<xsl:with-param name="LOB" select="$PASS_LOB"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$Code ='LA' or $Code ='AI' or $Code ='IO' or $Code ='CI' or $Code ='MT' or $Code ='SM' or $Code ='TM' or $Code ='LC'">
				<xsl:call-template name="BuildInterest">
					<xsl:with-param name="UseCode" select="$Code"/>
					<xsl:with-param name="UseLocation" select="$Location"/>
					<xsl:with-param name="ZipCode" select="$Zip"/>
					<xsl:with-param name="DescLine1" select="$Line1"/>
					<xsl:with-param name="DescLine2" select="$Line2"/>
					<xsl:with-param name="DescLine3" select="$Line3"/>
					<xsl:with-param name="DescLine4" select="$Line4"/>
					<xsl:with-param name="LoanNumber" select="$Loan"/>
					<xsl:with-param name="City" select="$Cty"/>
					<xsl:with-param name="State" select="$St"/>
					<xsl:with-param name="ZipPlus" select="$ZPlus"/>
					<xsl:with-param name="LOB" select="$PASS_LOB"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$Code ='GL'">
				<xsl:call-template name="BuildInterest">
					<xsl:with-param name="UseCode" select="$Code"/>
					<xsl:with-param name="UseLocation" select="$Location"/>
					<xsl:with-param name="ZipCode" select="$Zip"/>
					<xsl:with-param name="DescLine1" select="$Line1"/>
					<xsl:with-param name="DescLine2" select="$Line2"/>
					<xsl:with-param name="DescLine3" select="$Line3"/>
					<xsl:with-param name="DescLine4" select="$Line4"/>
					<!--<xsl:with-param name="LoanNumber"/>-->
					<xsl:with-param name="City" select="$Cty"/>
					<xsl:with-param name="State" select="$St"/>
					<xsl:with-param name="ZipPlus" select="$ZPlus"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$Code ='MH'">
				<xsl:call-template name="MobileHome">
					<xsl:with-param name="UseCode" select="$Code"/>
					<xsl:with-param name="DescLine1"/>
					<xsl:with-param name="DescLine2"/>
					<xsl:with-param name="DescLine3"/>
					<xsl:with-param name="DescLine4"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="BuildInterest">
		<xsl:param name="UseCode"/>
		<xsl:param name="UseLocation"/>
		<xsl:param name="ZipCode"/>
		<xsl:param name="DescLine1"/>
		<xsl:param name="DescLine2"/>
		<xsl:param name="DescLine3"/>
		<xsl:param name="DescLine4"/>
		<xsl:param name="LoanNumber"/>
		<xsl:param name="City"/>
		<xsl:param name="State"/>
		<xsl:param name="ZipPlus"/>
		<xsl:param name="LOB"/>
		<AdditionalInterest>
			<GeneralPartyInfo>
				<NameInfo>
					<xsl:if test="$LOB='ACV' or $LOB='APV'">	<!-- Case 39354 added test for APV -->
						<CommlName>
							<CommercialName>
								<xsl:value-of select="normalize-space($DescLine1)"/>
							</CommercialName>
							<IndexName/>
							<SupplementaryNameInfo>
								<SupplementaryNameCd/>
								<SupplementaryName/>
							</SupplementaryNameInfo>
						</CommlName>
					</xsl:if>
					<xsl:if test="$LOB='HP'">			<!-- Case 39354 remove test for APV -->
						<PersonName>
							<Surname>
								<xsl:value-of select="normalize-space($DescLine1)"/>
							</Surname>
						</PersonName>
					</xsl:if>
					<LegalEntityCd/>
					<xsl:if test="$UseCode !='GL'">
						<com.csc_LongName>
							<xsl:value-of select="normalize-space(concat($DescLine1,' ',$DescLine2))"/>
						</com.csc_LongName>
					</xsl:if>
					<com.csc_OtherIdentifier/>
				</NameInfo>
				<Addr>
					<AddrTypeCd>FMI</AddrTypeCd>
					<!-- 39354 Begin -->
					<Addr1>
						<xsl:value-of select="$DescLine2"/>
					</Addr1>
					<Addr2>
						<xsl:value-of select="$DescLine3"/>
					</Addr2>
					<Addr3/>
					<Addr4/>
					<!-- 39354 eND -->
					<City>
						<xsl:value-of select="$City"/>
					</City>
					<StateProvCd>
						<xsl:value-of select="$State"/>
					</StateProvCd>
					<StateProv/>
					<PostalCode>
						<xsl:choose>
							<xsl:when test="string-length($ZipPlus) &gt; 0">
								<xsl:value-of select="concat($ZipCode,'-',$ZipPlus)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$ZipCode"/>
							</xsl:otherwise>
						</xsl:choose>
					</PostalCode>
					<CountryCd/>
					<Country>USA</Country>
					<Latitude/>
					<Longitude/>
					<County/>
					<com.csc_AddrTypeInfo>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
						<EffectiveDt/>
						<ExpirationDt/>
						<AddrTypeCd/>
						<com.csc_NumberYearsAtAddr/>
						<ActionCd/>
					</com.csc_AddrTypeInfo>
					<com.csc_EffectiveDt/>
					<com.csc_ExpirationDt/>
					<com.csc_OtherIdentifier/>
					<com.csc_ActionCd/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
						<EmailAddr/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier/>
						<com.csc_ActionCd/>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<AdditionalInterestInfo>
				<NatureInterestCd>
					<xsl:value-of select="$UseCode"/>
				</NatureInterestCd>
				<InterestRank/>
				<xsl:if test="$LOB='ACV' or $LOB='HP'">
					<xsl:if test="$UseCode !='GL'">
						<AccountNumberId>
							<xsl:value-of select="$LoanNumber"/>
						</AccountNumberId>
					</xsl:if>
				</xsl:if>
				<InterestIdNumber/>
				<PayorInd/>
				<BillFrequencyCd/>
				<CertificateFrequencyCd/>
				<CertificateRequiredDt/>
				<CertificateIssuedDt/>
				<PolicyRequiredDt/>
				<PolicyIssuedDt/>
				<InterestEndDt/>
				<InformationalBillingInd/>
				<PolicyFrequencyCd/>
			</AdditionalInterestInfo>
		</AdditionalInterest>
	</xsl:template>
	<xsl:template name="MobileHome">
		<xsl:param name="UseCode"/>
		<xsl:param name="DescLine1" select="DESCRIPTION__FULL__SEG/DESCRIPTION__INFORMATION/DESCRIPTION__LINE__1"/>
		<xsl:param name="DescLine2" select="DESCRIPTION__FULL__SEG/DESCRIPTION__INFORMATION/DESCRIPTION__LINE__2"/>
		<xsl:param name="DescLine3" select="DESCRIPTION__FULL__SEG/DESCRIPTION__INFORMATION/DESCRIPTION__LINE__3"/>
		<xsl:param name="DescLine4" select="DESCRIPTION__FULL__SEG/DESCRIPTION__INFORMATION/DESCRIPTION__LINE__4"/>
		<MobileHome>
			<CookingLocationCd/>
			<CostNewAmt>
				<Amt/>
				<CurCd/>
				<com.csc_ItemIdInfo>
					<AgencyId/>
					<InsurerId/>
					<SystemId/>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
			</CostNewAmt>
			<MiscParty>
				<ItemIdInfo>
					<AgencyId/>
					<InsurerId/>
					<SystemId/>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</ItemIdInfo>
				<GeneralPartyInfo>
					<NameInfo>
						<CommlName>
							<CommercialName>
								<xsl:value-of select="normalize-space($DescLine4)"/>
							</CommercialName>
							<IndexName/>
							<SupplementaryNameInfo>
								<SupplementaryNameCd/>
								<SupplementaryName/>
							</SupplementaryNameInfo>
						</CommlName>
						<LegalEntityCd/>
						<com.csc_LongName/>
						<com.csc_OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</com.csc_OtherIdentifier>
					</NameInfo>
					<Addr>
						<AddrTypeCd/>
						<Addr1/>
						<Addr2/>
						<Addr3/>
						<Addr4/>
						<City/>
						<StateProvCd/>
						<StateProv/>
						<PostalCode/>
						<CountryCd/>
						<Country/>
						<Latitude/>
						<Longitude/>
						<County/>
						<com.csc_AddrTypeInfo>
							<OtherIdentifier>
								<OtherIdTypeCd/>
								<OtherId/>
							</OtherIdentifier>
							<EffectiveDt/>
							<ExpirationDt/>
							<AddrTypeCd/>
							<com.csc_NumberYearsAtAddr/>
							<ActionCd/>
						</com.csc_AddrTypeInfo>
						<com.csc_ActionCd/>
						<com.csc_EffectiveDt/>
						<com.csc_ExpirationDt/>
						<com.csc_OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</com.csc_OtherIdentifier>
						<com.csc_ValidationStatusCd/>
						<com.cscAddressValidationError/>
					</Addr>
					<Communications>
						<PhoneInfo>
							<PhoneTypeCd/>
							<CommunicationUseCd/>
							<PhoneNumber/>
							<EmailAddr/>
							<com.csc_EffectiveDt/>
							<com.csc_ExpirationDt/>
							<com.csc_OtherIdentifier>
								<OtherIdTypeCd/>
								<OtherId/>
							</com.csc_OtherIdentifier>
							<com.csc_ActionCd/>
						</PhoneInfo>
					</Communications>
					<com.csc_ItemIdInfo>
						<AgencyId/>
						<InsurerId/>
						<SystemId/>
						<OtherIdentifier>
							<OtherIdTypeCd/>
							<OtherId/>
						</OtherIdentifier>
					</com.csc_ItemIdInfo>
					<com.csc_ActionInfo>
						<ActionCd/>
						<com.csc_ReasonCd/>
					</com.csc_ActionInfo>
				</GeneralPartyInfo>
				<MiscPartyInfo>
					<MiscPartyRoleCd>MobileHomePark</MiscPartyRoleCd>
					<MiscPartySubRoleCd/>
				</MiscPartyInfo>
				<com.csc_ItemIdInfo>
					<AgencyId/>
					<InsurerId/>
					<SystemId/>
					<OtherIdentifier>
						<OtherIdTypeCd/>
						<OtherId/>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
				<com.csc_ActionInfo>
					<ActionCd/>
					<com.csc_ReasonCd/>
				</com.csc_ActionInfo>
			</MiscParty>
			<DoublewideMobileHomeTrailerInd/>
			<Length>
				<NumUnits>
					<xsl:value-of select="substring-after($DescLine2,' ')"/>
				</NumUnits>
				<UnitMeasurementCd/>
			</Length>
			<MobileHomeInParkInd/>
			<MobileHomeParkEstablishedDt/>
			<MobileTrailerHomeSkirtedInd/>
			<NumPermSpacesMobileHomePark/>
			<PermanentConnectionToElectricInd/>
			<PermanentConnectionToPhoneInd/>
			<PermanentConnectionToSewerInd/>
			<PermanentConnectionToWaterInd/>
			<PurchasedNewInd/>
			<TieDownCd/>
			<Width>
				<NumUnits>
					<xsl:value-of select="substring-after($DescLine2,'x')"/>
				</NumUnits>
				<UnitMeasurementCd/>
			</Width>
			<ItemDefinition>
				<ItemTypeCd/>
				<Manufacturer>
					<xsl:value-of select="substring-after($DescLine1,' ')"/>
				</Manufacturer>
				<Model>
					<xsl:value-of select="substring-before($DescLine2,' ')"/>
				</Model>
				<SerialIdNumber>
					<xsl:value-of select="substring-after($DescLine3,'S#')"/>
				</SerialIdNumber>
				<ModelYear>
					<xsl:value-of select="substring-before($DescLine1,' ')"/>
				</ModelYear>
				<ItemDesc/>
			</ItemDefinition>
			<com.csc_MobileHomePipesExposedInd/>
		</MobileHome>
	</xsl:template>
	<xsl:template name="CreateAdditionalInterestFrom43SEG">
		<xsl:param name="LOB"/>
		<xsl:param name="LJ-LOCNUM"/>
		<xsl:choose>
			<xsl:when test="$LOB='WCP' or $LOB='WC'">
				<xsl:for-each select="//PMD4J__NAME__ADDR__SEG">
					<xsl:choose>
						<xsl:when test="PMD4J__SEGMENT__KEY/PMD4J__LEVEL__KEY/PMD4J__LOCATION__NUMBER=$LJ-LOCNUM                                       and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__LEVEL__CODE='L'                                      and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__PART__CODE='J'                                      and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SUB__PART__CODE='1'                                      and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__INSURANCE__LINE='WC'">
							<AdditionalInterest>
								<GeneralPartyInfo>
									<NameInfo>
										<CommlName>
											<CommercialName>
												<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__1"/>
											</CommercialName>
											<SupplementaryNameInfo>
												<SupplementaryNameCd/>
												<SupplementaryName/>
											</SupplementaryNameInfo>
										</CommlName>
										<TaxIdentity>
											<TaxIdTypeCd>FEIN</TaxIdTypeCd>
											<TaxId>
												<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ID__NUMBER"/>
											</TaxId>
										</TaxIdentity>
									</NameInfo>
									<Addr>
										<Addr1>
											<xsl:value-of select="concat(normalize-space(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__2/PMD4J__ADDR__LIN__2__POS__1),normalize-space(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__1__2/PMD4J__ADDRESS__LINE__2/PMD4J__ADDR__LIN__2__POS__2__30))"/>
										</Addr1>
										<xsl:choose>
											<xsl:when test="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4 != ''">
												<Addr2>
													<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3"/>
												</Addr2>
												<xsl:variable name="Addr4City">
													<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,',')"/>
												</xsl:variable>
												<xsl:variable name="Addr4State">
													<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,',')"/>
												</xsl:variable>
												<xsl:choose>
													<xsl:when test="$Addr4City != ''">
														<City>
															<xsl:value-of select="$Addr4City"/>
														</City>
														<StateProvCd>
															<xsl:value-of select="$Addr4State"/>
														</StateProvCd>
													</xsl:when>
													<xsl:when test="string-length(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4) &gt; 2">
														<!-- No comma between city and state. -->
														<City>
															<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,' ')"/>
														</City>
														<StateProvCd>
															<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4,' ')"/>
														</StateProvCd>
													</xsl:when>
													<xsl:otherwise>
														<!-- If Address line 4 is not empty, and there is no comma seperating the city and state, then the state could be on line 4 by itself. -->
														<City>
															<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3"/>
														</City>
														<StateProvCd>
															<xsl:value-of select="substring(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__4, 1, 2)"/>
														</StateProvCd>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<Addr2/>
												<xsl:variable name="Addr3City">
													<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3,',')"/>
												</xsl:variable>
												<xsl:variable name="Addr3State">
													<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3,',')"/>
												</xsl:variable>
												<xsl:choose>
													<xsl:when test="$Addr3City != ''">
														<City>
															<xsl:value-of select="$Addr3City"/>
														</City>
														<StateProvCd>
															<xsl:value-of select="$Addr3State"/>
														</StateProvCd>
													</xsl:when>
													<xsl:otherwise>
														<!-- There is no comma seperating the city and state. -->
														<City>
															<xsl:value-of select="substring-before(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3, ' ')"/>
														</City>
														<StateProvCd>
															<xsl:value-of select="substring-after(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ADDRESS__LINE__3, ' ')"/>
														</StateProvCd>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
										<PostalCode>
											<xsl:value-of select="concat(concat(PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__BASIC, PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__DASH), PMD4J__SEGMENT__DATA/PMD4J__NAME__GROUP/PMD4J__ZIP__POSTAL__CODE/PMD4J__ZIP__USA/PMD4J__ZIP__EXPANDED__A)"/>
										</PostalCode>
									</Addr>
								</GeneralPartyInfo>
								<AdditionalInterestInfo>
									<NatureInterestCd>
										<xsl:value-of select="PMD4J__SEGMENT__KEY/PMD4J__VARIABLE__KEY/PMD4J__USE__CODE"/>
									</NatureInterestCd>
									<AccountNumberId>
										<xsl:value-of select="PMD4J__SEGMENT__DATA/PMD4J__INTEREST__ITEM"/>
									</AccountNumberId>
									<InterestIdNumber>
										<xsl:value-of select="PMD4J__SEGMENT__KEY/PMD4J__VARIABLE__KEY/PMD4J__SEQUENCE__USE__CODE"/>
									</InterestIdNumber>
								</AdditionalInterestInfo>
							</AdditionalInterest>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="TestIntRs.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->