<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template name="S2Vehdes53Template">
<xsl:param name="VehicleNumber"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>VEHICLE__DESCRIPTION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<VEHICLE__DESCRIPTION__SEG>
				<USVD__REC__LLBB>
					<USVD__REC__LENGTH/>
					<USVD__ACTION__CODE/>
					<USVD__FILE__ID/>
				</USVD__REC__LLBB>
				<USVD__ID>35</USVD__ID>
				<USVD__KEY>
					<USVD__UNIT__NUMBER>
						<!-- Case 35934 - Start -->
						<xsl:call-template name="FormatData">
							<xsl:with-param name="FieldName">UNIT</xsl:with-param>
							<xsl:with-param name="FieldLength">3</xsl:with-param>
							<xsl:with-param name="Value" select="$VehicleNumber"/>
							<xsl:with-param name="FieldType">N</xsl:with-param>
						</xsl:call-template>
						<!--<fill_0>00</fill_0>
						<USVD__NUNIT__LST__DIGIT>
							<xsl:value-of select="$VehicleNumber"/>
						</USVD__NUNIT__LST__DIGIT>-->
						<!-- Case 35934 - End -->
					</USVD__UNIT__NUMBER>
					<USVD__SEGMENT__NUMBER>0</USVD__SEGMENT__NUMBER>
					<USVD__AMENDMENT__NUMBER/>
				</USVD__KEY>
				<USVD__STATE>
					<!-- Issue 80442 Begin -->
					<xsl:choose>
						<xsl:when test="$LOB = 'ACV' or $LOB = 'AFV'">
							<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
  								<!-- xsl:with-param name="Value" select="//CommlPolicy/ControllingStateProvCd"/ -->
								<xsl:with-param name="Value" select="../StateProvCd"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
					<!-- Issue 80442 End -->
					<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
  						<xsl:with-param name="Value" select="//PersPolicy/ControllingStateProvCd"/>
					</xsl:call-template>
					<!-- Issue 80442 Begin -->
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 80442 End -->
				</USVD__STATE>
					<USVD__TERRITORY>
						<xsl:value-of select="TerritoryCd"/>
					</USVD__TERRITORY>
				<USVD__YEAR__MAKE>
					<USVD__YEAR__CC>
						<xsl:value-of select="substring(ModelYear,1,2)"/>
					</USVD__YEAR__CC>
					<USVD__YEAR__YY>
						<xsl:value-of select="substring(ModelYear,3,2)"/>
					</USVD__YEAR__YY>
				</USVD__YEAR__MAKE>
				<USVD__MAKE__DESC>
					<xsl:value-of select="concat(Manufacturer,' ',Model)"/>
				</USVD__MAKE__DESC>
				<USVD__SERIAL__NUMBER>
					<xsl:value-of select="VehIdentificationNumber"/>
				</USVD__SERIAL__NUMBER>
				<USVD__TYPE__VEHICLE>
					<xsl:value-of select="VehBodyTypeCd"/>
				</USVD__TYPE__VEHICLE>
				<USVD__PERFORMANCE>
					<xsl:value-of select="VehPerformanceCd"/>
				</USVD__PERFORMANCE>
				<USVD__BODY>
					<xsl:value-of select="string('S')"/>
				</USVD__BODY>
				<USVD__COST__NEW>
					<xsl:choose>
						<xsl:when test="CostNewAmt/Amt > 0">
							<!-- xsl:value-of select="CostNewAmt/Amt"/ -->
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">COST__NEW</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Value" select="CostNewAmt/Amt"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('     ')"/>
						</xsl:otherwise>
					</xsl:choose>
				</USVD__COST__NEW>
				<USVD__SYMBOL>
					<xsl:value-of select="VehSymbolCd"/>
				</USVD__SYMBOL>
				<USVD__DRIVER__ASSIGNED>
					<xsl:value-of select="string(' ')"/>
				</USVD__DRIVER__ASSIGNED>
				<USVD__USE__CODE>
					<xsl:value-of select="VehUseCd"/>
				</USVD__USE__CODE>
				<USVD__MILES__TO__WORK>
					<!-- Case 39354 Begin -->
					<!-- xsl:value-of select="DistanceOneWay/NumUnits"/ -->
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">MILES__TO__WORK</xsl:with-param>
						<xsl:with-param name="FieldLength">2</xsl:with-param>
						<xsl:with-param name="Value" select="DistanceOneWay/NumUnits"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
					<!-- Case 39354 End -->
				</USVD__MILES__TO__WORK>
				<USVD__DAYS__PER__WEEK>
					<xsl:choose>
						<xsl:when test="NumDaysDrivenPerWeek=''">5</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="NumDaysDrivenPerWeek"/>
						</xsl:otherwise>
					</xsl:choose>
				</USVD__DAYS__PER__WEEK>
				<USVD__ANNUAL__MILES>
					<!-- Case 39354 Begin -->
					<!-- xsl:value-of select="EstimatedAnnualDistance/NumUnits"/ -->
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">ANNUAL__MILES</xsl:with-param>
						<xsl:with-param name="FieldLength">2</xsl:with-param>
						<xsl:with-param name="Value" select="EstimatedAnnualDistance/NumUnits"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
					<!-- Case 39354 End -->
				</USVD__ANNUAL__MILES>
				<USVD__VEH__CLASS__CODE>
					<!-- Issue 80442 Begin -->
					<xsl:choose>
						<xsl:when test="$LOB = 'ACV' or $LOB = 'AFV'">
							<xsl:value-of select="CommlVehSupplement/PrimaryClassCd"/>
						</xsl:when>
						<xsl:otherwise>
					<!-- Issue 80442 End -->
					<xsl:value-of select="RateClassCd"/>
					<!-- Issue 80442 Begin -->
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 80442 End -->
				</USVD__VEH__CLASS__CODE>
				<USVD__DRIVER__CLASS/>
				<USVD__MULTICAR__OVERRIDE/>
				<USVD__ANTILOCK__DISC>
					<xsl:value-of select="AntiLockBrakeCd"/>
				</USVD__ANTILOCK__DISC>
				<USVD__PIP__RATING__BASIS/>
				<USVD__RENEWAL__PR__VEHICLE/>
				<USVD__ANTITHEFT__DEVICE>
					<!-- Issue 80442 Begin -->
					<xsl:choose>
						<xsl:when test="string-length(AntiTheftDeviceCd) = '0'">N</xsl:when>
						<xsl:otherwise>
					<!-- Issue 80442 End -->
					<xsl:value-of select="AntiTheftDeviceCd"/>
					<!-- Issue 80442 Begin -->
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 80442 End -->
				</USVD__ANTITHEFT__DEVICE>
				<USVD__PASSIVE__RESTRAINT>
					<xsl:value-of select="AirBagTypeCd"/>
				</USVD__PASSIVE__RESTRAINT>
				<USVD__SPECIAL__USE/>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal">
					<xsl:if test="InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='NI'">
						<USVD__ZIP__POSTAL__CODE>
							<xsl:value-of select="substring(concat(' ',GeneralPartyInfo/Addr/PostalCode),1,6)"/>
						</USVD__ZIP__POSTAL__CODE>
					</xsl:if>
				</xsl:for-each>
				<USVD__TAX__LOCATION/>
				<USVD__CONTROL>
					<VEHICLE__RATING__AGE/>
					<VEHICLE__ACTUAL__AGE/>
					<USVD__DATE__LAST__CHANGE>
						<USVD__YEAR>
							<USVD__DATE__LAST__CHANGE__CC/>
							<USVD__DATE__LAST__CHANGE__YY/>
						</USVD__YEAR>
						<USVD__MONTH/>
						<USVD__DAY/>
					</USVD__DATE__LAST__CHANGE>
				</USVD__CONTROL>
				<USVD__PURCHASE__DATE>
					<USVD__PURCHASE__DATE__CCYY>
						<USVD__PURCHASE__DATE__CC/>
						<USVD__PURCHASE__DATE__YY/>
					</USVD__PURCHASE__DATE__CCYY>
					<USVD__PURCHASE__DATE__MM/>
					<USVD__PURCHASE__DATE__DD/>
				</USVD__PURCHASE__DATE>
				<USVD__VEH__WEIGHT/>
				<USVD__STATED__AMOUNT__COLLISION/>
				<USVD__ANTIQUE__AUTO__VALUE/>
				<USVD__TORT__VALUE>
					<USVD__TORT__INDICATOR/>
					<!-- Case 35934 - Start -->
					<!--fill_0/-->
					<fill_5/>
					<!-- Case 35934 - End -->
					<USVD__TORT__5/>
				</USVD__TORT__VALUE>
				<USVD__CUBIC__CENTIMETERS>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">UNIT</xsl:with-param>
						<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="NumCylinders"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</USVD__CUBIC__CENTIMETERS>
				<USVD__CUBIC__CENTIMETERS__NUM/>
				<USVD__NUMBER__EMPL/>
				<USVD__COMMUTER__DISCOUNT/>
				<USVD__ALCOHOL__DRUG__DISC/>
				<USVD__STATED__AMOUNT/>
				<USVD__STATED__AMOUNT__NUM>
					<xsl:choose>
						<xsl:when test="com.csc_StatedAmt/Amt=''">00000</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="com.csc_StatedAmt/Amt"/>
						</xsl:otherwise>
					</xsl:choose>
				</USVD__STATED__AMOUNT__NUM>
					<USVD__VIN>
						<xsl:value-of select="VehIdentificationNumber"/>
					</USVD__VIN>
				<USVD__PL__NUM__TYPE>
					<USVD__PLATE__TYPE/>
					<USVD__PLATE__COLOR/>
					<USVD__PLATE__NUMBER/>
				</USVD__PL__NUM__TYPE>
				<USVD__SAFE__DRIVER__DISCOUNT/>
				<USVD__HAP__MOD__INDICATOR>
					<xsl:value-of select="string('N')"/>
				</USVD__HAP__MOD__INDICATOR>
				<USVD__COL__CLASS/>
				<USVD__FARTHEST__ZONE/>
				<USVD__CUSTOMER__DISCOUNT>
					<USVD__CUST__DISC__TYPES/>
					<!-- Case 35934 - Start -->
					<!--fill_1/-->
					<fill_6/>
					<!-- Case 35934 - End -->
				</USVD__CUSTOMER__DISCOUNT>
				<USVD__SIZE/>
				<USVD__COMM__USE/>
				<USVD__RADIUS/>
				<USVD__SPECIAL__INDUSTRY/>
				<USVD__SCHOOL__BUS__POL__SUBDIV/>
				<USVD__SCHOOL__CHART__REG__PLATE/>
				<USVD__NON__PROFIT__RELIG__ORG/>
				<USVD__HA__DIRECT__EXCESS__SW/>
				<USVD__RIDE__SHARING/>
				<USVD__MUN__LIAB__VOL__WORKER/>
				<USVD__DRIVE__TRAIN__DUAL__CONTROL/>
				<USVD__SNOWMOBILE__PASS__HAZARD/>
				<USVD__RATE__BOOK__ID__STORE/>
				<USVD__LEASED__AUTO__INDICATOR/>
				<USVD__FLEET__INDICATOR/>
				<USVD__CEDED__INDICATOR/>
				<USVD__REG__TERRITORY/>
				<USVD__STAT__TERRITORY/>
				<USVD__CPP__PACK__MOD__ASSIGN/>
				<USVD__POINT__DRIVER/>
				<USVD__PASS__OR__FAIL/>
				<USVD__MIN__RETAINED__SW/>
				<USVD__SUBSTITUTE/>
				<USVD__DDC__FACTOR/>
				<USVD__DDC__INDICATOR/>
				<USVD__ANTI__THEFT__FACTOR/>
				<USVD__PASS__RESTRAINT__FACT/>
				<USVD__GST__INDICATOR/>
				<USVD__GDD__INDICATOR/>
				<USVD__DTD__INDICATOR/>
				<USVD__GDD__FACTOR/>
				<USVD__DGD__FACTOR/>
				<USVD__DGD__INDICATOR/>
				<USVD__TOTAL__OPERATORS/>
				<USVD__YOUTHFUL__OPERATORS/>
				<USVD__POLICY__OPERATORS/>
				<USVD__TOTAL__VEHICLES/>
				<USVD__ANTILOCK__FACTOR/>
				<USVD__NH__SDIP__PERCENT/>
				<fill_2/>
				<USVD__APC__FACTOR/>
				<USVD__APC__INDICATOR/>
				<USVD__WLW__FACTOR/>
				<USVD__WLW__INDICATOR/>
				<USVD__CDD__INDICATOR/>
				<USVD__VEHICLE__ADD__DATE/>
				<USVD__XX__RATE__BOOK__ID__TABLE>
					<USVD__XX__RATE__BOOK__ID__DATA>
						<USVD__XX__COV__RATE__BOOK__ID/>
					</USVD__XX__RATE__BOOK__ID__DATA>
				</USVD__XX__RATE__BOOK__ID__TABLE>
				<USVD__PMS__FUTURE__USE/>
				<USVD__CUST__FUTURE__USE>
					<USVD__VEH__MS__MC__CLASS>
						<USVD__VEH__MS__MC__PRI__CLASS/>
						<USVD__VEH__MS__MC__SEC__CLASS/>
					</USVD__VEH__MS__MC__CLASS>
					<fill_3/>
				</USVD__CUST__FUTURE__USE>
				<USVD__YR2000__CUST__USE/>
				<USVD__DAY__LIGHTS__FIELDS>
					<USVD__DAY__LIGHTS__DISC__FLDS>
							<USVD__DAY__LIGHTS__IND>
								<xsl:value-of select="DaytimeRunningLightInd"/>
							</USVD__DAY__LIGHTS__IND>
					</USVD__DAY__LIGHTS__DISC__FLDS>
				</USVD__DAY__LIGHTS__FIELDS>
				<USVD__DUP__KEY__SEQ__NUM/>
			</VEHICLE__DESCRIPTION__SEG>
		</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>
<!-- Stylesheet edited using Stylus Studio - (c)1998-2002 eXcelon Corp. -->
