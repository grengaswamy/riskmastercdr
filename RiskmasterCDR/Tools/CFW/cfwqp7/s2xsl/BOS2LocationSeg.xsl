<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2LocationTemplate" match="/">
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMD4J__NAME__ADDR__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMD4J__NAME__ADDR__SEG>
				<PMD4J__SEGMENT__KEY>
					<PMD4J__REC__LLBB/>
					<PMD4J__SEGMENT__ID>43</PMD4J__SEGMENT__ID>
					<PMD4J__SEGMENT__STATUS>A</PMD4J__SEGMENT__STATUS>
					<PMD4J__TRANSACTION__DATE>
						<PMD4J__YEAR__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,1,4)"/>
						</PMD4J__YEAR__TRANSACTION>
						<PMD4J__MONTH__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,6,2)"/>
						</PMD4J__MONTH__TRANSACTION>
						<PMD4J__DAY__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,9,2)"/>
						</PMD4J__DAY__TRANSACTION>
					</PMD4J__TRANSACTION__DATE>
					<PMD4J__SEGMENT__ID__KEY>
						<PMD4J__SEGMENT__LEVEL__CODE>J</PMD4J__SEGMENT__LEVEL__CODE>
						<PMD4J__SEGMENT__PART__CODE>J</PMD4J__SEGMENT__PART__CODE>
						<PMD4J__SUB__PART__CODE>1</PMD4J__SUB__PART__CODE>
						<PMD4J__INSURANCE__LINE>
							<xsl:value-of select="string('  ')"/>
						</PMD4J__INSURANCE__LINE>
					</PMD4J__SEGMENT__ID__KEY>
					<PMD4J__LEVEL__KEY>
						<PMD4J__LOCATION__NUMBER>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">SITE</xsl:with-param>
								<xsl:with-param name="FieldLength">4</xsl:with-param>
								<xsl:with-param name="Value" select="ItemIdInfo/InsurerId"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</PMD4J__LOCATION__NUMBER>
						<PMD4J__SUB__LOCATION__NUMBER>
							<xsl:value-of select="string('   ')"/>
						</PMD4J__SUB__LOCATION__NUMBER>
						<PMD4J__RISK__UNIT__GROUP__KEY>
							<PMD4J__RISK__UNIT__GROUP>
								<xsl:value-of select="string('000')"/>
							</PMD4J__RISK__UNIT__GROUP>
							<PMD4J__SEQ__RSK__UNT__GRP>
								<xsl:value-of select="string('000')"/>
							</PMD4J__SEQ__RSK__UNT__GRP>
						</PMD4J__RISK__UNIT__GROUP__KEY>
						<PMD4J__RISK__UNIT>
							<xsl:value-of select="string('      ')"/>
						</PMD4J__RISK__UNIT>
						<PMD4J__SEQUENCE__RISK__UNIT>
							<PMD4J__RISK__SEQUENCE>
								<xsl:value-of select="string('0')"/>
							</PMD4J__RISK__SEQUENCE>
							<PMD4J__RISK__TYPE__IND>
								<xsl:value-of select="string(' ')"/>
							</PMD4J__RISK__TYPE__IND>
						</PMD4J__SEQUENCE__RISK__UNIT>
					</PMD4J__LEVEL__KEY>
					<PMD4J__ITEM__EFFECTIVE__DATE>
						<PMD4J__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMD4J__YEAR__ITEM__EFFECTIVE>
						<PMD4J__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMD4J__MONTH__ITEM__EFFECTIVE>
						<PMD4J__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMD4J__DAY__ITEM__EFFECTIVE>
					</PMD4J__ITEM__EFFECTIVE__DATE>
					<PMD4J__VARIABLE__KEY>
						<PMD4J__USE__CODE>LOC</PMD4J__USE__CODE>
						<PMD4J__SEQUENCE__USE__CODE__A>
							<PMD4J__SEQUENCE__USE__CODE>
								<xsl:value-of select="string('000')"/>
							</PMD4J__SEQUENCE__USE__CODE>
						</PMD4J__SEQUENCE__USE__CODE__A>
					</PMD4J__VARIABLE__KEY>
					<PMD4J__PROCESS__DATE>
						<PMD4J__YEAR__PROCESS>
							<xsl:value-of select="substring($TransRqDt,1,4)"/>
						</PMD4J__YEAR__PROCESS>
						<PMD4J__MONTH__PROCESS>
							<xsl:value-of select="substring($TransRqDt,6,2)"/>
						</PMD4J__MONTH__PROCESS>
						<PMD4J__DAY__PROCESS>
							<xsl:value-of select="substring($TransRqDt,9,2)"/>
						</PMD4J__DAY__PROCESS>
					</PMD4J__PROCESS__DATE>
				</PMD4J__SEGMENT__KEY>
				<PMD4J__SEGMENT__DATA>
					<PMD4J__USE__CODE__DATA>L</PMD4J__USE__CODE__DATA>
					<PMD4J__NAME__GROUP>
						<PMD4J__SORT__NAME__AREA>
							<PMD4J__SORT__NAME>
								<xsl:value-of select="substring('Addr/Addr1',1,4)"/>
							</PMD4J__SORT__NAME>
							<PMD4J__TAX__LOC>
								<xsl:value-of select="string('      ')"/>
							</PMD4J__TAX__LOC>
						</PMD4J__SORT__NAME__AREA>
						<PMD4J__NAME__TYPE__IND>
							<xsl:value-of select="string(' ')"/>
						</PMD4J__NAME__TYPE__IND>
						<PMD4J__ADDRESS__LINE__1__2>
							<PMD4J__ADDRESS__LINE__1>
								<xsl:value-of select="Addr/Addr1"/>
							</PMD4J__ADDRESS__LINE__1>
							<PMD4J__ADDRESS__LINE__2>
								<PMD4J__ADDR__LIN__2__POS__1>
									<xsl:value-of select="substring(Addr/Addr2, 1, 1)"/>
								</PMD4J__ADDR__LIN__2__POS__1>
								<PMD4J__ADDR__LIN__2__POS__2__30>
									<xsl:value-of select="substring(Addr/Addr2, 2, 29)"/>
								</PMD4J__ADDR__LIN__2__POS__2__30>
							</PMD4J__ADDRESS__LINE__2>
						</PMD4J__ADDRESS__LINE__1__2>
						<PMD4J__ADDRESS__LINE__3>
							<xsl:if test="string-length(Addr/City) &lt; 27">
								<xsl:value-of select="concat(Addr/City, ', ')"/>
								<xsl:value-of select="Addr/StateProvCd"/>
							</xsl:if>
							<xsl:if test="string-length(City) &gt; 26">
								<xsl:value-of select="concat(substring(Addr/City, 1, 26), ', ')"/>
								<xsl:value-of select="Addr/StateProvCd"/>
							</xsl:if>
						</PMD4J__ADDRESS__LINE__3>
						<PMD4J__ADDRESS__LINE__4>
							<xsl:value-of select="string('                              ')"/>
						</PMD4J__ADDRESS__LINE__4>
						<PMD4J__ID__NUMBER>
							<xsl:value-of select="string('             ')"/>
						</PMD4J__ID__NUMBER>
						<PMD4J__ZIP__POSTAL__CODE>
							<PMD4J__ZIP__USA>
								<PMD4J__ZIP__BASIC>
									<xsl:value-of select="substring(Addr/PostalCode, 1, 5)"/>
								</PMD4J__ZIP__BASIC>
								<PMD4J__ZIP__DASH>
									<xsl:value-of select="string('-')"/>
								</PMD4J__ZIP__DASH>
								<PMD4J__ZIP__EXPANDED>
									<xsl:value-of select="substring(Addr/PostalCode, 7, 4)"/>
								</PMD4J__ZIP__EXPANDED>
							</PMD4J__ZIP__USA>
						</PMD4J__ZIP__POSTAL__CODE>
					</PMD4J__NAME__GROUP>
					<PMD4J__PHONE>
						<PMD4J__PHONE__AREA__A>
							<PMD4J__PHONE__AREA>
								<xsl:value-of select="string('   ')"/>
							</PMD4J__PHONE__AREA>
						</PMD4J__PHONE__AREA__A>
						<PMD4J__PHONE__EXCHANGE__A>
							<PMD4J__PHONE__EXCHANGE>
								<xsl:value-of select="string('   ')"/>
							</PMD4J__PHONE__EXCHANGE>
						</PMD4J__PHONE__EXCHANGE__A>
						<PMD4J__PHONE__NUMBER__A>
							<PMD4J__PHONE__NUMBER>
								<xsl:value-of select="string('    ')"/>
							</PMD4J__PHONE__NUMBER>
						</PMD4J__PHONE__NUMBER__A>
						<PMD4J__PHONE__EXTENSION>
							<xsl:value-of select="string('0000')"/>
						</PMD4J__PHONE__EXTENSION>
					</PMD4J__PHONE>
					<PMD4J__INTEREST__ITEM>
						<xsl:value-of select="string('                              ')"/>
					</PMD4J__INTEREST__ITEM>
					<PMD4J__LOC__LINE__EXCLUSION>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__LOC__LINE__EXCLUSION>
					<PMD4J__SURCHARGE>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__SURCHARGE>
					<PMD4J__MUNICIPAL__TAX>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__MUNICIPAL__TAX>
					<PMD4J__02__AUDIT__CODE>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__02__AUDIT__CODE>
					<PMD4J__02__LEGAL__ENTITY>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__02__LEGAL__ENTITY>
					<PMD4J__DEC__CHANGE__FLAG>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__DEC__CHANGE__FLAG>
					<PMD4J__BLKT__MISC__LOC__EXCLUSION>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__BLKT__MISC__LOC__EXCLUSION>
					<PMD4J__LOCATION__STATE>
						<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
							<xsl:with-param name="Value" select="Addr/StateProvCd"/>
						</xsl:call-template>
					</PMD4J__LOCATION__STATE>
					<PMD4J__MATCHING__LOC__SW>
						<xsl:value-of select="string(' ')"/>
					</PMD4J__MATCHING__LOC__SW>
					<PMD4J__ITEM__EXPIRE__DATE>
						<PMD4J__ITEM__EXPIRE__YEAR>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PMD4J__ITEM__EXPIRE__YEAR>
						<PMD4J__ITEM__EXPIRE__MONTH>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PMD4J__ITEM__EXPIRE__MONTH>
						<PMD4J__ITEM__EXPIRE__DAY>
							<xsl:value-of select="substring($ExpDt,9,2)"/>
						</PMD4J__ITEM__EXPIRE__DAY>
					</PMD4J__ITEM__EXPIRE__DATE>
					<PMD4J__PMS__FUTURE__USE>
						<xsl:value-of select="string('                                                        ')"/>
					</PMD4J__PMS__FUTURE__USE>
					<PMD4J__CUST__SPL__USE>
						<xsl:value-of select="string('                    ')"/>
					</PMD4J__CUST__SPL__USE>
					<PMD4J__YR2000__CUST__USE>
						<xsl:value-of select="string('                                                                                                    ')"/>
					</PMD4J__YR2000__CUST__USE>
				</PMD4J__SEGMENT__DATA>
			</PMD4J__NAME__ADDR__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="BORQ_S2_ACORD.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="BORQ_S2_ACORD.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="pmd4jRecord.xml" destSchemaRoot="pmd4jRecord" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->