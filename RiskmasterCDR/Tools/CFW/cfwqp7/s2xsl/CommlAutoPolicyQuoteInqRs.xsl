<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform Series II records into internal XML
prior to posting data to the iSolutions database.
E-Service case .NET 
***********************************************************************************************
-->
  <xsl:include href="S2CommonFuncRs.xsl"/>
  <xsl:include href="S2SignOnRs.xsl"/>
  <xsl:include href="S2ErrorRs.xsl"/>
  <xsl:include href="S2ProducerRs.xsl"/>
  <xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
  <xsl:include href="S2MessageStatusRs.xsl"/>
  <!-- Issue 80442 Begin -->
  <xsl:include href="S2CommlPolicyRs.xsl"/>
  <xsl:include href="S2CreditSurchargeRs.xsl"/>
  <xsl:include href="S2FormsRs.xsl"/>
  <xsl:include href="S2CACommlRateStateRs.xsl"/>
  <xsl:include href="S2CACommlVehRs.xsl"/>
  <xsl:include href="S2AdditionalInterestRs.xsl"/>
  <xsl:include href="S2CACommlCoverageRs.xsl"/>
  <!-- Issue 80442 End -->

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<ACORD>
		      <xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
			        <RqUID/>
				<CommlAutoPolicyQuoteInqRs>
				        <RqUID/>
				          <TransactionResponseDt/>
				          <TransactionEffectiveDt/>
				          <CurCd>USD</CurCd>
					  <xsl:call-template name="BuildMessageStatus">
						<xsl:with-param name="MessageType">
							<xsl:value-of select="string('RATING')"/>
						</xsl:with-param>
					  </xsl:call-template>
					<!-- Issue 80442 Begin -->
					<xsl:for-each select="CommlAutoPolicyQuoteInqRs/POLICY__INFORMATION__SEG">
						<xsl:variable name="LOB" select="string('CA')"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Producer"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Business"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="CommlPolicy">
								<xsl:with-param name="LOB" select="$LOB"/>
							</xsl:apply-templates>
					</xsl:for-each>
					<!-- Issue 80442 End -->
					<!-- Issue 80442 CommlPolicy>
						<xsl:attribute name="id">
							<xsl:value-of select="CommlAutoPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__APP__ID"/>
						</xsl:attribute>
					</CommlPolicy -->
					<!-- Issue 80442 Begin -->
					<CommlAutoLineBusiness>
						<LOBCd>CA</LOBCd>
						<xsl:call-template name="CreateCommlRateState"/>
					</CommlAutoLineBusiness>
					<!-- Issue 80442 End -->
				</CommlAutoPolicyQuoteInqRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>

</xsl:stylesheet>