<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service Issue # 35270		 
***********************************************************************************************
-->

	<xsl:template name="S2HOErrorsRsTemplate">
		<xsl:choose>
			<xsl:when test="count(//ERROR__DESC__SEG)&gt; 0">
				<MsgStatus>
					<MsgStatusCd>1</MsgStatusCd>
					<MsgErrorCd></MsgErrorCd>
					<MsgStatusDesc>RATING ERROR(s).</MsgStatusDesc>
					<xsl:for-each select="/*/ERROR__DESC__SEG">
						<ExtendedStatus>
							<ExtendedStatusCd>
								<xsl:value-of select="concat(./ERR__SEGMENT__DATA/ERR__MANUAL__SCREEN,substring(./ERR__SEGMENT__DATA/ERR__ERROR__CODE,1,1))"/>
							</ExtendedStatusCd>
							<ExtendedStatusDesc>
								<xsl:value-of select="concat(./ERR__SEGMENT__DATA/ERR__ERROR__DESC,./ERR__SEGMENT__DATA/ERR__XML__ERROR__DESC)"/>
							</ExtendedStatusDesc>
						</ExtendedStatus>
					</xsl:for-each>
				</MsgStatus>
			</xsl:when>
			<xsl:otherwise>
				<MsgStatus>
					<MsgStatusCd>
						<xsl:text>INFO</xsl:text>
					</MsgStatusCd>
					<MsgErrorCd>0</MsgErrorCd>
					<MsgStatusDesc>Success</MsgStatusDesc>
				</MsgStatus>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="no" name="WaterCraft.xml" userelativepaths="yes" externalpreview="no" url="..\WaterCraft.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="CFWintermediat Response File_eITH eRROR.xml" htmlbaseurl="" outputurl="..\WaterCraftRS.xml" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\Accords\APPID&#x2D;11702240033000004024_Host&#x2D;PT_DB&#x2D;SYS_Resp_Intermediate.xml" srcSchemaRoot="HomePolicyQuoteInqRs" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\Accords\APPID&#x2D;11702240033000004024_Host&#x2D;PT_DB&#x2D;SYS_Resp.xml" destSchemaRoot="ACORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->