<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="PersPolicy">
		<xsl:param name="LOB"/>
		<PersPolicy>
			<xsl:attribute name="id">
				<xsl:value-of select="PIF__ISOL__APP__ID"/>
			</xsl:attribute>
			<PolicyNumber>
				<xsl:value-of select="PIF__KEY/PIF__POLICY__NUMBER"/>
			</PolicyNumber>
			<PolicyVersion>
				<xsl:value-of select="PIF__KEY/PIF__MODULE"/>
			</PolicyVersion>
			<CompanyProductCd>
				<xsl:value-of select="PIF__KEY/PIF__SYMBOL"/>
			</CompanyProductCd>
			<LOBCd>
				<xsl:value-of select="PIF__LINE__BUSINESS"/>
			</LOBCd>
			<!--Removed for Issue 35934 <LOBSubCd/>-->
			<NAICCd>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>
			</NAICCd>
			<ControllingStateProvCd>
				<xsl:call-template name="BuildStateProvCd">
					<xsl:with-param name="StateCd" select="PIF__RISK__STATE__PROV"/>
				</xsl:call-template>
			</ControllingStateProvCd>
			<ContractTerm>
				<EffectiveDt>
					<!--Modified for Issue 35934
					<xsl:value-of select="normalize-space(PIF__EFFECTIVE__DATE)"/>-->
					<xsl:value-of select="PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR"/>/<xsl:choose><xsl:when test="string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)='1'">0<xsl:value-of select="PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO"/></xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)"/></xsl:otherwise></xsl:choose>/<xsl:choose>
						<xsl:when test="string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DA)='1'">0<xsl:value-of select="normalize-space(PIF__EFFECTIVE__DATE/PIF__EFF__DA)"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="normalize-space(PIF__EFFECTIVE__DATE/PIF__EFF__DA)"/>
						</xsl:otherwise></xsl:choose></EffectiveDt>
				<ExpirationDt>
					<!--Modified for Issue 35934
					<xsl:value-of select="normalize-space(PIF__EXPIRATION__DATE)"/>-->
					<xsl:value-of select="PIF__EXPIRATION__DATE/PIF__EXP__DATE__YR__MO/PIF__EXP__YR"/>/<xsl:choose><xsl:when test="string-length(PIF__EXPIRATION__DATE/PIF__EXP__DATE__YR__MO/PIF__EXP__MO)='1'">0<xsl:value-of select="PIF__EXPIRATION__DATE/PIF__EXP__DATE__YR__MO/PIF__EXP__MO"/></xsl:when><xsl:otherwise><xsl:value-of select="normalize-space(PIF__EXPIRATION__DATE/PIF__EXP__DATE__YR__MO/PIF__EXP__MO)"/></xsl:otherwise></xsl:choose>/<xsl:choose>
						<xsl:when test="string-length(PIF__EXPIRATION__DATE/PIF__EXP__DA)='1'">0<xsl:value-of select="normalize-space(PIF__EXPIRATION__DATE/PIF__EXP__DA)"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="normalize-space(PIF__EXPIRATION__DATE/PIF__EXP__DA)"/>
						</xsl:otherwise></xsl:choose></ExpirationDt>
				<DurationPeriod>
					<NumUnits>
						<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
					</NumUnits>
				</DurationPeriod>
			</ContractTerm>
			<BillingAccountNumber/>
			<BillingMethodCd/>
			<CurrentTermAmt>
				<Amt>
					<xsl:value-of select="/*/PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/>
				</Amt>
				<CurCd/>
			</CurrentTermAmt>
			<GroupId/>
			<OtherInsuranceWithCompanyCd/>
			<PayorCd/>
			<RateEffectiveDt/>
			<RenewalBillingMethodCd/>
			<RenewalPayorCd/>
			<RenewalTerm>
				<EffectiveDt/>
				<ExpirationDt/>
			</RenewalTerm>
			<xsl:if test="$LOB='APV'">
				<xsl:apply-templates select="/*/COMPUTER__ISSUE__FORMS__SEG" mode="BuildForm"/>
			</xsl:if>
			<OtherOrPriorPolicy>
				<PolicyCd/>
				<PolicyNumber/>
				<LOBCd/>
				<NAICCd/>
				<InsurerName/>
				<ContractTerm>
					<EffectiveDt/>
					<ExpirationDt/>
				</ContractTerm>
				<PolicyAmt>
					<Amt/>
					<CurCd/>
				</PolicyAmt>
				<RatingFactor/>
				<CompanyProductCd/>
			</OtherOrPriorPolicy>
			<PaymentOption>
				<PaymentPlanCd>
					<xsl:value-of select="concat(PIF__PAY__SERVICE__CODE,PIF__MODE__CODE)"/>
				</PaymentPlanCd>
				<MethodPaymentCd/>
				<NextTermPaymentPlanCd/>
				<DepositAmt>
					<Amt/>
					<CurCd/>
				</DepositAmt>
				<DownPaymentPct/>
				<FirstPaymentDueDt/>
				<PaymentIntervalCd/>
				<NumPayments>
					<xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__NUMBER__INSTALLMENTS"/>
				</NumPayments>
				<LOBCd/>
				<LOBSubCd/>
				<com.csc_BillingPlanCd/>
			</PaymentOption>
			<!-- Policy Level Additional Interests -->
			<xsl:call-template name="CreateAdditionalInterestFrom12SEG">
				<xsl:with-param name="PASS_LOB" select="$LOB"/>
				<xsl:with-param name="UseCode"/>
				<xsl:with-param name="UnitNo">000</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="$LOB='APV'">
				<xsl:apply-templates select="/*/DRIVER__SEG" mode="CreateAccidentViolation"/>
				<xsl:apply-templates select="/*/DRIVER__SEG" mode="CreateDriverVeh"/>
			</xsl:if>
			<com.csc_CompanyPolicyProcessingId>
				<!--Modified for Issue 35934<xsl:value-of select="PIF__LOCATION"/>-->
				<xsl:choose>
					<xsl:when test="string-length(PIF__LOCATION)='1'">0<xsl:value-of select="PIF__LOCATION"/></xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="PIF__LOCATION"/>
					</xsl:otherwise>
				</xsl:choose>				
			</com.csc_CompanyPolicyProcessingId>
			<com.csc_InsuranceLineIssuingCompany>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>
			</com.csc_InsuranceLineIssuingCompany>
			<com.csc_PolicyTermMonths>
				<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
			</com.csc_PolicyTermMonths>
			<!-- Added 10/20/03 -->
			<xsl:variable name="DwellInd"/>
			<!-- Added 10/06/03, moved policy level discounts to here in particular Group Discount -->
			<xsl:call-template name="CreditSurcharge">
				<xsl:with-param name="POLICY-LOB" select="$LOB"/>
				<xsl:with-param name="DwellInd" select="'N'"/>
			</xsl:call-template>
		</PersPolicy>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->