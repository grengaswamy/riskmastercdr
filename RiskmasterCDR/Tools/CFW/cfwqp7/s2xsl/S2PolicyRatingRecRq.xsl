<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform iSolutions data into Series II record format.
E-Service case 34768 
***********************************************************************************************
-->

<xsl:template name="S2PolicyRatingRecTemplate">
	<xsl:param name="AnnDt"/>

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMDL4W1__POLICY__RATING__REC</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMDL4W1__POLICY__RATING__REC>
				<PMDL4W1__SEGMENT__KEY>
					<PMDL4W1__REC__LLBB>
						<PMDL4W1__REC__LENGTH/>
						<PMDL4W1__ACTION__CODE/>
						<PMDL4W1__FILE__ID/>
					</PMDL4W1__REC__LLBB>
					<PMDL4W1__SEGMENT__ID>43</PMDL4W1__SEGMENT__ID>
					<PMDL4W1__SEGMENT__STATUS>A</PMDL4W1__SEGMENT__STATUS>
					<PMDL4W1__TRANSACTION__DATE>
						<PMDL4W1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDL4W1__YEAR__TRANSACTION>
						<PMDL4W1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDL4W1__MONTH__TRANSACTION>
						<PMDL4W1__DAY__TRANSACTION>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDL4W1__DAY__TRANSACTION>
					</PMDL4W1__TRANSACTION__DATE>
					<PMDL4W1__SEGMENT__ID__KEY>
						<PMDL4W1__SEGMENT__LEVEL__CODE>L</PMDL4W1__SEGMENT__LEVEL__CODE>
						<PMDL4W1__SEGMENT__PART__CODE>X</PMDL4W1__SEGMENT__PART__CODE>
						<PMDL4W1__SUB__PART__CODE>1</PMDL4W1__SUB__PART__CODE>
						<PMDL4W1__INSURANCE__LINE>WC</PMDL4W1__INSURANCE__LINE>
					</PMDL4W1__SEGMENT__ID__KEY>
					<PMDL4W1__LEVEL__KEY>
						<PMDL4W1__LOCATION__NUMBER>0000</PMDL4W1__LOCATION__NUMBER>
						<fill_0/>	<!-- Filler length of 17 -->
						<PMDL4W1__SPLIT__RATE__SEQ>00</PMDL4W1__SPLIT__RATE__SEQ>
					</PMDL4W1__LEVEL__KEY>
					<PMDL4W1__ITEM__EFFECTIVE__DATE>
						<PMDL4W1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDL4W1__YEAR__ITEM__EFFECTIVE>
						<PMDL4W1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDL4W1__MONTH__ITEM__EFFECTIVE>
						<PMDL4W1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDL4W1__DAY__ITEM__EFFECTIVE>
					</PMDL4W1__ITEM__EFFECTIVE__DATE>
					<PMDL4W1__VARIABLE__KEY>
						<PMDL4W1__AUDIT__NUMBER>00</PMDL4W1__AUDIT__NUMBER>
						<PMDL4W1__AUDIT__NUM__SEQ>0</PMDL4W1__AUDIT__NUM__SEQ>
						<fill_1/>	<!-- Filler length of 3 -->
					</PMDL4W1__VARIABLE__KEY>
					<PMDL4W1__PROCESS__DATE>
						<PMDL4W1__YEAR__PROCESS>
							<xsl:value-of select="substring($ActDate,1,4)"/>
						</PMDL4W1__YEAR__PROCESS>
						<PMDL4W1__MONTH__PROCESS>
							<xsl:value-of select="substring($ActDate,6,2)"/>
						</PMDL4W1__MONTH__PROCESS>
						<PMDL4W1__DAY__PROCESS>
							<xsl:value-of select="substring($ActDate,9,2)"/>
						</PMDL4W1__DAY__PROCESS>
					</PMDL4W1__PROCESS__DATE>
				</PMDL4W1__SEGMENT__KEY>
				<PMDL4W1__SEGMENT__DATA>
					<PMDL4W1__ITEM__EXPIRE__DATE>
						<PMDL4W1__YEAR__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PMDL4W1__YEAR__ITEM__EXPIRE>
						<PMDL4W1__MONTH__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PMDL4W1__MONTH__ITEM__EXPIRE>
						<PMDL4W1__DAY__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,9,2)"/>
						</PMDL4W1__DAY__ITEM__EXPIRE>
					</PMDL4W1__ITEM__EXPIRE__DATE>
					<PMDL4W1__RATING__PROGRAM__TYPE>G</PMDL4W1__RATING__PROGRAM__TYPE>
					<xsl:choose>
						<!-- <xsl:when test="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/CompanyProductCd, 3, 1) = 'V'"> -->			<!-- Case 40090 -->
						<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/LOBSubCd = 'VOL'">					<!-- Case 40090 -->
							<PMDL4W1__POLICY__TYPE>S</PMDL4W1__POLICY__TYPE>
						</xsl:when>
						<xsl:otherwise>
							<PMDL4W1__POLICY__TYPE>
								<!-- <xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/CompanyProductCd, 3, 1)"/> -->	<!-- Case 40090 -->
								<xsl:value-of select="string('A')"/>									<!-- Case 40090 -->
							</PMDL4W1__POLICY__TYPE>
						</xsl:otherwise>
					</xsl:choose>
					<PMDL4W1__FED__EMP__ID__NUMBER>
						<xsl:choose>
							<xsl:when test="string-length(/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId) &gt; 0">
								<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="string('UNKNOWN')"/>
							</xsl:otherwise>
						</xsl:choose>
					</PMDL4W1__FED__EMP__ID__NUMBER>
					<PMDL4W1__COMMISSION__TYPE>S</PMDL4W1__COMMISSION__TYPE>
					<PMDL4W1__COMMISSION__SCHEDULE/>
					<PMDL4W1__COMM__SCHED__MG__IND>G</PMDL4W1__COMM__SCHED__MG__IND>
					<PMDL4W1__COMMISSION__RATE>000000</PMDL4W1__COMMISSION__RATE>
					<PMDL4W1__COMM__RATE__MG__IND/>
					<PMDL4W1__INTERSTATE__RISK__ID__NO/>
					<PMDL4W1__INTERSTATE__EXP__MOD__1>00000</PMDL4W1__INTERSTATE__EXP__MOD__1>
					<PMDL4W1__INTER__MOD__1__TYPE/>
					<PMDL4W1__INTERSTATE__EXP__MOD__2>00000</PMDL4W1__INTERSTATE__EXP__MOD__2>
					<PMDL4W1__INTER__MOD__2__TYPE/>
					<PMDL4W1__ANN__RATE__DTE__MMDD>
						<PMDL4W1__ANN__RATE__DTE__MM>
							<xsl:value-of select="substring($AnnDt,6,2)"/>
						</PMDL4W1__ANN__RATE__DTE__MM>
						<PMDL4W1__ANN__RATE__DTE__DD>
							<xsl:value-of select="substring($AnnDt,9,2)"/>
						</PMDL4W1__ANN__RATE__DTE__DD>
					</PMDL4W1__ANN__RATE__DTE__MMDD>
					<PMDL4W1__SPLIT__RATE__IND>
						<xsl:choose>
							<xsl:when test="$AnnDt != $EffDt">
								<xsl:value-of select="string('S')"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</PMDL4W1__SPLIT__RATE__IND>
					<PMDL4W1__PREV__CANC__REASON/>
					<PMDL4W1__PREV__CANC__DATE/>
					<PMDL4W1__COVERAGE__II__LMTS__STD>
						<PMDL4W1__COV__II__LMTS__STD__EACH>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">LMTS_STD_EACH</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Value" select="number(CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
								<xsl:with-param name="NumberMask">00000</xsl:with-param>
							</xsl:call-template>
						</PMDL4W1__COV__II__LMTS__STD__EACH>
						<PMDL4W1__COV__II__LMTS__STD__POL>
							<xsl:choose>
								<xsl:when test="substring(/ACORD/InsuranceSvcRq/*/CommlPolicy/CompanyProductCd, 3, 1) = 'A'">
									<xsl:value-of select="string('00000')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="FormatData">
										<xsl:with-param name="FieldName">LMTS_STD_POL</xsl:with-param>
										<xsl:with-param name="FieldLength">5</xsl:with-param>
										<xsl:with-param name="Value" select="number(CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
										<xsl:with-param name="FieldType">N</xsl:with-param>
										<xsl:with-param name="NumberMask">00000</xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</PMDL4W1__COV__II__LMTS__STD__POL>
					</PMDL4W1__COVERAGE__II__LMTS__STD>
					<PMDL4W1__COVERAGE__II__LMTS__VC>
						<PMDL4W1__COV__II__LMTS__VC__EACH>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">LMTS_VC_EACH</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Value" select="number(CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
								<xsl:with-param name="NumberMask">00000</xsl:with-param>
							</xsl:call-template>
						</PMDL4W1__COV__II__LMTS__VC__EACH>
						<PMDL4W1__COV__II__LMTS__VC__POL>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">LMTS_VC_POL</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Value" select="number(CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
								<xsl:with-param name="NumberMask">00000</xsl:with-param>
							</xsl:call-template>
						</PMDL4W1__COV__II__LMTS__VC__POL>
					</PMDL4W1__COVERAGE__II__LMTS__VC>
					<PMDL4W1__COVERAGE__II__LMTS__FED>
						<PMDL4W1__COV__II__LMTS__FED__EACH>00000</PMDL4W1__COV__II__LMTS__FED__EACH>
						<PMDL4W1__FED__PROGRAM__TYPE/>
					</PMDL4W1__COVERAGE__II__LMTS__FED>
					<PMDL4W1__USLH__COVERAGE__FLAG>N</PMDL4W1__USLH__COVERAGE__FLAG>
					<PMDL4W1__VOL__COMP__COVERAGE__FLAG>N</PMDL4W1__VOL__COMP__COVERAGE__FLAG>
					<PMDL4W1__RETRO__OPTION__ID>N</PMDL4W1__RETRO__OPTION__ID>
					<PMDL4W1__OTHER__ST__COVERAGE__FLAG>D</PMDL4W1__OTHER__ST__COVERAGE__FLAG>
					<PMDL4W1__OTHER__STATES__COVERED__X/>
					<PMDL4W1__STANDARD__PREMIUM>0000000000</PMDL4W1__STANDARD__PREMIUM>
					<PMDL4W1__PREMIUM__DISCOUNT__AMT>000000000</PMDL4W1__PREMIUM__DISCOUNT__AMT>
					<PMDL4W1__EXPENSE__CONSTANT__STATE/>
					<PMDL4W1__EXPENSE__CONSTANT__AMT>0000</PMDL4W1__EXPENSE__CONSTANT__AMT>
					<PMDL4W1__POLICY__MIN__PREM__STATE/>
					<PMDL4W1__POLICY__MIN__PREMIUM>00000</PMDL4W1__POLICY__MIN__PREMIUM>
					<PMDL4W1__POLICY__TERM__PREMIUM>0000000000</PMDL4W1__POLICY__TERM__PREMIUM>
					<PMDL4W1__TAX__ASSESS__CHARGE>0000000000</PMDL4W1__TAX__ASSESS__CHARGE>
					<PMDL4W1__DEPOSIT__PREMIUM__PCT>000000</PMDL4W1__DEPOSIT__PREMIUM__PCT>
					<PMDL4W1__DEP__PREM__PCT__MG__IND>G</PMDL4W1__DEP__PREM__PCT__MG__IND>
					<PMDL4W1__DEPOSIT__PREMIUM>0000000000</PMDL4W1__DEPOSIT__PREMIUM>
					<PMDL4W1__DEP__PREM__MG__IND>G</PMDL4W1__DEP__PREM__MG__IND>
					<PMDL4W1__FEDERAL__COVERAGE__CODE/>
					<PMDL4W1__POL__JULIAN__EFF__DATE>
						<PMDL4W1__POL__JULIAN__EFF__DTE__YY>0000</PMDL4W1__POL__JULIAN__EFF__DTE__YY>
						<PMDL4W1__POL__JULIAN__EFF__DTE__DDD>000</PMDL4W1__POL__JULIAN__EFF__DTE__DDD>
					</PMDL4W1__POL__JULIAN__EFF__DATE>
					<PMDL4W1__PREV__CANCEL__ENTRY__DATE>00000000</PMDL4W1__PREV__CANCEL__ENTRY__DATE>
				</PMDL4W1__SEGMENT__DATA>
				<PMDL4W1__INTERST__ARAP__RATE__1>00000</PMDL4W1__INTERST__ARAP__RATE__1>
				<PMDL4W1__INTERST__ARAP__RATE__2>00000</PMDL4W1__INTERST__ARAP__RATE__2>
				<PMDL4W1__LATEST__COVII__IDS>
					<PMDL4W1__CURR__COVII__STD__ID>B1</PMDL4W1__CURR__COVII__STD__ID>
					<PMDL4W1__CURR__COVII__VOL__ID>B1</PMDL4W1__CURR__COVII__VOL__ID>
					<PMDL4W1__CURR__COVII__ADM__ID/>
				</PMDL4W1__LATEST__COVII__IDS>
				<PMDL4W1__PMS__FUTURE__USE/>
				<PMDL4W1__MMS__CHANGE__BYTE/>
				<PMDL4W1__CUSTOMER__FUTURE__USE/>
				<PMDL4W1__YR2000__CUST__USE/>
			</PMDL4W1__POLICY__RATING__REC>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>