<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template name="BuildSignOn">
		<SignonRs>
			<Status>
				<StatusCd>0</StatusCd>
				<StatusDesc>Success</StatusDesc>
			</Status>
			<SignonRoleCd/>
			<CustId>
				<SPName/>
				<CustLoginId/>
			</CustId>
			<GenSessKey/>
			<ClientDt>2003-04-02T15:56:00</ClientDt>
			<CustLangPref>1</CustLangPref>
			<ClientApp>
				<Org></Org>
				<Name>S2</Name>
				<Version></Version>
			</ClientApp>
			<ServerDt/>
			<Language/>
		</SignonRs>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->