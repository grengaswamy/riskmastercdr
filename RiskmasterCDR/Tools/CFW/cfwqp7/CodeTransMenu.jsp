<%@ page import="java.util.Map" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.cfw.codetrans.CodeTranslator" %>
<%
    String sResetCache = request.getParameter("resetCache");
    if ("true".equals(sResetCache))
        CodeTranslator.resetCachedCodeTables();
    
    Map nativeTransMap = CodeTranslator.getCachedCodesNativeToExternal();
    Map externalTransMap = CodeTranslator.getCachedCodesExternalToNative();
    int nativeTransCnt = nativeTransMap.size();
    int externalTransCnt = externalTransMap.size();
    
%>

<%@page import="java.util.Iterator"%><html>  
<link href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <head>
  	  <title>Code Translation Support Maintenance - Communications Framework</title>
      <meta name="GENERATOR" content="IBM WebSphere Studio"/>
  </head>
  <body BGCOLOR="#C0C0C0">
    <br/>
    <div align="center">
      <H1><u>Code Translation Maintenance</u></H1> 
      <br/>
      <form name="ExeFrm">
        <input name="contrlMaintenance" type="button" value="Control File Maintenance" onclick="location.href='CodeTransControlMaint.jsp'"/>
        <br /><br />
        <input name="generateTable" type="button" value="Generate Code Translation Tables" onclick="location.href='CodeTransGen.jsp'"/>
        <br /><br />
        <input name="resetCache" type="button" value="Reset Code Translation Cache" onclick="location.href='CodeTransMenu.jsp?resetCache=true'"/>
      </form>
      <br /><br />
      <table width="600" border="1">
        <tr>
          <td><b>Code translation file path:</b></td>
          <td><%=CommFwSystem.current().getProperty(CommFwSystem.SYSPROP_CodeTransPath) %></td>
        </tr>
        <tr>
          <td><b>Cached code translation tables:</b></td>
          <td><%= nativeTransCnt %></td>
        </tr>
<%
        if (nativeTransCnt > 0)
        {
            Iterator it = nativeTransMap.keySet().iterator();
%>
        <tr>
          <td><b>Native cached tables:</b></td>
          <td>
            <table border="0">
<%
            while (it.hasNext())
            {
                String tableName = (String)it.next();
                Map tableMap = (Map)nativeTransMap.get(tableName);
                int tableSize = (tableMap != null ? tableMap.size() : 0);
%>
              <tr>
                <td><%=tableName %></td>
                <td><%=tableSize %></td>
              </tr>
<%
            }
%>
            </table>
          </td>
        </tr>
<%

        }
        
        if (externalTransCnt > 0)
        {
            Iterator it = externalTransMap.keySet().iterator();
%>
        <tr>
          <td><b>External cached tables:</b></td>
          <td>
            <table border="0">
<%
            while (it.hasNext())
            {
                String tableName = (String)it.next();
                Map tableMap = (Map)externalTransMap.get(tableName);
                int tableSize = (tableMap != null ? tableMap.size() : 0);
%>
              <tr>
                <td><%=tableName %></td>
                <td><%=tableSize %></td>
              </tr>
<%
            }
%>
            </table>
          </td>
        </tr>
<%

        }
%>
      </table>
      <br/><br/>
    </div>
  </body>
</html>
