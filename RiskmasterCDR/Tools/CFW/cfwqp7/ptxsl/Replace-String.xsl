﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

 <!-- reusable replace-string function -->
 <xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="from"/>
    <xsl:param name="to"/>

    <xsl:choose>
      <xsl:when test="contains($text, $from)">

	<xsl:variable name="before" select="substring-before($text, $from)"/>
	<xsl:variable name="after" select="substring-after($text, $from)"/>
	<xsl:variable name="prefix" select="concat($before, $to)"/>

	<xsl:value-of select="$before"/>
	<xsl:value-of select="$to"/>
        <xsl:call-template name="replace-string">
	  <xsl:with-param name="text" select="$after"/>
	  <xsl:with-param name="from" select="$from"/>
	  <xsl:with-param name="to" select="$to"/>
	</xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
 </xsl:template>

 <!-- test the function -->
 <!--
 <xsl:template match="/">
 	<xsl:variable name="a1">COVERAGE_KEY_LOCATION_COMPANY</xsl:variable>
 	 <xsl:variable name="a2"><xsl:value-of select="substring-after($a1,'COVERAGE_')"/></xsl:variable>
 	 <xsl:variable name="a3">
 	 <xsl:call-template name="replace-string">
        <xsl:with-param name="text"
             select="$a2"/>
        <xsl:with-param name="from" select="'_'"/>
        <xsl:with-param name="to" select="'__'"/>
    </xsl:call-template>
 	 </xsl:variable>
         <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$a3"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text> 		
 </xsl:template>
 -->
</xsl:stylesheet>

