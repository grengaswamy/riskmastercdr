<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template name="cvtdateMMDDYYtoMMDDCCYY">
	<xsl:param name="datefld"/>
	<xsl:choose>
	<xsl:when test="string-length($datefld) = 10">	

		<xsl:variable name="year" select="substring($datefld,7,4)"/>
		<xsl:variable name="mo" select="substring($datefld,1,2)"/>
		<xsl:variable name="day" select="substring($datefld,4,2)"/>
		<xsl:value-of select="concat($mo,$day,$year)"/>
	</xsl:when>
	<xsl:when test="string-length($datefld)> 6 and contains($datefld , '/')">
	
		<xsl:variable name="year" select="substring($datefld,7,2)"/>
		<xsl:variable name="mo" select="substring($datefld,1,2)"/>
		<xsl:variable name="day" select="substring($datefld,4,2)"/>
		<xsl:choose>
			<xsl:when test="($year > 39)"><xsl:value-of select="concat($mo,$day,'19',$year)"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat($mo,$day,'20',$year)"/></xsl:otherwise>
		</xsl:choose>
		
	</xsl:when>
	<!-- This condition is to test whether a date field has 7 digit value .There are cases when date fields are not for display purposes -->
	<xsl:when test="string-length($datefld) = 7">
		<xsl:value-of select="$datefld"/>
	</xsl:when>
	<xsl:when test="$datefld = '00000000'" >
		<xsl:variable name="ISODATE">01010001</xsl:variable>
	<xsl:value-of select="$ISODATE"/>
	</xsl:when>
	<xsl:when test="string-length($datefld) = 8">
		<xsl:value-of select="$datefld"/>
	</xsl:when>
	
	<xsl:otherwise>
	<xsl:variable name="ISODATE">01010001</xsl:variable>
	<xsl:value-of select="$ISODATE"/>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:template>
</xsl:stylesheet>