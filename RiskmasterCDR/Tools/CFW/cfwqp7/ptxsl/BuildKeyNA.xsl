<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--*********************************************************************************************-->
<!--* Programmer : 	Hardik M. Desai           		Date       : 10/16/2006           		    *-->
<!--* FSIT Number: 	92668		    				Resolution : 42389		   					*-->
<!--* Package	 : 	Q00034 - FSIT # 92668 - CLT - Incorrect update of multiple address when the	*-->
<!--				"Add Next Address" button is not clicked.									*-->
<!--* Description: 	Add PROC_LDA_RECORD_IND in the request key for adding additional address.	*-->
<!--*********************************************************************************************-->
<!--* Programmer : 	Vishal Bansal           		Date       : 12/06/2006         *--> 		   
<!--* FSIT Number: 	75542		    				Resolution : 42213	*-->
<!--* Package	 : 	Q00016 - FSIT# 75542 Req 10 - NAICS Code Issue                 *-->
<!--* Description: 	Add CONTRACT__NUMBER in the KEY__FIELDS__ROW .                          *--> 
<!--*********************************************************************************************-->
<!--* Programmer	: Vishal Bansal				Date      : 03/29/2007	       *-->
<!--* FSIT Number	: 91223	       				Resolution Number: 43359       *--> 
<!--* Package Name      : Q00134a-FSIT#91223- Auto Assign Group Number          	       *-->
<!--* Description	: Add PROC__USER__FLAG and PROC__LDA__ISSUE__CODE in the PROCESSING__INFO.*-->
<!--********************************************************************************************-->
<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
<xsl:template name="NAKeyTemplate" >
        						<KEY__FIELDS__ROW>
        						<KEY__CLIENT__SEQUENCE__NUMBER><xsl:value-of select="//KEY_CLIENT_SEQUENCE_NUMBER"/></KEY__CLIENT__SEQUENCE__NUMBER>
        						<KEY__ADDRESS__SEQUENCE__NUMBER><xsl:value-of select="//KEY_ADDRESS_SEQUENCE_NUMBER"/></KEY__ADDRESS__SEQUENCE__NUMBER>
        						<KEY__HISTORY__SEQUENCE__NUMBER><xsl:value-of select="//KEY_HISTORY_SEQUENCE_NUMBER"/></KEY__HISTORY__SEQUENCE__NUMBER>
							<A__CLIENT__NAME><xsl:value-of select="//A_CLIENT_NAME"/></A__CLIENT__NAME>
							<A__NAME__TYPE><xsl:value-of select="//A_NAME_TYPE"/></A__NAME__TYPE>	
        						<A__FIRST__NAME><xsl:value-of select="//A_FIRST_NAME"/></A__FIRST__NAME>
        						<A__CITY><xsl:value-of select="//A_CITY"/></A__CITY>
        						<A__STATE><xsl:value-of select="//A_STATE"/></A__STATE>
        						<A__ZIP__CODE><xsl:value-of select="//A_ZIP_CODE"/></A__ZIP__CODE>
        						<A__DOING__BUSINESS__AS><xsl:value-of select="//A_DOING_BUSINESS_AS"/></A__DOING__BUSINESS__AS>
        						<A__GROUP__NUMBER><xsl:value-of select="//A_GROUP_NUMBER"/></A__GROUP__NUMBER>
<!--FSIT Issue#75542 Resolution#42213 - Start-->
							<A__CONTRACT__NUMBER><xsl:value-of select="//A_CONTRACT_NUMBER"/></A__CONTRACT__NUMBER>
<!--FSIT Issue#75542 Resolution#42213 - End-->
							<A__INDIVIDUAL__SSN><xsl:value-of select="//A_INDIVIDUAL_SSN"/></A__INDIVIDUAL__SSN>
							<A__PHONE__1><xsl:value-of select="//A_PHONE_1"/></A__PHONE__1>				
        						</KEY__FIELDS__ROW>
<!--FSIT Issue#92668 Resolution#42389 - Start-->
        						<PROCESSING__INFO>
        							<PROC__LDA__RECORD__IND><xsl:value-of select="//PROC_LDA_RECORD_IND"/></PROC__LDA__RECORD__IND>
<!-- FSIT# 91223 Resolution # 43359 -Start-->
								<PROC__USER__FLAG><xsl:value-of select="//PROC_USER_FLAG"/></PROC__USER__FLAG>
								<PROC__LDA__ISSUE__CODE><xsl:value-of select="//PROC_LDA_ISSUE_CODE"/></PROC__LDA__ISSUE__CODE>
<!--FSIT# 91223 Resolution # 43359-End-->
        						</PROCESSING__INFO>
<!--FSIT Issue#92668 Resolution#42389 - End-->        						
	 </xsl:template>
</xsl:stylesheet>