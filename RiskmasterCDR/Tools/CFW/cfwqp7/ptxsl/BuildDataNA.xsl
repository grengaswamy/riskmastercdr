<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--*********************************************************************************************-->
<!--* Programmer : 	Ramnik Singh           		Date       : 11/15/2006           		    *-->
<!--* FSIT Number: 	75542	    		        Resolution : 42213	                            *-->
<!--* Package	 : 	Q00034 - FSIT # 75542 -NAICS Code - Need to capture this code at client level.      *-->
<!--*			 Need multi seq #'s to capture changes in descriptio.                               *-->
<!--* Description: 	NAICS Code - Need to capture this code at client level.                    	    *-->
<!--*              	Need multi seq #'s to capture changes in descriptio.                    	    *-->
<!--*********************************************************************************************-->
<!--* Programmer : 	Akash Agrawal          		Date       : 03/06/2007           		    *-->
<!--* FSIT Number: 	100049	    		        Resolution : 43304	                            *-->
<!--* Package	 : 	Q00129 - FSIT#100049 - RCV - System Abend when trying to delete a Bank Number from  *-->
<!--*                   the Customer Information screen.                                                    *-->
<!--* Description: 	Added a check that if C_EFT_TRANSITROUTING_NUMBER contains value '0' then convert   *--> 
<!--*                   it to '00000000' because it is showing a decimal data error in backend.             *-->
<!--*********************************************************************************************************-->
<!--*********************************************************************************************-->
<!--* Programmer : 	Ajay R. C.           		Date       : 03/09/2007           			*-->
<!--* FSIT Number: 	91223	    		        Resolution :	43359					*-->
<!--* Package	 : 	Q00134 - FSIT#91223 -WNI Req 135a -  Auto Assign Group Number		*-->
<!--*	Description	 :	If user selected  Individual Group and hence the related Indicator is		*-->
<!--*				turned ON (set to 'I')										*-->
<!--*				Change NAME TYPE value to G(Group) from value "A"(Individual Group)		*-->					
<!--*********************************************************************************************-->

 <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
 <xsl:template  name="BuildData" match="//PayLoad">
 
		<KEY__FIELDS__ROW>
			 <xsl:for-each select="//PayLoad/*[starts-with(name(),'KEY_')]">
				<xsl:variable name="finnodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</KEY__FIELDS__ROW>

		<NAME__DETAILS>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'A_') or starts-with(name(),'AI_')]">

							<xsl:choose>
    				    			<xsl:when test ="name() = 'A_PHONE_1'">
    				    			<xsl:variable name="Workphone" select="//PayLoad/A_PHONE_1"/>
       	    	 	 				<xsl:variable name="Workextension" select="//PayLoad/AB_PHONE_1_EXT"/>
       	    						<A__PHONE__1><xsl:value-of select="concat($Workphone,' ',$Workextension)"/></A__PHONE__1>
   				    		        </xsl:when>
   				    		<xsl:when test ="name() = 'AI_PHONE_1'">
    				    			<xsl:variable name="Contactphone" select="//PayLoad/AI_PHONE_1"/>
       	    	 	 				<xsl:variable name="Contactextension" select="//PayLoad/AB_PHONE_2_EXT"/>
       	    						<AI__PHONE__1><xsl:value-of select="concat($Contactphone,' ',$Contactextension)"/></AI__PHONE__1>
   				    		        </xsl:when>
							<!-- FSIT#91223  RESO#43359 START-->
							<xsl:when test ="(name() = 'A_NAME_TYPE' and //PayLoad/BAS1_INDICATOR_01 ='I' )">
							<A__NAME__TYPE>G</A__NAME__TYPE>			
							</xsl:when>
							<!-- FSIT#91223  RESO#43359 END-->
   				    		        

							<xsl:otherwise>

				<xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:choose><xsl:when test="contains(name(),'DATE')"><xsl:call-template name="cvtdateMMDDYYtoMMDDCCYY"><xsl:with-param name="datefld" select="."/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="."/></xsl:otherwise></xsl:choose><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>

							</xsl:otherwise>
							</xsl:choose>
			</xsl:for-each>
		</NAME__DETAILS>

		<CUSTOMER__INFO__ATTRIBUTES>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'C_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
			<!-- FSIT # 100049 Resolution # 43304 Begin -->
				 <xsl:choose>
					<xsl:when test = "(name() = 'C_EFT_TRANSITROUTING_NUMBER') and (..//C_EFT_TRANSITROUTING_NUMBER ='0')">
						<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>00000000<xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
			<!-- FSIT # 100049 Resolution # 43304 End -->
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			<!-- FSIT # 100049 Resolution # 43304 Begin -->
				    </xsl:otherwise>
				 </xsl:choose>
			<!-- FSIT # 100049 Resolution # 43304 End -->
				 </xsl:for-each>
		</CUSTOMER__INFO__ATTRIBUTES>

		<CUSTOMER__INFORMATION>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'CI_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</CUSTOMER__INFORMATION>

		<ADDITIONAL__ADDRESS>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'AA_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</ADDITIONAL__ADDRESS>
 		
			

<!-- FSIT # 75542 Resolution # 42213 Begin --> 			
		<NAICS__INFO__RECORD>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'NAICS_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</NAICS__INFO__RECORD>
			
		<BAS__FIELDS__RECORD__0100>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'BAS1_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</BAS__FIELDS__RECORD__0100>
		
		<BAS__FIELDS__RECORD__0900>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'BAS9_')]">
				 <xsl:variable name="finnodename"> <xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'_'"/><xsl:with-param name="to" select="'__'"/></xsl:call-template></xsl:variable>
				 <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$finnodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			 </xsl:for-each>
		</BAS__FIELDS__RECORD__0900>
<!-- FSIT # 75542 Resolution # 42213 End -->

		
		
		<PROCESSING__INFO>
			<xsl:for-each select="//PayLoad/*[starts-with(name(),'PROC_')]">
					<xsl:if test="not(//PROC_SECURITY_INDIC)">
					<xsl:variable name="finnodename">
						<xsl:call-template name="replace-string">
							<xsl:with-param name="text" select="name()"/>
							<xsl:with-param name="from" select="'_'"/>
							<xsl:with-param name="to" select="'__'"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
					<xsl:value-of select="$finnodename"/>
					<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
					<xsl:value-of select="."/>
					<xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
					<xsl:value-of select="$finnodename"/>
					<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
					</xsl:if>
			</xsl:for-each>
		</PROCESSING__INFO>
	</xsl:template>    			  	    				
</xsl:stylesheet>