<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<!--*********************************************************************************************-->
<!--* Programmer	: Vishal Bansal				Date      : 03/29/2007	       *-->
<!--* FSIT Number	: 91223	       				Resolution Number: 43359       *--> 
<!--* Package Name      : Q00134a-FSIT#91223- Auto Assign Group Number          	       *-->
<!--* Description	: Added the request code BASNAMEACPGRq for copy Group Client functionality*-->
<!--********************************************************************************************-->
     <xsl:include href="BuildDataNA.xsl"/>
    <xsl:include href="BuildKeyNA.xsl"/>
    <xsl:include href="Replace-String.xsl"/>
    <xsl:include href="ConvertDateMMDDYYtoMMDDCCYY.xsl"/>
    <xsl:output  method="xml" indent="yes"/>
	<xsl:template match="/">
		<BASNAMEARq>
		<xsl:call-template name="test">
		</xsl:call-template>
		<xsl:choose>
		 <xsl:when test="(//RqUID='BASNAMEAINQRq')">
            <xsl:call-template name="BuildNAKey">
                <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
            </xsl:call-template>
            </xsl:when>
            <xsl:when test="(//RqUID='BASNAMEADFTRq')">
            <xsl:call-template name="BuildNAKey">
            <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
            </xsl:call-template>
            </xsl:when>
            <xsl:when test="(//RqUID='BASNAMEACHGRq')">
            <xsl:call-template name="BuildNAData">
            <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
             </xsl:call-template>
            </xsl:when>
            <xsl:when test="(//RqUID='BASNAMEAADDRq')">
             <xsl:call-template name="BuildNAData">
            <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
            </xsl:call-template>
            </xsl:when>
			<xsl:when test="(//RqUID='BASNAMEADLTRq')">
             <xsl:call-template name="BuildNAData">
            <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
            </xsl:call-template>
            </xsl:when>
	    <!-- FSIT# 91223 Resolution # 43359 -Start-->
            <xsl:when test="(//RqUID='BASNAMEACPGRq')">
            <xsl:call-template name="BuildNAKey">
            <xsl:with-param name="TableName">PAYLOAD</xsl:with-param>
            </xsl:call-template>
            </xsl:when>
            <!-- FSIT# 91223 Resolution # 43359 -End-->
          </xsl:choose>
 		</BASNAMEARq>
		</xsl:template>

	<xsl:template name= "test">
	<BUS__OBJ__RECORD>
	<RECORD__NAME__ROW>
			<RECORD__NAME>action</RECORD__NAME>
    </RECORD__NAME__ROW>
    <ACTION__RECORD__ROW>
        	<ACTION><xsl:value-of select="//RqUID"/></ACTION>
         </ACTION__RECORD__ROW>
    	</BUS__OBJ__RECORD>
  	</xsl:template>		

    <xsl:template name="BuildNAKey">
       <xsl:param name="TableName"/>
    	<BUS__OBJ__RECORD>
    		<RECORD__NAME__ROW>
    			<RECORD__NAME>NameAddres</RECORD__NAME>
    		</RECORD__NAME__ROW>
        	<NA__FIELDS__RECORDS__ROW>
		<xsl:choose>
			<xsl:when test="$TableName='PAYLOAD'">
				<xsl:call-template name="NAKeyTemplate"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
            </NA__FIELDS__RECORDS__ROW>>
    	</BUS__OBJ__RECORD>
   </xsl:template>

<xsl:template name="BuildNAData">
        <xsl:param name="TableName"/>
    	<BUS__OBJ__RECORD>
    		<RECORD__NAME__ROW>
    			<RECORD__NAME>NameAddres</RECORD__NAME>
    		</RECORD__NAME__ROW>
        	<NA__FIELDS__RECORDS__ROW>
		<xsl:choose>
			<xsl:when test="$TableName='PAYLOAD'">
				<xsl:apply-templates select="//PayLoad"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
            </NA__FIELDS__RECORDS__ROW>>
    	</BUS__OBJ__RECORD>

	</xsl:template>
   


</xsl:stylesheet>