<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
	<xsl:template name="AcordToACCGL" match="/">
		<xsl:variable name="PolicyNumber" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyNumber"/>
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
		<CLAIM ID="CLM1" EVENT="EVT1">
			<CLAIM_NUMBER>GENERATE</CLAIM_NUMBER>
			<CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
			<FILING_STATE>
					<xsl:attribute name="CODE"><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ControllingStateProvCd"/></xsl:attribute>
			</FILING_STATE>
			<xsl:variable name="LossDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
			<DATE_OF_LOSS>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($LossDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($LossDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($LossDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossDt"/>
			</DATE_OF_LOSS>
			<xsl:variable name="LossTime" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossTime"/>
			<TIME_OF_LOSS>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($LossTime,1,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($LossTime,4,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($LossTime,7,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossTime"/>
			</TIME_OF_LOSS>
			<xsl:variable name="RptDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportedDt"/>
			<DATE_REPORTED>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($RptDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($RptDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($RptDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,1,10)"/>
			</DATE_REPORTED>
			<TIME_REPORTED>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($RptDt,12,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($RptDt,15,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($RptDt,18,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,12,8)"/>
			</TIME_REPORTED>
			<AUTHORITY_CONTACTED>
				<AUTHORITY>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ResponsibleDept"/>
				</AUTHORITY>
				<REPORT_NUMBER>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportNumber"/>
				</REPORT_NUMBER>
			</AUTHORITY_CONTACTED>
			<PREVIOUSLY_REPORTED>
				<xsl:attribute name="INDICATOR">
					<xsl:if test="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd='1'">
						<xsl:text>Yes</xsl:text>
					</xsl:if>
					<xsl:if test="not(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd ='1')">
						<xsl:text>No</xsl:text>
					</xsl:if>
				</xsl:attribute>
			</PREVIOUSLY_REPORTED>
			<ACCIDENT_DESCRIPTION CODE=""><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/IncidentDesc"/></ACCIDENT_DESCRIPTION>
			<POLICY>
				<POLICY_NUMBER>
					<xsl:attribute name="SYMBOL">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/CompanyProductCd"></xsl:value-of>
					</xsl:attribute>
					<xsl:attribute name="LOCATION">00</xsl:attribute>
					<xsl:attribute name="MODULE">
					    <xsl:choose>
						<xsl:when test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion)&gt;0">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion"/>
						</xsl:when>
						<xsl:otherwise>00</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="$PolicyNumber"/>
				</POLICY_NUMBER>
				<MASTER_COMPANY>
					<xsl:attribute name="CODE">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/NAICCd"></xsl:value-of>
					</xsl:attribute>
				</MASTER_COMPANY>
				<LINE_OF_BUSINESS>
<!--Unitrin changed to CPP		<xsl:attribute name="CODE">GL</xsl:attribute></LINE_OF_BUSINESS>-->
					<xsl:attribute name="CODE">CPP</xsl:attribute></LINE_OF_BUSINESS>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt)>1">
				<EFFECTIVE_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,9,2)"/>
					</xsl:attribute>
				</EFFECTIVE_DATE>
				</xsl:if>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt)>1">
				<EXPIRATION_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,9,2)"/>
					</xsl:attribute>
				</EXPIRATION_DATE>
				</xsl:if>
				<!-- Commented as not to load the unit as insured unit
				<INSURED_UNIT>
					<UNIT_REFERENCE ID="UNT1"></UNIT_REFERENCE>
				</INSURED_UNIT>-->
				<xsl:for-each select="$EntityNode">
					<xsl:choose>
						<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Insured'">
							<PARTY_INVOLVED CODE="PHLD">
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
								<ROLE>PolicyHolder</ROLE>
							</PARTY_INVOLVED>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</POLICY>
            <!--Begin 05/22/07   Fix problem with PROPERTY NODE created when no PRO party role code--> 
			<xsl:for-each select="$EntityNode">
			   <xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='PRO'">
                    <xsl:variable name="NameId" select="@id"></xsl:variable>
				<PROPERTY_LOSS>
				<UNIT_REFERENCE>
					<xsl:attribute name="ID">
						<xsl:value-of select="concat('UNT',substring($NameId,3,1))"/>
					</xsl:attribute>
				</UNIT_REFERENCE>
					<LOSS_INFORMATION>
						<ESTIMATED_DAMAGE_AMOUNT>
							<xsl:value-of select="ClaimsPartyInfo/com.csc_EstimatedLossAmt/Amt"/>
						</ESTIMATED_DAMAGE_AMOUNT>
						<!--Begin 05/30/07 Adding Where and When Vehicle Can Be Seen tags -->
						<xsl:if test="(string-length(ClaimsPartyInfo/com.csc_ContactWhereDesc)>0) or (string-length(ClaimsPartyInfo/com.csc_ContactWhen/com.csc_ContactWhenDesc)>0)">
						<CAN_BE_SEEN>
						 <WHERE>
						     <xsl:value-of select="concat(ClaimsPartyInfo/com.csc_ContactWhereDesc,' ', ClaimsPartyInfo/com.csc_ContactWhen/com.csc_ContactWhenDesc)"/>
						 </WHERE>
						</CAN_BE_SEEN>
						</xsl:if>	
						<!--End 05/30/07 Adding Where and When Vehicle Can Be Seen tags -->
					</LOSS_INFORMATION>
				</PROPERTY_LOSS>
		       		 </xsl:when>
			   </xsl:choose>
			</xsl:for-each>
			<!--End 05/22/07   Fix problem with INJURY NODE created when no CLM party role code--> 
            <!--Begin 05/21/07   Fix problem with INJURY NODE created when no CLM party role code--> 
			<xsl:for-each select="$EntityNode">
			   <xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CLM'">
			<INJURY>
				<xsl:attribute name="ID">
					<xsl:value-of select="concat('INJ',position())"/>
				</xsl:attribute>
				<INJURED_PARTY>
					<xsl:attribute name="ID">
						<xsl:value-of select="concat('ENT',position())"/>
					</xsl:attribute>
				</INJURED_PARTY >
				<WORK_ACTIVITY>
					<xsl:value-of select="ClaimsInjuredInfo/ClaimsInjury/LossCauseDesc"/>
				</WORK_ACTIVITY>
				<NATURE_OF_INJURY>
					<xsl:attribute name="CODE"><xsl:value-of select="ClaimsInjuredInfo/ClaimsInjury/InjuryNatureCd"/></xsl:attribute>
				</NATURE_OF_INJURY>
				<MEDICATION>
					<xsl:value-of select="ClaimsInjuredInfo/ClaimsInjury/InjuryNatureDesc"/>
				</MEDICATION>
				<xsl:variable name="injDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
				<xsl:if test=" string-length($injDt)>0">
				<DATE_OF_INJURY>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring($injDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring($injDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring($injDt,9,2)"/>
					</xsl:attribute>
					<xsl:value-of select="$injDt"/>
				</DATE_OF_INJURY>
				</xsl:if>
				<TIME_OF_INJURY>
					<xsl:attribute name="HOUR">
						<xsl:value-of select="substring($LossTime,1,2)"/>
					</xsl:attribute>
					<xsl:attribute name="MINUTE">
						<xsl:value-of select="substring($LossTime,4,2)"/>
					</xsl:attribute>
					<xsl:attribute name="SECOND">
						<xsl:value-of select="substring($LossTime,7,2)"/>
					</xsl:attribute>
					<xsl:value-of select="$LossTime"/>
				</TIME_OF_INJURY>
				<!--Begin 05/22/07   Fix problem with INJURY NODE created when no CLM party role code-->   				
				<PARTY_INVOLVED CODE="INJ">
					<xsl:attribute name="ID">
						<xsl:value-of select="concat('ENT',position())"/>
					</xsl:attribute>
					<ROLE>Injured</ROLE>
				</PARTY_INVOLVED>
			    <!--End 05/22/07   Fix problem with INJURY NODE created when no CLM party role code--> 
			</INJURY>
			<!--Begin 05/21/07   Fix problem with INJURY NODE created when no CLM party role code--> 
		       		</xsl:when>
			   </xsl:choose>
			</xsl:for-each>
			<!--End 05/21/07   Fix problem with INJURY NODE created when no CLM party role code--> 
			<xsl:for-each select="$EntityNode">
				<xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CC'">
						<PARTY_INVOLVED CODE="CNT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Contact</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='OT'">
						<PARTY_INVOLVED CODE="OTH">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Other</TYPE>
						</PARTY_INVOLVED>
 					</xsl:when>
       				<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employer'">
						<PARTY_INVOLVED CODE="EMR">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>EMPLOYER</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
      				<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='WIT'">
						<PARTY_INVOLVED CODE="WT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>WITNESS</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
            <!--Begin 05/22/07   Fix problem with PROPERTY NODE created when no PRO party role code--> 
			<xsl:variable name="LossCount" select="count($EntityNode/ClaimsPartyInfo[ClaimsPartyRoleCd='PRO'])"/>
			<xsl:if test="$LossCount>0">
			<INVOLVED_UNITS>
	  			<xsl:attribute name="COUNT">
					<xsl:value-of select="$LossCount"></xsl:value-of>
				</xsl:attribute>
			<xsl:for-each select="$EntityNode">
			   <xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='PRO'">
                    <xsl:variable name="NameId" select="@id"></xsl:variable>
				<UNIT UNIT_TYPE="L">
					<xsl:attribute name="ID">
						<xsl:value-of select="concat('UNT',substring($NameId,3,1))"/>
					</xsl:attribute>
					<DESCRIPTION>
						<xsl:value-of select="ClaimsPartyInfo/InvolvementDesc"/>
					</DESCRIPTION>
					<!--<ESTIMATED_DAMAGE_AMOUNT>
						<xsl:value-of select="ClaimsPartyInfo/com.csc_EstimatedLossAmt/Amt"/>
					</ESTIMATED_DAMAGE_AMOUNT>-->
					<PARTY_INVOLVED CODE="OWN">
						<xsl:attribute name="ID">
							<xsl:value-of select="concat('ENT',position())"/>
						</xsl:attribute>
						<TYPE>Owner</TYPE>
					</PARTY_INVOLVED>
				</UNIT>
	       		 </xsl:when>
			   </xsl:choose>
			</xsl:for-each>
			</INVOLVED_UNITS>
			</xsl:if>
			<!--End 05/22/07   Fix problem with INJURY NODE created when no CLM party role code--> 
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENT_REFERENCE ID="CMT01"/>
			</xsl:if>
		</CLAIM>
				
		
		<xsl:call-template name="AddInvolvedEntity"></xsl:call-template>
		<xsl:call-template name="AddEntityAddress"></xsl:call-template>
		<xsl:call-template name="AddCGLComments"></xsl:call-template>
			
	</xsl:template>
	
		
	<xsl:template name="AddCGLComments">
		<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
		<COMMENTS COUNT="01" NEXT_ID="02">
			<COMMENT ID="CMT01">
			<CATEGORY TYPE="N">FNOL General Comment</CATEGORY>
			<SUBJECT>Remark</SUBJECT>
			<AUTHOR USER_ID="UBIAgent"></AUTHOR>
	         <TEXT><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt"/></TEXT>
			</COMMENT>
		</COMMENTS>
		</xsl:if>
	</xsl:template>
<!--End 05/30/07   Modifying Comments logic--> 		
	
</xsl:stylesheet>