<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
	<xsl:template name="AcordToACWork" match="/">
		<xsl:variable name="PolicyNumber" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyNumber"/>
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
		<CLAIM ID="CLM17" EVENT="EVT1">
			<CLAIM_NUMBER>GENERATE</CLAIM_NUMBER>
			<LINE_OF_BUSINESS>
				<xsl:attribute name="CODE">WCV</xsl:attribute>
			</LINE_OF_BUSINESS>
			<CLAIM_TYPE>
				<xsl:attribute name="CODE">DWC</xsl:attribute>
			</CLAIM_TYPE>
			<ACCIDENT_STATE>
				<xsl:attribute name="CODE"><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ControllingStateProvCd"/></xsl:attribute>
			</ACCIDENT_STATE>
			<CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
			<xsl:variable name="LossDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
			<DATE_OF_LOSS>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($LossDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($LossDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($LossDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossDt"/>
			</DATE_OF_LOSS>
			<xsl:variable name="LossTime" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossTime"/>
			<TIME_OF_LOSS>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($LossTime,1,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($LossTime,4,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($LossTime,7,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossTime"/>
			</TIME_OF_LOSS>
			<xsl:variable name="RptDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportedDt"/>
			<DATE_REPORTED>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($RptDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($RptDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($RptDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,1,10)"/>
			</DATE_REPORTED>
			<TIME_REPORTED>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($RptDt,12,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($RptDt,15,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($RptDt,18,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,12,8)"/>
			</TIME_REPORTED>
			<AUTHORITY_CONTACTED>
				<AUTHORITY>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ResponsibleDept"/>
				</AUTHORITY>
				<REPORT_NUMBER>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportNumber"/>
				</REPORT_NUMBER>
			</AUTHORITY_CONTACTED>
			<PREVIOUSLY_REPORTED>
				<xsl:attribute name="INDICATOR">
					<xsl:if test="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd='1'">
						<xsl:text>Yes</xsl:text>
					</xsl:if>
					<xsl:if test="not(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd ='1')">
						<xsl:text>No</xsl:text>
					</xsl:if>
				</xsl:attribute>
			</PREVIOUSLY_REPORTED>
			<DESCRIPTION>
			<xsl:value-of select="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/InjuryNatureDesc"/>
			</DESCRIPTION>
			<ACCIDENT_DESCRIPTION CODE=""><xsl:value-of select="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/InjuryNatureDesc"/></ACCIDENT_DESCRIPTION>
			<POLICY>
			<POLICY_NUMBER>
					<xsl:attribute name="SYMBOL">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/CompanyProductCd"></xsl:value-of>
					</xsl:attribute>
					<xsl:attribute name="LOCATION">00</xsl:attribute>
					<xsl:attribute name="MODULE">
					    <xsl:choose>
						<xsl:when test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion)&gt;0">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion"/>
						</xsl:when>
						<xsl:otherwise>00</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="$PolicyNumber"/>
				</POLICY_NUMBER>
				<MASTER_COMPANY>
					<xsl:attribute name="CODE">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/NAICCd"></xsl:value-of>
					</xsl:attribute>
				</MASTER_COMPANY>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt)>1">
				<EFFECTIVE_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,9,2)"/>
					</xsl:attribute>
				</EFFECTIVE_DATE>
				</xsl:if>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt)>1">
				<EXPIRATION_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,9,2)"/>
					</xsl:attribute>
				</EXPIRATION_DATE>
				</xsl:if>
				<xsl:for-each select="$EntityNode">
					<xsl:choose>
						<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employer'">
							<PARTY_INVOLVED CODE="PHLD">
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
								<ROLE>PolicyHolder</ROLE>
							</PARTY_INVOLVED>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</POLICY>
			<xsl:variable name="CompInfo" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/WorkCompLossInfo"/>	
			<UNIT_STAT_DCI>
					<!--Begin 06/11/07   Adding DATE EMPLOYER NOTIFIED--> 		
					<xsl:variable name="DtEmployerNotified" select="$CompInfo/EmployerInfo/com.csc_NotificationDt"/>
					<xsl:if test=" string-length($DtEmployerNotified)>0">
					<DATE_EMPLOYER_NOTIFIED>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="substring($DtEmployerNotified,1,4)"/>
						</xsl:attribute>
						<xsl:attribute name="MONTH">
							<xsl:value-of select="substring($DtEmployerNotified,6,2)"/>
						</xsl:attribute>
						<xsl:attribute name="DAY">
							<xsl:value-of select="substring($DtEmployerNotified,9,2)"/>
						</xsl:attribute>
						<xsl:value-of select="$DtEmployerNotified"/>
					</DATE_EMPLOYER_NOTIFIED>
					</xsl:if>
					<!--End 06/07/07   Adding DATE EMPLOYER NOTIFIED--> 		
				<EMPLOYEE CLAIMANT_NUMBER="1">
				<xsl:for-each select="$EntityNode">
				<xsl:choose>
						<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employee'">
						<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
					<EMPLOYMENT_STATUS>
						<xsl:attribute name="CODE">
							<xsl:choose>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='REG'">
									<xsl:text>01</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='TIME'">
									<xsl:text>02</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='UNEM'">
									<xsl:text>03</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='com.csc_DISA'">
									<xsl:text>05</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='RET'">
									<xsl:text>06</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='OT'">
									<xsl:text>07</xsl:text>
								</xsl:when>
								<xsl:when test="$CompInfo/EmployeeInfo/EmploymentStatusCd ='STRI'">
									<xsl:text>08</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:attribute>
					</EMPLOYMENT_STATUS>
					<!--Begin 06/07/07   Adding Date Hired--> 		
					<xsl:variable name="DtHired" select="$CompInfo/EmployeeInfo/HiredDt"/>
					<xsl:if test=" string-length($DtHired)>0">
					<DATE_HIRED>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="substring($DtHired,1,4)"/>
						</xsl:attribute>
						<xsl:attribute name="MONTH">
							<xsl:value-of select="substring($DtHired,6,2)"/>
						</xsl:attribute>
						<xsl:attribute name="DAY">
							<xsl:value-of select="substring($DtHired,9,2)"/>
						</xsl:attribute>
						<xsl:value-of select="$DtHired"/>
					</DATE_HIRED>
					</xsl:if>
					<!--End 06/07/07   Adding Date Hired--> 		
					<LOCATION_DESCRIPTION>
						<xsl:value-of select="$CompInfo/EmployeeInfo/RegularDept"/>
					</LOCATION_DESCRIPTION>
					<PAY>
					<!--Begin 06/18/07   Adding PayFrequencyCd--> 		
					<xsl:if test="string-length($CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd)>0">					
						<xsl:attribute name="TYPE">
								<xsl:choose>
									<xsl:when test="$CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd ='B'">
										<xsl:text>BW</xsl:text>
									</xsl:when>
									<xsl:when test="$CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd ='C'">
										<xsl:text>PCM</xsl:text>
									</xsl:when>
									<xsl:when test="$CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd ='M'">
										<xsl:text>MO</xsl:text>
									</xsl:when>
									<xsl:when test="$CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd ='O'">
										<xsl:text>OT</xsl:text>
									</xsl:when>
									<xsl:when test="$CompInfo/EmployeeInfo/EmployeePay/PayFrequencyCd ='W'">
										<xsl:text>PW</xsl:text>
									</xsl:when>
								</xsl:choose>
						</xsl:attribute>
					</xsl:if>						
					<!--Begin 06/18/07   Adding PayFrequencyCd--> 		
										
						<!--Begin 06/15/07   Adding FullPayDayInjuredInd--> 		
						<xsl:attribute name="FULL_PAY_DAY_INJURED">
							<xsl:if test="$CompInfo/EmployeeInfo/FullPayDayInjuredInd='1'">
								<xsl:text>Yes</xsl:text>
							</xsl:if>
							<xsl:if test="not($CompInfo/EmployeeInfo/FullPayDayInjuredInd='1')">
								<xsl:text>No</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<!--End 06/15/07   Adding FullPayDayInjuredInd--> 		
						<!--Begin 06/15/07   Adding SalaryContinuanceInd--> 		
						<xsl:attribute name="CONTINUED">
							<xsl:if test="$CompInfo/EmployeeInfo/SalaryContinuanceInd='1'">
								<xsl:text>Yes</xsl:text>
							</xsl:if>
							<xsl:if test="not($CompInfo/EmployeeInfo/SalaryContinuanceInd='1')">
								<xsl:text>No</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<!--End 06/15/07   Adding SalaryContinuanceInd--> 	
						<xsl:value-of select="$CompInfo/EmployeeInfo/EmployeePay/AvgAmt/Amt"/>	
					</PAY>
					<!--Begin 06/19/07   Adding Start Time--> 	
					<WEEKLY_SCHEDULE SPECIFIED="PREINJURY">
						<DAY> 
						<xsl:if test="$CompInfo/EmployeeInfo/WorkBeganTime">
							<xsl:variable name="WorkBeganTime" select="$CompInfo/EmployeeInfo/WorkBeganTime"/>
								<SHIFT_START_TIME>
									<xsl:attribute name="HOUR">
										<xsl:value-of select="substring($WorkBeganTime,1,2)"/>
									</xsl:attribute>
									<xsl:attribute name="MINUTE">
										<xsl:value-of select="substring($WorkBeganTime,4,2)"/>
									</xsl:attribute>
									<xsl:attribute name="SECOND">
										<xsl:value-of select="substring($WorkBeganTime,7,2)"/>
									</xsl:attribute>
									<xsl:value-of select="$WorkBeganTime"/>
								</SHIFT_START_TIME>
						</xsl:if>						
						</DAY>
					</WEEKLY_SCHEDULE>
					<!--End 06/19/07   Adding Start Time--> 	
					
					<xsl:variable name="LastDt" select="$CompInfo/EmployeeInfo/com.csc_LastWorkDt"/>
					<xsl:if test=" string-length($LastDt)>0">
					<!--Begin 06/11/07   Modify incorrect tag--> 		
					<LAST_DAY_WORKED>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="substring($LastDt,1,4)"/>
						</xsl:attribute>
						<xsl:attribute name="MONTH">
							<xsl:value-of select="substring($LastDt,6,2)"/>
						</xsl:attribute>
						<xsl:attribute name="DAY">
							<xsl:value-of select="substring($LastDt,9,2)"/>
						</xsl:attribute>
						<xsl:value-of select="$LastDt"/>
					</LAST_DAY_WORKED>
					<!-- End 06/11/07   Modify incorrect tag--> 		
					</xsl:if>
					<xsl:variable name="retDt" select="$CompInfo/EmployeeInfo/com.csc_ReturnToWorkDt"/>
					<xsl:if test=" string-length($retDt)>0">
					<DATE_RETURNED_TO_WORK>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="substring($retDt,1,4)"/>
						</xsl:attribute>
						<xsl:attribute name="MONTH">
							<xsl:value-of select="substring($retDt,6,2)"/>
						</xsl:attribute>
						<xsl:attribute name="DAY">
							<xsl:value-of select="substring($retDt,9,2)"/>
						</xsl:attribute>
						<xsl:value-of select="$retDt"/>
					</DATE_RETURNED_TO_WORK>
					</xsl:if>
				</EMPLOYEE>
			</UNIT_STAT_DCI>
			<INJURY ID="INJ1">
				<INJURED_PARTY>
					<xsl:attribute name="ID">
						<xsl:for-each select="$EntityNode">
							<xsl:if test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employee'">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:if>
						</xsl:for-each>
					</xsl:attribute>
				</INJURED_PARTY>
				<SPECIFICS>
					<xsl:attribute name="ON_PREMISE_IND">
						<xsl:value-of select="$CompInfo/OccurredPremisesCd='1'"/>
					</xsl:attribute>

					<!--Begin 07/03/07  --> 
					<xsl:if test="string-length($CompInfo/EmployeeInfo/SafeguardsUsedInd)>0">
						<xsl:choose>
							<xsl:when test="$CompInfo/EmployeeInfo/SafeguardsUsedInd ='1'">
								<xsl:attribute name="SAFETY_EQP_USED">
									<xsl:text>Yes</xsl:text>
								</xsl:attribute>
							</xsl:when>								
							<xsl:when test="not($CompInfo/EmployeeInfo/SafeguardsUsedInd ='1')">
								<xsl:attribute name="SAFETY_EQP_USED">
									<xsl:text>No</xsl:text>
								</xsl:attribute>
							</xsl:when>		
						</xsl:choose>							
					</xsl:if>
					
					<!--Begin 07/03/07  --> 
					<xsl:if test="string-length($CompInfo/EmployeeInfo/SafeguardsProvidedInd)>0">
						<xsl:choose>
							<xsl:when test="$CompInfo/EmployeeInfo/SafeguardsProvidedInd ='1'">
								<xsl:attribute name="SAFETY_EQP_AVAIL">
									<xsl:text>Yes</xsl:text>
								</xsl:attribute>
							</xsl:when>								
							<xsl:when test="not($CompInfo/EmployeeInfo/SafeguardsProvidedInd ='1')">
								<xsl:attribute name="SAFETY_EQP_AVAIL">
									<xsl:text>No</xsl:text>
								</xsl:attribute>
							</xsl:when>		
						</xsl:choose>							
					</xsl:if>
				</SPECIFICS>
				<!--Begin 07/03/07  --> 				
				<xsl:if test="string-length($EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/LossCauseDesc)>0">				
					<WORK_ACTIVITY>
						<xsl:value-of select="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/LossCauseDesc"/>
					</WORK_ACTIVITY>
				</xsl:if>
				<!--End 07/03/07  --> 				
				<NATURE_OF_INJURY>
					<xsl:attribute name="CODE"><xsl:value-of select="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/InjuryNatureCd"/></xsl:attribute>
				<xsl:value-of select="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/InjuryNatureCd"/>
				</NATURE_OF_INJURY>
				<xsl:variable name="injDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
				<xsl:if test=" string-length($injDt)>0">
				<DATE_OF_INJURY>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring($injDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring($injDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring($injDt,9,2)"/>
					</xsl:attribute>
					<xsl:value-of select="$injDt"/>
				</DATE_OF_INJURY>
				</xsl:if>
				<TIME_OF_INJURY>
					<xsl:attribute name="HOUR">
						<xsl:value-of select="substring($LossTime,1,2)"/>
					</xsl:attribute>
					<xsl:attribute name="MINUTE">
						<xsl:value-of select="substring($LossTime,4,2)"/>
					</xsl:attribute>
					<xsl:attribute name="SECOND">
						<xsl:value-of select="substring($LossTime,7,2)"/>
					</xsl:attribute>
					<xsl:value-of select="$LossTime"/>
				</TIME_OF_INJURY>
				<xsl:variable name="disDt" select="$CompInfo/EmployeeInfo/com.csc_DisabilityDt"/>
				<xsl:if test="string-length($disDt)>0">
				<DATE_DISABILITY_BEGAN>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring($disDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring($disDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring($disDt,9,2)"/>
					</xsl:attribute>
					<xsl:value-of select="$disDt"/>
				</DATE_DISABILITY_BEGAN>
				</xsl:if>
				<xsl:variable name="dDt" select="$CompInfo/EmployeeInfo/com.csc_DeathDt"/>
				<xsl:if test="string-length($dDt)>1">
					<DATE_OF_DEATH>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="substring($dDt,1,4)"/>
						</xsl:attribute>
						<xsl:attribute name="MONTH">
							<xsl:value-of select="substring($dDt,6,2)"/>
						</xsl:attribute>
						<xsl:attribute name="DAY">
							<xsl:value-of select="substring($dDt,9,2)"/>
						</xsl:attribute>
						<xsl:value-of select="$dDt"/>
					</DATE_OF_DEATH>
				</xsl:if>
					<BODY_PART>
						<xsl:attribute name="CODE">
							<xsl:choose>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multiple Head Injury'">
									<xsl:text>10</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Skull'">
									<xsl:text>11</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Brain'">
									<xsl:text>12</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Ear'">
									<xsl:text>13</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Eye'">
									<xsl:text>14</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Nose'">
									<xsl:text>15</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Teeth'">
									<xsl:text>16</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Mouth'">
									<xsl:text>17</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Other Head Soft Tissue'">
									<xsl:text>18</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Facial Bones'">
									<xsl:text>19</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multipl Neck Injury'">
									<xsl:text>20</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Vertebrae'">
									<xsl:text>21</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Disc'">
									<xsl:text>22</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Spinal Cord'">
									<xsl:text>23</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Larynx'">
									<xsl:text>24</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Neck Soft Tissue'">
									<xsl:text>25</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Trachea'">
									<xsl:text>26</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multiple Upper Extremities'">
									<xsl:text>30</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Upper Arm'">
									<xsl:text>31</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Elbow'">
									<xsl:text>32</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Lower Arm'">
									<xsl:text>33</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Wrist'">
									<xsl:text>34</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Hand'">
									<xsl:text>35</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Finger'">
									<xsl:text>36</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Thumb'">
									<xsl:text>37</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Shoulder'">
									<xsl:text>38</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Wrist and Hand'">
									<xsl:text>39</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multiple Trunk'">
									<xsl:text>40</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Upper Back'">
									<xsl:text>41</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Lower Back'">
									<xsl:text>42</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Disc'">
									<xsl:text>43</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Chest'">
									<xsl:text>44</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Sacrum and Coccyx'">
									<xsl:text>45</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Pelvis'">
									<xsl:text>46</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Spinal Cord'">
									<xsl:text>47</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Internal Organs'">
									<xsl:text>48</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Heart'">
									<xsl:text>49</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multiple Lower Extremities'">
									<xsl:text>50</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Hip'">
									<xsl:text>51</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Upper Leg'">
									<xsl:text>52</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Knee'">
									<xsl:text>53</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Lower Leg'">
									<xsl:text>54</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Ankle'">
									<xsl:text>55</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Foot'">
									<xsl:text>56</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Toe'">
									<xsl:text>57</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Great Toe'">
									<xsl:text>58</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Lungs'">
									<xsl:text>60</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Abdomen'">
									<xsl:text>61</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Buttocks'">
									<xsl:text>62</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Lumbar/Sacral Vertebrae'">
									<xsl:text>63</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Artificial Appliance'">
									<xsl:text>64</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Insufficient Info to Identify'">
									<xsl:text>65</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='No physical Injury'">
									<xsl:text>66</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Multiple Body Parts'">
									<xsl:text>90</xsl:text>
								</xsl:when>
								<xsl:when test="$EntityNode[ClaimsPartyInfo/ClaimsPartyRoleCd='Employee']/ClaimsInjuredInfo/ClaimsInjury/BodyPartDesc ='Body Systems and Multiple Body Systems'">
									<xsl:text>91</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:attribute>
					</BODY_PART>

			</INJURY>
			<xsl:for-each select="$EntityNode">
				<xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='WIT'">
						<PARTY_INVOLVED CODE="WT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>WITNESS</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CC'">
						<PARTY_INVOLVED CODE="CNT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Contact</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employee'">
						<PARTY_INVOLVED CODE="EME">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>EMPLOYEE</TYPE>
						</PARTY_INVOLVED>
						<PARTY_INVOLVED CODE="CLT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Claimant</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Employer'">
						<PARTY_INVOLVED CODE="EMR">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>EMPLOYER</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='HOSP'">
						<PARTY_INVOLVED CODE="HOS">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Hospital</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='PHY'">
						<PARTY_INVOLVED CODE="MPV">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Medical Provider</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENT_REFERENCE ID="CMT01"/>
			</xsl:if>
		</CLAIM>
		
		<xsl:call-template name="AddInvolvedEntity"></xsl:call-template>
		<xsl:call-template name="AddEntityAddress"></xsl:call-template>
		<xsl:call-template name="AddWorkComments"></xsl:call-template>

	</xsl:template>
	

	<xsl:template name="AddWorkComments">
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
  		  <xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENTS COUNT="01" NEXT_ID="02">
					<COMMENT ID="CMT01">
					<CATEGORY TYPE="N">FNOL General Comment</CATEGORY>
					<SUBJECT>Remark</SUBJECT>
					<AUTHOR USER_ID="UBIAgent"></AUTHOR>
         			<TEXT><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt"/></TEXT>
					</COMMENT>
			</COMMENTS>		
		  </xsl:if>
	</xsl:template>
	
</xsl:stylesheet>