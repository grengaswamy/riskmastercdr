<!-- This stylesheet is used to convert a First Notice Of Loss ACORD Message to Advanced Claims XML format
      It uses Hardcoded code conversions for most part because there is no other way to convert code from ACORD to AC. If there are some changes inside Advanced claims the code changes should reflect in thses styelsheets as well.
      For example it will need updates for different line of business codes if they differ from normal convention. Also party invloved codes have been translated usign the Base version of Advanced claims.
      They might need changes according to the customer implementation.-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt">

	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
<xsl:include href="ACORDToACAuto.xsl"/>
<xsl:include href="ACORDToACHome.xsl"/>
<xsl:include href="ACORDToACWork.xsl"/>
<xsl:include href="ACORDToACCGL.xsl"/>
	<xsl:template match="/">
		
		<CLAIMEXTRACT>
			<HEADER DOCVERSION="2006.1" SOURCEAPP="AgencyLink" SOURCEDB="AgencyLink" APPVERSION="1.2006.38">
            <xsl:attribute name="USERID"><xsl:value-of select="/ACORD/SignonRq/SignonPswd/CustId/CustLoginId"/></xsl:attribute>
            <xsl:attribute name="PASSWORD"><xsl:value-of select="/ACORD/SignonRq/SignonPswd/CustPswd/Pswd"/></xsl:attribute>
            <xsl:attribute name="TARGETDB"><xsl:value-of select="/ACORD/SignonRq/ClientApp/com.csc_DataSourceName"/></xsl:attribute>
				<DOC_NAME>FNOL</DOC_NAME>
				<DESCRIPTION>First Notice of Loss</DESCRIPTION>
				<RqUID>{57DFCE00-C131-47BA-9B31-4D710B31C4CA}</RqUID>
				<DATE_CREATED>
				<xsl:attribute name="YEAR"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt, 1,4)"/></xsl:attribute>
				<xsl:attribute name="MONTH"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt, 6,2)"/></xsl:attribute>
				<xsl:attribute name="DAY"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt, 9,2)"/></xsl:attribute>
				</DATE_CREATED>
				<TIME_CREATED>
				<xsl:attribute name="HOUR"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt,12,2)"/></xsl:attribute>
				<xsl:attribute name="MINUTE"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt,15,2)"/></xsl:attribute>
				<xsl:attribute name="SECOND"><xsl:value-of select="substring(/ACORD/SignonRq/ClientDt,18,2)"/></xsl:attribute>
				<xsl:attribute name="TIME_ZONE">EST</xsl:attribute>
				</TIME_CREATED>
			</HEADER>
			<PROCESSING_MESSAGE TYPE="REQUEST">
				<FUNCTION>ADDCOMMENT</FUNCTION>
				<RESPOND_TO>URL</RESPOND_TO>
				<RESPOND_BY>100</RESPOND_BY>
				<PARAMETERS COUNT="8">
				<!-- Following Parameters describe how to process this XML inside Advanced Claims, dont change-->
  <PARAMETER NAME="IMPORT" TYPE="STRING">COMMENT:ADDRESS:ENTITY:EVENT:CLAIM:POLICY</PARAMETER>
  <PARAMETER NAME="LOGFILE" TYPE="STRING">AgencyLink.Log</PARAMETER>
  <PARAMETER NAME="CLEARLOG" TYPE="BOOLEAN">YES</PARAMETER>
  <PARAMETER NAME="IMPORT.ENTITY.MATCHNAME" TYPE="BOOLEAN">YES</PARAMETER>
  <PARAMETER NAME="IMPORT.ENTITY.MATCHTAXID" TYPE="BOOLEAN">YES</PARAMETER>
  <PARAMETER NAME="IMPORT.ENTITY.UPDATE" TYPE="BOOLEAN">YES</PARAMETER>
   <PARAMETER NAME="IMPORT.CLAIM.UPDATE" TYPE="BOOLEAN">NO</PARAMETER>
  </PARAMETERS>
			</PROCESSING_MESSAGE>
			<EVENT ID="EVT1">
			<DESCRIPTION><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/IncidentDesc"/></DESCRIPTION>
			<LOCATION_DESCRIPTION><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/Addr/Addr1"/></LOCATION_DESCRIPTION>
			<NUMBER>GENERATE</NUMBER>
			<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
			<xsl:for-each select="$EntityNode">
				<xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='ReptTo'">
						<PARTY_INVOLVED CODE="RTO">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>ReportedTo</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='ReptBy'">
						<PARTY_INVOLVED CODE="RBY">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>ReportedBy</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					</xsl:choose>
					</xsl:for-each>
			</EVENT>
			<!-- CLAIM INFORMATION -->
				<xsl:if test="(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/LOBCd)='HOME'">
				<xsl:call-template name="AcordToACHome"></xsl:call-template>
				</xsl:if>
				<xsl:if test="(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/LOBCd)='AUTOP'">
				<xsl:call-template name="AcordToACAuto"></xsl:call-template>
				</xsl:if>
                <xsl:if test="(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/LOBCd)='WORK'">
				<xsl:call-template name="AcordToACWork"></xsl:call-template>
				</xsl:if>
                <xsl:if test="(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/LOBCd)='CGL'">
<xsl:call-template name="AcordToACCGL"></xsl:call-template>
				</xsl:if>
      </CLAIMEXTRACT>
	</xsl:template>
</xsl:stylesheet>