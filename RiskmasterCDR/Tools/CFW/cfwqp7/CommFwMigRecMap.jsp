<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ------------------------------------------------------------------- --%>
<%-- Migrate the XMLTagMap peoperty file to CommFwRecord??.xml.          --%>
<%-- ------------------------------------------------------------------- --%>
<%@page language='java' contentType='text/html'%>
<%@page import="java.io.BufferedReader" %>
<%@page import="java.io.IOException" %>
<%@page import="java.io.PrintWriter" %>
<%@page import="java.io.StringWriter" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.List" %>
<%@page import="com.csc.cfw.process.Envelope" %>
<%@page import="com.csc.cfw.recordbuilder.RecordManagerFactory" %>
<%@page import="com.csc.cfw.util.CommFwSystem" %>
<%@page import="com.csc.cfw.xml.domain.CobolRecordFactory" %>
<%@page import="com.csc.fw.util.FwException" %>
<%@page import="com.csc.fw.util.FwSystem" %>
<%@page import="com.csc.fw.util.PropertyFileManager" %>
<%@page import="com.csc.fw.util.ServletUtil" %>
<%@page import="com.csc.fw.util.XMLDocument" %>

<HTML>
  <HEAD>
    <TITLE>Communications Framework Migrate Record Map</TITLE>
    <META name="GENERATOR" content="IBM WebSphere Studio">
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    <SCRIPT LANGUAGE="JAVASCRIPT">
    </SCRIPT>
    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD ALIGN="Center"  >
          <H2>Communications Framework Migrate Record Map</H2>
        </TD>
      </TR>
      <TR>
        <TD  >

<%
    Envelope envelope = new Envelope();
    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;

    String recordFileName = CobolRecordFactory.getMapFile();
    String recordFilePath = ServletUtil.fullFilePath(getServletContext(), request, recordFileName);
    if (recordFilePath == null)
    {
        recordFileName += ".mig";
        recordFilePath = ServletUtil.fullFilePath(getServletContext(), request, recordFileName);
    }
    
    String recordManagerName = request.getParameter("recordManagerName");
    if (recordManagerName == null)
        recordManagerName = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
    if (recordManagerName == null)
        recordManagerName = "";

    String recordCtrlFileName = RecordManagerFactory.DEFAULT_FILENAME;
    List recordCtrlFileList = RecordManagerFactory.current().configFileList(null);
    if (recordCtrlFileList == null)
        recordCtrlFileList = new ArrayList();
    if (recordCtrlFileList.size() == 0)
        recordCtrlFileList.add(recordCtrlFileName);
    String recordCtrlPropFile = request.getParameter("recordCtrlPropFile");
    if (recordCtrlPropFile != null)
        recordCtrlFileName = recordCtrlPropFile;
    else
    {
        /* Default the filename to CommFwRecord%%APPLICATION%%.xml */
        String recordCtrlFileDefault = recordCtrlFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && recordCtrlFileDefault.endsWith(sfx))
        {
            recordCtrlFileDefault = recordCtrlFileDefault.substring(0, recordCtrlFileDefault.length() - sfx.length());
            recordCtrlFileDefault += app + sfx;
        }
        for (int i = 0; i < recordCtrlFileList.size(); i++)
        {
            String s = recordCtrlFileList.get(i).toString();
            if (recordCtrlFileDefault.equals(s))
            {
                recordCtrlFileName = recordCtrlFileDefault;
                break;
            }
        }
    }
    String recordCtrlFilePath = ServletUtil.fullFilePath(getServletContext(), request, recordCtrlFileName);
    String errorMessageRecordCtrl = null;
    	
    
    if ("POST".equals(request.getMethod()))
    {
        String func = request.getParameter("function");
        if ("Reset Record Cache".equalsIgnoreCase(func))
            CobolRecordFactory.getCurrent().recordDescCache.reset();
        else
        if ("Migrate Record Property File".equalsIgnoreCase(func))
        {
            String path = recordFilePath.substring(0, recordFilePath.length() - recordFileName.length());
            RecordManagerFactory.current().migrateTagMap(envelope, path, recordManagerName, recordFileName);

            // Reevaluate the RecordMap file name/path if changed by migrate.
            recordFileName = CobolRecordFactory.getMapFile();
            recordFilePath = ServletUtil.fullFilePath(getServletContext(), request, recordFileName);
            if (recordFilePath == null)
            {
                recordFileName += ".mig";
                recordFilePath = ServletUtil.fullFilePath(getServletContext(), request, recordFileName);
            }
        }
        else
        if ("Reset Record Control Cache".equalsIgnoreCase(func))
        {
            RecordManagerFactory.reset();
        }
        else
        if ("Update Record Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("RecordTextArea");
            try
            {
                XMLDocument.parseDoc(updateTextBuffer);
            }
            catch (FwException e)
            {
                errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Updated Record Control XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageRecordCtrl == null)
                try
                {
                    PropertyFileManager.writePropertyFile(recordCtrlFilePath, updateTextBuffer);
                    RecordManagerFactory.current().configSetFileCache(recordCtrlFileName, updateTextBuffer);
                    RecordManagerFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Error updating Record Control file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
    }
%>

          <TABLE>
            <TR ALIGN="Center"><TD ALIGN="Center"  >
              <TABLE>


                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgRecordMapForm" ACTION="CommFwMigRecMap.jsp">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">

                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Record Map Properties</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD>
                               <SELECT NAME="recordManagerName" SIZE="1">
              <%
                                {
                                    Iterator items = RecordManagerFactory.current().getRecordManagerMap().keySet().iterator();
                                    while (items.hasNext())
                                    {
                                        String key = items.next().toString();
              %>
                                 <OPTION VALUE="<%=key%>"  <%=ServletUtil.setSelected(recordManagerName,key) %>><%=key%></OPTION>
              <%
                                    }
                                }
              %>
                               </SELECT>
                              </TD>
                            </TR>


<%
            if (recordFilePath != null)
            {
%>
                            <TR>
                              <TD><B>Path:</B> <%=recordFilePath%></TD>
                            </TR>
<%
            }
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="RecordPropTextArea">
<%
                                if (recordFilePath == null)
                                {
                                    Iterator items = CobolRecordFactory.getCurrent().register.keySet().iterator();
                                    while (items.hasNext())
                                    {
                                        String key = (String)items.next();
%><%=key+"="+(String)(CobolRecordFactory.getCurrent().register.get(key))%>
<%
                                    }
                                }
                                else
                                {
                                    BufferedReader reader = FwSystem.getResourceAsReader(recordFilePath);
                                    for (String line = reader.readLine();
                                         line != null;
                                         line = reader.readLine() )
                                    {
%><%=line%>
<%
                                    }
                                    reader.close();
				}
%></TEXTAREA>
                              </TD>
                            </TR>
                            <TR>
                              <TD ALIGN="Left"  >
<%
				if (recordFilePath != null)
				{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<INPUT VALUE="Migrate Record Property File" TYPE="SUBMIT" NAME="function">
<%
				}
                                if (recordCtrlPropFile != null)
                                {
%>
                                <input type="hidden" name="recordCtrlPropFile" value="<%=recordCtrlPropFile%>" />
<%
				}
%>
                              </TD>
                            </TR>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>


                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgRecordForm" ACTION="CommFwMigRecMap.jsp#records">
                   <A NAME="records"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Record Control</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD>
              <%
				if (recordCtrlFileList.size() > 1)
				{
              %>
                               <SELECT NAME="recordCtrlPropFile" SIZE="1" onChange="document.CfwCfgRecordForm.submit()">
              <%
						for (int i = 0; i < recordCtrlFileList.size(); i++)
						{
							String filename = (String)recordCtrlFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(recordCtrlFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (recordCtrlFilePath != null)
		{
%>
                            <TR>
                              <TD><B>Path:</B> <%=recordCtrlFilePath%></TD>
                            </TR>
<%
				if (errorMessageRecordCtrl != null)
				{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageRecordCtrl %><BR />
                              </TD WIDTH="100%"  >
                            </TR>
<%		
				}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="RecordTextArea">
<%
				if (errorMessageRecordCtrl != null)
				{
%><%=updateTextBuffer%><%				
				}
				else
				{
					StringWriter sw = new StringWriter();
					PrintWriter writer = new PrintWriter(sw);
					PropertyFileManager.readPropertyFile(recordCtrlFilePath, writer);
%><%=sw.toString()%><%
                                }
%></TEXTAREA>
                              </TD>
                            </TR>
                            <TR>
                              <TD ALIGN="Left"  >
                                <INPUT VALUE="Reset" TYPE="RESET">
                                &nbsp;&nbsp;&nbsp;&nbsp;<INPUT VALUE="Reset Record Control Cache" TYPE="SUBMIT" NAME="function">
                                &nbsp;&nbsp;&nbsp;&nbsp;<INPUT VALUE="Update Record Control File" TYPE="SUBMIT" NAME="function">
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>



              </TABLE>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
