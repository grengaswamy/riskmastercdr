<%-- ----------------------------------------------------------------- --%>
<%-- This is an include JSP for form switches for XML Test JSP's.      --%>
<%-- ----------------------------------------------------------------- --%>

          <TR>
           <TD>
            <DIV ALIGN="Center">
             <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>
             <P><FONT SIZE="+1"><STRONG><U>Transaction Modification Options</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
            <TABLE BORDER="1">
             <TR>
              <TD  >
               <B>Processing step to complete:</B><BR>
               <SELECT NAME="s3TransformString" SIZE="1">
                <OPTION VALUE=""  <%=ServletUtil.setSelected("0",commFwData.getS3TransformString()) %>> Complete
                <OPTION VALUE="1" <%=ServletUtil.setSelected("1",commFwData.getS3TransformString()) %>> Show input XML Message (ACORD Message)
                <OPTION VALUE="2" <%=ServletUtil.setSelected("2",commFwData.getS3TransformString()) %>> Show request XML message after XSL translation (Internal XML Message)
                <OPTION VALUE="3" <%=ServletUtil.setSelected("3",commFwData.getS3TransformString()) %>> Show data inside of record wrapper before being sent to the backend system.
                <OPTION VALUE="4" <%=ServletUtil.setSelected("4",commFwData.getS3TransformString()) %>> Show data inside of record wrapper after being sent to the backend system
                <OPTION VALUE="5" <%=ServletUtil.setSelected("5",commFwData.getS3TransformString()) %>> Show internal XML response
               </SELECT>
              </TD>
             </TR>
             <TR>
              <TD  >
               <TABLE>
                <TR>
                 <TD><B>Transaction Trace Level:</B></TD>
                 <TD>
                  <SELECT NAME="trace" SIZE="1">
                   <OPTION VALUE="0" <%=ServletUtil.setSelected("0",String.valueOf(commFwData.getTrace())) %> >0: Off
                   <OPTION VALUE="1" <%=ServletUtil.setSelected("1",String.valueOf(commFwData.getTrace())) %> >1: Basic
                   <OPTION VALUE="2" <%=ServletUtil.setSelected("2",String.valueOf(commFwData.getTrace())) %> >2: + Req/Rsp XML
                   <OPTION VALUE="3" <%=ServletUtil.setSelected("3",String.valueOf(commFwData.getTrace())) %> >3: + Internal XML
                  </SELECT>
                 </TD>
                <TD VALIGN="center"><B>Repeat Count:</B></TD>
                <TD > <INPUT NAME="repostCount" SIZE=5 ></TD>
                </TR>
                <TR>
                 <TD>
                  <B>Transaction XSL Trace:</B>
                  <P><Font SIZE="-1">(multiples allowed)</FONT></P>
                 </TD>
                 <TD COLSPAN=3>
                  <SELECT NAME="xslTraceOptions" MULTIPLE SIZE="4">
                   <OPTION VALUE="0"  >None
                   <OPTION VALUE="1" <%=ServletUtil.setSelected("1",String.valueOf(commFwData.getXslTrace() & 1)) %> >Print information as each node is executed in the stylesheet.
                   <OPTION VALUE="2" <%=ServletUtil.setSelected("2",String.valueOf(commFwData.getXslTrace() & 2)) %> >Print information after each result-tree generation event.
                   <OPTION VALUE="4" <%=ServletUtil.setSelected("4",String.valueOf(commFwData.getXslTrace() & 4)) %> >Print information after each selection event.
                   <OPTION VALUE="8" <%=ServletUtil.setSelected("8",String.valueOf(commFwData.getXslTrace() & 8)) %> >Print information whenever a template is invoked.
                  </SELECT>
                 </TD>
                </TR>
               </TABLE>
              </TD>
             </TR>

             <TR>
              <TD  >
               <TABLE>
                <TR>
                 <TD>
                  <TABLE>
                   <TD VALIGN="center"><B>Override System<br>Properties:</B></TD>
                   <TD><TEXTAREA COLS="18" WRAP="OFF" ROWS="2" NAME="overridesText"><% if (commFwData.getOverridesText() != null) { %><jsp:getProperty name='commFwData' property='overridesText'/><% } %></TEXTAREA></TD>
                  </TABLE>
                 </TD>
                 <TD>
                  <TABLE>
                   <TR>
                    <TD VALIGN="center"><B>Request Encoding:</B></TD>
                    <TD > <INPUT NAME="rqEncoding" 
<%  if (commFwData.getRqEncoding() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='rqEncoding'/>" 
<%
    }
%>
                                 SIZE=5 ></TD>
                   </TR>
                   <TR>
                    <TD VALIGN="center"><B>Response Encoding:</B></TD>
                    <TD > <INPUT NAME="rsEncoding" 
<%  if (commFwData.getRsEncoding() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='rsEncoding'/>" 
<%
    }
%>
                                 SIZE=5 ></TD>
                   </TR>
                  </TABLE>
                 </TD>
                </TR>
                <TR>
                 <TD>
                  <TABLE> 
                   <TR>
                    <TD><INPUT VALUE="true"     TYPE="CHECKBOX" NAME="noTransformCache" <%=ServletUtil.setChecked("true", request.getParameter("noTransformCache")) %> ><B>Bypass Transform Cache</B></TD>
                   </TR>
                   <TR>
                     <TD><INPUT VALUE="Y"        TYPE="CHECKBOX" NAME="webServicesCall" <%=ServletUtil.setChecked("Y", request.getParameter("webServicesCall")) %> ><B>Web Services Call</B></TD>
                   </TR>
                   <TR>
                     <TD><INPUT VALUE="Y"        TYPE="CHECKBOX" NAME="useCFWProcess" <%=ServletUtil.setChecked("Y", request.getParameter("useCFWProcess")) %> ><B>Use CFW to Process</B></TD>
                   </TR>
                  </TABLE>
                 </TD>
                 <TD>
                  <TABLE> 
                   <TR>
                    <TD>
<%
	if (fForceValidationOff)
	{
%>
                     <INPUT VALUE="false" TYPE="hidden" NAME="validateRq">
<%
	}
	else
	{
%>
                     <INPUT VALUE="true" TYPE="CHECKBOX" NAME="validateRq" <%=ServletUtil.setChecked("true", request.getParameter("validateRq")) %> ><B>Validate Request</B>
<%
	}
%>
                    </TD>
                   </TR>
                   <TR>
                    <TD>
<%
	if (fForceValidationOff)
	{
%>
                     <INPUT VALUE="false" TYPE="hidden" NAME="validateRs">
<%
	}
	else
	{
%>
                     <INPUT VALUE="true" TYPE="CHECKBOX" NAME="validateRs" <%=ServletUtil.setChecked("true", request.getParameter("validateRs")) %> ><B>Validate Response</B>
<%
	}
%>
                    </TD>
                   </TR>
                  </TABLE>
                 </TD>
                </TR>
               </TABLE>
              </TD>
             </TR>

            </TABLE>

            
            </DIV>
            <HR WIDTH="100%">
           </TD>
          </TR>






