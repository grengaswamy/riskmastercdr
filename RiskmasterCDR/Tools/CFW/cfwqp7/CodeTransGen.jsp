<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP is created for generating Code Translation files    --%>
<%-- ----------------------------------------------------------------- --%>
<%@ page language='java' contentType='text/html'%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Vector" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page errorPage="error.jsp" %>
<%@ page pageEncoding="UTF-8" %>


<html>  
<link href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <head>
    <script language="JavaScript">
        function window_onload()
        {
            var frm = document.ExeForm;
            frm.driver.selectedIndex = 0;
            driverChange();
        }

        function resetFields()
        {
            var frm = document.ExeForm;
            frm.qual.value = "";
            frm.username.value = "";
            frm.pwd.value = "";
            frm.url_context.value = "";
            frm.ssid.value = "";
            frm.hostip.value = "";
            frm.port.value = "";
            frm.urlOptions.value = "";
        }

        function driverChange()
        {
            var frm = document.forms.ExeForm;
            var driverIdx = frm.driver.selectedIndex;
            if (driverIdx == 0)
            {
                // Data Source
                frm.url_context.value = "";
                frm.ssid.value = "";
                frm.hostip.value = "";
                frm.port.value = "";
                frm.urlOptions.value = "";
                frm.url_context.disabled = false;
                setTDDisable("t_url_context", false);
                frm.ssid.disabled = true;
                setTDDisable("t_ssid", true);
                frm.hostip.disabled = true;
                setTDDisable("t_hostip", true);
                frm.port.disabled = true;
                setTDDisable("t_port", true);
                frm.urlOptions.disabled = true;
                setTDDisable("t_urlOptions", true);
            }
            else
            if (driverIdx == 1)
            {
                // DB2 Client
                frm.url_context.value = "";
                frm.ssid.value = "";
                frm.hostip.value = "";
                frm.port.value = "";
                frm.urlOptions.value = "";
                frm.url_context.disabled = false;
                setTDDisable("t_url_context", false);
                frm.ssid.disabled = true;
                setTDDisable("t_ssid", true);
                frm.hostip.disabled = true;
                setTDDisable("t_hostip", true);
                frm.port.disabled = true;
                setTDDisable("t_port", true);
                frm.urlOptions.disabled = true;
                setTDDisable("t_urlOptions", true);
            }
            else
            if (driverIdx == 2)
            {
                // DB2 Direct
                frm.url_context.value = "";
                frm.ssid.value = "";
                frm.hostip.value = "";
                frm.port.value = "";
                frm.url_context.disabled = true;
                setTDDisable("t_url_context", true);
                frm.ssid.disabled = false;
                setTDDisable("t_ssid", false);
                frm.hostip.disabled = false;
                setTDDisable("t_hostip", false);
                frm.port.disabled = false;
                setTDDisable("t_port", false);
                frm.urlOptions.disabled = false;
                setTDDisable("t_urlOptions", false);
            }

            return true;
        }

        function setTDDisable(fieldId, disable)
        {
            var fld = document.getElementById(fieldId);
            if (disable)
                fld.className = "LABEL_DISABLED";
            else
                fld.className = "LABEL";
        }
        
        function formsubmit()
        {
            var frm = document.ExeForm;
            var driverIdx = frm.driver.selectedIndex;
            if ((driverIdx == 0 &&
                 (frm.qual.value=="" || frm.url_context.value == ""))
                 || (driverIdx == 1 &&
                     (frm.qual.value=="" || frm.username.value=="" || frm.pwd.value=="" || frm.url_context.value == ""))
                 || (driverIdx == 2 &&
                     (frm.qual.value=="" || frm.username.value=="" || frm.pwd.value=="" || frm.hostip.value=="" || frm.port.value=="" || frm.ssid.value=="")) )
                alert("Enter required fields");
            else
                document.ExeForm.submit();
        }
	</script>
    <title>Code Translation Generator - Communications Framework</title>
    <meta name="GENERATOR" content="IBM WebSphere Studio"/>
  </head>
  <body BGCOLOR="#C0C0C0">
    <br/><br/>
    <div align="center">
      <H1><U>Generate Code Translation Tables</U></H1>
      <br/>
      <form name="ExeForm" method="post" action="CodeTransGenExec.jsp">
      <table align="center" width="85%">	
        <tr>
          <td align="center">
            <table cellpadding="4" cellspacing="4">
              <tr>
                <td class="LABEL" id="t_qual"><B>*Qualifier: </B></td>
                <td>
                  <input name="qual" type="text" value="" style="width:78;" maxlength="32"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL" id="t_username"><B>*Username: </B></td>
                <td>
                  <input  name="username" type="text" value="" style="width:78;" maxlength="32"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL" id="t_pwd"><B>*Password: </B></td>
                <td>
                  <input name="pwd" type="password" value="" STYLE="width:78;" maxlength="32"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL" id="t_driver"><B>Database Driver: </B></td>
                <td>
                  <select name="driver" onChange="javascript:driverChange();">
                    <option value="" selected="selected">Server Data Source</option>
                    <option value="COM.ibm.db2.jdbc.app.DB2Driver">DB2 Client JDBC Driver</option>
                    <option value="com.ibm.db2.jcc.DB2Driver">DB2 Universal JDBC Driver</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="LABEL" id="t_url_context"><B>*URL/Context: </B></td>
                <td>
                  <input name="url_context" type="text" value="" style="width:250;" maxlength="100"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL_DISABLED" id="t_hostip"><B>*Host name or IP: </B></td>
                <td>
                  <input disabled="disabled" name="hostip" type="text" value="" style="width:150;" maxlength="20"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL_DISABLED" id="t_port"><B>*Port: </B></td>
                <td>
                  <input disabled="disabled" name="port" type="text" value="" style="width:78;" maxlength="8"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL_DISABLED" id="t_ssid"><B>*DB Name (DB2 SSID): </B></td>
                <td>
                  <input disabled="disabled" name="ssid" type="text" value="" style="width:78;" maxlength="16"/>
                </td>
              </tr>
              <tr>
                <td class="LABEL_DISABLED" id="t_urlOptions"><B>URL Options: </B></td>
                <td>
                  <input disabled="disabled" name="urlOptions" type="text" value="" style="width:150;" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="left">
                  <input name="submitQuery" type="button" value="Submit" style="width:80;" onclick="formsubmit();" />
                </td>
              </tr>			
              <tr>
                <td colspan="2" align="left">
			        <a href="CodeTransMenu.jsp">Return to Code Translation Menu</a>	
                </td>
              </tr>			
            </table>
          </td>
        </tr>
      </table>
      </form>
	</div>
  </body>
</html>
