package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Enumeration;
import java.util.Vector;
import com.csc.fw.util.FwSystem;
import com.csc.fw.util.ServletUtil;

public final class XMLTest_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/WEB-INF/jspf/CFwXMLDataForm.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/CFwXMLResultForm.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      com.csc.cfw.util.test.CommFwTestData commFwData = null;
      synchronized (_jspx_page_context) {
        commFwData = (com.csc.cfw.util.test.CommFwTestData) _jspx_page_context.getAttribute("commFwData", PageContext.PAGE_SCOPE);
        if (commFwData == null){
          commFwData = new com.csc.cfw.util.test.CommFwTestData();
          _jspx_page_context.setAttribute("commFwData", commFwData, PageContext.PAGE_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("commFwData"), request);
      out.write('\r');
      out.write('\n');

boolean fExecute = (commFwData.function != null);
boolean fForceValidationOff = false;
if (fExecute)
{
	commFwData.newTest();
	commFwData.getAuthCredentials(request, session);
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", request.getParameter("noTransformCache")));
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
	commFwData.setRepostCount(request.getParameter("repostCount"));
	commFwData.setXslTraceOptions(request.getParameterValues("xslTraceOptions"));
	commFwData.execute();
}
else
{
	commFwData.newTest();
}

      out.write("\r\n");
      out.write("\r\n");
      out.write("<HTML>\r\n");
      out.write(" <HEAD>\r\n");
      out.write("  <META http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\r\n");
      out.write("  <LINK href=\"includes/INFUtil.css\" type=\"text/css\" rel=\"stylesheet\">\r\n");
      out.write("  <TITLE>XML Test");
      out.print(fExecute ? " Response" : "" );
      out.write(" - Communications Framework</TITLE>\r\n");
      out.write(" </HEAD>\r\n");
      out.write(" <BODY BGCOLOR=\"#C0C0C0\">\r\n");
      out.write(" <SCRIPT LANGUAGE=\"JAVASCRIPT\">\r\n");
      out.write("  ");
      out.print( commFwData.getXmlStringsAsJS() );
      out.write("\r\n");
      out.write(" </SCRIPT>\r\n");
      out.write(" <DIV ALIGN=\"Center\" ID=\"HtmlDiv1\">\r\n");
      out.write("  <TABLE>\r\n");
      out.write("   <TR>\r\n");
      out.write("    <TD ALIGN=\"Center\"  >\r\n");
      out.write("     <H4>Communications Framework XML Test</H4>\r\n");
      out.write("     <HR WIDTH=\"100%\">\r\n");
      out.write("    </TD>\r\n");
      out.write("   </TR>\r\n");
      out.write("   <TR>\r\n");
      out.write("    <TD  >\r\n");
      out.write("      <TABLE>\r\n");
      out.write("       <TR ALIGN=\"Center\">\r\n");
      out.write("        <TD ALIGN=\"Center\"  >\r\n");
      out.write("         <FORM METHOD=\"POST\" NAME=\"CommFwTestForm\" ACTION=\"XMLTest.jsp\">\r\n");
      out.write("         <TABLE>\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Transaction XML Data</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <B>Copy input XML Message below:</B>\r\n");
      out.write("            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT VALUE=\"Y\"     TYPE=\"CHECKBOX\" NAME=\"nonXML\" ");
      out.print(ServletUtil.setChecked("Y", request.getParameter("nonXML")) );
      out.write(" ><B>Non-XML</B><br>\r\n");
      out.write("            <TEXTAREA cols=\"75\" WRAP=\"OFF\" ROWS=\"9\" NAME=\"requestXml\">");
 if (commFwData.getRequestXml() != null) { 
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(commFwData.getRequestXml(),"&","&amp;"),"<","&lt;") /*jsp:getProperty name='commFwData' property='requestXml'/*/);
 } 
      out.write("</TEXTAREA>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD>\r\n");
      out.write("            <B>Enter XML URL below:</B><BR />\r\n");
      out.write("            <INPUT NAME=\"xmlUrl\"\r\n");
  if (commFwData.getXmlUrl() != null)
    {

      out.write("\r\n");
      out.write("                   VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getXmlUrl())));
      out.write("\"  \r\n");

    }

      out.write("\r\n");
      out.write("                   SIZE=\"75\">\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD ALIGN=\"Left\"  >\r\n");
      out.write("            <INPUT VALUE=\"Execute XML\" TYPE=\"SUBMIT\" NAME=\"function\"> \r\n");
      out.write("            <INPUT VALUE=\"Reset\" TYPE=\"RESET\">\r\n");
      out.write("            <HR WIDTH=\"100%\">\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <INPUT TYPE=\"hidden\" NAME=\"locale\" \r\n");
  if (commFwData.getLocale() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getLocale())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write(">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Transaction Modification Options</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TABLE BORDER=\"1\">\r\n");
      out.write("             <TR>\r\n");
      out.write("              <TD  >\r\n");
      out.write("               <B>Processing step to complete:</B><BR>\r\n");
      out.write("               <SELECT NAME=\"s3TransformString\" SIZE=\"1\">\r\n");
      out.write("                <OPTION VALUE=\"\"  ");
      out.print(ServletUtil.setSelected("0",commFwData.getS3TransformString()) );
      out.write("> Complete</OPTION>\r\n");
      out.write("                <OPTION VALUE=\"1\" ");
      out.print(ServletUtil.setSelected("1",commFwData.getS3TransformString()) );
      out.write("> Show input XML Message (ACORD Message)</OPTION>\r\n");
      out.write("                <OPTION VALUE=\"2\" ");
      out.print(ServletUtil.setSelected("2",commFwData.getS3TransformString()) );
      out.write("> Show request XML message after XSL translation (Internal XML Message)</OPTION>\r\n");
      out.write("                <OPTION VALUE=\"3\" ");
      out.print(ServletUtil.setSelected("3",commFwData.getS3TransformString()) );
      out.write("> Show data inside of record wrapper before being sent to the backend system.</OPTION>\r\n");
      out.write("                <OPTION VALUE=\"4\" ");
      out.print(ServletUtil.setSelected("4",commFwData.getS3TransformString()) );
      out.write("> Show data inside of record wrapper after being sent to the backend system</OPTION>\r\n");
      out.write("                <OPTION VALUE=\"5\" ");
      out.print(ServletUtil.setSelected("5",commFwData.getS3TransformString()) );
      out.write("> Show internal XML response</OPTION>\r\n");
      out.write("               </SELECT>\r\n");
      out.write("              </TD>\r\n");
      out.write("             </TR>\r\n");
      out.write("             <TR>\r\n");
      out.write("              <TD  >\r\n");
      out.write("               <TABLE>\r\n");
      out.write("                <TR>\r\n");
      out.write("                 <TD><B>Transaction Trace Level:</B></TD>\r\n");
      out.write("                 <TD>\r\n");
      out.write("                  <SELECT NAME=\"trace\" SIZE=\"1\">\r\n");
      out.write("                   <OPTION VALUE=\"0\" ");
      out.print(ServletUtil.setSelected("0",String.valueOf(commFwData.getTrace())) );
      out.write(" >0: Off</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"1\" ");
      out.print(ServletUtil.setSelected("1",String.valueOf(commFwData.getTrace())) );
      out.write(" >1: Basic</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"2\" ");
      out.print(ServletUtil.setSelected("2",String.valueOf(commFwData.getTrace())) );
      out.write(" >2: + Req/Rsp XML</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"3\" ");
      out.print(ServletUtil.setSelected("3",String.valueOf(commFwData.getTrace())) );
      out.write(" >3: + Internal XML</OPTION>\r\n");
      out.write("                  </SELECT>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                <TD VALIGN=\"middle\"><B>Repeat Count:</B></TD>\r\n");
      out.write("                <TD > <INPUT name=\"repostCount\" size=\"5\" /></TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("                <TR>\r\n");
      out.write("                 <TD>\r\n");
      out.write("                  <B>Transaction XSL Trace:</B>\r\n");
      out.write("                  <P><Font SIZE=\"-1\">(multiples allowed)</FONT></P>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                 <TD COLSPAN=\"3\">\r\n");
      out.write("                  <SELECT name=\"xslTraceOptions\" multiple=\"multiple\" size=\"4\">\r\n");
      out.write("                   <OPTION VALUE=\"0\"  >None</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"1\" ");
      out.print(ServletUtil.setSelected("1",String.valueOf(commFwData.getXslTrace() & 1)) );
      out.write(" >Print information as each node is executed in the stylesheet.</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"2\" ");
      out.print(ServletUtil.setSelected("2",String.valueOf(commFwData.getXslTrace() & 2)) );
      out.write(" >Print information after each result-tree generation event.</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"4\" ");
      out.print(ServletUtil.setSelected("4",String.valueOf(commFwData.getXslTrace() & 4)) );
      out.write(" >Print information after each selection event.</OPTION>\r\n");
      out.write("                   <OPTION VALUE=\"8\" ");
      out.print(ServletUtil.setSelected("8",String.valueOf(commFwData.getXslTrace() & 8)) );
      out.write(" >Print information whenever a template is invoked.</OPTION>\r\n");
      out.write("                  </SELECT>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("               </TABLE>\r\n");
      out.write("              </TD>\r\n");
      out.write("             </TR>\r\n");
      out.write("\r\n");
      out.write("             <TR>\r\n");
      out.write("              <TD  >\r\n");
      out.write("               <TABLE>\r\n");
      out.write("                <TR>\r\n");
      out.write("                 <TD>\r\n");
      out.write("                  <TABLE>\r\n");
      out.write("                   <TR>\r\n");
      out.write("                   <TD VALIGN=\"middle\"><B>Override System<br>Properties:</B></TD>\r\n");
      out.write("                   <TD><TEXTAREA COLS=\"18\" WRAP=\"OFF\" ROWS=\"2\" NAME=\"overridesText\">");
 if (commFwData.getOverridesText() != null) { 
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getOverridesText())));
 } 
      out.write("</TEXTAREA></TD>\r\n");
      out.write("                   </TR>\r\n");
      out.write("                  </TABLE>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                 <TD>\r\n");
      out.write("                  <TABLE>\r\n");
      out.write("                   <TR>\r\n");
      out.write("                    <TD VALIGN=\"middle\"><B>Request Encoding:</B></TD>\r\n");
      out.write("                    <TD > <INPUT NAME=\"rqEncoding\" \r\n");
  if (commFwData.getRqEncoding() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getRqEncoding())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write("                                 SIZE=\"5\" ></TD>\r\n");
      out.write("                   </TR>\r\n");
      out.write("                   <TR>\r\n");
      out.write("                    <TD VALIGN=\"middle\"><B>Response Encoding:</B></TD>\r\n");
      out.write("                    <TD > <INPUT NAME=\"rsEncoding\" \r\n");
  if (commFwData.getRsEncoding() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getRsEncoding())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write("                                 SIZE=\"5\" ></TD>\r\n");
      out.write("                   </TR>\r\n");
      out.write("                  </TABLE>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("                <TR>\r\n");
      out.write("                 <TD colspan=\"3\">\r\n");
      out.write("                  <table>\r\n");
      out.write("                  <tr>\r\n");
      out.write("                   <TD valign=\"top\">\r\n");
      out.write("                    <TABLE> \r\n");
      out.write("                     <TR>\r\n");
      out.write("                      <TD><INPUT VALUE=\"true\"     TYPE=\"CHECKBOX\" NAME=\"noTransformCache\" ");
      out.print(ServletUtil.setChecked("true", request.getParameter("noTransformCache")) );
      out.write(" /><B>Bypass Transform Cache</B></TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                     <TR>\r\n");
      out.write("                       <TD><INPUT VALUE=\"Y\"        TYPE=\"CHECKBOX\" NAME=\"webServicesCall\" ");
      out.print(ServletUtil.setChecked("Y", request.getParameter("webServicesCall")) );
      out.write(" /><B>Web Services Call</B></TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                     <TR>\r\n");
      out.write("                       <TD><INPUT VALUE=\"Y\"        TYPE=\"CHECKBOX\" NAME=\"useCFWProcess\" ");
      out.print(ServletUtil.setChecked("Y", request.getParameter("useCFWProcess")) );
      out.write(" /><B>Use CFW to Process</B></TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </TD>\r\n");
      out.write("                   <TD valign=\"top\">\r\n");
      out.write("                    <TABLE> \r\n");
      out.write("                     <TR>\r\n");
      out.write("                      <TD>\r\n");

	if (fForceValidationOff)
	{

      out.write("\r\n");
      out.write("                       <INPUT VALUE=\"false\" TYPE=\"hidden\" NAME=\"validateRq\">\r\n");

	}
	else
	{

      out.write("\r\n");
      out.write("                       <INPUT VALUE=\"true\" TYPE=\"CHECKBOX\" NAME=\"validateRq\" ");
      out.print(ServletUtil.setChecked("true", request.getParameter("validateRq")) );
      out.write(" ><B>Validate Request</B>\r\n");

	}

      out.write("\r\n");
      out.write("                      </TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                     <TR>\r\n");
      out.write("                      <TD>\r\n");

	if (fForceValidationOff)
	{

      out.write("\r\n");
      out.write("                       <INPUT VALUE=\"false\" TYPE=\"hidden\" NAME=\"validateRs\"/>\r\n");

	}
	else
	{

      out.write("\r\n");
      out.write("                       <INPUT VALUE=\"true\" TYPE=\"CHECKBOX\" NAME=\"validateRs\" ");
      out.print(ServletUtil.setChecked("true", request.getParameter("validateRs")) );
      out.write(" /><B>Validate Response</B>\r\n");

	}

      out.write("\r\n");
      out.write("                      </TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </TD>\r\n");
      out.write("                   <TD valign=\"top\">\r\n");
      out.write("                    <TABLE> \r\n");
      out.write("                     <TR>\r\n");
      out.write("                      <TD><B>Sign-on ID:</B></TD>\r\n");
      out.write("                      <TD><INPUT type=\"text\" name=\"signonID\"\r\n");
  if (commFwData.getSignonID() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getSignonID())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write("                                 SIZE=\"10\" ></TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                     <TR>\r\n");
      out.write("                      <TD><B>Sign-on Password:</B></TD>\r\n");
      out.write("                      <TD><INPUT type=\"password\" name=\"signonPassword\"\r\n");
  if (commFwData.getSignonPassword() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getSignonPassword())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write("                                 SIZE=\"10\" ></TD>\r\n");
      out.write("                     </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </TD>\r\n");
      out.write("                  </tr>\r\n");
      out.write("                  </table>\r\n");
      out.write("                 </TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("               </TABLE>\r\n");
      out.write("              </TD>\r\n");
      out.write("             </TR>\r\n");
      out.write("\r\n");
      out.write("            </TABLE>\r\n");
      out.write("\r\n");
      out.write("            \r\n");
      out.write("            </DIV>\r\n");
      out.write("            <HR WIDTH=\"100%\">\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("         </TABLE>\r\n");
      out.write("         </FORM>\r\n");
      out.write("\r\n");
      out.write("         <FORM METHOD=\"POST\" NAME=\"CommFwResultForm\" ACTION=\"\">\r\n");
      out.write("         <INPUT TYPE=\"hidden\" NAME=\"locale\" \r\n");
  if (commFwData.getLocale() != null)
    {

      out.write("\r\n");
      out.write("                                 VALUE=\"");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((com.csc.cfw.util.test.CommFwTestData)_jspx_page_context.findAttribute("commFwData")).getLocale())));
      out.write("\" \r\n");

    }

      out.write("\r\n");
      out.write(">\r\n");
      out.write("\r\n");
      out.write("         <TABLE>\r\n");
      out.write("\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write(" \r\n");
      out.write(" <script language=\"JavaScript1.2\" type=\"text/javascript\">\r\n");
      out.write("  // Global variable for subwindow reference\r\n");
      out.write("  var newWindow;\r\n");
      out.write("  var newWindowText;\r\n");
      out.write("  var newWindowTitle;\r\n");
      out.write("  var newWindowMimeType;\r\n");
      out.write("  \r\n");
      out.write("  // Generate and fill the new window\r\n");
      out.write("  function makeNewWindow(text, title, mimeType)\r\n");
      out.write("  {\r\n");
      out.write("    newWindowText = text;\r\n");
      out.write("    newWindowTitle = title;\r\n");
      out.write("    newWindowMimeType = mimeType;\r\n");
      out.write("    // make sure it isn't already opened\r\n");
      out.write("    if (!newWindow || newWindow.closed)\r\n");
      out.write("    {\r\n");
      out.write("      newWindow = window.open(\r\n");
      out.write("              \";\" + newWindowMimeType, \r\n");
      out.write("              \"sub\",\r\n");
      out.write("              \"resizable,dependent,scrollbars,,height=500,width=700\");\r\n");
      out.write("      // handle Navigator 2, which doesn't have an opener property\r\n");
      out.write("      if (!newWindow.opener)\r\n");
      out.write("      {\r\n");
      out.write("        newWindow.opener = window;\r\n");
      out.write("      }\r\n");
      out.write("      \r\n");
      out.write("      // delay writing until window exists in IE/Windows\r\n");
      out.write("      writeToWindow();\r\n");
      out.write("    }\r\n");
      out.write("    else if (newWindow.focus)\r\n");
      out.write("    {\r\n");
      out.write("      // window is already open and focusable, so bring it to the front\r\n");
      out.write("      newWindow.focus();\r\n");
      out.write("      writeToWindow();\r\n");
      out.write("    }\r\n");
      out.write("  }\r\n");
      out.write("  \r\n");
      out.write("  function writeToWindow()\r\n");
      out.write("  {\r\n");
      out.write("    newWindow.document.open(\"text/html\");\r\n");
      out.write("    newWindow.document.write(\"<html><title>\" + newWindowTitle + \"</title>\");\r\n");
      out.write("    newWindow.document.write(\"<pre>\");\r\n");
      out.write("    newWindow.document.write(xmlEncode(newWindowText, false));\r\n");
      out.write("    newWindow.document.write(\"</pre></html>\");\r\n");
      out.write("    newWindow.document.close();\r\n");
      out.write("  }\r\n");
      out.write("  \r\n");
      out.write("  function xmlEncode(input, skipEncAmp)\r\n");
      out.write("  {\r\n");
      out.write("    var len = input.length;\r\n");
      out.write("    var newBuff = \"\";\r\n");
      out.write("    var chars = input;\r\n");
      out.write("    var i;\r\n");
      out.write("    for (i = 0; i < len; i++)\r\n");
      out.write("    {\r\n");
      out.write("      var c = chars.charAt(i);\r\n");
      out.write("      if (c == '&' && skipEncAmp)\r\n");
      out.write("      {\r\n");
      out.write("        // If this is an ampersand, see if it is already\r\n");
      out.write("        // an \"&amp; entity reference.\r\n");
      out.write("        if ( ((i + 5) <= len)\r\n");
      out.write("           && chars.charAt(i+1) == 'a'\r\n");
      out.write("           && chars.charAt(i+2) == 'm'\r\n");
      out.write("           && chars.charAt(i+3) == 'p'\r\n");
      out.write("           && chars.charAt(i+4) == ';')\r\n");
      out.write("        {\r\n");
      out.write("          newBuff += \"&amp;\"; //$NON-NLS-1$\r\n");
      out.write("          i += 4;\r\n");
      out.write("          continue;\r\n");
      out.write("        }\r\n");
      out.write("      }\r\n");
      out.write("        \r\n");
      out.write("      switch (c)\r\n");
      out.write("      {\r\n");
      out.write("        case '&':\r\n");
      out.write("          newBuff += \"&amp;\"; //$NON-NLS-1$\r\n");
      out.write("          break;\r\n");
      out.write("        case '<':\r\n");
      out.write("          newBuff += \"&lt;\"; //$NON-NLS-1$\r\n");
      out.write("          break;\r\n");
      out.write("        case '>':\r\n");
      out.write("          newBuff += \"&gt;\"; //$NON-NLS-1$\r\n");
      out.write("          break;\r\n");
      out.write("        case '\"':\r\n");
      out.write("          newBuff += \"&quot;\"; //$NON-NLS-1$\r\n");
      out.write("          break;\r\n");
      out.write("        default:\r\n");
      out.write("          newBuff += c;\r\n");
      out.write("          break;\r\n");
      out.write("      }\r\n");
      out.write("    }\r\n");
      out.write("    \r\n");
      out.write("    return newBuff;\r\n");
      out.write("  }\r\n");
      out.write("\r\n");
      out.write(" </script>\r\n");
      out.write(" \r\n");
  if (commFwData.function != null)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <P />\r\n");
      out.write("            <DIV ALIGN=\"center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Transaction Results</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"center\">\r\n");

    if (commFwData.isResponseSecurityMasked())
    {

      out.write("\r\n");
      out.write("            <b>NOTE: Secure Logging enabled: The results were modified to mask secure fields.</b><br />\r\n");

    }

      out.write("\r\n");
      out.write("            <TEXTAREA COLS=\"75\" WRAP=\"");
      out.print(ServletUtil.xmlWrap(commFwData.getS3TransformValue()) );
      out.write("\" ROWS=\"20\" NAME=\"xmlDisplayTextArea\">");
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(commFwData.getResponseXml(),"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ );
      out.write("</TEXTAREA>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"left\">\r\n");
      out.write("              <A href=\"javascript:makeNewWindow(document.forms[1].xmlDisplayTextArea.value, 'Transaction Results', 'text/xml')\">Expand View</A>\r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TABLE BORDER=\"1\">\r\n");
      out.write("             <TR>\r\n");
      out.write("              <TD  >\r\n");
      out.write("               <B>Display Options:</B>\r\n");
      out.write("               <INPUT VALUE=\"requestXml\"   TYPE=\"RADIO\" NAME=\"XmlDisplayRadioButtonSet\" onClick=xmlTypeChanged(this) >Request XML\r\n");
      out.write("               <INPUT VALUE=\"responseXml\"  TYPE=\"RADIO\" NAME=\"XmlDisplayRadioButtonSet\" onClick=xmlTypeChanged(this) ");
      out.print(ServletUtil.setChecked(fExecute) );
      out.write(" >Response XML\r\n");
      out.write("              </TD>\r\n");
      out.write("             </TR>\r\n");
      out.write("            </TABLE>\r\n");
      out.write("            <HR WIDTH=\"100%\">\r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");

    String logData = commFwData.getLogContents(); 
    if (logData != null)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <P></P>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Transaction Log</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"logDisplayTextArea\">");
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ );
      out.write("</TEXTAREA>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"left\">\r\n");
      out.write("              <A href=\"javascript:makeNewWindow(document.forms[1].logDisplayTextArea.value, 'Transaction Log', 'text/plain')\">Expand View</A>\r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
   } 
      out.write("\r\n");
      out.write("\r\n");

    logData = commFwData.getLogInternalXmlReq(); 
    if (logData != null)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <P></P>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Internal Request XML</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"logIntReqDisplayTextArea\">");
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ );
      out.write("</TEXTAREA>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"left\">\r\n");
      out.write("              <A href=\"javascript:makeNewWindow(document.forms[1].logIntReqDisplayTextArea.value, 'Internal Request XML', 'text/xml')\">Expand View</A>\r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
   } 
      out.write("\r\n");
      out.write("\r\n");

    logData = commFwData.getLogInternalXmlRes();
    if (logData != null)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <P></P>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Internal Response XML</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"logIntResDisplayTextArea\">");
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ );
      out.write("</TEXTAREA>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <BR />\r\n");
      out.write("            <a href=\"javascript:makeNewWindow(document.forms[1].logIntResDisplayTextArea.value, 'Internal Response XML', 'text/xml')\">Expand View</a>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
   } 
      out.write("\r\n");
      out.write("\r\n");

    logData = commFwData.getLogProcessTrace(); 
    if (logData != null)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <P></P>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P><FONT SIZE=\"+1\"><STRONG><U>Process Trace</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("            <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"logProcTraceDisplayTextArea\">");
      out.print(FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ );
      out.write("</TEXTAREA>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"left\">\r\n");
      out.write("              <A href=\"javascript:makeNewWindow(document.forms[1].logProcTraceDisplayTextArea.value, 'Process Trace', 'text/plain')\">Expand View</A>\r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");
      out.write("\r\n");
   } 
      out.write("\r\n");
      out.write("\r\n");
  } 
      out.write("\r\n");
      out.write("\r\n");

    if (fExecute)
    {

      out.write("\r\n");
      out.write("          <TR>\r\n");
      out.write("           <TD  >\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("              <P><FONT SIZE=\"+1\"><STRONG><U>Transaction Run Statistics</U></STRONG></FONT></P>\r\n");
      out.write("            </DIV>\r\n");
      out.write("            <DIV ALIGN=\"Center\">\r\n");
      out.write("             <P>\r\n");
      out.write("              <TABLE BORDER=\"1\">\r\n");
      out.write("               <TR>\r\n");
      out.write("                <TH>Item</TH><TH>Time</TH><TH>Time from<BR/>Previous Step</TH>\r\n");
      out.write("               </TR>\r\n");

        Vector vec = new Vector();
        commFwData.getTestTransaction().getPerfResults(vec);
        Enumeration items = vec.elements();
        while (items.hasMoreElements())
        {
            Vector item = (Vector)items.nextElement();
            String intervalStg = (String)item.elementAt(1);
            String diffStg = "&nbsp;";
            int idx = intervalStg.indexOf(":");
            if (idx > 0)
            {
                diffStg = intervalStg.substring(idx + 1);
                intervalStg = intervalStg.substring(0,idx);
            }

      out.write("\r\n");
      out.write("               <TR>\r\n");
      out.write("                <TD>");
      out.print(item.elementAt(0));
      out.write("</TD><TD ALIGN=\"right\">");
      out.print(intervalStg);
      out.write("</TD><TD ALIGN=\"right\">");
      out.print(diffStg);
      out.write("</TD>\r\n");
      out.write("               </TR>\r\n");

        }

      out.write("\r\n");
      out.write("\r\n");
      out.write("              </TABLE>\r\n");
      out.write("             </P>\r\n");

    if (commFwData.getTestTransaction().getTranID() != null && commFwData.getTestTransaction().getTranID().length() > 0) 
    {

      out.write("             \r\n");
      out.write("             ID: ");
      out.print(commFwData.getTestTransaction().getTranID());
      out.write("\r\n");
      out.write("\r\n");

    }

      out.write("            \r\n");
      out.write("            </DIV>\r\n");
      out.write("           </TD>\r\n");
      out.write("          </TR>\r\n");

    }

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("         </TABLE>\r\n");
      out.write("         </FORM>\r\n");
      out.write("        </TD>\r\n");
      out.write("       </TR>\r\n");
      out.write("      </TABLE>\r\n");
      out.write("    </TD>\r\n");
      out.write("   </TR>\r\n");
      out.write("  </TABLE>\r\n");
      out.write(" </DIV>\r\n");
      out.write(" </BODY>\r\n");
      out.write("</HTML>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
