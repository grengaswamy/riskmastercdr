#------------------------------------------------------------------------------
# Create a DB2 JDBC Connection Pool Provider and Data Source script
#
#------------------------------------------------------------------------------
import sys

#------------------------------------------------------------------------------
# Print script usage
#------------------------------------------------------------------------------
def printUsage():
    print "Usage wsadmin CreateDB2JDBCConnPool.py  >>"
    print "          -db2path db2path >>"
    print "          -dbalias aliasname >>"
    print "          -dbname dbname >>"
    print "          -dbserver server >>"
    print "          -dbport port >>"
    print "          [-server server] >>"
    print "          [-node node] >>"
    print "          [-cell cell]"


#------------------------------------------------------------------------------
# Look for a DB2 Universal JDBC provider and create one if not found
#------------------------------------------------------------------------------
def createJDBCProv(cell, node, server, jdbcpath):
    db2provName = 'DB2 Universal JDBC Driver Provider'
    prov = None
    provlist = AdminConfig.getid('/JDBCProvider:/')
    try:
        provIdx = provlist.index(db2provName)
        prov = provlist[provIdx:(provlist[provIdx:].index('"') + 1)]
    except:
        scope = "Node=" + node
        if server != None:
            scope = scope + ",Server=" + server
        if cell != None:
            scope = "Cell=" + cell + "," + scope
        prov = AdminTask.createJDBCProvider('[-scope ' + scope + ' -databaseType DB2 -providerType "DB2 Universal JDBC Driver Provider" -implementationType "Connection Pool data source" -name "DB2 Universal JDBC Driver Provider" -description "Non-XA DB2 Universal JDBC Driver-compliant Provider. Datasources created under this provider support only 1-phase commit processing except inthe case where driver type 2 is used under WAS z/OS. On WAS z/OS driver type 2 uses RRS and supports 2-phase commit processing." -classpath ${DB2UNIVERSAL_JDBC_DRIVER_PATH}/db2jcc.jar;${UNIVERSAL_JDBC_DRIVER_PATH}/db2jcc_license_cu.jar;${DB2UNIVERSAL_JDBC_DRIVER_PATH}/db2jcc_license_cisuz.jar -nativePath ${DB2UNIVERSAL_JDBC_DRIVER_NATIVEPATH}]')
        AdminTask.setVariable('[-variableName DB2UNIVERSAL_JDBC_DRIVER_PATH -variableValue "' + jdbcpath + '/java" -scope ' + scope + ']')
        AdminTask.setVariable('[-variableName DB2UNIVERSAL_JDBC_DRIVER_NATIVEPATH -variableValue "' + jdbcpath + '/bin" -scope ' + scope + ']')

    print prov
    return prov


################################################################################
#
# Create a DB2 Universal Provider and Data Source
#
################################################################################

print ""
print ""
print "Create DB2 JDBC Connection Pool Data Provider and Data Source"
print ""


jdbcpath = None #'c:/IBM/SQLLIB'
dbaliasName = None #'db2a'
dbname = None #'PMSDB2A'
dbserver = None #'hostf.fsg.amer.csc.com'
dbserverport = None #'6032'
node = None #AdminControl.getNode()
cell = None #AdminControl.getCell()
server = None

argc = len(sys.argv)
if argc < 1:
	printUsage()
	sys.exit(101)	
else:
    idx = 0
    while idx < argc:
        arg = sys.argv[idx]
        if arg == "-db2path":
            idx = idx + 1
            jdbcpath = sys.argv[idx]
        elif arg == "-dbalias":
            idx = idx + 1
            dbaliasName = sys.argv[idx]
        elif arg == "-dbname":
            idx = idx + 1
            dbname = sys.argv[idx]
        elif arg == "-dbserver":
            idx = idx + 1
            dbserver = sys.argv[idx]
        elif arg == "-dbport":
            idx = idx + 1
            dbserverport = sys.argv[idx]
        elif arg == "-server":
            idx = idx + 1
            server = sys.argv[idx]
        elif arg == "-node":
            idx = idx + 1
            node = sys.argv[idx]
        elif arg == "-cell":
            idx = idx + 1
            cell = sys.argv[idx]
        else:
            sys.stderr.write("Invalid option:  " + arg + "\n")
            printUsage()			
            sys.exit(102)
        idx = idx + 1
    if dbaliasName == None or dbname == None or jdbcpath == None or dbserver == None or dbserverport == None:
        sys.stderr.write("Missing argument" + "\n")
        printUsage()
        sys.exit(103)
    if node == None:
        node = AdminControl.getNode()
    #if cell == None:
    #    cell = AdminControl.getCell()

    #------------------------------------------------------------------------------
    # Main program
    #------------------------------------------------------------------------------
    jdbcProv = createJDBCProv(cell, node, server, jdbcpath)
    AdminTask.createDatasource(jdbcProv, ['-name', 'Universal JDBC Driver DataSource ' + dbaliasName, '-jndiName', 'jdbc/' + dbaliasName, '-dataStoreHelperClassName', \
          'com.ibm.websphere.rsadapter.DB2UniversalDataStoreHelper', '-configureResourceProperties', \
          '[[databaseName java.lang.String ' + dbname + '] [driverType java.lang.Integer 4] [serverName java.lang.String ' + dbserver + '] [portNumber java.lang.Integer ' + dbserverport + ']]']) 
    AdminConfig.save()
