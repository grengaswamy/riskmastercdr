<%-- ----------------------------------------------------------------- --%>
<%-- This is an include JSP for XML result data for XML Test JSP's.    --%>
<%-- ----------------------------------------------------------------- --%>
 
 <script language="JavaScript1.2" type="text/javascript">
  // Global variable for subwindow reference
  var newWindow;
  var newWindowText;
  var newWindowTitle;
  var newWindowMimeType;
  
  // Generate and fill the new window
  function makeNewWindow(text, title, mimeType)
  {
    newWindowText = text;
    newWindowTitle = title;
    newWindowMimeType = mimeType;
    // make sure it isn't already opened
    if (!newWindow || newWindow.closed)
    {
      newWindow = window.open(
              ";" + newWindowMimeType, 
              "sub",
              "resizable,dependent,scrollbars,,height=500,width=700");
      // handle Navigator 2, which doesn't have an opener property
      if (!newWindow.opener)
      {
        newWindow.opener = window;
      }
      
      // delay writing until window exists in IE/Windows
      writeToWindow();
    }
    else if (newWindow.focus)
    {
      // window is already open and focusable, so bring it to the front
      newWindow.focus();
      writeToWindow();
    }
  }
  
  function writeToWindow()
  {
    newWindow.document.open("text/html");
    newWindow.document.write("<html><title>" + newWindowTitle + "</title>");
    newWindow.document.write("<pre>");
    newWindow.document.write(xmlEncode(newWindowText, false));
    newWindow.document.write("</pre></html>");
    newWindow.document.close();
  }
  
  function xmlEncode(input, skipEncAmp)
  {
    var len = input.length;
    var newBuff = "";
    var chars = input;
    var i;
    for (i = 0; i < len; i++)
    {
      var c = chars.charAt(i);
      if (c == '&' && skipEncAmp)
      {
        // If this is an ampersand, see if it is already
        // an "&amp; entity reference.
        if ( ((i + 5) <= len)
           && chars.charAt(i+1) == 'a'
           && chars.charAt(i+2) == 'm'
           && chars.charAt(i+3) == 'p'
           && chars.charAt(i+4) == ';')
        {
          newBuff += "&amp;"; //$NON-NLS-1$
          i += 4;
          continue;
        }
      }
        
      switch (c)
      {
        case '&':
          newBuff += "&amp;"; //$NON-NLS-1$
          break;
        case '<':
          newBuff += "&lt;"; //$NON-NLS-1$
          break;
        case '>':
          newBuff += "&gt;"; //$NON-NLS-1$
          break;
        case '"':
          newBuff += "&quot;"; //$NON-NLS-1$
          break;
        default:
          newBuff += c;
          break;
      }
    }
    
    return newBuff;
  }

 </script>
 
<%  if (commFwData.function != null)
    {
%>
          <TR>
           <TD  >
            <P />
            <DIV ALIGN="center">
             <P><FONT SIZE="+1"><STRONG><U>Transaction Results</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="center">
            <TEXTAREA COLS="75" WRAP="<%=ServletUtil.xmlWrap(commFwData.getS3TransformValue()) %>" ROWS="20" NAME="xmlDisplayTextArea"><%=FwSystem.replaceAll(FwSystem.replaceAll(commFwData.getResponseXml(),"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ %></TEXTAREA>
            </DIV>
            <DIV ALIGN="left">
              <A href="javascript:makeNewWindow(document.forms[1].xmlDisplayTextArea.value, 'Transaction Results', 'text/xml')">Expand View</A>
            </DIV>
           </TD>
          </TR>



          <TR>
           <TD  >
            <DIV ALIGN="Center">
            <TABLE BORDER="1">
             <TR>
              <TD  >
               <B>Display Options:</B>
               <INPUT VALUE="requestXml"   TYPE="RADIO" NAME="XmlDisplayRadioButtonSet" onClick=xmlTypeChanged(this) >Request XML
               <INPUT VALUE="responseXml"  TYPE="RADIO" NAME="XmlDisplayRadioButtonSet" onClick=xmlTypeChanged(this) <%=ServletUtil.setChecked(fExecute) %> >Response XML
              </TD>
             </TR>
            </TABLE>
            <HR WIDTH="100%">
            </DIV>
           </TD>
          </TR>

<%
    String logData = commFwData.getLogContents(); 
    if (logData != null)
    {
%>
          <TR>
           <TD  >
            <P></P>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Transaction Log</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
            <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="logDisplayTextArea"><%=FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ %></TEXTAREA>
            </DIV>
            <DIV ALIGN="left">
              <A href="javascript:makeNewWindow(document.forms[1].logDisplayTextArea.value, 'Transaction Log', 'text/plain')">Expand View</A>
            </DIV>
           </TD>
          </TR>

<%   } %>

<%
    logData = commFwData.getLogInternalXmlReq(); 
    if (logData != null)
    {
%>
          <TR>
           <TD  >
            <P></P>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Internal Request XML</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
            <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="logIntReqDisplayTextArea"><%=FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ %></TEXTAREA>
            </DIV>
            <DIV ALIGN="left">
              <A href="javascript:makeNewWindow(document.forms[1].logIntReqDisplayTextArea.value, 'Internal Request XML', 'text/xml')">Expand View</A>
            </DIV>
           </TD>
          </TR>

<%   } %>

<%
    logData = commFwData.getLogInternalXmlRes();
    if (logData != null)
    {
%>
          <TR>
           <TD  >
            <P></P>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Internal Response XML</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
            <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="logIntResDisplayTextArea"><%=FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ %></TEXTAREA>
            </DIV>
            <BR />
            <a href="javascript:makeNewWindow(document.forms[1].logIntResDisplayTextArea.value, 'Internal Response XML', 'text/xml')">Expand View</a>
           </TD>
          </TR>

<%   } %>

<%
    logData = commFwData.getLogProcessTrace(); 
    if (logData != null)
    {
%>
          <TR>
           <TD  >
            <P></P>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Process Trace</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
            <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="logProcTraceDisplayTextArea"><%=FwSystem.replaceAll(FwSystem.replaceAll(logData,"&","&amp;"),"<","&lt;")  /*jsp:getProperty name='commFwData' property='responseXml'/*/ %></TEXTAREA>
            </DIV>
            <DIV ALIGN="left">
              <A href="javascript:makeNewWindow(document.forms[1].logProcTraceDisplayTextArea.value, 'Process Trace', 'text/plain')">Expand View</A>
            </DIV>
           </TD>
          </TR>

<%   } %>

<%  } %>

<%
    if (fExecute)
    {
%>
          <TR>
           <TD  >
            <DIV ALIGN="Center">
              <P><FONT SIZE="+1"><STRONG><U>Transaction Run Statistics</U></STRONG></FONT></P>
            </DIV>
            <DIV ALIGN="Center">
             <P>
              <TABLE BORDER>
               <TR>
                <TH>Item</TH><TH>Time</TH><TH>Time from<BR>Previous Step</TH>
               </TR>
<%
        Vector vec = new Vector();
        commFwData.getTestTransaction().getPerfResults(vec);
        Enumeration items = vec.elements();
        while (items.hasMoreElements())
        {
            Vector item = (Vector)items.nextElement();
            String intervalStg = (String)item.elementAt(1);
            String diffStg = "&nbsp;";
            int idx = intervalStg.indexOf(":");
            if (idx > 0)
            {
                diffStg = intervalStg.substring(idx + 1);
                intervalStg = intervalStg.substring(0,idx);
            }
%>
               <TR>
                <TD><%=item.elementAt(0)%></TD><TD ALIGN="right"><%=intervalStg%></TD><TD ALIGN="right"><%=diffStg%></TD>
               </TR>
<%
        }
%>

              </TABLE>
             </P>
<%
    if (commFwData.getTestTransaction().getTranID() != null && commFwData.getTestTransaction().getTranID().length() > 0) 
    {
%>             
             ID: <%=commFwData.getTestTransaction().getTranID()%>

<%
    }
%>            
            </DIV>
           </TD>
          </TR>
<%
    }
%>


