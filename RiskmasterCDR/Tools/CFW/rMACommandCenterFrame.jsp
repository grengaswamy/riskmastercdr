<%--
**************************************************************************************************
* Programmer: Mark Williams                          		Date: 02/25/2009                   	 *                                  
* FSIT Issue:  114459                                 		Resolution : 46556                   *                                          
* Package   :  Proj50001, Proj50009															     *  
* Description: UI Modernization																     *
**************************************************************************************************
//*****************************************************************************************************
//* Programmer:  Mohammad Quamar Khursheed 	            	     Date: 04/02/2010                     *
//* FSIT Issue:   115393                              Resolution Number: 46774                        *
//* Description: Change in system override date so as to even allow blank entry into override date    *
//*****************************************************************************************************
//* Programmer : Tarun Srivastava								Date : 10/15/2012       *
//* FSIT Issue : 166122										Resolution : 64934      *
//* Description: Underwrite as you go integration of POINT IN rules to POINT IN J					*
//***********************************************************************************************************************
--%>
<%@ page import="com.csc.pt.util.PTFwException,com.csc.pt.util.PTFwSystem,com.csc.fw.util.FwSystem,com.csc.pt.helper.common.CommandCenterHelper"%>
<%@ page import="com.csc.pt.access.DbBean" %>
<%@ page import="java.util.Properties,com.csc.fw.util.PropertyFileManager "%>

<%!
public static final String SESSION_ATTRIBUTE_HELPER = "CommandCenterHelper";
%>

<%
Properties Clprop = new Properties();
Clprop =PropertyFileManager.current().getProperties("properties.POINT");
String firstTitle = Clprop.getProperty("ApplicationTitle");

FwSystem ptFwSystem = null;

Properties POINTProperties=null;
Properties CommandCenterProperties=null;
Properties SurroundProductsProperties=null;

DbBean db = null;
String ssouser = request.getParameter("pntUser");
String ssopwd = request.getParameter("pntPwd");
session.setAttribute("User",ssouser);
session.setAttribute("Password",ssopwd);

//Create cookies for this session. Cookies will expire after 24 hours.
Cookie usercki = new Cookie ("user",ssouser);
Cookie passcki = new Cookie ("pass",ssopwd);
response.addCookie(usercki);
response.addCookie(passcki);
System.out.println("*************************************************  INSIDE rMACommandCenterFrame.jsp ***********************************");
String userName = (String) session.getAttribute("User");
String passWord = (String)session.getAttribute("Password");
String systemOverrideDate = (session.getAttribute("SystemOverrideDate") == null) ? "" : (String)session.getAttribute("SystemOverrideDate");
String fullSystemOverrideDate = (String)session.getAttribute("DisplaySystemOverrideDate");
String allOpt = (String)session.getAttribute("AllOptions");
String[] userInformation;
String LOC = null;
String MCO = null;
String env = null;
String key = null;
String email = null;
String groupPrf = null;
boolean userfound = true;

userName = userName.toUpperCase();
String redirectURL ="";
if (userName.equalsIgnoreCase("")){
%>
	<jsp:forward page="../jsp/SysSessionTimeOut.jsp">
		<jsp:param value="Y" name="firstCall"/>
	</jsp:forward>
<%
}

ptFwSystem = PTFwSystem.current();
CommandCenterProperties = PropertyFileManager.current().getProperties("properties.CommandCenter");

CommandCenterHelper cmCenterHlp = null;	
String locationInd = request.getParameter("locationInd");
if(locationInd!= null && locationInd.equals("Y"))
{
	session.removeAttribute(SESSION_ATTRIBUTE_HELPER);
}
cmCenterHlp = (CommandCenterHelper)session.getAttribute(SESSION_ATTRIBUTE_HELPER);
if(cmCenterHlp == null)
{
	cmCenterHlp = new CommandCenterHelper();
	session.setAttribute(SESSION_ATTRIBUTE_HELPER, cmCenterHlp);
	ptFwSystem.getTraceLogger().println(PTFwSystem.LOG_LEVEL_DEBUG,"",PTFwSystem.formatLogMessage(userName,"CommandCenterFrame.jsp:: First access to command center. Created cache"));
}
ptFwSystem.getTraceLogger().println(PTFwSystem.LOG_LEVEL_DEBUG,"",PTFwSystem.formatLogMessage(userName,"CommandCenterFrame.jsp:: Caching is ON"));

POINTProperties=cmCenterHlp.getPOINTProperties();
SurroundProductsProperties=cmCenterHlp.getSurroundProductsProperties();

key = request.getParameter("Key");
if(key == null) {
	key = "LOGIN";
}

key= java.net.URLEncoder.encode(key.substring(0,key.length() -2), "UTF8") + key.substring(key.length() -2);

//* Connect to DBBean for JDBC calls.
db = new com.csc.pt.access.DbBean();
db.connect(userName, null, null);


LOC = request.getParameter("LOC");
MCO = request.getParameter("MCO");

cmCenterHlp.retrieveAll(userName, null, null,LOC,db);

if ((LOC == null) || (MCO == null)){
	LOC = "";
	MCO = "";
	//Get System override date
	//FSIT # 115393, Resolution # 46774 - Start
	//systemOverrideDate = cmCenterHlp.GetSystemOverrideDate(null, null, null, null);
	systemOverrideDate = cmCenterHlp.GetSystemOverrideDate(null, null, null, null)==null?"":cmCenterHlp.GetSystemOverrideDate(null, null, null, null);
	
	if(!systemOverrideDate.equals("")) {
	//FSIT # 115393, Resolution # 46774 - End
	fullSystemOverrideDate = systemOverrideDate.substring(3,5) + "/" + systemOverrideDate.substring(5,7) + "/" + systemOverrideDate.substring(1,3);
	//FSIT # 115393, Resolution # 46774 - Start
	} else {
		fullSystemOverrideDate = "  /  /  ";
	}
	//FSIT # 115393, Resolution # 46774 - End
	//Get the user profile information.
	userInformation = cmCenterHlp.GetUserInformation(userName, null, null, db);
	allOpt = userInformation[0];
	LOC = userInformation[1];
	MCO = userInformation[2];
	email = userInformation[3];
	System.out.println("Inside rMACommandCenterFrame -- " + userInformation);
	if(userInformation[4] == "N"){
		userfound = false;
	}
}


if (userfound){
	session.setAttribute("User",userName);
	session.setAttribute("Password", passWord);
	session.setAttribute("SystemOverrideDate",systemOverrideDate);
	session.setAttribute("AllOptions",allOpt);
	session.setAttribute("DisplaySystemOverrideDate",fullSystemOverrideDate);
	//Get enviorment name and set session var.
	env = POINTProperties.getProperty("environment");
	session.setAttribute("env",env);
	session.setAttribute("Email",email);
	//FSIT#166122 Resolution#64934 - Start
	Clprop.setProperty("SessionID",session.getId());
	//FSIT#166122 Resolution#64934 - End
	groupPrf = cmCenterHlp.getGroupPrf(userName, db);
	//Set session attribute if group was returned
	if(!groupPrf.equals(userName)) {
		session.setAttribute("GroupProfile", groupPrf);
	}
}
String policySymbol = request.getParameter("Symbol");
String policyNumber = request.getParameter("PolNum");
String policyModule = request.getParameter("Module");

String polAction = request.getParameter("PolAction");
String polEndDt = request.getParameter("PolEndDt");
String polEffDt = request.getParameter("PolEffDt");
String polExpDt = request.getParameter("PolExpDt");
String lob = request.getParameter("LOB");
String actionDesc = request.getParameter("actionDesc");
String srcLoc=request.getParameter("srcLoc");
String longname="";
String policyStatus="";


String urlString = "servlet/FirstServlet?Symbol="+policySymbol+"&PolNum="+policyNumber+"&Module="+policyModule+"&LOC="+LOC+"&MCO="+MCO+"&PolAction="+polAction+"&PolEndDt="+polEndDt+"&PolEffDt="+polEffDt+"&PolExpDt="+polExpDt+"&LOB="+lob+"&longname="+longname+"&policyStatus="+policyStatus;

%>


<HTML>
<HEAD>
	<TITLE>
		<%=firstTitle %>
	</TITLE>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Expires" content="-1">
</HEAD>


<script>
function changeTitle(title)
{
	var myvar = '<%=firstTitle%>'; 
	top.document.title= myvar + ' - ' + title;
}

function changeFrame(){
if(document.getElementById('FrameMain').src == ""){	document.getElementById('FrameMain').src = "<%=urlString%>";
}
}

function noop() {
	alert('<%=CommandCenterProperties.getProperty("NooptionMessage")%>');
	window.close();
	window.location = '../servlet/Logout';
}

function attachFlicker() {
//IE has strange issues when going full screen with navigation open.
//This stops the screen from going blank when pressing F11.
	var flicker = function() {
		top.document.getElementById('Frameset1').style.display = 'none';
		top.document.getElementById('Frameset1').style.display = 'block';
	};
	
	if(top.attachEvent) {
		top.attachEvent('onresize', flicker);
	}
	else if(top.addEventListener) {
		top.addEventListener('onresize', flicker);
	}
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

</script>

<%
if(!userfound) {
%>
<script>
	noop();
</script>
<%
}
System.out.println("******** " + urlString);
%>

<FRAMESET id="Frameset1" border="0" frameSpacing="0" frameBorder="1" rows="81,*" onload="attachFlicker();">
	<FRAME name="LogoFrame" marginWidth="0" marginHeight="0" src="jsp/MainHeader.jsp?Key=<%=key%>&LOC=<%=LOC%>&MCO=<%=MCO%>&srcLoc=<%=srcLoc%>" frameBorder="0" scrolling="no" >
		<FRAMESET id="Frameset2" border="0" frameSpacing="0" frameBorder="0" cols="220,*">
			<FRAME name="First" marginWidth="0" marginHeight="0" src="jsp/CommandCenter.jsp?Key=<%=key%>" frameBorder="0" scrolling="auto" onload="changeFrame();">
			<FRAME name="FrameMain" src="" frameBorder="0">
		</FRAMESET>
	<NOFRAMES></NOFRAMES>
</FRAMESET>
</HTML>
<%if (db!=null) db.close();%>