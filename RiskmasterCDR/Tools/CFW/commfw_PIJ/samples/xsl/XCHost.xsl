<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output  method="xml" indent="yes"/>
	
    <xsl:template match="/">
        <XCHost>
            <xsl:for-each select="/*/InsuranceSvcRq/*">
                <xsl:if test="local-name()!= 'RqUID'">
                <BUS__OBJ__RECORD>
                    <URQM__MSG__HDR>
                        <URQM__MSG__BUS__OBJ__NM><xsl:value-of select="local-name()"/></URQM__MSG__BUS__OBJ__NM>
                        <URQM__ACTION__CODE>FETCH</URQM__ACTION__CODE>
                        <URQM__BUS__OBJ__DATA__LENGTH/>
                    </URQM__MSG__HDR>
                    <xsl:copy-of select="."/>
                </BUS__OBJ__RECORD>
                </xsl:if>
            </xsl:for-each>
        </XCHost>
    </xsl:template>

</xsl:stylesheet>
