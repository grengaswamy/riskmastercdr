<process name="FaxDocument">

<!-- define variables 

	Note: internal variables
	RequestDOM   = Incoming XML Message as a DOM
	_data_       = Incoming XML Message as a string
	_source_     = Incoming XML Message as a DOMSource

	validMessage = result of test for ACORD message
	response     = response to be returned to the calling application
	docSolError  = Document Solution error message
-->

	<variables>
		<variable name="validMessage" type="xsd:boolean" />
		<variable name="response" type="xsd:element" />	
		<variable name="docSolError" type="xsd:string"/>			
	</variables>

<!--
	Initialize Document Solution error message
	Then query incoming message - if ACORD message, then condition will evaluate to true
-->

	<assign>
		<copy>
			<from expression="'Document Solutions Retrieve Document Failed'"/>
			<to variable="docSolError"/>
		</copy>
		<copy> 
		<to variable="validMessage"/>
		<from variable="RequestDom" query="/ACORD/InsuranceSvcRq/com.csc_ElectronicCommunicationSendRq/OtherIdentifier/OtherIdTypeCd != ''"/>		
		</copy>
	</assign>

<!--
	If ACORD message,
	 1.) Define variables for status information - input parameters to translate of
	 	internal response message to ACORD response message later in process
	 2.) Define variable to save original Request DOM (RequestDom_In)
	 3.) Define variable for name of xsl file for transform operation (TransformFileName - internal variable)
	 
	 4.) Action statement will execute transform of ACORD message to internal message
	 5.) Internal message is assigned to internal variable _data_
-->
		
	<switch>
		<case condition="validMessage = 'true'">
			<variables>
				<variable name="MsgStatusCd" type="xsd:string"/>
				<variable name="ExtendedStatusDesc" type="xsd:string"/>				
				<variable name="RequestDom_In" type="xsd:element"/>
				<variable name="TransformFileName" type="xsd:string"/>			
			</variables>
			<assign>
				<copy>
				<from variable="RequestDom"/>
				<to  variable="RequestDom_In"/>
				</copy>
				<copy>
				<from expression="'com.csc_ElectronicCommunicationSendRq'"/>
				<to variable="TransformFileName"/>
				</copy>
			</assign>

			<action name="ReqTransform" portType="CommFwAction" operation="commfw-req-xform">
					<Description>Transform Request XML to Internal XML</Description>
			</action>
			<assign>
				<copy>
				<from variable="_data_"/>
				<to variable="RequestDom"/>
				</copy>
			</assign>

		</case>
		<default>

		</default>
	</switch>
		
<!-- 
	import WSDL definition so the messages can be referenced in variable statement
-->	
	<import location="WSLocationDocumentRetrieve"/>

<!-- 
	Define variables for request and response messages - reference WSDL messages
	Define variables for result of operations
	Define variable to hold collection of attachments
	Define variables that will be used to let CFW know that specific message parts are referenced attachments		
-->

	<variables>
		<variable name="docRequestMessage" messageType="CDocPrintService.callDocumentSolutions"/>
		<variable name="docResponseMessage" messageType="CDocPrintService.callDocumentSolutionsResponse"/>
		<variable name="docResult" type="xsd:short"/>
		<variable name="faxResult" type="xsd:element"/>
		<variable name="attachments" type="cfw:attachments"/>
		<variable name="ExtrDataAttachHolder" type="cfw:attachmentPart"/>
		<variable name="XMLDataAttachHolder" type="cfw:attachmentPart"/>
		<variable name="OutPrtFileAttachHolder" type="cfw:attachmentPart"/>				
	</variables>
	
<!--
	Set message parts for Document Solution Request 
		'Retrieve' is the request for DS
		Values from incoming message include Unique ID and Environment
		The predefines attachment part variables are assigned to message parts 
		to indicate referenced attachments
-->	

	<assign>
		<copy>
		<from expression="'Retrieve'"/>
		<to variable="docRequestMessage" part="strRunType"/>
		</copy>
		<copy>
		<from variable="RequestDom" query="/FaxDocument/RequestId"/>
		<to variable="docRequestMessage" part="strUniqueId"/>
		</copy>
		<copy>
		<from variable="RequestDom" query="/FaxDocument/Environment"/>
		<to variable="docRequestMessage" part="strEnvrnmnt"/>
		</copy>
		<copy>
		<from variable="RequestDom" query="/FaxDocument/MIMEType"/>
		<to variable="docRequestMessage" part="strMIMEType"/>
		</copy>
<!-- attachment -->		
		<copy>
		<from variable="ExtrDataAttachHolder" />
		<to variable="docRequestMessage" part="ExtrDataAttach"/>
		</copy>
		<copy>
		<from variable="XMLDataAttachHolder" />
		<to variable="docRequestMessage" part="XMLDataAttach"/>
		</copy>
		<copy>
		<from variable="OutPrtFileAttachHolder" />
		<to variable="docRequestMessage" part="OutPrtFileAttach"/>
		</copy>						
		
	</assign>
<!--
	Invoke Document Solutions Web Service
	
	The location of the WSDL is the same as the previous import
	The portType and Operation must be defined in the WSDL
	The input variable and output variable refer to the previously defined variables
	
	On return form service, save message result in variable
-->
	<invoke name="DocumentSolutions-WS-Call" 
			portType="CDocPrintServiceSoapPort"
			operation="callDocumentSolutions"
			inputVariable="docRequestMessage"
			outputVariable="docResponseMessage"
			locate="WSLocationDocumentRetrieve"/>

	<assign>
		<copy>
		<from variable="docResponseMessage" part="Result"/>
		<to variable="docResult"/>
		</copy>
		</assign>
   
<!--
	Test return from Document Solutions
	If 0, then continue process
-->   
   
	<switch>
		<case condition="docResult = '0'">

<!--
	Prepare to send fax	
	import WSDL definition so the messages can be referenced in variable statement
-->		
			<import location="WSLocationFaxDocument"/>

<!-- 
	Define variables for request and response messages - reference WSDL messages	
-->
			<variables>
				<variable name="faxRequestMessage" messageType="INFSrvFaxDriver.FaxServiceSend"/>
				<variable name="faxResponseMessage" messageType="INFSrvFaxDriver.FaxServiceSendResponse"/>
			</variables>
<!--
	Copy attachment from Document Solutions to attachments collection
	Copy values from incoming message to message parts
-->
			<assign>
				<copy>
				<from variable="docResponseMessage" part="OutPrtFileAttach"/>
				<to variable="faxRequestMessage" part="ReceivedAttachment"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/RecipientName"/>
				<to variable="faxRequestMessage" part="RecipientName"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/RecipientAddress"/>
				<to variable="faxRequestMessage" part="RecipientAddress"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/RecipientCompany"/>
				<to variable="faxRequestMessage" part="RecipientCompany"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/RecipientFaxNumber"/>
				<to variable="faxRequestMessage" part="RecipientFaxNumber"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/FaxSubject"/>
				<to variable="faxRequestMessage" part="FaxSubject"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/FaxNote"/>
				<to variable="faxRequestMessage" part="FaxNote"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/SenderName"/>
				<to variable="faxRequestMessage" part="SenderName"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/SenderEmail"/>
				<to variable="faxRequestMessage" part="SenderEmail"/>
				</copy>
				<copy>
				<from variable="RequestDom" query="/FaxDocument/DocumentName"/>
				<to variable="faxRequestMessage" part="DocumentName"/>
				</copy>
				
				<copy>
				<from variable="RequestDom" query="/FaxDocument/NotifyEmail"/>
				<to variable="faxRequestMessage" part="NotifyEmail"/>
				</copy>

			</assign>
<!--
	Invoke Fax Web Service
	
	The location of the WSDL is the same as the previous import
	The portType and Operation must be defined in the WSDL
	The input variable and output variable refer to the previously defined variables
	
	On return form service, save message result in response variable
-->
			<invoke name="fax-ws-call"
				portType="INFSrvFaxDriverSoapPort"
				operation="FaxServiceSend"
				inputVariable="faxRequestMessage"
				outputVariable="faxResponseMessage"
				locate="WSLocationFaxDocument"/>

			<assign>
				<copy>
				<from variable="faxResponseMessage" part="Result"/>
				<to variable="response"/>
				</copy>
			</assign>	
    
<!-- Response for Fax 
	Start by testing for ACORD message
	
	If ACORD message,
	 1.) Set variables previously defined with status values from the email service
	 2.) Set variable to name of xsl file for transform operation (TransformFileName - internal variable)	 
	 3.) Action statement will execute transform of internal response to an ACORD message
	 4.) Internal message in _data_ (transform results) is assigned to internal variable response
-->
			<switch>
				<case condition="validMessage = 'true'">
					<assign>
					  <copy>
					  <from variable="response" query="Return/StatusCd"/>
					  <to variable="MsgStatusCd"/>
					  </copy>
					  <copy>
					  <from variable="response" query="Return/StatusDesc"/>
					  <to variable="ExtendedStatusDesc"/>
					  </copy>
					  <copy>
					  <from expression="'com.csc_ElectronicCommunicationSendRs'"/>
					  <to variable="TransformFileName"/>
					  </copy>			
					</assign>

					<action name="ResTransform" portType="CommFwAction" operation="commfw-res-xform">
					</action>
					
					<assign>
					  <copy>
					  <from variable="_data_"/>
					  <to variable="response"/>
					  </copy>
					</assign>					

				</case>
				<default>
			     
				</default>
			</switch>										
<!--  End Fax Response -->

			</case>
			
<!-- Response to Failed Document Solutions Retrieve -->
<!--
	Start by testing for ACORD message
	
	If ACORD message,
	 1.) Set variables previously defined with status values from the email service
	 2.) Set variable to name of xsl file for transform operation (TransformFileName - internal variable)	 
	 3.) Action statement will execute transform of internal response to an ACORD message
	 4.) Internal message in _data_ (transform results) is assigned to internal variable response
-->
			<default>

				<switch>
				<case condition="validMessage = 'true'">

					<assign>
					  <copy>
					  <from variable="docResult"/>
					  <to variable="MsgStatusCd"/>
					  </copy>
				 	  <copy>					  
					  <from variable="docSolError"/>
					  <to variable="ExtendedStatusDesc"/>
					  </copy>
					  <copy>
					   <from expression="'com.csc_ElectronicCommunicationSendRs'"/>
					   <to variable="TransformFileName"/>
					  </copy>
					</assign>

					<action name="ResTransform" portType="CommFwAction" operation="commfw-res-xform">
					</action>
					<assign>
					<copy>
					<from variable="_data_"/>
					<to variable="response"/>
					</copy>
					</assign>					

				</case>
				<default>
				
<!--
	Not ACORD message, so return Document Solutions error information by
	inserting return information directly into original request message	
-->	
					<assign>

					<copy>
					<from variable="RequestDom"/>
					<to variable="response"/>
					</copy>
					<copy>
					<from expression="-1"/>
					<to variable="response" xquery="/FaxDocument/StatusCd"/>
					</copy>	
					<copy>			  
					<from variable="docSolError"/>
					<to variable="ExtendedStatusDesc"/>
					</copy>

					</assign>
					
				</default>
				</switch>					

			</default>
		</switch>
</process>