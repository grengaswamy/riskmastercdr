#-------------------------------------------------------------------------------
#--- Create Communications Framework WebSphereMQ Queues for a specified context
#--- This is for z/OS MQ Series/WebSphereMQ. This provides suffixes and prefixes
#    to specify the qualified dataset names.
#-------------------------------------------------------------------------------


if {[llength $argv] < 7 || [llength $argv] > 10} {
    puts ""
    set tIncorrectNumArgs "Incorrect number of arguments."
    puts "$tIncorrectNumArgs"
    puts "USAGE: wsadmin -f CFWQueueSetup_WebSphereMQ.jacl context MQHost MQPort MQQueMan MQChannel HostQuePfx HostQueSfx [server] [node] [cell]"
    exit $FAIL
}

#-------------------------------------------------------------------------------
#--- Extract the arguments from the argument list and validate
#-------------------------------------------------------------------------------

if {[llength $argv] > 0} {
    set contextName   [lindex $argv 0]
    set MQHost        [lindex $argv 1]
    set MQPort        [lindex $argv 2]
    set MQQueMan      [lindex $argv 3]
    set MQChannel     [lindex $argv 4]
    set hostQuePfx    [lindex $argv 5]
    set hostQueSfx    [lindex $argv 6]
} 
set WServer ""
set WNode   ""
set WCell   ""
if {[llength $argv] > 7} {
    set WServer       [lindex $argv 7] 
} 
if {[llength $argv] > 8} {
    set WNode         [lindex $argv 8] 
} 
if {[llength $argv] > 9} {
    set WCell         [lindex $argv 9] 
} 
 

#-------------------------------------------------------------------------------
#--- Get the WebSphereMQ Resource Adapter
#-------------------------------------------------------------------------------
set cell [$AdminConfig showAttribute [$AdminConfig getid /Cell:$WCell/] name]
set node [$AdminConfig showAttribute [$AdminConfig getid /Node:$WNode/] name]
set server [$AdminConfig showAttribute [$AdminConfig getid /Server:$WServer/] name]
set jmsp [$AdminConfig getid "/Cell:$cell/Node:$node/Server:$server/JMSProvider:WebSphere MQ JMS Provider/"]

set queuePfx ${contextName}_

#-------------------------------------------------------------------------------
#--- Create the QueueConnectionFactory jms/ConnectionFactory
#-------------------------------------------------------------------------------
$AdminConfig attributes MQQueueConnectionFactory

set baseQManName     [list queueManager "$MQQueMan"]
set queueManagerHost [list host "$MQHost"]
set queueManagerPort [list port "$MQPort"]
set channelName      [list channel "$MQChannel"]
set xaEnable         [list XAEnabled "false"]
set transportType    [list transportType "CLIENT"]

set name [list name ${queuePfx}ConnectionFactory]
set jndi [list jndiName jms/${queuePfx}ConnectionFactory]
set mqqcfAttrs [list $name $jndi $xaEnable $transportType $baseQManName $queueManagerHost $queueManagerPort $channelName]
set template [lindex [$AdminConfig listTemplates MQQueueConnectionFactory] 0]
if {[catch {$AdminConfig createUsingTemplate MQQueueConnectionFactory $jmsp $mqqcfAttrs $template} result]} {
           puts "Connection Factory $jndiName is not created, exception = $result"
}
 
#-------------------------------------------------------------------------------
#--- Create the Queue objects
#-------------------------------------------------------------------------------
$AdminConfig attributes MQQueue

set baseQManName     [list baseQueueManagerName "$MQQueMan"]
set queueManagerHost [list queueManagerHost "$MQHost"]
set queueManagerPort [list queueManagerPort "$MQPort"]
set channelName      [list serverConnectionChannelName "$MQChannel"]

for {set x 0} {$x < 5} {incr x} {

    if {$x == 0} {
        #--- Create the CFWRequestQueue ----------------------------
        set queName ${queuePfx}CFWRequestQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWREQ${hostQueSfx}
        set queDesc "Communications Manager ${contextName} Request Queue"
    } elseif {$x == 1} {
        #--- Create the CFWResponseQueue ----------------------------
        set queName ${queuePfx}CFWResponseQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWRSP${hostQueSfx}
        set queDesc "Communications Manager ${contextName} Response Queue"
    } elseif {$x == 2} {
        #--- Create the CFWAsynchRequestQueue ----------------------------
        set queName ${queuePfx}CFWAsynchRequestQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWAREQ${hostQueSfx}
        set queDesc "Communications Manager ${contextName} Asynchronous Request Queue"
    } elseif {$x == 3} {
        #--- Create the CFWAsynchResponseQueue ----------------------------
        set queName ${queuePfx}CFWAsynchResponseQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWARSP${hostQueSfx}
        set queDesc "Communications Manager ${contextName} Asynchronous Response Queue"
    } elseif {$x == 4} {
        #--- Create the CFWAsynchErrorQueue ----------------------------
        set queName ${queuePfx}CFWAsynchErrorQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWAERR${hostQueSfx}
        set queDesc "Communications Manager ${contextName} Asynchronous Error Queue"
    } elseif {$x == 5} {
        #--- Create the CFWQueue (MMF) ----------------------------
        set queName ${queuePfx}CFWQueue
        set queJNDIName jms/${queName}
        set hostQueName ${hostQuePfx}CFWQUE${hostQueSfx}
        set queDesc "Communications ${contextName} Manager MMF Queue"
    }

    puts "Creating Queue $queName"
    set name [list name $queName]
    set desc [list description "$queDesc"]
    set jndi [list jndiName $queJNDIName]
    set baseQN [list baseQueueName "$hostQueName"]
#    set mqqAttrs [list $name $jndi $baseQN $desc $baseQManName $queueManagerHost $queueManagerPort $channelName]
    set mqqAttrs [list $name $jndi $baseQN $desc]

    set template [lindex [$AdminConfig listTemplates MQQueue] 0]
    set queue [$AdminConfig createUsingTemplate MQQueue $jmsp $mqqAttrs $template]
    puts $queue
}

#-------------------------------------------------------------------------------
#--- Save the configuration changes
#-------------------------------------------------------------------------------
$AdminConfig save
