<%@ page import="com.csc.cfw.codetrans.CodeTransGenerateTables" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.StringWriter" %>
<html>  
  <link href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <head>
  	<title>Code Translation Generator - Communications Framework</title>
    <meta name="GENERATOR" content="IBM WebSphere Studio"/>
  </head>
  <script language="JavaScript">
    function formsubmit()
    {
      var frm = document.ExeForm;
      document.ExeForm.submit();
    }
  </script>
  <body BGCOLOR="#C0C0C0">
    <br/><br/>
    <div align="center">
      <H1><U>Generate Code Translation Files</U></H1>
      <br/>
      <a href="CodeTransGen.jsp">Return to Generator</a>&nbsp;&nbsp;
      <a href="CodeTransMenu.jsp">Return to Code Translation Menu</a>
      <form name="ExeForm" method="post" action="CodeTransGen.jsp">
<%
        String qualifier = request.getParameter("qual");
        String userId = request.getParameter("username");
        String password=request.getParameter("pwd");
        String driver=request.getParameter("driver");
        if (driver == null)
            driver = "";
        String url_context=request.getParameter("url_context");
        if (url_context == null)
            url_context = "";
        String hostip=request.getParameter("hostip");
        if (hostip == null)
            hostip = "";
        String port=request.getParameter("port");
        if (port == null)
            port = "";
        String ssid=request.getParameter("ssid");
        if (ssid == null)
            ssid = "";
        String urlOptions=request.getParameter("urlOptions");
        if (urlOptions == null)
            urlOptions = "";
        String ConnectionStr[]= {
                userId,
                password,
                driver,
                url_context,
                hostip,
                port,ssid,
                qualifier,
                urlOptions};
        CodeTransGenerateTables tableGen = new CodeTransGenerateTables();
        StringWriter log = new StringWriter();
        PrintWriter prtLog = new PrintWriter(log);
        boolean ret = tableGen.generate(ConnectionStr, prtLog);
        prtLog.flush();
        if (ret) {
%>
        <div align="center">
          <H2>Files Successfully created.</H2>
<%			
        }
        else
        {
%>
          <H2>Error generating Code Translation Tables</H2>
<%			
            if (tableGen.getException() != null)
            {
%>
          <div align="left">
            <b>Error Information: </b><%=tableGen.getException().toString() %>
          </div>
<%			
            }
        }
%>
          <H3>Execution Results</H3>
          <table>
            <tr>
              <td><pre><%=log.toString()%></pre></td>
            </tr>
          </table>
        </div>
        <br />
      </form>
    </div>
  </body>
</html>
