<process name="BASVRUDTEXTRq">
	<variables>
		<variable name="inFileName" type="xsd:string" />
		<variable name="outFileName" type="xsd:string" />
		<variable name="inFile" type="cfw:inFile" />
		<variable name="outFile" type="cfw:outFile" />
		<variable name="loopFlag" type="xsd:boolean" />
		<variable name="teofFlag" type="xsd:boolean" />
		<variable name="internalDom" type="xsd:element" />
		<variable name="tranSize" type="xsd:int" />
		<variable name="fileSize" type="xsd:double" />
		<variable name="maxFileSize" type="xsd:double" />
		<variable name="maxFileSizeLoopFlag" type="xsd:boolean" />
		<variable name="fileNameSeq" type="xsd:int" />
		<variable name="tranContents" type="xsd:string" />
		<variable name="tranCounter" type="xsd:int" />
		<variable name="claimCount" type="xsd:int" />
		<variable name="policyCount" type="xsd:int" />
		<variable name="timeStamp" type="xsd:int" />
		<variable name="date" type="xsd:int" />
		<variable name="response" type="xsd:element" />
		<variable name="respString" type="xsd:string" />
		<variable name="respFiles" type="xsd:string" />

	</variables>

	<assign>
		<copy>
			<to variable="maxFileSize" />
			<!--from expression="149000000" /-->
			<from expression="149000000" />
		</copy>
		<copy>
			<to variable="maxFileSizeLoopFlag" />
			<from expression="'true'" />
		</copy>
		<copy>
			<to variable="fileNameSeq" />
			<from expression="1" />
		</copy>
		<copy>
			<to variable="inFileName" />
			<from variable="RequestDom" query="/BASVRUDTEXTRq/InputFilePath" />
		</copy>
		<copy>
			<to variable="inFile" />
			<from variable="inFileName" />
		</copy>
	</assign>

	<!-- Loop while we have record data to process -->
	<while condition="maxFileSizeLoopFlag = 'true'">

		<assign>
			<copy>
				<to variable="fileSize" />
				<from expression="0" />
			</copy>
			<copy>
				<to variable="maxFileSizeLoopFlag" />
				<from expression="'false'" />
			</copy>
			<copy>
				<to variable="loopFlag" />
				<from expression="'true'" />
			</copy>
			<copy>
				<to variable="outFileName" />
				<from variable="RequestDom" query="/BASVRUDTEXTRq/OutputFilePath" />
			</copy>
		</assign>

		<switch>
			<case condition="number(fileNameSeq) >= 1">
				<assign>
					<copy>
						<to variable="outFileName"/>
						<from expression="concat(outFileName, fileNameSeq)" />
					</copy>
				</assign>
			</case>
		</switch>

		<scope>

			<assign>
				<copy>
					<from expression="concat(outFileName,'.xml')"/>
					<to variable="outFileName" />		
				</copy>
				<copy>
					<to variable="outFile"/>
					<from variable="outFileName" />		
				</copy>
				<copy>
					<to variable="outFile"/>
					<from expression="'&lt;ACORD>'" />
				</copy>
				<copy>
					<from expression="concat(respFiles, 
concat(concat('&lt;ResponseFile>',outFileName),'&lt;/ResponseFile>'))"/>
					<to variable="respFiles"/>
				</copy>
			</assign>

			<switch>
				<case condition="tranContents != ''">
					<assign>
						<copy>
							<to variable="outFile"/>
							<from variable="tranContents" />
						</copy>
						<copy>
							<to variable="fileSize"/>
							<from expression="fileSize + tranSize" />
						</copy>
					</assign>
				</case>
			</switch>

			<!-- Loop while we have record data to process -->
			<while condition="(loopFlag = 'true') and not(teofFlag = 'true')">

				<action name="ResConversion" portType="CommFwAction" operation="commfw-res-conv">
					<Description>Convert Record Stream to Internal XML</Description>
					<output>
						<source property="inFile" />
					</output>
					<input property="internalDom" />
				</action>

				<action name="ResTransform" portType="CommFwAction" operation="commfw-res-xform">
					<Description>Transform Response XML to ACORD XML and write to output 
file</Description>
					<output>
						<source property="internalDom" />
					</output>
					<input property="tranContents" />
				</action>

				<assign>
					<copy>
						<to variable="tranSize" />
						<from expression="string-length(tranContents)" />
					</copy>
					<copy>
						<to variable="tranCounter" />
						<from expression="tranCounter + 1" />
					</copy>
					<copy>
						<to variable="teofFlag" />
						<from variable="internalDom" 
query="/BASVRUDTEXTRs/RECORD__NAME__ROW/RECORD__NAME = 'TEOF'"/>
					</copy>
				</assign>

				<switch>
					<case condition="(fileSize + tranSize) &lt; maxFileSize">
						<assign>
							<copy>
								<to variable="outFile"/>
								<from variable="tranContents" />
							</copy>
							<copy>
								<to variable="fileSize"/>
								<from expression="fileSize + tranSize" />
							</copy>
							<copy>
								<to variable="loopFlag" />
								<from 
expression="not(/BASVRUDTEXTRs/RECORD__NAME__ROW/RECORD__NAME = 'TEOF')"/>
							</copy>
						</assign>
					</case>

					<otherwise>
						<assign>
							<copy> 
								<to variable="loopFlag"/>
								<from expression="'false'" />
							</copy>
							<copy>
								<to variable="maxFileSizeLoopFlag" />
								<from expression="'true'" />
							</copy>
							<copy>
								<to variable="fileNameSeq" />
								<from expression="fileNameSeq + 1" />
							</copy>
						</assign>
					</otherwise>
				</switch>

				<switch>
					<case condition="teofFlag = 'true'">
						<assign>
							<copy>
								<to variable="claimCount" />
								<from variable="internalDom" 
query="/BASVRUDTEXTRs/TEOF__RECORD/Claim_Count"/>
							</copy>
							<copy>
								<to variable="policyCount" />
								<from variable="internalDom" 
query="/BASVRUDTEXTRs/TEOF__RECORD/Policy_Count"/>
							</copy>
							<copy>
								<to variable="timeStamp" />
								<from variable="internalDom" 
query="/BASVRUDTEXTRs/TEOF__RECORD/Time"/>
							</copy>
							<copy>
								<to variable="date" />
								<from variable="internalDom" 
query="/BASVRUDTEXTRs/TEOF__RECORD/Date"/>
							</copy>
						</assign>
					</case>

				</switch>

			</while>

			<assign>
				<copy> 
					<to variable="outFile"/>
					<from expression="'&lt;/ACORD>'" />		
				</copy>
			</assign>

		</scope>

	</while>

	<assign>
		<copy>
			<to variable="respString" />
			<from>&lt;BASVRUDTEXTRs>
				&lt;transaction_count/>
				&lt;policy_count/>
				&lt;claim_count/>
				&lt;timeStamp/>
				&lt;date/>
			</from>
		</copy>
		<copy>
			<to variable="respString" />
			<from expression="concat(respString,respFiles)" />
		</copy>
		<copy>
			<to variable="respString" />
			<from expression="concat(respString, '&lt;/BASVRUDTEXTRs>')" />
		</copy>
		<copy>
			<to variable="response" />
			<from variable="respString" />
		</copy>
		<copy>
			<to variable="response" query="/BASVRUDTEXTRs/transaction_count" />
			<from variable="tranCounter" />
		</copy>
		<copy>
			<to variable="response" query="/BASVRUDTEXTRs/claim_count" />
			<from variable="claimCount" />
		</copy>
		<copy>
			<to variable="response" query="/BASVRUDTEXTRs/policy_count" />
			<from variable="policyCount" />
		</copy>
		<copy>
			<to variable="response" query="/BASVRUDTEXTRs/timeStamp" />
			<from variable="timeStamp" />
		</copy>
		<copy>
			<to variable="response" query="/BASVRUDTEXTRs/date" />
			<from variable="date" />
		</copy>
	</assign>
</process>
