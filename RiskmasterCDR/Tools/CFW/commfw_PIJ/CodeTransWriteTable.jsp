<%@ page import="java.io.*" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<html>
    <link href="includes/INFUtil.css" type="text/css" rel="stylesheet">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">		
    </head>
    <body BGCOLOR="#C0C0C0">
    <H1><B>Code Translation Control Table Maintenance</B></H1>
<%
    String[] arrTableName=request.getParameterValues("TableName");
    String[] arrNativeColumn=request.getParameterValues("NativeColumn");
    String[] arrExternalColumn=request.getParameterValues("ExternalColumn");
    String path = (String)CommFwSystem.current().getProperty(CommFwSystem.SYSPROP_CodeTransPath);
    if (path != null && path.length() > 0)
    {
        path += '/' + CommFwSystem.CODE_TRANS_TABLE_LIST_FILENAME;
        //File outputFile = new File("D:\\Program Files\\IBM\\WebSphere\\AppServer\\profiles\\AppSrv01\\installedApps\\Q13N2K76Node01Cell\\commfw_8386_q13n2k11.ear\\commfw.war\\EBSI\\translationdata\\Table.xml");
        PrintWriter writer = null;
        try 
        {
            File outputFile = new File(path);
            
            outputFile.createNewFile();
            FileWriter outfile = new FileWriter(outputFile);
            writer = new PrintWriter(outfile);
            writer.println("<Root>");
            if (arrTableName != null)
                for (int i=0; i< arrTableName.length; i++)
                {	
                    writer.println("    <Table>");
                    writer.println("        <TableName>" + arrTableName[i] + "</TableName>");
                    writer.println("        <NativeColumn>" + arrNativeColumn[i] + "</NativeColumn>");
                    writer.println("        <ExternalColumn>" + arrExternalColumn[i] + "</ExternalColumn>");
                    writer.println("    </Table>");
                }
            
            writer.println("</Root>");
            writer.flush();
            writer.close();
            response.sendRedirect("CodeTransControlMaint.jsp");
        }
        catch( Exception er ) 
        { 
%>
<b>Error writing Code Translation control file:</b> <%= er.getMessage()%>
<%
            if (writer != null)
                writer.close();
        }
    }
    else
    {
%>
			<h2>Code Translation file path property CodeTransPath not defined in CommFw.properties</h2>
			<br />
<%
    }
			
%>
			<a href="CodeTransControlMaint.jsp">Return to Code Translation Control Maintenance</a>	

  </body>
</html>
