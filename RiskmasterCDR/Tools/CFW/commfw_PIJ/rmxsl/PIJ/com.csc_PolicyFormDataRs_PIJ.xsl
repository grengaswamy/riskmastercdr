<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
 <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_PolicyFormDataRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <xsl:apply-templates select="/POINTJDBCRs/Rows/Row" />
        </com.csc_PolicyFormDataRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>

<xsl:template match="/POINTJDBCRs/Rows/Row" >
    <xsl:if test="(string-length(OUT-Z1B8NB) > 0) and (string-length(OUT-ZRIETX) > 0)">
      <xsl:call-template name="DataRow"/>  
    </xsl:if>
  </xsl:template>

  <xsl:template name="DataRow">
    <form>
      <FormNumber>
        <xsl:value-of select="normalize-space(OUT-Z1B8NB)"/>
      </FormNumber>
      <FormName/>
      <FormDesc>
        <xsl:value-of select="normalize-space(OUT-ZRIETX)"/>
      </FormDesc>
      <EditionDt>
        <xsl:value-of select="OUT-ZRRBTX"/>
      </EditionDt>
      <IterationNumber>
		<xsl:value-of select="normalize-space(OUT-Z1MDATA)"/>
	  </IterationNumber>
      <FormTextContent>
        <xsl:value-of select="normalize-space(OUT-VRQHST)"/>
      </FormTextContent>
      <FormDataArea>
        <xsl:value-of select="normalize-space(OUT-Z1FDATA)"/>
      </FormDataArea>
      <com.csc_ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Stat</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1AAST)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Unit</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1B9NB)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Bldg</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1EGNB)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Loc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1BRNB)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1AGTX)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_EZScrn</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-Z1RDATA)"/>
          </OtherId>
        </OtherIdentifier>
      <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RATEOP</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-RATEOP)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_ENTRYDTE</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(OUT-ENTRYDTE)"/>
          </OtherId>
        </OtherIdentifier>
      </com.csc_ItemIdInfo>
    </form>
  </xsl:template>
</xsl:stylesheet>