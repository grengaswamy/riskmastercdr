<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <PT4JPROC>
      <xsl:call-template name="ProcName" />
    </PT4JPROC>
  </xsl:template>
  <xsl:template name= "ProcName" >
  <xsl:variable name="CancelPolicyCoverage">
   <xsl:value-of select="'0'"/>
   </xsl:variable>
    <xsl:variable name="lossdate">
      <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
        <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/LossDt"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Reporteddate">
      <xsl:call-template name="cvtdateMMDDYYYYtoCYYMMDD">
        <xsl:with-param name="datefld" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/ReportedDt"/>
      </xsl:call-template>
    </xsl:variable>
    <SQLStmt>
      CALL BASCLCOVL1('<xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/CompanyProductCd"/>
        <xsl:with-param name="size">3</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/PolicyNumber"/>
        <xsl:with-param name="size">7</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatIntParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
        <xsl:with-param name="size">5</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatIntParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>
        <xsl:with-param name="size">5</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatIntParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>
        <xsl:with-param name="size">5</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="$lossdate"/>
        <xsl:with-param name="size">7</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatIntParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
        <xsl:with-param name="size">30</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatIntParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>
        <xsl:with-param name="size">6</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="$Reporteddate"/>
        <xsl:with-param name="size">7</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="$CancelPolicyCoverage"/>
         <xsl:with-param name="size">1</xsl:with-param>
      </xsl:call-template>
	  ')
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>
