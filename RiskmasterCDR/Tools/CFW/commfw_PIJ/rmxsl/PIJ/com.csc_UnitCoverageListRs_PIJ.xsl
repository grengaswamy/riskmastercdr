<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="lob" select="default"/>
  <xsl:param name="baseLOBline" select="default"/>
  <xsl:param name="issue-cd" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="client-file" select="default"/>
  
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_UnitCoverageListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
		    <Policy id=''>
            <PolicyNumber>
              <xsl:value-of select="normalize-space(//KEY__POLICY__NUMBER)"/>
            </PolicyNumber>
            <CompanyProductCd>
              <xsl:value-of select="normalize-space(//KEY__SYMBOL)"/>
            </CompanyProductCd>
            <LOBCd>
              <xsl:value-of select="normalize-space($lob)"/>
            </LOBCd>
            <com.csc_BaseLOBLine>
              <xsl:value-of select="normalize-space($baseLOBline)"/>
            </com.csc_BaseLOBLine>
            <com.csc_ActionCd></com.csc_ActionCd>
            <com.csc_IssueCd>
              <xsl:value-of select="normalize-space($issue-cd)"/>
            </com.csc_IssueCd>
          </Policy>
          <xsl:apply-templates select="/POINTJDBCRs/Rows/Row"/>
        </com.csc_UnitCoverageListRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template match="/POINTJDBCRs/Rows/Row" >
    <xsl:if test="string-length(MAJPERIL) > 0 and string-length(COVSEQ) > 0">
      <xsl:call-template name="CovRow"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="CovRow">
    <com.csc_CoverageLossInfo id="">
      <ActionCd>A</ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(UNITNO)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(COVSEQ)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_TrxnSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(TRANSSEQ)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(STATE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(PRODUCT)"/>
          </OtherId>
        </OtherIdentifier>
		<OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(INSLINE)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(MAJPERIL)"/>
        </CoverageCd>
        <CoverageDesc>
          <xsl:value-of select="normalize-space(MAJPERILDESC1)"/>
        </CoverageDesc>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMITOCCUR"/>
              </xsl:call-template>

            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>OccurenceLimit</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMITAGGER"/>
              </xsl:call-template>
            
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>AggregateLimit</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVA"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVA</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVB"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVB</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVC"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVC</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVD"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVD</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVE"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVE</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="LIMIT-COVF"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>LIMIT-COVF</LimitAppliesToCd>
        </Limit>  
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(TOTALORIG)"/>
        </CurrentTermAmt>
        <com.csc_OriginalPremium>
          <xsl:value-of select="normalize-space(ORIGINAL)"/>
        </com.csc_OriginalPremium>
        <com.csc_WrittenPremium>
          <xsl:value-of select="normalize-space(PREMIUM)"/>
        </com.csc_WrittenPremium>
        <com.csc_TotalPremium>
          <xsl:value-of select="normalize-space(TOTALPREM)"/>
        </com.csc_TotalPremium>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="DEDUCTIBLE"/>
              </xsl:call-template>
              </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <ExcessWorkCompDeductible>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:call-template name="remove-leading-zeros">
                <xsl:with-param name="value" select="WC07-DED-AMT"/>
              </xsl:call-template>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <com.csc_AggrAmt>
            <xsl:call-template name="remove-leading-zeros">
              <xsl:with-param name="value" select="WC07-DED-AGG"/>
            </xsl:call-template>
          </com.csc_AggrAmt>
        </ExcessWorkCompDeductible>
        <EffectiveDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="COVEFFDTE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="COVEXPDTE"/>
          </xsl:call-template>
        </ExpirationDt>
        <com.csc_StateProvCd/>
        <com.csc_Exposure>
          <xsl:call-template name="remove-leading-zeros">
            <xsl:with-param name="value" select="EXPOSURE"/>
          </xsl:call-template>

        </com.csc_Exposure>
       
        <com.csc_Commission/>
        <com.csc_CommissionAmt/>
        <TerritoryCd/>
		
	 <ClaimsMadeInd>
          <xsl:value-of select="normalize-space(BC-ISCLAIMSMADE)"/>
        </ClaimsMadeInd>
	
      </Coverage>
      <com.csc_IssueCd/>
      <com.csc_ClassCd>
        <xsl:value-of select="normalize-space(CLASSNUM)"/>
      </com.csc_ClassCd>
      <com.csc_ClassDesc>
        <xsl:value-of select="normalize-space(CLASSDESC)"/>
      </com.csc_ClassDesc>
      <com.csc_PartCd/>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(COVSTATUS)"/>
      </com.csc_RecordStatus>
      <com.csc_AnnualStatement>
		<xsl:value-of select="normalize-space(ASLINE)"/>
	  </com.csc_AnnualStatement>
      <com.csc_ProductLine>
		<xsl:value-of select="normalize-space(PRODLINE)"/>
	  </com.csc_ProductLine>
      <com.csc_Subline>
        <xsl:value-of select="normalize-space(SUBLINE)"/>
      </com.csc_Subline>
      <com.csc_SublineDesc>
        <xsl:value-of select="normalize-space(SUBLINDESC)"/>
      </com.csc_SublineDesc>
      <com.csc_BureauType/>
      <com.csc_TaxLoc/>
      <com.csc_ExtendDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="EXTENDDTE"/>
        </xsl:call-template>
      </com.csc_ExtendDt>
      <com.csc_RetroDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="RETRODTE"/>
        </xsl:call-template>
      </com.csc_RetroDt>
      <com.csc_ManualEntryInd/>
      <com.csc_FinancialInd/>
      <com.csc_DropRecordInd/>
      <com.csc_TrxnDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="TRANSDTE"/>
        </xsl:call-template>
      </com.csc_TrxnDt>
      <com.csc_FinancialInd/>
      <com.csc_ReInsuranceInd><xsl:value-of select="normalize-space(REINSIND)"/></com.csc_ReInsuranceInd>
      <com.csc_EntryDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="ENTRYDTE"/>
        </xsl:call-template>
      </com.csc_EntryDt>
      <com.csc_AcctDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYY">
          <xsl:with-param name="datefld" select="ACCTGDTE"/>
        </xsl:call-template>
      </com.csc_AcctDt>
      <com.csc_ChangeDt/>
      <com.csc_AuditFrequency/>
      <com.csc_BillingInd/>
    </com.csc_CoverageLossInfo>
  </xsl:template>
</xsl:stylesheet>
