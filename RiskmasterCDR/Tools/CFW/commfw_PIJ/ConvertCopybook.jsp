<%@page import="com.csc.cfw.util.CommFwSystem" %>
<%@page import="com.csc.fw.util.ServletUtil" %>

<%@ page language="java" import="org.apache.commons.fileupload.*, java.util.List, java.io.File, java.util.Iterator, java.lang.reflect.Method" contentType='text/html'%>


<jsp:useBean id='copybookData' scope='session' class='com.csc.cfw.util.CopybookConverterBean'/>

<%
		String path = request.getContextPath();
		String basePath = 
			request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		String recPath =
			CommFwSystem.current().getProperty("RecordUrlPath").toString();
		String error = null;
//		boolean fBrowseServer = false;
		boolean fExecuteConvert = false;
		File savedFile = null;
		FileItem savedFileItem = null;
		
		//String dir = new File(copybookData.getOutName()).list;

		if (recPath.endsWith("/"))
		{
			recPath = recPath.substring(0, recPath.length() - 1);

		}

		int temp = (recPath.indexOf(path) + path.length());
		recPath = recPath.substring(temp);
		if (copybookData.getFunction() == null)
			copybookData.setOutName(
				getServletConfig().getServletContext().getRealPath(recPath));

		boolean fExecute = (request.getMethod().equals("POST"));

		if (fExecute)
		{

			// set the default values for the data bean 
			//if ((copybookData.getFunction() == null)
			//	&& copybookData.getFunction() != "fromBrowser")
			//	copybookData.newConvert();

			// first check if the upload request coming in is a multipart request
			boolean isMultipart = FileUpload.isMultipartContent(request);

			// if not, send to message page with the error message

			if (isMultipart)
			{
				//		request.setAttribute("msg", "Request was not multipart!");
				//		request.getRequestDispatcher("error.jsp").forward(request, response);
				//		return;

				// now lets create a handler for the upload request.
				DiskFileUpload upload = new DiskFileUpload();

				// parse this request by the handler
				// this gives us a list of items from the request
				List items = upload.parseRequest(request);

				// now iterate over this list
				Iterator itr = items.iterator();

				while (itr.hasNext())
				{

					FileItem item = (FileItem) itr.next();

					// check if the current item is a form field or an uploaded file
					if (item.isFormField())
					{

						// get the name of the field
						String fieldName = item.getFieldName();

						try
						{
							String setterName =
								"set"
									+ Character.toUpperCase(
										fieldName.charAt(0))
									+ fieldName.substring(1);
							Method setter =
								copybookData.getClass().getMethod(
									setterName,
									new Class[] { String.class });
							setter.invoke(
								copybookData,
								new Object[] { item.getString()});
						}
						catch (Exception e)
						{
						}

						if (fieldName.equals("outName"))
						{
							copybookData.setOutName(item.getString());
						}
//						if (fieldName.equals("function")
//							&& ("Browse Server..."
//								.equals(copybookData.function)))
//						{
//							fBrowseServer = true;
//						}
					}

					else
					{
						// the item must be an uploaded file
						// save it to disk

						String fieldName = item.getFieldName();

						if (fieldName.equals("inName"))
						{
							String fieldValue = item.getName();
							if (fieldValue
								!= null & (fieldValue.length() != 0))
							{
								String fileSep = System.getProperty("file.separator");
								String uploadPath = null;
								uploadPath = (String)CommFwSystem.current().getProperty("COMMFWLOGFOLDER");
								if (uploadPath == null)
								{
									uploadPath = System.getProperty("java.io.tmpdir");
									if (uploadPath == null)
										uploadPath = System.getProperty("user.home");
								}
								
								if (uploadPath.endsWith(fileSep) || uploadPath.endsWith("/"))
									uploadPath = uploadPath.substring(0, uploadPath.length() - 1);
								
								String copybookDir = uploadPath + fileSep + "copybooks";
								File dir = new File(copybookDir);
								if (!dir.isDirectory())
								{
									if (!dir.mkdirs())
									{
										// Could not create directory, use uploadPath
										copybookDir = uploadPath;
										dir = new File(uploadPath);
									}
								}
								
								File fullFile = new File(fieldValue);
								savedFile =
									new File(
										copybookDir + fileSep,
										fullFile.getName());
								copybookData.setInName(
									savedFile.toString());
								if (item.getSize() != 0)
									savedFileItem = item;
								else
									error = "No Copybook file was uploaded: " + fullFile.getName();
							}
						}
						//			else if (fieldName.equals("outName"))
						//			{
						//			copybookData.setOutName(savedFile.toString());
						//			}

					}
				}
				
//				if (fBrowseServer)
//				{
//					pageContext.forward("");
//					return;
//				}

				if ("Convert Copybook".equals(copybookData.function)
					&& savedFileItem != null)
				{
					fExecuteConvert = true;
					savedFileItem.write(savedFile);
				}
			}
		}
		else
		{
			//copybookData.newConvert();
		}

    

%>
<jsp:setProperty name='copybookData' property='*' />
<%
		if (fExecute && fExecuteConvert)
		{
			error = copybookData.callCopybookConverter();
			if (error == null || "OK".equals(error))
			{
				// No error
				error = null;
				String oname = copybookData.getOutName();
				copybookData.resetAllData();
				if (oname == null || oname.length() == 0)
					copybookData.setOutName(
						getServletConfig().getServletContext().getRealPath(recPath));
				else
					copybookData.setOutName(oname);
			}
		}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Convert Copybook to Java Record XML</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <META http-equiv="Content-Type" content="text/html" charset="UTF-8" />
    <LINK href="includes/INFUtil.css" type=text/css rel=stylesheet>
  </head>
  
 <BODY BGCOLOR="#C0C0C0">
 <SCRIPT LANGUAGE="JAVASCRIPT">
  <!--%= copybookData.getXmlStringsAsJS() %-->
 </SCRIPT>
 <DIV ALIGN="Center" ID="HtmlDiv1">
  <TABLE>
   <TR>
    <TD ALIGN="Center"  >
     <H2>Copybook Converter</H2>
     <HR WIDTH="100%">
    </TD>
   </TR>
<%
		if (error != null)
		{
%>
   <TR>
    <TD width="120"><FONT SIZE="+1"><B>Error Converting Copybook: </B></FONT><%=error%></TD>
   </TR>
<%
		}
%>
   <TR>
    <TD  >
     <FORM METHOD="POST" NAME="CopybookForm" ACTION="ConvertCopybook.jsp"  enctype="multipart/form-data">

      <TABLE>
       <TR ALIGN="Center">
        <TD ALIGN="Center"  >
         <TABLE>
          <TR>
           <TD>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Converter Options</U></STRONG></FONT></P>
            </DIV>
          <TR>
          <TD>
              <TABLE>
                <TR>
                 <TD>
                  <TABLE>
                   <TD VALIGN="center"><B>Operating System:<br>(Default is MVS)</B></TD>
                   <TD> 
                  	<SELECT NAME="systemType" SIZE="1">
                   		<OPTION VALUE="MVS" <%=ServletUtil.setSelected("MVS",String.valueOf(copybookData.getSystemType())) %> >MVS
                   		<OPTION VALUE="NT" <%=ServletUtil.setSelected("NT",String.valueOf(copybookData.getSystemType())) %> >NT
                  	</SELECT>
				   </TD>
                  </TABLE>
                 </TD>
                 <TD>
                  <TABLE>
                   <TR>
                    <TD VALIGN="center">
                       <INPUT VALUE="Y" TYPE="CHECKBOX" NAME="initNumeric" <%=ServletUtil.setChecked("Y", copybookData.getInitNumeric()) %> ><B>Initalize Numeric Fields</B>                       
                    </TD>
                   </TR>
                   <TR>
                    <TD VALIGN="center">
                    <INPUT VALUE="Y" TYPE="CHECKBOX" NAME="cvtUppercase" <%=ServletUtil.setChecked("Y", copybookData.getCvtUppercase()) %> ><B>Convert Values to Uppercase</B>                    
                    </TD>
                   </TR>
                  </TABLE>
                 </TD>
                </TR>
                <TR>
                 <TD>
                  <TABLE>
                  <TR>
                   <TD VALIGN="center"><B>Code page:<br>(Default is 037)</B></TD>
                   <TD> 
                    <!--INPUT NAME="codepage" VALUE="<jsp:getProperty name='copybookData' property='codepage'/>" SIZE="5"-->
                    <INPUT NAME="codepage" VALUE="<%=copybookData.getCodepage() != null ? copybookData.getCodepage() : "" %>" SIZE="5">
				   </TD>
				   </TR>
                  </TABLE>
                 </TD>
                 <TD>
                  <TABLE> 
                   <TR>
                    <TD>

                    <INPUT VALUE="Y" TYPE="CHECKBOX" NAME="keepWhitespace" <%=ServletUtil.setChecked("Y", copybookData.getKeepWhitespace()) %> ><B>Keep all white space in the values</B>

                    </TD>
                   </TR>
                   <TR>
                    <TD>

                    <INPUT VALUE="Y" TYPE="CHECKBOX" NAME="ignoreDataConError" <%=ServletUtil.setChecked("Y", copybookData.getIgnoreDataConError()) %> ><B>Suppress XML Errors</B>

                    </TD>
                   </TR>
                   
                  </TABLE>
                 </TD>
                </TR>
               </TABLE>
                 </TD>
                </TR>

          <TR>
           <TD>
           <HR WIDTH="100%">
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Copybook input and output files</U></STRONG></FONT></P>
            </DIV>
            <BR><B>Enter Output Record file path below:</B><BR>
            <!--%=setValue(Sting.valueOf(getServletConfig().getServletContext().getRealPath("/")) %-->
            <INPUT TYPE="INPUT" NAME="outName" 
                   VALUE="<jsp:getProperty name='copybookData' property='outName'/>"  
                   SIZE="50">                  
           <!--INPUT VALUE="Browse Server..." TYPE="SUBMIT" NAME="function"--> 
           <P><B>Enter Copybook file path below:</B><BR>
            <INPUT TYPE="FILE" NAME="inName"
                   VALUE="XYZ"  
                   SIZE="50"></P>
           
                   <!--VALUE=jsp:getProperty name='copybookData' property='inName'/>"-->           		        
           </TD>
          <TR>
           <TD><!--INPUT TYPE="TEXT" VALUE = ""-->
           </TD>
          </TR>


          <TR>
           <TD ALIGN="Left"  >
            <INPUT VALUE="Convert Copybook" TYPE="SUBMIT" NAME="function">
            <HR WIDTH="100%">
           </TD>
          </TR>
          
         </TABLE>
        </TD>
       </TR>
      </TABLE>
     </FORM>
    </TD  >
   </TR>
  </TABLE> 
</body>
</html>
