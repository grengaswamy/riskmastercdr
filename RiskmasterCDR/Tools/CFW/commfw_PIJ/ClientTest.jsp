<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP to Test a Client Locate and Add Transaction.        --%>
<%-- The XML will be generated.                                        --%>
<%-- ----------------------------------------------------------------- --%>
<%@page language='java' contentType='text/html'%>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Vector" %>
<%@page import="com.csc.fw.util.FwSystem" %>
<%@page import="com.csc.fw.util.ServletUtil" %>
<%@ page pageEncoding="UTF-8" %>

<jsp:useBean id='commFwData' scope='page' class='com.csc.cfw.util.test.ApplicationData'/>
<jsp:setProperty name='commFwData' property='*' />
<%
boolean fExecute = (commFwData.function != null);
boolean fForceValidationOff = true;
commFwData.newTest();
commFwData.buildFormParmsFromRequest(request);
commFwData.getAuthCredentials(request, session);
if (fExecute)
{
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", request.getParameter("noTransformCache")));
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
	commFwData.setXslTraceOptions(request.getParameterValues("xslTraceOptions"));
	commFwData.execute();
}
%>

<HTML>
 <HEAD>
  <META http-equiv="Content-Type" content="text/html" charset="UTF-8" />
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <TITLE>Client Test<%=fExecute ? " Response" : "" %> - Communications Framework</TITLE>
 </HEAD>
 <BODY BGCOLOR="#C0C0C0">
 <SCRIPT LANGUAGE="JAVASCRIPT">
  <%= commFwData.getXmlStringsAsJS() %>
 </SCRIPT>
 <DIV ALIGN="Center" ID="HtmlDiv1">
  <TABLE>
   <TR>
    <TD ALIGN="Center"  >
     <H4>Communications Framework Client Test</H4>
     <HR WIDTH="100%">
    </TD>
   </TR>
   <TR>
    <TD  >
     <!--FORM METHOD="POST" NAME="CommFwForm" ACTION="ClientTest.jsp"-->

      <TABLE>
       <TR ALIGN="Center">
        <TD ALIGN="Center"  >
         <FORM METHOD="POST" NAME="CommFwForm" ACTION="ClientTest.jsp">
         <TABLE>
          <TR>
           <TD WIDTH="100%"  >
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Transaction XML Data</U></STRONG></FONT></P>
            </DIV>
            <TABLE BORDER="2" WIDTH="100%">
             <TR>
              <TD WIDTH="100%"  >
               <TABLE WIDTH="100%">
                <TR>
                 <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Name</FONT></FONT></B></TD>
                </TR>
                <TR>
                 <TD WIDTH="100%"  >
                  <TABLE>
                   <TR>
                    <TD  >
                     <TABLE>
                      <TR>
                       <TD  >First:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="personNameFirstName" VALUE="<%=commFwData.getFormParameter("personNameFirstName")%>"></TD>
                      </TR>

                     </TABLE>
                    </TD>
                    <TD  >
                     <TABLE>
                      <TR>
                       <TD  >Middle:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="personNameMiddleName" VALUE="<%=commFwData.getFormParameter("personNameMiddleName")%>"></TD>
                      </TR>

                     </TABLE>
                    </TD>
                    <TD  >
                     <TABLE>
                      <TR>
                       <TD  >Last:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="personNameLastName" VALUE="<%=commFwData.getFormParameter("personNameLastName")%>"></TD>
                      </TR>

                     </TABLE>
                    </TD>
                   </TR>
                  </TABLE>
                 </TD>
                </TR>
               </TABLE>
              </TD>
             </TR>
            </TABLE>
           </TD>
          </TR>


          <TR>
           <TD  >
            <TABLE BORDER="2" WIDTH="100%">
             <TR>
              <TD WIDTH="100%"  >
               <TABLE WIDTH="100%">
                <TR>
                 <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Address</FONT></FONT></B></TD>
                </TR>
                <TR>
                 <TD WIDTH="100%"  >
                  <TABLE BORDER="0">
                   <TR>

                    <TD VALIGN="Top"  >
                     <TABLE>
                      <TR>
                       <TD  >Street line1:</TD>
                       <TD  ><INPUT SIZE="15" TYPE="TEXT" NAME="customerAddressAddr1" VALUE="<%=commFwData.getFormParameter("customerAddressAddr1")%>"></TD>
                      </TR>
                      <TR>
                       <TD  >Street line 2:</TD>
                       <TD  ><INPUT SIZE="15" TYPE="TEXT" NAME="customerAddressAddr2" VALUE="<%=commFwData.getFormParameter("customerAddressAddr2")%>"></TD>
                      </TR>
                     </TABLE>
                    </TD>

                    <TD VALIGN="Top"  >
                     <TABLE>
                      <TR>
                       <TD  >City:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="customerAddressCity" VALUE="<%=commFwData.getFormParameter("customerAddressCity")%>"></TD>
                      </TR>
                      <TR>
                       <TD  >State:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="customerAddressStateProv" VALUE="<%=commFwData.getFormParameter("customerAddressStateProv")%>"></TD>
                      </TR>
                     </TABLE>
                    </TD>

                    <TD VALIGN="Top"  >
                     <TABLE>
                      <TR>
                       <TD  >Postcode:</TD>
                       <TD  ><INPUT SIZE="10" TYPE="TEXT" NAME="customerAddressPostalCode" VALUE="<%=commFwData.getFormParameter("customerAddressPostalCode")%>"></TD>
                      </TR>
                     </TABLE>
                    </TD>

                   </TR>
                  </TABLE>
                 </TD>
                </TR>
               </TABLE>
              </TD>
             </TR>
            </TABLE>
           </TD>
          </TR>


          <TR>
           <TD  >
            <TABLE BORDER="2" WIDTH="100%">
             <TR>
              <TD  >
               <TABLE WIDTH="100%">
                <TR>
                 <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Phone</FONT></FONT></B></TD>
                </TR>
                <TR>
                 <TD WIDTH="100%"  >
                  <TABLE>
                   <TR>

                    <TD  >
                     <TABLE>
                      <TR>
                       <TD  >Home Phone:</TD>
                       <TD  ><INPUT SIZE="20" TYPE="TEXT" NAME="personContactEvePhone" VALUE="<%=commFwData.getFormParameter("personContactEvePhone")%>"></TD>
                      </TR>
                     </TABLE>
                    </TD>

                    <TD  >
                     <TABLE>
                      <TR>
                       <TD  >Work Phone:</TD>
                       <TD  ><INPUT SIZE="20" TYPE="TEXT" NAME="personContactDayPhone" VALUE="<%=commFwData.getFormParameter("personContactDayPhone")%>"></TD>
                      </TR>
                     </TABLE>
                    </TD>

                   </TR>
                  </TABLE>
                 </TD>
                </TR>
               </TABLE>
              </TD>
             </TR>
            </TABLE>
           </TD>
          </TR>


          <TR>
           <TD  >
            <TABLE>
             <TR>
              <TD  >Client ID:&nbsp;<%=commFwData.getFormParameter("ClientID")%></TD>
             </TR>
            </TABLE>
           </TD>
           <TD ALIGN="Left"  >
            <HR WIDTH="100%">
           </TD>
          </TR>


          <TR>
           <TD ALIGN="Left"  >
            <INPUT TYPE="hidden" NAME="tranName"/>
            <INPUT TYPE="hidden" NAME="transformResponse"/>
            <INPUT VALUE="Locate Client" TYPE="SUBMIT" NAME="function" onClick="document.CommFwForm.tranName.value='TestClientLocate'"/>  
            <INPUT VALUE="Add Client" TYPE="SUBMIT" NAME="function" onClick="document.CommFwForm.tranName.value='TestClientAdd'; document.CommFwForm.transformResponse.value='true'"/>   
            <INPUT VALUE="Reset" TYPE="RESET"/>
            <HR WIDTH="100%"/>
           </TD>
          </TR>

<%@ include file="WEB-INF/jspf/CFwXMLDataForm.jspf" %>

         </TABLE>
         </FORM>

         <FORM METHOD="POST" NAME="CommFwResultForm" ACTION="">

         <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>

         <TABLE align="center" width="100%">
<%@ include file="WEB-INF/jspf/CFwXMLResultForm.jspf" %>
         </TABLE>

         </FORM>
        </TD>
       </TR>
      </TABLE>
     <!--/FORM-->
    </TD>
   </TR>
  </TABLE>
 </DIV>
 </BODY>
</HTML>
