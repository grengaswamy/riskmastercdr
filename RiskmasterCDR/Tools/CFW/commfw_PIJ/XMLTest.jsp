<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP to Test an XML Transaction.                         --%>
<%-- The XML will be directly entered by the user.                     --%>
<%-- ----------------------------------------------------------------- --%>
<%@page language='java' contentType='text/html'%>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Vector" %>
<%@page import="com.csc.fw.util.FwSystem" %>
<%@page import="com.csc.fw.util.ServletUtil" %>
<%@ page errorPage="error.jsp" %>
<%@ page pageEncoding="UTF-8" %>

<jsp:useBean id='commFwData' scope='page' class='com.csc.cfw.util.test.CommFwTestData'/>
<jsp:setProperty name='commFwData' property='*' />
<%
boolean fExecute = (commFwData.function != null);
boolean fForceValidationOff = false;
if (fExecute)
{
	commFwData.newTest();
	commFwData.getAuthCredentials(request, session);
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", request.getParameter("noTransformCache")));
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
	commFwData.setRepostCount(request.getParameter("repostCount"));
	commFwData.setXslTraceOptions(request.getParameterValues("xslTraceOptions"));
	commFwData.execute();
}
else
{
	commFwData.newTest();
}
%>

<HTML>
 <HEAD>
  <META http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <TITLE>XML Test<%=fExecute ? " Response" : "" %> - Communications Framework</TITLE>
 </HEAD>
 <BODY BGCOLOR="#C0C0C0">
 <SCRIPT LANGUAGE="JAVASCRIPT">
  <%= commFwData.getXmlStringsAsJS() %>
 </SCRIPT>
 <DIV ALIGN="Center" ID="HtmlDiv1">
  <TABLE>
   <TR>
    <TD ALIGN="Center"  >
     <H4>Communications Framework XML Test</H4>
     <HR WIDTH="100%">
    </TD>
   </TR>
   <TR>
    <TD  >
      <TABLE>
       <TR ALIGN="Center">
        <TD ALIGN="Center"  >
         <FORM METHOD="POST" NAME="CommFwTestForm" ACTION="XMLTest.jsp">
         <TABLE>
          <TR>
           <TD>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Transaction XML Data</U></STRONG></FONT></P>
            </DIV>
            <B>Copy input XML Message below:</B>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT VALUE="Y"     TYPE="CHECKBOX" NAME="nonXML" <%=ServletUtil.setChecked("Y", request.getParameter("nonXML")) %> ><B>Non-XML</B><br>
            <TEXTAREA cols="75" WRAP="OFF" ROWS="9" NAME="requestXml"><% if (commFwData.getRequestXml() != null) { %><%=FwSystem.replaceAll(FwSystem.replaceAll(commFwData.getRequestXml(),"&","&amp;"),"<","&lt;") /*jsp:getProperty name='commFwData' property='requestXml'/*/%><% } %></TEXTAREA>
           </TD>
          </TR>

          <TR>
           <TD>
            <B>Enter XML URL below:</B><BR />
            <INPUT NAME="xmlUrl"
<%  if (commFwData.getXmlUrl() != null)
    {
%>
                   VALUE="<jsp:getProperty name='commFwData' property='xmlUrl'/>"  
<%
    }
%>
                   SIZE="75">
           </TD>
          </TR>


          <TR>
           <TD ALIGN="Left"  >
            <INPUT VALUE="Execute XML" TYPE="SUBMIT" NAME="function"> 
            <INPUT VALUE="Reset" TYPE="RESET">
            <HR WIDTH="100%">
           </TD>
          </TR>

<%@ include file="WEB-INF/jspf/CFwXMLDataForm.jspf" %>

         </TABLE>
         </FORM>

         <FORM METHOD="POST" NAME="CommFwResultForm" ACTION="">
         <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>

         <TABLE>

<%@ include file="WEB-INF/jspf/CFwXMLResultForm.jspf" %>

         </TABLE>
         </FORM>
        </TD>
       </TR>
      </TABLE>
    </TD>
   </TR>
  </TABLE>
 </DIV>
 </BODY>
</HTML>
