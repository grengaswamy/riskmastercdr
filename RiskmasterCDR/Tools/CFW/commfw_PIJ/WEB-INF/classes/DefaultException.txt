<?xml version="1.0" encoding="UTF-8" ?>
<{0} {5}>
	<Status>
		<StatusCd>SystemError</StatusCd>
		<StatusDesc>{3}</StatusDesc>
	</Status>
	<{1}>
		<{2}>
			<MsgStatus>
				<MsgStatusCd>SystemError</MsgStatusCd>
				<MsgStatusDesc>{3}</MsgStatusDesc>
				<ExtendedStatus>
					<ExtendedStatusCd>SystemError</ExtendedStatusCd>
					<ExtendedStatusDesc>{4}</ExtendedStatusDesc>
				</ExtendedStatus>
				<ExtendedStatus>
					<ExtendedStatusCd>UserError</ExtendedStatusCd>
					<ExtendedStatusDesc>{3}</ExtendedStatusDesc>
				</ExtendedStatus>
			</MsgStatus>
		</{2}>
	</{1}>
</{0}>
