<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ******************************************************************* --%>
<%-- * This is a JSP to Monitor Queue installed over CommFw.           * --%>
<%-- * Ashwini K Shukla 10/03/2008 For FBL Queue development           * --%>
<%-- ******************************************************************* --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.ByteArrayInputStream" %>
<%@ page import="java.io.ByteArrayOutputStream" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.io.OutputStream" %>
<%@ page import="java.io.OutputStreamWriter" %>
<%@ page import="java.lang.reflect.Method" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.text.MessageFormat" %>
<%@ page import="java.text.NumberFormat" %>

<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.jms.BytesMessage" %>
<%@ page import="javax.jms.Connection" %>
<%@ page import="javax.jms.ConnectionFactory" %>
<%@ page import="javax.jms.Destination" %>
<%@ page import="javax.jms.JMSException" %>
<%@ page import="javax.jms.Message" %>
<%@ page import="javax.jms.MessageConsumer" %>
<%@ page import="javax.jms.Queue" %>
<%@ page import="javax.jms.QueueBrowser" %>
<%@ page import="javax.jms.Session" %>
<%@ page import="javax.jms.TextMessage" %>

<%@ page import="com.csc.fw.util.FwEnvelope" %>
<%@ page import="com.csc.fw.util.FwException" %>
<%@ page import="com.csc.fw.util.FwEvent" %>
<%@ page import="com.csc.fw.util.FwSecurityManager" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.Logger" %>
<%@ page import="com.csc.fw.util.LRUCache" %>
<%@ page import="com.csc.fw.util.PropertyFileConfigurator" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page import="com.csc.cfw.process.QueueMessageHandler" %>
<%@ page import="com.csc.cfw.process.QueueMessageRestrictionFilter" %>

<%@ page errorPage="error.jsp" %>
<%
    String  programName = "QueueAdmin.jsp";
    String  programTitle = "Queue Administration";
    String  programMainHeader = "Communications Framework Queue Administration";
    boolean adminMode = true;
%>

<%@ include file="WEB-INF/jspf/QueueMonitor.jspf" %>
