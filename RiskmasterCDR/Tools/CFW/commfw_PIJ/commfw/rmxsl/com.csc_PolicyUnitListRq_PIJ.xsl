<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <PT4JPROC>
      <xsl:call-template name="ProcName" />
    </PT4JPROC>
  </xsl:template>
  <xsl:template name= "ProcName" >
    <SQLStmt>
      CALL RSKCLUNIT1('<xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/CompanyProductCd"/>
        <xsl:with-param name="size">3</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/PolicyNumber"/>
        <xsl:with-param name="size">7</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="FormatParameterValue">
        <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
        <xsl:with-param name="size">2</xsl:with-param>
      </xsl:call-template>')
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>