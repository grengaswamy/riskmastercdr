<%@ page import="javax.xml.parsers.DocumentBuilder" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="org.w3c.dom.Element" %>
<%@ page import="org.w3c.dom.Node" %>
<%@ page import="org.w3c.dom.NodeList" %>
<%@ page import="org.w3c.dom.NamedNodeMap" %>
<%@ page import="org.xml.sax.InputSource,java.io.*" %>
<%@ page import="com.csc.fw.util.XMLDocument" %>
<%!

	public String getTableXML(String fileName) throws Exception
	{	
		int totalRows; //total number of palnts 
		String bgColor="white"; //grid background color 
	
		//Get Document Builder Factory
		DocumentBuilderFactory factory = 
			DocumentBuilderFactory.newInstance();
	
		//Get the Builder
		DocumentBuilder builder = 
			factory.newDocumentBuilder(); 
				
		//Get your DOM document by loading file
		Document document = 
			builder.parse(new java.io.File(fileName));
				
		//Get all nodes			
		NodeList nodeList = 
			document.getElementsByTagName("Table");
		totalRows = nodeList.getLength();
			
		//check the number of rows returned
    	StringBuffer buf = new StringBuffer();    	
		if (totalRows < 1)
		{
			buf.append("<br/><b>No rows in file</b><br/>");
		}    	
    	
        buf.append("<table id='datatable' border=0 cellspacing=1 cellpadding=1 bgcolor=gray>");         

		if (totalRows < 1)
		{
			buf.append("<th align='center'><b>TableName</b></th>");
			buf.append("<th align='center'><b>NativeColumn</b></th>");
			buf.append("<th align='center'><b>ExternalColumn</b></th>");
		}    	
		else
		{
			buf.append("<tr bgcolor=#C0C0C0>");
			
			int i,j,k;        
			for (i=0;i<nodeList.item(0).getChildNodes().getLength();i++)
			{
				Node node=nodeList.item(0).getChildNodes().item(i);
				if (node.getNodeType()==Node.ELEMENT_NODE)
				{  
					buf.append("<th align='center'><b>" + node.getNodeName() + "</b></th>"); 
				}
			} 
			buf.append("</tr>");              
			
			for (i = 0; i < totalRows; i++)
			{                
				buf.append("<tr bgcolor=" + bgColor + ">"); 
				for (j=0; j<nodeList.item(i).getChildNodes().getLength();j++)
				{     
					Node thisNode = nodeList.item(i).getChildNodes().item(j); 
					if (thisNode.getNodeType()==Node.ELEMENT_NODE)
					{  
						buf.append("<td><input type='text' name='" + thisNode.getNodeName() + "' value="+getNodeValue(thisNode) +"></input</td>"); 					
					}
				} 
				buf.append("</tr>"); 
			}
		}
		buf.append("</table>");             
        return buf.toString(); 
	}
		
	public String getNodeValue(Node node) 
	{	
		StringBuffer buf = new StringBuffer();	
		NodeList children = node.getChildNodes();	
		for (int i = 0; i < children.getLength(); i++)
		{	    
			Node textChild = children.item(i);	    
			if (textChild.getNodeType() != Node.TEXT_NODE)
			{		
				System.err.println("Mixed content! Skipping child element " + 
					textChild.getNodeName());		
				continue;	    
			}	    
			buf.append(textChild.getNodeValue());	
		}	
		return buf.toString();    
	}
	
	public String SaveXML(String table)
	{
		return table;
	}
%>