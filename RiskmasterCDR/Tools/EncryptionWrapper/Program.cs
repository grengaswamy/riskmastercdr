using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Security.Encryption;

namespace EncryptionWrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            //If the user 
            if (args.Length == 2)
            {
                string strEncryptedUID = RMCryptography.EncryptString(args[0]);

                //Create the Registry Key for the Database User Name
                RegistryManager.WriteRegKey(RiskmasterRegistryKeys.DTGSecurity32UserKey, string.Empty, strEncryptedUID);

                string strEncryptedPWD = RMCryptography.EncryptString(args[1]);

                //Create the Registry Key for the Database Password
                RegistryManager.WriteRegKey(RiskmasterRegistryKeys.DTGSecurity32PasswordKey, string.Empty, strEncryptedPWD);
            } // if
            else
            {
                string strErrorMsg = "Please enter the database user name and password in the following format: " + Environment.NewLine;
                strErrorMsg += "EncryptionWrapper.exe mydbusername mydbpassword";
                Console.WriteLine(strErrorMsg);
            } // else


        }
    }
}
