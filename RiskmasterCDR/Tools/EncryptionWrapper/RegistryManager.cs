using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace EncryptionWrapper
{
    public class RegistryManager
    {

        /// <summary>
        /// Method to write the required Riskmaster security key into the database
        /// </summary>
        /// <param name="strDBUserID"></param>
        /// <param name="strDBPassword"></param>
        public static void WriteRegKey(string strSubKey, string strSubKeyValue,  string strRegKeyValue)
        {
            RegistryKey objRK = Registry.LocalMachine.CreateSubKey(strSubKey);

            //Set the value in the Registry
            objRK.SetValue(strSubKeyValue, strRegKeyValue);
            
            //Clean up
            objRK = null;
        }//method: WritRegKey()
    }
}

