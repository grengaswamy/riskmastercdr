using System;
using System.Collections.Generic;
using System.Text;

namespace EncryptionWrapper
{
    /// <summary>
    /// Provides the names and values of the Registry Keys
    /// utilized by Riskmaster
    /// </summary>
    internal struct RiskmasterRegistryKeys
    {
        public static string SMServerKey = @"Software\DTG\SORTMASTER Server";
        public static string SMServerDSNValue = "DSN";
        public static string SMTPServerValue = "SMTPServer";
        public static string DTGSecurity32UserKey = @"Software\DTG\RISKMASTER\Security\EncDBUserID";
        public static string DTGSecurity32PasswordKey = @"Software\DTG\RISKMASTER\Security\EncDBPassword";
    }//struct
}
