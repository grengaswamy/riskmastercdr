using Riskmaster.Security.Encryption;
using System;
using System.Collections.Generic;
using System.Text;

namespace EncryptionWrapper
{

    /// <summary>
    /// Allows encryption and decryption of secured Riskmaster values
    /// using the Riskmaster Cryptography algorithm
    /// </summary>
    internal class RiskmasterCryptography
    {
        /// <summary>
        /// Constant which contains the salt key
        /// required to encrypt and decrypt Riskmaster secured string values
        /// </summary>
        private const string RM_AUTH_CODE = "6378b87457a5ecac8674e9bac12e7cd9";

        /// <summary>
        /// Gets the Riskmaster Authorization code
        /// required to encrypt and decrypt secured
        /// Riskmaster credentials
        /// </summary>
        internal static string RMAuthCode
        {
            get { return RM_AUTH_CODE; }
        }


        /// <summary>
        /// Decrypts the encrypted Riskmaster string
        /// and returns the decrypted string value
        /// </summary>
        /// <param name="strEncrypted">string containing the encrypted Riskmaster value</param>
        /// <returns>string containing the decrypted value</returns>
        internal static string DecryptString(string strEncrypted)
        {
            string strDecrypted = string.Empty;
            int intIndex = 0;

            DTGCrypt32 objCrypt = new DTGCrypt32();

            try
            {
                //Decrypt the password
                strDecrypted = objCrypt.DecryptString(strEncrypted, RMAuthCode);

                //Get the index of the colon character within the string
                intIndex = strDecrypted.IndexOf(":");

                //Obtain the actual decrypted password
                strDecrypted = strDecrypted.Substring(intIndex + 1);
            }//try
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }//catch
            finally
            {
                //Clean up
                objCrypt = null;
            }//finally


            //return the decrypted string
            return strDecrypted;
        }//method: DecryptString()


        /// <summary>
        /// Encrypts the clear text Riskmaster string
        /// and returns the encrypted string value
        /// </summary>
        /// <param name="strUnencrypted">string containing the clear text Riskmaster value</param>
        /// <returns>string containing the encrypted value</returns>
        internal static string EncryptString(string strUnencrypted)
        {
            string strEncrypted = string.Empty;

            DTGCrypt32 objCrypt = new DTGCrypt32();

            try
            {
                //Encrypt the password
                strEncrypted = objCrypt.EncryptString(strUnencrypted, RMAuthCode);

            }//try
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }//catch
            finally
            {
                //Clean up
                objCrypt = null;
            }//finally


            //return the encrypted string
            return strEncrypted;
        }//method: EncryptString()
    } // class



}
