using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using System.Reflection;
using System.Text;

namespace Riskmaster.Tools
{
	/// <summary>
	/// Command line App to correctly convert and DUPLICATE mail merge templates for RMX from RMWorld\Net so that 
	/// they do not become corrupted by incompatibilities with the previous products.
	/// </summary>
	class MergeTemplateUpgrade
	{
		static string m_DocPath="";
		static string m_WorkPath=AppDomain.CurrentDomain.BaseDirectory + "work\\";
		static string m_ConnStr="";
		static UserLogin m_objUserLogin=null;
		static int m_LoadCounter=0;
		static bool m_bDbDocType=false;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if(!CheckParams(args))
				return;
			
			//Get UserLogin
			if(m_ConnStr==null || m_ConnStr=="")
			{
				Riskmaster.Security.Login objLogin = new  Login(SecurityDatabase.Dsn);
				try
				{
					objLogin.RegisterApplication(0,0,ref m_objUserLogin);
				}
				catch(Exception e)
				{
					Console.WriteLine("Login Exception:");
					while(e!=null)
					{
						Console.WriteLine(e.Message);
						e = e.InnerException;
					}
				}
				if(m_objUserLogin==null)
				{
					Console.WriteLine("Authentication Failed - No templates processed");
					return;
				}

				m_ConnStr = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
				m_bDbDocType = (m_objUserLogin.objRiskmasterDatabase.DocPathType!=0);
				m_DocPath = m_objUserLogin.objRiskmasterDatabase.GlobalDocPath;

				if(!m_bDbDocType && !System.IO.Directory.Exists(m_DocPath))
				{
					Console.WriteLine(String.Format("Invalid Document Path {0} - No templates processed",m_DocPath));
					return;
				}

				if(m_bDbDocType)
				{
					DbConnection cn = DbFactory.GetDbConnection(m_DocPath);
					try{cn.Open();cn.Close();}
					catch{cn = null;}
					if(cn==null)
					{
						Console.WriteLine(String.Format("Invalid Document Path {0} - No templates processed",m_DocPath));
						return;
					}
				}
				
				
			{//Check if Upgrade Wizard has already been run - bail out.
				DbConnection cn = DbFactory.GetDbConnection(m_ConnStr);
				cn.Open();
				if(((int)cn.ExecuteInt("SELECT MAX(FORM_ID) FROM MERGE_FORM")) >5000)
				{
					cn.Close();
					Console.WriteLine(String.Format("Template Upgrade Wizard has already been run against this Riskmaster database - No templates processed"));
					return;
				}
				cn.Close();
			}
			}

			//TODO:
			//Create and Populate Working folder
			PullWorkingFiles();

			//Convert files in Work folder
			ConvertWorkingFiles();

			//Deploy new files back to  DocPath\DocStorage
			PushWorkingFiles();

			//Update MERGE_XXX tables from RMX.
			DupeFormDefinitions();

			//Report Results
			Console.WriteLine(String.Format("MergeTemplateUpgrade {0} files upgraded.",m_LoadCounter));
		}
		static bool PullWorkingFiles()
		{
		
			string srcFileName="";
			string newFileName = "";
			string sTrash = "";
			
			FileStorageManager objFileManager = null;
			
			if(m_bDbDocType)
			{
				objFileManager = new FileStorageManager();
				objFileManager.FileStorageType = StorageType.DatabaseStorage;
				objFileManager.DestinationStoragePath = m_DocPath;
			}

			DbConnection cn = DbFactory.GetDbConnection(m_ConnStr);
			DbReader rdr = null;
			try
			{
				cn.Open();

				//Clear out and ensure working folder exists.
				if(System.IO.Directory.Exists(m_WorkPath))
					System.IO.Directory.Delete(m_WorkPath,true);
				System.IO.Directory.CreateDirectory(m_WorkPath);
			

				rdr = cn.ExecuteReader("SELECT FORM_FILE_NAME FROM MERGE_FORM");
				while(rdr.Read())
				{
					srcFileName = rdr.GetString("FORM_FILE_NAME");
					newFileName = "RMX_" + srcFileName.ToLower().Replace(".doc",".rtf");

					if(m_bDbDocType) //Pull from DocStorage
					{
						try{objFileManager.RetrieveFile(srcFileName, m_WorkPath + newFileName, out sTrash);}
						catch{continue;}
					}
					else  //Pull from DocPath
					{
						try{System.IO.File.Copy(m_DocPath + "\\" + srcFileName,m_WorkPath + newFileName,true);}
						catch{continue;}
					}
				}
			}
			finally
			{
				if(rdr !=null)rdr.Close();
				rdr = null;

				if(cn !=null)cn.Close();
				cn = null;

				if(objFileManager!=null)objFileManager= null;
			}
			return true;
		}
		static bool PushWorkingFiles()
		{
			FileStorageManager objFileManager = null;
			try
			{

				if(m_bDbDocType)
				{
					objFileManager = new FileStorageManager();
					objFileManager.FileStorageType = StorageType.DatabaseStorage;
					objFileManager.DestinationStoragePath = m_DocPath;
				}

				foreach (string srcFile in System.IO.Directory.GetFiles(m_WorkPath))  
				{
					if(m_bDbDocType) //Push into DocStorage
					{
						try{objFileManager.StoreFile(srcFile, System.IO.Path.GetFileName(srcFile));}
						catch{continue;}
					}
					else  //Push into DocPath
					{
						try{System.IO.File.Copy(srcFile,m_DocPath + "\\" + System.IO.Path.GetFileName(srcFile),true);}
						catch{continue;}
					}
				}
			}
			finally
			{
				if(objFileManager!=null)objFileManager= null;
			}
			return true;
		}
		static string SafeCharVal(string s){return SafeCharVal(s,0);}
		static string SafeCharVal(string s,int MaxLength)
		{
			string sRet = s;
			
			if(MaxLength>0)
				s = s.Substring(0,Math.Min(s.Length,MaxLength));
			
			sRet = (s=="") ? "NULL":"'" + s.Replace("'", "''") + "'";
			return sRet;
		}
		
		static bool DupeFormDefinitions()
		{
			string srcFileName="";
			string newFileName = "";
			StringBuilder sbSQL = null;
			int MaxLengthFormName = 0;
			int MaxLengthFormDesc = 0;

			DbConnection cn = DbFactory.GetDbConnection(m_ConnStr);
			DbConnection cn1 = DbFactory.GetDbConnection(m_ConnStr);
			DbTransaction txn=null;
			DbReader rdr = null;

			try
			{
				cn.Open();
				cn1.Open();
				txn = cn1.BeginTransaction();
				
				rdr = cn.ExecuteReader("SELECT * FROM MERGE_FORM WHERE FORM_ID<5000");
				MaxLengthFormDesc = (int)rdr.GetSchemaTable().Select("ColumnName='FORM_DESC'")[0]["ColumnSize"];
				MaxLengthFormName = (int)rdr.GetSchemaTable().Select("ColumnName='FORM_NAME'")[0]["ColumnSize"];
				while(rdr.Read())
				{
					srcFileName = rdr.GetString("FORM_FILE_NAME");
					newFileName = "RMX_" + srcFileName.ToLower().Replace(".doc",".rtf");

					//MERGE_FORM
					sbSQL = new StringBuilder("INSERT INTO MERGE_FORM VALUES(");
					sbSQL.Append((rdr.GetInt("FORM_ID") + 5000) + ",");
					sbSQL.Append(rdr.GetInt("CAT_ID")+ ",");
					sbSQL.Append(SafeCharVal("*RMX* " + rdr.GetString("FORM_NAME"),MaxLengthFormName) + ",");
					sbSQL.Append(SafeCharVal("*RMX* " + rdr.GetString("FORM_DESC"),MaxLengthFormDesc) + ",");
					sbSQL.Append(SafeCharVal(rdr.GetString("TABLE_QUALIFY")) + ",");
					sbSQL.Append(SafeCharVal("RMX_" + rdr.GetString("FORM_FILE_NAME").ToLower().Replace(".doc",".rtf")) + ",");
					sbSQL.Append(rdr.GetInt("LINE_OF_BUS_CODE") + ",");
					sbSQL.Append(rdr.GetInt("STATE_ID") + ",");
					sbSQL.Append(rdr.GetInt("MERGE_DOC_TYPE_CODE") + ",");
					sbSQL.Append(rdr.GetInt("MERGE_FORMAT_TYPE_CODE") + ")");
					cn1.ExecuteNonQuery(sbSQL.ToString(),txn);
				}
				rdr.Close();

				//MERGE_FORM_DEF
				rdr = cn.ExecuteReader("SELECT * FROM MERGE_FORM_DEF WHERE FORM_ID<5000");
				while(rdr.Read())
				{
					sbSQL = new StringBuilder("INSERT INTO MERGE_FORM_DEF VALUES(");
					sbSQL.Append((rdr.GetInt("FORM_ID") + 5000) + ",");
					sbSQL.Append(rdr.GetInt("FIELD_ID")+ ",");
					sbSQL.Append(rdr.GetInt("ITEM_TYPE")+ ",");
					sbSQL.Append(rdr.GetInt("SEQ_NUM")+ ",");
					sbSQL.Append(rdr.GetInt("SUPP_FIELD_FLAG")+ ",");
					sbSQL.Append(SafeCharVal(rdr.GetString("MERGE_PARAM")) + ")");
					cn1.ExecuteNonQuery(sbSQL.ToString(),txn);
				}rdr.Close();

				rdr = cn.ExecuteReader("SELECT * FROM MERGE_FORM_PERM WHERE FORM_ID<5000");
				while(rdr.Read())
				{
					//MERGE_FORM_PERM
					sbSQL = new StringBuilder("INSERT INTO MERGE_FORM_PERM VALUES(");
					sbSQL.Append((rdr.GetInt("FORM_ID") + 5000) + ",");
					sbSQL.Append(rdr.GetInt("USER_ID")+ ",");
					sbSQL.Append(rdr.GetInt("GROUP_ID")+ ")");
					cn1.ExecuteNonQuery(sbSQL.ToString(),txn);
				}
				rdr.Close();
				//Update Glossary for MERGE_FORM_ID
				cn1.ExecuteNonQuery("UPDATE GLOSSARY SET NEXT_UNIQUE_ID=(SELECT MAX(FORM_ID) FROM MERGE_FORM) WHERE SYSTEM_TABLE_NAME='MERGE_FORM'",txn);
				txn.Commit();

			}
			finally
			{
				if(rdr !=null)rdr.Close();
				rdr = null;


				if(cn !=null)cn.Close();
				cn = null;
				
				if(cn1 !=null)cn1.Close();
				cn1 = null;

			}
			return true;
				
		}

		static bool ConvertWorkingFiles()
		{
			Type tApp =null;
			Object objApp = null;
			try
			{
				// Use reflection to create instance of Word...avoids version compatibility issue
				tApp = System.Type.GetTypeFromProgID("Word.Application");
				objApp = Activator.CreateInstance(tApp);
				foreach (object srcFile in System.IO.Directory.GetFiles(m_WorkPath))  
				{
					Console.WriteLine("Converting file: " + srcFile + "...");
					object oDocs = tApp.InvokeMember("Documents", BindingFlags.GetProperty, null, objApp, null);
					object aDoc = oDocs.GetType().InvokeMember("Open",BindingFlags.InvokeMethod, null, oDocs, new object[]{srcFile});
					aDoc.GetType().InvokeMember("Activate", BindingFlags.InvokeMethod, null, aDoc, null);
					object oActiveDoc =	tApp.InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, objApp, null);
					oActiveDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, oActiveDoc,new object[]{srcFile,6});
					aDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, aDoc, new object[]{false, null, null});
				}
			}
			finally
			{
				//Closing the application
				if(objApp !=null)
					objApp.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, objApp, new object[]{false, null, null});
			}
			return true;
		}
		static bool CheckParams(string[] args)
		{
			if(args.Length >0)
			{
				Console.WriteLine("WARNING:  MergeTemplateUpgrade.exe does not use any command line parameters.");
				Console.WriteLine("Simply select the target Riskmaster database by logging in using the provided UI.");
				Console.WriteLine("Merge Templates will be imported\\duplicated for RMX based on the Document Path and Database contents of the selected Riskmaster Database.");
				Console.WriteLine("");
				Console.WriteLine("Command line parameter(s) were ignored.");
			}

			return true;
		}
	}
}