﻿using Riskmaster.Security.Encryption;

public class RiskmasterCryptography
{
    public const string CRYPTO_KEY = "6378b87457a5ecac8674e9bac12e7cd9";

    /// <summary>
    /// decrypt specified encrypted string
    /// </summary>
    /// <param name="strEncrypted">encrypted string</param>
    /// <returns>decrypted string</returns>
    public static string DecryptString(string strEncrypted)
    {
        var objCrypto = new DTGCrypt32();
        var strDecrypted = objCrypto.DecryptString(strEncrypted, CRYPTO_KEY);

        return strDecrypted;
    }

    /// <summary>
    /// encrypt specified string
    /// </summary>
    /// <param name="strDecrypted">plain string</param>
    /// <returns>encrypted string</returns>
    public static string EncryptString(string strDecrypted)
    {
        var objCrypto = new DTGCrypt32();
        var strEncrypted = objCrypto.EncryptString(strDecrypted, CRYPTO_KEY);

        return strEncrypted;
    }
}

