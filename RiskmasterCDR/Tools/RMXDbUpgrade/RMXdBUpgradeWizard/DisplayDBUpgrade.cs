﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;

using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;

namespace RMXdBUpgradeWizard
{
    class DisplayDBUpgrade
    {
        #region "Global Variables"
        public static int g_iDSNID;
        public static int dbLookup;
        public static int g_dbMake; //dB manufacturer
        public static string sODBCName;
        public static string sBaseLogType; //Govind

        
        public static string gsDBRelease; //holds release_number of current DB (one logged into)
        public static string gsDBVersion; //holds dtg_version of current DB
        const string STRING_DELIMITER = ",";
        
        #region public properties
        public static string gsDBTargetRel;
        public static string gsDBTargetVer;
        public static string gsFORM_TITLE;
        public static string gsSecurityScripts;
        public static string gsBaseScripts;
        public static string gsBasePriorScripts;

        public static string gsSessionScripts;
        public static string gsTaskManagerScripts;
        public static string gsStoredProcs;
        public static string gsSqlStoredProcs;
        public static string gsOracleStoredProcs;
        public static string gsHPBaseScripts;
        public static string gsHPSecurityScripts;
        //NADIM PRIMARY KEY
        public static string gsStoredProcs4Primarkeys;
        public static string gsRunPrimarykeys;
        public static string gsIsBRSEnabled;
        public static string gsViewScripts;
        public static string gsCustomScripts;
        public static string gsInsertXmlViews;
        public static string gsInsertViewsOnly;
        public static string gsHistoryTrackingScripts; //Mgaba2:R7:History Tracking
        public static string gsHistoryStoredProcs;//Mgaba2:R7:History Database Stored Procedures
        public static string gsSortMasterScripts;
        public static string gsCustomViewOnly;
        public static string gsHPStagingScripts;    //added by swati agarwal MITS # 33414
        public static string gsCustomSecurityOnly;
        /// <summary>
        /// Gets all of the configured triggers to run on the target database
        /// </summary>
        internal static string[] TRIGGERS
        {
            get
            {
                string strTriggers = ConfigurationManager.AppSettings["RMTRIGGERS"];
                return UpgradeScripts.GetSQLScriptFiles(strTriggers, STRING_DELIMITER);
            }

        }
        internal static string[] ORACLE_TRIGGERS
        {
            get
            {
                string strTriggers = ConfigurationManager.AppSettings["RM_ORA_TRIGGERS"];
                return UpgradeScripts.GetSQLScriptFiles(strTriggers, STRING_DELIMITER);
            }

        }
        internal static string DBTARGETREL
        {
            get
            {
                try
                {
                    gsDBTargetRel = ConfigurationManager.AppSettings["RMRELEASEDATE"];
                }
                catch (Exception)
                {
                }

                return gsDBTargetRel;
            }
            set
            {
                gsDBTargetRel = value;
            }
        }
        internal static string DBTARGETVER
        {
            get
            {
                try
                {
                    gsDBTargetVer = ConfigurationManager.AppSettings["RMRELEASEVERSION"];
                }
                catch (Exception)
                {
                }

                return gsDBTargetVer;
            }
            set
            {
                gsDBTargetVer = value;
            }
        }
        internal static string FORMTITLE
        {
            get
            {
                try
                {
                    gsFORM_TITLE = ConfigurationManager.AppSettings["FORM_TITLE"];
                }
                catch (Exception)
                {
                }

                return gsFORM_TITLE;
            }
            set
            {
                gsFORM_TITLE = value;
            }
        }

        // Addded by abhinav for MITS33063
        internal static string OSHOMIGSCRIPTS
        {
            get
            {
                try
                {
                    gsFORM_TITLE = ConfigurationManager.AppSettings["OSHOMIGSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsFORM_TITLE;
            }
            set
            {
                gsFORM_TITLE = value;
            }
        }


        //-----------------------------------------------
        
        internal static string SECURITYSCRIPTS
        {
            get
            {
                try
                {
                    gsSecurityScripts = ConfigurationManager.AppSettings["RMSECURITYSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSecurityScripts;
            }
            set
            {
                gsSecurityScripts = value;
            }
        }
        internal static string BASESCRIPTS
        {
            get
            {
                try
                {
                    gsBaseScripts = ConfigurationManager.AppSettings["RMSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsBaseScripts;
            }
            set
            {
                gsBaseScripts = value;
            }
        }

        internal static string BASEPRIORSCRIPTS
        {
            get
            {
                try
                {
                    gsBasePriorScripts = ConfigurationManager.AppSettings["RMPRIORSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsBasePriorScripts;
            }
            set
            {
                gsBasePriorScripts = value;
            }
        }
        internal static string RMHPMAINDBSCRIPTS
        {
            get
            {
                try
                {
                    gsHPBaseScripts = ConfigurationManager.AppSettings["RMHPMAINDBSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsHPBaseScripts;
            }
            set
            {
                gsHPBaseScripts = value;
            }
        }
        //Added by Swati agarwal for staging tables MITS # 33414, 36013
        internal static string RMASTAGINGSCRIPTS
        {
            get
            {
                try
                {
                    gsHPStagingScripts = ConfigurationManager.AppSettings["RMASTAGINGSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsHPStagingScripts;
            }
            set
            {
                gsHPStagingScripts = value;
            }
        }
        //changes end here by swati
        internal static string RMHPSECURITYDBSCRIPTS
        {
            get
            {
                try
                {
                    gsHPSecurityScripts = ConfigurationManager.AppSettings["RMHPSECURITYDBSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsHPSecurityScripts;
            }
            set
            {
                gsHPSecurityScripts = value;
            }
        }
        internal static string SESSIONSCRIPTS
        {
            get
            {
                try
                {
                    gsSessionScripts = ConfigurationManager.AppSettings["RMSESSIONSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSessionScripts;
            }
            set
            {
                gsSessionScripts = value;
            }
        }
                
        internal static string TASKMANAGERSCRIPTS
        {
            get
            {
                try
                {
                    gsTaskManagerScripts = ConfigurationManager.AppSettings["RMTASKMANAGERCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsTaskManagerScripts;
            }
            set
            {
                gsTaskManagerScripts = value;
            }
        }
        internal static string STOREDPROCS
        {
            get
            {
                try
                {
                    gsStoredProcs = ConfigurationManager.AppSettings["RMSTOREDPROCEDURES"];
                }
                catch (Exception)
                {
                }

                return gsStoredProcs;
            }
            set
            {
                gsStoredProcs = value;
            }
        }
        // akaushik5 Added for MITS 33420 Starts
        /// <summary>
        /// Gets or sets the sqlstoredprocs.
        /// </summary>
        /// <value>
        /// The sqlstoredprocs.
        /// </value>
        internal static string SQLSTOREDPROCS
        {
            get
            {
                try
                {
                    gsSqlStoredProcs = ConfigurationManager.AppSettings["RMSQLSPECIFICSTOREDPROCEDURES"];
                }
                catch
                {
                }

                return gsSqlStoredProcs;
            }
        }

        /// <summary>
        /// Gets or sets the oraclestoredprocs.
        /// </summary>
        /// <value>
        /// The oraclestoredprocs.
        /// </value>
        internal static string ORACLESTOREDPROCS
        {
            get
            {
                try
                {
                    gsOracleStoredProcs = ConfigurationManager.AppSettings["RMORACLESPECIFICSTOREDPROCEDURES"];
                }
                catch
                {
                }

                return gsOracleStoredProcs;
            }
        }
        // akaushik5 Added for MITS 33420 Ends

        //NADIM PRIMARY KEY
        internal static string STOREDPROCS4PRIMARYKEYS
        {
            get
            {
                try
                {
                    gsStoredProcs4Primarkeys = ConfigurationManager.AppSettings["RMPRIMARYKEYSPROCEDURES"];
                }
                catch (Exception)
                {
                }

                return gsStoredProcs4Primarkeys;
            }
            set
            {
                gsStoredProcs4Primarkeys = value;
            }
        }
        //NADIM PRIMARY KEY

        //NADIM PRIMARY KEY
        internal static string RunPrimarykey
        {
            get
            {
                try
                {
                    gsRunPrimarykeys = ConfigurationManager.AppSettings["RUN_PRIMARY_KEYS"];
                }
                catch (Exception)
                {
                }

                return gsRunPrimarykeys;
            }
            set
            {
                gsRunPrimarykeys = value;
            }
        }
        internal static string EnableBRS
        {
            get
            {
                try
                {
                    gsIsBRSEnabled = ConfigurationManager.AppSettings["IS_BRS_ACTIVATED"];
                }
                catch (Exception)
                {
                }

                return gsIsBRSEnabled;
            }
            set
            {
                gsIsBRSEnabled = value;
            }
        }
        //NADIM PRIMARY KEY
        internal static string VIEWSCRIPTS
        {
            get
            {
                try
                {
                    gsViewScripts = ConfigurationManager.AppSettings["RMVIEWSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsViewScripts;
            }
            set
            {
                gsViewScripts = value;
            }
        }
        internal static string CUSTOMSCRIPTS
        {
            get
            {
                try
                {
                    gsCustomScripts = ConfigurationManager.AppSettings["CUSTOMSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsCustomScripts;
            }
            set
            {
                gsCustomScripts = value;
            }
        }
        internal static string CUSTOMVIEWONLY
        {
            get
            {
                try
                {
                    gsCustomViewOnly = ConfigurationManager.AppSettings["CUSTOMVIEWONLY"];
                }
                catch (Exception)
                {
                }

                return gsCustomViewOnly;
            }
            set
            {
                gsCustomViewOnly = value;
            }
        }
        internal static string CUSTOMSECURITYONLY
        {
            get
            {
                try
                {
                    gsCustomSecurityOnly = ConfigurationManager.AppSettings["CUSTOMSECURITYONLY"];
                }
                catch (Exception)
                {
                }

                return gsCustomSecurityOnly;
            }
            set
            {
                gsCustomSecurityOnly = value;
            }
        }
        internal static string INSERTXMLVIEWS
        {
            get
            {
                try
                {
                    gsInsertXmlViews = ConfigurationManager.AppSettings["INSERT_XML_VIEWS"];
                }
                catch (Exception)
                {
                }

                return gsInsertXmlViews;
            }
            set
            {
                gsInsertXmlViews = value;
            }
        }
        internal static string INSERTVIEWSONLY
        {
            get
            {
                try
                {
                    gsInsertViewsOnly = ConfigurationManager.AppSettings["INSERT_VIEWS_ONLY_NO_DB_UPGRADE"];
                }
                catch (Exception)
                {
                }

                return gsInsertViewsOnly;
            }
            set
            {
                gsInsertViewsOnly = value;
            }
        }

        //Mgaba2:R7:History Tracking
        //This script will upgrade the database where Audit tables will be stored
        //internal static string HISTORYTRACKINGSCRIPTS
        //{
        //    get
        //    {
        //        try
        //        {
        //            gsHistoryTrackingScripts = ConfigurationManager.AppSettings["RMHISTORYTRACKINGSCRIPTS"];
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        return gsHistoryTrackingScripts;
        //    }
        //    set
        //    {
        //        gsHistoryTrackingScripts = value;
        //    }
        //}

        //Mgaba2:R7:History Tracking
        //This will return the stored procedures to be run on History/Audit database
        //internal static string HISTORYSTOREDPROCS
        //{
        //    get
        //    {
        //        try
        //        {
        //            gsHistoryStoredProcs = ConfigurationManager.AppSettings["RMHISTORYSTOREDPROCEDURES"];
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        return gsHistoryStoredProcs;
        //    }
        //    set
        //    {
        //        gsHistoryStoredProcs = value;
        //    }
        //}

        internal static string RMSORTMASTERSCRIPTS
        {
            get
            {
                try
                {
                    gsSortMasterScripts = ConfigurationManager.AppSettings["RMSORTMASTERSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSortMasterScripts;
            }
            set
            {
                gsSortMasterScripts = value;
            }
        }
        #endregion

        //support "Silent" operation for inclusion into Unit Testing
        public static bool g_bNoPromptMode; //no USER interaction allowed - will be run in an automated env.
        public static bool g_bNoSessionMode; //Do NOT attempt Session DB Update - will run w/o touching session & not running any .Net Interop code
        public static string g_sNoPromptTargetDSN; //passed in command line option to specify which dB to upgrade
        //public string g_sNoPromptLogFullPath;

        public static Dictionary<string, int> dictSelectedItems = new Dictionary<string, int>();
        public static Dictionary<string, int> dictFailedItems = new Dictionary<string, int>();
        public static Dictionary<string, int> dictPassedItems = new Dictionary<string, int>();

        //===============RISKMASTER Login Functions====================
        public static bool bAborted;
        public static bool bStop;
        public static bool g_bOrgSecOn;
        //public static Login objLogin;
        public static string g_sConnectString;
        public static string g_SecConnectString;
        public static string g_ViewConnectString;
        public static string g_SessionConnectString;
        public static string g_TaskManagerConnectString;
        public static string g_DocumentConnectString;
        public static string g_UserId;
        public static string g_DocUserId;
        public static string g_DocDB;
        public const string strDSNColumn = "DSN";
        public const string strDSNIDColumn = "DSNID";
        public static string g_HistTrackConnectString;//Mgaba2:R7: History Tracking
        public static string g_ReportServer_ConnectionString;
public static int g_DsnId = 0;//nadim for primary key

        #endregion

        #region "DSNSelection"
        /// <summary>
        /// SelectDatabase
        /// </summary>
        /// <returns></returns>
        public static bool SelectDatabase()
        {
            string sObj = String.Empty;
            return SelectDatabase(-1, ref sObj);
        }

        /// <summary>
        /// Handles the retrieval of the connection string present in the database based on the DSN ID
        /// </summary>
        /// <param name="iDSNID">DSN ID of the specified data source</param>
        /// <returns></returns>
        public static bool SelectDatabase(int iDSNID, ref string sErrorMsg)
        {
            bool blnDBExists = false;
            DbReader dbDSNRdr = null;

            try
            {
                using (dbDSNRdr = ADONetDbAccess.ExecuteReader(g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(iDSNID)))
                {
                    while (dbDSNRdr.Read())
                    {
                        blnDBExists = true;

                        //get the user id
                        g_UserId = RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString());

                        //form a connection string so we can connect and do rest of upgrade
                        g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), g_UserId, RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));

                        CheckForOracleString(ref g_sConnectString);

                        g_DsnId = iDSNID;//nadim for primary key

                        //get the database make
                        g_dbMake = GetDatabaseMake(g_sConnectString);

                        //get the database release and version information
                        gsDBRelease = ADONetDbAccess.ExecuteString(g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                        gsDBVersion = ADONetDbAccess.ExecuteString(g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                        if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                        {
                            g_bOrgSecOn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sErrorMsg = ex.Message;
                blnDBExists = false;
            }
            
            return blnDBExists;
        }

        /// <summary>
        /// CheckForOracleString
        /// </summary>
        /// <param name="g_sConnectString"></param>
        public static void CheckForOracleString(ref string g_sConnectString)
        {
            bool bOracleVer = true;
            string sOracleDriver = String.Empty;
            Regex regex = new Regex(@"^(?<start>.*){(?<driver>Oracle.*)}(?<end>.*)$", RegexOptions.IgnoreCase);
            Match match = regex.Match(g_sConnectString);

            if (match.Success)
            {
                string[] arrDrivers = UpgradeScripts.GetODBCDrivers();

                foreach (string driver in arrDrivers)
                {
                    if (driver.Contains("Oracle"))
                    {
                        //Deb : Removed Specific version check
                        bOracleVer = true;// CheckOracleClientVersion();

                        if (!bOracleVer)
                        {
                            g_sConnectString = String.Empty;
                        }
                        else
                        {
                            //sOracleDriver = driver;
                            sOracleDriver = match.Groups["driver"].Value;
                        }

                        break;
                    }
                }

                g_sConnectString = String.Format("{0}{{{1}}}{2}", match.Groups["start"].Value, sOracleDriver, match.Groups["end"].Value);
            }
        }
        #endregion

        /// <summary>
        /// gets the database type based on the specified database connection string
        /// </summary>
        /// <param name="strDBConnString">connection string</param>
        /// <returns>database type</returns>
        public static int GetDatabaseMake(string strDBConnString)
        {
            g_dbMake = ADONetDbAccess.DbType(strDBConnString);

            return g_dbMake;
        }

        /// <summary>
        /// Validates the various database connections configured
        /// in the connectionStrings.config file
        /// </summary>
        /// <returns></returns>
        public static List<ConnectionStringSettings> ValidateConnections()
        {
            List<ConnectionStringSettings> arrFailedConnections = new List<ConnectionStringSettings>();

            var connSettingsColl = DisplayDBUpgrade.GetGenericConnectionStringsCollection();

            var  filteredconnSettings = connSettingsColl.Where(connString => connString.Name != "LocalSqlServer" 
                && connString.Name != "OraAspNetConString"
                && !connString.Name.StartsWith("Pabblo")
                && !connString.Name.StartsWith("MDA")).ToList<ConnectionStringSettings>();

            foreach (ConnectionStringSettings connSetting in filteredconnSettings)
            {
                bool blnFailed = VerifyConnection(connSetting.ConnectionString);

                if (blnFailed == false)
                {
                    arrFailedConnections.Add(connSetting);
                }//if
                
            }//foreach

            return arrFailedConnections;
        }//method: ValidateConnections()

        public static bool VerifyConnection(string strConnString)
        {
            bool blnValidated = false;

            try
            {
                using (DbConnection dbConn = DbFactory.GetDbConnection(strConnString))
                {
                    dbConn.Open();
                }//using


                blnValidated = true;
            }
            catch (Exception ex)
            {
                string strLogFilePath = Application.StartupPath + "\\dBUpgrade.log";
                LogErrors(strLogFilePath, ex.Message + " stack trace:" + ex.StackTrace.ToString()); 
                blnValidated = false;  
            }

            return blnValidated;
        }//method: VerifyConnection

        /// <summary>
        /// based on app.config, determines whether expected directories and script files exist, etc.
        /// </summary>
        /// <returns></returns>
        public static bool ValidateStartup(bool bIsCustom)
        {
            bool blnVS = true;
            char[] strDelimiters = new[] { ',' };
            string[] arrRMSecurityScripts = new[] { String.Empty };
            string[] arrRMBaseScripts = new[] { String.Empty };
            string[] arrRMViewScripts = new[] { String.Empty };

            string strValidationDirectories = ConfigurationManager.AppSettings["VALIDATION_DIRECTORIES"];
            string[] arrValidationDirectories = strValidationDirectories.Split(strDelimiters, StringSplitOptions.RemoveEmptyEntries);

            if (!bIsCustom)
            {
                string strRMSecurityScripts = ConfigurationManager.AppSettings["RMSECURITYSQLSCRIPTS"];
                arrRMSecurityScripts = strRMSecurityScripts.Split(strDelimiters, StringSplitOptions.RemoveEmptyEntries);

                string strRMBaseScripts = ConfigurationManager.AppSettings["RMSQLSCRIPTS"];
                arrRMBaseScripts = strRMBaseScripts.Split(strDelimiters, StringSplitOptions.RemoveEmptyEntries);

                string strRMViewScripts = ConfigurationManager.AppSettings["RMVIEWSQLSCRIPTS"];
                arrRMViewScripts = strRMViewScripts.Split(strDelimiters, StringSplitOptions.RemoveEmptyEntries);
            }

            string sCmd = Environment.GetCommandLineArgs().ToString();
            g_bNoSessionMode = (sCmd.IndexOf("nosession") > 0);
            g_bNoPromptMode = (sCmd.IndexOf("noprompt") > 0);

            string strLogFilePath = Application.StartupPath + "\\dBUpgrade.log";

            StringBuilder strErrorMessage = new StringBuilder();

            //write missing script file name to log
            strErrorMessage.AppendLine("dBUgrade started: " + DateTime.Now);
            strErrorMessage.AppendLine();
            LogErrors(strLogFilePath, strErrorMessage.ToString());

            if (g_bNoPromptMode)
            {
                int iPos = sCmd.IndexOf("target:") + 7;
                int iPosEnd = sCmd.LastIndexOf(":");

                if (iPosEnd <= 0)
                {
                    string sTarget = sCmd.Substring(iPos, iPosEnd - iPos);
                    g_sNoPromptTargetDSN = sTarget;
                }
            }

            bool blnValidateStartUp = Convert.ToBoolean(ConfigurationManager.AppSettings["PERFORM_VALIDATION"]);
            int iFailed = 0;

            if (blnValidateStartUp)
            {
                foreach (string strDirectory in arrValidationDirectories)
                {
                    if (!Directory.Exists(Application.StartupPath + '\\' + strDirectory))
                    {
                        if (!Directory.Exists(Application.StartupPath + @"\..\..\" + strDirectory))
                        {
                            iFailed++;

                            strErrorMessage = new StringBuilder();

                            //write missing script file name to log
                            strErrorMessage.AppendLine("--- Error Msg: Could not find expected directory");
                            strErrorMessage.AppendLine("       directory-" + strDirectory);
                            LogErrors(strLogFilePath, strErrorMessage.ToString());
                        }
                    }
                    
                    if (strDirectory == "scripts")
                    {
                        //validate whether required and specified scripts exist before proceeding security scripts
                        foreach (string strScriptFilename in arrRMSecurityScripts)
                        {
                            if (!String.IsNullOrEmpty(strScriptFilename))
                            {
                                string sScriptPath = Application.StartupPath + '\\' + strDirectory + "\\" + strScriptFilename;
                                if (!UpdateFunctions.FileExists(sScriptPath))
                                {
                                    sScriptPath = String.Format(@"{0}\..\..\{1}\{2}", Application.StartupPath, strDirectory, strScriptFilename);
                                    if (!UpdateFunctions.FileExists(sScriptPath))
                                    {
                                        iFailed++;

                                        strErrorMessage = new StringBuilder();

                                        //write missing script file name to log
                                        strErrorMessage.AppendLine("- Error Msg: Could not find database upgrade script");
                                        strErrorMessage.AppendLine("       file-" + strScriptFilename);
                                        LogErrors(strLogFilePath, strErrorMessage.ToString());
                                    }
                                }
                            }
                        }

                        //validate whether required and specified scripts exist before proceeding base scripts
                        foreach (string strScriptFilename in arrRMBaseScripts)
                        {
                            if (!String.IsNullOrEmpty(strScriptFilename))
                            {
                                string sScriptPath = Application.StartupPath + '\\' + strDirectory + "\\" + strScriptFilename;
                                if (!UpdateFunctions.FileExists(sScriptPath))
                                {
                                    sScriptPath = String.Format(@"{0}\..\..\{1}\{2}", Application.StartupPath, strDirectory, strScriptFilename);
                                    if (!UpdateFunctions.FileExists(sScriptPath))
                                    {
                                        iFailed++;

                                        strErrorMessage = new StringBuilder();

                                        //write missing script file name to log
                                        strErrorMessage.AppendLine("- Error Msg: Could not find database upgrade script");
                                        strErrorMessage.AppendLine("       file-" + strScriptFilename);
                                        LogErrors(strLogFilePath, strErrorMessage.ToString());
                                    }
                                }
                            }
                        }

                        //validate whether required and specified scripts exist before proceeding view scripts
                        foreach (string strScriptFilename in arrRMViewScripts)
                        {
                            if (!String.IsNullOrEmpty(strScriptFilename))
                            {
                                string sScriptPath = Application.StartupPath + '\\' + strDirectory + "\\" + strScriptFilename;
                                if (!UpdateFunctions.FileExists(sScriptPath))
                                {
                                    sScriptPath = String.Format(@"{0}\..\..\{1}\{2}", Application.StartupPath, strDirectory, strScriptFilename);
                                    if (!UpdateFunctions.FileExists(sScriptPath))
                                    {
                                        iFailed++;

                                        strErrorMessage = new StringBuilder();

                                        //write missing script file name to log
                                        strErrorMessage.AppendLine("- Error Msg: Could not find database upgrade script");
                                        strErrorMessage.AppendLine("       file-" + strScriptFilename);
                                        LogErrors(strLogFilePath, strErrorMessage.ToString());
                                    }
                                }
                            }
                        }
                    }
                }

                if (iFailed > 0)
                {
                    blnVS = false;
                }
            }
            
            //add a blank line to log file
            strErrorMessage = new StringBuilder();
            strErrorMessage.AppendLine();
            LogErrors(strLogFilePath, strErrorMessage.ToString());

            return blnVS;
        }

        /// <summary>
        /// message box display - text only
        /// </summary>
        /// <param name="sMsg">message box text</param>
        /// <returns></returns>
        public static DialogResult DirectedMsgBox(string sMsg)
        {
            DirectedMsgBox(sMsg, MessageBoxButtons.OK, MessageBoxIcon.None, String.Empty);

            return DialogResult.Yes;
        }

        /// <summary>
        /// message box display - overload add msgbox button
        /// </summary>
        /// <param name="sMsg">message box text</param>
        /// <param name="buttons">message box button</param>
        /// <returns></returns>
        public static DialogResult DirectedMsgBox(string sMsg, MessageBoxButtons buttons)
        {
            DirectedMsgBox(sMsg, buttons, MessageBoxIcon.None, String.Empty);

            return DialogResult.Yes;
        }

        /// <summary>
        /// message box display - overload add msgbox title
        /// </summary>
        /// <param name="sMsg">message box text</param>
        /// <param name="buttons">message box button</param>
        /// <param name="sTitle">message box title</param>
        /// <returns></returns>
        public static DialogResult DirectedMsgBox(string sMsg, MessageBoxButtons buttons, string sTitle)
        {
            DirectedMsgBox(sMsg, buttons, MessageBoxIcon.None, sTitle);

            return DialogResult.Yes;
        }

        /// <summary>
        /// message box display - overload add msgbox icon
        /// </summary>
        /// <param name="sMsg">message box text</param>
        /// <param name="buttons">message box button</param>
        /// <param name="style">message box icon</param>
        /// <param name="sTitle">message box title</param>
        /// <returns></returns>
        public static DialogResult DirectedMsgBox(string sMsg, MessageBoxButtons buttons, MessageBoxIcon style, string sTitle)
        {
            return MessageBox.Show(sMsg, sTitle, buttons, style);
        }

        /// <summary>
        /// determines the file name and path for the Error Log file
        /// </summary>
        /// <returns>path</returns>
        public static string GetLogFilePath()
        {
            string strLogFilePath = String.Empty;

            //initialize the value for the parent directory
            string strDirectoryPath = Application.StartupPath;

            //intialize the value for the Error Log path
            if (!String.IsNullOrEmpty(sODBCName))
            {
                if (string.IsNullOrEmpty(sBaseLogType))
                {
                    strLogFilePath = String.Format("{0}\\dBUpgrade({1})_{2}.log", strDirectoryPath, sODBCName, DBTARGETVER);
                }
                else
                {
                    strLogFilePath = String.Format("{0}\\dBUpgrade({1})_{2}_{3}.log", strDirectoryPath, sODBCName, sBaseLogType, DBTARGETVER);
                }
                
            }
            else
            {
                strLogFilePath = strDirectoryPath + "\\dBUpgrade.log";
            }

            return strLogFilePath;
        }

        /// <summary>
        /// responsible for logging all errors as they occur during the database upgrade
        /// </summary>
        /// <param name="strLogFilePath"></param>
        /// <param name="sMsg"></param>
        public static void LogErrors(string strLogFilePath,string sMsg)
        {
            File.AppendAllText(strLogFilePath, sMsg);

            //Insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }

        /// <summary>
        /// Provides detailed .Net Exception Handling trace/debug information
        /// </summary>
        /// <param name="objDicErrorData"></param>
        /// <param name="strErrorMethod"></param>
        /// <param name="strErrorStackTrace"></param>
        /// <param name="strErrorMsg"></param>
        public static void LogErrors(IDictionary objDicErrorData, string strErrorMethod, string strErrorStackTrace, string strErrorMsg)
        {
            StringBuilder strErrorData = new StringBuilder();

            //get the path to the error Log File
            string strErrorLogPath = GetLogFilePath();

            foreach(var dictKey in objDicErrorData.Keys)
            {
                strErrorData.AppendLine(dictKey.ToString());
            }

            //append the remaining information to the Error Data String
            strErrorData.AppendLine(strErrorMethod);
            strErrorData.AppendLine(strErrorStackTrace);
            strErrorData.AppendLine(strErrorMsg);

            //write out the entire contents of the error message to the Log File
            File.AppendAllText(strErrorLogPath, strErrorData.ToString());
        }

        /// <summary>
        /// retreive next UID for given system table name
        /// </summary>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public static int GetNextUID(string sTableName)
        {
            int intNextUID = ADONetDbAccess.ExecuteScalar(g_sConnectString, RISKMASTERScripts.GetNextUID(UpdateFunctions.sSQLStringLiteral(sTableName)));
            ADONetDbAccess.ExecuteNonQuery(g_sConnectString, RISKMASTERScripts.UpdateGlossary(intNextUID + 1, UpdateFunctions.sSQLStringLiteral(sTableName)));

            return intNextUID;
        }


        /// <summary>
        /// Gets all of the connection strings currently configured in the ConnectionStrings section
        /// </summary>
        /// <returns></returns>
        public static ConnectionStringSettingsCollection GetConnectionStringsCollection()
        {
            return ConfigurationManager.ConnectionStrings;
        }//method: GetConnectionStringsCollection();

        public static List<ConnectionStringSettings> GetGenericConnectionStringsCollection()
        {
            var connSettings = GetConnectionStringsCollection();
            List<ConnectionStringSettings> arrConnSettings = new List<ConnectionStringSettings>();

            foreach (ConnectionStringSettings connSetting in connSettings)
            {
                arrConnSettings.Add(connSetting);
            }//foreach

            return arrConnSettings;
        }//method: GetConnectionStringsCollection();

        /// <summary>
        /// Check installed version of Oracle Client
        /// </summary>
        /// <returns></returns>
        /// Deb : Commented no need of any version
        //public static bool CheckOracleClientVersion()
        //{
        //    bool bValid = true;
        //    Version rmxVersion = null;
        //    Version clientVersion = null;
        //    StringBuilder sb = new StringBuilder();

        //    Assembly[] aRMXAssemblies = AppDomain.CurrentDomain.GetAssemblies();
        //    foreach (Assembly aObj in aRMXAssemblies)
        //    {
        //        Match m = Regex.Match(aObj.FullName, "oracle", RegexOptions.IgnoreCase);
        //        if (m.Success)
        //        {
        //            AssemblyName rmxName = aObj.GetName();
        //            rmxVersion = rmxName.Version;

        //            sb.AppendFormat("RMX Assembly version: {0}", rmxVersion);
        //            sb.AppendLine();
        //            break;
        //        }
        //    }
        //    sb.AppendLine();

        //    try
        //    {
        //        Assembly clientAssembly = Assembly.Load("Oracle.DataAccess, Version=2.111.7.0, Culture=neutral, PublicKeyToken=89b483f429c47342");

        //        AssemblyName clientName = clientAssembly.GetName();
        //        clientVersion = clientName.Version;

        //        sb.AppendFormat("Client Assembly version: {0}", clientVersion);
        //        sb.AppendLine();
        //        sb.AppendLine();
        //    }
        //    catch (System.IO.FileNotFoundException ex)
        //    {
        //        sb.AppendFormat("Error: {0}", ex.Message);
        //        bValid = false;
        //    }
        //    catch (Exception ex2)
        //    {
        //        sb.AppendFormat("Error: {0}", ex2.Message);
        //        bValid = false;
        //    }

        //    int iSame = rmxVersion.CompareTo(clientVersion);
        //    if (iSame != 0)
        //    {
        //        sb.AppendLine("The provider is not compatible with the version of Oracle client.");
        //        sb.AppendLine();
        //        sb.AppendLine("You must install Oracle Client 11gR2 (32bit only) in order to proceed.");
        //        bValid = false;

        //        MessageBox.Show(sb.ToString(), "debug", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
            
        //    return bValid;
        //}

        /// <summary>
        /// Gets the appropriate connection string based on the specified enumeration type
        /// </summary>
        /// <param name="connStringType"></param>
        /// <returns></returns>
        public static string GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings connStringType)
        {
            string strConnString = string.Empty;
            ConnectionStringSettingsCollection connSettings = GetConnectionStringsCollection();

            switch (connStringType)
            {
                case DbUpgradeConnectionStrings.RMTaskManager:
                    strConnString = connSettings["TaskManagerDataSource"].ConnectionString;
                    break;
                case DbUpgradeConnectionStrings.RMSecurity:
                    strConnString = connSettings["RMXSecurity"].ConnectionString;
                    break;
                //case DbUpgradeConnectionStrings.RMHistory:
                //    strConnString = connSettings["HistoryDataSource"].ConnectionString;
                //    break;
                case DbUpgradeConnectionStrings.RMViews:
                    strConnString = connSettings["ViewDataSource"].ConnectionString;
                    break;
                case DbUpgradeConnectionStrings.RMSession:
                    strConnString = connSettings["SessionDataSource"].ConnectionString;
                    break;
                case DbUpgradeConnectionStrings.RMSortmaster:
                    strConnString = connSettings["ReportServer_ConnectionString"].ConnectionString;
                    break;
                default:
                    break;
            }//switch

            return strConnString;
        }//method: GetDbUpgradeConnectionStrings();
    }//class

    /// <summary>
    /// Enumeration for Database Upgrade Connection Strings
    /// </summary>
    public enum DbUpgradeConnectionStrings
    {
        RMTaskManager,
        RMSecurity,
        //RMHistory,
        RMViews,
        RMSession,
        RMSortmaster
    }
}//namespace