﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using DevComponents.DotNetBar;
using Riskmaster.Db;

namespace RMXdBUpgradeWizard
{
    class UpdateFunctions
    {
        #region " VARIABLE DECLARATIONS "

        #region " PRIVATE VARIABLES "
        private static string sConnDb = string.Empty;
               
        //They may be changed in the script by the {ASSIGN] statments in the script file.

        //THIS MODULE IS FOR ANY GENERAL ROUTINES THAT ALL UPGRADE MODULES MAY WANT TO USE.
        //IN GENERAL, DO EVERYTHING POSSIBLE FROM YOUR ROUTINE SINCE THESE ROUTINES AREN'T
        //GUARANTEED TO WORK ON FUTURE VERSIONS OF RM/WIN.

        // Added by abhinav- Mits 33063
        public static int result = 1;

        public static string[] sParmArray = new string[9]; //used by execute sql - these values may be initialized by the call to bExecuteSQLScript
        #endregion //PRIVATE VARIABLES

        #region " PUBLIC VARIABLES "
        public static int g_Ifnest; // Level of IF nesting (only executes line if == 0)
        public static int iDBMake; //dB Type
        public static Boolean bForgive = false;
        public static string sODBCError = string.Empty;
        public static string sNativeError = string.Empty;
        public static int iLineCount = 0;
        public static bool _bDebugModeLogging = false;
        public static bool _bCopyCustomViews = false;
        #endregion //PUBLIC VARIABLES

        #endregion //VARIABLE DECLARATIONS

        #region "PROPERTIES"
        public static string ConnectedDB
        {
            get
            {
                return sConnDb;
            }
            set
            {
                sConnDb = value;
            }
        }
        #endregion
        /// <summary>
        /// processes and executes the specified SQL contained in the SQL Script files
        /// </summary>
        /// <param name="strDBConnString"></param>
        /// <param name="sScriptFilename"></param>
        /// <param name="sParms"></param>
        /// <returns></returns>
        public static void ExecuteSQLScriptExtended(string strDBConnString, string sScriptFilename, string sParms, bool bIsSilent)
        {
            #region local variables
            bool bProceed = false;
            bool bGoMode = false;
            bool bHasParms = false;
            int iCount = 0;
            int iDebugCount = -1;
            int iProgressCount = 0;
            string sSQL = String.Empty;
            string sLine = String.Empty;
            string sScriptPath = String.Empty;
            bool bIsIfCondition = false;
            #endregion

            iLineCount = 0;

            //set as IF block are encountered that are designed to stop SQL script from executing under certain circumstances
            g_Ifnest = 0;

            //parse out parms into a global array
            for (iCount = 1; iCount < 9; iCount++)
            {
                sParmArray[iCount] = String.Empty; //blank out all parms first
            } // for

            iCount = 1;
            char c = Convert.ToChar(9);

            if (String.IsNullOrEmpty(sParms.Trim()))
            {
                bHasParms = false;
            }//if
            else
            {
                bHasParms = true;

                if (sParms.IndexOf(c) > 0)
                {
                    string[] sTmp = sParms.Split(c);

                    foreach (string strParm in sTmp)
                    {
                        if (String.IsNullOrEmpty(strParm))
                        {
                            continue;
                        }//if

                        sParmArray[iCount] = strParm;
                        iCount++;
                    }//foreach
                }//if
                else
                {
                    sParmArray[iCount] = String.Empty;
                }//else
            }//else

            bHasParms = (String.IsNullOrEmpty(sParms.Trim())) ? false : true;

            //get the path for the scripts to be executed
            sScriptPath = GetScriptPath(GetSystemPath(), sScriptFilename);

            if (!CheckIfScriptExists(sScriptFilename, bIsSilent, ref sScriptPath))
            {
                return;
            }

            StreamReader sr = File.OpenText(sScriptPath);

            do
            {
                iDebugCount++;
            } while ((sr.ReadLine()) != null);

            sr.Close();

            if (!bIsSilent)
            {
                //set status bar maximum on process form
                frmWizard.SetCurrentProgressBarProperties(iDebugCount);
            }

            //Deb Multi Currency Chnages
            //Changed the Encoding to Default as UTF8 was not supporting € currency symbol
            //sr = File.OpenText(sScriptPath);
            sr = new StreamReader(sScriptPath,UTF8Encoding.Default);


            //log the name of the file
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(),"\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sScriptFilename + " Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            do
            {
                try
                {
                    //read the line from the file
                    sLine = sr.ReadLine();

                    //increment the counter after reading the line
                    iProgressCount++;
                    iLineCount++;
                    //start-rsushilaggar: added functionality to check the if condition in the sql script.
                    //DATE - 06/24/2010
                    if (bIsIfCondition)
                    {
                        if(sLine.ToUpper().Contains("[ENDIF]"))
                            bIsIfCondition = false;
                        continue;
                    }
                    //End rsushilaggar
                    if ((sLine.Contains("\r") | sLine.Contains("\n")))
                    {
                        //frmWizard.ErrorDisplay("<b>Database upgrade script may be damaged. <br/>Please contact CSC RISKMASTER Technical Support.</b>", "error", eWizardButtonState.False);
                        //DisplayDBUpgrade.bStop = true;

                        StringBuilder strErrorMsg = new StringBuilder();

                        strErrorMsg.AppendLine("--- ErrorMsg: Database upgrade script may be damaged");
                        strErrorMsg.AppendLine(" file-" + sScriptFilename + " line-" + iLineCount + ") " + sLine);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMsg.ToString());
                        return;
                    }//if

                    if (sLine.IndexOf(";") != 0 &&
                        sLine.IndexOf("'") != 0 &&
                        sLine.IndexOf("REM") != 0 &&
                        sLine.IndexOf("rem") != 0 &&
                        !String.IsNullOrEmpty(sLine.Trim())
                       )
                    {
                        if ((sLine.Trim() == "[GO_DELIMITER]") || (sLine.Trim() == "[GO_DELIMETER]"))
                        {
                            bGoMode = true;
                            sSQL = String.Empty;
                        }//if
                        else if (IsPseudoStatement(sLine))
                        {
                            //SPECIFIC FUNCTION PROCESSING or error
                                if (!ProcessPseudoStatement(sLine, strDBConnString, iLineCount, iDebugCount, sScriptFilename, bIsSilent, ref bIsIfCondition))
                            {
                                return;
                            }

                            //frmWizard.UpdateExecutingText(sLine);
                            sSQL = String.Empty;
                            bProceed = false;
                        }//else if
                        else
                        {
                            bProceed = true;

                            //collect line per statement mode
                            if (bGoMode)
                            {
                                if (sLine.ToUpper().Trim() != "GO")
                                {
                                    bProceed = false;

                                    if (DisplayDBUpgrade.g_dbMake == 4)
                                    {
                                        sSQL = sSQL + String.Format(" {0} ", sLine);
                                    }
                                    else
                                    {
                                        sSQL += sLine + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    sLine = sSQL;
                                    bGoMode = false;
                                }
                            }//if
                            else
                            {
                                sSQL = sLine; //single line per statement mode
                            }

                            if (bProceed && !String.IsNullOrEmpty(sLine.Trim()))
                            {
                                if (!bGoMode)
                                {
                                    //subst parms if available and subst in any carriage returns
                                    sSQL = sFormatMsgString(sLine);
                                }

                                //check and see if line is dB specific
                                sSQL = sCheckDBSpecific(sSQL);

                                if (iDBMake == -1 || iDBMake == DisplayDBUpgrade.g_dbMake)
                                {
                                    if (!bIsSilent)
                                    {
                                        frmWizard.UpdateExecutingText(sLine);
                                    }

                                    //check if the sql statement is trying to create an existing table again
                                    if (!CreateDuplicateTable(sSQL, strDBConnString))
                                    {
                                        //check for upgrade SQL extensions
                                        sSQL = CheckForgive(sSQL);

                                        //check to see if we need to directly execute statement
                                        sSQL = sCheckDBSpecific(sSQL);

                                        if (!bIsSilent)
                                        {
                                            frmWizard.UpdateExecutingText(sLine);
                                        }

                                        if (g_Ifnest == 0) //only execute if not in an IF statement
                                        {
                                            if (!bForgive)
                                            {
                                                //UPDATE SYS_PARMS SET PRODUCT_BUILD = '%BUILD_NUMBER%-rmx-r3-original'
                                                if (sSQL.Contains("rmx-r3-original"))
                                                {
                                                    sSQL = String.Format("UPDATE SYS_PARMS SET PRODUCT_BUILD = '{0}'", DisplayDBUpgrade.DBTARGETVER);
                                                }
                                                ADONetDbAccess.ExecuteNonQuery(strDBConnString, sSQL);
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    //Deb Changes 
                                                    int iCountCheck = CheckForAssign(ref sLine);
                                                    if (iCountCheck > 0)
                                                    {
                                                        NameValueCollection nvColl = new NameValueCollection();
                                                        for (int i = 1; i <= iCountCheck; i++)
                                                        {
                                                           nvColl.Add(i.ToString().Trim(), sParmArray[i].ToString());
                                                        }
                                                        ADONetDbAccess.ExecuteNonQuery(strDBConnString, sSQL, nvColl);
                                                    }
                                                    //Deb Changes 
                                                    else
                                                    {
                                                        ADONetDbAccess.ExecuteNonQuery(strDBConnString, sSQL);
                                                    }
                                                    //Update Hist_Track_Dictionary table : CDC ukusvaha
                                                    if(ConnectedDB == "RMX Base DB")
                                                        UpdateHistTrackDictionary(sSQL);
                                                }//try
                                                catch (Exception ex)
                                                {
                                                    string sTemp = ex.Message;

                                                    if (sTemp.ToUpper().IndexOf("ALREADY") >= 0 |
                                                        sTemp.ToUpper().IndexOf("DUPLIC") >= 0 |
                                                        sTemp.ToUpper().IndexOf("UNIQUE") >= 0)
                                                    {
                                                        result = 0; //// Added by abhinav- Mits 33063
                                                        sTemp = String.Format("- Error Msg: {0} - {1}", iLineCount, sTemp);
                                                    }
                                                    else if (sTemp.ToUpper().IndexOf("SYNTAX") >= 0)
                                                    {
                                                        sTemp = String.Format("- Error Msg: {0} - {1}", iLineCount, sTemp);
                                                    }
                                                    else
                                                    {
                                                        sTemp = String.Format("- Error Msg: {0} - {1}", iLineCount, sTemp);
                                                    }

                                                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sTemp);
                                                }//catch
                                            }//else
                                        }//if
                                    }//if
                                }//if

                                sSQL = String.Empty;
                            }//if
                        }//else
                    }//if
                }//try
                catch (System.Data.Odbc.OdbcException ex)
                {
                    ProcessException(sSQL, iLineCount, sScriptFilename, ex.Message);
                    continue;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    ProcessException(sSQL, iLineCount, sScriptFilename, ex.Message);
                    continue;
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.Message;
                    strErrorMessage = "--- Error Msg: [UpdateFunctions.ExecuteSQLScriptExtended] " + strErrorMessage +
                                      " - file: " + sScriptFilename + ", line: " + iLineCount;
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage);
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), String.Format("---            [SQL Query]: {0}", sSQL));

                    continue;
                }

                if (!bIsSilent)
                {
                    frmWizard.UpdateCurrentProgressBar(iProgressCount);
                }
                else
                {
                    #region commented out code
                    //string sProgress = String.Empty;
                    //if (((iLineCount % 100) == 0) && (iLineCount > 0))
                    //{
                    //    sProgress = "+" + Environment.NewLine;
                    //}
                    //else
                    //{
                    //    sProgress = "-";
                    //}

                    //Console.Write(sProgress);
                    #endregion
                }
            } while (!sr.EndOfStream);

            sr.Close();
            sr.Dispose();

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), String.Format("{0} Finished - {1}\r\n", sScriptFilename, DateTime.Now));
        }

        private static int CheckForAssign(ref string sLine)
        {
            int iCount = 0;
            int nPos = 0;
            sLine = sLine.Replace("\n", Environment.NewLine);
            for (int i = 1; i < 10; i++)
            {
                nPos = sLine.IndexOf("~" + i.ToString().Trim() + "~");
                if (nPos != -1)
                {
                    iCount = iCount + 1;
                }
            }
            return iCount;
        }

        /// <summary>
        /// CheckIfScriptExists
        /// </summary>
        /// <param name="sScriptFilename"></param>
        /// <param name="bIsSilent"></param>
        /// <param name="sScriptPath"></param>
        /// <returns></returns>
        public static bool CheckIfScriptExists(string sScriptFilename, bool bIsSilent, ref string sScriptPath)
        {
            bool bScriptExists = true;

            if (!FileExists(sScriptPath)) //not found
            {
                sScriptPath = GetDevPath(GetSystemPath(), sScriptFilename);

                if (!FileExists(sScriptPath)) //not found
                {
                    StringBuilder strErrorMessage = new StringBuilder();

                    strErrorMessage.AppendLine("--- Error Msg: Could not find database upgrade script ");
                    strErrorMessage.AppendLine("file-" + sScriptFilename);

                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                    if (!bIsSilent)
                    {
                        //send error back to wizard and abort process
                        frmWizard.ErrorDisplay("Could not find database upgrade script: <b><font color='#000000'>" + sScriptFilename + "</font></b><br/><br/>Please contact CSC RISKMASTER Technical Support.", "error", eWizardButtonState.False, String.Empty);
                    }
                    else
                    {
                        Console.WriteLine("Could not find database upgrade script: " + sScriptFilename + "\n\nPlease contact CSC RISKMASTER Technical Support.");
                    }

                    DisplayDBUpgrade.bStop = true;

                    bScriptExists = false;
                }//if
            }//if

            return bScriptExists;
        }

        /// <summary>
        /// InsertStoredProcedure
        /// </summary>
        /// <param name="strDBConnString"></param>
        /// <param name="sScriptFilename"></param>
        /// <param name="bIsSilent"></param>
        public static bool InsertStoredProcedure(string strDBConnString, string sScriptFilename, int iDBMake, bool bIsSilent)
        {
            #region local variables
            bool bExecuteSP = false;
            bool bProceed = false;
            int iDebugCount = -1;
            int iProgressCount = 0;
            string sSQL = String.Empty;
            string sLine = String.Empty;
            string sScriptPath = String.Empty;
            #endregion

            iLineCount = 0;

            //get the path for the scripts to be executed
            sScriptPath = GetScriptPath(GetSystemPath(), sScriptFilename);

            if (!CheckIfScriptExists(sScriptFilename, bIsSilent, ref sScriptPath))
            {
                return bExecuteSP;
            }

            StreamReader sr = File.OpenText(sScriptPath);

            do
            {
                iDebugCount++;
            } while ((sr.ReadLine()) != null);

            sr.Close();

            if (!bIsSilent)
            {
                //set status bar maximum on process form
                frmWizard.SetCurrentProgressBarProperties(iDebugCount);
            }

            sr = File.OpenText(sScriptPath);

            //log the name of the file
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(),"\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sScriptFilename + " Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            do
            {
                try
                {
                    //read the line from the file
                    sLine = sr.ReadLine();

                    //increment the counter after reading the line
                    iProgressCount++;
                    iLineCount++;

                    if ((sLine.Contains("\r") | sLine.Contains("\n")))
                    {
                        StringBuilder strErrorMsg = new StringBuilder();

                        strErrorMsg.AppendLine("--- ErrorMsg: Database upgrade script may be damaged");
                        strErrorMsg.AppendLine(" file-" + sScriptFilename + " line-" + iLineCount + ") " + sLine);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMsg.ToString());

                        return bExecuteSP;
                    }//if

                    if ((sLine.Trim() == "[GO_DELIMITER]") || (sLine.Trim() == "[GO_DELIMETER]"))
                    {
                        sSQL = String.Empty;
                    }
                    else if (sLine.Trim() == "[EXECUTE_SP]")
                    {
                        bExecuteSP = true;
                    }
                    else
                    {
                        //collect line per statement mode
                        if (sLine.ToUpper().Trim() != "GO")
                        {
                            bProceed = false;

                            if (iDBMake == 4)
                            {
                                sSQL = sSQL + String.Format(" {0} ", sLine) + " \n";
                            }
                            else
                            {
                                sSQL += sLine + Environment.NewLine;
                            }
                        }
                        else
                        {
                            bProceed = true;
                            sLine = sSQL;
                        }

                        if (bProceed && !String.IsNullOrEmpty(sLine.Trim()))
                        {
                            if (!bIsSilent)
                            {
                                frmWizard.UpdateExecutingText(sLine);
                            }

                            try
                            {
                                ADONetDbAccess.ExecuteNonQuery(strDBConnString, sLine);
                            }//try
                            catch (Exception ex)
                            {
                                string sTemp = ex.Message;

                                sTemp = String.Format("- Error Msg: {0} - {1}", iLineCount, sTemp);

                                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sTemp);
                            }//catch

                            sSQL = String.Empty;
                        }//if
                    }//else
                }//try
                catch (System.Data.Odbc.OdbcException ex)
                {
                    ProcessException(sSQL, iLineCount, sScriptFilename, ex.Message);
                    continue;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    ProcessException(sSQL, iLineCount, sScriptFilename, ex.Message);
                    continue;
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.Message;
                    strErrorMessage = "--- Error Msg: [UpdateFunctions.ExecuteSQLScriptExtended] " + strErrorMessage +
                                      " - file: " + sScriptFilename + ", line: " + iLineCount;
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage);
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), String.Format("---            [SQL Query]: {0}", sSQL));

                    continue;
                }

                if (!bIsSilent)
                {
                    frmWizard.UpdateCurrentProgressBar(iProgressCount);
                }
            } while (!sr.EndOfStream);

            sr.Close();
            sr.Dispose();

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), String.Format("{0} Finished - {1}\r\n", sScriptFilename, DateTime.Now));

            return bExecuteSP;
        }

        /// <summary>
        /// replace single quote with two(2) single quotes
        /// </summary>
        /// <param name="sText"></param>
        /// <returns></returns>
        public static string sSQLStringLiteral(string sText)
        {
            return sText.Replace("'", "''");
        }

        /// <summary>
        /// obtains the current executables directory path
        /// </summary>
        /// <returns></returns>
        public static string GetSystemPath()
        {
            string strDirectoryPath = String.Empty;

            if (Application.StartupPath.EndsWith("\\"))
            {
                strDirectoryPath = Application.StartupPath;
            }
            else
            {
                strDirectoryPath = Application.StartupPath + "\\";
            }

            return strDirectoryPath;
        }

        /// <summary>
        /// Obtains the scripts directory from the specified path
        /// </summary>
        /// <param name="strRootPath"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static string GetScriptPath(string strRootPath, string strFileName)
        {
            return strRootPath + "scripts\\" + strFileName;
        }

        /// <summary>
        /// Obtains the scripts directory from 2 directories below if in development environment
        /// </summary>
        /// <param name="sPath"></param>
        /// <param name="sFileName"></param>
        /// <returns></returns>
        public static string GetDevPath(string sPath, string sFileName)
        {
            return String.Format(@"{0}..\..\scripts\{1}", sPath, sFileName);
        }

        /// <summary>
        /// test for existance of specified file
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns>true or false</returns>
        public static bool FileExists(string FileName)
        {
            return File.Exists(FileName);
        }

        /// <summary>
        /// process exception from try....catch blocks
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="iLine"></param>
        /// <param name="sScriptFilename"></param>
        /// <param name="sError"></param>
        public static void ProcessException(string sSQL, int iLine, string sScriptFilename, string sError)
        {
            bool bShowHighPriorityErrors = true;

            if (sError.IndexOf(" already") > 0 | sError.IndexOf(" duplic") > 0 | sError.IndexOf(" unique") > 0)
            {
                sError = "- Error Msg: " + sError;
                bShowHighPriorityErrors = false;
            }
            else if (sError.IndexOf("Syntax") > 0 | sError.IndexOf(" syntax") > 0)
            {
                sError = "--- Error Msg: " + sError;
            }
            else
            {
                sError = "-- Error Msg: " + sError;
            }

            if (sError.Substring(2) != "\n")
            {
                sError += Environment.NewLine;
            }

            sError += "file: " + sScriptFilename + ", line: " + iLine.ToString().Trim();

            if (_bDebugModeLogging)//rslanki2: logging updates 
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sError + "(DebugLog):" + sSQL + Environment.NewLine);
                MessageBox.Show("DebugLog: error processing sql > " + sSQL);
                
            }
            else if (bShowHighPriorityErrors)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), sError + ":" + sSQL + Environment.NewLine);
            }
        }

        /// <summary>
        /// check if SQL Statement is dB Specific
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public static string sCheckDBSpecific(string sSQL)
        {
            iDBMake = -1;
            string sTemp = sSQL.Trim();
            int iSqlLen = sTemp.Length;

            if (iSqlLen >= 12)
            {
                if (sTemp.ToUpper().Substring(0, 12) == "[SQL SERVER]")
                {
                    sTemp = sTemp.Substring(12);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_SQLSRVR;
                }
            }

            if (iSqlLen >= 10)
            {
                if (sTemp.ToUpper().Substring(0, 10) == "[INFORMIX]")
                {
                    sTemp = sTemp.Substring(10);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_INFORMIX;
                }
            }

            if (iSqlLen >= 9)
            {
                if (sTemp.ToUpper().Substring(0, 9) == "[SQLSRVR]")
                {
                    sTemp = sTemp.Substring(9);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_SQLSRVR;
                }
            }

            if (iSqlLen >= 8)
            {
                if (sTemp.ToUpper().Substring(0, 8) == "[SYBASE]")
                {
                    sTemp = sTemp.Substring(8);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_SYBASE;
                }

                if (sTemp.ToUpper().Substring(0, 8) == "[ACCESS]")
                {
                    sTemp = sTemp.Substring(8);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_ACCESS;
                }

                if (sTemp.ToUpper().Substring(0, 8) == "[ORACLE]")
                {
                    sTemp = sTemp.Substring(8);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_ORACLE;
                }
            }

            if (iSqlLen >= 5)
            {
                if (sTemp.ToUpper().Substring(0, 5) == "[DB2]")
                {
                    sTemp = sTemp.Substring(5);
                    iDBMake = RiskmasterDBTypes.DBMS_IS_DB2;
                }
            }

            return sTemp;
        }

        /// <summary>
        /// check if it's a create table statement and if the table already exists
        /// </summary>
        /// <param name="sCommand"></param>
        /// <param name="strDBConnString"></param>
        /// <returns></returns>
        public static bool CreateDuplicateTable(string sCommand, string strDBConnString)
        {
            bool bReturn = false;
            int intRecords = 0;

            string sTableName = GetNewTableName(sCommand);

            if (!String.IsNullOrEmpty(sTableName))
            {
                try
                {
                    string strSQL = "SELECT COUNT(*) FROM " + sTableName + " WHERE 0=1";
                    intRecords = ADONetDbAccess.ExecuteScalar(strDBConnString, strSQL);
                }
                catch(Exception)
                {
                    intRecords = 0;
                }
            }

            if (intRecords > 0)
            {
                bReturn = true;
            }

            return bReturn;
        }

        /// <summary>
        /// get the table name which is to be created
        /// </summary>
        /// <param name="sCommand"></param>
        /// <returns></returns>
        public static string GetNewTableName(string sCommand)
        {
            Regex TABLE_EXP_PATTERN = new Regex(@"[\w\W]*CREATE\s+TABLE\s+", RegexOptions.IgnoreCase);
            MatchCollection myMatches = TABLE_EXP_PATTERN.Matches(sCommand);
            string sTableName = String.Empty;

            foreach (var myMatch in myMatches)
            {
                string sMatch = myMatch.ToString();
                string sPartCommand = sCommand.Substring(sMatch.Length);
                int i = sPartCommand.IndexOf("(");
                if (i > 0)
                {
                    sTableName = sPartCommand.Substring(0, i - 1);
                }
            }

            return sTableName.Trim();
        }

       

      
        /// <summary>
        /// get max ID for specified table
        /// </summary>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public static int GetMaxFieldID(string sTableName)
        {
            int intMaxFieldID = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.GetMaxID(sTableName));
            return intMaxFieldID;
        }

        public static int GetMaxRecordID(string sTableName,string sFieldName)
        {
            int intMaxFieldID = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.GetMaxRecordID(sTableName,sFieldName));
            return intMaxFieldID;
        }

        public static int GetMaxRecordID(string sTableName, string sFieldName,string sConnectionString)
        {
            int intMaxFieldID = ADONetDbAccess.ExecuteScalar(sConnectionString, RISKMASTERScripts.GetMaxRecordID(sTableName, sFieldName));
            return intMaxFieldID;
        }

        /// <summary>
        /// format message string
        /// </summary>
        /// <param name="sText"></param>
        /// <returns></returns>
        public static string sFormatMsgString(string sText)
        {
            int nPos = 0;
            StringBuilder sbWork = new StringBuilder();
            string sFirstPart = String.Empty;
            string sSecondPart = String.Empty;

            //plug in carriage returns
            sText = sText.Replace("\n", Environment.NewLine);

            //plug in parameters
            for (int i = 1; i < 10; i++)
            {
                nPos = sText.IndexOf("%%" + i.ToString().Trim());
                if(nPos != -1)
                {
                    sText = sText.Replace("%%" + i.ToString(), sParmArray[i]);
                }
            }

            return sText;
        }

        /// <summary>
        /// get and return state_row_id
        /// </summary>
        /// <param name="sState"></param>
        /// <returns></returns>
        public static int GetStateID(string sState)
        {
            string sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID ='" + sState.Trim() + "'";
            return ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
        }

        /// <summary>
        /// return the current date and time in string format
        /// </summary>
        /// <returns></returns>
        public static string GetDateTimeFormat()
        {
            return DateTime.Now.ToString("yyyyMMddHHmm00");
        }

        /// <summary>
        /// update glossary time stamp for specified table
        /// </summary>
        /// <param name="sTableName"></param>
        public static void UpdateGlossaryTimeStamp(string sTableName)
        {
            string sTmp = GetDateTimeFormat();
            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + sTmp + "'  WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'");
        }

        /// <summary>
        /// check if SQL statement is a pseudo/false statement
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public static bool IsPseudoStatement(string sSQL)
        {
            bool bIsPseudo = false;
            int iSqlLen = sSQL.Length;

            if (iSqlLen >= 4)
            {
                if (sSQL.ToUpper().Substring(0, 4) == "[IF ")
                {
                    bIsPseudo = true;
                }
            }

            if (iSqlLen >= 6)
            {
                if (sSQL.ToUpper().Substring(0, 6) == "[ENDIF")
                {
                    bIsPseudo = true;
                }
            }

            if (iSqlLen >= 7)
            {
                if (sSQL.ToUpper().Substring(0, 7) == "[ASSIGN")
                {
                    bIsPseudo = true;
                }
            }

            if (iSqlLen >= 15)
            {
                if (sSQL.ToUpper().Substring(0, 15) == "[UPDT_TBL_STAMP")
                {
                    bIsPseudo = true;
                }
            }

            return bIsPseudo;
        }

        /// <summary>
        /// process line with specific function ASSIGN, IF, etc.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="strDBConnString"></param>
        /// <param name="iLine"></param>
        /// <param name="iDCount"></param>
        /// <param name="sScriptFileName"></param>
        /// <param name="bIsSilent"></param>
        /// <param name="IsIfCondition"></param>
        /// <returns></returns>
        public static bool ProcessPseudoStatement(string sSQL, string strDBConnString, int iLine, int iDCount, string sScriptFileName, bool bIsSilent, ref bool IsIfCondition)
        {
            #region local variables
            bool bContinue = true;
            var nPos = 0;
            var nNumParms = 0;
            var nRegister = 0;
            string sWork = String.Empty;
            string sParm = String.Empty;
            string sTemp2 = String.Empty;
            string sFunction = String.Empty;
            List<string> sTmpArray = new List<string>();
            StringBuilder strErrorMessage = new StringBuilder();
            strErrorMessage.Length = 0;
            strErrorMessage.Capacity = 0;
            #endregion

            sWork = sSQL.Trim();

            //UpdateDisplay.RefreshForm2(iLine, sSQL);

            try
            {
                //remove brackets from statement
                if (sWork.Substring(0, 1) != "[" | sWork.Substring(sWork.Length - 1) != "]")
                {
                    //SYNTAX ERROR - incorrect delimiter for pseudo statement
                    strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Parse Error");
                    strErrorMessage.AppendLine("Incorrect Bracketing of Pseudo Statement.");
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT PARSE ERROR</b><br /><br /> Incorrect Bracketing of Pseudo Statement.", "error", eWizardButtonState.False, String.Empty);
                    //DisplayDBUpgrade.bStop = true;

                    return false;
                }

                sWork = sWork.Substring(1, sWork.Length - 2);

                //clean up log file
                if (sWork == "ENDIF")
                {
                    return true; 
                }

                //determine which pseudo statement this line is
                if (sWork.Substring(0, 6).ToUpper() == "ASSIGN")
                {
                    if (g_Ifnest > 0)
                    {
                        return true; //leave if we are in an IF block we shouldn't be executing
                    }

                    //ASSIGN = assigns values to a register (%%1 through %%9)
                    //strip off assign pseudo statement keyword
                    sWork = sWork.Substring(7).Trim();
                    sTemp2 = sWork.Substring(0, 4);

                    if (sTemp2.IndexOf("%%") < 0 | Convert.ToInt32(sTemp2.Substring(2, 1)) > 9 | Convert.ToInt32(sTemp2.Substring(2, 1)) < 0 | sTemp2.IndexOf("=") != 3)
                    {
                        //SYNTAX ERROR - unexpected format
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Parse Error");
                        strErrorMessage.AppendLine("Incorrect Register Assignment Syntax.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT PARSE ERROR</b><br /><br /> Incorrect Register Assignment Syntax.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }

                    nRegister = Convert.ToInt32(sTemp2.Substring(2, 1));

                    //strip off register assignment portion
                    sWork = sWork.Substring(4).Trim();
                    //Deb MTS 31020 : skip the replacement of %%1,%%2 etc for ML Enh.
                    int iPos = sWork.IndexOf("(");
                    string sMethodName = sWork.Substring(0, iPos).Trim().ToUpper();
                    if (sMethodName == "ADD_GLOBAL_RESOURCE" || sMethodName == "ADD_LOCAL_RESOURCE") { ;}
                    else
                    {
                        //process remainder through parameter substitution function
                        sWork = sFormatMsgString(sWork);
                    }

                    //determine assignment function
                    if (sWork.Substring(0, 6).ToUpper() != "QUERY(" & sWork.IndexOf("(") == -1)
                    {
                        //SYNTAX ERROR - function format incorrect
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Parse Error");
                        strErrorMessage.AppendLine("Pseudo Function Is Missing Open Parenthesis.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT PARSE ERROR</b><br /><br /> Pseudo Function Is Missing Open Parenthesis.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }

                    //query security database
                    if (sWork.Substring(0, 9).ToUpper() == "QUERYSEC(")
                    {
                        sFunction = "QUERYSEC";
                        sWork = sWork.Substring(9).Trim();
                    }
                    else if (sWork.Substring(0, 6).ToUpper() == "QUERY(") //special processing for query directive
                    {
                        sFunction = "QUERY";
                        sWork = sWork.Substring(6).Trim();
                    }
                    //Deb : Changes
                    else if (sWork.Substring(0, 6).ToUpper() == "VALUE(")
                    {
                        sWork = sWork.Substring(6).Trim();
                        sWork = sWork.Substring(0, sWork.Length - 1).Trim();
                        sParmArray[nRegister] = sWork.ToString().Trim();
                        return true;
                    }
                    //Deb : Changes
                    else
                    {
                        nPos = sWork.IndexOf("(");
                        sFunction = sWork.Substring(0, nPos).Trim().ToUpper();
                        sWork = sWork.Substring(nPos + 1);
                    }

                    if (sWork.Substring(sWork.Length - 1) != ")")
                    {
                        //SYNTAX ERROR - invalid function syntax - missing closing parenthesis
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Parse Error");
                        strErrorMessage.AppendLine("Pseudo Function Is Missing Closing Parenthesis.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT PARSE ERROR</b><br /><br /> Pseudo Function Is Missing Closing Parenthesis.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }
                    //strip off closing parenthesis
                    sWork = sWork.Substring(0, sWork.Length - 1).Trim();

                    if (sFunction == "QUERY" | sFunction == "QUERYSEC")
                    {
                        nNumParms = 1;
                        sTmpArray.Clear();
                        sTmpArray.Add(sWork);
                    }
                    else
                    {
                        //tear out parameters to function
                        nNumParms = 0;
                        sTmpArray.Clear();

                        do
                        {
                            nNumParms++;

                            if (sWork.IndexOf(",") != -1)
                            {
                                sTmpArray.Add(sWork.Substring(0, sWork.IndexOf(",")));
                                sWork = sWork.Substring(sWork.IndexOf(",") + 1).Trim();
                            }
                            else
                            {
                                sTmpArray.Add(sWork);
                                sWork = string.Empty;
                                bContinue = false;
                            }

                        } while (bContinue);
                    }

                    if (!bIsSilent)
                    {
                        frmWizard.UpdateExecutingText(sSQL.Trim());
                    }

                    //provide special processing for function statements
                    return ProcessFunctionStatements(sFunction, strDBConnString, nRegister, nNumParms, sTmpArray);
                }
                if (sWork.Substring(0, 2).ToUpper() == "IF")
                {
                    //strip off assign pseudo statement keyword
                    sWork = sWork.Substring(3).Trim();
                    sTemp2 = sWork.Substring(0, 4);

                    if (sTemp2.IndexOf("%%") < 0 | Convert.ToInt32(sTemp2.Substring(2, 1)) > 9 | Convert.ToInt32(sTemp2.Substring(2, 1)) < 0 )
                    {
                        //SYNTAX ERROR - unexpected format
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Parse Error");
                        strErrorMessage.AppendLine("Incorrect Register Assignment Syntax.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                        return false;
                    }

                    if (sWork.Length == 5)
                    {
                        nRegister = Convert.ToInt32(sTemp2.Substring(2, 1));

                        //strip off register assignment portion
                        sWork = sWork.Substring(3).Trim();
                        string sOperator = sWork.Substring(0, 1);
                        string sTemp = sWork.Substring(1, 1);
                        int sVar1 = !string.IsNullOrEmpty(sParmArray[nRegister]) ? Convert.ToInt32(sParmArray[nRegister]) : 0;
                        int sVar2 = !string.IsNullOrEmpty(sTemp) ? Convert.ToInt32(sTemp) : 0;
                        switch (sOperator)
                        {
                            case "=":
                                if (!(sVar1 == sVar2))
                                    IsIfCondition = true;
                                break;
                            case "<":
                                if (!(sVar1 < sVar2))
                                    IsIfCondition = true;
                                break;
                            case ">":
                                if (!(sVar1 > sVar2))
                                    IsIfCondition = true;
                                break;
                        }
                    }
                    if (sWork.Length == 6)
                    {
                        nRegister = Convert.ToInt32(sTemp2.Substring(2, 1));

                        //strip off register assignment portion
                        sWork = sWork.Substring(3).Trim();
                        string sOperator = sWork.Substring(0, 2);
                        string sTemp = sWork.Substring(2, 1);
                        int sVar1 = !string.IsNullOrEmpty(sParmArray[nRegister]) ? Convert.ToInt32(sParmArray[nRegister]) : 0;
                        int sVar2 = !string.IsNullOrEmpty(sTemp) ? Convert.ToInt32(sTemp) : 0;
                        switch (sOperator)
                        {
                            case "==":
                                if (!(sVar1 == sVar2))
                                    IsIfCondition = true;
                                break;
                            case "<=":
                                if (!(sVar1 <= sVar2))
                                    IsIfCondition = true;
                                break;
                            case ">=":
                                if (!(sVar1 >= sVar2))
                                    IsIfCondition = true;
                                break;
                            case "<>":
                            case "!=":
                                if (!(sVar1 != sVar2))
                                    IsIfCondition = true;
                                break;
                        }
                    }
                }
                //end: rsushilaggar

                return true;
            }
            catch (System.Data.Odbc.OdbcException ex)
            {
                ProcessException(sSQL, iLine, sScriptFileName, ex.Message);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                ProcessException(sSQL, iLine, sScriptFileName, ex.Message);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);
            }

            return true;
        }

        /// <summary>
        /// process fucntion statements embedded as part of the SQL Scripts file
        /// </summary>
        /// <param name="sFunctionName"></param>
        /// <param name="strDBConnString"></param>
        /// <param name="nRegister"></param>
        /// <param name="nNumParms"></param>
        /// <param name="sTmpArray"></param>
        /// <returns></returns>
        public static bool ProcessFunctionStatements(string sFunctionName, string strDBConnString, int nRegister, int nNumParms, List<String> sTmpArray)
        {
            #region local variables
            bool bLookup = true;
            bool bDeleted = true;
            bool bRequired = true;
            //bool blnSucceed = true;
            bool bIsPatterned = true;
            var db = 0;
            var iCatID = 0;
            var iNLSCode = 0;
            var iUSLLimit = 0;
            var iActiveFLag = 0;
            var iDeletedFlag = 0;
            var iFormCategory = 0;
            var iLineOfBusCode = 0;
            var iPrimaryFormFlag = 0;
            var iWeightingStepVal = 0;
            var iRatingMethodCOde = 0;
            var lLOB = 0;
            var lTemp = 0;
            var lRelCodeID = 0;
            var lFieldType = 0;
            var lFieldSize = 0;
            var iStateRowID = 0;
            var dMaxClaimAmount = 0.00;
            var dEmployerLiability = 0.00;
            string sSC = String.Empty;
            string sParm = String.Empty;
            string sPattern = String.Empty;
            string sStateID = String.Empty;
            string sHashCRC = String.Empty;
            string sOptMask = String.Empty;
            string sFormName = String.Empty;
            string sFileName = String.Empty;
            string sFormTitle = String.Empty;
            string sCodeTable = String.Empty;
            string sFieldName = String.Empty;
            string sFieldDesc = String.Empty;
            string sStateName = String.Empty;
            string sFieldType = String.Empty;
            string sStateTable = String.Empty;
            string sUserPrompt = String.Empty;
            string sFieldTable = String.Empty;
            string sDisplayCat = String.Empty;
            string sDescription = String.Empty;
            string sSysCodeTable = String.Empty;
            string sSysFieldName = String.Empty;
            
            StringBuilder strErrorMessage = new StringBuilder();
            strErrorMessage.Length = 0;
            strErrorMessage.Capacity = 0;
            #endregion
            // npadhy Inserting new records to PARMS_NAME_VALUE
            string sParmName = string.Empty;
            string sParmValue = string.Empty;
            string sParmDesc = string.Empty;
            string sParmDataType = string.Empty;
            // npadhy Inserting new records to PARMS_NAME_VALUE

            //rsushilaggar MITS 25089
            string sTaskConfig = string.Empty;
            string sTaskName = string.Empty;
            string sTaskDesc = string.Empty;
            int iStatus = 0;
            string sSysModuleName = string.Empty;

            //SPECIFIC FUNCTION PROCESSING - *** BEGIN ***
            switch (sFunctionName)
            {
                case "GET_TABLE":
                    #region GET_TABLE
                    {
                        if (nNumParms != 1)
                        {
                            //ERROR - incorrect number of parameters to GETTABLE function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Give to GET_TABLEID Function (1 is required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>Incorrect Number of Parameters Give to GET_TABLEID Function (1 is required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sParm = sTmpArray[0].Trim();

                        lTemp = GetTableID(sParm);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - System Table Name Not Found
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("System Table Name Specified in GET_TABLEID call is Invalid.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>System Table Name Specified in GET_TABLEID call is Invalid.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case "ATTACH_TABLE":
                    #region ATTACH_TABLE
                    {
                        //ERROR - not supported
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                        strErrorMessage.AppendLine("Function- " + sFunctionName + " macro is no longer supported.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>" + sFunctionName + " macro is no longer supported.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }
                    #endregion
                case "REATTACH_TABLE":
                    #region REATTACH_TABLE
                    {
                        //ERROR - not supported
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                        strErrorMessage.AppendLine("Function- " + sFunctionName + " macro is no longer supported.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>" + sFunctionName + " macro is no longer supported.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }
                    #endregion
                case "NEXT_UID":
                    #region NEXT_UID
                    {
                        if (nNumParms != 1)
                        {
                            //ERROR - incorrect number of parameters to NEXTUID function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to NEXT_UID Function (1 is required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>Incorrect Number of Parameters Given to NEXT_UID Function (1 is required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sParm = sTmpArray[0].Trim();

                        lTemp = DisplayDBUpgrade.GetNextUID(sParm);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - System Table Name Not Found
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("System Table Name Specified in NEXT_UID call is Invalid OR Unable to Obtain Glossary Unique ID.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>System Table Name Specified in NEXT_UID call is Invalid OR Unable to Obtain Glossary Unique ID.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                case "QUERY":
                case "QUERYSEC":
                    #region QUERY/QUERYSEC
                    {
                        if (nNumParms != 1)
                        {
                            //ERROR - Error in script - Incorrect Number of Parameters Given to Query Function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to Query Function (1 is required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>Incorrect Number of Parameters Given to Query Function (1 is required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sParm = sTmpArray[0].Trim();

                        db = (sFunctionName == "QUERY") ? DisplayDBUpgrade.dbLookup : ADONetDbAccess.DbType(strDBConnString);

                        DbReader drQuery = null;

                        using (drQuery = ADONetDbAccess.ExecuteReader(strDBConnString, sParm))
                        {
                            sParmArray[nRegister] = (!drQuery.Read()) ? String.Empty : drQuery[0].ToString();
                        }

                        return true;
                    }
                    #endregion
                case "ADD_CODE":
                    #region ADD_CODE
                    {
                        if (nNumParms != 5 & nNumParms != 6)
                        {
                            //ERROR - incorrect number of parameters to ADD_CODE function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to ADD_CODE Function (5 or 6 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />Incorrect Number of Parameters Given to ADD_CODE Function (5 or 6 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return true;
                        }

                        sSC = sTmpArray[0].Trim();
                        iNLSCode = Convert.ToInt32(sTmpArray[1]);
                        sDescription = sTmpArray[2].Trim();
                        sSysCodeTable = sTmpArray[3].Trim();
                        lRelCodeID = Convert.ToInt32(sTmpArray[4]);

                        lLOB = (sTmpArray.Count == 6) ? Convert.ToInt32(sTmpArray[5]) : 0;

                        lTemp = AddCode(sSC, iNLSCode, sDescription, sSysCodeTable, lRelCodeID, lLOB);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - System Table Name Not Found
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_CODE call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />One or more parameters to the ADD_CODE call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case "ADD_JURISDICTION":
                    #region ADD_JURISDICTION
                    {
                        if (nNumParms != 8)
                        {
                            //ERROR - incorrect number of parameters to ADD_JURISDICTION function (8 are required)
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to ADD_JURISDICTION Function (8 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />Incorrect Number of Parameters Given to ADD_JURISDICTION Function (8 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sStateID = sTmpArray[0].Trim();
                        sStateName = sTmpArray[1].Trim();
                        dEmployerLiability = Convert.ToDouble(sTmpArray[2]);
                        dMaxClaimAmount = Convert.ToDouble(sTmpArray[3]);
                        iRatingMethodCOde = Convert.ToInt32(sTmpArray[4]);
                        iUSLLimit = Convert.ToInt32(sTmpArray[5]);
                        iWeightingStepVal = Convert.ToInt32(sTmpArray[6]);
                        iDeletedFlag = Convert.ToInt32(sTmpArray[7]);

                        lTemp = AddJurisdiction(sStateID, sStateName, dEmployerLiability, dMaxClaimAmount, iRatingMethodCOde, iUSLLimit, iWeightingStepVal, iDeletedFlag);

                        if (lTemp == 0)
                        {
                            //ERROR - error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case "ADD_JURISDICTIONAL_FROI":
                    #region ADD_JURISDICTIONAL_FROI
                    {
                        if (nNumParms != 7)
                        {
                            //ERROR - incorrect number of parameters given
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_JURISDICTIONAL_FROI Function (7 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />Incorrect Number of Parameters Given to the ADD_JURISDICTIONAL_FROI Function (7 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //(1)STATE_ROW_ID, (2)FORM_NAME, (3)PDF_FILE_NAME, (4)ACTIVE_FLAG
                        //(5)HASH_CRC, (6)PRIMARY_FORM_FLAG, (7)LINE_OF_BUS_CODE
                        sStateID = sTmpArray[0].Trim();
                        sFormName = sSQLStringLiteral(sTmpArray[1].Trim()); //prevent SQL errors if single quote appears in form name
                        sFileName = sTmpArray[2].Trim();
                        iActiveFLag = Convert.ToInt32(sTmpArray[3]);

                        if (String.IsNullOrEmpty(sTmpArray[4]))
                        {
                            sTmpArray[4] = "no value";
                        }

                        sHashCRC = sTmpArray[4].Trim();
                        iPrimaryFormFlag = Convert.ToInt32(sTmpArray[5]);
                        iLineOfBusCode = Convert.ToInt32(sTmpArray[6]);

                        lTemp = AddJurisdictionalFROI(Convert.ToInt32(sStateID), sFormName, sFileName, iActiveFLag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode);

                        if (lTemp == 0)
                        {
                            //ERROR - error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case "ADD_STATE_FIELD":
                    #region ADD_STATE_FIELD
                    {
                        if (nNumParms != 11)
                        {
                            //ERROR - incorrect number of parameters in ADD_STATE_FIELD function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to ADD_STATE_FIELD Function (11 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to ADD_STATE_FIELD Function (11 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sStateTable = sTmpArray[0].Trim();
                        sUserPrompt = sTmpArray[1].Trim();
                        sSysFieldName = sTmpArray[2].Trim();
                        lFieldType = Convert.ToInt32(sTmpArray[3]);
                        lFieldSize = Convert.ToInt32(sTmpArray[4]);
                        bRequired = Convert.ToInt32(sTmpArray[5]) != 0;
                        bDeleted = Convert.ToInt32(sTmpArray[6]) != 0;
                        bLookup = Convert.ToInt32(sTmpArray[7]) != 0;
                        bIsPatterned = Convert.ToInt32(sTmpArray[8]) != 0;
                        sPattern = sTmpArray[9].Trim();
                        sCodeTable = sTmpArray[10].Trim();

                        lTemp = AddStateField(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern, sCodeTable);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_STATE_FIELD call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return true;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case "ADD_STATE_FIELD_2":
                    #region ADD_STATE_FIELD_2
                    {
                        //;Supp Fields
                        //;jtodd22 01/30/2008 these are also used by the SIEDRS EDI application
                        //;jtodd22 01/31/2008 these are hard coded in the RMExtenderLib.dll
                        strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                        strErrorMessage.AppendLine("Function- " + sFunctionName + " macro is no longer supported.");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>" + sFunctionName + " macro is no longer supported.", "error", eWizardButtonState.False, String.Empty);
                        //DisplayDBUpgrade.bStop = true;

                        return false;
                    }
                    #endregion
                case ("ADD_MERGE_DICTIONARY"):
                    #region ADD_MERGE_DICTIONARY
                    {
                        if (nNumParms != 8)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_MERGE_DICTIONARY Function (8 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_MERGE_DICTIONARY Function (8 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        iCatID = Convert.ToInt32(sTmpArray[0]);
                        sFieldName = sTmpArray[1].Trim();
                        sFieldDesc = sTmpArray[2].Trim();
                        sFieldTable = sTmpArray[3].Trim();
                        sFieldType = sTmpArray[4].Trim();
                        sOptMask = sTmpArray[5].Trim();
                        sDisplayCat = sTmpArray[6].Trim();
                        sCodeTable = sTmpArray[7].Trim();

                        lTemp = AddMergeDictionaryLine(iCatID, sFieldName, sFieldDesc, sFieldTable, sFieldType, sOptMask, sDisplayCat, sCodeTable);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_MERGE_DICTIONARY call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_MERGE_DICTIONARY call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                //by rkaur7 - MITS 16668
                case ("ADD_SEARCH_DICTIONARY"):
                    #region ADD_SEARCH_DICTIONARY
                    {
                        if (nNumParms != 8)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_SEARCH_DICTIONARY Function (8 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_SEARCH_DICTIONARY Function (8 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        iCatID = Convert.ToInt32(sTmpArray[0]);
                        sFieldName = sTmpArray[1].Trim();
                        sFieldDesc = sTmpArray[2].Trim();
                        sFieldTable = sTmpArray[3].Trim();
                        sFieldType = sTmpArray[4].Trim();
                        sOptMask = sTmpArray[5].Trim();
                        sDisplayCat = sTmpArray[6].Trim();
                        sCodeTable = sTmpArray[7].Trim();

                        lTemp = AddSearchDictionaryLine(iCatID, sFieldName, sFieldDesc, sFieldTable, sFieldType, sOptMask, sDisplayCat, sCodeTable);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_SEARCH_DICTIONARY call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_SEARCH_DICTIONARY call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion

                // npadhy for Inserting values in PARMS_NAME_VALUE
                #region ADD_PARMS_NAME_VALUE
                case ("ADD_PARMS_NAME_VALUE"):
                    
                    {
                        if (nNumParms != 4)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_PARMS_NAME_VALUE Function (4 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_SEARCH_DICTIONARY Function (8 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sParmName = sTmpArray[0];
                        sParmValue = sTmpArray[1].Trim();
                        sParmDesc = sTmpArray[2].Trim();
                        sParmDataType = sTmpArray[3].Trim();

                        lTemp = AddParmsNameValueLine(sParmName, sParmValue, sParmDesc, sParmDataType);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_PARMS_NAME_VALUE call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_SEARCH_DICTIONARY call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                //code end - rkaur7
                case ("ADD_WCP_FORM"):
                    #region ADD_WCP_FORM
                    {
                        if (nNumParms != 9)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_WCP_FORM Function (9 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_WCP_FORM Function (9 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        iStateRowID = Convert.ToInt32(sTmpArray[0]);
                        iFormCategory = Convert.ToInt32(sTmpArray[1]);
                        sFormTitle = sTmpArray[2].Trim();
                        sFormName = sTmpArray[3].Trim();
                        sFileName = sTmpArray[4].Trim();
                        iActiveFLag = Convert.ToInt32(sTmpArray[5]);

                        if (String.IsNullOrEmpty(sTmpArray[6]))
                        {
                            sTmpArray[6] = "no value";
                        }

                        sHashCRC = sTmpArray[6].Trim();
                        iPrimaryFormFlag = Convert.ToInt32(sTmpArray[7]);
                        iLineOfBusCode = Convert.ToInt32(sTmpArray[8]);

                        #region debug
                        //STATE_ROW_ID:  	 52
                        //FORM_CATEGORY: 	 -1
                        //FORM_ID: 	         1820
                        //FORM_TITLE: 	     CARRIER'S REQUEST FOR REDUCTION OF INCOME BENEFITS DUE TO CONTRIBUTION
                        //FORM_NAME: 	     DWC 33
                        //FILE_NAME: 	     tx_dwc-33_20051001.pdf
                        //ACTIVE_FLAG: 	     0
                        //HASH_CRC: 	     no value
                        //PRIMARY_FORM_FLAG: -1
                        //LINE_OF_BUS_CODE:  243

                        //[ASSIGN %%2=ADD_WCP_FORM(%%1,
                        //                         -1,
                        //                         CARRIER'S REQUEST FOR REDUCTION OF INCOME BENEFITS DUE TO CONTRIBUTION,
                        //                         TWCC DWC 33,
                        //                         TX_TWCC-DWC33_20051001.pdf,
                        //                         0,
                        //                         ,
                        //                         -1,
                        //                         243)]
                        #endregion
                        lTemp = AddWCPForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFLag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode);
                        #region debug
                        //COLUMN_NAME       C_DATATYPE C_LENGTH
                        //----------------- ---------- --------
                        //STATE_ROW_ID      NUMBER     22
                        //FORM_CATEGORY     NUMBER     22
                        //FORM_TITLE        VARCHAR2   128
                        //FORM_NAME         VARCHAR2   64
                        //FILE_NAME         VARCHAR2   64
                        //ACTIVE_FLAG       NUMBER     22
                        //HASH_CRC          VARCHAR2   128
                        //PRIMARY_FORM_FLAG NUMBER     22
                        //LINE_OF_BUS_CODE  NUMBER     22
                        //FORM_ID           NUMBER     22
                        #endregion

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_WCP_FORM call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_WCP_FORM call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("ADD_CL_FORM"):
                    #region ADD_CL_FORM
                    {
                        if (nNumParms != 9)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_CL_FORM Function (9 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_CL_FORM Function (9 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        iStateRowID = Convert.ToInt32(sTmpArray[0]);
                        iFormCategory = Convert.ToInt32(sTmpArray[1]);
                        sFormTitle = sTmpArray[2].Trim();
                        sFormName = sTmpArray[3].Trim();
                        sFileName = sTmpArray[4].Trim();
                        iActiveFLag = Convert.ToInt32(sTmpArray[5]);

                        if (String.IsNullOrEmpty(sTmpArray[6].Trim()))
                        {
                            sTmpArray[6] = "no value";
                        }

                        sHashCRC = sTmpArray[6].Trim();
                        iPrimaryFormFlag = Convert.ToInt32(sTmpArray[7]);
                        iLineOfBusCode = Convert.ToInt32(sTmpArray[8]);

                        lTemp = AddClaimForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFLag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_CL_FORM call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_CL_FORM call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("ADD_EV_FORM"):
                    #region ADD_EV_FORM
                    {
                        if (nNumParms != 9)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_EV_FORM Function (9 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_EV_FORM Function (9 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        iStateRowID = Convert.ToInt32(sTmpArray[0]);
                        iFormCategory = Convert.ToInt32(sTmpArray[1]);
                        sFormTitle = sTmpArray[2].Trim();
                        sFormName = sTmpArray[3].Trim();
                        sFileName = sTmpArray[4].Trim();
                        iActiveFLag = Convert.ToInt32(sTmpArray[5]);

                        if (String.IsNullOrEmpty(sTmpArray[6].Trim()))
                        {
                            sTmpArray[6] = "no value";
                        }

                        sHashCRC = sTmpArray[6].Trim();
                        iPrimaryFormFlag = Convert.ToInt32(sTmpArray[7]);
                        iLineOfBusCode = Convert.ToInt32(sTmpArray[8]);

                        lTemp = AddEventForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFLag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_EV_FORM call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_EV_FORM call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("ADD_GLOSSARY"):
                    #region ADD_GLOSSARY
                    {
                        if (nNumParms != 4)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_GLOSSARY Function (4 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_GLOSSARY Function (4 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        lTemp = UpdateGlossary(sTmpArray[0].Trim(), sTmpArray[1].Trim(), Convert.ToInt32(sTmpArray[2]), Convert.ToInt32(sTmpArray[3]), 0);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_GLOSSARY call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_GLOSSARY call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("ADD_GLOSSARY_EX"):
                    #region ADD_GLOSSARY_EX
                    {
                        if (nNumParms != 5)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_GLOSSARY_EX Function (5 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_GLOSSARY_EX Function (5 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        lTemp = UpdateGlossary(sTmpArray[0].Trim(), sTmpArray[1].Trim(), Convert.ToInt32(sTmpArray[2]), Convert.ToInt32(sTmpArray[3]), Convert.ToInt32(sTmpArray[4]));

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_GLOSSARY_EX call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            
                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_GLOSSARY_EX call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("WCPFORMS_DUALCASEOFF"):
                    #region WCPFORMS_DUALCASEOFF
                    {
                        if (nNumParms != 1)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the WCPFORMS_DUALCASEOFF Function (1 is required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the WCPFORMS_DUALCASEOFF Function (1 is required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        lTemp = WCPFormsDualCaseOff(sTmpArray[0].Trim());

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("- Error Msg: Form " + sTmpArray[0].Trim() + " could not be found to validate case, please run complete form installer.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            //DisplayDBUpgrade.bStop = true;
                            //return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("ADD_PERMISSION_ENABLED"):
                    #region ADD_PERMISSION_ENABLED
                    {
                        if (nNumParms != 4)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_PERMISSION_ENABLED Function (4 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_PERMISSION_ENABLED Function (4 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        lTemp = AddPermissions(Convert.ToInt32(sTmpArray[0]), sTmpArray[1].Trim(), Convert.ToInt32(sTmpArray[2]), Convert.ToInt32(sTmpArray[3]), DisplayDBUpgrade.g_dbMake);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_PERMISSION_ENABLED call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_PERMISSION_ENABLED call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                case ("DATABASE_UPGRADE_A"):
                    #region DATABASE_UPGRADE_A
                    //TODO:
                    //;jtodd22 01/27/2008 failure to create a key does not stop the upgrade dead.
                    //;jtodd22 01/27/2008 the create statement is put into the Forms DB Upgrade Exe to kill 
                    //;jtodd22 01/27/2008 the upgrade before more damage is done.
                    //[ASSIGN %%2=DATABASE_UPGRADE_A()]
                    {
                        if (nNumParms != 1)
                        {
                            //ERROR - incorrect number of parameters to DATABASE_UPGRADE_A function
                            strErrorMessage.AppendLine("--- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to DATABASE_UPGRADE_A Function (1 is required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />Incorrect Number of Parameters Given to DATABASE_UPGRADE_A Function (1 is required).", "error", eWizardButtonState.False, String.Empty);
                            DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        sParm = sTmpArray[0].Trim();

                        lTemp = DataBaseUpgradeA(DisplayDBUpgrade.g_dbMake);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - System Table Name Not Found
                            strErrorMessage.AppendLine("--- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the DATABASE_UPGRADE_A call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br />One or more parameters to the DATABASE_UPGRADE_A call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                case ("ADD_TASK_MANAGER_JOB"):
                    #region ADD_TASK_MANAGER
                    {
                        if (nNumParms != 5)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_TASK_MANAGER Function (8 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> Incorrect Number of Parameters Given to the ADD_SEARCH_DICTIONARY Function (8 are required).", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        
                        sTaskName = sTmpArray[0].Trim();
                        sTaskDesc = sTmpArray[1].Trim();
                        sTaskConfig= sTmpArray[2].Trim();
                        iStatus = Convert.ToInt32(sTmpArray[3]);
                        sSysModuleName = sTmpArray[4].Trim();

                        //lTemp = AddSearchDictionaryLine(iCatID, sFieldName, sFieldDesc, sFieldTable, sFieldType, sOptMask, sDisplayCat, sCodeTable);
                        lTemp = AddTaskManagerLine(sTaskName, sTaskDesc, sTaskConfig,iStatus,sSysModuleName);

                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_TASK_MANAGER call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                            //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> One or more parameters to the ADD_TASK_MANAGER call failed OR the call itself failed.", "error", eWizardButtonState.False, String.Empty);
                            //DisplayDBUpgrade.bStop = true;

                            return false;
                        }

                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();

                        return true;
                    }
                    #endregion
                //Deb ML changes
                case ("ADD_PAGE_INFO"):
                    #region ADD_PAGE_INFO
                    {
                        if (nNumParms != 3)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_PAGE_INFO Function (3 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        lTemp = AddPageInfo(Convert.ToInt32(sTmpArray[0]),Convert.ToInt32(sTmpArray[1]), sTmpArray[2].Trim());
                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_PAGE_INFO call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                case ("ADD_LOCAL_RESOURCE"):
                    #region ADD_LOCAL_RESOURCE
                    {
                        if (nNumParms != 9)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_LOCAL_RESOURCE Function (9 are required). and Page id is " + sTmpArray[0]);
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        lTemp = AddLocalResource(Convert.ToInt32(sTmpArray[0]), Convert.ToInt32(sTmpArray[1]), Convert.ToInt32(sTmpArray[2]), sTmpArray[3].Trim(), Convert.ToInt32(sTmpArray[4]), sTmpArray[5].Trim(), sTmpArray[6].Trim(), sTmpArray[7].Trim(), sTmpArray[8].Trim(), DisplayDBUpgrade.g_dbMake);
                        if (lTemp == 0)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_LOCAL_RESOURCE call failed OR the call itself failed.  and Page id is " + sTmpArray[0]);
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                case ("ADD_GLOBAL_RESOURCE"):
                    #region ADD_GLOBAL_RESOURCE
                    {
                        if (nNumParms != 8)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_GLOBAL_RESOURCE Function (8 are required). and Key Name is " + sTmpArray[2]);
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        lTemp = AddGlobalResource(Convert.ToInt32(sTmpArray[0]), Convert.ToInt32(sTmpArray[1]), sTmpArray[2].Trim(), Convert.ToInt32(sTmpArray[3]), sTmpArray[4].Trim(), sTmpArray[5].Trim(), sTmpArray[6].Trim(), sTmpArray[7].Trim(), DisplayDBUpgrade.g_dbMake);
                        if (lTemp < 1)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_GLOBAL_RESOURCE call failed OR the call itself failed.  and Key Name is " + sTmpArray[2]);
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                case ("ADD_MDI_MENU"):
                    #region ADD_MDI_MENU
                    {
                        if (nNumParms != 3)
                        {
                            //ERROR incorrect number of parameters for function
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("Incorrect Number of Parameters Given to the ADD_MDI_MENU Function (3 are required).");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        string sMDIMenuPath = GetScriptPath(GetSystemPath(), sTmpArray[1]);
                        string sChilScreenPath = GetScriptPath(GetSystemPath(), sTmpArray[2]);
                        int iCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, "SELECT COUNT(*) FROM MDI_MENU WHERE LANGUAGE_CODE=" + sTmpArray[0].Trim());
                        if (iCount > 0)
                        {
                            XmlDocument domMDIMenu = new XmlDocument();
                            XmlDocument domChildScreen = new XmlDocument();
                            domMDIMenu.Load(sMDIMenuPath);
                            domChildScreen.Load(sChilScreenPath);
                            
                            string sSQL = "UPDATE MDI_MENU SET MDIMENU_XML=~MDIMENUXML~, CHILDSCREEN_XML=~CHILDSCREENXML~, DTTM_RCD_LAST_UPD=~DBDATETIME~ WHERE LANGUAGE_CODE=" + sTmpArray[0].Trim();
                            NameValueCollection nvColl = new NameValueCollection();
                            nvColl.Add("~MDIMENUXML~", domMDIMenu.OuterXml);
                            nvColl.Add("~CHILDSCREENXML~", domChildScreen.OuterXml);
                            nvColl.Add("~DBDATETIME~", ToDbDateTime(DateTime.Now));
                            lTemp = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, sSQL, nvColl);
                        }
                        else
                        {
                            XmlDocument domMDIMenu = new XmlDocument();
                            XmlDocument domChildScreen = new XmlDocument();
                            domMDIMenu.Load(sMDIMenuPath);
                            domChildScreen.Load(sChilScreenPath);
                            string sSQL = string.Format("INSERT INTO MDI_MENU(LANGUAGE_CODE,MDIMENU_XML,CHILDSCREEN_XML,DTTM_RCD_LAST_UPD) VALUES({0},~MDIMENUXML~,~CHILDSCREENXML~,~DBDATETIME~)",sTmpArray[0].Trim());
                            NameValueCollection nvColl = new NameValueCollection();
                            nvColl.Add("~MDIMENUXML~", domMDIMenu.OuterXml);
                            nvColl.Add("~CHILDSCREENXML~", domChildScreen.OuterXml);
                            nvColl.Add("~DBDATETIME~", ToDbDateTime(DateTime.Now));
                            lTemp = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, sSQL, nvColl);
                        }
                        if (lTemp < 1)
                        {
                            //ERROR - Error in script - parameters missing or call failed
                            strErrorMessage.AppendLine("-- Error Msg: Pseudo Statement Syntax Error");
                            strErrorMessage.AppendLine("One or more parameters to the ADD_MDI_MENU call failed OR the call itself failed.");
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());
                            return false;
                        }
                        //if successful, place value in designated register
                        sParmArray[nRegister] = lTemp.ToString().Trim();
                        return true;
                    }
                    #endregion
                //Deb ML changes
                default:
                    #region unsupported functions
                    {
                        //ERROR - UNSUPPORTED FUNCTION
                        strErrorMessage.AppendLine("--- Error Msg: Pseudo Statement Syntax Error");
                        strErrorMessage.AppendLine("This ASSIGN subfunction is unsupported (" + sFunctionName + ").");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strErrorMessage.ToString());

                        frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br /><br /> This ASSIGN subfunction is unsupported (" + sFunctionName + ").", "error", eWizardButtonState.False, String.Empty);
                        DisplayDBUpgrade.bStop = true;
                        return false;
                    }
                    #endregion
            }
        }
        #region FUNCTION STATEMENT FUNCTIONS
        //Deb ML changes
        /// <summary>
        /// Add Page Info
        /// </summary>
        /// <param name="iPageID"></param>
        /// <param name="iDSNId"></param>
        /// <param name="sPageName"></param>
        /// <returns></returns>
        private static int AddPageInfo(int iPageID, int iDSNId, string sPageName)
        {
            string sSQL = "SELECT PAGE_ID FROM PAGE_INFO WHERE PAGE_ID =" + iPageID + " AND DATA_SOURCE_ID=" + iDSNId;
            int PageID = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, sSQL);
            if (PageID != 0)
            {
                DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                dbWriter.Tables.Add("PAGE_INFO");
                dbWriter.Where.Add((String.Format("PAGE_ID={0}", iPageID)));
                dbWriter.Fields.Add("PAGE_ID", iPageID);
                dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                dbWriter.Fields.Add("PAGE_NAME", sPageName);
                dbWriter.Execute();
                return PageID;
            }
            else
            {
                DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                dbWriter.Tables.Add("PAGE_INFO");
                dbWriter.Fields.Add("PAGE_ID", iPageID);
                dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                dbWriter.Fields.Add("PAGE_NAME", sPageName);
                dbWriter.Execute();
            }
            return iPageID;
        }

        /// <summary>
        /// Add Local resource
        /// </summary>
        /// <param name="iPageID"></param>
        /// <param name="iDSNId"></param>
        /// <param name="iLangId"></param>
        /// <param name="sKeyName"></param>
        /// <param name="iType"></param>
        /// <param name="sResValue"></param>
        /// <param name="sAddDate"></param>
        /// <param name="sUpdateDate"></param>
        /// <param name="sUser"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static int AddLocalResource(int iPageID, int iDSNId, int iLangId, string sKeyName, int iType, string sResValue, string sAddDate, string sUpdateDate, string sUser, int db)
        {
            sResValue = sResValue.Replace("<replacecomma>", ",");// for replacing ; with ,
            if (string.IsNullOrEmpty(sAddDate))
                sAddDate = ToDbDateTime(DateTime.Now);
            if (string.IsNullOrEmpty(sUpdateDate))
                sUpdateDate = ToDbDateTime(DateTime.Now);
            string sSQL = "SELECT LOCAL_RESOURCE_ID FROM LOCAL_RESOURCE WHERE PAGE_ID =" + iPageID + " AND DATA_SOURCE_ID=" + iDSNId + " AND LANGUAGE_ID=" + iLangId + " AND RESOURCE_KEY='" + sKeyName + "'";
            int iResId = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, sSQL);
            if (iResId != 0)
            {
                DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                dbWriter.Tables.Add("LOCAL_RESOURCE");
                dbWriter.Where.Add((String.Format("LOCAL_RESOURCE_ID={0}", iResId)));
                dbWriter.Fields.Add("PAGE_ID", iPageID);
                dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                iResId = dbWriter.Execute();
                return iResId;
            }
            else
            {
                if (db == RiskmasterDBTypes.DBMS_IS_SQLSRVR)
                {
                    DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                    dbWriter.Tables.Add("LOCAL_RESOURCE");
                    dbWriter.Fields.Add("PAGE_ID", iPageID);
                    dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                    dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                    dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                    dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                    dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                    dbWriter.Fields.Add("DTTM_RCD_ADDED", sAddDate);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                    dbWriter.Fields.Add("ADDED_BY_USER", sUser);
                    iResId = dbWriter.Execute();
                }
                else if (db == RiskmasterDBTypes.DBMS_IS_ORACLE)
                {
                    int iNextId = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, "SELECT SEQ_LOCAL_RESOURCE_ID.NEXTVAL FROM DUAL");
                    DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                    dbWriter.Tables.Add("LOCAL_RESOURCE");
                    dbWriter.Fields.Add("LOCAL_RESOURCE_ID", iNextId);
                    dbWriter.Fields.Add("PAGE_ID", iPageID);
                    dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                    dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                    dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                    dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                    dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                    dbWriter.Fields.Add("DTTM_RCD_ADDED", sAddDate);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                    dbWriter.Fields.Add("ADDED_BY_USER", sUser);
                    iResId = dbWriter.Execute();
                }
            }
            return iResId;
        }

        /// <summary>
        /// Add Global Resource
        /// </summary>
        /// <param name="iDSNId"></param>
        /// <param name="iLangId"></param>
        /// <param name="sKeyName"></param>
        /// <param name="iType"></param>
        /// <param name="sResValue"></param>
        /// <param name="sAddDate"></param>
        /// <param name="sUpdateDate"></param>
        /// <param name="sUser"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static int AddGlobalResource(int iDSNId, int iLangId, string sKeyName, int iType, string sResValue, string sAddDate, string sUpdateDate, string sUser, int db)
        {
            sResValue = sResValue.Replace("<replacecomma>", ",");// for replacing ; with ,
            if (string.IsNullOrEmpty(sAddDate))
                sAddDate = ToDbDateTime(DateTime.Now);
            if (string.IsNullOrEmpty(sUpdateDate))
                sUpdateDate = ToDbDateTime(DateTime.Now);
            string sSQL = "SELECT GLOBAL_RESOURCE_ID FROM GLOBAL_RESOURCE WHERE DATA_SOURCE_ID=" + iDSNId + " AND LANGUAGE_ID=" + iLangId + " AND RESOURCE_KEY='" + sKeyName + "'";
            int iResId = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, sSQL);
            if (iResId != 0)
            {
                DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                dbWriter.Tables.Add("GLOBAL_RESOURCE");
                dbWriter.Where.Add((String.Format("GLOBAL_RESOURCE_ID={0}", iResId)));
                dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                dbWriter.Fields.Add("ADDED_BY_USER", sUser);
                iResId = dbWriter.Execute();
                return iResId;
            }
            else
            {
                if (db == RiskmasterDBTypes.DBMS_IS_SQLSRVR)
                {
                    DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                    dbWriter.Tables.Add("GLOBAL_RESOURCE");
                    dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                    dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                    dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                    dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                    dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                    dbWriter.Fields.Add("DTTM_RCD_ADDED", sAddDate);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                    dbWriter.Fields.Add("ADDED_BY_USER", sUser);
                    iResId = dbWriter.Execute();
                }
                else if (db == RiskmasterDBTypes.DBMS_IS_ORACLE)
                {
                    int iNextId = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, "SELECT SEQ_GLOBAL_RESOURCE_ID.NEXTVAL FROM DUAL");
                    DbWriter dbWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_ViewConnectString);
                    dbWriter.Tables.Add("GLOBAL_RESOURCE");
                    dbWriter.Fields.Add("GLOBAL_RESOURCE_ID", iNextId);
                    dbWriter.Fields.Add("DATA_SOURCE_ID", iDSNId);
                    dbWriter.Fields.Add("LANGUAGE_ID", iLangId);
                    dbWriter.Fields.Add("RESOURCE_KEY", sKeyName);
                    dbWriter.Fields.Add("RESOURCE_TYPE", iType);
                    dbWriter.Fields.Add("RESOURCE_VALUE", sResValue);
                    dbWriter.Fields.Add("DTTM_RCD_ADDED", sAddDate);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", sUpdateDate);
                    dbWriter.Fields.Add("ADDED_BY_USER", sUser);
                    iResId = dbWriter.Execute();
                }
            }
            return iResId;
        }
        /// <summary>Gets a common date and time format using the 24-hour clock for RISKMASTER</summary>
        /// <param name="date">DateTime to be converted into Riskmaster Database date format "YYYYMMDDHHMMSS."</param>
        /// <returns>String value for DateTime date in "YYYYMMDDHHMMSS" format.</returns>
        /// <remarks>Returns "00000000000000" if DateTime is equal to DateTime.MinValue.
        /// Note: Modified 08.04.2005 to return 14 characters rather than 12.  This means
        /// seconds are included properly now.</remarks>
        public static string ToDbDateTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "00000000000000";
            return date.ToString("yyyyMMddHHmmss");  
        }
        //Deb ML changes

        /// <summary>
        /// add code to specified code table
        /// </summary>
        /// <param name="sShortCode"></param>
        /// <param name="iNLSCode"></param>
        /// <param name="sDescription"></param>
        /// <param name="sSysCodeTable"></param>
        /// <param name="lRelatedCodeID"></param>
        /// <param name="lLOB"></param>
        /// <returns></returns>
        public static int AddCode(string sShortCode, int iNLSCode, string sDescription, string sSysCodeTable, int lRelatedCodeID, int lLOB)
        {
            bool bJustAddDesc = false;
            int lCodeID = 0;
            int lTableID = 0;
            int lAddCode = 0;
            string sSQL = String.Empty;

            //get table ID of code table to add to
            lTableID = GetTableID(sSysCodeTable);

            if (lTableID <= 0)
            {
                //ERROR - incorrect number of parameters to ADD_CODE function
                //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/>" + sSysCodeTable + "  is not a Valid Code Table Name.", "error", eWizardButtonState.False);

                string strMessage = "-Error: " + sSysCodeTable + " is not a Valid Code Table Name";
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);

                return 0;
            }

            if (sSysCodeTable.ToUpper() == "STATES")
            {
                //see if this code already exists
                sSQL = "SELECT COUNT(*) FROM STATES WHERE STATE_ID = '" + sSQLStringLiteral(sShortCode.Trim()) + "'";
                lCodeID = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (lCodeID > 0)
                {
                    return lCodeID;
                }

                lCodeID = DisplayDBUpgrade.GetNextUID("STATES");

                if (lCodeID <= 0)
                {
                    string strMessage = "-Error: Unable to assign next ID for STATES table";
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);
                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/> Unable to assign next ID for STATES table.", "error", eWizardButtonState.False);

                    return 0;
                }

                //insert the values into the codes text table
                sSQL  = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, DELETED_FLAG)";
                //Beg Rgupta221 DB Upgrade Add_Code function not working for States Code Table JIRA RMA-11443
             //   sSQL += " VALUES (" + lCodeID + ","  + sSQLStringLiteral(sShortCode.Trim()) + "','";
                sSQL += " VALUES (" + lCodeID + "," +"'" + sSQLStringLiteral(sShortCode.Trim()) + "','";
                //End Rgupta221 DB Upgrade Add_Code function not working for States Code Table JIRA RMA-11443
                sSQL += sSQLStringLiteral(sDescription.Trim()) + "', 0)";

                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

                //stamp codes for recache
                UpdateGlossaryTimeStamp("STATES");

                return lCodeID;
            }
            //start: Added by Nitin goel, For National Interstate enhancement,08/19/2013, MITS 30910
            if (sSysCodeTable.ToUpper() == "COVERAGE_GROUP")
            {
                //see if this code already exists
                sSQL = "SELECT COVERAGE_GROUP_ID FROM COVERAGE_GROUP WHERE COV_GROUP = '" + sSQLStringLiteral(sShortCode.Trim()) + "'";
                lCodeID = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                string sTmpDate = string.Empty;
                sTmpDate = GetDateTimeFormat();
                string sUser = "SYSTEM";
                if (lCodeID > 0)
                {
                   

                    InsertAggregateLimitforGroup(lCodeID,sTmpDate,sUser);
                    InsertCodeDescription(lCodeID, iNLSCode, sSQLStringLiteral(sShortCode.Trim()), sSQLStringLiteral(sDescription.Trim()));
                    return lCodeID;
                }

                lCodeID = DisplayDBUpgrade.GetNextUID("COVERAGE_GROUP");

                if (lCodeID <= 0)
                {
                    string strMessage = "-Error: Unable to assign next ID for COVERAGE_GROUP table";
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);
                    return 0;
                }

                //insert the values into the codes text table
             
                sSQL = string.Format("INSERT INTO COVERAGE_GROUP (COVERAGE_GROUP_ID, COV_GROUP, DELETED_FLAG,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,ADDED_BY_USER,UPDATED_BY_USER) VALUES ({0},'{1}',0,'{2}','{2}','{3}','{3}')", lCodeID, sSQLStringLiteral(sShortCode.Trim()), sTmpDate, sUser);

                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

                //stamp codes for recache
                UpdateGlossaryTimeStamp("COVERAGE_GROUP");

                InsertAggregateLimitforGroup(lCodeID,sTmpDate, sUser);
                InsertCodeDescription(lCodeID, iNLSCode, sSQLStringLiteral(sShortCode.Trim()), sSQLStringLiteral(sDescription.Trim()));

                return lCodeID;
            }
            //end: added by Nitin goel,for National Interstate Enhancement MITS 30910,08/19/2013
            //see if this code already exists
            sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + lTableID + " AND SHORT_CODE = '" +
                   sSQLStringLiteral(sShortCode.Trim()) + "'";
            lCodeID = Convert.ToInt32(ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL));

            if (lCodeID > 0)
            {
                sSQL = "SELECT CODE_ID FROM CODES_TEXT WHERE CODE_ID = " + lCodeID;
                lAddCode = Convert.ToInt32(ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (lAddCode == lCodeID)
                {
                    return lCodeID;
                }
                if (lAddCode == 0)
                {
                    bJustAddDesc = true;
                }
            }
            else if (lCodeID < 0)
            {
                string strMessage = "-Error: Invalid Code ID for CODES table: " + sShortCode + " " + lTableID;
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);

                return 0;
            }

            if (!bJustAddDesc)
            {
                //get a unique id for the code
                lCodeID = DisplayDBUpgrade.GetNextUID("CODES");

                if (lCodeID <= 0)
                {
                    string strMessage = "-Error: Unable to assign next ID for CODES table";
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);

                    return 0;
                }

                //insert the values into the codes table
                sSQL  = "INSERT INTO CODES (CODE_ID,TABLE_ID,SHORT_CODE,RELATED_CODE_ID,DELETED_FLAG,LINE_OF_BUS_CODE)";
                sSQL += " VALUES (" + lCodeID + "," + lTableID + ",'" + sSQLStringLiteral(sShortCode.Trim());
                sSQL += "'," + lRelatedCodeID + ",0," + lLOB + ")";

                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
            }

            //insert the values into the codes_text table
            sSQL  = "INSERT INTO CODES_TEXT (CODE_ID,LANGUAGE_CODE,SHORT_CODE,CODE_DESC)";
            sSQL += " VALUES (" + lCodeID + "," + iNLSCode + ",'" + sSQLStringLiteral(sShortCode.Trim());
            sSQL += "','" + sSQLStringLiteral(sDescription.Trim()) + "')";

            ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

            UpdateGlossaryTimeStamp("CODES");

            //return code id
            return lCodeID;
        }

        /// <summary>
        /// retrieve table ID for given table name
        /// </summary>
        /// <param name="sSysTableName"></param>
        /// <returns></returns>
        public static int GetTableID(string sSysTableName)
        {
            return ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.GetTableID(sSysTableName));
        }

        /// <summary>
        /// add jurisdiction to states table
        /// </summary>
        /// <param name="sStateID"></param>
        /// <param name="sStateName"></param>
        /// <param name="dEmployerLiability"></param>
        /// <param name="dMaxClaimAmount"></param>
        /// <param name="iRatingMethodCode"></param>
        /// <param name="iUSLLimit"></param>
        /// <param name="iWeightingStepVal"></param>
        /// <param name="iDeletedFlag"></param>
        /// <returns></returns>
        public static int AddJurisdiction(string sStateID, string sStateName, double dEmployerLiability, double dMaxClaimAmount, int iRatingMethodCode, int iUSLLimit, int iWeightingStepVal, int iDeletedFlag)
        {
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;

            try
            {
                var dictFields = new Dictionary<string, object>();

                sStateID = sStateID.ToUpper();

                //determine if a row already exists if so, send back the state_id and exit 
                var sSQL = "SELECT COUNT(*) FROM STATES WHERE STATE_ID = '" + sSQLStringLiteral(sStateID.Trim()) + "'";
                var iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (iRowCnt == 1)
                {
                    sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" + sSQLStringLiteral(sStateID.Trim()) + "'";

                    return Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }

                if (iRowCnt == 0) //no row exists create a new one
                {
                    var lID = DisplayDBUpgrade.GetNextUID("STATES");

                    //fill dictionary with values for eventual insert into STATES table
                    dictFields.Add("STATE_ROW_ID", lID);
                    dictFields.Add("STATE_ID", sStateID);
                    dictFields.Add("STATE_NAME", sStateName);
                    dictFields.Add("EMPLOYER_LIABILITY", dEmployerLiability);
                    dictFields.Add("MAX_CLAIM_AMOUNT", dMaxClaimAmount);
                    dictFields.Add("RATING_METHOD_CODE", iRatingMethodCode);
                    dictFields.Add("USL_LIMIT", iUSLLimit);
                    dictFields.Add("WEIGHTING_STEP_VAL", iWeightingStepVal);
                    dictFields.Add("DELETED_FLAG", iDeletedFlag);

                    strSQLquery = RISKMASTERScripts.InsertNewRow("STATES", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                    if (String.IsNullOrEmpty(strSQLquery))
                    {
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddJurisdiction] " + strErrDesc + " The statement has been terminated.");
                    }
                    else
                    {
                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                    }

                    return lID;
                }
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);
                return 0;
            }

            return 0;
        }

        /// <summary>
        /// add form to JURIS_FORMS table
        /// </summary>
        /// <param name="iStateRowID"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFileName"></param>
        /// <param name="iActiveFlag"></param>
        /// <param name="sHashCRC"></param>
        /// <param name="iPrimaryFormFlag"></param>
        /// <param name="iLineOfBusCode"></param>
        /// <returns></returns>
        public static int AddJurisdictionalFROI(int iStateRowID, string sFormName, string sFileName, int iActiveFlag, string sHashCRC, int iPrimaryFormFlag, int iLineOfBusCode)
        {
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;

            try
            {
                var dictFields = new Dictionary<string, object>();

                //make sure case is not an issue with preserved filename extensions, we want lowercase
                if (sFileName.IndexOf("PDF") > 0)
                {
                    sFileName = sFileName.Replace("PDF", "pdf");
                }

                //determine if a row already exists if so, send back the form_id and exit 
                var sSQL = "SELECT COUNT(*) FROM JURIS_FORMS WHERE STATE_ROW_ID = " + iStateRowID;
                var iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (iRowCnt == 1)
                {
                    sSQL = "SELECT FORM_ID FROM JURIS_FORMS WHERE STATE_ROW_ID = " + iStateRowID;

                    return Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }

                if (iRowCnt == 0) //no row exists create a new one
                {
                    var lID = DisplayDBUpgrade.GetNextUID("JURIS_FORMS");

                    //fill dictionary with values for eventual insert into JURIS_FORMS table
                    dictFields.Add("STATE_ROW_ID",iStateRowID);
                    dictFields.Add("FORM_ID",lID);
                    dictFields.Add("FORM_NAME",sFormName);
                    dictFields.Add("PDF_FILE_NAME",sFileName);
                    dictFields.Add("ACTIVE_FLAG",iActiveFlag);
                    dictFields.Add("HASH_CRC",sHashCRC);
                    dictFields.Add("PRIMARY_FORM_FLAG",iPrimaryFormFlag);
                    dictFields.Add("LINE_OF_BUS_CODE",iLineOfBusCode);

                    strSQLquery = RISKMASTERScripts.InsertNewRow("JURIS_FORMS", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                    if (String.IsNullOrEmpty(strSQLquery))
                    {
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddJurisdictionalFROI] " + strErrDesc + " The statement has been terminated.");
                    }
                    else
                    {
                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                    }

                    return lID;
                }
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);
                return 0;
            }

            return 0;
        }

        /// <summary>
        /// add state field
        /// </summary>
        /// <param name="sStateTable"></param>
        /// <param name="sUserPrompt"></param>
        /// <param name="sSysFieldName"></param>
        /// <param name="lFieldType"></param>
        /// <param name="lFieldSize"></param>
        /// <param name="bRequired"></param>
        /// <param name="bDeleted"></param>
        /// <param name="bLookup"></param>
        /// <param name="bIsPatterned"></param>
        /// <param name="sPattern"></param>
        /// <param name="sCodeTable"></param>
        /// <returns></returns>
        public static int AddStateField(string sStateTable, string sUserPrompt, string sSysFieldName, int lFieldType, int lFieldSize, bool bRequired, bool bDeleted, bool bLookup, bool bIsPatterned, string sPattern, string sCodeTable)
        {
            int lID = 0;
            int iRowCnt = 0;
            string sSQL = String.Empty;
            string strErrDesc = String.Empty;
            string strSQLquery = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();

            sSysFieldName = sSysFieldName.ToUpper();

            switch (lFieldType)
            {
                #region field type
                case 0: //normal string
                    {
                        if (lFieldSize == 0)
                        {
                            lFieldSize = 50;
                        }
                        break;
                    }
                case 3: //date string(8)
                    {
                        lFieldSize = 8;
                        break;
                    }
                case 4: //time string(6)
                    {
                        lFieldSize = 6;
                        break;

                    }
                case 10:
                case 12: //claim number lookup, event number lookup string(25)
                    {
                        lFieldSize = 25;
                        break;
                    }
                case 13: //vehicle lookup - VIN string(20)
                    {
                        lFieldSize = 20;
                        break;
                    }
                default: //number, id, etc.
                    {
                        if (lFieldSize != 0)
                        {
                            lFieldSize = 0;
                        } // if
                        break;
                    }
                #endregion
            }

            try
            {
                //determine if a row already exists if so, send back the form_id and exit function
                sSQL = String.Format("SELECT COUNT(*) FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '{0}' AND SYS_FIELD_NAME = '{1}'", sStateTable, sSysFieldName);

                int.TryParse(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL), out iRowCnt);

                if (iRowCnt == 1)
                {
                    sSQL = String.Format("SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '{0}' AND SYS_FIELD_NAME = '{1}'", sStateTable, sSysFieldName);

                    return ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
                }
                else if (iRowCnt == 0)
                {
                    lID = DisplayDBUpgrade.GetNextUID("SUPP_DICTIONARY");

                    dictFields.Add("FIELD_ID", lID);
                    dictFields.Add("SEQ_NUM", lID);
                    dictFields.Add("SUPP_TABLE_NAME", sStateTable);
                    dictFields.Add("SYS_FIELD_NAME", sSysFieldName);
                    dictFields.Add("FIELD_TYPE", lFieldType);
                    dictFields.Add("FIELD_SIZE", lFieldSize);
                    dictFields.Add("REQUIRED_FLAG", bRequired);
                    dictFields.Add("DELETE_FLAG", bDeleted);
                    dictFields.Add("LOOKUP_FLAG", bLookup);
                    dictFields.Add("IS_PATTERNED", bIsPatterned);
                    dictFields.Add("PATTERN", sPattern);
                    dictFields.Add("CODE_FILE_ID", GetTableID(sCodeTable));
                    dictFields.Add("GRP_ASSOC_FLAG", 0);
                    dictFields.Add("HELP_CONTEXT_ID", DisplayDBUpgrade.GetNextUID("HELP_DEF"));
                    dictFields.Add("USER_PROMPT", sUserPrompt);

                    strSQLquery = RISKMASTERScripts.InsertNewRow("SUPP_DICTIONARY", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                    if (String.IsNullOrEmpty(strSQLquery))
                    {
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddStateField] " + strErrDesc + " The statement has been terminated.");
                    }
                    else
                    {
                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }

            if (lFieldType != 14 && lFieldType != 15 && lFieldType != 16)
            {
                try
                {
                    sSQL = String.Format("SELECT {0} FROM {1} WHERE 0=1", sSysFieldName, sStateTable);
                    string rowCnt = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL);
                }
                catch(Exception)
                {
                    sSQL = String.Format("ALTER TABLE {0} ADD {1} {2}", sStateTable, sSysFieldName, sTypeMap(lFieldType, lFieldSize));
                    ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL);
                }
            }

            return lID;
        }

        /// <summary>
        /// add merge dictionary row
        /// </summary>
        /// <param name="iCatID"></param>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldDesc"></param>
        /// <param name="sFieldTable"></param>
        /// <param name="sFieldType"></param>
        /// <param name="sOptMask"></param>
        /// <param name="sDisplayCat"></param>
        /// <param name="sCodeTable"></param>
        /// <returns></returns>
        public static int AddMergeDictionaryLine(int iCatID, string sFieldName, string sFieldDesc, string sFieldTable, string sFieldType, string sOptMask, string sDisplayCat, string sCodeTable)
        {
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;

            try
            {
                var dictFields = new Dictionary<string, object>();
                var iRowCnt = 0;

                //test for whether a row already exists for specified criteria 
                var sSQL = "SELECT FIELD_ID FROM MERGE_DICTIONARY WHERE FIELD_NAME ";
                //gagnihotri 10/30/2009: Corrected the sql statement formation
                sSQL += (String.IsNullOrEmpty(sFieldName)) ? "IS NULL" : "= '" + sFieldName + "'";
                
                sSQL += " AND FIELD_TABLE = '" +
                        sFieldTable + "' AND CAT_ID  = " + iCatID + " AND FIELD_TYPE = " + sFieldType;

                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }
                catch (Exception)
                {
                    iRowCnt = 0;
                }

                if(iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                var lID = GetMaxFieldID("MERGE_DICTIONARY");

                if(lID == 0)
                {
                    //ERROR - could not determine next unique id for MERGE DICTIONARY
                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/> Could not determine next unique id for MERGE DICTIONARY.", "error", eWizardButtonState.False);
                    return 0;
                }

                dictFields.Add("FIELD_NAME", sFieldName);   // DB_PutData rs, "FIELD_NAME", sFieldName
                dictFields.Add("CAT_ID", iCatID);           // DB_PutData rs, "CAT_ID", iCatID
                dictFields.Add("FIELD_ID", lID);            // DB_PutData rs, "FIELD_ID", lID
                dictFields.Add("FIELD_TABLE", sFieldTable); // DB_PutData rs, "FIELD_TABLE", sFieldTable
                dictFields.Add("FIELD_DESC", sFieldDesc);   // DB_PutData rs, "FIELD_DESC", sFieldDesc
                dictFields.Add("FIELD_TYPE", sFieldType);   // DB_PutData rs, "FIELD_TYPE", sFieldType
                dictFields.Add("OPT_MASK", sOptMask);       // DB_PutData rs, "OPT_MASK", sOptMask
                dictFields.Add("DISPLAY_CAT", sDisplayCat); // DB_PutData rs, "DISPLAY_CAT", sDisplayCat
                dictFields.Add("CODE_TABLE", sCodeTable);   // DB_PutData rs, "CODE_TABLE", sCodeTable

                strSQLquery = RISKMASTERScripts.InsertNewRow("MERGE_DICTIONARY", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddMergeDictionaryLine] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        //added by rkaur7 - MITS 16668
        /// <summary>
        /// add search dictionary row
        /// </summary>
        /// <param name="iCatID"></param>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldDesc"></param>
        /// <param name="sFieldTable"></param>
        /// <param name="sFieldType"></param>
        /// <param name="sOptMask"></param>
        /// <param name="sDisplayCat"></param>
        /// <param name="sCodeTable"></param>
        /// <returns></returns>
        public static int AddSearchDictionaryLine(int iCatID, string sFieldName, string sFieldDesc, string sFieldTable, string sFieldType, string sOptMask, string sDisplayCat, string sCodeTable)
        {
            int iRowCnt = 0;
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();

            //test for whether a row already exists for specified criteria 
            string sSQL = "SELECT FIELD_ID FROM SEARCH_DICTIONARY WHERE FIELD_NAME ";
            sSQL += (String.IsNullOrEmpty(sFieldName)) ? "IS NULL" : String.Format("= '{0}'", sFieldName);
            sSQL += String.Format(" AND FIELD_TABLE = '{0}' AND CAT_ID = {1} AND FIELD_TYPE = {2}", sFieldTable, iCatID, sFieldType);

            try
            {
                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }
                catch (Exception)
                {
                    iRowCnt = 0;
                }

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = GetMaxFieldID("SEARCH_DICTIONARY");

                if (lID == 0)
                {
                    //ERROR - could not determine next unique id for SEARCH DICTIONARY
                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/> Could not determine next unique id for SEARCH DICTIONARY.", "error", eWizardButtonState.False);
                    return 0;
                }

                dictFields.Add("FIELD_NAME", sFieldName);   // DB_PutData rs, "FIELD_NAME", sFieldName
                dictFields.Add("CAT_ID", iCatID);           // DB_PutData rs, "CAT_ID", iCatID
                dictFields.Add("FIELD_ID", lID);            // DB_PutData rs, "FIELD_ID", lID
                dictFields.Add("FIELD_DESC", sFieldDesc);   // DB_PutData rs, "FIELD_DESC", sFieldDesc
                dictFields.Add("FIELD_TABLE", sFieldTable); // DB_PutData rs, "FIELD_TABLE", sFieldTable
                dictFields.Add("FIELD_TYPE", sFieldType);   // DB_PutData rs, "FIELD_TYPE", sFieldType
                dictFields.Add("OPT_MASK", sOptMask);       // DB_PutData rs, "OPT_MASK", sOptMask
                dictFields.Add("DISPLAY_CAT", sDisplayCat); // DB_PutData rs, "DISPLAY_CAT", sDisplayCat
                dictFields.Add("CODE_TABLE", sCodeTable);   // DB_PutData rs, "CODE_TABLE", sCodeTable

                strSQLquery = RISKMASTERScripts.InsertNewRow("SEARCH_DICTIONARY", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddSearchDictionaryLine] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        //added by rsushilaggar MITS 25089
        /// <summary>
        /// add task manager row
        /// </summary>
        /// <param name="iCatID"></param>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldDesc"></param>
        /// <param name="sFieldTable"></param>
        /// <param name="sFieldType"></param>
        /// <param name="sOptMask"></param>
        /// <param name="sDisplayCat"></param>
        /// <param name="sCodeTable"></param>
        /// <returns></returns>
        public static int AddTaskManagerLine(string sTaskName, string sTaskDesc, string sTaskConfig,int iStatus,string sSysModuleName)
        {
            int iRowCnt = 0;
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();
            //INSERT INTO TM_TASK_TYPE (TASK_TYPE_ID, NAME, DESCRIPTION, CONFIG) VALUES (1, 'Process WPA Diaries', 'Processes WPA Diaries', '<Task Name="WPA" cmdline="yes"><Path>WPA Processing.exe</Path></Task>') 
            //test for whether a row already exists for specified criteria 
            string sSQL = "SELECT TASK_TYPE_ID FROM TM_TASK_TYPE WHERE NAME ";
            sSQL += (String.IsNullOrEmpty(sTaskName)) ? "IS NULL" : String.Format("= '{0}'", sTaskName);
            //sSQL += String.Format(" AND FIELD_TABLE = '{0}' AND CAT_ID = {1} AND FIELD_TYPE = {2}", sFieldTable, iCatID, sFieldType);

            try
            {
                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_TaskManagerConnectString, sSQL));
                }
                catch (Exception)
                {
                    iRowCnt = 0;
                }

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = GetMaxRecordID("TM_TASK_TYPE", "TASK_TYPE_ID",DisplayDBUpgrade.g_TaskManagerConnectString);

                if (lID == 0)
                {
                    //ERROR - could not determine next unique id for SEARCH DICTIONARY
                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/> Could not determine next unique id for SEARCH DICTIONARY.", "error", eWizardButtonState.False);
                    return 0;
                }

                dictFields.Add("NAME", sTaskName);   // DB_PutData rs, "NAME", sTaskName
                dictFields.Add("DESCRIPTION", sTaskDesc);           // DB_PutData rs, "DESCRIPTION", sTaskDesc
                dictFields.Add("CONFIG", sTaskConfig);            // DB_PutData rs, "CONFIG", sTaskConfig
                dictFields.Add("TASK_TYPE_ID", lID);   // DB_PutData rs, "TASK_TYPE_ID", lID
                dictFields.Add("DISABLE_DETAILED_STATUS", iStatus);   // DB_PutData rs, "DISABLE_DETAILED_STATUS", iStatus
                dictFields.Add("SYSTEM_MODULE_NAME", sSysModuleName);   // DB_PutData rs, "SYSTEM_MODULE_NAME", sSysModuleName

                strSQLquery = RISKMASTERScripts.InsertNewRow("TM_TASK_TYPE", dictFields, ref strErrDesc, DisplayDBUpgrade.g_TaskManagerConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddTaskManagerLine] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_TaskManagerConnectString, strSQLquery);
                }

                return lID;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        public static int AddParmsNameValueLine(string sParmName, string sParmValue, string sParmDesc, string sParmDataType)
        {
            int iRowCnt = 0;
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();
            int iDefaultCategory = 50;
            //test for whether a row already exists for specified criteria 
            string sSQL = String.Format("SELECT ROW_ID FROM PARMS_NAME_VALUE WHERE PARM_NAME = '{0}'",sParmName);
            try
            {
                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }
                catch (Exception exc)
                {
                    iRowCnt = 0;
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + exc.Message);
                }

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = GetMaxRecordID("PARMS_NAME_VALUE","ROW_ID");

                if (lID == 0)
                {
                    //ERROR - could not determine next unique id for SEARCH DICTIONARY
                    //frmWizard.ErrorDisplay("<b>PSEUDO STATEMENT SYNTAX ERROR</b><br/><br/> Could not determine next unique id for SEARCH DICTIONARY.", "error", eWizardButtonState.False);
                    return 0;
                }

                dictFields.Add("PARM_NAME", sParmName);   
                dictFields.Add("STR_PARM_VALUE", sParmValue); 
                dictFields.Add("ROW_ID", lID);            
                dictFields.Add("PARM_DESC", sParmDesc);   
                dictFields.Add("PARM_DATA_TYPE", sParmDataType);
                dictFields.Add("PARM_CATEGORY", iDefaultCategory);

                strSQLquery = RISKMASTERScripts.InsertNewRow("PARMS_NAME_VALUE", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddParmsNameValueLine] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }
        //Code end - rkaur7

        /// <summary>
        /// add row to WCP_FORMS table
        /// </summary>
        /// <param name="iStateRowID"></param>
        /// <param name="iFormCategory"></param>
        /// <param name="sFormTitle"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFileName"></param>
        /// <param name="iActiveFlag"></param>
        /// <param name="sHashCRC"></param>
        /// <param name="iPrimaryFormFlag"></param>
        /// <param name="iLineOfBusCode"></param>
        /// <returns></returns>
        public static int AddWCPForm(int iStateRowID, int iFormCategory, string sFormTitle, string sFormName, string sFileName, int iActiveFlag, string sHashCRC, int iPrimaryFormFlag, int iLineOfBusCode)
        {
            string strErrDesc = String.Empty;
            string strSQLquery = String.Empty;

            try
            {
                int iRowCnt = 0;
                Dictionary<string, object> dictFields = new Dictionary<string, object>();

                string sSQL = "SELECT FORM_ID FROM WCP_FORMS WHERE STATE_ROW_ID = " + iStateRowID + " AND FORM_CATEGORY = " +
                              iFormCategory + " AND UPPER(FILE_NAME) ='" + sFileName.ToUpper() + "'";
                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
                }
                catch(Exception)
                {
                    iRowCnt = 0;
                }

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = DisplayDBUpgrade.GetNextUID("WCP_FORMS");
                dictFields.Add("STATE_ROW_ID",iStateRowID);
                dictFields.Add("FORM_CATEGORY",iFormCategory);
                dictFields.Add("FORM_ID",lID);
                dictFields.Add("FORM_TITLE",sFormTitle);
                dictFields.Add("FORM_NAME",sFormName);
                dictFields.Add("FILE_NAME",sFileName);
                dictFields.Add("ACTIVE_FLAG",iActiveFlag);
                dictFields.Add("HASH_CRC",sHashCRC);
                dictFields.Add("PRIMARY_FORM_FLAG",iPrimaryFormFlag);
                dictFields.Add("LINE_OF_BUS_CODE",iLineOfBusCode);

                strSQLquery = RISKMASTERScripts.InsertNewRow("WCP_FORMS", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddWCPForm] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        /// <summary>
        /// add claim form
        /// </summary>
        /// <param name="iStateRowID"></param>
        /// <param name="iFormCategory"></param>
        /// <param name="sFormTitle"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFileName"></param>
        /// <param name="iActiveFlag"></param>
        /// <param name="sHashCRC"></param>
        /// <param name="iPrimaryFormFlag"></param>
        /// <param name="iLineOfBusCode"></param>
        /// <returns></returns>
        public static int AddClaimForm(int iStateRowID, int iFormCategory, string sFormTitle, string sFormName, string sFileName, int iActiveFlag, string sHashCRC, int iPrimaryFormFlag, int iLineOfBusCode)
        {
            string strErrDesc = String.Empty;
            string strSQLquery = String.Empty;

            try
            {
                Dictionary<string, object> dictFields = new Dictionary<string, object>();

                string sSQL = "SELECT FORM_ID FROM CL_FORMS WHERE STATE_ROW_ID = " + iStateRowID + " AND FORM_CATEGORY = " +
                              iFormCategory + " AND UPPER(FILE_NAME) ='" + sFileName.ToUpper() + "'";
                int iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = DisplayDBUpgrade.GetNextUID("CL_FORMS");
                dictFields.Add("STATE_ROW_ID", iStateRowID);
                dictFields.Add("FORM_CATEGORY", iFormCategory);
                dictFields.Add("FORM_ID", lID);
                dictFields.Add("FORM_TITLE", sFormTitle);
                dictFields.Add("FORM_NAME", sFormName);
                dictFields.Add("FILE_NAME", sFileName);
                dictFields.Add("ACTIVE_FLAG", iActiveFlag);
                dictFields.Add("HASH_CRC", sHashCRC);
                dictFields.Add("PRIMARY_FORM_FLAG", iPrimaryFormFlag);
                dictFields.Add("LINE_OF_BUS_CODE", iLineOfBusCode);

                strSQLquery = RISKMASTERScripts.InsertNewRow("CL_FORMS", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddClaimForm] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        /// <summary>
        /// add a new permission and default it to be enabled
        /// insert a record to table FUNCTION_LIST in the security database
        /// insert a record to table GROUP_PERMISSIONS in riskmaster database
        /// for each security group in table USER_GROUPS
        /// </summary>
        /// <param name="lFunctionID"></param>
        /// <param name="sFunctionName"></param>
        /// <param name="lParentID"></param>
        /// <param name="lEntryType"></param>
        /// <param name="dbsec"></param>
        /// <returns></returns>
        public static int AddPermissions(int lFunctionID, string sFunctionName, int lParentID, int lEntryType, int dbsec)
        {
            string sSQL = "SELECT FUNC_ID FROM FUNCTION_LIST WHERE FUNC_ID =" + lFunctionID;
            int iFuncID = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_SecConnectString, sSQL);

            if (iFuncID != 0)
            {
                sSQL = "UPDATE FUNCTION_LIST SET FUNCTION_NAME = '" + sFunctionName + "', PARENT_ID = " + lParentID +
                       ", ENTRY_TYPE =" + lEntryType + " WHERE FUNC_ID = " + lFunctionID;
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_SecConnectString, sSQL);

                return iFuncID;
            }

            sSQL = "INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES(" + lFunctionID + ",'" +
                   sFunctionName + "'," + lParentID + "," + lEntryType + ")";

            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_SecConnectString, sSQL);

            //retrieve all the group id(s) from riskmaster table USER_GROUPS
            Collection<Int32> myList = new Collection<Int32>();

            sSQL = "SELECT GROUP_ID FROM USER_GROUPS";

            using (DbReader dBRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
            {
                if (dBRdr != null)
                {
                    while (dBRdr.Read())
                    {
                        myList.Add(Convert.ToInt32(dBRdr["GROUP_ID"]));
                    }
                }
            }
            
            //set the default permission to be enabled
            if (myList.Count > 0)
            {
                foreach(int iGroupID in myList)
                {
                    string sSqlExistingRow = "SELECT COUNT(*) FROM GROUP_PERMISSIONS WHERE GROUP_ID = " + iGroupID +
                                             " AND FUNC_ID = " + lFunctionID;
                    int lCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSqlExistingRow);

                    if (lCount == 0)
                    {
                        sSQL = "INSERT INTO GROUP_PERMISSIONS(GROUP_ID, FUNC_ID) VALUES(" + iGroupID + "," + lFunctionID + ")";
                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                    }
                }
            }

            return lFunctionID;
        }

        /// <summary>
        /// add event form
        /// </summary>
        /// <param name="iStateRowID"></param>
        /// <param name="iFormCategory"></param>
        /// <param name="sFormTitle"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFileName"></param>
        /// <param name="iActiveFlag"></param>
        /// <param name="sHashCRC"></param>
        /// <param name="iPrimaryFormFlag"></param>
        /// <param name="iLineOfBusCode"></param>
        /// <returns></returns>
        public static int AddEventForm(int iStateRowID, int iFormCategory, string sFormTitle, string sFormName, string sFileName, int iActiveFlag, string sHashCRC, int iPrimaryFormFlag, int iLineOfBusCode)
        {
            string strErrDesc = String.Empty;
            string strSQLquery = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();

            try
            {
                string sSQL = "SELECT FORM_ID FROM EV_FORMS WHERE STATE_ROW_ID = " + iStateRowID + " AND FORM_CATEGORY = " +
                           iFormCategory + " AND UPPER(FILE_NAME) ='" + sFileName.ToUpper() + "'";

                int iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                int lID = DisplayDBUpgrade.GetNextUID("EV_FORMS");
                dictFields.Add("STATE_ROW_ID", iStateRowID);
                dictFields.Add("FORM_CATEGORY", iFormCategory);
                dictFields.Add("FORM_ID", lID);
                dictFields.Add("FORM_TITLE", sFormTitle);
                dictFields.Add("FORM_NAME", sFormName);
                dictFields.Add("FILE_NAME", sFileName);
                dictFields.Add("ACTIVE_FLAG", iActiveFlag);
                dictFields.Add("HASH_CRC", sHashCRC);
                dictFields.Add("PRIMARY_FORM_FLAG", iPrimaryFormFlag);
                dictFields.Add("LINE_OF_BUS_CODE", iLineOfBusCode);

                strSQLquery = RISKMASTERScripts.InsertNewRow("EV_FORMS", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddEventForm] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return lID;

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        /// <summary>
        /// clean up possible multi case versions of the same file name
        /// </summary>
        /// <param name="sFileName"></param>
        /// <returns></returns>
        public static int WCPFormsDualCaseOff(string sFileName)
        {
            string sFileNameLCase = sFileName.ToLower();
            string sFilenameUCase = sFileName.ToUpper();

            var sSQL = "SELECT COUNT(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG <> 0 AND (FILE_NAME = '" +
                       sFileNameLCase + "' OR FILE_NAME = '" + sFilenameUCase + "')";
            int lCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

            if( lCount == 2)
            {
                //turn off upper case version
                sSQL = "UPDATE WCP_FORMS SET PRIMARY_FORM_FLAG = 0 WHERE FILE_NAME = '" + sFilenameUCase + "'";
                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
            }

            return lCount;
        }

        /// <summary>
        /// get code ID using supplied tableID and Short Code
        /// </summary>
        /// <param name="sShortCode"></param>
        /// <param name="lTableID"></param>
        /// <returns></returns>
        public static int GetCodeIDWithShort(string sShortCode, int lTableID)
        {
            int lCodeId = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + lTableID + " AND SHORT_CODE = '" + sShortCode.Trim() + "'");

            return lCodeId;
        }

        /// <summary>
        /// update glossary and glossary_text tables accordingly
        /// </summary>
        /// <param name="sSysTableName"></param>
        /// <param name="sUserTableName"></param>
        /// <param name="iGlossType"></param>
        /// <param name="iNLSCode"></param>
        /// <param name="lParentTableID"></param>
        /// <returns></returns>
        public static int UpdateGlossary(string sSysTableName, string sUserTableName, int iGlossType, int iNLSCode, int lParentTableID)
        {
            int lGlossaryTypeCode = GetCodeIDWithShort(iGlossType.ToString(), GetTableID("GLOSSARY_TYPES"));

            //return 0 if no code id is returned
            if (lGlossaryTypeCode == 0)
            {
                return 0;
            }

            string sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sSysTableName.Trim() + "'";
            int lTableID = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

            //existing table found return table id
            if (lTableID != 0)
            {
                return lTableID;
            }

            //if existing table id not found proceed
            lTableID = DisplayDBUpgrade.GetNextUID("GLOSSARY");

            //next unique ID not found for table used return 0
            if (lTableID <= 0)
            {
                return 0;
            }

            //insert the values into the GLOSSARY table
            if (lParentTableID == 0)
            {
                sSQL = "INSERT INTO GLOSSARY (TABLE_ID, SYSTEM_TABLE_NAME, GLOSSARY_TYPE_CODE, NEXT_UNIQUE_ID, " + 
                       "DELETED_FLAG, REQD_REL_TABL_FLAG, ATTACHMENTS_FLAG, REQD_IND_TABL_FLAG, LINE_OF_BUS_FLAG) " + 
                       "VALUES(" + lTableID + ",'" + sSysTableName.Trim() + "'," + lGlossaryTypeCode + ",1,0,0,0,0,0)";
            }
            else
            {
                sSQL = "INSERT INTO GLOSSARY (TABLE_ID, SYSTEM_TABLE_NAME, GLOSSARY_TYPE_CODE, NEXT_UNIQUE_ID, " + 
                       "DELETED_FLAG, REQD_REL_TABL_FLAG, ATTACHMENTS_FLAG, REQD_IND_TABL_FLAG, LINE_OF_BUS_FLAG) " + 
                       "VALUES(" + lTableID + ",'" + sSysTableName.Trim() + "'," + lGlossaryTypeCode + "," + lParentTableID + ",0,-1,0,0,0)";
            }

            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

            //insert the values into the GLOSSARY_TEXT table
            sSQL = "INSERT INTO GLOSSARY_TEXT (TABLE_ID, TABLE_NAME, LANGUAGE_CODE) VALUES(" + lTableID + ",'" + sUserTableName + "'," + iNLSCode + ")";

            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

            //stamp glossary for recache
            UpdateGlossaryTimeStamp("GLOSSARY");
            
            return lTableID;
        }

        /// <summary>
        /// used to process all forms db upgrade for jurisdictional pdf forms
        /// </summary>
        /// <param name="dbsec"></param>
        /// <returns></returns>
        public static int DataBaseUpgradeA(int dbsec)
        {
            int iReturnVal = 0;

            try
            {
                string sSQL = "DELETE FROM WCP_FORMS WHERE FILE_NAME IS NULL";
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                //rpandey20 02/27/2009  -- MITS 14446 Removing duplicate rows from table ******
                sSQL = "  DELETE FROM WCP_FORMS WHERE FORM_ID NOT IN ( ";
                sSQL += " SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 GROUP BY FILE_NAME ";
                sSQL += "  UNION ";
                sSQL += " SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=-1 GROUP BY FILE_NAME)";
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                //-------------------------------------------------------------

                sSQL = "DELETE FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 AND FILE_NAME IN  ";
                sSQL += " (SELECT FILE_NAME FROM WCP_FORMS GROUP BY FILE_NAME HAVING COUNT(FILE_NAME)> 1)";
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                // DBMS_IS_ACCESS   = 0,
                // DBMS_IS_SQLSRVR  = 1,
                // DBMS_IS_SYBASE   = 2,
                // DBMS_IS_INFORMIX = 3,
                // DBMS_IS_ORACLE   = 4,
                // DBMS_IS_ODBC     = 5,
                // DBMS_IS_DB2      = 6,

                switch (dbsec)
                {
                    case RiskmasterDBTypes.DBMS_IS_ACCESS:
                        //    'jtodd22 03/18/2008 --MITS 11819
                        try
                        {
                            sSQL = "DROP INDEX RM_WCP_FORMS ON WCP_FORMS";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME TEXT(64) NOT NULL";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS(FILE_NAME)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
                        }
                        break;
                    case RiskmasterDBTypes.DBMS_IS_DB2:
                        //    'jtodd22 03/18/2008 --MITS 11819
                        try
                        {
                            sSQL = "DROP INDEX RM_WCP_FORMS";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME VARCHAR(64) NOT NULL";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS(FILE_NAME)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
                        }
                        break;
                    case RiskmasterDBTypes.DBMS_IS_INFORMIX:
                        //    'jtodd22 03/18/2008 --MITS 11819
                        try
                        {
                            sSQL = "DROP INDEX RM_WCP_FORMS";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "ALTER TABLE WCP_FORMS MODIFY (FILE_NAME VARCHAR(64) NOT NULL)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS(FILE_NAME)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
                        }
                        break;
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        //    'jtodd22 03/18/2008 --MITS 11819
                        try
                        {
                            sSQL = "DROP INDEX RM_WCP_FORMS";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "ALTER TABLE WCP_FORMS MODIFY(FILE_NAME VARCHAR2(64) NOT NULL)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS(FILE_NAME)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
                        }
                        break;
                    case RiskmasterDBTypes.DBMS_IS_ODBC:
                    case RiskmasterDBTypes.DBMS_IS_SYBASE:
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                    default:
                        //    'jtodd22 03/18/2008 --MITS 11819
                        try
                        {
                            sSQL = "DROP INDEX WCP_FORMS.RM_WCP_FORMS";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "ALTER TABLE WCP_FORMS ALTER COLUMN FILE_NAME VARCHAR(64) NOT NULL";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                            sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS(FILE_NAME)";
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
                        }
                        break;
                }

                iReturnVal = 1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: [UpdateFunctions.DataBaseUpgradeA] " + ex.Message);
            }

            return iReturnVal;
        }

        #endregion

        /// <summary>
        /// return proper error codes for forgive statements
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public static string CheckForgive(string sSQL)
        {
            bForgive = false;
            
            sSQL = sSQL.Replace(" \t", " ");
            sSQL = sSQL.Replace(" \t\t", " ");
            sSQL = sSQL.Replace("\t", " ");
            sSQL = sSQL.Replace(" (", "(");
            sSQL = sSQL.Trim();

            if (sSQL.Length >= 9)
            {
                if (sSQL.Substring(0, 9).ToUpper() == "[FORGIVE]")
                {
                    sSQL = sSQL.Substring(9);

                    if (sSQL.IndexOf("ALTER TABLE ") >= 0)
                    {
                        sSQL = "[FORGIVE_ALTER] " + sSQL;
                    }
                    else if (sSQL.IndexOf("CREATE TABLE ") >= 0)
                    {
                        sSQL = "[FORGIVE_CREATETABLE] " + sSQL;
                    }
                    else if (sSQL.IndexOf("CREATE INDEX ") >= 0)
                    {
                        sSQL = "[FORGIVE_CREATEINDEX] " + sSQL;
                    }
                    else if (sSQL.IndexOf("CREATE UNIQUE INDEX ") >= 0)
                    {
                        sSQL = "[FORGIVE_CREATEINDEX] " + sSQL;
                    }
                    else if (sSQL.IndexOf("DROP INDEX ") >= 0)
                    {
                        sSQL = "[FORGIVE_DROPINDEX] " + sSQL;
                    }
                    else if (sSQL.IndexOf("DROP TABLE ") >= 0)
                    {
                        sSQL = "[FORGIVE_DROPTABLE] " + sSQL;
                    }
                    else if (sSQL.IndexOf("INSERT INTO ") >= 0)
                    {
                        sSQL = "[FORGIVE_INSERT] " + sSQL;
                    }
                    else
                    {
                        sSQL = "[FORGIVE] " + sSQL;
                    }
                }
            }

            if (sSQL.IndexOf("[FORGIVE] ") >= 0)
            {
                sSQL = sSQL.Substring(10);
                bForgive = true; //traditional - FORGIVE EVERYTHING
                sNativeError = string.Empty;
                sODBCError = string.Empty;
            }
            else if(sSQL.IndexOf("[FORGIVE_ALTER] ") >= 0)
            {
                sSQL = sSQL.Substring(15);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "2705";
                            sODBCError = "S0021";
                            break;
                        }
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "1430";
                            sODBCError = String.Empty;
                            break;
                        }
                    default:
                    {
                        sNativeError = String.Empty;
                        sODBCError = "S0021";
                        break;
                    }
                }
            }
            else if (sSQL.IndexOf("[FORGIVE_CREATETABLE] ") >= 0)
            {
                sSQL = sSQL.Substring(22);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "2714";
                            sODBCError = "S0001";
                            break;
                        } 
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "955";
                            sODBCError = "S0001";
                            break;
                        }
                    default:
                        {
                            sNativeError = String.Empty;
                            sODBCError = "S0001";
                            break;
                        }
                }
            }
            else if (sSQL.IndexOf("[FORGIVE_CREATEINDEX] ") >= 0)
            {
                sSQL = sSQL.Substring(22);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "1913";
                            sODBCError = "S0011";
                            break;
                        }
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "955";
                            sODBCError = "S0001";
                            break;
                        }
                    default:
                        {
                            sNativeError = String.Empty;
                            sODBCError = "S0011";
                            break;
                        }
                }
            }
            else if (sSQL.IndexOf("[FORGIVE_DROPTABLE] ") >= 0)
            {
                sSQL = sSQL.Substring(20);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "3701";
                            sODBCError = "S0002";
                            break;
                        }
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "942";
                            sODBCError = "S0002";
                            break;
                        }
                    default:
                        {
                            sNativeError = String.Empty;
                            sODBCError = "S0002";
                            break;
                        }
                }
            }
            else if (sSQL.IndexOf("[FORGIVE_DROPINDEX] ") >= 0)
            {
                sSQL = sSQL.Substring(20);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "3703";
                            sODBCError = "S0012";
                            break;
                        }
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "1418";
                            sODBCError = String.Empty;
                            break;
                        }
                    default:
                        {
                            sNativeError = String.Empty;
                            sODBCError = "S0012";
                            break;
                        }
                }
            }
            else if (sSQL.IndexOf("[FORGIVE_INSERT] ") >= 0)
            {
                sSQL = sSQL.Substring(17);
                bForgive = true; //FORGIVE EVERYTHING - log if not a duplicate

                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                        {
                            sNativeError = "2601";
                            sODBCError = "23000";
                            break;
                        }
                    case RiskmasterDBTypes.DBMS_IS_ORACLE:
                        {
                            sNativeError = "1";
                            sODBCError = "23000";
                            break;
                        }
                    default:
                        {
                            sNativeError = String.Empty;
                            sODBCError = "23000";
                            break;
                        }
                }
            }

            return sSQL;
        }

        /// <summary>
        /// convert data types per DBType for use in alter table statements
        /// </summary>
        /// <param name="nFieldType"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        public static string sTypeMap(int nFieldType, int nSize)
        {
            var sType = string.Empty;

            switch (nFieldType)
            {
                case (0):
                case (3):
                case (4):
                case (10):
                case (12):
                case (13)://string, date, time, claim lookup, event lookup, vehicle lookup
                    {
                        switch (DisplayDBUpgrade.g_dbMake)
                        {
                            case (RiskmasterDBTypes.DBMS_IS_SQLSRVR):
                                {
                                    sType = "VARCHAR";
                                    break;
                                }
                            case (RiskmasterDBTypes.DBMS_IS_ORACLE):
                                {
                                    sType = "VARCHAR2";
                                    break;
                                }
                        }
                        sType += "(" + nSize + ")";
                        break;
                    }
                case (6):
                case (7):
                case (8):
                case (9):
                case (14):
                case (15):
                case (22): //User look up
                case (16)://code, primary key, entity, state, multi-code, multi-entity, multi-state
                case (31)://MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 (Adding a new supplemental field type "checkbox" ) 
                    {
                        switch (DisplayDBUpgrade.g_dbMake)
                        {
                            case (RiskmasterDBTypes.DBMS_IS_SQLSRVR):
                                {
                                    sType = "INT";
                                    break;
                                }
                            case (RiskmasterDBTypes.DBMS_IS_ORACLE):
                                {
                                    sType = "NUMBER(10,0)";
                                    break;
                                }
                        }
                        break;
                    } 
                case (1):
                case (2):// number, currency (float)
                    {
                        switch (DisplayDBUpgrade.g_dbMake)
                        {
                            case (RiskmasterDBTypes.DBMS_IS_SQLSRVR):
                                {
                                    sType = "FLOAT";
                                    break;
                                }
                            case (RiskmasterDBTypes.DBMS_IS_ORACLE):
                                {
                                    sType = "NUMBER";
                                    break;
                                }
                        }
                        break;
                    }
                case (5):
                case (25): // HTML text 
                case (11): //free text (and old multi-valued)
                    {
                        switch (DisplayDBUpgrade.g_dbMake)
                        {
                            case (RiskmasterDBTypes.DBMS_IS_SQLSRVR):
                                {
                                    sType = "TEXT";
                                    break;
                                }
                            case (RiskmasterDBTypes.DBMS_IS_ORACLE):
                                {
                                    sType = "CLOB";
                                    break;
                                }
                        }
                        break;
                    }              
            }

            //add null for sql and oracle fields
            if(DisplayDBUpgrade.g_dbMake == RiskmasterDBTypes.DBMS_IS_SQLSRVR | DisplayDBUpgrade.g_dbMake == RiskmasterDBTypes.DBMS_IS_ORACLE )
            {
                sType += " NULL";
            }

            return sType;
        }

        /// <summary>
        /// updates the XML Views in the dB for Form Data Manager
        /// </summary>
        /// <returns></returns>
        public static bool UpdateFDMViews(bool bIsSilent)
        {
            bool bInsertXmlViews = (DisplayDBUpgrade.INSERTXMLVIEWS.ToUpper() == bool.TrueString.ToUpper()) ? true : false;

            string sLogPath = DisplayDBUpgrade.GetLogFilePath();
            string strViewsPath = String.Format("{0}\\views", Application.StartupPath);

            if (!Directory.Exists(strViewsPath))
            {
                strViewsPath = String.Format(@"{0}\..\..\views", Application.StartupPath);
                if (!Directory.Exists(strViewsPath))
                {
                    DisplayDBUpgrade.LogErrors(sLogPath, "---Error: views directory not found!");
                    DisplayDBUpgrade.LogErrors(sLogPath, "\r\n===================================================================================");

                    return false;
                }
            }

            if (!bIsSilent)
            {
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
                frmWizard.UpdateScriptName("Updating Data Entry Base Views.....");
            }
            else
            {
                //Console.WriteLine("");
                //Console.WriteLine("Updating Data Entry Base Views");
            }

            // npadhy If the InsertXmlViews is false in the App.Config then do not insert existing views as well
            if (bInsertXmlViews)
            {
                DisplayDBUpgrade.LogErrors(sLogPath, "\r\nCopying Existing Views Started - " + DateTime.Now);
                DisplayDBUpgrade.LogErrors(sLogPath, "===================================================================================\r\n");

                //copy existing views from base db to view db -
                //- BUT, if the views already exist in the view db, return false

                bool bCopySuccess = CopyExistingViews(DisplayDBUpgrade.g_iDSNID);

                //rsolanki2: RM extensibility updates
                if (UpdateFunctions._bCopyCustomViews)
                {
                    CopyExistingCustomViews();//DisplayDBUpgrade.g_iDSNID);
                }

                if (bCopySuccess)
                {
                    DisplayDBUpgrade.LogErrors(sLogPath, "\r\n===================================================================================");
                    DisplayDBUpgrade.LogErrors(sLogPath, "Copying Existing Views Finished - " + DateTime.Now + "\r\n");
                }
                else
                {
                    DisplayDBUpgrade.LogErrors(sLogPath, "\r\n===================================================================================");
                    DisplayDBUpgrade.LogErrors(sLogPath, "Views already exist or were not found - " + DateTime.Now + "\r\n");
                    //DisplayDBUpgrade.bAborted = true;
                    //return false;
                }


                DisplayDBUpgrade.LogErrors(sLogPath, "\r\nUpdating Views Started - " + DateTime.Now);
                DisplayDBUpgrade.LogErrors(sLogPath, "===================================================================================\r\n");

                try
                {
                    int lViewCnt = 0;

                    string[] fFiles = Directory.GetFiles(strViewsPath);

                    if (!bIsSilent)
                    {
                        frmWizard.SetCurrentProgressBarProperties(fFiles.Length);
                        frmWizard.SetOverallProgressBarProperties(fFiles.Length);
                    }

                    foreach (string strFileName in fFiles)
                    {
                        FileInfo objFileInfo = new FileInfo(strFileName);

                        if (objFileInfo.Extension.ToLower().Equals(".xml"))
                        {
                            lViewCnt++;

                            string sFile = objFileInfo.Name;

                            if (!bIsSilent)
                            {
                                frmWizard.UpdateExecutingText("Updating view: " + sFile);
                                frmWizard.UpdateCurrentProgressBar(lViewCnt);
                                frmWizard.UpdateOverallProgressBar(lViewCnt);
                            }

                            InsertSystemDataScreen(strFileName, 0);
                        }

                        //Collection<Int32> coll = new Collection<Int32>();

                        //using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_ViewConnectString, RISKMASTERScripts.GetFDMViews(DisplayDBUpgrade.g_iDSNID)))
                        //{
                        //    while (rdr.Read())
                        //    {
                        //        coll.Add(Convert.ToInt32(rdr["VIEW_ID"]));
                        //    }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    //ERROR - Error processing view update
                    //frmWizard.ErrorDisplay("<b>Error updating views:</b><br/><br/> "+ex.Message,"error",eWizardButtonState.False);
                    DisplayDBUpgrade.bAborted = true;
                    DisplayDBUpgrade.LogErrors(sLogPath, "-Error: " + ex.Message);
                    DisplayDBUpgrade.LogErrors(sLogPath, "\r\n===================================================================================");
                    DisplayDBUpgrade.LogErrors(sLogPath, "Updating Views Finished - " + DateTime.Now + "\r\n");

                    return false;
                }

                DisplayDBUpgrade.LogErrors(sLogPath, "\r\n===================================================================================");
                DisplayDBUpgrade.LogErrors(sLogPath, "Updating Views Finished - " + DateTime.Now + "\r\n");
            }

            return true;
        }

        /// <summary>
        /// insert form XML into database
        /// </summary>
        /// <param name="strFormPath"></param>
        /// <param name="lViewID"></param>
        /// <returns></returns>
        public static bool InsertSystemDataScreen(string strFormPath, int lViewID)
        {
            string sFormName = String.Empty;
            string sFormTitle = String.Empty;
            const string strParameterName = "XMLVIEW";
            StringBuilder strSQLBuilder = new StringBuilder();
            XmlDocument dom = new XmlDocument();

            try
            {
                dom.Load(strFormPath);

                sFormTitle = (dom.SelectSingleNode("//form/@title") != null) ? dom.SelectSingleNode("//form/@title").InnerText 
                                                                             : "Unspecified";

                FileInfo objFileInfo = new FileInfo(strFormPath);
                sFormName = objFileInfo.Name;

                //Delete existing view
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, RISKMASTERScripts.DeletePowerViews(lViewID, sFormName, DisplayDBUpgrade.g_iDSNID));

                string strParameterType = RiskmasterDBTypes.ConvertDBParameters(DisplayDBUpgrade.g_dbMake);
                string strParameter = string.Format("~{0}~", strParameterName);

                //Add in view
                strSQLBuilder.Append("INSERT INTO NET_VIEW_FORMS (VIEW_ID, DATA_SOURCE_ID, FORM_NAME, TOPLEVEL, CAPTION, VIEW_XML)");

                //Dictionary<string, object> dictParams = new Dictionary<string, object>();
                //DbFactory.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, strSQLBuilder.ToString(), dictParams);
                //strSQLBuilder.Append(string.Format("{0}", "~TEST~"));
                //dictParams.Add("TEST", lViewID);
                
                strSQLBuilder.Append("VALUES (");
                strSQLBuilder.Append(lViewID);
                strSQLBuilder.Append(",");
                strSQLBuilder.Append(DisplayDBUpgrade.g_iDSNID);
                strSQLBuilder.Append(",");
                strSQLBuilder.Append(string.Format("'{0}'", sFormName));
                strSQLBuilder.Append(",1,");
                strSQLBuilder.Append(string.Format("'{0}'", sFormTitle));
                strSQLBuilder.Append(",");
                strSQLBuilder.Append(strParameter);
                strSQLBuilder.Append(")");

                NameValueCollection nvColl = new NameValueCollection {{strParameterName, dom.OuterXml}};

                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, strSQLBuilder.ToString(), nvColl);

                return true;
            }
            catch(Exception ex)
            {
                string sExMessage = ex.Message;
                //ERROR - Error processing view update
                //frmWizard.ErrorDisplay("<b>Error updating views:</b><br/><br/> " + ex.Message , "error", eWizardButtonState.False);
                DisplayDBUpgrade.bAborted = true;

                return false;
            }
        }

         /// <summary>
        /// copy views data from source dB to new VIEWS dB
        /// </summary>
        /// <param name="iDSN"></param>
        /// <returns></returns>
        public static void CopyExistingCustomViews()
        {
            //rsolanki2: RM extensibility updates

            //deleting the custom views if found in view database
            string sSQL = "DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID < 0 AND  DATA_SOURCE_ID = " 
                + DisplayDBUpgrade.g_iDSNID;
            
            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, sSQL);

            sSQL = "SELECT VIEW_ID, FORM_NAME, TOPLEVEL, CAPTION, VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID < 0 ";// AND DATA_SOURCE_ID = " + DisplayDBUpgrade.g_iDSNID;
            StringBuilder strSQLBuild = new StringBuilder();

            frmWizard.UpdateExecutingText("Copying Custom Views.......");
            using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
            {
                while (rdr.Read())
                {
                    strSQLBuild.Remove(0, strSQLBuild.Length);//clear existing string

                    DbConnection oConnection = null;
                    DbCommand oCommand = null;
                    DbParameter objParam = null;

                    oConnection = DbFactory.GetDbConnection(DisplayDBUpgrade.g_ViewConnectString);
                    oConnection.Open();
                    oCommand = oConnection.CreateCommand();

                    strSQLBuild.AppendFormat("INSERT INTO NET_VIEW_FORMS (VIEW_ID, DATA_SOURCE_ID, FORM_NAME, TOPLEVEL, CAPTION, VIEW_XML) VALUES ({0},{1},'{2}',1,'{3}',~VXML~)",
                        rdr["VIEW_ID"], //param 0
                        DisplayDBUpgrade.g_iDSNID, //param 1
                        sSQLStringLiteral(rdr["FORM_NAME"].ToString()), //param 2
                        sSQLStringLiteral(rdr["CAPTION"].ToString())); //param 3
                    oCommand.Parameters.Clear();
                    objParam = oCommand.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = sSQLStringLiteral(rdr["VIEW_XML"].ToString());
                    objParam.ParameterName = "VXML";
                    objParam.SourceColumn = "VIEW_XML";
                    oCommand.Parameters.Add(objParam);
                    oCommand.CommandText = strSQLBuild.ToString();
                    oCommand.ExecuteNonQuery();

                    oConnection.Close();
                    oConnection.Dispose();
                }
            }
        }

        /// <summary>
        /// copy views data from source dB to new VIEWS dB
        /// </summary>
        /// <param name="iDSN"></param>
        /// <returns></returns>
        public static bool CopyExistingViews(int iDSN)
        {
            StringBuilder strSQLBuild = new StringBuilder();

            try
            {
                //Check if the base view for this data source exists in the View data
                //rsolanki2 : extensibility updates : checking only the entries for power views (skipping base and custom views)
                string sSQL = "SELECT COUNT(*) ROWCNT FROM NET_VIEW_FORMS WHERE VIEW_ID > 0 AND DATA_SOURCE_ID = " + DisplayDBUpgrade.g_iDSNID;
                int lCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, sSQL);

                //If there are base view forms in the View DB, we should not try to copy the views.
                if (lCount > 0)
                {
                    return true;
                }

                frmWizard.UpdateExecutingText("Copying Views.......");

                //copy net views table
                #region NET_VIEWS
                sSQL = "SELECT * FROM NET_VIEWS WHERE VIEW_ID > 0";

                using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    while (rdr.Read())
                    {
                        //clear existing string
                        strSQLBuild.Remove(0, strSQLBuild.Length);

                        strSQLBuild.Append("INSERT INTO NET_VIEWS (VIEW_ID, DATA_SOURCE_ID, VIEW_NAME, VIEW_DESC, HOME_PAGE, PAGE_MENU) ");
                        strSQLBuild.Append("VALUES(");
                        strSQLBuild.Append(rdr["VIEW_ID"]);
                        strSQLBuild.Append(",");
                        strSQLBuild.Append(DisplayDBUpgrade.g_iDSNID);
                        strSQLBuild.Append(",'");
                        strSQLBuild.Append(rdr["VIEW_NAME"].ToString().Replace("'", "''"));
                        strSQLBuild.Append("','");
                        strSQLBuild.Append(rdr["VIEW_DESC"].ToString().Replace("'", "''"));
                        strSQLBuild.Append("','");
                        strSQLBuild.Append(rdr["HOME_PAGE"].ToString().Replace("'", "''"));
                        strSQLBuild.Append("','");
                        strSQLBuild.Append(rdr["PAGE_MENU"]);
                        strSQLBuild.Append("')");

                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, strSQLBuild.ToString());
                    }
                }
                #endregion

                //copy net_views_members table
                #region NET_VIEWS_MEMBERS
                sSQL = "SELECT * FROM NET_VIEWS_MEMBERS WHERE VIEW_ID > 0";
                using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    while (rdr.Read())
                    {
                        //clear existing string
                        strSQLBuild.Remove(0, strSQLBuild.Length);

                        strSQLBuild.Append("INSERT INTO NET_VIEWS_MEMBERS (VIEW_ID, DATA_SOURCE_ID, MEMBER_ID, ISGROUP) ");
                        strSQLBuild.Append("VALUES(");
                        strSQLBuild.Append(rdr["VIEW_ID"]);
                        strSQLBuild.Append(",");
                        strSQLBuild.Append(DisplayDBUpgrade.g_iDSNID);
                        strSQLBuild.Append(",");
                        strSQLBuild.Append(rdr["MEMBER_ID"]);
                        strSQLBuild.Append(",");
                        strSQLBuild.Append(rdr["ISGROUP"]);
                        strSQLBuild.Append(")");

                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, strSQLBuild.ToString());
                    }
                }
                #endregion

                //copy net_view_forms table
                #region NET_VIEW_FORMS
                sSQL = "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID > 0";
                using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    while (rdr.Read())
                    {
                        strSQLBuild.Remove(0, strSQLBuild.Length);//clear existing string

                        DbConnection oConnection = null;
                        DbCommand oCommand = null;
                        DbParameter objParam = null;

                        oConnection = DbFactory.GetDbConnection(DisplayDBUpgrade.g_ViewConnectString);
                        oConnection.Open();
                        oCommand = oConnection.CreateCommand();

                        strSQLBuild.AppendFormat("INSERT INTO NET_VIEW_FORMS (VIEW_ID, DATA_SOURCE_ID, FORM_NAME, TOPLEVEL, CAPTION, VIEW_XML) VALUES ({0},{1},'{2}',1,'{3}',~VXML~)", 
                            rdr["VIEW_ID"], //param 0
                            DisplayDBUpgrade.g_iDSNID, //param 1
                            sSQLStringLiteral(rdr["FORM_NAME"].ToString()), //param 2
                            sSQLStringLiteral(rdr["CAPTION"].ToString())); //param 3
                        oCommand.Parameters.Clear();
                        objParam = oCommand.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = sSQLStringLiteral(rdr["VIEW_XML"].ToString());
                        objParam.ParameterName = "VXML";
                        objParam.SourceColumn = "VIEW_XML";
                        oCommand.Parameters.Add(objParam);
                        oCommand.CommandText = strSQLBuild.ToString();
                        oCommand.ExecuteNonQuery();

                        oConnection.Close();
                        oConnection.Dispose();
                    }
                }
                #endregion
            }
            catch(Exception ex)
            {
                frmWizard.UpdateExecutingText(String.Empty);
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(),"-Error: " + ex.Message + "\r\n" + strSQLBuild.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// update power views
        /// </summary>
        /// <returns></returns>
        public static bool UpdatePowerViews(bool bIsSilent)
        {
            XmlDocument dom = new XmlDocument();
            NameValueCollection collViews = new NameValueCollection();

            if (!bIsSilent)
            {
                frmWizard.UpdateScriptName("Updating Powerviews.....");
            }
            else
            {
                //Console.WriteLine("Updating Powerviews");
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\nUpdating Powerviews Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================\r\n");

            //Go through powerview screens that have known changes
            using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_ViewConnectString, RISKMASTERScripts.GetPowerViews(DisplayDBUpgrade.g_iDSNID)))
            {
                while (rdr.Read())
                {
                    collViews.Add(rdr["VIEW_ID"].ToString(), rdr["FORM_NAME"].ToString());
                }
            }

            if (!bIsSilent)
            {
                frmWizard.SetCurrentProgressBarProperties(collViews.Count);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.SetOverallProgressBarProperties(collViews.Count);
                frmWizard.UpdateOverallProgressBar(0);
            }

            foreach (string myKey in collViews.AllKeys)
            {
                foreach (string myValue in collViews.GetValues(myKey))
                {
                    bool bModified = false;
                    string strSQL = String.Empty;
                    string strViewXML = String.Empty;

                    if (!bIsSilent)
                    {
                        frmWizard.UpdateExecutingText(String.Format("Updating form: {0} in Powerview ID: {1}", myValue, myKey));
                    }

                    strSQL = RISKMASTERScripts.GetPowerViewsByName(Convert.ToInt32(myKey), myValue, DisplayDBUpgrade.g_iDSNID);

                    using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_ViewConnectString, strSQL))
                    {
                        while (rdr.Read())
                        {
                            strViewXML = rdr["VIEW_XML"].ToString();
                        }
                    }

                    if (String.IsNullOrEmpty(strViewXML))
                    {
                        continue;
                    }
                    else
                    {
                        dom.LoadXml(strViewXML);
                    }

                    switch (myValue)
                    {
                        case "eventdatedtext.xml":
                            #region eventdatedtext.xml
                            {
                                XmlElement xmlElement = (XmlElement)dom.SelectSingleNode("//group[@name='datedtext']");
                                if (xmlElement != null)
                                {
                                    xmlElement.SetAttribute("name", "datetextgroup");
                                    bModified = true;
                                }
                                break;
                            }
                            #endregion
                        case "entitymaint.xml":
                            #region entitymaint.xml
                            {
                                XmlElement xmlElement = (XmlElement)dom.SelectSingleNode("//control[@name='EntityXOperatingAsGrid' and @hidenodes='|OperatingId|EntityId|Initial|']");
                                if (xmlElement != null)
                                {
                                    xmlElement.SetAttribute("hidenodes", "|OperatingId|EntityId|");
                                    bModified = true;
                                }
                                break;
                            }
                            #endregion
                        case "people.xml":
                            #region people.xml
                            {
                                XmlElement xmlElement = (XmlElement)dom.SelectSingleNode("//control[@name='EntityXOperatingAsGrid' and @hidenodes='|OperatingId|EntityId|Initial|']");
                                if (xmlElement != null)
                                {
                                    xmlElement.SetAttribute("hidenodes", "|OperatingId|EntityId|");
                                    bModified = true;
                                }
                                break;
                            }
                            #endregion
                        case "event.xml":
                            #region event.xml
                            {
                                XmlElement xmlElement = (XmlElement)dom.SelectSingleNode("//control[@name='eventdescription' and @readonly='true']");
                                if (xmlElement != null)
                                {
                                    xmlElement.RemoveAttribute("readonly");
                                    bModified = true;
                                }
                                break;
                            }
                            #endregion
                        default:
                            break;
                    }

                    //Update the xml
                    if (bModified)
                    {
                        string sSQL = String.Format("UPDATE NET_VIEW_FORMS SET VIEW_XML = '{0}' WHERE VIEW_ID = {1} AND FORM_NAME = '{2}'", sSQLStringLiteral(dom.OuterXml), Convert.ToInt32(myKey), myValue);

                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_ViewConnectString, sSQL);
                    }
                }

                // akaushik5 Added for MITS 30290 Starts
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CREATEDYNAMICVIEWS"])
                    && ConfigurationManager.AppSettings["CREATEDYNAMICVIEWS"].ToLower().Equals("true"))
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DYNAMICVIEWS"]))
                    {
                        StringBuilder sInsertQuery = new StringBuilder();
                        List<string> dynamicViews = ConfigurationManager.AppSettings["DYNAMICVIEWS"].Split(',').ToList();
                        dynamicViews.ForEach(x =>
                        {
                            string sSql = string.Format("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='{1}' AND DATA_SOURCE_ID = {2}", Convert.ToInt32(myKey), string.Format("{0}d.xml", x), DisplayDBUpgrade.g_iDSNID);
                            int lCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_ViewConnectString, sSql);

                            if (lCount == 0)
                            {
                                sInsertQuery.Remove(0, sInsertQuery.Length);
                                string caption = string.Empty;
                                string viewXml = string.Empty;
                                string pageContent = string.Empty;
                                sSql = RISKMASTERScripts.GetDynamicPowerViews(DisplayDBUpgrade.g_iDSNID, Convert.ToInt32(myKey), string.Format("{0}.xml", x));
                                using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_ViewConnectString, sSql))
                                {
                                    while (rdr.Read())
                                    {
                                        sInsertQuery.Remove(0, sInsertQuery.Length);

                                        using (DbConnection oConnection = DbFactory.GetDbConnection(DisplayDBUpgrade.g_ViewConnectString))
                                        {
                                            oConnection.Open();
                                            using (DbCommand oCommand = oConnection.CreateCommand())
                                            {
                                                DbParameter objParam = null;

                                                sInsertQuery.AppendFormat("INSERT INTO NET_VIEW_FORMS ( DATA_SOURCE_ID, VIEW_ID, FORM_NAME, TOPLEVEL, CAPTION, VIEW_XML) VALUES ({0},{1},'{2}',1,'{3}',~VXML~)",
                                                    DisplayDBUpgrade.g_iDSNID,
                                                    rdr["VIEW_ID"],
                                                    sSQLStringLiteral(string.Format("{0}d.xml", x)), //param 2
                                                    sSQLStringLiteral(rdr["CAPTION"].ToString())); //param 3
                                                oCommand.Parameters.Clear();
                                                objParam = oCommand.CreateParameter();
                                                objParam.Direction = ParameterDirection.Input;
                                                objParam.Value = sSQLStringLiteral(rdr["VIEW_XML"].ToString());
                                                objParam.ParameterName = "VXML";
                                                objParam.SourceColumn = "VIEW_XML";
                                                oCommand.Parameters.Add(objParam);
                                                oCommand.CommandText = sInsertQuery.ToString();
                                                oCommand.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        );
                    }
                }
                // akaushik5 Added for MITS 30290 Ends
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Powerviews Finished - " + DateTime.Now + "\r\n");

            return true;
        }

        /// <summary>
        /// update wpa diary
        /// </summary>
        public static void UpdateWPADiary(bool bIsSilent)
        {
            if (!bIsSilent)
            {
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.UpdateExecutingText(String.Empty);
                //frmWizard.UpdateScriptName("Updating WPA_DIARY_ENTRY.ATTACH_TABLE column.....");
                frmWizard.UpdateScriptName("Updating WPA Diaries.....");
            }
            else
            {
                //Console.WriteLine("Updating WPA Diaries");
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating WPA Diaries Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            try
            {
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateDiary());
            }
            catch(Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating WPA Diaries - " + ex.Message);
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating WPA Diaries Finished - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
        }

        /// <summary>
        /// refresh BES views for SQL Server
        /// </summary>
        public static void RefreshSQLServerBESViews(bool bIsSilent)
        {
            int iErrorCnt = 0;
            string sSqlQuery = String.Empty;

            if (!bIsSilent)
            {
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.UpdateExecutingText(String.Empty);
                frmWizard.UpdateScriptName("Refreshing BES Views.....");
            }
            else
            {
                //Console.WriteLine("Refreshing BES Views");
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Refreshing BES Views Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            using (DbReader rdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.GetBESTables()))
            {
                while (rdr.Read())
                {
                    string strTableSchema = rdr["TABLE_SCHEMA"].ToString();
                    string strTableName = rdr["TABLE_NAME"].ToString();
                    string strTableType = rdr["TABLE_TYPE"].ToString();

                    if (strTableType.ToUpper() == "BASE TABLE")
                    {
                        sSqlQuery = RISKMASTERScripts.GrantPermsToBESView(strTableSchema, strTableName);

                        try
                        {
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSqlQuery);
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating - " + strTableSchema + "." + strTableName + " - " + ex.Message);
                            iErrorCnt++;
                        }
                    }
                }
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Refreshing BES views Finished - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            if (iErrorCnt > 0)
            {
                string sErrorMessage = iErrorCnt.ToString() + " BES View(s) did not update properly...<br />Please contact RISKMASTER support.";
                if (!bIsSilent)
                {
                    frmWizard.ErrorDisplay(sErrorMessage, "error", eWizardButtonState.False, String.Empty);
                }
                else
                {
                    Console.Error.WriteLine("Error: " + sErrorMessage);
                }

                DisplayDBUpgrade.bStop = true;
            }
        }

        /// <summary>
        /// This function will parse Alter and sp_rename statement
        /// and update HIST_TRACK_DICTIONARY_TABLE accordingly.
        /// </summary>
        /// <param name="sSQL">sSQL Statement</param>
        private static void UpdateHistTrackDictionary(string sSQL)
        {
            string sTableName = string.Empty;
            string sOperation = string.Empty;
            string sRestQuery = string.Empty;
            string sColName = string.Empty;
            string sDataType = string.Empty;
            string sRegExp =string.Empty;
            int iRecordAffected = 0;
            int iTableId = 0;
            int iSize = 0; 
            int? iPrecision = null;
            int? iScale = null;

            sSQL = sSQL.Trim().ToUpper();
            if (ValidateDataType(sSQL))
            {
            //parse alter statement and get Table name, operation and rest part of the query
            Regex regex = new Regex(@"ALTER[\s]+TABLE[\s]+(?<TABLENAME>[A-Za-z0-9_]*)[\s]+"+
                                    @"(?<OPERATION>[A-Za-z0-9_]*)[\s]*(?<RESTPART>.*)", RegexOptions.IgnoreCase);
            Match match = regex.Match(sSQL);
            if (match.Success)
            {
                sTableName = match.Groups["TABLENAME"].Value.Trim();
                sOperation = match.Groups["OPERATION"].Value.Trim();
                sRestQuery = match.Groups["RESTPART"].Value.Trim();
                iTableId = GetHistTrackTableID(sTableName);
                if (iTableId > 0)
                {
                    switch (sOperation)
                    {
                        case "ADD":
                            if (sRestQuery.Contains("CONSTRAINT "))
                            {   //insert into sync table
                                InsertHistTrackSync(sTableName, sSQL);
                            }
                            else
                            {
                                sRestQuery.TrimStart('(').TrimEnd(')').Trim();
                                string[] sArray = sRestQuery.Split(',');
                                for (int iCount = 0; iCount < sArray.Length; iCount++)
                                {
                                    string sRquery = sArray[iCount];
                                    sRegExp = @"^(?<COLNAME>[a-zA-z0-9_]*)[\s]+(?<DATATYPE>[a-zA-z0-9_]+)[\s]*" +
                                              @"(?<SIZE>[\(][\s]*[0-9]*[\s]*,*[0-9]*[\s]*[\)])*";
                                    GetMetaDataOfColumn(sRquery, sRegExp, ref sColName, ref sDataType, ref iSize, ref iPrecision, ref iScale);

                                    //Add new record in Hist_Track_Dictionary table
                                    AddHistTrackDictionaryLine(sColName, sDataType, iTableId, iSize, iPrecision, iScale);
                                }
                                //insert into sync table
                                InsertHistTrackSync(sTableName, sSQL);
                            }
                            break;
                        case "ALTER":
                            sRegExp = @"COLUMN[\s]*(?<COLNAME>[a-zA-z0-9_]*)[\s]+(?<DATATYPE>[a-zA-z0-9_]+)[\s]*" +
                                      @"(?<SIZE>[\(][\s]*[0-9]*[\s]*,*[0-9]*[\s]*[\)]*)*";
                            GetMetaDataOfColumn(sRestQuery, sRegExp, ref sColName, ref sDataType, ref iSize, ref iPrecision, ref iScale);
                            //update existing record in Hist_Track_Dictionary table
                            iRecordAffected = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackDictionary(sColName, sDataType, iTableId, iSize, iPrecision, iScale));
                            if (iRecordAffected > 0 && IsTrackingEnabledClmn(sColName,iTableId))
                            {
                                //set UPDATED_FLAG of the table in Hist_Track_Table
                                //so whenever History Tracking Executables runs , changetable get reconfigured 
                                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackTableFlag(iTableId));
                            }

                            //insert into sync table
                            InsertHistTrackSync(sTableName, sSQL);
                            break;
                            
                        case "MODIFY":
                            sRegExp = @"\([\s]*(?<COLNAME>[a-zA-z0-9_]*)[\s]+(?<DATATYPE>[a-zA-z0-9_]+)[\s]*" +
                                      @"(?<SIZE>[\(][\s]*[0-9]*[\s]*,*[0-9]*[\s]*[\)])*";
                            GetMetaDataOfColumn(sRestQuery, sRegExp, ref sColName, ref sDataType, ref iSize, ref iPrecision, ref iScale);
                            //update existing record in Hist_Track_Dictionary table
                            iRecordAffected = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackDictionary(sColName, sDataType, iTableId, iSize, iPrecision, iScale));
                            if (iRecordAffected > 0 && IsTrackingEnabledClmn(sColName, iTableId))
                            {   //set UPDATED_FLAG of the table in Hist_Track_Table
                                //so whenever History Tracking Executables runs , changetable get reconfigured 
                                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackTableFlag(iTableId));
                                
                            }
                            //insert into sync table
                            InsertHistTrackSync(sTableName, sSQL);
                            break;
                        case "RENAME":
                            regex = new Regex(@"COLUMN[\s]*(?<FROMCOLNAME>[a-zA-z0-9_]*)[\s]+TO[\s]+(?<TOCOLNAME>[a-zA-z0-9_]+)", RegexOptions.IgnoreCase);
                            match = regex.Match(sRestQuery);
                            if (match.Success)
                            {
                                sColName = match.Groups["FROMCOLNAME"].Value.Trim();
                                string sToColName = match.Groups["TOCOLNAME"].Value.Trim();
                                //update existing record in Hist_Track_Dictionary table
                                iRecordAffected = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackDictionary(sColName, sToColName, iTableId));
                                if (iRecordAffected > 0 && IsTrackingEnabledClmn(sToColName, iTableId))
                                {
                                    //set UPDATED_FLAG of the table in Hist_Track_Table
                                    //so whenever History Tracking Executables runs , changetable get reconfigured 
                                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackTableFlag(iTableId));
                                }
                                //insert into sync table
                                InsertHistTrackSync(sTableName, sSQL);
                            }
                            break;
                        case "DROP":
                            if (sRestQuery.Contains("CONSTRAINT "))
                            {
                                //insert into sync table
                                InsertHistTrackSync(sTableName, sSQL);
                            }
                            else
                            {
                                regex = new Regex(@"COLUMN[\s]*(?<COLNAME>[a-zA-z0-9_]*)[\s]*", RegexOptions.IgnoreCase);
                                match = regex.Match(sRestQuery);
                                if (match.Success)
                                {
                                    sColName = match.Groups["COLNAME"].Value.Trim();
                                    //Delete record from Hist_Track_Dictionary table
                                    iRecordAffected = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.DeleteHistTrackDictionaryLine(sColName, iTableId));
                                    if (iRecordAffected > 0 && IsTrackingEnabledClmn(sColName, iTableId))
                                    {
                                        //Delete entry of column from Hist_Track_Columns table
                                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.DeleteHisttrackClmnLine(sColName, iTableId));
                                        //set UPDATED_FLAG of the table in Hist_Track_Table table
                                        //so whenever History Tracking Executables runs , changetable get reconfigured
                                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackTableFlag(iTableId));
                                    }

                                }
                            }
                            break;

                    }
                }
                               
            }

            //parsing sp_rename statement
            regex = new Regex(@"SP_RENAME[\s]+['](?<TABLENAME>[a-zA-Z0-9_]+)[\.]"+
                              @"(?<FROMCOLNAME>[a-zA-Z0-9_]+)['][\s]*[,][\s]*['](?<TOCOLNAME>[a-zA-Z0-9_]+)[']", RegexOptions.IgnoreCase);
            match = regex.Match(sSQL);
            if (match.Success)
            {
                sTableName = match.Groups["TABLENAME"].Value;
                iTableId = GetHistTrackTableID(sTableName);
                if (iTableId > 0)
                {
                    sColName = match.Groups["FROMCOLNAME"].Value;
                    string sToColName = match.Groups["TOCOLNAME"].Value;
                    //update existing record in Hist_Track_Dictionary table
                    iRecordAffected = ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackDictionary(sColName, sToColName, iTableId));
                    if (iRecordAffected > 0 && IsTrackingEnabledClmn(sToColName, iTableId))
                    {
                        //set UPDATED_FLAG of the table in Hist_Track_Dictionary table
                        //so whenever History Tracking Executables runs , changetable get reconfigured
                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.UpdateHistTrackTableFlag(iTableId));
                    }
                }

                //insert into sync table
                InsertHistTrackSync(sTableName, sSQL);
            }

            //parsing DROP INDEX statement
            regex = new Regex(@"DROP[\s]+INDEX[\s]+[a-zA-Z0-9_]+[\s]+ON[\s]+(?<TABLENAME>[a-zA-Z0-9_]+)", RegexOptions.IgnoreCase);
            match = regex.Match(sSQL);
            if (match.Success)
            {
                sTableName = match.Groups["TABLENAME"].Value;
                iTableId = GetHistTrackTableID(sTableName);
                if (iTableId > 0)
                {
                    //insert into sync table
                    InsertHistTrackSync(sTableName, sSQL);
                }
            }

            //parsing CREATE UNIQUE INDEX statement
            regex = new Regex(@"CREATE[\s]+UNIQUE[\s]+INDEX[\s]+[a-zA-Z0-9_]+[\s]+ON[\s]+(?<TABLENAME>[a-zA-Z0-9_]+)", RegexOptions.IgnoreCase);
            match = regex.Match(sSQL);
            if (match.Success)
            {
                sTableName = match.Groups["TABLENAME"].Value;
                iTableId = GetHistTrackTableID(sTableName);
                if (iTableId > 0)
                {
                    //insert into sync table
                    InsertHistTrackSync(sTableName, sSQL);
                    }
                }
            }

        }

        /// <summary>
        /// This function fetch metadata of column 
        /// by parsing the rest part of the query
        /// </summary>
        /// <param name="sRquery">rest part of the query</param>
        /// <param name="strRegExpr">regular expression</param>
        /// <param name="sColName">Column Name</param>
        /// <param name="sDataType">Data type of Column</param>
        /// <param name="iSize">size</param>
        /// <param name="iPrecision">Precesion</param>
        /// <param name="iScale">Scale</param>
        private static void GetMetaDataOfColumn(string sRquery, string strRegExpr, ref string sColName,ref string sDataType, ref int iSize, ref int? iPrecision, ref int? iScale)
        {

            string sSize = string.Empty;
            iSize = 0;
            iPrecision = null;
            iScale = null;
            Regex regex = new Regex(strRegExpr, RegexOptions.IgnoreCase);
            Match match = regex.Match(sRquery);
            if (match.Success)
            {
                sColName = match.Groups["COLNAME"].Value.Trim();
                sDataType = match.Groups["DATATYPE"].Value.Trim();
                sSize = match.Groups["SIZE"].Value.Trim();

                //removing the ( ) from size
                sSize = sSize.TrimStart('(').TrimEnd(')');
                
                if (!string.IsNullOrEmpty(sSize))
                {
                    //if datatype is VARCHAR OR VARCHAR2
                    if (sDataType.IndexOf("VARCHAR") >= 0)
                    {
                        iSize = Convert.ToInt32(sSize);
                    }
                    //In case of NUMBER like NUMBER(5,2)or NUMBER(10)
                    else
                    {

                        if (sSize.IndexOf(',') > 0)
                        {
                            regex = new Regex(@"^(?<PRECISION>[\d]*)[\s]*[,][\s]*(?<SCALE>[\d]*)", RegexOptions.IgnoreCase);
                            match = regex.Match(sSize);
                            iPrecision = Convert.ToInt32(match.Groups["PRECISION"].Value.Trim());
                            iScale = Convert.ToInt32(match.Groups["SCALE"].Value.Trim());
                        }
                        else
                        {
                            iPrecision = Convert.ToInt32(sSize);
                            iScale = 0;
                        }
                    }

                }
            }
        }

        /// <summary>
        /// This function will add new record in HIST_TRACK_DICTIONARY 
        /// table
        /// </summary>
        /// <param name="sColName">Column Name</param>
        /// <param name="sDataType">Data type</param>
        /// <param name="iTableId">Table id</param>
        /// <param name="iSize">Size</param>
        /// <param name="iPrecision">Precision</param>
        /// <param name="iScale">Scale</param>
        /// <returns>if record added reurns 1 else 0 </returns>
        private static int AddHistTrackDictionaryLine( string sColName, string sDataType,int iTableId, int iSize, int? iPrecision, int? iScale)
        {
            int iRowCnt = 0;
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;
            StringBuilder sbSQL = new StringBuilder();
            Dictionary<string, object> dictFields = new Dictionary<string, object>();

            //test for whether a row already exists for specified criteria 
            
            sbSQL.Append("SELECT COLUMN_ID FROM HIST_TRACK_DICTIONARY ");
            sbSQL.AppendFormat(" WHERE TABLE_ID ={0} AND COLUMN_NAME ='{1}'", iTableId, sColName);
            try
            {
                try
                {
                    iRowCnt = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sbSQL.ToString()));
                }
                catch (Exception exc)
                {
                    iRowCnt = 0;
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + exc.Message);

                }

                if (iRowCnt != 0)//row exists return value
                {
                    return iRowCnt;
                }

                
                if(iDBMake == RiskmasterDBTypes.DBMS_IS_ORACLE)
                    dictFields.Add("COLUMN_ID", "HIST_TRACK_DICTIONARY_SEQ.NEXTVAL");
                dictFields.Add("TABLE_ID", iTableId);  
                dictFields.Add("COLUMN_NAME", sColName);           
                dictFields.Add("DATATYPE", sDataType);            
                dictFields.Add("USER_PROMPT", sColName);   
                dictFields.Add("COLUMN_SIZE", iSize);
                if (iPrecision != null)
                    dictFields.Add("NUMERIC_PRECISION", iPrecision);
                if (iScale != null)
                    dictFields.Add("NUMERIC_SCALE", iScale);


                strSQLquery = RISKMASTERScripts.InsertNewRow("HIST_TRACK_DICTIONARY", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

                if (String.IsNullOrEmpty(strSQLquery))
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.AddHistTrackDictionaryLine] " + strErrDesc + " The statement has been terminated.");
                }
                else
                {
                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
                }

                return 1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.ProcessPseudoStatement]" + ex.Message);

                return 0;
            }
        }

        /// <summary>
        /// Get table id from HIST_TRACK_TABLE
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <returns>Table Id</returns>
        private static int GetHistTrackTableID(string sTableName)
        {
            return ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, RISKMASTERScripts.GetHistTrackTableID(sTableName));
        }

        /// <summary>
        /// Check whether tracking is enabled 
        /// on the column or not
        /// </summary>
        /// <param name="sColname">Column name</param>
        /// <param name="iTableId">Table Id</param>
        /// <returns>True/False</returns>
        private static bool IsTrackingEnabledClmn(string sColname, int iTableId)
        {
            int iRowCount = 0;
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT HC.COLUMN_ID FROM HIST_TRACK_COLUMNS HC INNER JOIN HIST_TRACK_DICTIONARY HD ");
            sbSql.Append(" ON HC.COLUMN_ID = HD.COLUMN_ID WHERE ");
            sbSql.AppendFormat(" HD.TABLE_ID ={0} AND HD.COLUMN_NAME ='{1}' ", iTableId, sColname);

            iRowCount = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sbSql.ToString()));
            if (iRowCount > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// This function insert record into 
        /// HIST_TRACK_SYNC table
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <param name="sSql">sql statement</param>
        /// <returns></returns>
        private static int InsertHistTrackSync(string sTableName, string sSql)
        {
            int iRowId = 0;
            string strSQLquery = String.Empty;
            string strErrDesc = String.Empty;
            Dictionary<string, object> dictFields = new Dictionary<string, object>();

            iRowId = GetMaxRecordID("HIST_TRACK_SYNC", "ROW_ID");


            dictFields.Add("ROW_ID", iRowId);
            dictFields.Add("TABLE_NAME", sTableName);
            dictFields.Add("SQL_STMNT", sSql);


            strSQLquery = RISKMASTERScripts.InsertNewRow("HIST_TRACK_SYNC", dictFields, ref strErrDesc, DisplayDBUpgrade.g_sConnectString);

            if (String.IsNullOrEmpty(strSQLquery))
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + iLineCount + " - [UpdateFunctions.InsertHistTrackSync] " + strErrDesc + " The statement has been terminated.");
            }
            else
            {
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strSQLquery);
            }

            return iRowId;
        }

        /// <summary>
        /// This function will execute each sql statement in HIST_TRACK_SYNC
        /// table on History tracking database.
        /// </summary>
        /// <param name="strDBConnString">connction string of history tracking db</param>
        /// <param name="bIsSilent">mode</param>
        public static void ExecuteHistTrackSyncSQL(string strDBConnString, bool bIsSilent)
        {
            int iSqlCount = -1;
            int iProgressCount = 0;
            int iRowId =0;
            int iHistTrackDbId = 0;
            string sTableName = string.Empty;
            string sNewTableName = string.Empty;
            string sSQL = String.Empty;
            string sPattern = string.Empty;
            string sReplacement = string.Empty;
            bool bFound = false;
            Regex regex = null;
            Match match = null;
            iHistTrackDbId = GetDatabaseID();


            
            sSQL = "SELECT COUNT(*) FROM HIST_TRACK_SYNC";
            iSqlCount = ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);

            if (!bIsSilent)
            {
                //set status bar maximum on process form
                frmWizard.SetCurrentProgressBarProperties(iSqlCount);
            }
            sSQL = "SELECT ROW_ID,TABLE_NAME,SQL_STMNT FROM HIST_TRACK_SYNC";
            using (DbReader objReader = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_sConnectString, sSQL))
            {
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        bFound = false;
                        iProgressCount++;
                        iRowId = objReader.GetInt32("ROW_ID");
                        sTableName = objReader.GetString("TABLE_NAME").ToUpper();
                        sSQL = objReader.GetString("SQL_STMNT").ToUpper();
                        sNewTableName = sTableName + "_HIST_" + iHistTrackDbId.ToString();


                        //alter statement
                        if (!bFound)
                        {
                            sPattern = @"ALTER[\s]+TABLE[\s]+(?<TABLENAME>[A-Za-z0-9_]*)[\s]+(?<OPERATION>[A-Za-z0-9_]*)[\s]*(?<RESTPART>.*)";
                            sReplacement = @"ALTER TABLE " + sNewTableName + @" ${OPERATION}  ${RESTPART}";
                            regex = new Regex(sPattern, RegexOptions.IgnoreCase);
                            match = regex.Match(sSQL);
                            if (match.Success)
                            {
                                bFound = true;
                                sSQL = Regex.Replace(sSQL, sPattern, sReplacement);

                            }
                        }

                        //sp_rename statement
                        if (!bFound)
                        {  
                            sPattern = @"SP_RENAME[\s]+['](?<TABLENAME>[a-zA-Z0-9_]+)[\.](?<FROMCOLNAME>[a-zA-Z0-9_]+)['][\s]*[,][\s]*['](?<TOCOLNAME>[a-zA-Z0-9_]+)[']";
                            sReplacement = @"SP_RENAME '" + sNewTableName + @".${FROMCOLNAME}', '${TOCOLNAME}'";
                            regex = new Regex(sPattern, RegexOptions.IgnoreCase);
                            match = regex.Match(sSQL);
                            if (match.Success)
                            {
                                bFound = true;
                                sSQL = Regex.Replace(sSQL, sPattern, sReplacement);

                            }
                        }

                        
                        //CREATE UNIQUE INDEX statement
                        if (!bFound)
                        {
                            sPattern = @"CREATE[\s]+UNIQUE[\s]+INDEX[\s]+(?<INDEXNAME>[a-zA-Z0-9_]+)[\s]+ON[\s]+(?<TABLENAME>[a-zA-Z0-9_]+)(?<RESTPART>.*)";
                            sReplacement = @"CREATE UNIQUE INDEX ${INDEXNAME} ON " + sNewTableName + @"${RESTPART}";
                            regex = new Regex(sPattern, RegexOptions.IgnoreCase);
                            match = regex.Match(sSQL);
                            if (match.Success)
                            {
                                bFound = true;
                                sSQL = Regex.Replace(sSQL, sPattern, sReplacement);

                            }
                        }


                        //DROP INDEX statement
                        if (!bFound)
                        {
                            sPattern = @"DROP[\s]+INDEX[\s]+(?<INDEXNAME>[a-zA-Z0-9_]+)[\s]+ON[\s]+(?<TABLENAME>[a-zA-Z0-9_]+)";
                            sReplacement = @"DROP INDEX  ${INDEXNAME} ON " + sNewTableName;
                            regex = new Regex(sPattern, RegexOptions.IgnoreCase);
                            match = regex.Match(sSQL);
                            if (match.Success)
                            {
                                bFound = true;
                                sSQL = Regex.Replace(sSQL, sPattern, sReplacement);

                            }
                        }

                        if (!bIsSilent)
                        {
                            frmWizard.UpdateExecutingText(sSQL);
                        }
                        try
                        {
                            // run sql query on History Tracking database and delete record from HIST_TRACK_SYNC table 
                            ADONetDbAccess.ExecuteNonQuery(strDBConnString, sSQL);
                            sSQL = string.Format("DELETE FROM HIST_TRACK_SYNC WHERE ROW_ID = {0}", iRowId);
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);

                        }
                        catch (Exception ex)
                        {
                            sSQL = string.Format("DELETE FROM HIST_TRACK_SYNC WHERE ROW_ID = {0}", iRowId);
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), ex.Message);
                        }
                        if (!bIsSilent)
                        {
                            frmWizard.UpdateCurrentProgressBar(iProgressCount);
                        }
                    }//end while
                }
            }//end using
        }

        private static bool ValidateDataType(string sSql)
        {
            bool bValidDataType = true;;
            string[] sArrDataType = new string[] {" VARCHAR(MAX)", " CLOB"," TEXT"," BFILE"," BLOB"," LONG"," NCLOB"," ROWID"," UROWID" };
            for (int iCounter = 0; iCounter < sArrDataType.Length; iCounter++)
            {
                if(sSql.Contains(sArrDataType[iCounter]))
                {
                    bValidDataType = false;
                    break;

                }
            }

            return bValidDataType;
        }
        /// <summary>
        /// This function wil retrieve database id.
        /// </summary>
        /// <returns></returns>
        private static int GetDatabaseID()
        {
            string sDbName = string.Empty;
            string sSql = string.Empty;
            int iDbId = 0;
            int iPos1;
            int iPos2;
            DbConnection objDbConn = null;
            try
            {

                objDbConn = DbFactory.GetDbConnection(DisplayDBUpgrade.g_sConnectString);
                objDbConn.Open();
                if ((objDbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + objDbConn.Database + "'";
                    iDbId = objDbConn.ExecuteInt(sSql);
                    
                }
                else
                {
                    iPos1 = DisplayDBUpgrade.g_sConnectString.IndexOf("UID=");
                    iPos2 = DisplayDBUpgrade.g_sConnectString.IndexOf("PWD=");
                    sDbName = DisplayDBUpgrade.g_sConnectString.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                    sSql = "SELECT USER_ID FROM USER_USERS WHERE USERNAME='" + sDbName.Trim().ToUpper() + "'";
                    iDbId = objDbConn.ExecuteInt(sSql);
                }

                objDbConn.Close();
                return iDbId;
            }
            finally
            {
                if (objDbConn != null)
                    objDbConn.Close();
            }


        }
        private static bool InsertAggregateLimitforGroup(int lCodeID,string sTempdate,string sUser)
        {
            int iLimitID = 0;
            int iHistLimitID = 0;
            string sSQL = string.Empty;
            sSQL = string.Format("SELECT CLAIM_X_POL_DED_AGG_LIMIT_ID FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0} " , lCodeID);
            iLimitID = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
            if (iLimitID <= 0)
            {
                iLimitID = DisplayDBUpgrade.GetNextUID("CLAIM_X_POL_DED_AGG_LIMIT");
                if (iLimitID <= 0)
                {
                    string strMessage = "-Error: Unable to assign next ID for CLAIM_X_POL_DED_AGG_LIMIT table";
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);
                    return false;
                }
                //insert the values into the codes text table
                sSQL = string.Format("INSERT INTO CLAIM_X_POL_DED_AGG_LIMIT (CLAIM_X_POL_DED_AGG_LIMIT_ID, COV_GROUP_CODE,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,ADDED_BY_USER,UPDATED_BY_USER) VALUES ({0},{1},'{2}','{2}','{3}','{3}')", iLimitID, lCodeID,sTempdate,sUser);

                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
                iHistLimitID = DisplayDBUpgrade.GetNextUID("CLAIM_X_POL_DED_AGG_LIMIT_HIST");
                if (iHistLimitID <= 0)
                {
                    string strMessage = "-Error: Unable to assign next ID for CLAIM_X_POL_DED_AGG_LIMIT_HIST table";
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), strMessage);
                    return false;
                }
                sSQL = string.Format("INSERT INTO CLAIM_X_POL_DED_AGG_LIMIT_HIST (DED_AGG_LIMIT_HIST_ID,CLAIM_X_POL_DED_AGG_LIMIT_ID, COV_GROUP_CODE,DATE_AGG_LIMIT_CHGD,AGG_LIMIT_CHGD_BY) VALUES ({0},{1},{2},'{3}','{4}')", iHistLimitID, iLimitID, lCodeID,sTempdate,sUser);
                ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL);
                //stamp codes for recache
                UpdateGlossaryTimeStamp("CLAIM_X_POL_DED_AGG_LIMIT_HIST");
                

            }

            return true;
       
        
        }
        private static bool InsertCodeDescription(int lCodeId, int iLNSCode, string sShortCode, string sDescription)
        {
            int iCount = 0;
            
            string sSQL = string.Empty;
            sSQL = String.Format("SELECT COUNT(*) FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID = {0} AND LANGUAGE_CODE = {1} ", lCodeId, iLNSCode);
            iCount = Convert.ToInt32(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, sSQL));
            if (iCount <= 0)
            {
                sSQL = string.Format("INSERT INTO COVERAGE_GROUP_TEXT VALUES({0},'{1}',{2},'{3}')", lCodeId,iLNSCode, sShortCode, sDescription);

                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
                      //stamp codes for recache
                UpdateGlossaryTimeStamp("COVERAGE_GROUP_TEXT");

              
            }
            return true;
        }


        //asharma326 JIRA# 6422 Starts 
        /// <summary>
        /// Add HTML Text default settings if setting not avail in DB.
        /// </summary>
        internal static void UpdateSessionCustomizeTable()
        {
            string s_Content=string.Empty,s_InsertContent = string.Empty;
            XmlDocument xdoc = new XmlDocument();
            try
            {
                xdoc.LoadXml(Convert.ToString(ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_SessionConnectString, @"SELECT CONTENT FROM CUSTOMIZE WHERE FILENAME = 'customize_settings'")));
                if (xdoc.SelectSingleNode("//customize_settings/RMAdminSettings/TextAreaSize/HtmlText") == null)
                {
                    var node = xdoc.SelectSingleNode("//customize_settings/RMAdminSettings/TextAreaSize");
                    XmlDocumentFragment xmlDocFrag = xdoc.CreateDocumentFragment();
                    xmlDocFrag.InnerXml = "<HtmlText><Width default=\"30\">30</Width><Height default=\"5\">5</Height></HtmlText>";
                    node.AppendChild(xmlDocFrag);
                    s_InsertContent = xdoc.InnerXml.ToString();
                    ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_SessionConnectString, @"UPDATE CUSTOMIZE SET CONTENT = '" + s_InsertContent + "'  WHERE FILENAME = 'customize_settings'");
                }
            }
            catch 
            { 
                //no need to catch internal error.
            }
        }
        //asharma326 JIRA# 6422 Ends
        
        
    }
}
