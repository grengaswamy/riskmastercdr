-- =============================================
-- Author:		CSC
-- Create date: 05/24/2012
-- Description:	Copies all the commetns from CLAIM_X_LITIATION table to COMMENTS_TEXT
-- Comments records over to the Comments_Text table
-- =============================================
[GO_DELIMITER]
DECLARE @nvSQL AS nVARCHAR(max) 
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'USP_COPY_LITIGATION_COMMENTS' AND TYPE = 'P')
	BEGIN
		DROP PROCEDURE USP_COPY_LITIGATION_COMMENTS
	END
		SET @nvSQL = 'CREATE PROCEDURE USP_COPY_LITIGATION_COMMENTS 
					  AS
					  BEGIN
						-- SET NOCOUNT ON added to prevent extra result sets from
						-- interfering with SELECT statements.
						SET NOCOUNT ON;

						--INSERT EVENT RECORDS INTO THE COMMENTS_TEXT table
						DECLARE @LITIGATION_ROW_ID INT,
								@COMMENT_ID INT,
								@COMMENTS VARCHAR(MAX),
								@HTMLCOMMENTS VARCHAR(MAX),
								@COUNT INT
								
						SELECT @COUNT=COUNT(*) FROM COMMENTS_TEXT WHERE ATTACH_TABLE = ''CLAIM_X_LITIGATION''
						IF (@COUNT > 1)
							RETURN
	
						--Declare the Event Cursor
						DECLARE curTables CURSOR FOR 
							  SELECT LITIGATION_ROW_ID,COMMENTS,HTMLCOMMENTS FROM CLAIM_X_LITIGATION WHERE COMMENTS IS NOT NULL
						--open the cursor
						OPEN curTables

						--loop through the cursor lines
						FETCH NEXT FROM curTables INTO @LITIGATION_ROW_ID, @COMMENTS, @HTMLCOMMENTS
						WHILE @@FETCH_STATUS = 0
						BEGIN
							SELECT @COMMENT_ID = ISNULL(MAX(COMMENT_ID), 0) FROM COMMENTS_TEXT ct
		
							INSERT INTO COMMENTS_TEXT
	 						(
 								COMMENT_ID,
 								ATTACH_RECORDID,
 								ATTACH_TABLE,
 								COMMENTS,
 								HTMLCOMMENTS
	 						)
	 						VALUES
	 						(
 								@COMMENT_ID + 1,
 								@LITIGATION_ROW_ID,
 								''CLAIM_X_LITIGATION'',
 								@COMMENTS,
 								@HTMLCOMMENTS
	 						)

	 						FETCH NEXT FROM curTables INTO @LITIGATION_ROW_ID, @COMMENTS, @HTMLCOMMENTS
						END

						--clean up
						CLOSE curTables
						DEALLOCATE curTables

						SELECT @COMMENT_ID = MAX(COMMENT_ID) FROM COMMENTS_TEXT ct

						--UPDATE THE GLOSSARY TABLE WITH THE NEXT_UNIQUE_ID
						UPDATE GLOSSARY
						SET NEXT_UNIQUE_ID = (@COMMENT_ID + 1)
						WHERE SYSTEM_TABLE_NAME = ''COMMENTS_TEXT''
	
					END' 
					
		EXECUTE sp_executesql @nvSQL 
[EXECUTE_SP]		
GO
