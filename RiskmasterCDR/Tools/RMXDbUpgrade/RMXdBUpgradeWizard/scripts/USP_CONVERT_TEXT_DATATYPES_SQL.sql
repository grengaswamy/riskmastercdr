-- =============================================
-- Author:		CSC
-- Create date: 12/09/2009
-- Description:	Converts all text data types
-- to varchar(MAX) for compliance with SQL Server 2005
-- and above platforms
-- =============================================
CREATE PROCEDURE USP_CONVERT_TEXT_DATATYPES_SQL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--DECLARE REQUIRED VARIABLES FOR PROCESSING DYNAMIC SQL
	DECLARE @TableName varchar(255),
			@ColumnName VARCHAR(255),
			@ALTER_TABLE_SQL NVARCHAR(MAX)
	

    SELECT OBJECT_NAME(c.OBJECT_ID) TableName, c.name ColumnName
	INTO #TABLE_DATATYPE_CONVERSIONS
	FROM sys.columns AS c
	JOIN sys.types AS t ON c.user_type_id=t.user_type_id
	WHERE t.name = 'text' --you can change text to other datatypes
	ORDER BY TableName;

	--DECLARE THE CURSOR FOR PROCESSING TABLE DATATYPE CHANGES 
	DECLARE curTables CURSOR FOR
	 SELECT TableName, ColumnName FROM #TABLE_DATATYPE_CONVERSIONS

	--open the cursor
	OPEN curTables

	--loop through the cursor lines
	FETCH NEXT FROM curTables INTO @TableName, @ColumnName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @ALTER_TABLE_SQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN ' + @ColumnName + ' varchar(MAX)';
		--PRINT @ALTER_TABLE_SQL;
		EXECUTE sp_executesql @ALTER_TABLE_SQL;
		
	 
	 FETCH NEXT FROM curTables INTO @TableName, @ColumnName
	END

	--clean up
	CLOSE curTables
	DEALLOCATE curTables
	
	--SELECT * FROM #TABLE_DATATYPE_CONVERSIONS;
	DROP TABLE #TABLE_DATATYPE_CONVERSIONS;
END
GO
