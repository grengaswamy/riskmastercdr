[GO_DELIMITER]
create or replace procedure USP_COPY_LITIGATION_COMMENTS 
AS 
BEGIN 
    DECLARE 
        LITIGATION_COMMENTID INTEGER;
        CT_CNT INTEGER;
        
        BEGIN
            SELECT count(*) INTO CT_CNT from comments_text where ATTACH_TABLE = 'CLAIM_X_LITIGATION';
            
            IF CT_CNT > 1 THEN
            	RETURN;
            END IF;
            
            BEGIN
                DECLARE
                  	CURSOR LITIGATION_COMMENT_C IS SELECT LITIGATION_ROW_ID, COMMENTS, HTMLCOMMENTS FROM CLAIM_X_LITIGATION WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
                BEGIN 
                    FOR LITIGATION_COMMENT_VAL IN LITIGATION_COMMENT_C 
                    LOOP 
                        SELECT NVL(MAX(COMMENT_ID), 1) INTO LITIGATION_COMMENTID FROM COMMENTS_TEXT WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
                        IF LITIGATION_COMMENTID != 0 THEN 
                               INSERT INTO COMMENTS_TEXT (COMMENT_ID, ATTACH_RECORDID, ATTACH_TABLE, COMMENTS, HTMLCOMMENTS) 
                                 VALUES (LITIGATION_COMMENTID + 1, LITIGATION_COMMENT_VAL.LITIGATION_ROW_ID, 'CLAIM_X_LITIGATION', LITIGATION_COMMENT_VAL.COMMENTS, LITIGATION_COMMENT_VAL.HTMLCOMMENTS);
                        END IF;
                    END LOOP;
                END;
                
                DECLARE 
                    MAXCOMMENT_ID INTEGER;
    
                BEGIN 
                    SELECT MAX(COMMENT_ID) INTO MAXCOMMENT_ID FROM COMMENTS_TEXT;
                    UPDATE GLOSSARY SET NEXT_UNIQUE_ID = (MAXCOMMENT_ID + 1) WHERE SYSTEM_TABLE_NAME = 'COMMENTS_TEXT';
                END;
                
                COMMIT;
            END;
            END;
END;
[EXECUTE_SP]
GO