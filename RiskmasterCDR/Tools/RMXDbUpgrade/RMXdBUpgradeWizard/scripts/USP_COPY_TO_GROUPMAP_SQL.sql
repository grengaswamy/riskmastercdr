
[GO_DELIMITER] 
DECLARE @spSQL AS nVARCHAR(max) 
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_COPY_TO_GROUPMAP')
BEGIN
	DROP PROCEDURE USP_COPY_TO_GROUPMAP
END
	SET @spSQL = 'CREATE PROCEDURE USP_COPY_TO_GROUPMAP
				  AS
					DECLARE @iCount INT
					DECLARE @iGroupId INT
					DECLARE @iUpdatedFlag INT
					DECLARE @iDeleted_fag INT
					DECLARE @iStatusFlag INT

					SET @iCount = 0
					SELECT @iCount = COUNT(*) FROM GROUP_MAP

					IF(@iCount = 0)
					BEGIN
						DECLARE OrgSecurityCursor CURSOR FOR SELECT GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG FROM ORG_SECURITY 
						OPEN OrgSecurityCursor
						FETCH OrgSecurityCursor INTO @iGroupId,@iUpdatedFlag,@iDeleted_fag,@iStatusFlag
	
						WHILE @@Fetch_Status = 0
						BEGIN
						    IF(@iUpdatedFlag = 0)
						     BEGIN
							  INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG) VALUES (@iGroupId,@iGroupId,-1,@iDeleted_fag,@iStatusFlag)
							 END
							ELSE
							 BEGIN 
							  INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG) VALUES (@iGroupId,@iGroupId,@iUpdatedFlag,@iDeleted_fag,@iStatusFlag)
							 END
							FETCH OrgSecurityCursor INTO @iGroupId,@iUpdatedFlag,@iDeleted_fag,@iStatusFlag
						END
						CLOSE OrgSecurityCursor
						DEALLOCATE OrgSecurityCursor
					END
				'
	EXECUTE sp_executesql @spSQL 
[EXECUTE_SP]	
GO


