[GO_DELIMITER]
create or replace
PROCEDURE USP_REFRESH_BES_ROLE (sBES_ROLE varchar2, sRM_USERID VARCHAR2,iConfRec INT,sStoredproc VARCHAR2,sNewUser VARCHAR2)  
AUTHID CURRENT_USER IS
BEGIN
	DECLARE
		iRoleCount INT;
		sSql VARCHAR(2000);
		TABLE_NAME_Cursor varchar(2000);
		sUserName VARCHAR(500);
 		sVIEW_TABLES VARCHAR(2000);    --pmittal5
   sStoredprocs varchar(5000);
   Index_Count INT;
	 sStoredprocName VARCHAR(5000);
	 sStoredprocRight VARCHAR(5000);
	 spLength INT;
	 PipeIndex INT;
		
	BEGIN
        --pmittal5 - Views for Funds_Auto are also created in case of Confidential Record
 		sVIEW_TABLES := 'CLAIM, CLAIMANT,EMPLOYEE, ENTITY, EVENT, FUNDS, PERSON_INVOLVED, RESERVE_CURRENT, RESERVE_HISTORY, ADJUST_DATED_TEXT, POLICY, POLICY_ENH,CLAIM_PRG_NOTE,';  
 		IF(iConfRec = -1) THEN   
 			sVIEW_TABLES := sVIEW_TABLES||'FUNDS_AUTO,';  
 		END IF;  
        --End - pmittal5
		DECLARE
			CURSOR Get_Table IS SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER=UPPER(sRM_USERID) AND IOT_TYPE IS NULL;

		BEGIN
                
			OPEN Get_Table;
				SELECT USERNAME INTO sUserName FROM ALL_USERS WHERE USERNAME=UPPER(sRM_USERID);
			
				LOOP
					FETCH Get_Table INTO TABLE_NAME_Cursor;
					EXIT WHEN Get_Table%NOTFOUND;
				
 					IF (INSTR(sVIEW_TABLES, TABLE_NAME_Cursor || ',') = 0 )  
					THEN
						sSql:='CREATE OR REPLACE PUBLIC SYNONYM '||TABLE_NAME_Cursor||' FOR '||sUserName||'.'||TABLE_NAME_Cursor;
                                              
						EXECUTE IMMEDIATE sSql;
					END IF;
sSql:='GRANT SELECT,INSERT,UPDATE,DELETE ON '||sRM_USERID||'.'||TABLE_NAME_Cursor|| ' TO '||sBES_ROLE; 
							EXECUTE IMMEDIATE sSql;
				END LOOP;
			CLOSE Get_Table;
      
     sStoredprocs := sStoredproc;
     PipeIndex := INSTR(sStoredprocs,'|');
	   WHILE PipeIndex <> 0
     LOOP
		  spLength := LENGTH(sStoredprocs);
		  sStoredprocName:=SUBSTR(sStoredprocs,1,PipeIndex-1);
		  sStoredprocRight:=SUBSTR(sStoredprocs,PipeIndex+1,spLength);
		  sStoredprocs:= REPLACE(sStoredprocs,sStoredprocs,sStoredprocRight);
		  SELECT COUNT(*) INTO Index_Count FROM all_objects where owner = UPPER(sUserName) and object_name=UPPER(sStoredprocName);
	    IF (Index_Count > 0)
		  THEN		 	 
		--Deb Changed from private to public
        --sSql:='CREATE OR REPLACE SYNONYM '||sNewUser||'.'||UPPER(sStoredprocName)||' FOR '||sUserName||'.'||UPPER(sStoredprocName);
		sSql:='CREATE OR REPLACE PUBLIC SYNONYM '||UPPER(sStoredprocName)||' FOR '||sUserName||'.'||UPPER(sStoredprocName);
        EXECUTE IMMEDIATE sSql;
         sSql:='GRANT EXECUTE ON '||sRM_USERID||'.'||UPPER(sStoredprocName)||' TO '|| sBES_ROLE; 
		  	EXECUTE IMMEDIATE sSql;
      END IF;
		  PipeIndex := INSTR(sStoredprocs,'|');
     END LOOP;
		END;
	END;
END;
GO