-- =============================================
-- Author:		CSC
-- Create date: 10/21/2009
-- Description:	Updates all null values for the 
-- FORM_ROW_ID in the CHECK_STOCK_FORMS table
-- =============================================
[GO_DELIMITER]
CREATE PROCEDURE USP_UPDATE_CHK_STOCK_FRMROW_ID
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	DECLARE @FORM_ROW_ID INT,
		@STOCK_ID INT,
		@IMAGE_ID INT,
		@FORM_TYPE INT,
		-- akaushik5 Added for MITS 37556 Starts
		@RECORDFOUND SMALLINT = 0
		-- akaushik5 Added for MITS 37556 Ends

	--Declare the CHECK_STOCK_FORMS Cursor
	DECLARE curTables CURSOR FOR 
		SELECT STOCK_ID, csf.IMAGE_IDX, csf.FORM_TYPE
		  FROM CHECK_STOCK_FORMS csf 
		WHERE csf.FORM_ROW_ID IS NULL OR csf.FORM_ROW_ID = 0

	--open the cursor
	OPEN curTables

	--loop through the cursor lines
	FETCH NEXT FROM curTables INTO @STOCK_ID, @IMAGE_ID, @FORM_TYPE
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @FORM_ROW_ID  = ISNULL(MAX(FORM_ROW_ID) + 1, 1)
		FROM CHECK_STOCK_FORMS csf

		UPDATE CHECK_STOCK_FORMS
		SET FORM_ROW_ID = @FORM_ROW_ID
		WHERE STOCK_ID = @STOCK_ID
		AND IMAGE_IDX = @IMAGE_ID
		AND FORM_TYPE = @FORM_TYPE
		-- akaushik5 Added for MITS 37556 Starts
		SET @RECORDFOUND = -1
		-- akaushik5 Added for MITS 37556 Ends
		

		FETCH NEXT FROM curTables INTO @STOCK_ID, @IMAGE_ID, @FORM_TYPE
	END

	--clean up
	CLOSE curTables
	DEALLOCATE curTables
	
	--INCREMENT THE VALUE OF THE NEXT_UNIQUE_ID IN THE GLOSSARY TABLE
		-- akaushik5 Added for MITS 37556 Starts
	IF @RECORDFOUND = -1
	BEGIN
		-- akaushik5 Added for MITS 37556 Ends
	SET @FORM_ROW_ID = @FORM_ROW_ID + 1
	
	UPDATE GLOSSARY
	SET NEXT_UNIQUE_ID = @FORM_ROW_ID
	WHERE SYSTEM_TABLE_NAME = 'CHECK_STOCK_FORMS'
	END
END
[EXECUTE_SP]
GO
