[GO_DELIMITER]
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
DECLARE @procSQL AS nVARCHAR(max)

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_DROP_USER_FIRST_TIME')
	BEGIN
		DROP PROCEDURE USP_DROP_USER_FIRST_TIME
	END

SET @procSQL = '
		CREATE PROCEDURE USP_DROP_USER_FIRST_TIME @iGroupId INT, @SQLUSER VARCHAR(100), @SQLUSER_NEW VARCHAR(100),@out_err varchar(500) OUTPUT
		AS

		BEGIN
			DECLARE @SCHEMA_ID INT
			DECLARE @VIEWS_COUNT INT
			DECLARE @VIEW_NAME VARCHAR(25)
			DECLARE @nvSql VARCHAR(1000)

			SELECT @SCHEMA_ID=SCHEMA_ID FROM SYS.SCHEMAS WHERE NAME =@SQLUSER
			
			IF @SCHEMA_ID is not NULL
				BEGIN
					SET @VIEWS_COUNT=(SELECT COUNT(*) FROM SYS.OBJECTS WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID)
					
					IF @VIEWS_COUNT<>0
						BEGIN
						
							DECLARE TableName CURSOR FOR SELECT NAME FROM SYS.OBJECTS 
											WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID
							OPEN TableName
							FETCH TableName INTO @VIEW_NAME
							WHILE @@Fetch_Status = 0
							BEGIN
								set @nvSql=''Drop View ['' + @SQLUSER + ''].'' + @VIEW_NAME

								Exec sp_executesql @nvSql

								FETCH TableName INTO @VIEW_NAME
							END
							CLOSE TableName
							DEALLOCATE TableName
						END
			
					Set @nvSql = ''sp_dropuser ['' + @SQLUSER + '']''
					Exec sp_executesql @nvSql
					
					Set @nvSql = ''sp_droplogin ['' + @SQLUSER + '']'' 
					Exec sp_executesql @nvSql
				END
			ELSE
				BEGIN
					SELECT @SCHEMA_ID=SCHEMA_ID FROM SYS.SCHEMAS WHERE NAME =@SQLUSER_NEW
					
					IF @SCHEMA_ID is not NULL
						BEGIN
							SET @VIEWS_COUNT=(SELECT COUNT(*) FROM SYS.OBJECTS WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID)
							
							IF @VIEWS_COUNT<>0
								BEGIN
									
						
							DECLARE TableName CURSOR FOR SELECT NAME FROM SYS.OBJECTS 
														WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID
							OPEN TableName
							FETCH TableName INTO @VIEW_NAME
							WHILE @@Fetch_Status = 0
							BEGIN
								set @nvSql=''Drop View ['' + @SQLUSER_NEW + ''].'' + @VIEW_NAME

								Exec sp_executesql @nvSql

								FETCH TableName INTO @VIEW_NAME
							END
							CLOSE TableName
							DEALLOCATE TableName
						END



							Set @nvSql = ''sp_dropuser ['' + @SQLUSER_NEW + '']''
							Exec sp_executesql @nvSql
							
							Set @nvSql = ''sp_droplogin ['' + @SQLUSER_NEW + '']'' 
							Exec sp_executesql @nvSql
						END
				END
		END'
		
	EXECUTE sp_executesql @procSQL 
GO