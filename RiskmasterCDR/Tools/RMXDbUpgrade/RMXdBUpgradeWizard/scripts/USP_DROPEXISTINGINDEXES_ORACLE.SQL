[GO_DELIMITER]
create or replace
PROCEDURE USP_DROPEXISTINGINDEXES(sRM_USERID VARCHAR2)
AUTHID CURRENT_USER IS
BEGIN
  DECLARE 
           sTABLENAME varchar2(100);
           sINDEXNAME varchar2(255);
           sINDEXTYPE varchar2(255);
           sCLUSTERED INTEGER;
           iFLAG INTEGER;
           sSQL varchar2(4000);
           sERRORMESSAGE varchar2(2000);
           dDATE DATE;
           sDATE VARCHAR2(50);
           sLOGENTRY varchar2(500);
           sInsertExcptn EXCEPTION;
           sInsertExcptn1 EXCEPTION;
           sSqlCursor varchar2(4000);
           MyIndexCursor SYS_REFCURSOR;
           sEcode NUMBER;
           sEmesg VARCHAR2(500);
           sSQLDATE VARCHAR2(500);
           iIS_CLUSTERED integer; 
          iIS_UNIQUE integer; 
          iIS_PRIMARY integer; 
          sCOLUMN_NAMES varchar2(250);
          iCount integer;
          sTABLE_NAME varchar2(250);
          sINDEX_NAME varchar2(250);
          sMSG VARCHAR2(2000);
            sUNIQUEiNDEX VARCHAR2(15);
  BEGIN
     
             sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
             EXECUTE IMMEDIATE sSQLDATE INTO sDATE;            
             
             sMSG:='DROPING CONSTRAINT/INDEX START';  
             sLOGENTRY:= 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'',''STARTING'','''||sMSG||''','''||sMSG||''','''||sMSG||''',''' || sDATE|| ''')';           
             EXECUTE IMMEDIATE sLOGENTRY;
             COMMIT;
             
              sSqlCursor:='SELECT DISTINCT TABLE_NAME, INDEX_NAME, IS_CLUSTERED,IS_UNIQUE,IS_PRIMARY,COLUMN_NAMES,IS_UNIQUE_INDEX FROM '||sRM_USERID||'.EXISTING_INDICES_TEMP';  
            
         BEGIN
              OPEN MyIndexCursor FOR sSqlCursor;             
			
                  LOOP
                        FETCH MyIndexCursor INTO sTABLE_NAME , sINDEX_NAME ,iIS_CLUSTERED , iIS_UNIQUE, iIS_PRIMARY , sCOLUMN_NAMES,sUNIQUEiNDEX;  
                      
                       EXIT WHEN MyIndexCursor%NOTFOUND;
                    
                          BEGIN                                 
                                         IF (((iIS_PRIMARY=1) OR (iIS_UNIQUE=1)) AND (sUNIQUEiNDEX='PK'))THEN              
                                          BEGIN 
                                                --sSQL:='SELECT COUNT(*) FROM ALL_INDEXES WHERE INDEX_NAME='''||sINDEXNAME||''' AND  OWNER='''||UPPER(sRM_USERID)||'''';
                                                  IF(1=1) THEN
                                                            sSQL:= 'ALTER TABLE '||sRM_USERID||'.'|| sTABLE_NAME||' DROP CONSTRAINT ' ||sINDEX_NAME;
                                                            EXECUTE IMMEDIATE sSQL; 
                                                            sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                            EXECUTE IMMEDIATE sSQLDATE INTO sDATE;  
                                                            sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                            sMSG:='PRIMARY/UNIQUE CONSTRAINT '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' DROPPED SUCCESSFULLY';
                                                            sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                            EXECUTE IMMEDIATE sLOGENTRY;
															COMMIT;
															 --Add by Kuladeep Singh
                                                            --Above comand was droping the constraint but not the index in some scenario So we are droping the index also at the same time.
                                                            sSQL:= 'DROP INDEX ' || sRM_USERID || '.' || sINDEX_NAME;
                                                            EXECUTE IMMEDIATE sSQL; 
                                                            sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                            EXECUTE IMMEDIATE sSQLDATE INTO sDATE;  
                                                            sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                            sMSG:='PRIMARY/UNIQUE CONSTRAINT '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' DROPPED SUCCESSFULLY';
                                                            sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                            COMMIT;
                                                           --RAISE sInsertExcptn;                                                         
                                                     END IF;
                                                    
                                                        EXCEPTION
                                                           
                                                           WHEN OTHERS THEN 
                                                              sEcode := SQLCODE;
                                                              sEmesg := SQLERRM;                                                                  
                                                             -- dbms_output.PUT_LINE(sERRORMESSAGE);                                                                 
                                                              sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                              EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                              sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' ||TO_CHAR(sEcode) || '-' || sEmesg;
                                                              sLOGENTRY := 'INSERT INTO INDEX_UPDATE_PROCESS_LOG (PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'','''||sTABLE_NAME||''',''FAILED'',''' || sERRORMESSAGE || ''',''' || sSQL || ''',''' ||sDATE|| ''')';                                                                 
                                                              EXECUTE IMMEDIATE sLOGENTRY;
                                                              COMMIT;
                                              END;
                                              
                                  ELSE
                                          BEGIN
                                               --sSQL:='SELECT COUNT(*) FROM ALL_INDEXES WHERE INDEX_NAME='''||sINDEXNAME||''' AND  OWNER='''||UPPER(sRM_USERID)||'''';
                                             
                                               IF(1=1) THEN
                                                          IF(sUNIQUEiNDEX='NONUNIQUE') THEN
                                                            BEGIN
                                                            sSQL := 'DROP INDEX ' || sRM_USERID || '.' || sINDEX_NAME;
                                                            EXECUTE IMMEDIATE sSQL;                                                           
                                                            sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                            EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                            sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                            sMSG:='INDEX  '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' DROPPED SUCCESSFULLY';
                                                            sLOGENTRY:= 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' || sSQL ||''',''' ||sDATE || ''')';
                                                            EXECUTE IMMEDIATE sLOGENTRY; 
                                                            COMMIT;
                                                           END;
                                                         END IF;  
                                                              --RAISE sInsertExcptn1;
                                                END IF;                                                  
                                                    EXCEPTION                                                    
                                                        WHEN OTHERS THEN
                                                             sEcode := SQLCODE;
                                                             sEmesg := SQLERRM;
                                                             sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' ||TO_CHAR(sEcode) || '-' || sEmesg;
                                                             sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                             EXECUTE IMMEDIATE sSQLDATE INTO sDATE;                                                                  
                                                             sLOGENTRY:= 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'','''||sTABLE_NAME||''',''FAILED'','''||sERRORMESSAGE||''',''' || sSQL ||''',''' ||sDATE || ''')';                                                                  
                                                             EXECUTE IMMEDIATE sLOGENTRY; 
                                                             COMMIT;
                                             END;             
                                     END IF;   -- END IF; 
                                END;
                     END LOOP;
                  CLOSE MyIndexCursor;
            END;
             sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
             EXECUTE IMMEDIATE sSQLDATE INTO sDATE;   
             
            sMSG:='DROPING CONSTRAINT/INDEX FINISHED';      
           sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_PROCESS_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_PROCESS_LOG_SEQ.NEXTVAL,''DROP EXISITNG INDICES'',''FINISHING'','''||sMSG||''','''||sMSG||''','''||sMSG||''',''' ||sDATE|| ''')';
            EXECUTE IMMEDIATE sLOGENTRY;  
            COMMIT;
     
  END;
END USP_DROPEXISTINGINDEXES;
GO