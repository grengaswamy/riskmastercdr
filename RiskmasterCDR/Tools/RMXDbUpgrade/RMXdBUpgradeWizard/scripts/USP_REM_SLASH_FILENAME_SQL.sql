--exec USP_REM_SLASH_FILENAME
-- =============================================
-- Author:		CSC
-- Create date: 03/15/2011
-- Description:	MITS 23998:Removes Slash from File Name in Document Storage table of Doc db
-- =============================================
[GO_DELIMITER]

DECLARE @spSQL AS nVARCHAR(max)

IF EXISTS(SELECT TYPE, NAME FROM sys.objects WHERE type = 'P' AND name = 'USP_REM_SLASH_FILENAME')
BEGIN
	DROP PROCEDURE USP_REPLACE_FN
END
	SET @spSQL = '
CREATE PROCEDURE USP_REM_SLASH_FILENAME 
AS
BEGIN
DECLARE @iMinDocID int,@iMaxDocID INT
DECLARE @str AS Varchar(2)
DECLARE @rep_str AS Varchar(2)
DECLARE @iLoopCount AS INT
set @str=''\''
set @rep_str=''''
SELECT @iMinDocID =  MIN(STORAGE_ID),@iMaxDocID=MAX(STORAGE_ID) FROM DOCUMENT_STORAGE
set @iLoopCount=@iMinDocID
	while @iLoopCount<=@iMaxDocID
	BEGIN
		UPDATE DOCUMENT_STORAGE SET FILENAME =REPLACE(FILENAME,@str,@rep_str) WHERE STORAGE_ID BETWEEN  @iLoopCount  AND (@iLoopCount + 4999)
		set @iLoopCount=@iLoopCount+5000
	END
END	'

EXECUTE sp_executesql @spSQL	
	
[EXECUTE_SP]	
GO