[GO_DELIMITER]
create or replace procedure USP_UPDATE_PARMS_NAME_VALUE 
AS 
BEGIN 
    DECLARE 
         Parm_Category INTEGER;
         Parm_Name VARCHAR2(20);
         Row_Id INTEGER;
         Counter INTEGER;
         sSql VARCHAR2(1000);
		 curParmsNameValue SYS_REFCURSOR;  
		 sSqlCursor VARCHAR2(2000); 
		 
		 BEGIN
			SELECT COUNT(*) INTO Counter FROM PARMS_NAME_VALUE WHERE ROW_ID IS NULL;
						
			IF Counter > 0 THEN
				SELECT MAX(CASE WHEN ROW_ID IS NULL THEN 0 ELSE ROW_ID END) + 1 INTO Row_Id FROM PARMS_NAME_VALUE;
							
			--Declare the Cursor
				sSqlCursor := 'SELECT PARM_CATEGORY, PARM_NAME FROM PARMS_NAME_VALUE WHERE ROW_ID IS NULL';

				BEGIN  
				OPEN curParmsNameValue FOR sSqlCursor;  
					LOOP  
						FETCH curParmsNameValue INTO Parm_Category, Parm_Name;  
						EXIT WHEN curParmsNameValue%NOTFOUND;
						sSql:=  'UPDATE PARMS_NAME_VALUE SET ROW_ID = '|| Row_Id ||' WHERE PARM_CATEGORY = '|| Parm_Category || ' AND PARM_NAME = '''|| Parm_Name || '''';  
                        EXECUTE IMMEDIATE sSql;  
						Row_Id := Row_Id + 1;

					END LOOP;  
					CLOSE curParmsNameValue; 
				END;
								
			END IF;
		END;
					
END;
[EXECUTE_SP]
GO