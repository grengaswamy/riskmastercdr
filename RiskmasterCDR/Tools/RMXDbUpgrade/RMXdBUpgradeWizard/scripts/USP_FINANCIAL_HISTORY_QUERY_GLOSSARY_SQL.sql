[GO_DELIMITER]
IF OBJECT_ID('USP_FINANCIAL_HISTORY_QUERY_GLOSSARY', 'P') IS NOT NULL
    DROP PROCEDURE USP_FINANCIAL_HISTORY_QUERY_GLOSSARY;
GO
CREATE PROCEDURE USP_FINANCIAL_HISTORY_QUERY_GLOSSARY
	@IACTION VARCHAR(20),
	@STIME  VARCHAR(20) OUTPUT,
	@LNEXTUNIQUEID  VARCHAR(20) OUTPUT,
	@RET BIT OUTPUT ,
	@P_SQLERRM  VARCHAR(20) OUTPUT
AS
DECLARE @V_DTTM_LAST_UPDATE  VARCHAR(14);
DECLARE @V_NEXT_UNIQUE_ID   VARCHAR(50);
DECLARE @V_RET INT;
DECLARE @V_SQLERRM VARCHAR(2000);
BEGIN
  BEGIN
  SET @V_DTTM_LAST_UPDATE = (SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST');
  SET @V_NEXT_UNIQUE_ID = (SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST');
  SET @V_RET = 0;
  END

  IF @V_RET= 1 
    SET @RET = 0;
  ELSE
    SET @RET = 1;

IF @IACTION='UPDATE' 
BEGIN
    BEGIN TRY
      UPDATE GLOSSARY SET NEXT_UNIQUE_ID = @V_NEXT_UNIQUE_ID+1, DTTM_LAST_UPDATE = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), GETDATE(), 126),'-',''),'T',''),':','')--SYSDATE
      WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';
      SET @V_RET =0;
      
      SET @STIME = @V_DTTM_LAST_UPDATE;
      SET @LNEXTUNIQUEID =@V_NEXT_UNIQUE_ID;
      END TRY
      BEGIN CATCH
     END CATCH;
END

IF @IACTION = 'QUERY' 
BEGIN
      SET @STIME = @V_DTTM_LAST_UPDATE;
      SET @LNEXTUNIQUEID = @V_NEXT_UNIQUE_ID;
      SET @V_RET =0;
END

 IF @V_RET = 1 
    SET @RET = 0;
  ELSE
    SET @RET = 1;
END
GO