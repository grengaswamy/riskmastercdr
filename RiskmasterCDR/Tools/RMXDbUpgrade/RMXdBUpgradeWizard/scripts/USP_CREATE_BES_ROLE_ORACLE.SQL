[GO_DELIMITER]
create or replace PROCEDURE USP_CREATE_BES_ROLE (sBES_ROLE  varchar2) 
AS 
BEGIN
	DECLARE 
		iRoleCount INT;
		sSql VARCHAR(2000);
		sRoleName VARCHAR(100);
                RoleExcptn EXCEPTION;
		
	BEGIN
		
				sSql:='CREATE ROLE '||sBES_ROLE;
				EXECUTE IMMEDIATE sSql;
                                RAISE RoleExcptn;			
               EXCEPTION
                    WHEN RoleExcptn THEN  
                         null;
                     WHEN OTHERS THEN
                         null;
	END;
  
END;
GO