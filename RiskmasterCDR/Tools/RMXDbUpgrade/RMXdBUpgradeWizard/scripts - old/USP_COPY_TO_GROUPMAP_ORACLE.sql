[GO_DELIMITER] 
create or replace
PROCEDURE USP_COPY_TO_GROUPMAP   
 AS   
 BEGIN  
 	DECLARE  
 		iCount INT;  
 		iGroupId INT;  
 		iUpdatedFlag INT;  
 		iDeleted_fag INT;  
 		iStatusFlag INT;  
   
 	BEGIN  
 		iCount := 0;  
 		SELECT COUNT(*) INTO  iCount FROM GROUP_MAP;  
   
 		IF(iCount = 0) THEN  
 		BEGIN  
 			DECLARE CURSOR OrgSecurityCursor IS SELECT GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG FROM ORG_SECURITY;  
            BEGIN
				FOR OrgSecurityCursor_Val IN OrgSecurityCursor
 				LOOP 
 				IF(OrgSecurityCursor_Val.UPDATED_FLAG = 0) THEN
						 BEGIN
				       	INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG) VALUES (OrgSecurityCursor_Val.GROUP_ID,OrgSecurityCursor_Val.GROUP_ID,-1,OrgSecurityCursor_Val.DELETED_FLAG,OrgSecurityCursor_Val.STATUS_FLAG);  
						 END;
				ELSE
				    BEGIN			 
					   INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,UPDATED_FLAG,DELETED_FLAG,STATUS_FLAG) VALUES (OrgSecurityCursor_Val.GROUP_ID,OrgSecurityCursor_Val.GROUP_ID,OrgSecurityCursor_Val.UPDATED_FLAG,OrgSecurityCursor_Val.DELETED_FLAG,OrgSecurityCursor_Val.STATUS_FLAG);  
					  END;
				END IF;	
 				END LOOP;	  
			END;
 		END;  
 		END IF;  
 	END;  
 END;  
[EXECUTE_SP]
GO