[GO_DELIMITER]
CREATE OR REPLACE
PROCEDURE USP_DISABLE_BES (sRM_USERID VARCHAR2,sUserNameNew VARCHAR2,sUserNameOld VARCHAR2)
AUTHID CURRENT_USER IS
BEGIN
  DECLARE  
  sSQL VARCHAR2(1000);
  iOldUserCount INT;
  iNewUserCount INT;
  BEGIN
  
    
  
            sSql:='SELECT COUNT(*)  FROM ALL_USERS WHERE USERNAME='''||UPPER(sUserNameOld)||'''';
            EXECUTE IMMEDIATE sSql INTO iOldUserCount;  
     IF(iOldUserCount>0) THEN
     BEGIN
        sSql:='DROP USER '||sUserNameOld||' CASCADE';
       EXECUTE IMMEDIATE sSql;
    END;  
    
    ELSE
    
    BEGIN
    
            sSql:='SELECT COUNT(*)  FROM ALL_USERS WHERE USERNAME='''||UPPER(sUserNameNew)||'''';
            EXECUTE IMMEDIATE sSql INTO iNewUserCount;
     IF (iNewUserCount>0) THEN
     BEGIN    
        sSql:='DROP USER '||sUserNameNew||' CASCADE';
       EXECUTE IMMEDIATE sSql;
     END;
     END IF;
    END;
  END IF;
  
  END;
END USP_DISABLE_BES;
GO