[GO_DELIMITER]
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
--<--AUTHOR-NADIM ZAFAR-->
--<--NAME-SP_REFRESH_BES_ROLE-->
--<--DATE CREATED-11/02/2009-->
go

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_REFRESH_BES_ROLE')
DROP PROCEDURE USP_REFRESH_BES_ROLE
go

CREATE PROCEDURE USP_REFRESH_BES_ROLE @sRole varchar(50),@sStoredproc varchar(5000)
AS
	--use cursor to execute row by row
	DECLARE @TABLE_NAME NVARCHAR(100)
	DECLARE @TABLE_TYPE NVARCHAR(30)
	DECLARE @nvSql as nVarchar(4000)
	
	DECLARE @Index_Count INT
	DECLARE @sStoredprocName VARCHAR(5000)
	DECLARE @sStoredprocRight VARCHAR(5000)
	DECLARE @spLength INT
	DECLARE @PipeIndex INT
	
	DECLARE TableName CURSOR FOR Select so1.name from Sys.objects so1 ,Sys.objects so2 where so1.schema_id = so2.schema_id and so1.type='u' and so2.type='u' and so2.name='CLAIM' order by so1.Name

	OPEN TableName
		FETCH TableName INTO @TABLE_NAME

		WHILE @@Fetch_Status = 0
		BEGIN
			Set @nvSql  = 'GRANT SELECT,UPDATE,INSERT,DELETE ON [' + @TABLE_NAME + '] TO '+@sRole
			--print @nvSql
			exec sp_executesql @nvSql
			FETCH TableName INTO @TABLE_NAME
		END
	CLOSE TableName
	DEALLOCATE TableName
    
    SET @PipeIndex = CHARINDEX('|', @sStoredproc)
	WHILE @PipeIndex <> 0
	BEGIN
		SET @spLength=LEN(@sStoredproc)
		SELECT @sStoredprocName=LEFT(@sStoredproc,@PipeIndex-1)
		SELECT @sStoredprocRight=RIGHT(@sStoredproc,@spLength-@PipeIndex)
		SELECT @sStoredproc=REPLACE(@sStoredproc,@sStoredproc,@sStoredprocRight)
		SELECT @Index_Count= COUNT(*) FROM sys.objects where name=@sStoredprocName
		IF @Index_Count>0
		BEGIN
			SET @nvSql='GRANT EXECUTE ON '+@sStoredprocName+' TO '+@sRole
			Exec sp_executesql @nvSql
		END
		SET @PipeIndex = CHARINDEX('|', @sStoredproc)
	END
RETURN
--EXEC sp_addrolemember N'BESRole', N'nd_test1'
--EXEC sp_droprolemember N'BESRole', N'nd_test1'
--EXEC sp_addrolemember N'db_ddladmin', N'sg00008'
--EXEC sp_addrolemember N'db_ddladmin', N'sg00008'
--EXEC sp_addrolemember N'db_securityadmin', N'sg00008'
--EXEC sp_addsrvrolemember 'sg00008', 'securityadmin'
--EXEC sp_addsrvrolemember 'sg00008', 'setupadmin'
--EXEC sp_dropsrvrolemember 'sg00008', 'setupadmin'
--sp_RefreshBesRole 'BESRole'
GO

