[GO_DELIMITER]
--<--AUTHOR-NADIM ZAFAR-->
--<--NAME-[USP_BUILD_ENTITY_MAP]-->
--<--DATE CREATED-11/02/2009-->
DECLARE @spSQL AS nVARCHAR(max) 
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_BUILD_ENTITY_MAP')
BEGIN
	DROP PROCEDURE USP_BUILD_ENTITY_MAP
END
	SET @spSQL = '
CREATE PROCEDURE USP_BUILD_ENTITY_MAP @iGroupId INT, @SQLUSER VARCHAR(100), @BES_COMPLEX_PASSWORD VARCHAR(50), @sBesRole varchar(50), @sRm_UserId varchar(100), @iPwd_Changed INT, @iConfRec INT, @sSubordinateList NVARCHAR(MAX), @out_err varchar(500) OUTPUT
AS
--	DECLARE @VIEW_TABLES varchar(500)
--	SET @VIEW_TABLES=''CLAIM|CLAIMANT|EMPLOYEE|ENTITY|EVENT|FUNDS|PERSON_INVOLVED|RESERVE_CURRENT|RESERVE_HISTORY|POLICY|POLICY_ENH|ADJUST_DATED_TEXT|FUNDS_AUTO|''
	DECLARE @Adjuster VARCHAR(200)
    DECLARE @GROUP_ENH_NOTES_TYPES VARCHAR(2000)
	DECLARE @Broker VARCHAR(200)
	DECLARE @Insurer VARCHAR(200)
	DECLARE @Group_Entities NVARCHAR(200)
    DECLARE @iBoeingBrokerSupport INT
	DECLARE @iSuperGroupId INT 
	DECLARE @iCarrierClaim INT 

	-- pmittal5 - @iGroupId is the User level GroupId of GROUP_MAP
	SELECT @iSuperGroupId = SUPER_GROUP_ID FROM GROUP_MAP WHERE GROUP_ID = @iGroupId

	SELECT @Group_Entities=GROUP_ENTITIES, @Adjuster=GROUP_ADJ_TEXT_TYPES,@GROUP_ENH_NOTES_TYPES=GROUP_ENH_NOTES_TYPES, @Broker=GROUP_BROKERS, @Insurer=GROUP_INSURERS 
	  FROM ORG_SECURITY WHERE GROUP_ID = @iSuperGroupId
	  -- AND GROUP_ID=@iGroupId 
	   --AND GROUP_ID = @iSuperGroupId
    
    SELECT @iBoeingBrokerSupport= USE_BROKER_BES FROM SYS_PARMS

	SELECT @iCarrierClaim= STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME=''MULTI_COVG_CLM''
	
	--DECLARE @iMaxGroupID int
	Declare @nvSql as nVarchar(4000)
	DECLARE @SCHEMA_ID INT
	DECLARE @SCHEMA_NAME VARCHAR(50)
	DECLARE @VIEWS_COUNT INT
	DECLARE @VIEW_NAME VARCHAR(25)
	--DECLARE @ERROR_MESSAGE NVARCHAR(MAX)
	--DECLARE @ERROR_lOG AS NVARCHAR(MAX)
		
	DECLARE @DB_TABLE_OWNER NVARCHAR(50)
    DECLARE @ICUSTOMIZE_ADMIN INT
begin try
	SELECT @SCHEMA_ID=SCHEMA_ID FROM SYS.SCHEMAS WHERE NAME =@SQLUSER
	SELECT @ICUSTOMIZE_ADMIN = COUNT(*) FROM SYS.OBJECTS WHERE TYPE=''P'' AND name = ''USP_CUSTOMIZE_ADMIN''
	
	IF @SCHEMA_ID is not NULL
	BEGIN
		SET @VIEWS_COUNT=(SELECT COUNT(*) FROM SYS.OBJECTS WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID)
		
		IF @VIEWS_COUNT<>0
			BEGIN

				
				DECLARE Table_Name CURSOR FOR SELECT NAME FROM SYS.OBJECTS 
											WHERE TYPE=''V'' AND SCHEMA_ID=@SCHEMA_ID
				OPEN Table_Name
				FETCH Table_Name INTO @VIEW_NAME
				WHILE @@Fetch_Status = 0
				BEGIN
					set @nvSql=''Drop View ['' + @SQLUSER + ''].'' + @VIEW_NAME

					Exec sp_executesql @nvSql

					FETCH Table_Name INTO @VIEW_NAME
				END
				CLOSE Table_Name
				DEALLOCATE Table_Name
				
			END
	END
	DECLARE @bIsAllOrgEntity bit
	set @bIsAllOrgEntity=0

	IF @bIsAllOrgEntity<>1
	BEGIN
		DECLARE @iSsQLsERVER2005 BIT
		SET @iSsQLsERVER2005=1

		IF(@iSsQLsERVER2005=1)
			BEGIN
				DECLARE @DATABASE VARCHAR(50)
				IF @SCHEMA_ID is NULL
				BEGIN
					Set @nvSql=''CREATE LOGIN [''+@SQLUSER+''] WITH PASSWORD=''''''+@BES_COMPLEX_PASSWORD+''''''''+'',CHECK_EXPIRATION=OFF,CHECK_POLICY=OFF''
					Exec sp_executesql @nvSql

					Set @nvSql=''CREATE USER ''+@SQLUSER
					Exec sp_executesql @nvSql

					Set @nvSql=''GRANT CREATE TABLE, CREATE VIEW TO ''+@SQLUSER
					Exec sp_executesql @nvSql

					Set @nvSql=''CREATE SCHEMA ''+@SQLUSER+''  AUTHORIZATION  ''+@SQLUSER
					Exec sp_executesql @nvSql
				END
				--pmittal5 - Password change functionality in case of SQL also.
				ELSE
				BEGIN 
					IF(@iPwd_Changed=-1)
					BEGIN
						Set @nvSql = ''ALTER LOGIN ''+@SQLUSER+'' WITH PASSWORD=''''''+@BES_COMPLEX_PASSWORD+''''''''
						Exec sp_executesql @nvSql
					END
				END
				--End - pmittal5
				Set @nvSql=''ALTER USER ''+@SQLUSER+'' WITH DEFAULT_SCHEMA =''+@SQLUSER
				Exec sp_executesql @nvSql
			END
	END
	
IF @bIsAllOrgEntity<>1
	BEGIN


		DECLARE @TABLE_QUALIFIER NVARCHAR(100)
		DECLARE @TABLE_OWNER NVARCHAR(50)
		DECLARE @TABLE_NAME NVARCHAR(100)
		DECLARE @TABLE_TYPE NVARCHAR(30)
		DECLARE @TABLE_REMARKS NVARCHAR(100)
		declare @num int
		set @num=1
		
--		DECLARE TableName CURSOR FOR Select so.name, ss.name 
--		      						   from Sys.objects so 
--							     inner join sys.schemas ss On so.Schema_id = ss.Schema_id 
--		     						  WHERE type=''U'' 
--		  						   order by ss.Name
--
--		OPEN TableName
--
--		FETCH TableName INTO @TABLE_NAME, @TABLE_OWNER
--
--		SET @DB_TABLE_OWNER=@TABLE_OWNER
		Select @DB_TABLE_OWNER=ss.name 
		      						   from Sys.objects so 
										inner join sys.schemas ss On so.Schema_id = ss.Schema_id 
		     							WHERE type=''U'' and so.name =''CLAIM'' 
--		WHILE @@Fetch_Status = 0
--			BEGIN
--				set @nvSql=''GRANT SELECT,UPDATE,INSERT,DELETE ON ''+@TABLE_OWNER+''.''+@TABLE_NAME +'' TO  ''+@SQLUSER
--
--				Exec sp_executesql @nvSql
--
--				FETCH TableName INTO @TABLE_NAME, @TABLE_OWNER
--			END
--		CLOSE TableName
--		DEALLOCATE TableName
		Set @nvSql= ''sp_addrolemember  [''+@sBesRole+''], [''+@SQLUSER+'']''
					Exec sp_executesql @nvSql

				IF (@iSsQLsERVER2005=1)
					BEGIN
						Set @nvSql=''EXECUTE AS USER=''''''+@SQLUSER +''''''''

						Exec sp_executesql @nvSql
-- pmittal5 - Modified View definitions for Confidential Record
						IF(@iConfRec = -1)
						BEGIN
							Set @nvSql=''CREATE VIEW CLAIM AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIM WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)''
							Exec sp_executesql @nvSql

							Set @nvSql=''CREATE VIEW CLAIMANT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIMANT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)''
	   						Exec sp_executesql @nvSql

							Set @nvSql=''CREATE VIEW EVENT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.EVENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW FUNDS AS SELECT * FROM ''+@DB_TABLE_OWNER+''.FUNDS WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW PERSON_INVOLVED AS SELECT * FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)'' 
							Exec sp_executesql @nvSql       

							Set @nvSql=''CREATE VIEW RESERVE_CURRENT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW RESERVE_HISTORY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'') AND (CONF_FLAG = 0 OR CONF_FLAG IS NULL) OR CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0)'' 
							Exec sp_executesql @nvSql

							Set @nvSql=''CREATE VIEW FUNDS_AUTO AS SELECT * FROM ''+@DB_TABLE_OWNER+''.FUNDS_AUTO WHERE CONF_EVENT_ID IN (SELECT EVENT_ID FROM CONF_EVENT_PERM WHERE USER_ID IN (''+@sSubordinateList+'') AND DELETED_FLAG = 0) OR CONF_EVENT_ID = 0'' 
							Exec sp_executesql @nvSql
						END
						ELSE
						BEGIN
							Set @nvSql=''CREATE VIEW CLAIM AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIM WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')''
							Exec sp_executesql @nvSql

							Set @nvSql=''CREATE VIEW CLAIMANT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIMANT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')''
	   						Exec sp_executesql @nvSql

							Set @nvSql=''CREATE VIEW EVENT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.EVENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW FUNDS AS SELECT * FROM ''+@DB_TABLE_OWNER+''.FUNDS WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW PERSON_INVOLVED AS SELECT * FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
							Exec sp_executesql @nvSql       

							Set @nvSql=''CREATE VIEW RESERVE_CURRENT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
							Exec sp_executesql @nvSql
						
							Set @nvSql=''CREATE VIEW RESERVE_HISTORY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
							Exec sp_executesql @nvSql
						END

						Set @nvSql=''CREATE VIEW EMPLOYEE AS SELECT * FROM ''+@DB_TABLE_OWNER+''.EMPLOYEE WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) +'')'' 
						Exec sp_executesql @nvSql
					
						IF (@iBoeingBrokerSupport=-1)
						BEGIN 
							Set @nvSql=''CREATE VIEW ENTITY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.ENTITY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = '' + cast(@iSuperGroupId as varchar(20)) + '') AND ENTITY_ID IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY_SUPP ''
							if @broker<>''''
								Set @nvSql = @nvSql + '' WHERE BROKER_FRM_EID IN ('' + @broker + '',0) OR BROKER_FRM_EID IS NULL) OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY_SUPP))''
							else
								Set @nvSql = @nvSql + ''))''
							Set @nvSql = @nvSql + '' OR CONF_EVENT_ID = 1 ''
							Exec sp_executesql @nvSql
						END
						ELSE
						BEGIN
							Set @nvSql=''CREATE VIEW ENTITY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.ENTITY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = ''+ cast(@iSuperGroupId as varchar(20)) +'') OR CONF_EVENT_ID = 1'' 
							Exec sp_executesql @nvSql
						END
						
						IF (@iBoeingBrokerSupport=-1)
						BEGIN 
							SET @nvSql=''CREATE VIEW POLICY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP ''
							if @broker<>''''
								SET @nvSql=@nvSql+ '' WHERE BROKER_FRM_EID IN ('' + @broker + '',0) OR BROKER_FRM_EID IS NULL)  OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_SUPP)''
							else
								SET @nvSql=@nvSql+'')))''
							Exec sp_executesql @nvSql
							SET @nvSql=''CREATE VIEW POLICY_ENH AS SELECT * FROM ''+@DB_TABLE_OWNER+''.POLICY_ENH WHERE POLICY_ID IN (SELECT POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP ''
							if @broker<>''''
								SET @nvSql=@nvSql+ '' WHERE BROKER_FRM_EID IN ('' + @broker + '',0) OR BROKER_FRM_EID IS NULL)  OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_SUPP)''
							else
								SET @nvSql=@nvSql+'')))''							
							Exec sp_executesql @nvSql
						END
						ELSE
						BEGIN
						    IF (@iCarrierClaim=-1)
							BEGIN
							  SET @nvSql=''CREATE VIEW POLICY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.POLICY'' 
							  Exec sp_executesql @nvSql
							END
							ELSE
							BEGIN
							  SET @nvSql=''CREATE VIEW POLICY AS SELECT * FROM ''+@DB_TABLE_OWNER+''.POLICY WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_X_INSURED PXI,''+@DB_TABLE_OWNER+''.ENTITY_MAP EM WHERE PXI.INSURED_EID = EM.ENTITY_ID AND EM.GROUP_ID ='' + cast(@iSuperGroupId as varchar(20)) +'') OR POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CONF_EVENT_ID > 0)'' 
							  Exec sp_executesql @nvSql
							END
							SET @nvSql=''CREATE VIEW POLICY_ENH AS SELECT * FROM ''+@DB_TABLE_OWNER+''.POLICY_ENH WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM ''+@DB_TABLE_OWNER+''.POLICY_X_INSRD_ENH PXII,''+@DB_TABLE_OWNER+''.ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID ='' + cast(@iSuperGroupId as varchar(20)) +'') OR POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CONF_EVENT_ID > 0)'' 
							Exec sp_executesql @nvSql
						END
					END
-- End - pmittal5	

		IF @Adjuster IS  NULL
			BEGIN
				SET @nvSql=	''CREATE VIEW ADJUST_DATED_TEXT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.ADJUST_DATED_TEXT ''
				Exec sp_executesql @nvSql    
			END
		ELSE 
			BEGIN
				DECLARE @HyphenIndex INT
				DECLARE @LENGTH INT
				SELECT @HyphenIndex =CHARINDEX(''-'', @Adjuster)
				IF @HyphenIndex = 1
					BEGIN
						SET @LENGTH=LEN(@Adjuster)
						SELECT @Adjuster=RIGHT(@Adjuster,@LENGTH-@HyphenIndex)
						SET @nvSql=	''CREATE VIEW ADJUST_DATED_TEXT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.ADJUST_DATED_TEXT WHERE  TEXT_TYPE_CODE NOT IN ('' + @Adjuster+ '' )''
						Exec sp_executesql @nvSql    
					END
				ELSE
					BEGIN
						SET @nvSql=	''CREATE VIEW ADJUST_DATED_TEXT AS SELECT * FROM ''+@DB_TABLE_OWNER+''.ADJUST_DATED_TEXT WHERE TEXT_TYPE_CODE IN (''+ @Adjuster+ '' )''
						Exec sp_executesql @nvSql    
					END
			END

                  IF @GROUP_ENH_NOTES_TYPES IS  NULL OR @GROUP_ENH_NOTES_TYPES = ''''
			BEGIN
				SET @nvSql=	''CREATE VIEW CLAIM_PRG_NOTE AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIM_PRG_NOTE ''
				Exec sp_executesql @nvSql    
			END
		ELSE 
			BEGIN
				DECLARE @HyphenIndex_enh INT
				DECLARE @LENGTH_enh INT
				SELECT @HyphenIndex_enh =CHARINDEX(''-'', @GROUP_ENH_NOTES_TYPES)
				IF @HyphenIndex_enh = 1
					BEGIN
						SET @LENGTH_enh=LEN(@GROUP_ENH_NOTES_TYPES)
						SELECT @GROUP_ENH_NOTES_TYPES=RIGHT(@GROUP_ENH_NOTES_TYPES,@LENGTH_enh-@HyphenIndex_enh)
						SET @nvSql=	''CREATE VIEW CLAIM_PRG_NOTE AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIM_PRG_NOTE WHERE  NOTE_TYPE_CODE NOT IN ('' + @GROUP_ENH_NOTES_TYPES+ '' )''
						Exec sp_executesql @nvSql    
					END
				ELSE
					BEGIN
						SET @nvSql=	''CREATE VIEW CLAIM_PRG_NOTE AS SELECT * FROM ''+@DB_TABLE_OWNER+''.CLAIM_PRG_NOTE WHERE NOTE_TYPE_CODE IN (''+ @GROUP_ENH_NOTES_TYPES+ '' )''
						Exec sp_executesql @nvSql    
					END
			END
		IF @ICUSTOMIZE_ADMIN > 0
		BEGIN
			EXEC USP_CUSTOMIZE_ADMIN @iGroupId,@DB_TABLE_OWNER
		END
				
		SET @nvSql =''REVERT''
		Exec sp_executesql @nvSql                        

	END

	SET @nvSql=''UPDATE STATISTICS ENTITY_MAP''
	Exec sp_executesql @nvSql

--pmittal5 Update GROUP_MAP for Confidential Record
	UPDATE GROUP_MAP SET UPDATED_FLAG=0 WHERE GROUP_ID=@iGroupId
	UPDATE ORG_SECURITY SET UPDATED_FLAG=0 WHERE GROUP_ID=@iSuperGroupId
	--USP_BUILD_ENTITY_MAP 15,''BES0111048200015'',''BES0111048200015'',''bes_role'',''sa''
end try
begin catch
	SELECT	@out_err = error_message()
end catch
	'
	EXECUTE sp_executesql @spSQL	
GO


