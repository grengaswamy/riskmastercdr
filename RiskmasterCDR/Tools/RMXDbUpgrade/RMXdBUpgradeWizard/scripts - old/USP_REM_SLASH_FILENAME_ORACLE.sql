[GO_DELIMITER]
create or replace
PROCEDURE USP_REM_SLASH_FILENAME (sRM_USERID VARCHAR2)  
IS
BEGIN
 DECLARE
		iMinDocID INTEGER;
		iMaxDocID INTEGER;		
		iLoopCount INTEGER;
		sSQL VARCHAR2(5000);		
        
	BEGIN
		iLoopCount:=0;      
    
    sSQL:='SELECT  MIN(STORAGE_ID)  FROM '||sRM_USERID||'.DOCUMENT_STORAGE';
                EXECUTE IMMEDIATE sSQL INTO iMinDocID;
                
		sSQL:='SELECT  MAX(STORAGE_ID)  FROM '||sRM_USERID||'.DOCUMENT_STORAGE';
                EXECUTE IMMEDIATE sSQL INTO iMaxDocID;
                
		iLoopCount:=iMinDocID;
		
		WHILE iLoopCount <= iMaxDocID
		LOOP
			
				sSQL:='UPDATE '||sRM_USERID||'.DOCUMENT_STORAGE  SET FILENAME =REPLACE(FILENAME,''\'','''') WHERE STORAGE_ID BETWEEN '||iLoopCount||'  AND ('||iLoopCount ||'+ 4999)';                                 
                                EXECUTE IMMEDIATE sSQL;                              
			
			iLoopCount:=iLoopCount+5000;
      
		END LOOP;
  END;
END;

[EXECUTE_SP]
GO