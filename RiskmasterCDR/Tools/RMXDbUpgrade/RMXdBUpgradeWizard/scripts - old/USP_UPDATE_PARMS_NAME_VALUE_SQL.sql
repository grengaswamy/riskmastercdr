--exec USP_UPDATE_PARMS_NAME_VALUE 
-- =============================================
-- Author:		CSC
-- Create date: 06/11/2010
-- Description:	Updates the Parms_Name_Value with Row_Id
-- =============================================
[GO_DELIMITER]
DECLARE @nvSQL AS nVARCHAR(max) 
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'USP_UPDATE_PARMS_NAME_VALUE' AND TYPE = 'P')
	BEGIN
		DROP PROCEDURE USP_UPDATE_PARMS_NAME_VALUE
	END
		SET @nvSQL = 'CREATE PROCEDURE USP_UPDATE_PARMS_NAME_VALUE 
					  AS
					  BEGIN
						-- SET NOCOUNT ON added to prevent extra result sets from
						-- interfering with SELECT statements.
						SET NOCOUNT ON;

						--INSERT ADDRESSES INFO INTO THE ENTITY_X_ADDRESSES table
						DECLARE @Parm_Category INT,
								@Parm_Name VARCHAR(20),
								@Row_Id INT,
								@Counter INT,
								@nvSql NVARCHAR(2000)
						
						SELECT @Counter = COUNT(*) FROM PARMS_NAME_VALUE WHERE ROW_ID IS NULL
						
						IF (@Counter > 0)
						BEGIN
							
							SELECT @Row_Id = MAX(CASE WHEN ROW_ID IS NULL THEN 0 ELSE ROW_ID END) + 1 FROM PARMS_NAME_VALUE 
							
								
							--Declare the Cursor
							DECLARE curParmsNameValue CURSOR FOR 
								SELECT PARM_CATEGORY, PARM_NAME FROM PARMS_NAME_VALUE 
								WHERE ROW_ID IS NULL

							--open the cursor
							OPEN curParmsNameValue

							--loop through the cursor lines
							FETCH NEXT FROM curParmsNameValue INTO @Parm_Category, @Parm_Name
							WHILE @@FETCH_STATUS = 0
							BEGIN
								UPDATE PARMS_NAME_VALUE SET ROW_ID = @Row_Id WHERE PARM_CATEGORY = @Parm_Category AND PARM_NAME = @Parm_Name
								SET @Row_Id = @Row_Id + 1

	 							FETCH NEXT FROM curParmsNameValue INTO @Parm_Category, @Parm_Name
							END

							--clean up
							CLOSE curParmsNameValue
							DEALLOCATE curParmsNameValue
							
						END	
							
					END' 
					
		EXECUTE sp_executesql @nvSQL 
[EXECUTE_SP]	
GO
