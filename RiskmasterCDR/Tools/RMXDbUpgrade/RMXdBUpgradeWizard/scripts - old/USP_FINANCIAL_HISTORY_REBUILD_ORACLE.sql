[GO_DELIMITER]
create or replace procedure USP_FINANCIAL_HISTORY_REBUILD(p_jobID   varchar2 Default 'direct',
                                                     P_MODE    IN VARCHAR2,
                                                     p_claimid IN VARCHAR2,
                                                     p_sqlerrm out varchar2)
  AUTHID CURRENT_USER IS  
   m_bFinHistLog       BOOLEAN;  
   m_bZeroBasedFinHist BOOLEAN := FALSE; -- KAPIL BY DEFAULT VALUE ADDED FOR CODE ZEROBASEDFINHIST/ ZEROBASEDCRITERIA  
   m_bFullBuild        BOOLEAN := TRUE;  
   isEventBased        NUMBER;  
   m_claimid           VARCHAR2(100);  
   m_DeptAbbr          VARCHAR2(100);  
   m_str_parm_value    varchar2(10) := '0';  
   p_retval            varchar2(20);  
   v_counter_n         number; 
   -- GC LOB Parameters  
   -- Declare type of GC LOB  
   TYPE m_GCLOBParm IS RECORD(  
     --m_collectionInRsv      SYS_PARMS_LOB.COLL_IN_RSV_BAL%type,  
     --m_collectionInIncurred SYS_PARMS_LOB.COLL_IN_INCUR_BAL%type,  
     m_collectionInRsv      BOOLEAN,  
     m_collectionInIncurred BOOLEAN,  
     m_perrsvcollectionInRsv      BOOLEAN,  
     m_perrsvcollectionInIncurred BOOLEAN, 
     m_reserveTracking      SYS_PARMS_LOB.RESERVE_TRACKING%type,  
     m_adjRsv               SYS_PARMS_LOB.ADJ_RSV%type,  
     m_lineOfBusCode        SYS_PARMS_LOB.LINE_OF_BUS_CODE%type,  
     --m_balToZero            SYS_PARMS_LOB.BAL_TO_ZERO%type,  
     m_balToZero            BOOLEAN,  
     m_negBalToZero         SYS_PARMS_LOB.NEG_BAL_TO_ZERO%type,  
     m_setToZero            SYS_PARMS_LOB.SET_TO_ZERO%type,  
     m_estNumofClaims       SYS_PARMS_LOB.EST_NUM_OF_CLAIMS%type,  
     m_durationCode         SYS_PARMS_LOB.DURATION_CODE%type);  
   -- Declare type of WC LOB  
   TYPE m_WCLOBParm IS RECORD(  
     --m_collectionInRsv      SYS_PARMS_LOB.COLL_IN_RSV_BAL%type,  
     --m_collectionInIncurred SYS_PARMS_LOB.COLL_IN_INCUR_BAL%type,  
     m_collectionInRsv      BOOLEAN,  
     m_collectionInIncurred BOOLEAN,  
     m_perrsvcollectionInRsv      BOOLEAN,  
     m_perrsvcollectionInIncurred BOOLEAN,  
     m_reserveTracking      SYS_PARMS_LOB.RESERVE_TRACKING%type,  
     m_adjRsv               SYS_PARMS_LOB.ADJ_RSV%type,  
     m_lineOfBusCode        SYS_PARMS_LOB.LINE_OF_BUS_CODE%type,  
     --m_balToZero            SYS_PARMS_LOB.BAL_TO_ZERO%type,  
     m_balToZero            BOOLEAN,  
     m_negBalToZero         SYS_PARMS_LOB.NEG_BAL_TO_ZERO%type,  
     m_setToZero            SYS_PARMS_LOB.SET_TO_ZERO%type,  
     m_estNumofClaims       SYS_PARMS_LOB.EST_NUM_OF_CLAIMS%type,  
     m_durationCode         SYS_PARMS_LOB.DURATION_CODE%type);  
   -- Declate type of VA LOB  
   TYPE m_VALOBParm IS RECORD(  
     --m_collectionInRsv      SYS_PARMS_LOB.COLL_IN_RSV_BAL%type,  
     --m_collectionInIncurred SYS_PARMS_LOB.COLL_IN_INCUR_BAL%type,  
     m_collectionInRsv      BOOLEAN,  
     m_collectionInIncurred BOOLEAN,  
     m_perrsvcollectionInRsv      BOOLEAN,  
     m_perrsvcollectionInIncurred BOOLEAN,  
     m_reserveTracking      SYS_PARMS_LOB.RESERVE_TRACKING%type,  
     m_adjRsv               SYS_PARMS_LOB.ADJ_RSV%type,  
     m_lineOfBusCode        SYS_PARMS_LOB.LINE_OF_BUS_CODE%type,  
     --m_balToZero            SYS_PARMS_LOB.BAL_TO_ZERO%type,  
     m_balToZero            BOOLEAN,  
     m_negBalToZero         SYS_PARMS_LOB.NEG_BAL_TO_ZERO%type,  
     m_setToZero            SYS_PARMS_LOB.SET_TO_ZERO%type,  
     m_estNumofClaims       SYS_PARMS_LOB.EST_NUM_OF_CLAIMS%type,  
     m_durationCode         SYS_PARMS_LOB.DURATION_CODE%type);  
   -- Declare type NONOCC LOB  
   TYPE m_NONOCCLOBParm IS RECORD(  
     --m_collectionInRsv      SYS_PARMS_LOB.COLL_IN_RSV_BAL%type,  
     --m_collectionInIncurred SYS_PARMS_LOB.COLL_IN_INCUR_BAL%type,  
     m_collectionInRsv      BOOLEAN,  
     m_collectionInIncurred BOOLEAN,  
     m_perrsvcollectionInRsv      BOOLEAN,  
     m_perrsvcollectionInIncurred BOOLEAN,  
     m_reserveTracking      SYS_PARMS_LOB.RESERVE_TRACKING%type,  
     m_adjRsv               SYS_PARMS_LOB.ADJ_RSV%type,  
     m_lineOfBusCode        SYS_PARMS_LOB.LINE_OF_BUS_CODE%type,  
     --m_balToZero            SYS_PARMS_LOB.BAL_TO_ZERO%type,  
     m_balToZero            BOOLEAN,  
     m_negBalToZero         SYS_PARMS_LOB.NEG_BAL_TO_ZERO%type,  
     m_setToZero            SYS_PARMS_LOB.SET_TO_ZERO%type,  
     m_estNumofClaims       SYS_PARMS_LOB.EST_NUM_OF_CLAIMS%type,  
     m_durationCode         SYS_PARMS_LOB.DURATION_CODE%type);  
   
   -- SYS Parameters  
   TYPE m_SysParms IS RECORD(  
     m_shift1Name               SYS_PARMS.SHIFT_1_NAME%type,  
     m_shift2Name               SYS_PARMS.SHIFT_2_NAME%type,  
     m_shift3Name               SYS_PARMS.SHIFT_3_NAME%type,  
     m_shift1Start              SYS_PARMS.SHIFT_1_START%type,  
     m_shift2Start              SYS_PARMS.SHIFT_2_START%type,  
     m_shift3Start              SYS_PARMS.SHIFT_3_START%type,  
     m_shift1End                SYS_PARMS.SHIFT_1_END%type,  
     m_shift2End                SYS_PARMS.SHIFT_2_END%type,  
     m_shift3End                SYS_PARMS.SHIFT_3_END%type,  
     m_FIN_HIST_EVAL            SYS_PARMS.FIN_HIST_EVAL%type,  
     m_ESSP                     VARCHAR2(100), --SYS_PARMS.ESSP%type,  
     m_useTranDate              boolean,  
     m_includeVoids             boolean, --Financial History option to include voids and offset them by negative transactions on the void date  JP 8/28/98  
     m_includeOnlyPrintedChecks boolean);  
   
   -- GC LOB Parameters  
   TYPE tr_m_GCLOBParm IS TABLE OF m_GCLOBParm;  
   m_m_GCLOBParm   m_GCLOBParm;  
   obj_m_GCLOBParm tr_m_GCLOBParm := tr_m_GCLOBParm();  
   v_counter       NUMBER := 1;  
   
   -- WC LOB Parameters  
   TYPE tr_m_WCLOBParm IS TABLE OF m_WCLOBParm;  
   m_m_WCLOBParm   m_WCLOBParm;  
   obj_m_WCLOBParm tr_m_WCLOBParm := tr_m_WCLOBParm();  
   
   -- VA LOB Parameters  
   TYPE tr_m_VALOBParm IS TABLE OF m_VALOBParm;  
   m_m_VALOBParm   m_VALOBParm;  
   obj_m_VALOBParm tr_m_VALOBParm := tr_m_VALOBParm();  
   
   -- NONOCC LOB Parameters  
   TYPE tr_m_NONOCCLOBParm IS TABLE OF m_NONOCCLOBParm;  
   m_m_NONOCCLOBParm   m_NONOCCLOBParm;  
   obj_m_NONOCCLOBParm tr_m_NONOCCLOBParm := tr_m_NONOCCLOBParm();  
   
   -- SYS Parameters  
   TYPE tr_m_SysParms IS TABLE OF m_SysParms;  
   m_m_SysParms   m_SysParms;  
   obj_m_SysParms tr_m_SysParms := tr_m_SysParms();  
   v_counter      NUMBER := 1;  
   
   --- Initlob Internal Variables----  
   v_location           number;  
   collectionInRsv      SYS_PARMS_LOB.COLL_IN_RSV_BAL%type;  
   collectionInIncurred SYS_PARMS_LOB.COLL_IN_INCUR_BAL%type;  
   perrsvcollectionInRsv SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL%type;  
   perrsvcollectionInIncurred SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL%type;  
   reserveTracking      SYS_PARMS_LOB.RESERVE_TRACKING%type;  
   adjRsv               SYS_PARMS_LOB.ADJ_RSV%type;  
   lineOfBusCode        SYS_PARMS_LOB.LINE_OF_BUS_CODE%type;  
   balToZero            SYS_PARMS_LOB.BAL_TO_ZERO%type;  
   negBalToZero         SYS_PARMS_LOB.NEG_BAL_TO_ZERO%type;  
   setToZero            SYS_PARMS_LOB.SET_TO_ZERO%type;  
   estNumofClaims       SYS_PARMS_LOB.EST_NUM_OF_CLAIMS%type;  
   durationCode         SYS_PARMS_LOB.DURATION_CODE%type;  
   ---------------------------------------------  
   ----INITSYSPARAMS Variables  
   sSQL                 varchar2(1000);  
   v_check_error        boolean := TRUE;  
   v_FH_VOIDS_INCLUDE_c boolean; --for conversion from number to boolean  
   v_FH_VOIDS_INCLUDE   Number; --SYS_PARMS.FH_VOIDS_INCLUDE%type;  
   v_FH_ONLY_PRINTED    NUMBER;  
   cnt                  NUMBER;
   
   shift1Name    SYS_PARMS.SHIFT_1_NAME%type;  
   shift2Name    SYS_PARMS.SHIFT_2_NAME%type;  
   shift3Name    SYS_PARMS.SHIFT_3_NAME%type;  
   shift1Start   SYS_PARMS.SHIFT_1_START%type;  
   shift2Start   SYS_PARMS.SHIFT_2_START%type;  
   shift3Start   SYS_PARMS.SHIFT_3_START%type;  
   shift1End     SYS_PARMS.SHIFT_1_END%type;  
   shift2End     SYS_PARMS.SHIFT_2_END%type;  
   shift3End     SYS_PARMS.SHIFT_3_END%type;  
   FIN_HIST_EVAL SYS_PARMS.FIN_HIST_EVAL%type;  
   ESSP          SYS_PARMS.ESSP%type;  
   -----------------------------  
   --Updatefinhistable procedure variables  
   sTime         varchar2(1000);  
   lastdate      varchar2(1000);  
   lnextuniqueid number;  
   retVal        boolean;  
   v_sqlerrm     varchar2(2000);  
   ----  
   --construct finhisttable  
   v_retval   varchar2(100);  
   sSQL2      varchar(2000);  
   sSQL3      varchar(2000);  
   counter    number := 0;  
   totalCount number;  
   v_count    number; -- it is used to take the count of table  
   -----  
   ----variables used for insertdatafromfunds  
   sTemp               varchar2(2000);  
   sTemp2              varchar2(2000);  
   sTemp_RES           Varchar2(2000);  
   iCounter            number := 0;  
   sInsertRCRowId      VARCHAR2(2000);  
   sInsertPolCovLossId VARCHAR2(2000);  
   bIsRecoveryBucket   number;  
   sRCRowId            VARCHAR2(50);  
   sPolCovLossId       VARCHAR2(50);  
   polcvg_loss_row_id  Number;  
   lpolcvg_loss_row_id Number;  
   v_col_exist         number;  
   v_idx_exist         number;  
   -------  
   
   --Variables for updatefinhist  
   lClaimID           varchar2(1000) := -1;  
   lClaimantEID       varchar2(1000) := 0;  
   lUnitID            varchar2(1000) := 0;  
   lReserveTypeCode   varchar2(1000) := 0;  
   lscratch           varchar2(1000);  
   cEvalDate          varchar2(10); -- need to check this variable  
   ptrIx              number; -- need to check this variable  
   sSQL_Insert        varchar2(2000);  
   outstandingBalance double precision;  
   incurred           double precision;  
   CLAIMANT_EID       varchar2(1000);  
   UNIT_ID            varchar2(1000);  
   RESERVE_TYPE_CODE  varchar2(1000);  
   LOB                varchar2(1000);  
   FH_ROW_ID          varchar2(1000);  
   EVAL_DATE          varchar2(10); -- need to check this variable  
   PAID               double precision;  
   COLLECTED          double precision;  
   RESERVE            double precision;  
   bClosed            boolean := FALSE;  
   bCollInRsvBal      boolean;  
   bCollInIncurred    boolean;  
   bPerRsvCollInRsvBal      boolean;  
   bPerRsvCollInIncurred    boolean; 
   temp_incurred      double precision;  
   bBalToZero         boolean;  
   paidBalance        double precision;  
   Type v_prevValues IS VARRAY(50) OF DOUBLE PRECISION;  
   prevValues v_prevValues := v_prevValues();  
   Type v_runningSum IS VARRAY(50) OF DOUBLE PRECISION;  
   runningSum    v_runningSum := v_runningSum();  
   incurredDelta double precision;  
   balanceDelta  double precision;  
   --//Variable will be used to determine whether data row need to be ignored or not.  
   bIgnoreRow boolean := FALSE;  
   TYPE cur_FH2 IS REF CURSOR;  
   c_FH2 cur_FH2;  
   
   BALANCE        double precision;  
   p_sqlerrm_date varchar2(20);  
   ----------  
   ----variables used in updatebackfinhist  
   lscratch    varchar2(1000);  
   count1      varchar2(1000);  
   lClaim_ID   varchar2(2000) := 0;  
   CLAIM_ID    varchar2(2000) := 0;  
   ENTITY_ID   Number := 0; --// mjha3 : 11/26/2008 : MITS 13798  
   v_Entity_Id ENTITY.entity_id%type;  
   v_retval    varchar2(100);  
   v_Claim_id  CLAIM.claim_id%type;  
   
   TYPE cur_TFH IS REF CURSOR;  
   c_TFH      cur_TFH;  
   c_Claim_id CLAIM.claim_id%type;  
   -------  
   -- Cursors used  
   -- Cursor for SYS_PARMS_LOB  
   cursor v_sysParmsLOB_cur is  
     SELECT RESERVE_TRACKING,  
            LINE_OF_BUS_CODE,  
            ADJ_RSV,  
            COLL_IN_RSV_BAL,  
            BAL_TO_ZERO,  
            NEG_BAL_TO_ZERO,  
            SET_TO_ZERO,  
            EST_NUM_OF_CLAIMS,  
            DURATION_CODE,  
            COLL_IN_INCUR_BAL,
            PER_RSV_COLL_IN_RSV_BAL,
            PER_RSV_COLL_IN_INCR_BAL
       FROM SYS_PARMS_LOB  
      WHERE LINE_OF_BUS_CODE IN (241, 242, 243, 844)  
      ORDER BY LINE_OF_BUS_CODE;  
   -- CURSOR for SYS_PARMS  
   Cursor v_sysParms_cur is  
     SELECT SHIFT_1_NAME,  
            SHIFT_2_NAME,  
            SHIFT_3_NAME,  
            SHIFT_1_START,  
            SHIFT_2_START,  
            SHIFT_3_START,  
            SHIFT_1_END,  
            SHIFT_2_END,  
            SHIFT_3_END,  
            FIN_HIST_EVAL,  
            ESSP  
       FROM SYS_PARMS;  
   --------  
   
 begin  
    
   IF p_mode = '1' OR p_mode = '3' OR p_mode = '5' THEN  
     UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '00000000', NEXT_UNIQUE_ID = 1 WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';  
   m_bFullBuild := TRUE;  
   ELSE  
  m_bFullBuild := FALSE;  
   END IF;  
   
   -- Create Zero-Based Financial History  
   IF p_mode = '2' OR p_mode = '3' OR p_mode = '4' OR p_mode = '5' THEN  
     m_bZeroBasedFinHist := TRUE;  
   END IF;  
   IF p_mode = '2' OR p_mode = '3' THEN  
     -- Create Zero-Based Financial History Based On Date Of Claim  
     isEventBased := 0;  
   ELSE  
     IF p_mode = '4' OR p_mode = '5' THEN  
       -- Create Zero-Based Financial History Based On Date Of Event  
       isEventBased := 1;  
     END IF;  
   END IF;  
   
   m_claimid := p_claimid;  
   
   Begin  
     v_location := 2;  
     SELECT DTTM_LAST_UPDATE, NEXT_UNIQUE_ID  
       into sTime, lnextuniqueid  
       FROM GLOSSARY  
      WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';  
   Exception  
     When NO_DATA_Found then  
       lnextuniqueid := 1; -- it is added while full rebuild is running first time then it is setting up  
       sTime         := null;  
       -- the value of lnextuniqueid to 1  
       v_sqlerrm := 'No Data Found in Glossary Table for Financial_Hist Table';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'QueryGlossary Procedure',  
          'Select Statement',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
     When others then  
       v_sqlerrm := SQLERRM;  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'QueryGlossary Procedure',  
          'Select Statement',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   End;  
   
   if (sTime is null or sTime = '') AND (m_bZeroBasedFinHist = FALSE) then  
     sTime := '00000000';  
   end if;  
   -- Kapil Zero Based Code  
   
   -- Populate SYS_PARAMS_LOB for GC, WC, VA and NONOCC  
   OPEN v_sysParmsLOB_cur;  
   LOOP  
     FETCH v_sysParmsLOB_cur  
       INTO reserveTracking,  
            lineOfBusCode,  
            adjrsv,  
            collectioninrsv,  
            baltozero,  
            negbaltozero,  
            settozero,  
            estnumofclaims,  
            durationcode,  
            collectionInIncurred,
            perrsvcollectionInRsv,
            perrsvcollectionInIncurred;  
     EXIT WHEN v_sysParmsLOB_cur%NOTFOUND;  
     case lineOfBusCode  
       when 241 then  
         m_m_GCLOBParm.m_collectionInRsv      := CASE collectioninrsv  
                                                 WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_GCLOBParm.m_collectionInIncurred := CASE collectionInIncurred  
                                                 WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
        m_m_GCLOBParm.m_perrsvcollectionInRsv   := CASE perrsvcollectionInRsv  
                                                 WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END; 
        m_m_GCLOBParm.m_perrsvcollectionInIncurred := CASE perrsvcollectionInIncurred  
                                                 WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END; 
                                                                                                  
         m_m_GCLOBParm.m_reserveTracking      := reserveTracking;  
         m_m_GCLOBParm.m_adjRsv               := adjrsv;  
         m_m_GCLOBParm.m_lineOfBusCode        := lineOfBusCode;  
         m_m_GCLOBParm.m_balToZero            := CASE baltozero  
                                                 WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_GCLOBParm.m_negBalToZero         := negbaltozero;  
         m_m_GCLOBParm.m_setToZero            := settozero;  
         m_m_GCLOBParm.m_estNumofClaims       := estnumofclaims;  
         m_m_GCLOBParm.m_durationCode         := durationcode;  
       when 243 then  
         m_m_WCLOBParm.m_collectionInRsv      := CASE collectioninrsv   
                                                 WHEN 0 THEN  
                                                   FALSE  
                                                 ELSE  
                                                   TRUE  
                                                 END;  
         m_m_WCLOBParm.m_collectionInIncurred := CASE collectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
       m_m_WCLOBParm.m_perrsvcollectionInRsv          := CASE perrsvcollectionInRsv   
                                                 WHEN 0 THEN  
                                                   FALSE  
                                                 ELSE  
                                                   TRUE  
                                                 END;  
         m_m_WCLOBParm.m_perrsvcollectionInIncurred := CASE perrsvcollectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END; 
         m_m_WCLOBParm.m_reserveTracking      := reserveTracking;  
         m_m_WCLOBParm.m_adjRsv               := adjrsv;  
         m_m_WCLOBParm.m_lineOfBusCode        := lineOfBusCode;  
         m_m_WCLOBParm.m_balToZero            := CASE baltozero WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_WCLOBParm.m_negBalToZero         := negbaltozero;  
         m_m_WCLOBParm.m_setToZero            := settozero;  
         m_m_WCLOBParm.m_estNumofClaims       := estnumofclaims;  
         m_m_WCLOBParm.m_durationCode         := durationcode;  
       when 242 then  
         m_m_VALOBParm.m_collectionInRsv      := CASE collectioninrsv WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_VALOBParm.m_collectionInIncurred := CASE collectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
        m_m_VALOBParm.m_perrsvcollectionInRsv          := CASE perrsvcollectionInRsv WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_VALOBParm.m_perrsvcollectionInIncurred := CASE perrsvcollectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_VALOBParm.m_reserveTracking      := reserveTracking;  
         m_m_VALOBParm.m_adjRsv               := adjrsv;  
         m_m_VALOBParm.m_lineOfBusCode        := lineOfBusCode;  
         m_m_VALOBParm.m_balToZero            := CASE baltozero WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_VALOBParm.m_negBalToZero         := negbaltozero;  
         m_m_VALOBParm.m_setToZero            := settozero;  
         m_m_VALOBParm.m_estNumofClaims       := estnumofclaims;  
         m_m_VALOBParm.m_durationCode         := durationcode;  
       when 844 then  
         m_m_NONOCCLOBParm.m_collectionInRsv      := CASE collectioninrsv WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_NONOCCLOBParm.m_collectionInIncurred := CASE collectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
        m_m_NONOCCLOBParm.m_perrsvcollectionInRsv          := CASE perrsvcollectionInRsv WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_NONOCCLOBParm.m_perrsvcollectionInIncurred := CASE perrsvcollectionInIncurred WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_NONOCCLOBParm.m_reserveTracking      := reserveTracking;  
         m_m_NONOCCLOBParm.m_adjRsv               := adjrsv;  
         m_m_NONOCCLOBParm.m_lineOfBusCode        := lineOfBusCode;  
         m_m_NONOCCLOBParm.m_balToZero            := CASE baltozero WHEN 0 THEN  
                                                 FALSE  
                                                 ELSE  
                                                 TRUE  
                                                 END;  
         m_m_NONOCCLOBParm.m_negBalToZero         := negbaltozero;  
         m_m_NONOCCLOBParm.m_setToZero            := settozero;  
         m_m_NONOCCLOBParm.m_estNumofClaims       := estnumofclaims;  
         m_m_NONOCCLOBParm.m_durationCode         := durationcode;  
     end case;  
   END LOOP;  
   CLOSE v_sysParmsLOB_cur;  
   OPEN v_sysParms_cur;  
   LOOP  
     FETCH v_sysParms_cur  
       INTO shift1Name,  
            SHIFT2NAME,  
            SHIFT3NAME,  
            SHIFT1START,  
            SHIFT2START,  
            SHIFT3START,  
            SHIFT1END,  
            SHIFT2END,  
            SHIFT3END,  
            FIN_HIST_EVAL,  
            ESSP;  
     EXIT WHEN v_sysParms_cur%NOTFOUND;  
     m_m_SysParms.m_shift1Name  := SHIFT1NAME;  
     m_m_SysParms.m_shift2Name  := SHIFT2NAME;  
     m_m_SysParms.m_shift3Name  := SHIFT3NAME;  
     m_m_SysParms.m_shift1Start := SHIFT1START;  
     m_m_SysParms.m_shift2Start := SHIFT2START;  
     m_m_SysParms.m_shift3Start := SHIFT3START;  
     m_m_SysParms.m_shift1End   := SHIFT1END;  
     m_m_SysParms.m_shift2End   := SHIFT2END;  
     m_m_SysParms.m_shift3End   := SHIFT3END;  
     case  
       when FIN_HIST_EVAL = 0 then  
         m_m_SysParms.m_useTranDate := TRUE; --it is negation of value i.e. if fin_hist_eval is false then it is true or vice versa  
       else  
         m_m_SysParms.m_useTranDate := FALSE;  
     end case;  
     --Assign Client Name from the sys_parms table, add more cases for other clients as required.  
     case  
       when ESSP = 1740 then  
         --Crawford  
         m_m_SysParms.m_ESSP := 'CRAWFORD';  
       else  
         m_m_SysParms.m_ESSP := '';  
     end case;  
   END LOOP;  
   CLOSE v_sysParms_cur;  
   
   -- See if FH Voids option is present   JP 8/28/98  
   m_m_SysParms.m_includeVoids := FALSE; -- JP 3/19/2003  
   
   Begin  
   v_check_error:=TRUE;  
     Execute IMMEDIATE 'SELECT FH_VOIDS_INCLUDE FROM SYS_PARMS'  
       into v_FH_VOIDS_INCLUDE;  
   
     if v_FH_VOIDS_INCLUDE = 0 then  
       v_FH_VOIDS_INCLUDE_c := FALSE;  
     else  
       v_FH_VOIDS_INCLUDE_c := TRUE;  
     end if;  
   
   Exception  
     When No_DATA_Found then  
       v_check_error               := false;  
       m_m_SysParms.m_includeVoids := FALSE;  
     When TOO_MANY_ROWS then  
       v_check_error               := false;  
       m_m_SysParms.m_includeVoids := FALSE;  
     When OTHERS then  
       v_check_error               := false;  
       m_m_SysParms.m_includeVoids := FALSE;  
   END;  
   
   if v_check_error then  
     m_m_SysParms.m_includeVoids := (v_FH_VOIDS_INCLUDE_c != FALSE); --'0' is used for false  
   end if;  
   
   -- See if option to include only printed checks is present   JP 7/24/2003  
   m_m_SysParms.m_includeOnlyPrintedChecks := FALSE;  
   Begin  
   v_check_error:=TRUE;  
     execute immediate 'SELECT FH_ONLY_PRINTED FROM SYS_PARMS'  
       into v_FH_ONLY_PRINTED;  
   Exception  
     When No_DATA_Found then  
       v_check_error                           := false;  
       m_m_SysParms.m_includeOnlyPrintedChecks := FALSE;  
     When TOO_MANY_ROWS then  
       v_check_error                           := false;  
       m_m_SysParms.m_includeOnlyPrintedChecks := FALSE;  
     When OTHERS then  
       v_check_error                           := false;  
       m_m_SysParms.m_includeOnlyPrintedChecks := FALSE;  
   END;  
   
   if v_check_error then  
     IF v_FH_ONLY_PRINTED <> 0 THEN  
         m_m_SysParms.m_includeOnlyPrintedChecks := TRUE;  
     ELSE  
         m_m_SysParms.m_includeOnlyPrintedChecks := FALSE;  
     END IF;  
   end if;  
   
   -- Below code is added to take out the value of param value of 'MULTI_COVG_CLM'  
   -- Start Here -- Dated: 12-Dec-2013  
   SELECT str_parm_value --parm_name,str_parm_value,parm_desc  
     into m_str_parm_value  
     FROM Parms_Name_Value  
    where parm_name = 'MULTI_COVG_CLM';  
   --End Here -- Dated: 12-Dec-2013  
   
   IF (m_str_parm_value <> '0') then  
     sRCRowId            := ' , rc_row_id';  
     sInsertRCRowId      := ' , NVL((SELECT Distinct RC_ROW_ID from RESERVE_CURRENT where RESERVE_CURRENT.CLAIM_ID = RESERVE_HISTORY.CLAIM_ID AND RESERVE_CURRENT.CLAIMANT_EID = RESERVE_HISTORY.CLAIMANT_EID AND RESERVE_CURRENT.UNIT_ID = RESERVE_HISTORY.UNIT_ID AND RESERVE_CURRENT.RESERVE_TYPE_CODE = RESERVE_HISTORY.RESERVE_TYPE_CODE AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = RESERVE_HISTORY.POLCVG_LOSS_ROW_ID),0) RC_ROW_ID ';  
     sPolCovLossId       := ' , polcvg_loss_row_id';  
     sInsertPolCovLossId := ' , NVL((SELECT POLCVG_LOSS_ROW_ID from RESERVE_CURRENT  where RESERVE_CURRENT.RC_ROW_ID = FTS.RC_ROW_ID),0) POLCVG_LOSS_ROW_ID ';  
   ELSE  
     sRCRowId            := '';  
     sPolCovLossId       := '';  
     sInsertRCRowId      := '';  
     sInsertPolCovLossId := '';  
   END if;  
   
   -- create Financial history table if 1. First Time or 2. Previous updation caused was terminated  
   select to_char(systimestamp, 'YYYYmmddhhmmss')  
     into p_sqlerrm_date  
     from dual;  
   
   p_sqlerrm := p_sqlerrm_date || ':' ||  
                ' DROP TABLES FH1,FH2,FH3 - START' || '|';  
   -- drop temp tables  
   sSQL  := 'DROP TABLE FH1';  
   sSQL2 := 'DROP TABLE FH2';  
   sSQL3 := 'DROP TABLE FH3';  
   
   v_location := 2;  
   begin  
     Execute Immediate sSQL;  
   exception  
     WHEN Others then  
       v_sqlerrm := SQLERRM;  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'constructFinHistTable Procedure',  
          'DROP TABLE FH1 Failed..',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
       v_sqlerrm := 'DROP TABLE FH1 Failed.';  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    v_sqlerrm || '|';  
   end;  
   
   v_location := 3;  
   
   begin  
     Execute Immediate sSQL2;  
   exception  
     WHEN Others then  
       v_sqlerrm := SQLERRM;  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'constructFinHistTable Procedure',  
          'DROP TABLE FH2 Failed..',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
       v_sqlerrm := 'DROP TABLE FH2 Failed.';  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                    v_sqlerrm || '|';  
   end;  
   
   v_location := 4;  
   begin  
     Execute Immediate sSQL3;  
   exception  
     WHEN Others then  
       v_sqlerrm := SQLERRM;  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'constructFinHistTable Procedure',  
          'DROP TABLE FH3 Failed..',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
       v_sqlerrm := 'DROP TABLE FH3 Failed.';  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                    v_sqlerrm || '|';  
   end;  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' DROP TABLES FH1,FH2,FH3 - END' || '|';  
   lastdate  := sTime; --9-dec-2013  
   
   if (lastDate = '00000000') then  
     v_location := 5;  
     Begin  
       execute immediate 'select count(1) from user_tables where table_name=:1'  
         into v_count  
         using 'FINANCIAL_HIST'; -- where table_name='||''''||tTable||'''';  
     exception  
       when no_data_found then  
         v_count   := 0;  
         v_sqlerrm := SQLERRM;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'constructFinHistTable Procedure',  
            'Calling of checkTable procedure from constructFinHistTable got Failed..',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
         v_sqlerrm := 'Calling of checkTable procedure from UpdateBackFinHist got Failed..';  
         p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                      v_sqlerrm || '|';  
       WHEN Others then  
         v_count   := 0;  
         v_sqlerrm := SQLERRM;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'constructFinHistTable Procedure',  
            'Calling of checkTable procedure from constructFinHistTable got Failed..',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
         v_sqlerrm := 'Calling of checkTable procedure from UpdateBackFinHist got Failed..';  
         p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                      v_sqlerrm || '|';  
     end;  
   
     if (v_count = 1) then  
       -- need to check this  
       -- it is calling another function checktable  
       -- how it is doing comparision here with &m_db  
       --{  
       -- drop indexes first  
   
       -- Start MITS 6987  
       -- Modified By: Ankur Saxena 05/16/2006  
       -- Reason for change: In Oracle, all constraints and indexes are automatically dropped if table is dropped.  
       -- But if dont want to drop the table, truncate is another option. Drop all indexes and constraints  
       -- after table has been truncated.  
   
       Begin  
         v_location := 6;  
         p_sqlerrm  := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                       ' TRUNCATE FINANCIAL_HIST START' || '|';  
         Execute Immediate 'TRUNCATE TABLE FINANCIAL_HIST';  
         p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                      ' TRUNCATE FINANCIAL_HIST END' || '|';  
       exception  
         WHEN Others then  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'constructFinHistTable Procedure',  
              'TRUNCATE TABLE FINANCIAL_HIST got Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
           v_sqlerrm := 'TRUNCATE TABLE FINANCIAL_HIST got Failed..';  
           p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                        v_sqlerrm || '|';  
       end;  
   
       -- Below query is added to check polcvg_loss_id col exists or not  
       IF (m_str_parm_value <> '0') then  
         select count(1)  
           into v_col_exist  
           from user_tab_columns  
          where table_name = 'FINANCIAL_HIST'  
            and column_name = 'POLCVG_LOSS_ROW_ID';  
         IF V_COL_EXIST = 0 THEN  
           EXECUTE IMMEDIATE 'ALTER TABLE FINANCIAL_HIST ADD (POLCVG_LOSS_ROW_ID NUMBER NOT NULL)';  
         END IF;  
       END IF;  
       -- Above query is added to check polcvg_loss_id col exists or not  
   
       Begin  
         v_location := 7;  
         Execute Immediate 'DROP INDEX FH_IDX1';  
       exception  
         WHEN Others then  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'constructFinHistTable Procedure',  
              'DROP INDEX FH_IDX1 got Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
           v_sqlerrm := 'DROP INDEX FH_IDX1 got Failed..';  
           p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                        v_sqlerrm || '|';  
       end;  
   
       Begin  
         v_location := 8;  
         Execute Immediate 'DROP INDEX FH_IDX2';  
       exception  
         WHEN Others then  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'constructFinHistTable Procedure',  
              'DROP INDEX FH_IDX2 got Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
           v_sqlerrm := 'DROP INDEX FH_IDX2 got Failed..';  
           p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                        v_sqlerrm || '|';  
       end;  
   
     else  
       select to_char(systimestamp, 'YYYYmmddhhmmss')  
         into p_sqlerrm_date  
         from dual;  
   
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' CREATE FINANCIAL_HIST START' || '|';  
       IF (m_str_parm_value = '0') then  
         sSQL := 'CREATE TABLE FINANCIAL_HIST (FH_ROW_ID NUMBER(10), EVAL_DATE VARCHAR2(8) NOT NULL, CLAIM_ID NUMBER(10) NOT NULL, CLAIMANT_EID NUMBER(10) NOT NULL, UNIT_ID NUMBER(10) NOT NULL, RESERVE_TYPE_CODE NUMBER(10) NOT NULL, PAID NUMBER, COLLECTED NUMBER, RESERVE NUMBER, BALANCE NUMBER, INCURRED NUMBER)';  
       else  
         sSQL := 'CREATE TABLE FINANCIAL_HIST (FH_ROW_ID NUMBER(10), EVAL_DATE VARCHAR2(8) NOT NULL, CLAIM_ID NUMBER(10) NOT NULL, CLAIMANT_EID NUMBER(10) NOT NULL, UNIT_ID NUMBER(10) NOT NULL, RESERVE_TYPE_CODE NUMBER(10) NOT NULL, PAID NUMBER, COLLECTED NUMBER, RESERVE NUMBER, BALANCE NUMBER, INCURRED NUMBER  
          , POLCVG_LOSS_ROW_ID NUMBER NOT NULL)';  
       end if;  
   
       Begin  
         v_location := 9;  
         Execute Immediate sSQL;  
       Exception  
         when Others then  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'constructFinHistTable Procedure',  
              'CREATE TABLE FINANCIAL_HIST got Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
           v_sqlerrm := 'CREATE TABLE FINANCIAL_HIST got Failed..';  
           p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                        v_sqlerrm || '|';  
       end;  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' CREATE FINANCIAL_HIST END' || '|';  
     end if;  
     sTemp     := ' ';  
     sTemp2    := ' ';  
     sTemp_RES := ' ';  
   else  --  financial hist app executing some other time  
     -- Below query is added to check polcvg_loss_id col exists or not  
     IF (m_str_parm_value <> '0') then  
       select count(1)  
         into v_col_exist  
         from user_tab_columns  
        where table_name = 'FINANCIAL_HIST'  
          and column_name = 'POLCVG_LOSS_ROW_ID';  
       IF V_COL_EXIST = 0 THEN  
         EXECUTE IMMEDIATE 'ALTER TABLE FINANCIAL_HIST ADD (POLCVG_LOSS_ROW_ID NUMBER NOT NULL)';  
       END IF;  
     END IF;  
     -- Above query is added to check polcvg_loss_id col exists or not  
     sSQL := 'DROP TABLE TEMP_FINHIST';  
     Begin  
       execute immediate sSQL;  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'DROP TABLE TEMP_FINHIST Failed...',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     end;  
   
     v_location := 2;  
     sSQL       := 'DROP TABLE TEMP_FH';  
     Begin  
       execute immediate sSQL;  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'DROP TABLE TEMP_FH Failed...',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     end;  
   
     sSQL := 'CREATE TABLE TEMP_FINHIST (CLAIM_ID NUMBER(10))';  
   
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'CREATE TABLE TEMP_FINHIST Failed...',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
         retval := FALSE;  
         if retval then  
           p_retval := 'TRUE';  
         else  
           p_retval := 'FALSE';  
         end if;  
     end;  
   
     v_location := 4;  
     sSQL       := 'CREATE TABLE TEMP_FH (CLAIM_ID NUMBER(10))';  
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         retval := FALSE;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'CREATE TABLE TEMP_FH Failed...',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
   
         if retval then  
           p_retval := 'TRUE';  
         else  
           p_retval := 'FALSE';  
         end if;  
     end;  
   
     --// Financial History Enhancement to accomodate Department wise Update.  
     if (m_DeptAbbr != '' or m_DeptAbbr is not null) then  
       v_location := 5;  
       sSQL       := 'SELECT ENTITY_ID FROM ENTITY WHERE LOWER(ABBREVIATION) = :1 AND ENTITY_TABLE_ID = 1012';  
       begin  
         execute immediate sSQL  
           into v_Entity_Id  
           using m_DeptAbbr;  
       exception  
         when No_data_found then  
           retval    := False;  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'UpdateBackFinHist Procedure',  
              'DEPARTMENT DOES NOT EXIST..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
           v_sqlerrm := 'DEPARTMENT DOES NOT EXIST.';  
           if retval then  
             p_retval := 'TRUE';  
           else  
             p_retval  := 'FALSE';  
             p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                          v_sqlerrm || '|';  
           end if;  
         when others then  
           retval    := FALSE;  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'UpdateBackFinHist Procedure',  
              'DEPARTMENT DOES NOT EXIST..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
       end;  
   
       if (Not retval) then  
         Return;  
       else  
         if (v_ENTITY_ID = 0) then  
           v_location := 7;  
           -- THe above conversion is not required as it is just converting the  
           -- integer value to a string and also ENTITY_ID is not used in the below code  
   
           retVal := FALSE;  
           if retval then  
             p_retval := 'TRUE';  
           else  
             p_retval := 'FALSE';  
           end if;  
         else  
           v_location := 8;  
           -- THe above conversion is not required as it is just converting the  
           -- integer value to a string and also ENTITY_ID is not used in the below code  
   
           sSQL := 'INSERT INTO TEMP_FH SELECT DISTINCT (CLAIM_ID) FROM CLAIM CL, EVENT EV, ENTITY EN ';  
           sSQL := sSQL ||  
                   'WHERE CL.EVENT_NUMBER = EV.EVENT_NUMBER AND EV.DEPT_EID = EN.ENTITY_ID ';  
           sSQL := sSQL || 'AND LOWER(EN.ABBREVIATION) = ' || m_DeptAbbr;  
           sSQL := sSQL || ' AND EN.ENTITY_TABLE_ID = 1012';  
   
           begin  
             execute immediate sSQL;  
           exception  
             when others then  
               v_sqlerrm := SQLERRM;  
               insert into fin_hist_err_log  
                 (job_id,  
                  s_no,  
                  Obj_name,  
                  err_statement,  
                  loc,  
                  err_msg,  
                  create_date)  
               values  
                 (p_jobID,  
                  fin_hist_err_log_seq.Nextval,  
                  'UpdateBackFinHist Procedure',  
                  'INSERT INTO TEMP_FH Failed..',  
                  v_location,  
                  v_SQLERRM,  
                  sysdate);  
               commit;  
               v_sqlerrm := 'INSERT INTO TEMP_FH Failed..';  
               retVal    := FALSE;  
               if retval then  
                 p_retval := 'TRUE';  
               else  
                 p_retval := 'FALSE';  
               end if;  
           end;  
         end if;  
       end if;  
     end if;  
   
     --// This case has been added if a sigle Claim Number is passed.  
     -- Kapil Dated: 17-Dec-2013  
     sSQL := '';  
     --Kapil -- Below code is added for claim id is not null scenario  
     if (m_claimid is not null) THEN  
       --INSERT INTO TEMP_FH SELECT CLAIM_ID FROM CLAIM WHERE LOWER(CLAIM_ID) = m_claimid;  
       INSERT INTO TEMP_FH values (m_claimid);  
     end IF;  
     --Kapil -- Above code is added for claim id is not null scenario  
   
     --// Dept. Abbreviation case has been added here.  
     sSQL := '';  
     if ((m_claimid = '' and m_DeptAbbr = '') or  
        (m_claimid is null and m_DeptAbbr is null)) then  
       sSQL := 'INSERT INTO TEMP_FH SELECT DISTINCT CLAIM_ID FROM FUNDS WHERE DTTM_RCD_LAST_UPD >= :01'; --||''''||lastDate||''''||;  
       --sSQL := 'INSERT INTO TEMP_FH SELECT DISTINCT CLAIM_ID FROM FUNDS WHERE claim_id=4' ;/*49918,12194*/ --||''''||lastDate||''''||;  
       v_location := 11;  
       begin  
         -- execute immediate sSQL;  
         execute immediate sSQL  
           using lastdate;  
       exception  
         when others then  
           v_sqlerrm := SQLERRM;  
   
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'UpdateBackFinHist Procedure',  
              'INSERT INTO TEMP_FH Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
   
           v_sqlerrm := 'INSERT INTO TEMP_FH Failed..';  
           p_sqlerrm := p_sqlerrm  || v_sqlerrm;  
           retVal    := FALSE;  
           if retval then  
             p_retval := 'TRUE';  
           else  
             p_retval := 'FALSE';  
           end if;  
       end;  
   
       --// MITS 12958 Manish C Jha 08/08/2008  
       --// This case has been added if a sigle Claim Number is passed.  
   
       v_location := 12;  
   
       sSQL := '';  
       sSQL := 'INSERT INTO TEMP_FH SELECT DISTINCT CLAIM_ID FROM RESERVE_HISTORY where DTTM_RCD_LAST_UPD >= :01'; --claim_id=12194' ;--||''''||lastDate||''''||;  
       --sSQL := 'INSERT INTO TEMP_FH SELECT DISTINCT CLAIM_ID FROM RESERVE_HISTORY where claim_id=4' ;--||''''||lastDate||''''||;  
       begin  
         --execute immediate sSQL;  
         execute immediate sSQL  
           using lastdate;  
       exception  
         when others then  
           v_sqlerrm := SQLERRM;  
           insert into fin_hist_err_log  
             (job_id,  
              s_no,  
              Obj_name,  
              err_statement,  
              loc,  
              err_msg,  
              create_date)  
           values  
             (p_jobID,  
              fin_hist_err_log_seq.Nextval,  
              'UpdateBackFinHist Procedure',  
              'INSERT INTO TEMP_FH Failed..',  
              v_location,  
              v_SQLERRM,  
              sysdate);  
           commit;  
   
           v_sqlerrm := 'INSERT INTO TEMP_FH Failed..';  
           p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                        v_sqlerrm || '|';  
   
           retVal := FALSE;  
           if retval then  
             p_retval := 'TRUE';  
           else  
             p_retval := 'FALSE';  
           end if;  
       end;  
     end if;  
     v_location := 13;  
     sSQL       := 'INSERT INTO TEMP_FINHIST SELECT DISTINCT CLAIM_ID FROM TEMP_FH';  
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         v_sqlerrm := SQLERRM;  
   
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'INSERT INTO TEMP_FH Failed..',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
   
         v_sqlerrm := 'INSERT INTO TEMP_FH Failed..';  
         p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' || ' ' ||  
                      v_sqlerrm || '|';  
   
         retVal := FALSE;  
         if retval then  
           p_retval := 'TRUE';  
         else  
           p_retval := 'FALSE';  
         end if;  
     end;  
     --//Start FIN_HIST_LOG  //Ankur Saxena 09/06/2006  
     if (m_bFinHistLog = TRUE) then  
       v_location := 15;  
   
       sSQL := 'SELECT CLAIM_ID FROM TEMP_FINHIST';  
       sSQL := sSQL || ' ORDER BY CLAIM_ID';  
   
       Execute Immediate 'Truncate table TEMP_FINHIST_CLAIMID_RUNTIME';  
   
       OPEN c_TFH FOR sSQL;  
       LOOP  
         FETCH c_TFH  
           INTO c_CLAIM_ID;  
         EXIT WHEN c_TFH%NOTFOUND;  
         Insert into TEMP_FINHIST_CLAIMID_RUNTIME values (c_CLAIM_ID);  
         --Added as per discussion happened with pawan sir on 2-Dec-2013  
       End Loop;  
       CLOSE c_TFH;  
     end if;  
     --//End FIN_HIST_LOG  
     /*sSQL       := 'CREATE UNIQUE INDEX TEMP_FINIDX ON TEMP_FINHIST (CLAIM_ID)';  
     v_location := 16;  
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         v_sqlerrm := SQLERRM;  
   
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'CREATE UNIQUE INDEX TEMP_FINIDX Failed..',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
   
         v_sqlerrm := 'CREATE UNIQUE INDEX TEMP_FINIDX Failed..';  
         p_sqlerrm := p_sqlerrm  || v_sqlerrm;  
   
         retVal := FALSE;  
         if retval then  
           p_retval := 'TRUE';  
         else  
           p_retval := 'FALSE';  
         end if;  
     end;*/  
     sSQL := 'DELETE FROM FINANCIAL_HIST WHERE CLAIM_ID IN (SELECT CLAIM_ID FROM TEMP_FINHIST)';  
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         v_sqlerrm := SQLERRM;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateBackFinHist Procedure',  
            'DELETE FROM FINANCIAL_HIST Failed..',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
   
         v_sqlerrm := 'DELETE FROM FINANCIAL_HIST Failed..';  
         p_sqlerrm := p_sqlerrm  || v_sqlerrm;  
   
         retVal := FALSE;  
         if retval then  
           p_retval := 'TRUE';  
         else  
           p_retval := 'FALSE';  
         end if;  
     end;  
     sTemp     := ',TEMP_FINHIST';  
     sTemp2    := ' AND TEMP_FINHIST.CLAIM_ID = F.CLAIM_ID ';  
     sTemp_RES := ',TEMP_FINHIST WHERE TEMP_FINHIST.CLAIM_ID = RESERVE_HISTORY.CLAIM_ID';  
   end if;  
   IF (m_str_parm_value <> '0') then  
    BEGIN
    SELECT COUNT(1) into v_col_exist FROM USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'PK_FINANCIAL_HIST' AND table_name = 'FINANCIAL_HIST';
    IF v_col_exist = 1 THEN
      select COUNT(1) into v_col_exist 
      from ALL_IND_COLUMNS 
      where table_name = 'FINANCIAL_HIST' 
      and index_name = 'PK_FINANCIAL_HIST'
      and column_name = 'POLCVG_LOSS_ROW_ID'
      and index_owner = (SELECT SYS_CONTEXT ('userenv', 'current_schema') FROM DUAL);
      IF v_col_exist = 0 THEN
        sSql :='alter table FINANCIAL_HIST DROP constraint PK_FINANCIAL_HIST';
        execute immediate sSQL;
      END IF;
    END IF;
    SELECT COUNT(1) into v_col_exist FROM USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'PK_FINANCIAL_HIST' AND table_name = 'FINANCIAL_HIST';
    IF v_col_exist = 0 THEN
        sSql :='alter table FINANCIAL_HIST add constraint PK_FINANCIAL_HIST primary key(EVAL_DATE,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,POLCVG_LOSS_ROW_ID)';
        execute immediate sSQL;
     END IF;
      Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'Create Primary Key PK_FINANCIAL_HIST',  
            'Create Primary Key PK_FINANCIAL_HIST Failed...',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;
      END;
   END IF;
   -- END Here UPDATEBACKFINHIST  
   
   --Code start here for insertdatafromfunds  
   sSQL := 'CREATE TABLE FH1  
             (EVAL_DATE VARCHAR2(8),  
             CLAIM_ID NUMBER(10),  
             CLAIMANT_EID NUMBER(10),  
             UNIT_ID NUMBER(10),  
             RESERVE_TYPE_CODE   NUMBER(10),  
             LOB NUMBER(10),  
             PAID NUMBER,  
             COLLECTED NUMBER,  
             BALANCE NUMBER,  
             RESERVE NUMBER,  
             INCURRED NUMBER ';  
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || ', RC_ROW_ID Number  
            , polcvg_loss_row_id Number';  
   end if;  
   sSQL := sSQL || ')  NOLOGGING ';  
   
   Begin  
     v_location := 2;  
     p_sqlerrm  := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                   ' CREATE FH1 START' || '|';  
     Execute immediate sSQL;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' CREATE FH1 END' || '|';  
     p_retval  := 'TRUE';  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'InsertDataFromFunds Procedure',  
          'Create Table FH1',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   End;  
   
   -- ... get payments  
   sSQL := 'INSERT INTO FH1 SELECT ';  
   -- Ankur CRAWFORD_FINANCIAL_HISTORY  
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     v_location := 4;  
     sSQL       := sSQL || 'F.FIN_IMPACT_DATE'; -- Crawford takes FIN_IMPACT_DATE instead of TRANS_DATE or DATE_OF_CHECK  
   else  
     case  
       when m_m_SysParms.m_useTranDate then  
         sSQL := sSQL || ' F.TRANS_DATE ';  
       else  
         sSQL := sSQL || ' F.DATE_OF_CHECK ';  
     end case;  
   end if;  
   v_location := 6;  
   sSQL       := sSQL ||  
                 ' EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, 0 LOB, SUM(FTS.AMOUNT) PAID,0.0 COLLECTED ,0.0 BALANCE,';  
   sSQL       := sSQL || '0.0 RESERVE,0.0 INCURRED ';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
   end if;  
   
   sSQL := sSQL || ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' || sTemp ||  
           ' WHERE F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
   
   -- Ankur CRAWFORD_FINANCIAL_HISTORY  
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL '; -- Only transactions that does not have BLANK FIN_IMPACT_DATE  
   end if;  
   
   if (Not m_m_SysParms.m_includeVoids) then  
     sSQL := sSQL || ' AND F.VOID_FLAG = 0';  
   end if;  
   
   if (m_m_SysParms.m_includeOnlyPrintedChecks) then  
     sSQL := sSQL || ' AND F.STATUS_CODE = 1054'; -- JP 7/24/2003   Option added to allow for only printed checks to be included in FH  
   end if;  
   
   -- Ankur CRAWFORD_FINANCIAL_HISTORY  
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     -- Treat external payments and service drafts as payments in financial history  
     sSQL := sSQL ||  
             ' AND (COLLECTION_FLAG=0 OR COLLECTION_FLAG IS NULL) GROUP BY ';  
     sSQL := sSQL || ' F.FIN_IMPACT_DATE ';  
   else  
     sSQL := sSQL || ' AND F.PAYMENT_FLAG <> 0 GROUP BY ';  
     case  
       when m_m_SysParms.m_useTranDate then  
         sSQL := sSQL || ' F.TRANS_DATE ';  
       else  
         sSQL := sSQL || ' F.DATE_OF_CHECK ';  
     end case;  
     -- need to check the above sql statement  
   end if;  
   v_location := 9;  
   sSQL       := sSQL ||  
                 ', F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || ', FTS.RC_ROW_ID ';  
   end if;  
   
   --sSQL       := Replace(sSQL, chr(39), ' ');  
   --sSQL       := Replace(sSQL, '#', '');  
   v_location := 10;  
   Begin  
     select to_char(systimestamp, 'YYYYmmddhhmmss')  
       into p_sqlerrm_date  
       from dual;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' INSERT INTO FH1 GET PAYMENTS START' || '|';  
     Execute Immediate sSQL;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' INSERT INTO FH1 GET PAYMENTS END' || '|';  
     p_retval  := 'TRUE';  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'InsertDataFromFunds Procedure',  
          'INSERT INTO FH1 - Get payments transactions:' || sSQL,  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   End;  
   
   -- ... get collections  
   sSQL := 'INSERT INTO FH1 ';  
   sSQL := sSQL || ' SELECT ';  
   
   -- Ankur CRAWFORD_FINANCIAL_HISTORY  
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     v_location := 12;  
     sSQL       := sSQL || 'F.FIN_IMPACT_DATE'; -- FIN_IMPACT_DATE for crawford instead of TRANS_DATE" or DATE_OF_CHECK  
   else  
     v_location := 13;  
     case  
       when m_m_SysParms.m_useTranDate then  
         sSQL := sSQL || ' F.TRANS_DATE ';  
       else  
         sSQL := sSQL || ' F.DATE_OF_CHECK ';  
     end case;  
     -- Need to check the above statement and need to modify it  
   end if;  
   
   sSQL := sSQL ||  
           ' EVAL_DATE ,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID,FTS.RESERVE_TYPE_CODE,0 LOB, 0.0 PAID,SUM(FTS.AMOUNT) COLLECTED,0.0 BALANCE ,';  
   sSQL := sSQL || ' 0.0 RESERVE,0.0 INCURRED  ';  
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
   end if;  
   sSQL := sSQL || ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' || sTemp ||  
           ' WHERE F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
   
   if (not m_m_SysParms.m_includeVoids) then  
     sSQL := sSQL || ' AND F.VOID_FLAG = 0'; -- Screen out voids unless option says to keep them around (with a negative  
     -- transaction on the void date)   JP 8/28/98  
   end if;  
   v_location := 15;  
   -- Ankur CRAWFORD_FINANCIAL_HISTORY  
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     v_location := 16;  
     --Only transactions that does not have blank FIN_IMPACT_DATE  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL';  
   
     -- Treat external payments and service drafts as payments. All other are collections.  
     sSQL := sSQL || ' AND (COLLECTION_FLAG=-1) GROUP BY ';  
     sSQL := sSQL || ' F.FIN_IMPACT_DATE ';  
   else  
     v_location := 17;  
     sSQL       := sSQL || ' AND F.PAYMENT_FLAG = 0 GROUP BY ';  
     case  
       when m_m_SysParms.m_useTranDate then  
         sSQL := sSQL || ' F.TRANS_DATE ';  
       else  
         sSQL := sSQL || ' F.DATE_OF_CHECK ';  
     end case;  
     --Need to check the above statement  
   end if;  
   sSQL := sSQL ||  
           ', F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ';  
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || ', FTS.RC_ROW_ID ';  
   end if;  
   --sSQL := Replace(sSQL, chr(39), ' ');  
   --sSQL := Replace(sSQL, '#', '');  
   
   v_location := 19;  
   Begin  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' INSERT INTO FH1 GET COLLECTION START' || '|';  
     EXECUTE IMMEDIATE sSQL;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' INSERT INTO FH1 GET COLLECTION END' || '|';  
     p_retval  := 'TRUE';  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'InsertDataFromFunds Procedure',  
          'INSERT INTO FH1 - 2 - Get collection transactions:' || sSQL,  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   End;  
   
   v_location := 21;  
   -- ... Process voids as negative transactions (if option set)  
   if (m_m_SysParms.m_includeVoids) then  
     -- ... handle voided payments  
     v_location := 22;  
     sSQL       := 'INSERT INTO FH1 SELECT ';  
     sSQL       := sSQL || 'F.VOID_DATE '; -- use void date as eval date  
     sSQL       := sSQL ||  
                   ' EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, 0 LOB,(-1.0 * SUM(FTS.AMOUNT)) PAID,0.0 COLLECTED,0.0 BALANCE, ';  
     sSQL       := sSQL || '0.0  RESERVE,0.0 INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
     end if;  
   
     sSQL := sSQL || ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS' || sTemp ||  
             ' WHERE F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL || ' AND F.VOID_FLAG <> 0';  
   
     if (m_m_SysParms.m_includeOnlyPrintedChecks) then  
       sSQL := sSQL || ' AND F.STATUS_CODE = 1054 '; -- JP 7/24/2003   Option added to allow for only                              //printed checks to be included in FH  
     end if;  
   
     -- Ankur CRAWFORD_FINANCIAL_HISTORY  
     if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
       v_location := 24;  
       -- Only transactions that does not have blank FIN_IMPACT_DATE  
       sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL ';  
       -- Treat external payments and service drafts as payments.  
       sSQL := sSQL ||  
               ' AND (COLLECTION_FLAG=0 OR COLLECTION_FLAG IS NULL) GROUP BY ';  
     else  
       v_location := 25;  
       sSQL       := sSQL || ' AND F.PAYMENT_FLAG <> 0 GROUP BY ';  
     end if;  
   
     sSQL := sSQL ||  
             ' F.VOID_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ';  
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
     --sSQL       := Replace(sSQL, chr(39), ' ');  
     --sSQL       := Replace(sSQL, '#', '');  
     v_location := 26;  
   
     Begin  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' INSERT INTO FH1 HANDLE VOIDED PAYMENT START' || '|';  
       Execute Immediate sSQL;  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' INSERT INTO FH1 HANDLE VOIDED PAYMENT END' || '|';  
       p_retval  := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 3 - Handle voided payments......' || ssql,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
     -- ... handle voided collections  
   
     sSQL := 'INSERT INTO FH1 ';  
     sSQL := sSQL || ' SELECT ';  
     sSQL := sSQL || ' F.VOID_DATE '; -- use void date as eval date  
     sSQL := sSQL ||  
             ' EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID,FTS.RESERVE_TYPE_CODE,0 LOB, 0.0 PAID,(-1.0 * SUM(FTS.AMOUNT)) COLLECTED,0.0 BALANCE, ';  
     sSQL := sSQL || ' 0.0 RESERVE,0.0  INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
     end if;  
   
     sSQL := sSQL || ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' || sTemp ||  
             ' WHERE F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL || ' AND F.VOID_FLAG <> 0 ';  
     -- Ankur CRAWFORD_FINANCIAL_HISTORY  
     if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
       -- Only transactions that does not have blank fin_impact_date  
       sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL ';  
       -- Payment=payments, external payments, service drafts other wise collections  
       sSQL := sSQL || ' AND (COLLECTION_FLAG=-1) GROUP BY ';  
     else  
       sSQL := sSQL || ' AND F.PAYMENT_FLAG = 0 GROUP BY ';  
     end if;  
   
     sSQL := sSQL ||  
             'F.VOID_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
     --sSQL := Replace(sSQL, chr(39), ' ');  
     --sSQL := Replace(sSQL, '#', '');  
   
     Begin  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' INSERT INTO FH1 HANDLE VOIDED COLLECTION START' || '|';  
       EXECUTE IMMEDIATE sSQL;  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' INSERT INTO FH1 HANDLE VOIDED COLLECTION END' || '|';  
       p_retval  := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 4 - Handle voided collections......' || sSQL,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
   end if;  
   
   -- Process Stop Pay transactions as negative transactions  
   
   if (m_m_SysParms.m_ESSP = 'CRAWFORD') then  
     v_location := 31;  
     --Handle Payments with Stop Pay status "Accepted"  
     sSQL := 'INSERT INTO FH1 SELECT ';  
     sSQL := sSQL || ' SPHIST.DATE_STATUS_CHGD '; -- Use Date Status Changed as Eval Date  
     sSQL := sSQL ||  
             ' EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, 0 LOB,(-1.0 * SUM(FTS.AMOUNT)) PAID,0.0  COLLECTED,0.0  BALANCE,';  
     sSQL := sSQL || '0.0 RESERVE,0.0 INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
     end if;  
   
     sSQL := sSQL ||  
             ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, STOP_PAY_HIST SPHIST ' ||  
             sTemp || ' WHERE     F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL ||  
             ' AND F.TRANS_ID=SPHIST.ORIGINAL_ID AND SPHIST.STATUS_CODE = 6935'; -- "Accepted" status  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL ';  
   
     if (Not m_m_SysParms.m_includeVoids) then  
       sSQL := sSQL || ' AND F.VOID_FLAG = 0 ';  
     end if;  
   
     if (m_m_SysParms.m_includeOnlyPrintedChecks) then  
       sSQL := sSQL || ' AND F.STATUS_CODE = 1054 '; -- Option added to allow for only printed checks to be included in FH  
     end if;  
   
     sSQL := sSQL ||  
             ' AND (COLLECTION_FLAG=0 OR COLLECTION_FLAG IS NULL) GROUP BY ';  
     sSQL := sSQL ||  
             ' F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE, SPHIST.DATE_STATUS_CHGD ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
   
     --sSQL       := Replace(sSQL, chr(39), ' ');  
     --sSQL       := Replace(sSQL, '#', '');  
     v_location := 34;  
   
     Begin  
       EXECUTE IMMEDIATE sSQL;  
       p_retval := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 5 - Handle Payments with Stop Pay status ACCEPTED' || sSQL,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
   
     -- Handle Payments with Stop Pay status "Undo/Released"  
     sSQL := 'INSERT INTO FH1 SELECT ';  
     sSQL := sSQL || 'SPHIST.DATE_STATUS_CHGD '; --Use Date Status Changed as Eval Date  
     sSQL := sSQL ||  
             'EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, 0 LOB, SUM(FTS.AMOUNT) PAID,0.0  COLLECTED,0.0 BALANCE,';  
     sSQL := sSQL || '0.0 RESERVE,0.0 INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
     end if;  
   
     sSQL := sSQL ||  
             ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, STOP_PAY_HIST SPHIST ' ||  
             sTemp || ' WHERE       F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL ||  
             ' AND F.TRANS_ID=SPHIST.ORIGINAL_ID AND SPHIST.STATUS_CODE = 6937 '; -- "Undo/Released" status  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL ';  
   
     if (Not m_m_SysParms.m_includeVoids) then  
       sSQL := sSQL || ' AND F.VOID_FLAG = 0 ';  
     end if;  
   
     if (m_m_SysParms.m_includeOnlyPrintedChecks) then  
       sSQL := sSQL || ' AND F.STATUS_CODE = 1054 '; -- Option added to allow for only printed checks to be included in FH  
     end if;  
   
     sSQL := sSQL ||  
             ' AND (COLLECTION_FLAG=0 OR COLLECTION_FLAG IS NULL) GROUP BY ';  
     sSQL := sSQL ||  
             ' F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE, SPHIST.DATE_STATUS_CHGD ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
   
     --sSQL := Replace(sSQL, chr(39), ' ');  
     --sSQL := Replace(sSQL, '#', '');  
   
     v_location := 37;  
     Begin  
       EXECUTE IMMEDIATE sSQL;  
       p_retval := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 6 - Handle Payments with Stop Pay status UNDO or RELEASED' || sSQL,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
   
     -- Handle Collections with Stop Pay status "Accepted"  
     sSQL := 'INSERT INTO FH1 ';  
     sSQL := sSQL || ' SELECT ';  
     sSQL := sSQL || ' SPHIST.DATE_STATUS_CHGD '; --Use Date Status Changed as Eval Date  
     sSQL := sSQL ||  
             ' EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID,FTS.RESERVE_TYPE_CODE,0 LOB, 0.0 PAID,(-1.0 * SUM        (FTS.AMOUNT)) COLLECTED,0.0 BALANCE,';  
     sSQL := sSQL || ' 0.0 RESERVE,0.0 INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || sRCRowId || sInsertPolCovLossId || ' ';  
     end if;  
   
     sSQL := sSQL ||  
             ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, STOP_PAY_HIST SPHIST ' ||  
             sTemp || ' WHERE       F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL ||  
             ' AND F.TRANS_ID=SPHIST.ORIGINAL_ID AND SPHIST.STATUS_CODE = 6935'; --"Accepted" status  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL';  
     if (Not m_m_SysParms.m_includeVoids) then  
       sSQL := sSQL || ' AND F.VOID_FLAG = 0';  
     end if;  
   
     sSQL := sSQL || ' AND (COLLECTION_FLAG=-1) GROUP BY ';  
     sSQL := sSQL ||  
             'F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE, SPHIST.DATE_STATUS_CHGD';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
   
     --sSQL := Replace(sSQL, chr(39), ' ');  
     --sSQL := Replace(sSQL, '#', '');  
   
     begin  
       EXECUTE IMMEDIATE sSQL;  
       p_retval := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 7 - Handle Collections with Stop Pay status ACCEPTED' || sSQL,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
   
     -- Handle Collections with Stop Pay status "Undo/Released"  
     sSQL := 'INSERT INTO FH1 SELECT ';  
     sSQL := sSQL || 'SPHIST.DATE_STATUS_CHGD '; --Use Date Status Changed as Eval Date  
     sSQL := sSQL ||  
             ' EVAL_DATE ,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, 0 LOB, 0.0 PAID,SUM(FTS.AMOUNT) COLLECTED,0.0 BALANCE,';  
     sSQL := sSQL || '0.0 RESERVE,0.0 INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID, 0 polcvg_loss_row_id ';  
     end if;  
   
     sSQL := sSQL ||  
             ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, STOP_PAY_HIST SPHIST ' ||  
             sTemp || ' WHERE F.TRANS_ID = FTS.TRANS_ID ' || sTemp2;  
     sSQL := sSQL ||  
             ' AND F.TRANS_ID=SPHIST.ORIGINAL_ID AND SPHIST.STATUS_CODE = 6937 '; -- "Undo/Released" status  
     sSQL := sSQL || ' AND F.FIN_IMPACT_DATE IS NOT NULL ';  
   
     if (Not m_m_SysParms.m_includeVoids) then  
       sSQL := sSQL || ' AND F.VOID_FLAG = 0';  
     end if;  
   
     sSQL := sSQL || ' AND (COLLECTION_FLAG=-1) GROUP BY ';  
     sSQL := sSQL ||  
             ' F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE, SPHIST.DATE_STATUS_CHGD';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', FTS.RC_ROW_ID ';  
     end if;  
     --sSQL := Replace(sSQL, chr(39), ' ');  
     --sSQL := Replace(sSQL, '#', '');  
   
     v_location := 42;  
     Begin  
       EXECUTE IMMEDIATE sSQL;  
       p_retval := 'TRUE';  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'InsertDataFromFunds Procedure',  
            'INSERT INTO FH1 - 8 - Handle Collections with Stop Pay status UNDO or RELEASED' || sSQL,  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     End;  
   
   end if;  
   --Code end here for insertdatafromfunds  
   
   --- Code start here for insertdatafromres  
   sSQL := 'INSERT INTO FH1 ';  
   sSQL := sSQL ||  
           ' SELECT DATE_ENTERED EVAL_DATE,RESERVE_HISTORY.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,0 LOB,0.0 PAID,0.0 COLLECTED,0.0 BALANCE,';  
   sSQL := sSQL || ' SUM(CHANGE_AMOUNT) RESERVE,0.0 INCURRED ';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || sInsertRCRowId || sPolCovLossId || ' ';  
   end if;  
   sSQL := sSQL || ' FROM RESERVE_HISTORY ' || sTemp_RES /*sTemp */  
           || ' GROUP BY  
                     DATE_ENTERED,RESERVE_HISTORY.CLAIM_ID,CLAIMANT_EID,  
                     UNIT_ID,RESERVE_TYPE_CODE';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || sPolCovLossId;  
   end if;  
   
   --sSQL      := Replace(sSQL, chr(39), ' ');  
   --sSQL      := Replace(sSQL, '#', '');  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' INSERT INTO FH1 FROM RESERVE_HISTORY START' || '|';  
   Execute Immediate sSQL;  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' INSERT INTO FH1 FROM RESERVE_HISTORY END' || '|';  
   p_retval  := 'TRUE';  
   --- Code end here for insertdatafromres  
   
   -- MERGE  
   --- Code start here for MergeFHData  
   sSQL := 'CREATE INDEX FH1_CL_IDX ON FH1(CLAIM_ID)';  
   
   begin  
     Execute Immediate sSQL;  
     p_retval := 'TRUE';  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'MergeFHData Procedure',  
          'CREATE INDEX FH1_CL_IDX',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   end;  
   
 /*  v_location := 2;  
   sSQL       := 'CREATE INDEX FH1_IDX ON FH1(EVAL_DATE,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE)';  
   begin  
     Execute Immediate sSQL;  
     p_retval := 'TRUE';  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'MergeFHData Procedure',  
          'CREATE INDEX FH1_IDX',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   end;*/  
   
   -- ORACLE was creating problem while processing temporary tables. Introduces a new temp table FH3 that  
   -- will take care of temp data and FH2 will be populated out of FH3.  
   -- Put all the data from FH1 to FH3 in desired format  
   v_location := 4;  
   -- Merge entries together  
   sSQL := 'CREATE TABLE FH3 NOLOGGING AS ';  
   sSQL := sSQL ||  
           'SELECT EVAL_DATE,FH1.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,CLAIM.LINE_OF_BUS_CODE  LOB,SUM(FH1.PAID) PAID,';  
   sSQL := sSQL ||  
           ' SUM(FH1.COLLECTED) COLLECTED,SUM(FH1.BALANCE) BALANCE,SUM(FH1.RESERVE) RESERVE,SUM(FH1.INCURRED) INCURRED ';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || ', RC_ROW_ID, polcvg_loss_row_id ';  
   end if;  
   
   sSQL := sSQL ||  
           ' FROM FH1,CLAIM WHERE FH1.CLAIM_ID = CLAIM.CLAIM_ID AND FH1.CLAIM_ID <> 0 GROUP BY       EVAL_DATE,FH1.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,CLAIM.LINE_OF_BUS_CODE ';  
   
   if m_str_parm_value <> '0' then  
     sSQL := sSQL || ', RC_ROW_ID, polcvg_loss_row_id ';  
   end if;  
   
   begin  
     select to_char(systimestamp, 'YYYYmmddhhmmss')  
       into p_sqlerrm_date  
       from dual;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' CREATE FH3 START' || '|';  
     Execute Immediate sSQL;  
     p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                  ' CREATE FH3 END' || '|';  
     p_retval  := 'TRUE';  
     retval    := TRUE;  
   Exception  
     when others then  
       v_sqlerrm := sqlerrm;  
       p_retval  := 'FALSE';  
       retval    := FALSE;  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'MergeFHData Procedure',  
          'CREATE TABLE FH3',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   end;  
   
   sSQL       := 'CREATE INDEX FH3_CL_IDX ON FH3 (CLAIM_ID)';  
     begin  
       execute immediate sSQL;  
     exception  
       when others then  
         v_sqlerrm := SQLERRM;  
  end;    
     
   if (retVal) then  
     -- need to check the error message  
     v_location := 5;  
     sSQL       := 'CREATE TABLE FH2 NOLOGGING AS SELECT EVAL_DATE,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,';  
     sSQL       := sSQL || 'LOB,PAID,COLLECTED,BALANCE,RESERVE,INCURRED ';  
   
     if m_str_parm_value <> '0' then  
       sSQL := sSQL || ', RC_ROW_ID, polcvg_loss_row_id ';  
     end if;  
   
     sSQL := sSQL || ' FROM FH3 ';  
   
     begin  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' CREATE FH2 START' || '|';  
       Execute Immediate sSQL;  
       p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                    ' CREATE FH2 END' || '|';  
       p_retval  := 'TRUE';  
       retval    := TRUE;  
     Exception  
       when others then  
         v_sqlerrm := sqlerrm;  
         p_retval  := 'FALSE';  
         retval    := FALSE;  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'MergeFHData Procedure',  
            'Create TABLE FH2',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     end;  
   
     -- Need to check the below code  
     if (retVal) then  
       if (m_bZeroBasedFinHist) then  
         --Process Zero based only if flag is set  
         if (isEventBased = 0) then  
           --Claim based  
           v_location := 6;  
           --Check if any claim does not have record in FH3 on it's date_of_claim. If so, enter a record in FH2  
           sSQL := 'INSERT INTO FH2 ';  
           if m_str_parm_value <> '0' then  
             sSQL := sSQL ||  
                     ' SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00,0,0 FROM CLAIM ';  
           else  
             sSQL := sSQL ||  
                     ' SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00 FROM CLAIM ';  
           end if;  
           sSQL := sSQL ||  
                   ' WHERE CLAIM.CLAIM_ID <> 0 AND CLAIM.DATE_OF_CLAIM < ';  
           IF (m_bFullBuild) THEN  
      sSQL := sSQL ||  
                   ' (SELECT MIN(EVAL_DATE) FROM FH3 WHERE FH3.CLAIM_ID=CLAIM.CLAIM_ID)';  
      ELSE  
      sSQL := sSQL ||  
                   ' (SELECT MIN(EVAL_DATE) FROM FINANCIAL_HIST WHERE FINANCIAL_HIST.CLAIM_ID=CLAIM.CLAIM_ID)';  
      END IF;  
   
           begin  
             p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                          ' INSERT INTO FH2 FOR CLAIM START' || '|';  
             Execute Immediate sSQL;  
             p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                          ' INSERT INTO FH2 FOR CLAIM END' || '|';  
             p_retval  := 'TRUE';  
             retval    := TRUE;  
           Exception  
             when others then  
               v_sqlerrm := sqlerrm;  
               p_retval  := 'FALSE';  
               retval    := FALSE;  
               insert into fin_hist_err_log  
                 (job_id,  
                  s_no,  
                  Obj_name,  
                  err_statement,  
                  loc,  
                  err_msg,  
                  create_date)  
               values  
                 (p_jobID,  
                  fin_hist_err_log_seq.Nextval,  
                  'MergeFHData Procedure',  
                  'INSERT INTO FH2 - 1 ',  
                  v_location,  
                  v_SQLERRM,  
                  sysdate);  
               commit;  
           end;  
   
           if (retVal) then  
             --Claims that does not have record in FH3, enter a record in FH2 for them  
             v_location := 7;  
             sSQL       := 'INSERT INTO FH2 ';  
             sSQL := sSQL ||  
                       ' SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00';  
      IF m_str_parm_value <> '0' THEN  
               sSQL := sSQL || ', 0, 0';  
      END IF;  
        
      IF (m_bFullBuild) THEN  
         sSQL := sSQL || ' FROM CLAIM, FH3 WHERE CLAIM.CLAIM_ID<>0 AND CLAIM.CLAIM_ID=FH3.CLAIM_ID(+) AND FH3.CLAIM_ID IS NULL';  
      ELSE  
        sSQL := sSQL || ' FROM CLAIM, FINANCIAL_HIST WHERE CLAIM.CLAIM_ID<>0 AND CLAIM.CLAIM_ID=FINANCIAL_HIST.CLAIM_ID(+) AND FINANCIAL_HIST.CLAIM_ID IS NULL';  
      END IF;  
        
             begin  
               select to_char(systimestamp, 'YYYYmmddhhmmss')  
                 into p_sqlerrm_date  
                 from dual;  
               p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                            ' INSERT INTO FH2 FOR CLAIM ZEROBASE START' || '|';  
               Execute Immediate sSQL;  
               p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                            ' INSERT INTO FH2 FOR CLAIM ZEROBASE END' || '|';  
               p_retval  := 'TRUE';  
               retval    := TRUE;  
             Exception  
               when others then  
                 v_sqlerrm := sqlerrm;  
                 p_retval  := 'FALSE';  
                 retval    := FALSE;  
                 insert into fin_hist_err_log  
                   (job_id,  
                    s_no,  
                    Obj_name,  
                    err_statement,  
                    loc,  
                    err_msg,  
                    create_date)  
                 values  
                   (p_jobID,  
                    fin_hist_err_log_seq.Nextval,  
                    'MergeFHData Procedure',  
                    'INSERT INTO FH2 - 2 ',  
                    v_location,  
                    v_SQLERRM,  
                    sysdate);  
                 commit;  
             end;  
           end if;  
   
         else  
           --//(isEventBased == 1)  //Event based  
           v_location := 8;  
           --//Check if any claim does not have record in FH3 on it's date_of_event. If so, enter a record in FH2  
           sSQL := 'INSERT INTO FH2 ';  
           if m_str_parm_value <> '0' then  
             sSQL := sSQL ||  
                     ' SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00,0,0 FROM CLAIM, EVENT ';  
           else  
             sSQL := sSQL ||  
                     ' SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00 FROM CLAIM, EVENT ';  
           end if;  
           sSQL := sSQL ||  
                   ' WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM.CLAIM_ID <> 0 AND EVENT.DATE_OF_EVENT < ';  
           IF (m_bFullBuild) THEN  
           sSQL := sSQL ||  
                   ' (SELECT MIN(EVAL_DATE) FROM FH3 WHERE FH3.CLAIM_ID=CLAIM.CLAIM_ID)';  
           ELSE  
           sSQL := sSQL ||  
                   ' (SELECT MIN(EVAL_DATE) FROM FINANCIAL_HIST WHERE FINANCIAL_HIST.CLAIM_ID=CLAIM.CLAIM_ID)';  
           END IF;  
           begin  
             p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                          ' INSERT INTO FH2 FOR EVENT START' || '|';  
             Execute Immediate sSQL;  
             p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                          ' INSERT INTO FH2 FOR EVENT END' || '|';  
             p_retval  := 'TRUE';  
             retval    := TRUE;  
           Exception  
             when others then  
               v_sqlerrm := sqlerrm;  
               p_retval  := 'FALSE';  
               retval    := FALSE;  
               insert into fin_hist_err_log  
                 (job_id,  
                  s_no,  
                  Obj_name,  
                  err_statement,  
                  loc,  
                  err_msg,  
                  create_date)  
               values  
                 (p_jobID,  
                  fin_hist_err_log_seq.Nextval,  
                  'MergeFHData Procedure',  
                  'INSERT INTO FH2 - 3',  
                  v_location,  
                  v_SQLERRM,  
                  sysdate);  
               commit;  
           end;  
   
           if (retVal) then  
             --//Claims that does not have record in FH3, enter a record in FH2 for them  
             v_location := 9;  
             sSQL       := 'INSERT INTO FH2 ';  
      sSQL := sSQL || ' SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00';  
             if m_str_parm_value <> '0' then  
               sSQL := sSQL || ',0,0';  
      END IF;  
        
      IF (m_bFullBuild) THEN  
        sSQL := sSQL ||  
                     ' FROM CLAIM, EVENT, FH3 WHERE CLAIM.CLAIM_ID<>0 AND CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=FH3.CLAIM_ID(+) AND FH3.CLAIM_ID IS NULL';  
      ELSE  
        sSQL := sSQL ||  
                     ' FROM CLAIM, EVENT, FINANCIAL_HIST WHERE CLAIM.CLAIM_ID<>0 AND CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=FINANCIAL_HIST.CLAIM_ID(+) AND FINANCIAL_HIST.CLAIM_ID IS NULL';  
      END IF;  
        
             begin  
               select to_char(systimestamp, 'YYYYmmddhhmmss')  
                 into p_sqlerrm_date  
                 from dual;  
               p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                            ' INSERT INTO FH2 FOR EVENT ZEROBASE START' || '|';  
               Execute Immediate sSQL;  
               p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                            ' INSERT INTO FH2 FOR EVENT ZEROBASE START' || '|';  
               p_retval  := 'TRUE';  
               retval    := TRUE;  
             Exception  
               when others then  
                 v_sqlerrm := sqlerrm;  
                 p_retval  := 'FALSE';  
                 retval    := FALSE;  
                 insert into fin_hist_err_log  
                   (job_id,  
                    s_no,  
                    Obj_name,  
                    err_statement,  
                    loc,  
                    err_msg,  
                    create_date)  
                 values  
                   (p_jobID,  
                    fin_hist_err_log_seq.Nextval,  
                    'MergeFHData Procedure',  
                    'INSERT INTO FH2 - 4',  
                    v_location,  
                    v_SQLERRM,  
                    sysdate);  
                 commit;  
             end;  
           end if;  
         end if;  
       end if;  
     end if;  
   end if;  
   --- Code End here for MergeFHData  
   
   --Getcount Function code here  
   execute immediate 'SELECT COUNT(*) FROM FH2 WHERE eval_date  >= :01'  
     into v_count  
     using lastdate;  
   
   --Code start here for updatefinhist  
   totalCount := v_count;  
   prevValues.extend(5); -- added as error coming 'ORA-06533: Subscript beyond count'  
   runningSum.extend(5);  
   FOR ptrix IN 1 .. 5 LOOP  
     prevValues(ptrIx) := 0.0;  
     runningSum(ptrIx) := 0.0;  
   end loop;  
   
   FH_ROW_ID := lNextUniqueID; -- need to check from where lNextUniqueID value is coming  
 IF m_str_parm_value <> '0' THEN  
   sSQL       := 'CREATE INDEX FH2_IDX ON FH2(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE, POLCVG_LOSS_ROW_ID) ';  
 ELSE  
   sSQL       := 'CREATE INDEX FH2_IDX ON FH2(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE) ';  
 END IF;    
   v_location := 3;  
   begin  
     execute immediate sSQL;  
   exception  
     when others then  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'UpdateFinHist Procedure',  
          'Index FH@_IDX failed........',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
       retval := FALSE;  
       if retval then  
         p_retval := 'TRUE';  
       else  
         p_retval := 'FALSE';  
       end if;  
   end;  
   
   if m_str_parm_value <> '0' then  
     sSQL := 'SELECT CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE,EVAL_DATE,PAID,COLLECTED,RESERVE,LOB, polcvg_loss_row_id  FROM FH2 ';  
     sSQL := sSQL ||  
             ' ORDER BY CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE, polcvg_loss_row_id';  
   else  
     sSQL := 'SELECT CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE,EVAL_DATE,PAID,COLLECTED,RESERVE,LOB  FROM FH2 ';  
     sSQL := sSQL ||  
             ' ORDER BY CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE';  
   end if;  
   --Above code added on 12-Dec-2013  
   
   CLAIM_ID          := 0;  
   CLAIMANT_EID      := 0;  
   UNIT_ID           := 0;  
   RESERVE_TYPE_CODE := 0;  
   EVAL_DATE         := 0;  
   PAID              := 0.0;  
   COLLECTED         := 0.0;  
   RESERVE           := 0.0;  
   cEvalDate         := 0;  
   
   v_location := 4;  
   select to_char(systimestamp, 'YYYYmmddhhmmss')  
     into p_sqlerrm_date  
     from dual;  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' CURSOR FH2 +INSERT INTO FINANCIAL START' || '|';  
   OPEN c_FH2 FOR sSQL;  
   LOOP  
     IF (m_str_parm_value <> '0') then  
       FETCH c_FH2  
         INTO CLAIM_ID,  
              CLAIMANT_EID,  
              UNIT_ID,  
              RESERVE_TYPE_CODE,  
              EVAL_DATE,  
              PAID,  
              COLLECTED,  
              RESERVE,  
              LOB,  
              polcvg_loss_row_id;  
     ELSE  
       FETCH c_FH2  
         INTO CLAIM_ID,  
              CLAIMANT_EID,  
              UNIT_ID,  
              RESERVE_TYPE_CODE,  
              EVAL_DATE,  
              PAID,  
              COLLECTED,  
              RESERVE,  
              LOB;  
     END IF;  
     EXIT WHEN c_FH2%NOTFOUND;  
   
     -- process row here  
     --//Ankur 01/01/2007 - Crawford Defect#4267  
     --//Code modified to exclude the rows that have 0's in all the following columns: PAID, COLLECTED, RESERVE, BALANCE, INCURRED  
   
     bIgnoreRow := FALSE;  
     counter    := counter + 1;  
     IF (m_str_parm_value <> '0') then  
       if (lClaimID != CLAIM_ID or lClaimantEID != CLAIMANT_EID or  
          lUnitID != UNIT_ID or lReserveTypeCode != RESERVE_TYPE_CODE or  
          lpolcvg_loss_row_id != polcvg_loss_row_id) then  
         FOR ptrix IN 1 .. 5 LOOP  
           prevValues(ptrIx) := 0.0; -- need to check its assignment  
           runningSum(ptrIx) := 0.0; -- need to check its assignment  
         end loop;  
       end if;  
     else  
       if (lClaimID != CLAIM_ID or lClaimantEID != CLAIMANT_EID or  
          lUnitID != UNIT_ID or lReserveTypeCode != RESERVE_TYPE_CODE) then  
         FOR ptrix IN 1 .. 5 LOOP  
           prevValues(ptrIx) := 0.0; -- need to check its assignment  
           runningSum(ptrIx) := 0.0; -- need to check its assignment  
         end loop;  
       end if;  
     end if;  
   
     runningSum(1) := runningSum(1) + PAID; -- need to check its assignment  
     runningSum(2) := runningSum(2) + COLLECTED; -- need to check its assignment  
     runningSum(3) := runningSum(3) + RESERVE; -- need to check its assignment  
   
     --// find paid balance  
     case LOB  
       when 241 then  
         bCollInRsvBal   := m_m_GCLOBParm.m_collectionInRsv;  
         bBalToZero      := m_m_GCLOBParm.m_balToZero;  
         bCollInIncurred := m_m_GCLOBParm.m_collectionInIncurred;  
         bPerRsvCollInRsvBal   := m_m_GCLOBParm.m_perrsvcollectionInRsv;  
         bPerRsvCollInIncurred := m_m_GCLOBParm.m_perrsvcollectionInIncurred;  
       when 242 then  
         bCollInRsvBal   := m_m_VALOBParm.m_collectionInRsv;  
         bBalToZero      := m_m_VALOBParm.m_balToZero;  
         bCollInIncurred := m_m_VALOBParm.m_collectionInIncurred;  
         bPerRsvCollInRsvBal   := m_m_GCLOBParm.m_perrsvcollectionInRsv;  
         bPerRsvCollInIncurred := m_m_GCLOBParm.m_perrsvcollectionInIncurred;  
       when 243 then  
         bCollInRsvBal   := m_m_WCLOBParm.m_collectionInRsv;  
         bBalToZero      := m_m_WCLOBParm.m_balToZero;  
         bCollInIncurred := m_m_WCLOBParm.m_collectionInIncurred;  
         bPerRsvCollInRsvBal   := m_m_GCLOBParm.m_perrsvcollectionInRsv;  
         bPerRsvCollInIncurred := m_m_GCLOBParm.m_perrsvcollectionInIncurred;  
       when 844 then  
         bCollInRsvBal   := m_m_NONOCCLOBParm.m_collectionInRsv;  
         bBalToZero      := m_m_NONOCCLOBParm.m_balToZero;  
         bCollInIncurred := m_m_NONOCCLOBParm.m_collectionInIncurred;  
         bPerRsvCollInRsvBal   := m_m_GCLOBParm.m_perrsvcollectionInRsv;  
         bPerRsvCollInIncurred := m_m_GCLOBParm.m_perrsvcollectionInIncurred;  
       else  
         bCollInRsvBal   := TRUE;  
         bBalToZero      := TRUE;  
         bCollInIncurred := TRUE;  
         bPerRsvCollInRsvBal:= TRUE;  
         bPerRsvCollInIncurred:= TRUE;  
     end case;  
     
     if(bPerRsvCollInRsvBal) then
           BEGIN
            SELECT count(1) into cnt FROM SYS_RES_COLL_IN_RSV_INCR_BAL where LINE_OF_BUS_CODE=LOB AND COLL_IN_RSV_BAL=-1 AND RES_TYPE_CODE=RESERVE_TYPE_CODE;
              if(cnt=0) then
                 bPerRsvCollInRsvBal := FALSE;
              END if;
           END;
      end if;  
   
     if (bCollInRsvBal or bPerRsvCollInRsvBal) then  
       --SET @paidBalance = @CurrentPaid - @CurrentCollection;  -- need to check its assignment  
       paidBalance := runningSum(1) - runningSum(2); -- need to check its assignment  
       --//if (paidBalance < 0.0 & bClipNegPaidBal) paidBalance = 0.0;  
       --//if (paidBalance < 0.0 & bLocalClipNegPaidBal) paidBalance = 0.0;  
       --// always clip neg paid bal  
       --//if (paidBalance < 0.0) paidBalance = 0.0;  
     else  
       paidBalance := runningSum(1); -- need to check its assignment  
     end if;  
   
     --//Ankur 12/04/2006 - Crawford Defect# 4146  
     --//Hardcoded thing, i wanna avoid it at first place but see the comments below.  
     --//Crawford comments: we need to show zero open reserves balance and a negative incurred  
     --//for the "Recoveries" bucket in order not to overstate the reserves and at the same time  
     --//reduce the total incurred by the amount of the recoveries. It may be best to apply the  
     --//logic to the financial history and have it reflect on the CM310 report rather than  
     --//modifying the Loss engine report in ERS.  
     begin  
       select NVL(CODE_ID, 0)  
         into bIsRecoveryBucket  
         from CODES  
        where CODE_ID = RESERVE_TYPE_CODE  
          and RELATED_CODE_ID =  
              (select CODE_ID  
                 from CODES  
                where TABLE_ID in  
                      (select TABLE_ID  
                         from GLOSSARY  
                        where SYSTEM_TABLE_NAME = 'MASTER_RESERVE')  
                  and SHORT_CODE = 'R');  
     exception  
       when no_data_found then  
         bIsRecoveryBucket := Null;  
     end;  
   
     IF bIsRecoveryBucket <> 0 then  
       -- THEN RECOVERY BUCKET  
       outstandingBalance := runningSum(3) - runningSum(2);  
    --akaushik5 Changed for MITS 36271 Starts  
    --incurred           := -runningSum(2); -- Added  
    if outstandingBalance < 0.0 then  
      incurred := runningSum(2);  
    else  
      incurred := outstandingBalance + runningSum(2);  
    end if;  
    --akaushik5 Changed for MITS 36271 Ends  
     else  
       --// find outstanding balance  
       if (bClosed and bBalToZero) then  
         outstandingBalance := 0.0;  
       else  
         outstandingBalance := runningSum(3) - paidBalance; -- need to check its assignment  
         if (outstandingBalance < 0.0) then  
           outstandingBalance := 0.0;  
         end if;  
       end if;  
   
       --Code added here -- 18-Dec-2013  
       temp_incurred := 0.0;  
       IF (bCollInRsvBal  or bPerRsvCollInRsvBal) THEN  
         --temp_incurred := CurrentPaid - CurrentCollection;  
         temp_incurred := runningSum(1) - runningSum(2);  
         --IF (temp_incurred < 0.0) then  
           --temp_incurred := 0.0;  
         --END IF;  
         IF (outstandingBalance < 0.0) THEN  
           incurred := temp_incurred;  
         ELSE  
           incurred := outstandingBalance + temp_incurred;  
         END IF;  
       ELSE  
         IF (outstandingBalance < 0.0) THEN  
           --incurred := CurrentPaid;  
           incurred := runningSum(1);  
         ELSE  
           --incurred := outstandingBalance + CurrentPaid;  
           incurred := outstandingBalance + runningSum(1);  
         END IF;  
       END IF;  
   
     if(bPerRsvCollInIncurred) then
           BEGIN
            SELECT count(1) into cnt FROM SYS_RES_COLL_IN_RSV_INCR_BAL where LINE_OF_BUS_CODE=LOB AND COLL_IN_RSV_BAL=0 AND RES_TYPE_CODE=RESERVE_TYPE_CODE;
              if(cnt=0) then
                 bPerRsvCollInIncurred := FALSE;
              END if;
           END;
      end if;  
       IF (bCollInIncurred or bPerRsvCollInIncurred) THEN  
         --incurred := incurred - CurrentCollection;  
         incurred := incurred - runningSum(2);  
       END IF;  
       -- akaushik5 Commented for MITS 37788 Starts
	   --IF (incurred < 0.0) THEN  
       --  incurred := 0.0;  
       --END IF;  
	   -- akaushik5 Commented for MITS 37788 Ends
       --Code added here END-- 18-Dec-2013  
     end if;  
   
     v_location := 8;  
   
     --SET @CurrentBalance = @outstandingBalance;  
     runningSum(5) := outstandingBalance;  
   
     --SET @CurrentIncurred = @incurred;  -- need to check its assignment  
     runningSum(4) := incurred; -- need to check its assignment  
   
     -- // find incurred  
     --incurred := outstandingBalance + paidBalance;  
     --runningSum(4) := incurred;  -- need to check its assignment  
   
     --// find differentials  
     --SET @incurredDelta = @CurrentIncurred - @PreviousIncurred;  -- need to check its assignment  
     incurredDelta := runningSum(4) - prevValues(4); -- need to check its assignment  
   
     if (abs(incurredDelta) < 0.009) then  
       -- need to check its assignment -- what is fabs function in it  
       incurredDelta := 0.0;  
     end if;  
   
     balanceDelta := runningSum(5) - prevValues(5);  
   
     if (abs(balanceDelta) < 0.009) then  
       balanceDelta := 0.0;  
     end if;  
   
     --//Ankur 01/01/2007 - Crawford Defect#4267  
     --//Code modified to exclude the rows that have 0's in all the following columns: PAID, COLLECTED, RESERVE, BALANCE, INCURRED  
     --//Ankur 12/20/2007 - MITS 10290 - Ignore the row only for Crawford client.  
   
     if (m_m_SysParms.m_ESSP = 'CRAWFORD' and PAID = 0.0 and COLLECTED = 0.0 and  
        RESERVE = 0.0 and incurredDelta = 0.0 and balanceDelta = 0.0) then  
       bIgnoreRow := TRUE;  
     end if;  
   
     v_location := 9;  
   
     if (bIgnoreRow = FALSE) then  
       --// build & execute update statement  
       --rc := SQLExecute(hstmtUpdate); -- need to check and ask which statement is getting executed here.  
       -- As per me it is executing above insert statement  
       if m_str_parm_value <> '0' then  
         sSQL_Insert := 'INSERT INTO FINANCIAL_HIST(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE,  
          PAID,COLLECTED,RESERVE,INCURRED,BALANCE,FH_ROW_ID, polcvg_loss_row_id) VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:13)';  
   
         Begin  
           execute immediate ssql_insert  
             using claim_id, claimant_eid, unit_id, reserve_type_code, eval_date, paid, collected, reserve, incurredDelta, balanceDelta --incurred, balance  
           , fh_row_id, polcvg_loss_row_id;  
   
         exception  
           when others then  
             v_sqlerrm := sqlerrm;  
             --v_sqlerrm :='Index FH@_IDX failed........';  
             insert into fin_hist_err_log  
               (job_id,  
                s_no,  
                Obj_name,  
                err_statement,  
                loc,  
                err_msg,  
                create_date)  
             values  
               (p_jobID,  
                fin_hist_err_log_seq.Nextval,  
                'UpdateFinHist Procedure',  
                'Insert into Financial_hist table failed........('||claim_id||','|| claimant_eid||','|| unit_id||','|| reserve_type_code||','|| eval_date||','||polcvg_loss_row_id||')',  
                v_location,  
                v_SQLERRM,  
                sysdate);  
             commit;  
         end;  
       else  
         sSQL_Insert := 'INSERT INTO FINANCIAL_HIST(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE,  
              PAID,COLLECTED,RESERVE,INCURRED,BALANCE,FH_ROW_ID) VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)';  
   
         v_location := 10;  
   
         Begin  
           execute immediate ssql_insert  
             using claim_id, claimant_eid, unit_id, reserve_type_code, eval_date, paid, collected, reserve, incurredDelta, balanceDelta --incurred, balance  
           , fh_row_id;  
   
           retval := TRUE;  
   
         exception  
           when others then  
             v_sqlerrm := sqlerrm;  
             insert into fin_hist_err_log  
               (job_id,  
                s_no,  
                Obj_name,  
                err_statement,  
                loc,  
                err_msg,  
                create_date)  
             values  
               (p_jobID,  
                fin_hist_err_log_seq.Nextval,  
                'UpdateFinHist Procedure',  
                'Insert into Financial_hist table failed........',  
                v_location,  
                v_SQLERRM,  
                sysdate);  
             commit;  
         end;  
       end if;  
       if retval then  
         p_retval := 'TRUE';  
       else  
         p_retval := 'FALSE';  
       end if;  
   
       --// Record values for next path  
       --for (ptrIx = 0; ptrIx < 5; ptrIx++)  -- need to check from where these values are coming and on what basis these increment                      -- is happening  
       FOR ptrix IN 1 .. 5 LOOP  
         prevValues(ptrIx) := runningSum(ptrIx); -- need to check its assignment  
       end loop;  
   
       v_location := 11;  
   
       --// Get next record  
       lClaimID         := CLAIM_ID; -- need to check these assignments i.e. from where these values are coming  
       lClaimantEID     := CLAIMANT_EID; -- need to check these assignments  
       lUnitID          := UNIT_ID; -- need to check these assignments  
       lReserveTypeCode := RESERVE_TYPE_CODE; -- need to check these assignments  
       cEvalDate        := EVAL_DATE;  
       IF (m_str_parm_value <> '0') then  
         lpolcvg_loss_row_id := polcvg_loss_row_id;  
       END if;  
   
     end if;  
   
     CLAIM_ID          := 0;  
     CLAIMANT_EID      := 0;  
     UNIT_ID           := 0;  
     RESERVE_TYPE_CODE := 0;  
     EVAL_DATE         := 0;  
     PAID              := 0.0;  
     COLLECTED         := 0.0;  
     RESERVE           := 0.0;  
   
     begin  
       select MOD(counter, 50000) into v_counter_n from dual;  
     exception  
       when no_data_found then  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateFinHist Procedure',  
            'No Value exists for Counter variable due to which select query with mod operator got failed........',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
       when others then  
         insert into fin_hist_err_log  
           (job_id,  
            s_no,  
            Obj_name,  
            err_statement,  
            loc,  
            err_msg,  
            create_date)  
         values  
           (p_jobID,  
            fin_hist_err_log_seq.Nextval,  
            'UpdateFinHist Procedure',  
            'Other problem in select query with mod operator due to which it got failed........',  
            v_location,  
            v_SQLERRM,  
            sysdate);  
         commit;  
     end;  
   
     if (v_counter_n = 0) then  
       commit;  
     end if;  
   
     --//Ankur 01/01/2007 - Crawford Defect#4267  
     --//If row has been ignored, don't increment unique row_id  
   
     if (bIgnoreRow = FALSE) then  
       FH_ROW_ID := FH_ROW_ID + 1; -- need to check from where this variable is getting set  
   
     end if;  
     -- End  
   END LOOP;  
   CLOSE c_FH2;  
   select to_char(systimestamp, 'YYYYmmddhhmmss')  
     into p_sqlerrm_date  
     from dual;  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' CURSOR FH2 +INSERT INTO FINANCIAL END' || '|';  
   commit;  
   p_retval  := 'TRUE';  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' UPDATE GLOSSARY START' || '|';  
   if (m_claimid is null or m_DeptAbbr is null) then  
     UPDATE GLOSSARY  
        SET NEXT_UNIQUE_ID   = FH_ROW_ID,  
            DTTM_LAST_UPDATE = to_char(sysdate, 'YYYYMMDDHHMISS') --sysdate  
      WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';  
   End if;  
   if (m_claimid is not null) then  
     UPDATE GLOSSARY  
        SET NEXT_UNIQUE_ID = FH_ROW_ID --, DTTM_LAST_UPDATE = to_char(sysdate,'YYYYMMDDHHMISS')--sysdate  
      WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';  
   End if;  
   Begin  
     v_location := 15;  
     select count(*)  
       into v_idx_exist  
       from user_indexes  
      where index_name = 'FH_IDX1'  
        and table_name = 'FINANCIAL_HIST';  
     IF (v_idx_exist = 0) THEN  
       Execute Immediate 'CREATE UNIQUE INDEX FH_IDX1 ON FINANCIAL_HIST (FH_ROW_ID)';  
     END IF;  
   Exception  
     when Others then  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'CREATE UNIQUE INDEX FH_IDX1',  
          'CREATE UNIQUE INDEX FH_IDX2 due to which it got failed........',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   end;  
   
   Begin  
     v_location := 16;  
     select count(*)  
       into v_idx_exist  
       from user_indexes  
      where index_name = 'FH_IDX2'  
        and table_name = 'FINANCIAL_HIST';  
     IF (v_idx_exist = 0) THEN  
       IF (m_str_parm_value <> '0') THEN  
         EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX FH_IDX2 ON FINANCIAL_HIST (CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE, EVAL_DATE, POLCVG_LOSS_ROW_ID)';  
       ELSE  
         EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX FH_IDX2 ON FINANCIAL_HIST (CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE, EVAL_DATE)';  
       END IF;  
     END IF;  
   Exception  
     when Others then  
       insert into fin_hist_err_log  
         (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
       values  
         (p_jobID,  
          fin_hist_err_log_seq.Nextval,  
          'CREATE UNIQUE INDEX FH_IDX2',  
          'CREATE UNIQUE INDEX FH_IDX2 due to which it got failed........',  
          v_location,  
          v_SQLERRM,  
          sysdate);  
       commit;  
   END;  
   p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||  
                ' UPDATE GLOSSARY END' || '|';  
   --Code End here for updatefinhist  
   commit;  
 Exception  
   when others then  
     v_sqlerrm := SQLERRM;  
     p_sqlerrm := p_sqlerrm  || v_sqlerrm;  
     insert into fin_hist_err_log  
       (job_id, s_no, Obj_name, err_statement, loc, err_msg, create_date)  
     values  
       (p_jobID,  
        fin_hist_err_log_seq.Nextval,  
        'Main Exception Handler',  
        p_SQLERRM,  
        v_location,  
        v_SQLERRM,  
        sysdate);  
     commit;  
 END USP_FINANCIAL_HISTORY_REBUILD;
GO