IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'USP_CONVERTCOMMENTS4HTML' AND type = 'P')
   DROP PROCEDURE USP_CONVERTCOMMENTS4HTML
GO

CREATE PROCEDURE USP_CONVERTCOMMENTS4HTML
	@table_name varchar(100),
	@source_col varchar(100),
	@dest_col varchar(100), 
	@id_col varchar(100)
AS

DECLARE @crlf varchar(5), @lf varchar(2), @cr varchar(2), @rep_str varchar(80)
DECLARE @nbsp varchar(10), @space varchar(2) 
DECLARE @record_id int, @nHTMLstr nvarchar(max), @nstr nvarchar(max)
DECLARE @nsql nvarchar(4000)
DECLARE @count int
DECLARE @ParmDefinition nvarchar(500);

SELECT @lf = CHAR(10)
SELECT @cr = CHAR(13)
SELECT @crlf = @cr + @lf
SELECT @rep_str = '<br/>'

SELECT @nbsp = '&nbsp;'
SELECT @space = ' ' 

SET NOCOUNT ON
PRINT 'Convert data for table ' + @table_name + ' column ' + @dest_col
SELECT @count = 0 

-- copy COMMENTS to HTMLCOMMENTS if COMMENTS is not null and HTMLCOMMENTS is null
-- Old version of RMWorld only save values to COMMENTS column
SELECT @nsql= N'UPDATE ' + @table_name + N' SET '
	+ @dest_col + N'= ' + @source_col + N' WHERE ' + @source_col + N' IS NOT NULL AND '
	+ @dest_col + N' IS NULL'

EXEC sp_executesql @nsql

-- replace \n\r to <br/> for HTMLCommnets, and replace &nbsp; with blank space
-- for Comments
SELECT @nsql = N'DECLARE comments_cursor CURSOR FOR SELECT ' 
	+ @id_col + N', ' + @source_col + N', ' + @dest_col + N' FROM ' + @table_name 
	+ N' WHERE ' + @dest_col + N' IS NOT NULL'
EXEC( @nsql )
OPEN comments_cursor
FETCH NEXT FROM comments_cursor INTO @record_id, @nstr, @nHTMLstr
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @count = @count + 1
	IF @count % 5000 = 0
		PRINT CAST(@count AS VARCHAR(20)) + ' records have been processed.'
 
	SELECT @nsql= N'UPDATE ' + @table_name + N' SET ' + @source_col + N'= @nstr,' +
		+ @dest_col + N'= @nHTMLstr WHERE ' + @id_col + N'=' + CAST(@record_id AS varchar(10))

	SELECT @nstr=REPLACE(@nstr, @nbsp, @space)

    SELECT @nHTMLstr=REPLACE(@nHTMLstr,@crlf, @rep_str)
    SELECT @nHTMLstr=REPLACE(@nHTMLstr,@cr, @rep_str)
    SELECT @nHTMLstr=REPLACE(@nHTMLstr,@lf, @rep_str)

	SET @ParmDefinition = N'@nstr varchar(max), @nHTMLstr varchar(max)'
    EXEC sp_executesql @nsql, @ParmDefinition, @nstr=@nstr, @nHTMLstr= @nHTMLstr

    FETCH NEXT FROM comments_cursor INTO @record_id, @nstr, @nHTMLstr
END
CLOSE comments_cursor
DEALLOCATE comments_cursor

GO

EXEC USP_CONVERTCOMMENTS4HTML 'COMMENTS_TEXT', 'COMMENTS', 'HTMLCOMMENTS', 'COMMENT_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'CLAIM', 'COMMENTS', 'HTMLCOMMENTS', 'CLAIM_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'CLAIM_PRG_NOTE', 'NOTE_MEMO_CARETECH', 'NOTE_MEMO', 'CL_PROG_NOTE_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'CLAIM_X_LITIGATION', 'COMMENTS', 'HTMLCOMMENTS', 'LITIGATION_ROW_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'CLAIMANT', 'COMMENTS', 'HTMLCOMMENTS', 'CLAIMANT_ROW_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'DEFENDANT', 'COMMENTS', 'HTMLCOMMENTS', 'DEFENDANT_ROW_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'DISABILITY_PLAN', 'COMMENTS', 'HTMLCOMMENTS', 'PLAN_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'ENTITY', 'COMMENTS', 'HTMLCOMMENTS', 'ENTITY_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'EVENT', 'COMMENTS', 'HTMLCOMMENTS', 'EVENT_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'EVENT', 'EVENT_DESCRIPTION', 'EVENT_DESCRIPTION_HTMLCOMMENTS', 'EVENT_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'EVENT_X_DATED_TEXT', 'DATED_TEXT', 'DATED_TEXT_HTMLCOMMENTS', 'EV_DT_ROW_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'FUNDS', 'COMMENTS', 'HTMLCOMMENTS', 'TRANS_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'PERSON_INVOLVED', 'COMMENTS', 'HTMLCOMMENTS', 'PI_ROW_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'POLICY', 'COMMENTS', 'HTMLCOMMENTS', 'POLICY_ID'
EXEC USP_CONVERTCOMMENTS4HTML 'ADJUST_DATED_TEXT', 'DATED_TEXT', 'DATED_TEXT_HTMLCOMMENTS', 'ADJ_DTTEXT_ROW_ID'
GO

IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'USP_CONVERTCOMMENTS4HTML' AND type = 'P')
   DROP PROCEDURE USP_CONVERTCOMMENTS4HTML
GO
