[GO_DELIMITER]
create or replace
PROCEDURE USP_CREATE_INDEXES(sRM_USERID VARCHAR2)
AUTHID CURRENT_USER IS
  BEGIN
    DECLARE
          V_COUNT NUMBER;          
          INDEX_Cursor varchar(2000);
          sSID varchar2(20);
          sSERIAL varchar(20);
          CursorFinalIndices SYS_REFCURSOR;
          MyIndexCursorInside SYS_REFCURSOR;
          sSqlCursor VARCHAR2(4000);
          sSQL varchar2(2000);
          iFLAG integer;
          iINDEX_NAME varchar2(250);
          iIS_CLUSTERED integer; 
          iIS_UNIQUE integer; 
          iIS_PRIMARY integer; 
          sCOLUMN_NAMES varchar2(250);
          iCount integer;
          sTABLE_NAME varchar2(250);
          sINDEX_NAME varchar2(250);
          sCreateP_k_Excptn EXCEPTION;
          sCreateU_k_Excptn EXCEPTION;
          sCreateC_I_Excptn EXCEPTION;
          sCreateNC_I_Excptn EXCEPTION;
          sEcode NUMBER;
          sEmesg VARCHAR2(500);
          sERRORMESSAGE varchar2(2000);
          sLOGENTRY VARCHAR2(2000);
          sLOGENTRY1 VARCHAR2(2000);
          sLOGENTRY2 VARCHAR2(2000);
          sDATE VARCHAR2(50);
          sSQLDATE VARCHAR2(500);
          sMSG VARCHAR2(2000);
           IS_UNIQUEINDEX VARCHAR2(15);
   BEGIN   
   
     sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
     EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
     sMSG:='STARTING INDEX/CONSTRAINT CREATION';
     sLOGENTRY:= 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT AND INDEXES EXISITNG INDICES'',''STARTING'','''||sMSG||''','''||sMSG||''','''||sMSG||''',''' || sDATE|| ''')';     
     EXECUTE IMMEDIATE sLOGENTRY;  
     COMMIT;     
          sSqlCursor:='SELECT TABLE_NAME , INDEX_NAME ,  IS_CLUSTERED , IS_UNIQUE, IS_PRIMARY , COLUMN_NAMES,IS_UNIQUE_INDEX FROM '||sRM_USERID||'.FINAL_INDICES';  
                  BEGIN      --inner cursor start
                    OPEN CursorFinalIndices FOR sSqlCursor;
                        LOOP          
                          FETCH CursorFinalIndices INTO sTABLE_NAME , sINDEX_NAME ,iIS_CLUSTERED , iIS_UNIQUE, iIS_PRIMARY , sCOLUMN_NAMES,IS_UNIQUEINDEX;  
                           EXIT WHEN CursorFinalIndices%NOTFOUND;
                             IF (iIS_PRIMARY=1) THEN
                               BEGIN   
                               IF(1=1) THEN
                                             sSQL:='ALTER TABLE '||sRM_USERID||'.'||sTABLE_NAME||' ADD CONSTRAINT '||sINDEX_NAME||' PRIMARY KEY('||sCOLUMN_NAMES||')';
                                             EXECUTE IMMEDIATE sSQL;
                                             sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                             EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                             sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                             sMSG:='PRIMARY CONSTRAINT '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' CREATED SUCCESSFULLY';
                                             sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';                                
                                             EXECUTE IMMEDIATE sLOGENTRY;
                                             COMMIT;
                                        -- RAISE sCreateP_k_Excptn;
                               END IF;  
                               EXCEPTION
                                   
                                      WHEN OTHERS THEN 
                                         BEGIN                                                                
                                               sEcode := SQLCODE;
                                               sEmesg := SQLERRM;                                                           
                                               sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' || sEmesg;                                               
                                               sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                               EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                               sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                               sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT'','''||sTABLE_NAME||''',''FAILED'','''||sERRORMESSAGE||''',''' ||sSQL ||''',''' ||sDATE|| ''')';                                                
                                               EXECUTE IMMEDIATE sLOGENTRY;
                                               COMMIT;
                                        END;
                               
                               END;
                             --
                              ELSIF((iIS_UNIQUE=1)AND (IS_UNIQUEINDEX='PK')) THEN  
                                   BEGIN   
                                     IF(1=1) THEN
                                                 sSQL:='ALTER TABLE '||sRM_USERID||'.'||sTABLE_NAME||' ADD CONSTRAINT '||sINDEX_NAME||' UNIQUE('||sCOLUMN_NAMES||')';
                                                 EXECUTE IMMEDIATE sSQL;
                                                 sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                 EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                 sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                 sMSG:='UNIQUE CONSTRAINT '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' CREATED SUCCESSFULLY';
                                                 sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                 EXECUTE IMMEDIATE sLOGENTRY;
                                                 COMMIT;
                                      -- RAISE sCreateU_k_Excptn;
                                     END IF;
                                      EXCEPTION
                                       
                                            WHEN OTHERS THEN 
                                                BEGIN                                                                
                                                   sEcode := SQLCODE;
                                                   sEmesg := SQLERRM;                                                           
                                                   sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' || sEmesg;
                                                   sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                   EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                   sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                   sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT'','''||sTABLE_NAME||''',''FAILED'','''||sERRORMESSAGE||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                   EXECUTE IMMEDIATE sLOGENTRY;
                                                   COMMIT;
                                                   END;
                                   END;
                             ELSIF(iIS_CLUSTERED=1) THEN
                               BEGIN  
                                     IF(1=1) THEN
                                                    sSQL:='CREATE UNIQUE CLUSTERED INDEX  '||sINDEX_NAME||' ON '||sRM_USERID||'.'||sTABLE_NAME||'('||sCOLUMN_NAMES||')';
                                                    -- EXECUTE IMMEDIATE sSQL;
                                                    sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                    EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                    sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                    sMSG:='INDEX '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' CREATED SUCCESSFULLY';
                                                    sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE INDEX'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                    EXECUTE IMMEDIATE sLOGENTRY;
                                                    COMMIT;
                                     --RAISE sCreateC_I_Excptn;
                                    END IF; 
                                       EXCEPTION
                                           WHEN OTHERS THEN 
                                                BEGIN                                                                
                                                   sEcode := SQLCODE;
                                                   sEmesg := SQLERRM;                                                           
                                                   sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' || sEmesg;
                                                   sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                   EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                   sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                   sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE INDEX'','''||sTABLE_NAME||''',''FAILED'','''||sERRORMESSAGE||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                   EXECUTE IMMEDIATE sLOGENTRY;
                                                   COMMIT;
                                                END;
                                END;  
                            ELSE 
                               BEGIN  
                                     IF(1=1)THEN
                                                IF(IS_UNIQUEINDEX='NONUNIQUE') THEN
                                                 BEGIN
                                                    sSQL:='CREATE INDEX  '||sINDEX_NAME||' ON '||sRM_USERID||'.'||sTABLE_NAME||'('||sCOLUMN_NAMES||')';  
                                                    EXECUTE IMMEDIATE sSQL;  
                                                  END;
                                                 ELSIF(IS_UNIQUEINDEX='UNIQUE') THEN
                                                  BEGIN
                                                      sSQL:='CREATE UNIQUE INDEX  '||sINDEX_NAME||' ON '||sRM_USERID||'.'||sTABLE_NAME||'('||sCOLUMN_NAMES||')';  
                                                    EXECUTE IMMEDIATE sSQL;  
                                                  END;
												  --rsushilaggar Added the default case here 
												 ELSE
												 BEGIN
												 sSQL:='CREATE INDEX  '||sINDEX_NAME||' ON '||sRM_USERID||'.'||sTABLE_NAME||'('||sCOLUMN_NAMES||')';  
													EXECUTE IMMEDIATE sSQL;
												END;
												--end rsushilaggar
												END IF;	
                                                   sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                   EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                   sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                   sMSG:='INDEX '||sINDEX_NAME||' ON COLUMN  '||sCOLUMN_NAMES||' FOR TABLE '||sTABLE_NAME||' CREATED SUCCESSFULLY';
                                                   sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE INDEX'','''||sTABLE_NAME||''',''SUCCESSFUL'','''||sMSG||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                   EXECUTE IMMEDIATE sLOGENTRY;
                                                   COMMIT;
                                                  -- RAISE sCreateNC_I_Excptn;
                                     END IF;
                                  EXCEPTION
                               
                                       WHEN OTHERS THEN 
                                               BEGIN                                                                
                                                    sEcode := SQLCODE;
                                                    sEmesg := SQLERRM;                                                           
                                                    sERRORMESSAGE := '	*** OOPS - ERROR OCCURRED*** : ' || sEmesg;
                                                    sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                                                    EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                                                    sSQL:=REPLACE(sSQL,CHR(39),CHR(34));
                                                    sLOGENTRY := 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE INDEX'','''||sTABLE_NAME||''',''FAILED'','''||sERRORMESSAGE||''',''' ||sSQL ||''',''' ||sDATE|| ''')';
                                                    EXECUTE IMMEDIATE sLOGENTRY;
                                                    COMMIT;
                                                END;
                               END; 
                            END IF;  
                        END LOOP;
                  CLOSE CursorFinalIndices;      
                END;
		
                sSQLDATE:='SELECT TO_CHAR(SYSDATE,''YYYY-MM-DD HH:MI:SS AM'') FROM DUAL';           
                EXECUTE IMMEDIATE sSQLDATE INTO sDATE; 
                sMSG:='FINISHED INDEX/CONSTRAINT CREATION';
                sLOGENTRY:= 'INSERT INTO '||sRM_USERID||'.INDEX_UPDATE_CREATE_LOG(PROCESS_LOG_ID,PROCESS,TABLE_NAME ,STATUS ,MESSAGE ,STATEMENT ,DATE_TIME ) VALUES(INDEX_UPDATE_CREATE_LOG_SEQ.NEXTVAL,''CREATE CONSTRAINT AND INDEXES EXISITNG INDICES'',''FINISHED'','''||sMSG||''','''||sMSG||''','''||sMSG||''',''' || sDATE|| ''')';
                EXECUTE IMMEDIATE sLOGENTRY;
                COMMIT; 
       END;           
  END USP_CREATE_INDEXES;
GO