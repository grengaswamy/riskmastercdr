--exec USP_MOVE_ENTITY_ADDRESSES
-- =============================================
-- Author:		CSC
-- Create date: 03/25/2010
-- Description:	Copies Address Info from Entity to Entity_X_Addresses Table
-- =============================================
[GO_DELIMITER]
DECLARE @nvSQL AS nVARCHAR(max) 
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'USP_MOVE_ENTITY_ADDRESSES' AND TYPE = 'P')
	BEGIN
		DROP PROCEDURE USP_MOVE_ENTITY_ADDRESSES
	END
		SET @nvSQL = 'CREATE PROCEDURE USP_MOVE_ENTITY_ADDRESSES 
					  AS
					  BEGIN
						-- SET NOCOUNT ON added to prevent extra result sets from
						-- interfering with SELECT statements.
						SET NOCOUNT ON;

						--INSERT ADDRESSES INFO INTO THE ENTITY_X_ADDRESSES table
						DECLARE 
						        --@ENTITY_ID INT,
								@ADDRESS_ID INT,
								@PHONE_ID INT,
								@OFFICE_CODEID INT,
								@HOME_CODEID INT,
								@ADDRESS_COUNT INT,
								@PHONE_COUNT INT,
								
								@iMinID INT,
								@iMaxID INT,
								@iLoopCount INT,
								@iFLAG INT
								
								SELECT @ADDRESS_COUNT=COUNT(ADDRESS_ID) FROM ENTITY_X_ADDRESSES
								SELECT @PHONE_COUNT=COUNT(PHONE_ID) FROM ADDRESS_X_PHONEINFO
								SELECT @OFFICE_CODEID = CODE_ID FROM CODES WHERE TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME=''PHONES_CODES'') AND SHORT_CODE=''O''
								SELECT @HOME_CODEID = CODE_ID FROM CODES WHERE TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME=''PHONES_CODES'') AND SHORT_CODE=''H''
								
								
							IF(@ADDRESS_COUNT=0 OR @PHONE_COUNT=0)
							BEGIN
							    SELECT @iMinID = MIN(ENTITY_ID) FROM ENTITY
						        SELECT @iMaxID = MAX(ENTITY_ID) FROM ENTITY
								--Declare the Cursor
								--DECLARE curTables CURSOR FOR 
								--	SELECT ENTITY_ID FROM ENTITY
									
								--open the cursor
								--OPEN curTables
								
								--loop through the cursor lines
								--FETCH NEXT FROM curTables INTO @ENTITY_ID 
								--WHILE @@FETCH_STATUS = 0
								
								SET @iLoopCount = @iMinID
							    WHILE @iLoopCount <= @iMaxID
								BEGIN
								 SELECT  @iFLAG = COUNT(*)  FROM ENTITY  WHERE ENTITY_ID BETWEEN  @iLoopCount AND (@iLoopCount + 49999)
									IF(@ADDRESS_COUNT=0 AND @iFLAG > 0)									
									BEGIN
										SELECT @ADDRESS_ID = ISNULL(MAX(ADDRESS_ID), 0) FROM ENTITY_X_ADDRESSES
										INSERT INTO ENTITY_X_ADDRESSES
	 									(	ENTITY_ID,
 											ADDRESS_ID,
 											ADDR1,
 											ADDR2,
 											CITY,
											COUNTY,
											COUNTRY_CODE,
											STATE_ID,
											ZIP_CODE,
											EMAIL_ADDRESS,
											FAX_NUMBER,
											PRIMARY_ADD_FLAG
	 									)SELECT  ENTITY_ID,@ADDRESS_ID + ROW_NUMBER() over (order by entity_id) ''ADDRESS_ID'',ADDR1, ADDR2, CITY,COUNTY,COUNTRY_CODE,STATE_ID,ZIP_CODE,EMAIL_ADDRESS,FAX_NUMBER ,''-1'' FROM ENTITY 
										WHERE (ADDR1 IS NOT NULL OR ADDR2 IS NOT NULL OR CITY IS NOT NULL OR COUNTY IS NOT NULL OR (COUNTRY_CODE <>0 AND COUNTRY_CODE IS NOT NULL) OR (STATE_ID <>0 AND STATE_ID IS NOT NULL) OR ZIP_CODE IS NOT NULL OR EMAIL_ADDRESS IS NOT NULL OR FAX_NUMBER IS NOT NULL) 
										AND ENTITY.ENTITY_ID >= @iLoopCount AND ENTITY.ENTITY_ID <= (@iLoopCount + 49999)
									END										
									
									IF(@PHONE_COUNT=0 AND @iFLAG > 0)
									BEGIN
										SELECT @PHONE_ID = ISNULL(MAX(PHONE_ID), 0) FROM ADDRESS_X_PHONEINFO
										INSERT INTO ADDRESS_X_PHONEINFO
	 										(	ENTITY_ID,
 												PHONE_ID,
 												PHONE_CODE,
 												PHONE_NO
	 										)SELECT ENTITY_ID,@PHONE_ID + ROW_NUMBER() over (order by entity_id) ''PHONE_ID'',@OFFICE_CODEID, PHONE1 FROM ENTITY WHERE (LEN(PHONE1)> 0) 
										AND ENTITY.ENTITY_ID >= @iLoopCount AND ENTITY.ENTITY_ID <= (@iLoopCount + 49999)
	 									SELECT @PHONE_ID = ISNULL(MAX(PHONE_ID), 0) FROM ADDRESS_X_PHONEINFO
	 									INSERT INTO ADDRESS_X_PHONEINFO
	 										(	ENTITY_ID,
 												PHONE_ID,
 												PHONE_CODE,
 												PHONE_NO
	 										)SELECT ENTITY_ID,@PHONE_ID + ROW_NUMBER() over (order by entity_id) ''PHONE_ID'',@HOME_CODEID, PHONE2 FROM ENTITY WHERE (LEN(PHONE2)> 0) 
	 										AND ENTITY.ENTITY_ID >= @iLoopCount AND ENTITY.ENTITY_ID <= (@iLoopCount + 49999)
	 								END		
									SET @iLoopCount = @iLoopCount + 50000
	 								--FETCH NEXT FROM curTables INTO @ENTITY_ID
								END

								--clean up
								--CLOSE curTables
								--DEALLOCATE curTables
						END
							
							IF(@ADDRESS_COUNT=0)
							BEGIN
								--UPDATE THE GLOSSARY TABLE WITH THE NEXT_UNIQUE_ID
								SELECT @ADDRESS_ID = ISNULL(MAX(ADDRESS_ID), 0) FROM ENTITY_X_ADDRESSES
								UPDATE GLOSSARY
								SET NEXT_UNIQUE_ID = (@ADDRESS_ID + 1)
								WHERE SYSTEM_TABLE_NAME = ''ENTITY_X_ADDRESSES''
							END

							IF(@PHONE_COUNT=0)
							BEGIN
								--UPDATE THE GLOSSARY TABLE WITH THE NEXT_UNIQUE_ID
								SELECT @PHONE_ID = ISNULL(MAX(PHONE_ID), 0) FROM ADDRESS_X_PHONEINFO
								UPDATE GLOSSARY
								SET NEXT_UNIQUE_ID = (@PHONE_ID + 1)
								WHERE SYSTEM_TABLE_NAME = ''ADDRESS_X_PHONEINFO''
							END
						END' 
					
		EXECUTE sp_executesql @nvSQL 
[EXECUTE_SP]	
GO
