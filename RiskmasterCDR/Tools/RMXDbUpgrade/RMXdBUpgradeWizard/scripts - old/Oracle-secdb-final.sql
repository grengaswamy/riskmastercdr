[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_DOMAIN_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON DOMAIN
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO DOMAIN_AUDIT
            VALUES (:new.DOMAINID, :new.NAME, :new.PROVIDER, :new.ENABLED, :new.UPDATED_BY_USER, :new.DTTM_RCD_LAST_UPD);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO DOMAIN_AUDIT
            VALUES (:old.DOMAINID,:old.NAME, :old.PROVIDER,:old.ENABLED,:old.UPDATED_BY_USER, :old.DTTM_RCD_LAST_UPD);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO DOMAIN_AUDIT
            VALUES (:new.DOMAINID,:new.NAME,:new.PROVIDER,:new.ENABLED,:new.UPDATED_BY_USER, :new.DTTM_RCD_LAST_UPD);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_SETTINGS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SETTINGS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO SETTINGS_AUDIT
            VALUES (:new.ID,:new.PARAMU, :new.PARAMP, :new.SECURE_DSN_LOGIN, :new.SMTP_SERVER, :new.ADMIN_EMAIL_ADDR, :new.DOMAIN_LOGIN_ENABLED, :new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD, ,:new.ALTERNATE_DOMAIN, :new.REPLYTO_DOMAIN );
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO SETTINGS_AUDIT
            VALUES (:old.ID,:old.PARAMU, :old.PARAMP, :old.SECURE_DSN_LOGIN, :old.SMTP_SERVER, :old.ADMIN_EMAIL_ADDR, :old.DOMAIN_LOGIN_ENABLED, :old.UPDATED_BY_USER,:old.DTTM_RCD_LAST_UPD, :old.ALTERNATE_DOMAIN, :old.REPLYTO_DOMAIN);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO SETTINGS_AUDIT
            VALUES (:new.ID,:new.PARAMU, :new.PARAMP, :new.SECURE_DSN_LOGIN, :new.SMTP_SERVER, :new.ADMIN_EMAIL_ADDR, :new.DOMAIN_LOGIN_ENABLED, :new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD, ,:new.ALTERNATE_DOMAIN,:new.REPLYTO_DOMAIN);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_USER_DETAILS_TABLE_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON USER_DETAILS_TABLE
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO USER_DETAILS_TABLE_AUDIT
            VALUES (:new.dsnid, :new.user_id, :new.login_name, :new.password, :new.privs_expire, :new.passwd_expire, :new.doc_path, :new.sun_start, :new.sun_end, :new.mon_start, :new.mon_end, :new.tues_start, :new.tues_end, :new.weds_start, :new.weds_end, :new.thurs_start, :new.thurs_end, :new.fri_start, :new.fri_end, :new.sat_start, :new.sat_end, :new.checksum, :new.force_change_pwd, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO USER_DETAILS_TABLE_AUDIT
            VALUES (:old.dsnid, :old.user_id, :old.login_name, :old.password, :old.privs_expire, :old.passwd_expire, :old.doc_path, :old.sun_start, :old.sun_end, :old.mon_start, :old.mon_end, :old.tues_start, :old.tues_end, :old.weds_start, :old.weds_end, :old.thurs_start, :old.thurs_end, :old.fri_start, :old.fri_end, :old.sat_start, :old.sat_end, :old.checksum, :old.force_change_pwd, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO USER_DETAILS_TABLE_AUDIT
            VALUES (:new.dsnid, :new.user_id, :new.login_name, :new.password, :new.privs_expire, :new.passwd_expire, :new.doc_path, :new.sun_start, :new.sun_end, :new.mon_start, :new.mon_end, :new.tues_start, :new.tues_end, :new.weds_start, :new.weds_end, :new.thurs_start, :new.thurs_end, :new.fri_start, :new.fri_end, :new.sat_start, :new.sat_end, :new.checksum, :new.force_change_pwd, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
create or replace TRIGGER TRIG_USER_ORGSEC_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON USER_ORGSEC
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO USER_ORGSEC_AUDIT
            VALUES (:new.DSN_ID, :new.USER_ID, :new.GROUP_ID, :new.DB_UID, :new.DB_PWD, :new.UPDATED_BY_USER, :new.DTTM_RCD_LAST_UPD,:new.PWD_CHANGED_FLAG);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO USER_ORGSEC_AUDIT
            VALUES (:old.DSN_ID, :old.USER_ID, :old.GROUP_ID, :old.DB_UID, :old.DB_PWD, :old.UPDATED_BY_USER, :old.DTTM_RCD_LAST_UPD,:old.PWD_CHANGED_FLAG);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO USER_ORGSEC_AUDIT
            VALUES (:new.DSN_ID, :new.USER_ID, :new.GROUP_ID, :new.DB_UID, :new.DB_PWD, :new.UPDATED_BY_USER, :new.DTTM_RCD_LAST_UPD,:new.PWD_CHANGED_FLAG);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
create or replace TRIGGER TRIG_USER_TABLE_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON USER_TABLE
   FOR EACH ROW

   BEGIN

      -- record new employee primary key
      IF INSERTING THEN
         INSERT INTO USER_TABLE_AUDIT
            VALUES (:new.user_id, :new.last_name, :new.first_name, :new.address1, :new.address2, :new.city, :new.state_abbr, :new.country, :new.zip_code, :new.email_addr, :new.manager_id, :new.home_phone, :new.office_phone, :new.title, :new.nls_code, :new.checksum, :new.company_name, :new.account_lock_status, :new.updated_by_user, :new.dttm_rcd_last_upd,:new.is_sms_user,:new.dttm_account_lock, :new.IS_USRPVG_ACCESS);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN
         INSERT INTO USER_TABLE_AUDIT
            VALUES (:old.user_id, :old.last_name, :old.first_name, :old.address1, :old.address2, :old.city, :old.state_abbr, :old.country, :old.zip_code, :old.email_addr, :old.manager_id, :old.home_phone, :old.office_phone, :old.title, :old.nls_code, :old.checksum, :old.company_name, :old.account_lock_status, :old.updated_by_user, :old.dttm_rcd_last_upd,:old.is_sms_user,:old.dttm_account_lock, :old.IS_USRPVG_ACCESS);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE
         INSERT INTO USER_TABLE_AUDIT
            VALUES (:new.user_id, :new.last_name, :new.first_name, :new.address1, :new.address2, :new.city, :new.state_abbr, :new.country, :new.zip_code, :new.email_addr, :new.manager_id, :new.home_phone, :new.office_phone, :new.title, :new.nls_code, :new.checksum, :new.company_name, :new.account_lock_status, :new.updated_by_user, :new.dttm_rcd_last_upd,:new.is_sms_user,:new.dttm_account_lock, :new.IS_USRPVG_ACCESS);

      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
create or replace TRIGGER TRIG_DATA_SOURCE_TABLE_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON DATA_SOURCE_TABLE
   FOR EACH ROW

   BEGIN

      -- record new employee primary key
      IF INSERTING THEN
         INSERT INTO DATA_SOURCE_TABLE_AUDIT
            VALUES (:new.orgsec_flag, :new.dsn, :new.dsnid, :new.status, :new.rm_userid, :new.rm_password, :new.dbtype, :new.checksum, :new.global_doc_path, :new.connection_string, :new.num_licenses, :new.lic_upd_date, :new.crc2, :new.doc_path_type, :new.updated_by_user, :new.dttm_rcd_last_upd,:new.def_bes_flag,:new.conf_flag,:new.primary_key_task);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN
         INSERT INTO DATA_SOURCE_TABLE_AUDIT
            VALUES (:old.orgsec_flag, :old.dsn, :old.dsnid, :old.status, :old.rm_userid, :old.rm_password, :old.dbtype, :old.checksum, :old.global_doc_path, :old.connection_string, :old.num_licenses, :old.lic_upd_date, :old.crc2, :old.doc_path_type, :old.updated_by_user, :old.dttm_rcd_last_upd,:old.def_bes_flag,:old.conf_flag,:old.primary_key_task);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE
         INSERT INTO DATA_SOURCE_TABLE_AUDIT
            VALUES (:new.orgsec_flag, :new.dsn, :new.dsnid, :new.status, :new.rm_userid, :new.rm_password, :new.dbtype, :new.checksum, :new.global_doc_path, :new.connection_string, :new.num_licenses, :new.lic_upd_date, :new.crc2, :new.doc_path_type, :new.updated_by_user, :new.dttm_rcd_last_upd,:new.def_bes_flag,:new.conf_flag,:new.primary_key_task);

      END IF;
END;
/
GO