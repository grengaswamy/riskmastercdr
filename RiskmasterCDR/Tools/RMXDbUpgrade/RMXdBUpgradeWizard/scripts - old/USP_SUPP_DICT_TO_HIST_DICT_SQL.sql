--exec USP_SUPP_DICT_TO_HIST_DICT
-- =============================================
-- Author:		CSC
-- Create date: 06/04/2010
-- Description:	Copies Data from Supp_dictionary To Hist_Track_Dictionary
-- =============================================
[GO_DELIMITER]
DECLARE @nvSQL AS nVARCHAR(max) 
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'USP_SUPP_DICT_TO_HIST_DICT' AND TYPE = 'P')
	BEGIN
		DROP PROCEDURE USP_SUPP_DICT_TO_HIST_DICT
	END
	SET @nvSQL = 'CREATE PROCEDURE USP_SUPP_DICT_TO_HIST_DICT
				  AS
				  BEGIN
					-- SET NOCOUNT ON added to prevent extra result sets from
					-- interfering with SELECT statements.
					SET NOCOUNT ON;

					
					DECLARE @iMinID INT,
							@iMaxID INT,
							@iLoopCount INT,
							@iFLAG INT
					BEGIN
						SELECT @iMinID = MIN(FIELD_ID) FROM SUPP_DICTIONARY				 
				
						SELECT @iMaxID = MAX(FIELD_ID) FROM SUPP_DICTIONARY
				
						BEGIN 
							SET @iLoopCount = @iMinID
							WHILE @iLoopCount <= @iMaxID
							BEGIN
								SELECT  @iFLAG = COUNT(*)  FROM SUPP_DICTIONARY  WHERE FIELD_ID BETWEEN  @iLoopCount AND (@iLoopCount + 4999)
										 
								IF (@iFLAG > 0 )
									BEGIN
										INSERT INTO HIST_TRACK_DICTIONARY(TABLE_ID,COLUMN_NAME,USER_PROMPT,RMDATA_TYPE,PRIMARY_KEY_FLAG,SYSTEM_TABLE_NAME,DATATYPE,NUMERIC_PRECISION,COLUMN_SIZE)
										(SELECT
										HTT.TABLE_ID,
										UPPER(SD.SYS_FIELD_NAME),
										CASE WHEN SD.USER_PROMPT IS NOT NULL THEN SD.USER_PROMPT
											 ELSE SD.SYS_FIELD_NAME 
											 END,
										SD.FIELD_TYPE,
										CASE WHEN FIELD_TYPE=7 THEN -1
											 ELSE 0 
											 END,
										CASE WHEN SD.CODE_FILE_ID <> 0 THEN G.SYSTEM_TABLE_NAME
											 ELSE NULL 
											 END,
										Case 
											WHEN FIELD_TYPE IN (0,3,4,10,12,13) THEN  ''VARCHAR''
											WHEN FIELD_TYPE IN (1,2) THEN ''FLOAT''
											WHEN FIELD_TYPE IN (6,7,8,9,14,15,16) THEN  ''INT''
											END,
										CASE WHEN Field_Type IN (1,2) THEN  53
											WHEN Field_Type IN (6,7,8,9,14,15,16) THEN  10 
											ELSE 0 
											END,
											SD.FIELD_SIZE
										FROM SUPP_DICTIONARY SD INNER JOIN HIST_TRACK_TABLES HTT
										 ON HTT.TABLE_NAME = SD.SUPP_TABLE_NAME 
										INNER JOIN GLOSSARY G ON SD.CODE_FILE_ID=G.TABLE_ID
										WHERE SD.FIELD_TYPE NOT IN (5,11,17)
										 AND SD.DELETE_FLAG=0
										 AND SD.FIELD_ID >= @iLoopCount AND SD.FIELD_ID <= (@iLoopCount + 4999))								
									END 
								SET @iLoopCount = @iLoopCount + 5000
							END
						END
					END							
				  END'
				   
					
		EXECUTE sp_executesql @nvSQL

[EXECUTE_SP]	
GO
