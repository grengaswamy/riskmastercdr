[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_SYS_LOB_RESERVES_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SYS_LOB_RESERVES
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO SYS_LOB_RESERVES_AUDIT
            VALUES (:new.LINE_OF_BUS_CODE,:new.RESERVE_TYPE_CODE,:new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO SYS_LOB_RESERVES_AUDIT
            VALUES (:old.LINE_OF_BUS_CODE,:old.RESERVE_TYPE_CODE,:old.UPDATED_BY_USER,:old.DTTM_RCD_LAST_UPD);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO SYS_LOB_RESERVES_AUDIT
            VALUES (:new.LINE_OF_BUS_CODE,:new.RESERVE_TYPE_CODE,:new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
create or replace
TRIGGER TRIG_SYS_PARMS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SYS_PARMS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO SYS_PARMS_AUDIT
           VALUES (:new.acc_link_flag,:new.blank_field_flag,:new.fin_hist_eval,:new.hierarchy_level,:new.print_claims_admin,:new.print_osha_desc,:new.wc_printer_setup,:new.row_id,:new.oc_fac_addr1,:new.oc_fac_addr2,:new.oc_fac_addr3,:new.oc_fac_addr4,:new.oc_fac_contact,:new.oc_fac_hcfa_no,:new.oc_fac_name,:new.oc_fac_phone,:new.work_fri,:new.work_mon,:new.work_sat,:new.work_sun,:new.work_thu,:new.work_tue,:new.work_wed,:new.security_flag,:new.date_stamp_flag,:new.shift_1_name,:new.shift_2_name,:new.shift_3_name,:new.shift_1_start,:new.shift_2_start,:new.shift_3_start,:new.shift_1_end,:new.shift_2_end,:new.shift_3_end,:new.client_name,:new.essp,:new.client_status_code,:new.date_installed,:new.addr1,:new.addr2,:new.city,:new.state,:new.zip_code,:new.phone_number,:new.fax_number,:new.primary_contact,:new.primary_title,:new.secondary_contact,:new.secondary_title,:new.data_entry_contact,:new.data_entry_title,:new.reporting_contact,:new.reporting_title,:new.dept_mgr_contact,:new.dept_mgr_title,:new.accounting_contact,:new.accounting_title,:new.dtg_product,:new.dtg_version,:new.hardware,:new.printer,:new.distribution_media,:new.modem_phone,:new.dtg_support_phone,:new.ev_prefix,:new.ev_inc_year_flag,:new.freeze_text_flag,:new.release_number,:new.use_sub_account,:new.ev_type_acc_flag,:new.x_axis,:new.y_axis,:new.sm_limit_direct,:new.res_do_not_recalc,:new.smtp_server,:new.excl_holidays,:new.use_default_carrier,:new.attach_diary_vis,:new.use_def_carrier,:new.froi_preparer,:new.usetande,:new.rate_level,:new.tandeacct_id,:new.freeze_date_flag,:new.date_adjuster_flag,:new.froi_bat_high_lvl,:new.froi_bat_low_lvl,:new.froi_bat_cld_clm,:new.auto_num_wc_emp,:new.wc_emp_prefix,:new.edit_wc_emp_num,:new.auto_num_vin,:new.auto_select_policy,:new.prev_res_backdating,:new.prev_res_modifyzero,:new.diary_org_level,:new.allow_global_peek,:new.res_show_orphans,:new.no_sub_dep_check,:new.default_time_flag,:new.default_dept_flag,:new.filter_oth_people,:new.freeze_event_desc,:new.use_mast_bank_acct,:new.p2_login,:new.p2_pass,:new.date_event_desc,:new.order_bank_acc,:new.supp_fld_sec_flag,:new.enh_hierarchy_list,:new.ev_caption_level,:new.use_enh_pol_flag,:new.prev_res_belowpaid,:new.assign_sub_account,:new.p2_url,:new.oracle_case_ins,:new.use_claimant_proc_flag,:new.claimant_process_url,:new.claimant_process_login,:new.claimant_process_pass,:new.hosp_phys_any_ent,:new.freeze_loc_desc,:new.leave_rsv_status,:new.use_billing_flag,:new.override_eob_print,:new.use_fl_wc_max_flag,:new.use_evt_loc_flag,:new.updated_by_user,:new.dttm_rcd_last_upd,:new.tax_id,:new.doing_bus_as,:new.legal_name,:new.mmsea_tin_edt_flag,:new.product_build,:new.multiple_reinsurer,:new.use_prgm_type,:new.use_broker_bes,:new.auto_launch_diary,:new.use_acrosoft_interface,:new.rmx_lss_user,:new.rmx_lss_enable,:new.multiple_insurer,:new.policy_drop_down,:new.delete_all_claim_diaries,:new.enh_print_order1,:new.enh_print_order2,:new.enh_print_order3,:new.frz_existing_notes,:new.use_all_trans_types,:new.default_assigned_to,:new.allow_td_filter,:new.enh_note_time_limit,:new.enh_note_edt_rights,:new.enh_bes,:new.use_res_filter,:new.show_jurisdiction,:new.state_eval,:new.create_wl_res_rcds,:new.clm_ltr_flag,:new.auto_froiacord_flag,:new.use_legacy_comments,:new.org_timezone_level);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO SYS_PARMS_AUDIT
          VALUES (:old.acc_link_flag,:old.blank_field_flag,:old.fin_hist_eval,:old.hierarchy_level,:old.print_claims_admin,:old.print_osha_desc,:old.wc_printer_setup,:old.row_id,:old.oc_fac_addr1,:old.oc_fac_addr2,:old.oc_fac_addr3,:old.oc_fac_addr4,:old.oc_fac_contact,:old.oc_fac_hcfa_no,:old.oc_fac_name,:old.oc_fac_phone,:old.work_fri,:old.work_mon,:old.work_sat,:old.work_sun,:old.work_thu,:old.work_tue,:old.work_wed,:old.security_flag,:old.date_stamp_flag,:old.shift_1_name,:old.shift_2_name,:old.shift_3_name,:old.shift_1_start,:old.shift_2_start,:old.shift_3_start,:old.shift_1_end,:old.shift_2_end,:old.shift_3_end,:old.client_name,:old.essp,:old.client_status_code,:old.date_installed,:old.addr1,:old.addr2,:old.city,:old.state,:old.zip_code,:old.phone_number,:old.fax_number,:old.primary_contact,:old.primary_title,:old.secondary_contact,:old.secondary_title,:old.data_entry_contact,:old.data_entry_title,:old.reporting_contact,:old.reporting_title,:old.dept_mgr_contact,:old.dept_mgr_title,:old.accounting_contact,:old.accounting_title,:old.dtg_product,:old.dtg_version,:old.hardware,:old.printer,:old.distribution_media,:old.modem_phone,:old.dtg_support_phone,:old.ev_prefix,:old.ev_inc_year_flag,:old.freeze_text_flag,:old.release_number,:old.use_sub_account,:old.ev_type_acc_flag,:old.x_axis,:old.y_axis,:old.sm_limit_direct,:old.res_do_not_recalc,:old.smtp_server,:old.excl_holidays,:old.use_default_carrier,:old.attach_diary_vis,:old.use_def_carrier,:old.froi_preparer,:old.usetande,:old.rate_level,:old.tandeacct_id,:old.freeze_date_flag,:old.date_adjuster_flag,:old.froi_bat_high_lvl,:old.froi_bat_low_lvl,:old.froi_bat_cld_clm,:old.auto_num_wc_emp,:old.wc_emp_prefix,:old.edit_wc_emp_num,:old.auto_num_vin,:old.auto_select_policy,:old.prev_res_backdating,:old.prev_res_modifyzero,:old.diary_org_level,:old.allow_global_peek,:old.res_show_orphans,:old.no_sub_dep_check,:old.default_time_flag,:old.default_dept_flag,:old.filter_oth_people,:old.freeze_event_desc,:old.use_mast_bank_acct,:old.p2_login,:old.p2_pass,:old.date_event_desc,:old.order_bank_acc,:old.supp_fld_sec_flag,:old.enh_hierarchy_list,:old.ev_caption_level,:old.use_enh_pol_flag,:old.prev_res_belowpaid,:old.assign_sub_account,:old.p2_url,:old.oracle_case_ins,:old.use_claimant_proc_flag,:old.claimant_process_url,:old.claimant_process_login,:old.claimant_process_pass,:old.hosp_phys_any_ent,:old.freeze_loc_desc,:old.leave_rsv_status,:old.use_billing_flag,:old.override_eob_print,:old.use_fl_wc_max_flag,:old.use_evt_loc_flag,:old.updated_by_user,:old.dttm_rcd_last_upd,:old.tax_id,:old.doing_bus_as,:old.legal_name,:old.mmsea_tin_edt_flag,:old.product_build,:old.multiple_reinsurer,:old.use_prgm_type,:old.use_broker_bes,:old.auto_launch_diary,:old.use_acrosoft_interface,:old.rmx_lss_user,:old.rmx_lss_enable,:old.multiple_insurer,:old.policy_drop_down,:old.delete_all_claim_diaries,:old.enh_print_order1,:old.enh_print_order2,:old.enh_print_order3,:old.frz_existing_notes,:old.use_all_trans_types,:old.default_assigned_to,:old.allow_td_filter,:old.enh_note_time_limit,:old.enh_note_edt_rights,:old.enh_bes,:old.use_res_filter,:old.show_jurisdiction,:old.state_eval,:old.create_wl_res_rcds,:old.clm_ltr_flag,:old.auto_froiacord_flag,:old.use_legacy_comments,:old.org_timezone_level);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO SYS_PARMS_AUDIT
          VALUES (:new.acc_link_flag,:new.blank_field_flag,:new.fin_hist_eval,:new.hierarchy_level,:new.print_claims_admin,:new.print_osha_desc,:new.wc_printer_setup,:new.row_id,:new.oc_fac_addr1,:new.oc_fac_addr2,:new.oc_fac_addr3,:new.oc_fac_addr4,:new.oc_fac_contact,:new.oc_fac_hcfa_no,:new.oc_fac_name,:new.oc_fac_phone,:new.work_fri,:new.work_mon,:new.work_sat,:new.work_sun,:new.work_thu,:new.work_tue,:new.work_wed,:new.security_flag,:new.date_stamp_flag,:new.shift_1_name,:new.shift_2_name,:new.shift_3_name,:new.shift_1_start,:new.shift_2_start,:new.shift_3_start,:new.shift_1_end,:new.shift_2_end,:new.shift_3_end,:new.client_name,:new.essp,:new.client_status_code,:new.date_installed,:new.addr1,:new.addr2,:new.city,:new.state,:new.zip_code,:new.phone_number,:new.fax_number,:new.primary_contact,:new.primary_title,:new.secondary_contact,:new.secondary_title,:new.data_entry_contact,:new.data_entry_title,:new.reporting_contact,:new.reporting_title,:new.dept_mgr_contact,:new.dept_mgr_title,:new.accounting_contact,:new.accounting_title,:new.dtg_product,:new.dtg_version,:new.hardware,:new.printer,:new.distribution_media,:new.modem_phone,:new.dtg_support_phone,:new.ev_prefix,:new.ev_inc_year_flag,:new.freeze_text_flag,:new.release_number,:new.use_sub_account,:new.ev_type_acc_flag,:new.x_axis,:new.y_axis,:new.sm_limit_direct,:new.res_do_not_recalc,:new.smtp_server,:new.excl_holidays,:new.use_default_carrier,:new.attach_diary_vis,:new.use_def_carrier,:new.froi_preparer,:new.usetande,:new.rate_level,:new.tandeacct_id,:new.freeze_date_flag,:new.date_adjuster_flag,:new.froi_bat_high_lvl,:new.froi_bat_low_lvl,:new.froi_bat_cld_clm,:new.auto_num_wc_emp,:new.wc_emp_prefix,:new.edit_wc_emp_num,:new.auto_num_vin,:new.auto_select_policy,:new.prev_res_backdating,:new.prev_res_modifyzero,:new.diary_org_level,:new.allow_global_peek,:new.res_show_orphans,:new.no_sub_dep_check,:new.default_time_flag,:new.default_dept_flag,:new.filter_oth_people,:new.freeze_event_desc,:new.use_mast_bank_acct,:new.p2_login,:new.p2_pass,:new.date_event_desc,:new.order_bank_acc,:new.supp_fld_sec_flag,:new.enh_hierarchy_list,:new.ev_caption_level,:new.use_enh_pol_flag,:new.prev_res_belowpaid,:new.assign_sub_account,:new.p2_url,:new.oracle_case_ins,:new.use_claimant_proc_flag,:new.claimant_process_url,:new.claimant_process_login,:new.claimant_process_pass,:new.hosp_phys_any_ent,:new.freeze_loc_desc,:new.leave_rsv_status,:new.use_billing_flag,:new.override_eob_print,:new.use_fl_wc_max_flag,:new.use_evt_loc_flag,:new.updated_by_user,:new.dttm_rcd_last_upd,:new.tax_id,:new.doing_bus_as,:new.legal_name,:new.mmsea_tin_edt_flag,:new.product_build,:new.multiple_reinsurer,:new.use_prgm_type,:new.use_broker_bes,:new.auto_launch_diary,:new.use_acrosoft_interface,:new.rmx_lss_user,:new.rmx_lss_enable,:new.multiple_insurer,:new.policy_drop_down,:new.delete_all_claim_diaries,:new.enh_print_order1,:new.enh_print_order2,:new.enh_print_order3,:new.frz_existing_notes,:new.use_all_trans_types,:new.default_assigned_to,:new.allow_td_filter,:new.enh_note_time_limit,:new.enh_note_edt_rights,:new.enh_bes,:new.use_res_filter,:new.show_jurisdiction,:new.state_eval,:new.create_wl_res_rcds,:new.clm_ltr_flag,:new.auto_froiacord_flag,:new.use_legacy_comments,:new.org_timezone_level);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
create or replace
TRIGGER TRIG_SYS_PARMS_LOB_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SYS_PARMS_LOB
   FOR EACH ROW

   BEGIN

      -- record new employee primary key
      IF INSERTING THEN
         INSERT INTO SYS_PARMS_LOB_AUDIT
            VALUES (:new.clm_acc_lmt_flag, :new.duplicate_flag, :new.inc_year_flag, :new.insuf_funds_flag, :new.pay_det_lmt_flag, :new.pay_lmt_flag, :new.prt_chk_lmt_flag, :new.res_lmt_flag, :new.reserve_tracking, :new.line_of_bus_code, :new.adj_rsv, :new.coll_in_rsv_bal, :new.bal_to_zero, :new.neg_bal_to_zero, :new.set_to_zero, :new.event_trigger, :new.inc_clm_type_flag, :new.inc_lob_code_flag, :new.inc_org_flag, :new.org_table_id, :new.duration_code, :new.est_num_of_claims, :new.noneg_res_adj_flag, :new.res_by_clm_type, :new.use_adjtext, :new.caption_level, :new.adj_rsv_claim, :new.per_cl_lmt_flag, :new.dup_pay_criteria, :new.dup_num_days, :new.no_weeks_per_month, :new.excl_neg_amts_flag, :new.coll_in_incur_bal, :new.use_autochk_reserve_flag, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.use_claim_progress_notes, :new.use_claim_comments,:new.use_rsv_wk,:new.attach_rsv_wk,:new.reason_comments_rsv_wk,:new.use_iso_submission,:new.use_enh_pol_flag,:new.is_policy_filter);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN
         INSERT INTO SYS_PARMS_LOB_AUDIT
            VALUES (:old.clm_acc_lmt_flag, :old.duplicate_flag, :old.inc_year_flag, :old.insuf_funds_flag, :old.pay_det_lmt_flag, :old.pay_lmt_flag, :old.prt_chk_lmt_flag, :old.res_lmt_flag, :old.reserve_tracking, :old.line_of_bus_code, :old.adj_rsv, :old.coll_in_rsv_bal, :old.bal_to_zero, :old.neg_bal_to_zero, :old.set_to_zero, :old.event_trigger, :old.inc_clm_type_flag, :old.inc_lob_code_flag, :old.inc_org_flag, :old.org_table_id, :old.duration_code, :old.est_num_of_claims, :old.noneg_res_adj_flag, :old.res_by_clm_type, :old.use_adjtext, :old.caption_level, :old.adj_rsv_claim, :old.per_cl_lmt_flag, :old.dup_pay_criteria, :old.dup_num_days, :old.no_weeks_per_month, :old.excl_neg_amts_flag, :old.coll_in_incur_bal, :old.use_autochk_reserve_flag, :old.updated_by_user, :old.dttm_rcd_last_upd, :old.use_claim_progress_notes, :old.use_claim_comments,:old.use_rsv_wk,:old.attach_rsv_wk,:old.reason_comments_rsv_wk,:old.use_iso_submission,:old.use_enh_pol_flag,:old.is_policy_filter);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE
         INSERT INTO SYS_PARMS_LOB_AUDIT
            VALUES (:new.clm_acc_lmt_flag, :new.duplicate_flag, :new.inc_year_flag, :new.insuf_funds_flag, :new.pay_det_lmt_flag, :new.pay_lmt_flag, :new.prt_chk_lmt_flag, :new.res_lmt_flag, :new.reserve_tracking, :new.line_of_bus_code, :new.adj_rsv, :new.coll_in_rsv_bal, :new.bal_to_zero, :new.neg_bal_to_zero, :new.set_to_zero, :new.event_trigger, :new.inc_clm_type_flag, :new.inc_lob_code_flag, :new.inc_org_flag, :new.org_table_id, :new.duration_code, :new.est_num_of_claims, :new.noneg_res_adj_flag, :new.res_by_clm_type, :new.use_adjtext, :new.caption_level, :new.adj_rsv_claim, :new.per_cl_lmt_flag, :new.dup_pay_criteria, :new.dup_num_days, :new.no_weeks_per_month, :new.excl_neg_amts_flag, :new.coll_in_incur_bal, :new.use_autochk_reserve_flag, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.use_claim_progress_notes, :new.use_claim_comments,:new.use_rsv_wk,:new.attach_rsv_wk,:new.reason_comments_rsv_wk,:new.use_iso_submission,:new.use_enh_pol_flag,:new.is_policy_filter);

      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_EVENT_ACC_LIMITS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON EVENT_ACC_LIMITS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO EVENT_ACC_LIMITS_AUDIT
            VALUES (:new.GROUP_ID,:new.USER_ID,:new.EVENT_TYPE_CODE,:new.CREATE_FLAG,:new.ACCESS_FLAG,:new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO EVENT_ACC_LIMITS_AUDIT
            VALUES (:old.GROUP_ID,:old.USER_ID,:old.EVENT_TYPE_CODE,:old.CREATE_FLAG,:old.ACCESS_FLAG,:old.UPDATED_BY_USER,:old.DTTM_RCD_LAST_UPD);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO EVENT_ACC_LIMITS_AUDIT
            VALUES (:new.GROUP_ID,:new.USER_ID,:new.EVENT_TYPE_CODE,:new.CREATE_FLAG,:new.ACCESS_FLAG,:new.UPDATED_BY_USER,:new.DTTM_RCD_LAST_UPD);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_FROI_OPTIONS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON FROI_OPTIONS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO FROI_OPTIONS_AUDIT
            VALUES (:new.base_level, :new.selected_eid, :new.carrier_org_level, :new.clmadmin_org_level, :new.attach_to_claim, :new.def_clam_admin_eid, :new.def_carrier_eid, :new.employer_level, :new.froi_preparer, :new.print_claim_admin, :new.print_osha_desc, :new.use_def_carrier, :new.virgin_parnt_level, :new.dttm_rcd_added, :new.added_by_user, :new.dttm_rcd_last_upd, :new.updated_by_user, :new.acord_copies, :new.print_acord_forms, :new.use_title, :new.force_print_ncci, :new.work_loss_field, :new.suspres_deflt_time, :new.contact_option, :new.juris_row_id, :new.ne_parent_option, :new.ne_parent_org_hier, :new.ne_parent_def_id, :new.tn_parent_option, :new.tn_parent_org_hier, :new.tn_parent_def_id, :new.va_parent_option, :new.va_parent_def_id, :new.print_clmnumb_top, :new.claimadmin_tpa_eid, :new.person_notfied_opt, :new.carr_claim_num_opt, :new.empee_hme_phne_opt, :new.tpa_use_opt, :new.tpa_eid);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO FROI_OPTIONS_AUDIT
            VALUES (:old.base_level, :old.selected_eid, :old.carrier_org_level, :old.clmadmin_org_level, :old.attach_to_claim, :old.def_clam_admin_eid, :old.def_carrier_eid, :old.employer_level, :old.froi_preparer, :old.print_claim_admin, :old.print_osha_desc, :old.use_def_carrier, :old.virgin_parnt_level, :old.dttm_rcd_added, :old.added_by_user, :old.dttm_rcd_last_upd, :old.updated_by_user, :old.acord_copies, :old.print_acord_forms, :old.use_title, :old.force_print_ncci, :old.work_loss_field, :old.suspres_deflt_time, :old.contact_option, :old.juris_row_id, :old.ne_parent_option, :old.ne_parent_org_hier, :old.ne_parent_def_id, :old.tn_parent_option, :old.tn_parent_org_hier, :old.tn_parent_def_id, :old.va_parent_option, :old.va_parent_def_id, :old.print_clmnumb_top, :old.claimadmin_tpa_eid, :old.person_notfied_opt, :old.carr_claim_num_opt, :old.empee_hme_phne_opt, :old.tpa_use_opt, :old.tpa_eid);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO FROI_OPTIONS_AUDIT
            VALUES (:new.base_level, :new.selected_eid, :new.carrier_org_level, :new.clmadmin_org_level, :new.attach_to_claim, :new.def_clam_admin_eid, :new.def_carrier_eid, :new.employer_level, :new.froi_preparer, :new.print_claim_admin, :new.print_osha_desc, :new.use_def_carrier, :new.virgin_parnt_level, :new.dttm_rcd_added, :new.added_by_user, :new.dttm_rcd_last_upd, :new.updated_by_user, :new.acord_copies, :new.print_acord_forms, :new.use_title, :new.force_print_ncci, :new.work_loss_field, :new.suspres_deflt_time, :new.contact_option, :new.juris_row_id, :new.ne_parent_option, :new.ne_parent_org_hier, :new.ne_parent_def_id, :new.tn_parent_option, :new.tn_parent_org_hier, :new.tn_parent_def_id, :new.va_parent_option, :new.va_parent_def_id, :new.print_clmnumb_top, :new.claimadmin_tpa_eid, :new.person_notfied_opt, :new.carr_claim_num_opt, :new.empee_hme_phne_opt, :new.tpa_use_opt, :new.tpa_eid);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_FUNDS_LIMITS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON FUNDS_LIMITS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO FUNDS_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO FUNDS_LIMITS_AUDIT
            VALUES (:old.group_id, :old.user_id, :old.line_of_bus_code, :old.max_amount, :old.updated_by_user, :old.dttm_rcd_last_upd, :old.CLAIM_TYPE_CODE);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO FUNDS_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_FUNDS_DET_LIMITS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON FUNDS_DET_LIMITS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO FUNDS_DET_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.RESERVE_TYPE_CODE, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO FUNDS_DET_LIMITS_AUDIT
            VALUES (:old.group_id, :old.user_id, :old.line_of_bus_code, :old.RESERVE_TYPE_CODE, :old.max_amount, :old.updated_by_user, :old.dttm_rcd_last_upd, :old.CLAIM_TYPE_CODE);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO FUNDS_DET_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.RESERVE_TYPE_CODE, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_GROUP_PERMISSIONS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON GROUP_PERMISSIONS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO GROUP_PERMISSIONS_AUDIT
            VALUES (:new.group_id, :new.FUNC_ID, :new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO GROUP_PERMISSIONS_AUDIT
            VALUES (:old.group_id, :old.FUNC_ID, :old.CRC, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO GROUP_PERMISSIONS_AUDIT
            VALUES (:new.group_id, :new.FUNC_ID, :new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_JURIS_OPTIONS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON JURIS_OPTIONS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO JURIS_OPTIONS_AUDIT
            VALUES (:new.base_level, :new.juris_row_id, :new.selected_eid, :new.carrier_org_level, :new.clmadmin_org_level, :new.attach_to_claim, :new.def_clam_admin_eid, :new.def_carrier_eid, :new.employer_level, :new.froi_preparer, :new.print_claim_admin, :new.print_osha_desc, :new.use_def_carrier, :new.virgin_parnt_level, :new.dttm_rcd_added, :new.added_by_user, :new.dttm_rcd_last_upd, :new.updated_by_user, :new.acord_copies, :new.print_acord_forms, :new.claimadmin_tpa_eid, :new.tpa_eid, :new.carr_claim_num_opt, :new.empee_hme_phne_opt, :new.check_status_opt, :new.tpa_use_opt);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO JURIS_OPTIONS_AUDIT
            VALUES (:old.base_level, :old.juris_row_id, :old.selected_eid, :old.carrier_org_level, :old.clmadmin_org_level, :old.attach_to_claim, :old.def_clam_admin_eid, :old.def_carrier_eid, :old.employer_level, :old.froi_preparer, :old.print_claim_admin, :old.print_osha_desc, :old.use_def_carrier, :old.virgin_parnt_level, :old.dttm_rcd_added, :old.added_by_user, :old.dttm_rcd_last_upd, :old.updated_by_user, :old.acord_copies, :old.print_acord_forms, :old.claimadmin_tpa_eid, :old.tpa_eid, :old.carr_claim_num_opt, :old.empee_hme_phne_opt, :old.check_status_opt, :old.tpa_use_opt);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO JURIS_OPTIONS_AUDIT
            VALUES (:new.base_level, :new.juris_row_id, :new.selected_eid, :new.carrier_org_level, :new.clmadmin_org_level, :new.attach_to_claim, :new.def_clam_admin_eid, :new.def_carrier_eid, :new.employer_level, :new.froi_preparer, :new.print_claim_admin, :new.print_osha_desc, :new.use_def_carrier, :new.virgin_parnt_level, :new.dttm_rcd_added, :new.added_by_user, :new.dttm_rcd_last_upd, :new.updated_by_user, :new.acord_copies, :new.print_acord_forms, :new.claimadmin_tpa_eid, :new.tpa_eid, :new.carr_claim_num_opt, :new.empee_hme_phne_opt, :new.check_status_opt, :new.tpa_use_opt);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_LOB_SUP_APPROVAL_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON LOB_SUP_APPROVAL
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO LOB_SUP_APPROVAL_AUDIT
            VALUES (:new.LOB_CODE, :new.USER_ID, :new.PAYMENT_MAX, :new.RESERVE_MAX, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO LOB_SUP_APPROVAL_AUDIT
            VALUES (:old.LOB_CODE, :old.USER_ID, :old.PAYMENT_MAX, :old.RESERVE_MAX, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO LOB_SUP_APPROVAL_AUDIT
            VALUES (:new.LOB_CODE, :new.USER_ID, :new.PAYMENT_MAX, :new.RESERVE_MAX, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_RESERVE_LIMITS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON RESERVE_LIMITS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO RESERVE_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.RESERVE_TYPE_CODE, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO RESERVE_LIMITS_AUDIT
            VALUES (:old.group_id, :old.user_id, :old.line_of_bus_code, :old.RESERVE_TYPE_CODE, :old.max_amount, :old.updated_by_user, :old.dttm_rcd_last_upd, :old.CLAIM_TYPE_CODE);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO RESERVE_LIMITS_AUDIT
            VALUES (:new.group_id, :new.user_id, :new.line_of_bus_code, :new.RESERVE_TYPE_CODE, :new.max_amount, :new.updated_by_user, :new.dttm_rcd_last_upd, :new.CLAIM_TYPE_CODE);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_SUPP_ASSOC_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SUPP_ASSOC
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO SUPP_ASSOC_AUDIT
            VALUES (:new.FIELD_ID, :new.ASSOC_CODE_ID, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO SUPP_ASSOC_AUDIT
            VALUES (:old.FIELD_ID, :old.ASSOC_CODE_ID, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO SUPP_ASSOC_AUDIT
            VALUES (:new.FIELD_ID, :new.ASSOC_CODE_ID, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
create or replace
TRIGGER TRIG_SUPP_DICTIONARY_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SUPP_DICTIONARY
   FOR EACH ROW

   BEGIN

      -- record new employee primary key
      IF INSERTING THEN
         INSERT INTO SUPP_DICTIONARY_AUDIT (field_id, seq_num, supp_table_name, sys_field_name, field_type, field_size, required_flag, delete_flag, lookup_flag, is_patterned, pattern, code_file_id, grp_assoc_flag, assoc_field, help_context_id, netvisible_flag, user_prompt, updated_by_user, dttm_rcd_last_upd)
            VALUES (:new.field_id, :new.seq_num, :new.supp_table_name, :new.sys_field_name, :new.field_type, :new.field_size, :new.required_flag, :new.delete_flag, :new.lookup_flag, :new.is_patterned, :new.pattern, :new.code_file_id, :new.grp_assoc_flag, :new.assoc_field, :new.help_context_id, :new.netvisible_flag, :new.user_prompt, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN
         INSERT INTO SUPP_DICTIONARY_AUDIT (field_id, seq_num, supp_table_name, sys_field_name, field_type, field_size, required_flag, delete_flag, lookup_flag, is_patterned, pattern, code_file_id, grp_assoc_flag, assoc_field, help_context_id, netvisible_flag, user_prompt, updated_by_user, dttm_rcd_last_upd)
            VALUES (:old.field_id, :old.seq_num, :old.supp_table_name, :old.sys_field_name, :old.field_type, :old.field_size, :old.required_flag, :old.delete_flag, :old.lookup_flag, :old.is_patterned, :old.pattern, :old.code_file_id, :old.grp_assoc_flag, :old.assoc_field, :old.help_context_id, :old.netvisible_flag, :old.user_prompt, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE
         INSERT INTO SUPP_DICTIONARY_AUDIT (field_id, seq_num, supp_table_name, sys_field_name, field_type, field_size, required_flag, delete_flag, lookup_flag, is_patterned, pattern, code_file_id, grp_assoc_flag, assoc_field, help_context_id, netvisible_flag, user_prompt, updated_by_user, dttm_rcd_last_upd)
            VALUES (:new.field_id, :new.seq_num, :new.supp_table_name, :new.sys_field_name, :new.field_type, :new.field_size, :new.required_flag, :new.delete_flag, :new.lookup_flag, :new.is_patterned, :new.pattern, :new.code_file_id, :new.grp_assoc_flag, :new.assoc_field, :new.help_context_id, :new.netvisible_flag, :new.user_prompt, :new.updated_by_user, :new.dttm_rcd_last_upd);
      END IF;
END;
/
GO



[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_SYS_EVENT_CAT_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON SYS_EVENT_CAT
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO SYS_EVENT_CAT_AUDIT
            VALUES (:new.EVENT_CAT_CODE, :new.DISPLAY_ID, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO SYS_EVENT_CAT_AUDIT
            VALUES (:old.EVENT_CAT_CODE, :old.DISPLAY_ID, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO SYS_EVENT_CAT_AUDIT
            VALUES (:new.EVENT_CAT_CODE, :new.DISPLAY_ID, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_USER_GROUPS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON USER_GROUPS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO USER_GROUPS_AUDIT
            VALUES (:new.GROUP_ID, :new.GROUP_NAME,:new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO USER_GROUPS_AUDIT
            VALUES (:old.GROUP_ID, :old.GROUP_NAME,:old.CRC, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO USER_GROUPS_AUDIT
            VALUES (:new.GROUP_ID, :new.GROUP_NAME,:new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_USER_MEMBERSHIP_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON USER_MEMBERSHIP
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO USER_MEMBERSHIP_AUDIT
            VALUES (:new.GROUP_ID, :new.USER_ID,:new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO USER_MEMBERSHIP_AUDIT
            VALUES (:old.GROUP_ID, :old.USER_ID,:old.CRC, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO USER_MEMBERSHIP_AUDIT
            VALUES (:new.GROUP_ID, :new.USER_ID,:new.CRC, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO

[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_RM_USER_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON RM_USER
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO RM_USER_AUDIT
            VALUES (:new.user_eid, :new.user_id, :new.rm_user_type_code, :new.doc_path, :new.notes, :new.security_code, :new.login_name, :new.deleted_flag, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO RM_USER_AUDIT
            VALUES (:old.user_eid, :old.user_id, :old.rm_user_type_code, :old.doc_path, :old.notes, :old.security_code, :old.login_name, :old.deleted_flag, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO RM_USER_AUDIT
            VALUES (:new.user_eid, :new.user_id, :new.rm_user_type_code, :new.doc_path, :new.notes, :new.security_code, :new.login_name, :new.deleted_flag, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO


[GO_DELIMITER]
-- Create Trigger
CREATE OR REPLACE TRIGGER TRIG_VBS_PROCS_AUDIT
   AFTER INSERT OR UPDATE OR DELETE ON VBS_PROCS
   FOR EACH ROW
   
   BEGIN
      
      -- record new employee primary key
      IF INSERTING THEN 
         INSERT INTO VBS_PROCS_AUDIT
            VALUES (:new.TYPE_ID, :new.SCRIPTPROCS, :new.updated_by_user, :new.dttm_rcd_last_upd);
      -- record primary key of the deleted row:
      ELSIF DELETING THEN                           
         INSERT INTO VBS_PROCS_AUDIT
            VALUES (:old.TYPE_ID, :old.SCRIPTPROCS, :old.updated_by_user, :old.dttm_rcd_last_upd);
      -- for updates, record the primary key
      -- of the row being updated:
      ELSE 
         INSERT INTO VBS_PROCS_AUDIT
            VALUES (:new.TYPE_ID, :new.SCRIPTPROCS, :new.updated_by_user, :new.dttm_rcd_last_upd);
        
      END IF;
END;
/
GO