[GO_DELIMITER]
create or replace procedure USP_COPY_EXISTING_COMMENTS 
AS 
BEGIN 
    DECLARE 
        EVENT_COMMENTID INTEGER;
        CT_CNT INTEGER;
        
        BEGIN
            SELECT count(*) INTO CT_CNT from comments_text;
            
            IF CT_CNT > 1 THEN
            	RETURN;
            END IF;
            
            BEGIN
                DECLARE
                    CURSOR EVENT_COMMENT_C IS SELECT EVENT_ID, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;

                BEGIN 
                    FOR EVENT_COMMENT_VAL IN EVENT_COMMENT_C 
                    LOOP 
                        SELECT NVL(MAX(COMMENT_ID), 1) INTO EVENT_COMMENTID FROM COMMENTS_TEXT WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
                        IF EVENT_COMMENTID != 0 THEN 
                               INSERT INTO COMMENTS_TEXT (COMMENT_ID, ATTACH_RECORDID, ATTACH_TABLE, COMMENTS, HTMLCOMMENTS) 
                                 VALUES (EVENT_COMMENTID + 1, EVENT_COMMENT_VAL.EVENT_ID, 'EVENT', EVENT_COMMENT_VAL.COMMENTS, EVENT_COMMENT_VAL.HTMLCOMMENTS);
                        END IF;
                    END LOOP;
                END;
                
                DECLARE 
                    CLAIM_COMMENTID INTEGER;
                    CURSOR CLAIM_COMMENT_C IS SELECT CLAIM_ID, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;

                BEGIN 
                    FOR CLAIM_COMMENT_VAL IN CLAIM_COMMENT_C 
                    LOOP 
                        SELECT NVL(MAX(COMMENT_ID), 0) INTO CLAIM_COMMENTID FROM COMMENTS_TEXT WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
                        IF CLAIM_COMMENTID != 0 THEN 
                            INSERT INTO COMMENTS_TEXT (COMMENT_ID, ATTACH_RECORDID, ATTACH_TABLE, COMMENTS, HTMLCOMMENTS) 
                                  VALUES (CLAIM_COMMENTID + 1, CLAIM_COMMENT_VAL.CLAIM_ID, 'CLAIM', CLAIM_COMMENT_VAL.COMMENTS, CLAIM_COMMENT_VAL.HTMLCOMMENTS);
                        END IF;
                    END LOOP;
                END;
                
                DECLARE 
                    MAXCOMMENT_ID INTEGER;
    
                BEGIN 
                    SELECT MAX(COMMENT_ID) INTO MAXCOMMENT_ID FROM COMMENTS_TEXT;
                    UPDATE GLOSSARY SET NEXT_UNIQUE_ID = (MAXCOMMENT_ID + 1) WHERE SYSTEM_TABLE_NAME = 'COMMENTS_TEXT';
                END;
                
                COMMIT;
            END;
            END;
END;
[EXECUTE_SP]
GO