[GO_DELIMITER]
create or replace
PROCEDURE USP_FINAL_INDICES(sRM_USERID VARCHAR2)
AUTHID CURRENT_USER IS
  BEGIN
    DECLARE
          V_COUNT NUMBER;          
          INDEX_Cursor varchar(2000);
          sSID varchar2(20);
          sSERIAL varchar(20);
          CursorFinalIndices SYS_REFCURSOR;
          MyIndexCursorInside SYS_REFCURSOR;
          sSqlCursor VARCHAR2(4000);
          sSQL varchar2(2000);
          iFLAG integer;
          iIS_CLUSTERED integer; 
          iIS_UNIQUE integer; 
          iIS_PRIMARY integer; 
          sCOLUMN_NAMES varchar2(250);
          iCount integer;
          sTABLE_NAME varchar2(250);
          sINDEX_NAME varchar2(250);
            IS_UNIQUEINDEX VARCHAR2(15);
   BEGIN   
             sSQL:='SELECT COUNT(*) FROM ALL_TABLES WHERE TABLE_NAME=''FINAL_INDICES''  AND OWNER='''||UPPER(sRM_USERID)||'''';             
             EXECUTE IMMEDIATE sSQL INTO iFLAG;
                
		IF iFLAG >0 THEN
			sSQL:='DROP TABLE '||sRM_USERID||'.FINAL_INDICES';
			EXECUTE IMMEDIATE sSQL;
		END IF;          
          
               sSQL:='CREATE TABLE '||sRM_USERID||'.FINAL_INDICES
                                                    (FINAL_ROW_ID INTEGER,
                                                    TABLE_NAME VARCHAR2(100) ,
                                                    INDEX_NAME VARCHAR2(255),
                                                    IS_CLUSTERED INTEGER,
                                                    IS_UNIQUE INTEGER,
                                                    IS_PRIMARY INTEGER,
                                                     COLUMN_NAMES VARCHAR2(255),
                                                     IS_UNIQUE_INDEX VARCHAR(10)
                                                    )';
              EXECUTE IMMEDIATE sSQL;
            
                
              sSQL:='SELECT COUNT(*) FROM ALL_SEQUENCES WHERE SEQUENCE_NAME=''FINAL_INDICES_SEQ''  AND SEQUENCE_OWNER='''||UPPER(sRM_USERID)||'''';
               EXECUTE IMMEDIATE sSQL INTO iFLAG;
               
                IF iFLAG >0 THEN
			sSQL:='DROP SEQUENCE '||sRM_USERID||'.FINAL_INDICES_SEQ';
                      EXECUTE IMMEDIATE sSQL;
		END IF; 
                
                sSQL:='SELECT MAX(OPTIMUM_ROW_ID) FROM '||sRM_USERID||'.OPTIMUM_INDICES';            
                EXECUTE IMMEDIATE sSQL INTO V_COUNT;
                COMMIT;
                
                V_COUNT:=V_COUNT+1;
                
               sSQL:='CREATE  SEQUENCE FINAL_INDICES_SEQ START WITH '||V_COUNT;
               EXECUTE IMMEDIATE sSQL;        
                
               sSQL:='INSERT INTO '||sRM_USERID||'.FINAL_INDICES (FINAL_ROW_ID,TABLE_NAME , INDEX_NAME ,
                              IS_CLUSTERED , IS_UNIQUE, IS_PRIMARY , COLUMN_NAMES) SELECT OPTIMUM_ROW_ID,TABLE_NAME , INDEX_NAME ,
                              IS_CLUSTERED , IS_UNIQUE, IS_PRIMARY , COLUMN_NAMES FROM '||sRM_USERID||'.OPTIMUM_INDICES';
                              
              EXECUTE IMMEDIATE sSQL; 
          
              sSqlCursor:='SELECT TABLE_NAME , INDEX_NAME ,  IS_CLUSTERED , IS_UNIQUE, IS_PRIMARY , COLUMN_NAMES,IS_UNIQUE_INDEX FROM '||sRM_USERID||'.EXISTING_INDICES_TEMP';  
                  BEGIN      --inner cursor start
                    OPEN CursorFinalIndices FOR sSqlCursor;
                        LOOP          
                          FETCH CursorFinalIndices INTO sTABLE_NAME , sINDEX_NAME ,iIS_CLUSTERED , iIS_UNIQUE, iIS_PRIMARY , sCOLUMN_NAMES,IS_UNIQUEINDEX;  
                           EXIT WHEN CursorFinalIndices%NOTFOUND;
                           
                               sSQL:='SELECT count(*) FROM '||sRM_USERID||'.FINAL_INDICES WHERE TABLE_NAME ='''||sTABLE_NAME||''' AND COLUMN_NAMES = '''||sCOLUMN_NAMES||'''';
                               EXECUTE IMMEDIATE sSQL INTO iCount;
                                  IF (iCount = 0) THEN  -- now copy existing into final but correct it also
                                    BEGIN    
                                    
                                          sSQL:='INSERT INTO '||sRM_USERID||'.FINAL_INDICES(FINAL_ROW_ID,TABLE_NAME , INDEX_NAME , IS_CLUSTERED , IS_UNIQUE, IS_PRIMARY , COLUMN_NAMES,IS_UNIQUE_INDEX) VALUES(FINAL_INDICES_SEQ.nextval,'''||sTABLE_NAME||''' ,'''||sINDEX_NAME||''' ,'||iIS_CLUSTERED||' ,'||iIS_UNIQUE||', '||iIS_PRIMARY||' , '''||sCOLUMN_NAMES||''','''||IS_UNIQUEINDEX||''')';  
                                         EXECUTE IMMEDIATE sSQL;
                                         COMMIT;
                                    END;
                                 END IF;   
                        END LOOP;
                  CLOSE CursorFinalIndices;      
                END;
       END;           
  END USP_FINAL_INDICES;

GO