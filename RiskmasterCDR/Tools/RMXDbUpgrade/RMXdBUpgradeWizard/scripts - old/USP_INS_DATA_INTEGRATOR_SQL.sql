[GO_DELIMITER]

DECLARE @spSQL AS nVARCHAR(max) 

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_INS_DATA_INTEGRATOR')
BEGIN
	DROP PROCEDURE USP_INS_DATA_INTEGRATOR
END
	SET @spSQL = 'CREATE PROCEDURE USP_INS_DATA_INTEGRATOR @spOPTIONSET_ID int,@user_id int,@module_name nVARCHAR(50),@optionset_name nVARCHAR(50),@xml_string nVARCHAR(8000)
				  AS				

				  	
				EXEC USP_INS_DATA_INTEGRATOR @spOPTIONSET_ID,@user_id,@module_name,@optionset_name,@xml_string

	'
	EXECUTE sp_executesql @spSQL 
GO