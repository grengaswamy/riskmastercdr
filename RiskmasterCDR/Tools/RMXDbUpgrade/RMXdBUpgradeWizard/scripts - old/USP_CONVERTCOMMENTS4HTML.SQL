create or replace
PROCEDURE USP_CONVERTCOMMENTS4HTML (table_name varchar2,source_col varchar2,dest_col varchar2,id_col varchar2)
AS
BEGIN
  DECLARE
      crlf VARCHAR2(50);
      lf VARCHAR(50);
      cr varchar(50);
      rep_str varchar2(800) ;
      record_id INTEGER;
      nstr clob;
      lv_clob clob;
      icount INTEGER;
      sSqlUpd varchar2(4000); 
      sSql clob;
      comments_cursor SYS_REFCURSOR; 
      sSqlCursor varchar2(4000);
      clob_length INTEGER;
      clob_offset INTEGER;
      clob_amount INTEGER;
      v_append VARCHAR2(32767) CHARACTER SET lv_clob%CHARSET;
    BEGIN
          
          lf := CHR(10);
          cr := CHR(13);
          crlf := cr ||lf;
          rep_str := '<br/>';
         
          icount := 0 ;
        -- copy COMMENTS to HTMLCOMMENTS if COMMENTS is not null and HTMLCOMMENTS is null
        -- Old version of RMWorld only save values to COMMENTS column           
             
        sSqlCursor := 'SELECT ' ||id_col ||' FROM '||table_name||' WHERE '||dest_col ||' IS NULL AND ' ||source_col ||' IS NOT NULL'; 
       
                BEGIN
                       OPEN comments_cursor FOR sSqlCursor; 
                  
                             LOOP
                                FETCH comments_cursor INTO record_id;
                                EXIT WHEN comments_cursor%NOTFOUND; 
                                icount := icount + 1;
                                    IF MOD(icount,5000) = 0 THEN
                                    dbms_output.PUT_LINE(icount||' records have been processed.');
                                    dbms_session.free_unused_user_memory;
                                    commit;
                                    
                                    END IF; 
                                    
                                --sSqlUpd := 'SELECT SUBSTR(' || source_col ||' , 1 , 32760) FROM '||table_name||' WHERE ' ||id_col ||'='||to_char(record_id);
                                sSqlUpd := 'SELECT ' || source_col ||' FROM '||table_name||' WHERE ' ||id_col ||'='||to_char(record_id);
                                EXECUTE IMMEDIATE sSqlUpd INTO nstr; 
                  
                                nstr:=REPLACE(nstr,crlf, rep_str); 
                                nstr:=REPLACE(nstr,cr,rep_str); 
                                nstr:=REPLACE(nstr,lf, rep_str); 
                                
                                sSqlUpd := 'UPDATE '||table_name|| ' SET '||dest_col||' = empty_clob()  WHERE ' ||id_col ||'='||to_char(record_id);                                
                                EXECUTE IMMEDIATE  sSqlUpd;
                                COMMIT; 
                              
                                --sSqlUpd := 'SELECT SUBSTR(' || dest_col ||' , 1 , 32760) FROM '||table_name||' WHERE ' ||id_col ||'='||to_char(record_id) || ' FOR UPDATE';
                                sSqlUpd := 'SELECT ' || dest_col ||' FROM '||table_name||' WHERE ' ||id_col ||'='||to_char(record_id) || ' FOR UPDATE';
                                
                                EXECUTE IMMEDIATE sSqlUpd INTO lv_clob;   
                                if (DBMS_LOB.GETLENGTH(nstr)< 32767) then
                                  dbms_lob.write(lv_clob,DBMS_LOB.GETLENGTH(nstr),1,to_char(nstr));
                                else
                                    begin
                                    clob_length := dbms_lob.getlength(nstr);
                                        clob_offset := 1;
                                        clob_amount := 32767;
                                        while clob_length > 32767
                                        loop
                                           clob_length := clob_length - 32767;
                                           v_append := dbms_lob.substr(nstr,clob_amount,clob_offset);
                                           dbms_lob.writeappend(lv_clob, length(v_append), v_append);
                                           clob_offset := clob_offset + 32767;
                                        end loop;
                                        clob_amount := dbms_lob.getlength(nstr) - clob_offset;
                                        v_append := dbms_lob.substr(nstr,clob_amount,clob_offset);
                                        dbms_lob.writeappend(lv_clob, length(v_append), v_append);
                                    
                                    end;
                                     end if ;           
                                 Commit; 
                  
                            END LOOP;
                       CLOSE comments_cursor;
                 END; 
                 /*
                   USP_CONVERTCOMMENTS4HTML ('COMMENTS_TEXT', 'COMMENTS', 'HTMLCOMMENTS', 'COMMENT_ID');                 
                                      
                   USP_CONVERTCOMMENTS4HTML ('CLAIM', 'COMMENTS', 'HTMLCOMMENTS', 'CLAIM_ID');                  
                                      
                   USP_CONVERTCOMMENTS4HTML ('CLAIM_X_LITIGATION', 'COMMENTS', 'HTMLCOMMENTS', 'LITIGATION_ROW_ID');                   
                                      
                   USP_CONVERTCOMMENTS4HTML ('CLAIMANT', 'COMMENTS', 'HTMLCOMMENTS', 'CLAIMANT_ROW_ID');                   
                                      
                   USP_CONVERTCOMMENTS4HTML ('DEFENDANT', 'COMMENTS', 'HTMLCOMMENTS', 'DEFENDANT_ROW_ID');                   
                                      
                   USP_CONVERTCOMMENTS4HTML ('DISABILITY_PLAN', 'COMMENTS', 'HTMLCOMMENTS', 'PLAN_ID');                   
                                      
                   USP_CONVERTCOMMENTS4HTML ('ENTITY', 'COMMENTS', 'HTMLCOMMENTS', 'ENTITY_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('EVENT', 'COMMENTS', 'HTMLCOMMENTS', 'EVENT_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('EVENT', 'EVENT_DESCRIPTION', 'EVENT_DESCRIPTION_HTMLCOMMENTS', 'EVENT_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('EVENT_X_DATED_TEXT', 'DATED_TEXT', 'DATED_TEXT_HTMLCOMMENTS', 'EV_DT_ROW_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('FUNDS', 'COMMENTS', 'HTMLCOMMENTS', 'TRANS_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('PERSON_INVOLVED', 'COMMENTS', 'HTMLCOMMENTS', 'PI_ROW_ID');                   
                                     
                   USP_CONVERTCOMMENTS4HTML ('POLICY', 'COMMENTS', 'HTMLCOMMENTS', 'POLICY_ID');                      
                  

*/          
           END; 
END USP_CONVERTCOMMENTS4HTML;
