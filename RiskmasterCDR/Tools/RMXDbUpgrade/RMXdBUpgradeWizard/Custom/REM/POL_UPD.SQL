create or replace
TRIGGER POLICY_UPD
BEFORE UPDATE OF BROKER_EID ON POLICY
FOR EACH ROW
BEGIN
   IF :old.broker_eid != :new.broker_eid THEN 

              UPDATE CLAIM
                SET SEC_BROKER_EID = :new.broker_eid
                WHERE PRIMARY_POLICY_ID = :new.policy_id;           
           
    END IF;            
END;
