; --This SQL file does NOT include upgrade a legacy Session DB.
; --This is because a second Session Db is Constructed for RMX and used in parrallel with the original. 


; --BSB This section is the Beta Upgrade Statements required for the 12/13/2005 Beta Release (Build 776) 
; -- (Supports Async Calling pattern created by Tanuj.)
[SQL Server] [FORGIVE_CREATETABLE] CREATE TABLE ASYNC_TRACKER(AsyncId varchar(50) NOT NULL,AsyncStatus INTEGER,AsyncState IMAGE ,PercentageCompletion float )
[SQL Server] [FORGIVE] ALTER TABLE ASYNC_TRACKER ADD CONSTRAINT PK_ASYNC_TRACKER PRIMARY KEY(AsyncId)
[Oracle] [FORGIVE_CREATETABLE]	CREATE TABLE ASYNC_TRACKER(AsyncId VARCHAR2(50) NOT NULL,AsyncStatus NUMBER(10) NULL,AsyncState BLOB NULL,PercentageCompletion NUMBER NULL)
[DB2] [FORGIVE_CREATETABLE]	CREATE TABLE ASYNC_TRACKER(AsyncId varchar(50) NOT NULL,AsyncStatus INTEGER NULL,AsyncState BLOB(10M) NULL,PercentageCompletion float NULL)
[Oracle] [FORGIVE]CREATE UNIQUE INDEX PK_ASYNC_TRACKER ON ASYNC_TRACKER(AsyncId)
[DB2] [FORGIVE] CREATE UNIQUE INDEX PK_ASYNC_TRACKER ON ASYNC_TRACKER(AsyncId)
;--BSB Instructions below this point are for reference only.

; --Populate default customization table Entries
; /*BSB Not Required for Beta Clients but this needs to be addressed for "General Availability" Migrations
;[FORGIVE] INSERT INTO CUSTOMIZE(ID,FOLDER,FILENAME,TYPE,IS_BINARY,CONTENT) VALUES(-1,null,'customize_captions',0,0,'<RMAdminSettings title="System Customization"><Captions title="Captions/Messages"><CompanyName default="***" title="Company Name:">Computer Sciences Corporation</CompanyName><AppTitle default="Riskmaster.Net" title="Application Title:">Riskmaster.Net</AppTitle><ReportAppTitle default="Sortmaster" title="Application Title:">Sortmaster</ReportAppTitle><AppCopyright default=") 2004 by CSC, All Rights Reserved." title="Application Title:">) 2004 by CSC, All Rights Reserved.</AppCopyright><ErrContact default="For assistance please consult the System Administrator." title="Contact on Error:">For assistance please consult the System Administrator.</ErrContact><AlertExistingRec default="You are working on a new record and this functionality is available for existing records only. Please save the data and try again." title="Alert Existing Record Required:">You are working on a new record and this functionality is available for existing records only. Please save the data and try again.</AlertExistingRec></Captions><Paths title="Paths"><OverrideDocPath default="***" title="Override Documents Path:" /></Paths></RMAdminSettings>')
;[FORGIVE] UPDATE CUSTOMIZE SET ID=(SELECT MAX(ID) FROM CUSTOMIZE)+1 WHERE FILENAME='customize_captions' AND ID=-1
;[FORGIVE] INSERT INTO CUSTOMIZE(ID,FOLDER,FILENAME,TYPE,IS_BINARY,CONTENT) VALUES(-2,null,'customize_settings',0,0,'<RMAdminSettings title="System Customization"><TextAreaSize title="Text Area Size"><TextML title="TextMl"><Width default="30">30</Width><Height default="5">5</Height></TextML><FreeCode title="Freecode"><Width default="30">30</Width><Height default="8">8</Height></FreeCode><ReadOnlyMemo title="Readonly Memo"><Width default="30">30</Width><Height default="5">5</Height></ReadOnlyMemo><Memo title="Memo"><Width default="30">30</Width><Height default="5">5</Height></Memo></TextAreaSize><Buttons title="Buttons"><Document title="Documents">-1</Document><Search title="Search">-1</Search><File title="Files">-1</File><Diary title="Diaries">-1</Diary><Report title="Reports">-1</Report></Buttons><Other><Soundex title="Choose Soundex on Searches">0</Soundex><ShowName title="Show User Name on Menu">-1</ShowName><ShowLogin title="Show Login on Menu">-1</ShowLogin><SaveShowActiveDiary title="Save Show Active Diary">-1</SaveShowActiveDiary></Other><Funds title="Funds Page Settings"><OFAC title="OFAC Check:" /></Funds></RMAdminSettings>')
;[FORGIVE] UPDATE CUSTOMIZE SET ID=(SELECT MAX(ID) FROM CUSTOMIZE)+1 WHERE FILENAME='customize_settings' AND ID=-2
;[FORGIVE] INSERT INTO CUSTOMIZE(ID,FOLDER,FILENAME,TYPE,IS_BINARY,CONTENT) VALUES(-3,null,'customize_reports',0,0,'<RMAdminSettings title="System Customization"><ReportEmail title="Report Email Settings"><From title="Override From:"></From><FromAddr title="Override From Address:"></FromAddr></ReportEmail><ReportMenu title="Report Menu Links"><ReportLabel title="Reports" default="Reports" value="-1"><AvlReports title="Available Reports:">-1</AvlReports><JobQueue title="Job Queue:">-1</JobQueue><NewReport title="Post New Report:">-1</NewReport><DeleteReport title="Delete Report:">-1</DeleteReport><ScheduleReport title="Schedule Reports:">-1</ScheduleReport><ViewSchedReport title="View Scheduled Reports:">-1</ViewSchedReport></ReportLabel><SMLabel title="SM.Net" default="SM.Net" value="-1"><Designer title="Designer:">-1</Designer><DraftReport title="Draft Reports:">-1</DraftReport><PostDraftRpt title="Post Draft Reports:">-1</PostDraftRpt></SMLabel><ExecSummLabel title="Exec. Summary" default="Exec. Summary" value="-1"><Configuration title="Configuration:">-1</Configuration><Claim title="Claim:">-1</Claim><Event title="Event:">-1</Event></ExecSummLabel><OtherLabel title="Other Reports" default="Other Reports" value="-1"><OSHA200 title="OSHA 200:">-1</OSHA200></OtherLabel><DCCLabel title="DCC" default="DCC">-1</DCCLabel><ReportQueue title="Report Queue Buttons"><Archive title="OSHA 200:">-1</Archive><Email title="OSHA 200:">-1</Email><Delete title="OSHA 200:">-1</Delete></ReportQueue></ReportMenu></RMAdminSettings>')
;[FORGIVE] UPDATE CUSTOMIZE SET ID=(SELECT MAX(ID) FROM CUSTOMIZE)+1 WHERE FILENAME='customize_reports' AND ID=-3
;[FORGIVE] INSERT INTO CUSTOMIZE(ID,FOLDER,FILENAME,TYPE,IS_BINARY,CONTENT) VALUES(-4,null,'customize_search',0,0,'<RMAdminSettings title="System Customization"><Search title="Search Links"><Claim title="Claims">-1</Claim><Event title="Events">-1</Event><Employee title="Employees">-1</Employee><Entity title="Entities">-1</Entity><Vehicle title="Vehicles">-1</Vehicle><Policy title="Policies">-1</Policy><Fund title="Funds">-1</Fund><Patient title="Patients">-1</Patient><Physician title="Physicians">-1</Physician></Search></RMAdminSettings>')
;[FORGIVE] UPDATE CUSTOMIZE SET ID=(SELECT MAX(ID) FROM CUSTOMIZE)+1 WHERE FILENAME='customize_search' AND ID=-4
;[FORGIVE] INSERT INTO CUSTOMIZE(ID,FOLDER,FILENAME,TYPE,IS_BINARY,CONTENT) VALUES(-5,null,'customize_images',0,0,'<RMAdminSettings title="System Customization"><ImageTop title="Images for Top Frame" imagepath="" imageurl=""><Top1><current name="Top1.gif"></current><default name="Top1.gif"></default></Top1><Top2><current name="Top2.gif"></current><default name="Top2.gif"></default></Top2><Top3><current name="Top3.gif"></current><default name="Top3.gif"></default></Top3></ImageTop><ImageTopAdmin title="Images for Top Admin Frame" imagepath="" imageurl=""><Top1><current name="Top1Admin.gif"></current><default name="Top1Admin.gif"></default></Top1><Top2><current name="Top2Admin.gif"></current><default name="Top2Admin.gif"></default></Top2><Top3><current name="Top3Admin.gif"></current><default name="Top3Admin.gif"></default></Top3></ImageTopAdmin></RMAdminSettings>')
;[FORGIVE] UPDATE CUSTOMIZE SET ID=(SELECT MAX(ID) FROM CUSTOMIZE)+1 WHERE FILENAME='customize_images' AND ID=-5
;*/

; /*-- BSB The following is SQL Server Specific Script sufficient to re-create the 
; --Expire WebFarmSession Job used to control kicking out idle sessions.
; --Script generated on 1/4/2001 3:58 PM

;BEGIN TRANSACTION            
;  DECLARE @JobID BINARY(16)  
;  DECLARE @ReturnCode INT    
;  SELECT @ReturnCode = 0     
;IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'[Uncategorized (Local)]') < 1 
;  EXECUTE msdb.dbo.sp_add_category @name = N'[Uncategorized (Local)]'

;  -- Delete the job with the same name (if it exists)
;  SELECT @JobID = job_id     
;  FROM   msdb.dbo.sysjobs    
;  WHERE (name = N'Expire Web Session')       
;  IF (@JobID IS NOT NULL)    
;  BEGIN  
;  -- Check if the job is a multi-server job  
;  IF (EXISTS (SELECT  * 
;              FROM    msdb.dbo.sysjobservers 
;             WHERE   (job_id = @JobID) AND (server_id <> 0))) 
;  BEGIN 
;    -- There is, so abort the script 
;    RAISERROR (N'Unable to import job ''Expire Web Session'' since there is already a multi-server job with this name.', 16, 1) 
;    GOTO QuitWithRollback  
;  END 
;  ELSE 
;    -- Delete the [local] job 
;    EXECUTE msdb.dbo.sp_delete_job @job_name = N'Expire Web Session' 
;    SELECT @JobID = NULL
;  END 

;BEGIN 

;  -- Add the job
;  EXECUTE @ReturnCode = msdb.dbo.sp_add_job @job_id = @JobID OUTPUT , @job_name = N'Expire Web Session', @owner_login_name = N'sa', @description = N'No description available.', @category_name = N'[Uncategorized (Local)]', @enabled = 1, @notify_level_email = 0, @notify_level_page = 0, @notify_level_netsend = 0, @notify_level_eventlog = 2, @delete_level= 0
;  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

;  -- Add the job steps
;  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Clean Session Older that 1 hour', @command = N'DECLARE @string_today varchar(14)
;DECLARE @stmp varchar(12)
;SET @stmp=LTRIM(STR(DATEPART(yyyy, GETDATE())))
;SET @string_today=@stmp

;SET @stmp=LTRIM(STR(DATEPART(mm, GETDATE())))
;IF LEN(@stmp)=1
;   SET @stmp=''0''+@stmp
;SET @string_today=@string_today+@stmp

;SET @stmp=LTRIM(STR(DATEPART(dd, GETDATE())))
;IF LEN(@stmp)=1
;   SET @stmp=''0''+@stmp
;SET @string_today=@string_today+@stmp

;SET @stmp=LTRIM(STR(DATEPART(hh, GETDATE())))
;IF LEN(@stmp)=1
;   SET @stmp=''0''+@stmp
;SET @string_today=@string_today+@stmp

;SET @stmp=LTRIM(STR(DATEPART(mi, GETDATE())))
;IF LEN(@stmp)=1
;   SET @stmp=''0''+@stmp
;SET @string_today=@string_today+@stmp



;SET @stmp=LTRIM(STR(DATEPART(ss, GETDATE())))
;IF LEN(@stmp)=1
;   SET @stmp=''0''+@stmp
;SET @string_today=@string_today+@stmp

;DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID IN (SELECT SESSION_ROW_ID FROM SESSIONS WHERE CAST(@string_today AS NUMERIC)  - CAST(LASTCHANGE AS NUMERIC) >= 360 OR DATA IS NULL)
;DELETE FROM SESSIONS WHERE CAST(@string_today AS NUMERIC)  - CAST(LASTCHANGE AS NUMERIC) >= 360 OR DATA IS NULL', @database_name = N'WebFarmSession', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'', @on_success_step_id = 0, @on_success_action = 1, @on_fail_step_id = 0, @on_fail_action = 2
;  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
;  EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 

;  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

;  -- Add the job schedules
;  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id = @JobID, @name = N'Expire Web Session', @enabled = 1, @freq_type = 4, @active_start_date = 20000427, @active_start_time = 0, @freq_interval = 1, @freq_subday_type = 4, @freq_subday_interval = 10, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_end_date = 99991231, @active_end_time = 235959
;  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

; -- Add the Target Servers
;  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
;  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

;END
;COMMIT TRANSACTION          
;GOTO   EndSave              
;QuitWithRollback:
;  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
;EndSave: */
; --Naresh Enhanced Policy Search
[FORGIVE] UPDATE CUSTOMIZE SET CONTENT = '<customize_search><Instance><meta><screen-id>/riskmaster/RMUtilities/CustomizeSearches</screen-id><user-id>a</user-id><session-id /><TestKey>a</TestKey></meta><ui><action /><LinkKey /><sort-column /><sort-order /><sort-data-type /><show-errors>false</show-errors><errors /></ui><Document><RMAdminSettings title="System Customization"><Search title="Search Links"><Claim title="Claims">-1</Claim><Event title="Events">-1</Event><Employee title="Employees">-1</Employee><Entity title="Entities">-1</Entity><Vehicle title="Vehicles">-1</Vehicle><Policy title="Policies">-1</Policy><Fund title="Funds">-1</Fund><Patient title="Patients">-1</Patient><Physician title="Physicians">-1</Physician><EnhPolicy title="EnhPolicies">-1</EnhPolicy><LeavePlan title="LeavePlans">-1</LeavePlan></Search></RMAdminSettings></Document></Instance></customize_search>' WHERE ID = 6