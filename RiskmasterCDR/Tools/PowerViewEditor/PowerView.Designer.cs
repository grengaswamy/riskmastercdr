﻿namespace PowerViewEditor
{
    partial class PowerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.rdbASPX = new System.Windows.Forms.RadioButton();
            this.rdbXML = new System.Windows.Forms.RadioButton();
            this.txtView = new System.Windows.Forms.TextBox();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cboFormNames = new System.Windows.Forms.ComboBox();
            this.cmdExit = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.cboViewNames = new System.Windows.Forms.ComboBox();
            this.cboDatabases = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.CheckBox1 = new System.Windows.Forms.CheckBox();
            this.btnFindNext = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdbASPX
            // 
            this.rdbASPX.AutoSize = true;
            this.rdbASPX.Checked = true;
            this.rdbASPX.Location = new System.Drawing.Point(727, 17);
            this.rdbASPX.Name = "rdbASPX";
            this.rdbASPX.Size = new System.Drawing.Size(53, 17);
            this.rdbASPX.TabIndex = 20;
            this.rdbASPX.TabStop = true;
            this.rdbASPX.Text = "ASPX";
            this.rdbASPX.UseVisualStyleBackColor = true;
            this.rdbASPX.CheckedChanged += new System.EventHandler(this.rdbASPX_CheckedChanged);
            // 
            // rdbXML
            // 
            this.rdbXML.AutoSize = true;
            this.rdbXML.Location = new System.Drawing.Point(638, 17);
            this.rdbXML.Name = "rdbXML";
            this.rdbXML.Size = new System.Drawing.Size(47, 17);
            this.rdbXML.TabIndex = 19;
            this.rdbXML.Text = "XML";
            this.rdbXML.UseVisualStyleBackColor = true;
            this.rdbXML.CheckedChanged += new System.EventHandler(this.rdbXML_CheckedChanged);
            // 
            // txtView
            // 
            this.txtView.AcceptsReturn = true;
            this.txtView.AcceptsTab = true;
            this.txtView.HideSelection = false;
            this.txtView.Location = new System.Drawing.Point(29, 87);
            this.txtView.MaxLength = 2147483647;
            this.txtView.Multiline = true;
            this.txtView.Name = "txtView";
            this.txtView.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtView.Size = new System.Drawing.Size(841, 361);
            this.txtView.TabIndex = 17;
            this.txtView.WordWrap = false;
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.BackColor = System.Drawing.SystemColors.Control;
            this.cmdUpdate.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdUpdate.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdUpdate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdUpdate.Location = new System.Drawing.Point(157, 472);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdUpdate.Size = new System.Drawing.Size(80, 25);
            this.cmdUpdate.TabIndex = 16;
            this.cmdUpdate.Text = "Update";
            this.cmdUpdate.UseVisualStyleBackColor = false;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cboFormNames
            // 
            this.cboFormNames.BackColor = System.Drawing.SystemColors.Window;
            this.cboFormNames.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboFormNames.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFormNames.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cboFormNames.Location = new System.Drawing.Point(551, 43);
            this.cboFormNames.Name = "cboFormNames";
            this.cboFormNames.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboFormNames.Size = new System.Drawing.Size(319, 22);
            this.cboFormNames.TabIndex = 15;
            this.cboFormNames.SelectedIndexChanged += new System.EventHandler(this.cboFormNames_SelectedIndexChanged);
            // 
            // cmdExit
            // 
            this.cmdExit.BackColor = System.Drawing.SystemColors.Control;
            this.cmdExit.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdExit.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdExit.Location = new System.Drawing.Point(282, 472);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdExit.Size = new System.Drawing.Size(80, 25);
            this.cmdExit.TabIndex = 11;
            this.cmdExit.Text = "Exit";
            this.cmdExit.UseVisualStyleBackColor = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.SystemColors.Control;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(470, 43);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(87, 25);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "Form Name:";
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(20, 46);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(68, 25);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "View Name:";
            // 
            // cboViewNames
            // 
            this.cboViewNames.BackColor = System.Drawing.SystemColors.Window;
            this.cboViewNames.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboViewNames.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboViewNames.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cboViewNames.Location = new System.Drawing.Point(86, 43);
            this.cboViewNames.Name = "cboViewNames";
            this.cboViewNames.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboViewNames.Size = new System.Drawing.Size(322, 22);
            this.cboViewNames.TabIndex = 14;
            this.cboViewNames.SelectedIndexChanged += new System.EventHandler(this.cboViewNames_SelectedIndexChanged);
            // 
            // cboDatabases
            // 
            this.cboDatabases.BackColor = System.Drawing.SystemColors.Window;
            this.cboDatabases.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboDatabases.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDatabases.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cboDatabases.Location = new System.Drawing.Point(86, 12);
            this.cboDatabases.Name = "cboDatabases";
            this.cboDatabases.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboDatabases.Size = new System.Drawing.Size(322, 22);
            this.cboDatabases.TabIndex = 22;
            this.cboDatabases.SelectedIndexChanged += new System.EventHandler(this.cboDatabases_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(20, 15);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(68, 25);
            this.label3.TabIndex = 21;
            this.label3.Text = "Database:";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(29, 472);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(92, 23);
            this.btnSelectAll.TabIndex = 23;
            this.btnSelectAll.Text = "Copy All Text";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.CheckBox1);
            this.GroupBox1.Controls.Add(this.btnFindNext);
            this.GroupBox1.Controls.Add(this.btnFind);
            this.GroupBox1.Controls.Add(this.TextBox2);
            this.GroupBox1.Location = new System.Drawing.Point(408, 459);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(224, 136);
            this.GroupBox1.TabIndex = 25;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Find Text";
            // 
            // CheckBox1
            // 
            this.CheckBox1.Location = new System.Drawing.Point(24, 104);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Size = new System.Drawing.Size(88, 16);
            this.CheckBox1.TabIndex = 4;
            this.CheckBox1.Text = "Word Wrap";
            this.CheckBox1.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // btnFindNext
            // 
            this.btnFindNext.Location = new System.Drawing.Point(88, 56);
            this.btnFindNext.Name = "btnFindNext";
            this.btnFindNext.Size = new System.Drawing.Size(56, 24);
            this.btnFindNext.TabIndex = 2;
            this.btnFindNext.Text = "Next";
            this.btnFindNext.Click += new System.EventHandler(this.btnFindNext_Click);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(24, 56);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(56, 24);
            this.btnFind.TabIndex = 1;
            this.btnFind.Text = "First";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(24, 32);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(184, 20);
            this.TextBox2.TabIndex = 0;
            // 
            // PowerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 607);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.cboDatabases);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rdbASPX);
            this.Controls.Add(this.rdbXML);
            this.Controls.Add(this.txtView);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.cboFormNames);
            this.Controls.Add(this.cboViewNames);
            this.Controls.Add(this.cmdExit);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "PowerView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Powerview Editor";
            this.Load += new System.EventHandler(this.PowerView_Load);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.RadioButton rdbASPX;
        internal System.Windows.Forms.RadioButton rdbXML;
        internal System.Windows.Forms.TextBox txtView;
        public System.Windows.Forms.Button cmdUpdate;
        public System.Windows.Forms.ComboBox cboFormNames;
        public System.Windows.Forms.Button cmdExit;
        public System.Windows.Forms.Label Label2;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.ComboBox cboViewNames;
        public System.Windows.Forms.ComboBox cboDatabases;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectAll;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.CheckBox CheckBox1;
        internal System.Windows.Forms.Button btnFindNext;
        internal System.Windows.Forms.Button btnFind;
        internal System.Windows.Forms.TextBox TextBox2;
    }
}

