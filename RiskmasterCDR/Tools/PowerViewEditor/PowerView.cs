﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using DevComponents.DotNetBar;
using System.Configuration;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;


namespace PowerViewEditor
{
    public partial class PowerView : Office2007Form
    {
        private string m_VPPConnectionString = string.Empty;
        private string m_FieldName = string.Empty;
        private const string g_RMXBaseViews = "RMX Base Views";
        public static Dictionary<int, CommonFunctions.MultiClientConnection> dictMultiTenant;
        public PowerView(int p_iClientId)//rkaur27
        {
            InitializeComponent();
      
                Globalvar.m_viewConnection = RMConfigurationManager.GetConfigConnectionString("ViewDataSource", p_iClientId);
                Globalvar.sSecurityConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId);

        }
        internal static class Globalvar
        {
            public static string m_viewConnection  { get; set; }
            public static string sSecurityConnectionString { get; set; }

        }
        private void PowerView_Load(object sender, EventArgs e)
        {
            try
            {
                //verify if the connection string exist and correct
                if (Globalvar.m_viewConnection == null)
                {
                    MessageBox.Show("Please add Powerview database connection string in the config file's connectionStrings section first.");
                    Application.Exit();
                }
               // string sConnectionString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
                DbConnection oDbConnection = DbFactory.GetDbConnection(Globalvar.m_viewConnection);
                try
                {
                    oDbConnection.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please check the connection string value in the config file." + System.Environment.NewLine + " The following error is returned: " + ex.Message);
                    Application.Exit();
                }
                finally
                {
                    if( oDbConnection != null )
                        oDbConnection.Close();
                }

                //Verify if the Riskmaster.config file path is correct.
                //if (ConfigurationManager.AppSettings["RiskmasterConfigPath"] == null)
                //{
                //    MessageBox.Show("Please add RiskmasterConfigPath in the config file's AppSettings section first.");
                //    Application.Exit();
                //}
                //string sPath = ConfigurationManager.AppSettings["RiskmasterConfigPath"];
                //if (!File.Exists(sPath))
                //{
                //    MessageBox.Show("Riskmaster config file: " + sPath + " does not exist.");
                //    Application.Exit();
                //}

                m_VPPConnectionString = Globalvar.m_viewConnection;
                PopulateDatabaseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboDatabases_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboViewNames.Items.Clear();
            cboDatabases.Text = string.Empty;

            cboFormNames.Items.Clear();
            cboFormNames.Text = string.Empty;
            PopulateViewList();
        }

        private void cboViewNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateFormList();
        }

        private void cboFormNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            RetrieveContent();
        }

        private void rdbXML_CheckedChanged(object sender, EventArgs e)
        {
            m_FieldName = "VIEW_XML";
            ContentTypeChanged();
        }

        private void rdbASPX_CheckedChanged(object sender, EventArgs e)
        {
            m_FieldName = "PAGE_CONTENT";
            ContentTypeChanged();
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            UpdateContent();
        }

        private void cmdExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PopulateDatabaseList()
        {
            string sSecurityConnectionString = Globalvar.sSecurityConnectionString ;
            string sSQL = "SELECT DSN, DSNID FROM DATA_SOURCE_TABLE ORDER BY DSN";
            string sDsnId = string.Empty;
            string sDsnName = string.Empty;
            using (DbReader oDbReader = DbFactory.GetDbReader(sSecurityConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    sDsnId = oDbReader["DSNID"].ToString();
                    sDsnName = oDbReader["DSN"].ToString();
                    cboDatabases.Items.Add(new KeyValuePair<string, string>(sDsnId, sDsnName));
                }
            }
        }


        private void PopulateViewList()
        {
            string sSQL = string.Empty;
            m_FieldName = "PAGE_CONTENT";

            //Retrieve all the view names

            //rsolanki2 : updating the query to retrive only the view name of the selected Datasource
            sSQL = "SELECT VIEW_ID, VIEW_NAME FROM NET_VIEWS WHERE DATA_SOURCE_ID=" +
                    ((KeyValuePair<string, string>)cboDatabases.SelectedItem).Key;

            //sSQL = "SELECT DISTINCT NET_VIEWS.VIEW_ID, NET_VIEWS.VIEW_NAME FROM NET_VIEWS INNER JOIN NET_VIEW_FORMS ON " +
            //        " NET_VIEWS.VIEW_ID = NET_VIEW_FORMS.VIEW_ID AND NET_VIEWS.DATA_SOURCE_ID=" + 
            //        ((KeyValuePair<string,string>)cboDatabases.SelectedItem).Key;
            using( DbReader oDbReader = DbFactory.GetDbReader(m_VPPConnectionString, sSQL))
            {
                string sViewName = string.Empty;
                string sViewID = string.Empty;
                while( oDbReader.Read() )
                {
                    sViewID = oDbReader["VIEW_ID"].ToString();
                    sViewName = oDbReader["VIEW_NAME"].ToString();
                    cboViewNames.Items.Add(new KeyValuePair<string, string>(sViewID, sViewName));
                }
            }

            //Add Base Views here
            cboViewNames.Items.Add(new KeyValuePair<string,string>("0", g_RMXBaseViews));

            if (cboViewNames.Items.Count == 1)
            {
                cboViewNames.SelectedIndex = 0;
            }

        }

        private void RetrieveContent()
        {
            StringBuilder sbSQL = new StringBuilder();
            string sViewXML = string.Empty;

            string sFormName = cboFormNames.Text;

            try
            {
                //Retrieve the appropriate SQL statement for viewing PowerViews
                sbSQL.Append("SELECT ");
                if (rdbASPX.Checked)
                    sbSQL.Append(" PAGE_CONTENT ");
                else
                    sbSQL.Append(" VIEW_XML ");

                sbSQL.Append(" FROM NET_VIEW_FORMS WHERE VIEW_ID=" + ((KeyValuePair<string, string>)cboViewNames.SelectedItem).Key);
                sbSQL.Append(" AND DATA_SOURCE_ID=" + ((KeyValuePair<string, string>)cboDatabases.SelectedItem).Key);
                if (rdbASPX.Checked)
                    sbSQL.Append(" AND PAGE_NAME='" + sFormName + "'");
                else
                    sbSQL.Append(" AND FORM_NAME='" + sFormName + "'");

                using (DbReader oDbReader = DbFactory.GetDbReader(m_VPPConnectionString, sbSQL.ToString()))
                {
                    if (oDbReader.Read())
                    {
                        if (rdbASPX.Checked)
                            txtView.Text = FormatAspxText(oDbReader["PAGE_CONTENT"].ToString());
                        else
                            txtView.Text = FormatXmlText(oDbReader["VIEW_XML"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        // <summary>
        // Retrieves the SQL for Power Views
        // </summary>
        // <param name="strViewName"></param>
        // <param name="strFormName"></param>
        // <returns></returns>
        // <remarks></remarks>
        private string GetPowerViewSQL(string strViewName, string strFormName)
        {
            string strSQL = string.Empty;
            string sColumnName = string.Empty;

            if (rdbASPX.Checked)
                sColumnName = "PAGE_NAME";
            else
                sColumnName = "FORM_NAME";

            switch (strViewName)
            {
                case g_RMXBaseViews:
                    strSQL = "SELECT NET_VIEW_FORMS.VIEW_XML, NET_VIEW_FORMS.PAGE_CONTENT FROM  NET_VIEW_FORMS WHERE (VIEW_ID = 0) AND (NET_VIEW_FORMS." + sColumnName + " = '" + strFormName + "')";
                    break;
                default:
                    strSQL = "SELECT NET_VIEW_FORMS.VIEW_XML, NET_VIEW_FORMS.PAGE_CONTENT FROM  NET_VIEW_FORMS INNER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID = NET_VIEWS.VIEW_ID WHERE (NET_VIEWS.VIEW_NAME = '" + strViewName + "') AND (NET_VIEW_FORMS." + sColumnName + " = '" + strFormName + "')";
                    break;
            }
            return strSQL;
        }

        private void PopulateFormList()
        {
            string sSQL = string.Empty;
            string sViewName = string.Empty;
            string vFormName = string.Empty;

            txtView.Text = string.Empty;
            cboFormNames.Items.Clear();
            cboFormNames.Text = string.Empty;
            sViewName = cboViewNames.Text.Trim();
            if( sViewName == string.Empty)
                return;

            sSQL = "SELECT NET_VIEW_FORMS.FORM_NAME, NET_VIEW_FORMS.PAGE_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = " +
                    ((KeyValuePair<string,string>)cboViewNames.SelectedItem).Key +
                    " AND DATA_SOURCE_ID=" + ((KeyValuePair<string,string>)cboDatabases.SelectedItem).Key + " ORDER BY FORM_NAME";

            string sFormName = string.Empty;
            using (DbReader oDbReader = DbFactory.GetDbReader(m_VPPConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    if (rdbASPX.Checked)
                        sFormName = oDbReader["PAGE_NAME"].ToString();
                    else
                        sFormName = oDbReader["FORM_NAME"].ToString();

                    if (!string.IsNullOrEmpty(sFormName))
                        cboFormNames.Items.Add(sFormName);
                }
            }
        }

        private void ContentTypeChanged()
        {
            txtView.Text = String.Empty;

            //Change the text for each item in cboForms
            PopulateFormList();
        }


        /// <summary>
        /// Format aspx page
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        private string FormatAspxText(string sInput)
        {
            StringBuilder sbOutput = null;
            int index = 0;
            string sNonXmlHeader;
            string sHtml;
            string[] arrNonXmlHeader;

            if(string.IsNullOrEmpty(sInput.Trim()))
                return sInput;

            //The lines before <html is not well-formatted xml
            index = sInput.IndexOf("<html ");
            sNonXmlHeader = sInput.Substring(0, index);
            arrNonXmlHeader = sNonXmlHeader.Split('<');

            //replace &nbsp and & because they are not part of a well-formatted xml
            sHtml = sInput.Substring(index);
            sHtml = sHtml.Replace("&nbsp;", "<nbsp/>");
            sHtml = sHtml.Replace("&", "&amp;");

            sHtml = FormatXmlText(sHtml);

            //remove the xml declaration if exists
            index = sHtml.IndexOf("<html ");
            if (index > 0)
            {
                sHtml = sHtml.Substring(index);
            }

            sHtml = sHtml.Replace("<nbsp/>", "&nbsp;");
            sHtml = sHtml.Replace("&amp;", "&");

            sbOutput = new StringBuilder();
            for( index = 1; index < arrNonXmlHeader.Length; index++)
            {
                sbOutput.Append("<" + arrNonXmlHeader[index] + Environment.NewLine);
            }
            sbOutput.Append(sHtml);

            return sbOutput.ToString();
        }


        /// <summary>
        /// format xml
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        private string FormatXmlText(string sInput)
        {
            XmlDocument oHtmlDoc;
            XmlTextWriter xmlWriter;
            StringWriter textWriter;

            if (string.IsNullOrEmpty(sInput.Trim()))
                return sInput;

            textWriter = new StringWriter();
            xmlWriter = new XmlTextWriter(textWriter);
            xmlWriter.Formatting = Formatting.Indented;
            oHtmlDoc = new XmlDocument();
            oHtmlDoc.LoadXml(sInput);
            oHtmlDoc.Save(xmlWriter);

            //remove encoding
            string sFormatedXml = textWriter.ToString();
            Regex regEncoding = new Regex(@"<\?xml\s+version=[\w\W]+encoding=[\w\W]+\?>");
            if (regEncoding.IsMatch(sFormatedXml))
            {
                sFormatedXml = regEncoding.Replace(sFormatedXml, "<?xml version=\"1.0\" ?>");
            }

            return sFormatedXml;
        }


    /// <summary>
    /// Handles the updating of the PowerView XML to be stored back into the database
    /// </summary>
    /// <param name="eventSender"></param>
    /// <param name="eventArgs"></param>
    /// <remarks></remarks>
    private void UpdateContent()
    {
        StringBuilder sbSQL;
        string sViewName;
        string sFormName;

        sViewName = cboViewNames.Text;
        if (string.IsNullOrEmpty(sViewName))
        {
            MessageBox.Show("Please select a view name first.");
            cboViewNames.Focus();
            return;
        }

        sFormName = cboFormNames.Text;
        if (string.IsNullOrEmpty(sFormName))
        {
            MessageBox.Show("Please select a form name first.");
            cboFormNames.Focus();
            return;
        }

        try
        {
            string sDatabaseID = ((KeyValuePair<string, string>)cboDatabases.SelectedItem).Key;
            string sViewID = ((KeyValuePair<string,string>)cboViewNames.SelectedItem).Key;

            sbSQL = new StringBuilder();
            using(DbConnection oConn = DbFactory.GetDbConnection(m_VPPConnectionString))
            {
                oConn.Open();
                DbCommand cmd = oConn.CreateCommand();
                DbParameter objParam = cmd.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.ParameterName = "ASPX";
                objParam.Value = txtView.Text;
                cmd.Parameters.Add(objParam);
                sbSQL.Append("UPDATE NET_VIEW_FORMS SET " );
                if( rdbASPX.Checked )
                    sbSQL.Append(" PAGE_CONTENT=~ASPX~" );
                else
                    sbSQL.Append(" VIEW_XML=~ASPX~");

                sbSQL.Append(", LAST_UPDATED = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                    + "' WHERE VIEW_ID = " + sViewID + " AND DATA_SOURCE_ID =" + sDatabaseID);

                if( rdbASPX.Checked )
                    sbSQL.Append(" AND PAGE_NAME='" + sFormName + "'" );
                else
                    sbSQL.Append(" AND FORM_NAME='" + sFormName + "'");

                cmd.CommandText = sbSQL.ToString();
                int iUpdate = cmd.ExecuteNonQuery();
                MessageBox.Show("The PowerView was updated successfully.", "PowerView Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        catch(Exception ex)
        {
            MessageBox.Show(ex.Message, "Error occurred during Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    private void btnSelectAll_Click(object sender, EventArgs e)
    {
        txtView.SelectAll();
        txtView.Copy();
    }

    private void btnFind_Click(object sender, EventArgs e)
    {
        int lIndex = 0;

        lIndex = txtView.Text.ToUpper().IndexOf(TextBox2.Text.ToUpper() , 0);

        // Determine if the word has been found and select it if it was.
        if (lIndex != -1)
        {
            // Select the string using the lIndex and the length of the string.
            txtView.SelectionStart = lIndex;
            txtView.SelectionLength = TextBox2.Text.Length;
            txtView.ScrollToCaret();
        }
    }

    private void btnFindNext_Click(object sender, EventArgs e)
    {
        int lIndex = 0;

        lIndex = txtView.Text.ToUpper().IndexOf(TextBox2.Text.ToUpper() , txtView.SelectionStart + 1);

        // Determine if the word has been found and select it if it was.
        if (lIndex != -1)
        {
            // Select the string using the lIndex and the length of the string.
            txtView.SelectionStart = lIndex;
            txtView.SelectionLength = TextBox2.Text.Length;
            txtView.ScrollToCaret();
        }
    }

    private void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        string sText = "";
        sText = txtView.Text;
        txtView.Text = "";
        txtView.WordWrap = CheckBox1.Checked;
        txtView.Text = sText;
    }

    }
}
