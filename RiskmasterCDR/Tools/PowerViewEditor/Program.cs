﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using Riskmaster.Security;

namespace PowerViewEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            int iClientId = 0;//rkaur27
            iClientId = Convert.ToInt32(ConfigurationManager.AppSettings["ClientId"]);//rkaur27
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PowerView(iClientId));//rkaur27
        }
    }
}
 