using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Reflection;


namespace Riskmaster.Tools.UnitTest 
{
	public class AutoBuildTestBase: TestBase
	{
		protected NameValueCollection m_AutoBuildSettings = null;
		protected XmlElement m_Targets = null;
		protected DbConnection m_conn = null;
		protected UserLogin m_objUserLogin=null;
		protected string m_dataSourceTarget;
		protected string m_dataSourceUserId;
		protected string m_dataSourcePassword;
		protected int m_buildNumber = 0;

		#region Construction/Destruction Logic
		public AutoBuildTestBase()
		{}
		override public  void Init()
		{
			base.Init();
			m_Targets = RMConfigurator.NamedNode("AutoBuildDeployment/Targets") as XmlElement;
			m_AutoBuildSettings = RMConfigurator.NameValueCollection("AutoBuildDeployment/Settings");
			string sBuildNumber = m_AutoBuildSettings["AutoBuildDeployment.BuildNumber"];
			if(sBuildNumber==null) sBuildNumber = "0";
			m_buildNumber = Int32.Parse(sBuildNumber);
		}

		//objElt is a "Target" node from the Riskmaster.config file.
		protected void SelectTarget(XmlElement objTargetElt)
		{
			// Apply mandatory settings from Riskmaster.config .
			m_dataSourceTarget = (objTargetElt.SelectSingleNode("DataSourceTarget") as XmlElement).InnerText;
			m_dataSourceUserId = (objTargetElt.SelectSingleNode("DataSourceUserId") as XmlElement).InnerText;
			m_dataSourcePassword = (objTargetElt.SelectSingleNode("DataSourcePassword") as XmlElement).InnerText;
			
			Login objLogin = null;
			m_objUserLogin = null;

			objLogin = new Login();
			m_objUserLogin = objLogin.AuthUser(m_dataSourceTarget, m_dataSourceUserId,  m_dataSourcePassword);
			
			m_conn = DbFactory.GetDbConnection(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
			m_conn.Open();

			objLogin = null;
		
		}

		override public void TearDown()
		{
			base.TearDown();
			if(m_conn!=null)
				m_conn.Close();
			m_objUserLogin = null;
			m_conn = null;
		}
		#endregion
	}
	/// <summary>
	/// AutoBuildDeployViews test fixture.  Turns out NUnit integration into
	/// Nant is not tight enough to specify by function name during the build.
	/// Only by fixture.
	/// Thus we end up with a number of indivual classes where functions 
	/// would be preferred.
	/// </summary>
	
	public class AutoBuildDeployViews: AutoBuildTestBase
	{
		#region Construction/Destruction Logic
		private string m_viewPath = "";
		public AutoBuildDeployViews()
		{}
		[SetUp]
		override public  void Init()
		{
			base.Init();
			m_viewPath = String.Format( m_AutoBuildSettings["AutoBuildDeployment.FDMViewPath"],this.m_buildNumber);
		}

		[TearDown]	 
		override public void TearDown()
		{
			base.TearDown();
		}
#endregion
		
		public void ResetSystemViewDataScreens()
		{
			try
			{
				foreach(XmlElement eltTarget in m_Targets.SelectNodes("Target"))
				{
					base.SelectTarget(eltTarget);
					Trace.WriteLine("Clearing all Database forms in " + m_conn.ConnectionString); 
					ClearSystemViewData();
					InsertSystemViewDataScreens();
					Trace.WriteLine("Reset all Database forms in '" + m_conn.ConnectionString + "' from disk path:" + m_viewPath); 
				}	
			}	
			catch(Exception e){Trace.WriteLine(e.Message + "\n" + e.StackTrace);}
		}

		#region Internal Functions to Implement Named Tests above.
		

		public void InsertSystemViewDataScreens()
		{
			string sTargetPath = m_viewPath;
			foreach(string sFileName in System.IO.Directory.GetFiles(sTargetPath,"*.xml"))
				InsertSystemViewDataScreen(System.IO.Path.GetFileName(sFileName));
		}		
		
		public void ClearSystemViewData()
		{
			m_conn.ExecuteNonQuery("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=0");
		}	
		public void InsertSystemViewDataScreen(string sFormName)
		{
			string sTargetPath = m_viewPath;
			string sFormTitle = "";
			string sFileName = sTargetPath + "\\" + sFormName; 
			string sSQL = "";
			XmlDocument dom = new XmlDocument();
			try{dom.Load(sFileName);}
			catch{Trace.WriteLine("Skipped Parser Error on : " + sFileName);}
			try{sFormTitle = dom.SelectSingleNode("//form/@title").Value;}
			catch{sFormTitle="Unspecified";}

			DbReader rdr = m_conn.ExecuteReader("SELECT * FROM NET_VIEW_FORMS WHERE 1=0");
			System.Data.DataTable objSchema = rdr.GetSchemaTable();
			rdr.Close();
			if(objSchema.Select("ColumnName='VIEW_XML_2'").Length==0)
				sSQL = String.Format("INSERT INTO NET_VIEW_FORMS VALUES(0,'{0}',1,~CAPTION~,~XML~)",System.IO.Path.GetFileName(sFileName));
			else
				sSQL = String.Format("INSERT INTO NET_VIEW_FORMS VALUES(0,'{0}',1,~CAPTION~,~XML~,NULL)",System.IO.Path.GetFileName(sFileName));

			//BSB 09.11.2006 Add parameters to the Parameters Collection in the order in which they 
			// appear in the SQL query to be executed (Oracle & ODBC both care about this)
			DbCommand cmd = m_conn.CreateCommand();
			DbParameter p2 = cmd.CreateParameter();
			p2.Value = sFormTitle;
			p2.ParameterName = "CAPTION";
			cmd.Parameters.Add(p2);

			DbParameter p = cmd.CreateParameter();
			p.Value = dom.OuterXml;
			p.ParameterName = "XML";
			cmd.Parameters.Add(p);
			
			// BSB 01.13.2006 Get Schema info for the table as some DB's have an extra column in this table.
			cmd.CommandText = sSQL;	
			cmd.ExecuteNonQuery();
		}
		#endregion

	}

	/// <summary>
	/// AutoBuildDeployment test fixture.  Turns out NUnit integration into
	/// Nant is not tight enough to specify by function name during the build.
	/// Only by fixture.
	/// Thus we end up with a number of indivual classes where functions 
	/// would be preferred.
	/// </summary>
	
	public class AutoBuildDeploySchema: AutoBuildTestBase
	{
		private DataModelFactory m_dmf = null;
		private string m_SchemaPath; 
		private string m_SchemaFile;	

		#region Construction/Destruction Logic
		public AutoBuildDeploySchema()
		{}
		[SetUp]
		override public void Init()
		{
			base.Init();
			m_SchemaPath = String.Format(m_AutoBuildSettings["AutoBuildDeployment.SchemaPath"],this.m_buildNumber);
            m_SchemaFile = m_AutoBuildSettings["AutoBuildDeployment.SchemaFile"];
		}

		[TearDown]	 
		override public void TearDown()
		{
			base.TearDown();
			if(m_dmf!=null)
				m_dmf.UnInitialize();
			m_dmf =null;
		}
		#endregion

		
		public void GenerateSchema()
		{
			//Only need to do this once - Just use the first declared target.
			base.SelectTarget(m_Targets.ChildNodes[0] as XmlElement);
			m_dmf = new DataModelFactory(base.m_objUserLogin);
			System.Reflection.Assembly a = System.Reflection.Assembly.GetAssembly(typeof(Riskmaster.DataModel.Event));
			Type[] types = a.GetTypes();
				using (StreamWriter w = File.CreateText(m_SchemaPath + m_SchemaFile))
				{
					w.Write(@"<?xml version=""1.0"" encoding=""utf-8""?>
				<xs:schema 
				elementFormDefault=""qualified"" 
				attributeFormDefault=""unqualified"" 
				xmlns:xs=""http://www.w3.org/2001/XMLSchema"" 
				xmlns:rm=""http://www.riskmaster.com/wizard""> 
				<xs:include schemaLocation=""RMInstanceTypes.xsd""  />");
					/*<xs:include schemaLocation=""RMControlTypes.xsd"" />*/
				
					foreach(Type t in types)
					{
						if(!t.IsClass)
							continue;
					
						if(t.BaseType.Name == "DataCollection")
							w.Write(GenerateListTypeSchema(t));
						else if(t.BaseType.Name != "DataObject" && t.BaseType.Name!="PersonInvolved" && t.BaseType.Name!="PersonInvolved" )
							continue;
						else
							w.Write(GenerateObjectSchema(t));
					}
					w.WriteLine("</xs:schema>");
				
			}//End Using Log File
		}

		#region Internal Functions to Implement Named Tests above.
		private static int Depth = 0;
		public string GenerateObjectSchema(Type t){return GenerateObjectSchema(t,"");}
		public string GenerateObjectSchema(Type t, string sProp)
		{
			Depth++;
			Trace.Write(t.Name);
			//if(t.Name == "ClaimAdjuster")
            
			
			if(sProp=="")
				sProp = t.Name;

			string result = "";
			string stmp="";
			Riskmaster.DataModel.ExtendedTypeAttribute ext = null;
			object o = m_dmf.GetDataModelObject(t.Name,false);
			DataObject objData =    (o as DataObject);
			SupplementalObject objSuppBase =(o as SupplementalObject); 
			if(objSuppBase == null && objData ==null)
			{
				Depth--;
				return "";
			}
			if(objSuppBase!=null && Depth<=1)
			{
				Depth--;
				return "";
			}
			if(Depth>1)
			{
				result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" minOccurs=\"0\" maxOccurs=\"1\"/>",sProp,t.Name);
				Depth--;
				return result;
			}
			else
			{
				result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />",sProp,t.Name);
				result += String.Format("<xs:complexType name=\"{0}-type\">",t.Name);
			}
			result += "<xs:sequence>";
			string s;
			//			foreach(string s in objData.Properties)
			foreach(DataObject.PropertyMetaInfo objInfo in objData.SerializationSequence)
			{
				s = objInfo.PropertyName;
				if(objData.OnTrimSerialization(s))
					continue;
				if(s =="Parent")
				{
					result += String.Format("<xs:element  name=\"Parent\" type=\"xs:string\" />");
					if(t.GetProperty("Supplementals") !=null)
						result +="<xs:element name=\"Supplementals\" type=\"Supplementals-Type\" minOccurs=\"0\" maxOccurs=\"1\"/>";

					continue;
				}
				//if(s == "CurrentAdjuster")
					

				ext =GetExtendedTypeAttribute(objData,t,s);
				
				if(objData.HasChildProperty(s,true) || s =="Parent") //It's a sub-object
				{
					DataObject objChild = objData.GetProperty(s) as DataObject;
					if(objChild != null) //Check that child is a valid singleton
					{
						if(!(objChild.GetType().Name=="FundsAuto" && t.Name=="FundsAutoSplit")) //Cyclical Object Ref
							if(!(objChild.GetType().Name=="FundsAuto" && t.Name=="FundsAutoBatch")) //Cyclical Object Ref
								if(!(objChild.GetType().Name=="FundsAutoBatch" && t.Name=="FundsAuto")) //Cyclical Object Ref
									result +=GenerateObjectSchema(objChild.GetType(),s);
					}
					else //Property Returned Null but in this case we want all possibilities for the schema but this one must be 
						// Optional in instance data
					{
						Type objType = GetReturnType(objData,s);
						result +=GenerateObjectSchema(objType,s);
					}

					DataCollection objList = objData.GetProperty(s) as DataCollection;
					if(objList != null) //Check that child is a valid list
					{
						result += String.Format("<xs:element name=\"{0}\" minOccurs=\"0\" maxOccurs=\"1\"><xs:complexType>",s); // BSB Internal types must be anonymous ...
						//result += String.Format("<xs:element name=\"{0}\"><xs:complexType name=\"{1}-type\">",s,objList.GetType().Name);
						result += "<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">";
						//if(objList.GetTypeName()!=objData.GetType().Name)
						//	result += GenerateObjectSchema(dmf.GetDataModelObject(objList.GetTypeName(),false).GetType()) ;
						//else
						result += String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />",objList.GetTypeName(),objList.GetTypeName()) ;
						result += "</xs:sequence>";
						result += "<xs:attribute name=\"count\" type=\"xs:int\"/>";
						result += "<xs:attribute name=\"committedcount\" type=\"xs:int\"/>";
						result += "</xs:complexType>";
						result += "</xs:element>";
					}
					DataSimpleList objSimpleList = objData.GetProperty(s) as DataSimpleList;
					if(objSimpleList != null)
					{
						result += "<xs:element name=\"" + s + "\" type=\"simpleList-type\">";//TODO - IMPLEMENT SIMPLELIST";
						result += "</xs:element>";
					}
				}
				else 
				{
					if(ext==null)
						stmp = "unspecified";
					else if(ext.FieldType == RMExtType.Code)
						stmp = "code";
					else if(ext.FieldType == RMExtType.CodeList)
						stmp = "codelist";
					else if(ext.FieldType==RMExtType.Entity)
						stmp = "entity";
					else if(ext.FieldType==RMExtType.EntityList)
						stmp = "entitylist";
					else if(ext.FieldType==RMExtType.OrgH)
						stmp = "orgh";
					else if(ext.FieldType==RMExtType.ChildLink)
						stmp = "childlink";
					else
						stmp="missing-RMExtType";

					result += "<xs:element name=\"" + s + "\" type=\"" + stmp + "\" />";
				}	
			
				//Suggest This Item be tagged with extended info...
				//				RecordSuggestion(objData, t,s);
			}// End For Each
			

			result += "</xs:sequence>";
			if(Depth>1)
				result += String.Format("</xs:element>");
			else
			{
				result += "<xs:attribute name=\"remove\" type=\"xs:string\" use=\"optional\"/>";
				result += String.Format("</xs:complexType>");
			}
			Depth--;
			return  result;
		}
		public string GenerateListTypeSchema(Type t)
		{
			string result = "";
			DataCollection objList = m_dmf.GetDataModelObject(t.Name,false) as DataCollection;
			if(objList != null) //Check that child is a valid list
			{
				result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />",t.Name,t.Name);
				result += String.Format("<xs:complexType name=\"{0}-type\">",t.Name);
				//	result += String.Format("<xs:element name=\"{0}\" minOccurs=\"0\" maxOccurs=\"1\"><xs:complexType>",s); // BSB Internal types must be anonymous ...
				//result += String.Format("<xs:element name=\"{0}\"><xs:complexType name=\"{1}-type\">",s,objList.GetType().Name);
				result += "<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">";
				result += String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />",objList.GetTypeName(),objList.GetTypeName()) ;
				result += "</xs:sequence>";
				result += "<xs:attribute name=\"count\" type=\"xs:int\"/>";
				result += "<xs:attribute name=\"committedcount\" type=\"xs:int\"/>";
				result += "</xs:complexType>";
				//result += "</xs:element>";
			}
			return result;
		}
		private static ExtendedTypeAttribute GetExtendedTypeAttribute(DataObject objData, Type t, string s)
		{
			ExtendedTypeAttribute ext = null;
			MemberInfo inf=null;
			try{inf = (t.GetMember(s)[0]);}
			catch
			{ext = null;}
			try{ext =inf.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute),false)[0] as Riskmaster.DataModel.ExtendedTypeAttribute;}
			catch(IndexOutOfRangeException){}
			return ext;
		}
		private static Type GetReturnType(DataObject objData, string sProp)
		{
			try
			{
				return ((objData.GetType().GetMember(sProp)[0]) as PropertyInfo).GetGetMethod().ReturnType;
			}
			catch(IndexOutOfRangeException){return null;}
		}
		#endregion

	}
}
