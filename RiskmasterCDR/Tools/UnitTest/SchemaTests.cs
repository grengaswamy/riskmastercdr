using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.DataModel;
using Riskmaster.Common;
using System.Reflection;
using Riskmaster.Security;


namespace Riskmaster.Tools.UnitTest
{
	/// <summary>
	/// Summary description for SchemaTests.
	/// </summary>
	
	public class SchemaTests  : DMFTestBase
	{
        const string SCHEMA_FILE_NAME = "schema-1.xsd";


        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="strWorkPath">string containing the directory path to the schema file</param>
        /// <param name="strDSN">string containing the DSN as specified in the RMX Security database DATA_SOURCE_TABLE</param>
        /// <param name="strUID">string containing the user name required to log into the RMX database</param>
        /// <param name="strPWD">string containing the password required to log into the RMX database</param>
        public SchemaTests(string strWorkPath, string strDSN, string strUID, string strPWD)
        {
            
            base.WorkPath = strWorkPath;
            base.DataSourceTarget = strDSN;
            base.DataSourceUserId = strUID;
            base.DataSourcePassword = strPWD;
            base.Init();
        }
		
        /// <summary>
        /// Generates the Data Model schema by connecting to a RISKMASTER Database
        /// and iterating through the current Data Model members using Reflection
        /// </summary>
        /// <remarks>This method is unable to create the file just prior to the creation
        /// and population of the schema file due to contention of resources.  Therefore,
        /// this schema file should automatically be removed as part of an autobuild process
        /// or a call to an external .Net assembly.</remarks>
		public void GenerateSchema()
		{
            string strSchemaFilePath = base.WorkPath + SCHEMA_FILE_NAME;
            const int INDENTATION_LEVEL = 4;
            StringBuilder rawXML = new StringBuilder();
            

            System.Reflection.Assembly a = System.Reflection.Assembly.GetAssembly(typeof(Riskmaster.DataModel.Event));
            Type[] types = a.GetTypes();

			XmlTextWriter objXmlWriter = new XmlTextWriter(strSchemaFilePath, Encoding.UTF8);

            //Set the formatting of the document
            objXmlWriter.Formatting = Formatting.Indented;
            objXmlWriter.Indentation = INDENTATION_LEVEL; //Set the indentation
            

            //Create a new line for the beginning of the document
            rawXML.AppendLine(Environment.NewLine);
            //Build the beginning of the XML Document
            rawXML.Append(@"<xs:schema elementFormDefault=""qualified""");
            rawXML.Append(@" attributeFormDefault=""unqualified""");
            rawXML.Append(@" xmlns:xs=""http://www.w3.org/2001/XMLSchema""");
            rawXML.Append(@" xmlns:rm=""http://www.riskmaster.com/wizard"">");
            rawXML.AppendLine(Environment.NewLine);
            rawXML.AppendLine(@" <xs:include schemaLocation=""RMInstanceTypes.xsd""  />");

				
			foreach(Type t in types)
			{
				if(!t.IsClass)
					continue;

                if (t.BaseType.Name == "DataCollection")
                {
                    //Append the XML to the StringBuilder
                    rawXML.AppendLine(GenerateListTypeSchema(t));
                }
                else if (t.BaseType.Name != "DataObject" && t.BaseType.Name != "PersonInvolved" && t.BaseType.Name != "PersonInvolved")
                    continue;
                else
                {
                    //Append the XML to the StringBuilder
                    rawXML.AppendLine(GenerateObjectSchema(t));
                }
			}
            //Close the schema document
			rawXML.AppendLine("</xs:schema>");

            //Create the start of the document
            objXmlWriter.WriteStartDocument();

            //Write out the raw XML
            objXmlWriter.WriteRaw(rawXML.ToString());

            //Close the XML Writer
            objXmlWriter.Close();
		}


        /// <summary>
        /// Creates the specified text file if it does not
        /// already exist
        /// </summary>
        /// <param name="strFilePath">string containing the file path 
        /// for the file to be created</param>
        private void CreateFile(string strFilePath)
        {
            if (!File.Exists(strFilePath))
            {
                File.CreateText(strFilePath);
            }//if
        }//method: CreateFile

		private static int Depth = 0;
		public string GenerateObjectSchema(Type t){return GenerateObjectSchema(t, string.Empty);}

        /// <summary>
        /// Generate Object Schema based on reflected members in DataModel
        /// </summary>
        /// <param name="t"></param>
        /// <param name="sProp"></param>
        /// <returns></returns>
		public string GenerateObjectSchema(Type t, string sProp)
		{
            StringBuilder rawXML = new StringBuilder();
            string stmp = string.Empty;
			Depth++;
			Trace.Write(t.Name);
			
            if (string.IsNullOrEmpty(sProp))
            {
                sProp = t.Name;
            }

			Riskmaster.DataModel.ExtendedTypeAttribute ext = null;
			object o = dmf.GetDataModelObject(t.Name,false);
			DataObject objData =    (o as DataObject);
			SupplementalObject objSuppBase =(o as SupplementalObject); 
			if(objSuppBase == null && objData ==null)
			{
				Depth--;
                return string.Empty;
			}
			if(objSuppBase!=null && Depth<=1)
			{
				Depth--;
                return string.Empty;
			}
			if(Depth>1)
			{
                rawXML.AppendLine(String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" minOccurs=\"0\" maxOccurs=\"1\"/>", sProp, t.Name));
				Depth--;
                return rawXML.ToString();
			}
			else
			{
                rawXML.AppendLine(String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />", sProp, t.Name));
                rawXML.AppendLine(String.Format("<xs:complexType name=\"{0}-type\">", t.Name));
			}
            rawXML.AppendLine("<xs:sequence>");
			
			foreach(string s in objData.Properties)
			{
				if(objData.OnTrimSerialization(s))
					continue;
				if(s =="Parent")
				{
                    rawXML.AppendLine(String.Format("<xs:element  name=\"Parent\" type=\"xs:string\" />"));
					continue;
				}

				ext =GetExtendedTypeAttribute(objData,t,s);
				
				if(objData.HasChildProperty(s,true) || s =="Parent") //It's a sub-object
				{
					DataObject objChild = objData.GetProperty(s) as DataObject;
					if(objChild != null) //Check that child is a valid singleton
					{
                        if (!(objChild.GetType().Name == "FundsAuto" && t.Name == "FundsAutoSplit")) //Cyclical Object Ref
                            if (!(objChild.GetType().Name == "FundsAuto" && t.Name == "FundsAutoBatch")) //Cyclical Object Ref
                                if (!(objChild.GetType().Name == "FundsAutoBatch" && t.Name == "FundsAuto")) //Cyclical Object Ref
                                    rawXML.AppendLine(GenerateObjectSchema(objChild.GetType(), s));
					}
					else //Property Returned Null but in this case we want all possibilities for the schema but this one must be 
						// Optional in instance data
					{
						Type objType = GetReturnType(objData,s);
                        rawXML.AppendLine(GenerateObjectSchema(objType, s));
					}

					DataCollection objList = objData.GetProperty(s) as DataCollection;
					if(objList != null) //Check that child is a valid list
					{
                        rawXML.AppendLine(String.Format("<xs:element name=\"{0}\" minOccurs=\"0\" maxOccurs=\"1\"><xs:complexType>", s)); 
                        rawXML.AppendLine("<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">");
                        rawXML.AppendLine(String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />", objList.GetTypeName(), objList.GetTypeName()));
                        rawXML.AppendLine("</xs:sequence>");
                        rawXML.AppendLine("<xs:attribute name=\"count\" type=\"xs:int\"/>");
                        rawXML.AppendLine("</xs:complexType>");
                        rawXML.AppendLine("</xs:element>");
					}
					DataSimpleList objSimpleList = objData.GetProperty(s) as DataSimpleList;
					if(objSimpleList != null)
					{
                        rawXML.AppendLine("<xs:element name=\"" + s + "\" type=\"simpleList-type\">"); //TODO - IMPLEMENT SIMPLELIST"
                        rawXML.AppendLine("</xs:element>");
					}
				}
				else 
				{
					if(ext==null)
						stmp = "unspecified";
					else if(ext.FieldType == RMExtType.Code)
						stmp = "code";
					else if(ext.FieldType == RMExtType.CodeList)
						stmp = "codelist";
					else if(ext.FieldType==RMExtType.Entity)
						stmp = "entity";
					else if(ext.FieldType==RMExtType.EntityList)
						stmp = "entitylist";
					else if(ext.FieldType==RMExtType.OrgH)
						stmp = "orgh";
					else
						stmp="missing-RMExtType";

                    rawXML.AppendLine("<xs:element name=\"" + s + "\" type=\"" + stmp + "\" />");
				}	
			
				//Suggest This Item be tagged with extended info...
//				RecordSuggestion(objData, t,s);
			}// End For Each

            if (t.GetProperty("Supplementals") != null)
                rawXML.AppendLine("<xs:element name=\"Supplementals\" type=\"Supplementals-Type\" minOccurs=\"0\" maxOccurs=\"1\"/>");

            rawXML.AppendLine("</xs:sequence>");
            if (Depth > 1)
                rawXML.AppendLine(String.Format("</xs:element>"));
            else
            {
                rawXML.AppendLine("<xs:attribute name=\"remove\" type=\"xs:string\" use=\"optional\"/>");
                rawXML.AppendLine(String.Format("</xs:complexType>"));
            }
			Depth--;
            return rawXML.ToString();
		}//method: GenerateObjectSchema()

        /// <summary>
        /// Generate List Schema information based on reflected members from DataModel
        /// </summary>
        /// <param name="t">reflected member from DataModel</param>
        /// <returns>string containing the resultant schema XML</returns>
		public string GenerateListTypeSchema(Type t)
		{
            StringBuilder rawXML = new StringBuilder();
			DataCollection objList = dmf.GetDataModelObject(t.Name,false) as DataCollection;
			if(objList != null) //Check that child is a valid list
			{
                //Use the StringBuilder object to build the XML rather than plain string concatenation
                rawXML.AppendLine(String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />", t.Name, t.Name));
                rawXML.AppendLine(String.Format("<xs:complexType name=\"{0}-type\">", t.Name));
                rawXML.AppendLine("<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">");
                rawXML.AppendLine(String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />", objList.GetTypeName(), objList.GetTypeName()));
                rawXML.AppendLine("</xs:sequence>");
                rawXML.AppendLine("<xs:attribute name=\"count\" type=\"xs:int\"/>");
                rawXML.AppendLine("</xs:complexType>");
				
			}
            return rawXML.ToString();
		}//method: GenerateListTypeSchema()

//		static public void RecordSuggestion(DataObject objData, Type t, string s)
//		{
//			string result = "unspecified";
//			string s2;
//			string status = "";
//			DbConnection cn = DbFactory.GetDbConnection(CODE_MAP_DSN);
//			ExtendedTypeAttribute ext=GetExtendedTypeAttribute(objData, t,s);
//			Riskmaster.DataModel.SummaryAttribute att = null;
//			att = t.GetCustomAttributes(typeof(Riskmaster.DataModel.SummaryAttribute),false)[0] as SummaryAttribute;
//
//			if(ext != null)
//				status="marked";
//			
//			s2 = objData.PropertyNameToField(s).ToLower();
//
//			if(s2.IndexOf("code")>=0)
//				result = "CODEID";
//			else if(s2.EndsWith("status"))
//				result = "CODEID";
//			else if(s2.EndsWith("type"))
//				result = "CODEID";
//			else if(s2.EndsWith("eid"))
//				result = "ENTITYID";
//			else if(s2.EndsWith("_id"))
//				result = "LINKID";
//
//			if(result !="unspecified")
//			{
//				cn.Open();
//				cn.ExecuteNonQuery(String.Format("INSERT INTO FIELD_MAP_AUTO (OBJECT, PROPERTY,TYPE, STATUS, DBTABLE, DBFIELD) VALUES('{0}','{1}','{2}','{3}','{4}','{5}') ",t.Name,s,result,status,att.TableName,objData.PropertyNameToField(s).ToUpper())); 
//				cn.Close();
//			}
//		}
		private static ExtendedTypeAttribute GetExtendedTypeAttribute(DataObject objData, Type t, string s)
		{
			ExtendedTypeAttribute ext = null;
			MemberInfo inf=null;
			try{inf = (t.GetMember(s)[0]);}
			catch
			{ext = null;}
			try{ext =inf.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute),false)[0] as Riskmaster.DataModel.ExtendedTypeAttribute;}
			catch(IndexOutOfRangeException){}
			return ext;
		}
		private static Type GetReturnType(DataObject objData, string sProp)
		{
			try
			{
				return ((objData.GetType().GetMember(sProp)[0]) as PropertyInfo).GetGetMethod().ReturnType;
			}
			catch(IndexOutOfRangeException){return null;}
		}

	}
}
