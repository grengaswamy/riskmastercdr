using System;
using Riskmaster.DataModel;
using Riskmaster.Common;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.Security;

namespace Riskmaster.Tools.UnitTest
{


	

	///Summary of UnitTestBase
	// This class addresses the need for a consistent 
	// pattern for getting settings into Unit Testing to avoid hard coded
	// and developer\machine specific strings: ie DSN's, file paths etc.
	
	public class TestBase
	{
		static TestBase.Listener TestListener = new TestBase.Listener();
		static NameValueCollection _unitTestSettings = null;
		public NameValueCollection UnitTestSettings{get{return _unitTestSettings;}}
        
	
		[SetUp]
		public virtual void Init()
		{
			Trace.Listeners.Add(TestListener);
		}

		[TearDown]	 
		virtual public void TearDown()
		{
			Trace.Listeners.Remove(TestListener);
		}

		protected string GetConfigString(string name)
		{
			// Unit Testing Settings are represented by name-value pairs.  
			if(_unitTestSettings==null)
                _unitTestSettings = RMConfigurator.NameValueCollection("UnitTestSettings");

			string ret = _unitTestSettings[name];
			if(ret ==null)
			{
				throw(new ConfigurationErrorsException(String.Format("Mandatory NUnit Test Configuration Setting '{0}' Not Found.",name)));
			}
			return ret;
		}

		public class Listener : TraceListener
		{
			public override void Write(string message)
			{
				Console.Write(message);
			}

			public override void WriteLine(string message)
			{
				Console.WriteLine(message);
			}
		}
	}
}

