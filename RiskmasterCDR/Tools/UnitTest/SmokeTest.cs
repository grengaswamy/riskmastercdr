using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Reflection;


namespace Riskmaster.Tools.UnitTest 
{
	
	public class SmokeTest:AutoBuildTestBase
	{

		
		#region Construction/Destruction Logic
		public SmokeTest()
		{}
		override public  void Init()
		{
			base.Init();
			string m_MasterConnStr = m_AutoBuildSettings["SmokeTest.MasterConnectionString"];

			// Detatch any existing copies of the Smoke Testing Environment Databases
			using(DbConnection cn = DbFactory.GetDbConnection(m_MasterConnStr))
			{
				cn.Open();
				
				try{	cn.ExecuteNonQuery("EXEC master.dbo.sp_detach_db @dbname = N'RMX_RELEASED'");}
				catch(Exception e){}
				try{	cn.ExecuteNonQuery("EXEC master.dbo.sp_detach_db @dbname = N'RMX_RELEASED_SESSION'");}
				catch(Exception e){}
				try{	cn.ExecuteNonQuery("EXEC master.dbo.sp_detach_db @dbname = N'RMX_RELEASED_SECURITY'");}
				catch(Exception e){}
				try{	cn.ExecuteNonQuery("EXEC master.dbo.sp_detach_db @dbname = N'RMX_RELEASED_SMSERVER'");}
				catch(Exception e){}
				
				cn.Close();
			}

			// Delete any existing Smoke Testing Environment MDF files.
			string m_ReleasedDbPath = m_AutoBuildSettings["SmokeTest.ReleasedDbPath"];
			//try
			//{
				File.Delete(m_ReleasedDbPath + "RMX_RELEASED.mdf");
				File.Delete(m_ReleasedDbPath + "RMX_RELEASED_SESSION.mdf");
				File.Delete(m_ReleasedDbPath + "RMX_RELEASED_SECURITY.mdf");
				File.Delete(m_ReleasedDbPath + "RMX_RELEASED_SMSERVER.mdf");
                DirectoryInfo dir = new DirectoryInfo(m_ReleasedDbPath);
                foreach( FileInfo f in dir.GetFiles("*.ldf"))
                    f.Delete();

            //System.IO.Directory.Delete(m_ReleasedDbPath + "*.ldf");
			//}				
			//catch(Exception e){}


			// In the Build\Latest\DB folder of the build machine (current directory\DB during unit testing)
			// Grab the Zip file containing RMX Database files (SQL MDF files) 
			System.IO.File.Copy(AppDomain.CurrentDomain.BaseDirectory + "\\RMXReleasedDatabases.zip",m_ReleasedDbPath + "RMXReleasedDatabases.zip",true);
			

			// RMX_RELEASED
			// RMX_RELEASED_SESSION
			// RMX_RELEASED_SECURITY
			// RMX_RELEASED_SMSERVER
			// Shell to Unzip the MDF files to a UNC share on a Database Server
			string m_ZipCmd = m_AutoBuildSettings["SmokeTest.ZipCmd"];
			string m_ZipArgs = m_AutoBuildSettings["SmokeTest.ZipArgs"];
			ExecCmdLine(m_ZipCmd,m_ZipArgs,m_ReleasedDbPath);


			// Attach the MDF files to the SQL Server instance
			// Detatch any existing copies of the Smoke Testing Environment Databases
			string m_DbAttachPath = m_AutoBuildSettings["SmokeTest.ReleasedDbPathServerLocal"];
			
			using(DbConnection cn = DbFactory.GetDbConnection(m_MasterConnStr))
			{
				cn.Open();
				try
				{
					cn.ExecuteNonQuery(String.Format("CREATE DATABASE [RMX_RELEASED] ON ( FILENAME = N'{0}\\RMX_RELEASED.mdf' ) FOR ATTACH",m_DbAttachPath));
					cn.ExecuteNonQuery(String.Format("CREATE DATABASE [RMX_RELEASED_SESSION] ON ( FILENAME = N'{0}\\RMX_RELEASED_SESSION.mdf' ) FOR ATTACH",m_DbAttachPath));
					cn.ExecuteNonQuery(String.Format("CREATE DATABASE [RMX_RELEASED_SECURITY] ON ( FILENAME = N'{0}\\RMX_RELEASED_SECURITY.mdf' ) FOR ATTACH",m_DbAttachPath));
					cn.ExecuteNonQuery(String.Format("CREATE DATABASE [RMX_RELEASED_SMSERVER] ON ( FILENAME = N'{0}\\RMX_RELEASED_SMSERVER.mdf' ) FOR ATTACH",m_DbAttachPath));
				}
				finally
				{
					cn.Close();
				}
			}

			// Shell to invoke the RMX Database Upgrade.
			ExecCmdLine(AppDomain.CurrentDomain.BaseDirectory + "\\RMXDBUpgrade"," -noprompt target:RMX_RELEASED",AppDomain.CurrentDomain.BaseDirectory);

			// Now the "Smoke Tests" may safely begin
		}

		override public void TearDown()
		{
			base.TearDown();
		}
		#endregion

		#region Private Util Routines
		private int ExecCmdLine(string sCmd, string sParams, string sWorkPath)
		{
			Process proc = new  Process();
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.FileName ="\"" + sCmd+ "\"";
			proc.StartInfo.WorkingDirectory = sWorkPath;
			proc.StartInfo.Arguments = sParams;
			proc.Start();
			proc.WaitForExit();
			return proc.ExitCode;
		}
		#endregion
		#region Smoke Tests to be Performed...
		
		public void TestPreparations()
		{
			Trace.WriteLine("Test Preparations Completed");
		}
		#endregion
	}
}

