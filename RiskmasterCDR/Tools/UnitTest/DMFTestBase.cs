using System;
using Riskmaster.DataModel;
using Riskmaster.Common;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.Security;
namespace Riskmaster.Tools.UnitTest
{
	/// <summary>
	/// Summary description for DMFTestBase.
	/// </summary>
	public class DMFTestBase
	{
		
		private DataModelFactory _dmf=null;
		public DataModelFactory dmf{get{return _dmf;}}
        public NameValueCollection UnitTestSettings { get; set; }

		//TEST SETTINGS FROM APPLICATION .CONFIG FILE
		protected string _workPath = "";
		public string WorkPath
        {
            get{return _workPath;}
            set
            {
                _workPath = value;
            }
        }
        protected string _dataSourceTarget = string.Empty;
		public string DataSourceTarget
        {
            get{return _dataSourceTarget;}
            set
            {
                _dataSourceTarget = value;
            }//set
        }
        protected string _dataSourceUserId = string.Empty;
		public string DataSourceUserId
        {
            get{return _dataSourceUserId;}
            set
            {
                _dataSourceUserId = value;
            }//set
        }
        protected string _dataSourcePassword = string.Empty;
		public string DataSourcePassword
        {
            get{return _dataSourcePassword;}
            set
            {
                _dataSourcePassword = value;
            }//set
        }
        

        public DMFTestBase() : base() { }

        /// <summary>
        /// Default class constructor
        /// </summary>
        public DMFTestBase(string strWorkPath, string strDSN, string strUID, string strPWD)
        {

            _workPath = strWorkPath;
            _dataSourceTarget = strDSN;
            _dataSourceUserId = strUID;
            _dataSourcePassword = strPWD;

        }

        public void Init()
        {
            //Create a new DataModel Factory object
            _dmf = new DataModelFactory(DataSourceTarget, DataSourceUserId, DataSourcePassword);
        }

        //[SetUp]
        //override public  void Init()
        //{
        //    base.Init();
        //    // Apply mandatory settings from Riskmaster.config .
        //    _workPath = GetConfigString("WorkPath");
        //    _dataSourceTarget = GetConfigString("DataSourceTarget");
        //    _dataSourceUserId = GetConfigString("DataSourceUserId");
        //    _dataSourcePassword = GetConfigString("DataSourcePassword");
			
        //    Login objLogin = null;
        //    UserLogin objUserLogin = null;

        //    //BSB 03.02.2005 Test alternate entry into DataModel
        //    objLogin = new Login();
        //    objUserLogin = objLogin.AuthUser(DataSourceTarget, DataSourceUserId,  DataSourcePassword);
        //    _dmf = new DataModelFactory(objUserLogin);
        //    //_dmf = new DataModelFactory(DataSourceTarget,DataSourceUserId,DataSourcePassword);
        //    objLogin = null;
        //}

        //[TearDown]	 
        //override public void TearDown()
        //{
        //    base.TearDown();
        //    dmf.UnInitialize();
        //    _dmf = null;
        //}
	}
}
