using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Security; 


namespace Riskmaster.Tools.C2N
{
    public class AppGlobals
    {
        public static string sUser = ""; //user login
        public static string sPwd = ""; //password
        public static string sDSN = ""; //database name
        public static bool bSilentMode = false;        

        // for storing the comments
        public static DataSet m_dsComments = new DataSet();

        // for storing the extracted Notes
        public static DataSet m_dsEnhNotes = new DataSet();

        public static string ConnectionString;//connection string

        public static UserLogin Userlogin;

    }
}
