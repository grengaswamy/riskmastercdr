using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Tools.C2N;


namespace Riskmaster.Tools.C2N
{
   
    ///<summary>
    ///The tool migrates the comments attached to events and claims into enhanced notes. 	 
    ///<remarks>*	The tool can either be run directly or can be executed via a batch file/installer in silent mode.
    ///Command line parameters-
    ///Riskmaster.Tools.C2N.exe uid pwd DSN
    ///</remarks>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] Args )
        {
            try 
	        {
                Application.Run(new frmCommentsMigration());
	        }
	        catch (Exception ex)
	        {
        		
		        string strErrMessage = "There seems to be some problem. \nError description:\n\n";
                //if (!AppGlobals.bSilentMode)
                MessageBox.Show(strErrMessage + ex.Message, "C2N: Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
	        }
        }
    }   
}