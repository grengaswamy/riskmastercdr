﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;

namespace Riskmaster.Tools.C2N
{
    public partial class frmCommentsMigration : Office2007RibbonForm
    {
        #region declare global variables

        private static DbWriter m_EnhcNotesdbWriter;
        private static DbConnection objCon = null;
        private static DbTransaction objTrans = null;
        private static DbCommand objCmd = null;

        private string m_strSelectedDSN = string.Empty, m_ConnectionString = string.Empty;
        private int m_iDSNId = 0;
         
        private static Hashtable m_UidCache = new Hashtable(150);
        private static Hashtable m_GroupidCache = new Hashtable(150);
        private static string[] m_LOBinfo = new string[20]; //assuming max 10 LOB division 
        private static int m_LOBCount;
        public static StreamWriter m_sw1;
        private static bool blnSuccess = false;

        public static int m_iLanguageCode;

        private int m_NoteTypeCodeId = -1;
        private int m_iNoteId = -1;
        private string strEnteredBy = string.Empty;
        private string strUid = string.Empty;
        private string strGroupID = string.Empty;

        private string m_dateFormat = string.Empty;
        private bool m_userName = false;
        // regular expression pattern to extract comments
        // it has been modified to accept dates in both mm/dd/yy or dd/mm/yy format ..... 
        //private string m_pattern = @"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?";

        //using the compiled regex now.....
        //Regex RexDtStamp = new Regex(@"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        //Regex RexDtStamp = new Regex(@"((19|20)?[0-9]{2})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        Regex RexDtStamp = null;
        Regex RexLineBreaks = new Regex(@"<\s?br/>", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        char[] m_Breaks = new char[] { '\t', '\n', ' ' };

        //zmohammad 10/16/2012
        string sqlLoadComment = string.Empty;
        StringBuilder sbSqlLOBGroup = new StringBuilder();

        private static long m_CommentsCount = 0;
        private static long m_EnhancedNotes = 0;

        public static string m_sEnhcNotesShortCode = System.Configuration.ConfigurationSettings.AppSettings["EnhcNotesShortCode"];
        //"C2N"; // Short code provided should be in UPPER case        
        public static string m_sEnhcNotesCodeDesc = System.Configuration.ConfigurationSettings.AppSettings["EnhcNotesShortDesc"];
        //"Comm2EnhNotes";

        // saving the "Now" time which will be appear as the created datetime & activity datetime in ehnc notes
        private string m_nowDateTime = Conversion.ToDbDate(System.DateTime.Now);
        private string m_nowTime = Conversion.ToDbTime(System.DateTime.Now);

        //  private static DbWriter m_EnhcNotesdbWriter;

        private int m_strDisplay = 0;
        //1 = event is displayed
        //2 = claim is displayed





        // below is the complete pattern which will match for 
        // with some relaxation 
        // in dates(mm/dd/yy); in place of slash, the user can have : or - or . 
        // in time ; user can skip the seconds part, can have it with AM/Pm text or whcihout it.
        // however it months value will have to be less than 12 
        // dates value will have to be les than 31 etc
        //private string m_pattern = @"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\(([a-zA-Z0-9]{1,12})\))?";



        #endregion

        #region Page Events

        /// <summary>
        /// frmCommentsMigration
        /// </summary>
        public frmCommentsMigration()
        {
            InitializeComponent();
        }

        /// <summary>
        /// frmCommentsMigration_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCommentsMigration_Load(object sender, EventArgs e)
        {
            try
            {
                m_sEnhcNotesShortCode = System.Configuration.ConfigurationSettings.AppSettings["EnhcNotesShortCode"];
                //"C2N"; // Short code provided should be in UPPER case        
                m_sEnhcNotesCodeDesc = System.Configuration.ConfigurationSettings.AppSettings["EnhcNotesShortDesc"];
                //"Comm2EnhNotes";
                // Get the Language Code
                m_iLanguageCode = Conversion.CastToType<int>(System.Configuration.ConfigurationSettings.AppSettings["LanguageCode"].ToString(), out blnSuccess);
                if (m_sEnhcNotesShortCode == null || m_sEnhcNotesCodeDesc == null || m_iLanguageCode == 0)
                {
                    MessageBox.Show("plz make sure EnhcNotesShortCode, EnhcNotesShortDesc and LanguageCode values are provided in the confilg file");
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("plz make sure EnhcNotesShortCode, EnhcNotesShortDesc and LanguageCode values are provided in the confilg file");
                throw;
            }
            m_sw1 = new StreamWriter("C2C.log", true);
            LoadRiskmasterDatabases();
        }

        //zmohammad 10/15/12  Start button action.
        /// <summary>
        /// Start Button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {

                m_dateFormat = System.Configuration.ConfigurationSettings.AppSettings["DateFormat"];
                m_userName = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["Username"]);
                if (m_dateFormat.Equals("YYYY/mm/dd"))
                {
                    if (m_userName) {
                        RexDtStamp = new Regex(@"((19|20)?[0-9]{2})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? (\([a-zA-Z0-9]{1,12}\))", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                    else
                    {
                        RexDtStamp = new Regex(@"((19|20)?[0-9]{2})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                }
                else if (m_dateFormat.Equals("dd/mm/YYYY"))
                {
                    if (m_userName) {
                        RexDtStamp = new Regex(@"(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? (\([a-zA-Z0-9]{1,12}\))", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                    else
                    {
                        RexDtStamp = new Regex(@"(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                }
                else if (m_dateFormat.Equals("mm/dd/YYYY"))
                {
                    if (m_userName)
                    {
                        RexDtStamp = new Regex(@"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? (\([a-zA-Z0-9]{1,12}\))", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                    else
                    {
                        RexDtStamp = new Regex(@"(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.]((19|20)?[0-9]{2}) *([0-9]{1,2}:[0-9]{1,2}:?[0-9]{0,2}) ?([AP]M)? ?(\([a-zA-Z0-9]{1,12}\))?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                }
   


                rtfStatus.Clear();
                //Creating connection and transaction
                objCon = DbFactory.GetDbConnection(m_ConnectionString);
                objCon.Open();
                objTrans = objCon.BeginTransaction();

                //get NoteTypeCode
                m_NoteTypeCodeId = GetNoteTypeCodeId(); 

                //Delete old records
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = "DELETE FROM CLAIM_PRG_NOTE WHERE NOTE_TYPE_CODE = " + m_NoteTypeCodeId.ToString();
                Dictionary<string, object> dictQParams = new Dictionary<string, object>();
                DbFactory.ExecuteNonQuery(objCmd, dictQParams);

                //Importing Comments
                CopyRMXComments(); //Copy Comments from Comments_Text Table
               // CopyRMWorldComments(); //Copy Comments from Claim and Event Table

                prgBrNotesSave.Value = prgBrNotesSave.Maximum;

                objTrans.Commit();
                setStatus("Comments to Enhanced notes migration completed.");
                writeLog("Comments to Enhanced notes migration completed.");
            
                objCon.Close();
            }
            catch (Exception ex)
            {
                rtfStatus.Clear();
                if (objTrans != null) objTrans.Rollback();
                string sError = string.Format("Error: {0} CallStatck: {1}", ex.Message, ex.StackTrace.ToString());
                writeLog(sError + "\n");
                rtfStatus.Text = sError;
               
                rtfStatus.AppendText("\n \t"+" Error! No comments were migrated....");
            }
        }

        private void wizard1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// wizard1_WizardPageChanging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            if (!String.IsNullOrEmpty(m_strSelectedDSN))
            {
                lblDatabase.Text = m_strSelectedDSN;
                Application.DoEvents();
            }

            //zmohammad 10/15/12
            rtfStatus.Clear();
            prgBrNotesSave.Value = 0;
            //navigating forward from database selection screen
            if (e.OldPage == wpDSNSelection && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //do not change page if no database has been selected
                if (lvDatabases.SelectedItems.Count == 0)
                {
                    //lblWarningWPDS.Visible = true;
                    e.Cancel = true;
                }
                else
                {
                    //Get the selected DSN information
                    GetDSNSelections();
                    btnStart.Enabled = true;
                } // else
            }//if            

        }

        /// <summary>
        /// Event Handled for when the Cancel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_CancelButtonClick(object sender, CancelEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// Event Handled for when the Finish button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizCommentsMigration_FinishButtonClick(object sender, CancelEventArgs e)
        {
            //Close the form
            Close();

            //Remove the application from memory
            System.Windows.Forms.Application.Exit();
        }

        #endregion

        #region CopyRMXComments for newer version.

        /// <summary>
        /// Copy Comments from Comments_Text Table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyRMXComments()
        {
            try
            {
                //getting new value for primary index    
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = "SELECT MAX(CL_PROG_NOTE_ID) +1 FROM CLAIM_PRG_NOTE";
                Dictionary<string, object> dictQParams = new Dictionary<string, object>();
                DbReader objDbReader = DbFactory.ExecuteReader(objCmd, dictQParams);
                objDbReader.Read();
                if (!objDbReader.IsDBNull(0))
                    m_iNoteId = objDbReader.GetInt32(0);

                objDbReader.Close();
                
                if (m_iNoteId == 0)
                    m_iNoteId = 1;

                //Get the count of comments
                string sSQL = "SELECT COUNT(*) FROM COMMENTS_TEXT";
                int iCommentsCount = int.Parse(DbFactory.ExecuteScalar(m_ConnectionString, sSQL).ToString());

                //loading comments attached to events
                prgBrNotesSave.Maximum = iCommentsCount;
                rtfStatus.AppendText("Loading comments attached to events...  \n");


                //preparing the dbWriter
                m_EnhcNotesdbWriter = DbFactory.GetDbWriter(objCon);
                m_EnhcNotesdbWriter.Tables.Add("CLAIM_PRG_NOTE");
                m_EnhcNotesdbWriter.Fields.Add("CL_PROG_NOTE_ID", 0);
                m_EnhcNotesdbWriter.Fields.Add("CLAIM_ID", "");
                m_EnhcNotesdbWriter.Fields.Add("EVENT_ID", "");
                m_EnhcNotesdbWriter.Fields.Add("DATE_ENTERED", "");
                m_EnhcNotesdbWriter.Fields.Add("DATE_CREATED", "");
                m_EnhcNotesdbWriter.Fields.Add("TIME_CREATED", "");
                m_EnhcNotesdbWriter.Fields.Add("NOTE_TYPE_CODE", m_NoteTypeCodeId.ToString());
                m_EnhcNotesdbWriter.Fields.Add("NOTE_MEMO", "");
                m_EnhcNotesdbWriter.Fields.Add("NOTE_MEMO_CARETECH", "");
                m_EnhcNotesdbWriter.Fields.Add("ENTERED_BY_NAME", "");


                //Retrieve the the COMMENTS for processing
                rtfStatus.AppendText("Retrieving comments for processing...  \n");
                int iOriginalNoteId = m_iNoteId;
                int iCommentsIndex = 0;
                sSQL = "SELECT ATTACH_TABLE, ATTACH_RECORDID, COMMENTS, HTMLCOMMENTS FROM COMMENTS_TEXT ORDER BY ATTACH_TABLE, ATTACH_RECORDID";
                using (DbReader oReader = DbFactory.ExecuteReader(m_ConnectionString, sSQL))
                {
                    while (oReader.Read())
                    {
                        Application.DoEvents();

                        string sAttachTable = oReader["ATTACH_TABLE"].ToString();
                        string sAttachRecordId = oReader["ATTACH_RECORDID"].ToString();
                        string sComments = oReader["COMMENTS"].ToString();
                        string sHTMLComments = oReader["HTMLCOMMENTS"].ToString();

                        iCommentsIndex++;
                        prgBrNotesSave.Value = iCommentsIndex;
                        txtProcessingNow.Text = sAttachTable;
                        txtNotesCount.Text = (m_iNoteId - iOriginalNoteId + 1).ToString();

                        txtCommentCount.Text = string.Concat(iCommentsIndex
                            , " / "
                            , iCommentsCount);

                        ConvertComments2Note(sAttachTable, sAttachRecordId, sComments, sHTMLComments);

                    }
                }
                m_CommentsCount = iCommentsIndex;
                btnStart.Enabled = false;

                //Set EVENT_ID values for those records where EVENT_ID=0 or null
                if (objCon.ConnectionType == eConnectionType.ManagedSqlServer)
                {
                    sSQL = "UPDATE CLAIM_PRG_NOTE SET EVENT_ID = CLAIM.EVENT_ID " +
                            "FROM CLAIM_PRG_NOTE INNER JOIN CLAIM " +
                            "ON CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID " +
                            "WHERE CLAIM_PRG_NOTE.EVENT_ID = 0 OR CLAIM_PRG_NOTE.EVENT_ID IS NULL";
                }
                else
                {
                    sSQL = "UPDATE CLAIM_PRG_NOTE SET EVENT_ID = " +
                            " (SELECT CLAIM.EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = CLAIM_PRG_NOTE.CLAIM_ID)" +
                            " WHERE (CLAIM_PRG_NOTE.EVENT_ID = 0 OR CLAIM_PRG_NOTE.EVENT_ID IS NULL)";
                }

                rtfStatus.AppendText("Saving notes... \n");
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = sSQL;
                int iCount = DbFactory.ExecuteNonQuery(objCmd, new Dictionary<string, string>());

                // Updating Glossary table
                writeLog("Updating Glossary table with NextUniqueValue");
                UpdateGlossary();
            }
            catch (Exception ex)
            {
                rtfStatus.Clear();
                string sError = string.Format("Error: {0} CallStatck: {1}", ex.Message, ex.StackTrace.ToString());
                writeLog(sError);
                rtfStatus.AppendText(sError);
                rtfStatus.AppendText("\n \t"+" Error! No comments were migrated....");
            }
        }

        /// <summary>
        /// Process each comments and convert it to Enahanced Notes
        /// </summary>
        /// <param name="sAttachTable">EVENT or CLAIM</param>
        /// <param name="sAttachRecordID">EVENT_ID or CLAIM_ID</param>
        /// <param name="sComments">text of Comments</param>
        /// <param name="sHtmlComments">HTML content of Comments</param>
        /// <param name="oDS">DataSet object</param>
        private void ConvertComments2Note(string sAttachTable, string sAttachRecordID, string sComments, string sHtmlComments)
        {
            string sSQL = string.Empty;
            MatchCollection MxComment, MxHTMLComment;
            int matchCount = 0;
            int index = 0;
            int length = 0;
            int HTMLindex = 0;
            int HTMLlength = 0;

            string sNotesText = string.Empty;
            string sNotesHtml = string.Empty;
            int startLoc = 0;
            int startHTMLLoc = 0;
            int endLoc = 0;
            int endHTMLLoc = 0;
            DateTime dtDateTimeTemp;
            string sDateTimeTemp = string.Empty;
            string sNotesDate = string.Empty;
            string sNotesTime = string.Empty;

            if (string.IsNullOrEmpty(sComments))
                return;

            MxComment = RexDtStamp.Matches(sComments);//,  RegexOptions.IgnoreCase | RegexOptions.Compiled);
            MxHTMLComment = RexDtStamp.Matches(sHtmlComments);//, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

            matchCount = MxComment.Count;

            // If in case a portion of text within the datetime stamp is some formatting attached
            // the same would otherwise fail the regex pattern matching. 
            // ignoring the markup in this case.
            if (MxComment.Count != MxHTMLComment.Count)
            {
                MxHTMLComment = MxComment;
                sHtmlComments = sComments;
            }

            if (matchCount > 0)
            {
                for (int j = 0; j < matchCount; j++)
                {
                    if (m_dateFormat.Equals("YYYY/mm/dd"))
                    {
                        sDateTimeTemp = string.Format(@"{0}/{1}/{2} {3} {4}"
                        , MxComment[j].Groups[1].ToString()
                        , MxComment[j].Groups[3].ToString()
                        , MxComment[j].Groups[4].ToString()
                        , MxComment[j].Groups[5].ToString()
                        , MxComment[j].Groups[6].ToString());
                    }
                    else if (m_dateFormat.Equals("dd/mm/YYYY"))
                    {
                        sDateTimeTemp = string.Format(@"{0}/{1}/{2} {3} {4}"
                        , MxComment[j].Groups[1].ToString()
                        , MxComment[j].Groups[2].ToString()
                        , MxComment[j].Groups[3].ToString()
                        , MxComment[j].Groups[5].ToString()
                        , MxComment[j].Groups[6].ToString());
                    }
                    else if (m_dateFormat.Equals("mm/dd/YYYY"))
                    {
                        sDateTimeTemp = string.Format(@"{0}/{1}/{2} {3} {4}"
                        , MxComment[j].Groups[1].ToString()
                        , MxComment[j].Groups[2].ToString()
                        , MxComment[j].Groups[3].ToString()
                        , MxComment[j].Groups[5].ToString()
                        , MxComment[j].Groups[6].ToString());
                    }
                    
                    bool bDateTimeParse = DateTime.TryParse(sDateTimeTemp, out dtDateTimeTemp);
                    if (bDateTimeParse)
                    {
                        sNotesDate = Conversion.ToDbDate(dtDateTimeTemp);
                        sNotesTime = Conversion.ToDbTime(dtDateTimeTemp);
                    }

                    index = MxComment[j].Index;
                    length = MxComment[j].Length;

                    HTMLindex = MxHTMLComment[j].Index;
                    HTMLlength = MxHTMLComment[j].Length;

                    //this extracted Uid cotains the enclosing backets as well so removing the backets.
                    strEnteredBy = MxComment[j].Groups[7].ToString();
                    if (strEnteredBy.Length > 2)
                        strEnteredBy = strEnteredBy.Substring(1, strEnteredBy.Length - 2);

                    if (j == 0 & index > 3)
                    {
                        // for the scenario where there is some text added before the first date-time stamp 
                        //( Date-time stamp was enabled). In this case,
                        // we don't know the Date/Time stamp and Enterby user name            
                        sNotesText = sComments.Substring(0, index).Trim();
                        sNotesHtml = sHtmlComments.Substring(0, HTMLindex).Trim();

                        sNotesText = RexLineBreaks.Replace(sNotesText, "").Trim(m_Breaks);
                        if (sNotesText.Length > 0)
                        {
                            InsertNotes(sAttachTable, sAttachRecordID, sNotesText, sNotesHtml, string.Empty, string.Empty, string.Empty);
                        }
                    }
                    startLoc = index + length;
                    startHTMLLoc = HTMLindex + HTMLlength;

                    if (j != matchCount - 1)
                    {
                        endLoc = MxComment[j + 1].Index - startLoc;
                        endHTMLLoc = MxHTMLComment[j + 1].Index - startHTMLLoc;
                    }
                    else
                    {
                        endLoc = sComments.Length - startLoc;
                        endHTMLLoc = sHtmlComments.Length - startHTMLLoc;
                    }

                    sNotesText = sComments.Substring(startLoc, endLoc).Trim();
                    sNotesHtml = sHtmlComments.Substring(startHTMLLoc, endHTMLLoc).Trim();

                    if (sNotesHtml.Length == 0) sNotesHtml = sNotesText;

                    sNotesText = RexLineBreaks.Replace(sNotesText, "").Trim(m_Breaks);
                    if (sNotesText.Length > 0)
                    {
                        InsertNotes(sAttachTable, sAttachRecordID, sNotesText, sNotesHtml, sNotesDate, sNotesTime, strEnteredBy);
                    }
                }
            }
            else
            {
                // scenario where dateTimeStamps are disabled.
                // In this case, directly copying all text from comments into a single row in enhc Notes.                            
                sNotesDate = m_nowDateTime;
                sNotesText = sComments;
                sNotesHtml = sHtmlComments;
                sNotesTime = m_nowTime;
                strEnteredBy = ConfigurationManager.AppSettings["LoginName"];
                strUid = ConfigurationManager.AppSettings["UserID"];
                strGroupID = ConfigurationManager.AppSettings["GroupID"];
                sNotesText = RexLineBreaks.Replace(sNotesText, "").Trim(m_Breaks);
                if (sNotesText.Length > 0)
                {
                    InsertNotes(sAttachTable, sAttachRecordID, sNotesText, sNotesHtml, sNotesDate, sNotesTime, strEnteredBy);
                }
            }
        }

        /// <summary>
        /// Inset Notes in Enhanced Notes Table
        /// </summary>
        /// <param name="sAttachTable"></param>
        /// <param name="sAttachRecordId"></param>
        /// <param name="sTextNote"></param>
        /// <param name="sHtmlNote"></param>
        /// <param name="sDate"></param>
        /// <param name="sTime"></param>
        /// <param name="sEnteredBy"></param>
        private void InsertNotes(string sAttachTable, string sAttachRecordId, string sTextNote, string sHtmlNote, string sDate, string sTime, string sEnteredBy)
        {
            string sSQL = string.Empty;
            string sEventId = "0";
            string sClaimId = "0";
            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            if (string.Compare(sAttachTable, "EVENT", true) == 0)
            {
                sEventId = sAttachRecordId;
            }
            else
            {
                sClaimId = sAttachRecordId;
            }

            m_EnhcNotesdbWriter.Fields["CL_PROG_NOTE_ID"].Value = m_iNoteId;
            m_EnhcNotesdbWriter.Fields["CLAIM_ID"].Value = sClaimId;
            m_EnhcNotesdbWriter.Fields["EVENT_ID"].Value = sEventId;
            m_EnhcNotesdbWriter.Fields["DATE_ENTERED"].Value = sDate;
            m_EnhcNotesdbWriter.Fields["DATE_CREATED"].Value = sDate;
            m_EnhcNotesdbWriter.Fields["TIME_CREATED"].Value = sTime;
            m_EnhcNotesdbWriter.Fields["NOTE_TYPE_CODE"].Value = m_NoteTypeCodeId;
            m_EnhcNotesdbWriter.Fields["NOTE_MEMO"].Value = sHtmlNote;
            m_EnhcNotesdbWriter.Fields["NOTE_MEMO_CARETECH"].Value = sTextNote;
            m_EnhcNotesdbWriter.Fields["ENTERED_BY_NAME"].Value = sEnteredBy;

            m_EnhcNotesdbWriter.Execute(objTrans);

            m_iNoteId++;
        }    

        /// <summary>
        /// update NextUniqueValue in Glossary Table
        /// </summary>
        private void UpdateGlossary()
        {
            int iNoteId = 0, iCodeId = 0;
            try
            {
                //updating next_unique_id for enhanced Notes  
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = "SELECT MAX(CL_PROG_NOTE_ID) +1 FROM CLAIM_PRG_NOTE";
                DbReader objReader = DbFactory.ExecuteReader(objCmd, new Dictionary<string, string>());
                objReader.Read();
                iNoteId = objReader.GetInt32(0);
                objReader.Close();
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CLAIM_PRG_NOTE')", iNoteId);
                DbFactory.ExecuteNonQuery(objCmd, new Dictionary<string, string>());

                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = "SELECT MAX(CODE_ID) +1 FROM CODES";
                objReader = DbFactory.ExecuteReader(objCmd, new Dictionary<string, string>());
                objReader.Read();
                iCodeId = objReader.GetInt32(0);
                objReader.Close();
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CODES')", iCodeId);
                DbFactory.ExecuteNonQuery(objCmd, new Dictionary<string, string>());
            }
            catch (Exception exp)
            {
                rtfStatus.Clear();
                writeLog(string.Format("ERROR:[Error updating glossary table ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
                //string sError = string.Format("ERROR:[Error updating glossary table ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString());
                //rtfStatus.AppendText(sError);
                //rtfStatus.AppendText("\n \t" + " Error! No comments were migrated....");
            }

            //dispose objects

        }

        #endregion

        #region CopyRMWorldComments for older version

        /// <summary>
        /// Copy Comments from Claim and Event Table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyRMWorldComments()
        {
            try
            {
                btnStart.Enabled = false;
                //preparing the enhc Notes dataset.
                //doing an extra table count check; basically usefull in debugging scenario only
                if (AppGlobals.m_dsEnhNotes.Tables.Count == 0)
                {
                    AppGlobals.m_dsEnhNotes.Tables.Add("ENHNOTES");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("EVENTID");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("EVENTNUMBER");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("DATE");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("TEXT");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("HTML_TEXT");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("CLAIMID");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("CLAIMNUMBER");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("TIME");
                    AppGlobals.m_dsEnhNotes.Tables[0].Columns.Add("ENTERED_BY_NAME");
                }


                //preparing the dbWriter 
                m_EnhcNotesdbWriter = DbFactory.GetDbWriter(objCon);
                m_EnhcNotesdbWriter.Tables.Add("CLAIM_PRG_NOTE");
                m_EnhcNotesdbWriter.Fields.Add("CL_PROG_NOTE_ID", 0);
                m_EnhcNotesdbWriter.Fields.Add("EVENT_ID", "");
                m_EnhcNotesdbWriter.Fields.Add("NOTE_MEMO", "");
                m_EnhcNotesdbWriter.Fields.Add("NOTE_MEMO_CARETECH", "");
                m_EnhcNotesdbWriter.Fields.Add("CLAIM_ID", "");
                m_EnhcNotesdbWriter.Fields.Add("DATE_ENTERED", "");
                m_EnhcNotesdbWriter.Fields.Add("DATE_CREATED", "");
                m_EnhcNotesdbWriter.Fields.Add("TIME_CREATED", "");
                m_EnhcNotesdbWriter.Fields.Add("ENTERED_BY", "");
                m_EnhcNotesdbWriter.Fields.Add("ENTERED_BY_NAME", "");
                m_EnhcNotesdbWriter.Fields.Add("USER_TYPE_CODE", "");
                m_EnhcNotesdbWriter.Fields.Add("NOTE_TYPE_CODE", m_NoteTypeCodeId.ToString());

                //getting new value for primary index 
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                objCmd.CommandText = "SELECT MAX(CL_PROG_NOTE_ID) +1 FROM CLAIM_PRG_NOTE";
                DbReader objReader = DbFactory.ExecuteReader(objCmd, new Dictionary<string, string>());
                objReader.Read();
                m_iNoteId = objReader.GetInt32(0);
                objReader.Close();

                loadLOBinfo();

                //loading comments attached to events
                sqlLoadComment = "SELECT  EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL ";
                txtProcessingNow.Text = "Events";
                setProgressStatus(string.Format("Progress for phase 1 of {0}", m_LOBCount / 2 + 1));
                loadComments(sqlLoadComment, txtProcessingNow.Text);
                // extract enhc notes here....
                ExtractNotes();
                m_strDisplay = 1;


                int i = 0;
                for (i = 0; i < m_LOBCount / 2; i++)
                {
                    txtProcessingNow.Text = string.Format("Claims ({0})", m_LOBinfo[i * 2 + 1]);
                    setProgressStatus(string.Format("Progress for phase {0} of {1}", Convert.ToString(i + 1), m_LOBCount / 2 + 1));
                    sqlLoadComment = string.Format("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL  AND LINE_OF_BUS_CODE = {0}", m_LOBinfo[i * 2]);
                    loadComments(sqlLoadComment, txtProcessingNow.Text);
                    m_strDisplay = 2;

                    // extracting enhc notes ....                
                    ExtractNotes();

                    // adding up Lob Codes for 
                    sbSqlLOBGroup.Append(m_LOBinfo[i * 2].ToString());
                    sbSqlLOBGroup.Append(",");

                }

                // for claims which might now be associated with any LOB
                if (m_LOBCount > 1)
                {
                    txtProcessingNow.Text = "Claims (unassociated)";
                    setProgressStatus(string.Format("Progress for phase {0} of {0}", m_LOBCount / 2 + 1));
                    sqlLoadComment = string.Format("SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL  AND LINE_OF_BUS_CODE NOT IN({0})", sbSqlLOBGroup.ToString().Substring(0, sbSqlLOBGroup.ToString().Length - 1));
                    loadComments(sqlLoadComment, txtProcessingNow.Text);
                    ExtractNotes();

                    // extract enhc notes here....            
                }
                prgBrNotesSave.Value = 150;

                btnStart.Enabled = false;

                // Updating Glossary table
                writeLog("Updating Glossary table with NextUniqueValue");
                UpdateGlossary();

            }
            catch (Exception ex)
            {
                rtfStatus.Clear();
                // if (objTrans != null) objTrans.Rollback();
                string sError = string.Format("Error: {0} CallStatck: {1}", ex.Message, ex.StackTrace.ToString());
                writeLog(sError);
                rtfStatus.AppendText(sError);
                rtfStatus.AppendText("\n \t" + " Error! No comments were migrated....");
            }

            // ("comments to Enhanced notes migration completed.");
        }

        public void loadLOBinfo()
        {
            int i = 0;
          //  string sqlNotes = "SELECT C.CODE_ID,CT.CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID IN ( SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) LIKE '%LINE_OF_BUSINESS%' ) ";
         //   DbReader objDbReader;

            objCmd = objCon.CreateCommand();
            objCmd.Transaction = objTrans;
            objCmd.CommandText = "SELECT C.CODE_ID,CT.CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID IN ( SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) LIKE '%LINE_OF_BUSINESS%' )";
            DbReader objReader = DbFactory.ExecuteReader(objCmd, new Dictionary<string, string>());
            
            
          //  m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);
         //   objDbReader = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sqlNotes);
            while (objReader.Read())
                if (!objReader.IsDBNull(0))
                {
                    m_LOBinfo[i] = Convert.ToString(objReader.GetInt32(0));
                    m_LOBinfo[i + 1] = objReader.GetString(1);
                    i += 2;
                }
            // m_LOBCount would have twice the number of LOB the actualy number of LOB's
            // their  Code & code_desc are stored in consecutive values.
            m_LOBCount = i;
            objReader.Close();

        }

        private void loadComments(string p_sqlLoadComment, string p_ProcessingNow)
        {
            DbConnection objLocalCon = DbFactory.GetDbConnection(m_ConnectionString);
            objLocalCon.Open();
            writeLog(string.Format("Loading comments for: {0}", p_ProcessingNow));
            setStatus(string.Format("Loading comments for: {0}", p_ProcessingNow));
            try
            {

                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(objLocalCon, p_sqlLoadComment);
                AppGlobals.m_dsComments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsComments);
            }
            catch (Exception exp)
            {
                rtfStatus.Clear();
                writeLog(string.Format("ERROR:[Load Comments] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
                //string sError = string.Format("ERROR:[Load Comments] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString());
                //rtfStatus.AppendText(sError);
                //rtfStatus.AppendText("\n \t" + " Error! No comments were migrated....");
            }
            finally
            {
                if (objLocalCon.State != ConnectionState.Closed) objLocalCon.Close();
            }
            writeLog(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            setStatus(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            m_CommentsCount += AppGlobals.m_dsComments.Tables[0].Rows.Count;
            txtCommentCount.Text = m_CommentsCount.ToString();
            writeLog(string.Format("{0} comments loaded so far", txtCommentCount.Text));

        }

        /// <summary>
        /// Extracting Notes from comments/event dataset
        /// </summary>
        private void ExtractNotes()
        {
            prgBrNotesSave.Value = 0;
            MatchCollection MxComment, MxHTMLComment;
            string CommentLine = string.Empty, HTMLCommentLine = string.Empty;
            string EventId = string.Empty, EventNumber = string.Empty;
            string ClaimId = string.Empty, ClaimNumber = string.Empty;
            string Comment = string.Empty, HTMLComment = string.Empty;
            string DateTimeStamp = string.Empty,  strTime = string.Empty, sDateTimeTemp = string.Empty;
           // strEnteredBy = string.Empty;
            DateTime dtDateTimeTemp;
            bool bDateTimeParse = false;
            string strUid = string.Empty, strGroupID = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            string sCommentTemp = string.Empty;

            int matchCount = 0, index = 0, length = 0, HTMLindex = 0, HTMLlength = 0;
            int i = 0, j = 0;
            //int iYear=0, iMonth=0, iDay=0;
            int startLoc = 0, endLoc = 0, startHTMLLoc = 0, endHTMLLoc = 0;
            //MessageBox.Show("extracting notes");
            writeLog("Extracting Notes from comments");

            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                CommentLine = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();

                //CommentLine = dataGridView1.Rows[i].Cells["COMMENTS"].Value.ToString();
                HTMLCommentLine = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();

                EventId = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_ID"].ToString();
                EventNumber = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_NUMBER"].ToString();
                if (m_strDisplay == 2)
                {
                    ClaimId = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_ID"].ToString();
                    ClaimNumber = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_NUMBER"].ToString();
                }
                else
                {
                    ClaimId = "";
                    ClaimNumber = "";
                }

                // Assuming that the "Htmlcomments" column contains the the same text 
                // as comments columns with just some extra HTML markup.  
                //MxComment = Regex.Matches(CommentLine, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                //MxHTMLComment = Regex.Matches(HTMLCommentLine, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                MxComment = RexDtStamp.Matches(CommentLine);//,  RegexOptions.IgnoreCase | RegexOptions.Compiled);
                MxHTMLComment = RexDtStamp.Matches(HTMLCommentLine);//, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                matchCount = MxComment.Count;

                // If in case a portion of text within the datetime stamp is some formatting attached
                // the same would otherwise fail the regex pattern matching. 
                // ignoring the markup in this case.
                if (MxComment.Count != MxHTMLComment.Count)
                {
                    MxHTMLComment = MxComment;
                    //RexDtStamp.Matches(HTMLCommentLine);// Regex.Matches(CommentLine, m_pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    HTMLCommentLine = CommentLine;
                }

                if (matchCount > 0)
                {
                    #region iterating thru all the matches within a comment text
                    for (j = 0; j < matchCount; j++)
                    {
                        //Comment = MxComment[j].Index.ToString();

                        //commenting as eventually we are only utilizing the "NOW" datetime

                        //DateTimeStamp = MxComment[j].Groups[3].ToString() + MxComment[j].Groups[1].ToString() + MxComment[j].Groups[2].ToString();
                        //int.TryParse(MxComment[j].Groups[3].ToString(), iYear);
                        //int.TryParse(MxComment[j].Groups[1].ToString(),iMonth);
                        //int.TryParse(MxComment[j].Groups[2].ToString(),iDay); 
                        sDateTimeTemp = string.Format(@"{0}/{1}/{2} {3} {4}", MxComment[j].Groups[1].ToString(), MxComment[j].Groups[2].ToString(), MxComment[j].Groups[3].ToString(), MxComment[j].Groups[5].ToString(), MxComment[j].Groups[6].ToString());
                        bDateTimeParse = DateTime.TryParse(sDateTimeTemp, out dtDateTimeTemp);
                        if (bDateTimeParse)
                        {
                            DateTimeStamp = Conversion.ToDbDate(dtDateTimeTemp);
                            strTime = Conversion.ToDbTime(dtDateTimeTemp);
                        }
                        else
                        {
                            DateTimeStamp = "";
                            strTime = "";
                        }

                        //DateTimeStamp = Conversion.ToDbDateTime(new DateTime(iYear, iMonth, iDay));
                        //strTime = MxComment[j].Groups[5].ToString();

                        //strTime = strTime.Replace(":", "");
                        index = MxComment[j].Index;
                        length = MxComment[j].Length;

                        HTMLindex = MxHTMLComment[j].Index;
                        HTMLlength = MxHTMLComment[j].Length;

                        //this extracted Uid cotains the enclosing backets as well so removing the backets.
                        strEnteredBy = MxComment[j].Groups[7].ToString();
                        if (strEnteredBy.Length > 2)
                            strEnteredBy = strEnteredBy.Substring(1, strEnteredBy.Length - 2);

                        if (j == 0 & index > 3)
                        {
                            // for the scenario where there is some text added before the first date-time stamp                                    

                            Comment = CommentLine.Substring(0, index).Trim();
                            HTMLComment = HTMLCommentLine.Substring(0, HTMLindex).Trim();

                            sCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                            if (sCommentTemp.Length > 0)
                                AppGlobals.m_dsEnhNotes.Tables[0].Rows.Add(EventId, EventNumber, DateTimeStamp, Comment, HTMLComment, ClaimId, ClaimNumber, strTime, strEnteredBy);//, strUid, strGroupID);                            
                        }
                        startLoc = index + length;
                        startHTMLLoc = HTMLindex + HTMLlength;

                        if (j != matchCount - 1)
                        {
                            endLoc = MxComment[j + 1].Index - startLoc;
                            endHTMLLoc = MxHTMLComment[j + 1].Index - startHTMLLoc;
                        }
                        else
                        {
                            endLoc = CommentLine.Length - startLoc;
                            endHTMLLoc = HTMLCommentLine.Length - startHTMLLoc;
                        }

                        Comment = CommentLine.Substring(startLoc, endLoc).Trim();
                        HTMLComment = HTMLCommentLine.Substring(startHTMLLoc, endHTMLLoc).Trim();

                        if (HTMLComment.Length == 0) HTMLComment = Comment;

                        sCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                        if (sCommentTemp.Length > 0)
                            AppGlobals.m_dsEnhNotes.Tables[0].Rows.Add(EventId, EventNumber, DateTimeStamp, Comment, HTMLComment, ClaimId, ClaimNumber, strTime, strEnteredBy);//, strUid, strGroupID);

                    }
                    #endregion

                    //MessageBox.Show("For Row:- "+i.ToString() + " \ndatetime stamp and user for the first attached comment:-\n" + RxComment.Groups[0].ToString() );
                    //AppGlobals.m_dsEnhNotes.Tables[0].Rows.Add(EventId, EventNumber, RxComment.Groups[0].ToString());                            
                }
                else
                {
                    // scenario where dateTimeStamps are disabled.
                    // In this case, directly copying all text from comments into a single row in enhc Notes.                            
                    DateTimeStamp = m_nowDateTime;
                    Comment = CommentLine;
                    HTMLComment = HTMLCommentLine;
                    strTime = m_nowTime;


                  //  strEnteredBy = AppGlobals.Userlogin.LoginName.ToString();
                  //  strUid = AppGlobals.Userlogin.UserId.ToString();
                  //  strGroupID = AppGlobals.Userlogin.GroupId.ToString();
                    sCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);

                    if (sCommentTemp.Length > 0)
                        AppGlobals.m_dsEnhNotes.Tables[0].Rows.Add(EventId, EventNumber, DateTimeStamp, Comment, HTMLComment, ClaimId, ClaimNumber, strTime, strEnteredBy);//, strUid, strGroupID);                   

                }

                if (i % 50 == 0)
                {
                    //writing to log progress afetr every 50 comments have been processed.                    
                    setStatus("Saving notes....");
                    
                }
                if (i % 10 == 0)
                {
                    //writing the notes count to log file
                    writeLog(string.Format("{0} Notes extracted so far.", txtNotesCount.Text));
                    
                }
                // saving the extracted enhc Notes here.                
                //Thread.Sleep(100);
                Application.DoEvents();
                saveNotes();
                prgBrNotesSave.Value = (i + 1) * 100 / AppGlobals.m_dsComments.Tables[0].Rows.Count;

                AppGlobals.m_dsEnhNotes.Tables[0].Clear();


            }
            //a small value might be left out at the end of the progress bar, so setting ti to 100 as we have already processed all the comments
            //prgBrNotesSave.Value = 100;
        }

        /// <summary>
        /// saving individual Notes into DB
        /// </summary>
        private void saveNotes()
        {

            try
            {
                int iRowCount = 0, iEnhNotesCount, iPercentCompleted;
                bool bDoSkip = false; // to be removed after dbupgrade patch
                iEnhNotesCount = AppGlobals.m_dsEnhNotes.Tables[0].Rows.Count;
                string strEnteredByName = string.Empty;
                int iUid = -1, iGroupID = -1;
              
                // string strUid = string.Empty, strGroupID = string.Empty;
                //strEnteredBy = ConfigurationManager.AppSettings["LoginName"];
                //strUid = ConfigurationManager.AppSettings["UserID"];
                //strGroupID = ConfigurationManager.AppSettings["GroupID"];

                //writeLog("Starting Enhc Notes save operation.");

                for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
                {
                    //iRowCount = p_rowNum;                

                    //adding values for each enh notes row.           
                    m_EnhcNotesdbWriter.Fields["CL_PROG_NOTE_ID"].Value = m_iNoteId.ToString();
                    m_iNoteId++;
                    m_EnhcNotesdbWriter.Fields["EVENT_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString();

                    /*    
                        bDoSkip = false;
                        if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Length > 3900)
                        {
                            MessageBox.Show(string.Format("[Text]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the text\nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
                            AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3] = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString().Substring(0, 3900);
                            bDoSkip = true;
                        }
                     */
                        m_EnhcNotesdbWriter.Fields["NOTE_MEMO"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][3].ToString();

                    //if (AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Length>3900)
                    //{
                    //    MessageBox.Show(string.Format("[HTMLtext]Notes max char limit of 4000 char reached note associated for event [{0}]\nSkipping the HTMLtext \nthis will be removed via dbupgrade", AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][0].ToString()));
                    //    AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4]=AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString().Substring(0,3900);
                    //    bDoSkip = true;
                    //}

                        m_EnhcNotesdbWriter.Fields["NOTE_MEMO_CARETECH"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][4].ToString();
                    m_EnhcNotesdbWriter.Fields["CLAIM_ID"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][5].ToString();
                    m_EnhcNotesdbWriter.Fields["DATE_ENTERED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
                    m_EnhcNotesdbWriter.Fields["DATE_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();
                    //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][2].ToString();

                    // the above value can be used for saving the extracted date into the db instead of datetime.Now
                    // basically we are indeed extracting the datetime values from the comments and keeping them in dataset
                    // but eventually we are saving the datetime.now values for 
                    // both the "date created" & "activity date" fields on the frontend

                    // in case where the extracted datetime is to be used and additional check will also need to be 
                    // employed to make the length of digits for each of the month & day fixed.
                    // for e.g if the number of digits in day decrease below 2 (i.e. when date is before the 10th)
                    // there should be a preceding '0' padded to it.


                    m_EnhcNotesdbWriter.Fields["TIME_CREATED"].Value = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
                    //System.DateTime.Now.TimeOfDay.ToString().Replace(":", "");
                    // AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][7].ToString();
                    strEnteredByName = AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();
                    m_EnhcNotesdbWriter.Fields["ENTERED_BY_NAME"].Value = strEnteredByName;

                    //iUid = getUid(strEnteredByName);

                    //iUid = Convert.ToInt32(strUid);
                    //if (iUid == -1)
                    //{
                    //    strUid = string.Empty;
                    //    strGroupID = string.Empty;
                    //}
                    //else
                    //{
                    //    strUid = iUid.ToString();
                    //    iGroupID = Convert.ToInt32(strGroupID);
                    //    if (iGroupID != -1) strGroupID = iGroupID.ToString();
                    //}
                    //m_EnhcNotesdbWriter.Fields["ENTERED_BY"].Value = strUid;
                    //m_EnhcNotesdbWriter.Fields["USER_TYPE_CODE"].Value = strGroupID;
                    //AppGlobals.m_dsEnhNotes.Tables[0].Rows[iRowCount][8].ToString();
                    //m_EnhcNotesdbWriter.Fields["note_type_code"].Value = m_NoteTypeCodeId;                

                    try
                    {
                        if (!bDoSkip) m_EnhcNotesdbWriter.Execute(objTrans);
                    }
                    catch (Exception exp)
                    {
                        rtfStatus.Clear();
                        writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                        throw new Exception(exp.Message.ToString(), exp.InnerException);
                        string sError = string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString());
                        rtfStatus.AppendText(sError);
                        rtfStatus.AppendText("\n \t" + " Error! No comments were migrated....");
                    }

                    //changing status & progress bar info
                    //iPercentCompleted = Convert.ToInt32(((Convert.ToSingle(iRowCount) * 100.0) / Convert.ToSingle(iEnhNotesCount)));
                    //setStatus(iPercentCompleted.ToString() + " % save completed");                
                    //prgBrNotesSave.Value = iPercentCompleted;

                    //disposing
                    //objDbReader.Close();

                    //updating the counter
                    m_EnhancedNotes = Convert.ToInt64(txtNotesCount.Text) + 1;
                    txtNotesCount.Text = m_EnhancedNotes.ToString();


                }
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nOuterexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }

        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// LoadRiskmasterDatabases
        /// </summary>
        private void LoadRiskmasterDatabases()
        {
            try
            {
                using (DataTable dtDSNs = RiskmasterDatabase.GetRiskmasterDatabases())
                {
                    //added list view to stay consistent with dB Upgrade Wizard - prg 04/10/09
                    foreach (DataRow dtRow in dtDSNs.Rows)
                    {
                        ListViewItem lvItem = new ListViewItem { Text = dtRow["DSN"].ToString() };

                        lvItem.SubItems.Add(dtRow["DSNID"].ToString());
                        lvItem.SubItems.Add(dtRow["CONNECTION_STRING"].ToString());

                        lvDatabases.Items.Add(lvItem);
                    }//foreach
                }//using
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check your connectionStrings.config. Error: " + ex.Message, "Database Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        /// <summary>
        /// Gets the database information that the user selects
        /// </summary>
        private void GetDSNSelections()
        {
            const int DSN_ID = 1; //Populated as the value for the ListViewSubItem
            const int DSN_CONNECTION_STRING = 2; //Populated as the value for the ListViewSubItem

            //changed to retrieve from list view added - prg 04/10/09
            foreach (ListViewItem lvItem in lvDatabases.SelectedItems)
            {
                m_strSelectedDSN = lvItem.Text.ToString();
                lblDatabase.Text = m_strSelectedDSN;
                Application.DoEvents();

                //Get the individually selected DSN ID
                m_iDSNId = Convert.ToInt32(lvItem.SubItems[DSN_ID].Text.ToString());

                //Get the individually selected connection strings
                m_ConnectionString = lvItem.SubItems[DSN_CONNECTION_STRING].Text.ToString();
            }
        } // method: GetDSNSelections

        /// <summary>
        /// appends text to the RTF box
        /// </summary>
        /// <param name="p_status"></param>
        private void setStatus(string p_status)
        {
            //lblStatus.Text=p_status;
            rtfStatus.AppendText(p_status + "\n");
        }

        /// <summary>
        /// updates the progress bar text box (its just above the progress bar)
        /// </summary>
        /// <param name="p_status"></param>
        private void setProgressStatus(string p_status)
        {
            txtProgressStatusMessage.Text = p_status;
        }

        /// <summary>
        /// returns the Code Type of 'Comments2EnhNotes' if presents otherwise creates one
        /// </summary>
        /// <returns></returns>
        private int GetNoteTypeCodeId()
        {
            int iNodeTypeCodeId = -1, iCodeIdFromCodesTable = -1, iCodeIdFromCodesTextTable = -1, iNotesGlossaryTableId = -1;
            // checking to see if the 'C2N' code type already exits, in which case directly retunr its Code_id
            string sqlNotes = string.Format("SELECT C.CODE_ID FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID=CT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE') AND UPPER(CT.CODE_DESC) = '{0}' ", m_sEnhcNotesCodeDesc.ToUpper());
            DbReader objDbReader;

            object objobject = DbFactory.ExecuteScalar(m_ConnectionString, sqlNotes);

            //if (!objDbReader.IsDBNull(0))
            iNodeTypeCodeId = Convert.ToInt32(objobject);

            // if the Code type already exits then returning the same.  
            if (!(iNodeTypeCodeId == -1 || iNodeTypeCodeId == 0)) return iNodeTypeCodeId;

            //getting Table id for enhanced note to be put in codes table
            sqlNotes = "SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE'";

            objDbReader = DbFactory.GetDbReader(m_ConnectionString, sqlNotes);
            objDbReader.Read();
            if (!objDbReader.IsDBNull(0))
                iNotesGlossaryTableId = objDbReader.GetInt32(0);

            // TO DO : encapsulate the following block within a transaction

            // getting new code_id from codes table
            sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES ";

            objDbReader = DbFactory.GetDbReader(m_ConnectionString, sqlNotes);
            objDbReader.Read();
            if (!objDbReader.IsDBNull(0))
                iCodeIdFromCodesTable = objDbReader.GetInt32(0);

            // getting new code_id from codes_text table
            sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES_TEXT ";

            objDbReader = DbFactory.GetDbReader(m_ConnectionString, sqlNotes);
            objDbReader.Read();
            if (!objDbReader.IsDBNull(0))
                iCodeIdFromCodesTextTable = objDbReader.GetInt32(0);

            // normally the two tables (Codes & Codes_text would be in sync)... just in case
            // taking the maximum of the two for our Comments2EnhNotes codes type
            iNodeTypeCodeId = (iCodeIdFromCodesTable > iCodeIdFromCodesTextTable) ? iCodeIdFromCodesTable : iCodeIdFromCodesTextTable;


            //srajindersin to check for validation of non-unique constraint on CODES table
            sqlNotes = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + iNotesGlossaryTableId.ToString() + " AND SHORT_CODE = '" + m_sEnhcNotesShortCode + "' AND LINE_OF_BUS_CODE IS NULL";
            objDbReader = DbFactory.GetDbReader(m_ConnectionString, sqlNotes);
            if (objDbReader.Read())
            {
                iNodeTypeCodeId = objDbReader.GetInt32(0);
            }
            else
            {
                //saving our new Codes into the table
                try
                {
                    DbWriter objWriter;

                    // Saving into the codes table
                    objWriter = DbFactory.GetDbWriter(objCon);
                    objWriter.Tables.Add("CODES");
                    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
                    objWriter.Fields.Add("TABLE_ID", iNotesGlossaryTableId.ToString());
                    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
                    objWriter.Fields.Add("DELETED_FLAG", "0");
                    objWriter.Execute(objTrans);

                    // Saving into the codes_text table
                    //sqlNotes = "insert into codes_text (code_id,short_code,code_desc) values (9999, 'C2N','Comm2EnhNotes')";

                    // Saving into the codes table
                    objWriter = DbFactory.GetDbWriter(objCon);
                    objWriter.Tables.Add("CODES_TEXT");
                    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
                    objWriter.Fields.Add("LANGUAGE_CODE", m_iLanguageCode);
                    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
                    objWriter.Fields.Add("CODE_DESC", m_sEnhcNotesCodeDesc);
                    objWriter.Execute(objTrans);
                }
                catch
                {
                    DbWriter objWriter;

                    // Saving into the codes table
                    objWriter = DbFactory.GetDbWriter(objCon);
                    objWriter.Tables.Add("CODES");
                    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
                    objWriter.Fields.Add("TABLE_ID", iNotesGlossaryTableId.ToString());
                    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
                    objWriter.Fields.Add("DELETED_FLAG", "0");
                    objWriter.Fields.Add("LINE_OF_BUS_CODE", "0");
                    objWriter.Execute(objTrans);

                    // Saving into the codes_text table
                    //sqlNotes = "insert into codes_text (code_id,short_code,code_desc) values (9999, 'C2N','Comm2EnhNotes')";

                    // Saving into the codes table
                    objWriter = DbFactory.GetDbWriter(objCon);
                    objWriter.Tables.Add("CODES_TEXT");
                    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
                    objWriter.Fields.Add("LANGUAGE_CODE", m_iLanguageCode);
                    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
                    objWriter.Fields.Add("CODE_DESC", m_sEnhcNotesCodeDesc);
                    objWriter.Execute(objTrans);

                }
            }
            return iNodeTypeCodeId;
        }

        public static string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

        /// <summary>
        /// writeLog
        /// </summary>
        /// <param name="p_strLogText"></param>
        public static void writeLog(string p_strLogText)
        {
            //string sMessage = string.Format("[{0}] {1}"
            //    , System.DateTime.Now.ToString()
            //    , p_strLogText);
            string sMessage = string.Concat("["
                , System.DateTime.Now.ToString()
                , "] "
                , p_strLogText);
            try
            {
                m_sw1.WriteLine(sMessage);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }

        }

        #endregion
  
    }//class: frmCommentsMigration
}//namespace: Riskmaster.Tools.C2C
