﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BESScheduler;

namespace BESScheduler
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
            if (AppGlobals.bDisableControls)
            {
                txtUserName.Visible = false;
                lblUserName.Visible = false;
                lblPassword.Visible = false;
                txtPassword.Visible = false;
                lblMessage.Visible = false;
                this.Text = "Rebuild";

                this.Size = new Size(294, 150);

                this.chkBox1.Location = new Point(this.chkBox1.Location.X+40, 30);

                this.label1.Location = new Point(this.label1.Location.X+40, 30);

                this.btnOk.Location = new Point(this.btnOk.Location.X, 70);
                this.btnCancel.Location = new Point(this.btnCancel.Location.X, 70);
            }
        }
        internal delegate void ReturnEventHandler(string sUserName, string sPassword,bool bCheckvalue);
        internal event ReturnEventHandler ReturnValue;
        private void btnOk_Click(object sender, EventArgs e)
        {
            ReturnValue(txtUserName.Text, txtPassword.Text,chkBox1.Checked);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ReturnValue("", "",false);
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
