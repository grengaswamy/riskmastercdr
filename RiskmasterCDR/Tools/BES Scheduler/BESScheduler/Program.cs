﻿using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Common;
using System.Configuration;
using Riskmaster.Db;
using Riskmaster.Security.Encryption;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;


namespace BESScheduler
{
    /// <summary>
    /// This will run to create BES views,it can run from taskmanager as well as silently
    /// </summary>
    /// <remarks>*	The tool can either be run directly or can be executed via taskmanager in silent mode.
    /// Command line parameters-
    /// BESScheduler.exe uid pwd DSN adminUid,adminPwd</remarks>
    public class Program
    {
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static string m_sLoginPwd = string.Empty;
        static string m_sAdminUserName = string.Empty;
        static string m_sAdminPassword = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sDbConnstringGlobal = string.Empty;
        static string m_sAdminDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string m_sDefaultTableSpaceForOracle = string.Empty;
        static string m_sTempTableSpaceForOracle = string.Empty;

        static int m_iOrgSec;
        static int m_iBES ;
        static int m_iDeffered ;
        static int m_iConfRec; //pmittal5
        static int m_iClientId = 0;
        //rsushilaggar setting this flag true by default
        static bool m_bCheckvalue = true;
        static bool m_blnSuccess = false;
        
        static DbConnection m_objDbConnection = null;
        static DbConnection m_objDbConnectionGlobal = null;
        static DbConnection m_objSecDbConnection = null;
        static DbCommand m_objSecDbCommand = null;
        static DbCommand m_objCmd = null;
        static eDatabaseType m_sDatabaseType = 0;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            UserLogin oUserLogin = null; 
 
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            bool blnSuccess = false;

            try
            {

                //args = new string[7];
                //args[0] = "-dsrmACloud";
                //args[1] = "-rucloud";
                //args[2] = "0b54f3a9959b766e20a5fb1e46c05874";
                //args[3] = "admin";
                //args[4] = "admin";
                //args[5] = "-ci1";

                int argCount = (args == null) ? 0 : args.Length;

                //Add & Change by kuladeep for Cloud----Start
                if (args.Length > 0)
                {
                    GetParameters(args);//m_iClientId will populate from arguments for Cloud
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    m_iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by kuladeep for Cloud----End

                if (argCount > 0)//Entry from TaskManager
                {
                    //GetParameters(args);//Code commented by kuladeep for Cloud.
                        
                    //MITS 22996 Ignore password when called from TaskManager
                    oUserLogin = new UserLogin(m_sLoginName, m_sDataSource, m_iClientId);//Add & Change by kuladeep for Cloud
                    if (oUserLogin.DatabaseId <= 0)
                    {
                        throw new Exception("Authentication failure.");
                        //throw new System.IO.IOException("IO Exception");
                    }
                    Initalize(oUserLogin);
                    
                    //creating connection string using admin userid and password.
                    if (!string.IsNullOrEmpty(m_sAdminUserName) && !string.IsNullOrEmpty(m_sAdminPassword))
                    {
                        SetAdminParams(m_sAdminUserName, m_sAdminPassword);
                    }
                    else
                    {
                        m_sAdminDbConnstring = m_sDbConnstring;
                    }
                }
                else//Entry for direct run
                {
                    GetBesFlag(ref m_iOrgSec);
                    if (m_iOrgSec == -1)
                    {
                        Login objLogin = new Login(m_iClientId);//Add & Change by kuladeep for Cloud
                        bool bCon = false;

                        bCon = objLogin.RegisterApplication(0, 0, ref AppGlobals.Userlogin, m_iClientId);//Add & Change by kuladeep for Cloud
                        if (bCon)
                        {
                            Initalize(AppGlobals.Userlogin);
                                bool bAdmin = false;
                                bool bIsIn = false;
                               // bAdmin = IsLoginAdmin();
                                SetAdminParams(m_sAdminUserName, m_sAdminPassword);
                                while (!bAdmin)
                                {
                                    try
                                    {
                                        if (bIsIn==false)
                                        bAdmin = IsLoginAdmin();
                                        if (!bAdmin)
                                        {
                                            GetAdminUIdandPwd();
                                            SetAdminParams(m_sAdminUserName, m_sAdminPassword);
                                            bAdmin = IsLoginAdmin();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                        bAdmin = false;
                                        bIsIn = true;
                                        Initalize(AppGlobals.Userlogin);
                                    }
                                    
                                    //creating connection string using admin userid and password.
                                  //SetAdminParams(m_sAdminUserName, m_sAdminPassword);
                                    
                                }
                        
                        }
                        else
                        {
                        return;

                        }
                    }
                    else
                    {
                        return;
                    }
                    if (AppGlobals.bShowDialog)
                    {
                        AppGlobals.bDisableControls = true;
                        GetAdminUIdandPwd();
                    }
                }
                    
                OpenAdminDbConnection();               
                
                GetBesConfAndDeffFlag(Conversion.CastToType<int>(m_sDSNID, out m_blnSuccess), ref m_iBES, ref m_iDeffered, ref m_iConfRec);
                
                ProcessBESGroups();
                
            }
            catch (Exception exc)
            {
                //Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                smsg = exc.Message;
                FormatAndDisplayExcMsg(smsg);

                Log.Write(exc.Message, "CommonWebServiceLog",m_iClientId);

            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();

                }
                if (m_objDbConnectionGlobal != null)
                {
                    m_objDbConnectionGlobal.Close();
                    m_objDbConnectionGlobal.Dispose();

                }
                if (m_objSecDbConnection != null)
                {
                    m_objSecDbConnection.Close();
                    m_objDbConnection.Dispose();
                }
                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objSecDbCommand != null)
                    m_objSecDbCommand = null;

            }
        }

        private static void OpenAdminDbConnection()
        {
            m_objDbConnection = DbFactory.GetDbConnection(m_sAdminDbConnstring);
            m_objDbConnectionGlobal = DbFactory.GetDbConnection(m_sDbConnstringGlobal);
            m_objDbConnectionGlobal.Open();
            m_objDbConnection.Open();
            m_sDatabaseType = m_objDbConnection.DatabaseType;

            m_objSecDbConnection = DbFactory.GetDbConnection(AppGlobals.SecurityDSN(m_iClientId));
            m_objSecDbConnection.Open();
        }
        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            m_sDSNID = p_oUserLogin.DatabaseId.ToString();
            m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
            m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
           // m_sDbConnstringForTableSpace = m_sDbConnstring;
            m_sDbConnstringGlobal = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            try
            {
                m_objDbConnection.Open();
                m_sDatabaseType = m_objDbConnection.DatabaseType;
                m_objDbConnection.Close();
              if( m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
              {
                SetTableSpaceForOracle();
              }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                m_objDbConnection.Close();
            }

        }
        /// <summary>
        /// Set Table space for oracle
        /// </summary>
        
        private static void SetTableSpaceForOracle()
        {
           
            DbReader objDbReader = null;
            string sSql=string.Empty;

            try
            {
                  
                sSql = "SELECT DEFAULT_TABLESPACE,TEMPORARY_TABLESPACE FROM USER_USERS";

                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSql))
                {

                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            m_sDefaultTableSpaceForOracle = objDbReader.GetString("DEFAULT_TABLESPACE");
                            m_sTempTableSpaceForOracle = objDbReader.GetString("TEMPORARY_TABLESPACE");


                        }

                    }
                }
           }
       

            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }

        }

        /// <summary>
        /// Create Admin connection string.
        /// </summary>
        /// <param name="p_sAdminUserName">Admin DbUser Name</param>
        /// <param name="p_sAdminPassword">Admin DbUser Password</param>
        private static void SetAdminParams(string p_sAdminUserName, string p_sAdminPassword)
        {
            if (!string.IsNullOrEmpty(p_sAdminUserName) && !string.IsNullOrEmpty(p_sAdminPassword))
            {

                m_sDbConnstring = GetBaseConnectionString(m_sDbConnstring);
                m_sAdminDbConnstring = m_sDbConnstring + "UID=" + p_sAdminUserName + ";PWD=" + p_sAdminPassword + ";";

            }
            else
            {
                m_sAdminDbConnstring = m_sDbConnstring;
                m_sAdminUserName = m_sDBOUserId;
            }

         
        }
     
        private static void SetAdminUid( ref string p_sAdminUserName)
        {
            if ((string.IsNullOrEmpty(p_sAdminUserName)) || (string.IsNullOrEmpty(p_sAdminUserName)))
            {
                m_sAdminUserName = m_sDBOUserId;
            }
              
        }

        /// <summary>
        /// Get Base connection string by removing UID and PWD
        /// </summary>
        /// <param name="strConnectionString">Connection String</param>
        /// <returns>Connection String without UID and PWD </returns>
        private static string GetBaseConnectionString(string strConnectionString)
        {
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string GROUP_NAME = "CONN_STR_VALUE";

            string strMatchValue = string.Empty;

            //Get the Match for the string
            Match regExMatch = Regex.Match(strConnectionString, strRegExpr, RegexOptions.IgnoreCase);

            //Get the matching value for the specified group name
            strMatchValue = regExMatch.Groups[GROUP_NAME].Value;

            return strMatchValue;
        }

        /// <summary>
        /// set Global varriables with Admin UserId and Password 
        /// </summary>
        /// <param name="sUserName">Admin DB User</param>
        /// <param name="sPassword">Admin DB Password</param>
        /// <param name="bCheckvalue">True/False(True :build all the bes groups)</param>
        private static void objAdmin_ReturnValue(string sUserName, string sPassword, bool bCheckvalue)
        {
            if (!string.IsNullOrEmpty(sUserName) && !string.IsNullOrEmpty(sPassword))
            {
                m_sAdminUserName = sUserName;
                m_sAdminPassword = sPassword;
            }
            //rsushilaggar Setting this flag value true by default
            //m_bCheckvalue = bCheckvalue;
        }
        /// <summary>
        /// Checks the User for database access permissions. For SQL Server it 
        /// checks for dbo rola and for Oracle it checks for DBA role 
        /// </summary>
        /// <returns>True if User is dbo/DBA else otherwise</returns>
        private static bool IsLoginAdmin()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            string sOraBESRole = string.Empty;                          
            string strDbConnString = string.Empty;
            const string DB_OWNER_ROLE = "db_owner";
            const string SYSADMIN_ROLE = "sysadmin";
             bool m_bIsAdmin = false;

            try
            {
              
                if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {         
            //        //Check if the user is a member of the db_owner role
                   if (IsSQLRoleMember(DB_OWNER_ROLE))
                   {
                        m_bIsAdmin = true;                       
                    }
                   else
                   {
                       m_bIsAdmin = false;
                   }
                }
                else if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                                       
                    sSQL = "SELECT USERNAME,GRANTED_ROLE FROM USER_ROLE_PRIVS WHERE GRANTED_ROLE = 'DBA'";                   
                        objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, sSQL);
                    
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            if (objDbReader["USERNAME"].ToString().Trim().Length > 0)
                            {                                
                                m_bIsAdmin = true;                               
                            }
                        }
                        else
                            m_bIsAdmin = false;    
                       
                       
                    }

               
                }
                else if (m_sDatabaseType == eDatabaseType.DBMS_IS_DB2)
                {
                    m_bIsAdmin = true;
                }


                return m_bIsAdmin;
              
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }
        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLRoleMember(string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helprolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsDBO = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;       
            const string SECURITYADMIN_ROLE = "securityadmin";
            const string SYSADMIN_ROLE = "sysadmin";
            DbReader objDbReader = null;


            try
            {

                using (objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, SQL_ROLE_NAME))
                {

                    while (objDbReader.Read())
                    {
                        string strMemberName = objDbReader[MEMBER_NAME_COLUMN].ToString();
                        strUserName = m_sAdminUserName;
                        //in case of sa user , member name will be dbo
                        if (strUserName.ToLower() == "sa" && strMemberName.ToLower() == "dbo")
                        {
                            blnIsDBO = true;
                            break;
                        }
                        else
                        {

                            if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                            {
                                blnIsDBO = true;
                                break;
                            }
                        }
                    }
                }


                if (blnIsDBO)
                {
                    if (IsSQLServerRoleMember(SYSADMIN_ROLE))
                    {
                        blnIsDBO = true;
                    }
                    else if (IsSQLServerRoleMember(SECURITYADMIN_ROLE))
                    {
                        blnIsDBO = true;
                    }
                    else
                    {
                        blnIsDBO = false;
                    }
                }
               
                return blnIsDBO;
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }

        ///// <summary>
        ///// Determines whether the user is a member of a specified SQL
        ///// Server server-level role
        ///// </summary>     
        ///// <param name="strRoleName">string containing the name of the role to verify membership</param>
        ///// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLServerRoleMember(string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helpsrvrolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsSysAdmin = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;
            DbReader objDbReader = null;
            
            //Get a DataReader object
            try
            {
                using (objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, SQL_ROLE_NAME))
                {
                    while (objDbReader.Read())
                    {
                        string strMemberName = objDbReader[MEMBER_NAME_COLUMN].ToString();
                        strUserName = m_sAdminUserName;
                        if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                        {
                            blnIsSysAdmin = true;
                            break;
                        }
                    }
                }

                return blnIsSysAdmin;
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }

        /// <summary>
        /// Starting Point of calling of stored Procedure on the 
        /// basis of OrgSecflag and DefBesFlag
        /// </summary>
        private static void ProcessBESGroups()
        {
            if (m_iBES == -1 && m_iDeffered == 0)//it will run during installation (to change all SG account to new account) or he wants to rebuild all group
            {

                ProcessBESGroupsFirstTime();   
            }
            else if (m_iBES == -1 && m_iDeffered == -1)//it will run during add/edit group
            {
                ProcessBESGroupOnAddEdit();     

            }

            else if (m_iBES == 0 && m_iDeffered == 0)//it will run during disable BES
            {

                ProcessDisableBes(true);
                
            }
            else if (m_iBES == 0 && m_iDeffered == -1)//client is new and running first time
            {
                ProceeNewBESSetUp();
                
            }

            ProcessDeletedGroups();
            
        }

        /// <summary>
        /// Build Existing BES Groups in RMX R6  
        /// </summary>
        private static void ProcessBESGroupsFirstTime()
        {
            string sSQL = string.Empty;
            try
            {
                RebuildFirstTime();

                sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG = -1 WHERE SUPER_GROUP_ID IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_MAP.SUPER_GROUP_ID = ORG_SECURITY.GROUP_ID" +
                        " AND ORG_SECURITY.GROUP_ENTITIES NOT LIKE '%<ALL>%') AND DELETED_FLAG=0 ";
                RunSql(sSQL, m_objDbConnectionGlobal);
                sSQL = "UPDATE ORG_SECURITY SET UPDATED_FLAG=-1 WHERE GROUP_ENTITIES NOT LIKE '%<ALL>%' AND DELETED_FLAG=0";
                RunSql(sSQL, m_objDbConnectionGlobal);

                CallUSP();
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
            }

        }

        /// <summary>
        /// Process BES groups on Edit of existing group or Add new groups  
        /// </summary>
        private static void ProcessBESGroupOnAddEdit()
        {
            CallUSP();
        }

        /// <summary>
        /// Disable BES set up
        /// </summary>
        /// <param name="bDropTriggers"> True : if Full Disable</param>
        private static void ProcessDisableBes(bool bDropTriggers)
        {   
            
            string sSQL = string.Empty;
            string sUserNameNew = string.Empty;
            string sUserNameOld = string.Empty;
            StringBuilder sGroupIds = null;
            int iGroupId = 0;
            bool bDelete = false;
            DbReader objDbReader = null;
            
            try
            {
                //sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID > 0 AND DELETED_FLAG =-1 AND GROUP_ENTITIES NOT LIKE '%<ALL>%'"; //pmittal5
                sSQL = "SELECT GROUP_ID FROM GROUP_MAP WHERE GROUP_ID > 0 AND DELETED_FLAG =-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                {
                    if (objDbReader != null)
                    {
                        sGroupIds = new StringBuilder();
                        sGroupIds.Append("(");
                        while (objDbReader.Read())
                        {
                            iGroupId = objDbReader.GetInt32("GROUP_ID");
                            sUserNameOld = "sg" + string.Format("{0:00000}", iGroupId);
                            sUserNameNew = FormatBESUserName(Conversion.CastToType<int>(m_sDSNID, out m_blnSuccess), iGroupId);
                            USP_DisableBes(sUserNameNew, "");
                            sGroupIds.Append(iGroupId);
                            sGroupIds.Append(",");
                            bDelete = true;
                        }

                        sGroupIds.Remove(sGroupIds.Length - 1, 1); 
                        sGroupIds.Append( ")");
                    }

                }

                if (bDelete)
                {
                    //sSQL = "DELETE FROM ORG_SECURITY WHERE GROUP_ID IN " + sGroupIds;  //pmittal5
                    sSQL = "DELETE FROM GROUP_MAP WHERE GROUP_ID IN " + sGroupIds;
                    RunSql(sSQL, m_objDbConnectionGlobal);
                    if (bDropTriggers)
                    {//Delete all the remaining groups
                        sSQL = "DELETE FROM ORG_SECURITY ";
                        RunSql(sSQL, m_objDbConnectionGlobal);
                        sSQL = "DELETE FROM GROUP_MAP "; //pmittal5
                        RunSql(sSQL, m_objDbConnectionGlobal);
                        USP_DropTriggers();
                    }
                }

            }

            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }
                
            }
        }

        /// <summary>
        /// Process BES groups in case of new BES setup
        /// </summary>
        private static void ProceeNewBESSetUp()
        {
            CallUSP();
        }

        /// <summary>
        /// Process groups which have been deleted from UI.
        /// </summary>
        private static void ProcessDeletedGroups()
        {
            int iResult=0 ;
            int iDeletedGroupId=0 ;

            GetDeletedGroup(ref iResult, ref iDeletedGroupId);

            if (iResult == -1 && (!(m_iBES == 0 && m_iDeffered == 0)))
            {
                ProcessDisableBes(false);

            }
        }

        /// <summary>
        /// Rebuild UserID, Password for all the BES group in new format 
        /// using old metadata
        /// </summary>
        private static void RebuildFirstTime()
        {
            int iGroup_ID ;  
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sUID = string.Empty;
            string sPassword = string.Empty;
            string sOld_Uid = string.Empty;
            string sNewUid = string.Empty;
            string BES_COMPLEX_PASSWORD = "R1$km@5ter";          

            try
            {
                //sSQL = "SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_ENTITIES NOT LIKE '%<ALL>%' AND DELETED_FLAG=0";
                sSQL = "SELECT GROUP_MAP.GROUP_ID FROM GROUP_MAP,ORG_SECURITY WHERE GROUP_MAP.SUPER_GROUP_ID = ORG_SECURITY.GROUP_ID"+
                        " AND ORG_SECURITY.GROUP_ENTITIES NOT LIKE '%<ALL>%' AND GROUP_MAP.DELETED_FLAG=0";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            iGroup_ID = objDbReader.GetInt32("GROUP_ID");
                            sUID = FormatBESUserName(Conversion.CastToType<int>(m_sDSNID, out m_blnSuccess), iGroup_ID);
                            sNewUid = sUID;
                            //updating Entity_Map_User with new format of BES dbuser 
                            sSQL = "UPDATE ENTITY_MAP_USER SET USER_NAME='" + sUID + "' WHERE GROUP_ID=" + iGroup_ID;
                            RunSql(sSQL, m_objDbConnectionGlobal);

                            if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                            {
                                sPassword = RMCryptography.EncryptString(BES_COMPLEX_PASSWORD + string.Format("{0:00000}", iGroup_ID));
                            }
                            else
                            {
                                sPassword = RMCryptography.EncryptString(sUID);
                            }
                            sUID = RMCryptography.EncryptString(sUID);

                            //updating USER_ORGSEC with new format of BES dbuser and password
                            sSQL = "UPDATE USER_ORGSEC SET DB_UID='" + sUID + "', DB_PWD='" + sPassword + "' WHERE GROUP_ID=" + iGroup_ID + " AND DSN_ID=" + m_sDSNID;
                            RunSql(sSQL, m_objSecDbConnection);
                            sOld_Uid = "sg" + string.Format("{0:00000}", iGroup_ID);



                            if ((m_iBES == -1 && m_iDeffered == -1) && (m_bCheckvalue == true))
                            {
                                sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG = -1 WHERE SUPER_GROUP_ID IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_MAP.SUPER_GROUP_ID = ORG_SECURITY.GROUP_ID" +
                                        " AND ORG_SECURITY.GROUP_ENTITIES NOT LIKE '%<ALL>%') AND DELETED_FLAG=0 "; 
                                RunSql(sSQL, m_objDbConnectionGlobal);
                                sSQL = "UPDATE ORG_SECURITY SET UPDATED_FLAG=-1 WHERE GROUP_ENTITIES NOT LIKE '%<ALL>%' AND DELETED_FLAG=0";
                                RunSql(sSQL, m_objDbConnectionGlobal);

                            }
                            //drop old BES dbuser which has format like sg0000X
                            if (m_iBES == -1 && m_iDeffered == 0)
                            {
                                //USP_DropUserFirstTime(iGroup_ID, sOld_Uid, sNewUid);
                            }

                        }
                    }
                }
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
               

                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }

        /// <summary>
        /// Call all the User Stored Procedure.
        /// </summary>
        private static void CallUSP()
        {
           
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            try
            {
                if ((m_iBES == 0 && m_iDeffered == -1) || (m_iBES == -1 && m_iDeffered == 0)) //call stored proc in case of already enabled BES and exe is running first time   or BES is getting enabled firs time.
                {
                    //rsushilaggar MITS 25592 Date 08/04/2011
                    if (m_bCheckvalue == true)
                    {
                        RebuilAllGroups();
                    }
                    USP_EnableBES();
                    USP_CreateTriggers(); //pmittal5 - Triggers creation moved to a separate stored procedure
                    USP_CreateBesRole();
                    sSQL = "UPDATE DATA_SOURCE_TABLE SET ORGSEC_FLAG = -1, DEF_BES_FLAG = -1 WHERE DSNID = " + m_sDSNID;
                    RunSql(sSQL, m_objSecDbConnection);
                    USP_BuildEntityMapBes();

                }
                else if (m_iBES == -1)//Call BuilEntityMap store proc in case of new/edit group
                {
                    //rsushilaggar MITS 25592 Date 08/04/2011
                    //Deb---Group Deletetion was corrupting existing views of others groups
                    int iResult = 0;
                    int iDeletedGroupId = 0;

                    GetDeletedGroup(ref iResult, ref iDeletedGroupId);
                    if (iResult == -1)
                    {
                        sSQL = "SELECT COUNT(GROUP_ID) FROM GROUP_MAP WHERE SUPER_GROUP_ID NOT IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_ENTITIES LIKE '%<ALL>%') " +
                            " AND UPDATED_FLAG =-1 AND DELETED_FLAG=0";
                        using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                        {
                            if (objDbReader != null)
                            {
                                while (objDbReader.Read())
                                {
                                    int iCount = objDbReader.GetInt32(0);
                                    if (iCount > 0)
                                    {
                                        m_bCheckvalue = true;
                                    }
                                    else
                                    {
                                        m_bCheckvalue = false;
                                    }
                                }
                            }
                        }
                    }
                    if (m_bCheckvalue == true)
                    {
                        sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG = -1 WHERE SUPER_GROUP_ID NOT IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_ENTITIES LIKE '%<ALL>%') " +
                               " AND DELETED_FLAG=0";
                        RunSql(sSQL, m_objDbConnectionGlobal);
                    }
                    //Deb---Group Deletetion was corrupting existing views of others groups
                    RebuilAllGroups();//Deb: MITS 34547
                    USP_CreateBesRole();
                    USP_BuildEntityMapBes();
                }

            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }

        }

        /// <summary>
        /// Create database User name for BES groups.        
        /// </summary>
        /// <param name="iDSNId"> DSN ID in Data_Source_Table</param>
        /// <param name="iGroupID">Group Id of BES group</param>
        /// <returns>New Database User Name</returns>
        private static string FormatBESUserName(int iDSNId, int iGroupID)
        {

            string sUserName = string.Empty;
            int iDbId = 0;


            iDbId = GetDatabaseID();
            sUserName = "BES" + string.Format("{0:0000}", iDbId) + string.Format("{0:0000}", iDSNId) + string.Format("{0:00000}", iGroupID);

            return sUserName;
        }

        /// <summary>
        /// Get group Id which has to be deleted.
        /// </summary>
        /// <param name="iResult">-1 : if any group has to be deleted</param>
        /// <param name="iGroupId">BES group ID</param>
        private static void GetDeletedGroup(ref int iResult, ref int iGroupId)
        {

            DbReader objDbReader = null;
            string sSQL = string.Empty;

            try
            {

                //sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID > 0 AND DELETED_FLAG =-1 AND GROUP_ENTITIES NOT LIKE '%<ALL>%'";
                sSQL = "SELECT GROUP_ID FROM GROUP_MAP WHERE GROUP_ID > 0 AND DELETED_FLAG =-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            iGroupId = objDbReader.GetInt32("GROUP_ID");
                            iResult = -1;
                        }
                    }
                }

            }

            catch (Exception exc)
            {

                throw (exc);

            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }


            }

        }

        /// <summary>
        /// Execute EnableBES stored Proc.
        /// </summary>
        private static void USP_EnableBES()
        {
            
            DbParameter objParam = null;
            DbParameter objParamout = null;
            string sSQL = string.Empty;
            DbCommand objCmd = null;
            string serr_msg = string.Empty;
            int iUpdateBulkSecDpt = 0;
            int iUpdateBulkConfRec = 0; //pmittal5
            try
            {
                iUpdateBulkSecDpt = -1;
                if (m_iBES == -1 && m_iDeffered == 0)
                    iUpdateBulkSecDpt = 0;
                if (m_iConfRec == -1)  //pmittal5
                    iUpdateBulkConfRec = -1;

                m_objCmd = m_objDbConnection.CreateCommand();

                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    //sSQL = "BEGIN " + m_sDBOUserId + ".USP_ENABLE_BES( '" + m_sDBOUserId + "'," + iUpdateBulkSecDpt + "); END;";
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_ENABLE_BES( '" + m_sDBOUserId + "'," + iUpdateBulkSecDpt + "," + iUpdateBulkConfRec + "); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_ENABLE_BES";
                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sRm_USERID";
                    objParam.Value = m_sDBOUserId;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.Int32;                    
                    objParam.ParameterName = "@iUpdateBulkSecDpt";
                    objParam.Value = iUpdateBulkSecDpt;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.Int32;
                    objParam.ParameterName = "@iUpdateBulkConfRec";
                    objParam.Value = iUpdateBulkConfRec;
                    m_objCmd.Parameters.Add(objParam);
                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);
                    
                }

                Console.WriteLine("0 ^*^*^ Running procedure USP_ENABLE_BES");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_ENABLE_BES executed successfully");
            }

            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objCmd != null)
                    m_objCmd = null;

            }

        }
        private static void USP_CreateTriggers()
        {
            DbParameter objParam = null;
            DbParameter objParamout = null;
            string sSQL = string.Empty;
            DbCommand objCmd = null;
            string serr_msg = string.Empty;
 
            try
            {
                m_objCmd = m_objDbConnection.CreateCommand();

                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_CREATE_TRIGGERS( '" + m_sDBOUserId + "'," + m_iConfRec + "); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_CREATE_TRIGGERS";
                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sRm_USERID";
                    objParam.Value = m_sDBOUserId;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.Int32;
                    objParam.ParameterName = "@iConfRec";
                    objParam.Value = m_iConfRec;
                    m_objCmd.Parameters.Add(objParam);

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);

                }

                Console.WriteLine("0 ^*^*^ Running procedure USP_CREATE_TRIGGERS");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_CREATE_TRIGGERS executed successfully");
            }

            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objCmd != null)
                    m_objCmd = null;

            }

        }

        /// <summary>
        /// Execute CreateBesRole stored Proc.
        /// </summary>
        private static void USP_CreateBesRole()
        { 
            string sSQL = string.Empty;
            string sBesRole = string.Empty;
            string serr_msg = string.Empty;
            string stored_proc = string.Empty;//MITS:24404
            int iDbId = 0;

            DbParameter objParam = null;
            DbParameter objParamout = null;

            try
            {
                m_objCmd = m_objDbConnection.CreateCommand();
                iDbId = GetDatabaseID();
                sBesRole = "BESROLE" + iDbId.ToString();
                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_CREATE_BES_ROLE('" + sBesRole + "'); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    stored_proc = ConfigurationManager.AppSettings["StoredProcExecutePermission4SQL"];//MITS:24404
                    sSQL = "USP_CREATE_BES_ROLE";

                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sRM_userID";
                    objParam.Value = m_sDBOUserId;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 50;
                    objParam.ParameterName = "@sBesRole";
                    objParam.Value = sBesRole;
                    m_objCmd.Parameters.Add(objParam);

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);
                    //MITS:24404
                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 2000;
                    objParam.ParameterName = "@sStoredproc";
                    objParam.Value = stored_proc.ToString().Trim();
                    m_objCmd.Parameters.Add(objParam);
                    //MITS:24404
                }
                
                Console.WriteLine("0 ^*^*^ Running procedure USP_CREATE_BES_ROLE");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_CREATE_BES_ROLE executed successfully");
            }

            catch (Exception exc)
            {

                if (m_sDatabaseType != eDatabaseType.DBMS_IS_ORACLE)
                {
                throw (exc);
                }

            }
            finally
            {
                if (m_objCmd != null)
                    m_objCmd = null;
            }

        }

        /// <summary>
        /// Execute BuildEntityMapBes stored Proc.
        /// </summary>
        private static void USP_BuildEntityMapBes()
        {   
            string sSQL = string.Empty;
            string sSqlUid = string.Empty;
            string sUID = string.Empty;
            string sPassword = string.Empty;
            string sBes_Role = string.Empty;
            string sGroup_name = string.Empty;
            string serr_msg = string.Empty;
            string stored_proc = string.Empty;//MITS:24404
            int iGroupID ;
            int iPwd_Changed =0;
            int iDbId;

            bool bEnhanceBES = false;
            bool bViewOnlyBES = false;
            
            DbReader objDbReader = null;
            DbReader objReaderUid = null;
            DbParameter objParam = null;
            DbParameter objParamout = null;
            int iSuperGroupId = 0;  //pmittal5
            string sUserId = string.Empty;    //pmittal5
            string sSubordinateList = string.Empty;     //pmittal5

            try
            {  //pmittal5 - Confidential Record - GROUP_MAP contains user level group Id 
                //sSQL = "SELECT * FROM ORG_SECURITY WHERE UPDATED_FLAG =-1 AND DELETED_FLAG=0";
                sSQL = "SELECT SUPER_GROUP_ID,GROUP_ID FROM GROUP_MAP WHERE SUPER_GROUP_ID NOT IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_ENTITIES LIKE '%<ALL>%') "+
                        " AND UPDATED_FLAG =-1 AND DELETED_FLAG=0";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            sUID = string.Empty;
                            sPassword = string.Empty;
                            iPwd_Changed = 0;
                            sGroup_name = string.Empty;
                            iDbId = 0;
                            sBes_Role = string.Empty;
                            m_objCmd = m_objDbConnection.CreateCommand();
                            iGroupID = objDbReader.GetInt32("GROUP_ID");
                            iSuperGroupId = objDbReader.GetInt32("SUPER_GROUP_ID"); //pmittal5
                            //pmittal5 - USER_ID is required to obtain the Subordinate list of users , to be used while creating View definitions
                            //sSqlUid = "SELECT DB_UID, DB_PWD,PWD_CHANGED_FLAG FROM USER_ORGSEC WHERE GROUP_ID=" + iGroupID + " AND DSN_ID=" + m_sDSNID ;
                            sSqlUid = "SELECT USER_ID, DB_UID, DB_PWD,PWD_CHANGED_FLAG FROM USER_ORGSEC WHERE GROUP_ID=" + iGroupID + " AND DSN_ID=" + m_sDSNID;
                            using (objReaderUid = DbFactory.GetDbReader(AppGlobals.SecurityDSN(m_iClientId), sSqlUid))
                            {
                                if (objReaderUid != null)
                                {

                                    if (objReaderUid.Read())
                                    {
                                        sUserId = Conversion.ConvertObjToStr(objReaderUid.GetInt32("USER_ID"));  //pmittal5
                                        sUID = RMCryptography.DecryptString(objReaderUid.GetString("DB_UID"));
                                        sPassword = RMCryptography.DecryptString(objReaderUid.GetString("DB_PWD"));
                                        iPwd_Changed = objReaderUid.GetInt32("PWD_CHANGED_FLAG");
                                    }
                                }
                            }
                            //pmittal5 In case of Confidential Record, obtain Subordinate list of users for current user to be used in View definition 
                            if (m_iConfRec == -1)
                                sSubordinateList = ConfRecSupervisor(sUserId);
                            //End - pmittal5

                            //sGroup_name = objDbReader.GetString("GROUP_NAME");  //pmittal5
                            sGroup_name = GetGroupName(iSuperGroupId);
                            //sSQL = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ID = " + iSuperGroupId;

                            //m_objCmd.CommandText = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ID = " + iSuperGroupId;
                            //sGroup_name = m_objCmd.ExecuteScalar().ToString();
                            iDbId = GetDatabaseID();
                            sBes_Role = "BESROLE" + iDbId.ToString();
                            // ExecuteSP("SP_BUILD_ENTITY_MAP_SQL(" + iGroupID + ",'" + sUID + "','" + sPassword + "','" + sBes_Role + "')", "", objDbConnection);
                            try
                            {
                                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                                {
                                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                                    stored_proc = ConfigurationManager.AppSettings["StoredProcExecutePermission4Oracle"];//MITS:24404
                                    bEnhanceBES = GetSettingofEnhanceBES();
                                    bViewOnlyBES = GetSettingofViewOnlyBES();
                                    m_sDefaultTableSpaceForOracle = m_sDefaultTableSpaceForOracle.ToUpper();
                                    m_sTempTableSpaceForOracle = m_sTempTableSpaceForOracle.ToUpper();
                                    //sSQL = " BEGIN "+m_sDBOUserId+".USP_BUILD_ENTITY_MAP(" + iGroupID + ",'" + sUID + "','" + sPassword + "','" + sBes_Role + "','" + m_sDBOUserId + "'," + iPwd_Changed + "," + bEnhanceBES + "," + bViewOnlyBES + ",'" + m_sDefaultTableSpaceForOracle + "','" + m_sTempTableSpaceForOracle + "'); END;";
                                    sSQL = " BEGIN " + m_sDBOUserId + ".USP_BUILD_ENTITY_MAP(" + iGroupID + ",'" + sUID + "','" + sPassword + "','" + sBes_Role + "','" + m_sDBOUserId + "'," + iPwd_Changed + "," + bEnhanceBES + "," + bViewOnlyBES + ",'" + m_sDefaultTableSpaceForOracle + "','" + m_sTempTableSpaceForOracle + "'," + m_iConfRec + ",'" + sSubordinateList + "','" + stored_proc + "'); END;";
                                    m_objCmd.CommandText = sSQL;
                                }
                                else
                                {
                                    //sSQL = "SP_BUILD_ENTITY_MAP_SQL " + iGroupID + ",'" + sUID + "','" + sPassword + "','" + sBes_Role + "','" + sRM_USERID + "'";
                                    sSQL = "USP_BUILD_ENTITY_MAP";
                                    //sSQL = "SP_CREATE123_BES_ROLE";


                                    m_objCmd.CommandText = sSQL;
                                    m_objCmd.CommandType = CommandType.StoredProcedure;
                                    objParam = null;
                                    objParamout = null;

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.Int16;
                                    //objParam.Size = 500;
                                    objParam.ParameterName = "@iGroupId";
                                    objParam.Value = iGroupID;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.String;
                                    objParam.Size = 100;
                                    objParam.ParameterName = "@SQLUSER";
                                    objParam.Value = sUID;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.String;
                                    objParam.Size = 50;
                                    objParam.ParameterName = "@BES_COMPLEX_PASSWORD";
                                    objParam.Value = sPassword;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.String;
                                    objParam.Size = 50;
                                    objParam.ParameterName = "@sBesRole";
                                    objParam.Value = sBes_Role;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.String;
                                    objParam.Size = 50;
                                    objParam.ParameterName = "@sRm_UserId";
                                    objParam.Value = m_sDBOUserId;
                                    m_objCmd.Parameters.Add(objParam);

                                    //pmittal5 - Password change functionality in case of SQL also.
                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.Int16;
                                    objParam.ParameterName = "@iPwd_Changed";
                                    objParam.Value = iPwd_Changed;
                                    m_objCmd.Parameters.Add(objParam);
                                    //End
                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.Int16;
                                    objParam.ParameterName = "@iConfRec";
                                    objParam.Value = m_iConfRec;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParam = m_objCmd.CreateParameter();
                                    objParam.Direction = System.Data.ParameterDirection.Input;
                                    objParam.DbType = DbType.String;
                                    objParam.ParameterName = "@sSubordinateList";
                                    objParam.Value = sSubordinateList;
                                    m_objCmd.Parameters.Add(objParam);

                                    objParamout = m_objCmd.CreateParameter();
                                    objParamout.Direction = System.Data.ParameterDirection.Output;
                                    objParamout.DbType = DbType.String;
                                    objParamout.Size = 500;
                                    objParamout.ParameterName = "@out_err";
                                    m_objCmd.Parameters.Add(objParamout);
                                    m_objCmd.CommandText = sSQL;
                                }
                                Console.WriteLine("0 ^*^*^ Running procedure USP_BUILD_ENTITY_MAP for group " + sGroup_name + "  AND GROUP_ID  " + iGroupID);
                                m_objCmd.ExecuteNonQuery();
                                serr_msg = string.Empty;
                                if (objParamout != null)
                                {
                                    serr_msg = objParamout.Value.ToString();
                                }
                                if (!string.IsNullOrEmpty(serr_msg))
                                {
                                    throw new Exception(serr_msg);
                                }

                                Console.WriteLine("0 ^*^*^ executed USP_BUILD_ENTITY_MAP for group " + sGroup_name + "  AND GROUP_ID  " + iGroupID + " successfully");
                                if (iPwd_Changed == -1)
                                {

                                    sSQL = "UPDATE USER_ORGSEC SET PWD_CHANGED_FLAG = 0 WHERE GROUP_ID =" + iGroupID + " AND DSN_ID = " + m_sDSNID;
                                    RunSql(sSQL, m_objSecDbConnection);
                                }

                            }
                            catch (Exception se)
                            {
                                sSQL = "UPDATE GROUP_MAP SET STATUS_FLAG = -1 WHERE GROUP_ID =" + iGroupID;
                                RunSql(sSQL, m_objDbConnectionGlobal);
                                sSQL = "UPDATE ORG_SECURITY SET STATUS_FLAG = -1 WHERE GROUP_ID = " + iSuperGroupId ;
                                RunSql(sSQL, m_objDbConnectionGlobal);
                                Console.WriteLine("1001 ^*^*^ {0} ", "Error occured for group " + sGroup_name + "  and group id " + iGroupID);
                                serr_msg = se.Message + "  Error occured for group " + sGroup_name + "  and group id " + iGroupID;
                                Log.Write(serr_msg, "CommonWebServiceLog",m_iClientId);
                                //throw (se);

                            }
                            
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);

            }
            finally
            {
                if (m_objCmd != null)
                    m_objCmd = null;

            }

        }

        //pmittal5 - Obtain Subordinate list of users for current User Id
        private static string ConfRecSupervisor(string p_sUserId)
        {
            DbReader objRdManager = null;
            String sSQL = String.Empty;
            String sManagerID = String.Empty;
            String sUserID = String.Empty;
            String sTmp = String.Empty;
            String[] sArrUsers;

            sManagerID = p_sUserId;
            try
            {
                while (sManagerID != "")
                {
                    sSQL = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID IN (" + sManagerID + ")";
                    using (objRdManager = DbFactory.ExecuteReader(AppGlobals.SecurityDSN(m_iClientId), sSQL))
                    {
                        sUserID = sManagerID + "," + sUserID;
                        sManagerID = "";
                        if (objRdManager != null)
                        {
                            while (objRdManager.Read())
                            {
                                sTmp = "";
                                sTmp = Conversion.ConvertObjToStr(objRdManager.GetValue("USER_ID"));

                                sArrUsers = sUserID.Split(',');
                                for (int iCount = 0; iCount < sArrUsers.GetUpperBound(0); iCount++)
                                {
                                    if (sArrUsers[iCount] == sTmp)
                                    {
                                        sTmp = "";
                                        break;
                                    }
                                }
                                if (sTmp != "")
                                    sManagerID = sTmp + "," + sManagerID;
                            }
                        }
                    }
                    sManagerID = sManagerID.TrimEnd(',');
                }
                sUserID = sUserID.TrimEnd(',');
            }
            catch (Exception p_objExp)
            {
                throw (p_objExp);
            }
            finally
            {
                if (objRdManager != null)
                    objRdManager.Dispose();
            }
            return sUserID;
        }
        /// <summary>
        /// Execute DisableBes stored Proc.
        /// </summary>
        /// <param name="sUserNameNew">DB user having format like sg0000x</param>
        /// <param name="sUserNameOld">DB user having format like BES000015789008</param>
        private static void USP_DisableBes(string sUserNameNew, string sUserNameOld)
        {
            string sSQL = string.Empty;
            string serr_msg = string.Empty;

            DbParameter objParam = null;
            DbParameter objParamout = null;
            
            try
            {
 
                m_objCmd = m_objDbConnection.CreateCommand();

                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sUserNameNew = sUserNameNew.ToUpper();
                    sUserNameOld = sUserNameOld.ToUpper();
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_DISABLE_BES( '" + m_sDBOUserId + "','" + sUserNameNew + "','" + sUserNameOld + "'); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_DISABLE_BES";
                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;


                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sRm_USERID";
                    objParam.Value = m_sDBOUserId;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sUserNameNew";
                    objParam.Value = sUserNameNew;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sUserNameOld";
                    objParam.Value = sUserNameOld;
                    m_objCmd.Parameters.Add(objParam);

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);
                    
                }

                Console.WriteLine("0 ^*^*^ Running procedure USP_DISABLE_BES");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_DISABLE_BES executed successfully");

            }

            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
               
                if (m_objCmd != null)
                    m_objCmd = null;
            }

        }

        /// <summary>
        /// Execute DisableBes stored Proc.
        /// </summary>
        private static void USP_DropTriggers()
        {
            DbParameter objParamout = null;
            string sSQL = string.Empty;
            string serr_msg = string.Empty;
            try
            {   
                m_objCmd = m_objDbConnection.CreateCommand();
                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_DROP_TRIGGERS( '" + m_sDBOUserId + "'); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_DROP_TRIGGERS";

                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParamout = null;

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);
                    m_objCmd.CommandText = sSQL;
                }

                Console.WriteLine("0 ^*^*^ Running procedure USP_DROP_TRIGGERS");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_DROP_TRIGGERS executed successfully");


            }

            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                
                if (m_objCmd != null)
                    m_objCmd = null;
            }

        }

        /// <summary>
        /// Execute DropUserFirstTime stored Proc.
        /// </summary>
        /// <param name="iGroup_ID">BES Group ID</param>
        /// <param name="sOld_Uid">DB user having format like sg0000x</param>
        /// <param name="sNewUid">DB user having format like BES000015789008</param>
        private static void USP_DropUserFirstTime(int iGroup_ID, string sOld_Uid, string sNewUid)
        { 
            string sSQL = string.Empty;
            string serr_msg = string.Empty;

            DbParameter objParam = null;
            DbParameter objParamout = null;
            try
            {
                
                m_objCmd = m_objDbConnection.CreateCommand();


                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sOld_Uid = sOld_Uid.ToUpper();
                    sNewUid = sNewUid.ToUpper();
                    sSQL = " BEGIN " + m_sDBOUserId + ".USP_DROP_USER_FIRST_TIME('" + sOld_Uid + "','" + sNewUid + "'); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_DROP_USER_FIRST_TIME";

                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.Int16;
                    //objParam.Size = 500;
                    objParam.ParameterName = "@iGroupId";
                    objParam.Value = iGroup_ID;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@SQLUSER";
                    objParam.Value = sOld_Uid;
                    m_objCmd.Parameters.Add(objParam);

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@SQLUSER_NEW";
                    objParam.Value = sNewUid;
                    m_objCmd.Parameters.Add(objParam);

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);
                    m_objCmd.CommandText = sSQL;

                }

                //Console.WriteLine("0 ^*^*^ Running procedure SP_DROP_USER_FIRST_TIME for group " + sGroup_name + "  AND GROUP_ID  " + iGroupID);
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                //Console.WriteLine("0 ^*^*^ executed SP_BUILD_ENTITY_MAP for group " + sGroup_name + "  AND GROUP_ID  " + iGroupID + " successfully");

            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                
                if (m_objCmd != null)
                    m_objCmd = null;
            }
        }

        /// <summary>
        /// Get Database ID
        /// </summary>
        /// <returns>DatabaseId</returns>
        private static int GetDatabaseID()
        {
            string sMainDbName = string.Empty;
            string sSql = string.Empty;
            int iDbId =0;
            int iPos1 ;
            int iPos2 ;

            DbReader objDbReader = null;

            try
            {

                if ((m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    iPos1 = m_sDbConnstringGlobal.IndexOf("Database=");
                    iPos2 = m_sDbConnstringGlobal.IndexOf("UID=");
                    sMainDbName = m_sDbConnstringGlobal.Substring(iPos1 + 9, iPos2 - iPos1 - 10);
                    sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + sMainDbName + "'";
                }
                else
                {
                    iPos1 = m_sDbConnstringGlobal.IndexOf("UID=");
                    iPos2 = m_sDbConnstringGlobal.IndexOf("PWD=");
                    sMainDbName = m_sDbConnstringGlobal.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                    sSql = "SELECT USER_ID FROM ALL_USERS WHERE USERNAME='" + sMainDbName.ToUpper() + "'";
                }
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSql))
                {

                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                            {
                                iDbId = objDbReader.GetInt32("DATABASE_ID");
                            }
                            else
                            {
                                iDbId = objDbReader.GetInt32("USER_ID");
                            }
                        }

                    }
                }


            }
            catch (Exception exc )
            {
                throw(exc);
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();

            }

            return iDbId;
        }

        /// <summary>
        /// Get Orgsec and DefBESFlag
        /// </summary>
        /// <param name="iDSNId">DSN Id </param>
        /// <param name="iBES">-1/0  -1 :if set </param>
        /// <param name="iDeffered">-1/0  -1 :if set</param>
        private static void GetBesConfAndDeffFlag(int iDSNId, ref int iBES, ref int iDeffered, ref int iConfRec)
        {

            DbReader objDbReader = null;

            string sSQL = string.Empty;

            try
            {
                //sSQL = "SELECT ORGSEC_FLAG, DEF_BES_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + iDSNId;  //pmittal5
                sSQL = "SELECT ORGSEC_FLAG, DEF_BES_FLAG, CONF_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + iDSNId;
                using (objDbReader = DbFactory.GetDbReader(AppGlobals.SecurityDSN(m_iClientId), sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {

                            iBES = Conversion.CastToType<int>(objDbReader.GetValue("ORGSEC_FLAG").ToString(), out m_blnSuccess);
                            iDeffered = Conversion.CastToType<int>(objDbReader.GetValue("DEF_BES_FLAG").ToString(), out m_blnSuccess);
                            iConfRec = Conversion.CastToType<int>(objDbReader.GetValue("CONF_FLAG").ToString(), out m_blnSuccess); //pmittal5
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                throw (exc);
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();

            }

        }
        /// <summary>
        /// Get Orgsec Flag
        /// </summary>      
        /// <param name="iBES">-1/0  -1 :if set </param>        
        private static void GetBesFlag( ref int iOrgSec)
        {

            DbReader objDbReader = null;

            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT ORGSEC_FLAG FROM DATA_SOURCE_TABLE WHERE ORGSEC_FLAG = -1";
                using (objDbReader = DbFactory.GetDbReader(AppGlobals.SecurityDSN(m_iClientId), sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {

                            iOrgSec = Conversion.CastToType<int>(objDbReader.GetValue("ORGSEC_FLAG").ToString(), out m_blnSuccess);                          

                        }
                    }
                }

            }
            catch (Exception exc)
            {
                throw (exc);
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();

            }

        }

        /// <summary>
        /// Execute Sql statement
        /// </summary>
        /// <param name="p_sSql">statement to be executed</param>
        /// <param name="p_objConnection">Riskmaster or security database connection object</param>
        private static void RunSql(string p_sSql, DbConnection p_objConnection)
        {
            try
            {
                p_objConnection.ExecuteNonQuery(p_sSql);
            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
        }

        /// <summary>
        /// Get Setting of Enhance BES
        /// </summary>
        /// <returns>true/false</returns>
        private static bool GetSettingofEnhanceBES()
        {
            string sSQL = string.Empty;
            XmlDocument objXmlDoc = null;
            XmlNode objXmlNode = null;
            bool bEnhanceBes = false;
            bool bEnhanceBesFromSession = false;
            bool bEnhanceBesFromUtilities = false;
            objXmlDoc = new XmlDocument();

            DbReader objDbReader = null;
            try
            {
                //Get value of bEnhanceBesFromSession
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXmlDoc.LoadXml(strContent);
                    objXmlNode = objXmlDoc.SelectSingleNode("//SpecialSettings/EnhanceBES");
                    if (objXmlNode != null)
                    {
                        if (objXmlNode.InnerText == "-1")
                        {
                            bEnhanceBesFromSession = true;
                        }
                    }
                }

                //Get value of bEnhanceBesFromUtilities
                sSQL = "SELECT ENH_BES  FROM SYS_PARMS";

                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {

                            bEnhanceBesFromUtilities = Conversion.CastToType<bool>(objDbReader.GetValue("ENH_BES").ToString(), out m_blnSuccess);

                        }
                    }
                }


                //Determine value of bEnhanceBes
                if (bEnhanceBesFromUtilities)
                {
                    bEnhanceBes = true;
                }
                if (!string.IsNullOrEmpty(strContent))
                {
                    if (bEnhanceBesFromSession)
                        bEnhanceBes = true;
                    else
                        bEnhanceBes = false;

                }
            }
            catch(Exception exc)
            {
                throw (exc);
            }
            
            return bEnhanceBes;

        }

        /// <summary>
        /// Get Setting of ViewOnlyBES 
        /// </summary>
        /// <returns>true/false</returns>
        private static bool GetSettingofViewOnlyBES()
        {

            XmlDocument objXmlDoc = null;
            XmlNode objXmlNode = null;
            bool bViewOnlyBES = false;

            objXmlDoc = new XmlDocument();



            //Get value of bViewOnlyBES From Session
            string strContent = RMSessionManager.GetCustomContent(m_iClientId);
            if (!string.IsNullOrEmpty(strContent))
            {
                objXmlDoc.LoadXml(strContent);
                objXmlNode = objXmlDoc.SelectSingleNode("//SpecialSettings/ViewOnlyBES");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText == "-1")
                    {
                        bViewOnlyBES = true;
                    }
                }
            }

            return bViewOnlyBES;

        }

        /// <summary>
        /// Get Admin User Id and Password from UI
        /// </summary>
        private static void GetAdminUIdandPwd()
        {
            frmAdmin objAdmin = new frmAdmin();
            objAdmin.ReturnValue += new frmAdmin.ReturnEventHandler(objAdmin_ReturnValue);
            objAdmin.ShowDialog();
            AppGlobals.bShowDialog = false;
            objAdmin.ReturnValue -= new frmAdmin.ReturnEventHandler(objAdmin_ReturnValue);
        }

        /// <summary>
        /// Format Exception Message returned from Stored Procedure.
        /// </summary>
        /// <param name="smsg">Exception Message</param>
        private static void FormatAndDisplayExcMsg(string smsg)
        {
            string[] sMsgArray = smsg.Split('\n');

            int iLengthofArray = sMsgArray.Length;
            for (int i = 0; i < iLengthofArray; i++)
            {
                sMsgArray[i] = sMsgArray[i].Replace("'", "");
                sMsgArray[i] = sMsgArray[i].Replace("-", " ");
                Console.WriteLine("1001 ^*^*^ {0} ", sMsgArray[i]);
            }
        }

        /// <summary>
        /// Get parameters sent from command line
        /// </summary>
        /// <param name="p_args">Array of argument sent from Command line</param>

        private static void GetParameters(string[] p_args)
        {
            int iLength = 0;
            string sPrefix= string.Empty;
            string sRebuildAll = string.Empty;
            iLength = p_args.Length;

            for (int i= 0; i < iLength; i++)
            {

                sPrefix = p_args[i].Trim();

                if(sPrefix.Length>3)
                {
                    sPrefix = sPrefix.Substring(0,3);
                }
                switch (sPrefix.ToLower())
                {
                    case "-ds":
                        m_sDataSource = p_args[i].Trim().Substring(3);
                        break;
                    case "-ru":
                        m_sLoginName = p_args[i].Trim().Substring(3);
                        break;
                    case "-rp":
                        m_sLoginPwd = p_args[i].Trim().Substring(3);
                        break;
                    case "-au":
                        m_sAdminUserName = p_args[i].Trim().Substring(3);
                        break;
                    case "-ap":
                        m_sAdminPassword = p_args[i].Trim().Substring(3);
                        break;
                    case "-ci"://Add by kuladeep for Cloud.
                        m_iClientId = Convert.ToInt32(p_args[i].Trim().Substring(3));
                        break;
                    case "-ra":
                        sRebuildAll = p_args[i].Trim().Substring(3);
                        if (sRebuildAll.ToLower() =="y")
                        {
                            m_bCheckvalue = true;
                        }
                        else
                        {
                            m_bCheckvalue = false;
                        }
                        break;
                }
            }

            

        }
        /// <summary>
        /// Call stored Procedure USP_REBUILD_ORG_HIERARCHY
        /// which update Entity_Map and set UPDATED_FLAG of 
        /// all the BES groups
        /// </summary>
        private static void RebuilAllGroups()
        {
            DbParameter objParam = null;
            DbParameter objParamout = null;
            string sSQL = string.Empty;
            DbCommand objCmd = null;
            string serr_msg = string.Empty;
            
            try
            {
                

                m_objCmd = m_objDbConnection.CreateCommand();

                if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    m_sDBOUserId = m_sDBOUserId.ToUpper();
                    sSQL = "BEGIN " + m_sDBOUserId + ".USP_REBUILD_ORG_HIERARCHY( '" + m_sDBOUserId + "'); END;";
                    m_objCmd.CommandText = sSQL;
                }
                else
                {
                    sSQL = "USP_REBUILD_ORG_HIERARCHY";
                    m_objCmd.CommandText = sSQL;
                    m_objCmd.CommandType = CommandType.StoredProcedure;
                    objParam = null;
                    objParamout = null;

                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@sRm_USERID";
                    objParam.Value = m_sDBOUserId;
                    m_objCmd.Parameters.Add(objParam);

                   

                    objParamout = m_objCmd.CreateParameter();
                    objParamout.Direction = System.Data.ParameterDirection.Output;
                    objParamout.DbType = DbType.String;
                    objParamout.Size = 500;
                    objParamout.ParameterName = "@out_err";
                    m_objCmd.Parameters.Add(objParamout);

                }

                Console.WriteLine("0 ^*^*^ Running procedure USP_REBUILD_ORG_HIERARCHY");
                m_objCmd.ExecuteNonQuery();
                if (objParamout != null)
                {
                    serr_msg = objParamout.Value.ToString();
                }
                if (!string.IsNullOrEmpty(serr_msg))
                {
                    throw new Exception(serr_msg);
                }
                Console.WriteLine("0 ^*^*^  procedure USP_REBUILD_ORG_HIERARCHY executed successfully");
            }

            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objCmd != null)
                    m_objCmd = null;

            }

        }

        private static string GetGroupName(int iSuperGroupId)
        {
            string sGroupName = string.Empty;
            string sSql = string.Empty;
            sSql = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ID = " + iSuperGroupId;
            using (DbReader objDbReader = DbFactory.GetDbReader(m_sDbConnstringGlobal, sSql))
            {

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
                        sGroupName = objDbReader.GetString(0);
                    }

                }
            }
            return sGroupName;
        }

    }
}
