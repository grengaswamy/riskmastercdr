﻿using System;
using Riskmaster.Security;
using System.Configuration;
using Riskmaster.Common;


namespace BESScheduler
{
   public class AppGlobals
    {
        public static UserLogin Userlogin;
        public static string ConnectionString;
        public static string sUser = ""; //user login
        public static string sPwd = ""; //password
        public static string sDSN = ""; //database name
        public static bool bSilentMode = false;
        public static bool bShowDialog = true;
        public static bool bDisableControls = false;

       //Add & change by kuladeep for Cloud Jira-1122 Start

        //public static string SecurityDSN 
        //{
        //    get
        //    {
        //        return ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
        //    }//get
        //}//property: SecurityDSN

        public static string SecurityDSN(int p_iClientId)
        {
            return RMConfigurationManager.GetConnectionString("RMXSecurity", p_iClientId);
        }

        //Add & change by kuladeep for Cloud Jira-1122 End
              
    }
}
