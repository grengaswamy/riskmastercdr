using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO; 
using System.Text;
using System.Xml; 
using System.Xml.Xsl;

using Riskmaster.Application.RMUtilities;
using Riskmaster.Common; 
using Riskmaster.DataModel;
using Riskmaster.Db; 
using Riskmaster.Security;

namespace Riskmaster.Tools.PvUpgrade
{
	/// <summary>
	/// Summary description for RMXForm.
	/// </summary>
	public class RMXForm : FormBase
	{
        //rsolanki2: Performance updates 
        private XslCompiledTransform XctModify;
        private int m_iClientId = 0;
        /// <summary>
        /// RMXForm
        /// </summary>
        /// <param name="oParent"></param>
        /// <param name="oFormListDoc"></param>
        /// <param name="oTraceListener"></param>
        /// <param name="oTraceSwitch"></param>
		public RMXForm(PowerView oParent, XmlDocument oFormListDoc, TextWriterTraceListener oTraceListener, TraceSwitch oTraceSwitch)
		{
			m_parent = oParent;
			m_OldFormXmlDoc = new XmlDocument();
			m_FormListDoc = oFormListDoc;
			m_TraceListener = oTraceListener;
			m_TraceSwitch = oTraceSwitch;
            //rsolanki2: Performance updates 
            XctModify = new XslCompiledTransform();
            XctModify.Load("Modify.xslt");
		}

        /// <summary>
        /// FormName
        /// </summary>
		public override string FormName
		{
			get{ return m_FormName; }
			set
			{ 
				m_FormName = value;
				m_IsPowerViewForm = m_parent.IsPVForm( m_FormName );

				if( m_FormName.StartsWith("admintracking|"))
					m_IsADT = true;
				else
					m_IsADT = false;
			}
		}

        //oForm.UpgradeForm(m_User, m_Password, m_DSN, m_iDSNId, m_ConnectionString, m_ViewID);
		/// <summary>
		/// Upgrade RMX version power view for
		/// </summary>
        /// 
		public override void UpgradeForm(string p_sUser, string p_sPassword, string p_sDSN, int p_iDSNId, string p_sConnString, int p_iViewId)
		{
            XmlDocument oInputXmlDoc = new XmlDocument();
            PVFormEdit objPVFormEdit = null;

            #region commented out code
            //string sCtrl = string.Empty;
            //XmlDocument oDefaultFormXmlDoc = new XmlDocument();

            ////Use same DOM object for existing and the upgraded form
            //m_NewFormXmlDoc = m_OldFormXmlDoc;
            //if(!m_IsADT)
            //{
            //    m_DSBaseView.Tables[0].DefaultView.RowFilter = "FORM_NAME='" + m_FormName + "'";
            //    if( m_DSBaseView.Tables[0].DefaultView.Count > 0 )   
            //    {
            //        oDefaultFormXmlDoc.LoadXml(m_DSBaseView.Tables[0].DefaultView[0].Row["VIEW_XML"].ToString());
            //    }
            //    else
            //        throw new Exception("Default View not available for View: " + m_ViewName + " Form: " + m_FormName); 
				
            //    //Supplementals are not part of the view template, so get the supplemental list
            //    AddSuppDefinition(m_User, m_Password, m_ConnectionString, m_DSN, oDefaultFormXmlDoc);
            //}
            //else
            //{
            //    oDefaultFormXmlDoc = GetDefaultAdmView(m_FormName);
            //}
									
            //AddHiddenElements(oDefaultFormXmlDoc, m_NewFormXmlDoc);
            ////Arnab:MITS-10531 - Replace javascript elements in the target Doc with the 
            ////ones in the Source Doc
            //AddJavaScriptElements(oDefaultFormXmlDoc, m_NewFormXmlDoc);
            //AddNewAttributes4Controls(oDefaultFormXmlDoc, m_NewFormXmlDoc);

            ////set tab index
            //SetTabIndex();
            #endregion

            // Naresh Instead of writing our own functions we will call the functions of PowerViewEditor
            oInputXmlDoc.LoadXml(GetTemplate(m_FormName, p_iViewId));
            objPVFormEdit = new Riskmaster.Application.RMUtilities.PVFormEdit(p_sUser, p_sPassword, p_sDSN, p_iDSNId.ToString(), p_sConnString, m_iClientId);
            m_NewFormXmlDoc = objPVFormEdit.Get(oInputXmlDoc);

            //Raman/Naresh 06/19/2009: Handles all the controls for which id's have been changed in R5.This was done to avoid duplicate controls in optional tabs in WC and DI
            //e.g employmentinfo and employmentinfowocasemgt
            // akaushik5 Changed for MITS 30290 Starts
            //if (m_FormName.ToLower() == "claimwc.xml" )
            if (m_FormName.ToLower() == "claimwc.xml" || m_FormName.ToLower().Equals("claimwcd.xml"))
            // akaushik5 Changed for MITS 30290 Ends
            {
                UpdateWCControls(ref m_NewFormXmlDoc);
            }

            if (m_FormName.ToLower() == "claimdi.xml")
            {
                UpdateDIControls(ref m_NewFormXmlDoc);
            }
		}

        /// <summary>
        /// Handles all the controls for which id's have been changed in R5.
        /// This was done to avoid duplicate controls in optional tabs in WC 
        /// e.g employmentinfo and employmentinfowocasemgt
        /// </summary>
        /// <param name="p_NewFormXmlDoc"></param>
        private void UpdateWCControls(ref XmlDocument p_NewFormXmlDoc)
        {
            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'empmonthlyrate_1']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'empmonthlyrate']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'empmonthlyrate']")).SetAttribute("value", "empmonthlyrate_1");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'emppayamount_1']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'emppayamount']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'emppayamount']")).SetAttribute("value", "emppayamount_1");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'calcaww4']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'calcaww2']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'calcaww2']")).SetAttribute("value", "calcaww4");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeerestrictions_1']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions']")).SetAttribute("value", "employeerestrictions_1");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeeworkloss_1']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss']")).SetAttribute("value", "employeeworkloss_1");
                }
            }
            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'empmonthlyrate_1|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'empmonthlyrate|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'empmonthlyrate|']")).SetAttribute("value", "empmonthlyrate_1|");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'emppayamount_1|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'emppayamount|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'emppayamount|']")).SetAttribute("value", "emppayamount_1|");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'calcaww4|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'calcaww2|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'calcaww2|']")).SetAttribute("value", "calcaww4|");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeerestrictions_1|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions|']")).SetAttribute("value", "employeerestrictions_1|");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeeworkloss_1|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss|']")).SetAttribute("value", "employeeworkloss_1|");
                }
            }
        }

        /// <summary>
        /// Handles all the controls for which id's have been changed in R5.
        /// This was done to avoid duplicate controls in optional tabs in DI
        /// e.g employmentinfo and employmentinfowocasemgt
        /// </summary>
        /// <param name="p_NewFormXmlDoc"></param>
        private void UpdateDIControls(ref XmlDocument p_NewFormXmlDoc)
        {
            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeerestrictions2']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions']")).SetAttribute("value", "employeerestrictions2");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeeworkloss2']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss']")).SetAttribute("value", "employeeworkloss2");
                }
            }
            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeerestrictions2|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeerestrictions|']")).SetAttribute("value", "employeerestrictions2|");
                }
            }

            if (p_NewFormXmlDoc.SelectSingleNode("//FormFields/FormFieldsoption[@value = 'employeeworkloss2|']") != null)
            {
                if (p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss|']") != null)
                {
                    ((XmlElement)p_NewFormXmlDoc.SelectSingleNode("//FormList/FormListoption[@value = 'employeeworkloss|']")).SetAttribute("value", "employeeworkloss2|");
                }
            }
        }

		/// <summary>
		/// Save upgraded powerview form back to database
		/// </summary>
		/// <param name="oCommand"></param>
        public override void SaveUpgradeForm(string p_sUser, string p_sPassword, string p_sDSN, int p_iDSNId, string p_sConnString)
		{
			string sSQL = String.Empty;

			try
            {
                #region commented out code
                //DbParameter objParam = null; 

                ////Modified by Navdeep
                ////sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML=~VXML~ WHERE VIEW_ID=" + m_ViewID + " AND FORM_NAME='" + m_FormName.Replace("'", "''") + "'";                
                //sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML=~VXML~ WHERE VIEW_ID=" + m_ViewID + " AND FORM_NAME='" + m_FormName.Replace("'", "''") + "' AND DATA_SOURCE_ID = " + m_DSNID;
                //oCommand.Parameters.Clear();
                //objParam = oCommand.CreateParameter();
                //objParam.Direction=ParameterDirection.Input;
                //objParam.Value = m_NewFormXmlDoc.OuterXml ;
                //objParam.ParameterName = "VXML";
                //objParam.SourceColumn="VIEW_XML";
                //oCommand.Parameters.Add(objParam);
                //oCommand.CommandText = sSQL;
                //oCommand.ExecuteNonQuery();
                #endregion

                PVFormEdit objPVFormEdit = null;
                XmlDocument objSaveXmlIn = new XmlDocument();
                //XslCompiledTransform xsl = new XslCompiledTransform();
                XsltArgumentList xslArg = new XsltArgumentList();
                StringWriter sw = new StringWriter();
                //xsl.Load("Modify.xslt");
                //xsl.Transform(m_NewFormXmlDoc, xslArg, sw);

                //rsolanki2: Performance updates                 
                XctModify.Transform(m_NewFormXmlDoc, xslArg, sw);
                objSaveXmlIn.LoadXml(sw.ToString());
                // Call Save
                objPVFormEdit = new Riskmaster.Application.RMUtilities.PVFormEdit(p_sUser, p_sPassword, p_sDSN, p_iDSNId.ToString(), p_sConnString, m_iClientId);
                objPVFormEdit.Save(objSaveXmlIn);
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:SaveUpgradeForm:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
		}

		/// <summary>
		/// Replace hidden elements in the target Doc with the ones in the Source Doc
		/// </summary>
		/// <param name="p_objSourceDoc"></param>
		/// <param name="p_objTargetDoc"></param>
		/// <returns></returns>
		protected bool AddHiddenElements(XmlDocument p_objSourceDoc, XmlDocument p_objTargetDoc)
		{
			XmlNodeList objList = null;
			XmlElement objXMLElem = null;
			XmlElement objXMLFormElem = null;
			string sName = string.Empty;
			string sGrpName = string.Empty;

			try
			{
				if(p_objSourceDoc == null)
					return false;

				//removing internal and security controls
				objList = p_objTargetDoc.SelectNodes("//internal|//security");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objNode.ParentNode.RemoveChild(objNode);  
					}
				}
				
				// Add all internal and security type elements to the powerview definition
				objXMLFormElem = (XmlElement)p_objTargetDoc.GetElementsByTagName("form").Item(0);
				if( objXMLFormElem == null )
					return true;

				objList = p_objSourceDoc.SelectNodes("//internal|//security");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objXMLElem = CloneXMLElement((XmlElement)objNode, p_objTargetDoc);
						objXMLFormElem.AppendChild(objXMLElem);
					}
				}

				//removing id type Controls
				objList = p_objTargetDoc.SelectNodes("//control[@type='id']");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objNode.ParentNode.RemoveChild(objNode);  
					}
				}

				//Adding all id type controls
				objList = p_objSourceDoc.SelectNodes("//control[@type='id']");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objXMLElem = CloneXMLElement((XmlElement)objNode, p_objTargetDoc);
						objXMLFormElem.AppendChild(objXMLElem);
					}
				}

				//removing hidden type Controls
				objList = p_objTargetDoc.SelectNodes("//control[@type='hidden']");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objNode.ParentNode.RemoveChild(objNode);  
					}
				}

				//Adding all hidden type controls
				objList = p_objSourceDoc.SelectNodes("//control[@type='hidden']");
				if(objList!=null)
				{
					foreach(XmlNode objNode in objList)
					{
						objXMLElem = CloneXMLElement((XmlElement)objNode, p_objTargetDoc);
						objXMLFormElem.AppendChild(objXMLElem);
					}
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:AddHiddenElements:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}

			return true;
		}

        /// Name		: AddJavaScriptElements
        /// Author		: Arnab Mukherjee
        /// Date Created: 03/27/2008	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Arnab:MITS-10531
        /// Replace javascript elements in the target Doc with the ones in the Source Doc
        /// </summary>
        /// <param name="p_objSourceDoc"></param>
        /// <param name="p_objTargetDoc"></param>
        /// <returns></returns>
        protected bool AddJavaScriptElements(XmlDocument p_objSourceDoc, XmlDocument p_objTargetDoc)
        {
            XmlNodeList objList = null;
            XmlElement objXMLElem = null;
            XmlElement objXMLFormElem = null;
            string sName = string.Empty;
            string sGrpName = string.Empty;

            try
            {
                if (p_objSourceDoc == null)
                    return false;

                // Add all javascript type elements to the powerview definition
                objXMLFormElem = (XmlElement)p_objTargetDoc.GetElementsByTagName("form").Item(0);
                if (objXMLFormElem == null)
                    return true;

                //removing javascript type Controls
                objList = p_objTargetDoc.SelectNodes("//control[@type='javascript']");
                if (objList != null)
                {
                    foreach (XmlNode objNode in objList)
                    {
                        objNode.ParentNode.RemoveChild(objNode);
                    }
                }

                //Adding all javascript type controls
                objList = p_objSourceDoc.SelectNodes("//control[@type='javascript']");
                if (objList != null)
                {
                    foreach (XmlNode objNode in objList)
                    {
                        objXMLElem = CloneXMLElement((XmlElement)objNode, p_objTargetDoc);
                        objXMLFormElem.AppendChild(objXMLElem);
                    }
                }
            }
            catch (Exception e)
            {
                m_TraceListener.WriteLine("RMXForm:AddJavaScriptElements:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
                throw e;
            }

            return true;
        }

		/// <summary>
		/// Get a new element in the target Documnet based on the source element
		/// </summary>
		/// <param name="objSourceElement">source element</param>
		/// <param name="objTargetDoc">document where new element created</param>
		/// <returns></returns>
		private XmlElement CloneXMLElement(XmlElement objSourceElement, XmlDocument objTargetDoc)
		{
            string sAttr = String.Empty;
            string sVal = String.Empty;
            string sCtrlName = String.Empty;
			XmlElement objTargetElement = null;
			XmlNode objXMLTempNode = null;

			if(objSourceElement == null || objTargetDoc == null)
				return null;

			try
			{
				sCtrlName = objSourceElement.Name;
				objTargetElement = objTargetDoc.CreateElement(sCtrlName);
				for(int i = 0; i < objSourceElement.Attributes.Count; i++)
				{
					sAttr = objSourceElement.Attributes[i].Name;
					sVal = objSourceElement.Attributes[i].Value;
					objTargetElement.SetAttribute(sAttr, sVal);   
				}

				//copy child nodes. for types like combo boxes
				foreach(XmlNode objXMLNode in objSourceElement.ChildNodes)
				{
					objXMLTempNode = objTargetElement.OwnerDocument.ImportNode(objXMLNode, true);
					objTargetElement.AppendChild(objXMLTempNode);
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:CloseXMLElement:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}

			return objTargetElement;
		}

		/// <summary>
		/// 1. Make all id/javascript/hidden controls to be form's children
		/// 2. Remove all group controls
		/// 3. Remove all button controls (Except "back" button)
		/// </summary>
		/// <param name="p_objDoc">The new form XML from default view</param>
		/// <returns></returns>
		protected bool RemoveAllControls(XmlDocument p_objDoc)
		{
			XmlElement objElemForm = null;
			XmlNodeList objNodeList = null;
			ArrayList arrNodes = new ArrayList();

			try
			{
				if(p_objDoc == null)
					return false;

				//remove id/hidden/js from inside the group and displaycolumn, and
				//Add them as children of form element
				objElemForm =(XmlElement)p_objDoc.GetElementsByTagName("form").Item(0);
				objNodeList = p_objDoc.SelectNodes(
					"//control[@type='id'] | //control[@type='javascript'] | //control[@type='hidden']");
				foreach(XmlNode objNode in objNodeList)
				{
					arrNodes.Add(objNode.ParentNode.RemoveChild(objNode));  
				}

				for(int i = 0; i <= arrNodes.Count - 1; i++)
					objElemForm.AppendChild((XmlNode)arrNodes[i]);
			
				//remove all groups
				objNodeList = p_objDoc.SelectNodes("//group");
				foreach(XmlNode objNode in objNodeList)
					objNode.ParentNode.RemoveChild(objNode);

				//remove all buttons
				objNodeList = p_objDoc.SelectNodes("//button | //buttonscript");
				foreach(XmlNode objNode in objNodeList)
				{
					//Do not remove the Back buttons. Use the one in the rmx template
					if(((XmlElement)objNode).GetAttribute("type")!="back")
						objNode.ParentNode.RemoveChild(objNode);  
				}
				return true;
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:RemoveAllControls:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
			finally
			{
				objElemForm=null;
				objNodeList = null;
			}
		}

		/// <summary>
		/// Add new attributes to the target nodelist's each element, or update the atrr
		/// value with the one from default view if these att are not editable in UI
		/// </summary>
		/// <param name="p_objSourceDoc"></param>
		/// <param name="p_objTargetDoc"></param>
		private void AddNewAttributes4Controls(XmlDocument p_objSourceDoc, XmlDocument p_objTargetDoc)
		{
			string sAttName = String.Empty;
			XmlElement oSourceElement = null;
			XmlElement oTargetElement = null;
			XmlNodeList oNodeList = null;

			try
			{
				//Process attributes for form elements
				AddNewAttributes4ControlType(p_objSourceDoc, p_objTargetDoc, "form");

                //Process attributes for toobar/button. Not all element has a name attribute, so they
                //has to be special processed.
                oNodeList = p_objTargetDoc.SelectNodes("//toolbar/button");
                foreach (XmlNode oNode in oNodeList)
                {
                    oTargetElement = (XmlElement)oNode;
                    oSourceElement = (XmlElement)p_objSourceDoc.SelectSingleNode("//toolbar/button[@type='" + oTargetElement.GetAttribute("type") + "']");
                    if (oSourceElement == null)
                        continue;

                    foreach (XmlAttribute oAtt in oSourceElement.Attributes)
                    {
                        sAttName = oAtt.Name;
                        if (!oTargetElement.HasAttribute(sAttName))
                        {
                            oTargetElement.SetAttribute(sAttName, oAtt.Value);
                        }
                        else if (PV_ATTRLIST.IndexOf("|" + sAttName + "|") < 0)
                        {
                            oTargetElement.SetAttribute(sAttName, oAtt.Value);
                        }
                    }
                }

				//Process attributes for group
				AddNewAttributes4ControlType(p_objSourceDoc, p_objTargetDoc, "group");

				//Process attributes for control elements
				AddNewAttributes4ControlType(p_objSourceDoc, p_objTargetDoc, "control");

				//process attributes for button elements
				AddNewAttributes4ControlType(p_objSourceDoc, p_objTargetDoc, "button");

				//process attributes for buttonscript elements
				AddNewAttributes4ControlType(p_objSourceDoc, p_objTargetDoc, "buttonscript");
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:AddNewAttributes4Controls:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
		}

		/// <summary>
		/// Add new attributes to the target nodelist's each element, or update the atrr
		/// value with the one from default view if these att are not editable in UI
		/// </summary>
		/// <param name="oSourceDoc"></param>
		/// <param name="oTargetDoc"></param>
		/// <param name="sType">xml element type</param>
		private void AddNewAttributes4ControlType(XmlDocument oSourceDoc, XmlDocument oTargetDoc, string sType)
		{
			string sAttName = String.Empty;
			XmlElement oSourceElement = null;
			XmlElement oTargetElement = null;
			string sXmlPath = String.Empty;

			try
			{
				XmlNodeList oNodeList = oTargetDoc.SelectNodes("//" + sType + "[@name]");
				foreach(XmlNode oNode in oNodeList)
				{
					oTargetElement = (XmlElement)oNode;
					sXmlPath = "//" + sType + "[@name='" + oTargetElement.GetAttribute("name") + "']";
					oSourceElement = (XmlElement)oSourceDoc.SelectSingleNode(sXmlPath);
				
					//add new attributes to the target element
					if( oSourceElement != null )
					{
						foreach(XmlAttribute oAtt in oSourceElement.Attributes)
						{
							sAttName = oAtt.Name;
							if( !oTargetElement.HasAttribute(oAtt.Name) )
							{
								oTargetElement.SetAttribute(oAtt.Name, oAtt.Value);
							}
							else if( PV_ATTRLIST.IndexOf("|" + sAttName + "|") < 0)
							{
								oTargetElement.SetAttribute(sAttName, oAtt.Value);
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMXForm:AddNewAttributes4ControlType:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
		}

        /// <summary>
        /// GetTemplate
        /// </summary>
        /// <param name="sFormName"></param>
        /// <param name="iViewId"></param>
        /// <returns></returns>
        private string GetTemplate(string sFormName, int iViewId)
        {
            StringBuilder sXmlIn = new StringBuilder();
            sXmlIn.Append("<form><group><sysrequired/><CommandButtons/><FormFields/><FormList/><ButtonList/><ToolBarButtons/><ToolBarList/>");
            sXmlIn.Append("<PVFormEdit><FormName>");
            sXmlIn.Append(sFormName);
            sXmlIn.Append("</FormName><Reset>0</Reset><Fields/><Captions/><Helps/><ReadOnly/><Buttons/><ButtonCaptions/><Toolbar/><ToolbarCaptions/>");
            sXmlIn.Append("</PVFormEdit><PVList><RowId>");
            sXmlIn.Append(iViewId.ToString());
            sXmlIn.Append("</RowId></PVList><OnlyTopDownLayout>False</OnlyTopDownLayout><ReadOnlyForm>False</ReadOnlyForm>");
            sXmlIn.Append("<IsTopDownLayout>False</IsTopDownLayout><IgnoreScripting>True</IgnoreScripting></group></form>");

            return sXmlIn.ToString();
        }
	}
}
