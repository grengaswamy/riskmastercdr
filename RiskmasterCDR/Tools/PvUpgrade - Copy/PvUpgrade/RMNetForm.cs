using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO; 
using System.Xml;

using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db; 
using Riskmaster.Security;

namespace Riskmaster.Tools.PvUpgrade
{
	/// <summary>
	/// Summary description for RMNetForm.
	/// </summary>
	public class RMNetForm : FormBase
	{
        /// <summary>
        /// RMNetForm
        /// </summary>
        /// <param name="oParent"></param>
        /// <param name="oFormListDoc"></param>
        /// <param name="oTraceListener"></param>
        /// <param name="oTraceSwitch"></param>
		public RMNetForm(PowerView oParent, XmlDocument oFormListDoc, TextWriterTraceListener oTraceListener, TraceSwitch oTraceSwitch)
		{
			m_parent = oParent;
			m_OldFormXmlDoc = new XmlDocument();
			m_NewFormXmlDoc = new XmlDocument();
			m_FormListDoc = oFormListDoc;
			m_TraceListener = oTraceListener;
			m_TraceSwitch = oTraceSwitch;
		}

        /// <summary>
        /// FormName
        /// </summary>
		public override string FormName
		{
			get{ return m_FormName; }
			set
			{ 
				m_FormName = value;
				m_IsPowerViewForm = m_parent.IsPVForm( m_FormName );
				if(m_FormName.EndsWith(".adt"))
				{
					m_IsADT = true;
					m_ADTName = m_FormName;
					m_FormName = "admintracking|" + m_FormName.Substring(0, m_FormName.Length-4) + ".xml";  
				}
				else
					m_IsADT = false;
			}
		}

		/// <summary>
		/// Upgrade RMNet version powerview form to RMX version
		/// </summary>
		public override void UpgradeForm()
		{
			//DOM object of new default form xml plus supp fields
			XmlDocument oListFieldsDoc = null;
			string sXML = string.Empty;

			oListFieldsDoc = new XmlDocument();
			if(!m_IsADT)
			{
				m_DSBaseView.Tables[0].DefaultView.RowFilter="FORM_NAME='" + m_FormName + "'";
				if(m_DSBaseView.Tables[0].DefaultView.Count>0)   
				{
                    sXML = m_DSBaseView.Tables[0].DefaultView[0].Row["VIEW_XML"].ToString();
					oListFieldsDoc.LoadXml( sXML );
					m_NewFormXmlDoc.LoadXml( sXML );
				}
				else
					throw new Exception("Default View not available for View: " + m_ViewName + " Form: " + m_FormName); 
				
				//Supplementals are not part of the view template, so get the supplemental list
				AddSuppDefinition(m_User, m_Password, m_ConnectionString, m_DSN, oListFieldsDoc);
			}
			else//admin tracking table
			{
				oListFieldsDoc = GetDefaultAdmView(m_FormName);
				m_NewFormXmlDoc = GetDefaultAdmView(m_FormName);
			}

			//Clear the structre for the base. Only hidden/id/javascript elements left.
			RemoveAllControls(m_NewFormXmlDoc);

            
            //added BY Navdeep
            AddSuppControl(m_NewFormXmlDoc);
			
            //Add the view id to the form tag
			XmlElement oForm = (XmlElement)m_NewFormXmlDoc.SelectSingleNode("//form");
			oForm.SetAttribute("viewid", m_ViewID.ToString());

			//For Toolbar buttons in the existing view form
			UpdateNetToolbarButtons2X( oListFieldsDoc ); 

			//For controls in the existing view form
			UpdateNetControls2X( oListFieldsDoc );

			//For button controls in the existing form
			UpdateNetButtons2X( oListFieldsDoc );

			//set tab index
			SetTabIndex();
		}

		/// <summary>
		/// Save upgraded powerview form back to database
		/// </summary>
		/// <param name="oCommand"></param>
		public override void SaveUpgradeForm(DbCommand oCommand )
		{
			string sSQL = string.Empty;
			DbParameter objParam = null;

			try
			{
                //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
                //We would be upgrading the existing view id for RMNet too

               //Modified by Navdeep - ADDED DSN ID
//                sSQL = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML) VALUES(" +                  
                sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML=~VXML~ WHERE VIEW_ID=" + m_ViewID + " AND FORM_NAME='" + m_FormName.Replace("'", "''") + "' AND DATA_SOURCE_ID = " + m_DSNID;

				oCommand.Parameters.Clear();
				objParam = oCommand.CreateParameter();
				objParam.Direction=ParameterDirection.Input;
				objParam.Value = m_NewFormXmlDoc.OuterXml ;
				objParam.ParameterName = "VXML";
				objParam.SourceColumn="VIEW_XML";
				oCommand.Parameters.Add(objParam);
				oCommand.CommandText = sSQL;
				oCommand.ExecuteNonQuery();

                // After this Save is done we call the save
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:SaveUpgradeForm:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
		}

		/// <summary>
		/// Upgrade RMNet version control to RMX version
		/// </summary>
		/// <param name="oListFieldsDoc">RMX version form xml (include supp fields)</param>
		private void UpdateNetControls2X(XmlDocument oListFieldsDoc)
		{
			XmlElement objParentElement = null;
			XmlElement objNewElement = null;
			XmlElement objGrpElm = null;
			XmlElement objNewDisp = null;
			XmlNode objBaseCtrlNode = null;
			int iNodeCount = 0;
			int iAcordCount = 0;
			string sCtrlName = string.Empty;
			string sControlType = string.Empty;

			//Check for the controls in each groups
			XmlNodeList objOldCtrlNodeList = null;
			XmlElement objNewGroupElement = null;
			XmlNodeList objOldGroupNodeList = m_OldFormXmlDoc.SelectNodes("//group");
			XmlElement objFormElement = (XmlElement)m_NewFormXmlDoc.SelectSingleNode("//form");
            XmlElement objOldFormElement = (XmlElement)m_OldFormXmlDoc.SelectSingleNode("//form");  //pmittal5 10/29/09 Mits 18376
            //XmlElement abv = null;
			try
			{
                bool bEntFirstTime = true;
                bool bPeopleFirstTime = true;
                //pmittal5 10/29/09 Mits 18376- Copy ReadOnly attribute of form to New Form Xml
                if (objOldFormElement != null && objFormElement != null)
                {
                    if (objOldFormElement.HasAttribute("readonly"))
                    {
                        string sReadOnlyFlag = objOldFormElement.Attributes["readonly"].Value;
                        objFormElement.SetAttribute("readonly", sReadOnlyFlag);
                    }
                }
                //End - pmittal5 
                foreach (XmlNode objOldGroupNode in objOldGroupNodeList)
                {
                    iNodeCount = 0;
                    iAcordCount = 1;               
                    objNewGroupElement = m_NewFormXmlDoc.CreateElement("group");
                   
                    //to be uncommented
                    if (m_NewFormXmlDoc.InnerXml.Contains("entitymaint"))
                    {
                        if (bEntFirstTime)
                        {
                            XmlNode objEntityTableId = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableid']");
                                                        
                            XmlNode objEntityTableTextName = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableidtextname']");

                            XmlNode objEntityTableText = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableidtext']");
                            
                            objOldGroupNode.InnerXml = "<displaycolumn>" + objEntityTableId.OuterXml + "</displaycolumn>" + "<displaycolumn>" + objEntityTableText.OuterXml + "</displaycolumn>" + "<displaycolumn>" + objEntityTableTextName.OuterXml + "</displaycolumn>" + objOldGroupNode.InnerXml;
                            bEntFirstTime = false;
                        }
                    }
                    if (m_NewFormXmlDoc.InnerXml.Contains("people"))
                    {
                        if (bPeopleFirstTime)
                        {
                            XmlNode objEntityTableId = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableid']");
                            XmlNode objEntityTableTextName = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableidtextname']");
                            XmlNode objEntityTableText = oListFieldsDoc.SelectSingleNode("//group//control[@name='entitytableidtext']");
                            objOldGroupNode.InnerXml = "<displaycolumn>" + objEntityTableId.OuterXml + "</displaycolumn>" + "<displaycolumn>" + objEntityTableText.OuterXml + "</displaycolumn>" + "<displaycolumn>" + objEntityTableTextName.OuterXml + "</displaycolumn>" + objOldGroupNode.InnerXml;
                            bPeopleFirstTime = false;
                        }
                    }                    

                    CopyNetNodeAttributs2X((XmlElement)objOldGroupNode, objNewGroupElement);  
						 
					if(m_NewFormXmlDoc.SelectNodes("//group").Count==0)
					{
						//Make the first group selected
						objNewGroupElement.SetAttribute("selected", "1");
						objFormElement.InsertAfter(objNewGroupElement, m_NewFormXmlDoc.SelectSingleNode("//toolbar"));
					}
					else
						objFormElement.InsertAfter(objNewGroupElement, m_NewFormXmlDoc.SelectNodes("//group")[m_NewFormXmlDoc.SelectNodes("//group").Count -1] );
					objGrpElm = objNewGroupElement;

					//Do the loop for each control in the current group
					string sGroupName = ((XmlElement)objOldGroupNode).GetAttribute("name");
					objOldCtrlNodeList = objOldGroupNode.SelectNodes("//group[@name='" + sGroupName + "']//control");
					foreach(XmlNode objOldCtrlNode in objOldCtrlNodeList)
					{
						//No need to copy the "id", "hidden" and "javascript" type of controls. 
						//They must be in the base rmx xml if required.
						sControlType = ((XmlElement)objOldCtrlNode).GetAttribute("type");
						if((sControlType=="id")||(sControlType=="hidden")||(sControlType=="javascript"))
							continue;

						sCtrlName = ((XmlElement)objOldCtrlNode).GetAttribute("name");
						
						//Special case for Acord. Acord controls tags are sequentially mapped. Not based on the control names
						if(((XmlElement)objOldGroupNode).GetAttribute("name")=="acord" || ((XmlElement)objOldGroupNode).GetAttribute("title").ToLower().IndexOf("acord")>=0)
						{
							objBaseCtrlNode = oListFieldsDoc.SelectSingleNode("//group[@name='acorddata']//control[" + iAcordCount + "]");
							if(objBaseCtrlNode == null)
								continue; //can ignore acord as the columns in CLAIM_ACORD_SUPP might not be all as per the xml
							iAcordCount++;
						}
						else
							objBaseCtrlNode = oListFieldsDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");

						//Try to find the control for a mapping file (RMX may change the name of the control)
						if(objBaseCtrlNode == null)
							objBaseCtrlNode = oListFieldsDoc.SelectSingleNode("//control[@name='" + m_parent.GetMappedControl(sCtrlName, m_FormName)  + "']");

						if(objBaseCtrlNode == null)
						{
							//Check for if it's a message type. message type controls are dynamically named
							if( sControlType == MESSAGE_TYPE )
								objBaseCtrlNode = objOldCtrlNode;
                            else if (sControlType == "space")  //pmittal5 10/29/09 Mits 18376 - Space character should be copied as it is from Powerview Xml
                                objBaseCtrlNode = objOldCtrlNode;
							else
							{
								m_TraceListener.WriteLine("RMNetForm:UpdateNetControls2X:: Cannot Map Control:: View: " + m_ViewName + " Form: " + m_FormName + " || Control: " + sCtrlName + "["+ ((XmlElement)objOldCtrlNode).GetAttribute("title") + "]");
								continue;
								//throw new Exception("Cannot Map Control. Form: " + m_FormName + " || Group: " + ((XmlElement)objOldGroupNode).GetAttribute("title") + " || Control: " + sCtrlName + "["+ ((XmlElement)objOldCtrlNode).GetAttribute("title") + "]"+ " -:");
							}
						}
						if( sControlType == "message" || sControlType == "linebreak" )
						{
							// new display column
							objNewDisp = m_NewFormXmlDoc.CreateElement("displaycolumn");
							objGrpElm.AppendChild(objNewDisp);
							objParentElement = objNewDisp;
							iNodeCount = 1;
						}
						if((iNodeCount % 2) == 0)
						{
							// new display column
							objNewDisp = m_NewFormXmlDoc.CreateElement("displaycolumn");
							objGrpElm.AppendChild(objNewDisp);
							objParentElement = objNewDisp;
						}
						objNewElement = CopyDefaultElement2X((XmlElement)objBaseCtrlNode, m_NewFormXmlDoc);
						CopyNetNodeAttributs2X((XmlElement)objOldCtrlNode, objNewElement);						
                        objParentElement.AppendChild(objNewElement);                        
						iNodeCount++;                        
					}	
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:UpdateNetControls2X:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;
			}
		}

		/// <summary>
		/// Upgrade RMNet version of buttons to RMX version
		/// </summary>
		/// <param name="oListFieldDocDoc">RMX version form xml (include supp fields)</param>
		private void UpdateNetButtons2X(XmlDocument oListFieldDocDoc)
		{
			//Bottom buttons
			XmlNodeList objOldButtonList = m_OldFormXmlDoc.SelectNodes("//form/button |//form/buttonscript"); 
			XmlElement objFormElement = (XmlElement)m_NewFormXmlDoc.SelectSingleNode("//form"); 
			string sCtrlName = string.Empty;
			XmlElement objBaseCtrlNode = null;
			string sMappedCtrl = string.Empty;
			string sCtrlType = string.Empty;

			try
			{
				foreach(XmlNode objOldButtonNode in objOldButtonList) 
				{
					sCtrlName = ((XmlElement)objOldButtonNode).GetAttribute("name");
					sCtrlType = ((XmlElement)objOldButtonNode).GetAttribute("type");
					if(sCtrlType=="back" || sCtrlName.ToLower().IndexOf("back")>=0)
						continue; //dont take back buttons. we will get it from rmx base xml.
					//The ones in rmnet caus problems as in rmx the back button has changed to single one

					objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//" + objOldButtonNode.Name + "[@name='" + sCtrlName + "']");
					//Try looking for buttonscript. Buttons in rmnet became buttonscript in rmx, in many cases
					if((objBaseCtrlNode==null) && (objOldButtonNode.Name=="button"))
						objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//buttonscript" + "[@name='" + sCtrlName + "']");

					if(objBaseCtrlNode == null)
					{
						//Check the Global resource mapping first for any entry
						sMappedCtrl = m_parent.GetMappedControl(sCtrlName, m_FormName);
						if( sMappedCtrl != string.Empty)
						{
							objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//" + objOldButtonNode.Name + "[@name='" + sMappedCtrl + "']");
							if(objBaseCtrlNode == null)
								if(objOldButtonNode.Name=="button")//if button is not found.. try buttonsctipt
									objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//buttonscript" + "[@name='" + sMappedCtrl + "']");
							//still not found then error
							if(objBaseCtrlNode ==null)
							{
								m_TraceListener.WriteLine("RMNetForm:UpdateNetButtons2X:: Cannot Map Control:: View: " + m_ViewName + " Form: " + m_FormName + " || Control: " + sCtrlName + "["+ ((XmlElement)objOldButtonNode).GetAttribute("title") + "]");
								continue;
								//throw new Exception("Cannot Map Control :: Form:- " + m_FormName + " || Control:- " + sCtrlName + "["+ ((XmlElement)objOldButtonNode).GetAttribute("title") + "]"+ " -:");
							}
						}
						else
						{
							objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//" + objOldButtonNode.Name + "[@name='" + sCtrlName + "']");
							if(objBaseCtrlNode ==null)
								if(objOldButtonNode.Name == "button")//if button is not found.. try buttonsctipt
									objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//buttonscript" + "[@name='" + sCtrlName + "']");

							//still not found then error
							if(objBaseCtrlNode ==null)
							{
								m_TraceListener.WriteLine("RMNetForm:UpdateNetButons2X:: Cannot Map Control:: View: " + m_ViewName + " Form: " + m_FormName + " || Control: " + sCtrlName + "["+ ((XmlElement)objOldButtonNode).GetAttribute("title") + "]");
								continue;
								//throw new Exception("Cannot Map Control :: Form:- " + m_FormName + " || Control:- " + sCtrlName + "["+ ((XmlElement)objOldButtonNode).GetAttribute("title") + "]"+ " -:");
							}
						}
					}
					
					XmlElement objNewButtonElement = CopyDefaultElement2X((XmlElement)objBaseCtrlNode, m_NewFormXmlDoc);
					CopyNetNodeAttributs2X((XmlElement)objOldButtonNode, objNewButtonElement);
					objFormElement.AppendChild(objNewButtonElement);
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:UpdateNetButtons2X:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;
			}
		}

		/// <summary>
		/// Upgrade RMNet version of toolbar buttons to RMX version
		/// </summary>
		/// <param name="oListFieldDocDoc">RMX version form xml (include supp fields)</param>
		private void UpdateNetToolbarButtons2X(XmlDocument oListFieldDocDoc)
		{
			//toolbar buttons
			XmlNodeList objOldButtonList = m_OldFormXmlDoc.SelectNodes("//toolbar//button"); 
			XmlElement objBaseCtrlNode = null;
			XmlElement objParentElement = null;
			string sCtrlType = string.Empty;

			try
			{
				objParentElement = (XmlElement)m_NewFormXmlDoc.GetElementsByTagName("toolbar").Item(0);
				foreach(XmlNode objOldButtonNode in objOldButtonList) 
				{
					sCtrlType = ((XmlElement)objOldButtonNode).GetAttribute("type");
					objBaseCtrlNode = (XmlElement)oListFieldDocDoc.SelectSingleNode("//toolbar//button[@type='" + sCtrlType + "']");
					if(objBaseCtrlNode == null)
					{
						m_TraceListener.WriteLine("RMNetForm:UpdateNetToolbarButtons2X:: Cannot Map Control:: View: " + m_ViewName + " Form: " + m_FormName + " || Button: " + sCtrlType + "["+ ((XmlElement)objOldButtonNode).GetAttribute("title") + "]");
						continue;
					}
							
					XmlElement objNewButtonElement = CopyDefaultElement2X((XmlElement)objBaseCtrlNode, m_NewFormXmlDoc);
					CopyNetNodeAttributs2X((XmlElement)objOldButtonNode, objNewButtonElement);
					objParentElement.AppendChild(objNewButtonElement);
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:UpdateNetButtons2X:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;
			}
		}

		/// <summary>
		/// Copy RMNet attributes to target upgraded element
		/// </summary>
		/// <param name="p_objFromElm">From element</param>
		/// <param name="p_objToElm">To Element</param>
		private void CopyNetNodeAttributs2X(XmlElement p_objFromElm, XmlElement p_objToElm)
		{
			string sAttName = string.Empty;

			if(p_objFromElm == null || p_objToElm == null)
				return;

			foreach(XmlAttribute objFromAtr in p_objFromElm.Attributes)
			{
				//Only copy the attributes in the list from RMNet to RMX
				sAttName = objFromAtr.Name;				
				if(PV_ATTRLIST.IndexOf("|" + sAttName + "|")>=0)
					p_objToElm.SetAttribute(sAttName, objFromAtr.Value);
			}
		}

		/// <summary>
		/// Copy default element to upgraded form
		/// </summary>
		/// <param name="objSourceElement">RMX default source element</param>
		/// <param name="objTargetDoc">RMX upgraded target element</param>
		/// <returns></returns>
		protected XmlElement CopyDefaultElement2X(XmlElement objSourceElement, XmlDocument objTargetDoc)
		{
            string sAttr = String.Empty;
            string sVal = String.Empty;
            string sCtrlName = String.Empty;
			XmlElement objTargetElement = null;
			XmlNode objXMLTempNode = null;

			if(objSourceElement == null || objTargetDoc == null)
				return null;

			try
			{
				sCtrlName = objSourceElement.Name;
				objTargetElement = objTargetDoc.CreateElement(sCtrlName);
				for(int i = 0; i < objSourceElement.Attributes.Count; i++)
				{
					sAttr = objSourceElement.Attributes[i].Name;
					sVal = objSourceElement.Attributes[i].Value;
					objTargetElement.SetAttribute(sAttr, sVal);
				}

				//copy child nodes. for types like combo boxes
				foreach(XmlNode objXMLNode in objSourceElement.ChildNodes)
				{
					objXMLTempNode = objTargetElement.OwnerDocument.ImportNode(objXMLNode, true);
					objTargetElement.AppendChild(objXMLTempNode);
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:CopyNetElement2X:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}

			return objTargetElement;
		}

        /// <summary>
        /// added BY Navdeep - To add Supplement control for RMNET forms
        /// </summary>
        /// <param name="p_objDoc"></param>
        /// <returns></returns>
        private bool AddSuppControl(XmlDocument p_objDoc)
        {
            ArrayList arrNodes = new ArrayList();
            DbReader oReader = null;
            String SSQL = String.Empty;
            string sTableName = String.Empty;
            string sColName = string.Empty;
            bool m_bIsOracle;
            string m_RMXPageConnectionString = string.Empty;
            
            try
            {                                                
               XmlNode xmlSupp = p_objDoc.SelectSingleNode("//form[@supp]");
               if (xmlSupp != null)
               {
                   sTableName = xmlSupp.Attributes["supp"].Value;
               }                
                if (sTableName.Contains("_SUPP"))
                {
                    m_RMXPageConnectionString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
                    //                using (DbConnection cn = DbFactory.GetDbConnection(m_RMXPageConnectionString))
                    using (DbConnection cn = DbFactory.GetDbConnection(m_ConnectionString))
                    {
                        cn.Open(); // BSB 05.12.2006 Must open to determine underlying dbms if connection string is odbc.
                        m_bIsOracle = (cn.DatabaseType == Riskmaster.Db.eDatabaseType.DBMS_IS_ORACLE);
                        cn.Close();
                    }
                    if (!m_bIsOracle)
                    {
                        SSQL = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' ";
                        SSQL = SSQL + " AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME WHERE Ku.TABLE_NAME = '" + sTableName + "' ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION";
                    }
                    else
                    {
                        SSQL = "SELECT DISTINCT cols.table_name, cols.column_name, cols.position, cons.status FROM all_constraints cons, all_cons_columns cols WHERE cols.table_name = '" + sTableName + "'";
                        SSQL = SSQL + " AND cons.constraint_type = 'P' AND cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner ORDER BY cols.table_name, cols.position";
                    }
                    oReader = DbFactory.GetDbReader(m_ConnectionString, SSQL);
                    while (oReader.Read())
                    {
                        sColName = oReader.GetString("COLUMN_NAME");
                    }
                    oReader.Close();
                    if(sColName != "")
                    {
                        XmlDocument objXML = p_objDoc ;
                        XmlNode objNew;
                        objNew = objXML.CreateElement("control");

                        objNew.Attributes.Append(objXML.CreateAttribute("name"));
                        objNew.Attributes["name"].Value = "supp_" + sColName.ToLower();

                        objNew.Attributes.Append(objXML.CreateAttribute("title"));
                        objNew.Attributes["title"].Value = "";

                        objNew.Attributes.Append(objXML.CreateAttribute("tabindex"));
                        objNew.Attributes["tabindex"].Value = "0";

                        objNew.Attributes.Append(objXML.CreateAttribute("ref"));
                        objNew.Attributes["ref"].Value = "/Instance/*/Supplementals/" + sColName.ToUpper(); ;

                        objNew.Attributes.Append(objXML.CreateAttribute("type"));
                        objNew.Attributes["type"].Value = "id";

                        //objXML.DocumentElement.AppendChild(objNew);
                        p_objDoc.DocumentElement.AppendChild(objNew); 
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_TraceListener.WriteLine("RMNetForm:RMNetForm:AddSuppControl: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
                throw e;
            }
        }

		/// <summary>
		/// 1. Make all id/javascript/hidden controls to be form's children
		/// 2. Remove all group controls
		/// 3. Remove all button controls (Except "back" button)
		/// </summary>
		/// <param name="p_objDoc">The new form XML from default view</param>
		/// <returns></returns>
		private bool RemoveAllControls(XmlDocument p_objDoc)
		{
			XmlElement objElemForm  =null;
			XmlNodeList objNodeList = null;
			ArrayList arrNodes = new ArrayList();

			try
			{
				if(p_objDoc == null)
					return false;

				//remove id/hidden/js from inside the group and displaycolumn, and
				//Add them as children of form element
				objElemForm =(XmlElement)p_objDoc.GetElementsByTagName("form").Item(0);
                objNodeList = p_objDoc.SelectNodes("//control[@type='id'] | //control[@type='javascript'] | //control[@type='hidden']");
				foreach(XmlNode objNode in objNodeList)
				{
					arrNodes.Add(objNode.ParentNode.RemoveChild(objNode));  
				}

				for(int i = 0; i <= arrNodes.Count - 1; i++)
					objElemForm.AppendChild((XmlNode)arrNodes[i]);


                ////remove all groups
                objNodeList = p_objDoc.SelectNodes("//group");
                foreach (XmlNode objNode in objNodeList)
                    objNode.ParentNode.RemoveChild(objNode);

                //remove all buttons
				objNodeList = p_objDoc.SelectNodes("//button | //buttonscript");
				foreach(XmlNode objNode in objNodeList)
				{
					//Do not remove the Back buttons. Use the one in the rmx template
					if(((XmlElement)objNode).GetAttribute("type")!="back")
						objNode.ParentNode.RemoveChild(objNode);  
				}
				return true;
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("RMNetForm:RMNetForm:RemoveAllControls:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
			finally
			{
				objElemForm=null;
				objNodeList = null;
			}
		}
	}
}
