﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Configuration;

namespace OverdueDiaryProcessing
{
    class Program
    {
       public static void Main(string[] args)
        {
            Boolean bCon = false;
            UserLogin oUserLogin = null;
            string sSQL = string.Empty;
            int iOverDueDays = 0;
            string sEmail = string.Empty;
            Mailer objMail = null;
            string sEmailAddr = string.Empty;
            WpaDiaryEntry objWPA = null;
            DataModelFactory objDMF = null;
            string sSMTP = string.Empty;
            string sAdminEmailAdd = string.Empty;
            string sEntryIds = string.Empty;
            DbConnection objConn = null;
            int iClientId = 0;//Add by kuladeep for Jira-1126
            bool blnSuccess = false;
            try
            {
                //args = new string[8];
                //args[0] = "rmAcloud";
                //args[1] = "cloud";
                //args[3] = "-1";
                //args[4] = "false";
                //args[5] = "false";
                //args[6] = "true";
                //args[7] = "0";//Client Id as per Requirements Add by kuladeep for Jira-1126

                //Add & Change by kuladeep for Cloud----Start
                if (args.Length > 7 && (string.IsNullOrEmpty(args[7]) == false))
                {

                    iClientId = Convert.ToInt32(args[7]);
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by kuladeep for Cloud----End

                oUserLogin = new UserLogin(args[1], args[0], iClientId);//Add by kuladeep for Jira-1126
          
                Console.WriteLine("0 ^*^*^ OverDueDiaryProcessing started ");


                if (oUserLogin.DatabaseId <= 0)
                {
                    Console.WriteLine("0 ^*^*^ Authentication Failed");
                    throw new Exception("Authentication failure.");
                }
                if (!string.IsNullOrEmpty(args[3]))
                    iOverDueDays = Conversion.CastToType<int>(args[3], out bCon);
                else
                    iOverDueDays = 0;
                sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE AUTO_LATE_NOTIFY=-1 AND NOTIFY_FLAG=0 AND DIARY_VOID = 0 ";


                switch (oUserLogin.objRiskmasterDatabase.DbType)
                {
                    case Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR:
                        sSQL = sSQL + " AND ASSIGNING_USER <> ISNULL(ASSIGNED_USER,' ')";
                        sSQL = sSQL + " AND DATEDIFF(dd,SUBSTRING(ISNULL(COMPLETE_DATE,'" + System.DateTime.Now.Date.ToString("yyyyMMdd") + "'),5,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + System.DateTime.Now.Date.ToString("yyyyMMdd") + "'),7,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + System.DateTime.Now.Date.ToString("yyyyMMdd") + "'),1,4),'" + System.DateTime.Now.Date.ToString("yyyyMMdd") + "') >= " + iOverDueDays;
                        break;
                    case Riskmaster.Db.eDatabaseType.DBMS_IS_ORACLE:
                        sSQL = sSQL + " AND ASSIGNING_USER <> nvl(ASSIGNED_USER,' ')";
                        sSQL = sSQL + " AND (TO_DATE('" + System.DateTime.Now.Date.ToString("MMddyyyy") + "','MM/DD/YYYY')-DTGToDate(COMPLETE_DATE)) >= " + iOverDueDays;
                        break;
                }
                if (args[5].Equals("true", StringComparison.InvariantCultureIgnoreCase) || args[6].Equals("true", StringComparison.InvariantCultureIgnoreCase))
                {
                    DataTable dtSMTPSettings = RMConfigurationSettings.GetSMTPServerSettings(iClientId);//Add by kuladeep for Jira-1126

                    sSMTP = dtSMTPSettings.Rows[0]["SMTP_SERVER"].ToString();
                    sAdminEmailAdd = dtSMTPSettings.Rows[0]["ADMIN_EMAIL_ADDR"].ToString();
                    if ((string.IsNullOrEmpty(sAdminEmailAdd) && string.IsNullOrEmpty(sSMTP)))
                    {
                        Console.WriteLine("0 ^*^*^ SMTP Server Not Configured");
                        throw new Exception("SMTP Server not configured");
                    }
                }

                using (DbReader objReader = DbFactory.GetDbReader(oUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        if (args[4].Equals("true", StringComparison.InvariantCultureIgnoreCase) || args[6].Equals("true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            objDMF = new DataModelFactory(oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.LoginName, oUserLogin.Password, iClientId);//Add by kuladeep for Jira-1126
                            objWPA = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
                            objWPA.AttFormCode = 0;
                            objWPA.AttFormScrty = 0;
                            objWPA.AttParentCode = 0;
                            objWPA.AttSecRecId = 0;
                            objWPA.AutoId = 0;
                            objWPA.DiaryDeleted = false;
                            objWPA.DiaryVoid = false;
                            objWPA.EstimateTime = 0;
                            objWPA.NotifyFlag = false;
                            objWPA.RouteFlag = false;
                            objWPA.EntryName = "OVERDUE DIARY NOTIFICATION " + objReader.GetString("ENTRY_NAME");
                            objWPA.EntryNotes = "OVERDUE DIARY NOTIFICATION " + objReader.GetString("ENTRY_NOTES");
                            objWPA.CreateDate = System.DateTime.Now.ToString();
                            objWPA.CompleteTime = Conversion.GetTime(System.DateTime.Now.TimeOfDay.ToString());
                            objWPA.CompleteDate = System.DateTime.Now.ToString("yyyyMMdd");
                            objWPA.Priority = 1;
                            objWPA.StatusOpen = true;
                            objWPA.AutoConfirm = false;
                            objWPA.ResponseDate = null;
                            objWPA.Response = null;
                            objWPA.AssignedUser = objReader.GetString("ASSIGNING_USER");
                            objWPA.AssigningUser = "SYSTEM";
                            objWPA.AssignedGroup = "NA";
                            objWPA.IsAttached = false;
                            objWPA.AttachTable = null;
                            objWPA.AttachRecordid = 0;
                            objWPA.Regarding = objReader.GetString("REGARDING");
                            objWPA.TeTracked = objReader.GetBoolean("TE_TRACKED");
                            objWPA.TeStartTime = objReader.GetString("TE_START_TIME");
                            objWPA.TeEndTime = objReader.GetString("TE_END_TIME");
                            objWPA.TeTotalHours = objReader.GetDouble("TE_TOTAL_HOURS");
                            objWPA.TeExpenses = objReader.GetDouble("TE_EXPENSES");
                            objWPA.CompletedByUser = null;
                            objWPA.AutoLateNotify = false;
                            objWPA.Save();
                            Console.WriteLine("0 ^*^*^ Diary created");
                            objWPA = null;
                        }

                        if (args[5].Equals("true", StringComparison.InvariantCultureIgnoreCase) || args[6].Equals("true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            sEmailAddr = GetEmailByLoginName(oUserLogin.LoginName, SecurityDatabase.GetSecurityDsn(iClientId));//Add by kuladeep for Jira-1126
                            objMail = new Mailer(iClientId);
                            objMail.To = sEmailAddr;
                            objMail.From = sAdminEmailAdd;
                            objMail.Subject = "OverDue Diary";
                            objMail.IsBodyHtml = true;
                            objMail.Body = GetEmailNotificationHtmlBody(objReader.GetInt("ENTRY_ID"), oUserLogin, iClientId);//Add by kuladeep for Jira-1126
                            objMail.SendMail();
                            Console.WriteLine("0 ^*^*^ Mail Sent");
                            objMail = null;
                        }

                        if (string.IsNullOrEmpty(sEntryIds))
                            sEntryIds = objReader.GetInt("ENTRY_ID").ToString();
                        else
                            sEntryIds = sEntryIds + "," + objReader.GetInt("ENTRY_ID").ToString();


                    }
                }

                if (!string.IsNullOrEmpty(sEntryIds))
                {
                    sSQL = "UPDATE WPA_DIARY_ENTRY SET NOTIFY_FLAG = -1 WHERE ENTRY_ID IN (" + sEntryIds + ")";
                }

                objConn = DbFactory.GetDbConnection(oUserLogin.objRiskmasterDatabase.ConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                Console.WriteLine("0 ^*^*^ Diaries Processed");

            }

            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                bool rethrow = ExceptionPolicy.HandleException(exc, "Logging Policy");

            }
            finally
            {
                if (objWPA != null)
                {
                    objWPA.Dispose();
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                }
                if (objMail != null)
                {
                    objMail.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                oUserLogin = null;
            }
        }

       private static string GetEmailNotificationHtmlBody(int p_iEntryId, UserLogin p_oUserLogin, int p_iClientId)//Add by kuladeep for Jira-1126
        {
            DataModelFactory objDMF = new DataModelFactory(p_oUserLogin, p_iClientId);//Add by kuladeep for Jira-1126
            WpaDiaryEntry objWPA = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
            string sSQL = string.Empty;

            string priority = string.Empty;
            StringBuilder sb = new StringBuilder("<html><body><table>");
            List<string> values = new List<string>();
            string[] arrActivity = null;
            try
            {
                objWPA.MoveTo(p_iEntryId);
                const string rowFormat = "<tr><td width=\"20%\" style=\"text-align:left;width:20%;background-color:#f5f5f5;\">{0}</td><td width=\"80%\" style=\"text-align:left;width:80%;background-color:#f5f5f5;font-weight:bold;\">{1}</td></tr>";



                sb.Append("<tr><td bgcolor=\"#D5CDA4\" colspan=\"2\">This diary has been assigned to " + objWPA.AssignedUser + ". Task details below.</td></tr>");

                sb.Append(String.Format(rowFormat, "Task Name:", objWPA.EntryName));
                sb.Append(String.Format(rowFormat, "Response:", objWPA.Response));


                sb.Append(String.Format(rowFormat, "Created On:", Conversion.GetDBDateFormat(objWPA.CreateDate, "d")));

                sSQL = "SELECT ACT_TEXT FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + p_iEntryId;
                using (DbReader rdr = DbFactory.GetDbReader(p_oUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (rdr.Read())
                    {
                        values.Add(rdr.GetValue(0) == null ? string.Empty : rdr.GetValue(0).ToString());
                    }

                }
                if (values.Count > 0)
                {
                    arrActivity = values.ToArray();
                }


                if (arrActivity != null)
                {
                    StringBuilder activities = new StringBuilder("<ul>");
                    foreach (string activity in arrActivity)
                    {
                        activities.Append("<li>" + activity + "</li>");
                    }
                    activities.Append("</ul>");
                    sb.Append(String.Format(rowFormat, "Work Activities", activities.ToString()));
                }
                else
                {
                    sb.Append(String.Format(rowFormat, "Work Activities", string.Empty));
                }

                sb.Append(String.Format(rowFormat, "Estimate Time:", Convert.ToString(objWPA.EstimateTime) + "(Hours)"));

                switch (objWPA.Priority.ToString())
                {
                    case "1":
                        priority = "Optional";
                        break;
                    case "2":
                        priority = "Important";
                        break;
                    case "3":
                        priority = "Required";
                        break;
                }
                sb.Append(String.Format(rowFormat, "Priority:", priority));
                sb.Append(String.Format(rowFormat, "Notes:", objWPA.EntryNotes));
                sb.Append(String.Format(rowFormat, "Regarding:", objWPA.Regarding));

                sb.Append(String.Format(rowFormat, "Due Date:", Conversion.GetDBDateFormat(objWPA.CompleteDate, "d")));
                sb.Append("</table></body></html>");


            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Couldnot Write Mail Body.");
                throw new Exception("Couldnot Write Mail Body.");
            }
            finally
            {
                if (objDMF != null)
                {
                    objDMF.Dispose();
                }
                if (objWPA != null)
                {
                    objWPA.Dispose();
                }

            }
            return sb.ToString();
        }


        private static string GetEmailByLoginName(string p_sUserName, string p_sConn)
        {


            string sTmp = string.Empty;
            string sEmailAddr = string.Empty;
            string sTemp = string.Empty;

            string sSQL = string.Empty;
            string sDSN = string.Empty;
            try
            {

                sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" + p_sUserName + "'";
                using (DbReader objReader = DbFactory.GetDbReader(p_sConn, sSQL))
                {
                    if (objReader.Read())
                        sEmailAddr = objReader.GetString("EMAIL_ADDR");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Email Id of Assigning User" + p_sUserName + " Not Found.");
                throw new Exception("Email Id of Assigning User" + p_sUserName + " Not Found.");
            }

            return sEmailAddr;
        }

    }

}

