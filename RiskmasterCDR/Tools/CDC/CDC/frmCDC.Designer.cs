﻿namespace Riskmaster.Tools.CDC
{
    public partial class frmCDC
    {
        private DevComponents.DotNetBar.WizardPage wpDSNSelection;
        private DevComponents.DotNetBar.WizardPage wpConfirmation;
        private System.Windows.Forms.ListBox lstDsnList;
        private DevComponents.DotNetBar.LabelX labelX1;
        public DevComponents.DotNetBar.Wizard wizard1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX labelX8;
        public DevComponents.DotNetBar.Controls.ListViewEx lvDatabases;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.LabelX lblWarningWPDS;
        private DevComponents.DotNetBar.WizardPage wpStatus;
        public DevComponents.DotNetBar.Controls.ProgressBarX progressBarCDC;
        public DevComponents.DotNetBar.LabelX labelX18;
        public DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ColumnHeader dsn;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        public DevComponents.DotNetBar.LabelX lblError;
        private DevComponents.DotNetBar.WizardPage wizardAdminCredentials;
        private System.Windows.Forms.TextBox txtAdminDBId;
        private System.Windows.Forms.TextBox txtAdminDBPwd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.LabelX labelX4;
        public DevComponents.DotNetBar.LabelX labelSubStatus;
        public System.Windows.Forms.Timer timerCDC;
        private System.ComponentModel.IContainer components;
    }
}