﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using Riskmaster.Db;
using System.Configuration;
using System.Collections.Specialized;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Text.RegularExpressions;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Tools.CDC
{

    #region Common Class
    internal class Common
    {
        #region Internal Method

        /// <summary>
        /// Get User Id and Password from Connection
        /// string.
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        /// <param name="p_sUId">User Id</param>
        /// <param name="p_sPwd">Password</param>
        internal static void GetUIdPwdFromConString(string p_sConnString, ref string p_sUId, ref string p_sPwd)
        {
            string sConnect = string.Empty;
            int iPos1 = 0;
            int iPos2 = 0;

            try
            {

                if (p_sConnString == "")
                {
                    p_sUId = "";
                    p_sPwd = "";
                }
                else
                {
                    sConnect = p_sConnString;
                    iPos1 = sConnect.IndexOf("UID=");
                    iPos2 = sConnect.IndexOf(";", iPos1);
                    p_sUId = sConnect.Substring(iPos1 + 4, iPos2 - iPos1 - 4);
                    iPos1 = sConnect.IndexOf("PWD=");
                    iPos2 = sConnect.IndexOf(";", iPos1);
                    if (iPos2 == -1)
                        iPos2 = sConnect.Length + 1;
                    p_sPwd = sConnect.Substring(iPos1 + 4, iPos2 - iPos1 - 4);
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonGetUIdPwdFromConStringErr, objEx);
            }

        }

        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLRoleMember(string strConnString, string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helprolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsDBO = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;

            try
            {
                //Parses out the User ID and Pwd from the database connection string
                GetUIdPwdFromConString(strConnString, ref strUserName, ref strPassword);

                //Get a DataReader object
                using (DbReader rdr = DbFactory.GetDbReader(strConnString, SQL_ROLE_NAME))
                {
                    //Loop through all of the records
                    while (rdr.Read())
                    {
                        string strMemberName = rdr[MEMBER_NAME_COLUMN].ToString();

                        //in case of sa user , member name will be dbo
                        if (strUserName.ToLower() == "sa" && strMemberName.ToLower() == "dbo")
                        {
                            blnIsDBO = true;
                            break;
                        }
                        else
                        {
                            //Check if the member name matches
                            if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                            {
                                blnIsDBO = true;
                                break;
                            }//if
                        }//if
                    }//while
                }//using
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonIsSQLRoleMemberErr, objEx);
            }
            return blnIsDBO;
        }//method: IsSQLRoleMember()

        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server server-level role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLServerRoleMember(string strConnString, string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helpsrvrolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsSysAdmin = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;
            try
            {
                //Parses out the User ID and Pwd from the database connection string
                GetUIdPwdFromConString(strConnString, ref strUserName, ref strPassword);

                //Get a DataReader object
                using (DbReader rdr = DbFactory.GetDbReader(strConnString, SQL_ROLE_NAME))
                {
                    //Loop through all of the records
                    while (rdr.Read())
                    {
                        string strMemberName = rdr[MEMBER_NAME_COLUMN].ToString();

                        //Check if the member name matches
                        //Nadim removing Case sensitivity-start
                        if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                        {
                            blnIsSysAdmin = true;
                            break;
                        }//if
                        //Nadim removing Case sensitivity-end
                    }//while
                }//using
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonIsSQLServerRoleMemberErr, objEx);
            }
            return blnIsSysAdmin;
        }//method: IsSQLServerRoleMember()

        /// <summary>
        /// Get Base connection string by removing UID and PWD
        /// </summary>
        /// <param name="strConnectionString">Connection String</param>
        /// <returns>Connection String without UID and PWD </returns>
        internal static string GetBaseConnectionString(string strConnectionString)
        {
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string GROUP_NAME = "CONN_STR_VALUE";

            string strMatchValue = string.Empty;
            try
            {
                //Get the Match for the string
                Match regExMatch = Regex.Match(strConnectionString, strRegExpr, RegexOptions.IgnoreCase);

                //Get the matching value for the specified group name
                strMatchValue = regExMatch.Groups[GROUP_NAME].Value;
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonGetBaseConnectionStringErr, objEx);
            }

            return strMatchValue;
        }

        /// <summary>
        /// Create Admin connection string.
        /// </summary>
        /// <param name="p_sAdminUserName">Admin DbUser Name</param>
        /// <param name="p_sAdminPassword">Admin DbUser Password</param>
        internal static void CreateAdminConnStr(string p_sAdminUserName, string p_sAdminPassword)
        {
            string sDbConnstring = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(p_sAdminUserName) && !string.IsNullOrEmpty(p_sAdminPassword))
                {

                    sDbConnstring = GetBaseConnectionString(AppGlobal.RMDBConnstring);
                    AppGlobal.RMAdminConnstring = sDbConnstring + "UID=" + p_sAdminUserName + ";PWD=" + p_sAdminPassword + ";";

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonCreateAdminConnStrErr, objEx);
            }
            
        }

        internal static string GetValueFromConfigFile(string p_sTagName)
        {
            return RMConfigurationManager.GetNameValueSectionSettings("HistoryTracking", AppGlobal.RMDBConnstring, AppGlobal.ClientId)[p_sTagName];
        }
        /// <summary>
        /// Get list of Columns with datatype
        /// </summary>
        /// <param name="p_objTable">Table Object</param>
        /// <returns></returns>

        internal static string GetColumnTypeList(Table p_objTable)
        {
            string sColumnName = string.Empty;
            string sDataType = string.Empty;
            StringBuilder sbClmnTypeLst = null;
            try
            {
                sbClmnTypeLst = new StringBuilder();

                foreach (ColoumnDetails Column in p_objTable.ColoumnList)
                {
                    sColumnName = Column.sName;
                    //in case of oracle database we need Column name with datatyp
                    if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sbClmnTypeLst.AppendFormat("{0}", sColumnName);
                        sDataType = Column.sType;
                        sbClmnTypeLst.Append(" ");
                        sbClmnTypeLst.Append(sDataType);
                        //if datatype is varchar or varchar2 add just length of the varchar 
                        if (sDataType.ToLower() == "varchar2" || sDataType.ToLower() == "varchar")
                        {
                            sbClmnTypeLst.AppendFormat("({0}),", Column.iSize.ToString());

                        }
                        //in case of NUMBER there may be scale and precision
                        else
                        {
                            if (Column.iNumPrecision != 0)
                            {
                                sbClmnTypeLst.AppendFormat("({0},{1}),", Column.iNumPrecision.ToString(), Column.iNumScale.ToString());

                            }
                            else
                            {
                                sbClmnTypeLst.Append(",");
                            }
                        }


                    }
                    //in case of sql database we don't need datatype
                    else
                    {
                        
                        sbClmnTypeLst.AppendFormat("{0},", sColumnName);
                    }


                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.CommonGetColumnTypeListErr, p_objTable.TableName.ToUpper()), objEx);
            }
            return sbClmnTypeLst.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Convert connection string(from config files)  to native form
        /// </summary>
        /// <param name="sConnectionString"></param>
        /// <returns>native connection string</returns>
        internal static string TranslateConnectionStringToNative(string sConnectionString)
        {
            string strParsedConnectionString = string.Empty, strProviderConnectionString = string.Empty;

            strParsedConnectionString = RemoveNameValuePair(sConnectionString, "driver");
            string ORCL_REGEX = "(Server=(?<TNS_NAME>[^;]+);Dbq=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)|(DBQ=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)";
            string ORCL_REPLACE_REGEX = "Data Source=${TNS_NAME};User Id=${UID};Password=${PWD};";

            switch (AppGlobal.DatabaseType) //Translate for the appropriate target DBMS
            {
                case eDatabaseType.DBMS_IS_SQLSRVR:
                    strProviderConnectionString = ReplaceItemName(strParsedConnectionString, "UID", "User ID");
                    break;
                case eDatabaseType.DBMS_IS_ORACLE:
                    strProviderConnectionString = Regex.Replace(strParsedConnectionString, ORCL_REGEX, ORCL_REPLACE_REGEX, RegexOptions.IgnoreCase);
                    break;
                default:
                    strProviderConnectionString = sConnectionString;
                    break;
            }

            return strProviderConnectionString;

        }
      
        internal static void StartAutoProgreeBar()
        {
            frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;
            myForm.timerCDC.Tick += new System.EventHandler(myForm.timerCDC_Tick);

            myForm.progressBarCDC.Maximum = 20;
            myForm.timerCDC.Interval = 1000;
            
            myForm.progressBarCDC.Step = 1;
            myForm.progressBarCDC.Value = 0;
            myForm.timerCDC.Start();

        }

        internal static void StopAutoProgreeBar()
        {
            frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;
            myForm.timerCDC.Stop();
            myForm.progressBarCDC.Value = 0;

        }

        internal static void DisplayMessage(string strMessage, Mode CurrentMode)
        {
            if (AppGlobal.IsSilentMode == true)
            {
                if(CurrentMode == Mode.Status)
                    Console.WriteLine("0 ^*^*^" + strMessage);
                else
                    Console.WriteLine("1001 ^*^*^" + strMessage);

            }
            else
            {
                UpdateStatus(strMessage, CurrentMode);
            }
        }

        /// <summary>
        /// update the overall progress bar on process page
        /// </summary>
        /// <param name="iMaximum"></param>
        internal static void SetProgressBarProperties(int iMaximum)
        {
            frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;

            if (myForm != null)
            {
                myForm.progressBarCDC.Maximum = iMaximum;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        /// <summary>
        /// update the progress bar on process page
        /// </summary>
        /// <param name="iValue"></param>
        internal static void UpdateProgressBar(int iValue)
        {
            frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;

            if (myForm != null)
            {
                myForm.progressBarCDC.Value = iValue;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        /// <summary>
        /// Update the status
        /// </summary>
        /// <param name="strLabelText">string containing the label text</param>
        /// <param name="blnUpdateProgressBar">boolean indicating whether or not to update the progress bar</param>
        internal static void UpdateStatus(string strLabelText, bool blnUpdateProgressBar, Mode CurrentMode)
        {
            frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;
            if (myForm != null)
            {
                switch (CurrentMode)
                {
                    case Mode.Status:
                        myForm.lblStatus.Text = strLabelText;
                        break;
                    case Mode.Error:
                        myForm.labelX18.Text = strLabelText;
                        myForm.labelX18.ForeColor = System.Drawing.Color.Red;
                        myForm.labelX18.WordWrap = true;
                        break;
                }

                if (blnUpdateProgressBar)
                {
                    myForm.progressBarCDC.PerformStep();
                } // if

                myForm.Refresh();
                System.Windows.Forms.Application.DoEvents();
            }

        } // method: UpdateStatus

        /// <summary>
        /// Update the status
        /// </summary>
        /// <param name="strLabelText"></param>
        /// <param name="CurrentMode"></param>
        internal static void UpdateStatus(string strLabelText, Mode CurrentMode)
        {
            UpdateStatus(strLabelText, true, CurrentMode);
        }

        /// <summary>
        /// Write error log file.
        /// Name of error log file is HistoryTrackingLog.log
        /// </summary>
        /// <param name="exc">Exception Object</param>
        internal static void WriteErrorLog(Exception exc, bool bAbortprocess)
        {
            if (bAbortprocess)
            {
                AppGlobal.IsError = true;
            }
            DisplayMessage("", Mode.Status);
            while (exc != null)
            {
                LogItem oLogItem = new LogItem();
                oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                oLogItem.Category = "HistoryTrackingLog";

                // ... dump exception info to log
                oLogItem.RMParamList.Add("Exception Source", exc.Source);
                oLogItem.RMParamList.Add("Exception Type", exc.GetType().Name);
                oLogItem.RMParamList.Add("Exception Message", exc.Message);
                oLogItem.RMParamList.Add("Exception StackTrace", exc.StackTrace);

                // ... flush entry to log file
                Log.Write(oLogItem,AppGlobal.ClientId);
                exc = exc.InnerException;

            }
        }
             
        /// <summary>
        /// Get list of Columns and their detail for 
        /// given table.
        /// </summary>
        /// <param name="iTableId">Table id</param>
        /// <returns></returns>
        internal static List<ColoumnDetails>GetColoumnList(int iTableId)
        {
            List<ColoumnDetails> lstColmnList = null;
            StringBuilder strSQL = new StringBuilder();
            ColoumnDetails objColumn = new ColoumnDetails();
            try
            {
                strSQL.Append("SELECT HD.COLUMN_ID,HD.COLUMN_NAME,HD.DATATYPE,HD.COLUMN_SIZE,HD.NUMERIC_PRECISION,HD.NUMERIC_SCALE ");
                strSQL.Append("FROM HIST_TRACK_DICTIONARY HD INNER JOIN HIST_TRACK_COLUMNS HC ON HD.COLUMN_ID = HC.COLUMN_ID ");
                strSQL.Append(string.Format(" WHERE HD.TABLE_ID= {0} ", iTableId));

                using (DbReader objDbReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, strSQL.ToString()))
                {
                    if (objDbReader != null)
                    {
                        lstColmnList = new List<ColoumnDetails>();

                        while (objDbReader.Read())
                        {
                            objColumn.sName = objDbReader.GetString("COLUMN_NAME");
                            objColumn.sType = objDbReader.GetString("DATATYPE");
                            objColumn.iClmnID = objDbReader.GetInt32("COLUMN_ID");
                            objColumn.iSize = objDbReader.GetInt32("COLUMN_SIZE");
                            objColumn.iNumPrecision = objDbReader.GetInt32("NUMERIC_PRECISION");
                            objColumn.iNumScale = objDbReader.GetInt32("NUMERIC_SCALE");

                            lstColmnList.Add(objColumn);
                        }
                    }
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.CommonGetColoumnListErr,iTableId), objEx);
            }
            return lstColmnList;
            }    
      
        /// <summary>
        /// soft delete a record of HIST_TRACK_PURGE
        /// </summary>
        /// <param name="p_iRecordId">record id</param>
       
        internal static void DeleteRecordofPurgeHistory(int p_iRecordId)
        {
            string sSql = string.Empty;
            
            try
            {
               using( DbConnection objDbconnection = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring))
               {
                 objDbconnection.Open();
                sSql = "UPDATE HIST_TRACK_PURGE SET DELETED_FLAG = -1 WHERE RECORD_ID =" + p_iRecordId;
                objDbconnection.ExecuteNonQuery(sSql);
                objDbconnection.Close();
               }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonDeleteRecordofPurgeHistoryErr, objEx);
            }
           
        }
     
        /// <summary>
        /// Get Database ID
        /// </summary>
        /// <returns>DatabaseId</returns>
        internal static void GetDatabaseNameAndID()
        {
            string sDbName = string.Empty;
            string sSql = string.Empty;
            int iDbId = 0;
            int iPos1;
            int iPos2;           
            DbConnection objDbConn = null;
            try
            {

                objDbConn = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring);
                objDbConn.Open();
                if ((AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                {


                    AppGlobal.RMDatabaseName = objDbConn.Database;
                    sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + AppGlobal.RMDatabaseName + "'";
                    iDbId = objDbConn.ExecuteInt(sSql);
                    AppGlobal.RMDatabaseId = iDbId;
                }
                else
                {
                    iPos1 = AppGlobal.RMDBConnstring.IndexOf("UID=");
                    iPos2 = AppGlobal.RMDBConnstring.IndexOf("PWD=");
                    sDbName = AppGlobal.RMDBConnstring.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                    sSql = "SELECT USER_ID FROM USER_USERS WHERE USERNAME='" + sDbName.Trim().ToUpper() + "'";
                    iDbId = objDbConn.ExecuteInt(sSql);
                    AppGlobal.RMDatabaseId = iDbId;
                }

                objDbConn.Close();

            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {//ResXFileCodeGenerator

                throw new RMAppException(Message.CommonGetDatabaseIDErr, objEx);
            }

            finally
            {
                if (objDbConn != null)
                    objDbConn.Close();
            }

           
        }
       
        /// <summary>
        /// List of Columns
        /// </summary>
        /// <param name="p_objTable">Table Object</param>
        /// <returns></returns>
        internal static string ConcatenateSourceColumns(Table p_objTable)
        {
            StringBuilder sbSql = null;
            string str = string.Empty;
            char seperator = ',';
            sbSql = new StringBuilder();
            if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sbSql.Append("SELECT COLUMN_NAME FROM DBA_PUBLISHED_COLUMNS ");
                sbSql.AppendFormat(" WHERE SOURCE_TABLE_NAME ='{0}' AND CHANGE_TABLE_NAME = 'HIST_{0}_CT' ", p_objTable.TableName.ToUpper());
                sbSql.AppendFormat(" AND SOURCE_SCHEMA_NAME ='{0}'", AppGlobal.DBOUserId.ToUpper());

            }
            else
            {
                sbSql.Append("SELECT CC.COLUMN_NAME FROM CDC.CAPTURED_COLUMNS CC INNER JOIN CDC.CHANGE_TABLES CT ");
                sbSql.AppendFormat(" ON CC.OBJECT_ID = CT.OBJECT_ID AND CT.CAPTURE_INSTANCE ='HIST_{0}'",p_objTable.TableName.ToUpper());
            }

            using(DbReader objReader = DbFactory.GetDbReader(AppGlobal.RMAdminConnstring,sbSql.ToString()))
            {
            
                while(objReader.Read())
                {
                    str += objReader.GetString(0).ToUpper() + seperator;
                }
            }
            return str.TrimEnd(seperator);
        }

        /// <summary>
        /// List of Column Id
        /// </summary>
        /// <param name="p_objTable">Table Object</param>
        /// <returns></returns>
        internal static string ConcatenateColumnsId(Table p_objTable)
        {
            
            string str = string.Empty;
            char seperator = ',';

            foreach(ColoumnDetails objClmDetail in p_objTable.ColoumnList)
            {
                str += objClmDetail.iClmnID.ToString() + seperator;
            }
            
            return str.TrimEnd(seperator);
        }

        /// <summary>
        /// Get sql statement to fetch record from change table.
        /// </summary>
        /// <param name="p_objtable">Table Object</param>
        /// <param name="p_iMaxNumRecordToRead">maximum number of record to be fetched</param>
        /// <returns></returns>
        internal static string GetSourceDataSql(Table p_objtable, int p_iMaxNumRecordToRead)
        {
            StringBuilder sbSql = null;
            string sSourceColumns =string.Empty;
            try
            {
                sbSql = new StringBuilder();
                sSourceColumns = ConcatenateSourceColumns(p_objtable);
                if (!string.IsNullOrEmpty(sSourceColumns))
                {
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sbSql.Append("SELECT  OPERATION$ OPERATION, "); 
                    sbSql.Append(" TO_CHAR(COMMIT_TIMESTAMP$,'yyyymmddHHMISS') DATE_TIMESTAMP, ");
                    if(string.IsNullOrEmpty(sSourceColumns))
                        sbSql.Append(" RSID$ ,OPERATION$ ");
                    else
                        sbSql.AppendFormat(" {0},RSID$ ,OPERATION$ ", sSourceColumns);
                    sbSql.AppendFormat(" FROM HIST_{0}_CT WHERE TO_CHAR(COMMIT_TIMESTAMP$,'yyyymmddHHMISS') <= {1} AND ROWNUM < {2}", p_objtable.TableName, Conversion.ToDbDateTime(DateTime.Now), p_iMaxNumRecordToRead);
                }
                else
                {
                    sbSql.AppendFormat("SELECT TOP {0} CASE __$OPERATION WHEN  1 THEN 'D' ", p_iMaxNumRecordToRead);
                    sbSql.Append("WHEN  2 THEN 'I' ");
                    sbSql.Append("WHEN  3 THEN 'UO' ");
                    sbSql.Append("ELSE  'UN' END OPERATION , REPLACE(CONVERT(VARCHAR(14),SYS.FN_CDC_MAP_LSN_TO_TIME(__$START_LSN),112) ");
                    sbSql.Append("+ CONVERT(VARCHAR(14),SYS.FN_CDC_MAP_LSN_TO_TIME(__$START_LSN),108),':','')DATE_TIMESTAMP, ");
                    if (string.IsNullOrEmpty(sSourceColumns))
                        sbSql.AppendFormat(" __$START_LSN,__$SEQVAL,__$OPERATION ");
                    else
                        sbSql.AppendFormat(" {0},__$START_LSN,__$SEQVAL,__$OPERATION ", sSourceColumns);
                    sbSql.AppendFormat(" FROM CDC.HIST_{0}_CT ", p_objtable.TableName.ToUpper());
                    sbSql.Append("WHERE REPLACE(CONVERT(VARCHAR(14),SYS.FN_CDC_MAP_LSN_TO_TIME(__$START_LSN),112) ");
                    sbSql.AppendFormat(" + CONVERT(VARCHAR(14),SYS.FN_CDC_MAP_LSN_TO_TIME(__$START_LSN),108),':','') <= {0}", Conversion.ToDbDateTime(DateTime.Now));
                }
            }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.CommonGetSourceDataSqlErr, p_objtable.TableName.ToUpper()), objEx);
            }
            return sbSql.ToString();
        }

        internal static List<string> GetlistOfTableName(int iTableId)
        {
            List<string> lstTableName = null;
            string sSql = string.Empty;
            string sTableName = string.Empty;
            lstTableName = new List<string>();
            int iRMDatabaseId = AppGlobal.RMDatabaseId;
            if(iTableId !=-1)
            {
                sSql = "SELECT TABLE_NAME FROM HIST_TRACK_TABLES WHERE TABLE_ID =" + iTableId;
                using (DbReader objReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, sSql))
                {
                    if(objReader.Read())
                    {
                        sTableName = objReader.GetString("TABLE_NAME").ToUpper();
                        sTableName = sTableName + "_HIST_" + iRMDatabaseId.ToString();
                    }
                }
                lstTableName.Add(sTableName);
            }
            else
            {
                sSql = "SELECT TABLE_NAME FROM HIST_TRACK_TABLES WHERE CREATED_TABLE_FLAG = -1";
                using(DbReader objReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, sSql))
                {
                    while (objReader.Read())
                    {
                        sTableName = objReader.GetString("TABLE_NAME").ToUpper();
                        sTableName = sTableName + "_HIST_" + iRMDatabaseId.ToString();
                        lstTableName.Add(sTableName);
                    }
                }


            }
            return lstTableName;

        }
     
        /// <summary>
        /// Set owner of tables to AppGlobal.DBOUserId
        /// </summary>
        internal static void SetRMDBO()
        {
            string sSql = string.Empty;
            try
            {
                sSql = "SELECT SCHEMA_NAME( SCHEMA_ID) FROM SYS.TABLES WHERE NAME ='CLAIM' AND TYPE = 'U'";
                AppGlobal.DBOUserId = DbFactory.ExecuteScalar(AppGlobal.RMAdminConnstring, sSql).ToString();
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            
            catch (Exception objEx)
            {
                throw new RMAppException(Message.CommonSetRMDBOErr, objEx);
            }
        }

        internal static void FinishCDCProcess()
        {
            DisplayMessage(Message.CommonFinishMsg, Mode.Status);
            if (!AppGlobal.IsSilentMode)
            {
                frmCDC myForm = System.Windows.Forms.Application.OpenForms["frmCDC"] as frmCDC;
                myForm.progressBarCDC.Visible = false;
            }
        }

        internal static void GetConnStrElements(ref string sUserId, ref string sPassword, ref string sServer, string sConnString)
        {
            Regex regex = new Regex(@"^(?<CONN_STR_VALUE>[^;].+)SERVER=(?<SERVER>[^;]+);[^;].+UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$", RegexOptions.IgnoreCase);
            Match match = regex.Match(sConnString);
            if (match.Success)
            {
                sServer = match.Groups["SERVER"].Value.Trim();
                sUserId = match.Groups["UID_VALUE"].Value.Trim();
                sPassword = match.Groups["PWD_VALUE"].Value.Trim();
            }
        }
        #endregion

        #region Private Method

        private static string ReplaceItemName(string sSource, string sOldItemName, string sNewItemName)
        {
            string[] sPairs = sSource.Split(';');
            string[] sPair = null;
            for (int i = 0; i < sPairs.Length; i++)
            {
                sPair = sPairs[i].Split('=');
                if (sPair[0].Trim().Equals(sOldItemName, StringComparison.OrdinalIgnoreCase))
                    sPairs[i] = String.Format("{0}={1}", sNewItemName, sPair[1]);
            }
            return String.Join(";", sPairs);
        }

        private static string RemoveNameValuePair(string sSource, string sItemName)
        {
            string[] sPairs = sSource.Split(';');
            string[] sPair = null;
            for (int i = 0; i < sPairs.Length; i++)
            {
                sPair = sPairs[i].Split('=');
                if (sPair[0].Trim().Equals(sItemName, StringComparison.OrdinalIgnoreCase))
                    return String.Join(";", sPairs, 0, i) + String.Join(";", sPairs, i + 1, ((sPairs.Length - i) - 1));
            }
            return sSource;
        }

        #endregion

    }

    #endregion

    #region ColumnDetail Structure
    /// <summary>
    /// Structure for details of column.
    /// </summary>
    internal struct ColoumnDetails
    {
        public string sName;
        public string sType;
        public int iClmnID;
        public int iSize;
        public int iNumPrecision;
        public int iNumScale;

    }
    #endregion

    #region Mode enum

    /// <summary>
    /// Status : For general status message.
    /// Error : Fre error message.
    /// </summary>
    internal enum Mode
    {
        Status,
        Error
    }
    #endregion

    #region Table Class

    /// <summary>
    /// Table class
    /// </summary>
    internal class Table
    {

        string m_sName = string.Empty;
        int m_iTableId = 0;
        bool m_bCDCEnabled = false;
        bool m_bCretaedTableFlag = false;
        List<ColoumnDetails> m_lstColoumnList = null;

        public Table(string sTableName,int iTableId,bool bCDCEnabled,bool bCreatedFlag)
        {
            this.m_sName = sTableName;
            this.m_iTableId = iTableId;
            this.m_bCDCEnabled = bCDCEnabled;
            this.m_bCretaedTableFlag = bCreatedFlag;
            this.m_lstColoumnList = Common.GetColoumnList(this.m_iTableId);
        }


        internal string TableName
        {
            get
            {
                return m_sName;
            }
            set
            {
                m_sName = value;
            }
        }

        internal int TableId
        {
            get
            {
                return m_iTableId;
            }
            set
            {
                m_iTableId = value;
            }
        }
        internal bool IsCDCEnabled
        {
            get
            {
                return m_bCDCEnabled;
            }
            set
            {
                m_bCDCEnabled = value;
            }
        }
        internal bool IsTableCreated
        {
            get
            {
                return m_bCretaedTableFlag;
            }
            set
            {
                m_bCretaedTableFlag = value;
            }
        }
        internal List<ColoumnDetails> ColoumnList
        {
            get
            {
                return m_lstColoumnList;
            }
            set
            {
                m_lstColoumnList = value;
            }
        }


    }
    #endregion
}
