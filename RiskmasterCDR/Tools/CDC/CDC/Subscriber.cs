﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Tools.CDC
{
    class Subscriber 
    {
              
        internal void CreateSubscription()
        {

        }

        internal void SubscribeAndActivate()
        {
        }

        internal void DropSubscription(string sTableName)
        {
            
            
        }
        /// <summary>
        /// This function enable supplemental logging of Table
        /// </summary>
        /// <param name="sTableName"></param>
        internal void EnableSuppLogging(string sTableName)
        {
            StringBuilder sbSql = null;
            DbConnection objDbConnection = null;
            DbCommand objCmd = null;
            try
            {
                objDbConnection = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring);
                objDbConnection.Open();
                objCmd = objDbConnection.CreateCommand();
                sbSql = new StringBuilder();
                sbSql.Append("ALTER TABLE ");
                sbSql.Append(sTableName);
                sbSql.Append(" ADD SUPPLEMENTAL LOG DATA (ALL) COLUMNS");
                objCmd.CommandText = sbSql.ToString();
                objCmd.ExecuteNonQuery();
                objDbConnection.Close();
                objDbConnection.Dispose();
            }

            catch (Exception)
            {

            }
            finally
            {
                if(objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
            }

        }

        /// <summary>
        /// Get list of tables which would be processed.
        /// </summary>
        /// <param name="bCDCProcess">true : CDC process 
        ///                           false : Populat Audit Table</param>
        /// <returns></returns>
        internal  List<Table> GetListofTables(bool bCDCProcess)
        {
            string sSQL = string.Empty;
            string sTableName = null;
            int iTableId = 0;
            bool bCDCEnabled = false;
            bool bCreatedTable = false;
            Table objTable = null;
            List<Table> lstTableList = null;

            try
            {
                lstTableList = new List<Table>();
                if (bCDCProcess)
                {   //fetch tables which needs to be orcessed
                    sSQL = "SELECT TABLE_NAME,TABLE_ID,ENABLED_FLAG,CREATED_TABLE_FLAG FROM HIST_TRACK_TABLES WHERE UPDATED_FLAG = -1 ";
                }
                else
                {   //fetch tables for which data would be populated in Audit tables
                    sSQL = "SELECT TABLE_NAME,TABLE_ID,ENABLED_FLAG,CREATED_TABLE_FLAG FROM HIST_TRACK_TABLES WHERE CREATED_TABLE_FLAG = -1 AND (ENABLED_FLAG=-1 OR (ENABLED_FLAG =0 AND UPDATED_FLAG = -1)) ";
                }

                using (DbReader objDbReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {

                            sTableName = objDbReader.GetString("TABLE_NAME");
                            iTableId = objDbReader.GetInt32("TABLE_ID");
                            bCDCEnabled = objDbReader.GetBoolean("ENABLED_FLAG");
                            bCreatedTable = objDbReader.GetBoolean("CREATED_TABLE_FLAG");
                            objTable = new Table(sTableName, iTableId, bCDCEnabled, bCreatedTable);
                            lstTableList.Add(objTable);
                            objTable = null;
                        }

                    }
                }
                return lstTableList;
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.CommonGetListofTablesErr, objEx);
            }


        }

        /// <summary>
        /// Get CDC flag status
        /// </summary>
        /// <param name="p_bHistTrackEnable">Enable Falg </param>
        /// <param name="p_bHistTrackStatus">Status Flag</param>
        internal void GetCDCFlagStatus(ref bool p_bHistTrackEnable, ref bool p_bHistTrackStatus)
        {

            SysSettings objSys = null;
            try
            {
                objSys = new SysSettings(AppGlobal.RMDBConnstring,AppGlobal.ClientId);//Add by kuladeep for Cloud.

                p_bHistTrackEnable = objSys.HistTrackEnable; //objSys.RetrieveSystemSettings("HIST_TRACK_ENABLE");
                p_bHistTrackStatus = objSys.HistTrackStatus;//objSys.RetrieveSystemSettings("HIST_TRACK_STATUS");
            }

            catch (Exception objExc)
            {

                throw new RMAppException(Message.SubscriberGetCDCFlagStatusError, objExc);
            }


        }


        /// <summary>
        /// set CDC flag status
        /// </summary>
        /// <param name="p_sFlagName">Flag Name</param>
        /// <param name="p_bValue">True/False</param>
        internal  void SetCDCFlagStatus(string p_sFlagName, bool p_bValue)
        {

            SysSettings objSys = null;
            try
            {
                objSys = new SysSettings(AppGlobal.RMDBConnstring,AppGlobal.ClientId);
                //objSys.AssignSystemSettings(p_sFlagName, p_bValue, true);
                //objSys.AssignSystemSettings(p_sFlagName, p_bValue, false);

                switch (p_sFlagName)
                {
                    case "HIST_TRACK_ENABLE":
                        objSys.HistTrackEnable = p_bValue;
                        break;
                    case "HIST_TRACK_STATUS":
                        objSys.HistTrackStatus = p_bValue;
                        break;
                }

            }

            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.SubscriberSetCDCFlagStatusErr, objEx);
            }

        }


        /// <summary>
        /// Set Table flags
        /// </summary>
        /// <param name="p_sFlagName">flag name</param>
        /// <param name="p_iTableID">Table id</param>
        /// <param name="p_bValue">true/false</param>
        internal void SetTableFlagStatus(string p_sFlagName, int p_iTableID, bool p_bValue)
        {

            StringBuilder sbSql = null;
            DbConnection objDbConn = null;

            try
            {
                objDbConn = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring);
                objDbConn.Open();
                sbSql = new StringBuilder();
                if (p_bValue)
                {
                    sbSql.AppendFormat("UPDATE HIST_TRACK_TABLES SET {0} = -1 ", p_sFlagName);

                }
                else
                {
                    sbSql.AppendFormat("UPDATE HIST_TRACK_TABLES SET {0} = 0 ", p_sFlagName);
                }

                //where condition on table id
                if (p_iTableID != 0)
                {
                    sbSql.AppendFormat(" WHERE  TABLE_ID = {0}", p_iTableID);
                }
                objDbConn.ExecuteNonQuery(sbSql.ToString());
                objDbConn.Close();

            }

            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.SubscriberSetTableFlagStatusErr, p_sFlagName, p_iTableID), objEx);
            }

            finally
            {
                if (objDbConn != null)
                    objDbConn.Close();

            }
        }


        /// <summary>
        /// Delete record from HIST_TRACK_COLUMNS
        /// for column id of specific table.
        /// </summary>
        /// <param name="p_objTable">table object</param>

        internal void PurgeHistTrackClmns(Table p_objTable)
        {

            StringBuilder sbSql = null;
            DbConnection objDbConn = null;

            try
            {
                objDbConn = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring);
                objDbConn.Open();
                sbSql = new StringBuilder();
                sbSql.AppendFormat("DELETE FROM HIST_TRACK_COLUMNS WHERE COLUMN_ID IN ({0})", Common.ConcatenateColumnsId(p_objTable));
                objDbConn.ExecuteNonQuery(sbSql.ToString());
                objDbConn.Close();

            }

            catch (Exception objEx)
            {

                throw (objEx);
            }


            finally
            {
                if (objDbConn != null)
                    objDbConn.Close();

            }
        }



        

    }
}
