﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;
using Riskmaster.Common;

namespace Riskmaster.Tools.CDC
{
    static class Program
    {
        [MTAThread]
        static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    System.Windows.Forms.Application.EnableVisualStyles();
                    System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                    //Add & Change by kuladeep for Cloud----Start Jira-1124
                    bool blnSuccess = false;
                    //Bydefalut ClientId will be zero for base functionality...but we can run this tool by providing Clientid for particular Client.
                    int iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                    //Add & Change by kuladeep for Cloud----End Jira-1124
                    if (!String.IsNullOrEmpty(AppGlobal.SecurityDSN(iClientId)))//Add clientId by kuladeep for Cloud Jira-1124
                    {
                        System.Windows.Forms.Application.Run(new frmCDC(AppGlobal.SecurityDSN(iClientId)));//Add clientId by kuladeep for Cloud Jira-1124
                    }
                } // if
                else
                {
                    //Run the program in silent mode without displaying the UI
                      AppGlobal.IsSilentMode = true;
                      //args = new string[7];
                      //args[0] = "-dsrmACloud";
                      //args[1] = "-rucloud";
                      //args[2] = "0b54f3a9959b766e20a5fb1e46c05874";
                      //args[3] = "admin";
                      //args[4] = "admin";
                      //args[5] = "-st0";
                      //args[6] = "-ci1";
                      SilentCDCProcess.Validation(args);
                    
                } // else
            }
            catch (Exception exc)
            {
                Common.WriteErrorLog(exc,true);
               
            }
        }

        
    }
}
