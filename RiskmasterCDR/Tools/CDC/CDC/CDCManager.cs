﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Riskmaster.Tools.CDC
{
	class CDCManager
	{
		static bool m_blnSuccess = false;
		static bool m_bHistTrackEnable = false;
		static bool m_bHistTrackStatus = false;
		static bool m_bCDCProcess = false;


		#region Internal Method

		/// <summary>
		/// This function is the entry point of 
		/// CDC setup
		/// </summary>
		internal static void ProcessCDCSetup()
		{

			string sSql = string.Empty;
			string sErrMsg = string.Empty;
			Publisher objPublisher = null;
			Subscriber objSubscriber = null;
			Audit objAudit = null;
			
			
			List<Table> lstTableList = null;
			try
			{
				m_bCDCProcess = true;
				objPublisher = new Publisher();
				objSubscriber = new Subscriber();
				objAudit = new Audit();
				objSubscriber.GetCDCFlagStatus(ref m_bHistTrackEnable, ref m_bHistTrackStatus);

				//list of tables which has to be processed. Only those tables would be processed 
				// which has its updated flag = -1
				lstTableList = objSubscriber.GetListofTables(m_bCDCProcess);

				//CDC Enable flag is on
				if (m_bHistTrackEnable)
				{

					//Start :Enable CDC on Database level
					//CDC is enabled first time if m_bHistTrackEnable =-1 and m_bHistTrackStatus =0
					if (!m_bHistTrackStatus)
					{
						if (AppGlobal.IsError)
						{
							Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
							throw new RMAppException(Message.CDCManagerCDCProcessAborted);
						}
						try
						{
							Common.DisplayMessage(Message.CDCManagerEnableInit, Mode.Status);
							objPublisher.EnableCDC();
							objAudit.InsertRecHTDataSource(AppGlobal.RMDatabaseId);
							m_bHistTrackStatus = true;

							//After processing of first time, set the HIST_TRACK_STATUS flag
							objSubscriber.SetCDCFlagStatus("HIST_TRACK_STATUS", m_bHistTrackStatus);
						}
						catch (Exception exc)
						{
							Common.DisplayMessage(Message.CDCManagerEnableErr, Mode.Error);
							Common.WriteErrorLog(exc,true);
						}
						
					}
					//End :Enable CDC on Database level




					//Start :Enable or Disable CDC on Table level

					if (lstTableList.Count > 0)
					{
						if (AppGlobal.IsError)
						{
							Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
							throw new RMAppException(Message.CDCManagerCDCProcessAborted);
						}


						//Start foreach 
						foreach (Table objTable in lstTableList)
						{
							try
							{
								Common.DisplayMessage("Processing Table : " + objTable.TableName, Mode.Status);

								//Start: Enable CDC on table level
								//Creating a new setup or in case of edit, drop existing
								//setup and create new setup
								//It will require new setup if updated flag=-1 , enabled flag =-1 and column count >0
								if (objTable.IsCDCEnabled && objTable.ColoumnList.Count > 0)
								{

									if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
									{

                                        //Create Permanent Audit Table if it does not exist
										if (!objTable.IsTableCreated)
										{
											objAudit.CreateAuditTables(objTable);
                                            objSubscriber.SetTableFlagStatus("CREATED_TABLE_FLAG", objTable.TableId, true);
										}

                                        //drop existing setup and create new setup
                                        objPublisher.AbortTableInstantiation(objTable.TableName);
                                        objSubscriber.EnableSuppLogging(objTable.TableName);
                                        objPublisher.InstantiateSourceTable(objTable.TableName);
                                        objPublisher.CreateChangeTable(objTable);

									}
									else
									{
										
										//Create Permanent Audit Table if it does not exist
										if (!objTable.IsTableCreated)
										{
											objAudit.CreateAuditTables(objTable);
											//Set Created flag =-1 after creating Audit table.
											objSubscriber.SetTableFlagStatus("CREATED_TABLE_FLAG", objTable.TableId, true);
										}
                                        //drop existing setup and               
                                        //create new setup
                                        objPublisher.CreateChangeTable(objTable);

									}


								}

								//End : Enable CDC on table level

								//Start: Disable CDC on table level
								if (!objTable.IsCDCEnabled)
								{
									//drop existing setup
									if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
									{
										
										objPublisher.AbortTableInstantiation(objTable.TableName);
										objPublisher.DropChangeTable(objTable.TableName);
										//objSubscriber.PurgeHistTrackClmns(objTable);
									}
									else
									{
										objPublisher.DropChangeTable(objTable.TableName);
										//objSubscriber.PurgeHistTrackClmns(objTable);
									}

								}
								//End : Disable CDC on table level

								//After successfull processing of table
								//reset  UPDATED_FLAG of Table
								objSubscriber.SetTableFlagStatus("UPDATED_FLAG", objTable.TableId, false);

							}
							catch (Exception exc)
							{
							   
								Common.DisplayMessage(string.Format( Message.CDCManagerTableProcessErr,objTable.TableName.ToUpper()), Mode.Error);
								Common.WriteErrorLog(exc,true);
							}
						}//End  foreach 

						//Change the data retention criteria of 'changetable'.
						//CDC create a clean up job which delete data from 'changetable'
						//on the basis of retention criteria. By default retention period is 3 days in sql. It
						//means data which are older than 3 days would get deleted by cleanup job. Using ChangeRetentionCriteria()
						//function we change this retention criteria to 8 days.
						objPublisher.ChangeRetentionCriteria();
					}
					//End :Enable or Disable CDC on Table level
				}


				//Start:Disable CDC on DB level
				//CDC Enable flag is off               
				if (!m_bHistTrackEnable && m_bHistTrackStatus)    
				{
					try
					{
						if (AppGlobal.IsError)
						{

							Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
							throw new RMAppException(Message.CDCManagerCDCProcessAborted);
						}
						Common.DisplayMessage(Message.CDCManagerDisableCDCMsg, Mode.Status);
						
						if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
						{
							//First disabling CDC on each table,then on database level
							foreach (Table objTable in lstTableList)
							{
								
								objPublisher.AbortTableInstantiation(objTable.TableName);
								objPublisher.DropChangeTable(objTable.TableName);
								//reset  UPDATED_FLAG  and ENABLED_FLAG of Table
								objSubscriber.SetTableFlagStatus("UPDATED_FLAG", objTable.TableId, false);
								objSubscriber.SetTableFlagStatus("ENABLED_FLAG", objTable.TableId, false);
								objSubscriber.PurgeHistTrackClmns(objTable);
							}

							objPublisher.DropChangeSet();
						}
						else
						{
							//First disabling CDC on each table,then on database level
							foreach (Table objTable in lstTableList)
							{
								objPublisher.DropChangeTable(objTable.TableName);
								//reset  UPDATED_FLAG  and ENABLED_FLAG of Table
								objSubscriber.SetTableFlagStatus("UPDATED_FLAG", objTable.TableId, false);
								objSubscriber.SetTableFlagStatus("ENABLED_FLAG", objTable.TableId, false);
								objSubscriber.PurgeHistTrackClmns(objTable);

							}
							objPublisher.DisableCDC();
						}
						//Reset HIST_TRACK_STATUS flag after full disable of CDC
						objSubscriber.SetCDCFlagStatus("HIST_TRACK_STATUS", false);
						objAudit.DeleteRecHTDataSource(AppGlobal.RMDatabaseId);
					   
					}
					catch (Exception objEx)
					{
						Common.DisplayMessage(Message.CDCManagerDisableCDCErr, Mode.Error);
						Common.WriteErrorLog(objEx,true);
					}
					
				}

				//End:Disable CDC on DB level               
			  
			}
		   
			catch (Exception p_objEx)
			{
				Common.WriteErrorLog(p_objEx,true);
			}

		}
		
		/// <summary>
		/// This function populate all the data from 
		/// change tables to permanent history tracking tables
		/// </summary>
		internal static void PopulateAuditTables()
		{
			
			List<Table> lstTableList = null;
			Audit objAudit = null;
			Subscriber objSubscriber = null;
			string sSql = string.Empty;
			string sRMADMinConn = string.Empty;
			string sHistoryTrackConn = string.Empty;
			int iMaxNumRecordToRead = 0;
			int iMaxNumRecordToWrite = 0;

			try
			{
				
				objSubscriber = new Subscriber();
				//Get CDC Flags status
				objSubscriber.GetCDCFlagStatus(ref m_bHistTrackEnable, ref m_bHistTrackStatus);
				//Data will be populated only if HistTrackStatus =true
				if (m_bHistTrackStatus == true)
				{
					Common.DisplayMessage(Message.CDCManagerPopulateDataInit, Mode.Status);
					objAudit = new Audit();                    
					m_bCDCProcess = false;
					//Maximum number of records retrieved in one read
					iMaxNumRecordToRead = Conversion.CastToType<int>(Common.GetValueFromConfigFile("MaxNumRecordToRead"), out m_blnSuccess);
					//Maximum number of record to be inserted in one trasaction
					iMaxNumRecordToWrite = Conversion.CastToType<int>(Common.GetValueFromConfigFile("MaxNumRecordToWrite"), out m_blnSuccess);

					//get list of table for which we have to poulate data 
					//from change table to permanent audit table
					lstTableList = objSubscriber.GetListofTables(m_bCDCProcess);

					if (lstTableList.Count > 0)
					{
					   
						foreach (Table objTable in lstTableList)
						{
							try
							{
								Common.DisplayMessage("Populating History Tracking Table : " + objTable.TableName, Mode.Status);                             

								// get SELECT sql statement using which we will retrive data from change table
								sSql = Common.GetSourceDataSql(objTable, iMaxNumRecordToRead);
								if (!string.IsNullOrEmpty(sSql))
								{
									if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
									{
										PopulateOraData(sSql, objTable);
									}
									else
									{
										PopulateSqlData(sSql, objTable, iMaxNumRecordToWrite);
									}
								}
							}
							catch (Exception objEx)
							{
								Common.WriteErrorLog(objEx,true);
							}


						}//End foreach
					}//End if
				}

			}
			catch (Exception objExc)
			{
				
				Common.WriteErrorLog(objExc,true);
			}
			finally
			{
				
			}


		}

		/// <summary>
		/// Purge Data from Audit tables
		/// </summary>
		internal static void PurgeDataFromAuditTables()
		{
			Audit objAudit = null;
			try
			{
			   
				if (!AppGlobal.IsError)
				{
					Common.DisplayMessage(Message.CDCManagerPurgeDataInit, Mode.Status);
					objAudit = new Audit();
					Common.DisplayMessage(Message.CDCManagerPurgeTableData, Mode.Status);
					
					objAudit.PurgeDatafromTable();
								
				}
			}
			catch(Exception objEx)
			{
				
				
				Common.WriteErrorLog(objEx,true);
			}
		}

		#endregion

		#region Private Method

		/// <summary>
		/// Poulate Data from change table to 
		/// Audit table for Sql database
		/// </summary>
		/// <param name="sSourceSql">sql statemant to retrieve data</param>
		/// 
		/// 
		/// <param name="objTable">table Object</param>
		/// <param name="iMaxNumRecordToWrite">Maximun number of record in one batch of bulkcopy</param>
		private static void PopulateSqlData(string sSourceSql, Table objTable, int iMaxNumRecordToWrite)
		{
			

			DataTable sourceDataTable = null;
			DataColumn dcol = null;
			string strDestTableName = string.Empty;
			
			int iRecordCount =0;

			Database adminDB = ADODbFactory.CreateDatabase(AppGlobal.RMAdminConnstring);
            Database historyDB = ADODbFactory.CreateDatabase(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId));//Add clientId by kuladeep for Cloud Jira-1124

				
				//Permanent Audit table name
				strDestTableName = string.Format("{0}_HIST_{1}", objTable.TableName, AppGlobal.RMDatabaseId.ToString());
				 
			   // start loop
				do
				{
					//Inialize DataTable and add one auto incremented DataColumn named 'RECORD_ID'
					// Auto increment value would be start from Max Record_Id +1 of the permanent
					//audit table
					sourceDataTable = new DataTable();
					dcol = new DataColumn("RECORD_ID", typeof(System.Int32));
					dcol.AutoIncrement = true;
					dcol.AutoIncrementSeed = GetMaxRecordId(strDestTableName) +1;
					sourceDataTable.Columns.Add(dcol);
					
					//Fetch Data from CDC change tables
					System.Data.Common.DbDataAdapter objSourceAd = adminDB.GetDataAdapter();

					//Initialize auto command builder. This command builder will be used to
					//create delete commands to delete data form change table
					System.Data.Common.DbCommandBuilder SqlSourceCmdBdr = adminDB.DbProviderFactory.CreateCommandBuilder();
					SqlSourceCmdBdr.DataAdapter = objSourceAd;

                    //create source dbcommand 
                    System.Data.Common.DbCommand objSourceCommand = adminDB.GetSqlStringCommand(sSourceSql);
                    objSourceCommand.Connection = adminDB.CreateConnection();
                    objSourceAd.SelectCommand = objSourceCommand;

					//Fill data into DataTable
					objSourceAd.Fill(sourceDataTable);
					iRecordCount = sourceDataTable.Rows.Count;
				   
					//Start : Populate Data into Audit Tables
					//Begin Transaction for BulkCopy
					using (SqlConnection objHistSqlConn = historyDB.CreateConnection() as SqlConnection)
					{
                        if (objHistSqlConn.State != ConnectionState.Open)
                            objHistSqlConn.Open();
						using (SqlTransaction objHistTran = objHistSqlConn.BeginTransaction("HistTransaction"))
						{
                            SqlBulkCopy objSqlBulkCopy = null;

							try
							{

								objSqlBulkCopy = new SqlBulkCopy(objHistSqlConn, SqlBulkCopyOptions.KeepIdentity, objHistTran);

								//Mapping columns of Datatable to permanent audit table columns.
								//Skip mapping of Column __$start_lsn , __$seqval and __$operation because there is no such columns in Audit table
								//These three columns make primary key for DataTable
								foreach (DataColumn Dcol in sourceDataTable.Columns)
								{

									if (Dcol.ColumnName.ToLower() != "__$start_lsn" && Dcol.ColumnName.ToLower() != "__$seqval" && Dcol.ColumnName.ToLower() != "__$operation")
									{
										SqlBulkCopyColumnMapping objSqlmapping = new SqlBulkCopyColumnMapping(Dcol.ColumnName.ToUpper(), Dcol.ColumnName.ToUpper());
										objSqlBulkCopy.ColumnMappings.Add(objSqlmapping);
									}

								}
								objSqlBulkCopy.DestinationTableName = String.Format("{0}_HIST_{1}", objTable.TableName.ToUpper(), AppGlobal.RMDatabaseId);
								objSqlBulkCopy.BatchSize = iMaxNumRecordToWrite;
								objSqlBulkCopy.WriteToServer(sourceDataTable);
								//End : Populate Data into Audit Tables

								//Start :Delete data from CDC Change tables.

								//First remove "OPERATION" column from DataTable because
								//there is no such physical column in change table
								sourceDataTable.Columns.Remove("OPERATION");

								//generating delete command automatically
								foreach (DataRow dr in sourceDataTable.Rows)
								{
									dr.Delete();
								}
                                objSourceCommand = SqlSourceCmdBdr.GetDeleteCommand();
                                using (System.Data.Common.DbConnection sqlAdminConn = objSourceCommand.Connection)
                                {

                                    if (sqlAdminConn.State != ConnectionState.Open)
                                    {
                                        sqlAdminConn.Open();
                                    }//if
                                    System.Data.Common.DbTransaction objAdminTrans = sqlAdminConn.BeginTransaction();
                                    objSourceCommand.Transaction = objAdminTrans;
                                    try
                                    {
                                        objSourceAd.DeleteCommand = objSourceCommand;
                                        objSourceAd.Update(sourceDataTable);

                                        //Commit Admin Transaction
                                        objAdminTrans.Commit();
                                    }
                                    catch (Exception objEx)
                                    {
                                        objAdminTrans.Rollback();
                                        throw new RMAppException(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper()), objEx);
                                    }
                                }//using
                                //End :Delete data from CDC Change tables.
								objHistTran.Commit();
							}
							catch (Exception objEx)
							{

								objHistTran.Rollback();
								Common.DisplayMessage(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper(), objTable.TableName.ToUpper()), Mode.Error);
								throw new RMAppException(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper()), objEx);
							}
							finally
							{
                                dcol.Dispose();
                                sourceDataTable.Dispose();

                                dcol = null;
                                sourceDataTable = null;
								objSqlBulkCopy.Close();
							}//finally 
						}//using
						 
					}//using

					
				}
				while (iRecordCount > 0);//Exit loop if number of fetched record from change table is zero
			   
			 
		}


	 
		/// <summary>
		/// Poulate Data from change table to 
		/// Audit table for Oracle database
		/// </summary>
		/// <param name="sSourceSql">sql statemant to retrieve data</param>
		/// <param name="sRMADMinConn">ChangeTable database connection string</param>
		/// <param name="sHistoryTrackConn">Audit database connection string</param>
		/// <param name="objTable">table Object</param>
		private static void PopulateOraData(string sSourceSql, Table objTable)
		{
			string strDestTableName = string.Empty;
			int iRecordCount = 0;

			Database adminDB = ADODbFactory.CreateDatabase(AppGlobal.RMAdminConnstring);
            Database historyDB = ADODbFactory.CreateDatabase(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId));//Add clientId by kuladeep for Cloud Jira-1124

			//Permanent Audit table name
			strDestTableName = string.Format("{0}_HIST_{1}", objTable.TableName, AppGlobal.RMDatabaseId.ToString());

			do
			{
				//Inialize DataTable and add one auto incremented DataColumn named 'RECORD_ID'
				// Auto increment value would be start from Max Record_Id +1 of the permanent
				//audit table
				DataTable sourceDataTable = new DataTable();
				DataColumn dcol = new DataColumn("RECORD_ID", typeof(Int32)) { AutoIncrement = true, AutoIncrementSeed = GetMaxRecordId(strDestTableName) + 1 };
				sourceDataTable.Columns.Add(dcol);


				//create DbDataAdapter 
				System.Data.Common.DbDataAdapter objSourceAd = adminDB.GetDataAdapter();

				//Create source dbcommand builder. This command builder will be used to
				//create delete commands to delete data form change table
				System.Data.Common.DbCommandBuilder objSourceCmdBdr = adminDB.DbProviderFactory.CreateCommandBuilder();
				objSourceCmdBdr.DataAdapter = objSourceAd;

				//create source dbcommand 
				System.Data.Common.DbCommand objSourceCommand = adminDB.GetSqlStringCommand(sSourceSql);
				objSourceCommand.Connection = adminDB.CreateConnection();
				objSourceAd.SelectCommand = objSourceCommand;

				//Fill data into DataTable
				objSourceAd.Fill(sourceDataTable);
				iRecordCount = sourceDataTable.Rows.Count;

				//Start : Populate Data into Audit Tables

				//generating list of columns for which we are poulating data in Audit tables.

				StringBuilder sbColumnsList = new StringBuilder();
				foreach (DataColumn Dcol in sourceDataTable.Columns)
				{
					if (Dcol.ColumnName.ToUpper() != "RSID$" && Dcol.ColumnName.ToUpper() != "OPERATION$")
					{

						sbColumnsList.AppendFormat("{0},", Dcol.ColumnName.ToUpper());
					}
				}
				// select query to get schema of audit table in histDataTable DataTable
				string sSql = string.Format("SELECT {0} FROM {1} WHERE 0=1", sbColumnsList.ToString().TrimEnd(','), strDestTableName);
				DataTable histDataTable = new DataTable();

				//create DbDataAdapter for historytracking db
				System.Data.Common.DbDataAdapter objHistAd = historyDB.GetDataAdapter();

				//Create history tracking dbcommand builder. This command builder will be used to
				//create insert commands to insert new records in permanent history tracking table form change table
				System.Data.Common.DbCommandBuilder objHistCmdBdr = historyDB.DbProviderFactory.CreateCommandBuilder();
				objHistCmdBdr.DataAdapter = objHistAd;

				//create history tracking dbcommand
				System.Data.Common.DbCommand objHistCommand = historyDB.GetSqlStringCommand(sSql);
				objHistCommand.Connection = historyDB.CreateConnection();
				objHistAd.SelectCommand = objHistCommand;

				objHistAd.Fill(histDataTable);

				//Transfer all the data from Source DataTable(sourceDataTable) to Audit DataTable(histDataTable)
				histDataTable.Merge(sourceDataTable, true, MissingSchemaAction.Ignore);
				
				//Change the RowState of each row to Added so that on update of adaptor
				//all the rows get inserted into Audit table.
				foreach (DataRow drHist in histDataTable.Rows)
				{
					drHist.SetAdded();
				}//foreach

				

				//Begin Audit transaction

				using (System.Data.Common.DbConnection oraHistoryConn = objHistCommand.Connection)
				{
					if (oraHistoryConn.State != ConnectionState.Open)
					{
						oraHistoryConn.Open();
					}//if


					System.Data.Common.DbTransaction objHistTran = oraHistoryConn.BeginTransaction();
					
					try
					{
						objHistAd.Update(histDataTable);

						//End : Populate Data into Audit Tables


						//Start :Delete data from CDC Change tables.

						//First remove "OPERATION" column from DataTable because
						//there is no such physical column in change table
						sourceDataTable.Columns.Remove("OPERATION");

						//generating delete command automatically
						foreach (DataRow dr in sourceDataTable.Rows)
						{
							dr.Delete();
						}


						using (System.Data.Common.DbConnection oraAdminConn =objSourceCommand.Connection)
						{

							if (oraAdminConn.State != ConnectionState.Open)
							{
								oraAdminConn.Open();
							}//if
							System.Data.Common.DbTransaction objAdminTrans = oraAdminConn.BeginTransaction();
							
							try
							{

								objSourceAd.Update(sourceDataTable);
							   
								//Commit Admin Transaction
								objAdminTrans.Commit();
							}
							catch (Exception objEx)
							{
								objAdminTrans.Rollback();
								throw new RMAppException(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper()), objEx);
							}
						}//using


						//Commit Audit transaction
						objHistTran.Commit();
					}
					catch (Exception objEx)
					{

						objHistTran.Rollback();
						Common.DisplayMessage(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper()), Mode.Error);
						throw new RMAppException(string.Format(Message.CDCManagerPopulateDataErr, objTable.TableName.ToUpper()), objEx);
					}//catch
					finally
					{
						//Clean up the DataTable objects before the next iteration of the loop
						sourceDataTable.Dispose();
						histDataTable.Dispose();
                        dcol.Dispose();

						sourceDataTable = null;
						histDataTable = null;
                        dcol = null;
					}//finally
				}//using

			}//do
			while (iRecordCount > 0);//Exit loop if number of fetched record from change table is zero
		}


		/// <summary>
		/// Get Max RECORD_ID of Audit table
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>MAX(RECORD_ID)</returns>
		private static int GetMaxRecordId(string p_sTableName)
		{
			int iMaxRecord = 0;
			StringBuilder sbSql = null;
			try
			{
				sbSql = new StringBuilder();
				sbSql.AppendFormat("SELECT MAX(RECORD_ID) FROM {0}", p_sTableName);
                using (DbReader objDbReader = DbFactory.ExecuteReader(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId), sbSql.ToString()))//Add clientId by kuladeep for Cloud Jira-1124
				{

					if (objDbReader.Read())
					{
						iMaxRecord = objDbReader.GetInt32(0);
					}
					else
					{
						iMaxRecord = 1;
					}

				}
			}
			catch (RMAppException objEx)
			{

				throw (objEx);
			}
			catch (Exception objEx)
			{

				throw new RMAppException(string.Format(Message.AuditGetMaxRecordIdErr, p_sTableName.ToUpper()), objEx);
			}
			return iMaxRecord;
		}
		#endregion

	}
}
