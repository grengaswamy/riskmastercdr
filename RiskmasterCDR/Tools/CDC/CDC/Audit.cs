﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using System.IO;
using System.Data;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Security.Encryption;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Tools.CDC
{
    class Audit
    {
        private bool m_blnSuccess = false;
        #region Internal Method
        /// <summary>
        /// Create Permanent Audit table in Audt database
        /// </summary>
        /// <param name="p_objTable">Table Object</param>
        internal void CreateAuditTables(Table p_objTable)
        {
           int iTableId = 0;
           string sTableName = string.Empty;          
           string sIndexColumns = string.Empty;
           string sIndexName = string.Empty;
           StringBuilder sbSql = null; ;
           Hashtable objIndexColumns = null;
           DbConnection objDbConnection = null;
           DbCommand objCmd = null;
           try
           {

               sbSql = new StringBuilder();
               sTableName = p_objTable.TableName;
               iTableId = p_objTable.TableId;

               //Start : Create Audit table
               // getting Create Table satatement
               sbSql.Append(CreateTableStatement(sTableName, iTableId));

               //Adding one more column RECORD_ID which would be primary key 
               if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                   sbSql.Append(" PRIMARY KEY (RECORD_ID))");
               else
               {
                   sbSql.AppendFormat("CONSTRAINT {0}_HIST_{1}_pk PRIMARY KEY (RECORD_ID))", sTableName, AppGlobal.RMDatabaseId);
               }

               //Open connection of Audit database
               objDbConnection = DbFactory.GetDbConnection(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId));//Add clientId by kuladeep for Cloud Jira-1124
               objDbConnection.Open();

               objCmd = objDbConnection.CreateCommand();
               objCmd.CommandText = sbSql.ToString();
               objCmd.ExecuteNonQuery();
               //End : Create Audit table


               //Start : Create Indexes 
               //Get all the Indexes in Table of Riskmaster database
               objIndexColumns = GetTableIndexes(sTableName);

               //Create same indexes on newly created audit table
               foreach (DictionaryEntry entry in objIndexColumns)
               {
                   sbSql.Length = 0;
                   //get array of columns as single index may included more than one column
                   try
                   {
                       string[] sColumnArray = entry.Value.ToString().Split(',');
                       if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                       {
                           sbSql.AppendFormat("CREATE NONCLUSTERED INDEX {0} ON ", entry.Key.ToString());
                       }
                       else
                       {
                           sbSql.AppendFormat("CREATE INDEX {0} ON ", entry.Key.ToString());
                       }
                       sbSql.AppendFormat("{0}_HIST_{1}", sTableName, AppGlobal.RMDatabaseId);
                       sbSql.Append("(  ");
                       for (int iColCount = 0; iColCount < sColumnArray.Length; iColCount++)
                       {
                           if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                           {
                               //if not last record
                               if (sColumnArray.Length != (iColCount + 1))
                                   sbSql.AppendFormat("{0} ASC , ", sColumnArray[iColCount]);
                               //last record
                               else
                                   sbSql.AppendFormat("{0} ASC ", sColumnArray[iColCount]);
                           }
                           else
                           {
                               //if not last record
                               if (sColumnArray.Length != (iColCount + 1))
                                   sbSql.AppendFormat("{0}, ", sColumnArray[iColCount]);
                               //last record
                               else
                                   sbSql.AppendFormat("{0}", sColumnArray[iColCount]);
                           }

                       }//End for

                       sbSql.Append(")");
                       objCmd.CommandText = sbSql.ToString();
                       objCmd.ExecuteNonQuery();
                   }
                   catch (Exception exc)
                   {
                       Common.WriteErrorLog(exc, false);
                   }
                   

               }//End foreach

               //End : Create Indexes 
               objDbConnection.Close();
               objDbConnection.Dispose();


           }
           catch (RMAppException objEx)
           {

               throw (objEx);
           }
           catch (Exception objEx)
           {

               throw new RMAppException(string.Format(Message.AuditCreateAuditTablesErr, p_objTable.TableName.ToUpper()), objEx);
           }

           finally
           {
               if (objDbConnection != null)
               {
                   objDbConnection.Close();
                   objDbConnection.Dispose();
               }
           }

        }

        /// <summary>
        /// This function call stored procedure to
        /// purge data from audit tables.
        /// </summary>
        internal void PurgeDatafromTable()
        {
            string sSql = string.Empty;
            string sFromDate = string.Empty;
            string sTodate = string.Empty;
            string serr_msg = string.Empty;
            int iTableId = 0;
            int iRecordId = 0;
            bool bRecordFound = false;
            List<string> lstTableName = null;
            DbConnection objDbConnection = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;
            DbParameter objParamout = null;

            try
            {
                //Open connection of Audit database
                objDbConnection = DbFactory.GetDbConnection(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId));//Add clientId by kuladeep for Cloud Jira-1124
                objDbConnection.Open();
                objCmd = objDbConnection.CreateCommand();

                //Get recrod from HIST_TRACK_PURGE and process each record by
                //calling stored procedure.
                sSql = "SELECT * from HIST_TRACK_PURGE WHERE DELETED_FLAG != -1";
                using (DbReader objDbReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, sSql))
                {
                    while (objDbReader.Read())
                    {
                        try
                        {
                            bRecordFound = true;
                            sFromDate = objDbReader.GetString("FROM_DATE");
                            sTodate = objDbReader.GetString("TO_DATE");
                            iTableId = objDbReader.GetInt32("TABLE_ID");

                            //get list of tables because if iTableId =-1 it meand
                            //we have to purge data from each audit tables
                            lstTableName = Common.GetlistOfTableName(iTableId);
                            iRecordId = objDbReader.GetInt32("RECORD_ID");

                            //Call stored procedure for each table
                            foreach (string sTableName in lstTableName)
                            {
                                //try
                                //{
                                    if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                                    {

                                        sSql = " BEGIN USP_PURGE_HISTTRK_TABLES('" + sTableName + "','" + sFromDate + "','" + sTodate + "'); END;";
                                        objCmd.CommandText = sSql;
                                    }
                                    else
                                    {

                                        sSql = "USP_PURGE_HISTTRK_TABLES";
                                        objCmd.CommandText = sSql;
                                        objCmd.CommandType = CommandType.StoredProcedure;
                                        objParam = null;
                                        objParamout = null;

                                        objParam = objCmd.CreateParameter();
                                        objParam.Direction = System.Data.ParameterDirection.Input;
                                        objParam.DbType = DbType.String;
                                        objParam.ParameterName = "@sTableName";
                                        objParam.Value = sTableName;
                                        objCmd.Parameters.Add(objParam);

                                        objParam = objCmd.CreateParameter();
                                        objParam.Direction = System.Data.ParameterDirection.Input;
                                        objParam.DbType = DbType.String;
                                        objParam.Size = 50;
                                        objParam.ParameterName = "@sToDate";
                                        objParam.Value = sTodate;
                                        objCmd.Parameters.Add(objParam);

                                        objParam = objCmd.CreateParameter();
                                        objParam.Direction = System.Data.ParameterDirection.Input;
                                        objParam.DbType = DbType.String;
                                        objParam.Size = 100;
                                        objParam.ParameterName = "@sFromDate";
                                        objParam.Value = sFromDate;
                                        objCmd.Parameters.Add(objParam);

                                        objParamout = objCmd.CreateParameter();
                                        objParamout.Direction = System.Data.ParameterDirection.Output;
                                        objParamout.DbType = DbType.String;
                                        objParamout.Size = 500;
                                        objParamout.ParameterName = "@out_err";
                                        objCmd.Parameters.Add(objParamout);
                                        objCmd.CommandText = sSql;
                                    }

                                    objCmd.ExecuteNonQuery();
                                    serr_msg = string.Empty;
                                    if (objParamout != null)
                                    {
                                        serr_msg = objParamout.Value.ToString();
                                    }
                                    if (!string.IsNullOrEmpty(serr_msg))
                                    {
                                        throw new Exception(serr_msg);
                                    }
                                    objCmd.Parameters.Clear();
                                //}
                                //catch (Exception objException)
                                //{
                                //    AppGlobal.IsError = true;
                                //    Common.WriteErrorLog(objException);
                                //}
                            }//End foreach

                            //soft delete the processed record
                            Common.DeleteRecordofPurgeHistory(iRecordId);

                        }
                        catch (Exception objException)
                        {
                            AppGlobal.IsError = true;
                            Common.WriteErrorLog(objException,true);
                        }

                    }
                    if (!bRecordFound)
                    {
                        Common.DisplayMessage("No record found for purging.", Mode.Status);
                    }

                }

                objDbConnection.Close();
                objDbConnection.Dispose();
            }

            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.AuditPurgeDatafromTableErr, objEx);
            }
            finally
            {
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }

            }
        }

        /// <summary>
        /// This function insert record into HIST_TRACK_DATASOURCE
        /// table for enabled CDC db
        /// </summary>
        /// <param name="iDbId">Database Id</param>
        internal void InsertRecHTDataSource(int iDbId)
        {
            int iNumberOfRecord = 0;
            string sUserId = string.Empty;
            string sPassword = string.Empty;
            string sServer  = string.Empty;

            Common.GetConnStrElements(ref sUserId, ref sPassword, ref sServer, AppGlobal.RMDBConnstring); 
            sUserId = RMCryptography.EncryptString(sUserId);
            sPassword = RMCryptography.EncryptString(sPassword);

            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT COUNT(*) FROM HIST_TRACK_DATASOURCE ");
            sbSql.AppendFormat("WHERE DATABASE_ID ={0}",iDbId);
            iNumberOfRecord = Conversion.CastToType<int>(DbFactory.ExecuteScalar(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId), sbSql.ToString()).ToString(), out m_blnSuccess);//Add clientId by kuladeep for Cloud Jira-1124

            sbSql.Length = 0;
            if (iNumberOfRecord > 0)
            {
                sbSql.Append("UPDATE HIST_TRACK_DATASOURCE SET ");
                sbSql.AppendFormat("DATABASE_NAME = '{0}', ",AppGlobal.RMDatabaseName);
                sbSql.AppendFormat("DSN = '{0}', ", AppGlobal.DSN);
                sbSql.AppendFormat("DATABASE_TYPE = {0}, ", Convert.ToInt32(AppGlobal.DatabaseType));
                sbSql.Append("DELETED_FLAG = 0, ");
                sbSql.AppendFormat("RM_USERID = '{0}', ",sUserId);
                sbSql.AppendFormat("RM_PASSWORD = '{0}', ", sPassword);
                sbSql.AppendFormat("SERVER_NAME = '{0}' ", sServer);
                sbSql.AppendFormat("WHERE DATABASE_ID ={0}", iDbId);

            }
            else
            {
                sbSql.Append("INSERT INTO HIST_TRACK_DATASOURCE (");
                sbSql.Append("DATABASE_ID,DATABASE_NAME,");
                sbSql.Append("DSN,DATABASE_TYPE,DELETED_FLAG, ");
                sbSql.Append("RM_USERID,RM_PASSWORD,SERVER_NAME) ");
                sbSql.AppendFormat(" VALUES ({0},'{1}',", AppGlobal.RMDatabaseId, AppGlobal.RMDatabaseName);
                sbSql.AppendFormat("'{0}',{1},{2},", AppGlobal.DSN, Convert.ToInt32(AppGlobal.DatabaseType), 0);
                sbSql.AppendFormat("'{0}','{1}','{2}')", sUserId,sPassword,sServer);
            
            }

            try
            {
                using (DbConnection objDbconnection = DbFactory.GetDbConnection(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId)))//Add clientId by kuladeep for Cloud Jira-1124
                {
                    objDbconnection.Open();

                    objDbconnection.ExecuteNonQuery(sbSql.ToString());
                    objDbconnection.Close();
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
        }

        /// <summary>
        /// Set deleted flag =-1 when
        /// cdc is disabled.
        /// </summary>
        /// <param name="iDbId">DatabaseId</param>
        internal void DeleteRecHTDataSource(int iDbId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("UPDATE HIST_TRACK_DATASOURCE SET ");
            sbSql.Append("DELETED_FLAG = -1 ");
            sbSql.AppendFormat("WHERE DATABASE_ID ={0}", iDbId);
            try
            {
                using (DbConnection objDbconnection = DbFactory.GetDbConnection(AppGlobal.HistoryTrackingDSN(AppGlobal.ClientId)))//Add clientId by kuladeep for Cloud Jira-1124
                {
                    objDbconnection.Open();

                    objDbconnection.ExecuteNonQuery(sbSql.ToString());
                    objDbconnection.Close();
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
        
        }
        #endregion 


        #region Private Method

        /// <summary>
        /// Get all the indexes of Table in Riskmaster database
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <returns>Hash table :collection of indexes</returns>
        private  Hashtable GetTableIndexes(string sTableName)
        {
            string sIndexName = string.Empty;
            string sIndexColumns = string.Empty;
            Hashtable objIndexColumns = null; 
            StringBuilder sbSql = null;
            sbSql = new StringBuilder();
            sbSql.Length = 0;
            if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
            {
                sbSql.Append(" SELECT SI.NAME INDEX_NAME,");
                sbSql.Append(" SC.NAME COLUMN_NAME ");
                sbSql.Append(" FROM SYS.OBJECTS SO INNER JOIN SYS.COLUMNS SC");
                sbSql.Append(" ON SO.OBJECT_ID = SC.OBJECT_ID");
                sbSql.Append(" INNER JOIN  SYS.INDEXES SI");
                sbSql.Append(" ON SO.OBJECT_ID = SI.OBJECT_ID");
                sbSql.Append(" INNER JOIN SYS.INDEX_COLUMNS SIC");
                sbSql.Append(" ON SO.OBJECT_ID = SIC.OBJECT_ID");
                sbSql.Append(" AND SI.OBJECT_ID = SIC.OBJECT_ID");
                sbSql.Append(" AND SI.INDEX_ID = SIC.INDEX_ID");
                sbSql.Append(" AND SC.COLUMN_ID = SIC.COLUMN_ID");
                sbSql.AppendFormat(" WHERE SO.NAME = '{0}' ORDER BY INDEX_NAME,COLUMN_NAME ", sTableName);
            }
            else
            {
                sbSql.Append("SELECT");
                sbSql.Append(" UIC.INDEX_NAME, UIC.COLUMN_NAME ");
                sbSql.Append(" FROM USER_IND_COLUMNS UIC, USER_INDEXES UI ");
                sbSql.AppendFormat(" WHERE UIC.INDEX_NAME=UI.INDEX_NAME  AND UIC.TABLE_NAME='{0}' ", sTableName);
                sbSql.Append(" ORDER BY UIC.TABLE_NAME, UIC.INDEX_NAME, UIC.COLUMN_POSITION");
            }

            //generating collection of indexexs in hash table as key-valye pair. 
            //Index name would be 'key'  and columns(seperated by comma) included in this index would be 'value'
            objIndexColumns = new Hashtable();
            using (DbReader objDbReader = DbFactory.GetDbReader(AppGlobal.RMDBConnstring, sbSql.ToString()))
            {
                objIndexColumns = new Hashtable();
                while (objDbReader.Read())
                {

                    sIndexName = objDbReader.GetString("INDEX_NAME");
                    sIndexColumns = objDbReader.GetString("COLUMN_NAME");

                    if (objIndexColumns.ContainsKey(sIndexName))
                    {
                        objIndexColumns[sIndexName] = objIndexColumns[sIndexName].ToString() + "," + sIndexColumns;
                    }
                    else
                    {
                        objIndexColumns.Add(sIndexName, sIndexColumns);
                    }


                }
            }
            return objIndexColumns;
        }
            
        /// <summary>
        /// Generate create table statement 
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <param name="iTableId">Table Id</param>
        /// <returns></returns>
        private string CreateTableStatement(string sTableName, int iTableId)
        {
            string sCreateStatement = string.Empty;
            StringBuilder sbSQL = null;
            DbConnection objDbConnection = null;
            objDbConnection = DbFactory.GetDbConnection(AppGlobal.RMDBConnstring);
            try
            {
                objDbConnection.Open();
                sbSQL = new StringBuilder();
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sbSQL.Length = 0;
                    sbSQL.Append(" DECLARE @Sql AS VARCHAR(8000) ");
                    sbSQL.AppendFormat(" SET @sql = 'CREATE TABLE {0}_HIST_{1}(RECORD_ID int,DATE_TIMESTAMP VARCHAR(14),OPERATION VARCHAR(10), '  ", sTableName, AppGlobal.RMDatabaseId.ToString());
                    sbSQL.Append("SELECT @sql = @sql+ COLUMN_NAME + ' ' + DATATYPE +");
                    sbSQL.Append("CASE WHEN DATATYPE = 'varchar' THEN '(' + CAST(COLUMN_SIZE AS VARCHAR(5)) + '), ' ELSE ', ' END ");
                    sbSQL.Append("FROM HIST_TRACK_DICTIONARY ");
                    sbSQL.AppendFormat("WHERE TABLE_ID = {0}", iTableId);
                    sbSQL.Append(" SELECT @Sql");

                    sCreateStatement = objDbConnection.ExecuteString(sbSQL.ToString());
                    //converting varchar(-1)to varchar(max) because for max we have -1 in our HIST_TRACK_DICTIONARY table
                    sCreateStatement = sCreateStatement.Replace("varchar(-1)", "varchar(max)");


                }
                else
                {
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT ");
                    sbSQL.AppendFormat("'CREATE TABLE {0}_HIST_{1} (RECORD_ID NUMBER(10),DATE_TIMESTAMP VARCHAR2(14),OPERATION VARCHAR2(10),'|| ", sTableName, AppGlobal.RMDatabaseId.ToString()); 
                    sbSQL.Append("SUBSTR(MAX(REPLACE(");
                    sbSQL.Append("SYS_CONNECT_BY_PATH( COLUMN_NAME ||' '|| DATATYPE || CASE  WHEN DATATYPE = 'VARCHAR2' OR ");

                    sbSQL.Append("DATATYPE = 'VARCHAR'   THEN ");
                    sbSQL.Append(" '('||COLUMN_SIZE||')' ELSE CASE  WHEN NUMERIC_PRECISION IS NOT NULL AND NUMERIC_PRECISION <> 0 ");

                    sbSQL.Append(" THEN '('||NUMERIC_PRECISION||','|| NUMERIC_SCALE ||')' ELSE ' ' END  END||',',  '/') ");
                    sbSQL.Append(",'/',' ')),2) CREATE_STATEMENT ");
                    sbSQL.Append(" FROM (");
                    sbSQL.Append("SELECT ");
                    sbSQL.Append("A.*, ");
                    sbSQL.Append(" ROW_NUMBER() OVER (PARTITION BY TABLE_ID ORDER BY TABLE_ID ) ROW# ");
                    sbSQL.AppendFormat("FROM HIST_TRACK_DICTIONARY A WHERE TABLE_ID ={0}) ", iTableId);
                    sbSQL.Append(" START WITH ROW#=1 ");
                    sbSQL.Append(" CONNECT BY PRIOR table_id = table_id ");
                    sbSQL.Append(" AND PRIOR ROW# = ROW# -1 ");
                    sbSQL.Append(" GROUP BY TABLE_ID ");
                    sCreateStatement = objDbConnection.ExecuteString(sbSQL.ToString());



                }
                objDbConnection.Close();
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.AuditCreateTableStatementErr, sTableName.ToUpper()), objEx);
            }
            finally
            {
                if (objDbConnection != null)
                    objDbConnection.Close();
            }
            return sCreateStatement;
        }

        #endregion

    }
}
