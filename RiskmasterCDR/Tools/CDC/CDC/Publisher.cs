﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Tools.CDC
{
    class Publisher
    {

        #region Private Members
        private const string CHANGE_SET_NAME = "CDC_RM_CHANGESET";
        private const string CHANGE_SET_DESC = "RISKMASTER CHANGESET";
        private const string CHANGE_SOURCE_NAME = "HOTLOG_SOURCE";
        private bool m_blnSuccess = false;

        #endregion


        #region Internal Method

        /// <summary>
        /// For Sql this function enables CDC on
        /// database level 
        /// For oracle it does all the operations
        /// which would be done only one time
        /// </summary>
        
        internal void EnableCDC()
        {
            string sSql = string.Empty;
            try
            {
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    //Create chageset and Enable capture 
                    CreateChangeSet();
                    EnableCapture();

                }
                else
                {
                    //Enabling CDC on database level
                    sSql = "SYS.SP_CDC_ENABLE_DB";
                    RunSql(sSql);

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.PublisherEnableCDCErr, objEx);
            }
      
        }

        /// <summary>
        /// This function disable CDC on database 
        /// level.
        /// </summary>
        internal void DisableCDC()
        {   
            string sSql = string.Empty;
            try
            {
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    //Disable CDC on database level
                    sSql = "SYS.SP_CDC_DISABLE_DB";
                    RunSql(sSql);
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.PublisherDisableCDCErr, objEx);
            }
            
        }

        /// <summary>
        /// This function Instantiae Table 
        /// for CDC setup.
        /// </summary>
        /// <param name="p_sTableName"> Table Name </param>
        internal void InstantiateSourceTable(string p_sTableName)
        {
            StringBuilder sbSql = null;
            try
            {
                sbSql = new StringBuilder();
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sbSql.Append("BEGIN DBMS_CAPTURE_ADM.PREPARE_TABLE_INSTANTIATION('");
                    sbSql.Append(AppGlobal.DBOUserId);
                    sbSql.Append(".");
                    sbSql.Append(p_sTableName.ToUpper());
                    sbSql.Append("'); End;");
                    RunSql(sbSql.ToString());

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.PublisherInstantiateSourceTableErr,p_sTableName.ToUpper()), objEx);
            }

        }       

        /// <summary>
        /// This function creates change table to enable 
        /// CDC on Table of Oracle.
        /// </summary>
        /// <param name="p_objTable">Object of Table</param>
        internal void CreateChangeTable(Table p_objTable)
        {
            string sColumn_typelist = string.Empty; 
            StringBuilder sbSql = null;
            sbSql = new StringBuilder();
            try
            {
                if (p_objTable != null)
                {
                    //Before ceating a new change table ,drop it if already exist
                    DropChangeTable(p_objTable.TableName);
                    //get the list of columns on which tracking would be enabled
                    sColumn_typelist = Common.GetColumnTypeList(p_objTable);
                }
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {

                    sbSql.Append("BEGIN DBMS_CDC_PUBLISH.CREATE_CHANGE_TABLE('");
                    sbSql.Append(AppGlobal.CDCAdminUID + "' ,'");
                    sbSql.AppendFormat("HIST_{0}_CT','", p_objTable.TableName);
                    sbSql.AppendFormat("{0}_{1}','",CHANGE_SET_NAME,AppGlobal.RMDatabaseId.ToString());
                    sbSql.Append(AppGlobal.DBOUserId.ToUpper() + "','");
                    sbSql.Append(p_objTable.TableName + "','");
                    sbSql.Append(sColumn_typelist + "',");
                    sbSql.Append("'BOTH', 'Y', 'N', 'N', 'N', 'N', 'N', 'Y', NULL); END;");
                    RunSql(sbSql.ToString());

                    
                    //After creating change table add primary key
                    //because we need primary key in change table while poulating data into audit tables and deleting same data from
                    //change table
                    sbSql.Length = 0;
                    sbSql.AppendFormat("ALTER TABLE HIST_{0}_CT ", p_objTable.TableName);
                    sbSql.AppendFormat(" ADD CONSTRAINT HIST_{0}_CT_PK PRIMARY KEY", p_objTable.TableName);
                    sbSql.Append("(RSID$,OPERATION$)");
                    RunSql(sbSql.ToString());

                    //Enable capture of data
                    EnableCapture();


                }
                else
                {
                    sbSql.Append("SYS.SP_CDC_ENABLE_TABLE ");
                    sbSql.AppendFormat("@source_schema = '{0}',", AppGlobal.DBOUserId);
                    sbSql.AppendFormat("@source_name = '{0}',", p_objTable.TableName);
                    sbSql.Append("@role_name = null,");
                    sbSql.Append("@supports_net_changes = 0,");
                    sbSql.AppendFormat("@captured_column_list = '{0}',", sColumn_typelist);
                    sbSql.AppendFormat("@capture_instance='HIST_{0}'", p_objTable.TableName.ToUpper());

                    RunSql(sbSql.ToString());

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.PublisherCreateChangeTableErr, p_objTable.TableName.ToUpper()), objEx);
            }


        }
       
        /// <summary>
        /// This function abort the instantiation of table while 
        /// disabling CDC on Table in oracle.
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        internal void AbortTableInstantiation(string sTableName)
        {           
            StringBuilder sbSql = null;
            try
            {
                sbSql = new StringBuilder();

                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {

                    if (IsTableInstantiated (sTableName))
                    {
                        sbSql.Append("BEGIN DBMS_CAPTURE_ADM.ABORT_TABLE_INSTANTIATION('");
                        sbSql.Append(AppGlobal.DBOUserId);
                        sbSql.Append(".");
                        sbSql.Append(sTableName);
                        sbSql.Append("'); END;");
                        RunSql(sbSql.ToString());
                    }
                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.PublisherAbortTableInstantiationErr, sTableName.ToUpper()), objEx);
            }
        }

        /// <summary>
        /// This function drop change table while
        /// disabling CDC on Table in oracle.
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        internal void DropChangeTable(string sTableName)
        {             
            StringBuilder sbSql = null;

            try
            {
                sbSql = new StringBuilder();

                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {

                    if (IsChangeTableExist(sTableName)) //if exist
                    {
                        sbSql.Append("BEGIN DBMS_CDC_PUBLISH.DROP_CHANGE_TABLE('");
                        sbSql.Append(AppGlobal.CDCAdminUID.ToUpper() + "',");
                        sbSql.AppendFormat("'HIST_{0}_CT','Y'); END;", sTableName.ToUpper());
                        RunSql(sbSql.ToString());

                    }


                }
                else
                {

                    if (IsChangeTableExist(sTableName))
                    {
                        sbSql.Append("SYS.SP_CDC_DISABLE_TABLE ");
                        sbSql.AppendFormat("@source_schema = '{0}', ", AppGlobal.DBOUserId.ToUpper());
                        sbSql.AppendFormat("@source_name = '{0}' ,", sTableName.ToUpper());
                        sbSql.AppendFormat("@capture_instance = 'HIST_{0}'", sTableName.ToUpper());
                        RunSql(sbSql.ToString());
                    }

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(string.Format(Message.PublisherDropChangeTableErr, sTableName.ToUpper()), objEx);
            }
        }

        /// <summary>
        /// This function drop changeset while disabling 
        /// CDC on table
        /// </summary>
        internal void DropChangeSet()
        {   StringBuilder sbSql = null;
        try
        {
            sbSql = new StringBuilder();
            if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sbSql.AppendFormat("BEGIN DBMS_CDC_PUBLISH.DROP_CHANGE_SET('{0}_{1}'); END;", CHANGE_SET_NAME,AppGlobal.RMDatabaseId.ToString());
                RunSql(sbSql.ToString());
            }
        }
        catch (RMAppException objEx)
        {

            throw (objEx);
        }
        catch (Exception objEx)
        {

            throw new RMAppException(Message.PublisherDropChangeSetErr, objEx);
        }
        }

        /// <summary>
        /// This function check whether CDC cleanup job
        /// exist or not
        /// </summary>
        /// <returns>true/false</returns>
        internal bool IsCDCCleanupJobExist()
        {
            const string JOB_NAME_COLUMN = "job_name";
            string sSql = "SP_CDC_HELP_JOBS";
            string sCDCCleaupJobName = string.Format("CDC.{0}_capture", AppGlobal.RMDatabaseName);
            bool bCDCCleanupJob = false;
           
            try
            {

                DbConnection objDbconnection = DbFactory.GetDbConnection(AppGlobal.RMAdminConnstring);
                //Get a DataReader object
                using (DbReader rdr = DbFactory.GetDbReader(AppGlobal.RMAdminConnstring, sSql))
                {
                    //Loop through all of the records
                    while (rdr.Read())
                    {
                        string sJobName = rdr[JOB_NAME_COLUMN].ToString();

                        //check if job name matches
                        if (sJobName.ToLower().Equals(sCDCCleaupJobName.ToLower()))
                        {
                            bCDCCleanupJob = true;
                            break;
                        }//if
                        
                    }//while
                }//using
            }
            catch (RMAppException objEx)
        {

            throw (objEx);
        }
        catch (Exception objEx)
        {

            throw new RMAppException(Message.PublisherIsCDCCleanupJob, objEx);
        }
            return bCDCCleanupJob;
        }

        /// <summary>
        /// This function change the retention criteria of CDC Cleanup job
        /// </summary>
        internal void ChangeRetentionCriteria()
        {
            StringBuilder sbSql = null;
            const int EIGHT_DAYS = 11520;    // (8*24)*60 = 11520 minutes
            try
            {
                sbSql = new StringBuilder();
                if ((AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR) && IsCDCCleanupJobExist())
                {
                    sbSql.Append("SYS.SP_CDC_CHANGE_JOB ");
                    sbSql.Append(" @job_type = N'cleanup' ,");
                    sbSql.AppendFormat("@retention = {0}", EIGHT_DAYS);

                    RunSql(sbSql.ToString());

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.PublisherChangeRetentionCriteria, objEx);
            }
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Check whether changetable exist or not
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <returns></returns>
        private bool IsChangeTableExist(string sTableName)
        {
            StringBuilder sbSql =null;
            sbSql = new StringBuilder();
            int iNumofRecords =0;
            bool bChangeTable = false;
            if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sbSql.Append(" SELECT COUNT(*) FROM USER_OBJECTS ");
                sbSql.Append(string.Format(" WHERE OBJECT_TYPE ='TABLE' AND OBJECT_NAME ='HIST_{0}_CT'", sTableName.ToUpper()));

            }
            else
            {
                sbSql.AppendFormat("SELECT COUNT(*) FROM CDC.CHANGE_TABLES WHERE CAPTURE_INSTANCE ='HIST_{0}'", sTableName.ToUpper());
                            
            }
            iNumofRecords = Conversion.CastToType<int>(DbFactory.ExecuteScalar(AppGlobal.RMAdminConnstring, sbSql.ToString()).ToString(), out m_blnSuccess);

            if (iNumofRecords > 0)
                bChangeTable = true;

            return bChangeTable;
               
        }

        /// <summary>
        /// Instantiate Source table
        /// </summary>
        /// <param name="sTableName">Table Name</param>
        /// <returns></returns>
        private bool IsTableInstantiated(string sTableName)
        {
            StringBuilder sbSql = null;
            sbSql = new StringBuilder();
            int iNumofRecords = 0;
            bool bTableInstantiated = false;
            if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sbSql.Append(" SELECT COUNT(*) FROM DBA_CAPTURE_PREPARED_TABLES ");
                sbSql.Append(string.Format(" WHERE TABLE_NAME ='{0}'", sTableName.ToUpper()));

            }
            
            iNumofRecords = Conversion.CastToType<int>(DbFactory.ExecuteScalar(AppGlobal.RMAdminConnstring, sbSql.ToString()).ToString(), out m_blnSuccess);

            if (iNumofRecords > 0)
                bTableInstantiated = true;

            return bTableInstantiated;

        }

        /// <summary>
        /// Execute sql query
        /// </summary>
        /// <param name="sSql"></param>
        private void RunSql(string sSql)
        {
            DbConnection objDbConnection = null;
            DbCommand objCmd = null;
            try
            {
                objDbConnection = DbFactory.GetDbConnection(AppGlobal.RMAdminConnstring);
                objDbConnection.Open();
                objCmd = objDbConnection.CreateCommand();

                objCmd.CommandText = sSql;
                objCmd.ExecuteNonQuery();

                objDbConnection.Close();
                objDbConnection.Dispose();
            }
            finally
            {
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }

            }

        }

        /// <summary>
        /// This function create changeset for 
        /// CDC setup in Oracle.
        /// </summary>
        private void CreateChangeSet()
        {
            StringBuilder sbSql = null;
            try
            {
                sbSql = new StringBuilder();
                sbSql.Append("BEGIN DBMS_CDC_PUBLISH.CREATE_CHANGE_SET( '");
                sbSql.AppendFormat("{0}_{1}','",CHANGE_SET_NAME,AppGlobal.RMDatabaseId.ToString());
                sbSql.Append(CHANGE_SET_DESC + "','");
                sbSql.Append(CHANGE_SOURCE_NAME + "',");
                sbSql.Append("'Y', NULL,NULL ); END;");
                RunSql(sbSql.ToString());
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.PublisherCreateChangeSetErr, objEx);
            }
        }

        /// <summary>
        /// This function Enable change data capture 
        /// in Oracle.
        /// </summary>
        private void EnableCapture()
        {
            StringBuilder sbSql = null;
            try
            {
                sbSql = new StringBuilder();
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sbSql.Append("BEGIN DBMS_CDC_PUBLISH.ALTER_CHANGE_SET(change_set_name=>'");
                    sbSql.AppendFormat("{0}_{1}",CHANGE_SET_NAME,AppGlobal.RMDatabaseId.ToString());
                    sbSql.Append("',enable_capture=> 'Y'); End;");
                    RunSql(sbSql.ToString());

                }
            }
            catch (RMAppException objEx)
            {

                throw (objEx);
            }
            catch (Exception objEx)
            {

                throw new RMAppException(Message.PublisherEnableCaptureErr, objEx);
            }
        }

        #endregion


    }

    
}
