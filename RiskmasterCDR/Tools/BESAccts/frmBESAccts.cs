using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BESAccts
{
    public partial class frmBESAccts : Form
    {
        public frmBESAccts()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Handles the Windows Form Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBESAccts_Load(object sender, EventArgs e)
        {
            dgvBESAccts.DataSource = DecryptedAccts();
        }

        /// <summary>
        /// Displays all of the Encrypted/Decrypted User Accts 
        /// for BES which are stored in the USER_ORGSEC table
        /// </summary>
        /// <returns></returns>
        private DataTable DecryptedAccts()
        {
            Database db = DatabaseFactory.CreateDatabase();
            IDataReader idr = db.ExecuteReader(CommandType.Text, "SELECT DB_UID, DB_PWD FROM USER_ORGSEC ORDER BY DB_UID");
            DataTable dtbBESAccts = new DataTable();
            dtbBESAccts.Columns.Add("Encrypted USER ID");
            dtbBESAccts.Columns.Add("Encypted USER PWD");
            dtbBESAccts.Columns.Add("Decrypted USER ID");
            dtbBESAccts.Columns.Add("Decrypted USER PWD");

            while (idr.Read())
            {
                DataRow dtr = dtbBESAccts.NewRow();
                dtr[0] = idr["DB_UID"].ToString();
                dtr[1] = idr["DB_PWD"].ToString();
                dtr[2] = Decrypt(idr["DB_UID"].ToString());
                dtr[3] = Decrypt(idr["DB_PWD"].ToString());

                //Add the row to the DataTable
                dtbBESAccts.Rows.Add(dtr);
            } // while

            return dtbBESAccts;
        } // method: DecryptedAccts

        /// <summary>
        /// Decrypts Riskmaster encrypted information
        /// </summary>
        /// <returns></returns>
        private string Decrypt(string strEncrypted)
        {
            string strDecrypted = string.Empty;

            Riskmaster.Security.Encryption.DTGCrypt32 objDecrypt = new Riskmaster.Security.Encryption.DTGCrypt32();
            strDecrypted = objDecrypt.DecryptString(strEncrypted, "6378b87457a5ecac8674e9bac12e7cd9");


            //Clean up
            objDecrypt = null;

            return strDecrypted;
        } // method: Decrypt


    }
}