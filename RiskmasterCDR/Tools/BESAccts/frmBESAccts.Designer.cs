namespace BESAccts
{
    partial class frmBESAccts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvBESAccts = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBESAccts)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBESAccts
            // 
            this.dgvBESAccts.AllowUserToAddRows = false;
            this.dgvBESAccts.AllowUserToDeleteRows = false;
            this.dgvBESAccts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBESAccts.Location = new System.Drawing.Point(12, 12);
            this.dgvBESAccts.Name = "dgvBESAccts";
            this.dgvBESAccts.ReadOnly = true;
            this.dgvBESAccts.Size = new System.Drawing.Size(642, 376);
            this.dgvBESAccts.TabIndex = 0;
            // 
            // frmBESAccts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(666, 400);
            this.Controls.Add(this.dgvBESAccts);
            this.Name = "frmBESAccts";
            this.Text = "Display BES Accounts";
            this.Load += new System.EventHandler(this.frmBESAccts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBESAccts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBESAccts;
    }
}

