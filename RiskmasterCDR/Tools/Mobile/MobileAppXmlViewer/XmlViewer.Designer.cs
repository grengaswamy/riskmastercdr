﻿namespace MobileAppXmlViewer
{
    partial class XmlViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRequest = new System.Windows.Forms.TextBox();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.lblRequest = new System.Windows.Forms.Label();
            this.lblResponse = new System.Windows.Forms.Label();
            this.cmdViewResponse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtRequest
            // 
            this.txtRequest.AcceptsReturn = true;
            this.txtRequest.AcceptsTab = true;
            this.txtRequest.HideSelection = false;
            this.txtRequest.Location = new System.Drawing.Point(32, 71);
            this.txtRequest.MaxLength = 2147483647;
            this.txtRequest.Multiline = true;
            this.txtRequest.Name = "txtRequest";
            this.txtRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRequest.Size = new System.Drawing.Size(841, 234);
            this.txtRequest.TabIndex = 18;
            this.txtRequest.WordWrap = false;
            // 
            // txtResponse
            // 
            this.txtResponse.AcceptsReturn = true;
            this.txtResponse.AcceptsTab = true;
            this.txtResponse.HideSelection = false;
            this.txtResponse.Location = new System.Drawing.Point(32, 362);
            this.txtResponse.MaxLength = 2147483647;
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResponse.Size = new System.Drawing.Size(841, 233);
            this.txtResponse.TabIndex = 19;
            this.txtResponse.WordWrap = false;
            // 
            // lblRequest
            // 
            this.lblRequest.BackColor = System.Drawing.SystemColors.Control;
            this.lblRequest.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblRequest.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequest.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblRequest.Location = new System.Drawing.Point(33, 43);
            this.lblRequest.Name = "lblRequest";
            this.lblRequest.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblRequest.Size = new System.Drawing.Size(582, 25);
            this.lblRequest.TabIndex = 22;
            this.lblRequest.Text = "Please enter Request XML";
            // 
            // lblResponse
            // 
            this.lblResponse.BackColor = System.Drawing.SystemColors.Control;
            this.lblResponse.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblResponse.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponse.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblResponse.Location = new System.Drawing.Point(33, 334);
            this.lblResponse.Name = "lblResponse";
            this.lblResponse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblResponse.Size = new System.Drawing.Size(582, 25);
            this.lblResponse.TabIndex = 23;
            this.lblResponse.Text = "Response XML";
            // 
            // cmdViewResponse
            // 
            this.cmdViewResponse.BackColor = System.Drawing.SystemColors.Control;
            this.cmdViewResponse.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdViewResponse.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdViewResponse.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdViewResponse.Location = new System.Drawing.Point(734, 43);
            this.cmdViewResponse.Name = "cmdViewResponse";
            this.cmdViewResponse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdViewResponse.Size = new System.Drawing.Size(101, 25);
            this.cmdViewResponse.TabIndex = 24;
            this.cmdViewResponse.Text = "View Response";
            this.cmdViewResponse.UseVisualStyleBackColor = false;
            this.cmdViewResponse.Click += new System.EventHandler(this.cmdViewResponse_Click);
            // 
            // XmlViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.ClientSize = new System.Drawing.Size(942, 607);
            this.Controls.Add(this.cmdViewResponse);
            this.Controls.Add(this.lblResponse);
            this.Controls.Add(this.lblRequest);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.txtRequest);
            this.Name = "XmlViewer";
            this.Text = "Xml Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtRequest;
        internal System.Windows.Forms.TextBox txtResponse;
        public System.Windows.Forms.Label lblRequest;
        public System.Windows.Forms.Label lblResponse;
        public System.Windows.Forms.Button cmdViewResponse;
    }
}

