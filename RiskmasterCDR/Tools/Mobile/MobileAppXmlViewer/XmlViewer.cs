﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

namespace MobileAppXmlViewer
{
    public partial class XmlViewer : Form
    {
        public XmlViewer()
        {
            InitializeComponent();
        }
        
        CommonWCFServiceMobAdj.CommonWCFServiceClient objClientMobAdj = new CommonWCFServiceMobAdj.CommonWCFServiceClient();
        CommonLocal.CommonWCFServiceClient objClientLocal = new CommonLocal.CommonWCFServiceClient();
      //  CommonWCFServiceLocal.CommonWCFServiceClient objClientLocal = new CommonWCFServiceLocal.CommonWCFServiceClient();
        //AuthenticationService.AuthenticationServiceClient objAuthentication1 = new AuthenticationService.AuthenticationServiceClient();
        CodesServiceLocal.CodesServiceClient objCodesServiceLocal = new CodesServiceLocal.CodesServiceClient();
        //CodesServiceDMZ.CodesServiceClient objCodesServiceMobAdj = new CodesServiceDMZ.CodesServiceClient();
        CodesServiceDMZ.CodesServiceClient objCodesServiceMobAdj = new CodesServiceDMZ.CodesServiceClient();
 //      CommonWCFServiceServer.CommonWCFServiceClient objCodesServiceCl = new CommonWCFServiceServer.CommonWCFServiceClient();
       ProgressNoteService.ProgressNoteServiceClient objProgressNoteService = new ProgressNoteService.ProgressNoteServiceClient();
       ProgressNoteServiceLocal.ProgressNoteServiceClient objProgressNoteServiceLocal = new ProgressNoteServiceLocal.ProgressNoteServiceClient();
       AuthLocal.AuthenticationServiceClient objAuthLocal = new AuthLocal.AuthenticationServiceClient();
           MobileAdjusterService.AdjusterServiceClient objAdjusterClient = new MobileAdjusterService.AdjusterServiceClient();
   //     CommonServiceClient.CommonWCFServiceClient objServiceClient = new CommonServiceClient.CommonWCFServiceClient();
        UploadService.MobileUploadServiceClient objUploadClient = new UploadService.MobileUploadServiceClient();
      //  MobileUploadServiceDMZ.MobileUploadServiceClient objUploadDMZ = new MobileUploadServiceDMZ.MobileUploadServiceClient();
        AuthenticationDMZ.Service1Client objDMZAuth = new AuthenticationDMZ.Service1Client();

        AuthenticationLocal.Service1Client objLocalAuth = new AuthenticationLocal.Service1Client();
        AuthDMZ.AuthenticationServiceClient objDMZAuthentication = new AuthDMZ.AuthenticationServiceClient();
        
        
        NewAuthenticationLocal.MobileAuthenticationServiceClient objNewAuthLocal = new NewAuthenticationLocal.MobileAuthenticationServiceClient();
        NewCodesLocal.MobileCodesServiceClient objNewCodesLocal = new NewCodesLocal.MobileCodesServiceClient();
        NewCommonWCFLocal.MobileCommonWCFServiceClient objNewCommonLocal = new NewCommonWCFLocal.MobileCommonWCFServiceClient();
        NewDocumentLocal.MobileDocumentServiceClient objNewDocumentLocal = new NewDocumentLocal.MobileDocumentServiceClient();
        NewProgressLocal.MobileProgressNoteServiceClient objNewProgressLocal = new NewProgressLocal.MobileProgressNoteServiceClient();
        NewAdjusterLocal.MobileAdjusterServiceClient objNewAdjusterLocal = new NewAdjusterLocal.MobileAdjusterServiceClient();
        //NewUploadLocal.MobileUploadServiceClient objNewUploadLocal = new NewUploadLocal.MobileUploadServiceClient();

        NewCommonWCFDMZ.MobileCommonWCFServiceClient objNewCommonDMZ = new NewCommonWCFDMZ.MobileCommonWCFServiceClient();
        AuthNewDMZ.MobileAuthenticationServiceClient objNewAuthDMZ = new AuthNewDMZ.MobileAuthenticationServiceClient();
        RMA131Common.CommonWCFServiceClient objRMA131Common = new RMA131Common.CommonWCFServiceClient();
        RMA131LocalCommon.CommonWCFServiceClient objRMA131CommonLocal = new RMA131LocalCommon.CommonWCFServiceClient();
        MoboUpload.MobileUploadServiceClient objMoboUpload = new MoboUpload.MobileUploadServiceClient();

       // RMA131MoboUploadLocal.MobileUploadServiceClient objRMA131UploadLocal = new RMA131MoboUploadLocal.MobileUploadServiceClient();

        RMA165Upload.MobileUploadServiceClient objRMA131Upload = new RMA165Upload.MobileUploadServiceClient();
        RMA131ProgressNotesLocal.MobileProgressNoteServiceClient objProgressNotes = new RMA131ProgressNotesLocal.MobileProgressNoteServiceClient();
        RMA131CodesServiceLocal.MobileCodesServiceClient objCodesService = new RMA131CodesServiceLocal.MobileCodesServiceClient();
        RMA131CommonDMZ.MobileCommonWCFServiceClient objCommonDMZ = new RMA131CommonDMZ.MobileCommonWCFServiceClient();
        ServiceReference1.MobileUploadServiceClient objMobo = new ServiceReference1.MobileUploadServiceClient();


        //New services For Bob
        BobMoboAuthentication.MobileAuthenticationServiceClient objBobAuthentication = new BobMoboAuthentication.MobileAuthenticationServiceClient();
        BobMoboCommonWCF.MobileCommonWCFServiceClient objBobCommonWCF = new BobMoboCommonWCF.MobileCommonWCFServiceClient();
        BobMoboCodes.MobileCodesServiceClient objBobCodes = new BobMoboCodes.MobileCodesServiceClient();
        //BobMoboUpload.MobileUploadServiceClient objMoboUpload = new BobMoboUpload.MobileUploadServiceClient();

        KTMoboCommonWCFService.MobileCommonWCFServiceClient objKTMoboCommon = new KTMoboCommonWCFService.MobileCommonWCFServiceClient();
        //New services For Bob
        private void cmdViewResponse_Click(object sender, EventArgs e)
        {
            XmlDocument objXmlDocument = null;
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                StringBuilder sb = new StringBuilder();
                ArrayList strResponses = new ArrayList(7);
                ArrayList strRequests = new ArrayList();
                int count = 0;

                strRequests.Add("<Message><Authorization>114a4781-bb67-41f7-9367-ac9cbcfeeeb2</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>DIMD00244676866</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>WC1990000002</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>WC1991000004</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>VAPD1992000005</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>GCGL1992000006</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>GCPL1996000008</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");
                //strRequests.Add("<Message><Authorization>5b6fec72-9684-424c-adb0-d6b54aa67793</Authorization><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobileAdjuster</caller><ClaimNumber>GCPROP1993000009</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>");

           //     txtResponse.Text = FormatXmlText(objAuthLocal.GetUserDSNsMobileAdjuster("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>aman</UserName><Password></Password></UserInfo></Authorization></Document></Message>"));
                //Thread.Sleep(10000);
                //TimeSpan ts1 = stopWatch.Elapsed;
               // string elapsedTime1 =
                //       String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                 //          ts1.Hours, ts1.Minutes, ts1.Seconds, ts1.Milliseconds / 10);
              //  if (!string.IsNullOrEmpty(txtRequest.Text))
                //string response = string.Empty;
                //for (int i = 0; i < 7; i++)
                //{
                //  response  = response + " , " + FormatXmlText(objClientMobAdj.ProcessRequest(txtRequest.Text));
                //}
                // txtResponse.Text = response;
             //  txtResponse.Text = FormatXmlText(objLocalAuth.GetUserSessionID("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>abk</UserName><DSNName>Bob_Amitosh</DSNName></UserInfo></Authorization></Document></Message>"));
             //  txtResponse.Text = FormatXmlText(objDMZAuth.GetUserSessionID("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>csc</UserName><DSNName>RMXR7_MOBILE_FEB16</DSNName></UserInfo></Authorization></Document></Message>"));
                //txtResponse.Text = objDMZAuthentication.GetUserSessionID("csc", "RMXR7_MOBILE_FEB16");
             //   txtResponse.Text = FormatXmlText(objDMZAuthentication.AuthenticateMobileUser("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>csc</UserName><DSNName>RMXR7_MOBILE_FEB16</DSNName></UserInfo></Authorization></Document></Message>"));
               //txtResponse.Text = FormatXmlText(objClientMobAdj.ProcessRequest(txtRequest.Text));
           //           txtResponse.Text = FormatXmlText(objClientLocal.ProcessRequest(txtRequest.Text));
              //     txtResponse.Text = FormatXmlText(objUploadClient.ProcessRequest(txtRequest.Text));
            //       txtResponse.Text = FormatXmlText(objCodesServiceCl.ProcessRequest(txtRequest.Text));
         //         txtResponse.Text = FormatXmlText(objProgressNoteService.LoadProgressNotesPartialForMobileAdj(txtRequest.Text));
   //             txtResponse.Text = FormatXmlText(objProgressNoteServiceLocal.LoadProgressNotesPartialForMobileAdj(txtRequest.Text));
        //        txtResponse.Text = FormatXmlText(objProgressNoteService.LoadProgressNotesPartialForMobileAdj(txtRequest.Text));
   //             txtResponse.Text = FormatXmlText(objProgressNoteService.SaveNotesForMobileAdj(txtRequest.Text));
     //           txtResponse.Text = FormatXmlText(objProgressNoteServiceLocal.SaveNotesForMobileAdj(txtRequest.Text));
       //      txtResponse.Text = FormatXmlText(objCodesServiceMobAdj.GetCodesForMobileAdj(txtRequest.Text));
            // txtResponse.Text = FormatXmlText(objCodesServiceLocal.GetCodesForMobileAdj(txtRequest.Text));
                //string sUserInfo = "<UserInfo><UserName>abk</UserName><DSNName>RMXR7_MOBILE_FEB16</DSNName></UserInfo>";
                //string sUserInfo1 = "<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>abk</UserName><DSNName>Bob_Amitosh</DSNName></UserInfo></Authorization></Document></Message>";
                //if (!string.IsNullOrEmpty(txtRequest.Text))
                //{
                //  //    txtResponse.Text = objAuthentication.GetUserSessionID("abk", "Bob_Amitosh");
                //    txtResponse.Text = FormatXmlText(objAuthentication.AuthenticateMobileUser(txtRequest.Text));
                //    // txtResponse.Text = FormatXmlText(objAuthentication.AuthenticateMobileUser(txtRequest.Text));
                //    //txtResponse.Text = FormatXmlText(objAuthLocal.GetUserSessionID(txtRequest.Text));
                    
                //}
                
             //  txtResponse.Text = FormatXmlText(objProgressNoteService.SaveNotesForMobileAdj(txtRequest.Text));
             //   txtResponse.Text = FormatXmlText(objAuthentication.GetUserSessionID(txtRequest.Text));
     //        txtResponse.Text = FormatXmlText(objAdjusterClient.AdjusterRowId(txtRequest.Text));
       //  txtResponse.Text = FormatXmlText(objServiceClient.ProcessRequest(txtRequest.Text));
           //    txtResponse.Text = FormatXmlText(objUploadClient.UploadRequest(txtRequest.Text));
           //     txtResponse.Text =objAuthentication.GetUserSessionID("abk","AdjMobile");
             
                 stopWatch.Stop();
                 // Get the elapsed time as a TimeSpan value.
                 TimeSpan ts = stopWatch.Elapsed;

                 // Format and display the TimeSpan value.
                 string elapsedTime =
                       String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                           ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                 ////Console.WriteLine("RunTime " + elapsedTime);
              //   txtResponse.Text = txtResponse.Text + "\nRunTime :" + elapsedTime;

          //      txtResponse.Text = FormatXmlText(objUploadDMZ.UploadRequest(txtRequest.Text));

                 //foreach (object s in strRequests)
                 //{
                 //    //  strResponses.Add(objClientLocal.ProcessRequest(s.ToString()));
                 //    strResponses.Add(objRMA131UploadLocal.ProcessRequest(s.ToString()));
                 //      txtResponse.Text = txtResponse.Text + "\n" + strResponses[count].ToString();
                 //    count++;
                 //}

            //    txtResponse.Text = FormatXmlText(objNewAuthLocal.AuthenticateMobileUser("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>aman</UserName><DSNName>DMZ_AMAN</DSNName></UserInfo></Authorization></Document></Message>"));
           
              // txtResponse.Text = FormatXmlText(objNewAuthLocal.GetUserDSNsMobileAdjuster("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>aman</UserName><Password>aman</Password></UserInfo></Authorization></Document></Message>"));
              //  txtResponse.Text = FormatXmlText(objNewCodesLocal.GetCodesForMobileAdj(txtRequest.Text));
        //   txtResponse.Text = FormatXmlText(objNewCommonLocal.ProcessRequest(txtRequest.Text));
               // objNewDocumentLocal.CreateDocTypeFromXml_MobAdj(txtRequest.Text);
           //   txtResponse.Text = objNewDocumentLocal.MobileDocumentUploadRequest(txtRequest.Text);
             // txtResponse.Text = FormatXmlText(objNewProgressLocal.LoadProgressNotesPartialForMobileAdj(txtRequest.Text));
           //     txtResponse.Text = FormatXmlText(objNewProgressLocal.SaveNotesForMobileAdj(txtRequest.Text));
            //   txtResponse.Text = FormatXmlText(objNewAdjusterLocal.AdjusterRowId(txtRequest.Text));
             //   txtResponse.Text = FormatXmlText(objNewUploadLocal.ProcessRequest(txtRequest.Text));
            //   txtResponse.Text = FormatXmlText(objNewUploadLocal.UploadRequest(txtRequest.Text));
              //   txtResponse.Text = FormatXmlText(objNewCommonDMZ.ProcessRequest(txtRequest.Text));
           //      txtResponse.Text = FormatXmlText(objMoboUpload.ProcessRequest(txtRequest.Text));
              //  txtResponse.Text = FormatXmlText(objNewAuthDMZ.AuthenticateMobileUser("<Message><Call><Function>LoginAdaptor.Authorization</Function></Call><Document><Authorization><UserInfo><UserName>csc</UserName><DSNName>RMXR7_MOBILE_FEB16</DSNName></UserInfo></Authorization></Document></Message>"));

              //  txtResponse.Text = FormatXmlText(objRMA131Common.ProcessRequest(txtRequest.Text));
              // txtResponse.Text = FormatXmlText(objRMA131CommonLocal.ProcessRequest(txtRequest.Text));

              //  txtResponse.Text = FormatXmlText(objRMA131UploadLocal.UploadRequest(txtRequest.Text));
            //   txtResponse.Text = FormatXmlText(objRMA131UploadLocal.ProcessRequest(txtRequest.Text));
                // txtResponse.Text = FormatXmlText(objRMA131Upload.ProcessRequest(txtRequest.Text));
             //   // txtResponse.Text = FormatXmlText(objRMA131Upload.UploadRequest(txtRequest.Text));
              //  txtResponse.Text = FormatXmlText(objProgressNotes.LoadProgressNotesPartialForMobileAdj(txtRequest.Text));
              //   txtResponse.Text = FormatXmlText(objProgressNotes.SaveNotesForMobileAdj(txtRequest.Text));
               //  txtResponse.Text = FormatXmlText(objCodesService.GetCodesForMobileAdj(txtRequest.Text));

               //  txtResponse.Text = FormatXmlText(objCommonDMZ.ProcessRequest(txtRequest.Text));

               //  txtResponse.Text = FormatXmlText(objMobo.ProcessRequest(txtRequest.Text));

                 //txtResponse.Text = FormatXmlText(objBobAuthentication.AuthenticateMobileUser(txtRequest.Text));    //login Service
                 //txtResponse.Text = FormatXmlText(objBobCommonWCF.ProcessRequest(txtRequest.Text));                 //CommonWCF Service
                 //txtResponse.Text = FormatXmlText(objBobCodes.GetCodesForMobileAdj(txtRequest.Text));                 //Codes Service

                //---------------------------------START-------------------------------------------------------

                 txtResponse.Text = FormatXmlText(objKTMoboCommon.ProcessRequest(txtRequest.Text));
                //----------------------------------END-----------------------------------------------------------
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.Message;
            }
        }

        private string FormatXmlText(string sInput)
        {
            XmlDocument oHtmlDoc;
            XmlTextWriter xmlWriter;
            StringWriter textWriter;

            if (string.IsNullOrEmpty(sInput.Trim()))
                return sInput;

            textWriter = new StringWriter();
            xmlWriter = new XmlTextWriter(textWriter);
            xmlWriter.Formatting = Formatting.Indented;
            oHtmlDoc = new XmlDocument();
            oHtmlDoc.LoadXml(sInput);
            oHtmlDoc.Save(xmlWriter);

            //remove encoding
            string sFormatedXml = textWriter.ToString();
            Regex regEncoding = new Regex(@"<\?xml\s+version=[\w\W]+encoding=[\w\W]+\?>");
            if (regEncoding.IsMatch(sFormatedXml))
            {
                sFormatedXml = regEncoding.Replace(sFormatedXml, "<?xml version=\"1.0\" ?>");
            }

            

            return sFormatedXml;
        }
    }
}
