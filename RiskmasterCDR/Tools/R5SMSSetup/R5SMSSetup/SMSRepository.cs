﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Tools.ChecksumManager;

namespace R5SMSSetup
{
    internal class SMSRepository
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserID"></param>
        /// <param name="strLastName"></param>
        /// <param name="strFirstName"></param>
        /// <param name="strUserChecksum"></param>
        /// <returns></returns>
        internal static string InsertUser(string strUserID, string strLastName, 
            string strFirstName, string strUserChecksum)
        {

            var USER_TABLE_FIELDS = new
            {
                USER_ID = "USER_ID",
                LAST_NAME = "LAST_NAME",
                FIRST_NAME = "FIRST_NAME",
                MANAGER_ID = "MANAGER_ID",
                NLS_CODE = "NLS_CODE",
                CHECKSUM = "CHECKSUM",
                LAST_UPDATED = "DTTM_RCD_LAST_UPD",
                IS_SMS_USER = "IS_SMS_USER"
            };
            
            
            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO USER_TABLE (USER_ID , LAST_NAME , FIRST_NAME , MANAGER_ID , NLS_CODE ,CHECKSUM , DTTM_RCD_LAST_UPD,IS_SMS_USER) ");
            sInsertQuery.Append("VALUES( ");
            sInsertQuery.Append(strUserID);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strLastName);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(strFirstName);
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(CalculateChecksums.MANAGER_ID.ToString());
            sInsertQuery.Append(" , ");
            sInsertQuery.Append(CalculateChecksums.LANGUAGE_CODE.ToString());
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strUserChecksum);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            sInsertQuery.Append("' , 1)");

            return sInsertQuery.ToString();
        } // method: InsertUser

        internal static string UpdateUser(string strUserID, string strLastName,
            string strFirstName, string strUserChecksum)
        {
            var USER_TABLE_FIELDS = new
            {
                USER_ID = "USER_ID",
                LAST_NAME = "LAST_NAME",
                FIRST_NAME = "FIRST_NAME",
                MANAGER_ID = "MANAGER_ID",
                NLS_CODE = "NLS_CODE",
                CHECKSUM = "CHECKSUM",
                LAST_UPDATED = "DTTM_RCD_LAST_UPD",
                IS_SMS_USER = "IS_SMS_USER",
                SMS_ADMIN = 1
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();

            strBldrUpdateQuery.Append("UPDATE USER_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.LAST_NAME, strLastName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.FIRST_NAME, strFirstName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.CHECKSUM, strUserChecksum));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("{0}={1} ", USER_TABLE_FIELDS.IS_SMS_USER, USER_TABLE_FIELDS.SMS_ADMIN));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1}", USER_TABLE_FIELDS.USER_ID, strUserID));



            return strBldrUpdateQuery.ToString();
        }//method: UpdateUser

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSN"></param>
        /// <param name="strDSNID"></param>
        /// <param name="strDbUserID"></param>
        /// <param name="strDBUserPwd"></param>
        /// <param name="strDbType"></param>
        /// <param name="strDbConnString"></param>
        /// <param name="strChecksum"></param>
        /// <param name="strCRC"></param>
        /// <returns></returns>
        internal static string InsertDSN(string strDSN, string strDSNID, string strDbUserID,
            string strDBUserPwd, string strDbType, string strDbConnString, string strChecksum, string strCRC)
        {
            const int DEFAULT_NUM_LICENSES = 1;

            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO DATA_SOURCE_TABLE (");
            sInsertQuery.Append("ORGSEC_FLAG, DSN, DSNID, STATUS, RM_USERID, RM_PASSWORD, DBTYPE,");
            sInsertQuery.Append("CHECKSUM, GLOBAL_DOC_PATH, CONNECTION_STRING,NUM_LICENSES,");
            sInsertQuery.Append("LIC_UPD_DATE,CRC2,DOC_PATH_TYPE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD)");
            sInsertQuery.Append(" VALUES (0 , '");
            sInsertQuery.Append(strDSN);
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(strDSNID);
            sInsertQuery.Append(" , 0 ,'");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strDbUserID));
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strDBUserPwd));
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(strDbType);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strChecksum);
            sInsertQuery.Append("' , NULL ,'");
            sInsertQuery.Append(strDbConnString);
            sInsertQuery.Append(string.Format("',{0},", DEFAULT_NUM_LICENSES));
            sInsertQuery.Append(string.Format("'{0}',", CalculateChecksums.ToDbDate(DateTime.Now)));
            sInsertQuery.Append("'");
            sInsertQuery.Append(strCRC);
            sInsertQuery.Append("' , 0 , NULL ,'");
            sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            sInsertQuery.Append("')");

            return sInsertQuery.ToString();
        } // method: InsertDSN

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSN"></param>
        /// <param name="strDSNID"></param>
        /// <param name="strDbUserID"></param>
        /// <param name="strDBUserPwd"></param>
        /// <param name="strDbType"></param>
        /// <param name="strDbConnString"></param>
        /// <param name="strChecksum"></param>
        /// <param name="strCRC"></param>
        /// <returns></returns>
        internal static string UpdateDSN(string strDSN, string strDSNID, string strDbUserID,
            string strDBUserPwd, string strDbType, string strDbConnString, string strChecksum, string strCRC)
        {
            var DATA_SOURCE_TABLE_FIELDS = new
            {
                ORGSEC_FLAG = "ORGSEC_FLAG",
                DSN = "DSN",
                DSNID = "DSNID",
                RM_USERID = "RM_USERID",
                RM_PASSWORD = "RM_PASSWORD",
                DBTYPE = "DBTYPE",
                CHECKSUM = "CHECKSUM",
                CONNECTION_STRING = "CONNECTION_STRING",
                CRC2 = "CRC2",
                UPDATED_BY_USER="UPDATED_BY_USER",
                LAST_UPDATED="DTTM_RCD_LAST_UPD",
                GLOBAL_DOC_PATH = "GLOBAL_DOC_PATH",
                NUM_LICENSES = "NUM_LICENSES",
                LIC_UPD_DATE = "LIC_UPD_DATE",
                STATUS = "STATUS",
                DOC_PATH_TYPE="DOC_PATH_TYPE"
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();
            strBldrUpdateQuery.Append("UPDATE DATA_SOURCE_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.DSN, strDSN));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.RM_USERID, CalculateChecksums.GetEncryptedString(strDbUserID)));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.RM_PASSWORD, CalculateChecksums.GetEncryptedString(strDBUserPwd)));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.DBTYPE, strDbType));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.CHECKSUM, strChecksum));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.CONNECTION_STRING, strDbConnString));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.CRC2, strCRC));

            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.GLOBAL_DOC_PATH, "NULL"));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.NUM_LICENSES, 1));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.LIC_UPD_DATE, CalculateChecksums.ToDbDate(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.STATUS, 0));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.DOC_PATH_TYPE, 0));

            strBldrUpdateQuery.Append(string.Format("{0}='{1}' ", DATA_SOURCE_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1}", DATA_SOURCE_TABLE_FIELDS.DSNID, strDSNID));

            return strBldrUpdateQuery.ToString();
        } // method: UpdateDSN


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSNID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLoginName"></param>
        /// <param name="strLoginPwd"></param>
        /// <param name="strChecksum"></param>
        /// <returns></returns>
        internal static string InsertUserDetails(string strDSNID, string strUserID, string strLoginName,
            string strLoginPwd, string strChecksum)
        {
            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO USER_DETAILS_TABLE ");
            sInsertQuery.Append("(DSNID, USER_ID, LOGIN_NAME, PASSWORD, PRIVS_EXPIRE, PASSWD_EXPIRE,");
            sInsertQuery.Append("DOC_PATH, SUN_START, SUN_END, MON_START, MON_END, ");
            sInsertQuery.Append("TUES_START, TUES_END, WEDS_START, WEDS_END, THURS_START, THURS_END,");
            sInsertQuery.Append("FRI_START, FRI_END, SAT_START, SAT_END, CHECKSUM,");
            sInsertQuery.Append("UPDATED_BY_USER, DTTM_RCD_LAST_UPD, FORCE_CHANGE_PWD)");
            sInsertQuery.Append(" VALUES (");
            sInsertQuery.Append(strDSNID);
            sInsertQuery.Append(" , ");
            sInsertQuery.Append(strUserID);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strLoginName);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strLoginPwd));
            sInsertQuery.Append("' , null , null , null , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '");
            sInsertQuery.Append(strChecksum);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(strLoginName);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            sInsertQuery.Append("', NULL)");

            return sInsertQuery.ToString();
        } // method: InsertUserDetails

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSNID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLoginName"></param>
        /// <param name="strLoginPwd"></param>
        /// <param name="strChecksum"></param>
        /// <returns></returns>
        internal static string UpdateUserDetails(string strDSNID, string strUserID, string strLoginName,
            string strLoginPwd, string strChecksum)
        {
            var USER_DETAILS_TABLE_FIELDS = new
            {
                DSNID = "DSNID",
                USER_ID = "USER_ID",
                LOGIN_NAME = "LOGIN_NAME",
                PASSWORD = "PASSWORD",
                CHECKSUM = "CHECKSUM",
                UPDATED_BY_USER="UPDATED_BY_USER",
                LAST_UPDATED="DTTM_RCD_LAST_UPD"
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();
            strBldrUpdateQuery.Append("UPDATE USER_DETAILS_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.LOGIN_NAME, strLoginName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.PASSWORD, CalculateChecksums.GetEncryptedString(strLoginPwd)));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.CHECKSUM, strChecksum));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.UPDATED_BY_USER, strLoginName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}' ", USER_DETAILS_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1} ", USER_DETAILS_TABLE_FIELDS.DSNID, strDSNID));
            strBldrUpdateQuery.Append(string.Format("AND {0}={1}", USER_DETAILS_TABLE_FIELDS.USER_ID, strUserID));


            return strBldrUpdateQuery.ToString();
        }//method: UpdateUserDetails

        //rsolanki2: adding functions for Rmworld db
        #region block for functions for Rmworld db

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserID"></param>
        /// <param name="strLastName"></param>
        /// <param name="strFirstName"></param>
        /// <param name="strUserChecksum"></param>
        /// <returns></returns>
        internal static string InsertUserLegacy(string strUserID, string strLastName,
            string strFirstName, string strUserChecksum)
        {

            var USER_TABLE_FIELDS = new
            {
                USER_ID = "USER_ID",
                LAST_NAME = "LAST_NAME",
                FIRST_NAME = "FIRST_NAME",
                MANAGER_ID = "MANAGER_ID",
                NLS_CODE = "NLS_CODE",
                CHECKSUM = "CHECKSUM"
                //,
                //LAST_UPDATED = "DTTM_RCD_LAST_UPD",
                //IS_SMS_USER = "IS_SMS_USER"
            };


            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO USER_TABLE (USER_ID , LAST_NAME , FIRST_NAME , MANAGER_ID , NLS_CODE ,CHECKSUM ) ");
            sInsertQuery.Append("VALUES( ");
            sInsertQuery.Append(strUserID);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strLastName);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(strFirstName);
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(CalculateChecksums.MANAGER_ID.ToString());
            sInsertQuery.Append(" , ");
            sInsertQuery.Append(CalculateChecksums.LANGUAGE_CODE.ToString());
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strUserChecksum);
            sInsertQuery.Append("' ) ");
            //sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            //sInsertQuery.Append("' , 1)");

            return sInsertQuery.ToString();
        } 

        internal static string UpdateUserLegacy(string strUserID, string strLastName,
            string strFirstName, string strUserChecksum)
        {
            var USER_TABLE_FIELDS = new
            {
                USER_ID = "USER_ID",
                LAST_NAME = "LAST_NAME",
                FIRST_NAME = "FIRST_NAME",
                MANAGER_ID = "MANAGER_ID",
                NLS_CODE = "NLS_CODE",
                CHECKSUM = "CHECKSUM"
                //,
                //LAST_UPDATED = "DTTM_RCD_LAST_UPD",
                //IS_SMS_USER = "IS_SMS_USER",
                //SMS_ADMIN = 1
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();

            strBldrUpdateQuery.Append("UPDATE USER_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.LAST_NAME, strLastName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.FIRST_NAME, strFirstName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}'", USER_TABLE_FIELDS.CHECKSUM, strUserChecksum));
            //strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            //strBldrUpdateQuery.Append(string.Format("{0}={1} ", USER_TABLE_FIELDS.IS_SMS_USER, USER_TABLE_FIELDS.SMS_ADMIN));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1}", USER_TABLE_FIELDS.USER_ID, strUserID));



            return strBldrUpdateQuery.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSN"></param>
        /// <param name="strDSNID"></param>
        /// <param name="strDbUserID"></param>
        /// <param name="strDBUserPwd"></param>
        /// <param name="strDbType"></param>
        /// <param name="strDbConnString"></param>
        /// <param name="strChecksum"></param>
        /// <param name="strCRC"></param>
        /// <returns></returns>
        internal static string InsertDSNLegacy(string strDSN, string strDSNID, string strDbUserID,
            string strDBUserPwd, string strDbType, string strDbConnString, string strChecksum, string strCRC)
        {
            const int DEFAULT_NUM_LICENSES = 1;

            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO DATA_SOURCE_TABLE (");
            sInsertQuery.Append("ORGSEC_FLAG, DSN, DSNID, STATUS, RM_USERID, RM_PASSWORD, DBTYPE,");
            sInsertQuery.Append("CHECKSUM, GLOBAL_DOC_PATH, CONNECTION_STRING,NUM_LICENSES,");
            sInsertQuery.Append("LIC_UPD_DATE,CRC2,DOC_PATH_TYPE)");
            sInsertQuery.Append(" VALUES (0 , '");
            sInsertQuery.Append(strDSN);
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(strDSNID);
            sInsertQuery.Append(" , 0 ,'");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strDbUserID));
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strDBUserPwd));
            sInsertQuery.Append("' , ");
            sInsertQuery.Append(strDbType);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strChecksum);
            sInsertQuery.Append("' , NULL ,'");
            sInsertQuery.Append(strDbConnString);
            sInsertQuery.Append(string.Format("',{0},", DEFAULT_NUM_LICENSES));
            sInsertQuery.Append(string.Format("'{0}',", CalculateChecksums.ToDbDate(DateTime.Now)));
            sInsertQuery.Append("'");
            sInsertQuery.Append(strCRC);
            sInsertQuery.Append("' , 0 )");
            //sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            //sInsertQuery.Append("')");

            return sInsertQuery.ToString();
        } // method: InsertDSN

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSN"></param>
        /// <param name="strDSNID"></param>
        /// <param name="strDbUserID"></param>
        /// <param name="strDBUserPwd"></param>
        /// <param name="strDbType"></param>
        /// <param name="strDbConnString"></param>
        /// <param name="strChecksum"></param>
        /// <param name="strCRC"></param>
        /// <returns></returns>
        internal static string UpdateDSNLegacy(string strDSN, string strDSNID, string strDbUserID,
            string strDBUserPwd, string strDbType, string strDbConnString, string strChecksum, string strCRC)
        {
            var DATA_SOURCE_TABLE_FIELDS = new
            {
                ORGSEC_FLAG = "ORGSEC_FLAG",
                DSN = "DSN",
                DSNID = "DSNID",
                RM_USERID = "RM_USERID",
                RM_PASSWORD = "RM_PASSWORD",
                DBTYPE = "DBTYPE",
                CHECKSUM = "CHECKSUM",
                CONNECTION_STRING = "CONNECTION_STRING",
                CRC2 = "CRC2",
                GLOBAL_DOC_PATH = "GLOBAL_DOC_PATH",
                NUM_LICENSES = "NUM_LICENSES",
                LIC_UPD_DATE = "LIC_UPD_DATE",
                STATUS = "STATUS",
                DOC_PATH_TYPE = "DOC_PATH_TYPE"

                //,
                //UPDATED_BY_USER = "UPDATED_BY_USER",
                //LAST_UPDATED = "DTTM_RCD_LAST_UPD"
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();
            strBldrUpdateQuery.Append("UPDATE DATA_SOURCE_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.DSN, strDSN));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.RM_USERID, CalculateChecksums.GetEncryptedString(strDbUserID)));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.RM_PASSWORD, CalculateChecksums.GetEncryptedString(strDBUserPwd)));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.DBTYPE, strDbType));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.CHECKSUM, strChecksum));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.CONNECTION_STRING, strDbConnString));

            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.GLOBAL_DOC_PATH, "NULL"));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.NUM_LICENSES, 1));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", DATA_SOURCE_TABLE_FIELDS.LIC_UPD_DATE, CalculateChecksums.ToDbDate(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.STATUS, 0));
            strBldrUpdateQuery.Append(string.Format("{0}={1},", DATA_SOURCE_TABLE_FIELDS.DOC_PATH_TYPE, 0));

            strBldrUpdateQuery.Append(string.Format("{0}='{1}'", DATA_SOURCE_TABLE_FIELDS.CRC2, strCRC));
            //strBldrUpdateQuery.Append(string.Format("{0}='{1}' ", DATA_SOURCE_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1}", DATA_SOURCE_TABLE_FIELDS.DSNID, strDSNID));

            return strBldrUpdateQuery.ToString();
        } // method: UpdateDSN


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSNID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLoginName"></param>
        /// <param name="strLoginPwd"></param>
        /// <param name="strChecksum"></param>
        /// <returns></returns>
        internal static string InsertUserDetailsLegacy(string strDSNID, string strUserID, string strLoginName,
            string strLoginPwd, string strChecksum)
        {
            StringBuilder sInsertQuery = new StringBuilder();
            sInsertQuery.Append("INSERT INTO USER_DETAILS_TABLE ");
            sInsertQuery.Append("(DSNID, USER_ID, LOGIN_NAME, PASSWORD, PRIVS_EXPIRE, PASSWD_EXPIRE,");
            sInsertQuery.Append("DOC_PATH, SUN_START, SUN_END, MON_START, MON_END, ");
            sInsertQuery.Append("TUES_START, TUES_END, WEDS_START, WEDS_END, THURS_START, THURS_END,");
            sInsertQuery.Append("FRI_START, FRI_END, SAT_START, SAT_END, CHECKSUM)");
            //sInsertQuery.Append("UPDATED_BY_USER, DTTM_RCD_LAST_UPD, FORCE_CHANGE_PWD)");
            sInsertQuery.Append(" VALUES (");
            sInsertQuery.Append(strDSNID);
            sInsertQuery.Append(" , ");
            sInsertQuery.Append(strUserID);
            sInsertQuery.Append(" , '");
            sInsertQuery.Append(strLoginName);
            sInsertQuery.Append("' , '");
            sInsertQuery.Append(CalculateChecksums.GetEncryptedString(strLoginPwd));
            sInsertQuery.Append("' , null , null , null , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '");
            sInsertQuery.Append(strChecksum);
            sInsertQuery.Append("')");
            //sInsertQuery.Append(strLoginName);
            //sInsertQuery.Append("' , '");
            //sInsertQuery.Append(CalculateChecksums.ToDbDateTime(DateTime.Now));
            //sInsertQuery.Append("', NULL)");

            return sInsertQuery.ToString();
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDSNID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLoginName"></param>
        /// <param name="strLoginPwd"></param>
        /// <param name="strChecksum"></param>
        /// <returns></returns>
        internal static string UpdateUserDetailsLegacy(string strDSNID, string strUserID, string strLoginName,
            string strLoginPwd, string strChecksum)
        {
            var USER_DETAILS_TABLE_FIELDS = new
            {
                DSNID = "DSNID",
                USER_ID = "USER_ID",
                LOGIN_NAME = "LOGIN_NAME",
                PASSWORD = "PASSWORD",
                CHECKSUM = "CHECKSUM"
                //,
                //UPDATED_BY_USER = "UPDATED_BY_USER",
                //LAST_UPDATED = "DTTM_RCD_LAST_UPD"
            };

            StringBuilder strBldrUpdateQuery = new StringBuilder();
            strBldrUpdateQuery.Append("UPDATE USER_DETAILS_TABLE ");
            strBldrUpdateQuery.Append("SET ");
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.LOGIN_NAME, strLoginName));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.PASSWORD, CalculateChecksums.GetEncryptedString(strLoginPwd)));
            strBldrUpdateQuery.Append(string.Format("{0}='{1}'", USER_DETAILS_TABLE_FIELDS.CHECKSUM, strChecksum));
            //strBldrUpdateQuery.Append(string.Format("{0}='{1}',", USER_DETAILS_TABLE_FIELDS.UPDATED_BY_USER, strLoginName));
            //strBldrUpdateQuery.Append(string.Format("{0}='{1}' ", USER_DETAILS_TABLE_FIELDS.LAST_UPDATED, CalculateChecksums.ToDbDateTime(DateTime.Now)));
            strBldrUpdateQuery.Append(string.Format("WHERE {0}={1} ", USER_DETAILS_TABLE_FIELDS.DSNID, strDSNID));
            strBldrUpdateQuery.Append(string.Format("AND {0}={1}", USER_DETAILS_TABLE_FIELDS.USER_ID, strUserID));


            return strBldrUpdateQuery.ToString();
        }


        #endregion


    }
}
