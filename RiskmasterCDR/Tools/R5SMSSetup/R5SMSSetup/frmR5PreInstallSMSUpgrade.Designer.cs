﻿namespace R5SMSSetup
{
    partial class frmR5PreInstallSMSUpgrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmR5PreInstallSMSUpgrade));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.lblUDLoginPwd = new System.Windows.Forms.Label();
            this.lblLoginName = new System.Windows.Forms.Label();
            this.lblUDDSNId = new System.Windows.Forms.Label();
            this.lblUDUserID = new System.Windows.Forms.Label();
            this.txtUDPwd = new System.Windows.Forms.TextBox();
            this.txtUDLoginName = new System.Windows.Forms.TextBox();
            this.txtUDDSNId = new System.Windows.Forms.TextBox();
            this.txtUDUserID = new System.Windows.Forms.TextBox();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.cmbODBCDrivers = new System.Windows.Forms.ComboBox();
            this.txtDbName = new System.Windows.Forms.TextBox();
            this.lblDbName = new System.Windows.Forms.Label();
            this.txtDBServer = new System.Windows.Forms.TextBox();
            this.lblDBServerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDSNId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.txtDbPwd = new System.Windows.Forms.TextBox();
            this.txtDbUserId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDBUserID = new System.Windows.Forms.Label();
            this.lblDBPwd = new System.Windows.Forms.Label();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.lblQueryResults = new System.Windows.Forms.Label();
            this.lblCRC2 = new System.Windows.Forms.Label();
            this.lblChecksum = new System.Windows.Forms.Label();
            this.ribbonTabUserConfig = new DevComponents.DotNetBar.RibbonPanel();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtUserId1 = new System.Windows.Forms.TextBox();
            this.ribbonTabUser = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabDSN = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabUserDetails = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabQuery = new DevComponents.DotNetBar.RibbonTabItem();
            this.office2007StartButton1 = new DevComponents.DotNetBar.Office2007StartButton();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer1 = new DevComponents.DotNetBar.GalleryContainer();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.btnGenerateChecksum = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.superTooltip1 = new DevComponents.DotNetBar.SuperTooltip();
            this.lblInsertQuery = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblUpdateQuery = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmdCopyToClipboardInsert = new DevComponents.DotNetBar.ButtonX();
            this.cmdCopyToClipboardUpdate = new DevComponents.DotNetBar.ButtonX();
            this.txtCRC = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCheckSum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonTabUserConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Controls.Add(this.ribbonTabUserConfig);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ribbonTabUser,
            this.ribbonTabDSN,
            this.ribbonTabUserDetails,
            this.ribbonTabQuery});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Office2007ColorTable = DevComponents.DotNetBar.Rendering.eOffice2007ColorScheme.VistaGlass;
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.office2007StartButton1,
            this.btnGenerateChecksum,
            this.qatCustomizeItem1});
            this.ribbonControl1.Size = new System.Drawing.Size(538, 511);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 0;
            this.ribbonControl1.Text = "ribbonSMS";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel3.Controls.Add(this.lblUDLoginPwd);
            this.ribbonPanel3.Controls.Add(this.lblLoginName);
            this.ribbonPanel3.Controls.Add(this.lblUDDSNId);
            this.ribbonPanel3.Controls.Add(this.lblUDUserID);
            this.ribbonPanel3.Controls.Add(this.txtUDPwd);
            this.ribbonPanel3.Controls.Add(this.txtUDLoginName);
            this.ribbonPanel3.Controls.Add(this.txtUDDSNId);
            this.ribbonPanel3.Controls.Add(this.txtUDUserID);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(538, 454);
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // lblUDLoginPwd
            // 
            this.lblUDLoginPwd.AutoSize = true;
            this.lblUDLoginPwd.BackColor = System.Drawing.Color.Transparent;
            this.lblUDLoginPwd.Location = new System.Drawing.Point(30, 135);
            this.lblUDLoginPwd.Name = "lblUDLoginPwd";
            this.lblUDLoginPwd.Size = new System.Drawing.Size(110, 13);
            this.lblUDLoginPwd.TabIndex = 26;
            this.lblUDLoginPwd.Text = "Enter Login Password";
            // 
            // lblLoginName
            // 
            this.lblLoginName.AutoSize = true;
            this.lblLoginName.BackColor = System.Drawing.Color.Transparent;
            this.lblLoginName.Location = new System.Drawing.Point(30, 100);
            this.lblLoginName.Name = "lblLoginName";
            this.lblLoginName.Size = new System.Drawing.Size(92, 13);
            this.lblLoginName.TabIndex = 24;
            this.lblLoginName.Text = "Enter Login Name";
            // 
            // lblUDDSNId
            // 
            this.lblUDDSNId.AutoSize = true;
            this.lblUDDSNId.BackColor = System.Drawing.Color.Transparent;
            this.lblUDDSNId.Location = new System.Drawing.Point(30, 62);
            this.lblUDDSNId.Name = "lblUDDSNId";
            this.lblUDDSNId.Size = new System.Drawing.Size(72, 13);
            this.lblUDDSNId.TabIndex = 22;
            this.lblUDDSNId.Text = "Enter DSN ID";
            // 
            // lblUDUserID
            // 
            this.lblUDUserID.AutoSize = true;
            this.lblUDUserID.BackColor = System.Drawing.Color.Transparent;
            this.lblUDUserID.Location = new System.Drawing.Point(30, 27);
            this.lblUDUserID.Name = "lblUDUserID";
            this.lblUDUserID.Size = new System.Drawing.Size(71, 13);
            this.lblUDUserID.TabIndex = 20;
            this.lblUDUserID.Text = "Enter User ID";
            // 
            // txtUDPwd
            // 
            this.txtUDPwd.Location = new System.Drawing.Point(170, 132);
            this.txtUDPwd.Name = "txtUDPwd";
            this.txtUDPwd.Size = new System.Drawing.Size(196, 20);
            this.superTooltip1.SetSuperTooltip(this.txtUDPwd, new DevComponents.DotNetBar.SuperTooltipInfo("Enter User Login Password", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtUDPwd.TabIndex = 27;
            this.txtUDPwd.UseSystemPasswordChar = true;
            // 
            // txtUDLoginName
            // 
            this.txtUDLoginName.Location = new System.Drawing.Point(170, 97);
            this.txtUDLoginName.Name = "txtUDLoginName";
            this.txtUDLoginName.Size = new System.Drawing.Size(196, 20);
            this.superTooltip1.SetSuperTooltip(this.txtUDLoginName, new DevComponents.DotNetBar.SuperTooltipInfo("Enter User Login Name", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtUDLoginName.TabIndex = 25;
            // 
            // txtUDDSNId
            // 
            this.txtUDDSNId.Location = new System.Drawing.Point(170, 57);
            this.txtUDDSNId.Name = "txtUDDSNId";
            this.txtUDDSNId.Size = new System.Drawing.Size(196, 20);
            this.superTooltip1.SetSuperTooltip(this.txtUDDSNId, new DevComponents.DotNetBar.SuperTooltipInfo("Enter new or existing Data Source ID", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtUDDSNId.TabIndex = 23;
            this.txtUDDSNId.Text = "1";
            // 
            // txtUDUserID
            // 
            this.txtUDUserID.Location = new System.Drawing.Point(170, 26);
            this.txtUDUserID.Name = "txtUDUserID";
            this.txtUDUserID.Size = new System.Drawing.Size(196, 20);
            this.superTooltip1.SetSuperTooltip(this.txtUDUserID, new DevComponents.DotNetBar.SuperTooltipInfo("Enter new or existing User ID", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtUDUserID.TabIndex = 21;
            this.txtUDUserID.Text = "1";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel2.Controls.Add(this.cmbODBCDrivers);
            this.ribbonPanel2.Controls.Add(this.txtDbName);
            this.ribbonPanel2.Controls.Add(this.lblDbName);
            this.ribbonPanel2.Controls.Add(this.txtDBServer);
            this.ribbonPanel2.Controls.Add(this.lblDBServerName);
            this.ribbonPanel2.Controls.Add(this.label3);
            this.ribbonPanel2.Controls.Add(this.txtDSNId);
            this.ribbonPanel2.Controls.Add(this.label4);
            this.ribbonPanel2.Controls.Add(this.txtDSN);
            this.ribbonPanel2.Controls.Add(this.txtDbPwd);
            this.ribbonPanel2.Controls.Add(this.txtDbUserId);
            this.ribbonPanel2.Controls.Add(this.label5);
            this.ribbonPanel2.Controls.Add(this.lblDBUserID);
            this.ribbonPanel2.Controls.Add(this.lblDBPwd);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(538, 454);
            this.ribbonPanel2.TabIndex = 2;
            this.ribbonPanel2.Visible = false;
            // 
            // cmbODBCDrivers
            // 
            this.cmbODBCDrivers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbODBCDrivers.FormattingEnabled = true;
            this.cmbODBCDrivers.Location = new System.Drawing.Point(147, 20);
            this.cmbODBCDrivers.Name = "cmbODBCDrivers";
            this.cmbODBCDrivers.Size = new System.Drawing.Size(268, 21);
            this.superTooltip1.SetSuperTooltip(this.cmbODBCDrivers, new DevComponents.DotNetBar.SuperTooltipInfo("Select Database Driver Type", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.cmbODBCDrivers.TabIndex = 7;
            this.cmbODBCDrivers.SelectedIndexChanged += new System.EventHandler(this.cmbODBCDrivers_SelectedIndexChanged);
            // 
            // txtDbName
            // 
            this.txtDbName.Location = new System.Drawing.Point(147, 126);
            this.txtDbName.Name = "txtDbName";
            this.txtDbName.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDbName, new DevComponents.DotNetBar.SuperTooltipInfo("Enter the name of the database or schema", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDbName.TabIndex = 15;
            // 
            // lblDbName
            // 
            this.lblDbName.AutoSize = true;
            this.lblDbName.BackColor = System.Drawing.Color.Transparent;
            this.lblDbName.Location = new System.Drawing.Point(30, 130);
            this.lblDbName.Name = "lblDbName";
            this.lblDbName.Size = new System.Drawing.Size(52, 13);
            this.lblDbName.TabIndex = 14;
            this.lblDbName.Text = "Db Name";
            // 
            // txtDBServer
            // 
            this.txtDBServer.Location = new System.Drawing.Point(147, 102);
            this.txtDBServer.Name = "txtDBServer";
            this.txtDBServer.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDBServer, new DevComponents.DotNetBar.SuperTooltipInfo("Enter Database Server Name/Database Server IP Address or Oracle TNS Name", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDBServer.TabIndex = 13;
            // 
            // lblDBServerName
            // 
            this.lblDBServerName.AutoSize = true;
            this.lblDBServerName.BackColor = System.Drawing.Color.Transparent;
            this.lblDBServerName.Location = new System.Drawing.Point(30, 104);
            this.lblDBServerName.Name = "lblDBServerName";
            this.lblDBServerName.Size = new System.Drawing.Size(101, 13);
            this.lblDBServerName.TabIndex = 12;
            this.lblDBServerName.Text = "Db Server Name/IP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(30, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Database Driver Type";
            // 
            // txtDSNId
            // 
            this.txtDSNId.Location = new System.Drawing.Point(147, 51);
            this.txtDSNId.Name = "txtDSNId";
            this.txtDSNId.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDSNId, new DevComponents.DotNetBar.SuperTooltipInfo("Enter new or existing Data Source ID", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDSNId.TabIndex = 9;
            this.txtDSNId.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(30, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Enter DSN Id";
            // 
            // txtDSN
            // 
            this.txtDSN.Location = new System.Drawing.Point(147, 76);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDSN, new DevComponents.DotNetBar.SuperTooltipInfo("Enter Desired DSN Name", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDSN.TabIndex = 11;
            // 
            // txtDbPwd
            // 
            this.txtDbPwd.Location = new System.Drawing.Point(147, 182);
            this.txtDbPwd.Name = "txtDbPwd";
            this.txtDbPwd.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDbPwd, new DevComponents.DotNetBar.SuperTooltipInfo("Entere database User Password", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDbPwd.TabIndex = 19;
            this.txtDbPwd.UseSystemPasswordChar = true;
            // 
            // txtDbUserId
            // 
            this.txtDbUserId.Location = new System.Drawing.Point(147, 152);
            this.txtDbUserId.Name = "txtDbUserId";
            this.txtDbUserId.Size = new System.Drawing.Size(268, 20);
            this.superTooltip1.SetSuperTooltip(this.txtDbUserId, new DevComponents.DotNetBar.SuperTooltipInfo("Enter database User ID", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtDbUserId.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(30, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Enter DSN Name";
            // 
            // lblDBUserID
            // 
            this.lblDBUserID.AutoSize = true;
            this.lblDBUserID.BackColor = System.Drawing.Color.Transparent;
            this.lblDBUserID.Location = new System.Drawing.Point(30, 159);
            this.lblDBUserID.Name = "lblDBUserID";
            this.lblDBUserID.Size = new System.Drawing.Size(86, 13);
            this.lblDBUserID.TabIndex = 16;
            this.lblDBUserID.Text = "Enter Db User Id";
            // 
            // lblDBPwd
            // 
            this.lblDBPwd.AutoSize = true;
            this.lblDBPwd.BackColor = System.Drawing.Color.Transparent;
            this.lblDBPwd.Location = new System.Drawing.Point(30, 189);
            this.lblDBPwd.Name = "lblDBPwd";
            this.lblDBPwd.Size = new System.Drawing.Size(98, 13);
            this.lblDBPwd.TabIndex = 18;
            this.lblDBPwd.Text = "Enter Db Password";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel4.Controls.Add(this.txtCheckSum);
            this.ribbonPanel4.Controls.Add(this.txtCRC);
            this.ribbonPanel4.Controls.Add(this.cmdCopyToClipboardUpdate);
            this.ribbonPanel4.Controls.Add(this.cmdCopyToClipboardInsert);
            this.ribbonPanel4.Controls.Add(this.lblUpdateQuery);
            this.ribbonPanel4.Controls.Add(this.lblInsertQuery);
            this.ribbonPanel4.Controls.Add(this.lblQueryResults);
            this.ribbonPanel4.Controls.Add(this.lblCRC2);
            this.ribbonPanel4.Controls.Add(this.lblChecksum);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(538, 454);
            this.ribbonPanel4.TabIndex = 4;
            // 
            // lblQueryResults
            // 
            this.lblQueryResults.AutoSize = true;
            this.lblQueryResults.BackColor = System.Drawing.Color.Transparent;
            this.lblQueryResults.Location = new System.Drawing.Point(14, 0);
            this.lblQueryResults.Name = "lblQueryResults";
            this.lblQueryResults.Size = new System.Drawing.Size(73, 13);
            this.lblQueryResults.TabIndex = 32;
            this.lblQueryResults.Text = "Query Results";
            // 
            // lblCRC2
            // 
            this.lblCRC2.AutoSize = true;
            this.lblCRC2.BackColor = System.Drawing.Color.Transparent;
            this.lblCRC2.Location = new System.Drawing.Point(14, 331);
            this.lblCRC2.Name = "lblCRC2";
            this.lblCRC2.Size = new System.Drawing.Size(35, 13);
            this.lblCRC2.TabIndex = 30;
            this.lblCRC2.Text = "CRC2";
            // 
            // lblChecksum
            // 
            this.lblChecksum.AutoSize = true;
            this.lblChecksum.BackColor = System.Drawing.Color.Transparent;
            this.lblChecksum.Location = new System.Drawing.Point(14, 370);
            this.lblChecksum.Name = "lblChecksum";
            this.lblChecksum.Size = new System.Drawing.Size(57, 13);
            this.lblChecksum.TabIndex = 28;
            this.lblChecksum.Text = "Checksum";
            // 
            // ribbonTabUserConfig
            // 
            this.ribbonTabUserConfig.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonTabUserConfig.Controls.Add(this.lblFirstName);
            this.ribbonTabUserConfig.Controls.Add(this.lblLastName);
            this.ribbonTabUserConfig.Controls.Add(this.lblUserId);
            this.ribbonTabUserConfig.Controls.Add(this.txtFirstName);
            this.ribbonTabUserConfig.Controls.Add(this.txtLastName);
            this.ribbonTabUserConfig.Controls.Add(this.txtUserId1);
            this.ribbonTabUserConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonTabUserConfig.Location = new System.Drawing.Point(0, 55);
            this.ribbonTabUserConfig.Name = "ribbonTabUserConfig";
            this.ribbonTabUserConfig.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonTabUserConfig.Size = new System.Drawing.Size(538, 454);
            this.ribbonTabUserConfig.TabIndex = 5;
            this.ribbonTabUserConfig.Visible = false;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.BackColor = System.Drawing.Color.Transparent;
            this.lblFirstName.Location = new System.Drawing.Point(29, 59);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(85, 13);
            this.lblFirstName.TabIndex = 2;
            this.lblFirstName.Text = "Enter First Name";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.BackColor = System.Drawing.Color.Transparent;
            this.lblLastName.Location = new System.Drawing.Point(28, 90);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(86, 13);
            this.lblLastName.TabIndex = 4;
            this.lblLastName.Text = "Enter Last Name";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.BackColor = System.Drawing.Color.Transparent;
            this.lblUserId.Location = new System.Drawing.Point(30, 27);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(71, 13);
            this.lblUserId.TabIndex = 0;
            this.lblUserId.Text = "Enter User ID";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(135, 52);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(207, 20);
            this.superTooltip1.SetSuperTooltip(this.txtFirstName, new DevComponents.DotNetBar.SuperTooltipInfo("Enter User First Name", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtFirstName.TabIndex = 3;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(135, 83);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(207, 20);
            this.superTooltip1.SetSuperTooltip(this.txtLastName, new DevComponents.DotNetBar.SuperTooltipInfo("Enter User Last Name", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtLastName.TabIndex = 5;
            // 
            // txtUserId1
            // 
            this.txtUserId1.Location = new System.Drawing.Point(135, 20);
            this.txtUserId1.Name = "txtUserId1";
            this.txtUserId1.Size = new System.Drawing.Size(207, 20);
            this.superTooltip1.SetSuperTooltip(this.txtUserId1, new DevComponents.DotNetBar.SuperTooltipInfo("Enter new or existing User ID", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.txtUserId1.TabIndex = 1;
            this.txtUserId1.Text = "1";
            // 
            // ribbonTabUser
            // 
            this.ribbonTabUser.Name = "ribbonTabUser";
            this.ribbonTabUser.Panel = this.ribbonTabUserConfig;
            this.superTooltip1.SetSuperTooltip(this.ribbonTabUser, new DevComponents.DotNetBar.SuperTooltipInfo("Create Users for Security Management System", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.ribbonTabUser.Text = "User Configuration";
            // 
            // ribbonTabDSN
            // 
            this.ribbonTabDSN.Name = "ribbonTabDSN";
            this.ribbonTabDSN.Panel = this.ribbonPanel2;
            this.superTooltip1.SetSuperTooltip(this.ribbonTabDSN, new DevComponents.DotNetBar.SuperTooltipInfo("Create Data Sources for Security Management System", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.ribbonTabDSN.Text = "DSN Configuration";
            // 
            // ribbonTabUserDetails
            // 
            this.ribbonTabUserDetails.Name = "ribbonTabUserDetails";
            this.ribbonTabUserDetails.Panel = this.ribbonPanel3;
            this.superTooltip1.SetSuperTooltip(this.ribbonTabUserDetails, new DevComponents.DotNetBar.SuperTooltipInfo("Create User Details for Security Management System", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.ribbonTabUserDetails.Text = "User Details Configuration";
            // 
            // ribbonTabQuery
            // 
            this.ribbonTabQuery.Checked = true;
            this.ribbonTabQuery.Name = "ribbonTabQuery";
            this.ribbonTabQuery.Panel = this.ribbonPanel4;
            this.superTooltip1.SetSuperTooltip(this.ribbonTabQuery, new DevComponents.DotNetBar.SuperTooltipInfo("View Generated Scripts", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.ribbonTabQuery.Text = "Generated Queries";
            // 
            // office2007StartButton1
            // 
            this.office2007StartButton1.AutoExpandOnClick = true;
            this.office2007StartButton1.CanCustomize = false;
            this.office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.office2007StartButton1.Image = ((System.Drawing.Image)(resources.GetObject("office2007StartButton1.Image")));
            this.office2007StartButton1.ImagePaddingHorizontal = 2;
            this.office2007StartButton1.ImagePaddingVertical = 2;
            this.office2007StartButton1.Name = "office2007StartButton1";
            this.office2007StartButton1.ShowSubItems = false;
            this.office2007StartButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.office2007StartButton1.Text = "&File";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer4});
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.galleryContainer1});
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem4});
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Icon = ((System.Drawing.Icon)(resources.GetObject("buttonItem4.Icon")));
            this.buttonItem4.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem4.Image")));
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 24;
            this.superTooltip1.SetSuperTooltip(this.buttonItem4, new DevComponents.DotNetBar.SuperTooltipInfo("Generate Script", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.buttonItem4.Text = "&Generate Script";
            this.buttonItem4.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // galleryContainer1
            // 
            // 
            // 
            // 
            this.galleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer1.EnableGalleryPopup = false;
            this.galleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer1.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer1.MultiLine = false;
            this.galleryContainer1.Name = "galleryContainer1";
            this.galleryContainer1.PopupUsesStandardScrollbars = false;
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem13});
            // 
            // buttonItem13
            // 
            this.buttonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem13.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem13.Image")));
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.SubItemsExpandWidth = 24;
            this.buttonItem13.Text = "E&xit";
            // 
            // btnGenerateChecksum
            // 
            this.btnGenerateChecksum.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGenerateChecksum.Icon")));
            this.btnGenerateChecksum.KeyTips = "GENERATE SCRIPT";
            this.btnGenerateChecksum.Name = "btnGenerateChecksum";
            this.superTooltip1.SetSuperTooltip(this.btnGenerateChecksum, new DevComponents.DotNetBar.SuperTooltipInfo("Generate Script", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.btnGenerateChecksum.Tag = "Generate Script";
            this.btnGenerateChecksum.Text = "&Generate Script";
            this.btnGenerateChecksum.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // superTooltip1
            // 
            this.superTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            // 
            // lblInsertQuery
            // 
            // 
            // 
            // 
            this.lblInsertQuery.Border.Class = "TextBoxBorder";
            this.lblInsertQuery.Location = new System.Drawing.Point(17, 26);
            this.lblInsertQuery.Multiline = true;
            this.lblInsertQuery.Name = "lblInsertQuery";
            this.lblInsertQuery.ReadOnly = true;
            this.lblInsertQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lblInsertQuery.Size = new System.Drawing.Size(407, 131);
            this.superTooltip1.SetSuperTooltip(this.lblInsertQuery, new DevComponents.DotNetBar.SuperTooltipInfo("Insert Query for Security Management System", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.lblInsertQuery.TabIndex = 35;
            // 
            // lblUpdateQuery
            // 
            // 
            // 
            // 
            this.lblUpdateQuery.Border.Class = "TextBoxBorder";
            this.lblUpdateQuery.Location = new System.Drawing.Point(17, 182);
            this.lblUpdateQuery.Multiline = true;
            this.lblUpdateQuery.Name = "lblUpdateQuery";
            this.lblUpdateQuery.ReadOnly = true;
            this.lblUpdateQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lblUpdateQuery.Size = new System.Drawing.Size(407, 131);
            this.superTooltip1.SetSuperTooltip(this.lblUpdateQuery, new DevComponents.DotNetBar.SuperTooltipInfo("Update Query for Security Management System", "", "", null, null, DevComponents.DotNetBar.eTooltipColor.Gray));
            this.lblUpdateQuery.TabIndex = 36;
            // 
            // cmdCopyToClipboardInsert
            // 
            this.cmdCopyToClipboardInsert.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdCopyToClipboardInsert.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.cmdCopyToClipboardInsert.Location = new System.Drawing.Point(430, 27);
            this.cmdCopyToClipboardInsert.Name = "cmdCopyToClipboardInsert";
            this.cmdCopyToClipboardInsert.Size = new System.Drawing.Size(96, 23);
            this.cmdCopyToClipboardInsert.TabIndex = 37;
            this.cmdCopyToClipboardInsert.Text = "Copy to Clipboard";
            this.cmdCopyToClipboardInsert.Click += new System.EventHandler(this.cmdCopyToClipboardInsert_Click);
            // 
            // cmdCopyToClipboardUpdate
            // 
            this.cmdCopyToClipboardUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdCopyToClipboardUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.cmdCopyToClipboardUpdate.Location = new System.Drawing.Point(430, 182);
            this.cmdCopyToClipboardUpdate.Name = "cmdCopyToClipboardUpdate";
            this.cmdCopyToClipboardUpdate.Size = new System.Drawing.Size(96, 23);
            this.cmdCopyToClipboardUpdate.TabIndex = 38;
            this.cmdCopyToClipboardUpdate.Text = "Copy to Clipboard";
            this.cmdCopyToClipboardUpdate.Click += new System.EventHandler(this.cmdCopyToClipboardUpdate_Click);
            // 
            // txtCRC
            // 
            // 
            // 
            // 
            this.txtCRC.Border.Class = "TextBoxBorder";
            this.txtCRC.Location = new System.Drawing.Point(17, 348);
            this.txtCRC.Name = "txtCRC";
            this.txtCRC.ReadOnly = true;
            this.txtCRC.Size = new System.Drawing.Size(407, 20);
            this.txtCRC.TabIndex = 39;
            // 
            // txtCheckSum
            // 
            // 
            // 
            // 
            this.txtCheckSum.Border.Class = "TextBoxBorder";
            this.txtCheckSum.Location = new System.Drawing.Point(17, 386);
            this.txtCheckSum.Name = "txtCheckSum";
            this.txtCheckSum.ReadOnly = true;
            this.txtCheckSum.Size = new System.Drawing.Size(411, 20);
            this.txtCheckSum.TabIndex = 40;
            // 
            // frmR5PreInstallSMSUpgrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 512);
            this.Controls.Add(this.ribbonControl1);
            this.MaximizeBox = false;
            this.Name = "frmR5PreInstallSMSUpgrade";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "R5 SMS SetUp";
            this.Load += new System.EventHandler(this.frmR5PreInstallSMSUpgrade_Load);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel3.PerformLayout();
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel2.PerformLayout();
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel4.PerformLayout();
            this.ribbonTabUserConfig.ResumeLayout(false);
            this.ribbonTabUserConfig.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabDSN;
        private DevComponents.DotNetBar.Office2007StartButton office2007StartButton1;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ButtonItem btnGenerateChecksum;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabUserDetails;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabQuery;
        private System.Windows.Forms.Label lblChecksum;
        private System.Windows.Forms.Label lblCRC2;
        private System.Windows.Forms.Label lblQueryResults;
        private System.Windows.Forms.TextBox txtUDUserID;
        private System.Windows.Forms.TextBox txtUDDSNId;
        private System.Windows.Forms.TextBox txtUDLoginName;
        private System.Windows.Forms.TextBox txtUDPwd;
        private System.Windows.Forms.Label lblUDUserID;
        private System.Windows.Forms.Label lblUDDSNId;
        private System.Windows.Forms.Label lblLoginName;
        private System.Windows.Forms.Label lblUDLoginPwd;
        private System.Windows.Forms.ComboBox cmbODBCDrivers;
        private System.Windows.Forms.TextBox txtDbName;
        private System.Windows.Forms.Label lblDbName;
        private System.Windows.Forms.TextBox txtDBServer;
        private System.Windows.Forms.Label lblDBServerName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDSNId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.TextBox txtDbPwd;
        private System.Windows.Forms.TextBox txtDbUserId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblDBUserID;
        private System.Windows.Forms.Label lblDBPwd;
        private DevComponents.DotNetBar.RibbonPanel ribbonTabUserConfig;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtUserId1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabUser;
        private DevComponents.DotNetBar.SuperTooltip superTooltip1;
        private DevComponents.DotNetBar.Controls.TextBoxX lblInsertQuery;
        private DevComponents.DotNetBar.Controls.TextBoxX lblUpdateQuery;
        private DevComponents.DotNetBar.ButtonX cmdCopyToClipboardInsert;
        private DevComponents.DotNetBar.ButtonX cmdCopyToClipboardUpdate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCRC;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCheckSum;


    }
}

