using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.Db;
using System.Diagnostics;
using Riskmaster.Scripting;
using Riskmaster.Security;
using Riskmaster.Common;
using System.Reflection;

namespace Riskmaster.Tools
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
        private UserLogin m_objUserLogin = null;
        private DbConnection m_objConn = null;
        private SCRIPT_RECORD m_xCurScript;

		private System.Windows.Forms.RichTextBox txtResponse;
		private System.Windows.Forms.Button cmdInsert;
		private System.Windows.Forms.Button cmdClearUI;
		private System.Windows.Forms.Button cmdFetchAll;
        private Button cmdFetchSingle;
        private TextBox txtRowId;
        private ComboBox cboScriptType;
        private Label lblRecordId;
        private Label lblScriptType;
        private ComboBox cboObject;
        private Label lblScriptObject;
        private Label label1;
        private TextBox txtDescription;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtResponse = new System.Windows.Forms.RichTextBox();
			this.cmdInsert = new System.Windows.Forms.Button();
			this.cmdClearUI = new System.Windows.Forms.Button();
			this.cmdFetchAll = new System.Windows.Forms.Button();
			this.cmdFetchSingle = new System.Windows.Forms.Button();
			this.txtRowId = new System.Windows.Forms.TextBox();
			this.cboScriptType = new System.Windows.Forms.ComboBox();
			this.lblRecordId = new System.Windows.Forms.Label();
			this.lblScriptType = new System.Windows.Forms.Label();
			this.cboObject = new System.Windows.Forms.ComboBox();
			this.lblScriptObject = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// txtResponse
			// 
			this.txtResponse.AcceptsTab = true;
			this.txtResponse.Location = new System.Drawing.Point(24, 40);
			this.txtResponse.Name = "txtResponse";
			this.txtResponse.Size = new System.Drawing.Size(560, 264);
			this.txtResponse.TabIndex = 0;
			this.txtResponse.Text = "";
			// 
			// cmdInsert
			// 
			this.cmdInsert.Location = new System.Drawing.Point(24, 358);
			this.cmdInsert.Name = "cmdInsert";
			this.cmdInsert.Size = new System.Drawing.Size(120, 23);
			this.cmdInsert.TabIndex = 9;
			this.cmdInsert.Text = "Compile && Save";
			this.cmdInsert.Click += new System.EventHandler(this.cmdInsert_Click);
			// 
			// cmdClearUI
			// 
			this.cmdClearUI.Location = new System.Drawing.Point(24, 434);
			this.cmdClearUI.Name = "cmdClearUI";
			this.cmdClearUI.Size = new System.Drawing.Size(120, 23);
			this.cmdClearUI.TabIndex = 10;
			this.cmdClearUI.Text = "Clear";
			this.cmdClearUI.Click += new System.EventHandler(this.cmdClearUI_Click);
			// 
			// cmdFetchAll
			// 
			this.cmdFetchAll.Location = new System.Drawing.Point(24, 385);
			this.cmdFetchAll.Name = "cmdFetchAll";
			this.cmdFetchAll.Size = new System.Drawing.Size(120, 23);
			this.cmdFetchAll.TabIndex = 11;
			this.cmdFetchAll.Text = "Fetch All";
			this.cmdFetchAll.Click += new System.EventHandler(this.cmdFetchAll_Click);
			// 
			// cmdFetchSingle
			// 
			this.cmdFetchSingle.Location = new System.Drawing.Point(24, 329);
			this.cmdFetchSingle.Name = "cmdFetchSingle";
			this.cmdFetchSingle.Size = new System.Drawing.Size(120, 23);
			this.cmdFetchSingle.TabIndex = 12;
			this.cmdFetchSingle.Text = "Fetch Single Script";
			this.cmdFetchSingle.Click += new System.EventHandler(this.cmdFetchSingle_Click);
			// 
			// txtRowId
			// 
			this.txtRowId.Location = new System.Drawing.Point(382, 329);
			this.txtRowId.Name = "txtRowId";
			this.txtRowId.Size = new System.Drawing.Size(121, 20);
			this.txtRowId.TabIndex = 13;
			this.txtRowId.Text = "0";
			this.txtRowId.TextChanged += new System.EventHandler(this.txtRowId_TextChanged);
			// 
			// cboScriptType
			// 
			this.cboScriptType.Location = new System.Drawing.Point(382, 355);
			this.cboScriptType.Name = "cboScriptType";
			this.cboScriptType.Size = new System.Drawing.Size(121, 21);
			this.cboScriptType.TabIndex = 14;
			this.cboScriptType.Text = "Not Selected";
			this.cboScriptType.SelectionChangeCommitted += new System.EventHandler(this.cboScriptType_SelectionChangeCommitted);
			// 
			// lblRecordId
			// 
			this.lblRecordId.AutoSize = true;
			this.lblRecordId.Location = new System.Drawing.Point(292, 334);
			this.lblRecordId.Name = "lblRecordId";
			this.lblRecordId.Size = new System.Drawing.Size(85, 16);
			this.lblRecordId.TabIndex = 15;
			this.lblRecordId.Text = "Script Record Id";
			// 
			// lblScriptType
			// 
			this.lblScriptType.AutoSize = true;
			this.lblScriptType.Location = new System.Drawing.Point(292, 358);
			this.lblScriptType.Name = "lblScriptType";
			this.lblScriptType.Size = new System.Drawing.Size(61, 16);
			this.lblScriptType.TabIndex = 16;
			this.lblScriptType.Text = "Script Type";
			this.lblScriptType.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// cboObject
			// 
			this.cboObject.Location = new System.Drawing.Point(382, 382);
			this.cboObject.MaxDropDownItems = 25;
			this.cboObject.Name = "cboObject";
			this.cboObject.Size = new System.Drawing.Size(121, 21);
			this.cboObject.TabIndex = 17;
			this.cboObject.Text = "Not Selected";
			// 
			// lblScriptObject
			// 
			this.lblScriptObject.AutoSize = true;
			this.lblScriptObject.Location = new System.Drawing.Point(292, 385);
			this.lblScriptObject.Name = "lblScriptObject";
			this.lblScriptObject.Size = new System.Drawing.Size(82, 16);
			this.lblScriptObject.TabIndex = 18;
			this.lblScriptObject.Text = "Scripted Object";
			this.lblScriptObject.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(292, 412);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 16);
			this.label1.TabIndex = 20;
			this.label1.Text = "Description";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// txtDescription
			// 
			this.txtDescription.AcceptsReturn = true;
			this.txtDescription.Location = new System.Drawing.Point(382, 412);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(238, 45);
			this.txtDescription.TabIndex = 21;
			this.txtDescription.Text = "";
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(632, 469);
			this.Controls.Add(this.txtDescription);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lblScriptObject);
			this.Controls.Add(this.cboObject);
			this.Controls.Add(this.lblScriptType);
			this.Controls.Add(this.lblRecordId);
			this.Controls.Add(this.cboScriptType);
			this.Controls.Add(this.txtRowId);
			this.Controls.Add(this.cmdFetchSingle);
			this.Controls.Add(this.cmdFetchAll);
			this.Controls.Add(this.cmdClearUI);
			this.Controls.Add(this.cmdInsert);
			this.Controls.Add(this.txtResponse);
			this.Name = "frmMain";
			this.Text = "Script Tool";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.Closed += new System.EventHandler(this.frmMain_Closed);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

        private void cmdInsert_Click(object sender, EventArgs e)
		{
            if (txtRowId.Text == "-1")
            {
                MessageBox.Show("All scripts cannot be saved back at once.\n\nOnly a single script table record may be edited at a time.");
                return;
            }
			ScriptEngine eng = new ScriptEngine(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
			eng.AddScript(txtResponse.Text);
			try{
				eng.Compile();
				eng.StoreBinary();
                m_xCurScript.sScriptText = txtResponse.Text;
                m_xCurScript.sObjectName = (string)cboObject.SelectedItem;
                m_xCurScript.iScriptType = (int)cboScriptType.SelectedValue;
                m_xCurScript.iRowId = Convert.ToInt32(txtRowId.Text);
                m_xCurScript.sDescription = txtDescription.Text;
                SaveScriptInDB(m_xCurScript);
                txtRowId.Text = m_xCurScript.iRowId.ToString();
             }
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		
		private void cmdFetchAll_Click(object sender, System.EventArgs e)
		{
            txtResponse.Text = "";
            txtRowId.Text = "-1";
            using (DbReader rdr = m_objConn.ExecuteReader("SELECT SCRIPT FROM VBS_SCRIPTS"))
            {
                while (rdr.Read())
                    txtResponse.Text += rdr.GetString("SCRIPT") + "\n\n";
                rdr.Close();
            };
        }

		private void frmMain_Load(object sender, System.EventArgs e)
		{
            Login objLogin = new Login();

            objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref m_objUserLogin);
            if (m_objUserLogin == null)
            {
                MessageBox.Show("Invalid RISKMASTER login.  \n\nProgram Cannot Continue");
                Application.Exit();
                return;
            }
            objLogin = null;

            m_objConn = DbFactory.GetDbConnection(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
            m_objConn.Open();

            //Prepare Combo Options
            cboScriptType.DisplayMember = "Display";
            cboScriptType.ValueMember = "Value";

            CBO_ITEM[] xScriptTypeItems = new CBO_ITEM[7];
            xScriptTypeItems[0] = new CBO_ITEM(0, "Not Selected");
            xScriptTypeItems[1] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_INITIALIZE, "Initialize");
            xScriptTypeItems[2] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_VALIDATE, "Validate");
            xScriptTypeItems[3] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_CALCULATE, "Calculate");
            xScriptTypeItems[4] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_BEFORESAVE, "BeforeSave");
            xScriptTypeItems[5] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_AFTERSAVE, "AfterSave");
            xScriptTypeItems[6] = new CBO_ITEM((int)ScriptTypes.SCRIPTID_SYSSCRIPT, "System Script");

            cboScriptType.DataSource = xScriptTypeItems;


            cboObject.DataSource = LoadDataModelObjectNames();
        }
        public class CBO_ITEM
        {
            private int m_Value;

            public int Value
            {
                get { return m_Value; }
            }
            private string m_Display;

            public string Display
            {
                get { return m_Display; }
            }

            public CBO_ITEM(int iValue, string sDisplay) { m_Value = iValue; m_Display = sDisplay; }
        }

        #region Load Data Model Object List, Using Reflection
        public ArrayList LoadDataModelObjectNames()
        {
            Assembly objDataModelAssembly = null;
            Type[] objTypes = null;
            Type objType = null;
            SortedList objSortedList = null;

            objDataModelAssembly = Assembly.GetAssembly(typeof(Riskmaster.DataModel.Event));
            objTypes = objDataModelAssembly.GetTypes();

            objSortedList = new SortedList();

            foreach (Type objTypeTemp in objTypes)
            {
                if (!objTypeTemp.IsClass)
                    continue;
                if (objTypeTemp.BaseType.Name == "DataCollection")
                    continue;
                if (objTypeTemp.BaseType.Name != "DataObject")
                    continue;

                objSortedList.Add(objTypeTemp.Name + "k", objTypeTemp.Name);
            }

            ArrayList arr = new ArrayList(objSortedList.Values);
            arr.Insert(0, "Not Selected");
            return arr;
        }

        #endregion 
        private void frmMain_Closed(object sender, System.EventArgs e)
        {
            if (m_objConn != null)
                m_objConn.Close();
            m_objConn = null;
        }

        private struct SCRIPT_RECORD
        {
   			public int iRowId;
            public int iScriptType;
            public string sScriptText;
            public string sObjectName;
            public string sAuthor;
            public string sCreatedOn;
            public string sModifiedBy;
            public string sModifiedOn;
            public string sDescription;
        }

        private void SaveScriptInDB( SCRIPT_RECORD xScript)
		{
          
            DbWriter objWriter = null ;
            try
            {
                //sDescription = xScript.sAuthor + Constants.SEPARATOR + xScript.sCreatedOn + Constants.SEPARATOR + xScript.sModifiedBy + Constants.SEPARATOR + xScript.sModifiedOn;
                objWriter = DbFactory.GetDbWriter(m_objConn);
                objWriter.Tables.Add("VBS_SCRIPTS");
                objWriter.Fields.Add("TYPE", xScript.iScriptType);
                objWriter.Fields.Add("OBJECT_NAME", xScript.sObjectName);
                objWriter.Fields.Add("SCRIPT", xScript.sScriptText);
                objWriter.Fields.Add("BINARY", new byte[]{}); //Still uses writer to accomplish binary field storing.
                //objWriter.Fields.Add("PDB", new byte[]{}); //Still uses writer to accomplish binary field storing.
                objWriter.Fields.Add("DESCRIPTION", xScript.sDescription);
                if (xScript.iRowId <= 0)
                {
                    xScript.iRowId = Utilities.GetNextUID(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "VBS_SCRIPTS");
                    objWriter.Fields.Add("ROW_ID", xScript.iRowId);
                }
                else
                {
                    objWriter.Where.Add("ROW_ID='" + xScript.iRowId + "'");
                }

                objWriter.Execute();
				MessageBox.Show("Script Saved Successfully");

            }
            finally
            {
                objWriter = null;
            }

		}

        private void cmdClearUI_Click(object sender, EventArgs e)
        {
            this.txtResponse.Clear();
            this.txtDescription.Clear();
            this.txtRowId.Clear();
            this.cboObject.SelectedIndex = 0;
            this.cboScriptType.SelectedIndex = 0;
        }

        private void cboScriptType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if ((int)cboScriptType.SelectedValue == (int)ScriptTypes.SCRIPTID_SYSSCRIPT)
                cboObject.SelectedIndex = 0;
            if ((int)cboScriptType.SelectedIndex == 0)
                cboObject.SelectedIndex = 0;
        }

        private void cmdFetchSingle_Click(object sender, EventArgs e)
        {
            txtResponse.Clear();
            int i = 0;
            try{i  = Convert.ToInt32(txtRowId.Text);}catch{;}
            string SQL;


            if (i > 0)
                SQL = String.Format("SELECT * FROM VBS_SCRIPTS WHERE ROW_ID={0}", i);
            else if (cboScriptType.SelectedIndex > 0 && cboObject.SelectedIndex > 0)
                SQL = String.Format("SELECT * FROM VBS_SCRIPTS WHERE TYPE={0} AND OBJECT_NAME LIKE '{1}'", cboScriptType.SelectedValue, cboObject.SelectedValue);
            else
            {
                MessageBox.Show("Please specify criteria for selecting a single script and try again.");
                return;
            }

            bool bResultFound = false;
            using(DbReader rdr = m_objConn.ExecuteReader(SQL))
            {
                
                while(rdr.Read()) //This better be a single record...
                {
                   bResultFound = true;
                   txtResponse.Text = rdr.GetString("SCRIPT");
                   txtDescription.Text = rdr.GetString("DESCRIPTION");
                   cboScriptType.SelectedValue = rdr.GetInt("TYPE");
                   if (cboObject.Items.Contains(rdr.GetString("OBJECT_NAME")))
                       cboObject.SelectedIndex = cboObject.Items.IndexOf(rdr.GetString("OBJECT_NAME"));
                   else
                       cboObject.SelectedIndex = 0;
                }
                rdr.Close();
            }
            if(!bResultFound)
                MessageBox.Show("No matching script found in the database.");

        }

        private void txtRowId_TextChanged(object sender, EventArgs e)
        {
            this.txtResponse.Clear();
            this.txtDescription.Clear();
            this.cboObject.SelectedIndex = 0;
            this.cboScriptType.SelectedIndex = 0;

        }

  
     }
}
