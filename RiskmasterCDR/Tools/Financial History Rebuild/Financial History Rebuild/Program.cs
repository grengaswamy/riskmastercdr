﻿using System;
using System.Data;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Configuration;

namespace Financial_History_Rebuild
{
    /// <summary>
    /// Author : Deb 
    /// Date : 12/23/2013
    /// </summary>
    public class Program
    {
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static string m_sLoginPwd = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string sJobID = string.Empty;
        static string sMode = string.Empty;
        static string sClaimId = string.Empty;
        static DbConnection m_objDbConnection = null;
        static DbConnection m_objSecDbConnection = null;
        static DbCommand m_objSecDbCommand = null;
        static DbCommand m_objCmd = null;
        static eDatabaseType m_sDatabaseType = 0;
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            UserLogin oUserLogin = null;
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            int iClientId = 0;//Add & Change by kuladeep for Cloud.
            string sTempClientId = string.Empty;
            bool blnSuccess = false;
            bool blnIsClientIdPassed = false;

            try
            {
                int argCount = (args == null) ? 0 : args.Length;

                //Add & Change by kuladeep for Cloud----Start
                if (args != null && args.Length > 3 && (args[3] != null))
                {
                    sTempClientId = args[3] ?? string.Empty;
                    blnIsClientIdPassed = Int32.TryParse(sTempClientId, out iClientId);
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run WPA through exe for different client by change ClientId in appSetting.config.
                else
                {
                    iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by kuladeep for Cloud----End

                if (argCount > 0) //Entry from TaskManager
                {
                    GetParameters(args, blnIsClientIdPassed);
                    oUserLogin = new UserLogin(m_sLoginName, m_sDataSource, iClientId);//Add & Change by kuladeep for Cloud.
                    if (oUserLogin.DatabaseId <= 0)
                    {
                        throw new Exception("Authentication failure.");
                    }
                    Initalize(oUserLogin);
                }
                else //Entry for direct run
                {
                    Login objLogin = new Login(iClientId);//Add & Change by kuladeep for Cloud.
                    bool bCon = false;
                    bCon = objLogin.RegisterApplication(0, 0, ref oUserLogin,iClientId);//Add & Change by kuladeep for Cloud.
                    if (bCon)
                    {
                        Initalize(oUserLogin);
                    }
                    else
                    {
                        return;
                    }
                }
                OpenDbConnection();     
                ProcessFinancialHistoryRebuild(iClientId);//psharma206
            }
            catch (Exception exc)
            {
                smsg = exc.Message;
                FormatAndDisplayExcMsg(smsg);
                Log.Write(exc.Message, "CommonWebServiceLog", iClientId);//psharma206
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();

                }
                if (m_objSecDbConnection != null)
                {
                    m_objSecDbConnection.Close();
                    m_objDbConnection.Dispose();
                }
                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objSecDbCommand != null)
                    m_objSecDbCommand = null;

            }
        }
        /// <summary>
        /// OpenDbConnection
        /// </summary>
        private static void OpenDbConnection()
        {
            m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
            m_objDbConnection.Open();
            m_sDatabaseType = m_objDbConnection.DatabaseType;
        }
        /// <summary>
        /// ProcessFinancialHistoryRebuild
        /// </summary>
        private static void ProcessFinancialHistoryRebuild(int p_iClientId)//psharma206
        {
            string sSQL = string.Empty;
            string serr_msg = string.Empty;
            DbParameter objParamout = null;
            DbParameter objParam = null;
            try
            {
                m_objCmd = m_objDbConnection.CreateCommand();
                sSQL = "USP_FINANCIAL_HISTORY_REBUILD";
                m_objCmd.CommandText = sSQL;
                m_objCmd.CommandType = CommandType.StoredProcedure;

                if (string.IsNullOrEmpty(sJobID)) sJobID = ConfigurationManager.AppSettings["JobId"];
                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.String;
                objParam.Size = 100;
                objParam.ParameterName = "@p_jobID";
                objParam.Value = sJobID;
                m_objCmd.Parameters.Add(objParam);

                if (string.IsNullOrEmpty(sMode)) sMode = ConfigurationManager.AppSettings["Mode"];
                objParam = m_objCmd.CreateParameter();
                objParam.Direction = System.Data.ParameterDirection.Input;
                objParam.DbType = DbType.String;
                objParam.Size = 100;
                objParam.ParameterName = "@p_mode";
                objParam.Value = sMode;
                m_objCmd.Parameters.Add(objParam);

                if (string.IsNullOrEmpty(sClaimId)) sClaimId = ConfigurationManager.AppSettings["ClaimId"];
                if (!string.IsNullOrEmpty(sClaimId))
                {
                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@p_claimid";
                    objParam.Value = sClaimId;
                    m_objCmd.Parameters.Add(objParam);
                }
                else if (eDatabaseType.DBMS_IS_ORACLE == m_sDatabaseType)
                {
                    objParam = m_objCmd.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.DbType = DbType.String;
                    objParam.Size = 100;
                    objParam.ParameterName = "@p_claimid";
                    objParam.Value = sClaimId;
                    m_objCmd.Parameters.Add(objParam);
                }

                objParamout = m_objCmd.CreateParameter();
                objParamout.Direction = System.Data.ParameterDirection.Output;
                objParamout.DbType = DbType.String;
                objParamout.Size = 4000;
                objParamout.ParameterName = "@p_sqlerrm";
                m_objCmd.Parameters.Add(objParamout);
                m_objCmd.CommandText = sSQL;
                
                Console.WriteLine("0 ^*^*^ Started procedure FINANCIAL_HISTORY_REBUILD ");
                m_objCmd.ExecuteNonQuery();
                serr_msg = objParamout.Value.ToString();
                string[] sOut = serr_msg.Split('|');
                foreach (string s in sOut)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        DateTime sDatetime = Conversion.ToDate(s.Split(':')[0]);
                        Console.WriteLine("0 ^*^*^ [" + sDatetime + "] " + s.Split(':')[1]);
                    }
                }
                Console.WriteLine("0 ^*^*^ Completed procedure FINANCIAL_HISTORY_REBUILD ");
            }
            catch (Exception ex)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", "Error occured in FINANCIAL_HISTORY_REBUILD");
                serr_msg = ex.Message;
                Log.Write(serr_msg, "CommonWebServiceLog", p_iClientId);//psharma206
            }
        }
        /// <summary>
        /// Format Exception Message returned from Stored Procedure.
        /// </summary>
        /// <param name="smsg">Exception Message</param>
        private static void FormatAndDisplayExcMsg(string smsg)
        {
            string[] sMsgArray = smsg.Split('\n');

            int iLengthofArray = sMsgArray.Length;
            for (int i = 0; i < iLengthofArray; i++)
            {
                sMsgArray[i] = sMsgArray[i].Replace("'", "");
                sMsgArray[i] = sMsgArray[i].Replace("-", " ");
                Console.WriteLine("1001 ^*^*^ {0} ", sMsgArray[i]);
            }
        }

        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            try
            {
                m_sDSNID = p_oUserLogin.DatabaseId.ToString();
                m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
                m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
                m_sDatabaseType = DbFactory.GetDatabaseType(m_sDbConnstring);
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
            }

        }
        /// <summary>
        /// GetParameters
        /// </summary>
        /// <param name="p_args"></param>
        private static void GetParameters(string[] p_args, bool blnIsClientIdPassed)
        {
            #region Args Passed
            //<Task Name="FinHist" cmdline="yes"><Path>finhistrebuild.exe</Path>
            //<Args><arg>RMA141_ACOFF</arg>
            //<arg>csc</arg>
            //<arg type="password">fa6db697515ef18b</arg>
            //<arg type="user">-zerobasedfinhist zerobasedcrit=claim -full -log</arg></Args>
            //</Task>
            #endregion
            m_sDataSource = p_args[0].Trim();
            m_sLoginName = p_args[1].Trim();
            m_sLoginPwd = p_args[2].Trim();
            string sUserParameter = string.Empty;
            int iLoopIndex = 3;

            if (blnIsClientIdPassed)
                iLoopIndex++;


            for (; iLoopIndex < p_args.Length; iLoopIndex++)
            {
                sUserParameter = sUserParameter + " " + p_args[iLoopIndex].Trim();
            }
            sUserParameter = sUserParameter.Replace("-log","").Trim().ToLower();
            switch (sUserParameter)
            {
                case "-zerobasedfinhist zerobasedcrit=claim":
                  sMode = "2";
                  break;
                case "-zerobasedfinhist zerobasedcrit=event":
                  sMode = "4";
                  break;
                case "-zerobasedfinhist zerobasedcrit=claim -full":
                  sMode = "3";
                  break;
                case "-zerobasedfinhist zerobasedcrit=event -full":
                  sMode = "5";
                  break;
                case "-full":
                  sMode = "1";
                  break;
                //Deb Commented: it can populated from the App.config and which can be used direct run of the exe also
                //default:
                // sMode="0";
                // break;
            }
        }
    }
}

