﻿namespace RiskmasterTools.PowerViewUpgrade
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConvert = new System.Windows.Forms.Button();
            this.pBar1 = new System.Windows.Forms.ProgressBar();
            this.chkTopDown = new System.Windows.Forms.CheckBox();
            this.chkReadOnly = new System.Windows.Forms.CheckBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.lstDSNList = new System.Windows.Forms.ListBox();
            this.rdbPVJuris = new System.Windows.Forms.RadioButton();
            this.rdbJurisData = new System.Windows.Forms.RadioButton();
            this.rdbPV = new System.Windows.Forms.RadioButton();
            this.bgWorkerUpgradePowerViews = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(100, 298);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(130, 23);
            this.btnConvert.TabIndex = 1;
            this.btnConvert.Text = "Upgrade PowerViews";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // pBar1
            // 
            this.pBar1.Location = new System.Drawing.Point(15, 342);
            this.pBar1.Name = "pBar1";
            this.pBar1.Size = new System.Drawing.Size(437, 23);
            this.pBar1.TabIndex = 3;
            // 
            // chkTopDown
            // 
            this.chkTopDown.AutoSize = true;
            this.chkTopDown.Location = new System.Drawing.Point(15, 20);
            this.chkTopDown.Name = "chkTopDown";
            this.chkTopDown.Size = new System.Drawing.Size(111, 17);
            this.chkTopDown.TabIndex = 15;
            this.chkTopDown.Text = "Top Down Layout";
            this.chkTopDown.UseVisualStyleBackColor = true;
            // 
            // chkReadOnly
            // 
            this.chkReadOnly.AutoSize = true;
            this.chkReadOnly.Location = new System.Drawing.Point(15, 43);
            this.chkReadOnly.Name = "chkReadOnly";
            this.chkReadOnly.Size = new System.Drawing.Size(76, 17);
            this.chkReadOnly.TabIndex = 16;
            this.chkReadOnly.Text = "Read Only";
            this.chkReadOnly.UseVisualStyleBackColor = true;
            // 
            // lblFileName
            // 
            this.lblFileName.Location = new System.Drawing.Point(12, 324);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(290, 15);
            this.lblFileName.TabIndex = 17;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(252, 298);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "Close";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lstDSNList
            // 
            this.lstDSNList.FormattingEnabled = true;
            this.lstDSNList.Location = new System.Drawing.Point(13, 13);
            this.lstDSNList.Name = "lstDSNList";
            this.lstDSNList.Size = new System.Drawing.Size(439, 160);
            this.lstDSNList.TabIndex = 19;
            // 
            // rdbPVJuris
            // 
            this.rdbPVJuris.AutoSize = true;
            this.rdbPVJuris.Checked = true;
            this.rdbPVJuris.Location = new System.Drawing.Point(6, 19);
            this.rdbPVJuris.Name = "rdbPVJuris";
            this.rdbPVJuris.Size = new System.Drawing.Size(198, 17);
            this.rdbPVJuris.TabIndex = 20;
            this.rdbPVJuris.TabStop = true;
            this.rdbPVJuris.Text = "Upgrade PowerViews and Juris Data";
            this.rdbPVJuris.UseVisualStyleBackColor = true;
            // 
            // rdbJurisData
            // 
            this.rdbJurisData.AutoSize = true;
            this.rdbJurisData.Location = new System.Drawing.Point(6, 65);
            this.rdbJurisData.Name = "rdbJurisData";
            this.rdbJurisData.Size = new System.Drawing.Size(140, 17);
            this.rdbJurisData.TabIndex = 21;
            this.rdbJurisData.Text = "Upgrade Juris Data Only";
            this.rdbJurisData.UseVisualStyleBackColor = true;
            // 
            // rdbPV
            // 
            this.rdbPV.AutoSize = true;
            this.rdbPV.Location = new System.Drawing.Point(6, 43);
            this.rdbPV.Name = "rdbPV";
            this.rdbPV.Size = new System.Drawing.Size(151, 17);
            this.rdbPV.TabIndex = 22;
            this.rdbPV.Text = "Upgrade PowerViews Only";
            this.rdbPV.UseVisualStyleBackColor = true;
            // 
            // bgWorkerUpgradePowerViews
            // 
            this.bgWorkerUpgradePowerViews.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerUpgradePowerViews_DoWork);
            this.bgWorkerUpgradePowerViews.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerUpgradePowerViews_RunWorkerCompleted);
            this.bgWorkerUpgradePowerViews.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerUpgradePowerViews_ProgressChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkTopDown);
            this.groupBox1.Controls.Add(this.chkReadOnly);
            this.groupBox1.Location = new System.Drawing.Point(15, 179);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PowerView Types";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbPVJuris);
            this.groupBox2.Controls.Add(this.rdbPV);
            this.groupBox2.Controls.Add(this.rdbJurisData);
            this.groupBox2.Location = new System.Drawing.Point(252, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 100);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PowerView Settings";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 377);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstDSNList);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.pBar1);
            this.Controls.Add(this.btnConvert);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PowerView Conversion Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.ProgressBar pBar1;
        private System.Windows.Forms.CheckBox chkTopDown;
        private System.Windows.Forms.CheckBox chkReadOnly;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ListBox lstDSNList;
        private System.Windows.Forms.RadioButton rdbPVJuris;
        private System.Windows.Forms.RadioButton rdbJurisData;
        private System.Windows.Forms.RadioButton rdbPV;
        private System.ComponentModel.BackgroundWorker bgWorkerUpgradePowerViews;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

