﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Collections.Generic;
using Riskmaster.Common;

namespace RiskmasterTools.PowerViewUpgrade
{
    public partial class Form1 : Form
    {
        private Login m_objLogin;
        private UserLogin m_userLogin;
        const int ADM_TYPE = 468;

        private string m_sConnectionString = String.Empty;

        private string ViewConnectionString
        {
            get{return m_sConnectionString;}
            set{m_sConnectionString = value;}
        }// property ViewConnectionString

        private string LoginName
        {
            get;
            set;
        }

        private string LoginPassword
        {
            get;
            set;
        }

        private int DSNID
        {
            get;
            set;
        } 


        public Form1()
        {
            InitializeComponent();

            this.ViewConnectionString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            // Set Minimum to 1 to represent the first file being copied.
            pBar1.Minimum = 1;
            // Set the initial value of the ProgressBar.
            pBar1.Value = 1;
            // Set the Step property to a value of 1 to represent each file being copied.
            pBar1.Step = 1;

            
            

            string[] args = {this.lstDSNList.SelectedValue.ToString(), this.ViewConnectionString};

            bool blnSuccess = UpgradePowerViews(this.lstDSNList.SelectedValue.ToString(), this.ViewConnectionString);

            if (blnSuccess)
            {
                MessageBox.Show("PowerView conversion completed Successfully");
            } // if
            else
            {
                MessageBox.Show("Unable to successfully convert all PowerViews.  Please review your error log for possible upgrade error messages.");
            } // else

            //bgWorkerUpgradePowerViews.RunWorkerAsync(args);
        }

        
        public bool UpgradePowerViews(string strSelectedDSN, string connectionString)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlDocument xdocAdmin = new XmlDocument();
            XmlDocument xdocJuris = new XmlDocument();
            StringBuilder convertedAspx;
            int iUpdate = 0;
            int iAdminTableRows = 0;
            DbCommand cmd = null;
            DbParameter objParam = null;
            DbParameter objParamXml = null;
            
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbSelectSql = new StringBuilder();
            StringBuilder sbAdminTablesCount = new StringBuilder();
            string sSQLAdminTablesList = string.Empty;
            DataTable ds = null;
            ArrayList retAdminTables = new ArrayList();
            bool bSuccess = false;
            string sPageName = string.Empty;
            string sFormName = string.Empty;
            string sViewName = string.Empty;
            bool bConvertWCJuris = false;
            XmlDocument objJuris = null;
            XmlNodeList objJurisData = null;
            XmlDocument objIndividualStateData = null;
            int iStateId = 0;
            object objDataSourceDB = null;
            StringBuilder sResponse = new StringBuilder();
            UTILITY.PowerViewMode CurrentMode = UTILITY.PowerViewMode.ALL;
            UTILITY.UpgradeSection CurrentSection;
            string sTempFormName = string.Empty;
            using (DbConnection oConn = DbFactory.GetDbConnection(connectionString))
            {
                try
                {
                    oConn.Open();

                    m_userLogin = new UserLogin(this.LoginName, this.LoginPassword, strSelectedDSN);
                    this.DSNID = m_userLogin.DatabaseId;

                    CurrentSection = GetSelectedSection();

                    if (CurrentSection == UTILITY.UpgradeSection.ONLYJURIS)
                        sTempFormName = "claimwc.xml";
                    sbSelectSql.Append("SELECT FORM_NAME,VIEW_XML, NET_VIEW_FORMS.VIEW_ID, VIEW_NAME FROM NET_VIEW_FORMS ");
                    sbSelectSql.Append(" LEFT OUTER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID=NET_VIEWS.VIEW_ID ");
                    sbSelectSql.Append(String.Format(" WHERE UPPER(FORM_NAME) NOT LIKE '%LIST.XML' AND UPPER(FORM_NAME) NOT LIKE '%.XSL' AND UPPER(FORM_NAME) NOT LIKE '%.SCC' AND NET_VIEW_FORMS.DATA_SOURCE_ID ='{0}'", this.DSNID));

                    if (! string.IsNullOrEmpty(sTempFormName))
                    {
                        sbSelectSql.Append(string.Format("AND UPPER(FORM_NAME) = '{0}'", sTempFormName));                        
                    }
                    sbSelectSql.Append(" ORDER BY NET_VIEW_FORMS.VIEW_ID, FORM_NAME");

                    Riskmaster.Application.RMUtilities.PowerViewUpgrade pvUpgrade = new Riskmaster.Application.RMUtilities.PowerViewUpgrade();

                    ds = GetPowerViews(connectionString, sbSelectSql.ToString());

                    if (ds.Rows.Count > 0)
                    {
                        // Set Maximum to the total number of files to copy.
                        pBar1.Maximum = ds.Rows.Count;

                        foreach (DataRow row in ds.Rows)
                        {
                            sViewName = IsBaseView(row["VIEW_NAME"].ToString());
                            sFormName = row["FORM_NAME"].ToString();
                            string sViewID = row["VIEW_ID"].ToString();
                            string sViewXML = row["VIEW_XML"].ToString();

                            sbSql.Length = 0;

                            UpdateProgressBar(sFormName, sViewName);

                            xdoc.LoadXml(sViewXML);
                            if (sViewID == "0")
                            {
                                if (sFormName.IndexOf("admintracking") < 0 && sFormName.IndexOf("claimwc") < 0)
                                {
                                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, connectionString, lstDSNList.SelectedValue.ToString(), xdoc, false, out retAdminTables, null, CurrentMode, "", CurrentSection);
                                }
                                else if (sFormName.IndexOf("claimwc.xml") > -1)
                                {
                                    objJuris = new XmlDocument();
                                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, connectionString, lstDSNList.SelectedValue.ToString(), xdoc, false, out retAdminTables, objJuris, CurrentMode, "", CurrentSection);
                                    bConvertWCJuris = true;
                                }
                                //if condition added to generate the aspx of admintracking tables
                                else if (sFormName.IndexOf("admintracking.xml") > -1)
                                {
                                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, connectionString, lstDSNList.SelectedValue.ToString(), xdoc, true, out retAdminTables, null, CurrentMode, "", CurrentSection);
                                    foreach (string[] arrItem in retAdminTables)
                                    {
                                        xdocAdmin.LoadXml(xdoc.OuterXml);
                                        sbSql.Length = 0;
                                        sbAdminTablesCount.Length = 0;

                                        ((XmlElement)xdocAdmin.GetElementsByTagName("form")[0]).SetAttribute("supp", arrItem[0].ToString());
                                        ((XmlNode)xdocAdmin.SelectSingleNode("//internal[@name='SysFormName']")).Attributes["value"].Value = "admintracking|" + arrItem[0].ToString();
                                        UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, connectionString, lstDSNList.SelectedValue.ToString(), xdocAdmin, false, out retAdminTables, null, CurrentMode, "", CurrentSection);
                                        pvUpgrade.UpgradeXmlToAspx(chkReadOnly.Checked, false, xdocAdmin, sFormName.Insert(sFormName.IndexOf('.'), arrItem[0].ToString()), out convertedAspx);
                                        cmd = oConn.CreateCommand();
                                        sbAdminTablesCount.Append(String.Format("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME LIKE '%{0}%' AND DATA_SOURCE_ID = '{1}'", arrItem[0], this.DSNID));
                                        cmd.CommandText = sbAdminTablesCount.ToString();
                                        iAdminTableRows = Convert.ToInt32(cmd.ExecuteScalar());
                                        if (iAdminTableRows <= 0)
                                        {
                                            cmd = oConn.CreateCommand();
                                            objParamXml = cmd.CreateParameter();
                                            objParamXml.Direction = ParameterDirection.Input;
                                            objParamXml.Value = xdocAdmin.OuterXml;
                                            objParamXml.ParameterName = "XML";
                                            objParamXml.SourceColumn = "VIEW_XML";
                                            objParam = cmd.CreateParameter();
                                            objParam.Direction = ParameterDirection.Input;
                                            objParam.Value = convertedAspx.ToString();
                                            objParam.ParameterName = "ASPX";
                                            objParam.SourceColumn = "PAGE_CONTENT";
                                            cmd.Parameters.Add(objParamXml);
                                            cmd.Parameters.Add(objParam);
                                            sSQLAdminTablesList = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_CONTENT,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (" +
                                                                  "0" + ",'" + "admintracking|" + arrItem[0] + ".xml" +
                                                                  "'," + "1" + ",'" + arrItem[1] + "'," +
                                                                  "~XML~,~ASPX~,'" + "admintracking" + arrItem[0] + ".aspx" + "','" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + this.DSNID.ToString() + "')";
                                            cmd.CommandText = sSQLAdminTablesList;
                                            iUpdate = cmd.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            cmd = oConn.CreateCommand();
                                            objParamXml = cmd.CreateParameter();
                                            objParamXml.Direction = ParameterDirection.Input;
                                            objParamXml.Value = xdocAdmin.OuterXml;
                                            objParamXml.ParameterName = "XML";
                                            objParamXml.SourceColumn = "VIEW_XML";
                                            objParam = cmd.CreateParameter();
                                            objParam.Direction = ParameterDirection.Input;
                                            objParam.Value = convertedAspx.ToString();
                                            objParam.ParameterName = "ASPX";
                                            objParam.SourceColumn = "PAGE_CONTENT";
                                            cmd.Parameters.Add(objParamXml);
                                            cmd.Parameters.Add(objParam);
                                            sPageName = sFormName.Replace("xml", "aspx");
                                            sbSql.Append(String.Format("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT =~ASPX~,LAST_UPDATED = '{0:yyyyMMddHHmmss}',PAGE_NAME = 'admintracking{1}.aspx' WHERE FORM_NAME = 'admintracking|{1}.xml' AND VIEW_ID = {2} AND DATA_SOURCE_ID ='{3}'", System.DateTime.Now, arrItem[0], row["VIEW_ID"], this.DSNID));
                                            cmd.CommandText = sbSql.ToString();
                                            iUpdate = cmd.ExecuteNonQuery();
                                        }
                                    }
                                    continue;
                                }
                                else
                                {
                                    continue;
                                }
                            }//if

                            pvUpgrade.UpgradeXmlToAspx(chkReadOnly.Checked, chkTopDown.Checked, xdoc, sFormName, out convertedAspx);
                            cmd = oConn.CreateCommand();
                            objParam = cmd.CreateParameter();
                            objParam.Direction = ParameterDirection.Input;
                            objParam.Value = convertedAspx.ToString();
                            objParam.ParameterName = "ASPX";
                            objParam.SourceColumn = "PAGE_CONTENT";
                            cmd.Parameters.Add(objParam);
                            sPageName = sFormName.Replace("xml", "aspx");
                            sbSql.Append(String.Format("UPDATE NET_VIEW_FORMS SET PAGE_CONTENT = ~ASPX~,PAGE_NAME = '{0}',LAST_UPDATED = '{1:yyyyMMddHHmmss}' WHERE FORM_NAME = '{2}' AND VIEW_ID = {3} AND DATA_SOURCE_ID ='{4}'", sPageName, System.DateTime.Now, row["FORM_NAME"], row["VIEW_ID"], this.DSNID));
                            cmd.CommandText = sbSql.ToString();
                            iUpdate = cmd.ExecuteNonQuery();

                            if (bConvertWCJuris)
                            {
                                objJurisData = objJuris.SelectNodes("/JurisData/*");

                                foreach (XmlNode objJurisNode in objJurisData)
                                {
                                    // Get the State
                                    sbSql.Remove(0, sbSql.Length);
                                    convertedAspx.Remove(0, convertedAspx.Length);
                                    sResponse.Remove(0, sResponse.Length);
                                    iStateId = Int32.Parse(objJurisNode.Attributes["StateId"].Value);
                                    objIndividualStateData = new XmlDocument();
                                    objIndividualStateData.LoadXml(objJurisNode.OuterXml);

                                    pvUpgrade.UpgradeXmlTagToAspxFormJuris(chkReadOnly.Checked, chkTopDown.Checked, objIndividualStateData, sFormName, out convertedAspx);

                                    sResponse.Append("<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>");
                                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>");
                                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>");
                                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>");
                                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>");
                                    sResponse.Append(convertedAspx.ToString());

                                    // Check the Existence of Record in DB
                                    sbSql.Append(String.Format("SELECT DATA_SOURCE_ID FROM JURIS_VIEWS WHERE DATA_SOURCE_ID = {0} AND STATE_ID = {1}", this.DSNID, iStateId));
                                    cmd = oConn.CreateCommand();
                                    cmd.CommandText = sbSql.ToString();
                                    objDataSourceDB = cmd.ExecuteScalar();
                                    cmd = oConn.CreateCommand();
                                    objParam = cmd.CreateParameter();
                                    objParam.Direction = ParameterDirection.Input;
                                    objParam.Value = sResponse.ToString();
                                    objParam.ParameterName = "ASPX";
                                    objParam.SourceColumn = "PAGE_CONTENT";
                                    cmd.Parameters.Add(objParam);
                                    sbSql.Remove(0, sbSql.Length);
                                    if (objDataSourceDB == null)
                                    {
                                        sbSql.Append("INSERT INTO JURIS_VIEWS(DATA_SOURCE_ID, STATE_ID, LAST_UPDATED, PAGE_CONTENT) VALUES(" + this.DSNID + "," + iStateId.ToString() + ",'" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "', ~ASPX~ )");
                                    }
                                    else
                                    {
                                        sbSql.Append("UPDATE JURIS_VIEWS SET LAST_UPDATED = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "', PAGE_CONTENT=~ASPX~ WHERE DATA_SOURCE_ID = " + this.DSNID + "AND STATE_ID = " + iStateId.ToString());
                                    }


                                    cmd.CommandText = sbSql.ToString();
                                    iUpdate = cmd.ExecuteNonQuery();
                                }
                                bConvertWCJuris = false;
                            }
                        }
                    }//if
                    else
                    {
                        MessageBox.Show("No PowerViews found to convert.");
                    }//else

                    bSuccess = true;
                }//try
                catch (Exception ex)
                {
                    CreateLogs objLogs = new CreateLogs();
                    objLogs.ErrorLog("PowerViewUpgradeLogs", ex);
                }
                finally
                {
                    //Perform DataTable object cleanup
                    ds = null;
                } // finally

            }//using
           

            return bSuccess;
        }

        private UTILITY.UpgradeSection GetSelectedSection()
        {
            UTILITY.UpgradeSection CurrentSection;

            if (rdbPVJuris.Checked)
                CurrentSection = UTILITY.UpgradeSection.ALL;
            else if (rdbPV.Checked)
                CurrentSection = UTILITY.UpgradeSection.ONLYSUPP;
            else
                CurrentSection = UTILITY.UpgradeSection.ONLYJURIS;
            return CurrentSection;
        }

        private void UpdateCustomPowerViews(XmlDocument xdoc, ref StringBuilder convertedAspx, Riskmaster.Application.RMUtilities.PowerViewUpgrade pvUpgrade, DataRow row)
        {
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            StringBuilder sbSql = new StringBuilder();

            //Get all the fields from the DataTable
            string sViewName = IsBaseView(row["VIEW_NAME"].ToString());
            string sFormName = row["FORM_NAME"].ToString();
            string sViewXML = row["VIEW_XML"].ToString();
            string sViewID = row["VIEW_ID"].ToString();

            

            string sPageName = sFormName.Replace("xml", "aspx");
            sbSql.Append(String.Format("UPDATE NET_VIEW_FORMS SET PAGE_CONTENT = ~ASPX~,PAGE_NAME = '{0}',LAST_UPDATED = '{1:yyyyMMddHHmmss}' WHERE FORM_NAME = '{2}' AND VIEW_ID = {3} AND DATA_SOURCE_ID ='{4}'", sPageName, System.DateTime.Now, sFormName, sViewID, this.DSNID));

            dictParams.Clear();
            dictParams.Add("ASPX", convertedAspx.ToString());

            int iUpdate = DbFactory.ExecuteNonQuery(this.ViewConnectionString, sbSql.ToString(), dictParams);
            
        }

        private void UpgradeJurisViews(ref XmlNodeList objJurisData, Riskmaster.Application.RMUtilities.PowerViewUpgrade pvUpgrade, string sFormName)
        {

            StringBuilder sbSql = new StringBuilder();
            StringBuilder convertedAspx = new StringBuilder();
            StringBuilder sResponse = new StringBuilder();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            XmlDocument objIndividualStateData = new XmlDocument();

            

                foreach (XmlNode objJurisNode in objJurisData)
                {
                    // Get the State
                    int iStateId = Int32.Parse(objJurisNode.Attributes["StateId"].Value);
                    
                    objIndividualStateData.LoadXml(objJurisNode.OuterXml);

                    pvUpgrade.UpgradeXmlTagToAspxFormJuris(chkReadOnly.Checked, chkTopDown.Checked, objIndividualStateData, sFormName, out convertedAspx);

                    sResponse.Append("<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>");
                    sResponse.Append(convertedAspx.ToString());

                    // Check the Existence of Record in DB
                    sbSql.AppendFormat(String.Format("SELECT DATA_SOURCE_ID FROM JURIS_VIEWS WHERE DATA_SOURCE_ID = {0} AND STATE_ID = {1}", this.DSNID, iStateId));
                    object objDataSourceDB = DbFactory.ExecuteScalar(this.ViewConnectionString, sbSql.ToString());

                    if (objDataSourceDB == null)
                    {
                        sbSql.AppendFormat(String.Format("INSERT INTO JURIS_VIEWS(DATA_SOURCE_ID, STATE_ID, LAST_UPDATED, PAGE_CONTENT) VALUES({0},{1},'{2:yyyyMMddHHmmss}', ~ASPX~ )", this.DSNID, iStateId, System.DateTime.Now));
                    }
                    else
                    {
                        sbSql.AppendFormat(String.Format("UPDATE JURIS_VIEWS SET LAST_UPDATED = '{0:yyyyMMddHHmmss}', PAGE_CONTENT=~ASPX~ WHERE DATA_SOURCE_ID = {1}AND STATE_ID = {2}", System.DateTime.Now, this.DSNID, iStateId));
                    }

                    dictParams.Clear();
                    dictParams.Add("ASPX", sResponse.ToString());

                    int iUpdate = DbFactory.ExecuteNonQuery(this.ViewConnectionString, sbSql.ToString(), dictParams);
                }
        }

        private void UpdateBaseViews(string strSelectedDSN, ref Riskmaster.Application.RMUtilities.PowerViewUpgrade pvUpgrade, DataRow row, ref StringBuilder convertedAspx)
        {
            StringBuilder sbSql = new StringBuilder(), sbAdminTablesCount = new StringBuilder();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            int iUpdate = 0;
            XmlDocument xdoc = new XmlDocument(), xdocAdmin = new XmlDocument(), objJuris = new XmlDocument();
            UTILITY.PowerViewMode CurrentMode = UTILITY.PowerViewMode.ALL;
            bool bConvertWCJuris = false;
            ArrayList retAdminTables = new ArrayList();
            UTILITY.UpgradeSection CurrentSection = GetSelectedSection();
            

            //Get all the fields from the DataTable
            string sViewName = IsBaseView(row["VIEW_NAME"].ToString());
            string sFormName = row["FORM_NAME"].ToString();
            string sViewXML = row["VIEW_XML"].ToString();
            string sViewID = row["VIEW_ID"].ToString();

            //Load the contents of the XML document
            xdoc.LoadXml(sViewXML);

            if (sViewID == "0")
            {
                if (sFormName.IndexOf("admintracking") < 0 && sFormName.IndexOf("claimwc") < 0)
                {
                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, this.ViewConnectionString, strSelectedDSN, xdoc, false, out retAdminTables, null, CurrentMode, "", CurrentSection);
                }
                else if (sFormName.IndexOf("claimwc.xml") > -1)
                {
                    
                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, this.ViewConnectionString, strSelectedDSN, xdoc, false, out retAdminTables, objJuris, CurrentMode, "", CurrentSection);
                    bConvertWCJuris = true;
                }
                //if condition added to generate the aspx of admintracking tables
                else if (sFormName.IndexOf("admintracking.xml") > -1)
                {
                    UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, this.ViewConnectionString, strSelectedDSN, xdoc, true, out retAdminTables, null, CurrentMode, "", CurrentSection);


                    foreach (string[] arrItem in retAdminTables)
                    {
                        xdocAdmin.LoadXml(xdoc.OuterXml);

                        ((XmlElement)xdocAdmin.GetElementsByTagName("form")[0]).SetAttribute("supp", arrItem[0].ToString());
                        ((XmlNode)xdocAdmin.SelectSingleNode("//internal[@name='SysFormName']")).Attributes["value"].Value = String.Format("admintracking|{0}", arrItem[0]);

                        UTILITY.AddSuppDefinitionPowerViewUpgrade(this.LoginName, this.LoginPassword, this.ViewConnectionString, strSelectedDSN, xdocAdmin, false, out retAdminTables, null, CurrentMode, "", CurrentSection);

                        pvUpgrade.UpgradeXmlToAspx(chkReadOnly.Checked, false, xdocAdmin, sFormName.Insert(sFormName.IndexOf('.'), arrItem[0].ToString()), out convertedAspx);

                        sbAdminTablesCount.AppendFormat(String.Format("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME LIKE '%{0}%' AND DATA_SOURCE_ID = '{1}'", arrItem[0], this.DSNID));

                        bool blnParsed = false;

                        int iAdminTableRows = Conversion.CastToType<int>(DbFactory.ExecuteScalar(this.ViewConnectionString, sbAdminTablesCount.ToString()).ToString(), out blnParsed);

                        //If there are currently no records available for Admin Tables
                        if (blnParsed && iAdminTableRows <= 0)
                        {
                            dictParams.Clear();
                            dictParams.Add("XML", xdocAdmin.OuterXml);
                            dictParams.Add("ASPX", convertedAspx.ToString());
                            sbAdminTablesCount.Length = 0;

                            sbAdminTablesCount.Append(String.Format("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_CONTENT,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (0,'admintracking|{0}.xml',1,'{1}',~XML~,~ASPX~,'admintracking{0}.aspx','{2:yyyyMMddHHmmss}','{3}')", arrItem[0], arrItem[1], System.DateTime.Now, this.DSNID));

                            iUpdate = DbFactory.ExecuteNonQuery(this.ViewConnectionString, sbAdminTablesCount.ToString(), dictParams);
                        }//if
                        //If Admin Table records already exist
                        else
                        {
                            string sPageName = sFormName.Replace("xml", "aspx");
                            sbSql.AppendFormat(String.Format("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT =~ASPX~,LAST_UPDATED = '{0:yyyyMMddHHmmss}',PAGE_NAME = 'admintracking{1}.aspx' WHERE FORM_NAME = 'admintracking|{1}.xml' AND VIEW_ID = {2} AND DATA_SOURCE_ID ='{3}'", System.DateTime.Now, arrItem[0], row["VIEW_ID"], this.DSNID));

                            dictParams.Clear();
                            dictParams.Add("XML", xdocAdmin.OuterXml);
                            dictParams.Add("ASPX", convertedAspx.ToString());

                            iUpdate = DbFactory.ExecuteNonQuery(this.ViewConnectionString, sbSql.ToString(), dictParams);
                        }//else
                    }
                }
            }//if
        }//method: UpdateBaseViews()

        private static string IsBaseView(string sViewName)
        {
            if (string.IsNullOrEmpty(sViewName))
            {
                sViewName = "Base View";
            }

            return sViewName;
        }

        private void UpdateProgressBar(string sFormName, string sViewName)
        {
            lblFileName.Text = String.Format("Converting: {0} -- {1}", sViewName, sFormName);
            pBar1.PerformStep();
            this.Refresh();
            Application.DoEvents();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CreateLogs objLogs = new CreateLogs();

            try
            {
                
                frmLogin oLoginForm = new frmLogin();
                oLoginForm.ShowDialog();
                if (!oLoginForm.LoginSuccess)
                {
                    MessageBox.Show("You must login first before you can run the PowerView upgrade tool.");
                    return;
                }

                this.LoginName = oLoginForm.UserID;
                this.LoginPassword = oLoginForm.Password;
                string[] arrDb;
                m_objLogin = new Login();
                arrDb = m_objLogin.GetDatabases();
                lstDSNList.DataSource = arrDb;
            }
            catch (Exception ex)
            {
               objLogs.ErrorLog("PowerViewUpgradeLogs", ex);
               MessageBox.Show(String.Format("An Error Has Occured. Message: {0}", ex.Message));
            }
            finally
            {
                if (objLogs != null)
                    objLogs = null;
                if (m_objLogin != null)
                    m_objLogin.Dispose();
                if (m_userLogin != null)
                    m_objLogin.Dispose();
            }
        }

        
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Gets the list of PowerViews to be updated/upgraded
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        private DataTable GetPowerViews(string connectionString, string strSQL)
        {
            Dictionary<string, string> dictParms = new Dictionary<string, string>();

            DataSet ds = DbFactory.ExecuteDataSet(connectionString, strSQL, dictParms);

            return ds.Tables[0];
        } // method: GetPowerViews


        /// <summary>
        /// Handles the Background Worker process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgWorkerUpgradePowerViews_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string[] args = (string[])e.Argument;

            string strSelectedDSN = args[0];
            string strViewConnStr = args[1];

            //e.Result = UpgradeXmlToAspx(strSelectedDSN, strViewConnStr);
            
            if (Convert.ToBoolean(e.Result))
            {
                MessageBox.Show("PowerView conversion completed Successfully");
            } // if
            else
            {
                MessageBox.Show("Unable to successfully convert all PowerViews.  Please review your error log for possible upgrade error messages.");
            } // else


                
        }

        private void bgWorkerUpgradePowerViews_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            pBar1.Value = e.ProgressPercentage;
        }

        private void bgWorkerUpgradePowerViews_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            pBar1.Value = 100; //Complete the progress bar
        }

    }
}
