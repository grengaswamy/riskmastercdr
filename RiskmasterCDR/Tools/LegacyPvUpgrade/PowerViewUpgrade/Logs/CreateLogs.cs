﻿using System;
using System.Text;
using System.IO;


namespace RiskmasterTools.PowerViewUpgrade
{

    internal class CreateLogs
    {
        private string sLogFormat;
        private string sErrorTime;

        public CreateLogs()
        {
            //sLogFormat used to create log files format :
            // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
            sLogFormat = DateTime.Now.ToShortDateString().ToString()+" "+DateTime.Now.ToLongTimeString().ToString()+" ==> ";
                    
            //this variable used to create log filename format "
            //for example filename : ErrorLogYYYYMMDD
            string sYear    = DateTime.Now.Year.ToString();
            string sMonth    = DateTime.Now.Month.ToString();
            string sDay    = DateTime.Now.Day.ToString();
            sErrorTime = sYear+sMonth+sDay;
        }

        public void ErrorLog(string sPathName, string sErrMsg)
        {
            StreamWriter sw = new StreamWriter(sPathName + sErrorTime + ".log", true);
            sw.WriteLine(sLogFormat + sErrMsg);
            sw.Flush();
            sw.Close();
        }

        public void ErrorLog(string sPathName, Exception ex)
        {
            StringBuilder sbErrorMessage = new StringBuilder();
            sbErrorMessage.Append("Message: " + System.Environment.NewLine + ex.Message);

            if (ex.InnerException != null)
            {
                sbErrorMessage.Append("InnerException: " + System.Environment.NewLine + ex.InnerException.Message);
            }

            sbErrorMessage.Append("StackTrace: " + System.Environment.NewLine + ex.StackTrace);

            StreamWriter sw = new StreamWriter(sPathName + sErrorTime + ".log", true);
            sw.WriteLine(sLogFormat + sbErrorMessage.ToString());
            sw.Flush();
            sw.Close();
        }

    }
}
