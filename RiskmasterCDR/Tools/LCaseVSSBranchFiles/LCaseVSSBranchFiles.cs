using System;
using System.Xml;
using System.IO;
using System.Diagnostics;
	/// <summary>
	/// Recursively renames all file contents of a given SS branch to be lower case.
	/// Uses config information from App.config file at runtime.
	/// </summary>
namespace Riskmaster.Tools
{
	class App
	{
		static System.Configuration.AppSettingsReader m_settings = new System.Configuration.AppSettingsReader();
		const string WORK_PATH_KEY = "WorkPath";
		const string SS_DIR = "SsDir";
		const string VSS_ID_KEY = "VssId";
		const string VSS_PWD_KEY = "VssPwd";
		const string VSS_TARGET_PROJ_KEY = "VssTargetProj";
		const string VSS_EXE_KEY = "VssExe";
		const string RECURSIVE_KEY = "Recursive";
		const string LOG_KEY = "LogFile";
		
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			//Recursive VSS Get to WorkingPath
			string sParams = String.Format(@" get ""{0}"" {1} -W -I- ", 
				m_settings.GetValue(VSS_TARGET_PROJ_KEY,typeof(string)),
				((string)m_settings.GetValue(RECURSIVE_KEY,typeof(string))).ToLower() =="true"?"-R":"");
			
			
			LogMsg("*** Started LCaseVSSBranchFiles with get cmd: " + sParams);

			InvokeVSS(sParams);

			//Recursively Walk the retrieved Files (updating file names in VSS as necessary)
			DirectoryInfo objDir = new DirectoryInfo((string)m_settings.GetValue(WORK_PATH_KEY,typeof(string)));
			RecursiveWalk(objDir);
			LogMsg("*** Completed LCaseVSSBranchFiles with get cmd: " + sParams);

		}

		static private void RecursiveWalk(DirectoryInfo objDir)
		{
			bool bRecurse = ((string)m_settings.GetValue(RECURSIVE_KEY,typeof(string))).ToLower() =="true";
			
			if(bRecurse)
				foreach(DirectoryInfo objSubDir in objDir.GetDirectories()) //Depth First Traversal of Directory Tree
					RecursiveWalk(objSubDir);

			foreach(FileInfo objFile in objDir.GetFiles())
			{
				if(objFile.Name != objFile.Name.ToLower()) //Needs to be "LCased in SS"
					ApplyVSSLCase(objFile);

				//BSB 06.21.2006 Add Logging of File names with spaces
				if(objFile.Name.IndexOf(" ")>0)
					LogMsg("VSS FileName contains Space: " + objFile.Name);
			}
		}

		static private bool ApplyVSSLCase(FileInfo objFile)
		{
			string sWorkPath = (string) m_settings.GetValue(WORK_PATH_KEY,typeof(string));
			string sRootProjPath = (string) m_settings.GetValue(VSS_TARGET_PROJ_KEY,typeof(string));
			string sProjPath = objFile.DirectoryName.Replace(sWorkPath,sRootProjPath).Replace("\\","/") + "/";
			LogMsg("Attempting to apply lower case name to: " + sProjPath + objFile.Name);
			try
			{
				//TODO CALL VSS "RENAME" CMD Line API. 
				// Twice - must make a more significant change than just character case in the 
				// file name otherwise VSS will ignore the change.
				// Note: Configured VSS user MUST have "add" permissions on the Project in order to 
				// be successfull.
				string sParams = String.Format(@" rename ""{0}"" ""{1}"" -I-", 
					sProjPath + objFile.Name,
					sProjPath + "XXX-" + objFile.Name);

				//Console.WriteLine("LCase File:" + sProjPath + objFile.Name);

				//Rename To Significant Changed Name (Work-around for VSS quirk)
				if(100==InvokeVSS(sParams))
					return false;

				sParams = String.Format(@" rename ""{0}"" ""{1}"" -I-", 
					sProjPath + "XXX-" + objFile.Name,
					sProjPath + objFile.Name.Replace("XXX-","").ToLower());
				
				//Rename To Orginal File Name (Lower Cased)
				InvokeVSS(sParams);
			}
			catch{return false;}
			LogMsg("Successfully applied lower case name to: " + sProjPath + objFile.Name);
			return true;
		}
		static private int InvokeVSS(string sParams)
		{
			Process proc = new  Process();
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.EnvironmentVariables.Add("SSDIR",(string)m_settings.GetValue(SS_DIR,typeof(string)));
			proc.StartInfo.FileName ="\"" +(string)m_settings.GetValue(VSS_EXE_KEY,typeof(string)) + "\"";
			proc.StartInfo.WorkingDirectory = (string) m_settings.GetValue(WORK_PATH_KEY,typeof(string));
			Console.WriteLine(sParams);
			//Apply User Identity Params (Common to All Commands)
			proc.StartInfo.Arguments = sParams + 
			String.Format(" -Y{0},{1}",
				m_settings.GetValue(VSS_ID_KEY,typeof(string)),
				m_settings.GetValue(VSS_PWD_KEY,typeof(string)));

			proc.Start();
			proc.WaitForExit();
			return proc.ExitCode;

		}
		static void LogMsg(string sMsg)
		{
			string sLogFile = (string)m_settings.GetValue(LOG_KEY,typeof(string));
			System.IO.StreamWriter stm = new StreamWriter(sLogFile,true);
			stm.WriteLine(sMsg);
			stm.Flush();
			stm.Close();

		}
	}
}