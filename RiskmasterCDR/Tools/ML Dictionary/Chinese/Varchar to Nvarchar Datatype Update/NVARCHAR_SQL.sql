--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: SEARCH_VIEW
--COLUMN:VIEW_NAME 
--DATABASE: Main database in the corresponding connection string should be used
--USE [RMACDR_ACOFF_ML]--Main database in the corresponding connection string should be used
GO

/****** Object:  Index [IDX_SV_VIEW_NAME]    Script Date: 10/28/2014 16:36:28 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SEARCH_VIEW]') AND name = N'IDX_SV_VIEW_NAME')
DROP INDEX [IDX_SV_VIEW_NAME] ON [dbo].[SEARCH_VIEW] WITH ( ONLINE = OFF )
GO

ALTER TABLE SEARCH_VIEW ALTER COLUMN VIEW_NAME NVARCHAR(25)

--USE [RMACDR_ACOFF_ML]--Main database in the corresponding connection string should be used
GO

/****** Object:  Index [IDX_SV_VIEW_NAME]    Script Date: 10/28/2014 16:36:31 ******/
CREATE NONCLUSTERED INDEX [IDX_SV_VIEW_NAME] ON [dbo].[SEARCH_VIEW] 
(
	[VIEW_NAME] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: SEARCH_VIEW
--COLUMN:VIEW_DESC 
--DATABASE: Main database in the corresponding connection string should be used
ALTER TABLE SEARCH_VIEW ALTER COLUMN VIEW_DESC NVARCHAR(250)

--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: MDI_MENU
--COLUMN: MDIMENU_XML
--DATABASE: VIEW DATABASE in the corresponding connection string should be used
ALTER TABLE MDI_MENU ALTER COLUMN MDIMENU_XML nvarchar(max)
