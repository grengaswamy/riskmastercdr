

--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: MDI_MENU
--COLUMN: MDIMENU_XML
--DATABASE: VIEW DATABASE in the corresponding connection string should be used
ALTER TABLE MDI_MENU ALTER COLUMN MDIMENU_XML nvarchar(max)

ALTER TABLE MDI_MENU ALTER COLUMN CHILDSCREEN_XML nvarchar(max)
/*TABLE: NET_VIEW_FORMS
DATABASE : VIEW Database */

--VIEW_XML
ALTER TABLE NET_VIEW_FORMS ALTER COLUMN VIEW_XML [varchar](max) NULL;
ALTER TABLE NET_VIEW_FORMS REBUILD;
ALTER TABLE NET_VIEW_FORMS ALTER COLUMN VIEW_XML [ntext]  NULL; 

/*TABLE: NET_VIEW_FORMS */
--PAGE_CONTENT
ALTER TABLE NET_VIEW_FORMS ALTER COLUMN PAGE_CONTENT [varchar](max) NULL;
ALTER TABLE NET_VIEW_FORMS REBUILD;
ALTER TABLE NET_VIEW_FORMS ALTER COLUMN PAGE_CONTENT [ntext]  NULL;

--VIEW_NAME
ALTER TABLE NET_VIEWS ALTER COLUMN VIEW_NAME nvarchar(50)
--VIEW_DESC
ALTER TABLE NET_VIEWS ALTER COLUMN VIEW_DESC nvarchar(250)

--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: LOCAL_RESOURCE
--COLUMN: RESOURCE_VALUE
--DATABASE: VIEW DATABASE in the corresponding connection string should be used
ALTER TABLE LOCAL_RESOURCE ALTER COLUMN RESOURCE_VALUE nvarchar(max)


--------------------------------------------------------------------------------------------------------------
--************************************************************************************************************
--TABLE: GLOBAL_RESOURCE
--COLUMN: RESOURCE_VALUE
--DATABASE: VIEW DATABASE in the corresponding connection string should be used
ALTER TABLE GLOBAL_RESOURCE ALTER COLUMN RESOURCE_VALUE nvarchar(max)
