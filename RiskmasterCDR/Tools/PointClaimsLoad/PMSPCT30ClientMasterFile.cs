﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCT30ClientMasterFile
    {
        public long PMSPCT30Client;
        public string PMSPCT30Ct30type;
        public string PMSPCT30ClientType;
        public string PMSPCT30LName;
        public string PMSPCT30FName;
        public string PMSPCT30MName;
        public string PMSPCT30SirName;
        public string PMSPCT30Address1;
        public string PMSPCT30Address2;
        public string PMSPCT30City;
        public string PMSPCT30State;
        public string PMSPCT30ZipCode;
        public string PMSPCT30Sex;
        public string PMSPCT30Age;
        public string PMSPCT30SSN;
        public string PMSPCT30Contact;
        public string PMSPCT30Phone;

        public string PMSPCT30TaxId;
        //Used for Entity updates to AC only
        //SI05603
        public long EntityID;
        //SI06356
        public string EntityType;

        public PMSPCT30ClientMasterFile()
        {
            Clear();
        }

        public void Clear()
        {
            PMSPCT30Client = 0;
            PMSPCT30Ct30type = "";
            PMSPCT30ClientType = "";
            PMSPCT30LName = "";
            PMSPCT30FName = "";
            PMSPCT30MName = "";
            PMSPCT30SirName = "";
            PMSPCT30Address1 = "";
            PMSPCT30Address2 = "";
            PMSPCT30City = "";
            PMSPCT30State = "";
            PMSPCT30ZipCode = "";
            PMSPCT30Sex = "";
            PMSPCT30Age = "";
            PMSPCT30SSN = "";
            PMSPCT30Contact = "";
            PMSPCT30Phone = "";
            PMSPCT30TaxId = "";
            //SI05603
            EntityID = 0;
            EntityType = "";
        }
        public PMSPCT30ClientMasterFile Clone()
        {
            PMSPCT30ClientMasterFile functionReturnValue = default(PMSPCT30ClientMasterFile);
            PMSPCT30ClientMasterFile obj = default(PMSPCT30ClientMasterFile);
            obj = new PMSPCT30ClientMasterFile();
            {
                obj.EntityID = EntityID;
                obj.PMSPCT30Address1 = PMSPCT30Address1;
                obj.PMSPCT30Address2 = PMSPCT30Address2;
                obj.PMSPCT30Age = PMSPCT30Age;
                obj.PMSPCT30City = PMSPCT30City;
                obj.PMSPCT30Client = PMSPCT30Client;
                obj.PMSPCT30ClientType = PMSPCT30ClientType;
                obj.PMSPCT30Contact = PMSPCT30Contact;
                obj.PMSPCT30Ct30type = PMSPCT30Ct30type;
                obj.PMSPCT30FName = PMSPCT30FName;
                obj.PMSPCT30LName = PMSPCT30LName;
                obj.PMSPCT30MName = PMSPCT30MName;
                obj.PMSPCT30Phone = PMSPCT30Phone;
                obj.PMSPCT30Sex = PMSPCT30Sex;
                obj.PMSPCT30SirName = PMSPCT30SirName;
                obj.PMSPCT30SSN = PMSPCT30SSN;
                obj.PMSPCT30State = PMSPCT30State;
                obj.PMSPCT30TaxId = PMSPCT30TaxId;
                obj.PMSPCT30ZipCode = PMSPCT30ZipCode;
                obj.EntityType = EntityType;
                //SI06356
            }
            functionReturnValue = obj;
            obj = null;
            return functionReturnValue;
        }

    }
}
