﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPAD00AdjustorMasterFile
    {
        public long PMSPAD00EntityID;
        public string PMSPAD00AdjustorNbr;
        public string PMSPAD00LongTaxID;
        public string PMSPAD00TaxIdSSN;
        public string PMSPAD00Name;
        public string PMSPAD00Address;
        public string PMSPAD00CityState;
        public string PMSPAD00ZipCode;
        public string PMSPAD00PhoneNumber;
        public string PMSPAD00TypeAdjustor;
        public string PMSPAD00ClaimsOfficeNumber;
        public string PMSPAD00StatusCode;
        public string PMSPAD00UserProfileCode;
        public long PMSPAD00DateStamp;
        public string PMSPAD00SSN;
        public string NumberType;
        public string EntityType;

        public PMSPAD00AdjustorMasterFile()
        {
            Clear();

        }

        public void Clear()
        {
            PMSPAD00EntityID = 0;
            PMSPAD00AdjustorNbr = "";
            PMSPAD00LongTaxID = "";
            PMSPAD00TaxIdSSN = "";
            PMSPAD00Name = "";
            PMSPAD00Address = "";
            PMSPAD00CityState = "";
            PMSPAD00ZipCode = "";
            PMSPAD00PhoneNumber = "";
            PMSPAD00TypeAdjustor = "";
            PMSPAD00ClaimsOfficeNumber = "";
            PMSPAD00StatusCode = "";
            PMSPAD00UserProfileCode = "CLAIMSPRO";
            PMSPAD00DateStamp = 0;
            PMSPAD00SSN = "";
            NumberType = "";
            //SI06356
            EntityType = "";
        }
        public PMSPAD00AdjustorMasterFile Clone()
        {
               PMSPAD00AdjustorMasterFile obj = new PMSPAD00AdjustorMasterFile();
               obj.PMSPAD00EntityID = PMSPAD00EntityID;
               obj.PMSPAD00AdjustorNbr = PMSPAD00AdjustorNbr;
               obj.PMSPAD00LongTaxID = PMSPAD00LongTaxID;
               obj.PMSPAD00TaxIdSSN = PMSPAD00TaxIdSSN;
               obj.PMSPAD00Name = PMSPAD00Name;
               obj.PMSPAD00Address = PMSPAD00Address;
               obj.PMSPAD00CityState = PMSPAD00CityState;
               obj.PMSPAD00ZipCode = PMSPAD00ZipCode;
               obj.PMSPAD00PhoneNumber = PMSPAD00PhoneNumber;
               obj.PMSPAD00TypeAdjustor = PMSPAD00TypeAdjustor;
               obj.PMSPAD00ClaimsOfficeNumber = PMSPAD00ClaimsOfficeNumber;
               obj.PMSPAD00StatusCode = PMSPAD00StatusCode;
               obj.PMSPAD00UserProfileCode = PMSPAD00UserProfileCode;
               obj.PMSPAD00DateStamp = PMSPAD00DateStamp;
               obj.PMSPAD00SSN = PMSPAD00SSN;
               obj.NumberType = NumberType;
               obj.EntityType = EntityType;

               return obj;
        }

    }
}
