﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class BASCLT0300
    {
        public long CLTSEQNUM;
        public long ADDRSEQNUM;
        public string ADDRSTATUS;
        public string ADDRTYPE;
        public string ADDRLN1;
        public string ADDRLN2;
        public string ADDRLN3;
        public string ADDRLN4;
        public string CITY;
        public string STATE;
        public string ZIPCODE;
        public string COUNTRY;
        public string COUNTY;
        public System.DateTime EFFDATE;
        public System.DateTime RECDATE;
        public System.DateTime RECTIME;


        public BASCLT0300()
        {
            Clear();
        }

        public void Clear()
        {
            string sTemp = null;

            CLTSEQNUM = 0;
            ADDRSEQNUM = 0;
            ADDRSTATUS = "";
            ADDRTYPE = "";
            ADDRLN1 = Globals.conNoAddress;
            ADDRLN2 = "";
            ADDRLN3 = "";
            ADDRLN4 = "";
            CITY = Globals.conNoCity;
            STATE = Globals.conNoStateCode;
            ZIPCODE = Globals.conNoPostalCode;
            COUNTRY = "USA";
            //SI06495 COUNTY = ""
            //COUNTY = Globals.conNoCountry;
            COUNTY = "";
            //SI06495
           // EFFDATE = System.DateTime;
           // RECDATE = System.DateTime;
           // RECTIME = Time;

        }
    }
}
