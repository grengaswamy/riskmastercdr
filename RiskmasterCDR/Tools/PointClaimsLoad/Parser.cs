﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
using CCP.XmlFormatting;
using CCP.LossBalance;
using RC = Riskmaster.Common;

namespace CCP.PointClaimsLoad
{
    public class Parser
    {
        #region private variables
        private static string sDateEntered;
        private static string strDescription;
        private static string sSql;
        private static string sClaimType;
        private static string sInsLine;
        private static string sReserveType;
        private static string sMember;
        private static string sLossCode;
        private static string sReserveCategory;
        private static string sLegacyCoverageKey;
        private static string sUnitNumber;
        private static string sCovSeq;
        private static string sTransSeq;
        private static string sTransType;
        private static string sActivityType;
        private static int iPayeeNo;
        private static string sInternalCat;
        private static bool bEmployeeEntityFound;
        private static string sAccidentState;
        private static string sTemp;
        private static CommonFunctions objCfunctions;
        #endregion

        //Constructor
        static Parser()
        {
            objCfunctions = new CommonFunctions();
            sDateEntered = string.Empty;
            strDescription = string.Empty;
            sSql = string.Empty;
            sClaimType = string.Empty;
            sInsLine = string.Empty;
            sReserveType = string.Empty;
            sMember = string.Empty;
            sLossCode = string.Empty;
            sReserveCategory = string.Empty;
            sLegacyCoverageKey = string.Empty;
            sUnitNumber = string.Empty;
            sCovSeq = string.Empty;
            sTransSeq = string.Empty;
            sTransType = string.Empty;
            sActivityType = string.Empty;
            iPayeeNo = 0;
            sInternalCat = string.Empty;
            bEmployeeEntityFound = false;
            sAccidentState = string.Empty;
            sTemp = string.Empty;
        }

        public static void UploadParser()
        {
            Parameters objParams = (Parameters)Globals.GetParams();
            FileFunctions objFile = Globals.CFleFnc();
            Errors objErrors = new Errors();
            try
            {
                Globals.dbg.PushProc("UploadParser");
                Globals.dbg.DebugTrace(objParams.InputPath, objParams.XMLFile, "", "", "");

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlDoc, ScriptingFunctions.conScpEvtStart);
                if (!objFile.FileExists(objParams.InputPath, objParams.XMLFile))
                {
                    Globals.bContinue = false;
                }
                Globals.XMLObj().XMLLoadDocument(objFile.JoinFileName(objParams.InputPath, objParams.XMLFile));
                ParseXML();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlDoc, ScriptingFunctions.conScpEvtEnd);
                Globals.dbg.PopProc();
            }
        }

        private static void ParseXML()
        {
            int iSuccess = 0;
            Globals.dbg.PushProc("ParseXML", "", "", "", "");
            Globals.objLossBal = new CCP.LossBalance.LossBalance();
            Parameters objParams = (Parameters)Globals.GetParams();
            try
            {
                CCP.LossBalance.LossBalance.Instance.DoClaimTotal = true;
                CCP.LossBalance.LossBalance.Instance.DoResvTypeTotal = false;
                CCP.LossBalance.LossBalance.Instance.BalanceLossXML(Globals.XMLObj());
            }
            catch (Exception ex) { throw (ex); }
            finally
            {
                Globals.dbg.PopProc();
            }
        }

        public static void ParseClaimDocXML()
        {
            Parameters objParams = Globals.GetParams();
            bool rst = false;
            try
            {
                Globals.dbg.PushProc("ParseClaimDocXML");

                Globals.XMLClaim = new CCP.XmlComponents.XMLClaim();

                Globals.XMLClaim = CCP.LossBalance.LossBalance.Instance.MInstance.XMLClaim;
                Globals.XMLClaim.XML = Globals.XMLObj();

                // Perform initialization routines
                InitializeParsing();
                ParseVendorEntities();


                 
                //Globals.objLogWriter.Write("Timestamp : " + DateTime.Now.ToString() + Environment.NewLine);
                //Globals.objLogWriter.Write("Point policy system : " + Globals.objConfig.ConfigElement.Name + Environment.NewLine);
                //Globals.objLogWriter.Write("RM database : " + Globals.GetParams().Database + Environment.NewLine);
                //Globals.objLogWriter.Write("Upload XML File : " + Globals.GetParams().XMLFile + Environment.NewLine);
                Globals.dbConnection = Riskmaster.Db.DbFactory.GetDbConnection(Globals.objConfig.ConfigElement.DataDSNConnStr);

                //Opening the connection to the point database.
                Globals.dbConnection.Open();
                Globals.objWriter = Riskmaster.Db.DbFactory.GetDbWriter(Globals.dbConnection);// (Globals.objConfig.ConfigElement.DataDSNConnStr);
                Globals.objLogWriter.Write(Environment.NewLine + "Opened connection to the point database. " + Environment.NewLine);

                rst = true;
                while ((Globals.XMLClaim.getClaim(null, rst) != null))
                {
                    rst = false;
                    ParseClaim();
                }

                if (objParams.IsSwitch(Globals.SWStartClaim))
                {
                    return;
                }

                LoadDatabase.UpdateAccountDates();

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

                Globals.objLogWriter.Write(Environment.NewLine + "Closing connection to the point database. " + Environment.NewLine);
                Globals.dbg.PopProc();
            }
        }

        private static void ParseUnitStat()
        {
            CCP.XmlComponents.XMLUnitStats xmlUnitStats = null;
            //note: Any data pulled from Injury needs to be loaded in the ParseEmployee routine.
            try
            {
                Globals.dbg.PushProc("ParseUnitStat");

                xmlUnitStats = Globals.XMLClaim.UnitStats;

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlWC, ScriptingFunctions.conScpEvtStart, xmlUnitStats);

                if (Globals.XMLClaim.UnitStats.Node == null)
                {
                    return;
                }
                Globals.bWCompDataProcessed = true;
                Globals.lClmUnitStatCount = 1;

                Globals.objPCL14.PMSPCL14Claim = Globals.objPCL20.PMSPCL20Claim;
                Globals.objPCL14.PMSPCL14LossEntDteMo = StringUtils.Mid(sDateEntered, 4, 2);
                Globals.objPCL14.PMSPCL14LossEntDteDy = StringUtils.Right(sDateEntered, 2);
                Globals.objPCL14.PMSPCL14LossEntDteYr = StringUtils.Left(sDateEntered, 3);

                Globals.objPCL14.PMSPCL14HistDate = Globals.objUtils.ConvertDateToPoint(DateTime.Now.ToString("yyyyMMdd"));

                Globals.objPCL14.PMSPCL14DateRepEmp = Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlUnitStats.EmployerNotifiedDate)));
                Globals.objPCL14.PMSPCL14Deductible = BoolToYN(objCfunctions.CnvData(xmlUnitStats.DeductibleReimbursement, CommonFunctions.ufDataTypes.ufdtBool));
                Globals.objPCL14.PMSPCL14Controvert = BoolToYN(objCfunctions.CnvData(xmlUnitStats.DisputedCase, CommonFunctions.ufDataTypes.ufdtBool));
                Globals.objPCL14.PMSPCL14InjuryZip = Globals.XMLClaim.LossLocZip;
                //dbisht6 RMA-10309
                //if (!string.IsNullOrEmpty(xmlUnitStats.FraudulentClaimInd_Code))
                //{
                //    Globals.objPCL14.PMSPCL14FraudClaim = StringUtils.Right(xmlUnitStats.FraudulentClaimInd_Code, 1);
                //}
                //else
                //{
                //    Globals.objPCL14.PMSPCL14FraudClaim = BoolToYN(objCfunctions.CnvData(xmlUnitStats.PossibleFraud, CommonFunctions.ufDataTypes.ufdtBool));
                //}
                //dbisht6 end
                Globals.objPCL14.PMSPCL14TypeRecv = xmlUnitStats.TypeRecovery_Code;
                Globals.objPCL14.PMSPCL14TypeCovg = xmlUnitStats.TypeCoverage_Code;
                Globals.objPCL14.PMSPCL14TypeLoss = xmlUnitStats.TypeLoss_Code;
                Globals.objPCL14.PMSPCL14Act = xmlUnitStats.LossAct_Code;
                Globals.objPCL14.PMSPCL14TypeSetmt = xmlUnitStats.SettlementType_Code;
                Globals.objPCL14.PMSPCL14DeductCode = xmlUnitStats.Deductible_Code;
                Globals.objPCL14.PMSPCL14LsStJuris = TranslateStateCode(xmlUnitStats.JurisdictionState_Code);
                Globals.objPCL20.PMSPCL20CatCode = xmlUnitStats.CatastropheNumber;
                Globals.objPCL14.PMSPCL14InjCode = xmlUnitStats.BenefitType_Code;
                Globals.objPCL14.PMSPCL14EmpPayInd = xmlUnitStats.EmployerWageRange_Code;
            }
            catch (Exception ex) { throw (ex); }
            finally
            {

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlWC, ScriptingFunctions.conScpEvtEnd, xmlUnitStats);
                xmlUnitStats = null;
                Globals.dbg.PopProc();
            }
        }

        private static void ParseClaim()
        {
            bool rst = false;
            string sTemp = null;
            long iClaimId = 0;
            long iExemptCLaimId = 0;
            Parameters objParams = Globals.GetParams();
            Globals.bupdateDefaultDOB = false;

            try
            {
                //Transaction started for a claim node in XML doc : Only when it is a sql database.
                Globals.dbTrans = null;

                //if (Globals.objConfig.ConfigElement.DataDSNConnStr.Contains("iSeries Access ODBC Driver"))
                //    {
                //    Globals.dbTrans = null;
                //    }
                //else
                //    {
                //     // Globals.objLogWriter.Write(Environment.NewLine + "*************** Transaction started. " + Environment.NewLine);
                //     // Globals.dbTrans = Globals.dbConnection.BeginTransaction(System.Data.IsolationLevel);
                //    }


                Globals.objLogWriter.Write(Environment.NewLine + "*************** Loading information for claim number : " + Globals.XMLClaim.ClaimNumber + Environment.NewLine);
                Globals.dbg.PushProc("ParseClaim");

                //if (objGetParams.IsTrue("/" + Globals.conTransEnabled))
                //{
                //Globals.dbTrans =   Globals.dbConnection.BeginTransaction();
                //}
                Globals.cClmPaidAmount = 0;
                Globals.cClmReserveAmount = 0;
                Globals.cClmReserveChange = 0;
                Globals.lClmTransactionCount = 0;
                Globals.lClmOffsetOnsetCount = 0;
                Globals.lClmUnitStatCount = 0;

                Globals.bNewClaim = false;
                Globals.bNewClaimant = false;
                Globals.bNewReserve = false;
                Globals.bWCompDataProcessed = false;
                bEmployeeEntityFound = false;

                Globals.bIsNewClaimUpload = false;

                Globals.InitAddressWork();
                Globals.InitForClaim(Globals.objPCL20);
                Globals.InitForUnitStat(Globals.objPCL14);

                iClaimId = Convert.ToInt64(Globals.XMLClaim.ClaimID);

                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExemptCLaimId))
                {
                    iExemptCLaimId = Convert.ToInt64(Globals.objConfig.ConfigElement.ExemptCLaimId);
                }
                if (iExemptCLaimId < iClaimId)
                {
                    Globals.bIsNewClaimUpload = true;
                }
                else
                {
                    Globals.bIsNewClaimUpload = false;
                }

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlClm, ScriptingFunctions.conScpEvtStart);
                System.Timers.Timer t1 = new System.Timers.Timer();
                t1.Start();
                TimeSpan sinceMidnight = DateTime.Now - DateTime.Today;
                double secs = sinceMidnight.TotalSeconds;


                //Sets the date and time stamp for subsequent loads
                Globals.sYYYYMMDDNow = string.Format("{0:yyyyMMdd}", DateTime.Now);
                Globals.sHHNNSS000000Now = String.Format("{0:HHmmss}", DateTime.Now) + StringUtils.Right(string.Format("{0:#0.00}", secs), 2);

                Globals.objPCL20.PMSPCL20Claim = Globals.XMLClaim.ClaimNumber.PadLeft(12, '0');

                Globals.sOrigClaimNumber = Globals.XMLClaim.ClaimNumber;
                Globals.sClaimNumber = Globals.objPCL20.PMSPCL20Claim;
                Globals.dbg.LogEntry("Start Claim " + Globals.sOrigClaimNumber);
                if (objParams.IsSwitch(Globals.SWStartClaim))
                {
                    if (objParams.Parameter(Globals.SWStartClaim) == Globals.objPCL20.PMSPCL20Claim)
                    {
                        objParams.RemoveParameter(Globals.SWStartClaim);
                    }
                    else
                    {
                        return;
                    }
                }

                Globals.objPCL20.PMSPCL20ClmDesc = Globals.XMLClaim.AccidentDescription;
                Globals.objPCL20.PMSPCL20DescCode = Globals.XMLClaim.AccidentDescription_Code;
                Globals.objPCL20.PMSPCL20ClaimState = Globals.XMLClaim.AccidentState_Code;

                if (string.IsNullOrEmpty(sTemp))
                    sTemp = Globals.XMLClaim.ClaimStatus_Code;
                Globals.objPCL20.PMSPCL20RecStatus = sTemp;

                Globals.objPCL20.PMSPCL20LossDte = string.IsNullOrEmpty(Globals.XMLClaim.DateOfLoss) ? 0 : Convert.ToInt64("0" + Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(Globals.XMLClaim.DateOfLoss))));
                Globals.objPCL20.PMSPCL20LossTime = string.IsNullOrEmpty(Globals.XMLClaim.TimeOfLoss) ? string.Empty : Convert.ToString(string.Format("{0:hh:mm:ss}", Globals.GetDateTime(Globals.XMLClaim.TimeOfLoss)));
                Globals.objPCL20.PMSPCL20RptDte = string.IsNullOrEmpty(Globals.XMLClaim.DateReported) ? 0 : Convert.ToInt32("0" + Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(Globals.XMLClaim.DateReported))));
                Globals.objPCL20.PMSPCL20RptTime = string.IsNullOrEmpty(Globals.XMLClaim.TimeReported) ? string.Empty : Convert.ToString(string.Format("{0:hh:mm:ss}", Globals.GetDateTime(Globals.XMLClaim.TimeReported)));
                sDateEntered = string.IsNullOrEmpty(Globals.XMLClaim.DateOfLoss) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(Globals.XMLClaim.DateOfLoss)));
                sInternalCat = Globals.XMLClaim.ClaimCatastrophe;
                Globals.objPCL20.PMSPCL20CatCode = Globals.XMLClaim.ClaimCatastrophe;
                Globals.dbg.LogEntry("GetClaimType called ");
                Globals.dbg.LogEntry(Globals.XMLClaim.ClaimType_Code);
                sClaimType = GetClaimType(Globals.XMLClaim.ClaimType_Code, Globals.XMLClaim.LOB_Code);
                Globals.dbg.LogEntry(sClaimType);
                Globals.objPCL20.PMSPCL20ClaimType = sClaimType;

                if (objParams.Parameter(Globals.conADJAssignLevel) == Globals.conADJAssignLevelClaim + "." + Globals.conADJAssignExaminer)
                {
                    if ((Globals.XMLClaim.PartyInvolved.findpartyInvolvedbyRole(Globals.sADJRole) != null))
                    {
                        if ((Globals.XMLClaim.PartyInvolved.Entity.findIDNumberByType(objParams.Parameter(Globals.conExmrLit)) != null))
                        {
                            Globals.objPCL20.PMSPCL20ExaminerCd = Globals.XMLClaim.PartyInvolved.Entity.IDNumber;
                        }
                    }
                }

                Globals.sNatureOfInjury = "";
                Globals.sCauseOfInjury = "";
                Globals.sBodyPart = "";
                Globals.bUpdateAIA = false;
                ParseUnitStat();
                ParseEmployee();
                Globals.ExaminerCd = Globals.XMLClaim.ExaminerCd;
                Globals.objPCL20.PMSPCL20ExaminerCd = Globals.XMLClaim.ExaminerCd;
                Globals.objPCL20.PMSPCL20Examiner = Globals.XMLClaim.Examiner;


                Globals.dbg.LogEntry("uploading claim");
                if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
                {
                    if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                    {
                        Globals.objPCL20.PMSPCL20ExaminerCd = Globals.objConfig.ConfigElement.ExaminerCD;
                    }
                }
                if (Globals.objConfig.ConfigElement.AddDefaultExaminer == "YES")
                {
                    if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.Examiner))
                    {
                        Globals.objPCL20.PMSPCL20Examiner = Globals.objConfig.ConfigElement.Examiner;
                    }
                }
                if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
                {
                    if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                    {
                        Globals.objPCL20.PMSPCL20ClmOffice = Globals.objConfig.ConfigElement.ClmOffice;
                    }
                }
                Globals.bProcessOffsetOnset = false;
                if (!string.IsNullOrEmpty(Globals.XMLClaim.NewClaimNumber))
                {
                    Globals.sNewClaimNumber = Globals.XMLClaim.NewClaimNumber.PadLeft(12, '0');
                    Globals.lNewLossDate = Globals.objPCL20.PMSPCL20LossDte;
                    Globals.sNewCatCode = Globals.objPCL20.PMSPCL20CatCode;
                    Globals.bProcessOffsetOnset = true;
                    Globals.bOffsetOnsetLossDetail = false;
                    rst = true;
                    while ((Globals.XMLClaim.LossDetail.getLossDetail(null, rst) != null))
                    {
                        rst = false;
                        if (Globals.XMLClaim.LossDetail.OffOnSet_Code == "OFFSET")
                        {
                            Globals.bOffsetOnsetLossDetail = true;
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                    }
                    OffSetOnset.ProcessOffsetOnset();
                    Globals.sClaimNumber = Globals.sNewClaimNumber;
                    Globals.bOffsetOnsetLossDetail = false;
                    Globals.lClmOffsetOnsetCount = 1;
                }
                else
                {
                    LoadDatabase.LocateClaim();
                }

                if (Globals.bWCompDataProcessed == true)
                {
                    if (bEmployeeEntityFound == true)
                    {
                        CheckEmployeeClient();
                    }
                    LoadDatabase.LocateUnitStat();
                }

                //Process Loss Details
                rst = true;
                while ((Globals.XMLClaim.LossDetail.getLossDetail(null, rst) != null))
                {
                    rst = false;
                    ParseLossDetail(Globals.XMLClaim.LossDetail);
                }

                //BalanceClaim
                LoadDatabase.UpdateAccountDates();

                if (!string.IsNullOrEmpty(Globals.sActivitiesUploaded))
                {
                    Globals.sActivitiesUploaded = Globals.sActivitiesUploaded + "," + Globals.XMLClaim.ActivityId;
                }
                else
                {
                    Globals.sActivitiesUploaded = Globals.XMLClaim.ActivityId;
                }
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlClm, ScriptingFunctions.conScpEvtEnd);

                LoadDatabase.UpdateClaimantPMSPCL30(Globals.objPCL20.PMSPCL20Claim, Globals.objPCL20.PMSPCL20RecStatus);
                Globals.dbg.LogEntry("End Claim   " + Globals.sOrigClaimNumber);
                Globals.objLogWriter.Write(Environment.NewLine + " ******************* Information loaded for the current node. " + Environment.NewLine);
                //if (Globals.dbTrans != null)
                //{
                //    Globals.dbTrans.Commit();
                //    Globals.objLogWriter.Write(Environment.NewLine + "*************** Transaction committed. " + Environment.NewLine);
                //}
            }
            catch (Exception ex)
            {
                Globals.objLogWriter.Write("Error Mesasage : " + ex.Message + Environment.NewLine);
                Globals.objLogWriter.Write("Error Description : " + ex.InnerException + Environment.NewLine);
                Globals.objLogWriter.Write("Error Source : " + ex.Source + Environment.NewLine);
                Globals.objLogWriter.Write("Error Method : " + ex.TargetSite + Environment.NewLine);
                Globals.objLogWriter.Write("Error Trace : " + ex.StackTrace + Environment.NewLine);

                Globals.objLogWriter.Write(Environment.NewLine + " xxxxxxxxx Above claim node loaded with errors. xxxxxxxxx" + Environment.NewLine);
                //if (Globals.dbTrans != null)
                //{
                //    Globals.dbTrans.Rollback();
                //    Globals.objLogWriter.Write(Environment.NewLine + "*************** Transaction rolledback. " + Environment.NewLine);
                //}

            }
            finally
            {
                Globals.dbg.PopProc();
            }


        }

        private static void ParseEmployee()
        {
            string sEmployerEntity = string.Empty;
            string sHiredDte = null;
            CCP.XmlComponents.XMLEmployee xmlEmployee = null;
            CCP.XmlComponents.XMLInjury xmlInjury = null;
            CCP.XmlComponents.XMLEntity xmlEntity = null;
            bool rst = false;
            try
            {
                Globals.dbg.PushProc("ParseEmployee");
                Globals.XMLClaim.UnitStats.XML = Globals.XMLClaim.XML;

                //If the UnitStat node does not exists, referencing the employee node will cause a 91 error
                if (Globals.XMLClaim.UnitStats.Node == null)
                {
                    return;
                }

                xmlEmployee = Globals.XMLClaim.UnitStats.Employee;
                xmlEmployee.XML = Globals.XMLClaim.XML;
                xmlInjury = Globals.XMLClaim.Injury;
                xmlInjury.getInjury(true);

                if (xmlEmployee.Node == null)
                {

                    return;
                }
                Globals.bWCompDataProcessed = true;
                Globals.lClmUnitStatCount = 1;
                sEmployerEntity = "";
                Globals.dbg.LogEntry("ParseEmployee");

                Globals.objPCL14.PMSPCL14Claim = Globals.sClaimNumber;
                Globals.objPCL14.PMSPCL14LossEntDteMo = StringUtils.Mid(sDateEntered, 4, 2);
                Globals.objPCL14.PMSPCL14LossEntDteDy = StringUtils.Right(sDateEntered, 2);
                Globals.objPCL14.PMSPCL14LossEntDteYr = StringUtils.Left(sDateEntered, 3);

                Globals.objPCL14.PMSPCL14HistDate = Globals.objUtils.ConvertDateToPoint(DateTime.Now.ToString("yyyyMMdd"));

                if (!string.IsNullOrEmpty(xmlEmployee.ClaimantNumber))
                {
                    Globals.objPCL14.PMSPCL14ClmtSeq = Convert.ToInt32(xmlEmployee.ClaimantNumber);
                }
                else
                {
                    Globals.objPCL14.PMSPCL14ClmtSeq = 1;
                }
                Globals.sCLMTSEQ = Globals.objPCL14.PMSPCL14ClmtSeq;
                Globals.dbg.LogEntry("ParseEmployee objPCL14.PMSPCL14ClmtSeq ");
                Globals.dbg.LogEntry(Convert.ToString(Globals.objPCL14.PMSPCL14ClmtSeq));

                xmlEmployee.EmployeeEntity.XML = Globals.XMLClaim.XML; // bsharma33
                //caggarwal4 Merged for RMA 9586
                xmlEmployee.EmployeeEntity.Parent.XML = Globals.XMLClaim.XML;  //bpaskova JIRA 9586 // bsharma33 : RMA-12757. Address for employee uploading with zz values.
                ParseEntityNode(xmlEmployee.EmployeeEntity);
                MapEmployeeEntity();

                if (!string.IsNullOrEmpty(xmlInjury.NatureOfInjury_Code))
                {
                    Globals.bUpdateAIA = true;
                    Globals.sNatureOfInjury = xmlInjury.NatureOfInjury_Code;
                }
                if (!string.IsNullOrEmpty(xmlInjury.CauseOfInjury_Code))
                {
                    Globals.bUpdateAIA = true;
                    Globals.sCauseOfInjury = xmlInjury.CauseOfInjury_Code;
                }
                if (xmlInjury.BodyPartCount > 0)
                {
                    xmlInjury.getBodyPart(true);
                    Globals.bUpdateAIA = true;
                    Globals.sBodyPart = xmlInjury.BodyPart_Code;
                }

                if ((Globals.XMLClaim.Policy.getPolicy(true) != null))
                {
                    if ((Globals.XMLClaim.Policy.PartyInvolved.findpartyInvolvedbyRole(Globals.sPHLDRole) != null))
                    {
                        xmlEntity = Globals.XMLClaim.Policy.PartyInvolved.Entity;
                    }
                    else if ((xmlEmployee.PartyInvolved.findpartyInvolvedbyRole(Globals.sPHLDRole) != null))
                    {
                        xmlEntity = xmlEmployee.PartyInvolved.Entity;
                    }
                    else if ((Globals.XMLClaim.PartyInvolved.findpartyInvolvedbyRole(Globals.sPHLDRole) != null))
                    {
                        xmlEntity = Globals.XMLClaim.PartyInvolved.Entity;
                    }
                    else
                    {
                        xmlEntity = xmlEmployee.EmployerEntity;
                    }
                }
                //dbisht6 RMA-10309
                //if ((xmlEntity != null))
                //{
                //    if (Convert.ToInt32(xmlEntity.EntityID) != 0)
                //    {
                //        Globals.objPCL14.PMSPCL14EmplrSic = xmlEntity.SIC;
                //        Globals.objPCL14.PMSPCL14Fein = StringUtils.Trim(xmlEntity.TaxId);
                //        Globals.objPCL14.PMSPCL14Fein = Globals.objPCL14.PMSPCL14Fein.Replace(" ", "");
                //        Globals.objPCL14.PMSPCL14Fein = Globals.objPCL14.PMSPCL14Fein.Replace("-", "");
                //        Globals.objPCL14.PMSPCL14Fein = Globals.objPCL14.PMSPCL14Fein.Replace(".", "");
                //    }
                //}
                //dbisht6 end
                Globals.objPCL14.PMSPCL14Fein = xmlEmployee.EmpFein;


                sHiredDte = string.IsNullOrEmpty(xmlEmployee.HiredDate) ? string.Empty : string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlEmployee.HiredDate));
                if (!string.IsNullOrEmpty(sHiredDte))
                {
                    Globals.objPCL14.PMSPCL14HireDteYr = StringUtils.Left(sHiredDte, 4);
                    Globals.objPCL14.PMSPCL14HireDteMo = StringUtils.Mid(sHiredDte, 5, 2);
                    Globals.objPCL14.PMSPCL14HireDteDy = StringUtils.Right(sHiredDte, 2);
                }
                if (!string.IsNullOrEmpty(xmlInjury.DateOfDeath))
                    Globals.objPCL14.PMSPCL14LsDod = string.Format("yyyyMMdd", xmlInjury.DateOfDeath);
                Globals.objPCL14.PMSPCL14LsOccCode = xmlEmployee.Occupation_Code;
                Globals.objPCL14.PMSPCL14LsOccuptn = xmlEmployee.Occupation;
                Globals.objPCL14.PMSPCL14EmplyStat = StringUtils.Right(" " + xmlEmployee.EmploymentStatus_Code, 1);

                if (!string.IsNullOrEmpty(xmlEmployee.NumberOfDependents))
                    Globals.objPCL14.PMSPCL14LsNbrDeps = Convert.ToInt32(objCfunctions.CnvData(xmlEmployee.NumberOfDependents, CommonFunctions.ufDataTypes.ufdtInteger));

                if (!string.IsNullOrEmpty(xmlInjury.PercentDisability))
                    Globals.objPCL14.PMSPCL14PctImpair = Convert.ToInt32(objCfunctions.CnvData(xmlInjury.PercentDisability, CommonFunctions.ufDataTypes.ufdtInteger));

                Globals.objPCL14.PMSPCL14LsRepIndr = BoolToYN(xmlEmployee.AttorneyRepresented);
                Globals.objPCL14.PMSPCL14DteOfDisab = string.IsNullOrEmpty(xmlInjury.DateDisabilityBegan) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlInjury.DateDisabilityBegan)));
                Globals.objPCL14.PMSPCL14DteMaxMed = string.IsNullOrEmpty(xmlInjury.DateMaxMedicalImprovement) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlInjury.DateMaxMedicalImprovement)));
                Globals.objPCL14.PMSPCL14SurgInd = BoolToYN(xmlInjury.Surgery);
                Globals.objPCL14.PMSPCL14McoInd = xmlInjury.ManagedCareOrgType_Code;

                Globals.objPCL14.PMSPCL14LsLstWrkDa = string.IsNullOrEmpty(xmlEmployee.LastDayWorked) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlEmployee.LastDayWorked)));
                Globals.objPCL14.PMSPCL14LsDteRetd = string.IsNullOrEmpty(xmlEmployee.ReturnToWorkDate) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlEmployee.ReturnToWorkDate)));
                if (!string.IsNullOrEmpty(xmlEmployee.AvgWklyWage))
                    Globals.objPCL14.PMSPCL14LsCurrAww = Convert.ToSingle(objCfunctions.CnvData(xmlEmployee.AvgWklyWage, CommonFunctions.ufDataTypes.ufdtCurrency));

                Globals.objPCL14.PMSPCL14LsMarStat = (string.IsNullOrEmpty(xmlEmployee.EmployeeEntity.MaritalStatus_Code)) ? string.Empty : xmlEmployee.EmployeeEntity.MaritalStatus_Code.ToUpper();

                // Process benefits
                rst = true;
                while ((xmlEmployee.getBenefitOffset(rst) != null))
                {
                    rst = false;
                    //     Select Case xmlEmployee.BenefitOffsetType_Code         
                    switch (xmlEmployee.BenefitOffsetType)
                    {
                        //SI05268
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTUnspecified:
                            break;
                        //Not assigned
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTUnemployment:
                            Globals.objPCL14.PMSPCL14BoUnempl = "Y";
                            break;
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTSocSec:
                            Globals.objPCL14.PMSPCL14BoSocSec = "Y";
                            break;
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTPensionPlan:
                            Globals.objPCL14.PMSPCL14BoPension = "Y";
                            break;
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTSpclFund:
                            Globals.objPCL14.PMSPCL14BoSpecial = "Y";
                            break;
                        case CCP.Constants.xcEmpBenefitType.xcEmpBTOther:
                            Globals.objPCL14.PMSPCL14BoOther = "Y";
                            break;
                    }
                }

                // Process Wages
                // We are going to assume that only weekly wages are sent to POINT
                rst = false;
                while ((xmlEmployee.getPay(rst) != null))
                {
                    rst = false;
                    switch (xmlEmployee.PaySpecified)
                    {
                        case CCP.Constants.xcEmpSpecified.xcEmpPostInjury:
                            Globals.objPCL14.PMSPCL14PostInjWge = Convert.ToInt32(objCfunctions.CnvData(xmlEmployee.Pay, CommonFunctions.ufDataTypes.ufdtSingle));//UniversalConstants.UC_ufdtCurrency
                            break;
                        case CCP.Constants.xcEmpSpecified.xcEmpOther:
                            Globals.objPCL14.PMSPCL14OthrWkPymt = Convert.ToSingle(objCfunctions.CnvData(xmlEmployee.Pay, CommonFunctions.ufDataTypes.ufdtSingle));//UniversalConstants.UC_ufdtCurrency
                            break;
                        case CCP.Constants.xcEmpSpecified.xcEmpCurrent:
                            Globals.objPCL14.PMSPCL14OthrWkPymt = Convert.ToSingle(objCfunctions.CnvData(xmlEmployee.Pay, CommonFunctions.ufDataTypes.ufdtSingle));//UniversalConstants.UC_ufdtCurrency
                            Globals.objPCL14.PMSPCL14WageMeth = StringUtils.Right(" " + xmlEmployee.PayCalculateMethod, 1);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                xmlEmployee = null;
                xmlInjury = null;
                Globals.dbg.PopProc();
            }
        }

        public static void MapEmployeeEntity()
        {
            Globals.dbg.PushProc("MapEmployeeEntity");
            Parameters objParams = Globals.GetParams();
            Globals.objPCL14.PMSPCL14LsMarStat = Globals.strMaritalStatus;
            Globals.objPCL14.PMSPCL14LsDob = string.IsNullOrEmpty(Globals.strBirthDay) ? string.Empty : string.Format("{0:yyyyMMdd}", Globals.GetDateTime(Globals.strBirthDay));
            //Globals.objPCL14.PMSPCL14Fein = Globals.strFein;
            Globals.objPCL14.PMSPCL14EmplrSic = Globals.strSICCode;

            Globals.InitForClaimant(Globals.objPCL30);
            if (Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient)) // CLIENTFILE_FLAG in POLICY_X_WEB
            {
                Globals.InitForClientFile(Globals.objPCT30);
            }
            MapClientFile();
            bEmployeeEntityFound = true;

            Globals.dbg.PopProc();
        }

        public static void CheckEmployeeClient()
        {
            try
            {
                Globals.dbg.PushProc("CheckEmployeeClient");

                Globals.objPCL30.PMSPCL30Claim = Globals.sClaimNumber;
                if (!string.IsNullOrEmpty(Globals.sBodyPart))
                    Globals.objPCL30.PMSPCL30AiaCode1 = Globals.sBodyPart;
                if (!string.IsNullOrEmpty(Globals.sNatureOfInjury))
                    Globals.objPCL30.PMSPCL30AiaCode2 = Globals.sNatureOfInjury;
                if (!string.IsNullOrEmpty(Globals.sCauseOfInjury))
                    Globals.objPCL30.PMSPCL30AiaCode3 = Globals.sCauseOfInjury;

                Globals.objPCL30.PMSPCL30ClmtSeq = Globals.sCLMTSEQ;

                if (Globals.strEntityType == "INDIVIDUAL")
                {
                    Globals.objPCT30.PMSPCT30ClientType = "I";
                }
                else
                {
                    Globals.objPCT30.PMSPCT30ClientType = "C";
                }

                LoadDatabase.LocateClaimant();

                Globals.objPCL14.PMSPCL14Client = Globals.objPCL30.PMSPCL30Client;
            }

            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

                Globals.dbg.PopProc();
            }
        }

        private static void ParseLossDetail(CCP.XmlComponents.XMLLossDetail xmlLD)
        {
            string[] vArray = null;
            bool rst = false;
            CCP.XmlComponents.XMLEntity xmlEntity = null;
            Parameters objParams = Globals.GetParams();
            string sCurrDate = string.Empty;


            try
            {
                Globals.dbg.PushProc("ParseLossDetail");

                xmlEntity = new CCP.XmlComponents.XMLEntity();

                Globals.InitForClaimant(Globals.objPCL30);
                if (!Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient))
                {
                    Globals.InitForClientFile(Globals.objPCT30);
                }

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlClmt, ScriptingFunctions.conScpEvtStart, xmlLD);

                //  objPCL30.PMSPCL30Claim = sClaimNumber
                Globals.sPolCovSeq = 0;
                sReserveType = "";
                sMember = "";
                sLossCode = "";
                sReserveCategory = "";
                Globals.bStatusChangeTrans = false;

                if (!string.IsNullOrEmpty(xmlLD.ClaimantSeq))
                {
                    Globals.sPolCovSeq = Convert.ToInt32(xmlLD.ClaimantSeq);
                }
                else
                {
                    Globals.sPolCovSeq = 1;
                }

                //NOTE: The code below was not changed even though OFFSET transactions
                //      are ignored in this interface
                if (string.IsNullOrEmpty(xmlLD.OffOnSet_Code))
                {
                    Globals.bOffsetOnsetLossDetail = false;
                    Globals.bOffsetLossDetail = false;
                    Globals.bOnsetLossDetail = false;
                }
                else
                {
                    Globals.bOffsetOnsetLossDetail = true;
                    if (xmlLD.OffOnSet_Code == "OFFSET")
                    {
                        Globals.bOffsetLossDetail = true;
                        Globals.bOnsetLossDetail = false;
                    }
                    else
                    {
                        Globals.bOffsetLossDetail = false;
                        Globals.bOnsetLossDetail = true;
                    }
                }
              
                if (!string.IsNullOrEmpty(xmlLD.ClaimantNumber))
                {
                    Globals.sCLMTSEQ = Convert.ToInt32(xmlLD.ClaimantNumber);
                }
                else
                {
                    Globals.sCLMTSEQ = 1;
                }
                Globals.InitAddressWork();
                if (!string.IsNullOrEmpty(xmlLD.ClaimantEntityID))
                {
                    xmlEntity.XML = xmlLD.XML;
                    xmlEntity.Parent.XML = xmlLD.XML;
                    xmlEntity.getEntitybyID(xmlLD.ClaimantEntityID);
                    ParseEntityNode(xmlEntity);
                }
                MapClientFile();

                LoadDatabase.LocateClaimant();

                Globals.InitForClaimCoverageFile(Globals.objPCL42);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlCovg, ScriptingFunctions.conScpEvtStart, xmlLD);

                Globals.objPCL42.PMSPCL42Claim = Globals.sClaimNumber;
                Globals.objPCL42.PMSPCL42ClmtSeq = Globals.sCLMTSEQ;
                Globals.objPCL42.PMSPCL42PolCovSeq = Globals.sPolCovSeq;

                if (!string.IsNullOrEmpty(xmlLD.ReserveType_Code))
                {
                    if (StringUtils.InStr(StringUtils.Trim(xmlLD.ReserveType_Code), "(") != -1) //jira 7504, if ( is not present in reserve type code, it is giving exception here
                        sReserveType = StringUtils.Mid(StringUtils.Trim(xmlLD.ReserveType_Code), 1, StringUtils.InStr(StringUtils.Trim(xmlLD.ReserveType_Code), "("));//aaggarwal29 : MITS 37929
                }
                else
                {
                    sReserveType = "DIR";
                }
                Globals.sResvType = sReserveType;

                sMember = xmlLD.LossMember_Code;
                sReserveCategory = xmlLD.ReserveCategory_Code;
                sInsLine = xmlLD.CoverageInsLine_Code;

                //NOTE:  POINT does not load the Bureau Code to PMSPCL42 and it is
                //       Ignored in this processing.
                //       If an implementation needs to upload the Bureau Code, then use
                //       the TranslateCOL to convert it to a bureau code.
                //       This Bureau Code will then be used to determine the reserve number
                if (sInsLine == "WC")
                {
                    sLossCode = xmlLD.LossDisability_Code;
                }
                else
                {
                    sLossCode = xmlLD.CauseOfLoss_Code;
                }
                TranslateCOL();
                Globals.objPCL42.PMSPCL42LossCause = sLossCode;

                if (!string.IsNullOrEmpty(xmlLD.PolicyCoverageLegacyKey))
                {
                    sLegacyCoverageKey = xmlLD.PolicyCoverageLegacyKey;
                }
                else
                {
                    sLegacyCoverageKey = "00001,1,1";
                }
                ParsePolicyNode(xmlLD.Policy);

                vArray = sLegacyCoverageKey.Split(',');
                if (vArray.Length < 2)
                {
                    sLegacyCoverageKey = "00001,1,1";
                    vArray = sLegacyCoverageKey.Split(',');
                }
                Globals.objPCL42.PMSPCL42UnitNo = vArray[0];
                Globals.objPCL42.PMSPCL42CovSeq = Convert.ToInt32("0" + vArray[1]);
                Globals.objPCL42.PMSPCL42TransSeq = Convert.ToInt32("0" + vArray[2]);

                BuildResvNo(xmlLD);
                if (sInsLine == "WC")
                {
                    Globals.bWCompInsLine = true;
                }
                else
                {
                    Globals.bWCompInsLine = false;
                }

                // Pull adjuster / examiner at Claim Level
                if (StringUtils.InStr(objParams.Parameter(Globals.conADJAssignLevel), Globals.conADJAssignLevelClaim) != 0)
                {
                    if ((Globals.XMLClaim.PartyInvolved.findpartyInvolvedbyRole(Globals.sADJRole) != null))
                    {
                        if (StringUtils.InStr(objParams.Parameter(Globals.conADJAssignLevel), Globals.conADJAssignExaminer) != 0)
                        {
                            if ((Globals.XMLClaim.PartyInvolved.Entity.findIDNumberByType(objParams.Parameter(Globals.conExmrLit)) != null))
                            {
                                Globals.objPCL42.PMSPCL42Examiner = Globals.XMLClaim.PartyInvolved.Entity.IDNumber;
                            }
                        }
                        else
                        {
                            ParseAdjuster(Globals.XMLClaim.PartyInvolved);
                            Globals.objPCL42.PMSPCL42Adjustor = Globals.strVenNumber;
                        }
                    }
                }

                // Pull adjuster / examiner at Claimant Level
                if (StringUtils.InStr(objParams.Parameter(Globals.conADJAssignLevel), Globals.conADJAssignLevelClaimant) != 0)
                {
                    //First find the claimant roles before looking for a specific entity
                    if ((Globals.XMLClaim.PartyInvolved.findpartyInvolvedbyRole("CLT") != null))
                    {
                        if ((Globals.XMLClaim.PartyInvolved.findPartyInvolvedByEntity(xmlLD.ClaimantEntityID, "") != null))
                        {
                            if ((Globals.XMLClaim.PartyInvolved.PartyInvolved.findpartyInvolvedbyRole(Globals.sADJRole) != null))
                            {
                                if (StringUtils.InStr(objParams.Parameter(Globals.conADJAssignLevel), Globals.conADJAssignExaminer) != 0)
                                {
                                    if ((Globals.XMLClaim.PartyInvolved.PartyInvolved.Entity.findIDNumberByType(objParams.Parameter(Globals.conExmrLit)) != null))
                                    {
                                        Globals.objPCL42.PMSPCL42Examiner = Globals.XMLClaim.PartyInvolved.PartyInvolved.Entity.IDNumber;
                                    }
                                }
                                else
                                {
                                    ParseAdjuster(Globals.XMLClaim.PartyInvolved.PartyInvolved);
                                    Globals.objPCL42.PMSPCL42Adjustor = Globals.strVenNumber;
                                }
                            }
                        }
                    }
                    xmlEntity = null;
                }
                if (xmlLD.ReserveAdjusterExaminerCd != string.Empty)
                {
                    Globals.objPCL42.PMSPCL42Examiner = xmlLD.ReserveAdjusterExaminerCd;
                }
                else
                {
                    Globals.objPCL42.PMSPCL42Examiner = Globals.ExaminerCd;
                }

                if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
                {
                    if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                    {
                        Globals.objPCL42.PMSPCL42Examiner = Globals.objConfig.ConfigElement.ExaminerCD;
                    }
                }
                if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
                {
                    if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                    {
                        Globals.objPCL42.PMSPCL42ClaimOff = Globals.objConfig.ConfigElement.ClmOffice;
                    }
                }

                LoadDatabase.LocateClaimCoverage();

                //Process Financial Transactions
                rst = true;

                while ((xmlLD.getFinTrans(rst) != null))
                {
                    rst = false;
                    ParseFinancial(xmlLD);
                    Globals.lClmTransactionCount = Globals.lClmTransactionCount + 1;
                }

                if (Globals.objPCL42.PMSPCL42RecStatus != Globals.objPCL50.PMSPCL50RecStatus)
                {
                    Globals.objPCL42.PMSPCL42RecStatus = Globals.objPCL50.PMSPCL50RecStatus;
                    LoadDatabase.UpdateClaimCoverageStatus();
                }
                else
                {
                    if (Globals.bNewReserve == true)
                    {
                        LoadDatabase.CheckClaimantStatus();
                    }
                }

                if (Globals.objConfig.ConfigElement.EntryInPMSP0000ForAL == "YES")
                {

                    sCurrDate = Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", DateTime.Now));
                    if (Globals.objPCL50.PMSPCL50TranEntDt != RC.Conversion.ConvertStrToLong(sCurrDate))
                    {

                        ParsePointALUpload(xmlLD);
                    }
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlCovg, ScriptingFunctions.conScpEvtEnd, xmlLD);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlClmt, ScriptingFunctions.conScpEvtEnd, xmlLD);
                xmlEntity = null;
                Globals.dbg.PopProc();
            }
        }

        private static void TranslateCOL()
        {
            Globals.dbg.PushProc("TranslateCOL");
        }

        public static void BuildResvNo(CCP.XmlComponents.XMLLossDetail xmlLD)
        {
            string sShortCode = null;
            string sReserveNo = null;

            try
            {
                Globals.dbg.PushProc("BuildResvNo");
                sShortCode = xmlLD.ReserveType_Code;
                // rrachev JIRA RMA-7504 Begin
                int OpenBrackedIdx = StringUtils.InStr(StringUtils.Trim(sShortCode), "(");
                if (OpenBrackedIdx > -1)
                {
                    sReserveNo = StringUtils.Mid(StringUtils.Trim(sShortCode), OpenBrackedIdx + 2, (StringUtils.InStr(StringUtils.Trim(sShortCode), ")") - 1) - StringUtils.InStr(StringUtils.Trim(sShortCode), "("));
                }
                //sReserveNo = StringUtils.Mid(StringUtils.Trim(sShortCode), StringUtils.InStr(StringUtils.Trim(sShortCode), "(") + 2, (StringUtils.InStr(StringUtils.Trim(sShortCode), ")") - 1) - StringUtils.InStr(StringUtils.Trim(sShortCode), "("));
                // rrachev JIRA RMA-7504 End
                if (string.IsNullOrEmpty(sReserveNo))
                {
                    throw new Exception("Incorrect format of reserve type code : " + sShortCode);
                }
                else
                {
                    Globals.iResvNo = Convert.ToInt32(sReserveNo);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                Globals.dbg.PopProc();
            }
        }

        public static string ResvCategory(string sResvno)
        {
            string functionReturnValue = null;
            object rowsAffected;

            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            try
            {
                Globals.dbg.PushProc("ResvCategory");
                strSQL.AppendFormat("SELECT RESVTYPE FROM TBCL006A  " + " WHERE RESVNO ={0}", "~RESVNO~");
                dictParams.Add("RESVNO", sResvno);
                string sResvType = Conversion.ConvertObjToStr(Riskmaster.Db.DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
                //string sResvType = Convert.ToString(LoadDatabase.ExecuteScalar("SELECT RESVTYPE FROM TBCL006A  " + " WHERE RESVNO ='" + sResvno + "'"));

                if (string.IsNullOrEmpty(sResvType))
                {
                    Globals.sResvType = "L";
                }
                else
                {
                    Globals.sResvType = sResvType;
                }
            }
            finally
            {

                Globals.dbg.PopProc();
            }
            return Globals.sResvType;
        }

        public static void MapClientFile()
        {
            Parameters objParams = Globals.GetParams();
            System.DateTime BIRTHDATE = default(System.DateTime);
            System.DateTime lossDate = default(System.DateTime);
            int age = 0;
            try
            {
                Globals.dbg.PushProc("MapClientFile");

                if (!Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient))
                {
                    Globals.objPCL30.Clear();
                    Globals.objPCL30.PMSPCL30Claim = Globals.sClaimNumber;
                    Globals.objPCL30.PMSPCL30ClmtSeq = Globals.sCLMTSEQ;
                    Globals.objPCL30.PMSPCL30AiaCode1 = Globals.sBodyPart;
                    Globals.objPCL30.PMSPCL30AiaCode2 = Globals.sNatureOfInjury;
                    Globals.objPCL30.PMSPCL30AiaCode3 = Globals.sCauseOfInjury;//KeyNotFoundException]
                    Globals.objPCL30.PMSPCL30RecStatus = "O";

                    if (!string.IsNullOrEmpty(Globals.XMLClaim.AIA))
                    {
                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 1, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode1 = StringUtils.Mid(Globals.XMLClaim.AIA, 1, 2);
                        }
                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 3, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode2 = StringUtils.Mid(Globals.XMLClaim.AIA, 3, 2);
                        }
                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 5, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode3 = StringUtils.Mid(Globals.XMLClaim.AIA, 5, 2);
                        }
                    }

                    Globals.objPCT30.Clear();
                    Globals.objPCT30.EntityID = Globals.lEntityID;
                    Globals.objPCT30.PMSPCT30Ct30type = "CL";
                    Globals.objPCT30.PMSPCT30ClientType = "C";
                    Globals.objPCT30.PMSPCT30Client = Globals.lClientNumber;
                    Globals.objPCT30.PMSPCT30LName = Globals.strLastName;
                    Globals.objPCT30.PMSPCT30FName = Globals.strFirstName;
                    Globals.objPCT30.PMSPCT30MName = Globals.strMiddleName;
                    Globals.objPCT30.PMSPCT30SirName = Globals.strNameSuffix;
                    if (StringUtils.Len(Globals.strAddress[1]) != 0)
                    {
                        Globals.objPCT30.PMSPCT30Address1 = Globals.strAddress[0];
                        Globals.objPCT30.PMSPCT30Address2 = Globals.strAddress[1];
                    }
                    else
                    {
                        Globals.objPCT30.PMSPCT30Address1 = Globals.strAddress[0];
                    }
                    Globals.objPCT30.PMSPCT30City = Globals.strCity;
                    Globals.objPCT30.PMSPCT30State = Globals.strStateCode;
                    Globals.objPCT30.PMSPCT30ZipCode = Globals.strPostalCd;
                    Globals.objPCT30.PMSPCT30Sex = Globals.strGender;
                    Globals.objPCT30.PMSPCT30Phone = Globals.strPhone[0];

                    if (Globals.strTaxIdType == "Y")
                    {
                        Globals.objPCT30.PMSPCT30SSN = Globals.strUnformatedTaxId;
                    }
                    else
                    {
                        Globals.objPCT30.PMSPCT30TaxId = Globals.strUnformatedTaxId;
                    }

                    if (Globals.strEntityType == "INDIVIDUAL")
                    {
                        Globals.objPCT30.PMSPCT30ClientType = "I";
                    }
                    else
                    {
                        Globals.objPCT30.PMSPCT30ClientType = "C";
                    }

                    if (!string.IsNullOrEmpty(Globals.strBirthDay))
                    {
                        BIRTHDATE = Globals.GetDateTime(Globals.strBirthDay);
                        Globals.bupdateDefaultDOB = false;
                        lossDate = Globals.GetDateTime(Globals.XMLClaim.DateOfLoss);

                        age = lossDate.Year - BIRTHDATE.Year - 1;
                        if ((lossDate.Month > BIRTHDATE.Month) || (lossDate.Month == BIRTHDATE.Month) && (lossDate.Day > BIRTHDATE.Day - 1))
                        {
                            age = age + 1;
                        }
                        Globals.objPCT30.PMSPCT30Age = Convert.ToString(age);
                    }
                    else
                    {
                        Globals.bupdateDefaultDOB = true;
                        Globals.objPCT30.PMSPCT30Age = "";
                    }

                }
                else
                {

                    Globals.objBASCLT1700.Clear();
                    Globals.objBASCLT1700.CLAIM = Globals.sClaimNumber;
                    Globals.objBASCLT1700.CLMTSEQ = Globals.sCLMTSEQ;
                    Globals.objBASCLT1700.CLTSEQNUM = Globals.lClientNumber;
                    Globals.objBASCLT1700.ADDRSEQNUM = Globals.lAddrSeqNumber;
                    Globals.objBASCLT1700.RMAENTITYID = Globals.lEntityID;
                    Globals.objBASCLT1700.ROLE = "CLAIMANT";

                    Globals.objBASCLT0100.Clear();
                    Globals.objBASCLT0100.EntityID = Globals.lEntityID;
                    Globals.objBASCLT0100.CLTSEQNUM = Globals.lClientNumber;

                    if (Globals.strTaxIdType == "Y")
                    {
                        Globals.objBASCLT0100.SSN = Globals.strTaxId;
                    }
                    else
                    {
                        Globals.objBASCLT0100.FEDTAXID = Globals.strTaxId;
                    }
                    //SISAMSUNG Groupno not uploaded by RMA
                    //Globals.objBASCLT0100.GROUPNO = "CSCCCP   ";
                    //End SISAMSUNG
                    Globals.objBASCLT0100.NAMESTATUS = "A";
                    Globals.objBASCLT0100.NAMETYPE = StringUtils.Left(Globals.strEntityType, 1);
                    Globals.objBASCLT0100.PREFIXNM = Globals.strNamePrefix;
                    Globals.objBASCLT0100.SUFFIXNM = Globals.strNameSuffix;
                    Globals.objBASCLT0100.LONGNAME = Globals.strFullName;

                    if (Globals.strEntityType == "INDIVIDUAL")
                    {
                        Globals.objBASCLT0100.CLIENTNAME = Globals.strLastName;
                        Globals.objBASCLT0100.FIRSTNAME = Globals.strFirstName;
                        Globals.objBASCLT0100.MIDNAME = Globals.strMiddleName;
                        Globals.objBASCLT0100.DBA = "";
                    }
                    else
                    {
                        Globals.objBASCLT0100.CLIENTNAME = Globals.strLongName;
                        Globals.objBASCLT0100.FIRSTNAME = "";
                        Globals.objBASCLT0100.MIDNAME = "";
                        Globals.objBASCLT0100.DBA = Globals.strDBA;
                    }

                    Globals.objBASCLT0100.CONTACT = Globals.strContact[0];
                    Globals.objBASCLT0100.PHONE1 = Globals.strPhone[0];
                    Globals.objBASCLT0100.PHONE2 = Globals.strPhone[1];
                    Globals.objBASCLT0100.SEX = Globals.strSex;

                    if (!string.IsNullOrEmpty(Globals.strBirthDay))
                    {
                        Globals.objBASCLT0100.BIRTHDATE = Globals.GetDateTime(Globals.strBirthDay);
                        Globals.bupdateDefaultDOB = false;
                    }
                    else
                    {
                        Globals.bupdateDefaultDOB = true;
                    }


                    Globals.objBASCLT0300.Clear();
                    Globals.objBASCLT0300.CLTSEQNUM = Globals.lClientNumber;
                    Globals.objBASCLT0300.ADDRTYPE = "PRIMARY";
                    Globals.objBASCLT0300.ADDRSTATUS = "A";
                    Globals.objBASCLT0300.ADDRSEQNUM = Globals.lAddrSeqNumber;
                    if (!string.IsNullOrEmpty(Globals.strAddress[0]))
                        Globals.objBASCLT0300.ADDRLN1 = StringUtils.Left(Globals.strAddress[0], 64);
                    if (!string.IsNullOrEmpty(Globals.strAddress[1]))
                        Globals.objBASCLT0300.ADDRLN2 = StringUtils.Left(Globals.strAddress[1], 64);
                    if (!string.IsNullOrEmpty(Globals.strAddress[2]))
                        Globals.objBASCLT0300.ADDRLN3 = StringUtils.Left(Globals.strAddress[2], 64);
                    if (!string.IsNullOrEmpty(Globals.strAddress[3]))
                        Globals.objBASCLT0300.ADDRLN4 = StringUtils.Left(Globals.strAddress[3], 64);
                    if (!string.IsNullOrEmpty(Globals.strCity))
                        Globals.objBASCLT0300.CITY = StringUtils.Left(Globals.strCity, 32);
                    if (!string.IsNullOrEmpty(Globals.strCountry))
                        Globals.objBASCLT0300.COUNTRY = StringUtils.Left(Globals.strCountry, 3);
                    if (!string.IsNullOrEmpty(Globals.strCounty))
                        Globals.objBASCLT0300.COUNTY = StringUtils.Left(Globals.strCounty, 3);
                    if (!string.IsNullOrEmpty(Globals.strStateCode))
                        Globals.objBASCLT0300.STATE = StringUtils.Left(Globals.strStateCode, 2);
                    if (!string.IsNullOrEmpty(Globals.strPostalCd))
                        Globals.objBASCLT0300.ZIPCODE = StringUtils.Left(Globals.strPostalCd, 11);


                    Globals.objPCL30.Clear();
                    Globals.objPCL30.PMSPCL30Claim = Globals.sClaimNumber;
                    Globals.objPCL30.PMSPCL30ClmtSeq = Globals.sCLMTSEQ;
                    Globals.objPCL30.PMSPCL30AiaCode1 = Globals.sBodyPart;
                    Globals.objPCL30.PMSPCL30AiaCode2 = Globals.sNatureOfInjury;
                    Globals.objPCL30.PMSPCL30AiaCode3 = Globals.sCauseOfInjury;
                    Globals.objPCL30.PMSPCL30RecStatus = "O";

                    if (!string.IsNullOrEmpty(Globals.XMLClaim.AIA))
                    {

                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 1, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode1 = StringUtils.Mid(Globals.XMLClaim.AIA, 1, 2);
                        }
                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 3, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode2 = StringUtils.Mid(Globals.XMLClaim.AIA, 3, 2);
                        }
                        if (!string.IsNullOrEmpty(StringUtils.Trim(StringUtils.Mid(Globals.XMLClaim.AIA, 5, 2))))
                        {
                            Globals.objPCL30.PMSPCL30AiaCode3 = StringUtils.Mid(Globals.XMLClaim.AIA, 5, 2);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

                Globals.dbg.PopProc();

            }
        }

        private static void ParseEntityNode(CCP.XmlComponents.XMLEntity xmlEntity)
        {
            string sTemp = null;
            bool rst = false;
            bool bPrim = false;
            object eid = null;
            int i = 0;
            int j = 0;

            Parameters objParams = Globals.GetParams();
            Globals.dbg.PushProc("ParseEntityNode");

            Globals.InitAddressWork();
            
            eid = xmlEntity.EntityID;
            if (CCP.Common.NumericUtils.IsNumeric(eid))
            {
                Globals.lEntityID = Convert.ToInt64(eid);
            }

            xmlEntity.getIDNumber(true);
            //  strEntityType = UCase(xmlEntity.EntityTypeTranslate(xmlEntity.EntityType))
            Globals.strEntityType = (string.IsNullOrEmpty(xmlEntity.EntityType_Code)) ? string.Empty : xmlEntity.EntityType_Code.ToUpper();
            Globals.strGender = xmlEntity.Gender_Code;
            sTemp = xmlEntity.TaxId.Replace("-", "");
            Globals.strUnformatedTaxId = sTemp;

            if (xmlEntity.TaxIdType == xmlEntity.TaxIdTranslate(CCP.Constants.entTaxIdType.entTaxIdFEIN))
            {

                Globals.strTaxIdType = "N";

                if (!string.IsNullOrEmpty(sTemp))
                {
                    sTemp = sTemp + "          ";
                    Globals.strLongTaxId = StringUtils.Trim(StringUtils.Left(sTemp, 2) + "-" + StringUtils.Mid(sTemp, 3));
                    if (objParams.IsTrue(Globals.conFormatSW + Globals.conTaxIDSW))
                    {
                        Globals.strTaxId = Globals.strLongTaxId;
                        Globals.strFein = Globals.strLongTaxId;
                    }
                    else
                    {
                        Globals.strTaxId = StringUtils.Trim(sTemp);
                        Globals.strFein = StringUtils.Trim(sTemp);
                    }
                }
            }
            else
            {
                Globals.strTaxIdType = "Y";

                if (!string.IsNullOrEmpty(sTemp))
                {
                    sTemp = sTemp + "          ";
                    Globals.strLongTaxId = StringUtils.Trim(StringUtils.Left(sTemp, 3) + "-" + StringUtils.Mid(sTemp, 4, 2) + "-" + StringUtils.Mid(sTemp, 6));
                    if (objParams.IsTrue(Globals.conFormatSW + Globals.conTaxIDSW))
                    {
                        Globals.strTaxId = Globals.strLongTaxId;
                    }
                    else
                    {
                        Globals.strTaxId = StringUtils.Trim(sTemp);
                    }
                }
            }
            Globals.strBirthDay = xmlEntity.DateOfBirth;
            Globals.strMaritalStatus = (string.IsNullOrEmpty(xmlEntity.MaritalStatus_Code)) ? string.Empty : xmlEntity.MaritalStatus_Code.ToUpper();
            Globals.strSex = xmlEntity.Gender_Code;//Globals.objDRFT.DRFTPayStatus = "P"
            Globals.strSICCode = xmlEntity.SIC;
            if ((xmlEntity.findIDNumberByType(objParams.Parameter(Globals.conSWPOINTVendor)) != null))
            {
                Globals.strVenNumber = xmlEntity.IDNumber;
            }
            else
            {
                Globals.strVenNumber = "";
            }

            Globals.lClientNumber = Convert.ToInt64(objCfunctions.CnvData(xmlEntity.IDNumber, CommonFunctions.ufDataTypes.ufdtLong));
            Globals.lAddrSeqNumber = Convert.ToInt64(objCfunctions.CnvData(xmlEntity.AddressSeqNum, CommonFunctions.ufDataTypes.ufdtLong));

            Globals.strCategory = "";
            if (objParams.Parameter(Globals.conSWVendorType) == Globals.conSWVendorTypeBusType)
            {
                if (!string.IsNullOrEmpty(xmlEntity.TypeOfBusiness_Code))
                {
                    Globals.strCategory = xmlEntity.TypeOfBusiness_Code;

                    if (!string.IsNullOrEmpty(sTemp))
                        Globals.strCategory = sTemp;
                }

            }
            else
            {

                rst = true;

                while ((xmlEntity.getCategory(rst) != null) & string.IsNullOrEmpty(Globals.strCategory))
                {

                    rst = false;
                    Globals.strCategory = xmlEntity.CategoryCode;

                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        Globals.strCategory = "";
                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(sTemp))
                            Globals.strCategory = sTemp;
                    }
                }
            }

            ParseNameNode(xmlEntity);

            //Get Contact Information
            rst = true;
            bPrim = false;
            sTemp = "";
            j = 0;
            while ((xmlEntity.getContact(rst) != null))
            {
                rst = false;
                Globals.strPhone[j] = xmlEntity.ContactInfo;// JIRA 7031: Merged changes
                //Contact type must be a Phone Type defined in
                //ContactType.PHONE = list of phone contact types separated by colons
                i = StringUtils.InStr(objParams.Parameter(Globals.conContactTypeSW + Globals.conPhoneSW), ":" + xmlEntity.ContactTypeCode + ":");
                //If xmlEntity.ContactTypeCode = mParams.Parameter(conContactTypeSW) And
                if (i > 0 & j <= 2)
                {
                    //Only bump for primary contacts items and rescan to see if there are other items
                    if (!string.IsNullOrEmpty(xmlEntity.PrimaryContactFlag))
                    {
                        Globals.strPhone[j] = xmlEntity.ContactInfo;
                        Globals.strContact[j] = xmlEntity.ContactWhoID;
                        j = j + 1;
                        rst = false;
                        bPrim = false;
                    }
                    else if (!bPrim)
                    {
                        Globals.strPhone[j] = xmlEntity.ContactInfo;
                        Globals.strContact[j] = xmlEntity.ContactWhoID;
                        bPrim = true;
                    }
                }

            }
            for (j = 0; j <= 2; j++)
            {
                if (!string.IsNullOrEmpty(Globals.strPhone[j]))
                {
                    Globals.strPhone[j] = Globals.strPhone[j].Replace("(", "");
                    Globals.strPhone[j] = Globals.strPhone[j].Replace(")", "");
                    Globals.strPhone[j] = Globals.strPhone[j].Replace("-", "");
                    Globals.strPhone[j] = Globals.strPhone[j].Replace(" ", "");
                    Globals.strPhone[j] = Globals.strPhone[j].Replace("Ext:", "");

                    if (objParams.IsTrue(Globals.conFormatSW + Globals.conPhoneSW))
                    {
                        if (StringUtils.Len(Globals.strPhone[j]) <= 7)
                        {
                            Globals.strPhone[j] = Globals.strPhone[j] + "       ";
                            Globals.strPhone[j] = StringUtils.Trim(StringUtils.Left(Globals.strPhone[j], 3) + "-" + StringUtils.Mid(Globals.strPhone[j], 4));
                        }
                        else
                        {
                            Globals.strPhone[j] = Globals.strPhone[j] + "                   ";
                            Globals.strPhone[j] = StringUtils.Trim(StringUtils.Left(Globals.strPhone[j], 3) + "-" + StringUtils.Mid(Globals.strPhone[j], 4, 3) + "-" + StringUtils.Mid(Globals.strPhone[j], 7));
                        }
                    }
                    if (!string.IsNullOrEmpty(Globals.strContact[j]))
                    {
                        CCP.XmlComponents.XMLEntity xe = new CCP.XmlComponents.XMLEntity();
                        // xe.Document = xmlEntity.Document;
                        if ((xe.getEntitybyID(Globals.strContact[j]) != null))
                        {
                            if (xe.EntityType == "INDIVIDUAL")
                            {
                                Globals.strContact[j] = xe.FirstName + " " + xe.MiddleName + " " + xe.LastName;
                            }
                            else
                            {
                                Globals.strContact[j] = xe.LastName;
                            }
                        }
                        else
                        {
                            Globals.strContact[j] = "";
                        }
                    }
                }
            }

            //Get Address.  Only puts the first one
            xmlEntity.AddressReference.getAddressReference(true);
            ParseAddressNode(xmlEntity.AddressReference.Address);

            xmlEntity.AddressReference.findAddressRefbyRole("1099");
            if (xmlEntity.AddressReference.Node == null)
            {
                Globals.b1099Address = false;
            }
            else
            {
                Parse1099AddressNode(xmlEntity.AddressReference.Address);
                Globals.b1099Address = true;
            }

            Globals.dbg.PopProc();
        }

        private static void ParseNameNode(CCP.XmlComponents.XMLEntity xmlEntity)
        {

            Globals.dbg.PushProc("ParseNameNode");
            if (Globals.strEntityType == "INDIVIDUAL")
            {
                Globals.strNamePrefix = string.IsNullOrEmpty(xmlEntity.Prefix) ? string.Empty : xmlEntity.Prefix.ToUpper();
                Globals.strLastName = string.IsNullOrEmpty(xmlEntity.LastName) ? string.Empty : xmlEntity.LastName.ToUpper();
                Globals.strFirstName = string.IsNullOrEmpty(xmlEntity.FirstName) ? string.Empty : xmlEntity.FirstName.ToUpper();
                Globals.strMiddleName = string.IsNullOrEmpty(xmlEntity.MiddleName) ? string.Empty : xmlEntity.MiddleName.ToUpper();
                Globals.strNameSuffix = string.IsNullOrEmpty(xmlEntity.Suffix) ? string.Empty : xmlEntity.Suffix.ToUpper();
                Globals.strFullName = Globals.strNamePrefix + (!string.IsNullOrEmpty(Globals.strNamePrefix) ? " " : "");
                Globals.strFullName = Globals.strFullName + Globals.strFirstName + (!string.IsNullOrEmpty(Globals.strFirstName) ? " " : "");
                Globals.strFullName = Globals.strFullName + Globals.strMiddleName + (!string.IsNullOrEmpty(Globals.strMiddleName) ? " " : "");
                Globals.strFullName = Globals.strFullName + Globals.strLastName + (!string.IsNullOrEmpty(Globals.strLastName) ? " " : "");
                Globals.strFullName = Globals.strFullName + Globals.strNameSuffix + (!string.IsNullOrEmpty(Globals.strNameSuffix) ? " " : "");
                Globals.strLongName = StringUtils.Trim(Globals.strFirstName + " " + Globals.strMiddleName + " " + Globals.strLastName);
                Globals.strDBA = "";
            }
            else
            {
                Globals.strFullName = string.IsNullOrEmpty(xmlEntity.LegalName) ? string.Empty : xmlEntity.LegalName.ToUpper();
                Globals.strLongName = Globals.strFullName;
                Globals.strDBA = (string.IsNullOrEmpty(xmlEntity.DBAName)) ? string.Empty : xmlEntity.DBAName;
                Globals.strNamePrefix = "";
                Globals.strNameSuffix = "";

                if (StringUtils.Len(xmlEntity.LegalName) < 15)
                {
                    Globals.strLastName = (string.IsNullOrEmpty(xmlEntity.LegalName)) ? string.Empty : xmlEntity.LegalName;
                    Globals.strFirstName = "";
                    Globals.strMiddleName = "";
                }
                else
                {
                    Globals.strLastName = (string.IsNullOrEmpty(xmlEntity.LegalName)) ? string.Empty : StringUtils.Mid(xmlEntity.LegalName, 1, 15).ToUpper();
                    Globals.strFirstName = (string.IsNullOrEmpty(xmlEntity.LegalName)) ? string.Empty : StringUtils.Mid(xmlEntity.LegalName, 16, 11).ToUpper();
                    Globals.strMiddleName = (string.IsNullOrEmpty(xmlEntity.LegalName)) ? string.Empty : StringUtils.Mid(xmlEntity.LegalName, 27, 1).ToUpper();
                }
            }
            Globals.dbg.PopProc();
        }

        private static void ParseAddressNode(CCP.XmlComponents.XMLAddress xmlAddress)
        {
            bool rst = false;
            int i = 0;
            Globals.dbg.PushProc("ParseAddressNode");
            rst = true;
            i = 0;
            string sAddressID = string.Empty;

            if (!string.IsNullOrEmpty(xmlAddress.AddressID) && xmlAddress.AddressID != "0")
            {
                if (xmlAddress.AddressID.Contains("MT"))
                {
                    sAddressID = xmlAddress.AddressID.Replace("MT", "");
                }
                else
                {
                    sAddressID = xmlAddress.AddressID;
                }
                Globals.objBASCLT1700.RMAADDRESSID = RC.Conversion.ConvertStrToLong(sAddressID);
            }
            while ((xmlAddress.getAddressLine(rst) != null))
            {
                if (i <= 3)
                    Globals.strAddress[i] = (string.IsNullOrEmpty(xmlAddress.AddressLine)) ? string.Empty : xmlAddress.AddressLine.ToUpper();
                rst = false;
                i = i + 1;
            }

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strAddress[0] = sTemp;

            Globals.strCity = (string.IsNullOrEmpty(xmlAddress.City)) ? string.Empty : xmlAddress.City.ToString();

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strCity = sTemp;

            if (!string.IsNullOrEmpty(xmlAddress.State_Code))
                Globals.strStateCode = (string.IsNullOrEmpty(xmlAddress.State_Code)) ? string.Empty : xmlAddress.State_Code.ToString();

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strStateCode = sTemp;

            Globals.strPostalCd = (string.IsNullOrEmpty(xmlAddress.PostalCode)) ? string.Empty : xmlAddress.PostalCode.ToString();

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strPostalCd = sTemp;

            Globals.strCounty = (string.IsNullOrEmpty(xmlAddress.County_Code)) ? string.Empty : xmlAddress.County_Code.ToString();

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strCounty = sTemp;

            if (!string.IsNullOrEmpty(xmlAddress.Country_Code))
                Globals.strCountry = (string.IsNullOrEmpty(xmlAddress.Country_Code)) ? string.Empty : xmlAddress.Country_Code.ToString();

            if (!string.IsNullOrEmpty(sTemp))
                Globals.strCountry = sTemp;

            xmlAddress = null;
            Globals.dbg.PopProc();
        }

        private static void Parse1099AddressNode(CCP.XmlComponents.XMLAddress xmlAddress)
        {
            bool rst = false;
            int i = 0;
            Globals.dbg.PushProc("ParseAddressNode");
            rst = true;
            i = 0;
            while ((xmlAddress.getAddressLine(rst) != null))
            {
                if (i <= 3)
                    Globals.str1099Address[i] = (string.IsNullOrEmpty(xmlAddress.AddressLine)) ? string.Empty : xmlAddress.AddressLine.ToUpper();
                rst = false;
                i = i + 1;
            }
            Globals.str1099City = (string.IsNullOrEmpty(xmlAddress.City)) ? string.Empty : xmlAddress.City.ToUpper();
            if (!string.IsNullOrEmpty(xmlAddress.State_Code))
                Globals.str1099StateCode = (string.IsNullOrEmpty(xmlAddress.State_Code)) ? string.Empty : xmlAddress.State_Code.ToUpper();
            Globals.str1099PostalCd = (string.IsNullOrEmpty(xmlAddress.PostalCode)) ? string.Empty : xmlAddress.PostalCode.ToUpper();
            Globals.str1099County = (string.IsNullOrEmpty(xmlAddress.County_Code)) ? string.Empty : xmlAddress.County_Code.ToUpper();
            if (!string.IsNullOrEmpty(xmlAddress.Country_Code))
                Globals.str1099Country = xmlAddress.Country_Code.ToUpper();
            xmlAddress = null;
            Globals.dbg.PopProc();
        }

        private static void ParsePolicyNode(CCP.XmlComponents.XMLPolicy xmlPolicy)
        {
            Globals.dbg.PushProc("ParsePolicyNode");
            Globals.objPCL42.PMSPCL42MasterCo = xmlPolicy.MasterCompany_Code;
            Globals.objPCL42.PMSPCL42PolicyNo = xmlPolicy.PolicyNumber;
            Globals.objPCL42.PMSPCL42Location = xmlPolicy.Location;
            Globals.objPCL42.PMSPCL42Module = xmlPolicy.Module;
            Globals.objPCL42.PMSPCL42Symbol = xmlPolicy.Symbol;
            Globals.dbg.PopProc();
        }

        private static void ParseFinancial(CCP.XmlComponents.XMLLossDetail xmlLD)
        {
            string sAddedByUser = null;
            int iDateTimeStamp;
            try
            {
                Globals.dbg.PushProc("ParseFinancial:ID." + xmlLD.LossDetailID);

                Globals.InitForReservePaymentTransaction(Globals.objPCL50);
                Globals.InitForDraftFile(Globals.objDRFT);
                Globals.InitForCheckFile(Globals.objAP00);
                Globals.InitForAdjustorMasterFile(Globals.objAD00);
                Globals.InitForAutomaticIndemnityFile(Globals.objPCL92);
                Globals.InitForTaxIdFile(Globals.objPTX00);

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtStart, xmlLD, "LossDetail");

                Globals.objPCL50.PMSPCL50Claim = Globals.sClaimNumber;
                Globals.objPCL50.PMSPCL50ClmtSeq = Globals.sCLMTSEQ;
                Globals.objPCL50.PMSPCL50PolCovSeq = Globals.sPolCovSeq;

                //NOTE: AP00 and DRFTFILE must have the same CRTTIME values
                Globals.sHHNNSS000000Now = DateTime.Now.ToString("HHmmss").ToString() + StringUtils.Right(string.Format("{0:#0.00}", System.DateTime.Now.TimeOfDay.TotalSeconds), 2);

                //If reserveless collection (status 'L') the "FP" needs to be changed to "FF"
                if (xmlLD.ReserveStatus_Code == "L" & xmlLD.ActivityType_Code == "FP")
                {
                    xmlLD.ActivityType_Code = "FF";
                }

                sActivityType = xmlLD.ActivityType_Code;
                sTransType = xmlLD.TransactionType_Code;
                Globals.objPCL50.PMSPCL50TranEntDt = string.IsNullOrEmpty(xmlLD.TransactionDate) ? 0 : Convert.ToInt64("0" + Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlLD.TransactionDate))));
                Globals.sHHNNSS000000Now = Globals.GetDateTime(xmlLD.TransactionTime).ToString("HHmmss").ToString() + StringUtils.Right(string.Format("{0:#0.00}", System.DateTime.Now.TimeOfDay.TotalSeconds), 2); ;//Format$(xmlLD.TransactionTime, "HHNNSS") & Right(Format(Timer, "#0.00"), 2)
                //JIRA 4729 start
                iDateTimeStamp = Conversion.ConvertStrToInteger(Globals.sHHNNSS000000Now);
                if (Globals.drDateTimeStmap.Count == 0)
                {
                    Globals.drDateTimeStmap.Add(iDateTimeStamp, Globals.sHHNNSS000000Now);
                }
                else
                {
                    while (Globals.drDateTimeStmap.ContainsKey(iDateTimeStamp))
                    {
                        Globals.sHHNNSS000000Now = Globals.GetDateTime(xmlLD.TransactionTime).ToString("HHmmss").ToString() + StringUtils.Right(string.Format("{0:#0.00}", System.DateTime.Now.TimeOfDay.TotalSeconds), 2); ;//Format$(xmlLD.TransactionTime, "HHNNSS") & Right(Format(Timer, "#0.00"), 2)
                        iDateTimeStamp = Conversion.ConvertStrToInteger(Globals.sHHNNSS000000Now);
                    }
                    Globals.drDateTimeStmap.Add(iDateTimeStamp, Globals.sHHNNSS000000Now);
                }
                //JIRA 4729 end
                Globals.objPCL50.PMSPCL50TranEntTm = Globals.sHHNNSS000000Now;
                sAddedByUser = xmlLD.AddedByUser;
                Globals.objPCL50.PMSPCL50RecStatus = xmlLD.ReserveStatus_Code;

                SetReserveStatus();

                Globals.objPCL50.PMSPCL50ClaimTrans = sActivityType;
                Globals.objPCL50.PMSPCL50ResvNo = Globals.iResvNo;
                if (xmlLD.ReserveAdjusterExaminerCd != string.Empty)
                {
                    Globals.objPCL50.PMSPCL50ExaminerCd = xmlLD.ReserveAdjusterExaminerCd;
                }
                else
                {
                    Globals.objPCL50.PMSPCL50ExaminerCd = Globals.ExaminerCd;
                }


                if ((xmlLD.RsvTransNode != null))
                {
                    ParseReserveNode(xmlLD);
                }

                if ((xmlLD.PayTransNode != null))
                {
                    ParsePaymentNode(xmlLD);
                }

                //Offsetting transactions for Offset/Onset processing are
                //ignored in the POINT interface.
                //The old claim is automatically offset in the ProcessOffsetOnset functionality
                if (Globals.bOffsetOnsetLossDetail && Globals.bProcessOffsetOnset)
                {
                    return;
                }

                LoadDatabase.LocateReservePayment();

                if (Globals.bProcessCheckDraft == true)
                {
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlDrft, ScriptingFunctions.conScpEvtStart, xmlLD);
                    MapDraftFileKey();
                    LoadDatabase.AddNewDraft();
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlChck, ScriptingFunctions.conScpEvtStart, xmlLD);

                    if (Globals.objDRFT.DRFTCheckNo != "0000000")
                    {
                        MapCheckFile();
                        LoadDatabase.LocateCheckFile();
                    }
                    //SI06203
                    //if (Globals.iResvNo == Globals.INDEMNITY_RESERVE)
                    //{
                    //    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlIndm, ScriptingFunctions.conScpEvtStart, xmlLD);
                    //    MapIndemnityFile();
                    //    LoadDatabase.LocateIndemnity();
                    //    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlIndm, ScriptingFunctions.conScpEvtEnd, xmlLD);
                    //}
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlChck, ScriptingFunctions.conScpEvtEnd, xmlLD);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlDrft, ScriptingFunctions.conScpEvtEnd, xmlLD);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtEnd, xmlLD, "LossDetail");
                Globals.dbg.PopProc();
            }
        }

        private static void ParseReserveNode(CCP.XmlComponents.XMLLossDetail xmlLD)
        {

            Globals.dbg.PushProc("ParseReserveNode");

            Globals.bProcessingReserve = true;
            Globals.bProcessingPayment = false;

            ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtStart, xmlLD, "Reserve");

            Globals.objPCL50.PMSPCL50CurrResv = Convert.ToSingle(xmlLD.ReserveBalance);
            Globals.objPCL50.PMSPCL50ResvPayAmt = Convert.ToSingle(xmlLD.ReserveChange);
            Globals.cClmReserveAmount = Globals.cClmReserveAmount + Globals.objPCL50.PMSPCL50CurrResv;
            Globals.cClmReserveChange = Globals.cClmReserveChange + Globals.objPCL50.PMSPCL50ResvPayAmt;

            if (Globals.sResvType == "R")
            {
                Globals.objPCL50.PMSPCL50CurrResv = Globals.objPCL50.PMSPCL50CurrResv * -1;
                Globals.objPCL50.PMSPCL50ResvPayAmt = Globals.objPCL50.PMSPCL50ResvPayAmt * -1;
            }

            switch (sActivityType)
            {
                case "NC":
                case "CL":
                    Globals.objPCL50.PMSPCL50ResvPayAmt = 0;
                    break;
                default:
                    break;
            }

            ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtEnd, xmlLD, "Reserve");
            Globals.dbg.PopProc();
        }

        private static void ParsePaymentNode(CCP.XmlComponents.XMLLossDetail xmlLD)
        {
            string sReportable = null;
            string sAccountName = null;
            string[] vArray = null;
            string sTemp = null;
            bool rst = false;
            string sSpecialPayee = null;
            string sMemoPhrase = null;
            string sMemoPart = null;
            int iMemoIdx = 0;
            int iMemoPos = 0;

            try
            {
                Globals.dbg.PushProc("ParsePaymentNode:ID." + xmlLD.LossDetailID);

                Globals.bProcessingReserve = false;
                Globals.bProcessingPayment = true;
                sReportable = "";

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtStart, xmlLD, "Payment");

                Globals.objPCL50.PMSPCL50ResvPayAmt = Convert.ToSingle(xmlLD.PayAmount);
                Globals.objPCL50.PMSPCL50TotPayment = Convert.ToSingle(Globals.objPCL50.PMSPCL50ResvPayAmt);
                /*Added by gbindra on 10/30/2014 start*/
                if (string.Compare(Globals.objConfig.ConfigElement.UploadVoucher, "-1", true) == 0)
                {
                    Globals.objDRFT.DRFTDrftAmt = Convert.ToSingle(xmlLD.Voucher.Amount);
                    Globals.objAP00.PMSPAP00CheckAmt = Convert.ToSingle(xmlLD.Voucher.Amount);
                }
                else
                {
                    Globals.objDRFT.DRFTDrftAmt = Globals.objPCL50.PMSPCL50TotPayment;
                    Globals.objAP00.PMSPAP00CheckAmt = Globals.objPCL50.PMSPCL50TotPayment;
                }
                /*Added by gbindra on 10/30/2014 end*/
                Globals.objDRFT.DRFTCheckNo = string.IsNullOrEmpty(xmlLD.Voucher.DocumentNumber) ? string.Empty : xmlLD.Voucher.DocumentNumber.PadLeft(7, '0');// string.Format("0000000", xmlLD.Voucher.DocumentNumber);
                Globals.objDRFT.DRFTDrftNumber = Globals.objDRFT.DRFTCheckNo;
                Globals.objAP00.PMSPAP00CheckNo = Globals.objDRFT.DRFTCheckNo;
                Globals.objDRFT.DRFTItemNo = xmlLD.Voucher.ControlNumber.PadLeft(7, '0'); ;//string.Format("0000000", xmlLD.Voucher.ControlNumber);
                Globals.objDRFT.DRFTSERDTEFROM = string.IsNullOrEmpty(xmlLD.Voucher.SERDTEFROM) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlLD.Voucher.SERDTEFROM)));

                Globals.objDRFT.DRFTSERDTETO = string.IsNullOrEmpty(xmlLD.Voucher.SERDTETO) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlLD.Voucher.SERDTETO)));

                xmlLD.Voucher.getInvoice(false);


                Globals.objDRFT.DRFTInvoiceNo1 = xmlLD.Voucher.InvoiceNumber;
                Globals.dbg.LogEntry("xmlLD.Voucher.InvoiceNumber");
                Globals.dbg.LogEntry(xmlLD.Voucher.InvoiceNumber);

                Globals.objAP00.PMSPAP00ItemNo = Globals.objDRFT.DRFTItemNo;
                sTemp = BoolToYN(xmlLD.Voucher.ManualInd);
                //if (Globals.bPaymentOffset == true)
                //{
                //    Globals.objDRFT.DRFTPayStatus = "V";
                //    Globals.objAP00.PMSPAP00CheckReconciled = 2;
                //}
                //else if (sTemp == "Y")
                //{
                //    Globals.objDRFT.DRFTPayStatus = "M";
                //}
                //else
                //{
                //    Globals.objDRFT.DRFTPayStatus = "P";
                //}
                if (sTemp == "Y")
                {
                    Globals.objDRFT.DRFTPayStatus = "M";
                }
                //else if (Globals.bPaymentOffset == true)
                //    {
                //    Globals.objDRFT.DRFTPayStatus = "V";
                //    Globals.objAP00.PMSPAP00CheckReconciled = 2;
                //    }
                else
                {
                    Globals.objDRFT.DRFTPayStatus = "P";
                }
                Globals.objAP00.PMSPAP00PayStatus = Globals.objDRFT.DRFTPayStatus;
                Globals.objAP00.PMSPAP00CheckDate = string.IsNullOrEmpty(xmlLD.Voucher.VoucherDate) ? string.Empty : Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Globals.GetDateTime(xmlLD.Voucher.VoucherDate)));

                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CheckDate))
                {
                    Globals.objDRFT.DRFTDrftPrtDte = Convert.ToInt64(Globals.objAP00.PMSPAP00CheckDate);
                }
                else
                {
                    Globals.objAP00.PMSPAP00CheckDate = Convert.ToString(Globals.objPCL50.PMSPCL50TranEntDt);
                    Globals.objDRFT.DRFTDrftPrtDte = Convert.ToInt64(Globals.objAP00.PMSPAP00CheckDate);
                }
                if (string.IsNullOrEmpty(xmlLD.Voucher.OriginatingBankNumber))
                {
                    Globals.objDRFT.DRFTBankCode = "DFLT";
                }
                else
                {
                    vArray = xmlLD.Voucher.OriginatingBankNumber.Split(':');
                    Globals.objDRFT.DRFTBankCode = Convert.ToString(vArray[0]);
                }
                Globals.objAP00.PMSPAP00BankCode = Globals.objDRFT.DRFTBankCode;

                //Split up Memo Phrase to DrftFile
                sMemoPhrase = StringUtils.Trim(xmlLD.Voucher.Memo);
                iMemoIdx = 0;
                while (!string.IsNullOrEmpty(sMemoPhrase))
                {
                    iMemoPos = StringUtils.InStrRev(sMemoPhrase, " ");
                    if (iMemoPos <= 0)
                        iMemoPos = 72;
                    sMemoPart = StringUtils.Trim(StringUtils.Left(sMemoPhrase, iMemoPos));
                    sMemoPhrase = StringUtils.Trim(StringUtils.Mid(sMemoPhrase, iMemoPos + 1));
                    iMemoIdx = iMemoIdx + 1;
                    switch (iMemoIdx)
                    {
                        case 1:
                            Globals.objDRFT.DRFTMemo1 = sMemoPart;
                            break;
                        case 2:
                            Globals.objDRFT.DRFTMemo2 = sMemoPart;
                            break;
                        case 3:
                            Globals.objDRFT.DRFTNote1 = sMemoPart;
                            break;
                        case 4:
                            Globals.objDRFT.DRFTNote2 = sMemoPart;
                            break;
                    }
                }

                rst = true;
                iPayeeNo = 1;
                while ((xmlLD.Voucher.getPayee(rst) != null))
                {
                    rst = false;
                    ParseEntityNode(xmlLD.Voucher.PartyInvolved.Entity);

                    if (Conversion.ConvertObjToBool(xmlLD.Voucher.PartyInvolved.DataItem.DataItemValue("TAX_REPORTABLE")) == true)
                    {
                        Globals.sPayCode = "P";
                    }
                    else
                    {
                        Globals.sPayCode = "X";
                    }
                    MapDraftPayee();
                    if (xmlLD.Voucher.PartyInvolved.EntityID != xmlLD.ClaimantEntityID && Globals.sPayCode == "P")
                    {
                        LoadDatabase.UpdateVendorFile(iPayeeNo);
                        if (!string.IsNullOrEmpty(Globals.strVenNumber) & string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50Vendor))
                        {
                            Globals.objPCL50.PMSPCL50Vendor = Globals.strVenNumber;
                        }
                    }
                    iPayeeNo = iPayeeNo + 1;
                }

                if ((xmlLD.Voucher.getMailToEntity(true) != null))
                {
                    ParseEntityNode(xmlLD.Voucher.PartyInvolved.Entity);
                    MapDraftMailto();
                    Globals.objDRFT.DRFTMailToCode = "P";
                }

                if (Conversion.ConvertObjToBool(xmlLD.Voucher.PayeeVerbiageManual) == true)
                {
                    sSpecialPayee = xmlLD.Voucher.PayeeVerbiage;
                    if (!string.IsNullOrEmpty(sSpecialPayee))
                    {
                        Globals.objDRFT.DRFTPayeeName1 = StringUtils.Mid(sSpecialPayee, 1, 30);
                        Globals.objDRFT.DRFTPayeeAdd11 = StringUtils.Mid(sSpecialPayee, 31, 30);
                        Globals.objDRFT.DRFTPayeeName2 = StringUtils.Mid(sSpecialPayee, 61, 30);
                        Globals.objDRFT.DRFTPayeeAdd21 = StringUtils.Mid(sSpecialPayee, 91, 30);
                        Globals.objDRFT.DRFTPayeeName3 = StringUtils.Mid(sSpecialPayee, 121, 30);
                        Globals.objDRFT.DRFTPayeeAdd31 = StringUtils.Mid(sSpecialPayee, 151, 30);
                        Globals.objDRFT.DRFTPayToCode1 = "S";
                    }
                }


                sReportable = xmlLD.ReportableType_Code;

                if ((sActivityType == "PO") | (sActivityType == "OA"))
                {
                    Globals.cClmPaidAmount = Globals.cClmPaidAmount - Globals.objPCL50.PMSPCL50ResvPayAmt;
                }
                else
                {
                    Globals.cClmPaidAmount = Globals.cClmPaidAmount + Globals.objPCL50.PMSPCL50ResvPayAmt;
                }

                if (Globals.sResvType == "R")
                {
                    if (sActivityType == "PO")
                    {
                        Globals.objPCL50.PMSPCL50ResvPayAmt = Globals.objPCL50.PMSPCL50ResvPayAmt * -1;
                    }
                    else if (sActivityType == "OA")
                    {
                        Globals.objPCL50.PMSPCL50ResvPayAmt = Globals.objPCL50.PMSPCL50ResvPayAmt * -1;
                    }
                }
                else
                {
                    if (sActivityType == "PO")
                    {
                        Globals.objPCL50.PMSPCL50TotPayment = Globals.objPCL50.PMSPCL50TotPayment * -1;
                    }
                    else if (sActivityType == "OA")
                    {
                        Globals.objPCL50.PMSPCL50TotPayment = Globals.objPCL50.PMSPCL50TotPayment * -1;
                    }
                    else
                    {
                        Globals.objPCL50.PMSPCL50ResvPayAmt = Globals.objPCL50.PMSPCL50ResvPayAmt * -1;
                    }
                }

                if (string.IsNullOrEmpty(sTransType))
                {
                    Globals.objPCL50.PMSPCL50PaymentTyp = sActivityType;
                }
                else
                {
                    Globals.objPCL50.PMSPCL50PaymentTyp = sTransType;
                }
            }
            catch (Exception ex) { throw (ex); }
            finally
            {

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlTrns, ScriptingFunctions.conScpEvtEnd, xmlLD, "Payment");
                Globals.dbg.PopProc();
            }
        }
        private static void SetReserveStatus()
        {
            string sTemp = null;
            try
            {
                Globals.dbg.PushProc("SetReserveStatus", sActivityType, Globals.sResvType, Globals.objPCL50.PMSPCL50RecStatus, "");

                Globals.bProcessCheckDraft = false;
                Globals.bPaymentOffset = false;
                //Not Used anywhere bOffsetOnsetPaymentOffset = False

                switch (sActivityType)
                {
                    case "OC":
                    case "NO":
                        Globals.objPCL50.PMSPCL50RecStatus = "O";
                        Globals.bStatusChangeTrans = true;
                        break;
                    case "RH":
                    case "RO":
                        Globals.objPCL50.PMSPCL50RecStatus = "R";
                        Globals.bStatusChangeTrans = true;
                        sActivityType = "RO";
                        break;
                    case "CL":
                        Globals.objPCL50.PMSPCL50RecStatus = "C";
                        Globals.bStatusChangeTrans = true;
                        break;
                    case "NC":
                        Globals.objPCL50.PMSPCL50RecStatus = "C";
                        Globals.bStatusChangeTrans = true;
                        break;
                    case "FP":
                        Globals.objPCL50.PMSPCL50RecStatus = "C";
                        Globals.bStatusChangeTrans = true;
                        Globals.bProcessCheckDraft = true;
                        break;
                    case "FF":
                        Globals.objPCL50.PMSPCL50RecStatus = "C";
                        Globals.bStatusChangeTrans = true;
                        Globals.bProcessCheckDraft = true;
                        break;
                    case "PP":
                        Globals.objPCL50.PMSPCL50RecStatus = "O";
                        Globals.bStatusChangeTrans = false;
                        Globals.bProcessCheckDraft = true;
                        break;
                    case "SP":
                        Globals.bStatusChangeTrans = false;
                        Globals.bProcessCheckDraft = true;
                        break;
                    //Case "PO"                                        
                    case "PO":
                    case "OA":
                        Globals.bStatusChangeTrans = false;
                        Globals.bPaymentOffset = true;
                        Globals.bProcessCheckDraft = true;
                        break;
                    default:
                        Globals.bStatusChangeTrans = false;
                        break;
                }

                if (Globals.sResvType == "R")
                {
                    Globals.bProcessCheckDraft = false;
                }

                if (!string.IsNullOrEmpty(sTemp) & sTemp != Globals.objPCL50.PMSPCL50RecStatus)
                {
                    Globals.objPCL50.PMSPCL50RecStatus = sTemp;
                    Globals.bStatusChangeTrans = false;
                }
            }
            catch (Exception ex) { throw (ex); }
            finally
            {

                Globals.dbg.PopProc(Globals.objPCL50.PMSPCL50RecStatus, "BST=" + Convert.ToString(Globals.bStatusChangeTrans), "PCD=" + Convert.ToString(Globals.bProcessCheckDraft), "PO=" + Convert.ToString(Globals.bPaymentOffset));
            }
        }

        public static void MapDraftFileKey()
        {
            try
            {
                Globals.dbg.PushProc("MapDraftFileKey");

                {
                    Globals.objDRFT.DRFTSymbol = Globals.objPCL42.PMSPCL42Symbol;
                    Globals.objDRFT.DRFTPolicyNo = Globals.objPCL42.PMSPCL42PolicyNo;
                    Globals.objDRFT.DRFTModule = Globals.objPCL42.PMSPCL42Module;
                    Globals.objDRFT.DRFTMasterCo = Globals.objPCL42.PMSPCL42MasterCo;
                    Globals.objDRFT.DRFTLocation = Globals.objPCL42.PMSPCL42Location;
                    Globals.objDRFT.DRFTClaim = Globals.sClaimNumber;
                    Globals.objDRFT.DRFTClmtSeq = Globals.sCLMTSEQ;
                    Globals.objDRFT.DRFTPolCovSeq = Globals.sPolCovSeq;
                    Globals.objDRFT.DRFTResvNo = Globals.iResvNo;
                    Globals.objDRFT.DRFTTransSeq = Globals.objPCL50.PMSPCL50TransSeq;
                    Globals.objDRFT.DRFTTransCode = sActivityType;
                }
            }
            finally
            {

                Globals.dbg.PopProc();
            }
        }

        public static void MapDraftPayee()
        {
            try
            {
                Globals.dbg.PushProc("MapDraftPayee");
                if (iPayeeNo == 1)
                {
                    {
                        Globals.objDRFT.DRFTPayeeName1 = StringUtils.Trim(Globals.strFullName);
                        Globals.objDRFT.DRFTPayeeAdd11 = Globals.strAddress[0];
                        Globals.objDRFT.DRFTPayeeAdd12 = Globals.strAddress[1];
                        Globals.objDRFT.DRFTPayeeAdd13 = Globals.strCity + ", " + Globals.strStateCode;
                        Globals.objDRFT.DRFTPayeeZip1 = Globals.strPostalCd;
                        Globals.objDRFT.DRFTPayeeNo1 = Globals.strVenNumber;
                        Globals.objDRFT.DRFTPayToCode1 = Globals.sPayCode;
                    }
                }
                else if (iPayeeNo == 2)
                {
                    {
                        Globals.objDRFT.DRFTPayeeName2 = StringUtils.Trim(Globals.strFullName);
                        Globals.objDRFT.DRFTPayeeAdd21 = Globals.strAddress[0];
                        Globals.objDRFT.DRFTPayeeAdd22 = Globals.strAddress[1];
                        Globals.objDRFT.DRFTPayeeAdd23 = Globals.strCity + ", " + Globals.strStateCode;
                        Globals.objDRFT.DRFTPayeeZip2 = Globals.strPostalCd;
                        Globals.objDRFT.DRFTPayeeNo2 = Globals.strVenNumber;
                        Globals.objDRFT.DRFTPayToCode2 = Globals.sPayCode;
                    }
                }
                else if (iPayeeNo == 3)
                {
                    {
                        Globals.objDRFT.DRFTPayeeName3 = StringUtils.Trim(Globals.strFullName);
                        Globals.objDRFT.DRFTPayeeAdd31 = Globals.strAddress[0];
                        Globals.objDRFT.DRFTPayeeAdd32 = Globals.strAddress[1];
                        Globals.objDRFT.DRFTPayeeAdd33 = Globals.strCity + ", " + Globals.strStateCode;
                        Globals.objDRFT.DRFTPayeeZip3 = Globals.strPostalCd;
                        Globals.objDRFT.DRFTPayeeNo3 = Globals.strVenNumber;
                        Globals.objDRFT.DRFTPayToCode3 = Globals.sPayCode;
                    }
                }
            }
            finally
            {


                Globals.dbg.PopProc();
            }
        }

        public static void MapDraftMailto()
        {
            try
            {
                Globals.dbg.PushProc("MapDraftMailto");

                Globals.objDRFT.DRFTMailToName = StringUtils.Trim(Globals.strFullName);

                if (string.IsNullOrEmpty(StringUtils.Trim(Globals.strAddress[0] + Globals.strAddress[1] + Globals.strCity + Globals.strStateCode + Globals.strPostalCd)))
                {
                    {
                        Globals.objDRFT.DRFTMailToAdd1 = Globals.objDRFT.DRFTPayeeAdd11;
                        Globals.objDRFT.DRFTMailToAdd2 = Globals.objDRFT.DRFTPayeeAdd12;
                        Globals.objDRFT.DRFTMailToAdd3 = Globals.objDRFT.DRFTPayeeAdd13;
                        Globals.objDRFT.DRFTMailToZip = Globals.objDRFT.DRFTPayeeZip1;
                    }
                }
                else
                {
                    {
                        Globals.objDRFT.DRFTMailToAdd1 = Globals.strAddress[0];
                        Globals.objDRFT.DRFTMailToAdd2 = Globals.strAddress[1];
                        Globals.objDRFT.DRFTMailToAdd3 = Globals.strCity + ", " + Globals.strStateCode;
                        Globals.objDRFT.DRFTMailToZip = Globals.strPostalCd;
                    }
                }
            }
            finally
            {
                Globals.dbg.PopProc();
            }
        }

        public static void MapCheckFile()
        {
            string spaces = "                              "; // 30 spaces
            try
            {
                Globals.dbg.PushProc("MapCheckFile");
                //Globals.objLogWriter.Write(" MapCheckFile method start " + Environment.NewLine); //commented the log statement, not required now
                Globals.objAP00.PMSPAP00PayType = "2";
                Globals.objAP00.PMSPAP00Sym = Globals.objPCL42.PMSPCL42Symbol;
                Globals.objAP00.PMSPAP00PolNo = Globals.objPCL42.PMSPCL42PolicyNo;
                Globals.objAP00.PMSPAP00Mod = Globals.objPCL42.PMSPCL42Module;
                Globals.objAP00.PMSPAP00Mco = Globals.objPCL42.PMSPCL42MasterCo;
                Globals.objAP00.PMSPAP00Loc = Globals.objPCL42.PMSPCL42Location;
                Globals.objAP00.PMSPAP00PayeeName = Globals.objDRFT.DRFTPayeeName1;
                Globals.objAP00.PMSPAP00Payee2 = Globals.objDRFT.DRFTMailToAdd1;
                Globals.objAP00.PMSPAP00MailLine3 = Globals.objDRFT.DRFTMailToAdd2;
                Globals.objAP00.PMSPAP00MailLine4 = Globals.objDRFT.DRFTMailToAdd3;
                Globals.objAP00.PMSPAP00ZipCode = Globals.objDRFT.DRFTMailToZip;
                Globals.objAP00.PMSPAP00Claim = Globals.sClaimNumber;
                Globals.objAP00.PMSPAP00ClmtSeq = Globals.sCLMTSEQ;
                Globals.objAP00.PMSPAP00PolCovSeq = Globals.sPolCovSeq;
                Globals.objAP00.PMSPAP00ResvNo = Globals.iResvNo;
                Globals.objAP00.PMSPAP00TransSeq = Globals.objPCL50.PMSPCL50TransSeq;

                if (Globals.objAP00.PMSPAP00Payee2 == spaces) // 30 spaces
                {
                }
                else
                {
                    Globals.objAP00.PMSPAP00MailLine3 = Globals.objAP00.PMSPAP00Payee2;
                    Globals.objAP00.PMSPAP00Payee2 = spaces;
                }
                // rrachev JIRA RMA-7701 Begin
                //Globals.objLogWriter.Write("Value of UPLOAD_PAYEE2_PMSPAP00 in POLICY_X_WEB  = " + Globals.objConfig.ConfigElement.UploadPayee2PMSPAP00 + Environment.NewLine); //commented the log statement, not required now.
                if (string.Compare(Globals.objConfig.ConfigElement.UploadPayee2PMSPAP00, "-1", true) == 0)
                {
                    Globals.objAP00.PMSPAP00Payee2 = Globals.objDRFT.DRFTPayeeName2;
                }
                //Globals.objLogWriter.Write("Value of Globals.objAP00.PMSPAP00Payee2  = " + Globals.objAP00.PMSPAP00Payee2 + Environment.NewLine);// commented the log statement, not required now
                // rrachev JIRA RMA-7701 End
                //Globals.objLogWriter.Write(" MapCheckFile method end " + Environment.NewLine);// commented the log statement, not required now.
            }
            finally
            {

                Globals.dbg.PopProc();
            }
        }

        public static void MapIndemnityFile()
        {
            try
            {
                Globals.dbg.PushProc("MapIndemnityFile");

                {
                    Globals.objPCL92.PMSPCL92Symbol = Globals.objPCL42.PMSPCL42Symbol;
                    Globals.objPCL92.PMSPCL92PolicyNo = Globals.objPCL42.PMSPCL42PolicyNo;
                    Globals.objPCL92.PMSPCL92Module = Globals.objPCL42.PMSPCL42Module;
                    Globals.objPCL92.PMSPCL92MasterCo = Globals.objPCL42.PMSPCL42MasterCo;
                    Globals.objPCL92.PMSPCL92Location = Globals.objPCL42.PMSPCL42Location;
                    Globals.objPCL92.PMSPCL92Claim = Globals.sClaimNumber;
                    Globals.objPCL92.PMSPCL92ClmtSeq = Globals.sCLMTSEQ;
                    Globals.objPCL92.PMSPCL92PolCovSeq = Globals.sPolCovSeq;
                    Globals.objPCL92.PMSPCL92Resvno = Globals.iResvNo;
                    Globals.objPCL92.PMSPCL92PaymentTyp = Globals.objDRFT.DRFTPayType;
                    Globals.objPCL92.PMSPCL92Payments = 0;
                    Globals.objPCL92.PMSPCL92PaymntFreq = " ";
                    Globals.objPCL92.PMSPCL92PaymntRate = Globals.objDRFT.DRFTDrftAmt;
                    //SI05157.PMSPCL92WeeklyWage = Val(objPCL14.PMSPCL14LsCurrAww)
                    Globals.objPCL92.PMSPCL92WeeklyWage = Globals.objPCL14.PMSPCL14LsCurrAww;
                    //SI05157
                    Globals.objPCL92.PMSPCL92StartDate = Globals.objDRFT.DRFTDrftPrtDte;
                    Globals.objPCL92.PMSPCL92DrftPrtDte = Globals.objDRFT.DRFTDrftPrtDte;
                    Globals.objPCL92.PMSPCL92BankCode = Globals.objDRFT.DRFTBankCode;
                    Globals.objPCL92.PMSPCL92DrftAmt = Globals.objAP00.PMSPAP00CheckAmt;
                    Globals.objPCL92.PMSPCL92DrftNumber = Globals.objDRFT.DRFTDrftNumber;
                    Globals.objPCL92.PMSPCL92PPCode = Globals.objDRFT.DRFTPPCode;
                    Globals.objPCL92.PMSPCL92PayePhrase = Globals.objDRFT.DRFTPayePhrase;
                    Globals.objPCL92.PMSPCL92PayToCode1 = Globals.objDRFT.DRFTPayToCode1;
                    Globals.objPCL92.PMSPCL92PayeeNo1 = Globals.objDRFT.DRFTPayeeNo1;
                    Globals.objPCL92.PMSPCL92PayeeName1 = Globals.objDRFT.DRFTPayeeName1;
                    Globals.objPCL92.PMSPCL92PayeeAdd11 = Globals.objDRFT.DRFTPayeeAdd11;
                    Globals.objPCL92.PMSPCL92PayeeAdd12 = Globals.objDRFT.DRFTPayeeAdd12;
                    Globals.objPCL92.PMSPCL92PayeeAdd13 = Globals.objDRFT.DRFTPayeeAdd13;
                    Globals.objPCL92.PMSPCL92PayeeZip1 = Globals.objDRFT.DRFTPayeeZip1;
                    Globals.objPCL92.PMSPCL92PayToCode2 = Globals.objDRFT.DRFTPayToCode2;
                    Globals.objPCL92.PMSPCL92PayeeNo2 = Globals.objDRFT.DRFTPayeeNo2;
                    Globals.objPCL92.PMSPCL92PayeeName2 = Globals.objDRFT.DRFTPayeeName2;
                    Globals.objPCL92.PMSPCL92PayeeAdd21 = Globals.objDRFT.DRFTPayeeAdd21;
                    Globals.objPCL92.PMSPCL92PayeeAdd22 = Globals.objDRFT.DRFTPayeeAdd22;
                    Globals.objPCL92.PMSPCL92PayeeAdd23 = Globals.objDRFT.DRFTPayeeAdd23;
                    Globals.objPCL92.PMSPCL92PayeeZip2 = Globals.objDRFT.DRFTPayeeZip2;
                    Globals.objPCL92.PMSPCL92PayToCode3 = Globals.objDRFT.DRFTPayToCode3;
                    Globals.objPCL92.PMSPCL92PayeeNo3 = Globals.objDRFT.DRFTPayeeNo3;
                    Globals.objPCL92.PMSPCL92PayeeName3 = Globals.objDRFT.DRFTPayeeName3;
                    Globals.objPCL92.PMSPCL92PayeeAdd31 = Globals.objDRFT.DRFTPayeeAdd31;
                    Globals.objPCL92.PMSPCL92PayeeAdd32 = Globals.objDRFT.DRFTPayeeAdd32;
                    Globals.objPCL92.PMSPCL92PayeeAdd33 = Globals.objDRFT.DRFTPayeeAdd33;
                    Globals.objPCL92.PMSPCL92PayeeZip3 = Globals.objDRFT.DRFTPayeeZip3;
                    Globals.objPCL92.PMSPCL92MailToCode = Globals.objDRFT.DRFTMailToCode;
                    Globals.objPCL92.PMSPCL92MailToNo = Globals.objDRFT.DRFTMailToNo;
                    Globals.objPCL92.PMSPCL92MailToName = Globals.objDRFT.DRFTMailToName;
                    Globals.objPCL92.PMSPCL92MailToAdd1 = Globals.objDRFT.DRFTMailToAdd1;
                    Globals.objPCL92.PMSPCL92MailToAdd2 = Globals.objDRFT.DRFTMailToAdd2;
                    Globals.objPCL92.PMSPCL92MailToAdd3 = Globals.objDRFT.DRFTMailToAdd3;
                    Globals.objPCL92.PMSPCL92MailToZip = Globals.objDRFT.DRFTMailToZip;
                    Globals.objPCL92.PMSPCL92MemoCode = Globals.objDRFT.DRFTMemoCode;
                    Globals.objPCL92.PMSPCL92Memo1 = Globals.objDRFT.DRFTMemo1;
                    Globals.objPCL92.PMSPCL92Memo2 = Globals.objDRFT.DRFTMemo2;
                    Globals.objPCL92.PMSPCL92Note1 = Globals.objDRFT.DRFTNote1;
                    Globals.objPCL92.PMSPCL92Note2 = Globals.objDRFT.DRFTNote2;
                }
            }
            finally
            {

                Globals.dbg.PopProc();
            }
        }

        private static string TranslateStateCode(string sStCd)
        {
            string sTemp = null;

            if (string.IsNullOrEmpty(sTemp))
                sTemp = sStCd;
            return sTemp;
        }

        private static void BalanceClaim()
        {

            ClaimTotalsData objClmTot = null;
            int iSuccess = 0;
            bool bAbortRun = false;
            try
            {
                Globals.dbg.PushProc("BalanceClaim");

                objClmTot = CCP.LossBalance.LossBalance.Instance.TotalsByClaim.Item(Globals.sOrigClaimNumber);

                bAbortRun = false;
                Globals.dbg.IgnoreErrors = true;

                if (Globals.lClmTransactionCount != objClmTot.TransactionCount)
                {
                    bAbortRun = true;
                }

                if (Globals.cClmPaidAmount != objClmTot.PaidAmount)
                {
                    bAbortRun = true;
                }

                if (Globals.cClmReserveAmount != objClmTot.ReserveAmount)
                {
                    bAbortRun = true;
                }

                if (Globals.cClmReserveChange != objClmTot.ReserveChange)
                {
                    bAbortRun = true;
                }

                if (Globals.lClmOffsetOnsetCount != objClmTot.OffsetOnsetCount)
                {
                    bAbortRun = true;
                }

                if (Globals.lClmUnitStatCount != objClmTot.UnitStatCount)
                {
                    bAbortRun = true;
                }

                Globals.dbg.IgnoreErrors = false;

                if (bAbortRun == true)
                {
                    //iSuccess = dbg.CErrors.ProcessAppError(ERRC_PROCESS_ABORTED, Constants.vbOKOnly, "", "", "Claim " + sClaimNumber + " Out Of Balance");
                }
            }
            catch (Exception ex) { throw (ex); }
            finally
            {
                Globals.dbg.PopProc();
            }

        }

        private static string BoolToYN(object vIndicator)
        {
            try
            {
                Globals.dbg.PushProc("BoolToYN");

                if (Convert.ToBoolean(objCfunctions.CnvData(Convert.ToString(vIndicator), CommonFunctions.ufDataTypes.ufdtBool)) == true)
                {
                    return "Y";
                }
                else
                {
                    return "N";
                }
            }
            finally
            {

                Globals.dbg.PopProc();
            }

        }

        public static string GetClaimType(string sClaimTypeCD, string sLOB)
        {
            Globals.dbg.PushProc("GetClaimType");
            Globals.dbg.LogEntry("GetClaimType CAlled");
            return StringUtils.Trim(sClaimTypeCD);
        }

        private static void InitializeParsing()
        {
            // Perform initialization routines
            string sTemp = null;
            Parameters objParams = Globals.GetParams();
            Globals.dbg.PushProc("InitializeParsing");

            if (string.IsNullOrEmpty(Globals.sADJRole))
                Globals.sADJRole = Globals.conADJ;


            if (string.IsNullOrEmpty(Globals.sEXMRole))
                Globals.sEXMRole = Globals.conEXM;

            if (string.IsNullOrEmpty(Globals.sPHLDRole))
                Globals.sPHLDRole = Globals.conPHLD;

            Globals.dbg.PopProc();
        }

        //Routine updates the Adjuster on the PMSPAD00 file.
        public static void ParseAdjuster(CCP.XmlComponents.XMLPartyInvolved oPI)
        {
            Parameters objParams = Globals.GetParams();
            ParseEntityNode(oPI.Entity);

            if ((oPI.Entity.findIDNumberByType(objParams.Parameter(Globals.conExmrLit)) != null))
            {
                Globals.objAD00.PMSPAD00AdjustorNbr = Globals.XMLClaim.PartyInvolved.Entity.IDNumber;
            }
            else
            {
                Globals.objAD00.PMSPAD00AdjustorNbr = Globals.strVenNumber;
            }
            Globals.objAD00.PMSPAD00Name = Globals.strFullName;
            Globals.objAD00.PMSPAD00Address = StringUtils.Trim(Globals.strAddress[0] + " " + Globals.strAddress[1]);
            Globals.objAD00.PMSPAD00CityState = Globals.strCity + ", " + Globals.strStateCode;
            Globals.objAD00.PMSPAD00ZipCode = Globals.strPostalCd;
            Globals.objAD00.PMSPAD00EntityID = Globals.lEntityID;
            Globals.objAD00.PMSPAD00TaxIdSSN = Globals.strTaxId;
            Globals.objAD00.PMSPAD00LongTaxID = Globals.strLongTaxId;
            Globals.objAD00.PMSPAD00SSN = Globals.strTaxIdType;
            Globals.objAD00.PMSPAD00TypeAdjustor = Globals.strCategory;
            Globals.objAD00.PMSPAD00PhoneNumber = Globals.strPhone[0];
            Globals.objAD00.NumberType = objParams.Parameter(Globals.conExmrLit);


            LoadDatabase.LocateVendor();

            Globals.strVenNumber = Globals.objAD00.PMSPAD00AdjustorNbr;

        }

        public static void ParseVendorEntities()
        {
            CCP.XmlComponents.XMLEntity xmlEntity = null;
            string sCatCode = null;
            bool rst = false;
            bool bProcessEntity = false;
            Parameters objParams = Globals.GetParams();
            Globals.dbg.PushProc("ParseVendor");

            ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlLoad, ScriptingFunctions.conScptlvlEntity);

            xmlEntity = new CCP.XmlComponents.XMLEntity();
            xmlEntity.getEntity(true);
            bProcessEntity = false;

            while (!string.IsNullOrEmpty(xmlEntity.EntityID))
            {

                rst = true;
                while ((xmlEntity.getCategory(rst) != null))
                {
                    rst = false;
                    sCatCode = string.IsNullOrEmpty(xmlEntity.CategoryCode) ? string.Empty : xmlEntity.CategoryCode.ToUpper();
                    if (Convert.ToBoolean(StringUtils.InStr(":" + ((string.IsNullOrEmpty(objParams.Parameter(Globals.conVendorCategorySW))) ? string.Empty : objParams.Parameter(Globals.conVendorCategorySW).ToUpper()) + ":", ":" + sCatCode + ":")))
                    {
                        bProcessEntity = true;
                    }
                }

                if (bProcessEntity)
                {
                    ParseEntityNode(xmlEntity);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlVndr, ScriptingFunctions.conScpEvtStart);

                    {
                        Globals.objAD00.PMSPAD00Name = xmlEntity.Name;
                        Globals.objAD00.PMSPAD00Address = StringUtils.Trim(Globals.strAddress[0] + " " + Globals.strAddress[1] + " " + Globals.strAddress[2] + " " + Globals.strAddress[3]);
                        Globals.objAD00.PMSPAD00CityState = Globals.strCity + " " + Globals.strStateCode;
                        Globals.objAD00.PMSPAD00ZipCode = Globals.strPostalCd;
                        Globals.objAD00.PMSPAD00EntityID = Globals.lEntityID;
                        Globals.objAD00.PMSPAD00AdjustorNbr = Globals.strVenNumber;
                        Globals.objAD00.PMSPAD00TaxIdSSN = Globals.strTaxId;
                        Globals.objAD00.PMSPAD00LongTaxID = Globals.strLongTaxId;
                        Globals.objAD00.PMSPAD00SSN = Globals.strTaxIdType;
                        Globals.objAD00.PMSPAD00TypeAdjustor = Globals.strCategory;
                        Globals.objAD00.PMSPAD00PhoneNumber = Globals.strPhone[0];
                        Globals.objAD00.NumberType = objParams.Parameter(Globals.conVendorLit);
                        Globals.objAD00.EntityType = xmlEntity.EntityType;
                    }

                    {
                        if (Globals.b1099Address)
                        {
                            Globals.objPTX00.PMSPTX00Name = xmlEntity.Name;
                            Globals.objPTX00.PMSPTX00Address = StringUtils.Trim(Globals.str1099Address[0] + " " + Globals.str1099Address[1]);
                            Globals.objPTX00.PMSPTX00CityState = Globals.str1099City + ", " + Globals.str1099StateCode;
                            Globals.objPTX00.PMSPTX00ZipCode = Globals.str1099PostalCd;
                        }
                        else
                        {
                            Globals.objPTX00.PMSPTX00Name = xmlEntity.Name;
                            Globals.objPTX00.PMSPTX00Address = StringUtils.Trim(Globals.strAddress[0] + " " + Globals.strAddress[1] + " " + Globals.strAddress[2] + " " + Globals.strAddress[3]);
                            Globals.objPTX00.PMSPTX00CityState = Globals.strCity + " " + Globals.strStateCode;
                            Globals.objPTX00.PMSPTX00ZipCode = Globals.strPostalCd;
                        }
                        Globals.objPTX00.PMSPTX00TaxIdSSN = Globals.strTaxId;
                        Globals.objPTX00.PMSPTX00LongTaxID = Globals.strLongTaxId;
                        Globals.objPTX00.PMSPTX00SSN = Globals.strTaxIdType;
                        Globals.objPTX00.PMSPTX00PhoneNumber = Globals.strPhone[0];
                    }

                    LoadDatabase.LocateVendor();

                    Globals.strVenNumber = Globals.objAD00.PMSPAD00AdjustorNbr;

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlVndr, ScriptingFunctions.conScpEvtEnd);
                }
                xmlEntity.getEntity(false);
                bProcessEntity = false;
            }

            Globals.dbg.PopProc();

        }

        private static void ParsePointALUpload(CCP.XmlComponents.XMLLossDetail xmlLD)
        {
            string sCurrDate = string.Empty;

            Globals.objPMSP0000.PMSP0000Symbol = xmlLD.Policy.Symbol;
            Globals.objPMSP0000.PMSP0000POLICY0NUM = xmlLD.Policy.PolicyNumber;
            Globals.objPMSP0000.PMSP0000Module = xmlLD.Policy.Module;
            Globals.objPMSP0000.PMSP0000MASTER0CO = xmlLD.Policy.MasterCompany_Code;
            Globals.objPMSP0000.PMSP0000Location = xmlLD.Policy.Location;

            //Globals.InitForPointALUploadFile(Globals.objPMSP0000);

            Globals.objPMSP0000.PMSP0000ACT0MONTH = StringUtils.Mid(RC.Conversion.ConvertObjToStr(Globals.objPCL50.PMSPCL50TranEntDt), 4, 2);
            Globals.objPMSP0000.PMSP0000ACT0DAY = StringUtils.Right(RC.Conversion.ConvertObjToStr(Globals.objPCL50.PMSPCL50TranEntDt), 2);
            Globals.objPMSP0000.PMSP0000ACT0YEAR = StringUtils.Left(RC.Conversion.ConvertObjToStr(Globals.objPCL50.PMSPCL50TranEntDt), 3);
            sCurrDate = Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", DateTime.Now));
            Globals.objPMSP0000.PMSP0000ENT0MONTH = StringUtils.Mid(sCurrDate, 4, 2);
            Globals.objPMSP0000.PMSP0000ENT0DAY = StringUtils.Right(sCurrDate, 2);
            Globals.objPMSP0000.PMSP0000ENT0YEAR = StringUtils.Left(sCurrDate, 3);

            LoadDatabase.LocatePointALUploadFile();

        }

    }
}
