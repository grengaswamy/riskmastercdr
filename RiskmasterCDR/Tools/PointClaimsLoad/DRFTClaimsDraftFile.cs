﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
   public class DRFTClaimsDraftFile
    {
        public string DRFTSymbol;
        public string DRFTPolicyNo;
        public string DRFTModule;
        public string DRFTMasterCo;
        public string DRFTLocation;
        public string DRFTClaim;
        public int DRFTClmtSeq;
        public int DRFTPolCovSeq;
        public int DRFTResvNo;
        public int DRFTTransSeq;
        public string DRFTTransCode;
        public long DRFTDrftPrtDte;
        public string DRFTBankCode;
        public float DRFTDrftAmt;
        public string DRFTDrftNumber;
        public string DRFTPPCode;
        public string DRFTPayePhrase;
        public string DRFTPayToCode1;
        public string DRFTPayeeNo1;
        public string DRFTPayeeName1;
        public string DRFTPayeeAdd11;
        public string DRFTPayeeAdd12;
        public string DRFTPayeeAdd13;
        public string DRFTPayeeZip1;
        public string DRFTPayToCode2;
        public string DRFTPayeeNo2;
        public string DRFTPayeeName2;
        public string DRFTPayeeAdd21;
        public string DRFTPayeeAdd22;
        public string DRFTPayeeAdd23;
        public string DRFTPayeeZip2;
        public string DRFTPayToCode3;
        public string DRFTPayeeNo3;
        public string DRFTPayeeName3;
        public string DRFTPayeeAdd31;
        public string DRFTPayeeAdd32;
        public string DRFTPayeeAdd33;
        public string DRFTPayeeZip3;
        public string DRFTMailToCode;
        public string DRFTMailToNo;
        public string DRFTMailToName;
        public string DRFTMailToAdd1;
        public string DRFTMailToAdd2;
        public string DRFTMailToAdd3;
        public string DRFTMailToZip;
        public string DRFTMemoCode;
        public string DRFTMemo1;
        public string DRFTMemo2;
        public string DRFTNote1;
        public string DRFTNote2;
        public string DRFTAPHistory;
        public string DRFTPayType;
        public string DRFTPayStatus;
        public string DRFTItemNo;
        public string DRFTCheckNo;
        public string DRFTCRTDate;
        public string DRFTCRTTime;
        public string DRFTSERDTEFROM;
        public string  DRFTSERDTETO;
        public string DRFTInvoiceNo1;

       public DRFTClaimsDraftFile()
       {
           Clear();    
       }
       public void Clear()
       {
           DRFTSymbol = "";
           DRFTPolicyNo = "";
           DRFTModule = "";
           DRFTMasterCo = "";
           DRFTLocation = "";
           DRFTClaim = "";
           DRFTClmtSeq = 0;
           DRFTPolCovSeq = 0;
           DRFTResvNo = 0;
           DRFTTransSeq = 0;
           DRFTTransCode = "";
           DRFTDrftPrtDte = 0;
           DRFTBankCode = "";
           DRFTDrftAmt = 0;
           DRFTDrftNumber = "";
           DRFTPPCode = "";
           DRFTPayePhrase = "";
           DRFTPayToCode1 = "";
           DRFTPayeeNo1 = "";
           DRFTPayeeName1 = "";
           DRFTPayeeAdd11 = "";
           DRFTPayeeAdd12 = "";
           DRFTPayeeAdd13 = "";
           DRFTPayeeZip1 = "";
           DRFTPayToCode2 = "";
           DRFTPayeeNo2 = "";
           DRFTPayeeName2 = "";
           DRFTPayeeAdd21 = "";
           DRFTPayeeAdd22 = "";
           DRFTPayeeAdd23 = "";
           DRFTPayeeZip2 = "";
           DRFTPayToCode3 = "";
           DRFTPayeeNo3 = "";
           DRFTPayeeName3 = "";
           DRFTPayeeAdd31 = "";
           DRFTPayeeAdd32 = "";
           DRFTPayeeAdd33 = "";
           DRFTPayeeZip3 = "";
           DRFTMailToCode = "";
           DRFTMailToNo = "";
           DRFTMailToName = "";
           DRFTMailToAdd1 = "";
           DRFTMailToAdd2 = "";
           DRFTMailToAdd3 = "";
           DRFTMailToZip = "";
           DRFTMemoCode = "";
           DRFTMemo1 = "";
           DRFTMemo2 = "";
           DRFTNote1 = "";
           DRFTNote2 = "";
           DRFTAPHistory = "";
           DRFTPayType = "";
           DRFTPayStatus = "";
           DRFTItemNo = "";
           DRFTCheckNo = "";
           DRFTCRTDate = "";
           DRFTCRTTime = "";
           DRFTSERDTEFROM = "";
           DRFTSERDTETO = "";
           DRFTInvoiceNo1 = "";

       }
    }
}
