﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class BASCLT0100
    {
        public long CLTSEQNUM;
        public string GROUPNO;
        public string NAMETYPE;
        public string NAMESTATUS;
        public string CLIENTNAME;
        public string FIRSTNAME;
        public string MIDNAME;
        public string PREFIXNM;
        public string SUFFIXNM;
        public string LONGNAME;
        public string DBA;
        public string CONTACT;
        public string PHONE1;
        public string PHONE2;
        public string FEDTAXID;
        public string SSN;
        public string SEX;
        public System.DateTime BIRTHDATE;
        public System.DateTime EFFDATE;
        public System.DateTime RECDATE;
        public System.DateTime RECTIME;
        public long EntityID;
        public BASCLT0100()
        {

        }
        public void Clear()
        { 
            CLTSEQNUM = 0;
            //GROUPNO = "CLAIMSPRO"       'SI05603
            GROUPNO = "CSCCCP";
            //SI05603
            NAMETYPE = "";
            NAMESTATUS = "";
            CLIENTNAME = "";
            FIRSTNAME = "";
            MIDNAME = "";
            PREFIXNM = "";
            SUFFIXNM = "";
            LONGNAME = "";
            DBA = "";
            CONTACT = "";
            PHONE1 = "";
            PHONE2 = "";
            FEDTAXID = "";
            SSN = "";
            SEX = "";
            //BIRTHDATE = "1/1/1900 12:00:00 AM";
            //EFFDATE = "1/1/1900 12:00:00 AM";
            //RECDATE = System.DateTime;
            //RECTIME = Time;
            EntityID = 0;
        }

        public BASCLT0100 Clone()
        { 
             BASCLT0100 obj = new BASCLT0100();
             
             obj.EntityID = EntityID;
             obj.CLTSEQNUM = CLTSEQNUM;
             obj.GROUPNO = GROUPNO;
             obj.NAMETYPE = NAMETYPE;
             obj.NAMESTATUS = NAMESTATUS;
             obj.CLIENTNAME = CLIENTNAME;
             obj.FIRSTNAME = FIRSTNAME;
             obj.MIDNAME = MIDNAME;
             obj.PREFIXNM = PREFIXNM;
             obj.SUFFIXNM = SUFFIXNM;
             obj.LONGNAME = LONGNAME;
             obj.DBA = DBA;
             obj.CONTACT = CONTACT;
             obj.PHONE1 = PHONE1;
             obj.PHONE2 = PHONE2;
             obj.FEDTAXID = FEDTAXID;
             obj.SSN = SSN;
             obj.SEX = SEX;
             obj.BIRTHDATE = BIRTHDATE;
             obj.EFFDATE = EFFDATE;
             obj.RECDATE = RECDATE;
             obj.RECTIME = RECTIME;

     return obj;
        }
    }
}
