﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class BASCLT1700
    {
        public string CLAIM;
        public long CLMTSEQ;
        public long CLTSEQNUM;
        public long ADDRSEQNUM;
        public string ROLE;
        public long RMAENTITYID;
        public long RMAADDRESSID;
        public System.DateTime RECDATE;
        public System.DateTime RECTIME;

        public BASCLT1700()
        {
            Clear();
        }

        public void Clear()
        {
            CLAIM = "";
            CLMTSEQ = 0;
            CLTSEQNUM = 0;
            ADDRSEQNUM = 0;
            ROLE = "";
            //RECDATE = null;
            //RECTIME = null;

        }
    }
}
