﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPTX00TaxIdFile
    {

        public string PMSPTX00LongTaxID;
        public string PMSPTX00TaxIdSSN;
        public string PMSPTX00Name;
        public string PMSPTX00Address;
        public string PMSPTX00CityState;
        public string PMSPTX00ZipCode;
        public string PMSPTX00PhoneNumber;
        public string PMSPTX00SSN;

        public PMSPTX00TaxIdFile()
        {
            Clear();

        }

        public void Clear()
        {
            PMSPTX00LongTaxID = "";
            PMSPTX00TaxIdSSN = "";
            PMSPTX00Name = "";
            PMSPTX00Address = "";
            PMSPTX00CityState = "";
            PMSPTX00ZipCode = "";
            PMSPTX00PhoneNumber = "";
            PMSPTX00SSN = "";
        }
    }
}
