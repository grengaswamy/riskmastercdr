﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Security;

namespace CCP.PointClaimsLoad
{
    public struct RMPolicySystemConfig
    {
        public string Name
        {
            get;
            set;
        }


        public string ClmOffice
        {
            get;
            set;
        }

        public string Examiner
        {
            get;
            set;
        }

        public string ExaminerCD
        {
            get;
            set;
        }

        public string ExemptCLaimId
        {
            get;
            set;
        }

        public string AddDefaultClmOffice
        {
            get;
            set;
        }

        public string AddDefaultExaminer
        {
            get;
            set;
        }

        public string AddDefaultExaminerCD
        {
            get;
            set;
        }

        public string AddDefaultAdjustor
        {
            get;
            set;
        }

        public string Adjustor
        {
            get;
            set;
        }

        public string EntryInPMSP0000ForAL
        {
            get;
            set;
        }

        public string DataDSN
        {
            get;
            set;
        }
        public string DataDSNUser
        {
            get;
            set;
        }
        public string DataDSNPwd
        {
            get;
            set;
        }

        public string DataDSNConnStr
        {
            get;
            set;
        }
        public string PointClient
        {
            get;
            set;
        }
        public string ReleaseVersion
        {
            get;
            set;
        }

        //dbisht6 mits 36536 start
        public string UploadPointEntity
        {
            get;
            set;
        }

        // mits 36536 end

        //Added by gbindra 10/30/2014 start
        public string UploadVoucher
        {
            get;
            set;
        }
        //Added by gbindra 10/30/2014 end
        //aaggarwal29: RMA-7701
        public string UploadPayee2PMSPAP00
        {
            get;
            set;
        }
    }

    public class UploadConfiguration
    {
        public RMPolicySystemConfig ConfigElement;
        private int m_iClientId = 0;

        public UploadConfiguration(int sPolicySystemId, string sUserName, string sRMDsnName, out UserLogin objUser)
        {
            string[] sDSNDetails = null;
            objUser = new UserLogin(sUserName, sRMDsnName, m_iClientId);

            Riskmaster.Application.PolicySystemInterface.SetUpPolicySystem objPolInt = new Riskmaster.Application.PolicySystemInterface.SetUpPolicySystem(sUserName, objUser.objRiskmasterDatabase.ConnectionString, m_iClientId);
            objPolInt.GetPolicyDSNDetails(ref sDSNDetails, sPolicySystemId);
            ConfigElement = new RMPolicySystemConfig();
            ConfigElement.Name = sDSNDetails[6];
            ConfigElement.DataDSN = sDSNDetails[1];
            ConfigElement.DataDSNUser = sDSNDetails[4];
            ConfigElement.DataDSNPwd = sDSNDetails[5];
            ConfigElement.DataDSNConnStr = sDSNDetails[7];
            ConfigElement.PointClient = sDSNDetails[8];
            ConfigElement.ReleaseVersion = sDSNDetails[9];
            ConfigElement.AddDefaultClmOffice = sDSNDetails[10];
            ConfigElement.AddDefaultExaminer = sDSNDetails[11];
            ConfigElement.AddDefaultAdjustor = sDSNDetails[12];
            ConfigElement.AddDefaultExaminerCD = sDSNDetails[13];
            ConfigElement.ClmOffice = sDSNDetails[14];
            ConfigElement.Adjustor = sDSNDetails[15];
            ConfigElement.Examiner = sDSNDetails[16];
            ConfigElement.EntryInPMSP0000ForAL = sDSNDetails[17];
            ConfigElement.ExaminerCD = sDSNDetails[18];
            ConfigElement.ExemptCLaimId = sDSNDetails[19];
            ConfigElement.UploadPointEntity = sDSNDetails[20];
            ConfigElement.UploadVoucher = sDSNDetails[21];  //Added by gbindra on 10/30/2014
            ConfigElement.UploadPayee2PMSPAP00 = sDSNDetails[22];//aaggarwal29: RMA-7701
        }
    }
}
