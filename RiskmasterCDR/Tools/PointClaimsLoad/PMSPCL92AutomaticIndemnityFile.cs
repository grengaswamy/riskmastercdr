﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL92AutomaticIndemnityFile
    {
        public string PMSPCL92Symbol;
        public string PMSPCL92PolicyNo;
        public string PMSPCL92Module;
        public string PMSPCL92MasterCo;
        public string PMSPCL92Location;
        public string PMSPCL92Claim;
        public int PMSPCL92ClmtSeq;
        public int PMSPCL92PolCovSeq;
        public int PMSPCL92Resvno;
        public string PMSPCL92PaymentTyp;
        public string PMSPCL92PayeeAge;
        public string PMSPCL92MonthsEmpl;
        public string PMSPCL92YearsEmpl;
        public int PMSPCL92Payments;
        public string PMSPCL92PaymntFreq;
        public float PMSPCL92PaymntRate;
        public float PMSPCL92WeeklyWage;
        public long PMSPCL92StartDate;
        public long PMSPCL92NxtPayDte;
        public long PMSPCL92StopDate;
        public string PMSPCL92Reason;
        public long PMSPCL92DrftPrtDte;
        public string PMSPCL92BankCode;
        public float PMSPCL92DrftAmt;
        public string PMSPCL92DrftNumber;
        public string PMSPCL92PPCode;
        public string PMSPCL92PayePhrase;
        public string PMSPCL92PayToCode1;
        public string PMSPCL92PayeeNo1;
        public string PMSPCL92PayeeName1;
        public string PMSPCL92PayeeAdd11;
        public string PMSPCL92PayeeAdd12;
        public string PMSPCL92PayeeAdd13;
        public string PMSPCL92PayeeZip1;
        public string PMSPCL92PayToCode2;
        public string PMSPCL92PayeeNo2;
        public string PMSPCL92PayeeName2;
        public string PMSPCL92PayeeAdd21;
        public string PMSPCL92PayeeAdd22;
        public string PMSPCL92PayeeAdd23;
        public string PMSPCL92PayeeZip2;
        public string PMSPCL92PayToCode3;
        public string PMSPCL92PayeeNo3;
        public string PMSPCL92PayeeName3;
        public string PMSPCL92PayeeAdd31;
        public string PMSPCL92PayeeAdd32;
        public string PMSPCL92PayeeAdd33;
        public string PMSPCL92PayeeZip3;
        public string PMSPCL92MailToCode;
        public string PMSPCL92MailToNo;
        public string PMSPCL92MailToName;
        public string PMSPCL92MailToAdd1;
        public string PMSPCL92MailToAdd2;
        public string PMSPCL92MailToAdd3;
        public string PMSPCL92MailToZip;
        public string PMSPCL92MemoCode;
        public string PMSPCL92Memo1;
        public string PMSPCL92Memo2;
        public string PMSPCL92Note1;
        public string PMSPCL92Note2;
        
        public PMSPCL92AutomaticIndemnityFile()
         {
             Clear();
         }

        public void Clear()
        {
            PMSPCL92Symbol = "";
            PMSPCL92PolicyNo = "";
            PMSPCL92Module = "";
            PMSPCL92MasterCo = "";
            PMSPCL92Location = "";
            PMSPCL92Claim = "";
            PMSPCL92ClmtSeq = 0;
            PMSPCL92PolCovSeq = 0;
            PMSPCL92Resvno = 0;
            PMSPCL92PaymentTyp = "";
            PMSPCL92PayeeAge = "";
            PMSPCL92MonthsEmpl = "";
            PMSPCL92YearsEmpl = "";
            PMSPCL92Payments = 0;
            PMSPCL92PaymntFreq = "";
            PMSPCL92PaymntRate = 0;
            PMSPCL92WeeklyWage = 0;
            PMSPCL92StartDate = 0;
            PMSPCL92NxtPayDte = 0;
            PMSPCL92StopDate = 0;
            PMSPCL92Reason = "";
            PMSPCL92DrftPrtDte = 0;
            PMSPCL92BankCode = "";
            PMSPCL92DrftAmt = 0;
            PMSPCL92DrftNumber = "";
            PMSPCL92PPCode = "";
            PMSPCL92PayePhrase = "";
            PMSPCL92PayToCode1 = "";
            PMSPCL92PayeeNo1 = "";
            PMSPCL92PayeeName1 = "";
            PMSPCL92PayeeAdd11 = "";
            PMSPCL92PayeeAdd12 = "";
            PMSPCL92PayeeAdd13 = "";
            PMSPCL92PayeeZip1 = "";
            PMSPCL92PayToCode2 = "";
            PMSPCL92PayeeNo2 = "";
            PMSPCL92PayeeName2 = "";
            PMSPCL92PayeeAdd21 = "";
            PMSPCL92PayeeAdd22 = "";
            PMSPCL92PayeeAdd23 = "";
            PMSPCL92PayeeZip2 = "";
            PMSPCL92PayToCode3 = "";
            PMSPCL92PayeeNo3 = "";
            PMSPCL92PayeeName3 = "";
            PMSPCL92PayeeAdd31 = "";
            PMSPCL92PayeeAdd32 = "";
            PMSPCL92PayeeAdd33 = "";
            PMSPCL92PayeeZip3 = "";
            PMSPCL92MailToCode = "";
            PMSPCL92MailToNo = "";
            PMSPCL92MailToName = "";
            PMSPCL92MailToAdd1 = "";
            PMSPCL92MailToAdd2 = "";
            PMSPCL92MailToAdd3 = "";
            PMSPCL92MailToZip = "";
            PMSPCL92MemoCode = "";
            PMSPCL92Memo1 = "";
            PMSPCL92Memo2 = "";
            PMSPCL92Note1 = "";
            PMSPCL92Note2 = "";
        }
    }
}
