﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
using Riskmaster.Db;

namespace CCP.PointClaimsLoad
{
    public class OffSetOnset
    {
        private static int iPCL50Count;
        private static int iPCL50PayCount;
        //LoadDatabase objDB;
        private static int iRecCntPMSPSCL50;
        public OffSetOnset()
        {
            //objDB = new LoadDatabase();
        }
        public static void ProcessOffsetOnset()
        {

            object rowsAffected;
            Parameters objParams = Globals.GetParams();
            string sSql = null;
            //SI04826
            //ADODB.Recordset rs = default(ADODB.Recordset);
            //SI04826
            string sFldList = null;
            //SI04826
            string sInsList = null;
            //SI04826
            string sPN = null;
            //SI04826
            string sAcctDte = null;
            //SI05628
            DbReader dbReader = null;
            DbReader dbReaderResPmt = null;
            //   Dim bPolicyMove As Boolean                   'SI05634

            try
            {
                sPN = "ProcessOffsetOnset";
                //SI04826
                Globals.dbg.PushProc(sPN, Globals.sClaimNumber, Globals.sNewClaimNumber, "", "");
                //SI04826

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtStart, Globals.sNewClaimNumber, Globals.lNewLossDate, Globals.sNewCatCode);

                //If there are transactions coming in on a policy that is not connected with this
                //claim already then we assume that this is a claim transfer to another policy and
                //we don't perform all of the copies.

                //   bPolicyMove = True
                //   XMLClaim.Policy.getPolicy True
                //   If XMLClaim.Policy.PolicyNumber <> "" Then
                //      bPolicyMove = IsPolicyOnThisClaim(sClaimNumber, XMLClaim.Policy.Symbol, _
                //                                                      XMLClaim.Policy.PolicyNumber, _
                //                                                      XMLClaim.Policy.Module, _
                //                                                      XMLClaim.Policy.MasterCompany_Code, _
                //                                                      XMLClaim.Policy.Location)
                //   End If
                //SI05634 Start
                //Even if we have the financial details we need to copy the CL20 records
                //If bOffsetOnsetLossDetail Then
                //   GoTo SkipCopies
                //End If
                //SI05634 End

                //This first section copies the original claim data to the new claim number with appropriate
                //value changes

                //PMSPCL20
                sSql = "SELECT * FROM PMSPCL20 WHERE 0=1";
                //dbReader = LoadDatabase.ExecuteReader(sSql);
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                sFldList = BuildFieldList(dbReader);

                //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                //sFldList = BuildFieldList(rs);
                //rs.Close();
                sInsList = "(" + sFldList + ")";
                sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");
                sFldList = sFldList.Replace("CATCODE,", "'" + Globals.sNewCatCode + "' AS CATCODE,");
                sFldList = sFldList.Replace("LOSSDTE,", Convert.ToString(Globals.lNewLossDate) + " AS LOSSDTE,");

                sSql = "INSERT INTO PMSPCL20 " + sInsList;
                sSql = sSql + " SELECT " + sFldList;
                sSql = sSql + " FROM PMSPCL20 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL20", sSql);
                Globals.dbg.DebugTrace("PMSPCL20", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL20", sSql, -1);

                //SI05634 Start
                //if (Globals.bOffsetOnsetLossDetail == true)
                //{
                //    goto SkipCopies;
                //}
               if (!Globals.bOffsetOnsetLossDetail)//remocing go to statement
                {
                   
                    //PMSPCL30
                    sSql = "SELECT * FROM PMSPCL30 WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);

                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");

                    sSql = "INSERT INTO PMSPCL30 " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM PMSPCL30 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL30", sSql);
                    Globals.dbg.DebugTrace("PMSPCL30", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL30", sSql);
                   
                    //Start SI06437
                    //BASCLT1700
                    //if (objParams.IsTrue(Globals.conPOINTClient))
                   if(Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient))
                    {
                        sSql = "SELECT * FROM BASCLT1700 WHERE 0=1";
                        //dbReader = LoadDatabase.ExecuteReader(sSql);
                        dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                        sFldList = BuildFieldList(dbReader);

                        //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                        //sFldList = BuildFieldList(rs);
                        //rs.Close();
                        sInsList = "(" + sFldList + ")";
                        sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");
                        sFldList = sFldList.Replace("RECUSER,", "'" + objParams.Parameter(Globals.SWDataUser) + "' AS RECUSER,");
                        sFldList = sFldList.Replace("RECDATE,", "'" + string.Format("{0:MM/dd/yyyy}", System.DateTime.Now.ToString()) + "' AS RECDATE,");
                        sFldList = sFldList.Replace("RECTIME,", "'" + string.Format("{0:HH:mm:ss}", System.DateTime.Now.ToString()) + "' AS RECTIME,");

                        sSql = "INSERT INTO BASCLT1700 " + sInsList;
                        sSql = sSql + " SELECT " + sFldList;
                        sSql = sSql + "  FROM BASCLT1700 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                        ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "BASCLT1700", sSql);
                        Globals.dbg.DebugTrace("BASCLT1700", sSql, "", "", "");
                        DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                        //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                        ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "BASCLT1700", sSql);
                    }
                    //End SI06437






                    //PMSPCL42
                    sSql = "SELECT * FROM PMSPCL42 WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);
                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");

                    sSql = "INSERT INTO PMSPCL42 " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM PMSPCL42 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL42", sSql);
                    Globals.dbg.DebugTrace("PMSPCL42", sSql, "", "", "");
                    //SI04826
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                   // Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL42", sSql);

                    //PMSPCL14
                    sSql = "SELECT * FROM PMSPCL14 WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);

                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    //sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,", 1, 1);
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");
                    sSql = "INSERT INTO PMSPCL14 " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM PMSPCL14 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL14", sSql);
                    Globals.dbg.DebugTrace("PMSPCL14", sSql, "", "", "");

                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL14", sSql);

                    //PMSPCL50
                    sSql = "SELECT * FROM PMSPCL50 WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);

                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    LoadDatabase.GetAccountDate(ref sAcctDte);
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");
                    sFldList = sFldList.Replace("ACCTDTE,", sAcctDte + " AS ACCTDTE,");
                    sFldList = sFldList.Replace("TRANENTDT,", Convert.ToInt64(Globals.sCurrentEntryDate) + " AS TRANENTDT,");

                    sSql = "INSERT INTO PMSPCL50 " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM PMSPCL50 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL50", sSql);
                    Globals.dbg.DebugTrace("PMSPCL50", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL50", sSql);

                    //PMSPCL92
                    sSql = "SELECT * FROM PMSPCL92 WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);


                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");

                    sSql = "INSERT INTO PMSPCL92 " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM PMSPCL92 WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL92", sSql);
                    Globals.dbg.DebugTrace("PMSPCL92", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL92", sSql);

                    //DRFTFILE
                    sSql = "SELECT * FROM DRFTFILE WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);

                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("CLAIM,", "'" + Globals.sNewClaimNumber + "' AS CLAIM,");
                    sFldList = sFldList.Replace("CRTDATE,", "'" + Convert.ToInt64(Globals.sCurrentEntryDate) + "' AS CRTDATE,");
                    sFldList = sFldList.Replace("CRTTIME,", "'" + Globals.sHHNNSS000000Now + "' AS CRTTIME,");

                    sSql = "INSERT INTO DRFTFILE " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM DRFTFILE WHERE CLAIM = '" + Globals.sClaimNumber + "'";

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "DRFTFILE", sSql);
                    Globals.dbg.DebugTrace("DRFTFILE", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "DRFTFILE", sSql);

                //SI05634 End
            }




                //SkipCopies:
                //This need to execute here before the ap00 and drftfile tables are updated


                //if (Globals.PointClaimDErsReservePaymentOffset.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
                //{
                //    Globals.PointClaimDErsReservePaymentOffset.Close();
                //}
                //PointClaimDEReservePaymentOffset(Globals.sClaimNumber);
                
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, "SELECT * FROM PMSPCL50  WHERE CLAIM ='" + Globals.sClaimNumber + "' ORDER BY CLMTSEQ, POLCOVSEQ, RESVNO, TRANSSEQ");
                iRecCntPMSPSCL50 = Convert.ToInt32(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,"SELECT count(CLMTSEQ) FROM PMSPCL50  WHERE CLAIM ='" + Globals.sClaimNumber + "' ORDER BY CLMTSEQ, POLCOVSEQ, RESVNO, TRANSSEQ"));
                //if ((Globals.PointClaimDErsReservePaymentOffset.BOF == true) || (Globals.PointClaimDErsReservePaymentOffset.EOF == true))
                if (dbReader.Read())
                {
                    PaymentOffsetOnset(dbReader);
                }
                //else
                //{
                //    PaymentOffsetOnset();
                //}


                //SI05634 Start
                //If bOffsetOnsetLossDetail Then
                //  GoTo SkipCopies_a
                //End If
                if (Globals.bOffsetOnsetLossDetail)
                {
                    //If we are processing the financials too , we will rebuild the AP00 rows.
                    sSql = "DELETE FROM PMSPAP00 ";
                    sSql = sSql + "WHERE CLAIMNO = '" + Globals.sClaimNumber + "' ";
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPAP00", sSql);
                    Globals.dbg.DebugTrace("PMSPAP00", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPAP00", sSql);
                }
                else
                {
                    //SI05634 End
                    //Can not copy AP00.  Need to reassign the claim number
                    sSql = "UPDATE PMSPAP00 ";
                    sSql = sSql + "SET CLAIMNO = '" + Globals.sNewClaimNumber + "' ";
                    sSql = sSql + "WHERE CLAIMNO = '" + Globals.sClaimNumber + "' ";
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPAP00", sSql);
                    Globals.dbg.DebugTrace("PMSPAP00", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPAP00", sSql);
                }
            //SkipCopies_a: --- not required, the matching go to already commented in vb code
                //SI05634

                //This section sets all of the statuses on the old claim
                sSql = "UPDATE PMSPCL20 SET RECSTATUS= 'C' WHERE CLAIM= '" + Globals.sClaimNumber + "'";
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL20_Status", sSql);
                Globals.dbg.DebugTrace("PMSPCL20", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL20_Status", sSql);

                sSql = "UPDATE PMSPCL30 SET RECSTATUS= 'C' WHERE CLAIM= '" + Globals.sClaimNumber + "'";
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL30_Status", sSql);
                Globals.dbg.DebugTrace("PMSPCL30", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL30_Status", sSql);

                sSql = "UPDATE PMSPCL42 SET RECSTATUS= 'C' WHERE CLAIM= '" + Globals.sClaimNumber + "'";
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL42_Status", sSql);
                Globals.dbg.DebugTrace("PMSPCL42", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL42_Status", sSql);

                //SI05634 Start
                sSql = "UPDATE PMSPCL50 SET OFFSETSW = 'Y' ";
                sSql = sSql + "  WHERE CLAIM = '" + Globals.sClaimNumber + "'";
                sSql = sSql + "  AND CLAIMTRANS IN ('FF','FP', 'SP', 'PP') AND OFFSETSW <> 'Y'";
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "PMSPCL50", sSql);
                Globals.dbg.DebugTrace("PMSPCL50", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "PMSPCL50", sSql);

                sSql = "UPDATE DRFTFILE SET PAYSTATUS = 'V' ";
                sSql = sSql + "  WHERE CLAIM = '" + Globals.sClaimNumber + "'";
                sSql = sSql + "  AND TRANSCODE IN ('FF','FP', 'SP', 'PP') AND PAYSTATUS <> 'V'";
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "DRFTFILE", sSql);
                Globals.dbg.DebugTrace("PMSPCL50", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "DRFTFILE", sSql);


            }
            catch (Exception ex)
            {
                //hError:

                //    int rslt = 0;
                //   // rslt = Globals.dbg.CErrors.ProcessError(1, sPN, Err);
                //    if (rslt == Constants.vbRetry)
                //        // ERROR: Not supported in C#: ResumeStatement

                //        if (rslt == Constants.vbIgnore)
                //            // ERROR: Not supported in C#: ResumeStatement

                //            Globals.bContinue = false;
                //    // ERROR: Not supported in C#: ResumeStatement
            }

            finally
            {
                //        hExit:
                //SI05634 End

                Globals.dbg.PopProc();
            }


        }
        //private static string BuildFieldList(ADODB.Recordset rs)
        private static string BuildFieldList(DbReader dbReader)
        {

            string sFlds = null;
            string sComma = null;
            Globals.dbg.PushProc("BuildFieldList");
            sFlds = "";
            sComma = "";
            
            //foreach (ADODB.Field fld in rs.Fields)
            for (int i = 0; i < dbReader.FieldCount; i++)
            {
                //sFlds = sFlds + sComma + fld.Name;
                sFlds = sFlds + sComma + dbReader.GetName(i);
                sComma = ",";
                Globals.dbg.DebugTrace(dbReader.GetName(i), sFlds, "", "", "");
            }

            Globals.dbg.PopProc();
            return sFlds;

        }

        private static void FormatOffsetPaymentRow(DbReader dbReader)
        {
            string strTemp = null;
            bool bWriteClose = false;
            bool bWriteFinalClose = false;
            Parameters objParams = Globals.GetParams();
            Globals.dbg.PushProc("FormatOffsetPaymentRow");
          
            //Parser.ResvCategory(Globals.PointClaimDErsReservePaymentOffset.Fields["RESVNO"].Value);
            Parser.ResvCategory(Convert.ToString(dbReader.GetValue("RESVNO")));
            //SI06823

            bWriteClose = false;
            if (iPCL50Count > 1)
            {
                //if ((Globals.objPCL50.PMSPCL50ClmtSeq != Globals.PointClaimDErsReservePaymentOffset.Fields["CLMTSEQ"].Value) || (Globals.objPCL50.PMSPCL50PolCovSeq != Globals.PointClaimDErsReservePaymentOffset.Fields["POLCOVSEQ"].Value) || (Globals.objPCL50.PMSPCL50ResvNo != Globals.PointClaimDErsReservePaymentOffset.Fields["RESVNO"].Value))
                if ((Globals.objPCL50.PMSPCL50ClmtSeq != Convert.ToInt32(dbReader.GetValue("CLMTSEQ"))) || (Globals.objPCL50.PMSPCL50PolCovSeq != Convert.ToInt32(dbReader.GetValue("POLCOVSEQ")) || (Globals.objPCL50.PMSPCL50ResvNo != Convert.ToInt32(dbReader.GetValue("RESVNO")))))
                {
                    if (Globals.objPCL50.PMSPCL50RecStatus == "O" || Globals.objPCL50.PMSPCL50RecStatus == "R")
                    {
                        bWriteClose = true;
                        //This closes out the previous reserve processing key
                        //prior to processing the new reserve key
                    }
                }
            }

            if (bWriteClose == true)
            {
                CloseReserve();
                bWriteClose = false;
            }

                Globals.objPCL50.PMSPCL50Claim = Convert.ToString(dbReader.GetValue("CLAIM"));
                Globals.objPCL50.PMSPCL50ClmtSeq = Convert.ToInt32(dbReader.GetValue("CLMTSEQ"));
                Globals.objPCL50.PMSPCL50PolCovSeq = Convert.ToInt32(dbReader.GetValue("POLCOVSEQ"));
                Globals.objPCL50.PMSPCL50ResvNo = Convert.ToInt32(dbReader.GetValue("RESVNO"));
                Globals.objPCL50.PMSPCL50TransSeq = Convert.ToInt32(dbReader.GetValue("TRANSSEQ"));
                Globals.objPCL50.PMSPCL50RecStatus = Convert.ToString(dbReader.GetValue("RECSTATUS"));
                Globals.objPCL50.PMSPCL50ClaimTrans = Convert.ToString(dbReader.GetValue("CLAIMTRANS"));
                Globals.objPCL50.PMSPCL50ResvPayAmt = Convert.ToSingle(dbReader.GetValue("RESVPAYAMT"));
                Globals.objPCL50.PMSPCL50CurrResv = Convert.ToSingle(dbReader.GetValue("CURRRESV"));
                Globals.objPCL50.PMSPCL50TotPayment = Convert.ToSingle(dbReader.GetValue("TOTPAYMENT"));
                Globals.objPCL50.PMSPCL50TranEntDt = Convert.ToInt64(dbReader.GetValue("TRANENTDT"));
                Globals.objPCL50.PMSPCL50TranEntTm = Convert.ToString(dbReader.GetValue("TRANENTTM"));
                Globals.objPCL50.PMSPCL50AcctDte = Convert.ToInt64(dbReader.GetValue("ACCTDTE"));
                Globals.objPCL50.PMSPCL50PaymentTyp = Convert.ToString(dbReader.GetValue("PAYMENTTYP"));
                Globals.objPCL50.PMSPCL50ReinsInd = " ";
                Globals.objPCL50.PMSPCL50Vendor = Convert.ToString(dbReader.GetValue("VENDOR"));
                Globals.objPCL50.PMSPCL50OffsetSw = Convert.ToString(dbReader.GetValue("OFFSETSW"));
                Globals.objPCL50.PMSPCL50Adjustor = Convert.ToString(dbReader.GetValue("ADJUSTOR"));
                //.PMSPCL50Adjustor = "004132"

                if (objParams.Parameter("AddDefaultAdjustor") == "YES")
                {
                    if (!string.IsNullOrEmpty(objParams.Parameter("ADJUSTOR")))
                    {
                        Globals.objPCL50.PMSPCL50Adjustor = objParams.Parameter("ADJUSTOR");
                    }

                }
                if (Globals.bIsNewClaimUpload)
                {
                    Globals.objPCL50.PMSPCL50ClmOffice = Convert.ToString(dbReader.GetValue("CLMOFFICE"));
                    Globals.objPCL50.PMSPCL50ExaminerCd = Convert.ToString(dbReader.GetValue("EXAMINERCD"));
                }

            Globals.sClaimNumber = Globals.objPCL50.PMSPCL50Claim;
            Globals.sCLMTSEQ = Globals.objPCL50.PMSPCL50ClmtSeq;
            Globals.sPolCovSeq = Globals.objPCL50.PMSPCL50PolCovSeq;
            string args = Convert.ToString(dbReader.GetValue("CLAIMTRANS"));
            switch (args)
            {
                case "PP":
                    WritePaymentOffset();
                    break;
                case "FP":
                    WritePaymentOffset();
                    break;
                case "FF":
                    WritePaymentOffset();
                    break;
                case "SP":
                    WritePaymentOffset();
                    break;
            }

            //This condition is needed because we are at the end of the record set and
            //need to check one final close condition
            bWriteFinalClose = false;
            if (iPCL50Count == iRecCntPMSPSCL50)
            {
                if (Globals.objPCL50.PMSPCL50RecStatus == "O" || Globals.objPCL50.PMSPCL50RecStatus == "R")
                {
                    bWriteFinalClose = true;
                }
            }

            if (bWriteFinalClose == true)
            {
                CloseReserve();
                bWriteFinalClose = false;
            }

            Globals.dbg.PopProc();
        }
        private static void PaymentOffsetOnset(DbReader dbReader)
        {

            Globals.dbg.PushProc("PaymentOffsetOnset");
            iPCL50Count = 1;
            iPCL50PayCount = 0;
            Globals.InitForReservePaymentTransaction(Globals.objPCL50);

            //Globals.PointClaimDErsReservePaymentOffset.MoveFirst();
            //while (!(iPCL50Count >  Globals.PointClaimDErsReservePaymentOffset.RecordCount))
            //{
            //    FormatOffsetPaymentRow();
            //    Globals.PointClaimDErsReservePaymentOffset.MoveNext();
            //    iPCL50Count = iPCL50Count + 1;
            //}
            do
            {
                FormatOffsetPaymentRow(dbReader);
                iPCL50Count = iPCL50Count + 1;

            } while (dbReader.Read());

            Globals.dbg.PopProc();
        }
        private static void CloseReserve()
        {
            if (iPCL50PayCount > 0)
            {
                Globals.objPCL50.PMSPCL50ClaimTrans = "CL";
            }
            else
            {
                Globals.objPCL50.PMSPCL50ClaimTrans = "NC";
            }
            Globals.objPCL50.PMSPCL50RecStatus = "C";
            Globals.objPCL50.PMSPCL50ResvPayAmt = 0;
            Globals.objPCL50.PMSPCL50TotPayment = 0;
            Globals.objPCL50.PMSPCL50CurrResv = 0;
            Globals.objPCL50.PMSPCL50AcctDte = 0;
            Globals.objPCL50.PMSPCL50TranEntDt = Convert.ToInt64(Globals.sCurrentEntryDate);
            Globals.bProcessingReserve = true;
            Globals.bProcessingPayment = false;
            Globals.bStatusChangeTrans = true;
            LoadDatabase.LocateReservePayment();
            iPCL50PayCount = 0;
        }
        private static void WritePaymentOffset()
        {

            long lHldTransSeq = 0;
            string sSql = null;
            //ADODB.Recordset rs = default(ADODB.Recordset);
            string sFldList = null;
            string sInsList = null;
            object rowsAffected;
            DbReader dbReader = null;
            try
            {
                Globals.dbg.PushProc("WritePaymentOffset");

                iPCL50PayCount = iPCL50PayCount + 1;

                if (IsPaymentAlreadyOffset())
                {
                    //goto hExit;
                    return;
                }

                //if (Globals.PointClaimDErsClaimCoverage.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
                //{
                //    Globals.PointClaimDErsClaimCoverage.Close();
                //}
                //LoadDatabase.PointClaimDEClaimCoverage(Globals.objPCL50.PMSPCL50Claim, Globals.objPCL50.PMSPCL50ClmtSeq, Globals.objPCL50.PMSPCL50PolCovSeq);
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, "SELECT * FROM PMSPCL42 WHERE CLAIM='" + Globals.objPCL50.PMSPCL50Claim + "' AND CLMTSEQ=" + Globals.objPCL50.PMSPCL50ClmtSeq + " AND POLCOVSEQ=" + Globals.objPCL50.PMSPCL50PolCovSeq);
                lHldTransSeq = Globals.objPCL50.PMSPCL50TransSeq;

                //First update the DRFTFILE entry for the original payment to void it out
                sSql = "UPDATE DRFTFILE SET PAYSTATUS = 'V' " + " WHERE CLAIM = '" + Globals.sClaimNumber + "'" + "   AND CLMTSEQ = " + Globals.objPCL50.PMSPCL50ClmtSeq + "   AND POLCOVSEQ = " + Globals.objPCL50.PMSPCL50PolCovSeq + "   AND RESVNO = " + Globals.objPCL50.PMSPCL50ResvNo + "   AND TRANSSEQ = " + Globals.objPCL50.PMSPCL50TransSeq;
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "DRFTFILE_Status", sSql);
                Globals.dbg.DebugTrace("DRFTFILE", sSql, "", "", "");
                DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "DRFTFILE_Status", sSql);

                //if (Globals.PointClaimDErsClaimCoverage.Fields["RECSTATUS"].Value == "C")
                if (dbReader.Read() && Convert.ToString(dbReader.GetValue("RECSTATUS")) == "C")
                {
                    Globals.objPCL50.PMSPCL50ClaimTrans = "PO";
                }
                else
                {
                    Globals.objPCL50.PMSPCL50ClaimTrans = "OA";
                }
                Globals.objPCL50.PMSPCL50ReinsInd = " ";

                Globals.objPCL50.PMSPCL50TotPayment = Globals.objPCL50.PMSPCL50TotPayment + Globals.objPCL50.PMSPCL50ResvPayAmt;
                Globals.objPCL50.PMSPCL50ResvPayAmt = Globals.objPCL50.PMSPCL50ResvPayAmt * -1;
                Globals.objPCL50.PMSPCL50AcctDte = 0;
                Globals.objPCL50.PMSPCL50TranEntDt = Convert.ToInt64(Globals.sCurrentEntryDate);
                Globals.bProcessingReserve = false;
                Globals.bProcessingPayment = true;
                Globals.bStatusChangeTrans = false;
                LoadDatabase.LocateReservePayment();

                if (Globals.cPointRelease > 8)
                {
                    //Need to add a corresonding offset in the DRFTFILE
                    sSql = "SELECT * FROM DRFTFILE WHERE 0=1";
                    //dbReader = LoadDatabase.ExecuteReader(sSql);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    sFldList = BuildFieldList(dbReader);

                    //rs = Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    //sFldList = BuildFieldList(rs);
                    //rs.Close();
                    sInsList = "(" + sFldList + ")";
                    sFldList = sFldList.Replace("TRANSSEQ,", Globals.objPCL50.PMSPCL50TransSeq + " AS TRANSSEQ,");
                    sFldList = sFldList.Replace("CRTDATE,", "'" + Convert.ToInt64(Globals.sCurrentEntryDate) + "' AS CRTDATE,");
                    sFldList = sFldList.Replace("CRTTIME,", "'" + Globals.sHHNNSS000000Now + "' AS CRTTIME,");
                    sFldList = sFldList.Replace("TRANSCODE,", "'" + Globals.objPCL50.PMSPCL50ClaimTrans + "' AS TRANSCODE,");
                    sFldList = sFldList.Replace("PAYSTATUS,", "' ' AS PAYSTATUS,");
                    sSql = "INSERT INTO DRFTFILE " + sInsList;
                    sSql = sSql + " SELECT " + sFldList;
                    sSql = sSql + "  FROM DRFTFILE WHERE CLAIM = '" + Globals.sClaimNumber + "'" + " AND CLMTSEQ = " + Globals.objPCL50.PMSPCL50ClmtSeq + " AND POLCOVSEQ = " + Globals.objPCL50.PMSPCL50PolCovSeq + " AND RESVNO = " + Globals.objPCL50.PMSPCL50ResvNo + " AND TRANSSEQ = " + lHldTransSeq;

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtBSave, "DRFTFILE_OFFSET", sSql);
                    Globals.dbg.DebugTrace("DRFTFILE", sSql, "", "", "");
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                    //Globals.PointClaimDEPointClaimAS400.Execute(sSql, out rowsAffected);
                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlOfOn, ScriptingFunctions.conScpEvtASave, "DRFTFILE_OFFSET", sSql);
                }
            }
            finally
            {

                //if (Globals.PointClaimDErsClaimCoverage.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
                //{
                //    Globals.PointClaimDErsClaimCoverage.Close();
                //}
                Globals.dbg.PopProc();
            }
        }

        private static bool IsPaymentAlreadyOffset()
        {
            bool bIsOffset = false;
            try
            {
                Globals.dbg.PushProc("IsPaymentAlreadyOffset");

                bIsOffset = false;

                if (!string.IsNullOrEmpty(StringUtils.Trim(Globals.objPCL50.PMSPCL50OffsetSw)))
                {
                    bIsOffset = true;
                    // goto ipa_Exit;
                    return bIsOffset;
                }

                //if (Globals.PointClaimDErsCheckRecord.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
                //{
                //    Globals.PointClaimDErsCheckRecord.Close();
                //}
                //PointClaimDECheckRecord(Globals.objPCL50.PMSPCL50Claim, Convert.ToString(Globals.objPCL50.PMSPCL50ClmtSeq), Convert.ToString(Globals.objPCL50.PMSPCL50PolCovSeq), Convert.ToString(Globals.objPCL50.PMSPCL50ResvNo), Convert.ToString(Globals.objPCL50.PMSPCL50TransSeq));
                string sResult = Convert.ToString(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, " SELECT MST_PAY_STATUS FROM PMSPAP00 WHERE CLAIMNO = '" + Globals.objPCL50.PMSPCL50Claim + "' AND CLMTSEQ = '" + Convert.ToString(Globals.objPCL50.PMSPCL50ClmtSeq) + "' AND POLCOVSEQ = '" + Convert.ToString(Globals.objPCL50.PMSPCL50PolCovSeq) + "'  AND RESVNO = '" + Convert.ToString(Globals.objPCL50.PMSPCL50ResvNo) + "' AND TRANSSEQ = '" + Convert.ToString(Globals.objPCL50.PMSPCL50TransSeq) + "'")); 
                //if ((Globals.PointClaimDErsCheckRecord.BOF == true) || (Globals.PointClaimDErsCheckRecord.EOF == true))
                //{
                //}
                //else
                if(!string.IsNullOrEmpty(sResult))
                {
                    if (sResult == "V")
                    {
                        bIsOffset = true;
                    }
                }
               // Globals.PointClaimDErsCheckRecord.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //ipa_Exit:

                Globals.dbg.PopProc();
            }
            return bIsOffset;
        }

        //public static void PointClaimDECheckRecord(string sPMSPCL50Claim, string sPMSPCL50ClmtSeq, string sPMSPCL50PolCovSeq, string sPMSPCL50ResvNo, string sPMSPCL50TransSeq)
        //{

        //    string sSql = null;

        //    if (Globals.PointClaimDErsCheckRecord.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
        //    {
        //        Globals.PointClaimDErsCheckRecord.Close();
        //    }
        //    sSql = " SELECT * FROM PMSPAP00 WHERE CLAIMNO = '" + sPMSPCL50Claim + "' AND CLMTSEQ = '" + sPMSPCL50ClmtSeq + "' AND POLCOVSEQ = '" + sPMSPCL50PolCovSeq + "'  AND RESVNO = '" + sPMSPCL50ResvNo + "' AND TRANSSEQ = '" + sPMSPCL50TransSeq + "'";
        //    Globals.PointClaimDErsCheckRecord.Open(sSql, Globals.PointClaimDEPointClaimAS400, ADODB.CursorTypeEnum.adOpenUnspecified, ADODB.LockTypeEnum.adLockUnspecified, 3);
        //    //Set PointClaimDErsCheckRecord = PointClaimDEPointClaimAS400.Execute(sSql)


        //}

        //public static void PointClaimDEReservePaymentOffset(string sClaimNumber)
        //{
        //    string sSql = null;
        //    // Set PointClaimDErsReservePaymentOffset = New ADODB.Recordset
        //    if (Globals.PointClaimDErsReservePaymentOffset.State == Convert.ToInt16(ADODB.ObjectStateEnum.adStateOpen))
        //    {
        //        Globals.PointClaimDErsReservePaymentOffset.Close();
        //    }
        //    sSql = "SELECT * FROM PMSPCL50  WHERE CLAIM ='" + sClaimNumber + "' ORDER BY CLMTSEQ, POLCOVSEQ, RESVNO, TRANSSEQ";
        //    Globals.PointClaimDErsReservePaymentOffset.Open(sSql, Globals.PointClaimDEPointClaimAS400, ADODB.CursorTypeEnum.adOpenUnspecified, ADODB.LockTypeEnum.adLockUnspecified, 3);
        //    //Set PointClaimDErsReservePaymentOffset = PointClaimDEPointClaimAS400.Execute(sSql)

        //}

    }
}
