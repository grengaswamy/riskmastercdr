﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using DTGSCRIPTLib;
using CCP.Common;
using Riskmaster.Application.PolicySystemInterface;
using System.IO;


namespace CCP.PointClaimsLoad
    {
    class ScriptingFunctions
        {
        //Scripting Variables
        public static string sScriptBlock;
        public static string sScriptFile;
        public static string sScriptPath;
        public static string sScriptProc;
        public static DTGScript DTGScriptObject;
#if VBS
#endif
        public object oVBS;

        public const string SWScript = "Script";
        public const string SWScriptPath = ".Path";
        public const string SWScriptProc = ".Proc";
        public const string sScriptErrorsSW = ".Errors";
        public const string sScriptMissingSW = ".Missing";
        public const string sScriptErrorAbort = "ABORT";
        public const string sScriptErrorIgnore = "IGNORE";
        public const string sScriptErrorNotify = "NOTIFY";

        public const string sScriptErrorLog = "LOG";

        //Scripting Constants

        public const string conScptLvlApp = "Application";
        public const string conScptLvlDoc = "Document";
        public const string conScptLvlLoad = "Load";
        public const string conScptLvlClm = "Claim";
        public const string conScptLvlCovg = "Coverage";
        public const string conScptLvlTrns = "Transaction";
        public const string conScptLvlDrft = "Draft";
        public const string conScptLvlChck = "Check";
        public const string conScptLvlIndm = "Indemnity";
        public const string conScptLvlClmt = "Claimant";
        public const string conScptLvlVndr = "Vendor";
        public const string conScptLvlWC = "WComp";
        public const string conScptLvlOfOn = "OffsetOnset";
        public const string conScpEvtStart = "Start";
        public const string conScpEvtEnd = "End";
        public const string conScpEvtBSave = "BeforeSave";
        public const string conScpEvtASave = "AfterSave";
        public const string conScptLvlTaxId = "TaxId";
        public const string conScptlvlEntity = "Entity";

        private int response;
        //These objects are handed over to scripting
        //CCPUF.CDebug
        private static Debug sdbg;
        //public static Debug dbg;
        //CCPUF.CParameters
        private static Parameters sParams;
        //CCPUF.SystemInfo
        private static SystemInfo sSysSet;
        //CCPUF.CFileFunctions           'SI06380
        private static FileFunctions sFleFnc;
        private static ScriptEngine m_ScriptEngine = null;
        private static ScriptEngine objScriptEngine
            {
            get
                {
                if (m_ScriptEngine == null)
                    m_ScriptEngine = new ScriptEngine(Globals.objConfig.ConfigElement.DataDSNConnStr);
                    return m_ScriptEngine;
                }
            }
        public static bool InitializeScripting()
            {

            bool functionReturnValue = false;
            // ERROR: Not supported in C#: OnErrorStatement

            string sPN = null;

            sPN = "InitializeScripting";
            Globals.dbg.PushProc(sPN);
            try
                {
                if (GetScriptEngine((object)Globals.dbg, (object)Globals.mParams(), (object)Globals.sysSettings()))
                    {
                    subRegGlobal("CFunctions", Globals.CFleFnc());
                    subRegGlobal("CDBFunctions", new DBFunctions());
                    subRegGlobal("cXML", Globals.XMLObj());
                    subRegGlobal("cXMLControls", CCP.LossBalance.LossBalance.Instance.cXMLSControls);
                    subRegGlobal("Utils", Globals.objUtils);
                    //SI06851
                    //SI06851'subRegGlobal "Utils", objUtils
                    //      subRegGlobal "POINTClaim", PointClaim
                    //      subRegGlobal "POINTSupport", PointSupport
                    //      subRegGlobal "UCT", UniversalTranslation
                    //      subRegGlobal "PointMrs7Tables", PointMrs7Tables
                    //      subRegGlobal("BASCLT", BASCLT)
                    subRegGlobal("objPCL20", Globals.objPCL20);
                    subRegGlobal("objPCL14", Globals.objPCL14);
                    subRegGlobal("objPCL30", Globals.objPCL30);
                    subRegGlobal("objPCT30", Globals.objPCT30);
                    subRegGlobal("objPCL42", Globals.objPCL42);
                    subRegGlobal("objPCL50", Globals.objPCL50);
                    subRegGlobal("objDRFT", Globals.objDRFT);
                    subRegGlobal("objAP00", Globals.objAP00);
                    subRegGlobal("objAD00", Globals.objAD00);
                    subRegGlobal("objPCL92", Globals.objPCL92);
                    subRegGlobal("objBASCLT0100", Globals.objBASCLT0100);
                    subRegGlobal("objBASCLT0300", Globals.objBASCLT0300);
                    subRegGlobal("objBASCLT1700", Globals.objBASCLT1700);
                    subRegGlobal("objPTX00", Globals.objPTX00);
                    }

                functionReturnValue = true;
                }
            catch (Exception ex)
                {
                // Probably add some logging here
                //response = sdbg.Errors.ProcessError(ERRC_SCRIPT_ENGINE_LOAD, sPN, Err);
                functionReturnValue = false;
                // ERROR: Not supported in C#: ResumeStatement
                }
            finally
                {
                Globals.dbg.PopProc();
                }


            return functionReturnValue;
            }

        public static bool GetScriptEngine(object odbg, object oParams, object oSysInfo)
            {
            //return true;

            // DTGScriptObject receives reference to scription engine
            // Function return true if everything was OK

            string sPN = null;

            // ERROR: Not supported in C#: OnErrorStatement
           

            sPN = "GetScriptEngine";
            sdbg = (Debug)odbg;
            sParams = (Parameters)oParams;
            sSysSet = (SystemInfo)oSysInfo;
           
            sdbg.PushProc(sPN,null,null,null,null);


            dynamic functionReturnValue;
       // reload_script:
            try
                {
            DTGScriptObject = null;
             /* Load the script during execution only.
            subLoadScriptFile();

            if (string.IsNullOrEmpty(sScriptBlock))
                {
                //functionReturnValue = false;
                return false;
                }
                */
            // Try to create scripting engine
            DTGScriptObject = new DTGScript();

          //  DTGScriptObject.AddScript(sScriptBlock);

            // Now plug global objects here using RegisterGlobalObject
            sFleFnc = Globals.CFleFnc();
            //SI06380
            sFleFnc.CDebug = sdbg;
            //SI06380
            DTGScriptObject.RegisterGlobalObject("FileFunctions", sFleFnc, 0);
            //SI06380
            DTGScriptObject.RegisterGlobalObject("Debug", sdbg, 0);
            DTGScriptObject.RegisterGlobalObject("Parameters", sParams, 0);
            DTGScriptObject.RegisterGlobalObject("SystemInfo", sSysSet, 0);

            functionReturnValue = true;
            }
                               catch(Exception ex)
           {
            // Probably add some logging here
            //response = sdbg.CErrors.ProcessError(ERRC_SCRIPT_ENGINE_LOAD, sPN, Err);
           // if (response == Constants.vbRetry)
                // ERROR: Not supported in C#: ResumeStatement

                DTGScriptObject = null;
            functionReturnValue = false;
            // ERROR: Not supported in C#: ResumeStatement
           }
     finally
         {
            sdbg.PopProc();
         }

            return functionReturnValue;

            }

        public static void subLoadScriptFile()
            {
            string snm = null;
            string spt = null;
            FileFunctions CFleFnc = Globals.CFleFnc();
            //  Dim cFle As Object 'CCPUF.CFileFunctions                                   'SI06380

            // ERROR: Not supported in C#: OnErrorStatement
            try
                {
                sdbg.PushProc("subLoadScriptFile");

                //  cFle = CreateObject("CCPUF.CFileFunctions") 'New CCPUF.CFileFunctions  'SI06380


                snm = Globals.mParams().Parameter(SWScript + "." + Globals.dbg.AppTitle);
                sdbg.DebugTrace(snm, "", "", "", "");

                if (!string.IsNullOrEmpty(snm))
                    {
                    if (StringUtils.InStr(snm, "\\") == 0)
                        {
                        spt = Globals.mParams().Parameter(SWScript + "." + Globals.dbg.AppTitle + SWScriptPath);
                        }
                    else
                        {
                        spt = "";
                        }
                    sScriptBlock = "";
                    if (CFleFnc.FileExists(spt, snm))
                        {
                        CFleFnc.FileOpen(spt, snm, FileFunctions.cFileAccess.enFAForWriting, false);
                        sScriptBlock = CFleFnc.FileRead(); //CFleFnc.FileGet
                        CFleFnc.FileClose();
                        sdbg.DebugTrace(sScriptBlock, "", "", "", "");
                        }
                    else
                        {
                        //sdbg.CErrors.ProcessAppError(ERRC_SCRIPT_FILE_NOTFOUND, , , , snm);
                        }
                    }
                }
            catch (Exception ex)
                {

                }
            finally
                {

                }


            // ERROR: Not supported in C#: ResumeStatement


            }

        public static void subEndScripting()
            {
            DTGScriptObject = null;
            sParams = null;
            sSysSet = null;
            sdbg = null;
            }

        public static void subRegGlobal(string nme, object obj)
            {
            if ((DTGScriptObject != null))
                {
                DTGScriptObject.RegisterGlobalObject(nme, obj, 0);
                }
            }

        //#if VBS
        //public void initVBS()
        //{
        //  oVBS =  Interaction.CreateObject("VBSPC.VBS");

        //    oVBS.CDebug = sdbg;
        //    oVBS.CDBFunctions = CDBFunc;
        //    oVBS.CFunctions = CFunc;
        //    oVBS.CFileFunctions = sFleFnc;
        //    oVBS.CParameters = sParams;
        //    oVBS.cXML = XMLObj;
        //    oVBS.cXMLClaim = XMLClaim;
        //    oVBS.cXMLControls = XMLControls;
        //    oVBS.vobjAD00 = objAD00;
        //    oVBS.vobjAP00 = objAP00;
        //    oVBS.vobjDRFT = objDRFT;
        //    oVBS.vobjPCL14 = objPCL14;
        //    oVBS.vobjPCL20 = objPCL20;
        //    oVBS.vobjPCL30 = objPCL30;
        //    oVBS.vobjPCL42 = objPCL42;
        //    oVBS.vobjPCL50 = objPCL50;
        //    oVBS.vobjPCL92 = objPCL92;
        //    oVBS.vobjPCT30 = objPCT30;
        //    oVBS.vobjPTX00 = objPTX00;
        //    oVBS.vobjBASCLT0100 = objBASCLT0100;
        //    oVBS.vobjBASCLT0300 = objBASCLT0300;
        //    oVBS.vobjBASCLT1700 = objBASCLT1700;
        //    oVBS.vobjUtils = objUtils;
        //    oVBS.PointClaim = PointClaim;
        //    oVBS.PointMrs7Tables = PointMrs7Tables;
        //    oVBS.PointSupport = PointSupport;
        //    oVBS.SystemInfo = sSysSet;
        //    oVBS.UCT = UniversalTranslation;

        //}
        //#endif


        //public static object funExecuteScript(string sEvent, string sLevel, Hashtable htInput)

        public static object funExecuteScript(string sEvent, string sLevel, params object[] var)
           
            {

            //ScriptEngine objScriptEngine = this.objScriptEngine;
            string sScriptText = string.Empty;
            string sObjectName = string.Empty;
            int iRowId = 0;
            //DataModelFactory objDMF;
            try
                {
                Log.Write("Omig coverage save process start");
                //objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword);
                //sObjectName = p_objXmlIn.SelectSingleNode("//ObjectName").InnerText;
                //Raman 6/27 Script Editor is crashing due to htmlencode at UI layer
                //sScriptText = p_objXmlIn.SelectSingleNode( "//ScriptText" ).InnerText ;

                //get the script text from file system
                using (StreamReader sr = new StreamReader(Path.Combine(Riskmaster.Common.RMConfigurator.UserDataPath, @"PolicyInterface\CustomUploadScripts\CustomScript.vb")))
                    {
                    Log.Write("script file locate");
                    sScriptText = sr.ReadToEnd();
                    //Console.WriteLine(line);
                    }

                sScriptText = System.Web.HttpUtility.HtmlDecode(sScriptText);

                //objScriptEngine = new ScriptEngine(m_sConnectString);

                //if (!objScriptEngine.IsReady)
                //{
                //objScriptEngine.ForceLoad();
                objScriptEngine.AddScript(sScriptText);
                //}

                objScriptEngine.StartEngine();

                //objScriptEngine.ResetEngine();

                //objScriptEngine.Compile();

                //objScriptEngine.StoreBinary();

                //objScriptEngine.StartEngine();

                //SaveScriptInDB(p_objXmlIn, ref iRowId);

                //this.LoadScript(iRowId);
                objScriptEngine.RunScriptMethod("Application_Start",null);

                }
            //catch (RMAppException p_objEx)
            //    {
            //    throw p_objEx;
            //    }
            catch (Exception p_objEx)
                {
               // throw new RMAppException(Globalization.GetString("ScriptEditorManager.SaveScript.Error"), p_objEx);
                }
            finally
                {

                }
            return "";
            //return (m_objXmlDocument);

           
            //System.Diagnostics.Process Proc = new System.Diagnostics.Process();
            //Proc.StartInfo.FileName = @"wscript";
            //Proc.StartInfo.Arguments = "//B //Nologo D:\\RMX\\RMA 14.1 Source-Policy Upload Conversion_New\\UI\\riskmaster\\userdata\\PolicyInterface\\ScriptFiles\\vbscript.vbs 1 2";
            //Proc.Start();
            //Proc.WaitForExit();
            //Proc.Close();

            ////DTGScriptObject.Run("D:\\RMX\\RMA 14.1 Source-Policy Upload Conversion_New\\UI\\riskmaster\\userdata\\PolicyInterface\\ScriptFiles\\vbscript.vbs", var);
            //return "";

            
//dynamic functionReturnValue = true;
//string sProcName = null;
//int response = 0;
//string sPN = null;
//object []val = null;
// // ERROR: Not supported in C#: OnErrorStatement

//sPN = "funExecuteScript";
//sdbg.PushProc(sPN, sEvent, sLevel,null,null);
//try
//    {

//if (!string.IsNullOrEmpty(sScriptBlock))
//    {//goto hExit;
//sScriptProc = Globals.mParams().Parameter(SWScript + "." + dbg.AppTitle + SWScriptProc);

//// Example:  Proc_StartClaim
//sProcName = sScriptProc + "_" + sEvent + sLevel;

////If you need to debug a particular routine and it is not in this case, then
////Put is in the case for direct execution.
////Your script needs to be added to the VBS.CLS code
////to put VBS = -1 on the command line in the project property to debug thru VB.
////When finish debugging, for recompiling needs to put VBS = 0

//#if VBS
//initVBS();
////   Select Case sProcName
////      Case "Proc_ApplicationStart"
////         oVBS.Proc_ApplicationStart
////      Case "Proc_ClaimantAfterSave"
////         oVBS.Proc_ClaimantAfterSave var(0), var(1)
////      Case "Proc_CoverageAfterSave"
////         oVBS.Proc_CoverageAfterSave var(0), var(1)
////      Case "Proc_ClaimEnd"
////         oVBS.Proc_ClaimEnd
////   End Select
//#endif

//sdbg.DebugTrace("b", sProcName,null,null,null);

////switch (Information.UBound(var)) {
//switch (var.Length - 1)
//    {
//    case -1:
//        val = DTGScriptObject.Run(sProcName, null);
//        break;
//    case 0:
//        val = DTGScriptObject.Run(sProcName, var[0]);
//        break;
//    case 1:
//        val = DTGScriptObject.Run(sProcName, var[1], var[0]);
//        break;
//    case 2:
//        val = DTGScriptObject.Run(sProcName, var[2], var[1], var[0]);
//        break;
//    case 3:
//        val = DTGScriptObject.Run(sProcName, var[3], var[2], var[1], var[0]);
//        break;
//    default:
//        val = DTGScriptObject.Run(sProcName, var[4], var[3], var[2], var[1], var[0]);
//        break;

//        functionReturnValue = true;
//    }}}
//            catch(Exception ex)
//    {

//long en = 0;
//string es = null;
//string ed = null;
////SI07412
//bool bhia = false;
//bool bhie = false;
//string sTmp = null;
////en = Err.Number;
////es = Err.Source;
////ed = Err.Description;
////SI07412
////SI07412
//if ((sdbg != null)) {
//    bhia = sdbg.Interactive;
//    bhie = sdbg.IgnoreErrors;
//    sTmp = SWScript + "." + dbg.AppTitle;
//    //SI07412funExecuteScript = Err.Number
//    //SI07412If Err.Number = SCRIPT_EXECUTION_ERROR Then
//    //funExecuteScript = en;
//    //SI07412
//    //SI07412
//    //if (en == SCRIPT_EXECUTION_ERROR) {
//    //    if (Strings.InStr(Err.Description, SCRIPT_PROC_NOTFOUND) > 0) {
//    //        if (mParams.Parameter(sTmp + sScriptMissingSW) == sScriptErrorLog) {
//    //            sdbg.Interactive = false;
//    //            sdbg.IgnoreErrors = true;
//    //        }
//    //        if (mParams.Parameter(sTmp + sScriptMissingSW) != sScriptErrorIgnore) {
//    //            //response = sdbg.CErrors.ProcessAppError(ERRC_SCRIPT_PROCEDURE_NOTFOUND, Constants.vbAbortRetryIgnore,0 ,0 , sProcName);
//    //        }
//    //        if (mParams.Parameter(sTmp + sScriptMissingSW) == sScriptErrorAbort) {
//    //            //response = Constants.vbAbort;
//    //        }
//    //    } else {
//    //        if (mParams.Parameter(sTmp + sScriptErrorsSW) == sScriptErrorLog) {
//    //            sdbg.Interactive = false;
//    //            sdbg.IgnoreErrors = true;
//    //        }
//    //        if (mParams.Parameter(sTmp + sScriptErrorsSW) != sScriptErrorIgnore) {
//    //            //Start 'SI07412
//    //            //response = sdbg.CErrors.ProcessAppError(ERRC_SCRIPT_ERROR, Constants.vbAbortRetryIgnore, ed, sProcName + ":" + DTGScriptObject.LastErrorSourceLine, en, es, DTGScriptObject.LastErrorSourceLineNum, DTGScriptObject.LastErrorSourceLineCharPos);
//    //            //               response = sdbg.CErrors.ProcessAppError(ERRC_SCRIPT_ERROR, _
//    //            //                                                      vbAbortRetryIgnore, Err.Description, _
//    //            //                                                      sProcName & ":" & DTGScriptObject.LastErrorSourceLine, _
//    //            //                                                      Err.Number, _
//    //            //                                                      Err.Source, _
//    //            //                                                      DTGScriptObject.LastErrorSourceLineNum, _
//    //            //                                                      DTGScriptObject.LastErrorSourceLineCharPos)
//    //            //End 'SI07412
//    //        }
//    //        if (mParams.Parameter(sTmp + sScriptErrorsSW) == sScriptErrorAbort) {
//    //            response = Constants.vbAbort;
//    //        }
//    //    }
//    //} else {
//    //    response = sdbg.CErrors.ProcessError(ERRC_SCRIPT_EXECUTION, sPN, Err);
//    //}
//    //sdbg.Interactive = bhia;
//    //sdbg.IgnoreErrors = bhie;
//    //if (response == Constants.vbRetry)
//    //     // ERROR: Not supported in C#: ResumeStatement

//    //if (response == Constants.vbIgnore)
//    //     // ERROR: Not supported in C#: ResumeStatement

//    //if (response == Constants.vbAbort)
//    //    bContinue = false;
//    // // ERROR: Not supported in C#: ResumeStatement

////SI07412
//} else {
//    //Err.Raise(en, es, ed);
//    //SI07412)
//}
//functionReturnValue = false;
}
//finally
//    {

//sdbg.PopProc(funExecuteScript, Globals.bContinue,null,null);

    //}

//return functionReturnValue;
          //  }

        }
    }
