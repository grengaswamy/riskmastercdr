﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL20ClaimsMasterFile
    {
        public string PMSPCL20Claim;
        public string PMSPCL20RecStatus;
        public string PMSPCL20ClmOffice;
        public string PMSPCL20Examiner;
        public string PMSPCL20ExaminerCd;
        //SI04558
        public long PMSPCL20LossDte;
        public string PMSPCL20LossTime;
        //SI04558
        public long PMSPCL20RptDte;
        public string PMSPCL20RptTime;
        //SI04558
        public long PMSPCL20ClmMadeDte;
        public string PMSPCL20RptByNme;
        public string PMSPCL20ClmDesc;
        public string PMSPCL20ClaimType;
        public string PMSPCL20ClaimState;
        public string PMSPCL20CatCode;
        //SI04558
        public long PMSPCL20NxtRvwDte;
        public string PMSPCL20DescCode;

        public PMSPCL20ClaimsMasterFile()
        {
            Clear();
        }
        public void Clear()
        {
            PMSPCL20Claim = "";
            PMSPCL20RecStatus = "";
            PMSPCL20ClmOffice = "";
            PMSPCL20Examiner = "";
            PMSPCL20ExaminerCd = "";
            PMSPCL20LossDte = 0;
            //SI04558
            PMSPCL20LossTime = "";
            PMSPCL20RptDte = 0;
            //SI04558
            PMSPCL20RptTime = "";
            PMSPCL20ClmMadeDte = 0;
            //SI04558
            PMSPCL20RptByNme = "";
            PMSPCL20ClmDesc = "";
            PMSPCL20ClaimType = "";
            PMSPCL20ClaimState = "";
            PMSPCL20CatCode = "";
            PMSPCL20NxtRvwDte = 0;
            //SI04558
            PMSPCL20DescCode = "";
        }
    }
}
