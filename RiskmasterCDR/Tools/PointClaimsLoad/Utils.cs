﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
namespace CCP.PointClaimsLoad
{
    /// <summary>
    /// Utility functions for scripting
    /// </summary>
    public class Utils
    {
        public string ConvertDateToPoint(string strPointYYYYMMDD)
        {
            string functionReturnValue = null;
            string sYYYY = null;
            string sCYY = null;

            functionReturnValue = "";

            if (!string.IsNullOrEmpty(strPointYYYYMMDD))
            {
                sYYYY = StringUtils.Left(strPointYYYYMMDD, 4);
                sCYY = ConvertYearToPoint(sYYYY);
                functionReturnValue = sCYY + StringUtils.Right(strPointYYYYMMDD, 4);
            }
            return functionReturnValue;

        }
        public string ConvertPointYear(string strPointYr)
        {
            string functionReturnValue = null;
            string strTemp = null;
            if (StringUtils.Left(strPointYr, 1)=="1")
            {
                functionReturnValue = "20" + StringUtils.Right(strPointYr, 2);
            }
            else
            {
                functionReturnValue = "19" + StringUtils.Right(strPointYr, 2);
            }
            return functionReturnValue;
        }
        public string ConvertYearToPoint(string strPointYr)
        {
            string functionReturnValue = null;
            string strTemp = null;
            if (StringUtils.Left(strPointYr, 1)=="2")
            {
                functionReturnValue = "1" + StringUtils.Mid(strPointYr, 3, 2);
            }
            else
            {
                functionReturnValue = "0" + StringUtils.Mid(strPointYr, 3, 2);
            }
            return functionReturnValue;
        }
        
//        Public Property Get Continue() As Boolean
//   Continue = bContinue
//End Property

//Public Property Let Continue(ByVal bNV As Boolean)
//   bContinue = bNV
//End Property


    }
}
