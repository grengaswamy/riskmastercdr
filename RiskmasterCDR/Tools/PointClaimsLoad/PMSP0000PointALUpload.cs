﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSP0000PointALUpload
    {
        public string PMSP0000Symbol;
        public string PMSP0000POLICY0NUM;
        public string PMSP0000Module;
        public string PMSP0000MASTER0CO;
        public string PMSP0000Location;
        public string PMSP0000TYPE0ACT;
        public string PMSP0000ACT0YEAR;
        public string PMSP0000ACT0MONTH;
        public string PMSP0000ACT0DAY;
        public string PMSP0000OPER0ID;
        public string PMSP0000COMPANY0NO;
        public string PMSP0000DELETE0IND;
        public string PMSP0000TRANS0STAT;
        public string PMSP0000ETIME;
        public string PMSP0000ENT0YEAR;
        public string PMSP0000ENT0MONTH;
        public string PMSP0000ENT0DAY;
        public string PMSP0000ERROR001;
        public string PMSP0000ERROR002;
        public string PMSP0000ERROR003;
        public string PMSP0000ERROR004;
        public string PMSP0000ERROR005;
        public string PMSP0000ERROR006;
        public string PMSP0000ERROR007;
        public string PMSP0000ERROR008;
        public string PMSP0000ERROR009;
        public string PMSP0000ERROR010;
        public string PMSP0000ERROR011;
        public string PMSP0000ERROR012;
        public string PMSP0000ERROR013;
        public string PMSP0000ERROR014;
        public string PMSP0000ERROR015;
        public string PMSP0000ERROR016;
        public string PMSP0000ERROR017;
        public string PMSP0000ERROR018;
        public string PMSP0000ERROR019;
        public string PMSP0000ERROR020;
        public string PMSP0000REC0CNT;
        public string PMSP0000COMPRATEIN;


        public PMSP0000PointALUpload()
        {
            Clear();
        }


        public void Clear()
        {
            PMSP0000Symbol = "";  //Symbol
            PMSP0000POLICY0NUM = ""; //POLICY0NUM
            PMSP0000Module = ""; //Module
            PMSP0000MASTER0CO = ""; //MASTER0CO
            PMSP0000Location = "";  //Location
            PMSP0000TYPE0ACT = "CL";  // CL for all records
            PMSP0000ACT0YEAR = "";  //     Blank for all records
            PMSP0000ACT0MONTH = "";  // Blank for all records
            PMSP0000ACT0DAY = "";  // Blank for all records
            PMSP0000OPER0ID = "";  // Entry user.(Username through which rmA connect to POINT)
            PMSP0000COMPANY0NO = "";  // blank for all records.
            PMSP0000DELETE0IND = "";  // blank
            PMSP0000TRANS0STAT = "V"; // V for all records
            PMSP0000ETIME = "0000"; // 0000  for all records.
            PMSP0000ENT0YEAR = "";  // Blank for all records
            PMSP0000ENT0MONTH = "";// Blank for all records
            PMSP0000ENT0DAY = ""; // Blank for all records
            PMSP0000ERROR001 = ""; // Blank for all records
            PMSP0000ERROR002 = ""; //Blank for all records
            PMSP0000ERROR003 = ""; // Blank for all records
            PMSP0000ERROR004 = ""; // Blank for all records
            PMSP0000ERROR005 = ""; // Blank for all records
            PMSP0000ERROR006 = ""; // Blank for all records
            PMSP0000ERROR007 = "";  // Blank for all records
            PMSP0000ERROR008 = ""; // Blank for all records
            PMSP0000ERROR009 = ""; // Blank for all records
            PMSP0000ERROR010 = ""; // Blank for all records
            PMSP0000ERROR011 = ""; // Blank for all records
            PMSP0000ERROR012 = ""; // Blank for all records
            PMSP0000ERROR013 = ""; // Blank for all records
            PMSP0000ERROR014 = ""; // Blank for all records
            PMSP0000ERROR015 = ""; // Blank for all records
            PMSP0000ERROR016 = "";  // Blank for all records
            PMSP0000ERROR017 = ""; // Blank for all records
            PMSP0000ERROR018 = ""; // Blank for all records
            PMSP0000ERROR019 = ""; // Blank for all records
            PMSP0000ERROR020 = "";  // Blank for all records
            PMSP0000REC0CNT = "0";  // Zero for all records
            PMSP0000COMPRATEIN = ""; // Blank for all records
        }
    }
}
