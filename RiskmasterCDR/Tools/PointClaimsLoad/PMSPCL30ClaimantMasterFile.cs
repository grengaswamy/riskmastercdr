﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL30ClaimantMasterFile
    {
        public string PMSPCL30Claim;
        public int PMSPCL30ClmtSeq;
        public string PMSPCL30RecStatus;
        public int PMSPCL30Suit;
        public long PMSPCL30Client;
        public string PMSPCL30AiaCode1;
        public string PMSPCL30AiaCode2;
        public string PMSPCL30AiaCode3;

        public PMSPCL30ClaimantMasterFile()
        {
            Clear();
        }
        
        public void Clear()
        {
            PMSPCL30Claim = "";
            PMSPCL30ClmtSeq = 0;
            PMSPCL30RecStatus = "";
            PMSPCL30Suit = 0;
            PMSPCL30Client = 0;
            PMSPCL30AiaCode1 = "";
            PMSPCL30AiaCode2 = "";
            PMSPCL30AiaCode3 = "";
        }

    }
}
