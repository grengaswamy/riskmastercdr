﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using CCP.Common;
using Riskmaster.Application.PolicySystemInterface;
using System.IO;


namespace CCP.PointClaimsLoad
    {
    class ScriptingFunctions
        {

        //Scripting Constants

        public const string conScptLvlApp = "PolicyUpload";
        public const string conScptLvlDoc = "ParseDocument";
        public const string conScptLvlLoad = "Load";
        public const string conScptLvlClm = "ParseClaim";
        public const string conScptLvlCovg = "Coverage";
        public const string conScptLvlTrns = "Transaction";
        public const string conScptLvlDrft = "DraftFile";
        public const string conScptLvlChck = "CheckFile";
        public const string conScptLvlIndm = "Indemnity";
        public const string conScptLvlClmt = "Claimant";
        public const string conScptLvlVndr = "Vendor";
        public const string conScptLvlWC = "WCompParseUnitStat";
        public const string conScptLvlOfOn = "OffsetOnset";
        public const string conScpEvtStart = "Start";
        public const string conScpEvtEnd = "End";
        public const string conScpEvtBSave = "BeforeSave";
        public const string conScpEvtASave = "AfterSave";
        public const string conScptLvlTaxId = "TaxId";
        public const string conScptlvlEntity = "Entity";

        //Scripting object
        private static ScriptEngine m_ScriptEngine = null;
        private static int m_iClientId = 0;

        private static ScriptEngine objScriptEngine
            {
            get
                {
                    if (m_ScriptEngine == null)
                        m_ScriptEngine = new ScriptEngine(Globals.objConfig.ConfigElement.DataDSNConnStr, ScriptEngine.ScriptTypes.SCRIPT_UPLOAD, m_iClientId);
                return m_ScriptEngine;
                }
            }
        //start changes for JIRA 7504
        public static bool ScriptFileExist(out string sFileName)
        {

            ScriptEngine objScriptEngine = ScriptingFunctions.objScriptEngine;
            sFileName = ScriptEngine.ScriptName;
            return File.Exists(Path.Combine(Riskmaster.Common.RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\" + ScriptEngine.ScriptName));
        }
        //end changes for JIRA 7504

        
        public static object ExecutePolicyUploadCustomScript(string sEvent, string sLevel, params object[] var)
            {

            ScriptEngine objScriptEngine = ScriptingFunctions.objScriptEngine;
            string sScriptText = string.Empty;
            string sObjectName = string.Empty;
            try
                {
                    // added if check for JIRA 7504, file exists
                    if (!File.Exists(Path.Combine(Riskmaster.Common.RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\" + ScriptEngine.ScriptName)))
                    {
                        //Globals.objLogWriter.WriteLine("Scripting Warning : Script file  " + ScriptEngine.ScriptName + "does not exist.");
                        return "";
                    }
                using (StreamReader sr = new StreamReader(Path.Combine(Riskmaster.Common.RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\" + ScriptEngine.ScriptName)))
                    {
                      sScriptText = sr.ReadToEnd();
                    }

                sScriptText = System.Web.HttpUtility.HtmlDecode(sScriptText);
                objScriptEngine.AddScript(sScriptText);
                objScriptEngine.StartEngine();
                objScriptEngine.RunScriptMethod(sEvent + "_" + sLevel, var);

                }
            catch (Exception p_objEx)
                {
                throw p_objEx;
                }
            finally
                {

                }
            return "";

            }
        }
    }