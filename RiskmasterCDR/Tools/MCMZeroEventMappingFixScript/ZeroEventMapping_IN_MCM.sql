--  /**************************************************************
--	 * $File		: ZeroEventMapping_IN_MCM.sql
--	 * $Revision	: 1.0.0.0
--	 * $Date		: 01/10/2010
--	 * $Author		: Rahul Solanki 
--	 * $Comment		: The tool migrates the docs attached(and mail merge templates) into MCM. 	 
--	*************************************************************   
--    *	
--  The script fixes the Zero event numbers from the Acrosoft Database
--  (1)There is a "Dry Run" mode.  If you provide the value 1 to the DRYRUN variable. 
--  It will only construct the dynamic sql scripts which it run. You can use the DRYRUN to check for errors.
--  If there are Errors indicated in the DRY run then, you can abort on running the script. 
--  "Dry run" mode is only intended for testing the script. 
--	(2)The script will make a update to the CLAIM table in the RISKMASTER database as well. 
--  It well search for Null / zero values the Event_Number  column and will replace them with the appropriate values retrieving 
--  them from the EVENT table based on the EVENT_ID ( this update is outside the Dry run, so even if the Dry run is enabled, 
--  This particular update to RMX db will go thru; )  
--	(3)The script is transactional , so if there are any errors, it will rollback the entire update. 
--  ( in case the script is run in dry run mode and there are NO errors, the updates to the CLAIM table in the RMX 
--  database would be committed. )		
--	(4)In case there were any attachments made to the record with Event number Zero AND subsequently it was 
--  got re-indexed; The script will merge the folders from the two records. there will be some duplicate folders.
--  The folders can be individually renamed thereafter.
--	(5)SQL server 2005 or above are supported.
--	(6)backup of both RMX and MCM databases should be taken before running the script. While the script is running,
--  make sure that none of the "end users" are using the Acrosoft MCM module. Once the script is run please verify that all attachments are directly accessible  and that the issue has been resolved. 
--    */

GO
BEGIN TRANSACTION

-- deleting temp table if already existing 
IF OBJECT_ID('tempdb..##TMP_MAPPING_TABLE' , 'U') IS NOT NULL
        drop table ##TMP_MAPPING_TABLE

DECLARE @@RMX_DB AS NVARCHAR(64)
DECLARE @@ACROSOFT_DB AS NVARCHAR(64)
DECLARE @@DBO_USER AS NVARCHAR(32)
DECLARE @@TEMPVAL AS NVARCHAR(500)
DECLARE @@SQL_NDX_RMXEVENT AS NVARCHAR(500)
DECLARE @@SQL_APP_FOLDER_TAB AS NVARCHAR(500)
DECLARE @@SQL AS NVARCHAR(500)
DECLARE @@CLAIM_NUMBER AS NVARCHAR(64)
DECLARE @@EVENT_NUMBER AS NVARCHAR(64)
DECLARE @@AF_FOLDERKEY AS NVARCHAR(100)
DECLARE @@OLD_AF_FOLDERKEY AS NVARCHAR(100)
DECLARE @@AF_TITLE AS NVARCHAR(64)
DECLARE @@OLD_AF_TITLE AS NVARCHAR(64)
DECLARE @PARMDEFINITION AS NVARCHAR(500);
DECLARE @PARMDEFINITION_NEW_FOLDER_KEY AS NVARCHAR(500);
DECLARE @PARMDEFINITION_OLD_FOLDER_KEY AS NVARCHAR(500);
DECLARE @DRYRUN AS INT
DECLARE @@RECORDCOUNT AS INT
DECLARE @@TOTALCOUNT AS INT
DECLARE @@NEW_FOLDER_KEY AS NVARCHAR(11)
DECLARE @@OLD_FOLDER_KEY AS NVARCHAR(11)


SET @@RMX_DB = 'DallasISD_NS_RMProd' --Enter RMX Database
SET @@ACROSOFT_DB = 'DallasISD_Acrosoft' --Enter Acrosoft Database
SET @@DBO_USER = 'dbo' 
SET @DRYRUN = 0 -- set this to 1 if just a 'dry run' is done. In which case, it will just display the set of sql statements to be run 
				--without actually updating anything into the database.

set @@RECORDCOUNT =1
SET @PARMDEFINITION = N'@OUTPARAM varchar(30) OUTPUT';
SET @PARMDEFINITION_NEW_FOLDER_KEY = N'@OUTPARAM varchar(30) OUTPUT, @NEW_FOLDER_KEY_OUT varchar(30) OUTPUT';
SET @PARMDEFINITION_OLD_FOLDER_KEY = N'@OLDFOLDER_KEY_OUT varchar(30) OUTPUT';


BEGIN TRY

	PRINT '--Removing NULL Event numbers from the CLAIM table in Riskmaster database.'

	SET @@SQL = 
	'UPDATE [' + @@RMX_DB + '].' + @@DBO_USER+'.CLAIM '+
	'SET ['+ @@RMX_DB + '].' + @@DBO_USER+'.CLAIM.EVENT_NUMBER = ['+ @@RMX_DB + '].' + @@DBO_USER + '.EVENT.EVENT_NUMBER '+
	'FROM ['+ @@RMX_DB + '].' + @@DBO_USER+'.CLAIM INNER JOIN ['+ @@RMX_DB + '].' + @@DBO_USER+'.EVENT '+
	'ON ['+@@RMX_DB + '].' + @@DBO_USER+'.CLAIM.EVENT_ID=[' + @@RMX_DB + '].' + @@DBO_USER+'.EVENT.EVENT_ID '+
	'WHERE ['+ @@RMX_DB +'].' + @@DBO_USER+'.CLAIM.EVENT_NUMBER IS NULL '

	PRINT (@@SQL)
	EXEC (@@SQL ) -- cant include a dryrun here as this is absolutely required for the scripot to run 

	--  Creating a list of records to be updated...
	PRINT '--Creating a list of records to be updated...'

	SET @@SQL = 'SELECT [' + @@RMX_DB + '].' + @@DBO_USER + '.CLAIM.EVENT_NUMBER, [' + @@RMX_DB + '].' + @@DBO_USER + '.CLAIM.CLAIM_NUMBER INTO ##TMP_MAPPING_TABLE
		FROM [' + @@RMX_DB + '].' + @@DBO_USER + '.CLAIM, [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT 
		WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_CLMNBR = [' + @@RMX_DB + '].' + @@DBO_USER + 
		'.CLAIM.CLAIM_NUMBER and [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_EVNTNBR LIKE ''0'''

	PRINT (@@SQL)
	EXEC (@@SQL) -- no need to add dryrun as this is just a temp table being created.

	SET @@TOTALCOUNT = (SELECT COUNT(*) FROM ##TMP_MAPPING_TABLE)
	PRINT '--TOTAL RECORDS TO UPDATE: ' + CAST(@@TOTALCOUNT AS NVARCHAR)

	WHILE ((SELECT COUNT(*) FROM ##TMP_MAPPING_TABLE) > 0)
	BEGIN
		-- extracting event and claim number from temp table.
		SELECT TOP 1 @@CLAIM_NUMBER = CLAIM_NUMBER, @@EVENT_NUMBER = EVENT_NUMBER FROM ##TMP_MAPPING_TABLE
		PRINT '--Processing record ' + CAST (@@RECORDCOUNT AS NVARCHAR)+' of '+ CAST(@@TOTALCOUNT AS NVARCHAR)
		SET @@RECORDCOUNT =@@RECORDCOUNT +1
		PRINT('--Claim Number: ' + @@CLAIM_NUMBER + ' Event Number: ' + @@EVENT_NUMBER)
		
		--Skipping event number in case it is preceded by a space or if its size is greater than 12 
--		if (SUBSTRING (@@EVENT_NUMBER,1,1)=' ' and len (@@EVENT_NUMBER) > 12)
--			PRINT ('Skipping Event number (Event number length > 12) : ' + @@EVENT_NUMBER )
--		ELSE
--		BEGIN		

			-- checking if record to be updated already exists in the NDX_RMXEVNT table in Acorosoft db ( in case, the folder was got reindexed)
			SET @@SQL = 'SELECT @OUTPARAM = 1 FROM [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT ' +
				' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_CLMNBR = ''' + @@CLAIM_NUMBER + 
				''' AND [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_EVNTNBR  = ''' + @@EVENT_NUMBER + ''''

			PRINT @@SQL 
			EXECUTE sp_executesql @@SQL, @PARMDEFINITION, @OUTPARAM=@@TEMPVAL OUTPUT;

			IF  (@@TEMPVAL) IS NULL
				BEGIN
					PRINT '--updating record'
					SET @@SQL_NDX_RMXEVENT = 'UPDATE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT 
						SET [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_EVNTNBR = ''' + @@EVENT_NUMBER  +
						''' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_CLMNBR = ''' + @@CLAIM_NUMBER + 
						''' AND [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_EVNTNBR like ''0'''
					
					PRINT @@SQL_NDX_RMXEVENT
					
					IF NOT @DRYRUN = 1 EXEC (@@SQL_NDX_RMXEVENT)
				END
			ELSE 
				BEGIN
					PRINT '--Record was got Reindexed before. Deleting the entry with Zero Event number'
					
					SET @@SQL_NDX_RMXEVENT = 'DELETE FROM  [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT WHERE [' + 
						@@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_CLMNBR = ''' + @@CLAIM_NUMBER + 
						''' AND [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_RMXEVNT.NDX_EVNTNBR like ''0'''

					PRINT (@@SQL_NDX_RMXEVENT)
					IF NOT @DRYRUN = 1 EXEC (@@SQL_NDX_RMXEVENT)
				END

			
			-- checking if record to be updated already exists in the APP_FOLDER_TAB table in Acorosoft db (in case, the folder was got reindexed)
			SET @@AF_FOLDERKEY = 'RMXEVNT|' + @@EVENT_NUMBER + '|' +@@CLAIM_NUMBER
			SET @@OLD_AF_FOLDERKEY = 'RMXEVNT|0|' +@@CLAIM_NUMBER
			SET @@AF_TITLE = @@EVENT_NUMBER + ' - ' + @@CLAIM_NUMBER
			SET @@OLD_AF_TITLE = '0 - ' + @@CLAIM_NUMBER
			SET @@TEMPVAL = NULL
			SET @@NEW_FOLDER_KEY = NULL
			SET @@OLD_FOLDER_KEY = NULL
			SET @@SQL = NULL

			SET @@SQL = 'SELECT @OUTPARAM = 1,@NEW_FOLDER_KEY_OUT=AF_FOLDERID  FROM [' + @@ACROSOFT_DB + '].' + @@DBO_USER  + '.APP_FOLDER_TAB 
				WHERE ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_FOLDERKEY = ''' + @@AF_FOLDERKEY + ''''
				--''' AND ' + @@ACROSOFT_DB + '.' + @@DBO_USER + '.APP_FOLDER_TAB.AF_TITLE = ''' + @@AF_TITLE +''''

			PRINT @@SQL 
			EXECUTE sp_executesql @@SQL, @PARMDEFINITION_NEW_FOLDER_KEY, @OUTPARAM=@@TEMPVAL OUTPUT, @NEW_FOLDER_KEY_OUT=@@NEW_FOLDER_KEY OUTPUT;
			
			IF  (@@TEMPVAL) IS NULL
				BEGIN				
					SET @@SQL_APP_FOLDER_TAB = 'UPDATE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB 
						SET ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_FOLDERKEY = ''' + @@AF_FOLDERKEY + 
						''', [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_TITLE = ''' + @@AF_TITLE + 
						''' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_TITLE LIKE ''' + @@OLD_AF_TITLE + ''''

					PRINT (@@SQL_APP_FOLDER_TAB)				
					IF NOT @DRYRUN = 1 EXEC (@@SQL_APP_FOLDER_TAB)
				END
			ELSE
				BEGIN
					PRINT '--Record was got Reindexed before. Updating the folder ID'
					SET @@SQL = NULL
					SET @@OLD_FOLDER_KEY = NULL				
	        
					--START get the Folder ID
					PRINT '--(step 1/5)getting the Old folder ID'
					SET @@SQL = 'SELECT @OLDFOLDER_KEY_OUT=AF_FOLDERID  FROM [' + @@ACROSOFT_DB + '].' + @@DBO_USER  + '.APP_FOLDER_TAB 
						WHERE ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_FOLDERKEY = ''' + @@OLD_AF_FOLDERKEY + ''''
						--''' AND ' + @@ACROSOFT_DB + '.' + @@DBO_USER + '.APP_FOLDER_TAB.OLD_AF_TITLE = ''' + @@AF_TITLE +''''

					PRINT @@SQL 
					EXECUTE sp_executesql @@SQL, @PARMDEFINITION_OLD_FOLDER_KEY, @OLDFOLDER_KEY_OUT=@@OLD_FOLDER_KEY OUTPUT;
					--END get the Old Folder ID

					
					-- START update folderID in CABINET_FOLDER_REL
			PRINT '--(step 2/5)updating folderID in CABINET_FOLDER_REL'
					SET @@SQL = 'UPDATE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.CABINET_FOLDER_REL 
						SET ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.CABINET_FOLDER_REL.CF_CABINET_ID = ''' + @@NEW_FOLDER_KEY + 
						''', [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.CABINET_FOLDER_REL.CF_PARENT_ID = ''' + @@NEW_FOLDER_KEY + 
						''' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.CABINET_FOLDER_REL.CF_CABINET_ID LIKE ''' + @@OLD_FOLDER_KEY + ''''

					PRINT (@@SQL)
					IF NOT @DRYRUN = 1 EXEC (@@SQL)
					-- END update folderID in CABINET_FOLDER_REL


					-- START update folderID in FOLDER_OBJECT_REL
			PRINT '--(step 3/5)updating folderID in FOLDER_OBJECT_REL'
					SET @@SQL = 'UPDATE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.FOLDER_OBJECT_REL 
						SET ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.FOLDER_OBJECT_REL.CT_ID = ''' + @@NEW_FOLDER_KEY + 
						''' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.FOLDER_OBJECT_REL.CT_ID LIKE ''' + @@OLD_FOLDER_KEY + ''''

					PRINT (@@SQL)
					IF NOT @DRYRUN = 1 EXEC (@@SQL)
					-- END update folderID in FOLDER_OBJECT_REL


					-- START update folderID in NDX_OBJ_STOR_RELS
			PRINT '--(step 4/5)updating folderID in NDX_OBJ_STOR_RELS'
					SET @@SQL = 'UPDATE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_OBJ_STOR_RELS 
						SET ['  + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_OBJ_STOR_RELS.NDX_OBJ_FOLDER_ID = ''' + @@NEW_FOLDER_KEY + 
						''' WHERE [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.NDX_OBJ_STOR_RELS.NDX_OBJ_FOLDER_ID LIKE ''' + @@OLD_FOLDER_KEY + ''''

					PRINT (@@SQL)
					IF NOT @DRYRUN = 1 EXEC (@@SQL)
					-- END update folderID in NDX_OBJ_STOR_RELS				
						

					-- START delete entry from APP_FOLDER_TAB
			PRINT '--(step 5/5)delete old folderID from APP_FOLDER_TAB'
					SET @@SQL_APP_FOLDER_TAB = 'DELETE FROM  [' + @@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB WHERE ['  + 
						@@ACROSOFT_DB + '].' + @@DBO_USER + '.APP_FOLDER_TAB.AF_FOLDERKEY = ''' + @@OLD_AF_FOLDERKEY + ''''
	--					''' AND ' + @@ACROSOFT_DB + '.' + @@DBO_USER + '.APP_FOLDER_TAB.AF_TITLE = ''' + @@OLD_AF_TITLE +''''

					PRINT (@@SQL_APP_FOLDER_TAB)
					IF NOT @DRYRUN = 1 EXEC (@@SQL_APP_FOLDER_TAB)
					-- END delete entry from APP_FOLDER_TAB
				END
--		END 
		-- deleting processed row from temp table
		SET @@SQL = 'DELETE FROM ##TMP_MAPPING_TABLE WHERE CLAIM_NUMBER LIKE ''' + @@CLAIM_NUMBER + 
			''' AND EVENT_NUMBER LIKE ''' +  @@EVENT_NUMBER +''''

		PRINT (@@SQL)
		EXEC (@@SQL) -- no need for Dry run check as deleting from a temp table

	END
	DROP TABLE ##TMP_MAPPING_TABLE

END TRY
BEGIN CATCH
	PRINT '------------------------------'
	PRINT '--Error number: ' + CAST (ERROR_NUMBER() AS NVARCHAR)+ ' 	   Error severity: ' + CAST (ERROR_SEVERITY()AS NVARCHAR)
	PRINT '--Error state number: ' + CAST (ERROR_STATE() AS NVARCHAR) +'     Error line number: ' + CAST (ERROR_LINE() AS NVARCHAR)      
	PRINT '--Error Message: '+ ERROR_MESSAGE() 
	PRINT '------------------------------'
	
	GOTO QuitWithRollback
END CATCH

PRINT '--Acrosoft DB Update operation successfully completed'
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
PRINT '--Errors. Rollback started.'
ROLLBACK TRANSACTION
EndSave:
