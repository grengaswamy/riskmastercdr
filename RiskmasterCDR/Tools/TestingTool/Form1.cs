using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.Db;
using System.Data.SqlClient;
using System.Diagnostics;
using RMObjLib;
using RMLoginLib;
using ROCKETCOMLib;
using COMCACHELib;

namespace test_event
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.DataGrid dgcom;
		private System.Windows.Forms.DataGrid dgdotnet;
		private System.Windows.Forms.Button button5;
		private RMObjLib.CRMApplication objApp;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button button6;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.dgcom = new System.Windows.Forms.DataGrid();
			this.dgdotnet = new System.Windows.Forms.DataGrid();
			this.button5 = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.button6 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgcom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgdotnet)).BeginInit();
			this.SuspendLayout();
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(296, 32);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(208, 32);
			this.button2.TabIndex = 1;
			this.button2.Text = "com log";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(296, 0);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(208, 32);
			this.button3.TabIndex = 2;
			this.button3.Text = "dotnet log";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// dgcom
			// 
			this.dgcom.CaptionText = "Log of COM Object";
			this.dgcom.DataMember = "";
			this.dgcom.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgcom.Location = new System.Drawing.Point(16, 104);
			this.dgcom.Name = "dgcom";
			this.dgcom.ReadOnly = true;
			this.dgcom.SelectionBackColor = System.Drawing.Color.Red;
			this.dgcom.Size = new System.Drawing.Size(912, 300);
			this.dgcom.TabIndex = 3;
			this.dgcom.Click += new System.EventHandler(this.dgcom_gotfocus);
			this.dgcom.Navigate += new System.Windows.Forms.NavigateEventHandler(this.dgcom_Navigate);
			this.dgcom.LostFocus += new System.EventHandler(this.dgcom_lostfocus);
			// 
			// dgdotnet
			// 
			this.dgdotnet.CaptionText = "Log Of DotNet Object";
			this.dgdotnet.DataMember = "";
			this.dgdotnet.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgdotnet.Location = new System.Drawing.Point(16, 408);
			this.dgdotnet.Name = "dgdotnet";
			this.dgdotnet.ReadOnly = true;
			this.dgdotnet.Size = new System.Drawing.Size(912, 300);
			this.dgdotnet.TabIndex = 4;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(296, 64);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(208, 32);
			this.button5.TabIndex = 6;
			this.button5.Text = "Log Diff";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// comboBox1
			// 
			this.comboBox1.Location = new System.Drawing.Point(16, 8);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(200, 21);
			this.comboBox1.TabIndex = 7;
			this.comboBox1.Text = "comboBox1";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(32, 40);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(128, 24);
			this.button6.TabIndex = 8;
			this.button6.Text = "Delete";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(976, 502);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.dgdotnet);
			this.Controls.Add(this.dgcom);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Name = "Form1";
			this.Text = "Form1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgcom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgdotnet)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			initObj();
				RMObjLib.CEvent ce=new RMObjLib.CEventClass();
				
				//RMObjLib.CEntity et=new RMObjLib.CEntityClass();
				//RMObjLib.CSupplementals cSups=new CSupplementalsClass();
				//cSups.TableName="";
				//RMObjLib.CSupplementalFieldClass cfsupp=new CSupplementalFieldClass();
				//cfsupp.
				ce.EventNumber="AAA111" + DateTime.Now;
				ce.EventDescription="A bad event happened but it's going to be ok because we are tracking it in Wizard.";
				ce.EventIndCode=2518;
				ce.DeptEid=83;
				ce.DateOfEvent="20040202";
				ce.TimeOfEvent="121200";
				ce.EventStatusCode=13;
				ce.EventTypeCode=3566;
				
				ce.FallIndicator.BedPositionCode=12;
				ce.EventQm.CdComments="Some CD Comments";
				ce.EventXMedwatch.BrandName="Patel and Patel";
				ce.EventXOsha.HowAccOccurred="Well an accident occurred you see. and it went like this: [woops!]";
				ce.RptdByEid=2269;
				
				RMObjLib.CPIOther objpi=new RMObjLib.CPIOtherClass();
				objpi.Comments="That was an awfull event!";
				objpi.PiEntity.LastName="Person Involved Wizard com Test1";
				//used other_people code 1046 directly
				objpi.PiEntity.EntityTableId=1046;
				objpi.PiEntity.Supplementals["TEST_ENTITY_TEXT"].Value="testing from com as pientity";
				Object myobj=objpi;
				ce.PIList.Add(ref myobj);
				
				
				//trying to save hole event object including supp
				//RMObjLib.CSupplementals cs=new RMObjLib.CSupplementalsClass();
				//ce.ReporterEntity.Supplementals["TEST_ENTITY_TEXT"].Value="testing from com obj";
				//cs=ce.Supplementals;
				ce.Supplementals["test_field_MEMO"].Value="i dont know";
			
				//RMObjLib.CSupplementalFieldClass csf=new CSupplementalFieldClass();
				//csf=(RMObjLib.CSupplementalFieldClass ) cs["test_field_MEMO"];
				//csf.Value="i dont know";
				
				//-------------------------------------------------
				RMObjLib.IPersistence isave;
				isave=(RMObjLib.IPersistence)ce;
				isave.Save();
				
				
				
			/*	//-----------------coding for supplements---------------
				
				RMObjLib.CSupplementals cs=new RMObjLib.CSupplementalsClass();
				
				cs=ce.Supplementals;
				
				RMObjLib.CSupplementalFieldClass csf=new CSupplementalFieldClass();
				csf=(RMObjLib.CSupplementalFieldClass ) cs["test_field_MEMO"];
				csf.Value="i dont know";
				cs.Save();
				//-----------------------------finish--------------
				*/
				//cs=null;
				//csf=null;
				ce=null;
				objpi=null;
				isave=null;
			closeObj();
			MessageBox.Show("done");
		}
		private void initObj()
		{
			objApp=new RMObjLib.CRMApplicationClass();
			objApp.WebInit2("guituiwui","Driver={SQL Server};Server=RPATEL34-1;Database=wizardcom;","dtg");
			//objApp.WebInit2("guituiwui","DSN=test_ora;UID=test_demo_ora;PWD=test_demo_ora","dtg");
		}
		private void closeObj()
		{
			objApp.WebClose("guituiwui");
			objApp=null;
			GC.Collect();
		}
		private void button2_Click(object sender, System.EventArgs e)
		{
			string sql;
			sql="(SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,OLD_VALUE,NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_DATA,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_DATA.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID) UNION (SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,'N/A',NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_BLOBS,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_BLOBS.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID)";
			DataSet ds=DbFactory.GetDataSet("dsn=wizardcom;UID=sa;Pwd=riskmaster;",sql);
			dgcom.DataSource=ds;
			dgcom.DataMember="TABLE";
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			comboBox1.Items.Add("--");
			comboBox1.Items.Add("COM LOG");
			comboBox1.Items.Add("DOTNET LOG");
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			string sql;
			sql="(SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,OLD_VALUE,NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_DATA,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_DATA.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID) UNION (SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,'N/A',NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_BLOBS,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_BLOBS.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID)";
			DataSet ds=DbFactory.GetDataSet("dsn=wizarddotnet;UID=sa;Pwd=riskmaster;",sql);
			dgdotnet.DataSource=ds;
			dgdotnet.DataMember="TABLE";
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			
			Trace.WriteLine("Adding Event");
			DataModelFactory dmf = new Riskmaster.DataModel.DataModelFactory("wizarddotnet","dtg","dtg");
			
			Event  myEvent = (Event)dmf.GetDataModelObject("Event",false);
			//Phase 1 Event Table Only Test
			myEvent.EventNumber = "AAA111" + DateTime.Now;
			//MessageBox.Show(myEvent.GetProperty("EventNumber").ToString());

			myEvent.EventDescription = "A bad event happened but it's going to be ok because we are tracking it in Wizard.";
			myEvent.EventIndCode = 2518; 
			myEvent.DeptEid = 83;
			myEvent.DateOfEvent = "20040202";
			myEvent.TimeOfEvent = "121200";
			myEvent.EventTypeCode = 3566;
			myEvent.EventStatusCode = 13;
			//Phase 2 Event & Singleton tables.
			myEvent.EventFallIndicator.BedPositionCode = 12;
			myEvent.EventQM.CdComments = "Some CD Comments";
			myEvent.EventMedwatch.BrandName = "Johnson and Johnson";
			myEvent.EventOsha.HowAccOccurred = "Well an accident occurred you see. and it went like this: [woops!]";
			//Phase 3 Event and New Reported by Entity Test
			// Completed //myEvent.ReporterEntity.LastName = "My Brand new entity for event AAA111x";
			//Phase 4 Event and Existing Reported by Entity.
			myEvent.RptdByEid = 2269;
			//Phase 5 Add Person Involved.
			PiOther objPI = myEvent.PiList.AddNewPiByType(PiType.PiOther) as PiOther;
			objPI.Comments = "That was an awfull event!";
			objPI.PiEntity.LastName = "Person Involved Wizard dotnet Test1";
			objPI.PiEntity.Supplementals["TEST_ENTITY_TEXT"].Value="testing from dotnet as pientity";
			//myEvent.ReporterEntity.Supplementals["TEST_ENTITY_TEXT"].Value="testing from dotnet object";
			myEvent.Supplementals["TEST_FIELD_MEMO"].Value="i dont know either";
			
			myEvent.Save();
			
			/* --------------codes for supp saving sapratly----
			Riskmaster.DataModel.Supplementals sup=myEvent.Supplementals;
			Riskmaster.DataModel.SupplementalField supf=sup["TEST_FIELD_MEMO"];
			supf.Value="i dont know either";
			sup.Save();
			--------------------------------------------------------------*/
			
			dmf.UnInitialize();
			MessageBox.Show("done");
		}
		
		private void dgcom_Navigate(object sender, System.Windows.Forms.NavigateEventArgs ne)
		{
		
		}
		
		private void button5_Click(object sender, System.EventArgs e)
		{
			
			string sql="(SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,OLD_VALUE,NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_DATA,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_DATA.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID)";
			sql=sql+" UNION ";
			sql=sql+"(SELECT DISTINCT NAME,PRIMARY_KEY,COL_NAME,'N/A',NEW_VALUE FROM SYSOBJECTS,AUDIT_LOG_BLOBS,AUDIT_LOG_TRANSACTIONS WHERE SYSOBJECTS.ID=AUDIT_LOG_TRANSACTIONS.SYSOBJ_ID AND AUDIT_LOG_BLOBS.AUDIT_LOG_TRANSACTION_ID=AUDIT_LOG_TRANSACTIONS.AUDIT_LOG_TRANSACTION_ID)";
			Debug.Write(sql);
			DataSet ds=DbFactory.GetDataSet("dsn=wizardcom;UID=sa;Pwd=riskmaster;",sql);

			dgcom.DataSource=ds;
			dgcom.DataMember="TABLE";
			
			DataSet dsnet=DbFactory.GetDataSet("dsn=wizarddotnet;UID=sa;Pwd=riskmaster;",sql);
			//da.SelectCommand=cmdnet;
			//da.Fill(dsnet);
			dgdotnet.DataSource=dsnet;
			dgdotnet.DataMember="TABLE";
			
			setgridstyle(dgcom,Color.Brown,"com");
			setgridstyle(dgdotnet,Color.Red,"net");
			int rcount=ds.Tables["TABLE"].Rows.Count-1;
			string oldVal="";
			string newVal="";
			for(int i=0;i<=rcount;i++)
			{

				DataRow	dr=ds.Tables["TABLE"].Rows[i];
				
				if(dr["OLD_VALUE"].ToString()=="")
					oldVal="";
				else
					oldVal=dr["OLD_VALUE"].ToString();
				if(dr["NEW_VALUE"].ToString()=="")
					newVal="";
				else
				newVal=dr["NEW_VALUE"].ToString();
				newVal= newVal.Replace("'","");
				//MessageBox.Show(newVal);
				//DataRow[] Drows=ds.Tables["net"].Select("NAME='" + dr["NAME"] + "' AND COL_NAME='" + dr["COL_NAME"] + "' AND OLD_VALUE='" + oldVal + "' AND NEW_VALUE='" + newVal + "'");
				DataRow[] Drows=dsnet.Tables["TABLE"].Select("NAME='" + dr["NAME"] + "' AND COL_NAME='" + dr["COL_NAME"] + "' AND NEW_VALUE='" + newVal + "'");
				if(Drows.Length<1)
				dgcom.Select(i);
		
			}
			
			rcount=dsnet.Tables["TABLE"].Rows.Count-1;
			//DataRow dr=new DataRow();
			for(int i=0;i<=rcount;i++)
			{
				DataRow dr=dsnet.Tables["TABLE"].Rows[i];
				if(dr["OLD_VALUE"].ToString()=="")
					oldVal="";
				else
					oldVal=dr["OLD_VALUE"].ToString();
				if(dr["NEW_VALUE"].ToString()=="")
					newVal="";
				else
				newVal=dr["NEW_VALUE"].ToString();
				newVal= newVal.Replace("'","");
				//DataRow[] Drows=ds.Tables["com"].Select("NAME='" + dr["NAME"] + "' AND COL_NAME='" + dr["COL_NAME"] + "' AND OLD_VALUE='" + oldVal + "' AND NEW_VALUE='" + newVal + "'");
				DataRow[] Drows=ds.Tables["TABLE"].Select("NAME='" + dr["NAME"] + "' AND COL_NAME='" + dr["COL_NAME"] + "' AND NEW_VALUE='" + newVal + "'");
				if(Drows.Length<1)
					dgdotnet.Select(i);
			}
			//dgcom.DataSource=ds;
			//dgcom.DataMember="TABLE";
			
			//DataSet dsnet=DbFactory.GetDataSet("dsn=wizarddotnet;UID=sa;Pwd=riskmaster;",sql);
			//da.SelectCommand=cmdnet;
			//da.Fill(dsnet);
			//dgdotnet.DataSource=dsnet;
			//dgdotnet.DataMember="TABLE";
			
		}
		private void dgcom_gotfocus(object sender,System.EventArgs e)
		{
			//MessageBox.Show("hi");
			//Boolean b;
			//dgcom.Enabled=false;
		}
		private void dgcom_lostfocus(object sender,System.EventArgs e)
		{
			//MessageBox.Show("lost");
			//dgcom.Enabled=true;
		}
		private void setgridstyle(DataGrid dg,Color mycolor,string mapname)
		{
			try
			{
				DataGridTableStyle mygridstyle=new DataGridTableStyle();
				mygridstyle.SelectionBackColor=mycolor;
				mygridstyle.SelectionForeColor=Color.White;
				mygridstyle.MappingName=mapname;
				
				dg.TableStyles.Add(mygridstyle);
			}
			catch(Exception e)
			{
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			//SqlConnection scdel=new SqlConnection();
			string scon="";
			string sql="DELETE FROM AUDIT_LOG_TRANSACTIONS";
			if(comboBox1.SelectedIndex==1)
			{
				//scdel.ConnectionString="server=RPATEL34-1;database=wizardcom;User ID=sa;Pwd=riskmaster;";
				//Riskmaster.Db.DbConnection scdel=Riskmaster.Db.DbFactory.GetDbConnection("dsn=wizardcom;UID=sa;Pwd=riskmaster;");
				//Riskmaster.Db.DbConnection scdel1=Riskmaster.Db.DbFactory.GetDbConnection("");
				scon="dsn=wizardcom;UID=sa;Pwd=riskmaster;";
				
			}
			else
			{
				//scdel.ConnectionString="server=RPATEL34-1;database=wizarddotnet;User ID=sa;Pwd=riskmaster;";
				//DbConnection scdel=DbFactory.GetDbConnection("dsn=wizarddotnet;UID=sa;Pwd=riskmaster;");
				scon="dsn=wizarddotnet;UID=sa;Pwd=riskmaster;";
			}
			Riskmaster.Db.DbConnection scdel=Riskmaster.Db.DbFactory.GetDbConnection(scon);
			if(comboBox1.SelectedIndex!=0)
			{
				try
				{
					//SqlCommand cmdel=scdel.CreateCommand();
					//SqlCommand cmdel1=scdel.CreateCommand();
					scdel.Open();
					DbCommand cmdel=scdel.CreateCommand();
					DbCommand cmdel1=scdel.CreateCommand();
					cmdel.CommandType=CommandType.Text;
					cmdel.CommandText=sql;
					cmdel1.CommandType=CommandType.Text;
					cmdel1.CommandText="DELETE FROM AUDIT_LOG_DATA";
					
					int recs=cmdel.ExecuteNonQuery();
					recs=cmdel1.ExecuteNonQuery();
					scdel.Close();
					MessageBox.Show("deleted");
				}
				catch(SqlException se)
				{
					MessageBox.Show(se.Message);
				}
			}

		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			initObj();
			RMObjLib.CEvent ce=new RMObjLib.CEventClass() ;
			RMObjLib.INavigation inav;
			inav=(RMObjLib.INavigation)ce;
			object[] obj={10925};
			//obj[0]=1024;
			inav.MoveTo(ref obj);
			MessageBox.Show(ce.PIList.Count.ToString());
			RMObjLib.CPIOther objpi;
			objpi=(RMObjLib.CPIOtherClass)ce.PIList.get_ItemByEntityId(10208);
			 MessageBox.Show(Convert.ToString(objpi.PiEntity.Supplementals["TEST_ENTITY_TEXT"].Value));
			RMObjLib.IPersistence iper;
			iper=(RMObjLib.IPersistence)objpi.PiEntity.Supplementals;
			iper.Delete();
			//MessageBox.Show(ce.PIList.get_GetPiEntity(
			closeObj();
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			DataModelFactory dmf = new Riskmaster.DataModel.DataModelFactory("wizarddotnet","dtg","dtg");
			Event  myEvent = (Event)dmf.GetDataModelObject("Event",false);
			//myEvent.IsChildrenAllowed=false;
			myEvent.MoveTo(10939);
			
			PiOther objPI =(myEvent.PiList.GetPiByEID(10225)) as PiOther;
			//PersonInvolved objPI=myEvent.PiList.GetPiByEID(10225);
			MessageBox.Show(Convert.ToString(objPI.PiEntity.Supplementals["TEST_ENTITY_TEXT"].Value));
			objPI.PiEntity.Supplementals.Delete();

			//objPI.Delete();
		}

		private void btngetfunds_Click(object sender, System.EventArgs e)
		{
			DataModelFactory dmf=new Riskmaster.DataModel.DataModelFactory("wizarddotnet","dtg","dtg");
			Funds myfunds=(Funds)dmf.GetDataModelObject("Funds",false);
			myfunds.ClaimId=10547;
			myfunds.AccountId=1001;
			myfunds.Addr1="my addr one";
			myfunds.Addr2="my addr two";
			myfunds.Amount=100;
			myfunds.CheckMemo="chk001";
			myfunds.City="troy";
			myfunds.Comments="nocomments";
			myfunds.DateOfCheck="07/15/04";
			myfunds.TransDate="07/15/04";
			myfunds.PayeeEid=10208;
			myfunds.AccountId=1;
			myfunds.ClaimantEid=8940;
			myfunds.PayeeEid=8684;
			myfunds.PayeeTypeCode=469;
			myfunds.PaymentFlag=true;
			myfunds.CollectionFlag=false;
			
			myfunds.PayeeEntity.Supplementals["TEST_ENTITY_TEXT"].Value="test from funds";
			
			FundsTransSplit objtrans=(FundsTransSplit)dmf.GetDataModelObject("FundsTransSplit",false);
			
			objtrans.Amount=100;
			objtrans.FromDate="05/22/04";
			objtrans.ToDate="08/22/04";
			objtrans.UpdatedByUser="ravi";
			objtrans.ReserveTypeCode=4700;
			objtrans.TransTypeCode=4716;

			myfunds.TransSplitList.Add(objtrans);
			objtrans=null;
			//FundsTransSplit objtrans1=(FundsTransSplit)dmf.GetDataModelObject("FundsTransSplit",false);
			//objtrans1.Amount=3000;
			//objtrans1.FromDate="04/22/04";
			//objtrans1.ToDate="07/22/04";
			//objtrans1.ReserveTypeCode=4700;
			//objtrans1.TransTypeCode=4716;
			//objtrans1.UpdatedByUser="ravi";

			//myfunds.TransSplitList.Add(objtrans1);

			myfunds.Save();
			MessageBox.Show("done");
		}

		private void button9_Click(object sender, System.EventArgs e)
		{	initObj();
				RMObjLib.CFunds myfunds=new CFundsClass();
				myfunds.ClaimId=10547;
				myfunds.AccountId=1001;
			
				myfunds.Addr1="my addr one";
				myfunds.Addr2="my addr two";
				myfunds.Amount=100;
				myfunds.CheckMemo="chk001";
				myfunds.City="troy";
				myfunds.Comments="nocomments";
				myfunds.DateOfCheck="07/15/04";
				myfunds.TransDate="07/15/04";
				//myfunds.PayeeEid=10208;
				myfunds.AccountId=1;
				myfunds.ClaimantEid=8940;
				myfunds.PayeeEid=8701;
				myfunds.PayeeTypeCode=469;
				myfunds.PaymentFlag=true;
				myfunds.CollectionFlag=false;
				
				myfunds.PayeeEntity.Supplementals["TEST_ENTITY_TEXT"].Value="test from funds";
				
			//for(i=0;i<=1;i++)
			//{
				RMObjLib.CFundsTransSplit objtrans=new CFundsTransSplitClass();
				objtrans.Amount=100;
				objtrans.SumAmount=100;
				objtrans.FromDate="05/22/04";
				objtrans.ToDate="08/22/04";
				objtrans.ReserveTypeCode=4700;
				objtrans.TransTypeCode=4716;
				//objtrans.UpdatedByUser="ravi";
				myfunds.TransSplitsList.Add(ref objtrans);
				
				//objtrans=new CFundsTransSplitClass();
				//objtrans.Amount=3000;
				//objtrans.FromDate="04/22/04";
				//objtrans.ToDate="07/22/04";
				//objtrans.ReserveTypeCode=4700;
				//objtrans.TransTypeCode=4716;
			
				//objtrans.UpdatedByUser="ravi";
				
				//myfunds.TransSplitsList.Add(ref objtrans);
				
				RMObjLib.IPersistence ipers;
				ipers=(RMObjLib.IPersistence)myfunds;
				ipers.Save();
				
				ipers=null;
				objtrans=null;
			closeObj();
			MessageBox.Show("done");
		}

		private void claimdotnet_Click(object sender, System.EventArgs e)
		{
			DataModelFactory dmf=new Riskmaster.DataModel.DataModelFactory("wizarddotnet","dtg","dtg");
			Claim myclaim=(Claim)dmf.GetDataModelObject("Claim",false);
			myclaim.MoveTo(10547);
					

			//myclaim.FundsList.Add(myfunds);

			myclaim.Save();
			MessageBox.Show("done");
			//myclaim=null;
			//myfunds=null;
			//myclaim.
		}

		private void claimcom_Click(object sender, System.EventArgs e)
		{
		initObj();
			RMObjLib.CClaim myclaim=new CClaimClass();
			RMObjLib.INavigation inav;
			inav=(RMObjLib.INavigation)myclaim;
			object[] obj={10547};
			inav.MoveTo(ref obj);
			
			RMObjLib.IPersistence iper;
			iper=(RMObjLib.IPersistence)myclaim;
			iper.Save();
			MessageBox.Show("done");
			iper=null;
			inav=null;
			myclaim=null;
			//objtrans=null;
		closeObj();
		}
	}
}
