if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AUDIT_LOG_BLOBS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AUDIT_LOG_BLOBS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AUDIT_LOG_DATA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AUDIT_LOG_DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AUDIT_LOG_TRANSACTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AUDIT_LOG_TRANSACTIONS]
GO

CREATE TABLE [dbo].[AUDIT_LOG_BLOBS] (
	[AUDIT_LOG_BLOBS_ID] [int] IDENTITY (1, 1) NOT NULL ,
	[AUDIT_LOG_TRANSACTION_ID] [int] NOT NULL ,
	[PRIMARY_KEY] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COL_NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NEW_TEXT_VALUE] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NEW_NTEXT_VALUE] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NEW_IMAGE_VALUE] [image] NULL ,
	[NEW_VALUE] AS (coalesce((convert(varchar(3600),[NEW_TEXT_VALUE])),(convert(varchar(3600),[NEW_NTEXT_VALUE])),(convert(varchar(3600),convert(varbinary(3600),[NEW_IMAGE_VALUE]))))) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[AUDIT_LOG_DATA] (
	[AUDIT_LOG_DATA_ID] [int] IDENTITY (1, 1) NOT NULL ,
	[AUDIT_LOG_TRANSACTION_ID] [int] NOT NULL ,
	[PRIMARY_KEY] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COL_NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OLD_VALUE] [varchar] (3600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NEW_VALUE] [varchar] (3600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AUDIT_LOG_TRANSACTIONS] (
	[AUDIT_LOG_TRANSACTION_ID] [int] IDENTITY (1, 1) NOT NULL ,
	[SYSOBJ_ID] [int] NOT NULL ,
	[AUDIT_ACTION_ID] [tinyint] NOT NULL ,
	[HOST_NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[APP_NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MODIFIED_BY] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[MODIFIED_DATE] [datetime] NOT NULL ,
	[AFFECTED_ROWS] [int] NOT NULL 
) ON [PRIMARY]
GO

