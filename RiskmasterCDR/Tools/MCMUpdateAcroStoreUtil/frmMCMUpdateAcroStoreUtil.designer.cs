namespace Riskmaster.Tools.MCMUpdateAcroStoreUtil
{
    partial class frmMCMUpdateAcroStoreUtil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.grdNoteType = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDeleteC2NenchNotes = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNoClaimNotes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNoClaimsComments = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNoEventNotes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNoEventComments = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.rtfStatus = new System.Windows.Forms.RichTextBox();
            this.prgBrUpdateStore = new System.Windows.Forms.ProgressBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtProgressStatusMessage = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtProcessingNow = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtProcessedFilesCount = new System.Windows.Forms.TextBox();
            this.txtTotalFilesCount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNoteType)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(833, 336);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Note Type";
            // 
            // grdNoteType
            // 
            this.grdNoteType.AllowUserToAddRows = false;
            this.grdNoteType.AllowUserToDeleteRows = false;
            this.grdNoteType.AllowUserToOrderColumns = true;
            this.grdNoteType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdNoteType.Location = new System.Drawing.Point(209, 224);
            this.grdNoteType.Name = "grdNoteType";
            this.grdNoteType.Size = new System.Drawing.Size(260, 82);
            this.grdNoteType.TabIndex = 6;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(14, 224);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(165, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Load \"Note types\"";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(207, 236);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(592, 285);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(584, 259);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Comments";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(584, 259);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Enh Notes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(10, 6);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(826, 339);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick_1);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.btnDeleteC2NenchNotes);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.grdNoteType);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(584, 259);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "misc";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(307, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "extract htmlcomm";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(226, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "grid bind";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDeleteC2NenchNotes
            // 
            this.btnDeleteC2NenchNotes.Location = new System.Drawing.Point(46, 6);
            this.btnDeleteC2NenchNotes.Name = "btnDeleteC2NenchNotes";
            this.btnDeleteC2NenchNotes.Size = new System.Drawing.Size(111, 23);
            this.btnDeleteC2NenchNotes.TabIndex = 9;
            this.btnDeleteC2NenchNotes.Text = "Delete C2N Ench Notes";
            this.btnDeleteC2NenchNotes.UseVisualStyleBackColor = true;
            this.btnDeleteC2NenchNotes.Click += new System.EventHandler(this.btnDeleteC2NenchNotes_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNoClaimNotes);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtNoClaimsComments);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtNoEventNotes);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtNoEventComments);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(14, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(504, 183);
            this.panel1.TabIndex = 10;
            // 
            // txtNoClaimNotes
            // 
            this.txtNoClaimNotes.Location = new System.Drawing.Point(392, 83);
            this.txtNoClaimNotes.Name = "txtNoClaimNotes";
            this.txtNoClaimNotes.ReadOnly = true;
            this.txtNoClaimNotes.Size = new System.Drawing.Size(100, 20);
            this.txtNoClaimNotes.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(275, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "No of notes extracted";
            // 
            // txtNoClaimsComments
            // 
            this.txtNoClaimsComments.Location = new System.Drawing.Point(153, 83);
            this.txtNoClaimsComments.Name = "txtNoClaimsComments";
            this.txtNoClaimsComments.ReadOnly = true;
            this.txtNoClaimsComments.Size = new System.Drawing.Size(100, 20);
            this.txtNoClaimsComments.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Claims";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "No of attached comments";
            // 
            // txtNoEventNotes
            // 
            this.txtNoEventNotes.Location = new System.Drawing.Point(390, 31);
            this.txtNoEventNotes.Name = "txtNoEventNotes";
            this.txtNoEventNotes.ReadOnly = true;
            this.txtNoEventNotes.Size = new System.Drawing.Size(100, 20);
            this.txtNoEventNotes.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(275, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "No of notes extracted";
            // 
            // txtNoEventComments
            // 
            this.txtNoEventComments.Location = new System.Drawing.Point(151, 32);
            this.txtNoEventComments.Name = "txtNoEventComments";
            this.txtNoEventComments.ReadOnly = true;
            this.txtNoEventComments.Size = new System.Drawing.Size(100, 20);
            this.txtNoEventComments.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Events";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "No of attached comments";
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(584, 259);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // rtfStatus
            // 
            this.rtfStatus.Location = new System.Drawing.Point(55, 397);
            this.rtfStatus.Name = "rtfStatus";
            this.rtfStatus.ReadOnly = true;
            this.rtfStatus.Size = new System.Drawing.Size(370, 35);
            this.rtfStatus.TabIndex = 10;
            this.rtfStatus.Text = "";
            // 
            // prgBrUpdateStore
            // 
            this.prgBrUpdateStore.Location = new System.Drawing.Point(16, 114);
            this.prgBrUpdateStore.Name = "prgBrUpdateStore";
            this.prgBrUpdateStore.Size = new System.Drawing.Size(345, 23);
            this.prgBrUpdateStore.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.prgBrUpdateStore);
            this.panel2.Controls.Add(this.btnStart);
            this.panel2.Controls.Add(this.txtProgressStatusMessage);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtProcessingNow);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtProcessedFilesCount);
            this.panel2.Controls.Add(this.txtTotalFilesCount);
            this.panel2.Location = new System.Drawing.Point(2, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(370, 179);
            this.panel2.TabIndex = 13;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(286, 143);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 18;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtProgressStatusMessage
            // 
            this.txtProgressStatusMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtProgressStatusMessage.Location = new System.Drawing.Point(17, 94);
            this.txtProgressStatusMessage.Name = "txtProgressStatusMessage";
            this.txtProgressStatusMessage.ReadOnly = true;
            this.txtProgressStatusMessage.Size = new System.Drawing.Size(340, 13);
            this.txtProgressStatusMessage.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Processing file:";
            // 
            // txtProcessingNow
            // 
            this.txtProcessingNow.Location = new System.Drawing.Point(191, 7);
            this.txtProcessingNow.Name = "txtProcessingNow";
            this.txtProcessingNow.ReadOnly = true;
            this.txtProcessingNow.Size = new System.Drawing.Size(170, 20);
            this.txtProcessingNow.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "No of files processed:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Total file count :";
            // 
            // txtProcessedFilesCount
            // 
            this.txtProcessedFilesCount.Location = new System.Drawing.Point(191, 63);
            this.txtProcessedFilesCount.Name = "txtProcessedFilesCount";
            this.txtProcessedFilesCount.ReadOnly = true;
            this.txtProcessedFilesCount.Size = new System.Drawing.Size(170, 20);
            this.txtProcessedFilesCount.TabIndex = 12;
            this.txtProcessedFilesCount.Text = "0";
            // 
            // txtTotalFilesCount
            // 
            this.txtTotalFilesCount.Location = new System.Drawing.Point(191, 35);
            this.txtTotalFilesCount.Name = "txtTotalFilesCount";
            this.txtTotalFilesCount.ReadOnly = true;
            this.txtTotalFilesCount.Size = new System.Drawing.Size(170, 20);
            this.txtTotalFilesCount.TabIndex = 10;
            this.txtTotalFilesCount.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(414, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 5;
            // 
            // frmMCMUpdateAcroStoreUtil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 195);
            this.Controls.Add(this.rtfStatus);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "frmMCMUpdateAcroStoreUtil";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "MCM Update Acrostore Util";
            this.Load += new System.EventHandler(this.frmMCMUpdateAcroStoreUtil_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMCMUpdateAcroStoreUtil_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNoteType)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView grdNoteType;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNoClaimNotes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNoClaimsComments;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNoEventNotes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNoEventComments;
        private System.Windows.Forms.ProgressBar prgBrUpdateStore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnDeleteC2NenchNotes;
        private System.Windows.Forms.RichTextBox rtfStatus;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtProcessedFilesCount;
        private System.Windows.Forms.TextBox txtTotalFilesCount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtProcessingNow;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtProgressStatusMessage;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

