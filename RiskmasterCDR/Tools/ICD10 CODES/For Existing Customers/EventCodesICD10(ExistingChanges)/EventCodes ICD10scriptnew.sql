declare @var int
select @var = TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME='CDC_ICD10_Event_Code'

update CODES_TEXT set CODE_DESC ='jump fall or push from high place' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y30' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='falling lying or running before moving object' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y31' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='digging shoveling and raking' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.H1' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='caregiving lifting' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.F2' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='caregiving bathing' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.F1' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='push-ups pull-ups sit-ups' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.B2' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='other involving climbing rappelling and jumping off' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.39' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='mountain climbing rock climbing and wall climbing' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.31' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='snow alpine downhill skiing snow boarding' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.23' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='surfing windsurfing and boogie boarding' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.18' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='rowing canoeing kayaking rafting and tubing' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.16' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='walking marching and hiking' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.01' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='handgun like pistol revolver' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y22' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='natural water like lake ocean pond river stream' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y21.4' and CODES.TABLE_ID= @var
update CODES_TEXT set CODE_DESC ='volleyball in beach court' from CODES_TEXT,CODES where CODES.CODE_ID = CODES_TEXT.CODE_ID and CODES_TEXT.SHORT_CODE='Y93.68' and CODES.TABLE_ID= @var
