;***********************************************************************************
; DB Upgrade Script for MCMBulkCopy tool
;***********************************************************************************
; adding column
[Oracle]     ALTER TABLE DOCUMENT ADD MEDIAVIEW_TRANSFER_STATUS NUMBER(10) NULL
[SQL Server] ALTER TABLE DOCUMENT ADD MEDIAVIEW_TRANSFER_STATUS INT NULL
[Oracle]     ALTER TABLE MERGE_FORM ADD MEDIAVIEW_TRANSFER_STATUS NUMBER(10) NULL
[SQL Server] ALTER TABLE MERGE_FORM ADD MEDIAVIEW_TRANSFER_STATUS INT NULL