   This scripts add the columns required the MediaviewBulkCopy tool into the RMX database.
   steps to run this script (MediaviewTableUpdateScript.sql) on the RMX database.
   1) copy this script to the RMX server to the WcfService\bin\scripts folder.
   2) open the file Riskmaster.Tools.RMXDbUpgrade.exe.config in a text editor.
   3) change the value for the <DBUPGRADE_CUSTOM> entry to "true". it should now read :-
		<add key="DBUPGRADE_CUSTOM" value="true" /> 
   4) add the filename to the <CUSTOMSCRIPTS> entry. it should now read :-
		<add key="CUSTOMSCRIPTS" value="McmTableUpdateScript.sql" />   
   5) run the Riskmaster.Tools.RMXDbUpgrade.exe file by double clicking it.
   6) after the upgrade is successfully, please revert the value for the <DBUPGRADE_CUSTOM> entry to "false"
   