using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Security;


namespace Riskmaster.Tools.MediaViewBulkCopy
{
    public class AppGlobals
    {
        public static UserLogin Userlogin;
    public static string ConnectionString;
        public static string sUser = ""; //user login
        public static string sPwd = ""; //password
        public static string sDSN = ""; //database name
        public static bool bSilentMode = false;
        
        public static long lTotalFileSize = 1; // to prevent Div by zero in worst case scenario
        public static long lProcessedFileSize = 0;

        public static int iProcessedFileCount = 0;

        public static string sStatusMessage = "";
        public static bool  bUseDummyFiles= false;
        
        //public static int iFileSizeTransmitted = 0;

        public static StringDictionary sdUserDocPaths = new  StringDictionary();
        
        //window resizing details
        //public static bool bIsDetailsView = true;
        //public static int iWinHeight = 0;

        // report 
        //public static XmlDocument objXmlReport = new XmlDocument();

        // for storing doc details
        public static DataSet m_dsDocs = new DataSet();
        public static DataSet m_dsMergeDocs = new DataSet();
      
        //screen populate details
        //txtDocPathType
        //txtDocPath
        //txtDocsCount
        //txtRemainingFilesCount
        //txtTotalSize
        //txtBytesTransmitted
        //txtBytesRemaining

        //progressBar1

        //txtIndiDocName
        //txtIndiDocFileName
        //txtIndiDocFileSize
        //txtIndiAttachmentType
        //txtIndiUserId
        //txtIndiUserDocUploadLoc

                
    }
}
