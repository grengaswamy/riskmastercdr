﻿namespace WPAProcessing
{
    partial class WPAForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WPAForm));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnSuspend = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.WpaprogressBar = new System.Windows.Forms.ProgressBar();
            this.lblDef = new System.Windows.Forms.Label();
            this.lblDiaries = new System.Windows.Forms.Label();
            this.lblTotDiaries = new System.Windows.Forms.TextBox();
            this.listBoxWPA = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(26, 293);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Tag = "0";
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnSuspend
            // 
            this.btnSuspend.Location = new System.Drawing.Point(199, 293);
            this.btnSuspend.Name = "btnSuspend";
            this.btnSuspend.Size = new System.Drawing.Size(75, 23);
            this.btnSuspend.TabIndex = 1;
            this.btnSuspend.Tag = "0";
            this.btnSuspend.Text = "Suspend";
            this.btnSuspend.UseVisualStyleBackColor = true;
            this.btnSuspend.Click += new System.EventHandler(this.btnSuspend_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(380, 293);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // WpaprogressBar
            // 
            this.WpaprogressBar.Location = new System.Drawing.Point(150, 218);
            this.WpaprogressBar.Name = "WpaprogressBar";
            this.WpaprogressBar.Size = new System.Drawing.Size(305, 23);
            this.WpaprogressBar.TabIndex = 3;
            // 
            // lblDef
            // 
            this.lblDef.AutoSize = true;
            this.lblDef.Location = new System.Drawing.Point(26, 218);
            this.lblDef.Name = "lblDef";
            this.lblDef.Size = new System.Drawing.Size(109, 13);
            this.lblDef.TabIndex = 4;
            this.lblDef.Text = "Definitions Processed";
            // 
            // lblDiaries
            // 
            this.lblDiaries.AutoSize = true;
            this.lblDiaries.Location = new System.Drawing.Point(26, 255);
            this.lblDiaries.Name = "lblDiaries";
            this.lblDiaries.Size = new System.Drawing.Size(106, 13);
            this.lblDiaries.TabIndex = 5;
            this.lblDiaries.Text = "Total Created Diaries";
            // 
            // lblTotDiaries
            // 
            this.lblTotDiaries.Location = new System.Drawing.Point(167, 248);
            this.lblTotDiaries.Name = "lblTotDiaries";
            this.lblTotDiaries.ReadOnly = true;
            this.lblTotDiaries.Size = new System.Drawing.Size(155, 20);
            this.lblTotDiaries.TabIndex = 6;
            this.lblTotDiaries.Text = "0";
            // 
            // listBoxWPA
            // 
            this.listBoxWPA.FormattingEnabled = true;
            this.listBoxWPA.Location = new System.Drawing.Point(26, 32);
            this.listBoxWPA.Name = "listBoxWPA";
            this.listBoxWPA.Size = new System.Drawing.Size(438, 160);
            this.listBoxWPA.TabIndex = 7;
            // 
            // WPAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 341);
            this.Controls.Add(this.listBoxWPA);
            this.Controls.Add(this.lblTotDiaries);
            this.Controls.Add(this.lblDiaries);
            this.Controls.Add(this.lblDef);
            this.Controls.Add(this.WpaprogressBar);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSuspend);
            this.Controls.Add(this.btnStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "WPAForm";
            this.Text = "WPA Processing";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnSuspend;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ProgressBar WpaprogressBar;
        private System.Windows.Forms.Label lblDef;
        private System.Windows.Forms.Label lblDiaries;
        private System.Windows.Forms.TextBox lblTotDiaries;
        private System.Windows.Forms.ListBox listBoxWPA;
    }
}

