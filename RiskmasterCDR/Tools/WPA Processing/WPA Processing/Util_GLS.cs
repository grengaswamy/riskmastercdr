﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Common;
using Riskmaster.Db;

namespace WPAProcessing
{
   public  class Util_GLS
    {
        public static string g_sProgramErrorString;
        static bool static_bInitGlossary_Ignore;
        static string static_bInitGlossary_sLastRefresh;
        public static bool bInitGlossary(bool bCheckSyncFlag, bool bLogin)
        {
            bool functionReturnValue = true;
            // ERROR: Not supported in C#: OnErrorStatement

            bool bDoCache = false;

            //functionReturnValue = true;
            bDoCache = bLogin;
            // flags whether caching should occur on this call or not

            // if sync checking is desired, check cache option settings to see what to do
            if (!static_bInitGlossary_Ignore && bCheckSyncFlag && !bLogin)
                bDoCache = bIsCacheNeeded("GLOSSARY", static_bInitGlossary_sLastRefresh, static_bInitGlossary_Ignore);

            // cache it in if desired
            if (bDoCache)
            {
                //if ((bInitGloss(ROCKETCOMLibDTGRocket_definst.DB_GetHdbc(dbLookup), true) != 0))
                //{
                static_bInitGlossary_sLastRefresh = DateTime.Now.ToString("yyyMMddHHmmss");
                //}
                //else
                //{
                //    functionReturnValue = false;
                //}
            }
            return functionReturnValue;
        }
        public static bool bIsCacheNeeded(string sTableName, string sDateTime, bool bIgnore)
        {
            bool functionReturnValue = false;

            string sNow = null;
            string sGlossaryDTTM = null;
            bool result = false;
            int dsGlossary = 0;
            bool bWarning = false;
            string sTmp = null;
            bool bNeeded;

            bNeeded = true;
            // sNow = VB6.Format(DateAndTime.Today + " " + DateAndTime.TimeOfDay, "YYYYMMDDHHNNSS");
            sNow = DateTime.Now.ToString("yyyyMMddHHmmss");
            // dsGlossary = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbLookup, "SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "' ", ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
            DbConnection objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_ConStrRiskmaster);
            objConn.Open();
            // m_stDataBaseType = objConn.DatabaseType;
            using (DbReader objReader = objConn.ExecuteReader("SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "' "))
            {
                while (objReader.Read())
                {
                    sGlossaryDTTM = objReader.GetString("DTTM_LAST_UPDATE") + "";

                    //if ((sDateTime >= sGlossaryDTTM) || (string.IsNullOrEmpty(sDateTime.Trim())))
                    //    bNeeded = false;
                    //if (sGlossaryDTTM > sNow)
                    //{
                    //    //result = ROCKETCOMLibDTGRocket_definst.DB_Edit(dsGlossary);
                    //    //result = ROCKETCOMLibDTGRocket_definst.DB_PutData(dsGlossary, "DTTM_LAST_UPDATE", sNow);
                    //    //result = ROCKETCOMLibDTGRocket_definst.DB_Update(dsGlossary);

                    //}
                }
            }
            //if (!ROCKETCOMLibDTGRocket_definst.DB_EOF(dsGlossary))
            //{
            //    //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //    sGlossaryDTTM = vDB_GetData(dsGlossary, "DTTM_LAST_UPDATE") + "";
            //    if ((sDateTime >= sGlossaryDTTM) || (string.IsNullOrEmpty(sDateTime.Trim())))
            //        bNeeded = false;
            //    if (sGlossaryDTTM > sNow)
            //    {
            //        result = ROCKETCOMLibDTGRocket_definst.DB_Edit(dsGlossary);
            //        result = ROCKETCOMLibDTGRocket_definst.DB_PutData(dsGlossary, "DTTM_LAST_UPDATE", sNow);
            //        result = ROCKETCOMLibDTGRocket_definst.DB_Update(dsGlossary);
            //    }
            //}
            //result = ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(dsGlossary, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);

            if (bNeeded)
            {
                bWarning = true;
                if (UTIL_REG.bRegDBGetValue("Options\\Caching", sTmp))
                {
                    result = UTIL_REG.bRegDBGetValue("Options\\Caching\\" + sTableName + "Allways", sTmp);
                    bNeeded = (Conversion.ConvertStrToBool(sTmp));
                    result = UTIL_REG.bRegDBGetValue("Options\\Caching\\" + sTableName + "Warning", sTmp);
                    bWarning = Conversion.ConvertStrToBool(sTmp);
                }
            }

            functionReturnValue = bNeeded;
            return functionReturnValue;
            //ERROR_TRAP_bIsCacheNeeded:


            //if (iGeneralErrorExt(Err().Number, Conversion.ErrorToString(), "UTIL_GLS.BAS\\bIsCacheNeeded") == IDRETRY)
            //    // ERROR: Not supported in C#: ResumeStatement

            //else
            //    functionReturnValue = false;
            //return functionReturnValue;


        }
    }
}
