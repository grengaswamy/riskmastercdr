﻿using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Text;
namespace WPAProcessing
{
    public partial class WPAForm : Form
    {        
        public UserLogin objUserLogin ;
        public  int gProcessStatus;
        const int PS_END = 0;
        const int  PS_GO = 1;
        const int PS_STOP = 2;
        const int PS_SUSPEND = 3;
       // 'rupal:start,r8 auto diary enh
        public string sAddedByUserColumn =string.Empty;
        public string sUpdatedByUserColumn = string.Empty;
        public string  sDateTimeEnteredOnColumn = string.Empty;
        //we need to define sFromEmailAdd, sSMTPServer as public variables because they will be used in a loop to send email and we dont want to fetch them each time in loop
        public string sFromEmailAdd = string.Empty;
        public string  sSMTPServer = string.Empty;
        //'rupal:end
        int nCount = 0;      
        //private static string m_sConnectionString=string.Empty;
        //private static string m_sConnectionSecurity = string.Empty;
        //public int m_iClientId = 0;
        public static Dictionary<int, CommonFunctions.MultiClientConnection> dictMultiTenant;//Add & change by kuladeep for Cloud
        public WPAForm()
        {
            InitializeComponent();
        }
        public WPAForm(UserLogin m_objLogin, int iClientId)
        {            
            InitializeComponent();
            if (m_objLogin != null)
            {
                lDSNID = m_objLogin.DatabaseId;
                UID = m_objLogin.UserId;
                m_sConnectionRiskmaster = m_objLogin.objRiskmasterDatabase.ConnectionString;
                if (m_objLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                    m_sConnectionRiskmaster += "MultipleActiveResultSets=True;";
                sUserName = m_objLogin.objUser.FirstName + " " + m_objLogin.objUser.LastName ;
                sDSNName = m_objLogin.objRiskmasterDatabase.DataSourceName;
                Globalvar.m_ConStrRiskmaster = m_sConnectionRiskmaster;
                //to position win form in center of teh screen
                this.StartPosition = FormStartPosition.CenterScreen;
                if (iClientId != 0)//Add & change by kuladeep for Cloud
                {                  
                    dictMultiTenant = CommonFunctions.MultiTenantConnection(iClientId);
		    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    Globalvar.m_sConnectionSecurity = StructConnString.SecConnectionString;
                    Globalvar.m_sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    Globalvar.m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource",iClientId);
                    Globalvar.m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity",iClientId);     
                }

                Globalvar.m_iClientId = iClientId;
                //m_iClientId = iClientId;
            }
            systemErrors = new BusinessAdaptorErrors(iClientId);
        }

        // For maintaining Risk master Connection string Globally
        internal static class Globalvar
        {
            public static string m_ConStrRiskmaster { get; set; }
            public static string m_sConnectionString { get; set; }//Add by kuladeep for cloud
            public static string m_sConnectionSecurity { get; set; }//Add by kuladeep for cloud
            public static int m_iClientId { get; set; }//Add by kuladeep for cloud
        }
        
        public int g_dbMake = 0;
        public int NumInfoDefs = 0;
        public  int lDSNID ;
        public string sDSNName = string.Empty;
        public long db;
        public long UID = 0;
        public string sUserName = string.Empty;
        internal class InfoSetting
        {
            public int RowID;
            public string Name;
            public string DefName;
            public int Index;
            public int SendCreate;           
            public int SendUpdate;          
            public int SendAdj;         
            public int SendAdjSupervisor;	
            public int NotifyDays;           
            public int RouteDays;            //ROUTE_DAYS
            public int Priority;             //PRIORITY
            public int TaskEst;              //TASK_ESTIMATE
            public int TaskEstType;          //TASK_EST_TYPE
            public int TimeBillable;         //TIME_BILLABLE_FLAG
            public int ExpSchedule;          //EXPORT_SCHEDULE
            public string Instruct;          //INSTRUCTIONS
            public int NumFilters;
            public string Activities;
            public string SendUsers;
            public string NotifyUsers;
            public string RouteUsers;
            public string ProcessDate;
            public FilterSetting[] FilterSet;
            public string SendGroups;
            public string NotifyGroups; //pkandhari Jira 6412
            public string RouteGroups; //pkandhari Jira 6412
            public int iDueDays;
            public int SendSuppField;
            public string SendSuppFieldName;
            public int Escalate_Highest_Level;
            public int Security_Mgmt_Relationship;
            public int ExportEmail;   
            public int ExpEmail;
        }
        internal class FilterSetting
        {
            public string Name;
            public int Number;
            public string Data;
        }
        internal class  FilterDefinition
        {
            public string Name;
            public int ID;
            public int FilterType;
            public long FilterMin;
            public long FilterMax;
            public string DefValue;
            public string table;
            public string SQLFill;
            public string SQLFrom;
            public string SQLWhere;
            public string  Procedure;
            public string  ProcMsg;
        }
     
        
        internal class InfoDefinition
        {
            public string Name;
            public int ID;
            public string ParentTable;
            public string AttachTable;
            public string AttachCol;
            public string AttachDesc;
            public int AttFormCode;
            public string AttSecRecID;
            public string SQL;
            public string tmpSQL;
            public string SQLRegard;
            public int NumFilters;
            public List< FilterDefinition> FilterDef = new List<FilterDefinition>();
        }


        //private static string m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource");
        //private static string m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity");      

        private static string m_sConnectionRiskmaster = string.Empty;
        public static bool IsCallFromTM = false;
        BusinessAdaptorErrors systemErrors = null;
        private  InfoSetting objInfotemp;
        private InfoDefinition ObjInfoDef = new InfoDefinition() ;
        private void btnStart_Click(object sender, EventArgs e)
        {
            //code moved to GetAutoDiaryCount
            if (btnStart.Tag.ToString() == "0")
            {

                //lstStatus.Clear
                //pnlAutoDef.Value = 0
                //lblDefsProc.Caption = "0 of 0"
                //lblTotDiaries.Caption = "0"
                IsCallFromTM = false;
                btnStart.Tag = "1";
                btnStart.Text = "&Stop";
                btnStart.Refresh();
                btnSuspend.Enabled = true;
                btnExit.Enabled = false;
                WpaprogressBar.Value = 0;

                GetAutoDiaryCount(IsCallFromTM);

                gProcessStatus = PS_END;
                btnStart.Tag = "0";
                btnStart.Text = "Start";
                btnSuspend.Tag = "0";
                btnSuspend.Text = "Suspend";
                btnSuspend.Enabled = false;
                btnExit.Enabled = true;
            }
            else
            {
                btnStart.Tag = "0";
                btnStart.Text = "Start";
                btnSuspend.Tag = "0";
                btnSuspend.Text = "Suspend";
                btnSuspend.Enabled = false;
                btnExit.Enabled = true;

                gProcessStatus = PS_STOP;                
            }
            #region Code Moved
            //// listBoxWPA.ClearSelected();
           // listBoxWPA.Items.Clear();
           // listBoxWPA.Refresh();
           // btnSuspend.Enabled = true;
           // btnExit.Enabled = false;
           // btnStart.Name = "Stop";
           // string sSQL = string.Empty;
           // DbReader objReader = null;
           // DbConnection objConn = null;
           // lblTotDiaries.Text ="0";
           // listBoxWPA.Items.Add("Auto Diary Processing Started @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
           // listBoxWPA.Refresh();
           // int nCount = 0;
           // try
           // {
           //     sSQL = "SELECT COUNT(*) FROM WPA_AUTO_SET ";
           //     //remove after getting correct connection string
           //     objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
           //     objConn.Open();
           //     using (objReader = objConn.ExecuteReader(sSQL))
           //     {
           //         while (objReader.Read())
           //         {
           //             nCount = objReader.GetInt32(0);
           //         }

           //     }
           //     if (nCount > 0)
           //     {
           //         processAuto();
           //         RSWPayTLDiary.GenerateTimeLapseDiaries(lDSNID);
           //     }
           //     WpaprogressBar.Value = 100; 
           //     listBoxWPA.Items.Add("Auto Diary Processing Completed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
           //     listBoxWPA.Refresh();
           //     btnExit.Enabled = true;
           // }
           // //catch (RMAppException p_objEx)
           // //{
           // //    //EventLog.WriteEntry("checkschedule : RMAppException", p_objEx.InnerException.Message + m_sConnectionString);
           // //    //systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
           // //    //logErrors("CheckSchedule", null, false, systemErrors);
           // //}
           // catch (RMAppException p_objEx)
           // {               
           //     systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
           //     WPAProcessing.logErrors("btnStart_Click", null, false, systemErrors);
           // }
           // finally
           // {
           //     if (objConn != null)
           //     {
           //         if (objConn.State == System.Data.ConnectionState.Open)
           //         {
           //             objConn.Close();
           //             objConn.Dispose();
           //         }
           //         if (objReader != null)
           //         {
           //             objReader.Close();
           //             objReader.Dispose();
           //         }
           //     }
            // }
            #endregion 
            
        }
       
        public int GetAutoDiaryCount(bool IsFromTM)// psharma206
        {
            IsCallFromTM = IsFromTM;
            int iCount = 0;
            listBoxWPA.Items.Clear();
            listBoxWPA.Refresh();
            btnSuspend.Enabled = true;
            btnExit.Enabled = false;
            btnStart.Name = "Stop";
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            lblTotDiaries.Text = "0";
           // listBoxWPA.Items.Add("Auto Diary Processing Started @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            UpdateStatus("Auto Diary Processing Started @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            listBoxWPA.Refresh();

            try
            {               
                sSQL = "SELECT COUNT(*) FROM WPA_AUTO_SET ";
                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        nCount = objReader.GetInt32(0);
                    }

                }
                if (nCount > 0)
                {
                    processAuto(Globalvar.m_iClientId);//Add & change by psharma206 for Cloud
                    //RSWPayTLDiary.GenerateTimeLapseDiaries(lDSNID);
                }
                RSWPayTLDiary.GenerateTimeLapseDiaries(lDSNID);//psharma206
                WpaprogressBar.Value = 100;
                if (gProcessStatus != PS_STOP)
                {
                    //listBoxWPA.Items.Add("Auto Diary Processing Completed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                    UpdateStatus("Auto Diary Processing Completed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                    //listBoxWPA.Refresh();
                }
                else
                UpdateStatus("Auto Diary Processing Completed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                //string path = @"D:\Sample\WPA\WPA\bin\Debug\wpaproc.log";
                //File.AppendAllText(path, "Auto Diary Processing Completed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
               
                btnExit.Enabled = true;               
            }
            //catch (RMAppException p_objEx)
            //{
            //    //EventLog.WriteEntry("checkschedule : RMAppException", p_objEx.InnerException.Message + m_sConnectionString);
            //    //systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
            //    //logErrors("CheckSchedule", null, false, systemErrors);
            //}
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("btnStart_Click", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }
            return iCount;
        }
        void processAuto()
        {
            processAuto(0); 
        }
        void processAuto(int iClientId)//Add & change by kuladeep for Cloud
        {
            string sSQL = string.Empty;
            string sqlfilter = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            //DbReader objReaderFilter = null;
            List<string> collSQL = new List<string>();
            List<InfoSetting> objInfoSettings = new List<InfoSetting>();
            //FilterSetting objFilter;
           // Database objSrcDb = null; 
            int progressBarLoad = 0;
            try
            {
                progressBarLoad = WpaprogressBar.Maximum / nCount;

                sSQL = "SELECT * FROM WPA_AUTO_SET";
                //remove after getting correct connection string
                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                g_dbMake = Convert.ToInt32(objConn.DatabaseType);
                //WpaprogressBar.Value = 10; 
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        WpaprogressBar.Value += progressBarLoad;
                        objInfotemp = new InfoSetting();
                        objInfotemp.RowID = objReader.GetInt32("AUTO_ROW_ID");
                        objInfotemp.Name = objReader.GetString("AUTO_NAME");
                        objInfotemp.DefName = objReader.GetString("DEF_NAME");
                        objInfotemp.Index = objReader.GetInt("DEF_INDEX");
                        LoadInfoDef(objInfotemp.Index, objInfotemp.DefName);
                        //listBoxWPA.Items.Add("Processing Auto Diary " +"'"+ objInfotemp.Name +"'" );
                        //listBoxWPA.Refresh();
                        UpdateStatus("Processing Auto Diary " + "'" + objInfotemp.Name + "'");
                        objInfotemp.SendCreate = objReader.GetInt32("SEND_CREATED");
                        objInfotemp.SendUpdate = objReader.GetInt32("SEND_UPDATED");
                        objInfotemp.SendAdj = objReader.GetInt32("SEND_ADJUSTER");
                        objInfotemp.SendAdjSupervisor = objReader.GetInt16("SEND_ADJUSTER_SUPERVISOR");
                        objInfotemp.NotifyDays = objReader.GetInt32("NOTIFICATION_DAYS");
                        objInfotemp.Priority = objReader.GetInt32("PRIORITY");
                        objInfotemp.RouteDays = objReader.GetInt32("ROUTE_DAYS");
                        objInfotemp.TaskEst = objReader.GetInt32("TASK_ESTIMATE");
                        objInfotemp.TaskEstType = objReader.GetInt32("TASK_EST_TYPE");
                        objInfotemp.ExpSchedule = objReader.GetInt32("EXPORT_SCHEDULE");
                       // 'rupal:start, r8 auto diary enh
                        if( objReader.IsDBNull( "EXPORT_EMAIL") )
                            objInfotemp.ExpEmail = 0;
                        else
                            objInfotemp.ExpEmail = objReader.GetInt32( "EXPORT_EMAIL");
        
                     //'rupal:end, r8 auto diary enh
                        objInfotemp.Instruct = objReader.GetString("INSTRUCTIONS");
                        objInfotemp.ProcessDate = objReader.GetString("PROCESS_DATE");
                        objInfotemp.iDueDays = objReader.GetInt32("DUE_DAYS");
                        objInfotemp.SendSuppField = objReader.GetInt32("SEND_SUPPFIELD");
                        objInfotemp.SendSuppFieldName = objReader.GetString("SEND_SUPPFIELDNAME");

                        if( ! objReader.IsDBNull( "SECURITY_MGMT_RELATIONSHIP")) 
                        objInfotemp.Security_Mgmt_Relationship = objReader.GetInt32("SECURITY_MGMT_RELATIONSHIP");        
                        if( ! objReader.IsDBNull("ESCALATE_HIGHEST_LEVEL")) 
                           objInfotemp.Escalate_Highest_Level = objReader.GetInt32("ESCALATE_HIGHEST_LEVEL");
       
                        //objInfotemp.Security_Mgmt_Relationship = objReader.GetInt16("SECURITY_MGMT_RELATIONSHIP");
                        //objInfotemp.Escalate_Highest_Level = objReader.GetInt16("ESCALATE_HIGHEST_LEVEL");
                        objInfotemp.FilterSet = LoadFilters(objInfotemp.RowID);
                        objInfotemp.NumFilters = objInfotemp.FilterSet.Count();
                        objInfotemp.Activities = LoadActivity(objInfotemp.RowID);
                        LoadUsers(objInfotemp.RowID);
                        objInfoSettings.Add(objInfotemp);
                        //check overdue diaries to see if notification/routing is necessary                      
                        CheckOverdue(lDSNID);
                        int iSinceLast = 0;
                        string sDate = objReader.GetString("LAST_RUN");
                        if (string.IsNullOrEmpty(sDate))
                        {
                            iSinceLast = 0;
                        }
                        else
                        {                            
                            DateTime LastDate = Convert.ToDateTime(sDate.Substring(0, 4) + "-" + sDate.Substring(4, 2) + "-" + sDate.Substring(6, 2));
                            TimeSpan ts = DateTime.Now - LastDate;
                            iSinceLast = ts.Days;
                            if (iSinceLast > 0)
                                iSinceLast = iSinceLast - 1;
                        }
                        InfoRun(iSinceLast,iClientId);
                        sSQL = "UPDATE WPA_AUTO_SET SET LAST_RUN = '" + DateTime.Now.ToString("yyyyMMdd") + "' WHERE AUTO_ROW_ID = " + objInfotemp.RowID;
                        collSQL.Add(sSQL);
                    }
                }
                foreach (string Str in collSQL)
                {
                    objConn.ExecuteNonQuery(Str);
                }
                //WpaprogressBar.Value =70; 
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("Process Auto", null, false, systemErrors);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }
        FilterSetting[] LoadFilters(int Rowid)
        {
            DbConnection objCon = null; ;
            DbReader objReaderFilter = null;
            List<FilterSetting> objFilter = new List<FilterSetting>();
            FilterSetting objFiltertemp = new FilterSetting();
            objCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objCon.Open();
            try
            {
                string sqlfilter = "SELECT * FROM WPA_AUTO_FILTER WHERE AUTO_ID = " + Rowid;
                using (objReaderFilter = objCon.ExecuteReader(sqlfilter))
                {
                    while (objReaderFilter.Read())
                    {
                        objFiltertemp = new FilterSetting();
                        objFiltertemp.Number = objReaderFilter.GetInt32("FILTER_INDEX");
                        objFiltertemp.Data = objReaderFilter.GetString("FILTER_DATA");
                        objFilter.Add(objFiltertemp);
                    }
                    //objReaderFilter.Close();
                }
            }
            catch (Exception p_objEx)
            {                
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("Load Filters", null, false, systemErrors);
            }
            finally
            {
                if (objReaderFilter != null)
                {
                    objReaderFilter.Close();
                    objReaderFilter.Dispose();
                }
                if (objCon != null)
                {
                    if (objCon.State == System.Data.ConnectionState.Open)
                    {
                        objCon.Close();
                        objCon.Dispose();
                    }
                }
            }    
            
            return objFilter.ToArray();
        }
        string LoadActivity(int RowID)
        {
            DbConnection objCon = null; ;
            DbReader objReaderActivity = null;
            string Activity = string.Empty;
            objCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objCon.Open();
            try
            {
                string sqlfilter = "SELECT * FROM WPA_AUTO_ACT WHERE AUTO_ID =  " + RowID;
                using (objReaderActivity = objCon.ExecuteReader(sqlfilter))
                {
                    while (objReaderActivity.Read())
                    {
                        Activity += objReaderActivity.GetInt("ACTIVITY_CODE").ToString() + "|" + objReaderActivity.GetString("ACTIVITY_TEXT") + ",";
                    }
                    objReaderActivity.Close();
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("Load Activity", null, false, systemErrors);
            }
            finally
            {
                if (objReaderActivity != null)
                {
                    objReaderActivity.Close();
                    objReaderActivity.Dispose();
                }
                if (objCon != null)
                {
                    if (objCon.State == System.Data.ConnectionState.Open)
                    {
                        objCon.Close();
                        objCon.Dispose();
                    }
                }
            }           
            return Activity;
        }
        void LoadUsers(int RowID)
        {
            DbConnection objUserCon = null; ;
            DbReader objReaderUsers = null;
            int UserID = 0;
            int UserType = 0;
            objUserCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objUserCon.Open();
            string Users;
            string sqluser = "SELECT * FROM WPA_AUTO_USER WHERE AUTO_ID = " + RowID;
            try
            {
                using (objReaderUsers = objUserCon.ExecuteReader(sqluser))
                {
                    while (objReaderUsers.Read())
                    {
                        UserID = objReaderUsers.GetInt("USER_ID");
                        UserType = objReaderUsers.GetInt("USER_TYPE");
                        Users = RSWPayTLDiary.sGetUserName(lUserName: string.Empty, lUserId: UserID);
                        switch (UserType)
                        {
                            //InfoSetting objInfotemp;
                            case 1: objInfotemp.SendUsers = objInfotemp.SendUsers + Users + ",";
                                break;
                            case 2:
                                objInfotemp.NotifyUsers = objInfotemp.NotifyUsers + Users + ",";
                                break;
                            case 3:
                                objInfotemp.RouteUsers = objInfotemp.RouteUsers + Users + ",";
                                break;
                            //pkandhari Jira 6412 starts
                            case 4:
                                objInfotemp.SendGroups = objInfotemp.SendGroups + UserID + ",";
                                break;
                            case 5:
                                objInfotemp.NotifyGroups = objInfotemp.NotifyGroups + UserID + ",";
                                break;
                            case 6:
                                objInfotemp.RouteGroups = objInfotemp.RouteGroups + UserID + ",";
                                break;
                            //pkandhari Jira 6412 ends
                        }
                    }

                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("Load Activity", null, false, systemErrors);
            }
            finally
            {
                if (objReaderUsers != null)
                {
                    objReaderUsers.Close();
                    objReaderUsers.Dispose();
                }
                if (objUserCon != null)
                {
                    if (objUserCon.State == System.Data.ConnectionState.Open)
                    {
                        objUserCon.Close();
                        objUserCon.Dispose();
                    }
                }               
            }
            // return Users;
        }

        public void CheckOverdue(int lDSNID)
        {
            string sSQL = null;
            string sSQLInsert = null;
            string sTemp = null;
            string sTmp = null;
            string sTempUser = null;
            int iPtr = 0;
            int i = 0;
            int lDiaries = 0;
            int lDiaryStart = 0;
            string sNotes = null;
            string sUser = null;
            DbConnection objUserCon = null;
            objUserCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            eDatabaseType m_stDataBaseType = objUserCon.DatabaseType;

            objUserCon.Open();
            //listBoxWPA.Items.Add("Checking existing diaries for Overdue Notification");
            //listBoxWPA.Refresh();
            UpdateStatus("Checking existing diaries for Overdue Notification");
            if (objInfotemp.NotifyDays > 0)
            {
                if (objInfotemp.Security_Mgmt_Relationship > 0)
                    if(objUserCon==null)
                        objUserCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);

                m_stDataBaseType = objUserCon.DatabaseType;
                
                using (DbReader objReader = objUserCon.ExecuteReader("SELECT ASSIGNED_USER FROM WPA_DIARY_ENTRY WHERE AUTO_ID = " + objInfotemp.RowID + "And NOTIFY_FLAG = 0"))
                {
                    if (objInfotemp.Escalate_Highest_Level == 1)
                    {
                        while (objReader.Read())
                        {
                            string user = objReader.GetString("ASSIGNED_USER");
                            sUser = Global.sGetHighestLevelManager(lDSNID, user);
                            objInfotemp.NotifyUsers += sUser + ",";                           
                        }
                    }
                    else if(objInfotemp.Escalate_Highest_Level == 3 || objInfotemp.Escalate_Highest_Level == 2)
                    {
                        while (objReader.Read())
                        {
                            string user = objReader.GetString("ASSIGNED_USER");
                            sUser = Global.sGetManager(  lDSNID,  user);
                            if(!string.IsNullOrEmpty(sUser))
                                objInfotemp.NotifyUsers += sUser + ",";                            
                        }
                    }
                }
                
                lDiaries = 0;
                lDiaryStart = Convert.ToInt32(lblTotDiaries.Text);             

                sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE AUTO_ID = " + objInfotemp.RowID + " AND NOTIFY_FLAG = 0";

                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                    sSQL = sSQL + " AND DATEDIFF(dd,SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),5,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),7,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),1,4),'" + DateTime.Now.ToString("MM/dd/yyyy") + "') >= " + objInfotemp.NotifyDays;
                    //sSQL = sSQL + " AND DATEDIFF(dd,SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyy-MM-dd") + "'),5,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyy-MM-dd") + "'),7,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyy-MM-dd") + "'),1,4),'" + DateTime.Now.ToString("MM/dd/yyyy") + "') >= " + objInfotemp.NotifyDays;
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_INFORMIX)
                    sSQL = sSQL + " AND (DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "')-DATE(COMPLETE_DATE[5,6] || " + char.ConvertFromUtf32(34) + "/" + char.ConvertFromUtf32(34) + " || COMPLETE_DATE[7,8] || " + char.ConvertFromUtf32(34) + "/" + char.ConvertFromUtf32(34) + " || COMPLETE_DATE[1,4])) >= " + objInfotemp.NotifyDays;
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    sSQL = sSQL + " AND (TO_DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "','MM/dd/yyyy')-DTGToDate(COMPLETE_DATE)) >= " + objInfotemp.NotifyDays;
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                    sSQL = sSQL + " AND (DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "')-DATE(SUBSTR(COMPLETE_DATE,5,2) || '/' || SUBSTR(COMPLETE_DATE,7,2) || '/' || SUBSTR(COMPLETE_DATE,1,4))) >= " + objInfotemp.NotifyDays;
                else
                    sSQL = sSQL + " AND DATEDIFF('d',MID(COMPLETE_DATE,5,2) + '/' + MID(COMPLETE_DATE,7,2) + '/' + MID(COMPLETE_DATE,1,4),'" + DateTime.Now.ToString("MM/dd/yyyy") + "') >= " + objInfotemp.NotifyDays;
                using (DbReader objReader2 = objUserCon.ExecuteReader(sSQL))
                {
                    sTempUser = objInfotemp.NotifyUsers;
                    String strUser;
                    String strManager;

                    while (objReader2.Read())
                    {
                        sSQL = "UPDATE WPA_DIARY_ENTRY SET NOTIFY_FLAG = -1 WHERE ENTRY_ID = " + objReader2.GetInt("ENTRY_ID");
                        int nRes = objUserCon.ExecuteNonQuery(sSQL);
                        if (objInfotemp.Security_Mgmt_Relationship > 0)
                        {
                            //33558 Start
                            strUser = String.Empty;
                            strManager = String.Empty;

                            using (DbReader objReader = objUserCon.ExecuteReader("SELECT ASSIGNED_USER FROM WPA_DIARY_ENTRY WHERE ENTRY_ID = " + objReader2.GetInt("ENTRY_ID")))
                            {
                                if (objInfotemp.Escalate_Highest_Level == 1)
                                {
                                    while (objReader.Read())
                                    {
                                        string user = objReader.GetString("ASSIGNED_USER");
                                        strUser = Global.sGetHighestLevelManager(lDSNID, user);
                                        strManager += strUser + ",";                           
                                    }
                                }
                                else if(objInfotemp.Escalate_Highest_Level == 3 || objInfotemp.Escalate_Highest_Level == 2)
                                {
                                    while (objReader.Read())
                                    {
                                        string user = objReader.GetString("ASSIGNED_USER");
                                        strUser = Global.sGetManager(lDSNID, user);
                                        strManager += strUser + ",";                            
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(strManager))
                                //iPtr = sTempUser.IndexOf(",");
                                iPtr = strManager.IndexOf(",");
                            if (iPtr != 0)
                            {
                                //sTmp = sTempUser.Substring(0, iPtr);
                                //sTempUser = sTempUser.Substring(iPtr + 1);
                                sTmp = strManager.Substring(0, iPtr);
                                sTempUser = strManager.Substring(iPtr + 1);
                            }
                            //33558 End

                            if (!string.IsNullOrEmpty(sTmp))
                            {
                                sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (";
                                for (i = 0; i <= objReader2.FieldCount - 1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    sSQLInsert = sSQLInsert + objReader2.GetName(i);
                                }
                                sSQLInsert = sSQLInsert + ") VALUES (";
                                for (i = 0; i <= objReader2.FieldCount - 1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    if (objReader2.GetName(i).ToUpper() == "ENTRY_ID")
                                    {
                                        sSQLInsert = sSQLInsert + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId);//psharma206
                                    }
                                    else if (objReader2.GetName(i).ToUpper() == "ASSIGNED_USER")
                                    {
                                        sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sTmp);
                                    }
                                    else if (objReader2.GetName(i).ToUpper() == "NOTIFY_FLAG")
                                    {
                                        //sSQLInsert = sSQLInsert + "-1";
                                        if( objInfotemp.Escalate_Highest_Level == 2 )
                                             sSQLInsert = sSQLInsert + "0";
                                        else if (objInfotemp.Escalate_Highest_Level == 1 || objInfotemp.Escalate_Highest_Level == 3) 
                                            sSQLInsert = sSQLInsert + "-1";                                       
                            
                                    }
                                    else if( objReader2.GetName(i).ToUpper() == "COMPLETE_DATE")
                                        sSQLInsert = sSQLInsert + DateTime.Now.ToString("yyyyMMdd");
                                    else if( objReader2.GetName(i).ToUpper() == "CREATE_DATE" )
                                        sSQLInsert = sSQLInsert + DateTime.Now.ToString("yyyyMMdd");
                                    else if (objReader2.GetName(i).ToUpper() == "ENTRY_NOTES")
                                    {                                        
                                        sNotes = "OVERDUE DIARY NOTIFICATION; ASSIGNED TO " + objReader2.GetString("ASSIGNED_USER") + ". " + objReader2.GetValue(i) + "";
                                        if (sNotes.Length > 200)
                                        {
                                            // int len = sNotes.Length;
                                          sNotes=  sNotes.Substring(0, 200);
                                            //sNotes = Strings.Left(sNotes, 200);
                                        }
                                        sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sNotes);
                                    }
                                    else
                                    {
                                        if (objReader2.IsDBNull(i))
                                        {
                                            sSQLInsert = sSQLInsert + "NULL";
                                        }
                                        else
                                        {
                                            switch (objReader2.GetDataTypeName(i).ToUpper())
                                            {
                                                case "INT":
                                                    // if(objReader2.GetDataTypeName(i).ToUpper() == "INT")
                                                    sSQLInsert = sSQLInsert + objReader2.GetInt(i);
                                                    break;
                                                case "FLOAT":
                                                case "DOUBLE":
                                                    sSQLInsert = sSQLInsert + objReader2.GetDouble(i);
                                                    break;
                                                case "STRING":
                                                    sSQLInsert = sSQLInsert + objReader2.GetString(i);
                                                    break;
                                                case "SMALLINT":
                                                    sSQLInsert = sSQLInsert + objReader2.GetInt16(i);
                                                    break;
                                                default:
                                                    sSQLInsert = sSQLInsert + "'" + objReader2.GetString(i) + "'";
                                                    break;
                                            }
                                        }
                                    }
                                }
                                sSQLInsert = sSQLInsert + ")";

                                objUserCon.ExecuteNonQuery(sSQLInsert);
                                // update status every ten diaries
                                lDiaries = lDiaries + 1;
                                if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) / 10)
                                {
                                    
                                    lblTotDiaries.Text = Convert.ToString(lDiaryStart + lDiaries);
                                    // check if diary processing has been suspended
                                    System.Windows.Forms.Application.DoEvents();
                                    while (gProcessStatus == PS_SUSPEND)
                                    {
                                        System.Windows.Forms.Application.DoEvents();
                                    }

                                    // check if diary processing has been stopped
                                     if( gProcessStatus == PS_STOP) 
                                     {
                                         if (CheckStop())
                                         {
                                             //DB_CloseRecordset rs, DB_DROP
                                         }
                                     }

                                }
                            }
                        }
                        else
                        {
                            sTemp = objInfotemp.NotifyUsers;
                            while (!string.IsNullOrEmpty(sTemp))
                            {
                                iPtr = sTemp.IndexOf(",");
                                sTmp = sTemp.Substring(0, iPtr);
                                sTemp = sTemp.Substring(iPtr + 1);
                                if (sTmp != objReader2.GetString("ASSIGNED_USER") + "")
                                {
                                    sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (";
                                    for (i = 0; i <= objReader2.FieldCount - 1; i++)
                                    {
                                        if (i >= 1)
                                            sSQLInsert = sSQLInsert + ",";
                                        sSQLInsert = sSQLInsert + objReader2.GetName(i);
                                    }
                                    sSQLInsert = sSQLInsert + ") VALUES (";
                                    for (i = 0; i <= objReader2.FieldCount - 1; i++) //ROCKETCOMLibDTGRocket_definst.DB_ColumnCount(rs)
                                    {
                                        if (i >= 1)
                                            sSQLInsert = sSQLInsert + ",";

                                        if (objReader2.GetName(i).ToUpper() == "ENTRY_ID")
                                        {
                                            sSQLInsert = sSQLInsert + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId);// +lGetNextUID("WPA_DIARY_ENTRY");
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "ASSIGNED_USER")
                                        {
                                            sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sTmp);
                                        }
                                        //pkandhari Jira 6412 starts
                                        else if (objReader2.GetName(i).ToUpper() == "ASSIGNED_GROUP")
                                        {
                                            sSQLInsert = sSQLInsert + "NULL";
                                        }
                                        //pkandhari Jira 6412 ends
                                        else if (objReader2.GetName(i).ToUpper() == "NOTIFY_FLAG")
                                        {
                                            sSQLInsert = sSQLInsert + "-1";
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "ENTRY_NOTES")
                                        {
                                            //pkandhari Jira 6412 starts
                                            if (objReader2.GetString("ASSIGNED_USER") == "" || objReader2.GetString("ASSIGNED_USER") == "NULL")
                                            {
                                                sNotes = "OVERDUE DIARY NOTIFICATION; ASSIGNED TO " + objReader2.GetString("ASSIGNED_GROUP") + ". " + objReader2.GetValue(i) + "";
                                            }
                                            else
                                            //pkandhari Jira 6412 ends
                                                sNotes = "OVERDUE DIARY NOTIFICATION; ASSIGNED TO " + objReader2.GetString("ASSIGNED_USER") + ". " + objReader2.GetValue(i) + "";
                                            if (sNotes.Length > 200)
                                               sNotes= sNotes.Substring(0, 200);
                                            sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sNotes);
                                        }
                                        else
                                        {

                                            if (objReader2.IsDBNull(i))
                                            {
                                                sSQLInsert = sSQLInsert + "NULL";
                                            }
                                            else
                                            {
                                                //Sai
                                                //if (ROCKETCOMLibDTGRocket_definst.DB_ColumnType(rs, i) == ROCKETCOMLib.DTGDBColTypes.ODBC_TEXT)
                                                //if (objReader2.GetDataTypeName(i) == ROCKETCOMLib.DTGDBColTypes.ODBC_TEXT.ToString())
                                                //{
                                                //    //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                //    sSQLInsert = sSQLInsert +UTILITY.sSQLTextArg(objReader2.GetData(i).ToString());
                                                //}
                                                //else
                                                //{
                                                //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                switch (objReader2.GetDataTypeName(i).ToUpper())
                                                {
                                                    case "INT":
                                                        // if(objReader2.GetDataTypeName(i).ToUpper() == "INT")
                                                        sSQLInsert = sSQLInsert + objReader2.GetInt(i);
                                                        break;
                                                    case "FLOAT":
                                                    case "DOUBLE":
                                                        sSQLInsert = sSQLInsert + objReader2.GetDouble(i);
                                                        break;
                                                    case "STRING":
                                                        sSQLInsert = sSQLInsert + objReader2.GetString(i);
                                                        break;
                                                    case "SMALLINT":
                                                        sSQLInsert = sSQLInsert + objReader2.GetInt16(i);
                                                        break;
                                                    default:
                                                        sSQLInsert = sSQLInsert + "'" + objReader2.GetString(i) + "'";
                                                        break;
                                                }
                                                // }
                                            }
                                        }
                                    }
                                    sSQLInsert = sSQLInsert + ")";

                                    objUserCon.ExecuteNonQuery(sSQLInsert);
                                    // update status every ten diaries
                                    lDiaries = lDiaries + 1;
                                    if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) / 10)
                                    {
                                        lblTotDiaries.Text = Convert.ToString(lDiaryStart + lDiaries);

                                        // check if diary processing has been suspended
                                        System.Windows.Forms.Application.DoEvents();
                                        while (gProcessStatus == PS_SUSPEND)
                                        {
                                            System.Windows.Forms.Application.DoEvents();
                                        }

                                        // check if diary processing has been stopped
                                        if (gProcessStatus == PS_STOP)
                                        {
                                            //UPGRADE_WARNING: Couldn't resolve default property of object CheckStop(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            if (CheckStop())
                                            {
                                                //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                                return;
                                            }
                                        }

                                    }

                                }
                            }
                        }
                        //pkandhari Jira 6412 starts
                        if (!string.IsNullOrEmpty(objInfotemp.NotifyGroups))
                        {
                            sTemp = objInfotemp.NotifyGroups;
                            while (!string.IsNullOrEmpty(sTemp))
                            {
                                iPtr = sTemp.IndexOf(",");
                                sTmp = sTemp.Substring(0, iPtr);
                                sTemp = sTemp.Substring(iPtr + 1);
                                if (sTmp != objReader2.GetString("ASSIGNED_GROUP") + "")
                                {
                                    sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (";
                                    for (i = 0; i <= objReader2.FieldCount - 1; i++)
                                    {
                                        if (i >= 1)
                                            sSQLInsert = sSQLInsert + ",";
                                        sSQLInsert = sSQLInsert + objReader2.GetName(i);
                                    }
                                    sSQLInsert = sSQLInsert + ") VALUES (";
                                    for (i = 0; i <= objReader2.FieldCount - 1; i++) //ROCKETCOMLibDTGRocket_definst.DB_ColumnCount(rs)
                                    {
                                        if (i >= 1)
                                            sSQLInsert = sSQLInsert + ",";

                                        if (objReader2.GetName(i).ToUpper() == "ENTRY_ID")
                                        {
                                            sSQLInsert = sSQLInsert + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId);// +lGetNextUID("WPA_DIARY_ENTRY");
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "ASSIGNED_GROUP")
                                        {
                                            sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sTmp);
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "ASSIGNED_USER")
                                        {
                                            sSQLInsert = sSQLInsert + "NULL";
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "NOTIFY_FLAG")
                                        {
                                            sSQLInsert = sSQLInsert + "-1";
                                        }

                                        else if (objReader2.GetName(i).ToUpper() == "ENTRY_NOTES")
                                        {
                                            if (objReader2.GetString("ASSIGNED_USER") == "" || objReader2.GetString("ASSIGNED_USER") == "NULL")
                                            {
                                                sNotes = "OVERDUE DIARY NOTIFICATION; ASSIGNED TO " + objReader2.GetString("ASSIGNED_GROUP") + ". " + objReader2.GetValue(i) + "";
                                            }
                                            else
                                                sNotes = "OVERDUE DIARY NOTIFICATION; ASSIGNED TO " + objReader2.GetString("ASSIGNED_USER") + ". " + objReader2.GetValue(i) + "";
                                            if (sNotes.Length > 200)
                                                sNotes = sNotes.Substring(0, 200);
                                            sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sNotes);
                                        }
                                        else
                                        {
                                            if (objReader2.IsDBNull(i))
                                            {
                                                sSQLInsert = sSQLInsert + "NULL";
                                            }
                                            else
                                            {
                                                //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                switch (objReader2.GetDataTypeName(i).ToUpper())
                                                {
                                                    case "INT":
                                                        // if(objReader2.GetDataTypeName(i).ToUpper() == "INT")
                                                        sSQLInsert = sSQLInsert + objReader2.GetInt(i);
                                                        break;
                                                    case "FLOAT":
                                                    case "DOUBLE":
                                                        sSQLInsert = sSQLInsert + objReader2.GetDouble(i);
                                                        break;
                                                    case "STRING":
                                                        sSQLInsert = sSQLInsert + objReader2.GetString(i);
                                                        break;
                                                    case "SMALLINT":
                                                        sSQLInsert = sSQLInsert + objReader2.GetInt16(i);
                                                        break;
                                                    default:
                                                        sSQLInsert = sSQLInsert + "'" + objReader2.GetString(i) + "'";
                                                        break;
                                                }
                                                // }
                                            }
                                        }
                                    }
                                    sSQLInsert = sSQLInsert + ")";

                                    objUserCon.ExecuteNonQuery(sSQLInsert);
                                    // update status every ten diaries
                                    lDiaries = lDiaries + 1;
                                    if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) / 10)
                                    {
                                        lblTotDiaries.Text = Convert.ToString(lDiaryStart + lDiaries);

                                        // check if diary processing has been suspended
                                        System.Windows.Forms.Application.DoEvents();
                                        while (gProcessStatus == PS_SUSPEND)
                                        {
                                            System.Windows.Forms.Application.DoEvents();
                                        }

                                        // check if diary processing has been stopped
                                        if (gProcessStatus == PS_STOP)
                                        {
                                            //UPGRADE_WARNING: Couldn't resolve default property of object CheckStop(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            if (CheckStop())
                                            {
                                                //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //pkandhari Jira 6412 ends

                    }
                }
            }
            //listBoxWPA.Items.Add("Generated " + lDiaries + " Notification Diary(s)");            
            //listBoxWPA.Items.Add("Checking existing diaries for Overdue Routing");
            //listBoxWPA.Refresh();
             UpdateStatus("Generated " + lDiaries + " Notification Diary(s)");
             UpdateStatus("Checking existing diaries for Overdue Routing");
          if (objInfotemp.RouteDays !=0)
            {
                lDiaries = 0;
                lDiaryStart =Convert.ToInt32( lblTotDiaries.Text);
                sSQL = "SELECT * FROM WPA_DIARY_ENTRY WHERE AUTO_ID = " + objInfotemp.RowID + " AND ROUTE_FLAG = 0";
                switch (m_stDataBaseType)
                {
                    case eDatabaseType.DBMS_IS_SQLSRVR:
                    case eDatabaseType.DBMS_IS_SYBASE:
                        sSQL = sSQL + " AND DATEDIFF(dd,SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),5,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),7,2)+'/'+SUBSTRING(ISNULL(COMPLETE_DATE,'" + DateTime.Now.ToString("yyyyMMdd") + "'),1,4),'" + DateTime.Now.ToString("yyyy/MM/dd") + "') >= " + objInfotemp.RouteDays;
                        break;
                    case eDatabaseType.DBMS_IS_INFORMIX:
                        sSQL = sSQL + " AND (DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "')-DATE(COMPLETE_DATE[5,6] || " + Char.ConvertFromUtf32(34) + "/" + Char.ConvertFromUtf32(34) + " || COMPLETE_DATE[7,8] || " + Char.ConvertFromUtf32(34) + "/" + Char.ConvertFromUtf32(34) + " || COMPLETE_DATE[1,4])) >= " + objInfotemp.RouteDays;
                        break;
                    case eDatabaseType.DBMS_IS_ORACLE:
                        sSQL = sSQL + " AND (TO_DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "','MM/dd/yyyy')-DTGToDate(COMPLETE_DATE)) >= " + objInfotemp.RouteDays;
                        break;
                    case eDatabaseType.DBMS_IS_DB2:
                        // JP 1/29/2003
                        sSQL = sSQL + " AND (DATE('" + DateTime.Now.ToString("MM/dd/yyyy") + "')-DATE(SUBSTR(COMPLETE_DATE,5,2) || '/' || SUBSTR(COMPLETE_DATE,7,2) || '/' || SUBSTR(COMPLETE_DATE,1,4))) >= " + objInfotemp.RouteDays;
                        break;
                    default:
                        sSQL = sSQL + " AND DATEDIFF('d',MID(COMPLETE_DATE,5,2) & '/' & MID(COMPLETE_DATE,7,2) & '/' & MID(COMPLETE_DATE,1,4),'" + DateTime.Now.ToString("MM/dd/yyyy") + "') >= " + objInfotemp.RouteDays;
                        break;
                }
                
              using(  DbReader objReader3 = objUserCon.ExecuteReader(sSQL))
              {                
                while(objReader3.Read())
                {                   
                    sSQL = "UPDATE WPA_DIARY_ENTRY SET ROUTE_FLAG = -1 WHERE ENTRY_ID = " + objReader3.GetInt( "ENTRY_ID");
                    objUserCon.ExecuteNonQuery(sSQL);
                    if (!string.IsNullOrEmpty(objInfotemp.RouteUsers)) //pkandhari Jira 6412
                    {
                        sTemp = objInfotemp.RouteUsers;
                        while (!string.IsNullOrEmpty(sTemp))
                        {
                            iPtr = sTemp.IndexOf(",");
                            sTmp = sTemp.Substring(0, iPtr);
                            sTemp = sTemp.Substring(iPtr + 1);
                            if (sTmp != objReader3.GetString("ASSIGNED_USER") + "")
                            {
                                sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (";
                                for (i = 0; i <= objReader3.FieldCount-1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    sSQLInsert = sSQLInsert + objReader3.GetName(i); //ROCKETCOMLibDTGRocket_definst.DB_ColumnName(rs, i);
                                }
                                sSQLInsert = sSQLInsert + ") VALUES (";
                                for (i = 0; i <= objReader3.FieldCount-1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    if (objReader3.GetName(i).ToUpper() == "ENTRY_ID")
                                    {
                                        sSQLInsert = sSQLInsert + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId);//+ lGetNextUID("WPA_DIARY_ENTRY");
                                    }
                                    else  if (objReader3.GetName(i).ToUpper() == "ASSIGNED_USER")
                                    {
                                        sSQLInsert = sSQLInsert +Utility.sSQLTextArg(sTmp);
                                    }
                                    //pkandhari Jira 6412 starts
                                    else if (objReader3.GetName(i).ToUpper() == "ASSIGNED_GROUP")
                                    {
                                        sSQLInsert = sSQLInsert + "NULL";
                                    }
                                    //pkandhari Jira 6412 ends
                                    else  if (objReader3.GetName(i).ToUpper() == "ROUTE_FLAG")
                                    {
                                        sSQLInsert = sSQLInsert + "-1";
                                    }
                                    else  if (objReader3.GetName(i).ToUpper() == "ENTRY_NOTES")
                                    {
                                        //pkandhari Jira 6412 starts
                                        if (objReader3.GetString("ASSIGNED_USER") == "" || objReader3.GetString("ASSIGNED_USER") == "NULL")
                                        {
                                            sNotes = "OVERDUE DIARY ROUTED; ORIGINALLY ASSIGNED TO " + objReader3.GetString("ASSIGNED_GROUP") + "." + objReader3.GetString(i) + "";
                                        }
                                        else
                                        //pkandhari Jira 6412 ends
                                            sNotes = "OVERDUE DIARY ROUTED; ORIGINALLY ASSIGNED TO " + objReader3.GetString("ASSIGNED_USER") + "." + objReader3.GetString(i) + "";
                                        if (sNotes.Length > 200)
                                            sNotes= sNotes.Substring(0, 200);
                                        sSQLInsert = sSQLInsert +Utility.sSQLTextArg(sNotes);
                                    }
                                    else
                                    {
                                        if (objReader3.IsDBNull( i))
                                        {
                                            sSQLInsert = sSQLInsert + "NULL";
                                        }
                                        else
                                        {
                                            //Sai
                                            // if (ROCKETCOMLibDTGRocket_definst.DB_ColumnType(rs, i) == ROCKETCOMLib.DTGDBColTypes.ODBC_TEXT)
                                            //if (objReader3.GetDataTypeName(i) == ROCKETCOMLib.DTGDBColTypes.ODBC_TEXT.ToString())
                                            //{
                                            //    //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            //    sSQLInsert = sSQLInsert +UTILITY.sSQLTextArg(objReader3.GetData( i).ToString());
                                            //}
                                            //else
                                            //{
                                            //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            //sSQLInsert = sSQLInsert + UTILITY.sSQLTextArg(objReader3.GetData(i));
                                            // }
                                            switch (objReader3.GetDataTypeName(i).ToUpper())
                                            {
                                                case "INT":
                                                    // if(objReader2.GetDataTypeName(i).ToUpper() == "INT")
                                                    sSQLInsert = sSQLInsert + objReader3.GetInt(i);
                                                    break;
                                                case "FLOAT":
                                                case "DOUBLE":
                                                    sSQLInsert = sSQLInsert + objReader3.GetDouble(i);
                                                    break;
                                                case "STRING":
                                                    sSQLInsert = sSQLInsert + objReader3.GetString(i);
                                                    break;
                                                case "SMALLINT":
                                                    sSQLInsert = sSQLInsert + objReader3.GetInt16(i);
                                                    break;
                                                default:
                                                    sSQLInsert = sSQLInsert + "'" + objReader3.GetString(i) + "'";
                                                    break;
                                            }
                                        }
                                    }
                                }
                                sSQLInsert = sSQLInsert + ")";
                                objUserCon.ExecuteNonQuery(sSQLInsert);

                                // update status every ten diaries
                                lDiaries = lDiaries + 1;
                                if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) % 10)
                                {
                                    lblTotDiaries.Text =Convert.ToString(lDiaryStart + lDiaries);

                                    // check if diary processing has been suspended
                                    System.Windows.Forms.Application.DoEvents();
                                    while (gProcessStatus == PS_SUSPEND)
                                    {
                                        System.Windows.Forms.Application.DoEvents();
                                    }

                                    // check if diary processing has been stopped
                                    if (gProcessStatus == PS_STOP)
                                    {
                                        if (CheckStop())
                                        {
                                            //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                            return;
                                        }
                                    }

                                }

                            }
                        }

                    }

                    //pkandhari Jira 6412 starts
                    if (!string.IsNullOrEmpty(objInfotemp.RouteGroups))
                    {
                        sTemp = objInfotemp.RouteGroups;
                        while (!string.IsNullOrEmpty(sTemp))
                        {
                            iPtr = sTemp.IndexOf(",");
                            sTmp = sTemp.Substring(0, iPtr);
                            sTemp = sTemp.Substring(iPtr + 1);
                            if (sTmp != objReader3.GetString("ASSIGNED_GROUP") + "")
                            {
                                sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (";
                                for (i = 0; i <= objReader3.FieldCount - 1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    sSQLInsert = sSQLInsert + objReader3.GetName(i); //ROCKETCOMLibDTGRocket_definst.DB_ColumnName(rs, i);
                                }
                                sSQLInsert = sSQLInsert + ") VALUES (";
                                for (i = 0; i <= objReader3.FieldCount - 1; i++)
                                {
                                    if (i >= 1)
                                        sSQLInsert = sSQLInsert + ",";
                                    if (objReader3.GetName(i).ToUpper() == "ENTRY_ID")
                                    {
                                        sSQLInsert = sSQLInsert + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId);//+ lGetNextUID("WPA_DIARY_ENTRY");
                                    }
                                    else if (objReader3.GetName(i).ToUpper() == "ASSIGNED_GROUP")
                                    {
                                        sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sTmp);
                                    }
                                    else if (objReader3.GetName(i).ToUpper() == "ASSIGNED_USER")
                                    {
                                        sSQLInsert = sSQLInsert + "NULL";
                                    }
                                    else if (objReader3.GetName(i).ToUpper() == "ROUTE_FLAG")
                                    {
                                        sSQLInsert = sSQLInsert + "-1";
                                    }
                                    else if (objReader3.GetName(i).ToUpper() == "ENTRY_NOTES")
                                    {
                                        if (objReader3.GetString("ASSIGNED_USER") == "" || objReader3.GetString("ASSIGNED_USER") == "NULL")
                                        {
                                            sNotes = "OVERDUE DIARY ROUTED; ORIGINALLY ASSIGNED TO " + objReader3.GetString("ASSIGNED_GROUP") + "." + objReader3.GetString(i) + "";
                                        }
                                        else
                                            sNotes = "OVERDUE DIARY ROUTED; ORIGINALLY ASSIGNED TO " + objReader3.GetString("ASSIGNED_USER") + "." + objReader3.GetString(i) + "";
                                        if (sNotes.Length > 200)
                                            sNotes = sNotes.Substring(0, 200);
                                        sSQLInsert = sSQLInsert + Utility.sSQLTextArg(sNotes);
                                    }
                                    else
                                    {
                                        if (objReader3.IsDBNull(i))
                                        {
                                            sSQLInsert = sSQLInsert + "NULL";
                                        }
                                        else
                                        {
                                            switch (objReader3.GetDataTypeName(i).ToUpper())
                                            {
                                                case "INT":
                                                    // if(objReader2.GetDataTypeName(i).ToUpper() == "INT")
                                                    sSQLInsert = sSQLInsert + objReader3.GetInt(i);
                                                    break;
                                                case "FLOAT":
                                                case "DOUBLE":
                                                    sSQLInsert = sSQLInsert + objReader3.GetDouble(i);
                                                    break;
                                                case "STRING":
                                                    sSQLInsert = sSQLInsert + objReader3.GetString(i);
                                                    break;
                                                case "SMALLINT":
                                                    sSQLInsert = sSQLInsert + objReader3.GetInt16(i);
                                                    break;
                                                default:
                                                    sSQLInsert = sSQLInsert + "'" + objReader3.GetString(i) + "'";
                                                    break;
                                            }
                                        }
                                    }
                                }
                                sSQLInsert = sSQLInsert + ")";
                                objUserCon.ExecuteNonQuery(sSQLInsert);
                                // update status every ten diaries
                                lDiaries = lDiaries + 1;
                                if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) % 10)
                                {
                                    lblTotDiaries.Text = Convert.ToString(lDiaryStart + lDiaries);
                                    // check if diary processing has been suspended
                                    System.Windows.Forms.Application.DoEvents();
                                    while (gProcessStatus == PS_SUSPEND)
                                    {
                                        System.Windows.Forms.Application.DoEvents();
                                    }
                                    // check if diary processing has been stopped
                                    if (gProcessStatus == PS_STOP)
                                    {
                                        if (CheckStop())
                                        {
                                            //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //pkandhari Jira 6412 ends
                  
                }
               
              }
            }
          //listBoxWPA.Items.Add("Routed " + lDiaries + " overdue Diary(s) to specified user(s)");
          //listBoxWPA.Refresh();
          UpdateStatus("Routed " + lDiaries + " overdue Diary(s) to specified user(s)");
            
        }

        public void InfoRun(int iSinceLast)
        {
            InfoRun(iSinceLast, 0);
        }

        public void InfoRun(int iSinceLast,int iClientId)
        {
            int j = 0;
            int iPtr = 0;
            int iStrt = 0;
            int iPtr2 = 0;
            string sSQLInsert = string.Empty;
            string sSQL = string.Empty;
            string sSQLValue = string.Empty;
            string sSQLSelect = string.Empty;
            string sSQLFrom = string.Empty;
            string sSQLWhere = string.Empty;
            string sSQLGroup = string.Empty;
            string sSQLOrder = string.Empty;
            string sTmp = string.Empty;
            string sTemp = string.Empty;
            string sDummy = string.Empty;
            string sKey = null;            
            int fidx = 0;
            string sWhereList = string.Empty;
            string sSelectList = string.Empty;
            string sFromList = string.Empty;
            string sGroupList = string.Empty;
            string sTempName = string.Empty;
            string[] tmpSQL = new string[1];           
            int lID = 0;
            int lDiaries = 0;
            int lUID = 0;
            string sAct = null;
            int lDiaryStart = 0;
            //int lDSNID = 0;            
            string sLast = string.Empty;
            string sFirst = string.Empty;
            string sRMUserID = string.Empty;
            int lUserID = 0;
            string sPriority = string.Empty;
            string sRegarding = string.Empty;
            string sGroups = string.Empty;
            int lManager_ID = 0;
            int lCase_Renamed = 0;

            string sRegardingForEmail = string.Empty;
            string sToEmailAdd = string.Empty;
            string sSenderName = string.Empty;
            string sSubject = string.Empty;
            string sBody = string.Empty;

            string sFinalstring = string.Empty;
            string[] sSplitString = null;
            int iIndex =0;

             if (sFromEmailAdd == "")
                 sFromEmailAdd = GetFromEmailAddressForAdmin();//Add & change by kuladeep for Cloud

             if (sSMTPServer == "")
                 sSMTPServer = GetSMTPServer();//Add & change by kuladeep for Cloud
            //lDSNID = objUser.DSNID;

            //sDS.Value = objLogin.SecurityDSN;
            // GetSecurityDSN(sDS)
            //sDSN = sTrimNull(sDS.Value);
            //db = ROCKETCOMLibDTGRocket_definst.DB_OpenDatabase(hEnv, sDSN, 0);

            //lDiaryStart = Conversion.Val(frmWPAProc.lblTotDiaries.Text);
            DbConnection ObjCon = null;
            ObjCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            ObjCon.Open();
            eDatabaseType m_stDataBaseType = ObjCon.DatabaseType;
            lCase_Renamed = 0;
            if (!string.IsNullOrEmpty(ObjInfoDef.tmpSQL))
            {
                sTmp = ObjInfoDef.tmpSQL;
                
                j = 0;
                do
                {
                    iPtr = string.CompareOrdinal(sTmp, "|");
                   
                    if (iPtr == -1)
                        iPtr = sTmp.Length + 1;
                    if (j > tmpSQL.GetUpperBound(0))
                        Array.Resize(ref tmpSQL, j + 1);
                   
                    tmpSQL[j] = sTmp.Substring( iPtr - 1);
                    sTmp = sTmp.Substring( iPtr + 1);
                    j = j + 1;
                } while (!(string.IsNullOrEmpty(sTmp)));
            }
            //'for Progres Notes, we have entered by ,entered on columns'
            //'name other then the standard columns, so we need to modify the constat
                if (ObjInfoDef.ID == 12) 
                {
                    sAddedByUserColumn = "ENTERED_BY_NAME";
                    sUpdatedByUserColumn = "ENTERED_BY_NAME" ;//'there is not any column in the table for updated by, so keep both feilds same
                    sDateTimeEnteredOnColumn = "DATE_CREATED";
                }
                else
                {
                    sAddedByUserColumn = "ADDED_BY_USER";
                    sUpdatedByUserColumn = "UPDATED_BY_USER";
                    sDateTimeEnteredOnColumn = "DTTM_RCD_ADDED";
                }
            sSQL = ObjInfoDef.SQL;
            iStrt = 0;
            
            iPtr = sSQL.IndexOf( "FROM");
           
             sSQLSelect = sSQL.Substring( iStrt, iPtr - 1).Trim();
            iStrt = iPtr + 1;
          
            iPtr =  sSQL.IndexOf( " WHERE ",iStrt);
            if (iPtr == -1)
                iPtr = sSQL.IndexOf(" GROUP ", iStrt);
              
            if (iPtr == -1)
                iPtr = sSQL.Length + 1;
           
            sSQLFrom = sSQL.Substring(iStrt - 1, iPtr - iStrt + 1).Trim();
          
            if (sSQL.IndexOf( " WHERE ")>0)
            {
                iStrt = iPtr + 1;
            
                iPtr =  sSQL.IndexOf( " GROUP ",iStrt);
                if (iPtr == -1)
                    iPtr = sSQL.IndexOf(" ORDER BY ", iStrt);
                  
                if (iPtr == -1)
                    iPtr = sSQL.Length + 1;
              
                sSQLWhere = sSQL.Substring(iStrt, iPtr - iStrt - 1).Trim();
            }
            else
            {
                sSQLWhere = "";
            }
            if (sSQL.IndexOf( " GROUP ")>0)
            {
                iStrt = iPtr + 1;
                
                iPtr = sSQL.IndexOf( " ORDER BY ",iStrt);
                if (iPtr == -1)
                    iPtr = sSQL.Length + 1;
              
                sSQLGroup = sSQL.Substring(iStrt, iPtr - iStrt).Trim();
            }
            else
            {
                sSQLGroup = "";
            }
            if (sSQL.IndexOf( " ORDER BY ")>0)
            {               
            
            sSQLOrder = sSQL.Substring( sSQL.IndexOf( " ORDER BY ") + 1);
            }
            else
            {
                sSQLOrder = "";
            }

            for (j = 0; j <= objInfotemp.NumFilters - 1; j++)
            {
                
                fidx = GetFilterIndex(objInfotemp.FilterSet[j].Number);
                switch (ObjInfoDef.FilterDef[fidx].FilterType)
                {
                    case 1:
                        
                        if (objInfotemp.FilterSet[j].Data.IndexOf( "<>")>0)
                        {
                            
                            if (!string.IsNullOrEmpty(ObjInfoDef.FilterDef[fidx].Name.Trim()))
                            {
                                sDummy = Utility.sGetTablesNotInSQLFrom(sSQLFrom, ObjInfoDef.FilterDef[fidx].SQLFrom.Trim());
                                if (!string.IsNullOrEmpty(sDummy))
                                {
                                    sSQLFrom = sSQLFrom + "," + sDummy;
                                }
                            }
                            // End addition by DB

                            sTmp = ObjInfoDef.FilterDef[fidx].SQLWhere;
                            do
                            {
                                sFinalstring = "";
                                iPtr = sTmp.IndexOf( "[");
                                if (iPtr == -1)
                                    break; 
                                sKey =sTmp.Substring(iPtr,  sTmp.IndexOf( "]",iPtr) + 1 - iPtr).ToUpper();
                                switch (sKey)
                                {
                                    case "[OPTION]":
                                        
                                        if (string.IsNullOrEmpty(objInfotemp.FilterSet[j].Data) || string.IsNullOrEmpty(objInfotemp.FilterSet[j].Data))
                                        {
                                           
                                           // sTmp = sTmp.Substring( sTmp.IndexOf( "{",iPtr) - 1) + sTmp.Substring(  sTmp.IndexOf( "}",iPtr) + 1);
                                            sTmp = sTmp.Replace("[OPTION]", objInfotemp.FilterSet[j].Data);
                                        }
                                        else
                                        {
                                //            'following code required to modify the option for where in query
                                //'when the 'where in' is defined for the query, we need to check if the option data is numeric or varchar
                                //'if the data is varchar(like userloginname) then we need to append single quote before and
                                //'after every option, i.e where in(csc,dtg) should be replaced by where in ('csc','dtg')
                                string sOptionString =string.Empty;
                                //sOptionString = Mid$(tmpInfoSet.FilterSet(j).Data, 2, Len(objInfotemp.FilterSet(j).Data) - 2)
                                sOptionString = objInfotemp.FilterSet[j].Data.Substring(1, objInfotemp.FilterSet[j].Data.Length - 2);
                                            Regex regExpr = new Regex("^[a-zA-Z]*$");

                                bool bIsAlphaNumeric =regExpr.IsMatch(sOptionString);
                               // bIsAlphaNumeric = checkAlpha(sOptionString)
                                            
                                if (bIsAlphaNumeric)
                                {
                                    sSplitString = sOptionString.Split(',');
                                    for (iIndex = 0; iIndex < sSplitString.Length; iIndex++)
                                    {
                                        if (string.IsNullOrEmpty(sFinalstring))
                                            sFinalstring = sFinalstring + ",'" + sSplitString[iIndex] + "'";
                                        else
                                            sFinalstring = "'" + sSplitString[iIndex] + "'";

                                    }
                                    if (sFinalstring == "")
                                        sFinalstring = sOptionString;
                                    sTmp = sTmp.Substring(iPtr - 1) + sFinalstring + sTmp.Substring(iPtr + 8);

                                }
                                else
                                    sTmp = sTmp.Substring(iPtr - 1) + objInfotemp.FilterSet[j].Data.Substring(2, objInfotemp.FilterSet[j].Data.Length - 2) + sTmp.Substring(iPtr + 8); 
                                            //Left$(sTmp, iPtr - 1) & Mid$(tmpInfoSet.FilterSet(j).Data, 2, Len(tmpInfoSet.FilterSet(j).Data) - 2) & Mid$(sTmp, iPtr + 8)   
                                            //sTmp = sTmp.Substring( iPtr - 1) + objInfotemp.FilterSet[j].Data + sTmp.Substring( iPtr + 8);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            } while (true);
                            if (!string.IsNullOrEmpty(sTmp.Trim()))
                            {
                                if (string.IsNullOrEmpty(sSQLWhere))
                                    sSQLWhere = "WHERE ";
                                else
                                    sSQLWhere = sSQLWhere + " AND ";
                                sSQLWhere = sSQLWhere + sTmp;
                            }
                        }
                        break;
                    case 2:
                    case 4:
                        
                        if (!string.IsNullOrEmpty(ObjInfoDef.FilterDef[fidx].SQLFrom))
                        {
                            sDummy = Utility.sGetTablesNotInSQLFrom(sSQLFrom, ObjInfoDef.FilterDef[fidx].SQLFrom);
                            if (!string.IsNullOrEmpty(sDummy))
                            {
                                sSQLFrom = sSQLFrom + "," + sDummy;
                            }
                        }
                        sTmp = ObjInfoDef.FilterDef[fidx].SQLWhere;
                        do
                        {
                            iPtr = sTmp.IndexOf( "[");
                            if (iPtr == -1)
                                break;
                            //srajindersin 2/5/2014 WPA in infinite loop issue
                            sKey = sTmp.Substring(iPtr, sTmp.IndexOf("]") - iPtr + 1).ToUpper();
                            
                            switch (sKey)
                            {
                                case "[OPTION]":
                                   
                                    if (string.IsNullOrEmpty(objInfotemp.FilterSet[j].Data) || objInfotemp.FilterSet[j].Data == ",")
                                    { 
                                        sTmp = sTmp.Replace("[OPTION]","");
                                    }
                                    else
                                    { 
                                        sTmp = sTmp.Substring( 0,iPtr ) + (objInfotemp.FilterSet[j].Data.Substring( 1, objInfotemp.FilterSet[j].Data.Length - 2)) + sTmp.Substring(iPtr + 8);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        } while (true);

                        if (!string.IsNullOrEmpty(sTmp.Trim()))
                        {
                            if (string.IsNullOrEmpty(sSQLWhere))
                                sSQLWhere = "WHERE ";
                            else
                                sSQLWhere = sSQLWhere + " AND ";
                            sSQLWhere = sSQLWhere + sTmp;
                        }
                        break;
                    case 3:
                    case 5:
					//jramkumar for MITS 35448
                    case 6:
                        
                        if (!string.IsNullOrEmpty(ObjInfoDef.FilterDef[fidx].SQLFrom) && !string.IsNullOrEmpty(ObjInfoDef.FilterDef[fidx].SQLFrom.Trim()))
                        {
                            sDummy = Utility.sGetTablesNotInSQLFrom(sSQLFrom, ObjInfoDef.FilterDef[fidx].SQLFrom.Trim());
                            if (!string.IsNullOrEmpty(sDummy))
                            {
                                sSQLFrom = sSQLFrom + "," + sDummy;
                            }
                        }                      
                        sTmp = ObjInfoDef.FilterDef[fidx].SQLWhere;
                        do
                        {
                            iPtr = sTmp.IndexOf( "[");
                            if (iPtr == -1)
                                break; 
                            sKey = sTmp.Substring(iPtr, sTmp.IndexOf( "]",iPtr) + 1 - iPtr).ToUpper();
                            switch (sKey)
                            {
                                case "[OPTION]":

                                    if (!string.IsNullOrEmpty(objInfotemp.FilterSet[j].Data))
                                    { 
                                       // sTmp = sTmp.Substring(sTmp.IndexOf( "{",iPtr) - 1) + sTmp.Substring( sTmp.IndexOf( "}",iPtr) + 1);
                                       // sTmp = sTmp.Substring(sTmp.IndexOf("{", iPtr) - 1) + sTmp.Substring(sTmp.IndexOf("}", iPtr) + 1);
                                        sTmp = sTmp.Replace("[OPTION]", objInfotemp.FilterSet[j].Data);
                                    }
                                    else
                                    {
                                        if (ObjInfoDef.FilterDef[fidx].FilterType == 5 & iSinceLast != 0)
                                        {
                                           
                                            if (sTmp.IndexOf( "<=") != 0)
                                            { 
                                                iPtr2 = sTmp.IndexOf( "<=",iPtr);
                                            }
                                            else
                                            {
                                                iPtr2 = sTmp.IndexOf(">=", iPtr);
                                            }
                                            if (iPtr2 > 0)
                                            {
                                                iPtr2 = 0;
                                            }
                                            else
                                            {
                                                iPtr2 = sTmp.IndexOf("=", iPtr);
                                            }
                                            if (iPtr2>0)
                                            {
                                                 sTmp = sTmp.Substring( iPtr2) + "BETWEEN " + objInfotemp.FilterSet[j].Data + " AND " + (objInfotemp.FilterSet[j].Data + iSinceLast) + sTmp.Substring( iPtr + 9);
                                            }
                                            else
                                            {
                                                sTmp = sTmp.Substring(  iPtr ) + objInfotemp.FilterSet[j].Data + sTmp.Substring(  iPtr + 9);
                                            }
                                        }
                                        else
                                        {
                                            
                                            sTmp = sTmp.Substring(  iPtr) + objInfotemp.FilterSet[j].Data +   sTmp.Substring(  iPtr + 9);
                                        }
                                    }
                                    break;
                                case "[TODAY]":
                                    
                                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                                    {
                                        sTmp =   sTmp.Substring( 0, iPtr ) + "'" + DateTime.Now.ToString("yyyyMMdd") + "'" +  sTmp.Substring(  iPtr + 7);
                                    }
                                    else
                                    {
                                        sTmp = sTmp.Substring( 0, iPtr ) + "'" + DateTime.Now.ToString("yyyyMMdd") + "'" + sTmp.Substring(  iPtr + 7);
                                    }
                                    break;
                                case "[DTG_TODAY]":
                                    sTmp = sTmp.Substring(0,  iPtr) + "'" + DateTime.Now.ToString("yyyyMMdd") + "'" + sTmp.Substring(iPtr + 11);
                                    break;
                                default:
                                    break;
                            }
                        } while (true);

                        if (!string.IsNullOrEmpty(sTmp.Trim()))
                        {
                            if (string.IsNullOrEmpty(sSQLWhere))
                                sSQLWhere = "WHERE ";
                            else
                                sSQLWhere = sSQLWhere + " AND ";
                            sSQLWhere = sSQLWhere + sTmp;
                        }
                        break;
                    default:
                        break;
                }
            }

            iPtr = sSQLSelect.IndexOf( "#TMP_");
            if (iPtr>0)
                sTempName = sSQLSelect.Substring( iPtr, sSQLSelect.IndexOf(".",iPtr)-iPtr);
                
            else
                sTempName = "";
          
             if (ObjInfoDef.tmpSQL.IndexOf( "[SELECTLIST]") != -1 && !string.IsNullOrEmpty(sSelectList) && !string.IsNullOrEmpty(sTempName))
            {

                sDummy = Utility.sGetTablesNotInSQLFrom(sSQLFrom, sTempName);
                if (!string.IsNullOrEmpty(sDummy))
                {
                    if (string.IsNullOrEmpty(sSQLFrom))
                        sSQLFrom = "FROM ";
                    else
                        sSQLFrom = sSQLFrom + ",";
                    sSQLFrom = sSQLFrom + sDummy;
                }
                

                sTmp = sSelectList;
                do
                {
                    iPtr = sSelectList.IndexOf( ",");
                    if (iPtr == -1)
                        iPtr = sTmp.Length + 1;
                   
                     sKey = sTmp.Substring( iPtr - 1);
                  
                      if (sSQLWhere.IndexOf( sTempName + sKey.Substring(sKey.IndexOf( ".")) + " = " + sKey) == 0)
                    {
                        if (string.IsNullOrEmpty(sSQLWhere))
                            sSQLWhere = "WHERE ";
                        else
                            sSQLWhere = sSQLWhere + " AND ";                       
                        sSQLWhere = sSQLWhere + sTempName + sKey.Substring( sKey.IndexOf(".")) + " = " + sKey;
                    }
                    sTmp =sTmp.Substring( iPtr + 1);
                } while (!(string.IsNullOrEmpty(sTmp)));
            }

            iStrt = 1;
            do
            {              
                  iPtr =  sSQLSelect.IndexOf( "#TMP_",iStrt);
                if (iPtr == -1)
                    break; 
                iPtr2 = sSQLSelect.IndexOf( ",",iPtr) - iPtr;
                if (iPtr2 > 0)
                {
                   
                     sTmp = sSQLSelect.Substring( iPtr, iPtr2);
                    if (string.IsNullOrEmpty(sSQLGroup))
                        sSQLGroup = "GROUP BY ";
                    else
                        sSQLGroup = sSQLGroup + ",";
                    sSQLGroup = sSQLGroup + sTmp;
                }
                iStrt = iPtr + 1;
            } while (true);

            //sSQLSelect = sSQLSelect + "," + ObjInfoDef.ParentTable + ".ADDED_BY_USER," + ObjInfoDef.ParentTable + ".UPDATED_BY_USER";
            //sSQLWhere = sSQLWhere + " AND NOT EXISTS (SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE AUTO_ID = " + objInfotemp.RowID + " AND ATTACH_RECORDID = " + ObjInfoDef.AttachTable + "." + ObjInfoDef.AttachCol + ")";
            //if (!string.IsNullOrEmpty(objInfotemp.ProcessDate))
            //    sSQLWhere = sSQLWhere + " AND " + ObjInfoDef.ParentTable + ".DTTM_RCD_ADDED >= '" + objInfotemp.ProcessDate + "000000'";

    //        'rupal:start , r8 auto diary enh
    //'The Added_By_user,Updated_By_user coulumns were hardcoded. there are tables which
    //'don't have the columns with same names, so we need to identify those tables and put the appropriate cloumns
    //'in the variables sAddedByUserColumn, sUpdatedByUserColumn, and sDateTimeEnteredOnColumn
    sSQLSelect = sSQLSelect + "," + ObjInfoDef.ParentTable + "." + sAddedByUserColumn + "," + ObjInfoDef.ParentTable + "." + sUpdatedByUserColumn;
    sSQLWhere = sSQLWhere + " AND NOT EXISTS (SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE AUTO_ID = " + objInfotemp.RowID + " AND ATTACH_RECORDID = " + ObjInfoDef.AttachTable + "." + ObjInfoDef.AttachCol + ")";
    if (!string.IsNullOrEmpty(objInfotemp.ProcessDate))
        sSQLWhere = sSQLWhere + " AND " + ObjInfoDef.ParentTable + "." + sDateTimeEnteredOnColumn + ">= '" + objInfotemp.ProcessDate + "000000'";
    //'rupal:end
            sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;            
            sSQL = sSQL.Replace( "{", "");
            sSQL = sSQL.Replace( "}", "");
            //if (bIsODBC)
            //    sSQL = sSQL.Replace( " AS ", " "); //uncomment
                //sSQL = ReplaceStr(sSQL, " AS ", " ");          
            do
            {
                iPtr = sSQL.IndexOf( "[",iPtr+1);
                if (iPtr == -1)
                    break; 
                 sKey = sSQL.Substring( iPtr, sSQL.IndexOf( "]",iPtr) + 1 - iPtr).ToUpper();
                switch (sKey)
                {
                    case "[CURRENT_USER]" :
                         sSQL = sSQL.Substring(0, iPtr ) + sSQL.Substring(iPtr + 14);
                        break;
                    case "[TODAY]":
                        sSQL = sSQL.Substring(0, iPtr) + "'" + DateTime.Now.ToString("MM/dd/yyyy") + "'" + sSQL.Substring(iPtr + 7);
                        
                        break;
                    case "[DTG_TODAY]":
                        sSQL = sSQL.Substring( 0,iPtr) + "'" + DateTime.Now.ToString("yyyyMMdd") + "'" + sSQL.Substring(iPtr + 11);
                        
                        break;
                    default:
                        if (sKey.IndexOf( "|")>0)
                        {                            
                            sKey = sKey.Substring( 2, sKey.Length - 2);
                            sTmp = sKey.Substring(sKey.IndexOf("|") + 1);                           
                            sTempName = sKey.Substring(sKey.IndexOf( "|" )- 1);                            
                            if (sSQL.IndexOf( sTempName) == 0)
                                sTmp = "";                           
                            sSQL = sSQL.Substring(0,iPtr ) + sTmp + sSQL.Substring(iPtr + sKey.Length + 2);
                        }
                        break;
                }
            } while (true);
           
            if (m_stDataBaseType == eDatabaseType.DBMS_IS_ACCESS || m_stDataBaseType == eDatabaseType.DBMS_IS_INFORMIX || m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE || m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                sSQL = sSQL.Replace( "#TMP_", "TMP_");

            for (j = 0; j <= tmpSQL.GetUpperBound(0); j++)
            {
                if (!string.IsNullOrEmpty(tmpSQL[j]))
                {
                    do
                    {
                        iPtr = tmpSQL[j].IndexOf( "[");
                        if (iPtr == -1)
                            break; 
                        sKey = tmpSQL[j].Substring(iPtr, tmpSQL[j].IndexOf("]", iPtr + 1 - iPtr)).ToUpper();
                        switch (sKey)
                        {
                            case "[SELECTLIST]":
                                if (string.IsNullOrEmpty(sSelectList))
                                {
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) + sSelectList + tmpSQL[j].Substring(iPtr + 12);
                                }
                                else
                                {
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) +","+ sFromList + tmpSQL[j].Substring(iPtr + 12);
                                }
                                break;
                            case "[FROMLIST]":
                                if (string.IsNullOrEmpty(sFromList))
                                {
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) + sFromList + tmpSQL[j].Substring(iPtr + 10);
                                }
                                else
                                {
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) + "," + tmpSQL[j].Substring(iPtr + 10);
                                }
                                break;
                            case "[WHERELIST]":
                                if (string.IsNullOrEmpty(sWhereList))
                                {                                   
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1)  + sWhereList + tmpSQL[j].Substring(iPtr + 11);
                                }
                                else
                                {
                                    
                                    if (tmpSQL[j].IndexOf( " WHERE ")>0)
                                    {                                        
                                        tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) + " AND " + sWhereList + tmpSQL[j].Substring(iPtr + 11);
                                    }
                                    else
                                    {                                       
                                        tmpSQL[j] = tmpSQL[j].Substring( iPtr - 1) + " WHERE " + sWhereList + tmpSQL[j].Substring( iPtr + 11);
                                    }
                                }
                                break;
                            case "[GROUPLIST]":
                                if (string.IsNullOrEmpty(sGroupList))
                                {                                  
                                    tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) +  tmpSQL[j].Substring(iPtr + 11);
                                }
                                else
                                {
                                   // if (Strings.InStr(tmpSQL[j], " GROUP BY "))
                                    if (tmpSQL[j].IndexOf(" GROUP BY ")>0)
                                    {                                      
                                        tmpSQL[j] = tmpSQL[j].Substring( iPtr - 1) + "," + sGroupList + tmpSQL[j].Substring( iPtr + 11);
                                    }
                                    else
                                    {                                       
                                        tmpSQL[j] = tmpSQL[j].Substring(iPtr - 1) + "GROUP BY" + sGroupList + tmpSQL[j].Substring(iPtr + 11);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    } while (true);
                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_ACCESS || m_stDataBaseType == eDatabaseType.DBMS_IS_INFORMIX || m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE || m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                    {
                        do
                        {
                           iPtr = tmpSQL[j].IndexOf( "#TMP_");
                            if (iPtr == -1)
                                break; 
                            tmpSQL[j] = tmpSQL[j].Substring( iPtr - 1) + "TMP_" + tmpSQL[j].Substring(iPtr + 5);
                        } while (false);
                    }
                    
                    ObjCon.ExecuteNonQuery(tmpSQL[j]);
                    
                }
            }
            // run any non-direct sql procedures
            for (j = 0; j <= objInfotemp.NumFilters - 1; j++)
            {                
                fidx = GetFilterIndex(objInfotemp.FilterSet[j].Number);
                if (ObjInfoDef.FilterDef[fidx].Procedure != null && !string.IsNullOrEmpty(ObjInfoDef.FilterDef[fidx].Procedure.Trim()))
                {
                    switch (ObjInfoDef.FilterDef[fidx].Procedure.Trim().ToUpper())
                    {
                        case "CALCLOSTDAYS":
                            UpdateStatus("     " + ObjInfoDef.FilterDef[fidx].ProcMsg);
                            LostWork objLostWork = new LostWork();
                            objLostWork.CalcLostDays();
                            break;
                        default:
                            break;
                        
                    }
                }
            }

           
         using(   DbReader objReader = ObjCon.ExecuteReader(sSQL))
         {
            while (objReader.Read())            
            {               
                lID = objReader.GetInt(ObjInfoDef.AttachCol);
                //create auto diaries
                sKey = "";
                sTmp = objInfotemp.SendUsers;
                if (objInfotemp.SendCreate>0)
                {
                   
                   // sKey = objReader.GetString( "ADDED_BY_USER") + "";
                    sKey = objReader.GetString(sAddedByUserColumn) + "";
                    //jramkumar for MITS 35448
                    if (!string.IsNullOrEmpty(sTmp))
                    {
                        if (sTmp.IndexOf(sKey + ",") == -1) //jramkumar for MITS 35095
                            sTmp = sTmp + sKey + ",";
                    }
                    else
                    {
                        sTmp = sKey + ",";
                    }
                }
                if (objInfotemp.SendUpdate>0)
                {
                   
                  //  sKey = objReader.GetString("UPDATED_BY_USER") + "";
                    sKey = objReader.GetString(sUpdatedByUserColumn) + "";
                    //jramkumar for MITS 35448
                    if (!string.IsNullOrEmpty(sTmp))
                    {
                        if (sTmp.IndexOf(sKey + ",") == -1) //jramkumar for MITS 35095
                            sTmp = sTmp + sKey + ",";
                    }
                    else
                    {
                        sTmp = sKey + ",";
                    }
                }
                
                //Added Supervisor-As the Diary will be Sent to Supervisor also.
                DbReader objReader2 = null;
                 if( objInfotemp.SendAdj != 0 && ObjInfoDef.AttachTable == "FUNDS" )
                {
                    lCase_Renamed = 1;
                    objReader2 = ObjCon.ExecuteReader("SELECT ENTITY.RM_USER_ID FROM CLAIM_ADJUSTER,ENTITY,FUNDS,CLAIM WHERE CLAIM_ADJUSTER.CLAIM_ID = CLAIM.CLAIM_ID AND FUNDS.CLAIM_ID=CLAIM.CLAIM_ID AND FUNDS.TRANS_ID=" + objReader.GetInt32(ObjInfoDef.AttachCol) + " AND CURRENT_ADJ_FLAG <> 0 AND ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID");           
                }
                else if ((objInfotemp.SendAdj != 0 || objInfotemp.SendAdjSupervisor != 0) && ObjInfoDef.AttachTable.ToUpper() == "CLAIM")//jramkumar for MITS 22541
                {                   
                    lCase_Renamed = 1;                    
                     objReader2 = ObjCon.ExecuteReader("SELECT ENTITY.RM_USER_ID FROM CLAIM_ADJUSTER, ENTITY WHERE CLAIM_ADJUSTER.CLAIM_ID = " + objReader.GetInt32(ObjInfoDef.AttachCol) + " AND CURRENT_ADJ_FLAG <> 0 AND ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID");
                }
                else if (objInfotemp.SendSuppField != 0 & !string.IsNullOrEmpty(objInfotemp.SendSuppFieldName.Trim()) && ObjInfoDef.AttachTable.ToUpper() == "EVENT")
                {                   
                     objReader2 = ObjCon.ExecuteReader("SELECT ENTITY.FIRST_NAME, ENTITY.LAST_NAME FROM EVENT_SUPP, ENTITY WHERE EVENT_SUPP.EVENT_ID = " + objReader.GetString(ObjInfoDef.AttachCol) + " AND ENTITY.ENTITY_ID = " + objInfotemp.SendSuppFieldName);
                  
                }
                //jramkumar for MITS 22541
                else if ((objInfotemp.SendAdj != 0 || objInfotemp.SendAdjSupervisor != 0) && ObjInfoDef.AttachTable.ToUpper() == "CLAIM_X_LITIGATION")
                {
                    lCase_Renamed = 1;
                    objReader2 = ObjCon.ExecuteReader("SELECT ENTITY.RM_USER_ID FROM CLAIM_ADJUSTER,CLAIM_X_LITIGATION,ENTITY WHERE CLAIM_X_LITIGATION.LITIGATION_ROW_ID = " + objReader.GetInt32(ObjInfoDef.AttachCol) + " AND CLAIM_X_LITIGATION.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID AND CURRENT_ADJ_FLAG <> 0 AND ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID");
                }
                //else if (objInfotemp.SendAdj != 0 && ObjInfoDef.AttachTable.ToUpper() == "FUNDS")
                //{
                //    lCase_Renamed = 1;                    
                //     objReader2 = ObjCon.ExecuteReader("SELECT ENTITY.RM_USER_ID FROM CLAIM_ADJUSTER,ENTITY,FUNDS,CLAIM WHERE CLAIM_ADJUSTER.CLAIM_ID = CLAIM.CLAIM_ID AND FUNDS.CLAIM_ID=CLAIM.CLAIM_ID AND FUNDS.TRANS_ID=" + objReader.GetString(ObjInfoDef.AttachCol) + " AND CURRENT_ADJ_FLAG <> 0 AND ENTITY.ENTITY_ID = CLAIM_ADJUSTER.ADJUSTER_EID");
                //}
               
                if (objReader2 != null)
                {
                    if (objReader2.Read())
                    {
                        if (lCase_Renamed != 1)
                        {

                            sFirst = objReader2.GetString(0);

                            sLast = objReader2.GetString(1);
                        }
                        else
                        {
                            sRMUserID = objReader2.GetInt32(0).ToString();//jramkumar for MITS 22541
                        }
                    }
                    else
                    {
                        sFirst = "";
                        sLast = "";
                        sRMUserID = "";

                    }
                }
                
                if(objReader2!= null)
                    objReader2.Dispose();
                 
                    //Getting ManagerID
                    if (lCase_Renamed != 1)
                    {
                        sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.MANAGER_ID FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.FIRST_NAME = '" + sFirst.Replace( "'", "''") + "' AND USER_TABLE.LAST_NAME = '" + sLast.Replace( "'", "''") + "' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID = " + lDSNID;
                    }
                    else
                    {
                        sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.MANAGER_ID FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = '" + sRMUserID.Replace( "'", "''") + "' AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID = " + lDSNID;
                    }
                    DbConnection ObjSecCon = DbFactory.GetDbConnection(Globalvar.m_sConnectionSecurity);
                    ObjSecCon.Open();
                    objReader2 = ObjSecCon.ExecuteReader(sSQL);
                    if (objReader2 != null)
                    {
                        if (objReader2.Read())
                        {
                            sKey = objReader2.GetString(0);

                            lManager_ID = objReader2.GetInt("MANAGER_ID");
                            //Checking for Adjuster
                            if (objInfotemp.SendAdj != 0)
                            {
                                // if (Strings.InStr(sTmp, sKey + ",") == 0)
                                //jramkumar for MITS 35448
                                if (!string.IsNullOrEmpty(sTmp))
                                {
                                    if (sTmp.IndexOf(sKey + ",") == -1) //jramkumar for MITS 35095
                                        sTmp = sTmp + sKey + ",";
                                }
                                else
                                {
                                    sTmp = sKey + ",";
                                }
                            }

                            //Checking for Supervisor
                            if (objInfotemp.SendAdjSupervisor != 0 && lManager_ID != 0)
                            {
                                sKey = Global.sGetUser( lDSNID, lManager_ID);
                                //if (Strings.InStr(sTmp, sKey + ",") == 0)
                                //jramkumar for MITS 35448
                                if (!string.IsNullOrEmpty(sTmp))
                                {
                                    if (sTmp.IndexOf(sKey + ",") == -1) //jramkumar for MITS 35095
                                        sTmp = sTmp + sKey + ",";
                                }
                                else
                                {
                                    sTmp = sKey + ",";
                                }
                            }
                            //End Naresh MITS 8349 06/11/2006
                        }
                        if (objReader2 != null)
                            objReader2.Dispose();
                    }
               // }
                //Mits 33843 
                int attRecord = objReader.GetInt32(ObjInfoDef.AttachCol);
                int parent = ParentRecord((string)ObjInfoDef.AttachTable, attRecord,Globalvar.m_iClientId);
                //build static part of insert statement
                // Added INTO to INSERT by Denis Basaric 24/10/1997
                sSQLInsert = "INSERT INTO WPA_DIARY_ENTRY (ATT_PARENT_CODE,ENTRY_NAME,CREATE_DATE,COMPLETE_TIME,COMPLETE_DATE,";
                sSQLInsert = sSQLInsert + "PRIORITY,STATUS_OPEN,AUTO_CONFIRM,IS_ATTACHED,ATTACH_TABLE,";
                //sSQLInsert = sSQLInsert + "ATT_FORM_CODE,ATT_SEC_REC_ID,ATT_PARENT_CODE,DIARY_VOID,DIARY_DELETED,";
                sSQLInsert = sSQLInsert + "ATT_FORM_CODE,ATT_SEC_REC_ID,DIARY_VOID,DIARY_DELETED,";
                sSQLInsert = sSQLInsert + "ASSIGNING_USER,NOTIFY_FLAG,ROUTE_FLAG,ESTIMATE_TIME,AUTO_ID,TE_TRACKED,TE_START_TIME,";
                sSQLInsert = sSQLInsert + "TE_END_TIME,TE_TOTAL_HOURS,TE_EXPENSES,ATT_FORM_SCRTY,REGARDING,ENTRY_ID,ATTACH_RECORDID,ASSIGNED_USER,ASSIGNED_GROUP,ENTRY_NOTES)";
                sSQLInsert = sSQLInsert + " VALUES (" + parent + "," + Utility.sSQLTextArg(objInfotemp.Name) + ",'" + DateTime.Now.ToString("yyyyMMddHHmmss") + "',";
                sSQLInsert = sSQLInsert + "'235959','";
                //& Format$(DateAdd("d", 1, Now), "YYYYMMDD") & "'," & tmpInfoSet.Priority & ",1,0,1,'"
                //vd 04/03/02
                if (objInfotemp.iDueDays > 0)
                {
                    //sSQLInsert = sSQLInsert + VB6.Format(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, objInfotemp.iDueDays, DateTime.Now), "YYYYMMDD");
                    sSQLInsert = sSQLInsert + DateTime.Now.AddDays(objInfotemp.iDueDays).ToString( "yyyyMMdd");
                   
                }
                else if (objInfotemp.iDueDays == -1)
                {
                    
                    sSQLInsert = sSQLInsert + DateTime.Now.ToString("yyyyMMdd");
                }
                else
                {
                   // if (gbSameDayDueDate) //change 
                    if (true) //change this
                    {
                        
                        sSQLInsert = sSQLInsert + DateTime.Now.ToString("yyyyMMdd");
                    }
                    else
                    {
                       // sSQLInsert = sSQLInsert + VB6.Format(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, DateAndTime.Now), "YYYYMMDD");
                        sSQLInsert = sSQLInsert + DateTime.Now.AddDays(1).ToString( "yyyyMMdd");
                    }
                }
                sSQLInsert = sSQLInsert + "'," + objInfotemp.Priority + ",1,0,1,'";
                sSQLInsert = sSQLInsert + ObjInfoDef.AttachTable + "'," + ObjInfoDef.AttFormCode + ",";
                if (string.IsNullOrEmpty(ObjInfoDef.AttSecRecID.Trim()))
                {
                    sKey = "0";
                }
                else
                {                    
                    sKey =Convert.ToString( objReader.GetInt32( ObjInfoDef.AttSecRecID));
                }
                sSQLInsert = sSQLInsert + sKey + ",0,0,'SYSTEM',0,0,";
                if (objInfotemp.TaskEstType >0)
                {
                    sSQLInsert = sSQLInsert + objInfotemp.TaskEst + ",";
                    //hours
                }
                else
                {
                    sSQLInsert = sSQLInsert + objInfotemp.TaskEst / 60 + ",";
                    //minutes
                }

                // Double Qoute Inside INSERT replaced with NULL 
                sSQLInsert = sSQLInsert + objInfotemp.RowID + ",0,NULL,NULL,0,0,0";

                if (string.IsNullOrEmpty(ObjInfoDef.SQLRegard.Trim()))
                {
                    sSQLInsert = sSQLInsert + ",NULL";
                }
                else
                {
                   
                    iPtr = ObjInfoDef.SQLRegard.IndexOf( "[ROW_ID]");
                    if (iPtr > 0)
                    {
                       // sSQL = ObjInfoDef.SQLRegard.Substring(0, iPtr) + objReader.GetInt32(ObjInfoDef.AttachCol) + ObjInfoDef.SQLRegard.Substring(iPtr + 8);
                        sSQL = ObjInfoDef.SQLRegard;
                        while (iPtr > 0)
                        {
                            if (iPtr > 0)
                                sSQL = sSQL.Substring(0,iPtr - 1) + objReader.GetInt32(ObjInfoDef.AttachCol) + sSQL.Substring(iPtr + 8);
                            
                            iPtr = sSQL.IndexOf( "[ROW_ID]");
                        }
                        using (objReader2 = ObjCon.ExecuteReader(sSQL))
                        {
                            if (objReader2.Read())
                            {
                                sSQLInsert = sSQLInsert + ",NULL";
                                sRegarding = "RISKMASTER";
                            }
                            else
                            {
                                sTemp = "";
                                for (j = 0; j <= objReader2.Depth - 1; j++)
                                {
                                    if (!string.IsNullOrEmpty(sTemp))
                                        sTemp = sTemp + " * ";

                                    sTemp = sTemp + objReader2.GetString(j) + "";
                                }
                                //'we dont want sRegarding to be trimmed to size less than 70 in case of email
                                sRegardingForEmail = sTemp;
                                if (sTemp.Length > 68)
                                    sTemp = sTemp.Substring(68);
                                sSQLInsert = sSQLInsert + "," + Utility.sSQLTextArg(sTemp);
                                sRegarding = sTemp;
                            }
                            if(objReader2!= null)
                                objReader2.Dispose();
                        }
                    }
                }
                //__________________________________________________________insert groups
                if (!string.IsNullOrEmpty(objInfotemp.SendGroups))
                {
                    sGroups = objInfotemp.SendGroups;
                    //create diary for each selected group
                    while (!string.IsNullOrEmpty(sGroups))
                    {                        
                        iPtr = sGroups.IndexOf( ",");
                        sKey = sGroups.Substring(0, iPtr); //pkandhari Jira 6412
                        sGroups = sGroups.Substring(iPtr + 1);
                        lUID = Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId); //lGetNextUID("WPA_DIARY_ENTRY");                  
                        sSQL = sSQLInsert + "," + lUID + "," + objReader.GetValue(ObjInfoDef.AttachCol) + ",NULL,'" + sKey + "'," + Utility.sSQLTextArg(objInfotemp.Instruct) + ")"; //pkandhari Jira 6412
                       // ROCKETCOMLibDTGRocket_definst.DB_SQLExecute(dbCreate, sSQL);
                        ObjCon.ExecuteNonQuery(sSQL);
                        lDiaries = lDiaries + 1;
                        if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) / 10)
                        {
                           lblTotDiaries.Text = Convert.ToString(lDiaryStart + lDiaries);
                            System.Windows.Forms.Application.DoEvents();

                            while (gProcessStatus == PS_SUSPEND)
                            {
                                System.Windows.Forms.Application.DoEvents();
                            }

                            // check if diary processing has been stopped
                            if (gProcessStatus == PS_STOP)
                            {
                                if (CheckStop())
                                {
                                    //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                    return;
                                }
                            }
                            
                        }
                    }
                }
                //_________________________________________________________________________________________
                //create an auto diary for each user selected
                while (!string.IsNullOrEmpty(sTmp))
                {
                    
                    iPtr = sTmp.IndexOf( ",");
                    sKey = sTmp.Substring(0, iPtr );                    
                    sTmp = sTmp.Substring( iPtr + 1);
                    // get next unique id for WPA_DIARY_ENTRY table
                    lUID = Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId); //lGetNextUID("WPA_DIARY_ENTRY"); //uncomment after adding vb dll
                    // add diary specific data to insert statement                    
                    sSQL = sSQLInsert + "," + lUID + "," + objReader.GetInt32(ObjInfoDef.AttachCol) + ",'" + sKey + "','NA'," +Utility.sSQLTextArg(objInfotemp.Instruct) + ")";

                  
                    ObjCon.ExecuteNonQuery(sSQL); //sai

                    
                    //if (iGlobalOLEMailType != 0 & objInfotemp.ExpSchedule)
                    if ( objInfotemp.ExpSchedule >0)
                    {
                        sSQL = "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + sKey + "' AND DSNID = " + lDSNID;
                       // rs2 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(db, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                        // objReader2 = ObjCon.ExecuteReader(sSQL); //Bkuzhanthaim - MITS 36699 : WPA_AUTO_SET table always get inserted with '0',If inserted '1' WPA Processing is crashing.
                        objReader2 = ObjSecCon.ExecuteReader(sSQL);
                        if (objReader2.Read())
                        {
                            lUserID = objReader2.GetInt(0);//GetInt( 1); Bkuzhanthaim - MITS 36699 
                        }
                        else
                        {
                            lUserID = 0;
                        }
                        
                        objReader2.Close();

                        if (lUserID > 0)
                        {
                            sPriority = objInfotemp.Priority + "";                          
                            if (Convert.ToInt32(sPriority) == 0)
                                sPriority = "1";
                         
                        }
                    }

            //        '**********************************************8
            //'rupal:start, R8 Auto Diary Enhancement for Export To Email
            //'*********************************************************
            
            if(objInfotemp.ExpEmail >0 ) 
            {
                sToEmailAdd = MAPIVB.sGetEmailByLoginName(sKey);
               // 'let the error be logged in bExportToEmail function if either sFromEmailAdd or sToEmailAdd is invalid
               //' If sFromEmailAdd = "" Or sToEmailAdd = "" Then
                                    
               // 'Else
                    sSenderName = "Admin Auto Diary";
                    sSubject = "Auto Diary Created : " + objInfotemp.Name + " * " + sRegarding;
                    sBody = "Auto diary has been created with following detail." 
                             + Constants.VBCrLf + Constants.VBCrLf + "Task Name - " + objInfotemp.Name 
                             + Constants.VBCrLf + "Attached Record - " + sRegardingForEmail 
                             + Constants.VBCrLf + "Notes - " + objInfotemp.Instruct 
                             + Constants.VBCrLf + Constants.VBCrLf + Constants.VBCrLf + "Thank You, " 
                             + Constants.VBCrLf + "RISKMASTER X";
                  // 'Call bSendEmail(sSenderName, sFromEmailAdd, sToEmailAdd, sSubject, sBody);
                    bExportToEmail(sSenderName, sFromEmailAdd, sToEmailAdd, sSubject, sBody, sKey);
                
            }
            //'*********************************************************
            //'rupal:end
                    //**********************************************8

                    //create activity record(s) for each diary created
                    sAct = objInfotemp.Activities;
                    while (!string.IsNullOrEmpty(sAct))
                    {
                        iPtr = sAct.IndexOf( ",");
                        sKey = sAct.Substring(0, iPtr);
                        sAct = sAct.Substring( iPtr + 1);
                        // Added INTO to INSERT by Denis Basaric 24/10/1997
                        sSQL = "INSERT INTO WPA_DIARY_ACT (ACTIVITY_ID,PARENT_ENTRY_ID,ACT_CODE,ACT_TEXT) VALUES (";                        
                        sSQL = sSQL + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ACT", WPAForm.Globalvar.m_iClientId) + "," + lUID + "," + sKey.Substring(0, sKey.IndexOf("|")) + ",";
                        sSQL = sSQL + Utility.sSQLTextArg(sKey.Substring(sKey.IndexOf("|") + 1)) + ")";                       
                        ObjCon.ExecuteNonQuery(sSQL); 
                    }

                    // update status every ten diaries
                    lDiaries = lDiaries + 1;
                    if ((lDiaryStart + lDiaries) / 10 == (lDiaryStart + lDiaries) / 10)
                    {                       
                        lblTotDiaries.Text =Convert.ToString( lDiaryStart + lDiaries);
                        // check if diary processing has been suspended
                        System.Windows.Forms.Application.DoEvents();
                        while (gProcessStatus == PS_SUSPEND)
                        {
                            System.Windows.Forms.Application.DoEvents();
                        }

                        // check if diary processing has been stopped
                        if (gProcessStatus == PS_STOP)
                        {
                            if (CheckStop())
                            {
                                //ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                                return;
                            }
                        }
                        
                    }
                }              
            } 
        }
            //objReader.Dispose();
            //listBoxWPA.Items.Add("Generated " + lDiaries + " Auto Diary(s)");
            //listBoxWPA.Refresh();
         UpdateStatus("Generated " + lDiaries + " Auto Diary(s)");

        }
       
       
       
        public int GetFilterIndex(int Index)
        {
            int functionReturnValue = 0;
            //  for (int i = 0; i <= objInfotemp.FilterSet.Count() - 1; i++)
            for (int i = 0; i <= ObjInfoDef.FilterDef.Count() - 1; i++)
            {                 
                if (Index == ObjInfoDef.FilterDef[i].ID)
                {
                    // functionReturnValue = i;
                    return i;
                }
            }
            return functionReturnValue;
        }
            public void LoadInfoDef( int ID, string sObjInfoDefName)
            {
              
                string sRec = null;
                int iPtr = 0;
                string sKeyword = null;
                string sValue = null;
                int InfoNum = 0;
                int FilterNum = 0;
                string sTmp = null;
                int iDBType = 0;
                bool bEnd = false;
                
                ObjInfoDef.Name = "";
                ObjInfoDef.ID = 0;
                ObjInfoDef.ParentTable = "";
                ObjInfoDef.AttachTable = "";
                ObjInfoDef.AttachCol = "";
                ObjInfoDef.AttachDesc = "";
                ObjInfoDef.AttFormCode = 0;
                ObjInfoDef.AttSecRecID = "";
                ObjInfoDef.tmpSQL = "";
                ObjInfoDef.SQL = "";
                ObjInfoDef.SQLRegard = "";
                ObjInfoDef.NumFilters = 0;
                ObjInfoDef.FilterDef.Clear();              
                InfoNum = -1;
                string path = Application.StartupPath +@"\wpainfo.def" ;               
                FileStream fileIn = File.Open(path, FileMode.Open, FileAccess.Read);
   
                if (fileIn.CanRead)
                {
                    fileIn.Close();
                    List<string> sRecords = File.ReadLines(path).ToList<string>();
                    var res = sRecords.Where(a => a.ToUpper() == "[" + sObjInfoDefName.ToUpper() + "]").FirstOrDefault();

                    if (res != null)
                    {
                        if (res.ToString().ToUpper() == "[" + sObjInfoDefName.ToUpper() + "]")
                        {
                            FilterNum = -1;
                            InfoNum = InfoNum + 1;
                            ObjInfoDef.Name = sObjInfoDefName;
                        }
                    }
                }
                

                    List<string> sCollRec = new List<string>();
                    sCollRec = File.ReadLines(path).ToList<string>();
                    foreach (string str in sCollRec)
                    {
                        sRec = str.Trim();
                        if (!string.IsNullOrEmpty(sRec))
                        {
                            if (sRec.Substring(0,1) == "[")
                            {
                                sKeyword = sRec.Substring(1, sRec.Length-2).Trim();
                                if (sKeyword.ToUpper() == "FILTER")
                                {
                                    FilterNum = FilterNum + 1;
                                    ObjInfoDef.NumFilters = ObjInfoDef.NumFilters + 1;
                                }
                                else
                                {
                                    if (bEnd)
                                        return;
                                    FilterNum = -1;
                                    InfoNum = InfoNum + 1;
                                    ObjInfoDef.Name = sKeyword;
                                }
                            }
                            else
                            {
                                iPtr = sRec.IndexOf("=");
                                if (sRec.Substring(0,2) == "//" || iPtr == -1)
                                    iPtr = 1;
                                sKeyword = sRec.Substring(0,iPtr).ToUpper().Trim();
                                sValue = sRec.Substring(iPtr + 1).Trim();
                                
                                if ((!sKeyword.Equals("ID")) && bEnd == false)
                                    continue;
                                
                                switch (sKeyword)
                                {
                                    case "NAME":
                                        ObjInfoDef.FilterDef[FilterNum].Name = sValue;
                                        break;
                                    case "ID":
                                        if (FilterNum == -1)
                                        {
                                            if (ID == Conversion.ConvertStrToInteger(sValue))
                                                bEnd = true;
                                            ObjInfoDef.ID = Conversion.ConvertStrToInteger(sValue);
                                        }
                                        else if (bEnd == true)
                                        {

                                            FilterDefinition objFiter = new FilterDefinition();
                                          //  ObjInfoDef.FilterDef[FilterNum+1] = objFiter;
                                           // FilterDefinition[ObjInfoDef.NumFilters] obj = new FilterDefinition();
                                            ObjInfoDef.FilterDef.Add(objFiter);
                                            
                                          //  ObjInfoDef.FilterDef[ObjInfoDef.NumFilters-1].ID = Conversion.ConvertStrToInteger(sValue);
                                            ObjInfoDef.FilterDef[FilterNum].ID = Conversion.ConvertStrToInteger(sValue);
                                        }
                                        break;
                                    case "ATTACH_TABLE":
                                        ObjInfoDef.AttachTable = sValue;
                                        break;
                                    case "PARENT_TABLE":
                                        ObjInfoDef.ParentTable = sValue;
                                        break;
                                    case "ATTACH_COLUMN":
                                        ObjInfoDef.AttachCol = sValue;
                                        break;
                                    case "ATTACH_DESC":
                                        ObjInfoDef.AttachDesc = sValue;
                                        break;
                                    case "ATTACH_SECREC_ID":
                                        ObjInfoDef.AttSecRecID = sValue;
                                        break;
                                    case "ATTACH_FORM_CODE":
                                        ObjInfoDef.AttFormCode = Conversion.ConvertStrToInteger(sValue);
                                        break;
                                    case "TMPSQL":
                                        if (sValue.Substring(1) == "[")
                                        {
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(2, iPtr - 2);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.tmpSQL = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.tmpSQL = sValue;
                                        }
                                        break;
                                    case "SQL":
                                        if (sValue.Substring(1) == "[")
                                        {
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(2, iPtr - 2);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.SQL = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.SQL = sValue;
                                        }
                                        break;
                                    case "SQLREGARD":
                                        if (sValue.Substring(0,1) == "[")
                                        {
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(1, iPtr - 1);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.SQLRegard = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.SQLRegard = sValue;
                                        }
                                        break;
                                    case "TYPE":
                                        switch (sValue.ToUpper())
                                        {
                                            case "CHECKBOX":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 1;
                                                break;
                                            case "LIST":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 2;
                                                break;
                                            case "TEXT":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 3;
                                                break;
                                            case "COMBO":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 4;
                                                break;
                                            case "DURATION":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 5;
                                                break;
											//jramkumar for MITS 35448
                                            case "NO_OPTION":
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 6;
                                                break;
                                            default:
                                                ObjInfoDef.FilterDef[FilterNum].FilterType = 0;
                                                break;
                                        }
                                        break;
                                    case "MIN":
                                        switch (sValue)
                                        {
                                            case "CURRENT_YEAR":
                                                ObjInfoDef.FilterDef[FilterNum].FilterMin = DateTime.Now.Year;
                                                break;
                                            default:
                                                ObjInfoDef.FilterDef[FilterNum].FilterMin = Conversion.ConvertStrToInteger(sValue);
                                                break;
                                        }
                                        break;
                                    case "MAX":
                                        ObjInfoDef.FilterDef[FilterNum].FilterMax = Conversion.ConvertStrToLong(sValue);
                                        break;
                                    case "DEFAULT":
                                        switch (sValue)
                                        {
                                            case "CURRENT_YEAR":                                                
                                                ObjInfoDef.FilterDef[FilterNum].DefValue = DateTime.Now.Year.ToString();
                                                break;
                                            default:                                                
                                                ObjInfoDef.FilterDef[FilterNum].DefValue = Convert.ToString(sValue);
                                                break;
                                        }
                                        break;
                                    case "TABLE":
                                        ObjInfoDef.FilterDef[FilterNum].table = sValue;
                                        break;
                                    case "SQLFILL":
                                        if (sValue.Substring(0,1) == "[")
                                        {
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(2, iPtr - 2);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.FilterDef[FilterNum].SQLFill = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.FilterDef[FilterNum].SQLFill = sValue;
                                        }
                                        break;
                                    case "SQLFROM":
                                        if (sValue.Substring(0,1) == "[")
                                        {
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(2, iPtr - 2);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.FilterDef[FilterNum].SQLFrom = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.FilterDef[FilterNum].SQLFrom = sValue;
                                        }
                                        break;
                                    case "SQLWHERE":
                                        if (sValue.Substring(0,1) == "[")
                                        {                                            
                                            iPtr = sValue.IndexOf("]", 2);
                                            sTmp = sValue.Substring(1, iPtr - 1);
                                            sValue = sValue.Substring(iPtr + 1);
                                            switch (sTmp.ToUpper())
                                            {
                                                case "SQLSERVER":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SQLSRVR);
                                                    break;
                                                case "SYBASE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_SYBASE);
                                                    break;
                                                case "INFORMIX":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_INFORMIX);
                                                    break;
                                                case "ORACLE":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ORACLE);
                                                    break;
                                                case "DB2":
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_DB2);
                                                    break;
                                                default:
                                                    iDBType = Convert.ToInt32(eDatabaseType.DBMS_IS_ACCESS);
                                                    break;
                                            }
                                            if (g_dbMake == iDBType)
                                                ObjInfoDef.FilterDef[FilterNum].SQLWhere = sValue;
                                        }
                                        else
                                        {
                                            ObjInfoDef.FilterDef[FilterNum].SQLWhere = sValue;
                                        }
                                        break;
                                    case "PROCEDURE":
                                        ObjInfoDef.FilterDef[FilterNum].Procedure = sValue;
                                        break;
                                    case "PROCMSG":
                                        ObjInfoDef.FilterDef[FilterNum].ProcMsg = sValue;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }               
                NumInfoDefs = InfoNum + 1;
                return;          
            }
            //public static void subOLExportSchPlus(ref short iOLEType, ref int lDestUserID, ref string sTaskName, ref string sProjName, ref string sPriority, ref object vDateDue, ref object vTimeDue, ref string sNote)
            //{
               
            //    object objApp = null;
            //    object objSched = null;
            //    object objTable = null;
            //    object objItem = null;
            //    //Dim rc As Integer
            //    int lUserID = 0;

            //    //outlook
            //    if (iOLEType == 1)
            //    {
            //        // Added by Denis Basaric, 11/05/1997
            //        // If user don't have Email adress skip him
            //        if (string.IsNullOrEmpty(Strings.Trim(sMailGetEmailName(lDestUserID))))
            //            return;

            //        objApp = Interaction.CreateObject("Outlook.Application");
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objApp.CreateItem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem = objApp.CreateItem(3);
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Subject. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Subject = sProjName + Strings.Chr(9) + sTaskName;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Body. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Body = sNote;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.DueDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        //UPGRADE_WARNING: Couldn't resolve default property of object vDateDue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.DueDate = Convert.ToDouble(vDateDue);
            //        //mjh11/19/99 objItem.Importance = frmDiaryCreate.cboPriority.ItemData(frmDiaryCreate.cboPriority.ListIndex) - 1
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Recipients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Recipients.Add(sMailGetEmailName(lDestUserID));
            //        lUserID = objUser.UserID;
            //        if (lUserID == lDestUserID)
            //        {
            //            //UPGRADE_WARNING: Couldn't resolve default property of object objItem.save. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //            objItem.save();
            //        }
            //        else
            //        {
            //            //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Assign. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //            objItem.Assign();
            //            //UPGRADE_WARNING: Couldn't resolve default property of object objItem.send. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //            objItem.send();
            //        }
            //        //UPGRADE_NOTE: Object objItem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objItem = null;
            //        //UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objApp = null;
            //        //shcedule plus
            //    }
            //    else
            //    {
            //        // ERROR: Not supported in C#: OnErrorStatement

            //        Err().Clear();
            //        objApp = Interaction.CreateObject("SchedulePlus.Application");
            //        if (Err().Number != 0)
            //        {
            //            switch (Err().Number)
            //            {
            //                case 429:
            //                    objApp = Interaction.CreateObject("SchedulePlus.Application.7");
            //                    Err().Clear();
            //                    break;
            //                default:
            //                    goto ET_subOLEExportSchPlus;
            //                    break;
            //            }
            //        }
            //        // ERROR: Not supported in C#: OnErrorStatement

            //        //UPGRADE_WARNING: Couldn't resolve default property of object objApp.Logon. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objApp.Logon();
            //        //objSched = objApp.ScheduleForUser '(sMailGetEmailName(lDestUserID), "", "", "", "")
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objApp.ScheduleLogged. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objSched = objApp.ScheduleLogged;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objSched.SingleTasks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objTable = objSched.SingleTasks;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objTable.New. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        //commented by pk
            //        //objItem = objTable.New

            //        //For VB3 OLE automation can not set multiple properties at a time
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Text = sProjName + Strings.Chr(9) + sTaskName;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Notes. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Notes = sNote;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Priority. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Priority = LConvertTo32bitPriority(sPriority);
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.Created. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.Created = DateAndTime.Now;
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.CreatorName. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.CreatorName = "RISKMASTER Auto Diary";
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.EndDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.EndDate = LConvertTo32bitDate(ref vDateDue);

            //        //UPGRADE_WARNING: Couldn't resolve default property of object objApp.Logoff. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objApp.Logoff();
            //        //UPGRADE_NOTE: Object objItem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objItem = null;
            //        //UPGRADE_NOTE: Object objTable may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objTable = null;
            //        //UPGRADE_NOTE: Object objSched may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objSched = null;
            //        //UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objApp = null;
            //    }

            //    return;
            //ET_subOLEExportSchPlus:
            //    short EResult = 0;
            //    if (Err().Number == 440)
            //    {
            //        // Changed by Denis Basaric, 11/05/97 to give users a chance to cancel this.
            //        //MsgBox "Schedule Plus might have been locked," & Chr$(10) & "Check Schedule Plus first, then Click OK."
            //        EResult = Interaction.MsgBox("Schedule Plus might have been locked," + Strings.Chr(10) + "Check Schedule Plus first, then Click OK.", MsgBoxStyle.OkCancel + MsgBoxStyle.Question);
            //        if (EResult == MsgBoxResult.Ok)
            //        {
            //            // ERROR: Not supported in C#: ResumeStatement

            //        }
            //        else
            //        {
            //            //UPGRADE_NOTE: Object objItem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objItem = null;
            //            //UPGRADE_NOTE: Object objTable may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objTable = null;
            //            //UPGRADE_NOTE: Object objSched may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objSched = null;
            //            //UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objApp = null;
            //            return;
            //        }
            //        // End Changes by Denis Basaric
            //    }
            //    else if (Err().Description == "You cannot send a task request to yourself.")
            //    {
            //        //UPGRADE_WARNING: Couldn't resolve default property of object objItem.save. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //        objItem.save();
            //        //UPGRADE_NOTE: Object objItem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objItem = null;
            //        //UPGRADE_NOTE: Object objTable may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objTable = null;
            //        //UPGRADE_NOTE: Object objSched may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objSched = null;
            //        //UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //        objApp = null;
            //        return;
            //    }
            //    else
            //    {
            //        EResult = iGeneralErrorExt(Err().Number, Conversion.ErrorToString(), "subOLEExportSchPlus");
            //    }
            //    switch (EResult)
            //    {
            //        case MsgBoxResult.Retry:
            //            // ERROR: Not supported in C#: ResumeStatement

            //            break;
            //        default:
            //            //UPGRADE_NOTE: Object objItem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objItem = null;
            //            //UPGRADE_NOTE: Object objTable may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objTable = null;
            //            //UPGRADE_NOTE: Object objSched may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objSched = null;
            //            //UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            //            objApp = null;
            //            return;

            //            break;
            //    }
            //}
          
           

           

            private void btnExit_Click(object sender, EventArgs e)
            {
                this.Close();
            }

            private void btnSuspend_Click(object sender, EventArgs e)
            {
                if (btnSuspend.Tag.ToString() == "0")
                {
                    btnSuspend.Tag = "1";
                    btnSuspend.Text = "Resume";
                    btnStart.Enabled = true;
                    btnExit.Enabled = true;
                    gProcessStatus = PS_SUSPEND;
                    listBoxWPA.Items.Add("Auto Diary Processing Suspended @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                }
                else
                {
                    btnSuspend.Tag = "0";
                    btnSuspend.Text = "Suspend";
                    btnStart.Enabled = true;
                    btnExit.Enabled = true;
                    gProcessStatus = PS_GO;
                    listBoxWPA.Items.Add("Auto Diary Processing Resumed @" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                }
            }
            
            public bool CheckStop()
            {
                bool CheckStop = false;
                if (MessageBox.Show("Are you sure you want to stop Auto Diary Processing (y/n)?", "AUTO DIARY PROCESSING", MessageBoxButtons.YesNoCancel).ToString() == "Yes")
                {
                    CheckStop = true;
                    listBoxWPA.Items.Add("Auto Diary Processing stopped " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                }
                else
                {
                    gProcessStatus = PS_GO;
                    btnStart.Tag = "1";
                    btnStart.Text = "&Stop";
                    btnStart.Refresh();
                    btnSuspend.Enabled = true;
                    btnExit.Enabled = false;
                }
                return CheckStop;
            }


            //'this function is used to export diary notification via email
            //'since the diary is already generated, we should not display any msgbox for error to user if email sending fails
            //'this fucntion works silently w/o msgbx, so any error will be logged in log file.
            private void bExportToEmail( string sSenderName , string sSenderEmail , 
                                       string sRecipientTo , string sSubject , string sBody , string sUser ) 
            {
    
                //Dim objSMTP         As EasyMailSMTPObj
                long  lReturn =0 ;
                string sErrString =string.Empty;
                bool bValidated = true;
                //System.Net.Mail.SmtpClient objSMTP = null;
               // System.Net.Mail.MailMessage objmail = new System.Net.Mail.MailMessage();             
                // objSMTP = new System.Net.Mail.SmtpClient();

                StringBuilder p_sTo = new StringBuilder();
                if (!string.IsNullOrEmpty(sRecipientTo))
                    p_sTo.Append((p_sTo.Length > 0) ? "," + sRecipientTo : sRecipientTo);
                if (string.IsNullOrEmpty( sRecipientTo )) 
                {
                    sErrString = "-> User '" + sUser + "' does not have a valid Email Address.";
                    bValidated = false;
                }
   
    
                if (string.IsNullOrEmpty(sSenderEmail) ) 
                {
                    sErrString = sErrString + " -> Please specify valid Admin Email Address using Security Management System.";
                    bValidated = false;
                }
   
                if (string.IsNullOrEmpty(sSMTPServer)) 
                {
                    sErrString = sErrString + " -> Please specify Mail Server using Security Management System.";
                    bValidated = false;
                }
                //else
                //    objSMTP.Host  = sSMTPServer;


                if (!bValidated)
                {
                    sErrString = "Export To Email Failed. " + sErrString;
                    return;
                }
                //System.Net.Mail.MailAddress objFrom = new System.Net.Mail.MailAddress();
                //objFrom.Address 
                Mailer objMailer = null;
                try
                {
                    objMailer = new Mailer(WPAForm.Globalvar.m_iClientId);//psharma206
                    objMailer.To = p_sTo.ToString();
                    objMailer.From = sSenderEmail;
                    objMailer.Subject = sSubject;
                    objMailer.Body = sBody;

                    objMailer.SendMail();
                }
                catch (Exception p_objException)
                {
                    sErrString = "Error in Sending Mail.";
                }
                finally
                {
                    if (objMailer != null)
                        objMailer = null;
                }

                //objmail.From = new System.Net.Mail.MailAddress(sSenderEmail);
               
                //objmail.Subject = sSubject;
                //objmail.Body = sBody;
                ////objmail.li = "Dorn Technology Group, Inc./30034206149719039930";
                // objSMTP.Send(objmail)  ;      //'If return value is 0 that means successful
                //if( lReturn != 0 )
                //{
                //    sErrString = "Error in Sending Mail.";
                //}
        
                //else
                //    bExportToEmail = true;
    
            //hExit:
            //    Exit Function
            //hRunTimeError:
            //    Call iGeneralErrorExt(Err, Error$, "GLOBAL.BAS\bExportToEmail")
            //    Exit Function
            //hValidationError:
            //    Call subWriteErrorLog("GLOBAL.BAS", 0, sErrString)
            //    Exit Function
                
            }


        //'this function is used to get admin's email address from database which will be used as "from" email address
        //'while sending email to users for auto diary notifications


       public string GetFromEmailAddressForAdmin()
       {
            //'get Admin email Address as From Address
            string sSQL = string.Empty;
            sSQL = "SELECT ADMIN_EMAIL_ADDR FROM SETTINGS WHERE ID = 1";
            DbReader objReader = null;
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(Globalvar.m_sConnectionSecurity);
                objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        if (objReader.IsDBNull("ADMIN_EMAIL_ADDR"))
                            sFromEmailAdd = "";
                        else
                            sFromEmailAdd = objReader.GetString("ADMIN_EMAIL_ADDR");
                    }
                    return sFromEmailAdd;
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("GetFromEmailAddressForAdmin", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }
            return sFromEmailAdd;
        }

        //'this function is used to get SMPT Server from database
        //'which will be used to send email notifications for diary
        public string GetSMTPServer()
        {
            //'get Admin email Address as From Address
            string sSQL = string.Empty;
            sSQL = "SELECT SMTP_SERVER FROM SETTINGS WHERE ID = 1";

            DbReader objReader = null;
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(Globalvar.m_sConnectionSecurity);
                objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        if (objReader.IsDBNull("SMTP_SERVER"))
                            sSMTPServer = "";
                        else
                            sSMTPServer = objReader.GetString("SMTP_SERVER");
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("GetSMTPServer", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }
            return sSMTPServer;
        }


        public  void UpdateStatus(string sMessage)
        {
            if (!IsCallFromTM)
            {

                listBoxWPA.Items.Add(sMessage);
                listBoxWPA.Refresh();
                while (gProcessStatus == PS_SUSPEND)
                {
                    System.Windows.Forms.Application.DoEvents();
                }
            }
            else
            {
                // RMA-4300 : WPA processing exe tries to write the log in Application.StartupPath
                RSWPayTLDiary.UpdateStatus(sMessage);
                //string path = Application.StartupPath + @"\wpaproc.log";                
                //File.AppendAllText(path, sMessage + Environment.NewLine);              
            }
            WriteToStdOut(0, sMessage);            
        }

        public static void WriteToStdOut( int lErrCode,  string sText)
        {
            string sWriteBuffer = null;  
            sWriteBuffer = Convert.ToString( lErrCode).Trim() + "^*^*^" + sText + Constants.VBCrLf;
            Console.WriteLine(sWriteBuffer);
        }
        
        /// <summary>
        /// To get the parent record id
        /// </summary>
        /// <param name="attachTable"></param>
        /// <param name="attachRecordId"></param>
        /// <returns></returns>
        public static int ParentRecord(string attachTable, int attachRecordId, int p_iClientId)
        {
            string sSQL = string.Empty;
            DbReader objRead = null;
            int parentRecordId = 0;
            switch (attachTable)
            {
                /* Event as Parent Record category */
                case "EVENTDATEDTEXT":
                    sSQL = "select event_id from EVENT_X_DATED_TEXT where ev_dt_row_id =" + attachRecordId;
                    break;
                case "OSHA":
                case "FALLINFO":
                case "MEDWATCHTEST":
                case "MEDWATCH":
                    parentRecordId = attachRecordId;
                    break;
                case "PERSON_INVOLVED":
                case "PIPHYSICIAN":
                case "PIMEDSTAFF":
                case "PIEMPLOYEE":
                case "PIPATIENT":
                case "PIDRIVER":
                case "PIWITNESS":
                case "PIOTHER":
                case "PIDEPENDENT":
                    sSQL = "select event_id as EVENT from PERSON_INVOLVED where pi_row_id=" + attachRecordId;
                    break;
                case "CONCOMITANT":
                    sSQL = "select event_id from EV_X_CONCOM_PROD where EV_CONCOM_ROW_ID = " + attachRecordId;
                    break;
                /* Claim as Parent Record category */
                case "SUBROGATION":
                case "SCHEDULED_ACTIVITY":
                    sSQL = "select claim_id from claim_x_subro where subrogation_row_id = " + attachRecordId;
                    break;
                case "CLAIMANT":
                    sSQL = "select claim_id from claimant where claimant_row_id =" + attachRecordId;
                    break;
                case "CLAIM_X_LITIGATION":
                case "LITIGATION":
                    sSQL = "select claim_id from CLAIM_X_LITIGATION where litigation_row_id = " + attachRecordId;
                    break;
                case "FUNDS_AUTO_BATCH":
                    sSQL = " select claim_id from funds where trans_id = " + attachRecordId;
                    break;
                case "AUTOCLAIMCHECKS":
                    sSQL = " select claim_id from FUNDS_AUTO where AUTO_batch_ID =  " + attachRecordId;
                    break;
                case "CM_X_TREATMENT_PLN":
                case "CMXTREATMENTPLN":
                    sSQL = "select claim_id from CM_X_TREATMENT_PLN tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmtp_row_id = " + attachRecordId;
                    break;
                case "ADJUSTER":
                    sSQL = "select claim_id from CLAIM_ADJUSTER where adj_row_id = " + attachRecordId;
                    break;
                case "CMXMEDMGTSAVINGS":
                    sSQL = "select claim_id from CM_X_MEDMGTSAVINGS tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmtp_row_id = " + attachRecordId;
                    break;
                case "PIRESTRICTION":
                    sSQL = "select claim_id from CLAIM where claim_id  = ( select claim_id from PI_X_RESTRICT pr INNER JOIN PERSON_INVOLVED pin ON pr.pi_row_id = pin.pi_row_id INNER JOIN CASE_MANAGEMENT cm ON cm.PI_ROW_ID = pin.pi_row_id where pi_restrict_row_id =" + attachRecordId + ")";
                    break;
                case "PIWORKLOSS":
                    sSQL = "select claim_id from CLAIM where claim_id  = ( select claim_id from PI_X_WORK_LOSS pw INNER JOIN PERSON_INVOLVED pin ON pw.pi_row_id = pin.pi_row_id INNER JOIN CASE_MANAGEMENT cm ON cm.PI_ROW_ID = pin.pi_row_id where pi_wl_row_id =" + attachRecordId + " )";
                    break;
                case "LEAVE":
                    sSQL = "select claim_id from CLAIM_LEAVE where leave_row_id = " + attachRecordId;
                    break;
                case "UNIT":
                    sSQL = "select claim_id from UNIT_X_CLAIM where unit_row_id = " + attachRecordId;
                    break;
                case "PROPERTYLOSS":
                    sSQL = "select claim_id from CLAIM_X_PROPERTYLOSS  where property_id = " + attachRecordId;
                    break;
                case "CMXACCOMMODATION":
                    sSQL = "select claim_id from CM_X_ACCOMMODATION ca INNER JOIN CASE_MANAGEMENT cm on ca.cm_row_id = casemgt_row_id where cm_accomm_row_id =" + attachRecordId;
                    break;
                case "CMXVOCREHAB":
                    sSQL = "select claim_id from CM_X_VOCREHAB cv INNER JOIN CASE_MANAGEMENT cm on cv.casemgt_row_id = cm.casemgt_row_id where cmvr_row_id = " + attachRecordId;
                    break;
                case "EXPERT":
                    sSQL = "select claim_id from EXPERT ex INNER JOIN claim_x_Litigation li on ex.litigation_row_id = li.litigation_row_id where EXPERT_ROW_ID =" + attachRecordId;
                    break;
                case "SALVAGE":
                    sSQL = "select claim_id from SALVAGE s INNER JOIN CLAIM_X_PROPERTYLOSS cp ON s.parent_id = cp.property_id where salvage_row_id = " + attachRecordId;
                    break;
                case "DEFENDANT":
                    sSQL = "select claim_id from DEFENDANT where defendant_row_id = " + attachRecordId;
                    break;
                case "CMXCMGRHIST":
                    sSQL = "select claim_id from CASE_MANAGEMENT where casemgt_row_id =" + attachRecordId;
                    break;
                /* Policy as Parent Record Category */
                case "POLICYCOVERAGE":
                    sSQL = "select policy_id from POLICY_X_CVG_TYPE where POLCVG_ROW_ID =" + attachRecordId;
                    break;
                case "POLICYBILLING":
                    sSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_ID =" + attachRecordId;
                    break;
            }
            try
            {
                if (!string.IsNullOrEmpty(sSQL))
                {
                    using (objRead = DbFactory.GetDbReader(m_sConnectionRiskmaster, sSQL))
                    {
                        if (objRead != null)
                        {
                            if (objRead.Read())
                            {
                                parentRecordId = Convert.ToInt32(objRead[0]);//MITS 34031 
                            }
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleInt.GeneralError", Globalvar.m_iClientId), p_objException);//psharma206
            }
            finally
            {
                if (objRead != null)
                {
                    objRead.Close();
                    objRead.Dispose();
                }
            }
            return parentRecordId;
        }
        
    }
}
