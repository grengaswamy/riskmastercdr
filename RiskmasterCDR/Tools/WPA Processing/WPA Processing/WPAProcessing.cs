﻿using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WPAProcessing
{
    class WPAProcessing
    {
        /// <summary>
        /// Logs errors.Taken from the CWS code.
        /// </summary>
        /// <param name="sAdaptorMethod"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        public static void logErrors(string sAdaptorMethod, XmlDocument xmlRequest, bool bCallResult, BusinessAdaptorErrors errors)
        {

            // First, log the error to the error log

            // Add overall result status
            string sCall = sAdaptorMethod;
            if (bCallResult)
                sCall += " (Success)";
            else
                sCall += " (Error)";

            // Log the overall call failure and the input envelope (only if call failed)
            if (bCallResult == false)
            {
                LogItem oLogItem = new LogItem();
                oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                oLogItem.Category = "CommonWebServiceLog";//WPAProcessingServiceLog
                oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                oLogItem.Message = sAdaptorMethod + " call failed.";
                //if (xmlRequest != null)
                //    oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
                Log.Write(oLogItem, WPAForm.Globalvar.m_iClientId);//psharma206
            }

            // Iterate through individual errors and log them. If exceptions are present, those will be walked (via InnerException) and all exceptions in the list will be logged.
            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... build error type
                    string sErrorType = null;
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            sErrorType = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            sErrorType = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            sErrorType = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            sErrorType = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            sErrorType = "PopupMessage";
                            break;
                        default:
                            sErrorType = "SystemError";
                            break;
                    };

                    // ... add individual error messages/warnings
                    // RMA-4300 :WPA processing exe tries to write the log in Application.StartupPath
                    if (err.ErrorCode == "WPAProcessing")
                    {
                        LogItem oLogItem = new LogItem();
                        oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                        oLogItem.Category = "WPAProcessingLog";
                        oLogItem.Message = err.ErrorDescription;
                        Log.Write(oLogItem, WPAForm.Globalvar.m_iClientId);
                    }

                    else if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // Init logfile item for this error
                        LogItem oLogItem = new LogItem();
                        oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                        oLogItem.Category = "CommonWebServiceLog";//WPAProcessingServiceLog
                        oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                        oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                        oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);
                        oLogItem.RMParamList.Add("Call Error Type", sErrorType);

                        // ... add error code/number
                        oLogItem.RMParamList.Add("Error Code", err.ErrorCode);

                        // ... add error description
                        oLogItem.Message = err.ErrorDescription;

                        // Flush entry to log file
                        Log.Write(oLogItem, WPAForm.Globalvar.m_iClientId);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        Exception exc = err.oException;

                        while (exc != null)
                        {
                            // Init logfile item for this error
                            LogItem oLogItem = new LogItem();
                            oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                            oLogItem.Category = "CommonWebServiceLog";//WPAProcessingServiceLog
                            oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                            oLogItem.RMParamList.Add("Call Error Type", sErrorType);
                            oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                            oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);


                            // ... determine error code - assembly + exception type
                            oLogItem.RMParamList.Add("Error Code", err.oException.Source + "." + err.oException.GetType().Name);

                            // ... add error description
                            if (err.ErrorDescription != "")
                                oLogItem.Message = err.ErrorDescription;
                            else
                                oLogItem.Message = err.oException.Message;

                            // ... dump exception info to log
                            oLogItem.RMParamList.Add("Exception Source", exc.Source);
                            oLogItem.RMParamList.Add("Exception Type", exc.GetType().Name);
                            oLogItem.RMParamList.Add("Exception Message", exc.Message);
                            oLogItem.RMParamList.Add("Exception StackTrace", exc.StackTrace);

                            // ... flush entry to log file
                            Log.Write(oLogItem, WPAForm.Globalvar.m_iClientId);//psharma206

                            // ... get next exception in chain (if there is one)
                            exc = exc.InnerException;
                        }
                    }  // Exception case
                }
            }

        }
    }
}
