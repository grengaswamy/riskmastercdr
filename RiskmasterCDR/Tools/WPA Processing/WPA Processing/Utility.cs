﻿using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPAProcessing
{
   public static class Utility
    {
      static BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(WPAForm.Globalvar.m_iClientId);
      // FUNCTION: sSQLTextArg
      // PURPOSE : Process varchar arguments for use directly in SQL statements.
      //           Quotes the string if not empty. Double quotes any single quotes so they will work.
      //           Returns the string NULL if string is empty.
      public static string sSQLTextArg(string sArg)
        {
            string sTmp = string.Empty;
            try
            {
                
                if (string.IsNullOrEmpty(sArg))
                {
                    sTmp = "NULL";
                }
                else
                {
                    sTmp = sArg.Replace("'", "''");
                    sTmp = "'" + sTmp.Replace("\"", "\"\"") + "'";
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("sSQLTextArg", null, false, systemErrors);
            }
            return sTmp;
        }
      public static bool IsTableInSQLFrom(string sSQLFrom, string sTableName)
      {
          bool functionReturnValue = false;
          //******************************************************
          //* Function check does table already exists in SQL    *
          //* FROM clause and if it exist return TRUE, otherwise *
          //* function return false.                             *
          //* sSQLFrom   - SQL From clause                       *
          //* sTableName - Table name to check for.              *
          //*                                                    *
          //*              *
          //******************************************************
          const string SQL_FROM = "FROM ";


          // Default is false
          functionReturnValue = false;

          if (string.IsNullOrEmpty(sSQLFrom) || string.IsNullOrEmpty(sTableName))
              return functionReturnValue;

          sSQLFrom = sSQLFrom.ToUpper().Trim();
          sTableName = sTableName.ToUpper().Trim();

          // Remember Length of sTableName
          int i = sTableName.Length;
          if (i > sSQLFrom.Length || i + SQL_FROM.Length > sSQLFrom.Length) //jramkumar for MITS 35095
              return functionReturnValue;
          // Check is Table name at begining of sSQLFrom
          if (sSQLFrom.Substring(0, i) == sTableName)
          {
              functionReturnValue = true;
              // Is it at the end ?
          }
          else if (sSQLFrom.Substring(i) == sTableName)
          {
              functionReturnValue = true;
              // Is it at begining with FROM
          }
          else if (sSQLFrom.Substring(0, i + SQL_FROM.Length) == SQL_FROM + sSQLFrom) //jramkumar for MITS 35095
          {
              functionReturnValue = true;
              // Is it inside with ,
          }
          else if (sSQLFrom.IndexOf(sTableName + ",") > 0)
          {
              functionReturnValue = true;
          }
          else if (sSQLFrom.IndexOf("," + sTableName) > 0 || sSQLFrom.IndexOf(", " + sTableName) > 0)
          {
              functionReturnValue = true;
          }
          else if (sSQLFrom == SQL_FROM + sTableName)
          {
              functionReturnValue = true;
          }
          return functionReturnValue;
      }
      public static string sGetTablesNotInSQLFrom(string sSQLFrom, string sTableNames)
      {
          //******************************************************
          //* Function check does Table/Tables separated with    *
          //* comma exist(s) in SQL From clause and return all   *
          //* table(s) that not exist there, comma separated.    *
          //* sSQLFrom    - SQL From clause                      *
          //* sTableNames - Table(s) name to check for.          *
          //* Return: String with all table(s) that don't exists *
          //*         in FROM clause, comma separated.           *
          //* Author:                  *
          //******************************************************
          int i = 0;
          int iLastPosition = 0;
          string s = null;
          string sReturn = null;

          const string SEPARATOR = ",";

          sTableNames = sTableNames.Trim();
          sSQLFrom = sSQLFrom.Trim();

          if (string.IsNullOrEmpty(sTableNames))
              return sReturn;

          if (string.IsNullOrEmpty(sSQLFrom))
          {
              sReturn = sTableNames;
              return sReturn;
          }

          // Check did we receive just one table
          i = sTableNames.IndexOf(SEPARATOR);
          if (i == -1)
          {
              // It's just one table
              if (!Utility.IsTableInSQLFrom(sSQLFrom, sTableNames))
              {
                  // Table not in FROM
                  sReturn = sTableNames;
              }
              return sReturn;
          }

          // Otherwise Parse all tables from string and check does they exists in SQL FROM clause
          iLastPosition = 0; //jramkumar for MITS 35095
          while (i > 0)
          {
              s = sTableNames.Substring(iLastPosition, i - iLastPosition);
              if (!Utility.IsTableInSQLFrom(sSQLFrom, s))
              {
                  if (!string.IsNullOrEmpty(sReturn))
                      sReturn = sReturn + SEPARATOR;
                  sReturn = sReturn + s;
              }
              iLastPosition = i + 1;
              i = sTableNames.IndexOf(SEPARATOR, iLastPosition);

              if (i == -1)
              {
                  // Get the last one
                  if (iLastPosition < sTableNames.Length)
                  {
                      s = sTableNames.Substring(iLastPosition);
                      if (!Utility.IsTableInSQLFrom(sSQLFrom, s))
                      {
                          if (!string.IsNullOrEmpty(sReturn))
                              sReturn = sReturn + SEPARATOR;
                          sReturn = sReturn + s;
                      }
                  }
              }
          }

          return sReturn;

      }
       // Obsolete methods Not USed in Conversion
     /* public static short bAreFormsActive(ref System.Windows.Forms.Form Frm)
      {
          short functionReturnValue = 0;
          // ERROR: Not supported in C#: OnErrorStatement

          string sTmp = null;
          sTmp = Frm.ActiveMdiChild.Text;
          if (Err().Number)
          {
              functionReturnValue = false;
          }
          else
          {
              functionReturnValue = true;
          }
          return functionReturnValue;
      }

      public static short bIsDate(string sDate)
      {
          short functionReturnValue = 0;
          if (Information.IsNumeric(sDate) & Strings.Len(sDate) == 8)
          {
              functionReturnValue = true;
          }
          else
          {
              functionReturnValue = false;
          }
          return functionReturnValue;
      }

      public static double dAnyVarToDouble(ref object a)
      {
          double functionReturnValue = 0;
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //1 a | Variant | Input | No information at this time
          //
          //****************************************************************
          //UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          switch (Information.VarType(a))
          {
              case V_NULL:
              case V_EMPTY:
                  functionReturnValue = 0;
                  return functionReturnValue;
              case V_INTEGER:
              case V_LONG:
              case V_SINGLE:
              case V_DOUBLE:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = a;
                  return functionReturnValue;
              case V_STRING:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = Conversion.Val(a);
                  return functionReturnValue;
          }
          return functionReturnValue;


      }

      public static double dDateTimeValue(ref string sDate, ref string sTime)
      {
          double dtmp = 0;
          //Dim sTmp As String
          //dTmp = Second(sTime) * 60
          //dTmp = dTmp + Minute(sTime) * 60
          //dTmp = dTmp + Hour(sTime) * 3600
          //dTmp = Val("." & Str(dTmp))
          //dTmp = dTmp + DateValue(sDate)
          //UPGRADE_WARNING: DateValue has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          dtmp = DateAndTime.DateValue(sDate).ToOADate() + DateAndTime.TimeValue(sTime).ToOADate();

          return dtmp;

      }

      public static double dDTTMToDouble(ref string sDTTM)
      {
          double dtmp = 0;
          string sDate = null;
          string sTime = null;

          if (!string.IsNullOrEmpty(Strings.Trim(sDTTM)))
          {
              sDate = Strings.Mid(sDTTM, 5, 2) + "-" + Strings.Mid(sDTTM, 7, 2) + "-" + Strings.Left(sDTTM, 4);
              sTime = Strings.Mid(sDTTM, 9, 2) + ":" + Strings.Mid(sDTTM, 11, 2) + ":" + Strings.Mid(sDTTM, 13, 2);
              //UPGRADE_WARNING: DateValue has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
              dtmp = DateAndTime.DateValue(sDate).ToOADate() + DateAndTime.TimeValue(sTime).ToOADate();
          }
          return dtmp;

      }

      public static double dFilterCurrency(ref double dAmt)
      {
          double functionReturnValue = 0;
          // PURPOSE: Rounds a number to zero if it is very, very small.

          if (dAmt < 0.005 & dAmt > -0.005)
              functionReturnValue = 0.0;
          else
              functionReturnValue = dAmt;
          return functionReturnValue;
      }

      public static short HIBYTE(ref short ShortInt)
      {
          return ShortInt / 256;
      }

      public static short iAnyVarToInt(ref object a)
      {
          short functionReturnValue = 0;
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //1 a | Variant | Input | No information at this time
          //
          //****************************************************************
          //UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          switch (Information.VarType(a))
          {
              case V_NULL:
              case V_EMPTY:
                  functionReturnValue = 0;
                  return functionReturnValue;
              case V_INTEGER:
              case V_LONG:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = a;
                  break;
              case V_SINGLE:
              case V_DOUBLE:
                  functionReturnValue = Conversion.Int(Convert.ToDouble(a));
                  return functionReturnValue;
              case V_STRING:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = Conversion.Val(a);
                  return functionReturnValue;
          }
          return functionReturnValue;
      }

      public static short iCalculateAge(ref string sDate1, ref string sDate2)
      {
          short functionReturnValue = 0;
          int lTmp = 0;

          if (Information.IsDate(sDate1) & Information.IsDate(sDate2))
          {
              //UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
              lTmp = Conversion.Int(DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.DayOfYear, Convert.ToDateTime(sDate1), Convert.ToDateTime(sDate2)) / 365.25);
              if (lTmp > 32000)
                  lTmp = 0;
              if (lTmp < 0)
                  lTmp = 0;
              functionReturnValue = lTmp;
          }
          else
          {
              functionReturnValue = 0;
          }
          return functionReturnValue;


      }

      public static short iFindItemInListbox(ref System.Windows.Forms.Control.ControlCollection lst, ref int lItemData)
      {
          short functionReturnValue = 0;
          short nCount = 0;
          short i = 0;
          //changes done by pk
          //UPGRADE_WARNING: Couldn't resolve default property of object lst.ListCount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
          //nCount = lst.ListCount - 1
          nCount = lst.Count - 1;

          for (i = 0; i <= nCount; i++)
          {
              //UPGRADE_WARNING: Couldn't resolve default property of object lst.ItemData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
              //If lst.ItemData(i) = lItemData Then
              functionReturnValue = i;
              return functionReturnValue;
              //End If
          }

          functionReturnValue = 0;
          return functionReturnValue;
      }

      


      public static int lAnyVarToLong(ref object a)
      {
          int functionReturnValue = 0;
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //1 a | Variant | Input | No information at this time
          //
          //****************************************************************
          //UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          switch (Information.VarType(a))
          {
              case V_NULL:
              case V_EMPTY:
                  functionReturnValue = 0;
                  return functionReturnValue;
              case V_INTEGER:
              case V_LONG:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = a;
                  break;
              case V_SINGLE:
              case V_DOUBLE:
                  functionReturnValue = Conversion.Int(Convert.ToDouble(a));
                  return functionReturnValue;
              case V_STRING:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = Conversion.Val(a);
                  return functionReturnValue;
          }
          return functionReturnValue;
      }

      public static short LOBYTE(ref short ShortInt)
      {
          return ShortInt % 256;
      }

      public static short LOWORD(ref int LongInt)
      {
          return LongInt % 65536;
      }

      public static void PutPrinter(object a, float X, float Y, ref short Justify)
      {
          Printer Printer = new Printer();
          // ERROR: Not supported in C#: OnErrorStatement

          //Global Const LEFT_JUSTIFY = 0  ' 0 - Left Justify
          //Global Const RIGHT_JUSTIFY = 1 ' 1 - Right Justify
          //Global Const CENTER = 2        ' 2 - Center
          //Global Const JUSTIFY = 3       ' 3 - JUSTIFY preforms line wrap not available
          float length = 0;
          string PtText = null;
          string Text = null;
          //UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          switch (Information.VarType(a))
          {
              case V_NULL:
              case V_EMPTY:
                  Text = "";
                  break;
              case V_INTEGER:
              case V_LONG:
              case V_SINGLE:
              case V_DOUBLE:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  Text = Conversion.Str(a);
                  break;
              case V_STRING:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  Text = a;
                  break;
          }

          PtText = Strings.Trim(Text);
          length = Printer.TextWidth(PtText);
          switch (Justify)
          {
              case 0:
                  Printer.CurrentY = Y;
                  Printer.CurrentX = X;
                  Printer.Print(PtText);
                  break;

              case 1:
                  Printer.CurrentY = Y;
                  Printer.CurrentX = X - length;
                  Printer.Print(PtText);
                  break;

              case 2:
                  Printer.CurrentY = Y;
                  Printer.CurrentX = X - (length / 2);
                  Printer.Print(PtText);
                  break;
          }
          return;
      ERROR_TRAP_subPutPrinter:
          short EResult = 0;
          EResult = iGeneralErrorExt(Err().Number, Conversion.ErrorToString(), "UTIL.BAS/subPutPrinter");
          switch (EResult)
          {
              case IDCANCEL:
                  return;

                  break;
              case IDRETRY:
                  // ERROR: Not supported in C#: ResumeStatement

                  break;
              default:
                  return;

                  break;
          }

      }

      public static object sAnyVarToString(ref object a)
      {
          object functionReturnValue = null;
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //1 a | Variant | Input | No information at this time
          //
          //****************************************************************
          //UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
          switch (Information.VarType(a))
          {
              case V_NULL:
              case V_EMPTY:
                  //UPGRADE_WARNING: Couldn't resolve default property of object sAnyVarToString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = "";
                  return functionReturnValue;
              case V_INTEGER:
              case V_LONG:
              case V_SINGLE:
              case V_DOUBLE:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = Conversion.Str(a);
                  return functionReturnValue;
              case V_STRING:
                  //UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  //UPGRADE_WARNING: Couldn't resolve default property of object sAnyVarToString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                  functionReturnValue = a;
                  return functionReturnValue;
          }
          return functionReturnValue;
      }

      public static string sDateTimeValue(ref string sDate, ref string sTime)
      {
          string sTmp = null;
          sTmp = VB6.Format(sDate + " " + sTime, "YYYYMMDDHHNNSS");
          return sTmp;
      }

      public static string sDBDateFormat(ref string sDate, ref string sFormat)
      {
          string functionReturnValue = null;
          string sTmp = null;
          functionReturnValue = "";
          if (!string.IsNullOrEmpty(Strings.Trim(sDate)))
          {
              sTmp = Strings.Mid(sDate, 5, 2) + "-" + Strings.Mid(sDate, 7, 2) + "-" + Strings.Left(sDate, 4);
              functionReturnValue = VB6.Format(sTmp, sFormat);
          }
          return functionReturnValue;
      }

      public static string sDBDTTMFormat(ref string sDTTM, ref string sDateFormat, ref string sTimeFormat)
      {
          string sTmp = null;
          string sDateTmp = null;
          sTmp = Strings.Mid(sDTTM, 5, 2) + "-" + Strings.Mid(sDTTM, 7, 2) + "-" + Strings.Left(sDTTM, 4);
          sDateTmp = VB6.Format(sTmp, sDateFormat);
          //mjh 7/25/96  I can not believe that this has never worked and no one has reported that
          //their time is always related to the year.  before it waould create time off of the year
          sTmp = Strings.Mid(sDTTM, 9, 2) + ":" + Strings.Mid(sDTTM, 11, 2) + ":" + Strings.Mid(sDTTM, 13, 2);
          sDateTmp = sDateTmp + " " + VB6.Format(sTmp, sTimeFormat);

          return sDateTmp;

      }

      public static string sDBTimeFormat(ref string sTime, ref string sFormat)
      {
          string functionReturnValue = null;
          string sTmp = null;
          functionReturnValue = "";
          if (!string.IsNullOrEmpty(Strings.Trim(sTime)))
          {
              sTmp = Strings.Left(sTime, 2) + ":" + Strings.Mid(sTime, 3, 2) + ":" + Strings.Mid(sTime, 5, 2);
              functionReturnValue = VB6.Format(sTmp, sFormat);
          }
          return functionReturnValue;

      }

      public static string sGetSystemPath()
      {
          string functionReturnValue = null;
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //
          //****************************************************************
          if (Strings.Right(My.Application.Info.DirectoryPath, 1) == "\\")
          {
              functionReturnValue = My.Application.Info.DirectoryPath;
          }
          else
          {
              functionReturnValue = My.Application.Info.DirectoryPath + "\\";
          }
          return functionReturnValue;

      }

     

      public static string sTrimNull(ref string sArg)
      {
          string functionReturnValue = null;
          //short Index = 0;
          // Index = Strings.InStr(1, sArg, Strings.Chr(0));
          int Index = sArg.IndexOf('0', 1);
          if (Index > 0)
          {
              //functionReturnValue = Strings.Left(sArg, Index - 1);
              functionReturnValue = sArg.Substring(Index - 1);
          }
          else
          {
              functionReturnValue = sArg;
          }
          return functionReturnValue;
      }

      public static void subCenterForm(ref System.Windows.Forms.Form pForm)
      {
          ////Technical Info************************************************
          //
          ////Description
          //This needs a description
          //
          ////Arguments
          //1 pForm | Form | Input | No information at this time
          //
          //****************************************************************

          pForm.Left = VB6.TwipsToPixelsX((VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - VB6.PixelsToTwipsX(pForm.Width)) / 2);
          pForm.Top = VB6.TwipsToPixelsY((VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - VB6.PixelsToTwipsY(pForm.Height)) / 2);

      }

      public static void subCopyCombo(ref System.Windows.Forms.ComboBox cb1, ref System.Windows.Forms.ComboBox cb2)
      {
          // Copies list contents of cb1 to cb2
          cb2.Items.Clear();
          short i = 0;
          for (i = 0; i <= cb1.Items.Count - 1; i++)
          {
              cb2.Items.Add(VB6.GetItemString(cb1, i));
              VB6.SetItemData(cb2, i, VB6.GetItemData(cb1, i));
          }
      }

      public static void subDB_CloseRecordset(ref short rs, ref short iType)
      {
          short result = 0;


          if (rs != 0)
          {
              result = ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, iType);
              if (iType == ROCKETCOMLib.DTGDBCloseOptions.DB_DROP)
                  rs = 0;
              // clear rs var once freed
          }
      }

      public static void subHourGlass(ref short bDisplay)
      {

          if (bBackground != 0)
              return;

          if (bDisplay)
          {
              //UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
              System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
          }
          else
          {
              //UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
              System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
          }

      }

      public static object vDB_GetData(ref short rs, ref object vField)
      {
          object vData = null;


          ROCKETCOMLibDTGRocket_definst.DB_GetData(rs, vField, vData);

          //UPGRADE_WARNING: Couldn't resolve default property of object vData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
          //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
          return vData;
      }*/
    }
}
