﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Db;

namespace WPAProcessing
{
    public class MAPIVB
    {
       public static  BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(WPAForm.Globalvar.m_iClientId); 
        public static string sGetEmailByLoginName(string sUser)
        {
            DbReader objReader = null;
            DbConnection objConn = null;
            string sTmp = string.Empty;
            string sEmailAddr = null;
            string sSQL = null;
            objConn = DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.Dsn);
            objConn.Open();
            sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" + sUser + "'";
            try
            {
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        sEmailAddr = objReader.GetString(0) + "";
                    }
                    else
                    {
                        sEmailAddr = "";
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("GetEmailByLoginName", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }
            // Query       
            return sEmailAddr;
        }
    }
}
