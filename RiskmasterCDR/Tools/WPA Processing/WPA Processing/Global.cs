﻿using Riskmaster.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Runtime.InteropServices;
using Riskmaster.BusinessAdaptor.Common;
namespace WPAProcessing
{
  public static class Global
    {
      //moved to UTIL_REG File
        //public const string REG_ROOT_KEY = "SOFTWARE\\DTG";
        //public const short REG_SZ = 1;
        //public  const int KEY_LENGTH = 256;
        //public const short REG_SUCCESS = 0;
        //public const short REG_BADKEY = 2;
        //public const int HKEY_LOCAL_MACHINE = 80000002;
        ////[DllImport("advapi32.dll", EntryPoint = "RegQueryValueA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]        
        //public static extern int RegQueryValue(int hKey, string lpSubKey, string lpValue, ref int lpcbValue);
        //private static string m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource");
        //private static string m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity");
        private static string m_sConnectionRiskmaster = WPAForm.Globalvar.m_ConStrRiskmaster;//"Driver={SQL Server};Server=20.198.58.89;Database=RMA132Govind;UID=sa;PWD=csc1234$;";
        public static BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(WPAForm.Globalvar.m_iClientId); 

        public static string sGetUser(  int lDSNID,  int lUserID)
        {
            string functionReturnValue = null;
            string sSQL = null;
            DbConnection objConn = null;  
             sSQL = "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE DSNID = " + lDSNID + " AND USER_ID = " + lUserID;
             try
             {
                 objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                 objConn.Open();       
                 using (DbReader objReaderUsers = objConn.ExecuteReader(sSQL))
                 {
                     if (objReaderUsers.Read())
                     {
                          functionReturnValue = objReaderUsers.GetString(0) + "";
                     }
                     else
                     {                         
                         functionReturnValue = "";
                     }

                 }                 
             }
             catch (Exception p_objEx)
             {
                 systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                 WPAProcessing.logErrors("sGetUser", null, false, systemErrors);
             }
             finally
             {
                 if (objConn != null)
                 {
                     if (objConn.State == System.Data.ConnectionState.Open)
                     {
                         objConn.Close();
                     }
                     objConn.Dispose();
                     
                 }
             }
             return functionReturnValue;
        }

        public static string sGetManager(  int lDSNID,  string sUser)
        {
            string functionReturnValue = null;

            short rs = 0;
            string sSQL = null;
            DbConnection objConn = null; 
            sSQL = "SELECT UDT2.LOGIN_NAME AS MANAGER_LOGIN_NAME FROM USER_TABLE UT1 INNER JOIN USER_DETAILS_TABLE UDT1 ON UT1.USER_ID = UDT1.USER_ID " + "LEFT OUTER JOIN USER_DETAILS_TABLE UDT2 ON UT1.MANAGER_ID = UDT2.USER_ID AND UDT1.DSNID = UDT2.DSNID " + "WHERE UDT1.DSNID=" + lDSNID + " AND UDT1.LOGIN_NAME='" + sUser + "'";

            objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
           objConn.Open();      
            
             try
             {
                 using (DbReader objReadermanager = objConn.ExecuteReader(sSQL))
                 {
                     while (objReadermanager.Read())
                     {
                            if( !string.IsNullOrEmpty( objReadermanager.GetString("MANAGER_LOGIN_NAME")))
                            functionReturnValue += objReadermanager.GetString("MANAGER_LOGIN_NAME") + "";
                            else
                            {                         
                                functionReturnValue = "";
                            }                         
                     }                     
                 }
             }
             catch (Exception p_objEx)
             {
                 systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                 WPAProcessing.logErrors("sGetManager", null, false, systemErrors);
             }
             finally
             {
                 if (objConn != null)
                 {
                     if (objConn.State == System.Data.ConnectionState.Open)
                     {
                         objConn.Close();
                     }
                     objConn.Dispose();

                 }
             }
            return functionReturnValue;

        }

        public static string sGetHighestLevelManager( int lDSNID,  string sUser)
        {
            string sHighestLevelManagerLoginName = null;
            sHighestLevelManagerLoginName = "";
            int iNextLevelManagerId = 0;
            int iHighestLevelManagerId = 0;            
            string sSQL = null;
            //need to implement caching here
            Dictionary<string, string> ObjHLMcache = new Dictionary<string, string>();
            sSQL = "SELECT ut1.MANAGER_ID FROM USER_TABLE UT1 INNER JOIN USER_DETAILS_TABLE UDT1 ON UT1.USER_ID = UDT1.USER_ID " + "WHERE UDT1.DSNID=" + lDSNID + " AND UDT1.LOGIN_NAME='" + sUser + "'";
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                objConn.Open();
                using (DbReader objReaderManager = objConn.ExecuteReader(sSQL))
                {
                    if (objReaderManager.Read())
                    {
                        iNextLevelManagerId = objReaderManager.GetInt(0);
                    }
                    else
                    {
                        iNextLevelManagerId = 0;
                    }
                }


                if (iNextLevelManagerId != 0)
                {
                    while (iNextLevelManagerId != 0)
                    {
                        iHighestLevelManagerId = iNextLevelManagerId;

                        sSQL = "SELECT ut1.MANAGER_ID FROM USER_TABLE ut1 WHERE ut1.USER_ID=" + iNextLevelManagerId;

                        using (DbReader objReader = objConn.ExecuteReader(sSQL))
                        {
                            if (objReader.Read())
                            {
                                iNextLevelManagerId = objReader.GetInt(0);
                            }
                            else
                            {
                                iNextLevelManagerId = 0;
                            }

                        }
                    }

                    sSQL = "SELECT udt1.LOGIN_NAME FROM USER_DETAILS_TABLE udt1 WHERE udt1.USER_ID=" + iHighestLevelManagerId + " AND udt1.DSNID=" + lDSNID;

                    using (DbReader objReader2 = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader2.Read())
                        {
                            sHighestLevelManagerLoginName += objReader2.GetString(0) + "";
                        }
                        else
                        {
                            sHighestLevelManagerLoginName = "";
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("GetHighestLevelManager", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            return sHighestLevelManagerLoginName;
        }
       
       
    

    }
}
