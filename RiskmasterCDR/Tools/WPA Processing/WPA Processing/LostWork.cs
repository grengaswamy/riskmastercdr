﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Db;

namespace WPAProcessing
{
    class LostWork
    {
        BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(WPAForm.Globalvar.m_iClientId); 
        public int iGetDayCount(string sDate1, string sDate2, int[] iDaysOfWeek)
        {
            int functionReturnValue = 0;
            int iDays = 0;
            int iNewdays = 0;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            if (DateTime.Parse(sDate1) is DateTime && DateTime.Parse(sDate2) is DateTime)
            {
                //UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                //iDays = DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Day, Convert.ToDateTime(sDate1), Convert.ToDateTime(sDate2)) + 1;
                //iDayOfWeekStart =  Convert.ToDateTime(sDate1).DayOfWeek;
                //iDayOfWeekEnd = DateAndTime.WeekDay(Convert.ToDateTime(sDate2));
                iDays = (Convert.ToDateTime(sDate1) - Convert.ToDateTime(sDate2)).Days + 1;
                iDayOfWeekStart = Convert.ToInt32(Convert.ToDateTime(sDate1).DayOfWeek);
                iDayOfWeekEnd = Convert.ToInt32(Convert.ToDateTime(sDate2).DayOfWeek);
                iNewdays = 0;
                for (int i = 1; i <= iDays; i++)
                {
                    // if (iDaysOfWeek[DateAndTime.WeekDay(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, i - 1, Convert.ToDateTime(sDate1)))])
                    //    iNewdays = iNewdays + 1;
                    if (iDaysOfWeek[Convert.ToDateTime(sDate1).AddDays(i - 1).Day] == 1)
                        iNewdays = iNewdays + 1;
                }
            }
            return functionReturnValue;
        }
        public void CalcLostDays()
        {
            string sSQL = null;
            string sDate1 = null;
            string sDate2 = null;
            int lDays = 0;
            DbReader objreader2 = null;
            DbConnection ObjCon = DbFactory.GetDbConnection(WPAForm.Globalvar.m_ConStrRiskmaster);
            // Recalc work loss
            // rs = DB_CreateRecordset(dbGlobalConnect2, "SELECT * FROM PI_X_WORK_LOSS WHERE DATE_RETURNED IS NULL OR DATE_RETURNED = ''", DB_FORWARD_ONLY, 0)
            ObjCon.Open();
            try
            {
                using (DbReader objreader = ObjCon.ExecuteReader("SELECT * FROM PI_X_WORK_LOSS WHERE DATE_RETURNED IS NULL OR DATE_RETURNED = ''"))
                {
                    while (objreader.Read())
                    {
                        if (!string.IsNullOrEmpty(objreader.GetString("DATE_LAST_WORKED")))
                        {
                            sSQL = "SELECT WORK_SUN_FLAG,WORK_MON_FLAG,WORK_TUE_FLAG,WORK_WED_FLAG,WORK_THU_FLAG,WORK_FRI_FLAG,WORK_SAT_FLAG";
                            sSQL = sSQL + " FROM PERSON_INVOLVED WHERE PI_ROW_ID = " + objreader.GetInt("PI_ROW_ID");
                            using (objreader2 = ObjCon.ExecuteReader(sSQL))
                            {
                                while (objreader2.Read())
                                {
                                    sDate1 = objreader.GetString("DATE_LAST_WORKED").Trim() + "";
                                    if (!string.IsNullOrEmpty(sDate1))
                                    {
                                        sDate1 = sDate1.Substring(4, 2) + "/" + sDate1.Substring(6, 2) + "/" + sDate1.Substring(0, 4);
                                        if (DateTime.Parse(sDate1) is DateTime)
                                        {
                                            sDate2 = objreader.GetString("DATE_RETURNED").Trim() + "";
                                            if (!string.IsNullOrEmpty(sDate2))
                                            {
                                                //sDate2 = Strings.Mid(sDate2, 5, 2) + "/" + Strings.Right(sDate2, 2) + "/" + Strings.Left(sDate2, 4);
                                                sDate2 = sDate2.Substring(4, 2) + "/" + sDate2.Substring(6, 2) + "/" + sDate2.Substring(0, 4);
                                            }

                                            if (!string.IsNullOrEmpty(sDate2) && DateTime.Parse(sDate2) is DateTime) //Information.IsDate(sDate2) &
                                            {
                                                // lDays = iCountEmpDays( rs2,  Convert.ToString(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, Convert.ToDateTime(sDate1))),  Convert.ToString(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, Convert.ToDateTime(sDate2))));
                                                lDays = iCountEmpDays(objreader2, Convert.ToDateTime(sDate1).ToString("MM/dd/yyyy"), Convert.ToDateTime(sDate2).ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                // lDays = iCountEmpDays( rs2,  Convert.ToString(DateTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, Convert.ToDateTime(sDate1))),  Convert.ToString(DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, DateAndTime.Today)));
                                                lDays = iCountEmpDays(objreader2, Convert.ToDateTime(sDate1).ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"));
                                            }

                                            if (lDays != objreader.GetInt("DURATION"))
                                            {
                                                sSQL = "UPDATE PI_X_WORK_LOSS SET DURATION = " + lDays + " WHERE PI_WL_ROW_ID = " + objreader.GetInt("PI_WL_ROW_ID");
                                                ObjCon.ExecuteScalar(sSQL);
                                                System.Windows.Forms.Application.DoEvents();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("CalcLostDays", null, false, systemErrors);
            }
            finally
            {
                if (ObjCon != null)
                    ObjCon.Dispose();
            }


        }
        public int iCountEmpDays(DbReader objreader2, string sDate1, string sDate2)
        {
            int functionReturnValue = 0;
            int[] iDaysOfWeek = new int[7];
            functionReturnValue = 0;
            if (DateTime.Parse(sDate1) is DateTime)
            {
                iDaysOfWeek[0] = Convert.ToInt32(objreader2.GetValue("WORK_SUN_FLAG"));
                iDaysOfWeek[1] = Convert.ToInt32(objreader2.GetValue("WORK_MON_FLAG"));
                iDaysOfWeek[2] = Convert.ToInt32(objreader2.GetValue("WORK_TUE_FLAG"));
                iDaysOfWeek[3] = Convert.ToInt32(objreader2.GetValue("WORK_WED_FLAG"));
                iDaysOfWeek[4] = Convert.ToInt32(objreader2.GetValue("WORK_THU_FLAG"));
                iDaysOfWeek[5] = Convert.ToInt32(objreader2.GetValue("WORK_FRI_FLAG"));
                iDaysOfWeek[6] = Convert.ToInt32(objreader2.GetValue("WORK_SAT_FLAG"));
                if (DateTime.Parse(sDate2) is DateTime)
                {
                    functionReturnValue = iGetDayCount(sDate1, sDate2, iDaysOfWeek);
                }
                else
                {
                    functionReturnValue = iGetDayCount(sDate1, DateTime.Now.ToString("yyyy/MM/dd"), iDaysOfWeek);
                }
            }
            return functionReturnValue;

        }
    }
}
