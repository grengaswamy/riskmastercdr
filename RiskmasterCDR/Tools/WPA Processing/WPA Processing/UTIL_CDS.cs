﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPASample
{
    static class UTIL_CDS
    {

       // Methods are Obsolete.
        /*  static short static_bInitCodes_Ignore;
          static string static_bInitCodes_sLastRefresh;
          public static short bInitCodes(ref short bCheckSyncFlag, ref short bLogin)
          {
              short functionReturnValue = 0;
              short bDoCache = 0;

              functionReturnValue = false;
              bDoCache = bLogin;
              // flags whether caching should occur on this call or not

              // if sync checking is desired, check cache option settings to see what to do
              //if (!static_bInitCodes_Ignore & bCheckSyncFlag & !bLogin)
              //    bDoCache = bIsCacheNeeded("CODES", static_bInitCodes_sLastRefresh, static_bInitCodes_Ignore);

              if (bDoCache)
              {
                  ////subsetsystemstatus "Downloading Codes from Database..."
                  //if (bCodesInit(ROCKETCOMLibDTGRocket_definst.DB_GetHdbc(dbLookup)))
                  //    static_bInitCodes_sLastRefresh = VB6.Format(DateAndTime.Today + " " + DateAndTime.TimeOfDay, "YYYYMMDDHHNNSS");
              }

              functionReturnValue = true;
              return functionReturnValue;

          }*/
        // Obsolete Method 
         /* public static string sGetArrayCodeDesc(ref int[] CodeAry, ref short iCodes)
          {

              string sDesc = null;
              short i = 0;

              sDesc = "";
              for (i = 0; i <= iCodes - 2; i++)
              {
                  sDesc = sDesc + sGetCodeDesc(ref CodeAry[i]) + ", ";
              }
              sDesc = sDesc + sGetCodeDesc(ref CodeAry[iCodes - 1]);

              return sDesc;

          }*/
        // Obsolete Method 
         /* public static string sGetCodeDesc(ref int lCode)
          {
              string functionReturnValue = null;

              VB6.FixedLengthString sCode = new VB6.FixedLengthString(128);
              int ln = 0;

              ln = 0;
              functionReturnValue = "";
              if (bInitCodes(ref false, ref false))
              {
                  if (bGetCodeDesc(ROCKETCOMLibDTGRocket_definst.DB_GetHdbc(dbLookup), lCode, sCode.Value, ln))
                      if (ln > 0)
                          functionReturnValue = Strings.Trim(sTrimNull(sCode.Value));
              }
              return functionReturnValue;

          }*/
        // Obsolete Method 
         /* public static string sGetListCodeDesc(ref string sCodes, ref short iCodes)
          {

              string sDesc = null;
              short i = 0;
              string sSel = null;
              short iPtr = 0;

              sSel = sCodes;
              sDesc = "";
              for (i = 1; i <= iCodes; i++)
              {
                  iPtr = Strings.InStr(sSel, ",");
                  sDesc = sDesc + sGetCodeDesc(ref Conversion.Val(Strings.Left(sSel, iPtr - 1))) + ", ";
                  sSel = Strings.Mid(sSel, iPtr + 1);
              }
              return Strings.Left(sDesc, Strings.Len(sDesc) - 1);

          }*/
           //replaced sGetShortCode method with GetShortCode From RMACode
        /* public static string sGetShortCode(ref int lCode)
         {
             string functionReturnValue = null;

             VB6.FixedLengthString sCode = new VB6.FixedLengthString(128);
             int ln = 0;

             ln = 0;
             functionReturnValue = "";
             if (bInitCodes(ref false, ref false))
             {
                 if (bGetShortCode(ROCKETCOMLibDTGRocket_definst.DB_GetHdbc(dbLookup), lCode, sCode.Value, ln))
                     if (ln > 0)
                         functionReturnValue = Strings.Trim(sTrimNull(sCode.Value));
             }
             return functionReturnValue;

         }*/
    }

}
