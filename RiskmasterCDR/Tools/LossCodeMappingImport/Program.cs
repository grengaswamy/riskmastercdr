﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Runtime.Serialization.Formatters.Binary;

namespace LossCodeMappingImport
{
 public   class Program
    {
     private static int m_iClientId = 0;
      public static void Main(string[] args)
        {
            string sSQL = string.Empty;
         
            UserLogin objUserLogin = null;
            LossCodeMapping objManager = null;
            DataSet objDataSet = null;
            string sErrorMsg = string.Empty;
            StringBuilder objStr = null;
            FileStream stream = null;
            Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping iReturnValue = 0;

            string sFileName = string.Empty;
            string sFilePath = string.Empty;
            StreamWriter objStream = null;
          LocalCache objCache = null;
          DataTable objDataTable = null;
            try
            {

                //args = new string[7];
                //args[0] = "RMA141CHP1_POLICY_QA"; //dsnname
                //args[1] = "abk"; //login name
                //args[2] = "False"; // to identify if save is for imported file or for replication :- true if replication else false.
                //args[3] = "2"; // RowId of  LOSS_CODE_MAPPING_FILE_CONTENT 
                //args[4] = "2"; // PolicySystemId from which we need to replicate

                //args[5] = "1"; // PolicySystemIds for which mapping will be saved
                //args[6] = "263"; //Job Id
                string sDSNname = args[0].ToString();
                string sLogin = args[1].ToString();
                objUserLogin = new UserLogin(sLogin, sDSNname, m_iClientId);
                Console.WriteLine("0 ^*^*^ Loss Code Mapping save started ");
                if (objUserLogin.DatabaseId <= 0)
                {
                    Console.WriteLine("0 ^*^*^ Authentication Failed");
                    throw new Exception("Authentication failure.");
                }

                objManager = new LossCodeMapping(objUserLogin.objRiskmasterDatabase.ConnectionString);
                objCache= new LocalCache(objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                if (Conversion.ConvertStrToBool(args[2]))
                {
                    sSQL = " SELECT POLICY_LOB,CVG_TYPE_CODE,LOSS_CODE FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_SYSTEM_ID=" + args[4];

                    objDataSet = DbFactory.GetDataSet(objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL, m_iClientId);
                    if ((objDataSet != null) && (objDataSet.Tables[0] != null))
                    {
                        objDataTable = objDataSet.Tables[0];
                    }

                }
                else
                {
                   objDataTable= GetDataTable(Conversion.ConvertStrToInteger(args[3]),objUserLogin.objRiskmasterDatabase.ConnectionString,objUserLogin.UserId);

                   
                }

                sFilePath = RMConfigurator.TempPath + "\\LossCodeMappingImport\\";
                sFileName = "LossCodeMapping" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".LOG";

                if (objDataTable != null) 
                {
                    objStr = new StringBuilder();
                    if (Conversion.ConvertStrToBool(args[2]))
                    {
                        Console.WriteLine("0 ^*^*^ Replication started ");
                        objStr.AppendLine(" Replication started ");
                    }
                    else
                    {
                        Console.WriteLine("0 ^*^*^ Import started ");
                        objStr.AppendLine(" Import started ");
                    }

                  
                    foreach (string sPolicySystemId in args[5].Split(','))
                    {
                        foreach (DataRow dr in objDataTable.Rows)
                    {
                      
                            iReturnValue = objManager.SaveMapping(Conversion.ConvertStrToInteger(sPolicySystemId), dr[0].ToString(),dr[1].ToString(), dr[2].ToString(), Conversion.ConvertStrToBool(args[2]), ref sErrorMsg);

                            if (!Conversion.ConvertStrToBool(args[2]))
                            {
                                if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.DuplicateMappingExists)
                                {
                                    objStr.AppendLine(" Duplicate mapping exits for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId),objUserLogin.objRiskmasterDatabase.ConnectionString));
                                    Console.WriteLine("0 ^*^*^ Duplicate mapping exits for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString));

                                }
                                else if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.ErrorOccured)
                                {
                                    objStr.AppendLine(" Error occured while saving  mapping  for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId),objUserLogin.objRiskmasterDatabase.ConnectionString));
                                    objStr.AppendLine(" Error message :-");
                                    objStr.AppendLine(sErrorMsg);

                                    Console.WriteLine("0 ^*^*^ Error occured while saving  mapping  for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString) + Environment.NewLine + " Error message :-" + Environment.NewLine + sErrorMsg);

                                }
                            }
                            else
                            {


                                if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.DuplicateMappingExists)
                                {
                                    objStr.AppendLine(" Duplicate mapping exits for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + " Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString));
                                    Console.WriteLine("0 ^*^*^ Duplicate mapping exits for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + " Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString));

                                }
                                else if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.ErrorOccured)
                                {
                                    objStr.AppendLine(" Error occured while saving  mapping  for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + " Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString));
                                    objStr.AppendLine(" Error message :-");
                                    objStr.AppendLine(sErrorMsg);
                                    Console.WriteLine("0 ^*^*^ Error occured while saving  mapping  for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + " Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId), objUserLogin.objRiskmasterDatabase.ConnectionString) + Environment.NewLine + " Error message :-" + Environment.NewLine + sErrorMsg);
                                }
                            }


                        }
                    }




                    if (File.Exists(sFilePath + sFileName))
                    {
                        File.Delete(sFilePath + sFileName);
                    }

                        objStream = new StreamWriter(sFilePath+ sFileName);
                        objStream.Write(objStr.ToString());
                        objStream.Close();
                    

                    objStream = null;
                }




            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                bool rethrow = ExceptionPolicy.HandleException(exc, "Logging Policy");
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream = null;

                }
               
                objStr = null;
                objManager = null;
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
                if (objStream != null)
                {
                    objStream.Close();
                    objStream = null;

                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                FileToDatabase(sFileName,args[6],sFilePath);
            }
        }
      public static void FileToDatabase(string sFileName,string iJobId,string sFilePath)
      {
          FileStream stream = null;
          byte[] fileData = null;
          try
          {
              stream = new FileStream(sFilePath+sFileName, FileMode.Open, FileAccess.Read);
              fileData = new byte[stream.Length];
              stream.Read(fileData, 0, (int)stream.Length);
              stream.Close();
          }
          catch (Exception e)
          {
              Console.WriteLine("0 ^*^*^ Error occured in reading zip file.");
              throw e;
          }
          finally
          {
              stream.Close();
              stream.Dispose();
          }
          try
          {
              Console.WriteLine("0 ^*^*^ Attach file to database.");
              using (DbConnection dbConnection = DbFactory.GetDbConnection(RMConfigurationManager.GetConnectionString("TaskManagerDataSource", m_iClientId)))
              {
                  dbConnection.Open();
                  DbCommand cmdInsert = dbConnection.CreateCommand();
                  DbParameter paramFileId = cmdInsert.CreateParameter();
                  DbParameter paramJobId = cmdInsert.CreateParameter();
                  DbParameter paramFileName = cmdInsert.CreateParameter();
                  DbParameter paramFileData = cmdInsert.CreateParameter();
                  DbParameter paramContentType = cmdInsert.CreateParameter();
                  DbParameter paramLength = cmdInsert.CreateParameter();

                  cmdInsert.CommandText = "INSERT INTO TM_JOBS_DOCUMENT " +
                      "(TM_FILE_ID, JOB_ID, FILE_NAME, FILE_DATA, CONTENT_TYPE, CONTENT_LENGTH) VALUES " +
                      "(~FileId~, ~JobId~, ~FileName~, ~FileData~, ~ContentType~, ~ContentLength~)";

                  paramFileId.Direction = ParameterDirection.Input;
                  paramFileId.Value = int.Parse(iJobId);
                  //paramFileId.Value = JobId;
                  paramFileId.ParameterName = "FileId";
                  paramFileId.SourceColumn = "TM_FILE_ID";
                  cmdInsert.Parameters.Add(paramFileId);

                  paramJobId.Direction = ParameterDirection.Input;
                  paramJobId.Value = int.Parse(iJobId);
                  //paramJobId.Value = JobId;
                  paramJobId.ParameterName = "JobId";
                  paramJobId.SourceColumn = "JOB_ID";
                  cmdInsert.Parameters.Add(paramJobId);

                  paramFileName.Direction = ParameterDirection.Input;
                  paramFileName.Value = sFileName;
                  paramFileName.ParameterName = "FileName";
                  paramFileName.SourceColumn = "FILE_NAME";
                  cmdInsert.Parameters.Add(paramFileName);

                  paramFileData.Direction = ParameterDirection.Input;
                  paramFileData.Value = fileData;
                  paramFileData.ParameterName = "FileData";
                  paramFileData.SourceColumn = "FILE_DATA";
                  cmdInsert.Parameters.Add(paramFileData);

                  paramContentType.Direction = ParameterDirection.Input;
                  paramContentType.Value = "";
                  paramContentType.ParameterName = "ContentType";
                  paramContentType.SourceColumn = "CONTENT_TYPE";
                  cmdInsert.Parameters.Add(paramContentType);

                  paramLength.Direction = ParameterDirection.Input;
                  paramLength.Value = fileData.Length;
                  paramLength.ParameterName = "ContentLength";
                  paramLength.SourceColumn = "CONTENT_LENGTH";
                  cmdInsert.Parameters.Add(paramLength);

                  cmdInsert.ExecuteNonQuery();

              }
              Console.WriteLine("0 ^*^*^ File attached to database successfully.");
              if (File.Exists(sFileName))
                  File.Delete(sFileName);

            
          }
          catch (System.Exception e)
          {
              Console.WriteLine("0 ^*^*^ Error occured in saving zip file to database.");
              throw e;
          }

          finally
          {
              if (fileData != null)
              {
                  fileData = null;
              }
          }

      }
      private static DataTable GetDataTable(int iRowId, string sConnStr,int iUserId)
      {
          byte[] bFileContent = null;
          BinaryWriter Writer = null;
          DataTable oDataTable = null;
          string xlsxFilePath = string.Empty;
          string sheetName = "rmALossCodeMapping";
          LossCodeMapping objManager = null;
          ITempFileInterface objFile = null;
          Dictionary<string, string> parms = null;
          try
          {
              //using (DbReader objRdr = DbFactory.GetDbReader(sConnStr, "SELECT  FILE_CONTENT FROM LOSS_CODE_FILE_CONTENT WHERE ROW_ID=" + iRowId))
              //{
              //    while (objRdr.Read())
              //    {
              //        bFileContent = objRdr.GetAllBytes("FILE_CONTENT");
              //    }
              //}

      
              objFile = TempFileStorageFactory.CreateInstance(StorageTypeEnumeration.TempStorageType.Database);
         parms=     new Dictionary<string, string>();
         parms.Add("sqlFilter", "ROW_ID= "+ iRowId);
         parms.Add("ConnString",sConnStr);


         bFileContent = objFile.Get(parms);

              xlsxFilePath = RMConfigurator.TempPath + "\\LossCodeMappingImport\\ImportFile_TM_" + iUserId + ".xlsx";

              Writer = new BinaryWriter(File.OpenWrite(xlsxFilePath));

              // Writer raw data                
              Writer.Write(bFileContent);
              Writer.Flush();
              Writer.Close();
              Writer = null;
              objManager = new LossCodeMapping(sConnStr);
              oDataTable = objManager.ExtractExcelSheetValuesToDataTable(xlsxFilePath, sheetName);

              if (File.Exists(xlsxFilePath))
                  File.Delete(xlsxFilePath);


              objFile.Remove(parms);

          }
          catch (Exception)
          {
              oDataTable = null;
          }
          finally
          {
              objManager = null;
              if (Writer != null)
              {
                  Writer.Flush();
                  Writer.Close();
                  Writer = null;
              }
              objFile = null;
              parms = null;
          }
          return oDataTable;
      }
      private static string GetPolicySystemName(int iPolicySystemId,string sConnStr)
      {
          string spolicySystemName = string.Empty;
          try
          {
              spolicySystemName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(sConnStr, "SELECT  POLICY_SYSTEM_NAME FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + iPolicySystemId));

          }
          catch (Exception)
          {
              spolicySystemName = string.Empty;
          }
          return spolicySystemName;
      }
    }
}
