﻿//===============================================================================
// Microsoft patterns & practices Enterprise Library Contribution
// Data Access Application Block
//===============================================================================

using System;
using System.Security;
using System.Resources;
using System.Reflection;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Management.Instrumentation;
using System.Runtime.ConstrainedExecution;

[assembly: SecurityPermission(SecurityAction.RequestMinimum)]
[assembly: ReliabilityContract(Consistency.WillNotCorruptState, Cer.None)]
[assembly: AssemblyTitle("Enterprise Library Contribution Data ODP.NET Provider")]
[assembly: AssemblyDescription("Enterprise Library Contribution Data ODP.NET Provider")]
[assembly: AssemblyVersion("4.1.0.0")]
[assembly: Instrumented(@"root\EnterpriseLibrary")]
[assembly: WmiConfiguration(@"root\EnterpriseLibrary", HostingModel = ManagementHostingModel.Decoupled, IdentifyLevel = false)]
[assembly: AllowPartiallyTrustedCallers]
[assembly: SecurityTransparent]
[assembly: NeutralResourcesLanguage("en")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
