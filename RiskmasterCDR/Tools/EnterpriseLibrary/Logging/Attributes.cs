// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Management.Instrumentation;

namespace Microsoft.Practices.EnterpriseLibrary.Logging
{

	// TODO_MIKEHAYT
	// 
	// Added EventSourceType in the future if its needed.

	//---------------------------------------------------------------------------------------

	/// <summary>
	/// Indicates a category that this InstrumentationType belongs to. This attribute should only be used 
	/// on classes/structures that have on one of their base classes/structures the InstrumentationType attribute. 
	/// </summary>
	/// <remarks>
	/// This attribute is only used by the ProjectInstaller class to determine how to layout the eventCategories 
	/// section when creating a default xml configuration file.
	/// </remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	[System.Security.Permissions.PermissionSetAttribute
		 (System.Security.Permissions.SecurityAction.LinkDemand, Name="FullTrust")]
	public sealed class InstrumentationCategoryAttribute : Attribute
    {
		private string name = null;
		private string description = null;

		/// <summary>
		/// Indicates a category that this InstrumentationType belongs to.
		/// </summary>
		/// <param name="name">The category name.</param>
		public InstrumentationCategoryAttribute(string name)
		{
			this.name = name;
		}

		/// <summary>
		/// Gets or sets the name of this category.
		/// </summary>
		public string Name
		{
			get { return name; }
		}

		/// <summary>
		/// Gets or sets the description of this category.
		/// </summary>
		public string Description
		{
			get { return description; }
			set { description = value; }
		}
    }

	/// <summary>
	/// Indicates that the field or property should not exposed when the raised to an EventSource.
	/// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
	[System.Security.Permissions.PermissionSetAttribute
		 (System.Security.Permissions.SecurityAction.LinkDemand, Name="FullTrust")]
	public sealed class InternalAttribute : IgnoreMemberAttribute 
    {
    }

	/// <summary>
	/// Indicates information about how the field or property should be exposed when the raised to an EventSource.
	/// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
	[System.Security.Permissions.PermissionSetAttribute
		 (System.Security.Permissions.SecurityAction.LinkDemand, Name="FullTrust")]
	public sealed class ExposedNameAttribute : ManagedNameAttribute
	{		
		private string exposedName;

		/// <summary>
		/// Constructs an instance with the specified name.
		/// </summary>
		/// <param name="name">Name</param>
		public ExposedNameAttribute(string name) : base(name)
		{			
			this.exposedName = name;
		}

		/// <summary>
		/// Gets the exposed name.
		/// </summary>
		public string ExposedName
		{
			get { return exposedName; }
		}
	}

	/// <summary>
	/// Indicates that this type can be raised to an EventSource.
	/// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
	[System.Security.Permissions.PermissionSetAttribute
		 (System.Security.Permissions.SecurityAction.LinkDemand, Name="FullTrust")]
	public sealed class InstrumentationTypeAttribute : System.Management.Instrumentation.InstrumentationClassAttribute
	{
		/// <summary>
		/// Default constructor that marks the class as a type that can be raised.
		/// </summary>
		public InstrumentationTypeAttribute() : base(InstrumentationType.Event)
		{
		}
	}

	/// <summary>
	/// Specifies that this assembly provides an EnterpriseInstrumentation schema. 
	/// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
	[System.Security.Permissions.PermissionSetAttribute
		 (System.Security.Permissions.SecurityAction.LinkDemand, Name="FullTrust")]
	public sealed class InstrumentationSchemaAttribute : System.Management.Instrumentation.InstrumentedAttribute
	{
		/// <summary>
		/// An attribute that marks this assembly as an EnterpriseInstrumentation schema. Any WMI events are raised to the root\default namespace.
		/// </summary>
		public InstrumentationSchemaAttribute() : base() {}

		/// <summary>
		/// An attribute that marks this assembly as an EnterpriseInstrumentation schema. Any WMI events are raised to the namespace passed.
		/// </summary>
		/// <param name="namespaceName">The namespace to raise WMI events to.</param>
		public InstrumentationSchemaAttribute(string namespaceName) : base(namespaceName) {}

		/// <summary>
		///		An attribute that marks this assembly as an EnterpriseInstrumentation schema. Any WMI events are raised to the namespace passed with the security settings passed as a parameter.
		/// </summary>
		/// <param name="namespaceName">The namespace to raise WMI events to.</param>
		/// <param name="securityDescriptor">String security descriptor that allows only the specified users or groups to run applications that provide the instrumentation supported by this assembly. </param>
		public InstrumentationSchemaAttribute(string namespaceName, string securityDescriptor) : base(namespaceName, securityDescriptor) {}
	}
}
