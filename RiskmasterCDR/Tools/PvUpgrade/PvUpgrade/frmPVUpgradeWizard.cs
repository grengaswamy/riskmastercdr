﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;

using DevComponents.DotNetBar;

using Riskmaster.Application.RMUtilities;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Security;

namespace Riskmaster.Tools.PvUpgrade
{
    public partial class frmPVUpgradeWizard : Office2007Form
    {
        #region variables
        //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
        //We would be upgrading the existing view id for RMNet too
        //private const string PREFIX_NAME = "RMX UPG of ";
        private static string m_DSN = string.Empty;
        private bool m_IsCmdLine = false;
        private static string sLogXml = string.Empty;
        private XmlDocument errXml = null;
        private string m_strUser = string.Empty;
        private string m_strPassword = string.Empty;
        private int m_iDSNId = 0;
        const int ADM_TYPE = 468;

        //R5 changes ..Raman Bhatia
        private string m_RMXPageConnectionString = string.Empty, m_RMXSecConnString = string.Empty;
        private string m_ConnectionString = string.Empty;
        private string m_strSelectedDSN = string.Empty;
        private const string RMXR5_PV_UPGRADE_CAPTION = "RMX R5/R6 PowerView Conversion";  //csingh7 MITS 19085

        // npadhy to Check whether these XMLs present in DB is for a Powerview created by Client or 
        // for a Powerview which is created by the PVUpgrade Tool or from R4 times. If it is created by Client, then we will not override the Powerviews.
        // But if it is created by tool or from R4 times we need to override that
        private const string R4_RELEASE = "R4";
        private const string R5PS2_RELEASE = "R5PS2";

        // npadhy Changes for logging the error and proceeding instead of erroring out
        private const string RMX_PV_UPGRADE_BLANK_LINE = "----------------------------------------";
        private const string RMX_PV_UPGRADE_TIMESTAMP = "Timestamp: ";
        private const string RMX_PV_UPGRADE_INITIALIZE = "An error occured while initalizing the View: ";
        private const string RMX_PV_UPGRADE_UPDATEVIEW = "An error occured while updating the View: ";
        private const string RMX_PV_UPGRADE_SAVEVIEW = "An error occured while saving the View: ";
        private const string RMX_PV_UPGRADE_PICKFROMBASE_RETRIEVE = "An error occured while retrieving the base xml for Pick From Base for the Form: ";
        private const string RMX_PV_UPGRADE_PICKFROMBASE_UPDATE = "An error occured while updating the view xml for Pick From Base for the Form: {0} and View: {1}";
        private const string RMX_PV_UPGRADE_NEWFDMFORMS_RETRIEVE = "An error occured while retrieving the base xml for New FDM Forms for the Form: ";
        private const string RMX_PV_UPGRADE_NEWFDMFORMS_UPDATE = "An error occured while updating the view xml for New FDM Forms for the Form: {0} and View: {1}";
        private const string RMX_PV_UPGRADE_ADMIN_TRACKING = "An error occured while getting the Details for Admin Tracking form: ";
        private const string RMX_PV_UPGRADE_CONVERT_XML2ASPX = "An error occured while converting the xml to aspx for the Form: {0} and View: {1}";
        private const string RMX_PV_UPGRADE_VIEW_COUNT = "An error occured while retrieving the view count for the Form: ";
        private const string RMX_PV_UPGRADE_EXCEPTION_MESSAGE = "The Exception Occured is: ";
        private const string RMX_PV_UPGRADE_STACKTRACE = "Stacktrace: ";
        // npadhy Changes for logging the error and proceeding instead of erroring out

        //rsolanki2: Performance updates 
        private XslCompiledTransform XctModify;
        private int m_iClientId = 0;

        #endregion

        /// <summary>
        /// frmPVUpgradeWizard
        /// </summary>
        /// <param name="strSecConnString"></param>
        /// <param name="strViewConnString"></param>
        public frmPVUpgradeWizard(string strSecConnString, string strViewConnString)
        {
            m_RMXPageConnectionString = strViewConnString;
            m_RMXSecConnString = strSecConnString;
            InitializeComponent();
            string sPath=string.Empty;
            FileInfo Fi ;
            
            try
            {
                //rsolanki2: trying to locate Modify.xslt in default folder.
                sPath = Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "Modify.xslt");
                Fi = new FileInfo(sPath);

                if (!Fi.Exists)
                {
                    //rsolanki2: trying to locate Modify.xslt using RmConfigurator (if not found in the default application directory )

                    sPath = RMConfigurator.AppFilesPath;
                    //sPath = Path.Combine(sPath.Substring(0, sPath.LastIndexOf(@"\")), @"wcfservice\bin\Modify.xslt");
                    sPath = Path.Combine(Path.GetDirectoryName(sPath), @"wcfservice\bin\Modify.xslt");                    
                    Fi = new FileInfo(sPath);

                    if (!Fi.Exists)
                    {
                        throw new Exception();
                    }
                }

                //rsolanki2: Performance updates 
                XctModify = new XslCompiledTransform();
                //XctModify.Load("Modify.xslt");
                XctModify.Load(sPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("file 'Modify.xslt' not found. Please make sure the BasePath entry is updated in App.config file");
                System.Windows.Forms.Application.Exit();
            }
            finally
            {
                Fi=null;
            }
           
        }

        /// <summary>
        /// InitializeComponent
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPVUpgradeWizard));
            this.wizard1 = new DevComponents.DotNetBar.Wizard();
            this.wpConfirmation = new DevComponents.DotNetBar.WizardPage();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.wpDSNSelection = new DevComponents.DotNetBar.WizardPage();
            this.lblWarningWPDS = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lvDatabases = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.dsn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstDsnList = new System.Windows.Forms.ListBox();
            this.wpSelectProcess = new DevComponents.DotNetBar.WizardPage();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lblWarningWPSP = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cbRMX = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbLegacy = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.wpLegacy = new DevComponents.DotNetBar.WizardPage();
            this.lblWarningWPLGCY = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.checkBox1 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.radioRMNet = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.radioRMX = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblViewName = new System.Windows.Forms.Label();
            this.wpRMXCurrent = new DevComponents.DotNetBar.WizardPage();
            this.txtFormName = new System.Windows.Forms.TextBox();
            this.rdbSingleForm = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblWarningWPRMX = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.rdbPVJuris = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.rdbPV = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.rdbJurisData = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.wpStatus = new DevComponents.DotNetBar.WizardPage();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.LblDsnList = new System.Windows.Forms.Label();
            this.LblDsnListHeader = new System.Windows.Forms.Label();
            this.wizard1.SuspendLayout();
            this.wpConfirmation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.wpDSNSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.wpSelectProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.wpLegacy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.wpRMXCurrent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.wpStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // wizard1
            // 
            this.wizard1.BackColor = System.Drawing.Color.White;
            this.wizard1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wizard1.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.wizard1.Cursor = System.Windows.Forms.Cursors.Default;
            this.wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizard1.FinishButtonTabIndex = 3;
            // 
            // 
            // 
            this.wizard1.FooterStyle.BackColor = System.Drawing.Color.White;
            this.wizard1.FooterStyle.BackColorGradientAngle = 90;
            this.wizard1.FooterStyle.BorderBottomWidth = 1;
            this.wizard1.FooterStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.FooterStyle.BorderLeftWidth = 1;
            this.wizard1.FooterStyle.BorderRightWidth = 1;
            this.wizard1.FooterStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Etched;
            this.wizard1.FooterStyle.BorderTopColor = System.Drawing.SystemColors.Control;
            this.wizard1.FooterStyle.BorderTopWidth = 1;
            this.wizard1.FooterStyle.Class = "";
            this.wizard1.FooterStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wizard1.FooterStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.FooterStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.wizard1.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.wizard1.HeaderDescriptionVisible = false;
            this.wizard1.HeaderHeight = 90;
            this.wizard1.HeaderImage = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.WizardBanner3;
            this.wizard1.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.wizard1.HeaderImageSize = new System.Drawing.Size(688, 52);
            // 
            // 
            // 
            this.wizard1.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.HeaderStyle.BackColorGradientAngle = 90;
            this.wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Etched;
            this.wizard1.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.wizard1.HeaderStyle.BorderBottomWidth = 1;
            this.wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.HeaderStyle.BorderLeftWidth = 1;
            this.wizard1.HeaderStyle.BorderRightWidth = 1;
            this.wizard1.HeaderStyle.BorderTopWidth = 1;
            this.wizard1.HeaderStyle.Class = "";
            this.wizard1.HeaderStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.HeaderTitleIndent = 702;
            this.wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.wizard1.Location = new System.Drawing.Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Size = new System.Drawing.Size(700, 550);
            this.wizard1.TabIndex = 0;
            this.wizard1.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.wpConfirmation,
            this.wpDSNSelection,
            this.wpSelectProcess,
            this.wpLegacy,
            this.wpRMXCurrent,
            this.wpStatus});
            this.wizard1.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_FinishButtonClick);
            this.wizard1.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_CancelButtonClick);
            this.wizard1.WizardPageChanging += new DevComponents.DotNetBar.WizardCancelPageChangeEventHandler(this.wizard1_WizardPageChanging);
            // 
            // wpConfirmation
            // 
            this.wpConfirmation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpConfirmation.BackColor = System.Drawing.Color.Transparent;
            this.wpConfirmation.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpConfirmation.Controls.Add(this.labelX8);
            this.wpConfirmation.Controls.Add(this.pictureBox1);
            this.wpConfirmation.Controls.Add(this.labelX1);
            this.wpConfirmation.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpConfirmation.Location = new System.Drawing.Point(7, 102);
            this.wpConfirmation.Name = "wpConfirmation";
            this.wpConfirmation.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpConfirmation.Style.BackColor = System.Drawing.Color.White;
            this.wpConfirmation.Style.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.TopLeft;
            this.wpConfirmation.Style.Class = "";
            this.wpConfirmation.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpConfirmation.StyleMouseDown.Class = "";
            this.wpConfirmation.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpConfirmation.StyleMouseOver.Class = "";
            this.wpConfirmation.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpConfirmation.TabIndex = 8;
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.Class = "";
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.Silver;
            this.labelX8.Location = new System.Drawing.Point(493, 0);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(99, 23);
            this.labelX8.TabIndex = 5;
            this.labelX8.Text = "Confirmation";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.Database;
            this.pictureBox1.Location = new System.Drawing.Point(528, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(90, 114);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(503, 201);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = resources.GetString("labelX1.Text");
            this.labelX1.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.labelX1.WordWrap = true;
            // 
            // wpDSNSelection
            // 
            this.wpDSNSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpDSNSelection.AntiAlias = false;
            this.wpDSNSelection.BackColor = System.Drawing.Color.Transparent;
            this.wpDSNSelection.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpDSNSelection.Controls.Add(this.lblWarningWPDS);
            this.wpDSNSelection.Controls.Add(this.labelX3);
            this.wpDSNSelection.Controls.Add(this.labelX9);
            this.wpDSNSelection.Controls.Add(this.pictureBox2);
            this.wpDSNSelection.Controls.Add(this.labelX2);
            this.wpDSNSelection.Controls.Add(this.lvDatabases);
            this.wpDSNSelection.Controls.Add(this.lstDsnList);
            this.wpDSNSelection.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpDSNSelection.Location = new System.Drawing.Point(7, 102);
            this.wpDSNSelection.Name = "wpDSNSelection";
            this.wpDSNSelection.PageDescription = "Select a Database to Upgrade";
            this.wpDSNSelection.PageTitle = "Database Selection Screen";
            this.wpDSNSelection.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpDSNSelection.Style.Class = "";
            this.wpDSNSelection.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDSNSelection.StyleMouseDown.Class = "";
            this.wpDSNSelection.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDSNSelection.StyleMouseOver.Class = "";
            this.wpDSNSelection.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpDSNSelection.TabIndex = 7;
            // 
            // lblWarningWPDS
            // 
            // 
            // 
            // 
            this.lblWarningWPDS.BackgroundStyle.Class = "";
            this.lblWarningWPDS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarningWPDS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPDS.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPDS.Name = "lblWarningWPDS";
            this.lblWarningWPDS.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPDS.TabIndex = 16;
            this.lblWarningWPDS.Text = "<font color=\"#BA1419\">You must select a database to continue.....</font>";
            this.lblWarningWPDS.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPDS.Visible = false;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(90, 338);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(531, 23);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "Press <b>Next</b> to continue....";
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.Class = "";
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Silver;
            this.labelX9.Location = new System.Drawing.Point(438, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(154, 23);
            this.labelX9.TabIndex = 14;
            this.labelX9.Text = "Database Selection";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.DatabaseSearch;
            this.pictureBox2.Location = new System.Drawing.Point(528, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(90, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(393, 23);
            this.labelX2.TabIndex = 12;
            this.labelX2.Text = "Select dB to <b>UPGRADE</b>";
            // 
            // lvDatabases
            // 
            // 
            // 
            // 
            this.lvDatabases.Border.BorderColor = System.Drawing.Color.LightGray;
            this.lvDatabases.Border.Class = "ListViewBorder";
            this.lvDatabases.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lvDatabases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dsn,
            this.id});
            this.lvDatabases.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvDatabases.ForeColor = System.Drawing.Color.Sienna;
            this.lvDatabases.FullRowSelect = true;
            this.lvDatabases.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvDatabases.Location = new System.Drawing.Point(90, 48);
            this.lvDatabases.MultiSelect = false;
            this.lvDatabases.Name = "lvDatabases";
            this.lvDatabases.Size = new System.Drawing.Size(405, 284);
            this.lvDatabases.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDatabases.TabIndex = 11;
            this.lvDatabases.UseCompatibleStateImageBehavior = false;
            this.lvDatabases.View = System.Windows.Forms.View.Details;
            this.lvDatabases.Click += new System.EventHandler(this.lvDatabases_Click);
            // 
            // dsn
            // 
            this.dsn.Width = 405;
            // 
            // id
            // 
            this.id.Width = 0;
            // 
            // lstDsnList
            // 
            this.lstDsnList.Location = new System.Drawing.Point(90, 103);
            this.lstDsnList.Name = "lstDsnList";
            this.lstDsnList.ScrollAlwaysVisible = true;
            this.lstDsnList.Size = new System.Drawing.Size(405, 199);
            this.lstDsnList.Sorted = true;
            this.lstDsnList.TabIndex = 10;
            this.lstDsnList.Visible = false;
            // 
            // wpSelectProcess
            // 
            this.wpSelectProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpSelectProcess.BackColor = System.Drawing.Color.White;
            this.wpSelectProcess.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpSelectProcess.Controls.Add(this.labelX6);
            this.wpSelectProcess.Controls.Add(this.labelX7);
            this.wpSelectProcess.Controls.Add(this.lblWarningWPSP);            
            this.wpSelectProcess.Controls.Add(this.cbRMX);
            this.wpSelectProcess.Controls.Add(this.cbLegacy);
            this.wpSelectProcess.Controls.Add(this.labelX5);
            this.wpSelectProcess.Controls.Add(this.labelX4);
            this.wpSelectProcess.Controls.Add(this.pictureBox3);
            this.wpSelectProcess.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpSelectProcess.Location = new System.Drawing.Point(7, 102);
            this.wpSelectProcess.Name = "wpSelectProcess";
            this.wpSelectProcess.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpSelectProcess.Style.Class = "";
            this.wpSelectProcess.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpSelectProcess.StyleMouseDown.Class = "";
            this.wpSelectProcess.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpSelectProcess.StyleMouseOver.Class = "";
            this.wpSelectProcess.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpSelectProcess.TabIndex = 10;
            this.wpSelectProcess.Text = "wizardPage1";
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.Class = "";
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(90, 213);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(503, 126);
            this.labelX7.TabIndex = 21;
            this.labelX7.Text = resources.GetString("labelX7.Text");
            // 
            // lblWarningWPSP
            // 
            // 
            // 
            // 
            this.lblWarningWPSP.BackgroundStyle.Class = "";
            this.lblWarningWPSP.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarningWPSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPSP.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPSP.Name = "lblWarningWPSP";
            this.lblWarningWPSP.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPSP.TabIndex = 20;
            this.lblWarningWPSP.Text = "<font color=\"#BA1419\">You must select a process to continue.....</font>";
            this.lblWarningWPSP.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPSP.Visible = false;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.Class = "";
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(90, 338);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(531, 23);
            this.labelX6.TabIndex = 19;
            this.labelX6.Text = "Press <b>Next</b> to continue....";
            // 
            // cbRMX
            // 
            // 
            // 
            // 
            this.cbRMX.BackgroundStyle.Class = "";
            this.cbRMX.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbRMX.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.cbRMX.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cbRMX.Checked = true;
            this.cbRMX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRMX.CheckValue = "Y";
            this.cbRMX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRMX.Location = new System.Drawing.Point(113, 172);
            this.cbRMX.Name = "cbRMX";
            this.cbRMX.Size = new System.Drawing.Size(370, 23);
            this.cbRMX.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.cbRMX.TabIndex = 18;
            this.cbRMX.Text = "Convert Views";//tmalhotra2
            this.cbRMX.TextColor = System.Drawing.Color.Sienna;
            this.cbRMX.Click += new System.EventHandler(this.cbRMX_Click);
            // 
            // cbLegacy
            // 
            // 
            // 
            // 
            this.cbLegacy.BackgroundStyle.Class = "";
            this.cbLegacy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbLegacy.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.cbLegacy.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cbLegacy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLegacy.Location = new System.Drawing.Point(113, 143);
            this.cbLegacy.Name = "cbLegacy";
            this.cbLegacy.Size = new System.Drawing.Size(370, 23);
            this.cbLegacy.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.cbLegacy.TabIndex = 17;
            this.cbLegacy.Text = "Upgrade <b>RISKMASTER</b> Views";//tmalhotra2
            this.cbLegacy.TextColor = System.Drawing.Color.Sienna;
            this.cbLegacy.Click += new System.EventHandler(this.cbLegacy_Click);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.Class = "";
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(90, 114);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(393, 23);
            this.labelX5.TabIndex = 16;
            this.labelX5.Text = "Select desired <b>PROCESS</b>......";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Silver;
            this.labelX4.Location = new System.Drawing.Point(439, 0);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(154, 23);
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "Select Process";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.CheckMark;
            this.pictureBox3.Location = new System.Drawing.Point(529, 29);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(64, 64);
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            // 
            // wpLegacy
            // 
            this.wpLegacy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpLegacy.AntiAlias = false;
            this.wpLegacy.BackColor = System.Drawing.Color.Transparent;
            this.wpLegacy.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpLegacy.Controls.Add(this.lblWarningWPLGCY);
            this.wpLegacy.Controls.Add(this.labelX24);
            this.wpLegacy.Controls.Add(this.labelX13);
            this.wpLegacy.Controls.Add(this.checkBox1);
            this.wpLegacy.Controls.Add(this.radioRMNet);
            this.wpLegacy.Controls.Add(this.radioRMX);
            this.wpLegacy.Controls.Add(this.labelX12);
            this.wpLegacy.Controls.Add(this.labelX10);
            this.wpLegacy.Controls.Add(this.pictureBox4);
            this.wpLegacy.Controls.Add(this.lblViewName);
            this.wpLegacy.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpLegacy.Location = new System.Drawing.Point(7, 102);
            this.wpLegacy.Name = "wpLegacy";
            this.wpLegacy.PageDescription = "Upgrade Powerviews";//tmalhotra2
            this.wpLegacy.PageTitle = "Upgrade Powerviews";//tmalhotra2
            this.wpLegacy.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpLegacy.Style.Class = "";
            this.wpLegacy.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpLegacy.StyleMouseDown.Class = "";
            this.wpLegacy.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpLegacy.StyleMouseOver.Class = "";
            this.wpLegacy.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpLegacy.TabIndex = 9;
            // 
            // lblWarningWPLGCY
            // 
            // 
            // 
            // 
            this.lblWarningWPLGCY.BackgroundStyle.Class = "";
            this.lblWarningWPLGCY.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarningWPLGCY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPLGCY.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPLGCY.Name = "lblWarningWPLGCY";
            this.lblWarningWPLGCY.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPLGCY.TabIndex = 26;
            this.lblWarningWPLGCY.Text = "<font color=\"#BA1419\">You must select a view to upgrade.....</font>";
            this.lblWarningWPLGCY.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPLGCY.Visible = false;
            // 
            // labelX24
            // 
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.Class = "";
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelX24.Location = new System.Drawing.Point(132, 221);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(496, 23);
            this.labelX24.TabIndex = 25;
            this.labelX24.Text = "(<i>Note:</i> Selecting this option would<b> IGNORE</b> all errors and upgrade th" +
                "e PowerViews)";
            // 
            // labelX13
            // 
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.Class = "";
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.Location = new System.Drawing.Point(90, 338);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(503, 23);
            this.labelX13.TabIndex = 23;
            this.labelX13.Text = "Press <b>Next</b> to begin upgrade....";
            // 
            // checkBox1
            // 
            // 
            // 
            // 
            this.checkBox1.BackgroundStyle.Class = "";
            this.checkBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBox1.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(113, 201);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(370, 23);
            this.checkBox1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.checkBox1.TabIndex = 22;
            this.checkBox1.Text = "Ignore <font color=\"#BA1419\"><b>ERRORS</b></font>";
            this.checkBox1.TextColor = System.Drawing.Color.Sienna;
            // 
            // radioRMNet
            // 
            // 
            // 
            // 
            this.radioRMNet.BackgroundStyle.Class = "";
            this.radioRMNet.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.radioRMNet.BackgroundStyle.TextColor = System.Drawing.Color.Silver;
            this.radioRMNet.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.radioRMNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioRMNet.Location = new System.Drawing.Point(113, 172);
            this.radioRMNet.Name = "radioRMNet";
            this.radioRMNet.Size = new System.Drawing.Size(370, 23);
            this.radioRMNet.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.radioRMNet.TabIndex = 21;
            this.radioRMNet.Text = "Upgrade <b>RMNet</b> Views";
            this.radioRMNet.TextColor = System.Drawing.Color.Silver;
            this.radioRMNet.Enabled = false;//commenting for mits 29932
            

            // 
            // radioRMX
            // 
            // 
            // 
            // 
            this.radioRMX.BackgroundStyle.Class = "";
            this.radioRMX.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.radioRMX.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.radioRMX.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.radioRMX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioRMX.Location = new System.Drawing.Point(113, 143);
            this.radioRMX.Name = "radioRMX";
            this.radioRMX.Size = new System.Drawing.Size(370, 23);
            this.radioRMX.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.radioRMX.TabIndex = 20;
            this.radioRMX.Text = "Upgrade <b>RISKMASTER</b> Views";//tmalhotra2
            this.radioRMX.TextColor = System.Drawing.Color.Sienna;
            this.radioRMX.Checked = true;
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.Class = "";
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.Location = new System.Drawing.Point(90, 114);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(455, 23);
            this.labelX12.TabIndex = 19;
            this.labelX12.Text = "Select desired power view upgrade options....";//tmalhotra2
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.Class = "";
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.Silver;
            this.labelX10.Location = new System.Drawing.Point(439, 0);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(154, 23);
            this.labelX10.TabIndex = 18;
            this.labelX10.Text = "Select Options";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.CheckMark;
            this.pictureBox4.Location = new System.Drawing.Point(529, 29);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(64, 64);
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // lblViewName
            // 
            this.lblViewName.Location = new System.Drawing.Point(3, 332);
            this.lblViewName.Name = "lblViewName";
            this.lblViewName.Size = new System.Drawing.Size(419, 24);
            this.lblViewName.TabIndex = 16;
            // 
            // wpRMXCurrent
            // 
            this.wpRMXCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpRMXCurrent.BackColor = System.Drawing.Color.White;
            this.wpRMXCurrent.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpRMXCurrent.Controls.Add(this.txtFormName);
            this.wpRMXCurrent.Controls.Add(this.rdbSingleForm);
            this.wpRMXCurrent.Controls.Add(this.lblWarningWPRMX);
            this.wpRMXCurrent.Controls.Add(this.labelX15);
            this.wpRMXCurrent.Controls.Add(this.rdbPVJuris);
            this.wpRMXCurrent.Controls.Add(this.rdbPV);
            this.wpRMXCurrent.Controls.Add(this.rdbJurisData);
            this.wpRMXCurrent.Controls.Add(this.labelX14);
            this.wpRMXCurrent.Controls.Add(this.labelX11);
            this.wpRMXCurrent.Controls.Add(this.pictureBox5);
            this.wpRMXCurrent.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpRMXCurrent.Location = new System.Drawing.Point(7, 102);
            this.wpRMXCurrent.Name = "wpRMXCurrent";
            this.wpRMXCurrent.PageDescription = "Upgrade RMXR5 and newer Powerviews";
            this.wpRMXCurrent.PageTitle = "Upgrade RMXR5 and newer Powerviews";
            this.wpRMXCurrent.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpRMXCurrent.Style.Class = "";
            this.wpRMXCurrent.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpRMXCurrent.StyleMouseDown.Class = "";
            this.wpRMXCurrent.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpRMXCurrent.StyleMouseOver.Class = "";
            this.wpRMXCurrent.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpRMXCurrent.TabIndex = 11;
            this.wpRMXCurrent.Text = "wizardPage1";
            // 
            // txtFormName
            // 
            this.txtFormName.Enabled = false;
            this.txtFormName.Location = new System.Drawing.Point(350, 288);
            this.txtFormName.Name = "txtFormName";
            this.txtFormName.Size = new System.Drawing.Size(158, 20);
            this.txtFormName.TabIndex = 31;
            // 
            // rdbSingleForm
            // 
            // 
            // 
            // 
            this.rdbSingleForm.BackgroundStyle.Class = "";
            this.rdbSingleForm.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rdbSingleForm.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.rdbSingleForm.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.rdbSingleForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbSingleForm.Location = new System.Drawing.Point(99, 288);
            this.rdbSingleForm.Name = "rdbSingleForm";
            this.rdbSingleForm.Size = new System.Drawing.Size(244, 23);
            this.rdbSingleForm.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rdbSingleForm.TabIndex = 30;
            this.rdbSingleForm.Text = "Upgrade a Specific Form";
            this.rdbSingleForm.TextColor = System.Drawing.Color.Sienna;
            this.rdbSingleForm.CheckedChanged += new System.EventHandler(this.rdbSingleForm_CheckedChanged);
            // 
            // lblWarningWPRMX
            // 
            // 
            // 
            // 
            this.lblWarningWPRMX.BackgroundStyle.Class = "";
            this.lblWarningWPRMX.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarningWPRMX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPRMX.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPRMX.Name = "lblWarningWPRMX";
            this.lblWarningWPRMX.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPRMX.TabIndex = 29;
            this.lblWarningWPRMX.Text = "<font color=\"#BA1419\">You must select one of the upgrade options to continue....." +
                "</font>";
            this.lblWarningWPRMX.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPRMX.Visible = false;
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.Class = "";
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.Location = new System.Drawing.Point(90, 338);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(503, 23);
            this.labelX15.TabIndex = 28;
            this.labelX15.Text = "Press <b>Next</b> to begin upgrade....";
            // 
            // rdbPVJuris
            // 
            // 
            // 
            // 
            this.rdbPVJuris.BackgroundStyle.Class = "";
            this.rdbPVJuris.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rdbPVJuris.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.rdbPVJuris.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.rdbPVJuris.Checked = true;
            this.rdbPVJuris.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rdbPVJuris.CheckValue = "Y";
            this.rdbPVJuris.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPVJuris.Location = new System.Drawing.Point(99, 201);
            this.rdbPVJuris.Name = "rdbPVJuris";
            this.rdbPVJuris.Size = new System.Drawing.Size(370, 23);
            this.rdbPVJuris.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rdbPVJuris.TabIndex = 27;
            this.rdbPVJuris.Text = "Upgrade PowerViews and Jurisdictional Data";
            this.rdbPVJuris.TextColor = System.Drawing.Color.Sienna;
            // 
            // rdbPV
            // 
            // 
            // 
            // 
            this.rdbPV.BackgroundStyle.Class = "";
            this.rdbPV.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rdbPV.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.rdbPV.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.rdbPV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPV.Location = new System.Drawing.Point(99, 230);
            this.rdbPV.Name = "rdbPV";
            this.rdbPV.Size = new System.Drawing.Size(370, 23);
            this.rdbPV.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rdbPV.TabIndex = 24;
            this.rdbPV.Text = "Upgrade PowerViews only";
            this.rdbPV.TextColor = System.Drawing.Color.Sienna;
            // 
            // rdbJurisData
            // 
            // 
            // 
            // 
            this.rdbJurisData.BackgroundStyle.Class = "";
            this.rdbJurisData.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rdbJurisData.BackgroundStyle.TextColor = System.Drawing.Color.Sienna;
            this.rdbJurisData.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.rdbJurisData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbJurisData.Location = new System.Drawing.Point(99, 259);
            this.rdbJurisData.Name = "rdbJurisData";
            this.rdbJurisData.Size = new System.Drawing.Size(370, 23);
            this.rdbJurisData.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rdbJurisData.TabIndex = 23;
            this.rdbJurisData.Text = "Upgrade Jurisdictional Data only";
            this.rdbJurisData.TextColor = System.Drawing.Color.Sienna;
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.Class = "";
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.Location = new System.Drawing.Point(90, 114);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(455, 36);
            this.labelX14.TabIndex = 20;
            this.labelX14.Text = "Select desired <b>RISKMASTER</b> Power View upgrade options....";//tmalhotra2
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.Class = "";
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Silver;
            this.labelX11.Location = new System.Drawing.Point(439, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(154, 23);
            this.labelX11.TabIndex = 19;
            this.labelX11.Text = "Select Options";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.CheckMark;
            this.pictureBox5.Location = new System.Drawing.Point(529, 29);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 64);
            this.pictureBox5.TabIndex = 18;
            this.pictureBox5.TabStop = false;
            // 
            // wpStatus
            // 
            this.wpStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpStatus.BackColor = System.Drawing.Color.White;
            this.wpStatus.CancelButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpStatus.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpStatus.Controls.Add(this.labelX18);
            this.wpStatus.Controls.Add(this.lblStatus);
            this.wpStatus.Controls.Add(this.labelX16);
            this.wpStatus.Controls.Add(this.pictureBox6);
            this.wpStatus.Controls.Add(this.progressBar1);
            this.wpStatus.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpStatus.Location = new System.Drawing.Point(7, 102);
            this.wpStatus.Name = "wpStatus";
            this.wpStatus.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpStatus.Style.Class = "";
            this.wpStatus.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpStatus.StyleMouseDown.Class = "";
            this.wpStatus.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpStatus.StyleMouseOver.Class = "";
            this.wpStatus.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpStatus.TabIndex = 12;
            this.wpStatus.Text = "wizardPage1";
            // 
            // labelX18
            // 
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.Class = "";
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX18.Location = new System.Drawing.Point(90, 231);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(455, 121);
            this.labelX18.TabIndex = 23;
            this.labelX18.Text = "When complete press <b>Back </b>to return to <i>Database Selection</i> screen, or" +
                " <b>Finish </b>to close application.";
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(90, 114);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(455, 23);
            this.lblStatus.TabIndex = 22;
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.Class = "";
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.Silver;
            this.labelX16.Location = new System.Drawing.Point(439, 0);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(154, 23);
            this.labelX16.TabIndex = 21;
            this.labelX16.Text = "Processing";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Riskmaster.Tools.PvUpgrade.Properties.Resources.DatabaseEdit;
            this.pictureBox6.Location = new System.Drawing.Point(529, 29);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(64, 64);
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.progressBar1.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.progressBar1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBar1.BackgroundStyle.BorderBottomWidth = 1;
            this.progressBar1.BackgroundStyle.BorderColor = System.Drawing.Color.LightGray;
            this.progressBar1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBar1.BackgroundStyle.BorderLeftWidth = 1;
            this.progressBar1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBar1.BackgroundStyle.BorderRightWidth = 1;
            this.progressBar1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBar1.BackgroundStyle.BorderTopWidth = 1;
            this.progressBar1.BackgroundStyle.Class = "";
            this.progressBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.progressBar1.ChunkColor = System.Drawing.Color.CornflowerBlue;
            this.progressBar1.ChunkColor2 = System.Drawing.Color.White;
            this.progressBar1.ChunkGradientAngle = 90;
            this.progressBar1.Location = new System.Drawing.Point(90, 143);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(508, 28);
            this.progressBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.progressBar1.TabIndex = 19;
            // 
            // LblDsnList
            // 
            this.LblDsnList.AutoSize = true;
            this.LblDsnList.BackColor = System.Drawing.Color.White;
            this.LblDsnList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDsnList.Location = new System.Drawing.Point(137, 75);
            this.LblDsnList.Name = "LblDsnList";
            this.LblDsnList.Size = new System.Drawing.Size(66, 13);
            this.LblDsnList.TabIndex = 4;
            this.LblDsnList.Text = "LblDsnList";
            this.LblDsnList.Visible = false;
            // 
            // LblDsnListHeader
            // 
            this.LblDsnListHeader.AutoSize = true;
            this.LblDsnListHeader.BackColor = System.Drawing.Color.White;
            this.LblDsnListHeader.Location = new System.Drawing.Point(8, 75);
            this.LblDsnListHeader.Name = "LblDsnListHeader";
            this.LblDsnListHeader.Size = new System.Drawing.Size(124, 13);
            this.LblDsnListHeader.TabIndex = 3;
            this.LblDsnListHeader.Text = "DataSource(s) selected :";
            this.LblDsnListHeader.Visible = false;
            // 
            // frmPVUpgradeWizard
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(700, 550);
            this.Controls.Add(this.LblDsnList);
            this.Controls.Add(this.LblDsnListHeader);
            this.Controls.Add(this.wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmPVUpgradeWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Powerview Upgrade Wizard";
            this.Load += new System.EventHandler(this.frmPVUpgradeWizard_Load);
            this.wizard1.ResumeLayout(false);
            this.wpConfirmation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.wpDSNSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.wpSelectProcess.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.wpLegacy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.wpRMXCurrent.ResumeLayout(false);
            this.wpRMXCurrent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.wpStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// Loads the contents of the PowerView upgrade form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPVUpgradeWizard_Load(object sender, EventArgs e)
        {
            DataTable dtDSNs = RiskmasterDatabase.GetRiskmasterDatabases(m_iClientId);
            bool bRMNet = false;//asingh263 mits 29932
            //added list view to stay consistent with dB Upgrade Wizard - prg 04/10/09
            foreach (DataRow dtRow in dtDSNs.Rows)
            {
                ListViewItem lvItem = new ListViewItem { Text=dtRow["DSN"].ToString() };

                lvItem.SubItems.Add(dtRow["DSNID"].ToString());
                lvItem.SubItems.Add(dtRow["CONNECTION_STRING"].ToString());

                lvDatabases.Items.Add(lvItem);
                
            }//foreach
            //asingh263 mits 29932 starts
            if (!string.IsNullOrEmpty(RMConfigurationManager.GetAppSetting("RMnetOption")))
            {
                bRMNet = Conversion.ConvertStrToBool(RMConfigurationManager.GetAppSetting("RMnetOption").ToString());
                if (bRMNet)
                {
                    this.radioRMNet.Enabled = true;
                    this.radioRMNet.TextColor = System.Drawing.Color.Sienna;
                }
                else
                {
                    this.radioRMNet.Enabled = false;
                    this.radioRMNet.TextColor = System.Drawing.Color.Silver;
                }
            }
            //asingh263 mits 29932 ends 
        }

        /// <summary>
        /// Gets the database information that the user selects
        /// </summary>
        private void GetDSNSelections()
        {
            const int DSN_ID = 1; //Populated as the value for the ListViewSubItem
            const int DSN_CONNECTION_STRING = 2; //Populated as the value for the ListViewSubItem

            LblDsnList.Text = string.Empty;

            //changed to retrieve from list view added - prg 04/10/09
            foreach (ListViewItem lvItem in lvDatabases.SelectedItems)
            {
                m_strSelectedDSN = lvItem.Text.ToString();

                //Get the individually selected DSN ID
                m_iDSNId = Convert.ToInt32(lvItem.SubItems[DSN_ID].Text.ToString());

                //Get the individually selected connection strings
                m_ConnectionString = lvItem.SubItems[DSN_CONNECTION_STRING].Text.ToString();

                //rsolanki2: mits 21693
                LblDsnList.Text = string.Concat(LblDsnList.Text, lvItem.Text, ", ");
            }

            //rsolanki2: mits 21693
            LblDsnList.Text = LblDsnList.Text.Substring(0, LblDsnList.Text.Length - 2);
            LblDsnList.Visible = true;
            LblDsnListHeader.Visible = true;

        } // method: GetDSNSelections

        #region Convert Legacy PowerViews
        /// <summary>
        /// Converts Legacy PowerViews to RMX compatibility
        /// </summary>
        private void ConvertLegacyPV()
        {
            string sSQL = string.Empty;

            DbReader oReader = null;
            int iViewID = 0;
            string sViewName = string.Empty;
            string sViewHome = string.Empty;
            string sViewDesc = string.Empty;
            string sPageMenu = string.Empty;
            string sMapFile = string.Empty;
            string sListFile = string.Empty;
            string sIgnoreChangeFile = string.Empty;
            DataSet oDSBaseView = null;
            DbTransaction oTransaction = null;
            DbCommand oCommand = null;
            DbConnection oConnection = null;
            XmlDocument oFormListDoc = null;
            XmlDocument oControlMapDoc = null;

            bool bErr = false;
            bool isRMXViewUpgrade = true;
            string sControlType = string.Empty;

            TextWriterTraceListener oTraceListener = null;
            TraceSwitch oTraceSwitch = null;

            PowerView oPView = null;
            ArrayList oPViewList = null;

            DataSet ds = null;
            PVFormEdit objPVFormEdit = null;
            string sFormName = string.Empty;
            XmlDocument xdocInput = new XmlDocument();
            XmlDocument xdocOutput = new XmlDocument();
            bool bTransactionCommitted = false;

            ArrayList alIgnoreFormNames = new ArrayList();
            Hashtable htChangeFormNames = new Hashtable();
            ArrayList alPickBaseFormNames = new ArrayList();
            ArrayList alNewFDMFormNames = new ArrayList();//Added by Shivendu for MITS 17587
            try
            {
                if (radioRMNet.Checked)
                {
                    isRMXViewUpgrade = false;
                } // if

                //Check for the mapping files
                string sPath = AppDomain.CurrentDomain.BaseDirectory;
                sMapFile = String.Format(@"{0}\Mapping.xml", AppDomain.CurrentDomain.BaseDirectory);
                sListFile = String.Format(@"{0}\formlist.xml", AppDomain.CurrentDomain.BaseDirectory);
                sIgnoreChangeFile = String.Format(@"{0}\Ignore_Change.xml", AppDomain.CurrentDomain.BaseDirectory);
                if (!File.Exists(sMapFile) || !File.Exists(sListFile))
                {
                    if (!m_IsCmdLine)
                    {
                        MessageBox.Show("Mapping.xml or formlist.xml file missing");
                        return;
                    }
                    else
                    {
                        throw new Exception("Mapping.xml or formlist.xml file missing");
                    }
                }

                //moved to first wizard page - prg 04/10/09
                //Incase its not executed from command line prompt for a back up
                //if (MessageBox.Show("Please be ABSOLUTELY certain that a backup of your database or database server has been performed before proceeding. " + "\n" +
                //    "Are you sure you want to proceed with the upgrade process? " + "\n" +
                //    "Computer Sciences Corporation cannot be held responsible for unauthorized or unapproved use of this program.  " + "\n" + "\n" +
                //    "Yes will upgrade the existing POWERVIEWS in " + m_strSelectedDSN + " to RISKMASTER X compatible form. ", "Confirm Upgrade",
                //    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                //    return;

                //Initialize trace switch
                string sLogFile = String.Format("{0}PvUpgrade_{1}.log", AppDomain.CurrentDomain.BaseDirectory, m_strSelectedDSN);
                string sErrorLogFile = String.Format("{0}PvUpgrade_{1}Error.log", AppDomain.CurrentDomain.BaseDirectory, m_strSelectedDSN);
                oTraceListener = new TextWriterTraceListener(sLogFile);
                Trace.Listeners.Add(oTraceListener);
                Trace.AutoFlush = true;
                oTraceSwitch = new TraceSwitch("mySwitch", "Entire application");

                this.Cursor = Cursors.WaitCursor;

                //Get base views for the database				
                //Raman Bhatia: Base Views now reside in RMX_Page

                //oDSBaseView = DbFactory.GetDataSet(sConnectionString, "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = 0");
                //oDSBaseView = DbFactory.GetDataSet(m_RMXPageConnectionString, "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND FORM_NAME NOT LIKE 'admintracking|%'");

                StringBuilder strSQL = new StringBuilder();
                Dictionary<string, string> dictParams = new Dictionary<string, string>();


                strSQL.Append("SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 ");
                strSQL.AppendFormat(string.Format("AND DATA_SOURCE_ID = {0} ", "~DSN_ID~"));
                strSQL.AppendFormat(string.Format("AND FORM_NAME NOT LIKE 'admintracking|%'"));

                dictParams.Add("DSN_ID", m_iDSNId.ToString());
                oDSBaseView = DbFactory.ExecuteDataSet(m_RMXPageConnectionString, strSQL.ToString(), dictParams, m_iClientId);

                oFormListDoc = GetFormList(sListFile, m_strSelectedDSN, m_strUser, m_strPassword);
                oControlMapDoc = new XmlDocument();
                oControlMapDoc.Load(sMapFile);

                //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
                //We would be upgrading the existing view id for RMNet too

                //Get the View List. If it's RMNet view upgrade, exclude RMNet views which were upgraded before 
                //(There exist views with name beginning with "RMX Upgrade of...")
                oPViewList = new ArrayList();


                //if (isRMXViewUpgrade)
                //{
                //Modified by Navdeep -  added DSN ID in query 
                sSQL = String.Format("SELECT * FROM NET_VIEWS WHERE VIEW_ID > 0 AND DATA_SOURCE_ID = {0} ORDER BY VIEW_NAME", m_iDSNId);
                /*    
                }
                    else
                    {
                        //Modified by Navdeep -  added DSN ID in query 
                        sSQL = "SELECT * FROM NET_VIEWS nv1 WHERE NOT EXISTS " +
                            "(SELECT * FROM NET_VIEWS nv2 WHERE nv2.VIEW_NAME = '" + PREFIX_NAME + "' + nv1.VIEW_NAME) AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " ORDER BY nv1.VIEW_NAME";
                    }
                 */

                oReader = DbFactory.GetDbReader(m_RMXPageConnectionString, sSQL);
                while (oReader.Read())
                {
                    try
                    {
                        sViewName = oReader.GetString("VIEW_NAME");
                        sViewHome = oReader.GetString("HOME_PAGE");
                        sViewDesc = oReader.GetString("VIEW_DESC");
                        sPageMenu = oReader.GetString("PAGE_MENU");
                        iViewID = oReader.GetInt32("VIEW_ID");
                        oPView = new PowerView(oTraceListener, oTraceSwitch);
                        oPView.Initialization(iViewID, sViewName, m_strSelectedDSN, m_strUser, m_strPassword,
                            m_ConnectionString, oFormListDoc, oControlMapDoc, oDSBaseView,
                            sViewHome, sViewDesc, sPageMenu);
                        oPViewList.Add(oPView);
                    }
                    catch (Exception ex)
                    {
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_INITIALIZE + sViewName);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                    }

                }
                oReader.Close();
                // rsolanki2: jan 15 2009: showing a user friendly message in case the powerviews are not there 
                if (!isRMXViewUpgrade && oPViewList.Count == 0)
                {
                    MessageBox.Show("No RmNet Powerviews found for upgrade.");
                    return;
                }

                progressBar1.Maximum = oPViewList.Count;

                for (int i = 0; i < oPViewList.Count; i++)
                {

                    oPView = (PowerView)oPViewList[i];
                    try
                    {
                        progressBar1.PerformStep();
                        UpdateStatus(String.Format("Updating view: {0}", oPView.ViewName));

                        //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
                        //We would be upgrading the existing view id for RMNet too

                        oPView.IsRMNetView = !isRMXViewUpgrade;
                        oPView.GetExistingFormXMLs(m_iDSNId.ToString());

                        oPView.DSNID = m_iDSNId;
                        if ((isRMXViewUpgrade && (!oPView.IsRMNetView)) || ((!isRMXViewUpgrade) && oPView.IsRMNetView))
                        {
                            oPView.UpgradeView(isRMXViewUpgrade);
                        }

                    }
                    catch (Exception ex)
                    {
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_UPDATEVIEW + oPView.ViewName);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                    }

                }

                //R5.. Set views for the database				
                //Navdeep: Views now reside in RMX_Page
                oConnection = DbFactory.GetDbConnection(m_RMXPageConnectionString);
                oConnection.Open();

                if (!isRMXViewUpgrade && oPView.IsRMNetView)
                {
                    oCommand = oConnection.CreateCommand();
                    oTransaction = oConnection.BeginTransaction();
                    oCommand.Transaction = oTransaction;
                }
                for (int i = 0; i < oPViewList.Count; i++)
                {

                    oPView = (PowerView)oPViewList[i];
                    try
                    {
                        progressBar1.Value = i + 1;

                        UpdateStatus("Updating database for view: " + oPView.ViewName);

                        //R5: We also need to set DSNID now
                        oPView.DSNID = m_iDSNId;
                        if (isRMXViewUpgrade && !oPView.IsRMNetView)
                        {

                            oPView.SaveUpgradedView();
                        }
                        else if (!isRMXViewUpgrade && oPView.IsRMNetView)
                        {

                            oPView.SaveUpgradedView(oCommand);
                        }
                    }
                    catch (Exception ex)
                    {
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_SAVEVIEW + oPView.ViewName);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                    }

                }

                //Commit Transaction
                if (oCommand != null)
                {
                    if (!bErr || this.checkBox1.Checked)
                    {
                        //Insert default views for non-pv forms					
                        oCommand.Transaction.Commit();
                        bTransactionCommitted = true;
                    }
                    else
                    {
                        if (oCommand.Transaction != null)
                            oCommand.Transaction.Rollback();
                    }
                }

                // If the RMNet views have been updated then we need to run 

                if (!isRMXViewUpgrade)
                {
                    strSQL.Remove(0, strSQL.Length);
                    strSQL.Append("SELECT FORM_NAME,VIEW_XML, NET_VIEW_FORMS.VIEW_ID VIEW_ID, VIEW_NAME FROM NET_VIEW_FORMS ");
                    strSQL.Append(" LEFT OUTER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID=NET_VIEWS.VIEW_ID ");
                    strSQL.Append(" WHERE UPPER(FORM_NAME) NOT LIKE '%LIST.XML' AND UPPER(FORM_NAME) NOT LIKE '%.XSL' AND");
                    strSQL.Append(" UPPER(FORM_NAME) NOT LIKE '%.SCC' AND NET_VIEW_FORMS.DATA_SOURCE_ID ='" + m_iDSNId.ToString() + "' AND NET_VIEWS.VIEW_ID != 0");

                    ds = DbFactory.GetDataSet(m_RMXPageConnectionString, strSQL.ToString(), m_iClientId);

                    objPVFormEdit = new Riskmaster.Application.RMUtilities.PVFormEdit(m_strUser, m_strPassword, m_DSN, m_iDSNId.ToString(), m_ConnectionString, m_iClientId);

                    GetIgnoreChangeFormNames(sIgnoreChangeFile, ref alIgnoreFormNames, ref htChangeFormNames);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        progressBar1.Value = 0;
                        int iRecordCount = 0;
                        progressBar1.Maximum = ds.Tables[0].Rows.Count;
                        //oConnection.Open();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {

                            UpdateStatus("Updating database for view: " + sViewName);
                            sFormName = row["FORM_NAME"].ToString();
                            sViewName = row["VIEW_NAME"].ToString();
                            try
                            {
                                //progressBar1.PerformStep();
                                progressBar1.Value = iRecordCount++;
                                if (alIgnoreFormNames.Contains(sFormName))
                                {
                                    continue;
                                }

                                if (htChangeFormNames.ContainsKey(sFormName))
                                {
                                    sSQL = "UPDATE NET_VIEW_FORMS SET FORM_NAME = '" + htChangeFormNames[sFormName] + "' WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + Convert.ToInt32(row["VIEW_ID"]) + " AND FORM_NAME = '" + sFormName + "'";

                                    oCommand = oConnection.CreateCommand();
                                    oCommand.CommandText = sSQL;
                                    oCommand.ExecuteNonQuery();

                                    sFormName = htChangeFormNames[sFormName].ToString();
                                }
                                xdocInput.LoadXml(GetTemplate(sFormName, Convert.ToInt32(row["VIEW_ID"])));

                                xdocOutput = objPVFormEdit.Get(xdocInput);

                                CreateSaveXmlIn(ref xdocOutput);
                            }
                            catch (Exception ex)
                            {
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_UPDATEVIEW + sViewName + " and Form: " + sFormName);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            }
                        }
                    }


                }
                string sUpgradeFrom = string.Empty;
                // Update the XMLS for Policyenh, policybilling same as Base View in Power Views
                GetPickBaseFormNames(sIgnoreChangeFile, ref alPickBaseFormNames, ref sUpgradeFrom);
                string sBaseXml = string.Empty;
                XmlDocument objBaseXml = new XmlDocument();
                string sViewId = string.Empty;
                string sPickBaseFormNames = string.Empty;
                //string sPickBaseoldName = string.Empty;//amitosh
                foreach (FormBase.FormNames objPickBaseFormNames in alPickBaseFormNames)
                {
                    if (sUpgradeFrom == R4_RELEASE || (sUpgradeFrom == R5PS2_RELEASE && !objPickBaseFormNames.PowerViewAvailable))
                    {
                        sPickBaseFormNames = objPickBaseFormNames.FormName;
                      //  sPickBaseoldName = objPickBaseFormNames.OldName;
                        //sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = '" + htChangeFormNames[sFormName] + "' WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + Convert.ToInt32(row["VIEW_ID"]) + " AND FORM_NAME = '" + sFormName + "'";
                        sSQL = "SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE FORM_NAME = '" + sPickBaseFormNames + "' AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = 0";

                        try
                        {
                            sBaseXml = (string)DbFactory.ExecuteScalar(m_RMXPageConnectionString, sSQL);

                            if (String.IsNullOrEmpty(sBaseXml))
                            {
                                continue;
                            }

                            objBaseXml.LoadXml(sBaseXml);
                            dictParams = new Dictionary<string, string>();//amitosh
                            dictParams.Add("XML", objBaseXml.OuterXml);

                            sSQL = "SELECT NET_VIEW_FORMS.VIEW_ID VIEW_ID, NET_VIEWS.VIEW_NAME VIEW_NAME FROM NET_VIEW_FORMS INNER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID = NET_VIEWS.VIEW_ID AND NET_VIEW_FORMS.DATA_SOURCE_ID = NET_VIEWS.DATA_SOURCE_ID WHERE FORM_NAME = '" + sPickBaseFormNames + "' AND NET_VIEWS.DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND NET_VIEWS.VIEW_ID != 0";
                            ds = DbFactory.GetDataSet(m_RMXPageConnectionString, sSQL, m_iClientId);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    sViewId = row["VIEW_ID"].ToString();
                                    sViewName = row["VIEW_NAME"].ToString();
                                    try
                                    {
                                        ((XmlElement)objBaseXml.SelectSingleNode("//form")).SetAttribute("viewid", sViewId);


                                        //Raman/Gagan 06/30/2009: Changing code to use parameterised query

                                        //     dictParams = new Dictionary<string, string>();

                                        // npadhy Start MITS 20740 If the XML has already a Single quote in it
                                        // This statement will duplicate the Single Quotes. The Paramteterized Query automatically handles the 
                                        // Single Quote and we do not need to replace it with double quotes
                                        //dictParams.Add("XML", objBaseXml.OuterXml.Replace("'", "''"));
                                        //    dictParams.Add("XML", objBaseXml.OuterXml);
                                        // npadhy End MITS 20740 If the XML has already a Single quote in it
                                        // This statement will duplicate the Single Quotes. The Paramteterized Query automatically handles the 
                                        // Single Quote and we do not need to replace it with double quotes

                                        //sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = '" + objBaseXml.OuterXml.Replace("'","''") + 
                                        //  "' WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = "  + sViewId + " AND FORM_NAME = '" + sPickBaseFormNames + "'";


                                        sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = ~XML~" +
                                          " WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + sViewId + " AND FORM_NAME = '" + sPickBaseFormNames + "'";

                                        ExecutePVUpgrade(m_RMXPageConnectionString, sSQL, dictParams);

                                    }
                                    catch (Exception ex)
                                    {
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                                        FormBase.LogErrors(sErrorLogFile, string.Format(RMX_PV_UPGRADE_PICKFROMBASE_UPDATE, sPickBaseFormNames, sViewName));
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                        FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                    }

                                    //oCommand = oConnection.CreateCommand();
                                    //oCommand.CommandText = sSQL;
                                    //oCommand.ExecuteNonQuery();
                                }
                            }
                            else
                            {

                                sSQL = "SELECT distinct(VIEW_ID) FROM NET_VIEWS WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString();
                                oReader = DbFactory.GetDbReader(m_RMXPageConnectionString, sSQL);

                                while (oReader.Read())
                                {
                                    ((XmlElement)objBaseXml.SelectSingleNode("//form")).SetAttribute("viewid", oReader.GetInt32("VIEW_ID").ToString());

                                    // dictParams = new Dictionary<string, string>();
                                    //dictParams.Add("XML", objBaseXml.OuterXml);

                                    sSQL = "INSERT INTO  NET_VIEW_FORMS (DATA_SOURCE_ID,VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML) values ( " +
                                      m_iDSNId.ToString() + "," + oReader.GetInt32("VIEW_ID") + ",'" + sPickBaseFormNames + "',1,'" + objPickBaseFormNames.Caption + "',~XML~)";

                                    ExecutePVUpgrade(m_RMXPageConnectionString, sSQL, dictParams);

                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_PICKFROMBASE_RETRIEVE + sPickBaseFormNames);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        }
                    }
                }
                //Start by Shivendu for MITS 17587

                // Update the XMLS for new FDM screens like funds,orghierarchymaint,future payments to add them in 
                //new power views when legacy views are converted. 
                GetNewFDMFormNames(sIgnoreChangeFile, ref alNewFDMFormNames, ref sUpgradeFrom);
                sBaseXml = string.Empty;
                sViewId = string.Empty;
                string sNewFDMFormNames;
                int countFundsRow = 0;
                string sCaption = string.Empty;
                foreach (FormBase.FormNames objNewFDMFormNames in alNewFDMFormNames)
                {
                    if (sUpgradeFrom == "R4" || (sUpgradeFrom == "R5PS2" && !objNewFDMFormNames.PowerViewAvailable))
                    {
                        sNewFDMFormNames = objNewFDMFormNames.FormName;
                        //sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = '" + htChangeFormNames[sFormName] + "' WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + Convert.ToInt32(row["VIEW_ID"]) + " AND FORM_NAME = '" + sFormName + "'";
                        sSQL = "SELECT VIEW_XML,CAPTION FROM NET_VIEW_FORMS WHERE FORM_NAME = '" + sNewFDMFormNames + "' AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = 0";

                        try
                        {
                            ds = DbFactory.GetDataSet(m_RMXPageConnectionString, sSQL, m_iClientId);
                            //sBaseXml = (string)DbFactory.ExecuteScalar(m_RMXPageConnectionString, sSQL);

                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                continue;
                            }

                            sBaseXml = ds.Tables[0].Rows[0]["VIEW_XML"].ToString();
                            sCaption = ds.Tables[0].Rows[0]["CAPTION"].ToString();

                            objBaseXml.LoadXml(sBaseXml);

                            sSQL = "SELECT DISTINCT NET_VIEW_FORMS.VIEW_ID VIEW_ID, NET_VIEWS.VIEW_NAME VIEW_NAME FROM NET_VIEW_FORMS INNER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID = NET_VIEWS.VIEW_ID AND NET_VIEW_FORMS.DATA_SOURCE_ID = NET_VIEWS.DATA_SOURCE_ID WHERE FORM_NAME = '" + sNewFDMFormNames + "' AND NET_VIEWS.DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND NET_VIEWS.VIEW_ID != 0";
                            ds = DbFactory.GetDataSet(m_RMXPageConnectionString, sSQL, m_iClientId);
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                sViewId = row["VIEW_ID"].ToString();
                                sViewName = row["VIEW_NAME"].ToString();
                                try
                                {
                                    ((XmlElement)objBaseXml.SelectSingleNode("//form")).SetAttribute("viewid", sViewId);

                                    sSQL = "SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE FORM_NAME = '" + sNewFDMFormNames + "' AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + sViewId;
                                    countFundsRow = Convert.ToInt32(DbFactory.ExecuteScalar(m_RMXPageConnectionString, sSQL));

                                    //Raman/Gagan 06/30/2009: Changing code to use parameterised query

                                    dictParams = new Dictionary<string, string>();
                                    // npadhy Start MITS 20740 If the XML has already a Single quote in it
                                    // This statement will duplicate the Single Quotes. The Paramteterized Query automatically handles the 
                                    // Single Quote and we do not need to replace it with double quotes
                                    //dictParams.Add("XML", objBaseXml.OuterXml.Replace("'", "''"));
                                    dictParams.Add("XML", objBaseXml.OuterXml);
                                    // npadhy End MITS 20740 If the XML has already a Single quote in it
                                    // This statement will duplicate the Single Quotes. The Paramteterized Query automatically handles the 
                                    // Single Quote and we do not need to replace it with double quotes

                                    //sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = '" + objBaseXml.OuterXml.Replace("'","''") + 
                                    //  "' WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = "  + sViewId + " AND FORM_NAME = '" + sPickBaseFormNames + "'";


                                    if (countFundsRow != 0)
                                    {
                                        sSQL = "UPDATE NET_VIEW_FORMS SET VIEW_XML = ~XML~" +
                                          " WHERE DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND VIEW_ID = " + sViewId + " AND FORM_NAME = '" + sNewFDMFormNames + "'";
                                    }
                                    else
                                    {
                                        //sSQL = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (" + sViewId + ",'" + sNewFDMFormNames + "',1,'FUNDS',~XML~,'" + sNewFDMFormNames.Replace("xml", "aspx") + "','" + System.DateTime.Now + "','" + m_iDSNId.ToString() + "')";

                                        sSQL = String.Format("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,VIEW_XML,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID,CAPTION) VALUES ({0},'{1}',1,~XML~,'{2}','{3:yyyyMMddHHmmss}','{4}','{5}')", sViewId, sNewFDMFormNames, sNewFDMFormNames.Replace("xml", "aspx"), System.DateTime.Now, m_iDSNId.ToString(), sCaption);
                                    }

                                    ExecutePVUpgrade(m_RMXPageConnectionString, sSQL, dictParams);
                                    countFundsRow = 0;
                                }
                                catch (Exception ex)
                                {
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                                    FormBase.LogErrors(sErrorLogFile, string.Format(RMX_PV_UPGRADE_NEWFDMFORMS_UPDATE, sNewFDMFormNames, sViewName));
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                }

                                //oCommand = oConnection.CreateCommand();
                                //oCommand.CommandText = sSQL;
                                //oCommand.ExecuteNonQuery();

                            }
                        }
                        catch (Exception ex)
                        {
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_NEWFDMFORMS_RETRIEVE + sNewFDMFormNames);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                        }
                    }
                }
                //End by Shivendu for MITS 17587

                //Write to xml.
                if (bErr)
                    errXml.Save(sLogXml);

                this.Cursor = Cursors.Default;
                if (!m_IsCmdLine)
                {
                    if (!bErr || this.checkBox1.Checked)
                        UpdateStatus("PowerViews Upgraded Successfully.", false);
                    else
                        UpdateStatus(String.Format("FAILED to Upgrade PowerViews. Please see the log file {0}", sLogXml), false);
                }
            }
            catch (Exception ex)
            {
                if (oCommand != null && !bTransactionCommitted)
                    if (oCommand.Transaction != null)
                    {
                        oCommand.Transaction.Rollback();
                    }

                oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));

                if (!m_IsCmdLine)
                    MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if (oReader != null)
                    oReader.Close();

                if (oConnection != null)
                    oConnection.Close();

                if (oTraceListener != null)
                    oTraceListener.Close();
            }
        }

        private void GetIgnoreChangeFormNames(string p_sIgnoreChangeFile, ref ArrayList p_alIgnoreForms, ref Hashtable p_htChangeForms)
        {
            XmlDocument objIgnoreChange = new XmlDocument();
            XmlNodeList objIgnoreList = null;
            XmlNodeList objChangeList = null;
            objIgnoreChange.Load(p_sIgnoreChangeFile);
            objIgnoreList = objIgnoreChange.SelectNodes("/Ignore_Change/Ignore/formname");
            foreach (XmlNode objIgnore in objIgnoreList)
            {
                p_alIgnoreForms.Add(objIgnore.InnerText);
            }

            objChangeList = objIgnoreChange.SelectNodes("/Ignore_Change/Change/formname");
            foreach (XmlNode objChange in objChangeList)
            {
                p_htChangeForms.Add(objChange.InnerText, objChange.Attributes["changedName"].Value);
            }
        }

        private void GetPickBaseFormNames(string p_sIgnoreChangeFile, ref ArrayList p_alPickBaseForms, ref string p_sUpgradeFrom)
        {

            RetrieveFormName("/Ignore_Change/PickFromBase/formname", p_sIgnoreChangeFile, ref p_alPickBaseForms, ref p_sUpgradeFrom);
        }
        /// <summary>
        /// Function added by Shivendu for MITS 17587
        /// </summary>
        /// <param name="p_sIgnoreChangeFile"></param>
        /// <param name="p_alPickBaseForms"></param>
        private void GetNewFDMFormNames(string p_sIgnoreChangeFile, ref ArrayList p_alNewFDMForms, ref string p_sUpgradeFrom)
        {
            RetrieveFormName("/Ignore_Change/NewFDMForm/formname", p_sIgnoreChangeFile,ref p_alNewFDMForms, ref p_sUpgradeFrom);
        }
        private void RetrieveFormName(string p_sPath, string p_sIgnoreChangeFile, ref ArrayList p_alForms, ref string p_sUpgradeFrom)
        {
            XmlDocument objIgnoreChange = new XmlDocument();
            XmlNodeList objList = null;
            XmlNode objUpgradeFrom = null;
            objIgnoreChange.Load(p_sIgnoreChangeFile);
            objList = objIgnoreChange.SelectNodes(p_sPath);
            FormBase.FormNames objFormName;
            foreach (XmlNode objPickBase in objList)//amitosh
            {
                if (objPickBase.Attributes["oldname"] != null)
                {
                    if (objPickBase.Attributes["powerviewavailable"] != null && objPickBase.Attributes["powerviewavailable"].Value.ToUpper() == "TRUE")
                        objFormName = new FormBase.FormNames(objPickBase.InnerText, true, objPickBase.Attributes["oldname"].Value.ToUpper(),objPickBase.Attributes["caption"].Value.ToUpper());
                    else
                        objFormName = new FormBase.FormNames(objPickBase.InnerText, false, objPickBase.Attributes["oldname"].Value.ToUpper(),objPickBase.Attributes["caption"].Value.ToUpper());
                }
                else
                {
                    if (objPickBase.Attributes["powerviewavailable"] != null && objPickBase.Attributes["powerviewavailable"].Value.ToUpper() == "TRUE")
                        objFormName = new FormBase.FormNames(objPickBase.InnerText, true);
                    else
                        objFormName = new FormBase.FormNames(objPickBase.InnerText, false);
                }


                //Start - Caption attribute is not getting picked up when we specify it in formname node in PickFromBase(Ignore_Change.xml)
                if (objPickBase.Attributes["caption"] != null)
                {
                    if (objPickBase.Attributes["caption"] != null && !string.IsNullOrWhiteSpace(objPickBase.Attributes["caption"].Value))
                        objFormName.Caption = objPickBase.Attributes["caption"].Value;
                }
                //End

                //if (objPickBase.Attributes["powerviewavailable"] != null && objPickBase.Attributes["powerviewavailable"].Value.ToUpper() == "TRUE")
                //    objFormName = new FormBase.FormNames(objPickBase.InnerText, true, objPickBase.Attributes["oldname"].Value.ToUpper());
                //else
                //    objFormName = new FormBase.FormNames(objPickBase.InnerText, false);
                p_alForms.Add(objFormName);
            }

            objUpgradeFrom = objIgnoreChange.SelectSingleNode("/Ignore_Change/UpgradeFrom");
            if (objUpgradeFrom != null)
            {
                p_sUpgradeFrom = objUpgradeFrom.InnerText;
            }
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sListFile"></param>
        /// <param name="m_strSelectedDSN"></param>
        /// <param name="sUser"></param>
        /// <param name="m_strPassword"></param>
        /// <returns></returns>
        private XmlDocument GetFormList(string sListFile, string m_strSelectedDSN, string sUser, string m_strPassword)
        {
            XmlDocument objXMLDocFormList = new XmlDocument();
            XmlElement objSelectElement = null;
            XmlElement objRowText = null;
            DataModelFactory objDmf = null;
            ADMTableList objAdmLst = null;
            ArrayList arrAdm = null;
            string[] retItem = new string[3];

            string sErrorLogFile = String.Format("{0}PvUpgrade_{1}Error.log", AppDomain.CurrentDomain.BaseDirectory, m_strSelectedDSN);

            //Retrieve form list from file
            objXMLDocFormList.Load(sListFile);
            objSelectElement = (XmlElement)objXMLDocFormList.SelectSingleNode("//forms");

            //retrieve all admin tracking table names
            objDmf = DataModelFactory.CreateDataModelFactory(m_ConnectionString, m_iClientId);
            objAdmLst = (ADMTableList)objDmf.GetDataModelObject("ADMTableList", false);
            arrAdm = objAdmLst.TableUserNames();

            foreach (string[] arrItem in arrAdm)
            {
                try
                {
                    objRowText = objXMLDocFormList.CreateElement("form");
                    objRowText.SetAttribute("name", arrItem[1]);
                    objRowText.SetAttribute("file", "admintracking" + "%" + arrItem[0] + ".xml");
                    objRowText.SetAttribute("toplevel", "1");
                    objSelectElement.AppendChild((XmlNode)objRowText);
                }
                catch (Exception ex)
                {
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_ADMIN_TRACKING + arrItem);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                    FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                }
            }

            return objXMLDocFormList;
        }
        #endregion

        #region RMX R5 PowerView Upgrade Panel

        /// <summary>
        /// btnConvert_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConvert_Click(object sender, EventArgs e)
        {
            #region commented out code
            //AuthenticateUser();

            ////Set default values for the progress bar control
            //// Set Minimum to 1 to represent the first file being copied.
            //progressBar1.Minimum = 1;
            //// Set the initial value of the ProgressBar.
            //progressBar1.Value = 1;
            //// Set the Step property to a value of 1 to represent each file being copied.
            //progressBar1.Step = 1;

            //var FormValues = new { ReadOnlyChecked = chkReadOnly.Checked, TopDownChecked = chkTopDown.Checked, SelectedDSN = m_strSelectedDSN };

            //bool bResult = ConvertPVtoAspx(FormValues.SelectedDSN, FormValues.ReadOnlyChecked, FormValues.TopDownChecked);
            //if (bResult)
            //{
            //    MessageBox.Show("PowerViews successfully converted for RMX R5", RMXR5_PV_UPGRADE_CAPTION);
            //} // if
            #endregion
        }//event: btnConvert_Click

        /// <summary>
        /// Converts older versions of RMX PowerViews to their
        /// ASP.Net equivalents required for RISKMASTER X R5
        /// </summary>
        /// <param name="strSelectedDSN">string containing the name of the selected DSN</param>
        /// <param name="strPVFormName"></param>
        /// <returns>boolean indicating whether or not the upgrade 
        /// process was successful</returns>
        public bool ConvertPVtoAspx(string strSelectedDSN, string strPVFormName)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlDocument xdocAdmin = new XmlDocument();
            XmlDocument xdocJuris = new XmlDocument();
            StringBuilder convertedAspx;
            int iUpdate = 0;
            int iAdminTableRows = 0;
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbSelectSql = new StringBuilder();
            StringBuilder sbAdminTablesCount = new StringBuilder();
            string sSQLAdminTablesList = string.Empty;
            ArrayList retAdminTables = new ArrayList();
            bool bSuccess = false;
            string sPageName = string.Empty;
            string sFormName = string.Empty;
            string sViewName = string.Empty;
            bool bConvertWCJuris = false;
            XmlDocument objJuris = null;
            XmlNodeList objJurisData = null;
            XmlDocument objIndividualStateData = null;
            int iStateId = 0;
            object objDataSourceDB = null;
            StringBuilder sResponse = new StringBuilder();
            PowerViewMode CurrentMode = PowerViewMode.ALL;
            UpgradeSection CurrentSection;            
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            const string BASE_VIEWS = "0";
            const string CUSTOM_VIEWS = "-1"; //Rsolanki2: Custom Views would have the viweId as -1 
            PowerViewUpgrade pvUpgrade = new PowerViewUpgrade(m_iClientId);
            bool bIsReadonly = false;
            bool bIsTopdown = false;
            string sErrorLogFile = String.Format("{0}PvUpgrade_{1}Error.log", AppDomain.CurrentDomain.BaseDirectory, m_strSelectedDSN);
            try
            {
                if (rdbPVJuris.Checked)
                    CurrentSection = UpgradeSection.ALL;
                else if(rdbJurisData.Checked)
                    CurrentSection = UpgradeSection.ONLYJURIS;
                else
                    CurrentSection = UpgradeSection.ONLYSUPP;

                if (CurrentSection == UpgradeSection.ONLYJURIS)
                {
                    strPVFormName = "claimwc.xml";
                } // if

                if (rdbSingleForm.Checked)
                {
                    strPVFormName = txtFormName.Text.Trim();
                }

                //Get the currently available views in the database
                // akaushik5 Changed for MITS 30290 Starts
                //using (DataSet dstViews = DbFactory.GetDataSet(m_RMXPageConnectionString, GetViews(strPVFormName)))
                using (DataSet dstViews = DbFactory.GetDataSet(m_RMXPageConnectionString, GetViews(strPVFormName, CurrentSection), m_iClientId))
                // akaushik5 Changed for MITS 30290 Ends
                {
                    //Get the count from the actual return results of the query
                    int intViewCount = GetViewCount(strPVFormName, CurrentSection);

                    if (intViewCount > 0)
                    {
                        progressBar1.Maximum = intViewCount;

                    } // if
                    else if (intViewCount == 0)
                    {
                        throw new ApplicationException("There are currently no PowerViews available to convert.");
                    } // else

                    //Iterate through the records using a DataTableReader instead
                    using (DataTableReader drView = dstViews.Tables[0].CreateDataReader())
                    {
                        #region Looping DataReader
                        while (drView.Read())
                        {
                            //Clear the contents of the StringBuilder
                            sbSql.Length = 0;

                            // Perform the increment on the ProgressBar.
                            sViewName = IsBaseView(drView["VIEW_NAME"].ToString());
                            sFormName = drView["FORM_NAME"].ToString();
                            try
                            {
                                string strViewXML = drView["VIEW_XML"].ToString();
                                string strViewID = drView["VIEW_ID"].ToString();

                                UpdateStatus(sViewName, sFormName);

                                xdoc.LoadXml(strViewXML);
                                if (strViewID == BASE_VIEWS || strViewID == CUSTOM_VIEWS)
                                {
                                    // akaushik5 Changed for MITS 30290 Starts
                                    //if (sFormName.IndexOf("admintracking") < 0 && sFormName.IndexOf("claimwc.xml") < 0)
                                    if (sFormName.IndexOf("admintracking") < 0 && sFormName.IndexOf("claimwc.xml") < 0 && sFormName.IndexOf("claimwcd.xml") < 0)
                                    // akaushik5 Changed for MITS 30290 Ends
                                    {
                                        PowerViewUpgrade.UpgradeSupplementalFields(m_ConnectionString, xdoc, null, CurrentMode, "", CurrentSection, m_iClientId);
                                    }
                                    // akaushik5 Changed for MITS 30290 Starts
                                    //else if (sFormName.IndexOf("claimwc.xml") > -1)
                                    else if (sFormName.IndexOf("claimwc.xml") > -1 || sFormName.IndexOf("claimwcd.xml") > -1)
                                    // akaushik5 Changed for MITS 30290 Ends
                                    {
                                        objJuris = new XmlDocument();
                                        PowerViewUpgrade.UpgradeSupplementalFields(m_ConnectionString, xdoc, objJuris, CurrentMode, "", CurrentSection, m_iClientId);
                                        bConvertWCJuris = true;
                                    }
                                    //if condition added to generate the aspx of admintracking tables
                                    else if (sFormName.IndexOf("admintracking.xml") > -1)
                                    {
                                        retAdminTables = PowerViewUpgrade.GetAdminTables(m_ConnectionString, m_iClientId);
                                        foreach (string[] arrItem in retAdminTables)
                                        {
                                            string strAdminFormName = arrItem[0];

                                            //Display the currently updating Admin table name
                                            UpdateStatus(sViewName, string.Format("{0}|{1}", sFormName, strAdminFormName));

                                            xdocAdmin.LoadXml(xdoc.OuterXml);
                                            //Clear the contents of the StringBuilder
                                            sbSql.Length = 0;

                                            sbAdminTablesCount.Length = 0;

                                            ((XmlElement)xdocAdmin.GetElementsByTagName("form")[0]).SetAttribute("supp", arrItem[0].ToString());
                                            ((XmlNode)xdocAdmin.SelectSingleNode("//internal[@name='SysFormName']")).Attributes["value"].Value = String.Format("admintracking|{0}", strAdminFormName);

                                            //Upgrade the supplemental fields for Admin Tracking tables
                                            PowerViewUpgrade.UpgradeSupplementalFields(m_ConnectionString, xdocAdmin, null, CurrentMode, "", CurrentSection, m_iClientId);

                                            bIsReadonly = IsReadonlyForm(xdocAdmin);
                                            bIsTopdown = IsTopdownForm(xdocAdmin);

                                            pvUpgrade.UpgradeXmlToAspx(bIsReadonly, bIsTopdown, xdocAdmin, sFormName.Insert(sFormName.IndexOf('.'), strAdminFormName), out convertedAspx);

                                            bool blnParsed = false;

                                            iAdminTableRows = GetFormCount(string.Format("admintracking|{0}.xml", strAdminFormName), out blnParsed);

                                            if (blnParsed && iAdminTableRows <= 0)
                                            {
                                                dictParams.Clear();
                                                dictParams.Add("XML", xdocAdmin.OuterXml);
                                                dictParams.Add("ASPX", convertedAspx.ToString());

                                                sSQLAdminTablesList = String.Format("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_CONTENT,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (0,'admintracking|{0}.xml',1,'{1}',~XML~,~ASPX~,'admintracking{0}.aspx','{2:yyyyMMddHHmmss}','{3}')", strAdminFormName, arrItem[1], System.DateTime.Now, m_iDSNId);

                                                iUpdate = DbFactory.ExecuteNonQuery(m_RMXPageConnectionString, sSQLAdminTablesList, dictParams);
                                            }
                                            else
                                            {

                                                sPageName = sFormName.Replace("xml", "aspx");
                                                sbSql.AppendFormat(String.Format("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT =~ASPX~,LAST_UPDATED = '{0:yyyyMMddHHmmss}',PAGE_NAME = 'admintracking{1}.aspx' WHERE FORM_NAME = 'admintracking|{1}.xml' AND VIEW_ID = {2} AND DATA_SOURCE_ID ='{3}'", System.DateTime.Now, strAdminFormName, strViewID, m_iDSNId));

                                                dictParams.Clear();
                                                dictParams.Add("XML", xdocAdmin.OuterXml);
                                                dictParams.Add("ASPX", convertedAspx.ToString());

                                                iUpdate = ExecutePVUpgrade(m_RMXPageConnectionString, sbSql.ToString(), dictParams);

                                            }
                                        }
                                        continue;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                //else
                                //{
                                //    PowerViewUpgrade.UpgradeSupplementalIndex(m_ConnectionString, xdoc, null, CurrentMode, "", CurrentSection);
                                //}

                                //Start by Shivendu to add the control ReserveTypeCodeFt to existing power views:Temporary fix
                                if (strViewID != BASE_VIEWS && strViewID != CUSTOM_VIEWS  && sFormName.ToLower().IndexOf("split.xml") > -1)
                                {
                                    AddNewReserveTypeControl(ref xdoc);
                                    //Mona:MITS 22993: Supp fields do not line-up correctly in PowerView for Fund_trans_split 
                                    ChangeClassOfSuppControl(ref xdoc, "popuprow");
                                }
                                //End by Shivendu to add the control ReserveTypeCodeFt to existing power views:Temporary fix

                                bIsReadonly = IsReadonlyForm(xdoc);
                                bIsTopdown = IsTopdownForm(xdoc);
                                pvUpgrade.UpgradeXmlToAspx(bIsReadonly, bIsTopdown, xdoc, sFormName, out convertedAspx);

                                //Raman/Naresh: 06/23/2009
                                //Legacy admin tracking powerviews have a "|" in the page name whereas R5 does not support the "|" character in formname
                                sPageName = sFormName.Replace("xml", "aspx").Replace("|", "");
                                sbSql.Append(String.Format("UPDATE NET_VIEW_FORMS SET PAGE_CONTENT = ~ASPX~,PAGE_NAME = '{0}',LAST_UPDATED = '{1:yyyyMMddHHmmss}' WHERE FORM_NAME = '{2}' AND VIEW_ID = {3} AND DATA_SOURCE_ID ='{4}'", sPageName, System.DateTime.Now, sFormName, strViewID, m_iDSNId));

                                dictParams.Clear();
                                dictParams.Add("ASPX", convertedAspx.ToString());

                                iUpdate = ExecutePVUpgrade(m_RMXPageConnectionString, sbSql.ToString(), dictParams);

                                if (bConvertWCJuris)
                                {
                                    objJurisData = objJuris.SelectNodes("/JurisData/*");

                                    foreach (XmlNode objJurisNode in objJurisData)
                                    {
                                        //Clear the contents of the string builder
                                        sbSql.Length = 0;

                                        // Get the State
                                        convertedAspx.Length = 0;

                                        sResponse.Length = 0;

                                        string sStateShortName = objJurisNode.Attributes["StateShortName"].Value;
                                        UpdateStatus(sViewName, string.Format("Juris|{0}", sStateShortName));

                                        iStateId = Int32.Parse(objJurisNode.Attributes["StateId"].Value);
                                        objIndividualStateData = new XmlDocument();
                                        objIndividualStateData.LoadXml(objJurisNode.OuterXml);

                                        bIsReadonly = IsReadonlyForm(objIndividualStateData);
                                        bIsTopdown = IsTopdownForm(objIndividualStateData);

                                        pvUpgrade.UpgradeXmlTagToAspxFormJuris(bIsReadonly, bIsTopdown, objIndividualStateData, sFormName, out convertedAspx);

                                        sResponse.Append(ImportNamespaces());
                                        sResponse.Append(RegisterDirectives());
                                        sResponse.Append(convertedAspx.ToString());

                                        // Check the Existence of Record in DB
                                        sbSql.AppendFormat(String.Format("SELECT DATA_SOURCE_ID FROM JURIS_VIEWS WHERE DATA_SOURCE_ID = {0} AND STATE_ID = {1}", m_iDSNId, iStateId));
                                        objDataSourceDB = DbFactory.ExecuteScalar(m_RMXPageConnectionString, sbSql.ToString());

                                        //Clear the contents of the string builder
                                        sbSql.Length = 0;

                                        if (objDataSourceDB == null)
                                        {
                                            sbSql.AppendFormat(String.Format("INSERT INTO JURIS_VIEWS(DATA_SOURCE_ID, STATE_ID, LAST_UPDATED, PAGE_CONTENT) VALUES({0},{1},'{2:yyyyMMddHHmmss}', ~ASPX~ )", m_iDSNId, iStateId, System.DateTime.Now));
                                        }
                                        else
                                        {
                                            sbSql.AppendFormat(String.Format("UPDATE JURIS_VIEWS SET LAST_UPDATED = '{0:yyyyMMddHHmmss}', PAGE_CONTENT=~ASPX~ WHERE DATA_SOURCE_ID = {1}AND STATE_ID = {2}", System.DateTime.Now, m_iDSNId, iStateId));
                                        }

                                        dictParams.Clear();
                                        dictParams.Add("ASPX", sResponse.ToString());

                                        iUpdate = ExecutePVUpgrade(m_RMXPageConnectionString, sbSql.ToString(), dictParams);
                                    }
                                    bConvertWCJuris = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                                FormBase.LogErrors(sErrorLogFile, String.Format(RMX_PV_UPGRADE_CONVERT_XML2ASPX,sFormName,sViewName));
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                            }
                        }//while 
                        #endregion                        
                    } // using DataTableReader

                    bSuccess = true;
                } // using DataSet

                if (rdbSingleForm.Checked && bSuccess)
                {
                    progressBar1.Value = progressBar1.Maximum;
                } // if

            }//try
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Error encountered while upgrading RMX Powerview: {0} -- {1}: {2}", sViewName, sFormName, ex.Message), RMXR5_PV_UPGRADE_CAPTION);
                throw ex;
            }

            return bSuccess;
        }
        /// <summary>
        /// Adds the control ReserveTypeCodeFt to existing power views:smishra25
        /// </summary>
        /// <param name="p_NewFormXmlDoc"></param>
        private void AddNewReserveTypeControl(ref XmlDocument p_NewFormXmlDoc)
        {
            XmlNode objNewReserveTypeCodeFt = p_NewFormXmlDoc.SelectSingleNode("//control[@name='ReserveTypeCodeFt']");
            if (objNewReserveTypeCodeFt == null)
            {
                XmlNode objSplitDetailGroup = p_NewFormXmlDoc.SelectSingleNode("//group[@name='splitdetaiilgroup']");
                if (objSplitDetailGroup != null)
                {
                    objSplitDetailGroup.InnerXml = "<displaycolumn><control name='ReserveTypeCodeFt' class='popuprow' id='ReserveTypeCodeFt' type='code' required='yes' title='Reserve Type:' codetable='RESERVE_TYPE' codeid='' ref='/option/ReserveTypeCode' /></displaycolumn>" + objSplitDetailGroup.InnerXml;
                }
            }
        }

        /// <summary>        
        /// Mona:MITS 22993: Supp fields do not line-up correctly in PowerView for Fund_trans_split 
        /// changes the class of Supplemental fields of split.xml from "half" to "popuprow"        
        /// </summary>
        /// <param name="p_XmlDoc">View Xml</param>
        /// <param name="sTargetClass">New value of Class attribute</param>
        private void ChangeClassOfSuppControl(ref XmlDocument p_XmlDoc, string sTargetClass)
        {
            XmlNodeList objSuppControls = p_XmlDoc.SelectNodes("//control[@fieldtype='supplementalfield']");
            foreach(XmlNode objSupp in objSuppControls )
            {
                objSupp.Attributes["class"].Value = sTargetClass;
            }
        }

        /// <summary>
        /// Provides the string of all Namespaces to import into the converted .aspx pages
        /// </summary>
        /// <returns>string containing the required namespaces to be imported</returns>
        private static string ImportNamespaces()
        {
            StringBuilder strImportNamespaces = new StringBuilder();

            strImportNamespaces.Append("<%@ Import Namespace=\"System.Data\" %>");
            strImportNamespaces.Append("<%@ Import Namespace=\"System.Xml\" %>");
          
            return strImportNamespaces.ToString();
        }

        /// <summary>
        /// Provides a string of all register directives to be included in the converted .aspx pages
        /// </summary>
        /// <returns>string containing all the required register directives to be included in the .aspx pages</returns>
        private static string RegisterDirectives()
        {
            StringBuilder strRegisterDirectives = new StringBuilder();

            strRegisterDirectives.Append("<%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>");
            strRegisterDirectives.Append("<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>");
            strRegisterDirectives.Append("<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>");
            strRegisterDirectives.Append("<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>");
            strRegisterDirectives.Append("<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>");
            strRegisterDirectives.Append("<%@ Register Assembly=\"MultiCurrencyCustomControl\" Namespace=\"MultiCurrencyCustomControl\"  TagPrefix=\"mc\" %>");//Deb

            return strRegisterDirectives.ToString();
        } // method: RegisterDirectives

        /// <summary>
        /// Check if the the form is a readonly form
        /// </summary>
        /// <param name="oXmlDoc"></param>
        /// <returns></returns>
        private bool IsReadonlyForm(XmlDocument oXmlDoc)
        {
            bool bReadonly = false;
            XmlNode oNode = oXmlDoc.SelectSingleNode("//form");
            if (oNode != null)
            {
                XmlAttribute oAttrReadonly = oNode.Attributes["readonly"];
                if (oAttrReadonly != null)
                {
                    if (oAttrReadonly.Value != "0")
                        bReadonly = true;
                }
            }

            return bReadonly;
        }

        /// <summary>
        /// Check the form is a topdown latout
        /// </summary>
        /// <param name="oXmlDoc"></param>
        /// <returns></returns>
        private bool IsTopdownForm(XmlDocument oXmlDoc)
        {
            bool bTopdown = false;
            XmlNode oNode = oXmlDoc.SelectSingleNode("//form");
            if (oNode != null)
            {
                XmlAttribute oAttrReadonly = oNode.Attributes["LayOut"];
                if (oAttrReadonly != null)
                {
                    if (oAttrReadonly.Value != "0")
                        bTopdown = true;
                }
            }

            return bTopdown;
        }

        /// <summary>
        /// Update the status
        /// </summary>
        /// <param name="sViewName"></param>
        /// <param name="sFormName"></param>
        private void UpdateStatus(string sViewName, string sFormName)
        {
            lblStatus.Text = String.Format("Converting: {0} -- {1}", sViewName, sFormName);
            progressBar1.PerformStep();
            this.Refresh();
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// Update the status
        /// </summary>
        /// <param name="strLabelText">string containing the label text</param>
        private void UpdateStatus(string strLabelText)
        {
            UpdateStatus(strLabelText, true);
        }

        /// <summary>
        /// Update the status
        /// </summary>
        /// <param name="strLabelText">string containing the label text</param>
        /// <param name="blnUpdateProgressBar">boolean indicating whether or not to update the progress bar</param>
        private void UpdateStatus(string strLabelText, bool blnUpdateProgressBar)
        {
            lblStatus.Text = strLabelText;

            if (blnUpdateProgressBar)
            {
                progressBar1.PerformStep();
            } // if

            this.Refresh();
            System.Windows.Forms.Application.DoEvents();

        } // method: UpdateStatus

        /// <summary>
        /// Gets the number of Views for the specified Form Name
        /// </summary>
        /// <param name="strFormName">string indicating the name of the PowerView</param>
        /// <param name="currentSelection">The current selection.</param>
        /// <returns></returns>
        // akaushik5 Changed for MITS 30290 Starts
        //private string GetViews(string strFormName)
        private string GetViews(string strFormName, UpgradeSection currentSelection)
        // akaushik5 Changed for MITS 30290 Ends
        {
            StringBuilder sbSelectSql = new StringBuilder();

            //Raman 06/23/2009 : Adding Data_Source_Id in Join 

            sbSelectSql.Append("SELECT FORM_NAME,VIEW_XML, NET_VIEW_FORMS.VIEW_ID, VIEW_NAME FROM NET_VIEW_FORMS ");
            sbSelectSql.Append(" LEFT OUTER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID=NET_VIEWS.VIEW_ID AND NET_VIEW_FORMS.DATA_SOURCE_ID=NET_VIEWS.DATA_SOURCE_ID ");
            sbSelectSql.Append(String.Format(" WHERE UPPER(FORM_NAME) NOT LIKE '%LIST.XML' AND UPPER(FORM_NAME) NOT LIKE '%.XSL' AND UPPER(FORM_NAME) NOT LIKE '%.SCC' AND NET_VIEW_FORMS.DATA_SOURCE_ID ='{0}'", m_iDSNId));

            if (!string.IsNullOrEmpty(strFormName))
            {
                // akaushik5 Changed for MITS 30290 Starts
                // sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) = '{0}'", strFormName.ToUpper()));
                if (currentSelection.Equals(UpgradeSection.ONLYJURIS) && !string.IsNullOrEmpty(RMConfigurationManager.GetAppSetting("DynamicViews")) && RMConfigurationManager.GetAppSetting("DynamicViews").Contains(strFormName.Replace(".xml", string.Empty)))
                {
                    sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) IN ('{0}','{1}')", strFormName.ToUpper(), string.Format("{0}d.xml", strFormName.Replace(".xml", string.Empty)).ToUpper()));
                }
                else
                {
                    sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) = '{0}'", strFormName.ToUpper()));
                }
                // akaushik5 Changed for MITS 30290 Ends
            }//if

            sbSelectSql.Append(" ORDER BY NET_VIEW_FORMS.VIEW_ID, FORM_NAME");

            return sbSelectSql.ToString();
        }

        /// <summary>
        /// Gets the number of Views for the specified Form Name
        /// </summary>
        /// <param name="strFormName">string indicating the name of the PowerView</param>
        /// <param name="currentSelection">Option selected</param>
        /// <returns></returns>
        private int GetViewCount(string strFormName, UpgradeSection currentSelection)
        {
            StringBuilder sbSelectSql = new StringBuilder();
            int iTotalCount = 0;
            string sErrorLogFile = String.Format("{0}PvUpgrade_{1}Error.log", AppDomain.CurrentDomain.BaseDirectory, m_strSelectedDSN);
            //Raman 06/24/2009 : Adding Data_Source_Id in Join
            try
            {
                sbSelectSql.Append("SELECT COUNT(*) FROM NET_VIEW_FORMS ");
                sbSelectSql.Append(" LEFT OUTER JOIN NET_VIEWS ON NET_VIEW_FORMS.VIEW_ID=NET_VIEWS.VIEW_ID AND NET_VIEW_FORMS.DATA_SOURCE_ID=NET_VIEWS.DATA_SOURCE_ID");
                sbSelectSql.Append(String.Format(" WHERE UPPER(FORM_NAME) NOT LIKE '%LIST.XML' AND UPPER(FORM_NAME) NOT LIKE '%.XSL' AND UPPER(FORM_NAME) NOT LIKE '%.SCC' AND NET_VIEW_FORMS.DATA_SOURCE_ID ='{0}'", m_iDSNId));

                if (!string.IsNullOrEmpty(strFormName))
                {
                    // akaushik5 Changed for MITS 30290 Starts
                    // sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) = '{0}'", strFormName.ToUpper()));
                    if (currentSelection.Equals(UpgradeSection.ONLYJURIS) && !string.IsNullOrEmpty(RMConfigurationManager.GetAppSetting("DynamicViews")) && RMConfigurationManager.GetAppSetting("DynamicViews").Contains(strFormName.Replace(".xml", string.Empty)))
                    {
                        sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) IN ('{0}','{1}')", strFormName.ToUpper(), string.Format("{0}d.xml", strFormName.Replace(".xml", string.Empty)).ToUpper()));
                    }
                    else
                    {
                        sbSelectSql.AppendFormat(string.Format("AND UPPER(FORM_NAME) = '{0}'", strFormName.ToUpper()));
                    }
                    // akaushik5 Changed for MITS 30290 Ends
                }//if

                int intRowCount = Convert.ToInt32(DbFactory.ExecuteScalar(m_RMXPageConnectionString, sbSelectSql.ToString()));

                //If PV+Juris or Juris, it should include Juris tables
                int iJurisCount = 0;
                if (currentSelection == UpgradeSection.ALL || currentSelection == UpgradeSection.ONLYJURIS)
                {
                    sbSelectSql.Length = 0;
                    sbSelectSql.Append("SELECT COUNT(DISTINCT SUPP_TABLE_NAME) FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME LIKE 'WC%SUPP' AND SUPP_TABLE_NAME LIKE '%'");
                    iJurisCount = Convert.ToInt32(DbFactory.ExecuteScalar(m_ConnectionString, sbSelectSql.ToString()));
                }

                //Get Admintracking table count
                int iAdminCount = 0;
                if (currentSelection == UpgradeSection.ALL || currentSelection == UpgradeSection.ONLYSUPP)
                {
                    if (string.IsNullOrEmpty(strFormName) || (string.Compare(strFormName, "admintracking.xml", true) == 0))
                    {
                        iAdminCount = PowerViewUpgrade.GetAdminTables(m_ConnectionString, m_iClientId).Count;
                    }
                }

                //TODO: This logic needs to be re-worked to match the actual execution query
                iTotalCount = intRowCount + iJurisCount + iAdminCount;
            }
            catch (Exception ex)
            {
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_TIMESTAMP + DateTime.Now);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_VIEW_COUNT + strFormName);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_EXCEPTION_MESSAGE + ex.Message);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_STACKTRACE + ex.StackTrace);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
                FormBase.LogErrors(sErrorLogFile, RMX_PV_UPGRADE_BLANK_LINE);
            }

            return iTotalCount;
        }

        /// <summary>
        /// Gets the number of forms affected by the update operation
        /// </summary>
        /// <param name="strFormName"></param>
        /// <param name="blnParsed"></param>
        /// <returns></returns>
        private int GetFormCount(string strFormName, out bool blnParsed)
        {
            int iAdminTableRows = 0;

            string strSQL = String.Format("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME LIKE '{0}' AND DATA_SOURCE_ID = '{1}'", strFormName, m_iDSNId);

            iAdminTableRows = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_RMXPageConnectionString, strSQL).ToString(), out blnParsed);

            return iAdminTableRows;
        } // method: GetFormCount

        /// <summary>
        /// IsBaseView
        /// </summary>
        /// <param name="sViewName"></param>
        /// <returns></returns>
        private static string IsBaseView(string sViewName)
        {
            if (string.IsNullOrEmpty(sViewName))
            {
                sViewName = "Base View";
            }

            return sViewName;
        }

        /// <summary>
        /// ExecutePVUpgrade
        /// </summary>
        /// <param name="m_RMXPageConnectionString"></param>
        /// <param name="strSQLStatement"></param>
        /// <param name="dictParams"></param>
        /// <returns></returns>
        private static int ExecutePVUpgrade(string m_RMXPageConnectionString, string strSQLStatement, Dictionary<string, string> dictParams)
        {
            int iUpdate = 0;

            iUpdate = DbFactory.ExecuteNonQuery(m_RMXPageConnectionString, strSQLStatement, dictParams);
            return iUpdate;
        }

        #endregion

        /// <summary>
        /// Event Handler for when the Finish button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_FinishButtonClick(object sender, CancelEventArgs e)
        {
            //Close the form
            Close();

            //Remove the application from memory
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// Event Handled for when the Cancel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_CancelButtonClick(object sender, CancelEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// handles page changing events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            //shut off(reset) warning labels while navigating
            lblWarningWPSP.Visible = false;
            lblWarningWPDS.Visible = false;
            lblWarningWPRMX.Visible = false;

            if (e.OldPage == wpConfirmation && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //If there is only one database, select it by default
                if (lvDatabases.Items.Count == 1)
                {
                    lvDatabases.Focus();
                    lvDatabases.Items[0].Selected = true;
                }
            }

            if (e.OldPage == wpSelectProcess && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //show warning if nothing is selected and stop navigation
                if (!cbLegacy.Checked && !cbRMX.Checked)
                {
                    lblWarningWPSP.Visible = true;
                    e.Cancel = true;
                }

                //switch to appropriate page based on selection
                if (cbLegacy.Checked)
                {
                    wizard1.SelectedPage = wpLegacy;
                    e.Cancel = true;
                }
                else
                {
                    wizard1.SelectedPage = wpRMXCurrent;
                    e.Cancel = true;
                }
            }

            //navigating forward from database selection screen
            if (e.OldPage == wpDSNSelection && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //do not change page if no database has been selected
                if (lvDatabases.SelectedItems.Count == 0)
                {
                    lblWarningWPDS.Visible = true;
                    e.Cancel = true;
                }
                else
                {
                    //Get the selected DSN information
                    GetDSNSelections();
                } // else
            }

            //rsolanki2: mits 21693
            if (e.NewPage == wpDSNSelection && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                LblDsnList.Visible = false;
                LblDsnListHeader.Visible = false;            
            }

            //navigating backward and the landing page is the database selected form
            if (e.OldPage == wpSelectProcess && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                wizard1.SelectedPage = wpDSNSelection;
                e.Cancel = true;
                //if navigating back to dsn selection page, clear current selections
                //currently selected item highlighting is not working, this will prevent
                //the possibility of proceeding without a selection
                lvDatabases.SelectedItems.Clear();
                
            }

            //navigating from either Upgrade page backwards
            if ((e.OldPage == wpRMXCurrent | e.OldPage == wpLegacy) && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                //return to process selection page
                wizard1.SelectedPage = wpSelectProcess;
                e.Cancel = true;
            }

            //navigating forward from LEGACY option page to status page
            if(e.OldPage == wpLegacy && e.PageChangeSource==eWizardPageChangeSource.NextButton)
            {
                if(!radioRMX.Checked && !radioRMNet.Checked)
                {
                    lblWarningWPLGCY.Visible = true;
                    e.Cancel = true;
                    return;
                }

                wizard1.SelectedPage = wpStatus;

                //Run legacy upgrade
                ConvertLegacyPV();

                e.Cancel = true;
            }

            if(e.OldPage == wpStatus && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                //processing is finished and the user want to select new options
                wizard1.SelectedPage = wpDSNSelection;
                e.Cancel = true;
            }

            //navigating forward from RMX R5 option page to status page
            if(e.OldPage == wpRMXCurrent && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //verify an upgrade option was selected
                if(!rdbJurisData.Checked && !rdbPV.Checked && !rdbPVJuris.Checked && !rdbSingleForm.Checked)
                {
                    lblWarningWPRMX.Visible = true;
                    e.Cancel = true;
                    return;
                }

                //naviagate to status page and run legacy upgrade
                UpdateStatus("Upgrading RMX views....", false);
                wizard1.SelectedPage = wpStatus;

                //commented out so wizard navigation could be tested
                //Set default values for the progress bar control
                // Set Minimum to 1 to represent the first file being copied.
                progressBar1.Minimum = 0;
                // Set the initial value of the ProgressBar.
                progressBar1.Value = 0;
                // Set the Step property to a value of 1 to represent each file being copied.
                progressBar1.Step = 1;

                var FormValues = new {SelectedDSN = m_strSelectedDSN, SpecificPowerView = txtFormName.Text.Trim().ToUpper() };

                try
                {
                    ConvertPVtoAspx(FormValues.SelectedDSN, FormValues.SpecificPowerView);
                    lblStatus.Text = "Powerviews successfully converted for RISKMASTER";
                }
                catch (ApplicationException ex)
                {
                    lblStatus.Text = ex.Message;
                    e.Cancel = true;
                } // catch
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// lvDatabases_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvDatabases_Click(object sender, EventArgs e)
        {
            //shut of warning label if a selection is made
            if (lblWarningWPDS.Visible)
            {
                lblWarningWPDS.Visible = false;
            }
        }

        /// <summary>
        /// cbLegacy_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbLegacy_Click(object sender, EventArgs e)
        {
            //shut off warning label if the checkbox is selected and the warning is visible
            if (lblWarningWPSP.Visible)
            {
                lblWarningWPSP.Visible = false;
            }
        }

        /// <summary>
        /// cbRMX_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbRMX_Click(object sender, EventArgs e)
        {
            //shut off warning label if the checkbox is selected and the warning is visible
            if (lblWarningWPSP.Visible)
            {
                lblWarningWPSP.Visible = false;
            }
        }

        /// <summary>
        /// Enables updating a single Form if the radio button is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbSingleForm_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbSingleForm.Checked)
            {
                txtFormName.Enabled = true;
            } // if
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_xdocOutput"></param>
        private void CreateSaveXmlIn(ref XmlDocument p_xdocOutput)
        {
            //XmlNode objNode = p_xdocOutput.SelectSingleNode("\\Document\form");

            //p_xdocOutput.InnerXml = objNode.OuterXml;
            PVFormEdit objPVFormEdit = null;
            XmlDocument objSaveXmlIn = new XmlDocument();
            XslCompiledTransform xsl = new XslCompiledTransform();
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            //xsl.Load("Modify.xslt");
            //xsl.Transform(p_xdocOutput, xslArg, sw);

            //rsolanki2: Performance updates 
            XctModify.Transform(p_xdocOutput, xslArg, sw);
            objSaveXmlIn.LoadXml(sw.ToString());
            // Call Save
            objPVFormEdit = new Riskmaster.Application.RMUtilities.PVFormEdit(m_strUser, m_strPassword, m_DSN, m_iDSNId.ToString(), m_ConnectionString, m_iClientId);
            objPVFormEdit.Save(objSaveXmlIn);
        }

        /// <summary>
        /// GetTemplate
        /// </summary>
        /// <param name="sFormName"></param>
        /// <param name="iViewId"></param>
        /// <returns></returns>
        private string GetTemplate(string sFormName, int iViewId)
        {
            StringBuilder sXmlIn = new StringBuilder();
            XDocument xDoc = new XDocument(new XElement("form",
                new XElement("group",
                    new XElement("sysrequired"),
                    new XElement("CommandButtons"),
                    new XElement("FormFields"),
                    new XElement("FormList"),
                    new XElement("ButtonList"),
                    new XElement("ToolBarButtons"),
                    new XElement("ToolBarList"),
                    new XElement("PVFormEdit",
                        new XElement("FormName", sFormName),
                        new XElement("Reset", 0),
                        new XElement("Fields"),
                        new XElement("Captions"),
                        new XElement("Helps"),
                        new XElement("ReadOnly"),
                        new XElement("Buttons"),
                        new XElement("ButtonCaptions"),
                        new XElement("Toolbar"),
                        new XElement("ToolbarCaptions")
                    ),
                    new XElement("PVList",
                        new XElement("RowId", iViewId.ToString())),
                    new XElement("OnlyTopDownLayout", "False"),
                    new XElement("ReadOnlyForm", "False"),
                    new XElement("IsTopDownLayout", "False"),
                    new XElement("IgnoreScripting", "True")
                )
            ));

            //return the XDocument as a string
            return xDoc.ToString();
        }
    }
}