﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="*">
      <form>
        <group>
          <sysrequired><xsl:value-of select="//sysrequired/@value"/></sysrequired>
          <CommandButtons>
          </CommandButtons>
          <FormFields>
          </FormFields>
          <FormList>
          </FormList>
          <ButtonList>
          </ButtonList>
          <ToolBarButtons>
          </ToolBarButtons>
          <ToolBarList>
          </ToolBarList>
          <PVFormEdit>
            <FormName><xsl:value-of select="//PVFormEdit/FormName"/></FormName>
            <Reset><xsl:value-of select="//PVFormEdit/Reset"/></Reset>
            <Fields><xsl:for-each select="//FormList/FormListoption">
              <!--<xsl:value-of select="substring-before(@value,'|')"/>|-->
              <xsl:choose>
                <xsl:when test="(contains(@value,'|'))"><xsl:value-of select="substring-before(@value,'|')"/>|</xsl:when>
                <xsl:otherwise><xsl:value-of select="@value"/>|</xsl:otherwise>
              </xsl:choose>
              </xsl:for-each></Fields>
            <Captions><xsl:for-each select="//FormList/FormListoption"><xsl:value-of select="."/><xsl:if test="not(contains(.,'|'))">|</xsl:if></xsl:for-each></Captions>
            <Helps><xsl:for-each select="//FormList/FormListoption">
              <!--<xsl:value-of select="substring-before(@value,'|')"/>|-->
              <xsl:choose>
                <xsl:when test="(contains(@value,'|'))"><xsl:value-of select="substring-after(@value,'|')"/>|</xsl:when>
                <xsl:otherwise><xsl:value-of select="@value"/>|</xsl:otherwise>
              </xsl:choose>
              </xsl:for-each></Helps>
            <ReadOnly>
              <xsl:for-each select="//FormList/FormListoption">
                <xsl:choose>
                  <xsl:when test="(contains(@powerviewreadonly,'|'))"><xsl:value-of select="substring-before(@powerviewreadonly,'|')"/>|</xsl:when>
                  <xsl:otherwise><xsl:value-of select="@powerviewreadonly"/>|</xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </ReadOnly>
            <Buttons><xsl:for-each select="//ButtonList/ButtonListoption"><xsl:value-of select="@value"/><xsl:if test="not(contains(@value,'|'))">|</xsl:if></xsl:for-each></Buttons>
            <ButtonCaptions><xsl:for-each select="//ButtonList/ButtonListoption"><xsl:value-of select="."/><xsl:if test="not(contains(@value,'|'))">|</xsl:if></xsl:for-each></ButtonCaptions>
            <Toolbar><xsl:for-each select="//ToolBarList/ToolBarListoption"><xsl:value-of select="@value"/><xsl:if test="not(contains(@value,'|'))">|</xsl:if></xsl:for-each></Toolbar>
            <ToolbarCaptions><xsl:for-each select="//ToolBarList/ToolBarListoption"><xsl:value-of select="."/><xsl:if test="not(contains(@value,'|'))">|</xsl:if></xsl:for-each></ToolbarCaptions>
          </PVFormEdit>
          <PVList>
            <RowId><xsl:value-of select="//PVList/RowId"/></RowId>
          </PVList>
          <OnlyTopDownLayout><xsl:choose><xsl:when test="//OnlyTopDownLayout='1'">True</xsl:when><xsl:otherwise>False</xsl:otherwise> </xsl:choose></OnlyTopDownLayout>
          <ReadOnlyForm><xsl:choose><xsl:when test="//ReadOnlyForm='1'">True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></ReadOnlyForm>
          <IsTopDownLayout><xsl:choose><xsl:when test="//IsTopDownLayout='1'">True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></IsTopDownLayout>
          <IgnoreScripting>True</IgnoreScripting>
          <IgnoreASPXGeneration>True</IgnoreASPXGeneration>
        </group>
      </form>
    </xsl:template>
</xsl:stylesheet>
