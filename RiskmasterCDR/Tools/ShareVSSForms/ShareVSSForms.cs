using System;
using System.Xml;
using System.IO;
using System.Diagnostics;
	/// <summary>
	/// Ensure all VSS form files are shared properly across Riskmaster products.
	/// Uses config information from App.config and ProjectMap.xml files at runtime.
	/// </summary>
namespace Riskmaster.Tools
{
	class App
	{
		static System.Configuration.AppSettingsReader m_settings = new System.Configuration.AppSettingsReader();
		const string WORK_PATH_KEY = "WorkPath";
		const string SS_DIR = "SsDir";
		const string VSS_ID_KEY = "VssId";
		const string VSS_PWD_KEY = "VssPwd";
		const string VSS_EXE_KEY = "VssExe";
		const string PROJECT_MAP_KEY = "ProjectMap";
		const string LOG_KEY = "LogFile";


		static private XmlDocument objMap = new XmlDocument();
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			
			XmlElement elt = null;
			bool bRecurse = false;
			LogMsg("*** Beginning Project Shares Verification ***");

			//Fetch Projects for which to verify the shares.
			objMap.Load((string)m_settings.GetValue(PROJECT_MAP_KEY,typeof(string)));
			
			foreach(XmlNode nd in objMap.SelectNodes("/Map/Project"))
			{
				elt = nd as XmlElement;
				bRecurse = (elt.GetAttribute("recursive").ToLower()=="true");
                LogMsg(String.Format("*** Verifying Project shares {0} for {1}",bRecurse?"(recursive)":"", elt.GetAttribute("vsspath")));
				VerifyProjectShares(elt.GetAttribute("vsspath"),bRecurse);
			}
			LogMsg("*** Completed Project Shares Verification ***");

		}
		static void ClearWorkFolder()
		{
			//Clear out Working Area
			DirectoryInfo objDir = new DirectoryInfo((string)m_settings.GetValue(WORK_PATH_KEY,typeof(string)));
			DirectoryInfo objDirParent = objDir.Parent;
			string sScratchFolder = objDir.Name;
			objDir.Delete(true);
			objDirParent.CreateSubdirectory(sScratchFolder);
		}

		static private void VerifyProjectShares(string sProj,bool bRecursive)
		{
			string sResult;
			ClearWorkFolder();
			DirectoryInfo objDir = new DirectoryInfo((string)m_settings.GetValue(WORK_PATH_KEY,typeof(string)));

			// Recursive VSS Get to WorkingPath
			string sParams = String.Format(@" get ""{0}"" {1} -W -I- ",sProj,bRecursive ?"-R":"");
			InvokeVSS(sParams,out sResult);

			RecursiveWalk(objDir,bRecursive,sProj);
		}
		static private void RecursiveWalk(DirectoryInfo objDir,bool bRecurse,string sProj)
		{
			XmlElement elt = (XmlElement) objMap.SelectSingleNode(String.Format("/Map/Project[@vsspath='{0}']",sProj));
			string sFilter = elt.GetAttribute("dirfilter");

			foreach(FileInfo objFile in objDir.GetFiles(sFilter))
				VerifyFileShares(objFile,sProj);

			string sExceptSubPath;
			if(bRecurse)
				foreach(DirectoryInfo objSubDir in objDir.GetDirectories()) //Breadth First Traversal of Directory Tree
				{
					sExceptSubPath = objSubDir.FullName.Replace((string)m_settings.GetValue(WORK_PATH_KEY,typeof(string)),"").Replace("/","\\");
					if(null==objMap.SelectSingleNode(String.Format("/Map/Project[@vsspath='{0}']/Exception[@subpath='{1}']",sProj,sExceptSubPath)))
						RecursiveWalk(objSubDir,bRecurse,sProj);
				}
		}

		static private string LineBreak(string sPath)
		{
			if(sPath.Length==80)
				return sPath;

			char[] arr = sPath.ToCharArray();
			string sRet="";
			for(int i=0;i<sPath.Length;i++)
			{ 
				if(((i+1)% 80) ==0)
					sRet +="\r\n";

				sRet += arr[i];
			}
			return sRet;
		}
		static private bool VerifyFileShares(FileInfo objFile, string sProj)
		{
			bool bDoShare = false;
			string sWorkPath = (string) m_settings.GetValue(WORK_PATH_KEY,typeof(string));
			string sRootProjPath = sProj;
			string sProjPath = objFile.DirectoryName.Replace(sWorkPath,sRootProjPath).Replace("\\","/") + "/";
			string sProjRecursiveSegment = objFile.DirectoryName.Replace(sWorkPath,"").Replace("\\","/");
			//string sSplitProjPath = LineBreak(sProjPath);
			string sSplitReqSharePath="";
			if(sProjRecursiveSegment=="/")
				sProjRecursiveSegment="";

			string sResult ="";
			string sTmp="";
			try
			{
				string sParams = String.Format(@" links ""{0}"" -I-", 
					sProjPath + objFile.Name);

				if(0!=InvokeVSS(sParams,out sResult))
					return false;

				//Got Cmd Output from SS into sResult - examine to see if all desired links are present.
				XmlElement elt;
				String sReqSharePath="";
				String sExtFilter =""; 
				foreach(XmlNode nd in objMap.SelectNodes(String.Format("/Map/Project[@vsspath='{0}']/Share",sProj)))
				{//For each Reference to check for this project (based on the Map xml file)
					elt = nd as XmlElement;
					sReqSharePath = elt.GetAttribute("vsspath") + sProjRecursiveSegment + "\r\n";
					sSplitReqSharePath=LineBreak(sReqSharePath);
					sExtFilter =elt.HasAttribute("extfilter")?elt.GetAttribute("extfilter"):""; 
					if(objFile.Name.EndsWith(sExtFilter) && sResult.IndexOf(sSplitReqSharePath)<=0)
					{
						//Not Shared with Target
						//Set current project and share it.
						sParams = String.Format(@" cp ""{0}"" -I- ",sReqSharePath.Replace("\r\n",""));
						bDoShare = (0==InvokeVSS(sParams,out sTmp));
						if(!bDoShare)
						{
							bDoShare = ForceCreateVSSPath(sReqSharePath.Replace("\r\n",""),sProj);
						}

						if(bDoShare)
						{
							sParams = String.Format(@" share ""{0}"" -I- ",sProjPath + objFile.Name);
							InvokeVSS(sParams,out sTmp);
						}
						//return false;
					}
				}
			}
			catch{return false;}
			return true;
		}

		static private bool ForceCreateVSSPath(string sPath,string sProj)
		{
			string sParams ="";
			string sPartialPath="";
			string sTmp="";
			bool bDoShare;
			string[] arr = sPath.Replace(sProj,"").Split('/');
			for(int i=0;i<arr.Length;i++)
			{
				sPartialPath=sPath.Substring(0,sPath.IndexOf(arr[i]) + arr[i].Length);
				//Share Project May Not Exist
				sParams = String.Format(@" cp ""{0}"" -I- ",sPartialPath);
				if(0!=InvokeVSS(sParams,out sTmp))
				{
					sParams = String.Format(@" create ""{0}"" -I- ",sPartialPath);
					InvokeVSS(sParams,out sTmp);
				}
			}

			 //Try setting to current again after creating project.
			sParams = String.Format(@" cp ""{0}"" -I- ",sPath);
			bDoShare= (0==InvokeVSS(sParams,out sTmp));
			
			if (!bDoShare)
				Console.WriteLine("Error Creating Share Target Project: " + sPath);
			
			return bDoShare;
		
		}
		static private int InvokeVSS(string sParams,  out string sResult)
		{
			Process proc = new  Process();
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.EnvironmentVariables.Add("SSDIR",(string)m_settings.GetValue(SS_DIR,typeof(string)));
			proc.StartInfo.FileName ="\"" +(string)m_settings.GetValue(VSS_EXE_KEY,typeof(string)) + "\"";
			proc.StartInfo.WorkingDirectory = (string) m_settings.GetValue(WORK_PATH_KEY,typeof(string));
			proc.StartInfo.RedirectStandardOutput = true;
//			DirectoryInfo objDir = new DirectoryInfo(proc.StartInfo.WorkingDirectory);
//			FileInfo[] arr = objDir.GetFiles("cmd-result.txt");
//			if(arr.Length==1)
//				arr[0].Delete();

			LogMsg(sParams);
			//Apply User Identity Params (Common to All Commands)
			proc.StartInfo.Arguments = sParams + 
			String.Format(" -Y{0},{1}",
				m_settings.GetValue(VSS_ID_KEY,typeof(string)),
				m_settings.GetValue(VSS_PWD_KEY,typeof(string)));

			proc.Start();
			sResult = proc.StandardOutput.ReadToEnd();
			
			proc.WaitForExit();
			LogMsg(sResult);
			return proc.ExitCode;

		}
		static void LogMsg(string sMsg)
		{
			string sLogFile = (string)m_settings.GetValue(LOG_KEY,typeof(string));
			System.IO.StreamWriter stm = new StreamWriter(sLogFile,true);
			stm.WriteLine(DateTime.Now.ToString("yyyyMMddhhmmss") + "\t" + sMsg);
			stm.Flush();
			stm.Close();

		}
	}
}