﻿namespace BatchFroiAndAcord
{
    partial class FroiAndAcordPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkBox1 = new System.Windows.Forms.CheckBox();
            this.lblFROI = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.chkBox2 = new System.Windows.Forms.CheckBox();
            this.lblACORD = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(45, 117);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(126, 117);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkBox1
            // 
            this.chkBox1.AutoSize = true;
            this.chkBox1.Location = new System.Drawing.Point(154, 71);
            this.chkBox1.Name = "chkBox1";
            this.chkBox1.Size = new System.Drawing.Size(15, 14);
            this.chkBox1.TabIndex = 6;
            this.chkBox1.UseVisualStyleBackColor = true;
            // 
            // lblFROI
            // 
            this.lblFROI.AutoSize = true;
            this.lblFROI.Location = new System.Drawing.Point(54, 72);
            this.lblFROI.Name = "lblFROI";
            this.lblFROI.Size = new System.Drawing.Size(63, 13);
            this.lblFROI.TabIndex = 7;
            this.lblFROI.Text = "Batch FROI";
            this.lblFROI.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(2, 9);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(272, 59);
            this.lblMessage.TabIndex = 8;
            this.lblMessage.Text = "Select the  options for  FROI/ACORD.";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkBox2
            // 
            this.chkBox2.AutoSize = true;
            this.chkBox2.Location = new System.Drawing.Point(154, 91);
            this.chkBox2.Name = "chkBox2";
            this.chkBox2.Size = new System.Drawing.Size(15, 14);
            this.chkBox2.TabIndex = 9;
            this.chkBox2.UseVisualStyleBackColor = true;
            // 
            // lblACORD
            // 
            this.lblACORD.AutoSize = true;
            this.lblACORD.Location = new System.Drawing.Point(54, 92);
            this.lblACORD.Name = "lblACORD";
            this.lblACORD.Size = new System.Drawing.Size(76, 13);
            this.lblACORD.TabIndex = 10;
            this.lblACORD.Text = "Batch ACORD";
            // 
            // FroiAndAcordPrompt
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(278, 157);
            this.Controls.Add(this.lblACORD);
            this.Controls.Add(this.chkBox2);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblFROI);
            this.Controls.Add(this.chkBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.MaximizeBox = false;
            this.Name = "FroiAndAcordPrompt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Batch Print FROI/ACORD";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkBox1;
        private System.Windows.Forms.Label lblFROI;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.CheckBox chkBox2;
        private System.Windows.Forms.Label lblACORD;
    }
}