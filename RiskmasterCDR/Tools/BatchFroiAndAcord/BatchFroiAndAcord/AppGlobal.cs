﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;
using System.Configuration;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using Riskmaster.Models;
using System.ServiceModel;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace BatchFroiAndAcord
{
      public class AppGlobals
    {
        public static UserLogin Userlogin;
        public static string ConnectionString;
        public static int iUserId = 0; //user ID
        public static string sUser = ""; //user login
        public static string sPwd = ""; //password
        public static string sDSN = ""; //database name
        public static bool bSilentMode = false;
        public static bool bShowDialog = true;
        public static bool bDisableControls = false;
        // added by atavaragiri mits 25686//
        public static string sBaseLocation = System.Configuration.ConfigurationManager.AppSettings["BaseServiceUrl"].ToString();
        public static int iTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TimeOut"]);
        // end 25686 //
       public enum HttpVerb
        {
            GET,
            POST,
            PUT,
            DELETE
        }
       //public static StringDictionary sdUserDocPaths = new StringDictionary();
       public static string SecurityDSN
       {
           get
           {
               return ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
           }//get
       }//property: SecurityDSN
        public static T GetResponse<T>(string uri, HttpVerb oMethod, string ContentType, object oData)
        {
            WebXClient client = null;
            string data = string.Empty;
            string responseString = string.Empty;
            try
            {
                client = new WebXClient(iTimeout);
                uri = uri.Trim('/');
                uri = sBaseLocation + uri;
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = ContentType; // working
                if (oMethod == HttpVerb.GET)
                {
                    responseString = client.DownloadString(uri);
                }
                else
                {
                    data = "{\"o" + oData.GetType().Name + "\": " + JSONSerializeDeserialize.Serialize(oData) + "}";
                    responseString = client.UploadString(uri, oMethod.ToString(), data);
                }
                return JSONSerializeDeserialize.DeserializeJSon<T>(responseString);
            }
            catch (WebException wex)
            {
                string exMessage = string.Empty;
                if (wex.Response != null)
                {
                    using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                    {
                        exMessage = r.ReadToEnd();
                    }
                    // the fault xml 
                    if (!string.IsNullOrEmpty(exMessage))
                    {
                        XElement oXFault = XElement.Parse(exMessage);
                        XNamespace ns = oXFault.Name.Namespace.NamespaceName;
                        var namespaceManager = new XmlNamespaceManager(new NameTable());
                        namespaceManager.AddNamespace("rmaprefix", oXFault.Name.Namespace.NamespaceName);
                        XElement oReason = oXFault.XPathSelectElement(".//rmaprefix:Reason/rmaprefix:Text", namespaceManager);
                        RMException theFault = new RMException();
                        try
                        {
                            XElement oXFaultReason = XElement.Parse(oReason.Value);
                            theFault.Errors = oReason.Value;
                            throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
                        }
                        catch
                        {
                            throw new FaultException<RMException>(theFault, new FaultReason(oReason.Value), new FaultCode("Sender"));
                        }
                    }
                    else
                    {
                        RMException theFault = new RMException();
                        throw new FaultException<RMException>(theFault, new FaultReason(wex.Message), new FaultCode("Sender"));
                    }
                }
                return default(T);
            }
            catch (Exception ex)
            {
                return default(T);
            }
            finally
            {
                if (client != null)
                {
                    client = null;
                }
            }
        }
    }
  }
public class JSONSerializeDeserialize
{
    /// <summary>
    /// Serialize the object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string Serialize<T>(T obj)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
        MemoryStream ms = new MemoryStream();
        serializer.WriteObject(ms, obj);
        string retVal = Encoding.UTF8.GetString(ms.ToArray());
        ms.Close();
        return retVal;
    }
    /// <summary>
    /// Deserialize the object with property exposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="json"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string json)
    {
        T obj1 = Activator.CreateInstance<T>();
        MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj1.GetType());
        if (json.Length > 0 && json.Substring(0, 1) == "{")
        {
            obj1 = (T)serializer.ReadObject(ms);
        }
        ms.Close();
        return obj1;
    }
    /// <summary>
    /// Deserialize the return json
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="jsonString"></param>
    /// <returns></returns>
    public static T DeserializeJSon<T>(string jsonString)
    {
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        if (stream.Length == 0) return default(T);
        T obj = (T)ser.ReadObject(stream);
        stream.Close();
        return obj;
    }
}
/// <summary>
/// Class to override/set the timeout for any request 
/// </summary>
public class WebXClient : WebClient
{
    private int _timeout;
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="p_timeout"></param>
    public WebXClient(int p_timeout)
    {
        this._timeout = p_timeout;
    }
    /// <summary>
    /// GetWebRequest
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    protected override WebRequest GetWebRequest(Uri address)
    {
        var request = base.GetWebRequest(address);
        request.Timeout = this._timeout;
        return request;
    }
}
