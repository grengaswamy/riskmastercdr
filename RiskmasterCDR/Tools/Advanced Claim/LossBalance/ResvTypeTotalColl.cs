﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.LossBalance
{
    public class ResvTypeTotalColl
    {

        private System.Collections.ArrayList mCol;
        public ResvTypeTotalData Add(string ReserveType, long LossDetailCount, long ReserveCount, long PaymentCount, float PaidAmount, float ReserveAmount, float ReserveChange, string sKey)
        {
            ResvTypeTotalData objNewMember = default(ResvTypeTotalData);
            objNewMember = new ResvTypeTotalData();

            objNewMember.ReserveType = ReserveType;
            objNewMember.LossDetailCount = LossDetailCount;
            objNewMember.ReserveCount = ReserveCount;
            objNewMember.PaymentCount = PaymentCount;
            objNewMember.PaidAmount = PaidAmount;
            objNewMember.ReserveAmount = ReserveAmount;
            objNewMember.ReserveChange = ReserveChange;

            //if (Strings.Len(sKey) == 0)
            //{
            //    mCol.Add(objNewMember);
            //}
            //else
            //{
            //    mCol.Add(objNewMember, sKey);
            //}
            for (int i = 0; i < Count; i++)
            {
                if ((((ResvTypeTotalData)mCol[i])).ReserveType == ReserveType)
                {
                    Remove(i);
                    mCol.Add(objNewMember);
                }
            }
            return objNewMember;

        }

        public ResvTypeTotalData Item(string vntIndexKey)
        {
            // ResvTypeTotalData functionReturnValue = default(ResvTypeTotalData);
            //On Error GoTo 0
            //On Error Resume Next
            //functionReturnValue = mCol(vntIndexKey);
            //if (Err.Number != 0)
            //{
            //    Add(vntIndexKey, 0, 0, 0, 0, 0, 0, vntIndexKey);
            //    functionReturnValue = mCol(vntIndexKey);
            //    Err.Clear();
            //}
            return (ResvTypeTotalData)mCol[Convert.ToInt32(vntIndexKey)];
        }

        public long Count
        {
            get { return mCol.Count; }
        }

        public void Remove(object vntIndexKey)
        {
            mCol.Remove(vntIndexKey);
        }

        public System.Collections.IEnumerator NewEnum //IUnknown
        {
            get { return mCol.GetEnumerator(); }
        }

        public ResvTypeTotalColl()
        {
            mCol = new System.Collections.ArrayList();
        }

        ~ResvTypeTotalColl()
        {
            mCol = null;
        }


    }
}
