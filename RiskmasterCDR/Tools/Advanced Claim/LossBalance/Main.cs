﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
namespace CCP.LossBalance
{
    public class Main
    {
        //Start SI05869
        //Public XMLobj As CCPXF.cXML
        //Public m_DBG As Object   'CCPUF.CDebug       'SI05857
        //public XMLControls As CCPXS.cXMLControls
        //NOTE: This object must ONLY be set by CLossBalance
        public  CCP.XmlSupport.XMLBalance XMLBalance;
        //public  LossBalance objBal;
        public  CCP.XmlComponents.XMLClaim xmlClaim;
        public  CCP.XmlComponents.XMLLossDetail xmlLossDtl;
        //End SI05869

        private  string sClaimNumber;
        private  string strSourceDB;
        private  string strTargetDB;
        private  string sActivityType;
        private  string sReserveType;
        private  string sTransType;
        private  float cReserveAmount;
        private  float cChangeAmount;
        private  float cPaymentAmount;
        private  float cCheckAmount;
        private  long lClaimCount;
        private  long lOffsetOnsetCount;
        private  long lUnitStatCount;
        private  long lEmployeeCount;
        private  long lLossDetailCount;
        private  long lFinancialCount;
        private  long lReserveCount;
        private  long lPaymentCount;
        private  float cReserveTotal;
        private  float cChangeTotal;
        private  float cPaymentTotal;
        private  float cCheckTotal;
        private  bool bTotalsInXML;
        private  bool bDoResvTypeTotal;
        private  bool bDoClaimTotal;
        private  ResvTypeTotalColl colResvTypeTot;
        private  ClaimTotalsColl colClaimTot;
        private  ResvTypeTotalData objResvTypeData;

        private  ClaimTotalsData objClaimData;
        public const string conClaimCnt = "CLAIM_COUNT";
        public const string conOnOffCnt = "OFFSET_ONSET_COUNT";
        public const string conUSCnt = "UNIT_STAT_COUNT";
        public const string conEmpCnt = "EMPLOYEE_COUNT";
        public const string conLDCnt = "LOSS_DETAIL_COUNT";
        public const string conFinCnt = "FINANCIAL_COUNT";
        public const string conRsvCnt = "RESERVE_COUNT";
        public const string conPayCnt = "PAYMENT_COUNT";
        public const string conRsvTot = "RESERVE_TOTAL";
        public const string conRsvChngTot = "RESERVE_CHANGE_TOTAL";
        public const string conPayTot = "PAYMENT_TOTAL";

        public const string conChkTot = "CHECK_TOTAL";

        public  Debug m_Dbg()
        {
            //CCPUF.CDebug
            return LossBalance.Instance.CDebug;

        }
        public  CCP.XmlFormatting.XML XMLObj
        {
            get { return LossBalance.Instance.cXML; }
        }

        //Start SI05869
        public  CCP.XmlComponents.XMLClaim XMLClaim
        {
            get
            {
             return xmlClaim;
            }
            set
            {
                xmlClaim = value;
            }
        }


        public  void ProcessLossXML(object oParent)
        {
            Debug dbg = m_Dbg();
            dbg.PushProc("ProcessLossXML");

            //'  Set s = ObjBalance

            xmlClaim = new CCP.XmlComponents.XMLClaim();
            if ((oParent != null))
            {
                xmlClaim.XML = (CCP.XmlFormatting.XML)oParent;
            }
            else
            {
                xmlClaim.XML = LossBalance.Instance.cXML;
            }

            bDoResvTypeTotal = LossBalance.Instance.DoResvTypeTotal;
            bDoClaimTotal = LossBalance.Instance.DoClaimTotal;

            if (bDoResvTypeTotal == true)
            {
                colResvTypeTot = new ResvTypeTotalColl();
                LossBalance.Instance.TotalsByReserveType = colResvTypeTot;
            }
            if (bDoClaimTotal == true)
            {
                colClaimTot = new ClaimTotalsColl();
                LossBalance.Instance.TotalsByClaim = colClaimTot;
            }

            strSourceDB = XMLObj.Header.SourceDB;
            strTargetDB = XMLObj.Header.TargetDB;

            ParseClaimDocXML();

            //'  Set XMLControls = New CCPXS.cXMLControls
            //'  Set XMLControls.CDebug = m_Dbg
            //'  Set XMLControls.cXML = XMLObj
            XMLBalance = new CCP.XmlSupport.XMLBalance();
            if ((oParent != null))
            {
                //'     Set XMLBalance.Parent = objBal.cXML
                //'    Else
                XMLBalance.Parent = (CCP.XmlFormatting.XMLACNode)oParent;
            }
            ParseBalanceTotalsXML();

            if (bTotalsInXML == true)
            {
                CompareBalanceTotals();
            }

            //xmlClaim = null;
            //'  Set objBal = Nothing
            //XMLBalance = null;
            //'  Set XMLControls = Nothing

            dbg.PopProc();

        }

        public  void ParseClaimDocXML()
        {
            Debug dbg = m_Dbg();
            bool rst = false;

            dbg.PushProc("ParseClaimDocXML");

            LossBalance.Instance.ConditionCode = "0";
            LossBalance.Instance.ClaimCount = 0;
            LossBalance.Instance.UnitStatCount = 0;
            LossBalance.Instance.EmployeeCount = 0;
            LossBalance.Instance.OffsetOnsetCount = 0;
            LossBalance.Instance.LossDetailCount = 0;
            LossBalance.Instance.FinancialTransCount = 0;
            LossBalance.Instance.ReserveCount = 0;
            LossBalance.Instance.PaymentCount = 0;
            LossBalance.Instance.ReserveAmount = 0;
            LossBalance.Instance.ReserveChgAmount = 0;
            LossBalance.Instance.PaymentAmount = 0;
            LossBalance.Instance.CheckAmount = 0;

            rst = true;

            LossBalance.Instance.ClaimCount = xmlClaim.Count;

            while ((xmlClaim.getClaim(null, rst) != null))
            {
                rst = false;
                ParseClaim();
            }

            dbg.PopProc();

        }
        private  void ParseClaim()
        {
            bool bWCompDataProcessed = false;
            bool rst = false;
            Debug dbg = m_Dbg();

            dbg.PushProc("LossBalanceMain.ParseClaim");

            sClaimNumber = xmlClaim.ClaimNumber;
            //SI04558

            if (bDoClaimTotal == true)
            {
                objClaimData = LossBalance.Instance.TotalsByClaim.Item(sClaimNumber);
            }

            bWCompDataProcessed = false;

            if ((xmlClaim.UnitStats.Node != null))
            {
                bWCompDataProcessed = true;
                LossBalance.Instance.UnitStatCount = LossBalance.Instance.UnitStatCount + 1;
                //SI06420 Start
                if ((xmlClaim.UnitStats.Employee.Node != null))
                {
                    bWCompDataProcessed = true;
                    LossBalance.Instance.EmployeeCount = LossBalance.Instance.EmployeeCount + 1;
                }
                //SI06420 End
            }
            //SI06420 Start
            //   If Not xmlClaim.UnitStats.Employee.Node Is Nothing Then
            //      bWCompDataProcessed = True
            //      LossBalance.Instance.EmployeeCount = LossBalance.Instance.EmployeeCount + 1
            //   End If
            //SI06420 Start

            if (bWCompDataProcessed == true)
            {
                if (bDoClaimTotal == true)
                {
                    objClaimData.UnitStatCount = objClaimData.UnitStatCount + 1;
                }
            }

            if (!string.IsNullOrEmpty(xmlClaim.NewClaimNumber))
            {
                LossBalance.Instance.OffsetOnsetCount = LossBalance.Instance.OffsetOnsetCount + 1;
                if (bDoClaimTotal == true)
                {
                    objClaimData.OffsetOnsetCount = objClaimData.OffsetOnsetCount + 1;
                }
            }

            xmlLossDtl = xmlClaim.LossDetail;
            LossBalance.Instance.LossDetailCount = LossBalance.Instance.LossDetailCount + xmlLossDtl.Count;

            rst = true;
            while ((xmlLossDtl.getLossDetail(null, rst) != null))
            {
                rst = false;
                ParseLossDetail();
            }

            xmlLossDtl = null;
            dbg.PopProc();

        }
        private  void ParseLossDetail()
        {
            bool rst = false;
            Debug dbg = m_Dbg();
            dbg.PushProc("LossBalanceMain.ParseLossDetail");

            sReserveType = xmlLossDtl.ReserveType_Code;

            if (bDoResvTypeTotal == true)
            {
                objResvTypeData = LossBalance.Instance.TotalsByReserveType.Item(sReserveType);
                objResvTypeData.LossDetailCount = objResvTypeData.LossDetailCount + 1;
            }

            LossBalance.Instance.FinancialTransCount = LossBalance.Instance.FinancialTransCount + xmlLossDtl.FinTransCount;

            rst = true;
            while ((xmlLossDtl.getFinTrans(rst) != null))
            {
                rst = false;
                ParseFinancial();
            }

            dbg.PopProc();

        }
        private  void ParseFinancial()
        {
            string sAddedByUser = null;
            Debug dbg = m_Dbg();
            dbg.PushProc("LossBalanceMain.ParseFinancial");

            if (bDoClaimTotal == true)
            {
                objClaimData.TransactionCount = objClaimData.TransactionCount + 1;
            }

            sActivityType = xmlLossDtl.ActivityType_Code;
            sTransType = xmlLossDtl.TransactionType_Code;

            if ((xmlLossDtl.RsvTransNode != null))
            {
                LossBalance.Instance.ReserveCount = LossBalance.Instance.ReserveCount + 1;
                ParseReserveNode();
            }
            if ((xmlLossDtl.PayTransNode != null))
            {
                LossBalance.Instance.PaymentCount = LossBalance.Instance.PaymentCount + 1;
                ParsePaymentNode();
            }

            dbg.PopProc();

        }
        private  void ParseReserveNode()
        {
            Debug dbg = m_Dbg();
            dbg.PushProc("LossBalanceMain.ParseReserveNode");

            cReserveAmount = float.Parse(xmlLossDtl.ReserveBalance,System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            cChangeAmount = float.Parse(xmlLossDtl.ReserveChange,System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            LossBalance.Instance.ReserveAmount = LossBalance.Instance.ReserveAmount + cReserveAmount;
            LossBalance.Instance.ReserveChgAmount = LossBalance.Instance.ReserveChgAmount + cChangeAmount;

            if (bDoResvTypeTotal)
            {
                objResvTypeData.ReserveCount = objResvTypeData.ReserveCount + 1;
                objResvTypeData.ReserveAmount = objResvTypeData.ReserveAmount + cReserveAmount;
                objResvTypeData.ReserveChange = objResvTypeData.ReserveChange + cChangeAmount;
            }

            if (bDoClaimTotal)
            {
                objClaimData.ReserveAmount = objClaimData.ReserveAmount + cReserveAmount;
                objClaimData.ReserveChange = objClaimData.ReserveChange + cChangeAmount;
            }

            dbg.PopProc();

        }

        private  void ParsePaymentNode()
        {
            Debug dbg = m_Dbg();
            object vArray = null;
            string sTemp = null;

            dbg.PushProc("ParsePaymentNode");

            cPaymentAmount = float.Parse(xmlLossDtl.PayAmount, System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;
            cCheckAmount = float.Parse(xmlLossDtl.Voucher.Amount, System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;

            switch (sActivityType)
            {
                case "PO":
                    cPaymentAmount = cPaymentAmount * -1;
                    break;
                case "OA":
                    cPaymentAmount = cPaymentAmount * -1;
                    break;
            }

            if (string.IsNullOrEmpty(sTransType))
            {
                sTransType = sActivityType;
            }
            else
            {
                sTransType = sTransType;
            }

            LossBalance.Instance.PaymentAmount = LossBalance.Instance.PaymentAmount + cPaymentAmount;
            LossBalance.Instance.CheckAmount = LossBalance.Instance.CheckAmount + cCheckAmount;

            if (bDoResvTypeTotal == true)
            {
                objResvTypeData.PaymentCount = objResvTypeData.PaymentCount + 1;
                objResvTypeData.PaidAmount = objResvTypeData.PaidAmount + cPaymentAmount;
            }

            if (bDoClaimTotal == true)
            {
                objClaimData.PaidAmount = objClaimData.PaidAmount + cPaymentAmount;
            }

            dbg.PopProc();

        }

        private  void ParseBalanceTotalsXML()
        {
            Debug dbg = m_Dbg();
            dbg.PushProc("ParseBalanceTotalsXML");

            lClaimCount = 0;
            lOffsetOnsetCount = 0;
            lUnitStatCount = 0;
            lEmployeeCount = 0;
            lLossDetailCount = 0;
            lFinancialCount = 0;
            lReserveCount = 0;
            lPaymentCount = 0;
            cReserveTotal = 0;
            cChangeTotal = 0;
            cPaymentTotal = 0;
            cCheckTotal = 0;
            bTotalsInXML = false;

            if ((XMLBalance.Node != null))
            {
                bTotalsInXML = true;
                lClaimCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conClaimCnt));
                lOffsetOnsetCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conOnOffCnt));
                lUnitStatCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conUSCnt));
                lEmployeeCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conEmpCnt));
                lLossDetailCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conLDCnt));
                lFinancialCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conFinCnt));
                lReserveCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conRsvCnt));
                lPaymentCount = Convert.ToInt64(XMLBalance.DataItem.DataItemValue(conPayCnt));
                cReserveTotal = float.Parse(XMLBalance.DataItem.DataItemValue(conRsvTot),System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                cChangeTotal = float.Parse(XMLBalance.DataItem.DataItemValue(conRsvChngTot), System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;
                cPaymentTotal = float.Parse(XMLBalance.DataItem.DataItemValue(conPayTot),System.Globalization.CultureInfo.InvariantCulture.NumberFormat);;
                cCheckTotal = float.Parse(XMLBalance.DataItem.DataItemValue(conChkTot), System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;
            }

            dbg.PopProc();

        }


        private  void CompareBalanceTotals()
        {
            //int iResult = 0;
            Debug dbg = m_Dbg();
            Errors err = new Errors();
            
            dbg.PushProc("CompareBalanceTotals");
            dbg.IgnoreErrors = true;

            if (lClaimCount != LossBalance.Instance.ClaimCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Claim Count", LossBalance.Instance.ClaimCount, lClaimCount);
                LossBalance.Instance.ConditionCode = "1";
            }

            if (lOffsetOnsetCount != LossBalance.Instance.OffsetOnsetCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Offset Onset Count", LossBalance.Instance.OffsetOnsetCount, lOffsetOnsetCount);
                LossBalance.Instance.ConditionCode = "2";
            }

            if (lUnitStatCount != LossBalance.Instance.UnitStatCount)
            {
                //iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Unit Stat Count", LossBalance.Instance.UnitStatCount, lUnitStatCount);
                LossBalance.Instance.ConditionCode = "3";
            }

            if (lEmployeeCount != LossBalance.Instance.EmployeeCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Employee Count", LossBalance.Instance.EmployeeCount, lEmployeeCount);
                LossBalance.Instance.ConditionCode = "4";
            }

            if (lLossDetailCount != LossBalance.Instance.LossDetailCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Loss Detail Count", LossBalance.Instance.LossDetailCount, lLossDetailCount);
                LossBalance.Instance.ConditionCode = "5";
            }

            if (lFinancialCount != LossBalance.Instance.FinancialTransCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Financial Transaction Count", LossBalance.Instance.FinancialTransCount, lFinancialCount);
                LossBalance.Instance.ConditionCode = "6";
            }

            if (lReserveCount != LossBalance.Instance.ReserveCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Reserve Transaction Count", LossBalance.Instance.ReserveCount, lReserveCount);
                LossBalance.Instance.ConditionCode = "7";
            }

            if (lPaymentCount != LossBalance.Instance.PaymentCount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Payment Transaction Count", LossBalance.Instance.PaymentCount, lPaymentCount);
                LossBalance.Instance.ConditionCode = "8";
            }

            if (cReserveTotal != LossBalance.Instance.ReserveAmount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Reserve Amount", LossBalance.Instance.ReserveAmount, cReserveTotal);
                LossBalance.Instance.ConditionCode = "9";
            }

            if (cChangeTotal != LossBalance.Instance.ReserveChgAmount)
            {
                //iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Reserve Change Amount", LossBalance.Instance.ReserveChgAmount, cChangeTotal);
                LossBalance.Instance.ConditionCode = "10";
            }

            if (cPaymentTotal != LossBalance.Instance.PaymentAmount)
            {
                //iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "", "", "Grand Total", "Payment Amount", LossBalance.Instance.PaymentAmount, cPaymentTotal);
                LossBalance.Instance.ConditionCode = "11";
            }

            if (cCheckTotal != LossBalance.Instance.CheckAmount)
            {
               // iResult = err.ProcessAppError(ErrorCodes.ERRC_OUT_OF_BALANCE, null, "Out Of Balance Check Amount Count", "", "Grand Total", "Check Amount", LossBalance.Instance.CheckAmount, cCheckTotal);
                LossBalance.Instance.ConditionCode = "12";
            }

            dbg.IgnoreErrors = false;
            dbg.PopProc();

        }




    }
}
