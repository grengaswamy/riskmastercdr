﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.LossBalance
{
    public class ClaimTotalsColl
    {
        //private System.Collections.Hashtable mCol;
        private System.Collections.Generic.Dictionary<string, ClaimTotalsData> mCol;
        public ClaimTotalsData Add(string ClaimNumber, long OffsetOnsetCount, long UnitStatCount, long TransactionCount, float PaidAmount, float ReserveAmount, float ReserveChange, string sKey)
        {
            ClaimTotalsData objNewMember = new ClaimTotalsData();

            objNewMember.ClaimNumber = ClaimNumber;
            objNewMember.OffsetOnsetCount = OffsetOnsetCount;
            objNewMember.UnitStatCount = UnitStatCount;
            objNewMember.TransactionCount = TransactionCount;
            objNewMember.PaidAmount = PaidAmount;
            objNewMember.ReserveAmount = ReserveAmount;
            objNewMember.ReserveChange = ReserveChange;

            //if (sKey.Length == 0) VB Code
            //{
            //    mCol.Add(objNewMember);
            //}
            //else
            //{
            //    mCol.Add(objNewMember, sKey);
            //}
            if (mCol.ContainsKey(ClaimNumber) == true)
            {
                Remove(ClaimNumber);
            }
            mCol.Add(ClaimNumber, objNewMember);
            //for (int i = 0; i < Count; i++)
            //{
            //    if ((((ClaimTotalsData)mCol[i])).ClaimNumber == ClaimNumber)
            //    {
                    
            //        Remove(i);
            //        mCol.Add(ClaimNumber,objNewMember);

            //    }
            //}
            return objNewMember;
        }

        public ClaimTotalsData Item(string vntIndexKey)
        {
            try
            {
                //On Error GoTo 0
                //On Error Resume Next
                //if (Err.Number != 0)
                // {
                Add(vntIndexKey, 0, 0, 0, 0, 0, 0, vntIndexKey);
                //   functionReturnValue = mCol[vntIndexKey];
                //     Err.Clear();
                // }
                
            }
            catch { }
            return ((ClaimTotalsData)mCol[vntIndexKey]);
        }

        public long Count
        {
            get { return mCol.Count; }
        }

        public void Remove(object vntIndexKey)
        {
            mCol.Remove(Convert.ToString(vntIndexKey));
        }

        public System.Collections.IEnumerator NewEnum //IUnknown
        {
            get { return mCol.GetEnumerator(); }
        }

        public ClaimTotalsColl()
        {
            mCol = new System.Collections.Generic.Dictionary<string,ClaimTotalsData>();
        }

        ~ClaimTotalsColl()
        {
            mCol = null;
        }
    }
}
