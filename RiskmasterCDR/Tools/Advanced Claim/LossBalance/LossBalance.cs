﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.LossBalance
{
    public class LossBalance
    {

        //SI05869
        private CCP.XmlFormatting.XMLControls m_XMLControls;
        //SI05857
        private string m_sConditionCode;
        //SI05857
        private string m_sUniqueID;
        //SI05857
        private bool m_bDoResvTypeTotal;
        //SI05857
        private bool m_bDoClaimTotal;
        //SI05857
        private long m_lClaimCount;
        //SI05857
        private long m_lOffsetOnsetCount;
        //SI05857
        private long m_lUnitStatCount;
        //SI05857
        private long m_lEmployeeCount;
        //SI05857
        private long m_lLossDetailCount;
        //SI05857
        private long m_lFinancialTransCount;
        //SI05857
        private long m_lReserveCount;
        //SI05857
        private long m_lPaymentCount;
        //SI05857
        private float m_cReserveAmount;
        //SI05857
        private float m_cReserveChgAmount;
        //SI05857
        private float m_cPaymentAmount;
        //SI05857
        private float m_cCheckAmount;
        //SI05857
        private string m_sXMLDocFile;

        private static LossBalance m_instance;
        private ResvTypeTotalColl mvarTotalsByReserveType;
        private ClaimTotalsColl mvarTotalsByClaim;
        public static LossBalance Instance
        {
            get
            {
                if (m_instance == null)
                {
                        m_instance = new LossBalance();
                }

                return m_instance;
            }
        }

        private Main m_Minstance;

        public  Main MInstance
        {
            get
            {
                if (m_Minstance == null)
                {
                    m_Minstance = new Main();
                }

                return m_Minstance;
            }
        }
        ~LossBalance()
        {
            m_instance = null;
            m_Minstance = null;
        }
        //Start SI05869
        public CCP.Common.Debug CDebug
        {
            //CCPUF.CDebug      'SI05857
            get { return cXMLSControls.Debug; }

            //  CCPUF.CDebug)   'SI05857
            set { cXMLSControls.Debug = value; }
        }
        //End SI05869

        public System.Xml.XmlDocument Document
        {
            get { return cXML.Document; }

            set { cXML.Document = value; }
        }

        //Start SI05869
        public CCP.XmlFormatting.XML cXML
        {
            get { return cXMLSControls.XML; }

            set { cXMLSControls.XML = value; }
        }


        public CCP.XmlFormatting.XMLControls cXMLSControls
        {
            get
            {
                if (m_XMLControls == null)
                {
                    m_XMLControls = new CCP.XmlFormatting.XMLControls();
                }
                return m_XMLControls;
            }
            set { m_XMLControls = value; }
        }
        //End SI05869

        public ResvTypeTotalColl TotalsByReserveType
        {
            set { mvarTotalsByReserveType = value; }

            get { return mvarTotalsByReserveType; }
        }

        public ClaimTotalsColl TotalsByClaim
        {
            get { return mvarTotalsByClaim; }

            set { mvarTotalsByClaim = value; }

        }

        public void terminate()
        {
            m_instance = null;
        }

        public void ReadLoadXML()
        {
            int iSuccess = 0;
            Instance.CDebug.PushProc("ReadLoadXML");
            if (string.IsNullOrEmpty(m_sXMLDocFile))
            {
                //iSuccess = Instance.CDebug.CErrors.ProcessAppError(ErrorCodes.ERRC_FILE_NONAME, CCP.Constants.Constants.IDCANCEL, "", "");
            }
            else
            {
                if ((Instance.cXML.XMLLoadDocument(m_sXMLDocFile) != null))
                {
                    iSuccess = 0;
                }
                else
                {
                    iSuccess = ErrorCodes.ERRC_XML_ERRORS;
                }
            }
            Instance.CDebug.PopProc();
        }

        public void WriteXML()
        {
            int iSuccess = 0;
            string strTemp = null;
            Instance.CDebug.PushProc("WriteXML");
            if (string.IsNullOrEmpty(m_sXMLDocFile))
            {
                //iSuccess = m_Dbg.CErrors.ProcessAppError(ERRC_FILE_NONAME, Constants.vbOKOnly, "", "");
            }
            else
            {
                Instance.cXML.XMLSaveDocument(null, m_sXMLDocFile, CCP.Constants.cxmlFileOptions.cxmlFOOverwrite);
            }
            Instance.CDebug.PopProc();
        }

        public bool ValidateXML()
        {
            bool functionReturnValue = false;
            Instance.CDebug.PushProc("CLossBalance.ValidateXML");
            functionReturnValue = Instance.cXML.XMLValidateDocument(null);
            Instance.CDebug.PopProc();
            return functionReturnValue;
        }

        public void BalanceLossXML(object oParent)
        {
            Instance.CDebug.PushProc("CLossBalance.BalanceLossXML");
            MInstance.ProcessLossXML(oParent);
            Instance.CDebug.PopProc();
        }

        public void WriteBalanceTotals(CCP.XmlFormatting.XMLACNode oParent)
        {
            MInstance.XMLBalance = new CCP.XmlSupport.XMLBalance();
            MInstance.XMLBalance.Parent = oParent;

            MInstance.XMLBalance.DataItem.addDataItem(Main.conClaimCnt, Convert.ToString(ClaimCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conOnOffCnt, Convert.ToString(OffsetOnsetCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conUSCnt, Convert.ToString(UnitStatCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conEmpCnt, Convert.ToString(EmployeeCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conLDCnt, Convert.ToString(LossDetailCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conFinCnt, Convert.ToString(FinancialTransCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conRsvCnt, Convert.ToString(ReserveCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conPayCnt, Convert.ToString(PaymentCount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conRsvTot, Convert.ToString(ReserveAmount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conRsvChngTot, Convert.ToString(ReserveChgAmount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conPayTot, Convert.ToString(PaymentAmount), "", CCP.Constants.cxmlDataType.dtNumber);
            MInstance.XMLBalance.DataItem.addDataItem(Main.conChkTot, Convert.ToString(CheckAmount), "", CCP.Constants.cxmlDataType.dtNumber);

            MInstance.XMLBalance = null;
        }

        //Start 'SI05857
        public string ConditionCode
        {
            get
            {
                return m_sConditionCode;
            }

            set { m_sConditionCode = value; }
        }

        public string UniqueID
        {
            get { return m_sUniqueID; }

            set { m_sUniqueID = value; }
        }

        public bool DoResvTypeTotal
        {
            get { return m_bDoResvTypeTotal; }

            set { m_bDoResvTypeTotal = value; }
        }

        public bool DoClaimTotal
        {
            get { return m_bDoClaimTotal; }

            set { m_bDoClaimTotal = value; }
        }

        public long ClaimCount
        {
            get { return m_lClaimCount; }

            set { m_lClaimCount = value; }
        }

        public long OffsetOnsetCount
        {
            get { return m_lOffsetOnsetCount; }

            set { m_lOffsetOnsetCount = value; }
        }

        public long UnitStatCount
        {
            get { return m_lUnitStatCount; }

            set { m_lUnitStatCount = value; }
        }

        public long EmployeeCount
        {
            get { return m_lEmployeeCount; }

            set { m_lEmployeeCount = value; }
        }

        public long LossDetailCount
        {
            get { return m_lLossDetailCount; }

            set { m_lLossDetailCount = value; }
        }

        public long FinancialTransCount
        {
            get { return m_lFinancialTransCount; }

            set { m_lFinancialTransCount = value; }
        }

        public long ReserveCount
        {
            get { return m_lReserveCount; }

            set { m_lReserveCount = value; }
        }

        public long PaymentCount
        {
            get { return m_lPaymentCount; }

            set { m_lPaymentCount = value; }
        }

        public float ReserveAmount
        {
            get { return m_cReserveAmount; }

            set { m_cReserveAmount = value; }
        }

        public float ReserveChgAmount
        {
            get { return m_cReserveChgAmount; }

            set { m_cReserveChgAmount = value; }
        }

        public float PaymentAmount
        {
            get { return m_cPaymentAmount; }

            set { m_cPaymentAmount = value; }
        }

        public float CheckAmount
        {
            get { return m_cCheckAmount; }

            set { m_cCheckAmount = value; }
        }

        public string XMLDocFile
        {
            get { return m_sXMLDocFile; }

            set { m_sXMLDocFile = value; }
        }
        //End 'SI05857

    }
}
