/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using CCP.Constants;
using System.Windows.Forms;


namespace CCP.Common
{
    public class FileFunctions
    {
        private Debug m_Dbg;            //Debugger
        private FileInfo m_file;        //ToDo: change comments
        private string curFilePath;     //path of the current file (replaces file object, due to File being static)
        private FileStream m_tst;       //Stream to access the current file
        private StreamReader m_trd;     //Reads from current file
        private StreamWriter m_twr;     //Writes to current file
        private bool bOverWrite;        //Is overwriting allowed?
        private int m_OpenRetry;        //Number of times to retry opening a file

        //enumerator for different ways to access a file
        public enum cFileAccess
        {
            enFAForReading = CommonGlobalConstants.FileAccessTypes.ForReading,
            enFAForWriting = CommonGlobalConstants.FileAccessTypes.ForWriting,
            enFAForAppending = CommonGlobalConstants.FileAccessTypes.ForAppending
        }

        //initializes object
        public FileFunctions()
        {
            curFilePath = "";
            bOverWrite = false;
            m_OpenRetry = 0;
            m_file = null;
        }

        ~FileFunctions()
        {
            //FileClose();
        }

        //gets and sets the debugger
        public Debug CDebug
        {
            set
            {
                if ((m_Dbg != null) && (value != null) && (m_Dbg.DebugGUID == value.DebugGUID))
                {
                    return;
                }
                if (value == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.FileFunctions = this;
                }
                else
                {
                    m_Dbg = value;
                }
            }
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.FileFunctions = this;
                }
                return m_Dbg;
            }
        }

        //Another name for the debugger
        public Debug Dbg
        {
            get { return CDebug; }
        }

        //returns the name of a file from the given full pathway
        public string FileName(string fullName)
        {
            string fnm, fpt;
            SplitFileName(fullName, out fpt, out fnm);
            return fnm;
        }

        //returns the directory path of a file from the given full pathway
        public string FilePath(string fullName)
        {
            string fnm, fpt;
            SplitFileName(fullName, out fpt, out fnm);
            return fpt;
        }

        //returns the name of a file w/o extention from the given full pathway
        public string FileShortName(string fullName)
        {
            string fnm, fpt;
            int i;
            SplitFileName(fullName, out fpt, out fnm);
            i = fnm.LastIndexOf(".");
            //SIN204 if (i != 0)
            if (i != -1)    //SIN204
            {
                fnm = StringUtils.Left(fnm, i - 1);
            }
            return fnm;
        }

        //returns the extention of a file from the given full pathway
        public string FileType(string fullName)
        {
            string fnm, fpt;
            int i;
            SplitFileName(fullName, out fpt, out fnm);
            i = fnm.LastIndexOf(".");
            //SIN204 if (i != 0)
            if (i != -1)    //SIN204
            {
                fnm = StringUtils.Mid(fnm, i + 1);
            }
            return fnm;
        }

        //splits a full pathway to the path and file
        public void SplitFileName(string fullName, out string fpath, out string fname)
        {
            string[] sArr;
            CDebug.PushProc("SplitFileName");
            char[] splitChar = { '\\' };
            sArr = fullName.Split(splitChar);
            //switch (StringUtils.UBound(sArr))
            switch (sArr.GetUpperBound(0))
            {
                case 0:
                    fname = sArr[0];
                    fpath = "";
                    break;
                case 1:
                    fpath = sArr[0];
                    fname = sArr[1];
                    break;
                default:
                    //fname = sArr[StringUtils.UBound(sArr)];
                    fname = sArr[sArr.GetUpperBound(0)];
                    //sArr[StringUtils.UBound(sArr)] = "";
                    sArr[sArr.GetUpperBound(0)] = "";
                    fpath = String.Join("\\",sArr);
                    break;
            }
            CDebug.PopProc();
        }

        //creates a full pathway given a path and file name
        public string JoinFileName(string fpath, string fname)
        {
            CDebug.PushProc("JoinFileName");
            fpath = fpath.Trim();
            fname = fname.Trim();

            if (fpath != "" && fpath != null)
            {
                if (StringUtils.Right(fpath, 1) != "\\")
                {
                    CDebug.PopProc();
                    return fpath + "\\" + fname;
                }
                else
                {
                    CDebug.PopProc();
                    return fpath + fname;
                }
            }
            CDebug.PopProc();
            return fname;
        }

        //Overloaded for optional open retry number
        public FileStream FileOpen(string fldrname, string FileName, cFileAccess opnoption, bool bConfirm)
        {
            return FileOpen(fldrname, FileName, opnoption, bConfirm, 0);
        }

        //Opens a file for read or write access
        public FileStream FileOpen(string fldrname, string FileName, cFileAccess opnoption, bool bConfirm, int iOpenRetry)
        {
            string sFullName;
            bool bExists;
            DialogResult resp;
            int irt;
            int irtc;


            CDebug.PushProc("FileOpen");

            FileClose();
            sFullName = JoinFileName(fldrname, FileName);

            if (sFullName != "" && sFullName != null)
            {
                bExists = File.Exists(sFullName);

                switch (opnoption)
                {
                    case cFileAccess.enFAForReading:
                        if (!bExists)
                        {
                            CDebug.Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_NOTEXISTS, MessageBoxButtons.OK, "File Not Found for Reading", "", sFullName);
                            CDebug.PopProc();
                            return m_tst;
                        }
                        break;
                    case cFileAccess.enFAForWriting:
                        if (bExists && CDebug.Interactive && !CDebug.IgnoreErrors && !bOverWrite && bConfirm)
                        {
                            resp = CDebug.Errors.ProcessAppError(ErrorGlobalConstants.ERRC_PARAM_FILE_WRITE_EXISTS, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, "", "", sFullName);
                            if (resp == DialogResult.Cancel)
                            {
                                CDebug.PopProc();
                                return m_tst;
                            }
                        }
                        break;
                }
                irtc = 0;
                do
                {
                    try
                    {
                        switch (opnoption)
                        {
                            case cFileAccess.enFAForReading:
                                m_tst = File.Open(sFullName, FileMode.Open, FileAccess.Read);
                                m_trd = new StreamReader(m_tst);
                                break;
                            case cFileAccess.enFAForWriting:
                                m_tst = File.Open(sFullName, FileMode.OpenOrCreate, FileAccess.Write);
                                m_twr = new StreamWriter(m_tst);
                                break;
                            case cFileAccess.enFAForAppending:
                                m_tst = File.Open(sFullName, FileMode.Append, FileAccess.Write);
                                m_twr = new StreamWriter(m_tst);
                                break;
                        }
                        m_file = new FileInfo(sFullName);
                        curFilePath = sFullName;
                        break;
                    }
                    catch (Exception e)
                    {
                        irt = OpenRetry;
                        if (iOpenRetry != 0)
                            irt = iOpenRetry;
                        if (irtc >= irt)
                        {
                            DialogResult rsp;
                            if ((fldrname == CDebug.LogPath) && (FileName == CDebug.LogFile))
                            {
                                CDebug.LogPath = Directory.GetCurrentDirectory();
                                CDebug.LogFile = CDebug.LogFile + ".Log";
                            }
                            rsp = CDebug.Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE,
                                                                 MessageBoxButtons.AbortRetryIgnore,
                                                                 "Error Type:" + e.GetType() + "\r\n" +
                                                                 e.Message + "\r\n" +
                                                                 "Source:" + e.Source, null, sFullName);
                            FileClose();
                            if (rsp == DialogResult.Abort)
                                break;
                        }
                        irtc++;
                        for (int i = 0; i < 10000; i++) { }   //wait a short time
                    }
                } while (true);
            }
            CDebug.PopProc();
            return m_tst;
        }

        //checks if a file exists on a given path
        public bool FileExists(string sFilePath, string sFileName)
        {
            string sFullName;

            CDebug.PushProc("FileExists");
            sFullName = JoinFileName(sFilePath, sFileName);

            if (sFullName != "" && sFullName != null)
            {
                CDebug.PopProc();
                return File.Exists(sFullName);
            }
            CDebug.PopProc();
            return false;
        }

        //checks if a directory exists
        public bool FolderExists(string fldrname)
        {
            CDebug.PushProc("FolderExists");
            if (fldrname != "" && fldrname != null)
            {
                CDebug.PopProc();
                return Directory.Exists(fldrname);
            }
            CDebug.PopProc();
            return false;
        }

        //closes the current file
        public void FileClose()
        {
            try
            {
                if (m_tst != null)
                    m_tst.Close();
                m_file = null;
                curFilePath = "";
                m_tst = null;
                //Start SIN204
                //if (m_trd != null)
                //    m_trd.Close();
                m_trd = null;
                //if (m_twr != null)
                //    m_twr.Close();
                //EndSIN204
                m_twr = null;
            }
            catch { }
        }

        //Reads a line from the current file
        public string FileRead()
        {
            string sInLine = "";
            string spn = "";
            
            try
            {
                spn = "FileRead";
                CDebug.PushProc(spn);
                sInLine = "";
                if (m_tst != null)
                {
                    if (!m_trd.EndOfStream)
                    {
                        sInLine = m_trd.ReadLine();
                    }
                }
                else
                {
                    CDebug.Errors.PostError(ErrorGlobalConstants.ERRC_FILE_READ_GENERAL, spn);
                }
            }
            catch (Exception e)
            {
                CDebug.Errors.ProcessError(ErrorGlobalConstants.ERRC_FILE_READ_GENERAL, spn, e);
            }
            CDebug.PopProc();
            return sInLine;
        }

        //Writes a line to the current file
        public void FileWrite(string sOutLine)
        {
            string spn = "FileWrite";
            try
            {
                CDebug.PushProc(spn);
                if (m_twr != null)
                {
                    m_twr.WriteLine(sOutLine);
                    m_twr.Flush();
                }
                else
                {
                    CDebug.Errors.PostError(ErrorGlobalConstants.ERRC_FILE_WRITE_GENERAL, spn);
                }
            }
            catch (Exception e)
            {
                CDebug.Errors.ProcessError(ErrorGlobalConstants.ERRC_FILE_WRITE_GENERAL, spn, e);
            }
            CDebug.PopProc();
        }

        //Since File is a static class and cannot be returned for direct access, returns
        //the full pathway of the current file for use in the static methods of File
        public FileInfo FileFile
        {
            get { return m_file; }
        }

        //returns the stream reader or writer depending on which is instantiated.
        public Object FileTextStream
        {
            get
            {
                if (m_trd == null)
                    return m_twr;
                else
                    return m_trd;
            }
        }

        //sets the current file and returns the full pathway
        public FileInfo GetFile(string fpath, string fname)
        {
            string sFullName;
            sFullName = JoinFileName(fpath, fname);
            m_file = new FileInfo(sFullName);
            curFilePath = JoinFileName(fpath, fname);
            return m_file;
        }

        //returns the errors instance used by this object
        public Errors CErrors
        {
            get { return CDebug.Errors; }
        }

        //sets or returns bool on allowing overwriting
        public bool OverWriteFile
        {
            get { return bOverWrite; }
            set { bOverWrite = value; }
        }

        //gets or sets the number of times to retry opening a file
        public int OpenRetry
        {
            get { return m_OpenRetry; }
            set
            {
                m_OpenRetry = value;
            }
        }

        //deletes the given file, throws error if there is a problem with deleting
        //modified to be void instead of return bool
        public void FileDelete(string fpath, string fname)
        {
            string sFullName = "";
            try
            {
                sFullName = JoinFileName(fpath, fname);
                if(FileExists(fpath,fname))
                    if(fname == "" || fname == null)
                        Directory.Delete(sFullName,true);
                    else
                        File.Delete(sFullName);
            }
            catch (Exception e)
            {
                e.Source = "CCP.Common.FileFunctions.FileDelete." + e.Source + "\r\n" + "File:" + sFullName;
                throw e;
            }
        }

        //Copies a file from one location to another possibly with a different name.
        //modified for void
        public void FileCopy(string fsrcpath, string fsrcname, string ftrgpath, string ftrgname)
        {
            string ssrc = "";
            string strg = "";
            try
            {
                ssrc = JoinFileName(fsrcpath, fsrcname);
                strg = JoinFileName(ftrgpath, ftrgname);
                if (FileExists(fsrcpath, fsrcname))
                    if (fsrcname == "" || fsrcname == null)
                        Copy(ssrc, strg);
                    else
                        File.Copy(ssrc, strg, true);
                else
                    throw new FileNotFoundException("Input File Not Found");
            }
            catch(Exception e)
            {
                e.Source = "CCP.Common.FileFunctions.FileCopy." + e.Source + "\r\n" + "Input:" + ssrc + "\r\n" + "Output:" + strg;
                throw e;
            }
        }

        //Renames the given file
        //modified for void
        public void FileRename(string fsrcpath, string fsrcname, string ftrgpath, string ftrgname)
        {
            string ssrc = "";
            string strg = "";
            try
            {
                ssrc = JoinFileName(fsrcpath, fsrcname);
                strg = JoinFileName(ftrgpath, ftrgname);
                if (FileExists(fsrcpath, fsrcname))
                {
                    FileCopy(fsrcpath, fsrcname, ftrgpath, ftrgname);
                    FileDelete(fsrcpath, fsrcname);
                }
            }
            catch (Exception e)
            {
                e.Source = "CCP.Common.FileFunctions.FileRename." + e.Source + "\r\n" + "Input:" + ssrc + "\r\n" + "Output:" + strg;
                throw e;
            }
        }

        //Returns the full pathway for a file in a temporary directory
        public string GetTempFileName(string sFileName, string sFileExt)
        {
            if (sFileName == "" || sFileName == null)
                sFileName = CDebug.AppTitle;
            if (sFileExt != "" && sFileExt != null)
            {
                if (StringUtils.Left(sFileExt, 1) != ".")
                    sFileExt = "." + sFileExt;
            }
            else
            {
                sFileExt = ".ini";
            }
            return Path.GetTempPath() + "\\" + sFileName + sFileExt;
        }

        //Returns a list of all the files with full path in an ArrayList in the directory with given parameters
        //Modified to eliminate need for DirList function
        public ArrayList FileList(string sDir, string sFileSpec)
        {
            string[] sArr = Directory.GetFiles(sDir,sFileSpec);
            ArrayList ret = new ArrayList(sArr.Length);
            for (int i = 0; i < sArr.Length; i++)
            {
                ret.Add(new FileInfo(sArr[i]));
            }
            return ret;
        }

        //used by FileCopy for directory copying
        private void Copy(string ssrc, string strg)
        {
            DirectoryInfo src = new DirectoryInfo(ssrc);
            DirectoryInfo trg = new DirectoryInfo(strg);
            CopyAll(src, trg);
        }

        //used by FileCopy for directory copying
        private void CopyAll(DirectoryInfo src, DirectoryInfo trg)
        {
            if (Directory.Exists(trg.FullName) == false)
            {
                Directory.CreateDirectory(trg.FullName);
            }
            foreach (FileInfo file in src.GetFiles())
            {
                file.CopyTo(JoinFileName(trg.ToString(), file.Name), true);
            }
            foreach (DirectoryInfo subdir in src.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = trg.CreateSubdirectory(subdir.Name);
                CopyAll(subdir, nextTargetSubDir);
            }
        }

        //Start SIN204
        public string GenerateUniqueFileName(string p_sFileNamePrefix, string p_sFileNameSuffix)
        {
            string sReturnFileName = "";

            if (p_sFileNamePrefix.Trim() != "")
                sReturnFileName += p_sFileNamePrefix;

            sReturnFileName += DateTime.Now.ToString("yyyyMMddhhmmssffffff");

            if (p_sFileNameSuffix.Trim() != "")
                sReturnFileName += p_sFileNameSuffix;

            return sReturnFileName;
        }
        //End SIN204
    }
}
