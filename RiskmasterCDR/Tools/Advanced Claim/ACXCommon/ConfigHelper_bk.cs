﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Globalization;
using System.Collections;
using System.Web;
using System.Windows.Forms;
using System.Xml;

namespace CCP.Common
{
    public class ConfigHelper
    {
        #region Private Variables

        private static string m_sConfigFilePath;
        private static string m_sConfigSection;
        private static IDictionary appSettingsDictionary;

        #endregion

        #region Static Constructor
        static ConfigHelper()
        {
            m_sConfigFilePath = string.Empty;
            m_sConfigSection = string.Empty;
            appSettingsDictionary = null;
        }
        #endregion

        #region Public Properties
        
        public static string ConfigFilePath
        {
            set {  m_sConfigFilePath = value; }
        }

        public static string ConfigSection
        {
            set { m_sConfigSection = value; }
        }

        #endregion

        public static string GetValue(string sKey)
        {
            if (appSettingsDictionary == null)
            {
                AppSettings();
            }
            if (appSettingsDictionary != null)
            {
                if (appSettingsDictionary[sKey] != null)
                {
                    return Convert.ToString(appSettingsDictionary[sKey]);
                }
                else
                {
                    throw new Exception("Configuration Error : Value for the key " + sKey + " is missing.");
                }
            }
            else
            {
                throw new Exception("Configuration Error : No settings loaded.");
            }
        }

        private static void AppSettings()
        {
            if (appSettingsDictionary == null)
            {
                try
                {
                    XmlDocument configXml = new XmlDocument();
                    XmlTextReader configXmlReader = new XmlTextReader(m_sConfigFilePath);
                    configXml.Load(configXmlReader);
                    configXmlReader.Close();
                    XmlNodeList configNodes = configXml.GetElementsByTagName(m_sConfigSection);
                    foreach (XmlNode configNode in configNodes)
                    {
                        if (configNode.LocalName == m_sConfigSection)
                        {
                            DictionarySectionHandler handler = new DictionarySectionHandler();
                            appSettingsDictionary = (IDictionary)handler.Create(null, null, configNode);
                        }

                    }
                }
                catch
                {
                    throw new Exception("Configuration Error : Error while loading configuration settings.");
                }
            }
        }
    }
}
