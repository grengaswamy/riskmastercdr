********************************************************************************************************
* SI4498    1/20/2004  mrb 16051  RX Fixes
* SI4555    3/04/2004  mrb 16051  Error Display for IX
* SI4651   10/01/2004  mrb 16051  Externalized Word Merge
* SI5068    4/25/2005  mrb 16051  Security Manager Errors
* SI05230   6/6/2005   kcb 17681  Collection Errors
* SI05308   7/13/2005  mrb        Generate CRC Constant
* SI05219   4/13/2005  mrb(16051) disable scripting: Relabeling
* SI05495   2/16/2006  mrb(16051) XML Error Reporting
* SI04107   5/26/2006  mrb        Batch Check Print
* si05593   9/8/2006   Lau        Min password length
* SI06023   04/28/2008 sw         No Event for export
* SI05428   04/28/2008 sw         Security Messages
********************************************************************************************************
* NOTE:  the ERRC values must match to the items in CCPUF->Error_Codes.bas
* NOTE:  DO NOT USE commas in descriptions.

* <%x> represent replaceable parameters.  Up to 4 can be specified
* the use of replaceable parameters must be coordinated with the processing of the error message in code

* Format of error line:
*   Error symbolic name, Internal Error Code, Help Reference number, Error description
*   Help Reference Number is expected to be within the context of the applications help file

* No Description
ERRC_NO_DESCRIPTION, 0, 0,
ERRC_ERROR, 1, 1, An Application Error has Occurred.  Check the debug log.
ERRC_ABORTED, 2, 2, Function Aborted
ERRC_PERMISSION, 3, 3, Do not have permission to execute this application.<CRLF>Application: <%1><CRLF>Function: <%2>
ERRC_COMPLETE, 4, 4, Process Complete: <%1>
*SI04107
ERRC_UCS_INITIALIZATION, 5, 5, Could not Initialize <%1> Extensions (<%2>).<CRLF>You won't be able to use some key product features.

* General text and messages
ERRC_OK_CANCEL, 10, 10, OK to Continue or Cancel to Abort
ERRC_SKIP_ITEM, 11, 11, Item is Skipped

*SI05068
ERRC_DATA_REQUIRED, 60, 60, Data Required for: <%1>
ERRC_VERIFY_DELETE, 61, 61, Are you sure you want to delete selected item?
ERRC_VERIFY_SAVE, 62, 62, Do You Want To Save Your Changes?<CRLF>Yes to Save or No to Abort
*Start SI05495
ERRC_CODE_LOOKUP, 63, 63, Short Code "<%2>" not found in Code Table <%1>
ERRC_STATE_LOOKUP, 64, 64, State Code "<%1>" invalid
ERRC_TABLE_LOOKUP, 65, 65, Table "<%1>" is not a valid table
*End SI05495

* XML Processing Errors
ERRC_XML_ERRORS, 100, 100, XML Errors
ERRC_NO_DOCUMENT, 101, 101, No COM Document Setup:
ERRC_NODE_NOT_FOUND, 102, 102, Node Not Found:
ERRC_NO_NODE, 103, 103, No Node:
ERRC_ATTRIBUTE_NOT_FOUND, 104, 104, Attribute Not Found:
ERRC_INVALID_NODE, 105, 105, Node not valid for operation: 
ERRC_INVALID_VALUE, 106, 106, Value specified is invalid: '<%1>'
ERRC_NO_NODE_NAME, 107, 107, Missing Node Name:
ERRC_DOCUMENT_LOAD_FAILED, 108, 108, Error loading the XML Document: <%1>
ERRC_DOCUMENT_SAVE_FAILED, 109, 109, Error saving the XML Document: <%1>
ERRC_INVALID_OPERATION, 110, 110, Operation Invalid for this Data Type
ERRC_NODE_COUNT_DIFFERENCE, 111, 111, Difference in Node Processing Count: Node=<%1>: Submitted=<%2>: Processed=<%3>
ERRC_DATA_VALIDATION_ERROR, 112, 112, Data Validation Error: Expected=<%1>: Found=<%2>
* Start SI03640
ERRC_INVALID_TIME_VALUE, 113, 113, Time Value Submitted is not valid:  Node=<%1>:  Time=<%2>
ERRC_INVALID_DATE_VALUE, 114, 114, Date Value Submitted is not valid:  Node=<%1>:  Date=<%2>
* End SI03640
* Start SI03799
ERRC_NEED_PARENT_COMP, 115, 115, Component <%1> requires a parent before processing
*Start SI05495
ERRC_MISSING_ELEMENT_REF, 116, 116, Missing element(<%1>) referenced by <%2> in <%3>:<%4>
*End SI05495

* Parameter File Errors

ERRC_PARAM_FILE, 1000, 1000, General Parameter File Error
ERRC_PARAM_FILE_READ_GENERAL, 1001, 1001, Error reading the parameter file:
ERRC_PARAM_FILE_WRITE_GENERAL, 1002, 1002, Error writing to the parameter file:
ERRC_PARAM_FILE_READ_NOTEXISTS, 1004, 1004, The requested Parameter File '<%1>' does not exist:
ERRC_PARAM_FILE_WRITE_EXISTS, 1005, 1005, The requested Parameter File '<%1>' already exists:

* File Processing Errors
ERRC_FILE, 1020, 1020, File Processing Error: File=<%1>
ERRC_FILE_READ_GENERAL, 1021, 1021, File Access Error for Reading: <%1>
ERRC_FILE_WRITE_GENERAL, 1022, 1022, File Access Error for Writing: <%1>
ERRC_FILE_NOTEXISTS, 1023, 1023, File Does Not Exist: <%1>
ERRC_FILE_EXISTS, 1024, 1024, File Already Exists: <%1>
ERRC_FILE_NONAME, 1025, 1025, Missing File Name:
ERRC_FILE_NO_FOLDER, 1026, 1026, Folder '<%1>' Does Not Exist
ERRC_FILE_BLANK_REC, 1027, 1027, Blank Record Read from File <%1>
ERRC_FILE_READ_ONLY, 1028, 1028, File <%1> is marked as read only
ERRC_FILE_SAVE_SUCCESSFUL, 1029, 1029, File <%1> Successfully Saved
ERRC_FILE_SAVE_UNSUCCESSFUL, 1030, 1030, File <%1> NOT Saved

* Parameter Errors

ERRC_PARAMETER_INVALID, 1010, 1010, :Invalid Input Parameter '<%1>':

* Merge Form Errors

ERRC_MF, 1200, 1200, Merge Form Processing Error:
ERRC_MF_FIELD_DB, 1201, 1201, Merge Form Field Extract Error: Form <%1>:<%2>; Field <%3>:<%4>
ERRC_MF_FILE_COLLISION, 1202, 1202, Attempting to add a form with the same file name as an existing form:  Form=<%1>:<%2>; File Name=<%3>
*SI04651
ERRC_MF_NO_RECORDS_FOUND, 1203, 1203, No records found for document merge.<crlf>Form=<%1>
ERRC_MF_DATA_MISSING, 1204, 1204, Data Missing for <%1> type of Merge.<crlf>Required information is <%2>: <%3>: <%4>.

* Import Processing Errors

ERRC_IMP_ERRORS, 1250, 1250, Import Processing Errors
ERRC_IMP_CSV_FLD_CNT, 1251, 1251, Field Count parsed '<%1>' does not match Field Count specified '<%2>'
ERRC_IMP_NO_CLAIM, 1252,1252, Component being processed requires claim or claim ID: Component:<%1>: Claim Number:<%2>
ERRC_IMP_NO_COMPONENT, 1253, 1253, Invalid Additional Data Component Definition: Component ID=<%1>: Component Name=<%2>
ERRC_IMP_SUPP_DEFINED, 1254, 1254, Invalid Supplemental Field / Additional Data Definition: Component ID=<%1>: Component Name=<%2>: Item Name=<%3>: Item Field=<%4>
ERRC_IMP_INCOMPAT_DATA_TYPE, 1255, 1255, Additional Data Data Type Incompatible with Database: Component ID=<%1>: Component Name=<%2>: Item Name=<%3>: Item Field=<%4>: DataType=<%5>
ERRC_IMP_FF_RECLEN, 1256, 1256, Record shorter than field specifications for record being processed: RType=<%1> FieldSpec=<%2> RecLen=<%3>
ERRC_IMP_NO_CLAIMANT, 1257, 1257, Import requires Claimaint: Claim:<%1> Component:<%2>:<%3>
ERRC_IMP_UNK_INV_IMPORT, 1258, 1258, Unknown Type of Invoice Import.  Consult Opertions Manuals. Defined as 'IMPORT_TYPE' Parameter in input file: Found <%1>
ERRC_IMP_NO_IMPORT_SWITCH, 1259, 1259, No Import Processing Specified. /IMPORT=? or <%2>\CCPIXImport.DAT=[<%1>]. 
* SI03896
ERRC_IMP_MISSING_REFERENCE, 1260,1260,Reference ID required for <%1> in <%2>.
ERRC_IMP_OH_PARENT_EID, 1261, 1261, Entity <%1> has Parent <%2> that does not match expected: <%3>.
ERRC_IMP_OH_TABLE_ID, 1262, 1262, Entity <%1> Table ID <%2> does not match expected: <%3>.
ERRC_IMP_BANK_CODE, 1263, 1263, Bank Code <%1> is invalid.  Assigning to Bank Code <%2>.
ERRC_IMP_VCH_MISMATCH, 1264, 1264, Data for <%1> does not match. Submitted=<%2>.  Found=<%3>.
ERRC_IMP_RECORD_NOT_FOUND, 1265, 1265, Record not found for Update: "<%1>".

* Database Access Errors
ERRC_DB_ERROR, 1270, 1270, General Database Error: Database=<%1>
ERRC_DB_NO_DB, 1271, 1271, No Database Name Specified
ERRC_DB_NO_USER, 1272, 1272, No User Name Specified
ERRC_DB_LOOKUP_ERROR, 1273, 1273, Database Lookup Error
ERRC_DB_LOGIN, 1274, 1274, Login Error.  Check UserId and Password.<crlf>Database=<%1><crlf>UserID=<%2)
*SI05219
ERRC_DB_NO_DB_OPEN, 1275, 1275, No Database Logged In to for Processing
*SI05869
ERRC_DB_LOGOFF_CONFIRM, 1276, 1276, Are you sure you want to Logoff from <%1>?

* Conversion Errors
ERRC_CNV_NO_CLAIM, 1280, 1280, No Claims Found for Conversion: <%1>
ERRC_CNV_RQRD_DATA, 1281, 1281, Data Required for Conversion: <%1>
ERRC_CNV_INVALID_CLASS, 1282, 1282, Class for Conversion Not Found: <%1>
ERRC_CNV_INVALID_PAY_STATUS, 1283, 1283, Payments of Status '<%1>' for Claim <%2> Transaction <%3> can not be converted.
ERRC_CNV_NO_DRFTFILE_REC, 1284, 1284, Payment Record has no Draft File Entry for Claim <%1> Transaction <%2> can not be converted.

* Claim Query Errors
ERRC_CQ_ERROR, 1300, 1300, General Query Request Error
ERRC_CQ_NO_CLAIM, 1301, 1301, Claim Not Found: <%1>.<%2>
ERRC_CQ_NO_EVENT, 1302, 1301, Event Not Found: <%1>.<%2>
* SI06023
ERRC_CQ_NO_DIARY, 1303, 1303, No Diaries Found: <%1>.<%2>.<%3>.<%4>
ERRC_CQ_NO_PARAMS, 1304, 1304, No Search Parameters Specified.

* Processing Errors
ERRC_PROCESS_ABORTED, 1500, 1500, Process Aborted: Reason=<%1>
ERRC_OUT_OF_BALANCE, 1501, 1501, Out Of Balance Error: Type=<%1>: Field=<%2>: Parsed Value '<%3>' does not match Control Value '<%4>'
ERRC_GUID_NOTEXISTS, 1502, 1502, Restart or Rerun: Unique File ID <%1> Does Not Exist in Run Log 
ERRC_GUID_EXISTS, 1503, 1503, Unique File ID <%1> Already Exists in Run Log 
ERRC_GUID_DIFFERENCE, 1504, 1504, Difference in Unique File ID: Parameter=<%1>: Document=<%2>
ERRC_RESTART_NOTEXISTS, 1505, 1505, Restart: Field=<%1> Value=<%2>: Does Not Exist in File
*SI05308
ERRC_CRC_MISMATCH, 1506, 1506, CRC Constant Mismatch indicates data integrity failure: Table=<%1>: Record=<%2>

* Scripting Errors  SI03722
ERRC_SCRIPT_ERROR, 1540, 1540, Error Occurred executing script: Number=<%1> Source=<%2> Line#=<%3> CharPos=<%4>
ERRC_SCRIPT_FILE_NOTFOUND, 1541, 1541, Script file <%1> not found
ERRC_SCRIPT_PROCEDURE_NOTFOUND, 1542, 1542, Procedure <%1> not found in script
ERRC_SCRIPT_ENGINE_LOAD, 1543, 1543, Error occurred loading the script engine and script
ERRC_SCRIPT_EXECUTION, 1544, 1544, Error attempting to execute a script

*' Collection errors SI#05230
ERRC_KEY_IS_NOT_UNIQUE, 35602, 35602, Key is not unique in collection

* RX Processing Errors SI04498
ERRC_RX_INTEGER_RQRD, 1601, 1601, Value for <%1> in Switch specified must be an integer

* RX Processing Errors SI04555
ERRC_OBJ_ERROR, 1700, 1700, Error occured processing business object <%1>
ERRC_OBJ_SAVE, 1701, 1701, Error saving object <%1>
ERRC_OBJ_READ, 1702, 1702, Error reading object <%1>

*Security Errors	SI05068
ERRC_SECURITY_ERRORS, 1800, 1800, 1800 - 1829 Reserved for Security Errors
ERRC_SEC_NO_DOCUMENT_PATH, 1801, 1801, Data Source Document Path is not set.<CRLF>You won't be able to use any Feature related to User Documents/Attachments.
ERRC_SEC_INVALID_LICENSE_CODE, 1802, 1802, You have entered an incorrect code.<CRLF>Please verify you have entered the code correctly.<CRLF>Please contact product support if you need assistance.
ERRC_SEC_SPACESIN_LOGINNAME, 1803, 1803, Login Name may not contain space(s).
ERRC_SEC_DATABASE_ERROR, 1804, 1804, Database Connection could not be edited.<CRLF>This indicate corrupted Database Connection record.
ERRC_SEC_PASSWORD_MISMATCH, 1805, 1805, Passwords Do No Match.  Re-Enter.
ERRC_SEC_DSN_ALREADY_DEFINED, 1806, 1806, Selected Data Source name: {<%1>} is already in use.
ERRC_SEC_LOGIN_ALREADY_DEFINED, 1807, 1807, User with Login Name {<%1>} already exists.<CRLF>Please type another Login Name.
ERRC_SEC_USER_ALREADY_DEFINED, 1808, 1808, User <%1> already exist.<CRLF>Please change Last and/or First name and try again.
ERRC_SEC_GROUP_ALREADY_DEFINED, 1809, 1809, Group with same name: {<%1>} is already defined.
ERRC_SEC_CHANGING_PASSWORD, 1810, 1810, You are about to change Security Management System login parameters.<CRLF>Please remember your new login name and password.
ERRC_SEC_PASSWORD_CHANGED, 1811, 1811, Password Successfully Changed !!
ERRC_SEC_LOGIN_INVALID, 1812, 1812, Login Information is Invalid
ERRC_SEC_LICENSE_CORRUPT, 1813, 1813, The license information has been improperly altered or is corrupt.<CRLF>Please contact product support for a re-activation code.
ERRC_SEC_WARNING_DAYS, 1814, 1814, # of Warning Days: <%1> before expiry of Password must be less than # of Password Expiry days: <%2>.
ERRC_SEC_CLOSE_WARNING, 1815, 1815, Close Database Connection Wizard and lose any changes?
ERRC_SEC_MIN_LENGTH, 1816, 1816, The Minimum password length cannot be less than 1
*SI05428
ERRC_SEC_VERIFY_LOGINNAME, 1817, 1817, Verify User Login Name: {<%1>}.<CRLF>Once User Definition is saved the Login Name can not be changed.
